package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum IssuerBlockRequestMotiveType {
		
		INCUMPLIMIENTO_PAGO_DERECHOS (Integer.valueOf(73),"INCUMPLIMIENTO PAGO DE DERECHOS"), 
		INCUMPLIMIENTO_CONTRATO (Integer.valueOf(74),"INCUMPLIMIENTO DEL CONTRATO"),
		INCUMPLIMIENTO_REGLAMENTO_CEVALDOM (Integer.valueOf(75),"INCUMPLIMIENTO REGLAMENTO CEVALDOM"),
		ORDER_SUPERINTENDENCIA (Integer.valueOf(76),"POR ORDEN DE LA SUPERINTENDENCIA DE VALORES"),
		OTHERS (Integer.valueOf(76),"OTROS");
		
		private Integer code;
		private String value;
		

		public static final List<IssuerBlockRequestMotiveType> list = new ArrayList<IssuerBlockRequestMotiveType>();
		public static final Map<Integer, IssuerBlockRequestMotiveType> lookup = new HashMap<Integer, IssuerBlockRequestMotiveType>();

		static {
			for (IssuerBlockRequestMotiveType s : EnumSet.allOf(IssuerBlockRequestMotiveType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private IssuerBlockRequestMotiveType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static IssuerBlockRequestMotiveType get(Integer code) {
			return lookup.get(code);
		}

		
}
