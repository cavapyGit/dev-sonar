package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum PepChargeType {
	
		HEAD_OF_STATE(Integer.valueOf(121),"PRESIDENTE / VICEPRESIDENTE DE LA REPUBLICA"),
		ELECTED_OFFICIAL_FOR(Integer.valueOf(804),"SENADOR"),
		DIPUTADO(Integer.valueOf(2780),"DIPUTADO"),
		GOBERNADOR(Integer.valueOf(2781),"GOBERNADOR / MIEMBRO DE LA JUNTA DEPARTAMENTAL"),
		INTENDENTE(Integer.valueOf(2782),"INTENDENTE / MIEMBRO DE LA JUNTA MUNICIPAL"),
		APPOINTED_OFFICER_EP(Integer.valueOf(805),"MINISTRO / VICEMINISTRO DEL PODER EJECUTIVO"),
		DEPENDIENTE(Integer.valueOf(2783),"DEPENDIENTE DEL PODER EJECUTIVO"),
		MINISTRO_CSJ(Integer.valueOf(2784),"MINISTRO DE LA CSJ"),
		JUDGE(Integer.valueOf(806),"JUEZ"),
		CONSEJO(Integer.valueOf(2785),"CONSEJO DE LA MAGISTRATURA"),
		FISCAL(Integer.valueOf(2786),"FISCAL O AGENTE FISCAL"),
		CONTRALOR(Integer.valueOf(2787),"CONTRALOR / PROCURADOR"),
		DEFENSOR_PUEBLO(Integer.valueOf(2788),"DEFENSOR DEL PUEBLO"),
		GERENTE_ENTES(Integer.valueOf(2789),"PRESIDENTE / DIRECTORIO / GERENTE DE ENTES DESCENTRALIZADOS"),
		SENIOR_MILITARY(Integer.valueOf(807),"FUERZAS MILITARES"),
		ENTIDADES_BINACIONALES(Integer.valueOf(2790),"ENTIDADES BINACIONALES"),
		EMBAJADOR(Integer.valueOf(2791),"EMBAJADOR / CONSUL"),
		POLITICAL_LEADER(Integer.valueOf(808),"PRESIDENTE / MIEMBRO DE PARTIDO POLITICO"),
		RECTOR(Integer.valueOf(2792),"RECTOR / DECANO / SECRETARIO DE UNIVERSIDAD NACIONAL"),
		ADUANAS(Integer.valueOf(2793),"DIRECCION NACIONAL DE ADUANAS"),
		ADM_FINANZAS(Integer.valueOf(2794),"UNIDAD DE ADM Y FINANZAS DE ORGANISMOS O ENTIDADES DEL ESTADO"),

		;
		
		private Integer code;
		private String value;
		

		public static final List<PepChargeType> list = new ArrayList<PepChargeType>();
		public static final Map<Integer, PepChargeType> lookup = new HashMap<Integer, PepChargeType>();

		static {
			for (PepChargeType s : EnumSet.allOf(PepChargeType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private PepChargeType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static PepChargeType get(Integer code) {
			return lookup.get(code);
		}

		
}
