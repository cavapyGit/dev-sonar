package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
/**
* <ul><li>Copyright EDV 2020.</li></ul> 
* The Class ApplicantConsultRequest.
* @author RCHIARA.
*/
@Entity
@Table(name="APPLICANT_CONSULT_REQUEST")
public class ApplicantConsultRequest implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="APPLICANT_CONSREGGEN", sequenceName="SECID_APPLICANTCONSREQPK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="APPLICANT_CONSREGGEN")
	@Column(name="ID_APPLICANT_CONS_REQ_PK")
	private Long idApplicantConsRegPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_APPLICANT_CONS_FK",referencedColumnName="ID_APPLICANT_CONS_PK")
	private ApplicantConsult applicantConsult;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;
	
	@Transient
	private Long idHolderFk;
	
	@Column(name="APPLICANT_NUM")
	private String applicantNum;
	
	@Column(name="DOC_TYPE")
	private Integer docType;
	
	@Column(name="DOC_NUM")
	private String docNum;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="ISSUED")
	private String issued;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REG_DATE_HOLDER")
	private Date regDateHolder;
	
	@Lob()
   	@Column(name="PORTFOLIO_REPORT")
   	private byte[] portfolioReport;
	
	@Lob()
   	@Column(name="ATTACHED_FILE")
   	private byte[] attachedFile;
	
	@Column(name="IND_STATE_HOLDER")
	private Integer indStateHolder;
	
	@Column(name="IND_STATE_CONSULT")
	private Integer indStateConsult;
	
	@Column(name="IND_VERIFIED_CONSULT")
	private Integer indVerifiedConsult;	
	
	@Column(name="IND_EDITABLE")
	private Integer indEditable;
	
	@Column(name="IND_CUI_EXIST")
	private Integer indCuiExist;
	
	@Column(name="IND_SEC_VALID")
	private Integer indSecValid;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp =  loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
			if(loggerUser.getAuditTime() != null) {
				lastModifyDate = loggerUser.getAuditTime();
			} else {
				lastModifyDate = objLoggerUser.getAuditTime();
			}
			if(loggerUser.getIpAddress() != null) {
				lastModifyIp = loggerUser.getIpAddress();
			} else {
				lastModifyIp = objLoggerUser.getIpAddress();
			}
			if(loggerUser.getUserName() != null) {
				lastModifyUser = loggerUser.getUserName();
			} else {
				lastModifyUser = objLoggerUser.getUserName();
			}
		} else {
			lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdApplicantConsRegPk() {
		return idApplicantConsRegPk;
	}

	public void setIdApplicantConsRegPk(Long idApplicantConsRegPk) {
		this.idApplicantConsRegPk = idApplicantConsRegPk;
	}

	public ApplicantConsult getApplicantConsult() {
		return applicantConsult;
	}

	public void setApplicantConsult(ApplicantConsult applicantConsult) {
		this.applicantConsult = applicantConsult;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	public String getApplicantNum() {
		return applicantNum;
	}

	public void setApplicantNum(String applicantNum) {
		this.applicantNum = applicantNum;
	}

	public Integer getDocType() {
		return docType;
	}

	public void setDocType(Integer docType) {
		this.docType = docType;
	}

	public String getDocNum() {
		return docNum;
	}

	public void setDocNum(String docNum) {
		this.docNum = docNum;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIssued() {
		return issued;
	}

	public void setIssued(String issued) {
		this.issued = issued;
	}

	public Date getRegDateHolder() {
		return regDateHolder;
	}

	public void setRegDateHolder(Date regDateHolder) {
		this.regDateHolder = regDateHolder;
	}

	public byte[] getPortfolioReport() {
		return portfolioReport;
	}

	public void setPortfolioReport(byte[] portfolioReport) {
		this.portfolioReport = portfolioReport;
	}

	public byte[] getAttachedFile() {
		return attachedFile;
	}

	public void setAttachedFile(byte[] attachedFile) {
		this.attachedFile = attachedFile;
	}

	public Integer getIndStateHolder() {
		return indStateHolder;
	}

	public void setIndStateHolder(Integer indStateHolder) {
		this.indStateHolder = indStateHolder;
	}

	public Integer getIndStateConsult() {
		return indStateConsult;
	}

	public void setIndStateConsult(Integer indStateConsult) {
		this.indStateConsult = indStateConsult;
	}

	public Integer getIndVerifiedConsult() {
		return indVerifiedConsult;
	}

	public void setIndVerifiedConsult(Integer indVerifiedConsult) {
		this.indVerifiedConsult = indVerifiedConsult;
	}

	public Integer getIndEditable() {
		return indEditable;
	}

	public void setIndEditable(Integer indEditable) {
		this.indEditable = indEditable;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getIndCuiExist() {
		return indCuiExist;
	}

	public void setIndCuiExist(Integer indCuiExist) {
		this.indCuiExist = indCuiExist;
	}

	public Integer getIndSecValid() {
		return indSecValid;
	}

	public void setIndSecValid(Integer indSecValid) {
		this.indSecValid = indSecValid;
	}

	public Long getIdHolderPk() {
		if(Validations.validateIsNotNullAndNotEmpty(this.holder)) {
			idHolderFk = holder.getIdHolderPk();
		}
		return idHolderFk;
	}

	public void setIdHolderPk(Long idHolderFk) {
		this.idHolderFk = idHolderFk;
	}
	
	
}
