package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum HolderRequestStateType {
	
	REGISTERED(Integer.valueOf(488),"REGISTRADO"),
	MODIFIED(Integer.valueOf(1211),"MODIFICADO"),
	CONFIRMED(Integer.valueOf(489),"CONFIRMADO"),
	REJECTED(Integer.valueOf(490),"RECHAZADO"),
	ANNULED(Integer.valueOf(491),"ANULADO"),
	APPROVED(Integer.valueOf(1210),"APROBADO");
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;		
	}
			
	private HolderRequestStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static HolderRequestStateType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<HolderRequestStateType> list = new ArrayList<HolderRequestStateType>();
	public static final Map<Integer, HolderRequestStateType> lookup = new HashMap<Integer, HolderRequestStateType>();
	static {
		for (HolderRequestStateType s : EnumSet.allOf(HolderRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
