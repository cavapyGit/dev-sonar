package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the LEGAL_REPRESENTATIVE_FILE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
@Entity
@Table(name="LEGAL_REPRESENTATIVE_FILE")
public class LegalRepresentativeFile implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id legal repre file pk. */
	@Id
	@SequenceGenerator(name="LEGAL_REPRE_FILE_IDLEGALREPREFILEPK_GENERATOR", sequenceName="SQ_ID_LEGAL_REPRE_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LEGAL_REPRE_FILE_IDLEGALREPREFILEPK_GENERATOR")
	
	@Column(name="ID_LEGAL_REPRE_FILE_PK")
	private Long idLegalRepreFilePk;

	/** The description. */
	private String description;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

    /** The file legal repre. */
    @Lob()
    @Column(name="DOCUMENT_FILE")
    @Basic(fetch=FetchType.LAZY)
	private byte[] fileLegalRepre;

	/** The filename. */
	private String filename;

	/** The legal representative. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LEGAL_REPRESENTATIVE_FK",referencedColumnName="ID_LEGAL_REPRESENTATIVE_PK")
	private LegalRepresentative legalRepresentative;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The legal repre file type. */
	@Column(name="LEGAL_REPRE_FILE_TYPE")
	private Integer legalRepreFileType;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state file. */
	@Column(name="STATE_FILE")
	private Integer stateFile;
	
	/** The was modified. */
	@Transient
	private boolean wasModified;


    /**
     * Instantiates a new legal representative file.
     */
    public LegalRepresentativeFile() {
    }

	/**
	 * Gets the id legal repre file pk.
	 *
	 * @return the id legal repre file pk
	 */
	public Long getIdLegalRepreFilePk() {
		return this.idLegalRepreFilePk;
	}

	/**
	 * Sets the id legal repre file pk.
	 *
	 * @param idLegalRepreFilePk the new id legal repre file pk
	 */
	public void setIdLegalRepreFilePk(Long idLegalRepreFilePk) {
		this.idLegalRepreFilePk = idLegalRepreFilePk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the file legal repre.
	 *
	 * @return the file legal repre
	 */
	public byte[] getFileLegalRepre() {
		return this.fileLegalRepre;
	}

	/**
	 * Sets the file legal repre.
	 *
	 * @param fileLegalRepre the new file legal repre
	 */
	public void setFileLegalRepre(byte[] fileLegalRepre) {
		this.fileLegalRepre = fileLegalRepre;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return this.filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the legal representative.
	 *
	 * @return the legal representative
	 */
	public LegalRepresentative getLegalRepresentative() {
		return legalRepresentative;
	}

	/**
	 * Sets the legal representative.
	 *
	 * @param legalRepresentative the new legal representative
	 */
	public void setLegalRepresentative(LegalRepresentative legalRepresentative) {
		this.legalRepresentative = legalRepresentative;
	}

	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the legal repre file type.
	 *
	 * @return the legal repre file type
	 */
	public Integer getLegalRepreFileType() {
		return this.legalRepreFileType;
	}

	/**
	 * Sets the legal repre file type.
	 *
	 * @param legalRepreFileType the new legal repre file type
	 */
	public void setLegalRepreFileType(Integer legalRepreFileType) {
		this.legalRepreFileType = legalRepreFileType;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return this.registryType;
	}

	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	
	/**
	 * Gets the state file.
	 *
	 * @return the state file
	 */
	public Integer getStateFile() {
		return stateFile;
	}

	/**
	 * Sets the state file.
	 *
	 * @param stateFile the new state file
	 */
	public void setStateFile(Integer stateFile) {
		this.stateFile = stateFile;
	}

	/**
	 * Checks if is was modified.
	 *
	 * @return true, if is was modified
	 */
	public boolean isWasModified() {
		return wasModified;
	}

	/**
	 * Sets the was modified.
	 *
	 * @param wasModified the new was modified
	 */
	public void setWasModified(boolean wasModified) {
		this.wasModified = wasModified;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
        if (loggerUser != null) {
        	if(loggerUser.getIdPrivilegeOfSystem() != null) {
        		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        	} else {
        		lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
        	}
        	if(loggerUser.getIdPrivilegeOfSystem() != null) {
        		lastModifyDate = loggerUser.getAuditTime();
        	} else {
        		lastModifyDate = objLoggerUser.getAuditTime();
        	}
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null) {
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}