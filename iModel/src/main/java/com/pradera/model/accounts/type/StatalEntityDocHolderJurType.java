package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum StatalEntityDocHolderJurType {
		
		
		CE(Integer.valueOf(1045),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(1046),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(1047),"PASAPORTE"), 
		LIC(Integer.valueOf(1048),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(1049),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(1050),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(1051),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(1052),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(1053),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(1054),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(1055),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(1056),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(1057),"DECRETO"),		
	    RCF(Integer.valueOf(1058),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(1059),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(1060),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    private Integer code;
		private String value;
		
		public static final List<StatalEntityDocHolderJurType> list = new ArrayList<StatalEntityDocHolderJurType>();
		public static final Map<Integer, StatalEntityDocHolderJurType> lookup = new HashMap<Integer, StatalEntityDocHolderJurType>();

		static {
			for (StatalEntityDocHolderJurType s : EnumSet.allOf(StatalEntityDocHolderJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private StatalEntityDocHolderJurType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static StatalEntityDocHolderJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
