package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum LegalRepresentativeStateType {
	
	REGISTERED(Integer.valueOf(492),"REGISTRADO"),	
	CONFIRMED(Integer.valueOf(493),"CONFIRMADO"),
	REJECTED(Integer.valueOf(494),"RECHAZADO"),
	ANNULLED(Integer.valueOf(495),"ANULADO");
	
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private LegalRepresentativeStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static LegalRepresentativeStateType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<LegalRepresentativeStateType> list = new ArrayList<LegalRepresentativeStateType>();
	public static final Map<Integer, LegalRepresentativeStateType> lookup = new HashMap<Integer, LegalRepresentativeStateType>();
	static {
		for (LegalRepresentativeStateType s : EnumSet.allOf(LegalRepresentativeStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
