package com.pradera.model.accounts;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.blockentity.BlockEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the CROSS_INFORMATION_RESULT database table.
 * 
 */
@Entity
@Table(name="CROSS_INFORMATION_RESULT")
public class CrossInformationResult implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CROSS_INFORMATION_RESULT_IDCROSSINFORMATIONPK_GENERATOR",sequenceName="SQ_ID_CROSS_INFORMATION_PK",initialValue=1,allocationSize=1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CROSS_INFORMATION_RESULT_IDCROSSINFORMATIONPK_GENERATOR")
	@Column(name="ID_CROSS_INFORMATION_PK")
	private Long idCrossInformationPk;

	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	@Column(name="FULL_NAME")
	private String fullName;

 	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_ENTITY_FK")
	private BlockEntity idBlockEntityFk;

 	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder idHolderFk;

	@JoinColumn(name="ID_LEGAL_REPRESENTATIVE_FK")
	@ManyToOne(fetch=FetchType.LAZY)
	private LegalRepresentative idLegalRepresentativeFk;

	@JoinColumn(name="ID_PARTICIPANT_FK")
	@ManyToOne(fetch=FetchType.LAZY)
	private Participant idParticipantFk;
	
	@JoinColumn(name="ID_PNA_PERSON_FK")
	@ManyToOne(fetch=FetchType.LAZY)
	private PnaPerson pnaPersonFk;
	
	@JoinColumn(name="ID_PEP_PERSON_FK")
	@ManyToOne(fetch=FetchType.LAZY)
	private PepPerson pepPersonFk;
	
	@JoinColumn(name="ID_OFAC_PERSON_FK")
	@ManyToOne(fetch=FetchType.LAZY)
	private OfacPerson ofacPersonFk;

	@Column(name="IND_BLOCK_ENTITY")
	private Integer indBlockEntity;

	@Column(name="IND_ISSUER")
	private Integer indIssuer;

	@Column(name="IND_PARTICIPANT")
	private Integer indParticipant;

	@Column(name="NATIONALITY")
	private Integer nationality;

	@Column(name="PERSON_TYPE")
	private Integer personType;

	@Column(name="RESIDENCE_COUNTRY")
	private Integer residenceCountry;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	
	public CrossInformationResult() {
	}

	public Long getIdCrossInformationPk() {
		return this.idCrossInformationPk;
	}

	public void setIdCrossInformationPk(Long idCrossInformationPk) {
		this.idCrossInformationPk = idCrossInformationPk;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Integer getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public BlockEntity getIdBlockEntityFk() {
		return this.idBlockEntityFk;
	}

	public void setIdBlockEntityFk(BlockEntity idBlockEntityFk) {
		this.idBlockEntityFk = idBlockEntityFk;
	}

	public Holder getIdHolderFk() {
		return this.idHolderFk;
	}

	public void setIdHolderFk(Holder idHolderFk) {
		this.idHolderFk = idHolderFk;
	}

	public LegalRepresentative getIdLegalRepresentativeFk() {
		return this.idLegalRepresentativeFk;
	}

	public void setIdLegalRepresentativeFk(LegalRepresentative idLegalRepresentativeFk) {
		this.idLegalRepresentativeFk = idLegalRepresentativeFk;
	}

	public Participant getIdParticipantFk() {
		return this.idParticipantFk;
	}

	public void setIdParticipantFk(Participant idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}

	public Integer getIndBlockEntity() {
		return this.indBlockEntity;
	}

	public void setIndBlockEntity(Integer indBlockEntity) {
		this.indBlockEntity = indBlockEntity;
	}

	public Integer getIndIssuer() {
		return this.indIssuer;
	}

	public void setIndIssuer(Integer indIssuer) {
		this.indIssuer = indIssuer;
	}

	public Integer getIndParticipant() {
		return this.indParticipant;
	}

	public void setIndParticipant(Integer indParticipant) {
		this.indParticipant = indParticipant;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getNationality() {
		return this.nationality;
	}

	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	public Integer getPersonType() {
		return this.personType;
	}

	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	public Integer getResidenceCountry() {
		return this.residenceCountry;
	}

	public void setResidenceCountry(Integer residenceCountry) {
		this.residenceCountry = residenceCountry;
	}
	
	public PnaPerson getPnaPersonFk() {
		return pnaPersonFk;
	}

	public void setPnaPersonFk(PnaPerson pnaPersonFk) {
		this.pnaPersonFk = pnaPersonFk;
	}

	public PepPerson getPepPersonFk() {
		return pepPersonFk;
	}

	public void setPepPersonFk(PepPerson pepPersonFk) {
		this.pepPersonFk = pepPersonFk;
	}

	public OfacPerson getOfacPersonFk() {
		return ofacPersonFk;
	}

	public void setOfacPersonFk(OfacPerson ofacPersonFk) {
		this.ofacPersonFk = ofacPersonFk;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
	    if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser =loggerUser.getUserName();           
        }		
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}   

}