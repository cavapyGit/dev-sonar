/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */
package com.pradera.model.accounts.holderaccounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum HolderAccountFileType {
	FORM_BANK_ACCOUNT(new Integer(1121),""),
	FORM_KNOW_CLIENT(new Integer(1122),""),
	FORM_OPEN_ACCOUNT(new Integer(1123),"");
	
	private Integer code;
	private String value;
	public static final List<HolderAccountFileType> list = new ArrayList<HolderAccountFileType>();
	public static final Map<Integer, HolderAccountFileType> lookup = new HashMap<Integer, HolderAccountFileType>();
	public static List<HolderAccountFileType> listSomeElements(HolderAccountFileType... transferSecuritiesTypeParams){
		List<HolderAccountFileType> retorno = new ArrayList<HolderAccountFileType>();
		for(HolderAccountFileType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountFileType s : EnumSet.allOf(HolderAccountFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private HolderAccountFileType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	public static HolderAccountFileType get(Integer codigo) {
		return lookup.get(codigo);
	}
}