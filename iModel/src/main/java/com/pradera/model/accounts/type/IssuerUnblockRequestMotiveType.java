package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum IssuerUnblockRequestMotiveType {
		
		LEVANTAMIENTO_SUSPENSION_ORDER_SUPERINTENDENCIA (Integer.valueOf(204),"LEVANTAMIENTO DE SUSPENSION POR ORDEN DE LA SUPERINTENDENCIA"), 
		LEVANTAMIENTO_SUSPENSION_ORDER_CEVALDOM (Integer.valueOf(660),"LEVANTAMIENTO DE SUSPENSION POR ORDEN DE CEVALDOM");
		
		private Integer code;
		private String value;
		

		public static final List<IssuerUnblockRequestMotiveType> list = new ArrayList<IssuerUnblockRequestMotiveType>();
		public static final Map<Integer, IssuerUnblockRequestMotiveType> lookup = new HashMap<Integer, IssuerUnblockRequestMotiveType>();

		static {
			for (IssuerUnblockRequestMotiveType s : EnumSet.allOf(IssuerUnblockRequestMotiveType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private IssuerUnblockRequestMotiveType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static IssuerUnblockRequestMotiveType get(Integer code) {
			return lookup.get(code);
		}

		
}
