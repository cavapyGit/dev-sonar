package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "HOLDER_ANNULATION_HISTORY")
@NamedQueries({
    @NamedQuery(name = "HolderAnnulationHistory.findAll", query = "SELECT h FROM HolderAnnulationHistory h"),
    @NamedQuery(name = "HolderAnnulationHistory.findByIdAnnulationHolderHisPk", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.idAnnulationHolderHisPk = :idAnnulationHolderHisPk"),
    @NamedQuery(name = "HolderAnnulationHistory.findByIdParticipantFk", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.idParticipantFk = :idParticipantFk"),
    @NamedQuery(name = "HolderAnnulationHistory.findByIdHolderFk", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.idHolderFk = :idHolderFk"),
    @NamedQuery(name = "HolderAnnulationHistory.findByMotiveAnnulation", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.motiveAnnulation = :motiveAnnulation"),
    @NamedQuery(name = "HolderAnnulationHistory.findByObservationAnnulation", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.observationAnnulation = :observationAnnulation"),
    @NamedQuery(name = "HolderAnnulationHistory.findByFileTypeAnnulation", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.fileTypeAnnulation = :fileTypeAnnulation"),
    @NamedQuery(name = "HolderAnnulationHistory.findByFileName", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.fileName = :fileName"),
    @NamedQuery(name = "HolderAnnulationHistory.findByStateAnnulation", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.stateAnnulation = :stateAnnulation"),
    @NamedQuery(name = "HolderAnnulationHistory.findByRegistryDate", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.registryDate = :registryDate"),
    @NamedQuery(name = "HolderAnnulationHistory.findByRequestCancelType", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.requestCancelType = :requestCancelType"),
    @NamedQuery(name = "HolderAnnulationHistory.findByRequestCancelDescription", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.requestCancelDescription = :requestCancelDescription"),
    @NamedQuery(name = "HolderAnnulationHistory.findByLastModifyUser", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "HolderAnnulationHistory.findByLastModifyDate", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "HolderAnnulationHistory.findByLastModifyIp", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "HolderAnnulationHistory.findByLastModifyApp", query = "SELECT h FROM HolderAnnulationHistory h WHERE h.lastModifyApp = :lastModifyApp")})
public class HolderAnnulationHistory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_HOLDER_ANNULATION_HIS", sequenceName="SQ_HOLDER_ANNULATION_HIS",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLDER_ANNULATION_HIS")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ANNULATION_HOLDER_HIS_PK", nullable = false)
    private Long idAnnulationHolderHisPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PARTICIPANT_FK", nullable = false)
    private Long idParticipantFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_HOLDER_FK", nullable = false)
    private Long idHolderFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MOTIVE_ANNULATION", nullable = false)
    private Integer motiveAnnulation;
    @Size(max = 1000)
    @Column(name = "OBSERVATION_ANNULATION", length = 1000)
    private String observationAnnulation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_TYPE_ANNULATION", nullable = false)
    private Integer fileTypeAnnulation;
    @Lob
    @Column(name = "FILE_ATTACHED")
    private Serializable fileAttached;
    @Size(min = 1, max = 265)
    @Column(name = "FILE_NAME", length = 265)
    private String fileName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE_ANNULATION", nullable = false)
    private Integer stateAnnulation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Column(name = "REQUEST_CANCEL_TYPE")
    private Integer requestCancelType;
    @Size(max = 500)
    @Column(name = "REQUEST_CANCEL_DESCRIPTION", length = 500)
    private String requestCancelDescription;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @JoinColumn(name = "ID_ANNULATION_HOLDER_FK", referencedColumnName = "ID_ANNULATION_HOLDER_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private HolderAnnulation idAnnulationHolderFk;

    public HolderAnnulationHistory() {
    }

    public HolderAnnulationHistory(Long idAnnulationHolderHisPk) {
        this.idAnnulationHolderHisPk = idAnnulationHolderHisPk;
    }

    public HolderAnnulationHistory(Long idAnnulationHolderHisPk, Long idParticipantFk, Long idHolderFk, Integer motiveAnnulation, Integer fileTypeAnnulation, Serializable fileAttached, String fileName, Integer stateAnnulation, Date registryDate, String lastModifyUser, Date lastModifyDate, String lastModifyIp, Long lastModifyApp) {
        this.idAnnulationHolderHisPk = idAnnulationHolderHisPk;
        this.idParticipantFk = idParticipantFk;
        this.idHolderFk = idHolderFk;
        this.motiveAnnulation = motiveAnnulation;
        this.fileTypeAnnulation = fileTypeAnnulation;
        this.fileAttached = fileAttached;
        this.fileName = fileName;
        this.stateAnnulation = stateAnnulation;
        this.registryDate = registryDate;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdAnnulationHolderHisPk() {
        return idAnnulationHolderHisPk;
    }

    public void setIdAnnulationHolderHisPk(Long idAnnulationHolderHisPk) {
        this.idAnnulationHolderHisPk = idAnnulationHolderHisPk;
    }

    public Long getIdParticipantFk() {
        return idParticipantFk;
    }

    public void setIdParticipantFk(Long idParticipantFk) {
        this.idParticipantFk = idParticipantFk;
    }

    public Long getIdHolderFk() {
        return idHolderFk;
    }

    public void setIdHolderFk(Long idHolderFk) {
        this.idHolderFk = idHolderFk;
    }

    public Integer getMotiveAnnulation() {
        return motiveAnnulation;
    }

    public void setMotiveAnnulation(Integer motiveAnnulation) {
        this.motiveAnnulation = motiveAnnulation;
    }

    public String getObservationAnnulation() {
        return observationAnnulation;
    }

    public void setObservationAnnulation(String observationAnnulation) {
        this.observationAnnulation = observationAnnulation;
    }

    public Integer getFileTypeAnnulation() {
        return fileTypeAnnulation;
    }

    public void setFileTypeAnnulation(Integer fileTypeAnnulation) {
        this.fileTypeAnnulation = fileTypeAnnulation;
    }

    public Serializable getFileAttached() {
        return fileAttached;
    }

    public void setFileAttached(Serializable fileAttached) {
        this.fileAttached = fileAttached;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getStateAnnulation() {
        return stateAnnulation;
    }

    public void setStateAnnulation(Integer stateAnnulation) {
        this.stateAnnulation = stateAnnulation;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public Integer getRequestCancelType() {
        return requestCancelType;
    }

    public void setRequestCancelType(Integer requestCancelType) {
        this.requestCancelType = requestCancelType;
    }

    public String getRequestCancelDescription() {
        return requestCancelDescription;
    }

    public void setRequestCancelDescription(String requestCancelDescription) {
        this.requestCancelDescription = requestCancelDescription;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public HolderAnnulation getIdAnnulationHolderFk() {
        return idAnnulationHolderFk;
    }

    public void setIdAnnulationHolderFk(HolderAnnulation idAnnulationHolderFk) {
        this.idAnnulationHolderFk = idAnnulationHolderFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAnnulationHolderHisPk != null ? idAnnulationHolderHisPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HolderAnnulationHistory)) {
            return false;
        }
        HolderAnnulationHistory other = (HolderAnnulationHistory) object;
        if ((this.idAnnulationHolderHisPk == null && other.idAnnulationHolderHisPk != null) || (this.idAnnulationHolderHisPk != null && !this.idAnnulationHolderHisPk.equals(other.idAnnulationHolderHisPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.HolderAnnulationHistory[ idAnnulationHolderHisPk=" + idAnnulationHolderHisPk + " ]";
    }
    
}
