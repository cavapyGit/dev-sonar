package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;


public enum BlockedFieldsHolderAccountType {
	BLOCKED(Integer.valueOf(1),"BLOCKEADO"),
	UNBLOCKED(Integer.valueOf(0),"DESBLOQUEADO");
	
	private Integer code;
	private String value;
	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * 
	 * @param code
	 * @param value
	 */
	private BlockedFieldsHolderAccountType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}	
	
	public static final List<BlockedFieldsHolderAccountType> list =  new ArrayList<BlockedFieldsHolderAccountType>();
	
	public static final HashMap<Integer, BlockedFieldsHolderAccountType> lookup = new HashMap<Integer, BlockedFieldsHolderAccountType>();
	static {
		for (BlockedFieldsHolderAccountType s : EnumSet.allOf(BlockedFieldsHolderAccountType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static  BlockedFieldsHolderAccountType get(Integer code) {
		return lookup.get(code);
	}
	
}
