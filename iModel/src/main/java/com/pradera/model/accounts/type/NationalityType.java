package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum NationalityType {
		NATIONAL(Integer.valueOf(1486),"NACIONAL"),
		FOREIGN(Integer.valueOf(1487),"EXTRANJERO");		
		
		private Integer code;
		private String value;
		

		public static final List<NationalityType> list = new ArrayList<NationalityType>();
		public static final Map<Integer, NationalityType> lookup = new HashMap<Integer, NationalityType>();

		static {
			for (NationalityType s : EnumSet.allOf(NationalityType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private NationalityType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static NationalityType get(Integer code) {
			return lookup.get(code);
		}

		
}
