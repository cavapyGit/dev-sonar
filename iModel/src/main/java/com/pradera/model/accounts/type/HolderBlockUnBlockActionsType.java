package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum HolderBlockUnBlockActionsType {
	
	BLOCK(Integer.valueOf(523),"BLOQUEO"),
	UNBLOCK(Integer.valueOf(524),"DESBLOQUEO");
	
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private HolderBlockUnBlockActionsType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static HolderBlockUnBlockActionsType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<HolderBlockUnBlockActionsType> list = new ArrayList<HolderBlockUnBlockActionsType>();
	public static final Map<Integer, HolderBlockUnBlockActionsType> lookup = new HashMap<Integer, HolderBlockUnBlockActionsType>();
	static {
		for (HolderBlockUnBlockActionsType s : EnumSet.allOf(HolderBlockUnBlockActionsType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
