package com.pradera.model.accounts.holderaccounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT_FILE_REQUEST database table.
 * 
 */
@Entity
@Table(name="HOLDER_ACCOUNT_FILE_REQUEST")
public class HolderAccountFileRequest implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder account req file pk. */
	@Id
	@SequenceGenerator(name="SQ_HOLDER_ACCOUNT_REQ_FILE_HYS_PK", sequenceName="SQ_ID_HOLDER_ACCOUNT_RE_FI_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLDER_ACCOUNT_REQ_FILE_HYS_PK")
	@Column(name="ID_HOLDER_ACCOUNT_FILE_REQ_PK")  
	private Long idHolderAccountReqFilePk;
	
	/** The holder account request. */
	@ManyToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name="ID_HOLDER_ACCOUNT_REQ_FK")
	private HolderAccountRequest holderAccountRequest;
	
	/** The filename. */
    @Column(name="FILENAME")
	private String filename;
	
    /** The file holder account. */
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_FILE")
	private byte[] documentFile;
    
    /** The ind main. */
	@Column(name="IND_MAIN")
	private Integer indMain;		

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;
	
	/** The have old or new. */
	@Column(name="IND_OLD_NEW")
	private Integer indOldNew;
	
	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
    
    /** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	 /** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
    
    /** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The description. */
	@Transient
	private String documentTypeDesc;
	
	@Transient
	private String fileExtension;
	@Transient
	private String fileSize;
	
	@Transient
	private boolean wasModified;
    /**
     * The Constructor.
     */
    public HolderAccountFileRequest() {
    }

	/**
	 * Gets the id holder account req file pk.
	 *
	 * @return the id holder account req file pk
	 */
	public Long getIdHolderAccountReqFilePk() {
		return this.idHolderAccountReqFilePk;
	}

	/**
	 * Sets the id holder account req file pk.
	 *
	 * @param idHolderAccountReqFilePk the id holder account req file pk
	 */
	public void setIdHolderAccountReqFilePk(Long idHolderAccountReqFilePk) {
		this.idHolderAccountReqFilePk = idHolderAccountReqFilePk;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the file holder account.
	 *
	 * @return the file holder account
	 */
	public byte[] getDocumentFile() {
		return this.documentFile;
	}

	/**
	 * Sets the file holder account.
	 *
	 * @param fileHolderAccount the file holder account
	 */
	public void setDocumentFile(byte[] fileHolderAccount) {
		this.documentFile = fileHolderAccount;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return this.filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the holder account request hy.
	 *
	 * @return the holder account request hy
	 */
	public HolderAccountRequest getHolderAccountRequest() {
		return holderAccountRequest;
	}

	/**
	 * Sets the holder account request hy.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 */
	public void setHolderAccountRequest(HolderAccountRequest holderAccountRequest) {
		this.holderAccountRequest = holderAccountRequest;
	}

	/**
	 * Gets the ind main.
	 *
	 * @return the ind main
	 */
	public Integer getIndMain() {
		return indMain;
	}

	/**
	 * Sets the ind main.
	 *
	 * @param indMain the new ind main
	 */
	public void setIndMain(Integer indMain) {
		this.indMain = indMain;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HolderAccountReqFileHy [idHolderAccountReqFilePk="
				+ idHolderAccountReqFilePk + ", documentType=" + documentType
				+ ", indMain=" + indMain + ", filename=" + filename
				+ ", lastModifyApp=" + lastModifyApp + ", lastModifyDate="
				+ lastModifyDate + ", lastModifyIp=" + lastModifyIp
				+ ", lastModifyUser=" + lastModifyUser + ", registryDate="
				+ registryDate + ", registryUser=" + registryUser+"]";
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}	
	
	/**
	 * Gets the old or new.
	 *
	 * @return the old or new
	 */
	public Integer getIndOldNew() {
		return indOldNew;
	}

	/**
	 * Sets the old or new.
	 *
	 * @param indOldNew the new old or new
	 */
	public void setIndOldNew(Integer indOldNew) {
		this.indOldNew = indOldNew;
	}	

	/**
	 * Gets the document type description.
	 *
	 * @return the document type description
	 */
	public String getDocumentTypeDesc() {
		return documentTypeDesc;
	}

	/**
	 * Sets the document type description.
	 *
	 * @param documentTypeDesc the new document type description
	 */
	public void setDocumentTypeDesc(String documentTypeDesc) {
		
		this.documentTypeDesc = documentTypeDesc;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public boolean isWasModified() {
		return wasModified;
	}

	public void setWasModified(boolean wasModified) {
		this.wasModified = wasModified;
	}
	
	
}