package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the INSTITUTION_INFORMATION database table.
 * 
 */

@NamedQueries({
	@NamedQuery(name = InstitutionInformation.INSTITUTION_INFORMATION_BY_ID, query = "Select distinct institution From InstitutionInformation institution where institution.documentNumber = :docNum  and institution.documentType = :docTyp" )
})
@Entity
@Table(name="INSTITUTION_INFORMATION")
public class InstitutionInformation implements Serializable {
	
	public static final String INSTITUTION_INFORMATION_BY_ID = "InstitutionInformation.finfInstitutionInformationById";	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id institution information pk. */
	@Id
	@SequenceGenerator(name="INSTITUTION_INFORMATION_ID_GENERATOR", sequenceName="SQ_ID_INST_INF_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INSTITUTION_INFORMATION_ID_GENERATOR")
	@Column(name="ID_INSTITUTION_INFORMATION_PK")
	private Long idInstitutionInformationPk;

	/** The address. */
	private String address;

    /** The constitution date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CONSTITUTION_DATE")
	private Date constitutionDate;

	/** The corporate name. */
	@Column(name="CORPORATE_NAME")
	private String corporateName;

	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private String economicActivity;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The number address. */
	@Column(name="NUMBER_ADDRESS")
	private String numberAddress;

	/** The payment regime. */
	@Column(name="PAYMENT_REGIME")
	private String paymentRegime;

	/** The phone number. */
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state institution information. */
	@Column(name="STATE_INSTITUTION_INFORMATION")
	private String stateInstitutionInformation;

	/** The tradename. */
	@Column(name="TRADE_NAME")
	private String tradename;
	
	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;
	
	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The urbanization. */
	private String urbanization;

    /**
     * The Constructor.
     */
    public InstitutionInformation() {
    }

	/**
	 * Gets the id institution information pk.
	 *
	 * @return the id institution information pk
	 */
	public Long getIdInstitutionInformationPk() {
		return idInstitutionInformationPk;
	}

	/**
	 * Sets the id rnc information pk.
	 *
	 * @param idRncInformationPk the id rnc information pk
	 */
	public void setIdInstitutionInformationPk(Long idInstitutionInformationPk) {
		this.idInstitutionInformationPk = idInstitutionInformationPk;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the constitution date.
	 *
	 * @return the constitution date
	 */
	public Date getConstitutionDate() {
		return constitutionDate;
	}

	/**
	 * Sets the constitution date.
	 *
	 * @param constitutionDate the constitution date
	 */
	public void setConstitutionDate(Date constitutionDate) {
		this.constitutionDate = constitutionDate;
	}

	/**
	 * Gets the corporate name.
	 *
	 * @return the corporate name
	 */
	public String getCorporateName() {
		return corporateName;
	}

	/**
	 * Sets the corporate name.
	 *
	 * @param corporateName the corporate name
	 */
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public String getEconomicActivity() {
		return economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the economic activity
	 */
	public void setEconomicActivity(String economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the number address.
	 *
	 * @return the number address
	 */
	public String getNumberAddress() {
		return numberAddress;
	}

	/**
	 * Sets the number address.
	 *
	 * @param numberAddress the number address
	 */
	public void setNumberAddress(String numberAddress) {
		this.numberAddress = numberAddress;
	}

	/**
	 * Gets the payment regime.
	 *
	 * @return the payment regime
	 */
	public String getPaymentRegime() {
		return paymentRegime;
	}

	/**
	 * Sets the payment regime.
	 *
	 * @param paymentRegime the payment regime
	 */
	public void setPaymentRegime(String paymentRegime) {
		this.paymentRegime = paymentRegime;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber the phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state institution information.
	 *
	 * @return the state institution information
	 */
	public String getStateInstitutionInformation() {
		return stateInstitutionInformation;
	}

	/**
	 * Sets the state institution information.
	 *
	 * @param stateRncInformation the state institution information
	 */
	public void setStateInstitutionInformation(String stateInstitutionInformation) {
		this.stateInstitutionInformation = stateInstitutionInformation;
	}

	/**
	 * Gets the tradename.
	 *
	 * @return the tradename
	 */
	public String getTradename() {
		return tradename;
	}

	/**
	 * Sets the tradename.
	 *
	 * @param tradename the tradename
	 */
	public void setTradename(String tradename) {
		this.tradename = tradename;
	}

	/**
	 * Gets the documentNumber.
	 *
	 * @return the documentNumber
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the documentNumber.
	 *
	 * @param documentNumber the documentNumber
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the documentType.
	 *
	 * @return the documentType
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the documentType.
	 *
	 * @param documentType the documentType
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the urbanization.
	 *
	 * @return the urbanization
	 */
	public String getUrbanization() {
		return urbanization;
	}

	/**
	 * Sets the urbanization.
	 *
	 * @param urbanization the urbanization
	 */
	public void setUrbanization(String urbanization) {
		this.urbanization = urbanization;
	}
	
}