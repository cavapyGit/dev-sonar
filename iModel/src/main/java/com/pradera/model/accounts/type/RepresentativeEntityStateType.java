package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum RepresentativeEntityStateType {
	
	REGISTERED(Integer.valueOf(498),"REGISTRADO"),
	DELETED(Integer.valueOf(501),"ELIMINADO");
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private RepresentativeEntityStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static RepresentativeEntityStateType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<RepresentativeEntityStateType> list = new ArrayList<RepresentativeEntityStateType>();
	public static final Map<Integer, RepresentativeEntityStateType> lookup = new HashMap<Integer, RepresentativeEntityStateType>();
	static {
		for (RepresentativeEntityStateType s : EnumSet.allOf(RepresentativeEntityStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
