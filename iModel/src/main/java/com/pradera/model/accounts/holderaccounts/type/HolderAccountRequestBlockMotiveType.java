/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestBlockDocumentType.*;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
public enum HolderAccountRequestBlockMotiveType {
	
	/** The embargoes. */
	EMBARG(new Integer(563), "EMBARGOS OPOSICIONES Y MEDIDAS CAUTELARES", new Integer[]{ACTO_ALGUACIL.getCode()}),
	
	/** The request act defuntion. */
	DIED(new Integer(564), "FALLECIMIENTO", new Integer[]{ACT_DEFUNTION.getCode(),BLOCK_REQUEST.getCode()}),
	
	/** The request supervisory authority financial system. */
	REQ_ASFI(new Integer(565), "A SOLICITUD DE LA AUTORIDAD DE SUPERVISION DEL SISTEMA FINANCIERO - ASFI", new Integer[]{REQ_BLOCK_SUP.getCode()}),
	
	/** The request others. */
	REQ_OTHERS(new Integer(802), "OTROS", new Integer[]{ISSUED_LETTER_BY_EDV.getCode()}),
	
	/** The req broker. */
	REQ_BROKER(new Integer(1290),"A SOLICITUD DEL PARTICIPANTE",new Integer[]{ISSUED_LETTER_BY_BROK.getCode()}),
	
	/** The req jud. */
	REQ_JUD(new Integer(1291),"POR ORDEN JUDICIAL",new Integer[]{COURT_ORDER.getCode()}),
	
	/** The masive. */
	MASIVE(new Integer(1344),"POR TRASPASO MASIVO", null),
	
	/** The unification cui. */
	UNIFICATION_CUI(new Integer(1635),"POR UNIFICACION DE CUI", null),
	
	OTHER(new Integer(-1),"other",null);
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The array documents. */
	private Integer[] arrayDocuments;
	
	/** The Constant list. */
	public static final List<HolderAccountRequestBlockMotiveType> list = new ArrayList<HolderAccountRequestBlockMotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountRequestBlockMotiveType> lookup = new HashMap<Integer, HolderAccountRequestBlockMotiveType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountRequestBlockMotiveType> listSomeElements(HolderAccountRequestBlockMotiveType... transferSecuritiesTypeParams){
		List<HolderAccountRequestBlockMotiveType> retorno = new ArrayList<HolderAccountRequestBlockMotiveType>();
		for(HolderAccountRequestBlockMotiveType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountRequestBlockMotiveType s : EnumSet.allOf(HolderAccountRequestBlockMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the array documents.
	 *
	 * @return the array documents
	 */
	public Integer[] getArrayDocuments() {
		return arrayDocuments;
	}

	/**
	 * Sets the array documents.
	 *
	 * @param arrayDocuments the new array documents
	 */
	public void setArrayDocuments(Integer[] arrayDocuments) {
		this.arrayDocuments = arrayDocuments;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountRequestBlockMotiveType(Integer codigo, String valor, Integer[] IntegerArray) {
		this.code = codigo;
		this.value = valor;
		this.arrayDocuments = IntegerArray;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static HolderAccountRequestBlockMotiveType get(Integer codigo) {
		return lookup.get(codigo);
	}
}