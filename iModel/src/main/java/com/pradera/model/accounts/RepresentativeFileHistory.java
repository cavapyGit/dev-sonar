package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the REPRESENTATIVE_FILE_HISTORY database table.
 * 
 */
@Entity
@Table(name="REPRESENTATIVE_FILE_HISTORY")
public class RepresentativeFileHistory implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id represent file his pk. */
	@Id
	@SequenceGenerator(name="REPRE_FILE_H_IDREPREFILEHPK_GENERATOR", sequenceName="SQ_ID_REPRESENT_FILE_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPRE_FILE_H_IDREPREFILEHPK_GENERATOR")
	@Column(name="ID_REPRESENT_FILE_HIS_PK")
	private Long idRepresentFileHisPk;

	/** The description. */
	private String description;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

    /** The file representative. */
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_FILE")
	private byte[] fileRepresentative;

	/** The filename. */
	private String filename;

	/** The legal representative history. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REPRESENTATIVE_HISTORY_FK",referencedColumnName="ID_REPRESENTATIVE_HISTORY_PK")
	private LegalRepresentativeHistory legalRepresentativeHistory;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The represent file hist type. */
	@Column(name="REPRESENT_FILE_HIST_TYPE")
	private Integer representFileHistType;

	/** The state file. */
	@Column(name="STATE_FILE")
	private Integer stateFile;
	
	/** The was modified. */
	@Transient
	private boolean wasModified;
	
	/** The flag legal import. */
	@Transient
	private boolean flagLegalImport;
	
	/** The flag active files by holder import. */
	@Transient
	private boolean flagActiveFilesByHolderImport;
	
	/** The flag by legal representative. */
	@Transient
	private boolean flagByLegalRepresentative;

    /**
     * Instantiates a new representative file history.
     */
    public RepresentativeFileHistory() {
    }
    
    

	/**
	 * Gets the id represent file his pk.
	 *
	 * @return the id represent file his pk
	 */
	public Long getIdRepresentFileHisPk() {
		return this.idRepresentFileHisPk;
	}

	/**
	 * Sets the id represent file his pk.
	 *
	 * @param idRepresentFileHisPk the new id represent file his pk
	 */
	public void setIdRepresentFileHisPk(Long idRepresentFileHisPk) {
		this.idRepresentFileHisPk = idRepresentFileHisPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the file representative.
	 *
	 * @return the file representative
	 */
	public byte[] getFileRepresentative() {
		return this.fileRepresentative;
	}

	/**
	 * Sets the file representative.
	 *
	 * @param fileRepresentative the new file representative
	 */
	public void setFileRepresentative(byte[] fileRepresentative) {
		this.fileRepresentative = fileRepresentative;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return this.filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the legal representative history.
	 *
	 * @return the legal representative history
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistory() {
		return legalRepresentativeHistory;
	}

	/**
	 * Sets the legal representative history.
	 *
	 * @param legalRepresentativeHistory the new legal representative history
	 */
	public void setLegalRepresentativeHistory(
			LegalRepresentativeHistory legalRepresentativeHistory) {
		this.legalRepresentativeHistory = legalRepresentativeHistory;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return this.registryType;
	}

	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the represent file hist type.
	 *
	 * @return the represent file hist type
	 */
	public Integer getRepresentFileHistType() {
		return this.representFileHistType;
	}

	/**
	 * Sets the represent file hist type.
	 *
	 * @param representFileHistType the new represent file hist type
	 */
	public void setRepresentFileHistType(Integer representFileHistType) {
		this.representFileHistType = representFileHistType;
	}

	/**
	 * Gets the state file.
	 *
	 * @return the state file
	 */
	public Integer getStateFile() {
		return this.stateFile;
	}

	/**
	 * Sets the state file.
	 *
	 * @param stateFile the new state file
	 */
	public void setStateFile(Integer stateFile) {
		this.stateFile = stateFile;
	}
	
	/**
	 * Checks if is was modified.
	 *
	 * @return true, if is was modified
	 */
	public boolean isWasModified() {
		return wasModified;
	}

	/**
	 * Sets the was modified.
	 *
	 * @param wasModified the new was modified
	 */
	public void setWasModified(boolean wasModified) {
		this.wasModified = wasModified;
	}
	
	/**
	 * Checks if is flag legal import.
	 *
	 * @return true, if is flag legal import
	 */
	public boolean isFlagLegalImport() {
		return flagLegalImport;
	}

	/**
	 * Sets the flag legal import.
	 *
	 * @param flagLegalImport the new flag legal import
	 */
	public void setFlagLegalImport(boolean flagLegalImport) {
		this.flagLegalImport = flagLegalImport;
	}

	/**
	 * Checks if is flag active files by holder import.
	 *
	 * @return true, if is flag active files by holder import
	 */
	public boolean isFlagActiveFilesByHolderImport() {
		return flagActiveFilesByHolderImport;
	}

	/**
	 * Sets the flag active files by holder import.
	 *
	 * @param flagActiveFilesByHolderImport the new flag active files by holder import
	 */
	public void setFlagActiveFilesByHolderImport(
			boolean flagActiveFilesByHolderImport) {
		this.flagActiveFilesByHolderImport = flagActiveFilesByHolderImport;
	}
	
	/**
	 * Checks if is flag by legal representative.
	 *
	 * @return true, if is flag by legal representative
	 */
	public boolean isFlagByLegalRepresentative() {
		return flagByLegalRepresentative;
	}

	/**
	 * Sets the flag by legal representative.
	 *
	 * @param flagByLegalRepresentative the new flag by legal representative
	 */
	public void setFlagByLegalRepresentative(boolean flagByLegalRepresentative) {
		this.flagByLegalRepresentative = flagByLegalRepresentative;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
            if(loggerUser.getAuditTime() != null) {
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null){
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}