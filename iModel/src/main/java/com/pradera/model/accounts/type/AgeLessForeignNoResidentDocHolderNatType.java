package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeLessForeignNoResidentDocHolderNatType {
		
				
		CE(Integer.valueOf(949),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(950),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(951),"PASAPORTE"), 
		LIC(Integer.valueOf(952),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(953),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(954),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(955),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(956),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(957),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(958),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(959),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(960),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(961),"DECRETO"),		
	    RCF(Integer.valueOf(962),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(963),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(964),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    		
		private Integer code;
		private String value;
		
		public static final List<AgeLessForeignNoResidentDocHolderNatType> list = new ArrayList<AgeLessForeignNoResidentDocHolderNatType>();
		public static final Map<Integer, AgeLessForeignNoResidentDocHolderNatType> lookup = new HashMap<Integer, AgeLessForeignNoResidentDocHolderNatType>();

		static {
			for (AgeLessForeignNoResidentDocHolderNatType s : EnumSet.allOf(AgeLessForeignNoResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeLessForeignNoResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeLessForeignNoResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
