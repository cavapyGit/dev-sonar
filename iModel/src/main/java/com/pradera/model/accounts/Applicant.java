package com.pradera.model.accounts;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
/**
* <ul><li>Copyright EDV 2020.</li></ul> 
* The Class Applicant.
* @author RCHIARA.
*/
@Entity
@Table(name="APPLICANT")
public class Applicant implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="APPLICANTPK_GEN", sequenceName="SECID_APPLICANTPK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="APPLICANTPK_GEN")
	@Column(name="ID_APPLICANT_PK")
	private Long idApplicantPk;
	
	@Column(name="APPLICANT_TYPE")
	private String applicantType;
	
	@Column(name="APPLICANT_DESC")
	private String applicantDesc;
	
	@Column(name="APPLICANT_NUM")
	private String applicantNum;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REQUEST_DATE")
	private Date requestDate;
	
	@Lob()
   	@Column(name="BACK_FILE")
   	private byte[] backFile;
	
	@Column(name="FILENAME")
	private String filename;
	
	@Column(name="USER_REG")
	private String userReg;
	
	@Column(name="PROCESS_DATE") 
	private Timestamp processDate;
	
	@Column(name="PROCESS_QUANTITY")
	private Integer processQuantity;
	
	@Column(name="PROCESS_OBSERVED")
	private Integer processObserved;
	
	@Column(name="PROCESS_VERIFIED")
	private Integer processVerified;
	
	@Column(name="PROCESS_STATE")
	private Integer processState;
	
	@Transient
	private String processType;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdApplicantPk() {
		return idApplicantPk;
	}

	public void setIdApplicantPk(Long idApplicantPk) {
		this.idApplicantPk = idApplicantPk;
	}

	public String getApplicantType() {
		return applicantType;
	}

	public void setApplicantType(String applicantType) {
		this.applicantType = applicantType;
	}

	public String getApplicantNum() {
		return applicantNum;
	}

	public void setApplicantNum(String applicantNum) {
		this.applicantNum = applicantNum;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public byte[] getBackFile() {
		return backFile;
	}
	
	public void setBackFile(byte[] backFile) {
		this.backFile = backFile;
	}
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}	
	
	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Timestamp processDate) {
		this.processDate = processDate;
	}

	public Integer getProcessQuantity() {
		return processQuantity;
	}

	public void setProcessQuantity(Integer processQuantity) {
		this.processQuantity = processQuantity;
	}

	public Integer getProcessObserved() {
		return processObserved;
	}

	public void setProcessObserved(Integer processObserved) {
		this.processObserved = processObserved;
	}

	public Integer getProcessVerified() {
		return processVerified;
	}

	public void setProcessVerified(Integer processVerified) {
		this.processVerified = processVerified;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}
	
	public String getApplicantDesc() {
		return applicantDesc;
	}

	public void setApplicantDesc(String applicantDesc) {
		this.applicantDesc = applicantDesc;
	}

	public String getUserReg() {
		return userReg;
	}

	public void setUserReg(String userReg) {
		this.userReg = userReg;
	}	

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp =  loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
			if(loggerUser.getAuditTime() != null) {
				lastModifyDate = loggerUser.getAuditTime();
			} else {
				lastModifyDate = objLoggerUser.getAuditTime();
			}
			if(loggerUser.getIpAddress() != null) {
				lastModifyIp = loggerUser.getIpAddress();
			} else {
				lastModifyIp = objLoggerUser.getIpAddress();
			}
			if(loggerUser.getUserName() != null) {
				lastModifyUser = loggerUser.getUserName();
			} else {
				lastModifyUser = objLoggerUser.getUserName();
			}
		} else {
			lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
		}
	}
}
