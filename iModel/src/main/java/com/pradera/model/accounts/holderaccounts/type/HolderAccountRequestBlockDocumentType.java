/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
public enum HolderAccountRequestBlockDocumentType {
	/** The Letter sent by EDV. */
	ISSUED_LETTER_BY_EDV(new Integer(797),"CARTA EMITIDA POR EDV"),
	
	/** The Letter sent by Participant. */
	ISSUED_LETTER_BY_BROK(new Integer(1305),"CARTA EMITIDA POR PARTICIPANTE"),
	
	/** The Injunction */
	COURT_ORDER(new Integer(1306),"ORDEN JUDICIAL"),
	
	/** The Sheriff Act with Notification. */
	ACTO_ALGUACIL(new Integer(798),"ACTO DE ALGUACIL CON LA NOTIFICACION"),
	
	/** Request Block O Close. */
	BLOCK_REQUEST(new Integer(799),"SOLICITUD DE BLOQUEO O CIERRE"),
	
	/** The act defuntion. */
	ACT_DEFUNTION(new Integer(800),"ACTA DE DEFUNCION"),
	
	/** The ownership. */
	REQ_BLOCK_SUP(new Integer(801),"SOLICITUD DEL SIV SOBRE CUENTA TITULAR");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<HolderAccountRequestBlockDocumentType> list = new ArrayList<HolderAccountRequestBlockDocumentType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountRequestBlockDocumentType> lookup = new HashMap<Integer, HolderAccountRequestBlockDocumentType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountRequestBlockDocumentType> listSomeElements(HolderAccountRequestBlockDocumentType... transferSecuritiesTypeParams){
		List<HolderAccountRequestBlockDocumentType> retorno = new ArrayList<HolderAccountRequestBlockDocumentType>();
		for(HolderAccountRequestBlockDocumentType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountRequestBlockDocumentType s : EnumSet.allOf(HolderAccountRequestBlockDocumentType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountRequestBlockDocumentType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static HolderAccountRequestBlockDocumentType get(Integer codigo) {
		return lookup.get(codigo);
	}
}