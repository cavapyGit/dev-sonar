package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the INTERNATIONAL_DEPOSITORY database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/02/2013
 */
@NamedQueries({
	@NamedQuery(name = InternationalDepository.INTERNATIONAL_DEPOSITORIES_BY_STATE, query = "Select IDep From InternationalDepository IDep Where IDep.stateIntDepositary = :stateIntDepository")
})
@Entity
@Table(name="INTERNATIONAL_DEPOSITORY")
public class InternationalDepository implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant INTERNATIONAL_DEPOSITORIES_BY_STATE. */
	public static final String INTERNATIONAL_DEPOSITORIES_BY_STATE = "InternationalDepository.internationalDepositoriesByState";

	/** The id international depository pk. */
	@Id
	@SequenceGenerator(name="INTERNATIONAL_DEPOSITORY_IDINTERNATIONALDEPOSITORYPK_GENERATOR", sequenceName="SQ_ID_INTER_DEPOSITORY_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INTERNATIONAL_DEPOSITORY_IDINTERNATIONALDEPOSITORYPK_GENERATOR")
	@Column(name="ID_INTERNATIONAL_DEPOSITORY_PK")
	private Long idInternationalDepositoryPk;

	/** The country. */
	private Integer country;

	/** The description. */
	private String description;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state int depositary. */
	@Column(name="STATE_INT_DEPOSITARY")
	private Integer stateIntDepositary;
	
	/** The bicCode. */
	@Column(name="BIC_CODE")
	private String bicCode;	
	
	
	/**
	 * @return the bicCode
	 */
	public String getBicCode() {
		return bicCode;
	}

	/**
	 * @param bicCode the bicCode to set
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}

	/** The participant int depositaries. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="internationalDepository")
	private List<ParticipantIntDepositary> participantIntDepositaries;

	/** The selected. */
	@Transient
	private boolean selected;
	
    /**
     * The Constructor.
     */
    public InternationalDepository() {
    }

	/**
	 * Gets the id international depository pk.
	 *
	 * @return the id international depository pk
	 */
	public Long getIdInternationalDepositoryPk() {
		return idInternationalDepositoryPk;
	}

	/**
	 * Sets the id international depository pk.
	 *
	 * @param idInternationalDepositoryPk the id international depository pk
	 */
	public void setIdInternationalDepositoryPk(Long idInternationalDepositoryPk) {
		this.idInternationalDepositoryPk = idInternationalDepositoryPk;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public Integer getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the country
	 */
	public void setCountry(Integer country) {
		this.country = country;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state int depositary.
	 *
	 * @return the state int depositary
	 */
	public Integer getStateIntDepositary() {
		return stateIntDepositary;
	}

	/**
	 * Sets the state int depositary.
	 *
	 * @param stateIntDepositary the state int depositary
	 */
	public void setStateIntDepositary(Integer stateIntDepositary) {
		this.stateIntDepositary = stateIntDepositary;
	}

	/**
	 * Gets the participant int depositaries.
	 *
	 * @return the participant int depositaries
	 */
	public List<ParticipantIntDepositary> getParticipantIntDepositaries() {
		return participantIntDepositaries;
	}

	/**
	 * Sets the participant int depositaries.
	 *
	 * @param participantIntDepositaries the participant int depositaries
	 */
	public void setParticipantIntDepositaries(
			List<ParticipantIntDepositary> participantIntDepositaries) {
		this.participantIntDepositaries = participantIntDepositaries;
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	/**
	 * Instantiates a new international depository.
	 *
	 * @param idInternationalDepositoryPk the id international depository pk
	 * @param description the description
	 * @param bicCode the bic code
	 */
	public InternationalDepository(Long idInternationalDepositoryPk,String description, String bicCode) {
		this.idInternationalDepositoryPk = idInternationalDepositoryPk;
		this.description = description;
		this.bicCode = bicCode;
	}
	/**
	 * Instantiates a new international depository.
	 *
	 * @param idInternationalDepositoryPk the id international depository pk
	 * @param description the description
	 */
	public InternationalDepository(Long idInternationalDepositoryPk,String description) {
		this.idInternationalDepositoryPk = idInternationalDepositoryPk;
		this.description = description;
	}
}