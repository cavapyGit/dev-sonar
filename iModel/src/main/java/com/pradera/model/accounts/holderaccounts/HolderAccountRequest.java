package com.pradera.model.accounts.holderaccounts;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.Hibernate;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountFileType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
@Entity
@NamedQueries({
	@NamedQuery(name = HolderAccountRequest.ACC_STATE, 
		query = "SELECT new com.pradera.model.accounts.holderaccounts.HolderAccountRequest(har.idHolderAccountReqPk, har.requestState) FROM HolderAccountRequest har WHERE har.idHolderAccountReqPk = :idAccountReqPk"),
	@NamedQuery(name = HolderAccountRequest.ACC_ACCOUNT_USED, 
		query = "SELECT count(har) FROM HolderAccountRequest har WHERE har.holderAccount.idHolderAccountPk = :idAccountPk AND har.requestState IN :statesParam")
})
@Table(name="HOLDER_ACCOUNT_REQUEST")
public class HolderAccountRequest implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant ACC_STATE. */
	public final static String ACC_STATE = "search.stateRequest";
	
	/** The Constant ACC_ACCOUNT_USED. */
	public final static String ACC_ACCOUNT_USED = "search.stateAccountUsed";

	/** The id holder account request pk. */
	@NotNull
	@Id
	@SequenceGenerator(name="SQ_HOLDER_ACCOUNT_REQ_PK", sequenceName="SQ_ID_HOLDER_ACCOUNT_REQ_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLDER_ACCOUNT_REQ_PK")	
	@Column(name="ID_HOLDER_ACCOUNT_REQ_PK")
	private Long idHolderAccountReqPk;
	
	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	/** The request type. */
	@Column(name="REQUEST_TYPE")
	private Integer requestType;
	
	/** The request number. */
	@Column(name="REQUEST_NUMBER")
	private Long requestNumber;
	
	/** The state holder account request. */
	@Column(name="REQUEST_STATE")
	private Integer requestState;
	
	/** The motive request. */
	@Column(name="REQUEST_MOTIVE")
	private Integer requestMotive;
	
	/** The other motive request. */
	@Column(name="REQUEST_OTHER_MOTIVE")
	private String requestOtherMotive;
	
	/** The holder account type. */
	@Column(name="HOLDER_ACCOUNT_TYPE")
	private Integer holderAccountType;
	
	/** The holder account group. */
	@Column(name="HOLDER_ACCOUNT_GROUP")
	private Integer holderAccountGroup;
	
	/** The doc block. */
	@Lob()
	@Basic(fetch=FetchType.LAZY)
	@Column(name="DOC_BLOCK")
	private byte[] docBlock;
	
	/** The doc block act. */
	@Lob()
	@Basic(fetch=FetchType.LAZY)
	@Column(name="DOC_BLOCK_ACT")
	private byte[] docBlockAct;
	
	/** The comments. */
	@Column(name="COMMENTS")
	private String comments;
	
	/** The requester block. */
	@Column(name="REQUESTER_BLOCK")
	private Integer requesterBlock;
	
	/** The related name. */
	@Column(name="RELATED_NAME")
	private String relatedName;

	/** The related name. */
	@Column(name="RELATED_NAME_ORIGINAL")
	private String relatedNameOriginal;
	
    /** The registry date. */
    @Temporal(TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The approve user. */
	@Column(name="APPROVE_USER")
	private String approveUser;
	
	/** The approve date. */
    @Temporal(TemporalType.DATE)
	@Column(name="APPROVE_DATE")
	private Date approveDate;
    
    /** The anull user. */
	@Column(name="ANULL_USER")
	private String anullUser;
	
	 /** The anull date. */
    @Temporal(TemporalType.DATE)
	@Column(name="ANULL_DATE")
	private Date anullDate;
	
    /** The confirm user. */
	@Column(name="CONFIRM_USER")
	private String confirmUser;

    /** The confirm date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;

    /** The reject user. */
	@Column(name="REJECT_USER")
	private String rejectUser;

	/** The reject date. */
    @Temporal(TemporalType.DATE)
	@Column(name="REJECT_DATE")
	private Date rejectDate;
    
    /** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;				
	
	@Column(name = "IND_PDD")
	private Integer indPdd;
	
	@Column(name = "IND_PDD_ORIGINAL")
	private Integer indPddOriginal;
	
	@Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_PDD")
	private byte[] documentPdd;
	
	@Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_PDD_ORIGINAL")
	private byte[] documentPddOriginal;
	
	/** The related name. */
	@Column(name = "NAME_DOCUMENT_PDD")
	private String nameDocumentPdd;

	/** The related name. */
	@Column(name = "NAME_DOCUMENT_PDD_ORIGINAL")
	private String nameDocumentPddOriginal;
	
	@Column(name = "STOCK_CODE")
	private Integer stockCode;
	
	/** The holder account request bank. */
	@OneToMany(mappedBy="holderAccountRequest", cascade=CascadeType.ALL, orphanRemoval=true , fetch=FetchType.LAZY)
	private List<HolderAccountBankRequest> holderAccountBankRequest;
	
	/** The holder account request files. */
	@OneToMany(mappedBy="holderAccountRequest", cascade=CascadeType.ALL, orphanRemoval=true , fetch=FetchType.LAZY)
	private List<HolderAccountFileRequest> holderAccountFilesRequest;

	/** The holder account request detail. */
	@OneToMany(mappedBy="holderAccountRequest", cascade=CascadeType.ALL, orphanRemoval=true , fetch=FetchType.LAZY)
	private List<HolderAccountDetRequest> holderAccountDetRequest;
	
	/** The state account name. */
	@Transient
	private String stateAccountName;
	
	/** The type description account name. */
	@Transient
	private String holderAccountTypeDescription;
	
	/** The request type name. */
	@Transient
	private String requestTypeName;
	
	@Transient
	private boolean blPdd;
	
    /**
     * Instantiates a new holder account request.
     */
    public HolderAccountRequest() {
    	participant = new Participant();
    }

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return this.comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		if(holderAccountDetRequest==null || holderAccountDetRequest.isEmpty()){
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder(); 
		for(HolderAccountDetRequest holderAccountReqDetHy: this.getHolderAccountDetRequest()){
			stringBuilder.append("* &nbsp").append(holderAccountReqDetHy.getHolder().getDescriptionHolder()).append("<br>");
		}
		return stringBuilder.toString();
	}	

	/**
	 * Gets the description cui.
	 *
	 * @return the description cui
	 */
	public String getDescriptionCui() {
		if(holderAccountDetRequest==null || holderAccountDetRequest.isEmpty()){
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder(); 
		for(HolderAccountDetRequest holderAccountReqDetHy: holderAccountDetRequest){
			Holder holderTmp = holderAccountReqDetHy.getHolder();
			stringBuilder.append(holderTmp.getIdHolderPk()).append("-").append(holderTmp.getDescriptionHolder());
			if(this.getHolderAccountDetRequest().indexOf(holderAccountReqDetHy)!=(this.getHolderAccountDetRequest().size()-1)){
				stringBuilder.append(", ");
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * Gets the id holder account req pk.
	 *
	 * @return the id holder account req pk
	 */
	public Long getIdHolderAccountReqPk() {
		return this.idHolderAccountReqPk;
	}

	/**
	 * Sets the id holder account req pk.
	 *
	 * @param idHolderAccountReqPk the new id holder account req pk
	 */
	public void setIdHolderAccountReqPk(Long idHolderAccountReqPk) {
		this.idHolderAccountReqPk = idHolderAccountReqPk;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the confirm user.
	 *
	 * @return the confirm user
	 */
	public String getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the new confirm user
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * Gets the anull date.
	 *
	 * @return the anull date
	 */
	public Date getAnullDate() {
		return anullDate;
	}

	/**
	 * Sets the anull date.
	 *
	 * @param anullDate the new anull date
	 */
	public void setAnullDate(Date anullDate) {
		this.anullDate = anullDate;
	}

	/**
	 * Gets the anull user.
	 *
	 * @return the anull user
	 */
	public String getAnullUser() {
		return anullUser;
	}

	/**
	 * Sets the anull user.
	 *
	 * @param anullUser the new anull user
	 */
	public void setAnullUser(String anullUser) {
		this.anullUser = anullUser;
	}

	/**
	 * Gets the approve date.
	 *
	 * @return the approve date
	 */
	public Date getApproveDate() {
		return approveDate;
	}

	/**
	 * Sets the approve date.
	 *
	 * @param approveDate the new approve date
	 */
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	/**
	 * Gets the approve user.
	 *
	 * @return the approve user
	 */
	public String getApproveUser() {
		return approveUser;
	}

	/**
	 * Sets the approve user.
	 *
	 * @param approveUser the new approve user
	 */
	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}

	/**
	 * Gets the holder account group.
	 *
	 * @return the holder account group
	 */
	public Integer getHolderAccountGroup() {
		return holderAccountGroup;
	}

	/**
	 * Sets the holder account group.
	 *
	 * @param holderAccountGroup the new holder account group
	 */
	public void setHolderAccountGroup(Integer holderAccountGroup) {
		this.holderAccountGroup = holderAccountGroup;
	}

	/**
	 * Gets the holder account type.
	 *
	 * @return the holder account type
	 */
	public Integer getHolderAccountType() {
		return holderAccountType;
	}

	/**
	 * Sets the holder account type.
	 *
	 * @param holderAccountType the new holder account type
	 */
	public void setHolderAccountType(Integer holderAccountType) {
		this.holderAccountType = holderAccountType;
	}

	/**
	 * Gets the holder account type description.
	 *
	 * @return the holder account type description
	 */
	public String getHolderAccountTypeDescription() {
		return this.holderAccountTypeDescription;
	}

	/**
	 * Sets the holder account type description.
	 *
	 * @param holderAccountTypeDescription the new holder account type description
	 */
	public void setHolderAccountTypeDescription(String holderAccountTypeDescription) {
		this.holderAccountTypeDescription = holderAccountTypeDescription;
	}
	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	/**
	 * Gets the motive request.
	 *
	 * @return the request motive.
	 */
	public Integer getRequestMotive() {
		return requestMotive;
	}

	/**
	 * Sets the request motive.
	 *
	 * @param requestMotive the new request motive
	 */
	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}		

	/**
	 * Gets the other motive request.
	 *
	 * @return the other motive request
	 */
	public String getRequestOtherMotive() {
		return requestOtherMotive;
	}

	/**
	 * Sets the other motive request.
	 *
	 * @param requestOtherMotive the new request other motive
	 */
	public void setRequestOtherMotive(String requestOtherMotive) {
		this.requestOtherMotive = requestOtherMotive;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param idParticipantFk the new participant
	 */
	public void setParticipant(Participant idParticipantFk) {
		this.participant = idParticipantFk;
	}

	/**
	 * Gets the reject date.
	 *
	 * @return the reject date
	 */
	public Date getRejectDate() {
		return this.rejectDate;
	}

	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the new reject date
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	/**
	 * Gets the reject user.
	 *
	 * @return the reject user
	 */
	public String getRejectUser() {
		return this.rejectUser;
	}

	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the new reject user
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return this.requestType;
	}
	
	/**
	 * Gets the request type description.
	 *
	 * @return the request type description
	 */
	public String getRequestTypeDescription() {
		return HolderAccountRequestHysType.lookup.get(this.getRequestType()).getValue();
	}
	
	/**
	 * Gets the request type description optional.
	 *
	 * @return the request type description optional
	 */
	public String getRequestTypeDescriptionOptional() {
		return HolderAccountRequestHysType.lookup.get(this.getRequestType()).getOptionalValue();
	} 

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}	

	/**
	 * Gets the state holder account req.
	 *
	 * @return the state holder account req
	 */
	public Integer getRequestState() {
		return this.requestState;
	}
	
	/**
	 * Gets the state holder account req description.
	 *
	 * @return the state holder account req description
	 */
	public String getStateHolderAccountReqDescription() {
		return HolderAccountRequestHysStatusType.lookup.get(this.getRequestState()).getValue();
	}

	/**
	 * Sets the state holder account req.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return this.holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the holder account req bank hys.
	 *
	 * @return the holder account req bank hys
	 */
	public List<HolderAccountBankRequest> getHolderAccountBankRequest() {
		return this.holderAccountBankRequest;
	}

	/**
	 * Sets the holder account req bank hys.
	 *
	 * @param holderAccountBankRequest the new holder account bank request
	 */
	public void setHolderAccountBankRequest(List<HolderAccountBankRequest> holderAccountBankRequest) {
		this.holderAccountBankRequest = holderAccountBankRequest;
	}

	/**
	 * Gets the holder account req det hys.
	 *
	 * @return the holder account req det hys
	 */
	public List<HolderAccountDetRequest> getHolderAccountDetRequest() {
		return this.holderAccountDetRequest;
	}

	/**
	 * Sets the holder account req det hys.
	 *
	 * @param holderAccountDetRequest the new holder account det request
	 */
	public void setHolderAccountDetRequest(List<HolderAccountDetRequest> holderAccountDetRequest) {
		this.holderAccountDetRequest = holderAccountDetRequest;
	}
	
	/**
	 * Gets the doc block.
	 *
	 * @return the doc block
	 */
	public byte[] getDocBlock() {
		return docBlock;
	}

	/**
	 * Sets the doc block.
	 *
	 * @param docBlock the new doc block
	 */
	public void setDocBlock(byte[] docBlock) {
		this.docBlock = docBlock;
	}	

	/**
	 * Gets the holder account req det description.
	 *
	 * @return the holder account req det description
	 */
	public String getHolderAccountReqDetDescription(){
		if(Validations.validateIsNullOrEmpty(this.getHolderAccountDetRequest())){
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder(); 
		for(HolderAccountDetRequest holderAccountReqDetHy: this.getHolderAccountDetRequest()){
			stringBuilder.append("* &nbsp").append(holderAccountReqDetHy.getHolder().getDescriptionHolder()).append("<br>");
		}
		return stringBuilder.toString();
	}
	
	/**
	 * Gets the holder account req det rnts.
	 *
	 * @return the holder account req det rnts
	 */
	public String getHolderAccountReqDetRnts(){
		if(Validations.validateIsNullOrEmpty(this.getHolderAccountDetRequest())){
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder(); 
		for(HolderAccountDetRequest holderAccountReqDetHy: this.getHolderAccountDetRequest()){
			Holder holderTmp = holderAccountReqDetHy.getHolder();
			stringBuilder.append(holderTmp.getIdHolderPk()).append("-").append(holderTmp.getDescriptionHolder());
			if(this.getHolderAccountDetRequest().indexOf(holderAccountReqDetHy)!=(this.getHolderAccountDetRequest().size()-1)){
				stringBuilder.append(", ");
			}
		}
		return stringBuilder.toString();
	}	
	
	/**
	 * Gets the doc block act.
	 *
	 * @return the doc block act
	 */
	public byte[] getDocBlockAct() {
		return docBlockAct;
	}

	/**
	 * Sets the doc block act.
	 *
	 * @param docBlockAct the new doc block act
	 */
	public void setDocBlockAct(byte[] docBlockAct) {
		this.docBlockAct = docBlockAct;
	}	

	/**
	 * Gets the requester block.
	 *
	 * @return the requester block
	 */
	public Integer getRequesterBlock() {
		return requesterBlock;
	}

	/**
	 * Sets the requester block.
	 *
	 * @param requesterBlock the new requester block
	 */
	public void setRequesterBlock(Integer requesterBlock) {
		this.requesterBlock = requesterBlock;
	}
		
	/**
	 * Gets the holder account req files.
	 *
	 * @return the holder account req files
	 */
	public List<HolderAccountFileRequest> getHolderAccountFilesRequest() {
		return holderAccountFilesRequest;
	}

	/**
	 * Sets the holder account req files.
	 *
	 * @param holderAccountFilesRequest the new holder account files request
	 */
	public void setHolderAccountFilesRequest(
			List<HolderAccountFileRequest> holderAccountFilesRequest) {
		this.holderAccountFilesRequest = holderAccountFilesRequest;
	}
	
	/**
	 * Delete form ban account.
	 */
	public void deleteFormBanAccount(){
		for(HolderAccountFileRequest holderAccountFile: holderAccountFilesRequest){
			if(holderAccountFile.getDocumentType().equals(HolderAccountFileType.FORM_BANK_ACCOUNT.getCode())){
				holderAccountFilesRequest.remove(holderAccountFile);return;
			}
		}
	}
	
	/**
	 * Delete form know client.
	 */
	public void deleteFormKnowClient(){
		for(HolderAccountFileRequest holderAccountFile: holderAccountFilesRequest){
			if(holderAccountFile.getDocumentType().equals(HolderAccountFileType.FORM_KNOW_CLIENT.getCode())){
				holderAccountFilesRequest.remove(holderAccountFile);return;
			}
		}
	}
	
	/**
	 * Delete form know client.
	 */
	public void deleteFormOpenAccount(){
		for(HolderAccountFileRequest holderAccountFile: holderAccountFilesRequest){
			if(holderAccountFile.getDocumentType().equals(HolderAccountFileType.FORM_OPEN_ACCOUNT.getCode())){
				holderAccountFilesRequest.remove(holderAccountFile);return;
			}
		}
	}
	
	/**
	 * Have form bank account.
	 *
	 * @return the boolean
	 */
	public boolean getHaveFormBankAccount(){
		if(holderAccountFilesRequest==null || holderAccountFilesRequest.isEmpty()){
			return Boolean.FALSE;
		}
		for(HolderAccountFileRequest holderAccountFile: holderAccountFilesRequest){
			if(holderAccountFile.getDocumentType().equals(HolderAccountFileType.FORM_BANK_ACCOUNT.getCode())){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Have form know client.
	 *
	 * @return the boolean
	 */
	public boolean getHaveFormKnowClient(){
		if(holderAccountFilesRequest==null || holderAccountFilesRequest.isEmpty()){
			return Boolean.FALSE;
		}
		for(HolderAccountFileRequest holderAccountFile: holderAccountFilesRequest){
			if(holderAccountFile.getDocumentType().equals(HolderAccountFileType.FORM_KNOW_CLIENT.getCode())){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Have form open account.
	 *
	 * @return the boolean
	 */
	public boolean getHaveFormOpenAccount(){
		if(holderAccountFilesRequest==null || holderAccountFilesRequest.isEmpty()){
			return Boolean.FALSE;
		}
		for(HolderAccountFileRequest holderAccountFile: holderAccountFilesRequest){
			if(holderAccountFile.getDocumentType().equals(HolderAccountFileType.FORM_OPEN_ACCOUNT.getCode())){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Gets the tamanio doc block file.
	 *
	 * @return the tamanio doc block file
	 */
	public String getTamanioDocBlockFile(){
		if(Validations.validateIsNotNull(this.getDocBlock())){
			return new StringBuilder().append(Double.parseDouble(String.valueOf(this.getDocBlock().length))/1000).append(" Kb").toString();
		}
		return "";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HolderAccountRequest [idHolderAccountReqPk="
				+ idHolderAccountReqPk + ", confirmDate=" + confirmDate
				+ ", confirmUser=" + confirmUser + ", holderAccountGroup="
				+ holderAccountGroup + ", holderAccountType="
				+ holderAccountType + ", lastModifyApp=" + lastModifyApp
				+ ", lastModifyDate=" + lastModifyDate + ", lastModifyIp="
				+ lastModifyIp + ", lastModifyUser=" + lastModifyUser
				+ ", requestMotive=" + requestMotive + ", requestOtherMotive="
				+ requestOtherMotive + ", registryDate=" + registryDate
				+ ", registryUser=" + registryUser + ", rejectDate="
				+ rejectDate + ", rejectUser=" + rejectUser + ", anullDate="
				+ anullDate + ", anullUser=" + anullUser + ", approveDate="
				+ approveDate + ", approveUser=" + approveUser
				+ ", requestNumber=" + requestNumber + ", requestType="
				+ requestType + ", requestState="
				+ requestState + ", participant="
				+ (participant!=null?Hibernate.isInitialized(participant)?participant.getIdParticipantPk():"":"") + ", docBlock="
				+ Arrays.toString(docBlock) 				
				+ ", comments=" + comments 
				+ ", pdd = " + indPdd
				+ ", holderAccount=" + holderAccount
				+ ", holderAccountReqBankHys=" + (holderAccountBankRequest!=null?Hibernate.isInitialized(holderAccountBankRequest)?holderAccountBankRequest:null:null)
				+ ", holderAccountReqFile=" + (holderAccountFilesRequest!=null?Hibernate.isInitialized(holderAccountFilesRequest)?holderAccountFilesRequest:null:null)
				+ ", holderAccountReqDetHys=" + (holderAccountDetRequest!=null?Hibernate.isInitialized(holderAccountDetRequest)?holderAccountDetRequest:null:null) 
				+"]";
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
            if(loggerUser.getAuditTime() != null) {
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null) {
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }            
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("holderAccountReqDetHys", holderAccountDetRequest);
        detailsMap.put("holderAccountReqBankHys", holderAccountBankRequest);
        detailsMap.put("holderAccountReqFiles", holderAccountFilesRequest);
        return detailsMap;
	}	
	
	/**
	 * Delete block doc.
	 */
	public void deleteBlockDoc(){
		docBlock = null;
	}
	
	/**
	 * Gets the have block doc.
	 *
	 * @return the have block doc
	 */
	public boolean getHaveBlockDoc(){
		return docBlock!=null;
	}	
	
	/**
	 * Delete block doc act.
	 */
	public void deleteBlockDocAct(){
		docBlockAct = null;
	}
	
	/**
	 * Gets the have block doc act.
	 *
	 * @return the have block doc act
	 */
	public boolean getHaveBlockDocAct(){
		return docBlockAct!=null;
	}

	/**
	 * Instantiates a new holder account request hy.
	 *
	 * @param idHolderAccountReqPk the id holder account req pk
	 * @param requestState the request state
	 */
	public HolderAccountRequest(Long idHolderAccountReqPk, Integer requestState) {
		super();
		this.idHolderAccountReqPk = idHolderAccountReqPk;
		this.requestState = requestState;
	}
	
	/**
	 * Gets the Related Name.
	 *
	 * @return the Related Name.
	 */
	public String getRelatedName() {
		return relatedName;
	}
	
	/**
	 * Sets the Related Name.
	 *
	 * @param relatedName the new related name
	 */
	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	/**
	 * Gets the state account Name.
	 *
	 * @return the state account.
	 */
	public String getStateAccountName() {
		return stateAccountName;
	}

	/**
	 * Sets the state account Name.
	 *
	 * @param stateAccountName the new state account name
	 */
	public void setStateAccountName(String stateAccountName) {
		this.stateAccountName = stateAccountName;
	}

	/**
	 * Gets the request type Name.
	 *
	 * @return the request type Name.
	 */
	public String getRequestTypeName() {
		return requestTypeName;
	}

	/**
	 * Sets the request type Name.
	 *
	 * @param requestTypeName the new request type name
	 */
	public void setRequestTypeName(String requestTypeName) {
		this.requestTypeName = requestTypeName;
	}

	public Integer getIndPdd() {
		return indPdd;
	}

	public void setIndPdd(Integer indPdd) {
		this.indPdd = indPdd;
	}

	public byte[] getDocumentPdd() {
		return documentPdd;
	}

	public void setDocumentPdd(byte[] documentPdd) {
		this.documentPdd = documentPdd;
	}

	public boolean isBlPdd() {
		return blPdd;
	}

	public void setBlPdd(boolean blPdd) {
		this.blPdd = blPdd;
	}

	public String getNameDocumentPdd() {
		return nameDocumentPdd;
	}

	public void setNameDocumentPdd(String nameDocumentPdd) {
		this.nameDocumentPdd = nameDocumentPdd;
	}

	public String getRelatedNameOriginal() {
		return relatedNameOriginal;
	}

	public void setRelatedNameOriginal(String relatedNameOriginal) {
		this.relatedNameOriginal = relatedNameOriginal;
	}

	public Integer getIndPddOriginal() {
		return indPddOriginal;
	}

	public void setIndPddOriginal(Integer indPddOriginal) {
		this.indPddOriginal = indPddOriginal;
	}

	public byte[] getDocumentPddOriginal() {
		return documentPddOriginal;
	}

	public void setDocumentPddOriginal(byte[] documentPddOriginal) {
		this.documentPddOriginal = documentPddOriginal;
	}

	public String getNameDocumentPddOriginal() {
		return nameDocumentPddOriginal;
	}

	public void setNameDocumentPddOriginal(String nameDocumentPddOriginal) {
		this.nameDocumentPddOriginal = nameDocumentPddOriginal;
	}

	/**
	 * @return the stockCode
	 */
	public Integer getStockCode() {
		return stockCode;
	}

	/**
	 * @param stockCode the stockCode to set
	 */
	public void setStockCode(Integer stockCode) {
		this.stockCode = stockCode;
	}	
	
}