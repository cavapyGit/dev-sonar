package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeMajorForeignNoResidentDocHolderNatType {
		
		CE(Integer.valueOf(901),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(902),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(903),"PASAPORTE"), 
		LIC(Integer.valueOf(904),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(905),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(906),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(907),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(908),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(909),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(910),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(911),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(912),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(913),"DECRETO"),		
	    RCF(Integer.valueOf(914),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(915),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(916),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    		
		private Integer code;
		private String value;
		
		public static final List<AgeMajorForeignNoResidentDocHolderNatType> list = new ArrayList<AgeMajorForeignNoResidentDocHolderNatType>();
		public static final Map<Integer, AgeMajorForeignNoResidentDocHolderNatType> lookup = new HashMap<Integer, AgeMajorForeignNoResidentDocHolderNatType>();

		static {
			for (AgeMajorForeignNoResidentDocHolderNatType s : EnumSet.allOf(AgeMajorForeignNoResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeMajorForeignNoResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeMajorForeignNoResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
