package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum BankDocRepreNatType {
		
		POR(Integer.valueOf(348),"PODER OTORGADO COMO REPRESENTANTE",Integer.valueOf(193)),
		CI(Integer.valueOf(349),"CEDULA DE IDENTIDAD",Integer.valueOf(193)), 
		PAS(Integer.valueOf(350),"PASAPORTE",Integer.valueOf(193)),
		LIC(Integer.valueOf(351),"LICENCIA DE CONDUCIR",Integer.valueOf(193)),
		DIO(Integer.valueOf(352),"DOCUMENTO DE IDENTIDAD DE PAIS ORIGEN",Integer.valueOf(193)),
		RNC(Integer.valueOf(353),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(193)), 
		DJCP(Integer.valueOf(354),"DECLARACION JURADA SOBRE CONDICION DE PEP",Integer.valueOf(193)),
	    OC(Integer.valueOf(355),"ORDEN DE CUSTODIA",Integer.valueOf(193)),
		CATL(Integer.valueOf(356),"DOCUMENTO DE ACREDITACION COMO TUTOR LEGAL",Integer.valueOf(193)),
		CDRL(Integer.valueOf(357),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(193));	
		
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<BankDocRepreNatType> list = new ArrayList<BankDocRepreNatType>();
		public static final Map<Integer, BankDocRepreNatType> lookup = new HashMap<Integer, BankDocRepreNatType>();

		static {
			for (BankDocRepreNatType s : EnumSet.allOf(BankDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private BankDocRepreNatType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static BankDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
