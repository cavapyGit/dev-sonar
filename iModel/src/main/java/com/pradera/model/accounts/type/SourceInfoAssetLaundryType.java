package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SourceInfoAssetLaundryType {
	
	TV(new Integer(437),"TV"),
	PRENSA(new Integer(438),"PRENSA"),
	RADIO(new Integer(439),"RADIO");
	
	private Integer code;
	private String value;
	
	public static final List<SourceInfoAssetLaundryType> list= new ArrayList<SourceInfoAssetLaundryType>();
	public static final Map<Integer, SourceInfoAssetLaundryType> lookup = new HashMap<Integer, SourceInfoAssetLaundryType >();
	
	static{
		for(SourceInfoAssetLaundryType type: EnumSet.allOf(SourceInfoAssetLaundryType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);
			
			
		}
		
		
	}
	
	
	
	private SourceInfoAssetLaundryType(Integer code, String value){
		this.code = code;
		this.value = value;
		
	}


	public Integer getCode() {
		return code;
	}


	public void setCode(Integer code) {
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}
	
	public static SourceInfoAssetLaundryType get(Integer code)
	{
		return lookup.get(code);
	}



}
