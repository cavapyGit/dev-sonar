package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantFileType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public enum RepresentativeFileType {
	
	/** The passport. */
	PASSPORT(Integer.valueOf(310),"PASAPORTE"),
	
	REPRESENTATIVECERTIFICATIONFILE(Integer.valueOf(317),"CERTIFICACIÓN QUE DESIGNA AL REPRESENTANTE LEGAL"),
	
	PHOTOCOCOPYREPRESENTATIVETESTIMONY(Integer.valueOf(268),"TESTIMONIO O FOTOCOPIA LEGALIZADA DEL PODER OTORGADO COMO REPRESENTANTE LEGAL"),
	
	PHOTOCOCOPYPASSPORT(Integer.valueOf(270),"FOTOCOPIA PASAPORTE"),
	
	PHOTOCOCOPYIDENTITYDOCUMENTCOUNTRYORIGIN(Integer.valueOf(272),"FOTOCOPIA DEL DOCUMENTO DE IDENTIDAD DE PAIS DE ORIGEN"),
	
	PEPAFFIDAVIT(Integer.valueOf(274),"DECLARACION JURADA SOBRE CONDICION DE PEP"),
	
	PHOTOCOCOPYPASSPORTDAD(Integer.valueOf(280),"FOTOCOPIA PASAPORTE"),
	
	PHOTOCOCOPYIDENTITYDOCUMENTDAD(Integer.valueOf(279),"FOTOCOPIA CEDULA DE IDENTIDAD"),
	
	PHOTOCOCOPYIDENTITYDOCUMENTCOUNTRYORIGINDAD(Integer.valueOf(282),"FOTOCOPIA DEL DOCUMENTO DE IDENTIDAD DE PAIS DE ORIGEN"),
	
	PEPAFFIDAVITDAD(Integer.valueOf(284),"DECLARACION JURADA SOBRE CONDICION DE PEP"),
	
	BIRTHCERTIFICATEDAD(Integer.valueOf(285),"CERTIFICADO DE NACIMIENTO DEL TITULAR A QUIEN REPRESENTA"),
	
	PHOTOCOCOPYPASSPORTREPRESENTATIVE(Integer.valueOf(290),"FOTOCOPIA PASAPORTE"),
	
	PHOTOCOCOPYIDENTITYDOCUMENTREPRESENTATIVE(Integer.valueOf(289),"FOTOCOPIA CEDULA DE IDENTIDAD"),
	
	PHOTOCOCOPYIDENTITYDOCUMENTCOUNTRYORIGINREPRESENTATIVE(Integer.valueOf(292),"FOTOCOPIA DEL DOCUMENTO DE IDENTIDAD DE PAIS DE ORIGEN"),
	
	PEPAFFIDAVITREPRESENTATIVE(Integer.valueOf(294),"DECLARACION JURADA SOBRE CONDICION DE PEP"),
	
	DESIGNATIONOFAUTHORITYREPRESENTATIVE(Integer.valueOf(296),"DESIGNACION DE AUTORIDAD COMPETENTE QUE ENCOMIENDE LA TUTELA O CURATELA");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant file type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private RepresentativeFileType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<RepresentativeFileType> list = new ArrayList<RepresentativeFileType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RepresentativeFileType> lookup = new HashMap<Integer, RepresentativeFileType>();
	static {
		for (RepresentativeFileType s : EnumSet.allOf(RepresentativeFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
