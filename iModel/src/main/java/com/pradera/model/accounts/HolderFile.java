package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;





import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_FILE database table.
 * 
 */
@Entity
@Table(name="HOLDER_FILE")
public class HolderFile implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder file pk. */
	@Id
	@SequenceGenerator(name="HOLDER_FILE_IDHOLDERFILEPK_GENERATOR", sequenceName="SQ_ID_HOLDER_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_FILE_IDHOLDERFILEPK_GENERATOR")
	@Column(name="ID_HOLDER_FILE_PK")
	private Long idHolderFilePk;

	/** The description. */
	private String description;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

    /** The file holder. */
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_FILE")
	private byte[] fileHolder;

	/** The filename. */
	private String filename;

	/** The holder file type. */
	@Column(name="HOLDER_FILE_TYPE")
	private Integer holderFileType;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state file. */
	@Column(name="STATE_FILE")
	private Integer stateFile;
	
	/** The holder. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;
    
	/** The was modified. */
	@Transient
	private boolean wasModified;

    /**
     * Instantiates a new holder file.
     */
    public HolderFile() {
    }

	/**
	 * Gets the id holder file pk.
	 *
	 * @return the id holder file pk
	 */
	public Long getIdHolderFilePk() {
		return this.idHolderFilePk;
	}

	/**
	 * Sets the id holder file pk.
	 *
	 * @param idHolderFilePk the new id holder file pk
	 */
	public void setIdHolderFilePk(Long idHolderFilePk) {
		this.idHolderFilePk = idHolderFilePk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the file holder.
	 *
	 * @return the file holder
	 */
	public byte[] getFileHolder() {
		return this.fileHolder;
	}

	/**
	 * Sets the file holder.
	 *
	 * @param fileHolder the new file holder
	 */
	public void setFileHolder(byte[] fileHolder) {
		this.fileHolder = fileHolder;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return this.filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the holder file type.
	 *
	 * @return the holder file type
	 */
	public Integer getHolderFileType() {
		return this.holderFileType;
	}

	/**
	 * Sets the holder file type.
	 *
	 * @param holderFileType the new holder file type
	 */
	public void setHolderFileType(Integer holderFileType) {
		this.holderFileType = holderFileType;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return this.registryType;
	}

	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state file.
	 *
	 * @return the state file
	 */
	public Integer getStateFile() {
		return this.stateFile;
	}

	/**
	 * Sets the state file.
	 *
	 * @param stateFile the new state file
	 */
	public void setStateFile(Integer stateFile) {
		this.stateFile = stateFile;
	}
	
	/**
	 * Checks if is was modified.
	 *
	 * @return true, if is was modified
	 */
	public boolean isWasModified() {
		return wasModified;
	}

	/**
	 * Sets the was modified.
	 *
	 * @param wasModified the new was modified
	 */
	public void setWasModified(boolean wasModified) {
		this.wasModified = wasModified;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
            if(loggerUser.getAuditTime() != null) {
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null) {
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }            
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}