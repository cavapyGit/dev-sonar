package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * The Enum ParticipantBlockUnblockRequestType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 13/03/2013
 */
public enum IssuerRequestStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(70),"REGISTRADO"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(71),"CONFIRMADO"),
	
	/** The rejected. */
	REJECTED(Integer.valueOf(72),"RECHAZADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuerRequestStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<IssuerRequestStateType> list = new ArrayList<IssuerRequestStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, IssuerRequestStateType> lookup = new HashMap<Integer, IssuerRequestStateType>();
	static {
		for (IssuerRequestStateType s : EnumSet.allOf(IssuerRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant block unblock request type
	 */
	public static IssuerRequestStateType get(Integer code) {
		return lookup.get(code);
	}

}
