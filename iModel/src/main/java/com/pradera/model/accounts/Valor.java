package com.pradera.model.accounts;

import java.io.Serializable;

public class Valor implements Serializable{
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;
	private String codigoValor;
	private String codigoCfiValor;
	private String descripcionValor;
	private String nemonicoValor;
	private String codeIsin;
	private Integer estadoValor;
	private Integer balanceAccountant;
	private Integer balanceAvailable;
	private Integer quantityTransfer;
	private boolean seleccionado;
	private Integer idGroupPK;
	
	
	
	public Valor(){}

	public Valor(String codigoValor, String codigoCfiValor,
			String descripcionValor, String nemonicoValor, String codeIsin,
			Integer estadoValor, Integer balanceAccountant,
			Integer balanceAvailable, Integer quantityTransfer,
			boolean seleccionado) {
		super();
		this.codigoValor = codigoValor;
		this.codigoCfiValor = codigoCfiValor;
		this.descripcionValor = descripcionValor;
		this.nemonicoValor = nemonicoValor;
		this.codeIsin = codeIsin;
		this.estadoValor = estadoValor;
		this.balanceAccountant = balanceAccountant;
		this.balanceAvailable = balanceAvailable;
		this.quantityTransfer = quantityTransfer;
		this.seleccionado = seleccionado;
	}



	public Integer getQuantityTransfer() {
		return quantityTransfer;
	}

	public void setQuantityTransfer(Integer quantityTransfer) {
		this.quantityTransfer = quantityTransfer;
	}

	public String getCodeIsin() {
		return codeIsin;
	}

	public void setCodeIsin(String codeIsin) {
		this.codeIsin = codeIsin;
	}

	public boolean isSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	public Integer getBalanceAccountant() {
		return balanceAccountant;
	}

	public void setBalanceAccountant(Integer balanceAccountant) {
		this.balanceAccountant = balanceAccountant;
	}

	public Integer getBalanceAvailable() {
		return balanceAvailable;
	}

	public void setBalanceAvailable(Integer balanceAvailable) {
		this.balanceAvailable = balanceAvailable;
	}

	public String getCodigoCfiValor() {
		return codigoCfiValor;
	}
	public void setCodigoCfiValor(String codigoCfiValor) {
		this.codigoCfiValor = codigoCfiValor;
	}
	
	public String getCodigoValor() {
		return codigoValor;
	}
	public void setCodigoValor(String codigoValor) {
		this.codigoValor = codigoValor;
	}
	public String getDescripcionValor() {
		return descripcionValor;
	}
	public void setDescripcionValor(String descripcionValor) {
		this.descripcionValor = descripcionValor;
	}
	public String getNemonicoValor() {
		return nemonicoValor;
	}
	public void setNemonicoValor(String nemonicoValor) {
		this.nemonicoValor = nemonicoValor;
	}
	public Integer getEstadoValor() {
		return estadoValor;
	}
	public void setEstadoValor(Integer estadoValor) {
		this.estadoValor = estadoValor;
	}

	/**
	 * @return the idGroupPK
	 */
	public Integer getIdGroupPK() {
		return idGroupPK;
	}

	/**
	 * @param idGroupPK the idGroupPK to set
	 */
	public void setIdGroupPK(Integer idGroupPK) {
		this.idGroupPK = idGroupPK;
	}
	
}
