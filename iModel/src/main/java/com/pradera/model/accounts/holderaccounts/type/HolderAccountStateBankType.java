/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountStatusType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
public enum HolderAccountStateBankType {
	
	/** The active. */
	REGISTERED(new Integer(1176),"REGISTERED", "ACTIVO"),
	
	/** The block. */
	DELETED(new Integer(1177),"DELETED", "INACTIVO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The value. */
	private String description;
	
	/** The Constant list. */
	public static final List<HolderAccountStateBankType> list = new ArrayList<HolderAccountStateBankType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountStateBankType> lookup = new HashMap<Integer, HolderAccountStateBankType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountStateBankType> listSomeElements(HolderAccountStateBankType... transferSecuritiesTypeParams){
		List<HolderAccountStateBankType> retorno = new ArrayList<HolderAccountStateBankType>();
		for(HolderAccountStateBankType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountStateBankType s : EnumSet.allOf(HolderAccountStateBankType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Instantiates a new holder account status type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountStateBankType(Integer codigo, String valor, String description) {
		this.code = codigo;
		this.value = valor;
		this.description = description;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account status type
	 */
	public static HolderAccountStateBankType get(Integer codigo) {
		return lookup.get(codigo);
	}
}