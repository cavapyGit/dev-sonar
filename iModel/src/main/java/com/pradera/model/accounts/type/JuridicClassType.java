package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum JuridicClassType {
		 
		NORMAL(Integer.valueOf(391),"NORMAL"), // 1 (comercial)
		ENTITY_STATE(Integer.valueOf(392),"ENTIDAD ESTATAL"), // 2	
		TRUST(Integer.valueOf(393),"FIDEICOMISO/FOGAVISP"); //3
		// FINANCIAL_ENTITY(Integer.valueOf(531),"ENTIDAD FINANCIERA"); not used ???
		
		
		private Integer code;
		private String value;
		

		public static final List<JuridicClassType> list = new ArrayList<JuridicClassType>();
		public static final Map<Integer, JuridicClassType> lookup = new HashMap<Integer, JuridicClassType>();

		static {
			for (JuridicClassType s : EnumSet.allOf(JuridicClassType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private JuridicClassType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static JuridicClassType get(Integer code) {
			return lookup.get(code);
		}

		
}
