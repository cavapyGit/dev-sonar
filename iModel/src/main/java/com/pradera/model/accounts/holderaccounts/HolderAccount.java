package com.pradera.model.accounts.holderaccounts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.LazyInitializationException;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountFileType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.constants.Constants;
import com.pradera.model.corporatives.CustodyCommissionDetail;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegParticStrucHy;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegParticipaStruct;
import com.pradera.model.negotiation.HolderAccountOperation;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT database table.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
@Entity
@NamedQueries({
	@NamedQuery(
		name = HolderAccount.HOLDER_ACCOUNT_STATE, 
		query = "Select new com.pradera.model.accounts.holderaccounts.HolderAccount(ha.idHolderAccountPk,ha.stateAccount,ha.accountNumber) From HolderAccount ha WHERE ha.idHolderAccountPk = :idHolderAccountPkParam")
})
@Table(name = "HOLDER_ACCOUNT")
public class HolderAccount implements Serializable, Auditable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant HOLDER_ACCOUNT_STATE. */
	public static final String HOLDER_ACCOUNT_STATE ="holderAccount.State";

	/** The id holder account pk. */
	@Id
	@SequenceGenerator(name = "SQ_HOLDER_ACCOUNT_PK", sequenceName = "SQ_ID_HOLDER_ACCOUNT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_HOLDER_ACCOUNT_PK")
	@Column(name = "ID_HOLDER_ACCOUNT_PK")
	private Long idHolderAccountPk;

	/** The account description. */
	@Transient
	private String accountDescription;

	/** The account doc type and number. */
	@Transient
	private String accountDocTypeAndNumber;
	
	/** The account description. */
	@Column(name = "DESCRIPTION")
	private String description;
	
	/** The account group. */
	@Column(name = "ACCOUNT_GROUP")
	private Integer accountGroup;

	/** The account number. */
	@Column(name = "ACCOUNT_NUMBER")
	private Integer accountNumber;

	/** The account type. */
	@Column(name = "ACCOUNT_TYPE")
	private Integer accountType;

	/** The alternate code. */
	@Column(name = "ALTERNATE_CODE")
	private String alternateCode;

	/** The closing date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CLOSING_DATE")
	private Date closingDate;

	/** The participant. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PARTICIPANT_FK")
	private Participant participant;

	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The opening date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "OPENING_DATE")
	private Date openingDate;

	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registryUser;

	/** The state account. */
	@Column(name = "STATE_ACCOUNT")
	private Integer stateAccount;
	
	/** The related name. */
	@Column(name = "RELATED_NAME")
	private String relatedName;

	/** The related account. */
	@Column(name = "RELATED_ACCOUNT")
	private Integer relatedAccount;
	/** The state account name. */
	@Transient
	private String stateAccountName;
	
	/** The related name. */
	@Column(name = "IND_PDD")
	private Integer indPdd;
	
	@Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_PDD")
	private byte[] documentPdd;
	
	/** The related name. */
	@Column(name = "NAME_DOCUMENT_PDD")
	private String nameDocumentPdd;
	
	@Column(name = "STOCK_CODE")
	private Integer stockCode;

	/** The selected. */
	@Transient
	private Boolean selected = Boolean.FALSE;

	/** The full name. */
	@Transient
	private String fullName;
	// bi-directional many-to-one association to HolderAccountBalance
	/** The holder account balances. */
	@Transient
	private List<String> holderDescriptionList;
	
	/** The holder account balances. */
	@OneToMany(mappedBy = "holderAccount")
	private List<HolderAccountBalance> holderAccountBalances;

	// bi-directional many-to-one association to HolderAccountBank
	/** The holder account banks. */
	@OneToMany(mappedBy = "holderAccount", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<HolderAccountBank> holderAccountBanks;

	/** The holder account files. */
	@OneToMany(mappedBy = "holderAccount", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<HolderAccountFile> holderAccountFiles;

	// bi-directional many-to-one association to HolderAccountDetail
	/** The holder account details. */
	@OneToMany(mappedBy = "holderAccount", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<HolderAccountDetail> holderAccountDetails;

	// bi-directional many-to-one association to HolderAccountOperation
	/** The holder account operations. */
	@OneToMany(mappedBy = "holderAccount")
	private List<HolderAccountOperation> holderAccountOperations;

	// bi-directional many-to-one association to HolderAccountRequestHy
	/** The holder account request hys. */
	@OneToMany(mappedBy = "holderAccount")
	private List<HolderAccountRequest> holderAccountRequest;

	/** The holder account request hys. */
	@OneToMany(mappedBy = "holderAccount")
	private List<HolderAccountAdjusment> holderAccountAdjuments;

	/** The placement seg participa structs. */
	@OneToMany(mappedBy = "holderAccount")
	private List<PlacementSegParticipaStruct> placementSegParticipaStructs;

	/** The placement seg partic struc hy. */
	@OneToMany(mappedBy = "holderAccount")
	private List<PlacementSegParticStrucHy> placementSegParticStrucHy;

	/** The custody commission detail list. */
	@OneToMany(mappedBy="holderAccount")
	private List<CustodyCommissionDetail> custodyCommissionDetailList;
	
	/** The holder. */
	@Transient
	private Holder holder;
	
	/** The state account description. */
	@Transient
	private String stateAccountDescription;

	/**
	 * Instantiates a new holder account.
	 */
	public HolderAccount() {
	}
	
	/**
	 * Instantiates a new holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 */
	public HolderAccount(Long idHolderAccountPk) {
		super();
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName            the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Gets the id holder account pk.
	 * 
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return this.idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 * 
	 * @param idHolderAccountPk
	 *            the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the account description. this method is useful to get Account with
	 * All holders in format Html
	 * 
	 * @return the account description
	 */
	public String getAccountDescription() {
		return accountDescription;
	}

	/**
	 * Gets the account rnt and description.
	 *
	 * @return the account rnt and description
	 */
	public String getAccountRntAndDescription() {
		try {
			StringBuilder sbResult = new StringBuilder();		
			if (Validations.validateListIsNotNullAndNotEmpty(holderAccountDetails)) {
				List<HolderAccountDetail> filteredHolderAccountDetails = holderAccountDetails.stream()
						.distinct()
						.collect(Collectors.toList());
				for (HolderAccountDetail holderAccountDetail : filteredHolderAccountDetails) {
					if(Validations.validateIsNotNullAndNotEmpty(holderAccountDetail.getHolder())){
						sbResult.append(Constants.ASTERISK);
						sbResult.append(holderAccountDetail.getHolder().getIdHolderPk());
						sbResult.append(Constants.DASH);
						sbResult.append(holderAccountDetail.getHolder().getDescriptionHolder());
						//sbResult.append(Constants.BR);
					}
				}
			}
			return sbResult.toString();
		} catch (LazyInitializationException e) {
			return null;
		}		
	}

	/**
	 * Gets the account doc type and number.
	 *
	 * @return the account doc type and number
	 */
	public String getAccountDocTypeAndNumber() {

		if (accountDocTypeAndNumber == null) {
			if (holderAccountDetails != null && !holderAccountDetails.isEmpty()) {
				StringBuilder stringBuilder = new StringBuilder();
				for (HolderAccountDetail holderAccountDetail : this.getHolderAccountDetails()) {
					stringBuilder.append("* &nbsp")
									.append(holderAccountDetail.getHolder().getDocumentType() != null ? holderAccountDetail.getHolder().getDocumentTypeDescription(): "")
									.append(" - ")
									.append(holderAccountDetail.getHolder().getDocumentNumber() != null ? holderAccountDetail.getHolder().getDocumentNumber(): "").append("<br>");
				}
				accountDocTypeAndNumber = stringBuilder.toString();
			} else {
				accountDocTypeAndNumber = "";
			}
		}

		return accountDocTypeAndNumber;
	}
	
	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {

		if (accountDocTypeAndNumber == null) {
			if (holderAccountDetails != null && !holderAccountDetails.isEmpty()) {
				StringBuilder stringBuilder = new StringBuilder();
				for (HolderAccountDetail holderAccountDetail : holderAccountDetails) {
					stringBuilder.append("* &nbsp")
									.append(holderAccountDetail.getHolder().getFullName()).append("<br>");
				}
				accountDocTypeAndNumber = stringBuilder.toString();
			} else {
				accountDocTypeAndNumber = "";
			}
		}

		return accountDocTypeAndNumber;
	}
	
	/**
	 * Gets the doc type doc number holder description.
	 *
	 * @return the doc type doc number holder description
	 */
	public String getDocTypeDocNumberHolderDescription(){
		StringBuilder stringBuilder = new StringBuilder();
		if (holderAccountDetails != null && !holderAccountDetails.isEmpty()) {
			for (HolderAccountDetail holderAccountDetail : holderAccountDetails) {
				stringBuilder.append("* &nbsp")
								.append(DocumentType.get(holderAccountDetail.getHolder().getDocumentType()).name())
								.append(Constants.DASH)
								.append(holderAccountDetail.getHolder().getDocumentNumber())
								.append(Constants.DASH)
								.append(holderAccountDetail.getHolder().getFullName())
								.append("<br>");
				}
		}
		return stringBuilder.toString();
	}

	/**
	 * Sets the account description.
	 * 
	 * @param accountDescription
	 *            the new account description
	 */
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	/**
	 * Gets the account group.
	 * 
	 * @return the account group
	 */
	public Integer getAccountGroup() {
		return this.accountGroup;
	}

	/**
	 * Sets the account group.
	 * 
	 * @param accountGroup
	 *            the new account group
	 */
	public void setAccountGroup(Integer accountGroup) {
		this.accountGroup = accountGroup;
	}

	/**
	 * Gets the account type.
	 * 
	 * @return the account type
	 */
	public Integer getAccountType() {
		return this.accountType;
	}

	/**
	 * Gets the account type description.
	 * 
	 * @return the account type description
	 */
	public String getAccountTypeDescription() {
		return HolderAccountType.lookup.get(this.accountType).getValue();
	}

	/**
	 * Sets the account type.
	 * 
	 * @param accountType
	 *            the new account type
	 */
	
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}


	/**
	 * Gets the alternate code.
	 * 
	 * @return the alternate code
	 */
	public String getAlternateCode() {
		return this.alternateCode;
	}

	/**
	 * Sets the alternate code.
	 * 
	 * @param alternateCode
	 *            the new alternate code
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	/**
	 * Gets the closing date.
	 * 
	 * @return the closing date
	 */
	public Date getClosingDate() {
		return this.closingDate;
	}

	/**
	 * Sets the closing date.
	 * 
	 * @param closingDate
	 *            the new closing date
	 */
	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	/**
	 * Gets the participant.
	 * 
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 * 
	 * @param participant
	 *            the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the last modify app.
	 * 
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 * 
	 * @param lastModifyApp
	 *            the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 * 
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 * 
	 * @param lastModifyDate
	 *            the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 * 
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 * 
	 * @param lastModifyIp
	 *            the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 * 
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 * 
	 * @param lastModifyUser
	 *            the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the opening date.
	 * 
	 * @return the opening date
	 */
	public Date getOpeningDate() {
		return this.openingDate;
	}

	/**
	 * Sets the opening date.
	 * 
	 * @param openingDate
	 *            the new opening date
	 */
	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	/**
	 * Gets the registry date.
	 * 
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 * 
	 * @param registryDate
	 *            the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 * 
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 * 
	 * @param registryUser
	 *            the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state account.
	 * 
	 * @return the state account
	 */
	public Integer getStateAccount() {
		return this.stateAccount;
	}

	/**
	 * Sets the state account.
	 * 
	 * @param stateAccount
	 *            the new state account
	 */
	public void setStateAccount(Integer stateAccount) {
		this.stateAccount = stateAccount;
	}

	/**
	 * Gets the holder account balances.
	 * 
	 * @return the holder account balances
	 */
	public List<HolderAccountBalance> getHolderAccountBalances() {
		return this.holderAccountBalances;
	}

	/**
	 * Sets the holder account balances.
	 * 
	 * @param holderAccountBalances
	 *            the new holder account balances
	 */
	public void setHolderAccountBalances(
			List<HolderAccountBalance> holderAccountBalances) {
		this.holderAccountBalances = holderAccountBalances;
	}

	/**
	 * Gets the holder account banks.
	 * 
	 * @return the holder account banks
	 */
	public List<HolderAccountBank> getHolderAccountBanks() {
		return this.holderAccountBanks;
	}

	/**
	 * Sets the holder account banks.
	 * 
	 * @param holderAccountBanks
	 *            the new holder account banks
	 */
	public void setHolderAccountBanks(List<HolderAccountBank> holderAccountBanks) {
		this.holderAccountBanks = holderAccountBanks;
	}

	/**
	 * Gets the holder account details.
	 * 
	 * @return the holder account details
	 */
	public List<HolderAccountDetail> getHolderAccountDetails() {
		return this.holderAccountDetails;
	}

	/**
	 * Sets the holder account details.
	 * 
	 * @param holderAccountDetails
	 *            the new holder account details
	 */
	public void setHolderAccountDetails(
			List<HolderAccountDetail> holderAccountDetails) {
		this.holderAccountDetails = holderAccountDetails;
	}

	/**
	 * Gets the holder account operations.
	 * 
	 * @return the holder account operations
	 */
	public List<HolderAccountOperation> getHolderAccountOperations() {
		return this.holderAccountOperations;
	}

	/**
	 * Sets the holder account operations.
	 * 
	 * @param holderAccountOperations
	 *            the new holder account operations
	 */
	public void setHolderAccountOperations(
			List<HolderAccountOperation> holderAccountOperations) {
		this.holderAccountOperations = holderAccountOperations;
	}
	
	/**
	 * Gets the holder account request hys.
	 * 
	 * @return the holder account request hys
	 */
	public List<HolderAccountRequest> getHolderAccountRequest() {
		return this.holderAccountRequest;
	}

	/**
	 * Sets the holder account request hys.
	 *
	 * @param holderAccountRequest the new holder account request
	 */
	public void setHolderAccountRequest(
			List<HolderAccountRequest> holderAccountRequest) {
		this.holderAccountRequest = holderAccountRequest;
	}

	/**
	 * Gets the holder account adjuments.
	 *
	 * @return the holderAccountAdjuments
	 */
	public List<HolderAccountAdjusment> getHolderAccountAdjuments() {
		return holderAccountAdjuments;
	}

	/**
	 * Sets the holder account adjuments.
	 *
	 * @param holderAccountAdjuments            the holderAccountAdjuments to set
	 */
	public void setHolderAccountAdjuments(
			List<HolderAccountAdjusment> holderAccountAdjuments) {
		this.holderAccountAdjuments = holderAccountAdjuments;
	}

	/**
	 * Gets the state account name.
	 * 
	 * @return the state account name
	 */
	public String getStateAccountName() {
		return stateAccountName;
	}

	/**
	 * Sets the state account name.
	 * 
	 * @param stateAccountName
	 *            the new state account name
	 */
	public void setStateAccountName(String stateAccountName) {
		this.stateAccountName = stateAccountName;
	}

	/**
	 * Gets the holder account files.
	 * 
	 * @return the holder account files
	 */
	public List<HolderAccountFile> getHolderAccountFiles() {
		return holderAccountFiles;
	}

	/**
	 * Sets the holder account files.
	 * 
	 * @param holderAccountFiles
	 *            the holder account files
	 */
	public void setHolderAccountFiles(List<HolderAccountFile> holderAccountFiles) {
		this.holderAccountFiles = holderAccountFiles;
	}

//	/**
//	 * Gets the stream content form bank account.
//	 * 
//	 * @return the stream content form bank account
//	 */
//	public StreamedContent getStreamContentFormBankAccount() {
//		InputStream is = null;
//		for (HolderAccountFile holderAccountFile : holderAccountFiles) {
//			if (holderAccountFile.getDocumentType().equals(
//					HolderAccountFileType.FORM_BANK_ACCOUNT.getCode())) {
//				is = new ByteArrayInputStream(
//						holderAccountFile.getDocumentFile());
//				return new DefaultStreamedContent(is, "application/pdf",
//						holderAccountFile.getFilename());
//			}
//		}
//		return null;
//	}

//	/**
//	 * Gets the stream content form know client.
//	 * 
//	 * @return the stream content form know client
//	 */
//	public StreamedContent getStreamContentFormKnowClient() {
//		InputStream is = null;
//		for (HolderAccountFile holderAccountFile : holderAccountFiles) {
//			if (holderAccountFile.getDocumentType().equals(
//					HolderAccountFileType.FORM_KNOW_CLIENT.getCode())) {
//				is = new ByteArrayInputStream(
//						holderAccountFile.getDocumentFile());
//				return new DefaultStreamedContent(is, "application/pdf",
//						holderAccountFile.getFilename());
//			}
//		}
//		return null;
//	}

//	/**
//	 * Gets the stream content form open account.
//	 * 
//	 * @return the stream content form open account
//	 */
//	public StreamedContent getStreamContentFormOpenAccount() {
//		InputStream is = null;
//		for (HolderAccountFile holderAccountFile : holderAccountFiles) {
//			if (holderAccountFile.getDocumentType().equals(
//					HolderAccountFileType.FORM_OPEN_ACCOUNT.getCode())) {
//				is = new ByteArrayInputStream(
//						holderAccountFile.getDocumentFile());
//				return new DefaultStreamedContent(is, "application/pdf",
//						holderAccountFile.getFilename());
//			}
//		}
//		return null;
//	}

	/**
 * Delete form ban account.
 */
public void deleteFormBanAccount() {
		for (HolderAccountFile holderAccountFile : holderAccountFiles) {
			if (holderAccountFile.getDocumentType().equals(
					HolderAccountFileType.FORM_BANK_ACCOUNT.getCode())) {
				holderAccountFiles.remove(holderAccountFile);
				return;
			}
		}
	}

	/**
	 * Have form bank account.
	 * 
	 * @return the boolean
	 */
	public boolean getHaveFormBankAccount() {
		if (holderAccountFiles == null || holderAccountFiles.isEmpty()) {
			return Boolean.FALSE;
		}
		for (HolderAccountFile holderAccountFile : holderAccountFiles) {
			if (holderAccountFile.getDocumentType().equals(
					HolderAccountFileType.FORM_BANK_ACCOUNT.getCode())) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Have form know client.
	 * 
	 * @return the boolean
	 */
	public boolean getHaveFormKnowClient() {
		if (holderAccountFiles == null || holderAccountFiles.isEmpty()) {
			return Boolean.FALSE;
		}
		for (HolderAccountFile holderAccountFile : holderAccountFiles) {
			if (holderAccountFile.getDocumentType().equals(
					HolderAccountFileType.FORM_KNOW_CLIENT.getCode())) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Have form open account.
	 * 
	 * @return the boolean
	 */
	public boolean getHaveFormOpenAccount() {
		if (holderAccountFiles == null || holderAccountFiles.isEmpty()) {
			return Boolean.FALSE;
		}
		for (HolderAccountFile holderAccountFile : holderAccountFiles) {
			if (holderAccountFile.getDocumentType().equals(
					HolderAccountFileType.FORM_OPEN_ACCOUNT.getCode())) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Gets the count holder account bank availables.
	 * 
	 * @return the count holder account bank availables
	 */
	public int getCountHolderAccountBankAvailables() {
		if (Validations.validateIsNotNull(this.getHolderAccountBanks())) {
			Integer size = 0;
			for (HolderAccountBank holderAccountBank : this.getHolderAccountBanks()) {
				if (holderAccountBank.getStateAccountBank().equals(HolderAccountStateBankType.REGISTERED.getCode())) {
					size++;
				}
			}
			return size;
		}
		return 0;
	}

	/**
	 * Gets the have bic added.
	 * 
	 * @return the have bic added
	 */
	public boolean getHaveBicAdded() {
		if (Validations.validateIsNotNull(this.getHolderAccountBanks())) {
			for (HolderAccountBank holderAccountBank : this
					.getHolderAccountBanks()) {
				if (holderAccountBank.getHaveBic().equals(
						BooleanType.YES.getCode())) {
					return BooleanType.YES.getBooleanValue();
				}
			}
		}
		return BooleanType.NO.getBooleanValue();
	}

	/**
	 * Gets the holder account req det rnts.
	 * 
	 * @return the holder account req det rnts
	 */
	public String getHolderAccountDetailDescription() {
		if (Validations.validateIsNullOrEmpty(this.getHolderAccountDetails())) {
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder();
		for (HolderAccountDetail holderAccountDetail : this.getHolderAccountDetails()) {
			stringBuilder.append("* &nbsp")
					.append(holderAccountDetail.getHolder().getFullName())
					.append("<br>");
		}
		return stringBuilder.toString();
	}
	
	public String getHolderAccountDetailCUI() {
		if (Validations.validateIsNullOrEmpty(this.getHolderAccountDetails())) {
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder();
		for (HolderAccountDetail holderAccountDetail : this.getHolderAccountDetails()) {
			stringBuilder.append("* &nbsp")
					.append(holderAccountDetail.getHolder().getIdHolderPk())
					.append("<br>");
		}
		return stringBuilder.toString();
	}

	/**
	 * Gets the holder account req det rnts.
	 * 
	 * @return the holder account req det rnts
	 */
	public String getHolderAccountReqDetCUI() {
		if (Validations.validateIsNullOrEmpty(this.getHolderAccountDetails())) {
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder();
		for (HolderAccountDetail holderAccountDetail : this.getHolderAccountDetails()) {
			Holder holderTmp = holderAccountDetail.getHolder();
			stringBuilder.append(holderTmp.getIdHolderPk()).append("-")
					.append(holderTmp.getDescriptionHolder());
			if (this.getHolderAccountDetails().indexOf(holderAccountDetail) != (this
					.getHolderAccountDetails().size() - 1)) {
				stringBuilder.append(", ");
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * Validate if bank account type exist here.
	 * 
	 * @param currencyType
	 *            the currency type
	 * @return true, if successful
	 */
	public boolean validateIfBankAccountCurrencyIsAvalilableOnHolderAccount(CurrencyType currencyType) {
		if (Validations.validateIsNotNull(this.getHolderAccountBanks())) {
			for (HolderAccountBank holderAccountBank : this.getHolderAccountBanks()) {
				if (holderAccountBank.getStateAccountBank().equals(HolderAccountStateBankType.REGISTERED.getCode()) && 
						holderAccountBank.getCurrency().equals(currencyType.getCode())) {
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Validate if rnt is registered.
	 * 
	 * @return true, if successful
	 */
	public boolean validateIfRntIsRegistered() {
		if (holderAccountDetails == null || holderAccountDetails.isEmpty()) {
			return Boolean.FALSE;
		}
		for (HolderAccountDetail holderAccountDetail : holderAccountDetails) {
			if (!holderAccountDetail.getHolder().getStateHolder()
					.equals(HolderStateType.REGISTERED.getCode())) {
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}

	/**
	 * Gets the representative holder.
	 *
	 * @return the representative holder
	 */
	public Holder getRepresentativeHolder() {
		if (holderAccountDetails != null) {
			if (holderAccountDetails.size() == 1) {
				return holderAccountDetails.get(0).getHolder();
			}
			for (HolderAccountDetail detail : holderAccountDetails) {
				if (detail.getHolder() != null
						&& detail.getIndRepresentative().equals(
								BooleanType.YES.getCode())) {
					return detail.getHolder();
				}
			}
		}
		return null;
	}

	/**
	 * Gets the placement seg participa structs.
	 * 
	 * @return the placement seg participa structs
	 */
	public List<PlacementSegParticipaStruct> getPlacementSegParticipaStructs() {
		return placementSegParticipaStructs;
	}

	/**
	 * Sets the placement seg participa structs.
	 * 
	 * @param placementSegParticipaStructs
	 *            the new placement seg participa structs
	 */
	public void setPlacementSegParticipaStructs(
			List<PlacementSegParticipaStruct> placementSegParticipaStructs) {
		this.placementSegParticipaStructs = placementSegParticipaStructs;
	}

	/**
	 * Gets the placement seg partic struc hy.
	 * 
	 * @return the placement seg partic struc hy
	 */
	public List<PlacementSegParticStrucHy> getPlacementSegParticStrucHy() {
		return placementSegParticStrucHy;
	}

	/**
	 * Sets the placement seg partic struc hy.
	 * 
	 * @param placementSegParticStrucHy
	 *            the new placement seg partic struc hy
	 */
	public void setPlacementSegParticStrucHy(
			List<PlacementSegParticStrucHy> placementSegParticStrucHy) {
		this.placementSegParticStrucHy = placementSegParticStrucHy;
	}

	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public Boolean getSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HolderAccount [idHolderAccountPk="
				+ idHolderAccountPk
				// + ", accountDescription=" + accountDescription
				+ ", accountGroup="
				+ accountGroup
				+ ", accountNumber="
				+ accountNumber
				+ ", accountType="
				+ accountType
				+ ", alternateCode="
				+ alternateCode
				+ ", closingDate="
				+ closingDate
				+ ", lastModifyApp="
				+ lastModifyApp
				+ ", lastModifyDate="
				+ lastModifyDate
				+ ", lastModifyIp="
				+ lastModifyIp
				+ ", lastModifyUser="
				+ lastModifyUser
				+ ", openingDate="
				+ openingDate
				+ ", registryDate="
				+ registryDate
				+ ", registryUser="
				+ registryUser
				+ ", stateAccount="
				+ stateAccount
				+ ", stateAccountName="
				+ stateAccountName
				+ ", selected="
				+ selected
				+ ", holderAccountBanks=";
//				+ (holderAccountBanks != null ? (Hibernate
//						.isInitialized(holderAccountBanks) ? holderAccountBanks
//						: null) : null)
//				+ ", holderAccountDetails="
//				+ (holderAccountDetails != null ? (Hibernate
//						.isInitialized(holderAccountDetails) ? holderAccountDetails
//						: null)
//						: null)
//				+ ", holderAccountFiles="
//				+ (holderAccountFiles != null ? (Hibernate
//						.isInitialized(holderAccountFiles) ? holderAccountFiles
//						: null) : null)
//				+ ", Participant="
//				+ (participant != null ? Hibernate.isInitialized(participant) ? participant
//						.getIdParticipantPk() : ""
//						: "");
	}

	/**
	 * To string only fields.
	 * 
	 * @return the string
	 */
	public String toStringOnlyFields() {
		return "HolderAccount [idHolderAccountPk="
				+ idHolderAccountPk
				// + ", accountDescription=" + accountDescription
				+ ", accountGroup=" + accountGroup + ", accountNumber="
				+ accountNumber + ", accountType=" + accountType
				+ ", alternateCode=" + alternateCode + ", closingDate="
				+ closingDate + ", participant=" + participant
				+ ", lastModifyApp=" + lastModifyApp + ", lastModifyDate="
				+ lastModifyDate + ", lastModifyIp=" + lastModifyIp
				+ ", lastModifyUser=" + lastModifyUser + ", openingDate="
				+ openingDate + ", registryDate=" + registryDate
				+ ", registryUser=" + registryUser + ", stateAccount="
				+ stateAccount + ", stateAccountName=" + stateAccountName
				+ ", selected=" + selected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder
	 * .LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
			if(loggerUser.getAuditTime() != null) {
				lastModifyDate = loggerUser.getAuditTime();
			} else {
				lastModifyDate = objLoggerUser.getAuditTime();
			}
			if(loggerUser.getIpAddress() != null) {
				lastModifyIp = loggerUser.getIpAddress();
			} else {
				lastModifyIp = objLoggerUser.getIpAddress();
			}
			if(loggerUser.getUserName() != null) {
				lastModifyUser = loggerUser.getUserName();
			} else {
				lastModifyUser = objLoggerUser.getUserName();
			}
		} else {
			lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("holderAccountDetails", holderAccountDetails);
		detailsMap.put("holderAccountBanks", holderAccountBanks);
		detailsMap.put("holderAccountFiles", holderAccountFiles);
		return detailsMap;
	}

	/**
	 * Instantiates a new holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param stateAccount the state account
	 */
	public HolderAccount(Long idHolderAccountPk, Integer stateAccount) {
		this.idHolderAccountPk = idHolderAccountPk;
		this.stateAccount = stateAccount;
	}
	
	/**
	 * Instantiates a new holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param stateAccount the state account
	 * @param accountNumber the account number
	 */
	public HolderAccount(Long idHolderAccountPk, Integer stateAccount, Integer accountNumber) {
		this.idHolderAccountPk = idHolderAccountPk;
		this.stateAccount = stateAccount;
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the custody commission detail list.
	 *
	 * @return the custody commission detail list
	 */
	public List<CustodyCommissionDetail> getCustodyCommissionDetailList() {
		return custodyCommissionDetailList;
	}
	
	/**
	 * Sets the custody commission detail list.
	 *
	 * @param custodyCommissionDetailList the new custody commission detail list
	 */
	public void setCustodyCommissionDetailList(
			List<CustodyCommissionDetail> custodyCommissionDetailList) {
		this.custodyCommissionDetailList = custodyCommissionDetailList;
	}

	/**
	 * Gets the related name.
	 *
	 * @return the related name
	 */
	public String getRelatedName() {
		return relatedName;
	}

	/**
	 * Sets the related name.
	 *
	 * @param relatedName the new related name
	 */
	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the related account.
	 *
	 * @return the related account
	 */
	public Integer getRelatedAccount() {
		return relatedAccount;
	}

	/**
	 * Sets the related account.
	 *
	 * @param relatedAccount the new related account
	 */
	public void setRelatedAccount(Integer relatedAccount) {
		this.relatedAccount = relatedAccount;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the holder description list.
	 *
	 * @return the holder description list
	 */
	public List<String> getHolderDescriptionList() {
		return holderDescriptionList;
	}

	/**
	 * Sets the holder description list.
	 *
	 * @param holderDescriptionList the new holder description list
	 */
	public void setHolderDescriptionList(List<String> holderDescriptionList) {
		this.holderDescriptionList = holderDescriptionList;
	}

	/**
	 * Gets the state account description.
	 *
	 * @return the state account description
	 */
	public String getStateAccountDescription() {
		return stateAccountDescription;
	}

	/**
	 * Sets the state account description.
	 *
	 * @param stateAccountDescription the new state account description
	 */
	public void setStateAccountDescription(String stateAccountDescription) {
		this.stateAccountDescription = stateAccountDescription;
	}
	
	/**
	 * Gets the account rnt and description custody.
	 *
	 * @return the account rnt and description custody
	 */
	public String getAccountRntAndDescriptionCustody() {
		try {			
			List<Long> lstHolderPks = null;
			StringBuilder sbResult = new StringBuilder();		
			if (Validations.validateListIsNotNullAndNotEmpty(holderAccountDetails)) {				
				for (HolderAccountDetail holderAccountDetail:holderAccountDetails) {
					if(Validations.validateIsNotNullAndNotEmpty(holderAccountDetail.getHolder())){						
						if(Validations.validateIsNotNullAndNotEmpty(holderAccountDetail.getIdHolderAccountDetailPk())){
							if(lstHolderPks == null){
								lstHolderPks = new ArrayList<Long>();
								sbResult.append(Constants.ASTERISK);
								sbResult.append(holderAccountDetail.getHolder().getIdHolderPk());
								sbResult.append(Constants.DASH);
								sbResult.append(holderAccountDetail.getHolder().getDescriptionHolder());
								sbResult.append(Constants.BR);
								lstHolderPks.add(holderAccountDetail.getHolder().getIdHolderPk());
							} else if(!lstHolderPks.contains(holderAccountDetail.getHolder().getIdHolderPk())){
								sbResult.append(Constants.ASTERISK);
								sbResult.append(holderAccountDetail.getHolder().getIdHolderPk());
								sbResult.append(Constants.DASH);
								sbResult.append(holderAccountDetail.getHolder().getDescriptionHolder());
								sbResult.append(Constants.BR);
								lstHolderPks.add(holderAccountDetail.getHolder().getIdHolderPk());
							}
						}						
					}
				}
			}
			return sbResult.toString();
		} catch (LazyInitializationException e) {
			return null;
		}		
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the holder account state.
	 *
	 * @return the holder account state
	 */
	public static String getHolderAccountState() {
		return HOLDER_ACCOUNT_STATE;
	}

	public Integer getIndPdd() {
		return indPdd;
	}

	public void setIndPdd(Integer indPdd) {
		this.indPdd = indPdd;
	}

	public byte[] getDocumentPdd() {
		return documentPdd;
	}

	public void setDocumentPdd(byte[] documentPdd) {
		this.documentPdd = documentPdd;
	}

	public String getNameDocumentPdd() {
		return nameDocumentPdd;
	}

	public void setNameDocumentPdd(String nameDocumentPdd) {
		this.nameDocumentPdd = nameDocumentPdd;
	}

	public Integer getStockCode() {
		return stockCode;
	}

	public void setStockCode(Integer stockCode) {
		this.stockCode = stockCode;
	}

	
	
}