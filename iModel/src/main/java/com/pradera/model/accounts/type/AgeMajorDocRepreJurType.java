package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeMajorDocRepreJurType {
		
		RNC(Integer.valueOf(358),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(194)),
		CDRL(Integer.valueOf(359),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(194)),	
		CRMF(Integer.valueOf(360),"CERTIFICADO DEL REGISTRO MERCANTIL DEL FIDUCIARIO",Integer.valueOf(194)), 
		DEFF(Integer.valueOf(361),"DOCUMENTO DE EJERCICIO DE FUNCIONES DEL FIDUCIARIO",Integer.valueOf(194));

		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<AgeMajorDocRepreJurType> list = new ArrayList<AgeMajorDocRepreJurType>();
		public static final Map<Integer, AgeMajorDocRepreJurType> lookup = new HashMap<Integer, AgeMajorDocRepreJurType>();

		static {
			for (AgeMajorDocRepreJurType s : EnumSet.allOf(AgeMajorDocRepreJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeMajorDocRepreJurType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeMajorDocRepreJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
