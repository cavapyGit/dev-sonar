package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum ParticipantMechanismStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(826),"REGISTRADO"),
	
	/** The blocked. */
	DELETED(Integer.valueOf(827),"ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ParticipantMechanismStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<ParticipantMechanismStateType> list = new ArrayList<ParticipantMechanismStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantMechanismStateType> lookup = new HashMap<Integer, ParticipantMechanismStateType>();
	static {
		for (ParticipantMechanismStateType s : EnumSet.allOf(ParticipantMechanismStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant state type
	 */
	public static ParticipantMechanismStateType get(Integer code) {
		return lookup.get(code);
	}

}
