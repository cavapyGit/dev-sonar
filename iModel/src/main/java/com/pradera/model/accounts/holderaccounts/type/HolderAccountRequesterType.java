/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestBlockDocumentType.*;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
public enum HolderAccountRequesterType {
	
	/** The sbbv. */
	ASFI(new Integer(1217),"AUTORIDAD COMPETENTE - CNV"),
	
	/** The warrant. */
	WARRANT(new Integer(1218),"AUTORIDAD COMPETENTE - PODER JUDICIAL"),
	
	/** The participant. */
	PARTICIPANT(new Integer(1219),"DEPOSITANTE"),
	
	/** The cevaldom. */
	CAVAPY(new Integer(1220),"CAVAPY" );
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<HolderAccountRequesterType> list = new ArrayList<HolderAccountRequesterType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountRequesterType> lookup = new HashMap<Integer, HolderAccountRequesterType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountRequesterType> listSomeElements(HolderAccountRequesterType... transferSecuritiesTypeParams){
		List<HolderAccountRequesterType> retorno = new ArrayList<HolderAccountRequesterType>();
		for(HolderAccountRequesterType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountRequesterType s : EnumSet.allOf(HolderAccountRequesterType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountRequesterType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static HolderAccountRequesterType get(Integer codigo) {
		return lookup.get(codigo);
	}
}