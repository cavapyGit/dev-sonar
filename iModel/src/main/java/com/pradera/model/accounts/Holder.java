package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.corporatives.stockcalculation.validation.StockByHolder;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = Holder.HOLDER_STATE, query = "SELECT new com.pradera.model.accounts.Holder(h.idHolderPk, h.stateHolder) FROM Holder h WHERE h.idHolderPk = :idHolderPkParam")
})
@Table(name="HOLDER")
public class Holder implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant HOLDER_STATE. */
	public static final String HOLDER_STATE = "holder.State";

	/** The id holder pk. */
	@Id
//	@SequenceGenerator(name="HOLDER_IDHOLDERPK_GENERATOR", sequenceName="SQ_ID_HOLDER_PK",initialValue=1,allocationSize=1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_IDHOLDERPK_GENERATOR")
	@Column(name="ID_HOLDER_PK")
	@NotNull(groups={StockByHolder.class})
	private Long idHolderPk;

    /** The birth date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="BIRTH_DATE")
	private Date birthDate;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private Integer economicActivity;
	
	@Column(name="INVESTOR_TYPE")
    private Integer investorType;

	/** The economic sector. */
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;

	/** The email. */
	private String email;

	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;

	/** The first last name. */
	@Column(name="FIRST_LAST_NAME")
	private String firstLastName;

	/** The full name. */
	@Column(name="FULL_NAME")
	private String fullName;

	/** The holder type. */
	@Column(name="HOLDER_TYPE")
	private Integer holderType;
	
	/** The juridic class. */
	@Column(name="JURIDIC_CLASS")
	private Integer juridicClass;

	/** The ind disabled. */
	@Column(name="IND_DISABLED")
	private Integer indDisabled;

	/** The ind residence. */
	@Column(name="IND_RESIDENCE")
	private Integer indResidence;

	/** The ind minor. */
	@Column(name="IND_MINOR")
	private Integer indMinor;
	
	/** The ind FATCA. */
	@Column(name="IND_FATCA")
	private Integer indFatca;
	
	/** The ind FATCA. */
	@Column(name="FILE_PHOTO_NAME")
	private String filePhotoName;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The legal address. */
	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;

	/** The legal department. */
	@Column(name="LEGAL_DEPARTMENT")
	private Integer legalDepartment;

	/** The legal district. */
	@Column(name="LEGAL_DISTRICT")
	private Integer legalDistrict;

	/** The legal province. */
	@Column(name="LEGAL_PROVINCE")
	private Integer legalProvince;

	/** The legal residence country. */
	@Column(name="LEGAL_RESIDENCE_COUNTRY")
	private Integer legalResidenceCountry;

	/** The mobile number. */
	@Column(name="MOBILE_NUMBER")
	private String mobileNumber;

	/** The name. */
	@Column(name="Name")
	private String name;

	/** The nationality. */
	@Column(name="Nationality")
	private Integer nationality;

	/** The home phone number. */
	@Column(name="HOME_PHONE_NUMBER")
	private String homePhoneNumber;
	
	/** The office phone number. */
	@Column(name="OFFICE_PHONE_NUMBER")
	private String officePhoneNumber;

	/** The postal address. */
	@Column(name="POSTAL_ADDRESS")
	private String postalAddress;

	/** The postal department. */
	@Column(name="POSTAL_DEPARTMENT")
	private Integer postalDepartment;

	/** The postal district. */
	@Column(name="POSTAL_DISTRICT")
	private Integer postalDistrict;

	/** The postal province. */
	@Column(name="POSTAL_PROVINCE")
	private Integer postalProvince;

	/** The postal residence country. */
	@Column(name="POSTAL_RESIDENCE_COUNTRY")
	private Integer postalResidenceCountry;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The second document number. */
	@Column(name="SECOND_DOCUMENT_NUMBER")
	private String secondDocumentNumber;

	/** The second document type. */
	@Column(name="SECOND_DOCUMENT_TYPE")
	private Integer secondDocumentType;

	/** The second last name. */
	@Column(name="SECOND_LAST_NAME")
	private String secondLastName;

	/** The second nationality. */
	@Column(name="SECOND_NATIONALITY")
	private Integer secondNationality;

	/** The sex. */
	private Integer sex;

	/** The state holder. */
	@Column(name="STATE_HOLDER")
	private Integer stateHolder;

	/** The participant. */
	//bi-directional many-to-one association to Participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;
    
    /** The document source. */
    @Column(name="DOCUMENT_SOURCE")
	private Integer documentSource;

    /** The document issuance date. */
    @Column(name="DOCUMENT_ISSUANCE_DATE")
    private Integer documentIssuanceDate;
   
    /** The related cui. */
    @Column(name="RELATED_CUI")
    private Long relatedCui;
    
    /** The fund administrator. */
    @Column(name="FUND_ADMINISTRATOR")
    private String fundAdministrator;
    
    /** The fund type. */
    @Column(name="FUND_TYPE")
    private String fundType;
    
    /** The transfer number. */
    @Column(name="TRANSFER_NUMBER")
    private String transferNumber;
    
    /** The mnemonic. */
    @Column(name="MNEMONIC")
	private String mnemonic;
    
    /** The married last name. */
    @Column(name="MARRIED_LAST_NAME")
    private String marriedLastName;
    
    /** The civil status. */
    @Column(name="CIVIL_STATUS")
    private Integer civilStatus;
    
    /** The main activity. */
    @Column(name="MAIN_ACTIVITY")
    private String mainActivity;
    
    /** The job source business name. */
    @Column(name="JOB_SOURCE_BUSINESS_NAME")
    private String jobSourceBusinessName;
    
//    @Column(name="JOB_SOURCE_ECONOMIC_ACTIVITY")
//    private String jobSourceEconomicActivity;
/** The job date admission. */
//    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="JOB_DATE_ADMISSION")
    private Date jobDateAdmission;
	
    /** The appointment. */
    @Column(name="APPOINTMENT")
    private String appointment;
	
    /** The job address. */
    @Column(name="JOB_ADDRESS")
    private String jobAddress;
    
//    @Column(name="OFFICE_EMAIL")
//    private String officeEmail;
/** The total income. */
//    
    @Column(name="TOTAL_INCOME")
    private Integer totalIncome;
    
//    @Column(name="OTHER_INCOME")
//    private BigDecimal otherIncome;
/** The per ref full name. */
//  
    @Column(name="PER_REF_FULL_NAME")
    private String perRefFullName;
    
    /** The per ref land line. */
    @Column(name="PER_REF_LANDLINE")
    private String perRefLandLine;
    
    /** The per ref cell phone. */
    @Column(name="PER_REF_CELLPHONE")
    private String perRefCellPhone;
    
    /** The comer ref business name. */
    @Column(name="COMER_REF_BUSINESS_NAME")
    private String comerRefBusinessName;
    
    /** The comer ref land line. */
    @Column(name="COMER_REF_LANDLINE")
    private String comerRefLandLine;

//    @Column(name="IND_ENTITY_PUBLIC_APPOINTMENT")
//    private Integer indEntityPublicAppointment;

//    @Column(name="ENTITY_PUBLIC")
//    private String entityPublic;
    
    /** The entity public appointment. */
@Column(name="ENTITY_PUBLIC_APPOINTMENT")
    private String entityPublicAppointment;
    
//    @Temporal(TemporalType.TIMESTAMP)
//	@Column(name="ENTITY_PUBLIC_DATE_INITIAL")
//    private Date entityPublicDateInitial;
    
//    @Temporal(TemporalType.TIMESTAMP)
//	@Column(name="ENTITY_PUBLIC_DATE_END")
//    private Date entityPublicDateEnd;

    /** The nit natural person. */
@Column(name="NIT_NATURAL_PERSON")
    private Integer nitNaturalPerson;
    
    /** The website. */
    @Column(name="WEBSITE")
	private String website;
    
    /** The fund company enrollment. */
    @Column(name="FUNDCOMPANY_ENROLLMENT")
    private String fundCompanyEnrollment;
    
    /** The constitution date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="CONSTITUTION_DATE")
    private Date constitutionDate;
    
    /** The file photo. */
    @Lob()
   	@Column(name="FILE_PHOTO")
   	private byte[] filePhoto;
    
    /** The file signing. */
    @Lob()
   	@Column(name="FILE_SIGNING")
   	private byte[] fileSigning;
    
	/** The ind can Negotiate. */
	@Column(name="IND_CAN_NEGOTIATE")
	private Integer indCanNegotiate;
	
	/** The ind sirtex neg. */
	@Column(name="IND_SIRTEX_NEG")
	private Integer indSirtexNeg;

    /** The issuer. */
    @OneToMany(mappedBy="holder",fetch=FetchType.LAZY)
	private List<Issuer> issuer;

	/** The holder account detail. */
	//bi-directional many-to-one association to HolderAccountDetail
	@OneToMany(mappedBy="holder")
	private List<com.pradera.model.accounts.holderaccounts.HolderAccountDetail> holderAccountDetail;

	/** The holder history states. */
	//bi-directional many-to-one association to HolderHistoryState
	@OneToMany(mappedBy="holder",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<HolderHistoryState> holderHistoryStates;

	/** The holder requests. */
	//bi-directional many-to-one association to HolderRequest
	@OneToMany(mappedBy="holder",fetch=FetchType.LAZY)
	private List<HolderRequest> holderRequests;
	
	/** The pep person. */
	@OneToMany(mappedBy="holder",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<PepPerson> pepPerson;
	
	/** The participants. */
	@OneToMany(mappedBy="holder")
	private List<Participant> participants;

	/** The bank account. */
	@OneToMany(mappedBy="holder")
	private List<HolderAccountBank> bankAccount;
	
/** The holder account req det hy. */
//	bi-directional many-to-one association to HolderAccountDetail
	@OneToMany(mappedBy="holder")
	private List<HolderAccountDetRequest> holderAccountReqDetHy;
	
	/** The represented entity. */
	@OneToMany(mappedBy="holder",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<RepresentedEntity> representedEntity;
	
	/** The holder file. */
	@OneToMany(mappedBy="holder",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<HolderFile> holderFile;
	
	/** The block entity. */
	@OneToMany(mappedBy="holder",fetch=FetchType.LAZY)
	private List<BlockEntity> blockEntity;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idHolderFk", fetch = FetchType.LAZY)
    private List<HolderAnnulation> holderAnnulationList;
	
	/** The description holder. */
	@Transient
	private String descriptionHolder;
	
	/** The state holder aux. */
	@Transient
	private Integer stateHolderAux;
	
	/** The description type holder. */
	@Transient
	private String descriptionTypeHolder;
	
	/** The description document source. */
	@Transient
	private String descriptionDocumentSource;
	
	/** The description type document. */
	@Transient
	private String descriptionTypeDocument;
	
	/** The state description. */
	@Transient
	private String stateDescription;
	
	/** The indicator disabled. */
	@Transient
	private boolean indicatorDisabled;
	
	/** The indicator minor. */
	@Transient
	private boolean indicatorMinor;
	
	/** The display fullname cui. */
	@Transient
	private String displayFullnameCui;
	
	/** The initial date. */
	@Transient
	private Date initialDate;
	
	/** The final date. */
	@Transient
	private Date finalDate;

	/** The list sector activity. */
	@Transient
	private List<Integer> listSectorActivity;
	
//	@Transient
// 	private boolean indicatorEntityPublicAppointment;
	
	/**
	 * Instantiates a new holder.
	 */
	public Holder() {
    }

	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return this.idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the birth date.
	 *
	 * @return the birth date
	 */
	public Date getBirthDate() {
		return this.birthDate;
	}

	/**
	 * Sets the birth date.
	 *
	 * @param birthDate the new birth date
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return this.documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}
	
	/**
	 * Gets the document type description.
	 *
	 * @return the document type description
	 */
	public String getDocumentTypeDescription() {
		if(Validations.validateIsNotNullAndNotEmpty(documentType))
			return DocumentType.lookup.get(this.getDocumentType()).getValue();
		return null;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public Integer getEconomicActivity() {
		return this.economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the new economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return this.economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return this.faxNumber;
	}

	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the new fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Gets the first last name.
	 *
	 * @return the first last name
	 */
	public String getFirstLastName() {
		return this.firstLastName;
	}

	/**
	 * Sets the first last name.
	 *
	 * @param firstLastName the new first last name
	 */
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return this.fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Gets the holder type.
	 *
	 * @return the holder type
	 */
	public Integer getHolderType() {
		return this.holderType;
	}
	
	/**
	 * Gets the holder type description.
	 *
	 * @return the holder type description
	 */
	public String getHolderTypeDescription() {
		return PersonType.lookup.get(this.getHolderType()).getValue();
	}

	/**
	 * Sets the holder type.
	 *
	 * @param holderType the new holder type
	 */
	public void setHolderType(Integer holderType) {
		this.holderType = holderType;
	}

	/**
	 * Gets the ind disabled.
	 *
	 * @return the ind disabled
	 */
	public Integer getIndDisabled() {
		return this.indDisabled;
	}

	/**
	 * Sets the ind disabled.
	 *
	 * @param indDisabled the new ind disabled
	 */
	public void setIndDisabled(Integer indDisabled) {
		this.indDisabled = indDisabled;
	}

	/**
	 * Gets the ind residence.
	 *
	 * @return the ind residence
	 */
	public Integer getIndResidence() {
		return this.indResidence;
	}

	/**
	 * Sets the ind residence.
	 *
	 * @param indResidence the new ind residence
	 */
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the legal address.
	 *
	 * @return the legal address
	 */
	public String getLegalAddress() {
		return this.legalAddress;
	}

	/**
	 * Sets the legal address.
	 *
	 * @param legalAddress the new legal address
	 */
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	/**
	 * Gets the legal department.
	 *
	 * @return the legal department
	 */
	public Integer getLegalDepartment() {
		return this.legalDepartment;
	}

	/**
	 * Sets the legal department.
	 *
	 * @param legalDepartment the new legal department
	 */
	public void setLegalDepartment(Integer legalDepartment) {
		this.legalDepartment = legalDepartment;
	}

	/**
	 * Gets the legal district.
	 *
	 * @return the legal district
	 */
	public Integer getLegalDistrict() {
		return this.legalDistrict;
	}

	/**
	 * Sets the legal district.
	 *
	 * @param legalDistrict the new legal district
	 */
	public void setLegalDistrict(Integer legalDistrict) {
		this.legalDistrict = legalDistrict;
	}

	/**
	 * Gets the legal province.
	 *
	 * @return the legal province
	 */
	public Integer getLegalProvince() {
		return this.legalProvince;
	}

	/**
	 * Sets the legal province.
	 *
	 * @param legalProvince the new legal province
	 */
	public void setLegalProvince(Integer legalProvince) {
		this.legalProvince = legalProvince;
	}

	/**
	 * Gets the legal residence country.
	 *
	 * @return the legal residence country
	 */
	public Integer getLegalResidenceCountry() {
		return this.legalResidenceCountry;
	}

	/**
	 * Sets the legal residence country.
	 *
	 * @param legalResidenceCountry the new legal residence country
	 */
	public void setLegalResidenceCountry(Integer legalResidenceCountry) {
		this.legalResidenceCountry = legalResidenceCountry;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return this.mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the nationality.
	 *
	 * @return the nationality
	 */
	public Integer getNationality() {
		return this.nationality;
	}

	/**
	 * Sets the nationality.
	 *
	 * @param nationality the new nationality
	 */
	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	

	/**
	 * Gets the home phone number.
	 *
	 * @return the home phone number
	 */
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	/**
	 * Sets the home phone number.
	 *
	 * @param homePhoneNumber the new home phone number
	 */
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	/**
	 * Gets the postal address.
	 *
	 * @return the postal address
	 */
	public String getPostalAddress() {
		return this.postalAddress;
	}

	/**
	 * Sets the postal address.
	 *
	 * @param postalAddress the new postal address
	 */
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	/**
	 * Gets the postal department.
	 *
	 * @return the postal department
	 */
	public Integer getPostalDepartment() {
		return this.postalDepartment;
	}

	/**
	 * Sets the postal department.
	 *
	 * @param postalDepartment the new postal department
	 */
	public void setPostalDepartment(Integer postalDepartment) {
		this.postalDepartment = postalDepartment;
	}

	/**
	 * Gets the postal district.
	 *
	 * @return the postal district
	 */
	public Integer getPostalDistrict() {
		return this.postalDistrict;
	}

	/**
	 * Sets the postal district.
	 *
	 * @param postalDistrict the new postal district
	 */
	public void setPostalDistrict(Integer postalDistrict) {
		this.postalDistrict = postalDistrict;
	}

	/**
	 * Gets the postal province.
	 *
	 * @return the postal province
	 */
	public Integer getPostalProvince() {
		return this.postalProvince;
	}

	/**
	 * Sets the postal province.
	 *
	 * @param postalProvince the new postal province
	 */
	public void setPostalProvince(Integer postalProvince) {
		this.postalProvince = postalProvince;
	}

	/**
	 * Gets the postal residence country.
	 *
	 * @return the postal residence country
	 */
	public Integer getPostalResidenceCountry() {
		return this.postalResidenceCountry;
	}

	/**
	 * Sets the postal residence country.
	 *
	 * @param postalResidenceCountry the new postal residence country
	 */
	public void setPostalResidenceCountry(Integer postalResidenceCountry) {
		this.postalResidenceCountry = postalResidenceCountry;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the second document number.
	 *
	 * @return the second document number
	 */
	public String getSecondDocumentNumber() {
		return this.secondDocumentNumber;
	}

	/**
	 * Sets the second document number.
	 *
	 * @param secondDocumentNumber the new second document number
	 */
	public void setSecondDocumentNumber(String secondDocumentNumber) {
		this.secondDocumentNumber = secondDocumentNumber;
	}

	/**
	 * Gets the second document type.
	 *
	 * @return the second document type
	 */
	public Integer getSecondDocumentType() {
		return this.secondDocumentType;
	}

	/**
	 * Sets the second document type.
	 *
	 * @param secondDocumentType the new second document type
	 */
	public void setSecondDocumentType(Integer secondDocumentType) {
		this.secondDocumentType = secondDocumentType;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return this.secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
	 * Gets the second nationality.
	 *
	 * @return the second nationality
	 */
	public Integer getSecondNationality() {
		return this.secondNationality;
	}

	/**
	 * Sets the second nationality.
	 *
	 * @param secondNationality the new second nationality
	 */
	public void setSecondNationality(Integer secondNationality) {
		this.secondNationality = secondNationality;
	}

	/**
	 * Gets the sex.
	 *
	 * @return the sex
	 */
	public Integer getSex() {
		return this.sex;
	}

	/**
	 * Sets the sex.
	 *
	 * @param sex the new sex
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * Gets the state holder.
	 *
	 * @return the state holder
	 */
	public Integer getStateHolder() {
		return this.stateHolder;
	}

	/**
	 * Sets the state holder.
	 *
	 * @param stateHolder the new state holder
	 */
	public void setStateHolder(Integer stateHolder) {
		this.stateHolder = stateHolder;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return this.participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the holder account detail.
	 *
	 * @return the holder account detail
	 */
	public List<com.pradera.model.accounts.holderaccounts.HolderAccountDetail> getHolderAccountDetail() {
		return this.holderAccountDetail;
	}

	/**
	 * Sets the holder account detail.
	 *
	 * @param holderAccountDetails the new holder account detail
	 */
	public void setHolderAccountDetail(List<com.pradera.model.accounts.holderaccounts.HolderAccountDetail> holderAccountDetails) {
		this.holderAccountDetail = holderAccountDetails;
	}
	
	/**
	 * Gets the holder history states.
	 *
	 * @return the holder history states
	 */
	public List<HolderHistoryState> getHolderHistoryStates() {
		return this.holderHistoryStates;
	}

	/**
	 * Sets the holder history states.
	 *
	 * @param holderHistoryStates the new holder history states
	 */
	public void setHolderHistoryStates(List<HolderHistoryState> holderHistoryStates) {
		this.holderHistoryStates = holderHistoryStates;
	}
	
	/**
	 * Gets the holder requests.
	 *
	 * @return the holder requests
	 */
	public List<HolderRequest> getHolderRequests() {
		return this.holderRequests;
	}

	/**
	 * Sets the holder requests.
	 *
	 * @param holderRequests the new holder requests
	 */
	public void setHolderRequests(List<HolderRequest> holderRequests) {
		this.holderRequests = holderRequests;
	}

	/**
	 * Gets the juridic class.
	 *
	 * @return the juridic class
	 */
	public Integer getJuridicClass() {
		return juridicClass;
	}

	/**
	 * Sets the juridic class.
	 *
	 * @param juridicClass the new juridic class
	 */
	public void setJuridicClass(Integer juridicClass) {
		this.juridicClass = juridicClass;
	}

	/**
	 * Gets the pep person.
	 *
	 * @return the pep person
	 */
	public List<PepPerson> getPepPerson() {
		return pepPerson;
	}

	/**
	 * Sets the pep person.
	 *
	 * @param pepPerson the new pep person
	 */
	public void setPepPerson(List<PepPerson> pepPerson) {
		this.pepPerson = pepPerson;
	}

	/**
	 * Gets the office phone number.
	 *
	 * @return the office phone number
	 */
	public String getOfficePhoneNumber() {
		return officePhoneNumber;
	}

	/**
	 * Sets the office phone number.
	 *
	 * @param officePhoneNumber the new office phone number
	 */
	public void setOfficePhoneNumber(String officePhoneNumber) {
		this.officePhoneNumber = officePhoneNumber;
	}

	/**
	 * Gets the ind minor.
	 *
	 * @return the ind minor
	 */
	public Integer getIndMinor() {
		return indMinor;
	}

	/**
	 * Sets the ind minor.
	 *
	 * @param indMinor the new ind minor
	 */
	public void setIndMinor(Integer indMinor) {
		this.indMinor = indMinor;
	}

	/**
	 * Gets the participants.
	 *
	 * @return the participants
	 */
	public List<Participant> getParticipants() {
		return participants;
	}

	/**
	 * Sets the participants.
	 *
	 * @param participants the new participants
	 */
	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}
	
	/**
	 * Gets the document source.
	 *
	 * @return the document source
	 */
	public Integer getDocumentSource() {
		return documentSource;
	}


	/**
	 * Sets the document source.
	 *
	 * @param documentSource the new document source
	 */
	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}


	/**
	 * Gets the holder account req det hy.
	 *
	 * @return the holder account req det hy
	 */
	public List<HolderAccountDetRequest> getHolderAccountReqDetHy() {
		return holderAccountReqDetHy;
	}

	/**
	 * Sets the holder account req det hy.
	 *
	 * @param holderAccountReqDetHy the new holder account req det hy
	 */
	public void setHolderAccountReqDetHy(
			List<HolderAccountDetRequest> holderAccountReqDetHy) {
		this.holderAccountReqDetHy = holderAccountReqDetHy;
	}

	/**
	 * Gets the holder file.
	 *
	 * @return the holder file
	 */
	public List<HolderFile> getHolderFile() {
		return holderFile;
	}

	/**
	 * Sets the holder file.
	 *
	 * @param holderFile the new holder file
	 */
	public void setHolderFile(List<HolderFile> holderFile) {
		this.holderFile = holderFile;
	}
	
	/**
	 * Gets the represented entity.
	 *
	 * @return the represented entity
	 */
	public List<RepresentedEntity> getRepresentedEntity() {
		return representedEntity;
	}

	/**
	 * Sets the represented entity.
	 *
	 * @param representedEntity the new represented entity
	 */
	public void setRepresentedEntity(List<RepresentedEntity> representedEntity) {
		this.representedEntity = representedEntity;
	}

	/**
	 * Gets the description holder.
	 *
	 * @return the description holder
	 */
	public String getDescriptionHolder() {
		if(Validations.validateIsNull(this.getHolderType())){
			if(Validations.validateIsNull(descriptionHolder)){
				return null;
			}else{
				return descriptionHolder;
			}
		}
		if(holderType.equals(PersonType.NATURAL.getCode())){   
			
			if(secondLastName != null){
				descriptionHolder = firstLastName+" "+secondLastName+","+name;
			}
			else{
				descriptionHolder = firstLastName+","+name;		
			}
		 }
	
		else if(holderType.equals(PersonType.JURIDIC.getCode())){
			 descriptionHolder = fullName;	
			}
			
			return descriptionHolder;
	}

	/**
	 * Sets the description holder.
	 *
	 * @param descriptionHolder the new description holder
	 */
	public void setDescriptionHolder(String descriptionHolder) {
		this.descriptionHolder = descriptionHolder;
	}

	/**
	 * Gets the state holder aux.
	 *
	 * @return the state holder aux
	 */
	public Integer getStateHolderAux() {
		return stateHolderAux;
	}

	/**
	 * Sets the state holder aux.
	 *
	 * @param stateHolderAux the new state holder aux
	 */
	public void setStateHolderAux(Integer stateHolderAux) {
		this.stateHolderAux = stateHolderAux;
	}

	/**
	 * Gets the description type holder.
	 *
	 * @return the description type holder
	 */
	public String getDescriptionTypeHolder() {
		return descriptionTypeHolder = PersonType.get(holderType).getValue();
	}

	/**
	 * Sets the description type holder.
	 *
	 * @param descriptionTypeHolder the new description type holder
	 */
	public void setDescriptionTypeHolder(String descriptionTypeHolder) {
		this.descriptionTypeHolder = descriptionTypeHolder;
	}

	/**
	 * Gets the description type document.
	 *
	 * @return the description type document
	 */
	public String getDescriptionTypeDocument() {
		return descriptionTypeDocument;
	}

	/**
	 * Sets the description type document.
	 *
	 * @param descriptionTypeDocument the new description type document
	 */
	public void setDescriptionTypeDocument(String descriptionTypeDocument) {
		this.descriptionTypeDocument = descriptionTypeDocument;
	}

	/**
	 * Gets the description document source.
	 *
	 * @return the description document source
	 */
	public String getDescriptionDocumentSource() {
		return descriptionDocumentSource;
	}


	/**
	 * Sets the description document source.
	 *
	 * @param descriptionDocumentSource the new description document source
	 */
	public void setDescriptionDocumentSource(String descriptionDocumentSource) {
		this.descriptionDocumentSource = descriptionDocumentSource;
	}


	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;			
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Checks if is indicator disabled.
	 *
	 * @return true, if is indicator disabled
	 */
	public boolean isIndicatorDisabled() {
		return indicatorDisabled;
	}

	/**
	 * Sets the indicator disabled.
	 *
	 * @param indicatorDisabled the new indicator disabled
	 */
	public void setIndicatorDisabled(boolean indicatorDisabled) {
		this.indicatorDisabled = indicatorDisabled;
	}

	/**
	 * Checks if is indicator minor.
	 *
	 * @return true, if is indicator minor
	 */
	public boolean isIndicatorMinor() {
		return indicatorMinor;
	}

	/**
	 * Sets the indicator minor.
	 *
	 * @param indicatorMinor the new indicator minor
	 */
	public void setIndicatorMinor(boolean indicatorMinor) {
		this.indicatorMinor = indicatorMinor;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null){
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
	        if(loggerUser.getAuditTime() != null) {
	        	lastModifyDate = loggerUser.getAuditTime();
	        } else {
	        	lastModifyDate = objLoggerUser.getAuditTime();
	        }
	        if(loggerUser.getIpAddress() != null) {
	        	lastModifyIp = loggerUser.getIpAddress();
	        } else {
	        	lastModifyIp = objLoggerUser.getIpAddress();
	        }
	        if(loggerUser.getUserName() != null){
	        	lastModifyUser = loggerUser.getUserName();
	        } else {
	        	lastModifyUser = objLoggerUser.getUserName();
	        }
	    } else {
	    	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
	    }
    }
	
	/**
	 * Gets the bank account.
	 *
	 * @return the bank account
	 */
	public List<HolderAccountBank> getBankAccount() {
		return bankAccount;
	}
	
	/**
	 * Sets the bank account.
	 *
	 * @param bankAccount the new bank account
	 */
	public void setBankAccount(List<HolderAccountBank> bankAccount) {
		this.bankAccount = bankAccount;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("holderFile",holderFile);
        detailsMap.put("pepPerson", pepPerson);
        detailsMap.put("representedEntity", representedEntity);
        return detailsMap;
	}


	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public List<Issuer> getIssuer() {
		return issuer;
	}


	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(List<Issuer> issuer) {
		this.issuer = issuer;
	}


	/**
	 * Gets the block entity.
	 *
	 * @return the block entity
	 */
	public List<BlockEntity> getBlockEntity() {
		return blockEntity;
	}


	/**
	 * Sets the block entity.
	 *
	 * @param blockEntity the new block entity
	 */
	public void setBlockEntity(List<BlockEntity> blockEntity) {
		this.blockEntity = blockEntity;
	}
	
	/**
	 * Instantiates a new holder.
	 *
	 * @param idHolderPk the id holder pk
	 * @param stateHolder the state holder
	 */
	public Holder(Long idHolderPk, Integer stateHolder) {
		this.idHolderPk = idHolderPk;
		this.stateHolder = stateHolder;
	}


	/**
	 * Instantiates a new holder.
	 *
	 * @param idHolderPk the id holder pk
	 */
	public Holder(Long idHolderPk) {
		super();
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Sets the display fullname cui.
	 *
	 * @param displayFullnameCui the new display fullname cui
	 */
	public void setDisplayFullnameCui(String displayFullnameCui) {
		this.displayFullnameCui = displayFullnameCui;
	}


	/**
	 * Gets the document issuance date.
	 *
	 * @return the document issuance date
	 */
	public Integer getDocumentIssuanceDate() {
		return documentIssuanceDate;
	}


	/**
	 * Sets the document issuance date.
	 *
	 * @param documentIssuanceDate the new document issuance date
	 */
	public void setDocumentIssuanceDate(Integer documentIssuanceDate) {
		this.documentIssuanceDate = documentIssuanceDate;
	}


	/**
	 * Gets the related cui.
	 *
	 * @return the related cui
	 */
	public Long getRelatedCui() {
		return relatedCui;
	}


	/**
	 * Sets the related cui.
	 *
	 * @param relatedCui the new related cui
	 */
	public void setRelatedCui(Long relatedCui) {
		this.relatedCui = relatedCui;
	}


	/**
	 * Gets the fund administrator.
	 *
	 * @return the fund administrator
	 */
	public String getFundAdministrator() {
		return fundAdministrator;
	}


	/**
	 * Sets the fund administrator.
	 *
	 * @param fundAdministrator the new fund administrator
	 */
	public void setFundAdministrator(String fundAdministrator) {
		this.fundAdministrator = fundAdministrator;
	}


	/**
	 * Gets the fund type.
	 *
	 * @return the fund type
	 */
	public String getFundType() {
		return fundType;
	}


	/**
	 * Sets the fund type.
	 *
	 * @param fundType the new fund type
	 */
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}


	/**
	 * Gets the transfer number.
	 *
	 * @return the transfer number
	 */
	public String getTransferNumber() {
		return transferNumber;
	}


	/**
	 * Sets the transfer number.
	 *
	 * @param transferNumber the new transfer number
	 */
	public void setTransferNumber(String transferNumber) {
		this.transferNumber = transferNumber;
	}


	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}


	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}


	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}


	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}


	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}


	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}


	/**
	 * Gets the list sector activity.
	 *
	 * @return the list sector activity
	 */
	public List<Integer> getListSectorActivity() {
		return listSectorActivity;
	}


	/**
	 * Sets the list sector activity.
	 *
	 * @param listSectorActivity the new list sector activity
	 */
	public void setListSectorActivity(List<Integer> listSectorActivity) {
		this.listSectorActivity = listSectorActivity;
	}


	/**
	 * Gets the married last name.
	 *
	 * @return the married last name
	 */
	public String getMarriedLastName() {
		return marriedLastName;
	}


	/**
	 * Sets the married last name.
	 *
	 * @param marriedLastName the new married last name
	 */
	public void setMarriedLastName(String marriedLastName) {
		this.marriedLastName = marriedLastName;
	}


	/**
	 * Gets the civil status.
	 *
	 * @return the civil status
	 */
	public Integer getCivilStatus() {
		return civilStatus;
	}


	/**
	 * Sets the civil status.
	 *
	 * @param civilStatus the new civil status
	 */
	public void setCivilStatus(Integer civilStatus) {
		this.civilStatus = civilStatus;
	}


	/**
	 * Gets the main activity.
	 *
	 * @return the main activity
	 */
	public String getMainActivity() {
		return mainActivity;
	}


	/**
	 * Sets the main activity.
	 *
	 * @param mainActivity the new main activity
	 */
	public void setMainActivity(String mainActivity) {
		this.mainActivity = mainActivity;
	}


	/**
	 * Gets the job source business name.
	 *
	 * @return the job source business name
	 */
	public String getJobSourceBusinessName() {
		return jobSourceBusinessName;
	}


	/**
	 * Sets the job source business name.
	 *
	 * @param jobSourceBusinessName the new job source business name
	 */
	public void setJobSourceBusinessName(String jobSourceBusinessName) {
		this.jobSourceBusinessName = jobSourceBusinessName;
	}


	/**
	 * Gets the job date admission.
	 *
	 * @return the job date admission
	 */
	public Date getJobDateAdmission() {
		return jobDateAdmission;
	}


	/**
	 * Sets the job date admission.
	 *
	 * @param jobDateAdmission the new job date admission
	 */
	public void setJobDateAdmission(Date jobDateAdmission) {
		this.jobDateAdmission = jobDateAdmission;
	}


	/**
	 * Gets the appointment.
	 *
	 * @return the appointment
	 */
	public String getAppointment() {
		return appointment;
	}


	/**
	 * Sets the appointment.
	 *
	 * @param appointment the new appointment
	 */
	public void setAppointment(String appointment) {
		this.appointment = appointment;
	}


	/**
	 * Gets the job address.
	 *
	 * @return the job address
	 */
	public String getJobAddress() {
		return jobAddress;
	}


	/**
	 * Sets the job address.
	 *
	 * @param jobAddress the new job address
	 */
	public void setJobAddress(String jobAddress) {
		this.jobAddress = jobAddress;
	}


	/**
	 * Gets the total income.
	 *
	 * @return the total income
	 */
	public Integer getTotalIncome() {
		return totalIncome;
	}


	/**
	 * Sets the total income.
	 *
	 * @param totalIncome the new total income
	 */
	public void setTotalIncome(Integer totalIncome) {
		this.totalIncome = totalIncome;
	}


	/**
	 * Gets the per ref full name.
	 *
	 * @return the per ref full name
	 */
	public String getPerRefFullName() {
		return perRefFullName;
	}


	/**
	 * Sets the per ref full name.
	 *
	 * @param perRefFullName the new per ref full name
	 */
	public void setPerRefFullName(String perRefFullName) {
		this.perRefFullName = perRefFullName;
	}


	/**
	 * Gets the per ref land line.
	 *
	 * @return the per ref land line
	 */
	public String getPerRefLandLine() {
		return perRefLandLine;
	}


	/**
	 * Sets the per ref land line.
	 *
	 * @param perRefLandLine the new per ref land line
	 */
	public void setPerRefLandLine(String perRefLandLine) {
		this.perRefLandLine = perRefLandLine;
	}


	/**
	 * Gets the per ref cell phone.
	 *
	 * @return the per ref cell phone
	 */
	public String getPerRefCellPhone() {
		return perRefCellPhone;
	}


	/**
	 * Sets the per ref cell phone.
	 *
	 * @param perRefCellPhone the new per ref cell phone
	 */
	public void setPerRefCellPhone(String perRefCellPhone) {
		this.perRefCellPhone = perRefCellPhone;
	}


	/**
	 * Gets the comer ref business name.
	 *
	 * @return the comer ref business name
	 */
	public String getComerRefBusinessName() {
		return comerRefBusinessName;
	}


	/**
	 * Sets the comer ref business name.
	 *
	 * @param comerRefBusinessName the new comer ref business name
	 */
	public void setComerRefBusinessName(String comerRefBusinessName) {
		this.comerRefBusinessName = comerRefBusinessName;
	}


	/**
	 * Gets the comer ref land line.
	 *
	 * @return the comer ref land line
	 */
	public String getComerRefLandLine() {
		return comerRefLandLine;
	}


	/**
	 * Sets the comer ref land line.
	 *
	 * @param comerRefLandLine the new comer ref land line
	 */
	public void setComerRefLandLine(String comerRefLandLine) {
		this.comerRefLandLine = comerRefLandLine;
	}


	/**
	 * Gets the entity public appointment.
	 *
	 * @return the entity public appointment
	 */
	public String getEntityPublicAppointment() {
		return entityPublicAppointment;
	}


	/**
	 * Sets the entity public appointment.
	 *
	 * @param entityPublicAppointment the new entity public appointment
	 */
	public void setEntityPublicAppointment(String entityPublicAppointment) {
		this.entityPublicAppointment = entityPublicAppointment;
	}


	/**
	 * Gets the display fullname cui.
	 *
	 * @return the display fullname cui
	 */
	public String getDisplayFullnameCui() {
		return displayFullnameCui;
	}


	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}


	/**
	 * Sets the website.
	 *
	 * @param website the new website
	 */
	public void setWebsite(String website) {
		this.website = website;
	}


	/**
	 * Gets the fund company enrollment.
	 *
	 * @return the fund company enrollment
	 */
	public String getFundCompanyEnrollment() {
		return fundCompanyEnrollment;
	}


	/**
	 * Sets the fund company enrollment.
	 *
	 * @param fundCompanyEnrollment the new fund company enrollment
	 */
	public void setFundCompanyEnrollment(String fundCompanyEnrollment) {
		this.fundCompanyEnrollment = fundCompanyEnrollment;
	}


	/**
	 * Gets the constitution date.
	 *
	 * @return the constitution date
	 */
	public Date getConstitutionDate() {
		return constitutionDate;
	}


	/**
	 * Sets the constitution date.
	 *
	 * @param constitutionDate the new constitution date
	 */
	public void setConstitutionDate(Date constitutionDate) {
		this.constitutionDate = constitutionDate;
	}


	/**
	 * Gets the nit natural person.
	 *
	 * @return the nit natural person
	 */
	public Integer getNitNaturalPerson() {
		return nitNaturalPerson;
	}


	/**
	 * Sets the nit natural person.
	 *
	 * @param nitNaturalPerson the new nit natural person
	 */
	public void setNitNaturalPerson(Integer nitNaturalPerson) {
		this.nitNaturalPerson = nitNaturalPerson;
	}


	/**
	 * Gets the file photo.
	 *
	 * @return the file photo
	 */
	public byte[] getFilePhoto() {
		return filePhoto;
	}


	/**
	 * Sets the file photo.
	 *
	 * @param filePhoto the new file photo
	 */
	public void setFilePhoto(byte[] filePhoto) {
		this.filePhoto = filePhoto;
	}


	/**
	 * Gets the file signing.
	 *
	 * @return the file signing
	 */
	public byte[] getFileSigning() {
		return fileSigning;
	}


	/**
	 * Sets the file signing.
	 *
	 * @param fileSigning the new file signing
	 */
	public void setFileSigning(byte[] fileSigning) {
		this.fileSigning = fileSigning;
	}


	public Integer getIndCanNegotiate() {
		return indCanNegotiate;
	}


	public void setIndCanNegotiate(Integer indCanNegotiate) {
		this.indCanNegotiate = indCanNegotiate;
	}


//	public boolean isIndicatorEntityPublicAppointment() {
//		return indicatorEntityPublicAppointment;
//	}
//
//
//	public void setIndicatorEntityPublicAppointment(
//			boolean indicatorEntityPublicAppointment) {
//		this.indicatorEntityPublicAppointment = indicatorEntityPublicAppointment;
//	}
	
	/**
	 * sirtex negotiate CUI
	 * **/
	public Integer getIndSirtexNeg() {
		return indSirtexNeg;
	}

	public void setIndSirtexNeg(Integer indSirtexNeg) {
		this.indSirtexNeg = indSirtexNeg;
	}

	public Integer getInvestorType() {
		return investorType;
	}

	public void setInvestorType(Integer investorType) {
		this.investorType = investorType;
	}

	/**
	 * @return the indFatca
	 */
	public Integer getIndFatca() {
		return indFatca;
	}


	/**
	 * @param indFatca the indFatca to set
	 */
	public void setIndFatca(Integer indFatca) {
		this.indFatca = indFatca;
	}
	
	/**
	 * @return the indFatca
	 */
	public String getFilePhotoName() {
		return filePhotoName;
	}


	/**
	 * @param indFatca the indFatca to set
	 */
	public void setFilePhotoName(String filePhotoName) {
		this.filePhotoName = filePhotoName;
	}
	
}