package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum PepPersonClassificationType {
		HIGH (Integer.valueOf(1760),"ALTA"),
		LOW(Integer.valueOf(1761),"BAJA");
		

		
		private Integer code;
		private String value;
		

		public static final List<PepPersonClassificationType> list = new ArrayList<PepPersonClassificationType>();
		public static final Map<Integer, PepPersonClassificationType> lookup = new HashMap<Integer, PepPersonClassificationType>();

		static {
			for (PepPersonClassificationType s : EnumSet.allOf(PepPersonClassificationType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private PepPersonClassificationType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static PepPersonClassificationType get(Integer code) {
			return lookup.get(code);
		}

		
}
