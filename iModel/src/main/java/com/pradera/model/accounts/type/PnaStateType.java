package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PnaStateType {
	
	REGISTER(new Integer(119),"REGISTRADO");
	
	private String value;
	private Integer code;
	
	public static final List<PnaStateType> list= new ArrayList<PnaStateType>();
	public static final List<PnaStateType> listNotOfac = new ArrayList<PnaStateType>();
	public static final Map<Integer, PnaStateType> lookup = new HashMap<Integer, PnaStateType >();
	
	static{
		for(PnaStateType type: EnumSet.allOf(PnaStateType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);
			
			
			
		}
		
		
	}
	
	
	private PnaStateType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


    public Integer getCode() {
		return code;
	}
	
	public static PnaStateType get(Integer code)
	{
		return lookup.get(code);
	}


	
	
	
	

}
