package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum HolderRequestType {
	
	CREATION(Integer.valueOf(517),"CREACION"),
	MODIFICATION(Integer.valueOf(518),"MODIFICACION"),
	BLOCK(Integer.valueOf(519),"BLOQUEO"),
	UNBLOCK(Integer.valueOf(530),"DESBLOQUEO");
	
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private HolderRequestType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static HolderRequestType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<HolderRequestType> list = new ArrayList<HolderRequestType>();
	public static final Map<Integer, HolderRequestType> lookup = new HashMap<Integer, HolderRequestType>();
	static {
		for (HolderRequestType s : EnumSet.allOf(HolderRequestType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
