package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum StateBlockUnBlockHolderRequestType {
	
	REGISTERED(Integer.valueOf(502),"REGISTRADO"),
	CONFIRMED(Integer.valueOf(503),"CONFIRMADO"),
	REJECTED(Integer.valueOf(504),"RECHAZADO"),
	ANNULLED(Integer.valueOf(505),"ANULADO");
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private StateBlockUnBlockHolderRequestType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<StateBlockUnBlockHolderRequestType> list = new ArrayList<StateBlockUnBlockHolderRequestType>();
	public static final Map<Integer, StateBlockUnBlockHolderRequestType> lookup = new HashMap<Integer, StateBlockUnBlockHolderRequestType>();
	static {
		for (StateBlockUnBlockHolderRequestType s : EnumSet.allOf(StateBlockUnBlockHolderRequestType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
