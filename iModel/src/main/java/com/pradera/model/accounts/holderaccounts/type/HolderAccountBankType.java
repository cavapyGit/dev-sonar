/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */
package com.pradera.model.accounts.holderaccounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountBankType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/05/2013
 */
public enum HolderAccountBankType {
	
	/** The savings. */
	SAVINGS(new Integer(453),"AHORROS"), 
 	
	/** The checking. */
	CHECKING(new Integer(454),"CORRIENTE");  
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<HolderAccountBankType> list = new ArrayList<HolderAccountBankType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountBankType> lookup = new HashMap<Integer, HolderAccountBankType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountBankType> listSomeElements(HolderAccountBankType... transferSecuritiesTypeParams){
		List<HolderAccountBankType> retorno = new ArrayList<HolderAccountBankType>();
		for(HolderAccountBankType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static { 
		for (HolderAccountBankType s : EnumSet.allOf(HolderAccountBankType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account bank type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountBankType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account bank type
	 */
	public static HolderAccountBankType get(Integer codigo) {
		return lookup.get(codigo);
	}
}