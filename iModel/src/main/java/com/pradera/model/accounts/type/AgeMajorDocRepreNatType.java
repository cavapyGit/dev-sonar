package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeMajorDocRepreNatType {
		
		TFLPRL(Integer.valueOf(268),"TESTIMONIO O FOTOCOPIA LEGALIZADA DEL PODER OTORGADO COMO REPRESENTANTE LEGAL"),
		CI(Integer.valueOf(269),"FOTOCOPIA CEDULA DE IDENTIDAD"),
		PASS(Integer.valueOf(270),"FOTOCOPIA PASAPORTE"),
		DIO(Integer.valueOf(272),"FOTOCOPIA DEL DOCUMENTO DE IDENTIDAD DE PAIS DE ORIGEN"),
		DJCP(Integer.valueOf(274),"DECLARACION JURADA SOBRE CONDICION DE PEP");
		
		
		private Integer code;
		private String value;
		
		public static final List<AgeMajorDocRepreNatType> list = new ArrayList<AgeMajorDocRepreNatType>();
		public static final Map<Integer, AgeMajorDocRepreNatType> lookup = new HashMap<Integer, AgeMajorDocRepreNatType>();

		static {
			for (AgeMajorDocRepreNatType s : EnumSet.allOf(AgeMajorDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeMajorDocRepreNatType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeMajorDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
