package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AssertLaundryType {
	
	PNA(new Integer(119),"PNA"),
    PEP(new Integer(434),"PEP"),
	OFAC(new Integer(435),"OFAC");
	
	private String value;
	private Integer code;
	
	public static final List<AssertLaundryType> list= new ArrayList<AssertLaundryType>();
	public static final List<AssertLaundryType> listNotOfac = new ArrayList<AssertLaundryType>();
	public static final Map<Integer, AssertLaundryType> lookup = new HashMap<Integer, AssertLaundryType >();
	
	static{
		for(AssertLaundryType type: EnumSet.allOf(AssertLaundryType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);
		}
		
		
	}
	
	
	private AssertLaundryType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


    public Integer getCode() {
		return code;
	}
	
	public static AssertLaundryType get(Integer code)
	{
		return lookup.get(code);
	}


	
	
	
	

}
