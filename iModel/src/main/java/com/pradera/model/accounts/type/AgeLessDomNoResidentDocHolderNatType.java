package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeLessDomNoResidentDocHolderNatType {
		
		CE(Integer.valueOf(933),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(934),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(935),"PASAPORTE"), 
		LIC(Integer.valueOf(936),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(937),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(938),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(939),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(940),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(941),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(942),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(943),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(944),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(945),"DECRETO"),		
	    RCF(Integer.valueOf(946),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(947),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(948),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    		
		private Integer code;
		private String value;
		
		public static final List<AgeLessDomNoResidentDocHolderNatType> list = new ArrayList<AgeLessDomNoResidentDocHolderNatType>();
		public static final Map<Integer, AgeLessDomNoResidentDocHolderNatType> lookup = new HashMap<Integer, AgeLessDomNoResidentDocHolderNatType>();

		static {
			for (AgeLessDomNoResidentDocHolderNatType s : EnumSet.allOf(AgeLessDomNoResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeLessDomNoResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeLessDomNoResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
