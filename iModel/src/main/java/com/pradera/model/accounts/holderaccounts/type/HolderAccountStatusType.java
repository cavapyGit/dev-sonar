/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountStatusType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
public enum HolderAccountStatusType {
	
	/** The active. */
	ACTIVE(new Integer(477),"REGISTRADO"),
	
	/** The block. */
	BLOCK(new Integer(478),"BLOQUEADO"),
	
	/** The closed. */
	CLOSED(new Integer(479),"CERRADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<HolderAccountStatusType> list = new ArrayList<HolderAccountStatusType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountStatusType> lookup = new HashMap<Integer, HolderAccountStatusType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountStatusType> listSomeElements(HolderAccountStatusType... transferSecuritiesTypeParams){
		List<HolderAccountStatusType> retorno = new ArrayList<HolderAccountStatusType>();
		for(HolderAccountStatusType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountStatusType s : EnumSet.allOf(HolderAccountStatusType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account status type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountStatusType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account status type
	 */
	public static HolderAccountStatusType get(Integer codigo) {
		return lookup.get(codigo);
	}
}