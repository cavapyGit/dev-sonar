package com.pradera.model.accounts.holderaccounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.generalparameter.type.CurrencyType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT_BANK_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
@Entity
@Table(name="HOLDER_ACCOUNT_BANK_REQUEST")
public class HolderAccountBankRequest implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id hold acc req bank pk. */
	@Id
	@SequenceGenerator(name="SQ_HOLD_ACCOUNT_REQ_BANK_PK", sequenceName="SQ_ID_HOLD_ACCOUNT_REQ_BANK_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLD_ACCOUNT_REQ_BANK_PK")
	@Column(name="ID_HOLDER_ACCOUNT_BANK_REQ_PK") 
	private Long idHoldAccReqBankPk;

	/** The holder account request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_REQ_FK")  
	private HolderAccountRequest holderAccountRequest;
	
	/** The bank. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BANK_FK")
	private Bank bank;
	
	/** The account number. */	
	@Column(name="ACCOUNT_NUMBER")
	private String accountNumber;	

	/** The bank account type. */
	@Column(name="BANK_ACCOUNT_TYPE")
	private Integer bankAccountType;

	/** The currency. */	
	@Column(name="CURRENCY")
	private Integer currency;
	
	/** The have bic. */
	@Column(name="HAVE_BIC")
	private Integer haveBic;
	
	/** The description bank. */
	@Column(name="DESCRIPTION")
	private String descriptionBank;	
	
	/** The have old or new. */
	@Column(name="IND_OLD_NEW")
	private Integer indOldNew;
	
	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

    /** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;  

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="CODIGO_SWIFT")
	private String swiftCode;
	
	/** The state account bank. */
	@Column(name="STATE_ACCOUNT_BANK")
	private Integer stateAccountBank;
	
	@Column(name="REF_ID_HOLDER_ACCOUNT_BANK_FK")
	private Long refIdHolderAccountBankFk;
	
	@Transient
	private boolean flagForeignAccount;
	
    /**
     * Instantiates a new holder account req bank hy.
     */
    public HolderAccountBankRequest() {
    }
	
	/**
	 * Gets the description bank.
	 *
	 * @return the description bank
	 */
	public String getDescriptionBank() {
		return descriptionBank;
	}

	/**
	 * Sets the description bank.
	 *
	 * @param descriptionBank the new description bank
	 */
	public void setDescriptionBank(String descriptionBank) {
		this.descriptionBank = descriptionBank;
	}

	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public Bank getBank() {
		return bank;
	}

	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(Bank bank) {
		this.bank = bank;
	}

	/**
	 * Gets the id hold acc req bank pk.
	 *
	 * @return the id hold acc req bank pk
	 */
	public Long getIdHoldAccReqBankPk() {
		return this.idHoldAccReqBankPk;
	}

	/**
	 * Sets the id hold acc req bank pk.
	 *
	 * @param idHoldAccReqBankPk the new id hold acc req bank pk
	 */
	public void setIdHoldAccReqBankPk(Long idHoldAccReqBankPk) {
		this.idHoldAccReqBankPk = idHoldAccReqBankPk;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return this.accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the bank account type description.
	 *
	 * @return the bank account type description
	 */
	public String getBankAccountTypeDescription() {
		return HolderAccountBankType.lookup.get(this.getBankAccountType()).getValue();
	}

	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public Integer getBankAccountType() {
		return this.bankAccountType;
	}

	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return this.currency;
	}
	
	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return CurrencyType.lookup.get(this.getCurrency()).getCodeIso();
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}	

	/**
	 * Gets the holder account request hy.
	 *
	 * @return the holder account request hy
	 */
	public HolderAccountRequest getHolderAccountRequest() {
		return holderAccountRequest;
	}

	/**
	 * Sets the holder account request hy.
	 *
	 * @param holderAccountRequest the new holder account request hy
	 */
	public void setHolderAccountRequest(HolderAccountRequest holderAccountRequest) {
		this.holderAccountRequest = holderAccountRequest;
	}
	
	/**
	 * Gets the bank description.
	 * this method is useful to get bank's description from Bank object
	 * and then will be save in descriptionBank Atribute
	 * @return the bank description
	 */
	public String getBankDescription(){
		return this.getBank().getDescription();
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the old or new.
	 *
	 * @return the old or new
	 */
	public Integer getIndOldNew() {
		return indOldNew;
	}

	/**
	 * Sets the old or new.
	 *
	 * @param indOldNew the new old or new
	 */
	public void setIndOldNew(Integer indOldNew) {
		this.indOldNew = indOldNew;
	}

	/**
	 * Gets the have bic.
	 *
	 * @return the have bic
	 */
	public Integer getHaveBic() {
		return haveBic;
	}

	/**
	 * Sets the have bic.
	 *
	 * @param haveBic the new have bic.
	 */
	public void setHaveBic(Integer haveBic) {
		this.haveBic = haveBic;
	}

	/**
	 * @return the swiftCode
	 */
	public String getSwiftCode() {
		return swiftCode;
	}

	/**
	 * @param swiftCode the swiftCode to set
	 */
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	/**
	 * @return the flagForeignAccount
	 */
	public boolean isFlagForeignAccount() {
		return flagForeignAccount;
	}

	/**
	 * @param flagForeignAccount the flagForeignAccount to set
	 */
	public void setFlagForeignAccount(boolean flagForeignAccount) {
		this.flagForeignAccount = flagForeignAccount;
	}

	public Integer getStateAccountBank() {
		return stateAccountBank;
	}

	public void setStateAccountBank(Integer stateAccountBank) {
		this.stateAccountBank = stateAccountBank;
	}
		
	/**
     * Gets the state description.
     *
     * @return the state description
     */
    public String getStateDescription() {
		return Validations.validateIsNotNull(this.getStateAccountBank())?
				HolderAccountStateBankType.lookup.get(this.getStateAccountBank()).getDescription() :
					HolderAccountStateBankType.REGISTERED.getDescription() ;
	}

	public Long getRefIdHolderAccountBankFk() {
		return refIdHolderAccountBankFk;
	}

	public void setRefIdHolderAccountBankFk(Long refIdHolderAccountBankFk) {
		this.refIdHolderAccountBankFk = refIdHolderAccountBankFk;
	}

    
    
}