package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	// TODO: Auto-generated Javadoc
/**
	 * <ul>
	 * <li>
	 * Project Pradera.
	 * Copyright 2013.</li>
	 * </ul>
	 * 
	 * The Enum DocumentType.
	 *
	 * @author PraderaTechnologies.
	 * @version 1.0 , 14/07/2013
	 */
	public enum DocumentType {
		
		
		CI(Integer.valueOf(86),"CEDULA DE IDENTIDAD"),
		CID(Integer.valueOf(2148),"CEDULA DE IDENTIDAD DUPLICADO"),
		PAS(Integer.valueOf(389),"PASAPORTE"),
		CIEE(Integer.valueOf(2149),"CEDULA DE IDENTIDAD EMITIDO EN EL EXTERIOR"),
		DIO(Integer.valueOf(390),"IDENTIDAD DE PAIS ORIGEN"),
		CIE(Integer.valueOf(387),"CEDULA DE IDENTIDAD DE EXTRANJERO"),
		DCDL(Integer.valueOf(2150),"CARNET DIPLOMATICO EN LIBRETA"),
		DCD(Integer.valueOf(2151),"CARNET DIPLOMATICO"),
		DCRA(Integer.valueOf(2152),"CREDENCIAL A"),
		DCRB(Integer.valueOf(2153),"CREDENCIAL B"),
		DLC(Integer.valueOf(2154),"LIBRETA CONSULAR"),
		DCC(Integer.valueOf(2155),"CARNET CONSULAR"),
		DCR(Integer.valueOf(2156),"CREDENCIAL"),
		RUC(Integer.valueOf(386),"REGISTRO UNICO DEL CONTRIBUYENTE (RUC)"),		
		EE(Integer.valueOf(1783),"EMPRESA EXTRANJERA"),
		CDN(Integer.valueOf(388),"CERTIFICADO DE NACIMIENTO");
		
		/** The code. */
		private Integer code;
		
		/** The value. */
		private String value;
		
		/** The Constant list. */
		public static final List<DocumentType> list = new ArrayList<DocumentType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, DocumentType> lookup = new HashMap<Integer, DocumentType>();

		static {
			for (DocumentType s : EnumSet.allOf(DocumentType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}
		
		public static String[] names() {

		    LinkedList<String> list = new LinkedList<String>();
		    for (DocumentType s : values()) {
		        list.add(s.name());
		    }

		    return list.toArray(new String[list.size()]);
		}

		/**
		 * Instantiates a new document type.
		 *
		 * @param code the code
		 * @param value the value
		 * @param abrev the abrev
		 */
		private DocumentType(Integer code, String value) {
			this.code = code;
			this.value = value;
		}
		
		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}
		
		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		
		/**
		 * Gets the description.
		 *
		 * @param locale the locale
		 * @return the description
		 */
		public String getDescription(Locale locale) {
			return null;
		}
		
		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the document type
		 */
		public static DocumentType get(Integer code) {
			return lookup.get(code);
		}
}