package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.type.ParticipantStateType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PARTICIPANT_HISTORY_STATE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/03/2013
 */
@Entity
@Table(name="PARTICIPANT_HISTORY_STATE")
public class ParticipantHistoryState implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id part history state pk. */
	@Id
	@SequenceGenerator(name="PARTICIPANT_HISTORY_STATE_IDPARTHISTORYSTATEPK_GENERATOR", sequenceName="SQ_ID_PART_HISTORY_STATE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_HISTORY_STATE_IDPARTHISTORYSTATEPK_GENERATOR")
	@Column(name="ID_PART_HISTORY_STATE_PK")
	private Long idPartHistoryStatePk;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The motive. */
	private String motive;

	/** The new state. */
	@Column(name="NEW_STATE")
	private Integer newState;

	/** The old state. */
	@Column(name="OLD_STATE")
	private Integer oldState;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

    /** The update state date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_STATE_DATE")
	private Date updateStateDate;

	//bi-directional many-to-one association to Participant
    /** The participant. */
	@ManyToOne
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;

	//bi-directional many-to-one association to ParticipantRequest
    /** The participant request. */
	@ManyToOne
	@JoinColumn(name="ID_PARTICIPANT_REQUEST_FK", referencedColumnName="ID_PARTICIPANT_REQUEST_PK")
	private ParticipantRequest participantRequest;

    /**
     * Instantiates a new participant history state.
     */
    public ParticipantHistoryState() {
    }
    
    /**
     * Gets the old state description.
     *
     * @return the old state description
     */
    public String getOldStateDescription(){
    	if(Validations.validateIsNotNullAndPositive(oldState)){
    		return ParticipantStateType.get(oldState).getValue();
    	}
    	return null;
    }
    
    /**
     * Gets the new state description.
     *
     * @return the new state description
     */
    public String getNewStateDescription(){
    	if(Validations.validateIsNotNullAndPositive(newState)){
    		return ParticipantStateType.get(newState).getValue();
    	}
    	return null;
    }

	/**
	 * Gets the id part history state pk.
	 *
	 * @return the id part history state pk
	 */
	public Long getIdPartHistoryStatePk() {
		return idPartHistoryStatePk;
	}

	/**
	 * Sets the id part history state pk.
	 *
	 * @param idPartHistoryStatePk the new id part history state pk
	 */
	public void setIdPartHistoryStatePk(Long idPartHistoryStatePk) {
		this.idPartHistoryStatePk = idPartHistoryStatePk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public String getMotive() {
		return motive;
	}

	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(String motive) {
		this.motive = motive;
	}

	/**
	 * Gets the new state.
	 *
	 * @return the new state
	 */
	public Integer getNewState() {
		return newState;
	}

	/**
	 * Sets the new state.
	 *
	 * @param newState the new new state
	 */
	public void setNewState(Integer newState) {
		this.newState = newState;
	}

	/**
	 * Gets the old state.
	 *
	 * @return the old state
	 */
	public Integer getOldState() {
		return oldState;
	}

	/**
	 * Sets the old state.
	 *
	 * @param oldState the new old state
	 */
	public void setOldState(Integer oldState) {
		this.oldState = oldState;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the update state date.
	 *
	 * @return the update state date
	 */
	public Date getUpdateStateDate() {
		return updateStateDate;
	}

	/**
	 * Sets the update state date.
	 *
	 * @param updateStateDate the new update state date
	 */
	public void setUpdateStateDate(Date updateStateDate) {
		this.updateStateDate = updateStateDate;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the participant request.
	 *
	 * @return the participant request
	 */
	public ParticipantRequest getParticipantRequest() {
		return participantRequest;
	}

	/**
	 * Sets the participant request.
	 *
	 * @param participantRequest the new participant request
	 */
	public void setParticipantRequest(ParticipantRequest participantRequest) {
		this.participantRequest = participantRequest;
	}    
	
	
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}