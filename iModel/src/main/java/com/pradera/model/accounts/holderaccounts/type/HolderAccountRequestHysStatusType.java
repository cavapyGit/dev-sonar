/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountRequestHysStatusType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/04/2013
 */
public enum HolderAccountRequestHysStatusType {
	
	/** The registered. */
	REGISTERED(new Integer(468),"REGISTRADO"),
	
	/** The confirm. */
	CONFIRM(new Integer(469),"CONFIRMADO"),
	
	/** The anulate. */
	ANULATE(new Integer(470),"ANULADO"),
	
	/** The reject. */
	REJECT(new Integer(471),"RECHAZADO"),
	
	/** The approve. */
	APPROVE(new Integer(527),"APROBADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<HolderAccountRequestHysStatusType> list = new ArrayList<HolderAccountRequestHysStatusType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountRequestHysStatusType> lookup = new HashMap<Integer, HolderAccountRequestHysStatusType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountRequestHysStatusType> listSomeElements(HolderAccountRequestHysStatusType... transferSecuritiesTypeParams){
		List<HolderAccountRequestHysStatusType> retorno = new ArrayList<HolderAccountRequestHysStatusType>();
		for(HolderAccountRequestHysStatusType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountRequestHysStatusType s : EnumSet.allOf(HolderAccountRequestHysStatusType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account request hys status type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountRequestHysStatusType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account request hys status type
	 */
	public static HolderAccountRequestHysStatusType get(Integer codigo) {
		return lookup.get(codigo);
	}
}