package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum UnableDomOrForeignResidentDocHolderNatType {
		
		CE(Integer.valueOf(965),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(966),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(967),"PASAPORTE"), 
		LIC(Integer.valueOf(968),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(969),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(970),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(971),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(972),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(973),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(974),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(975),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(976),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(977),"DECRETO"),		
	    RCF(Integer.valueOf(978),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(979),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(980),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    private Integer code;
		private String value;
		
		public static final List<UnableDomOrForeignResidentDocHolderNatType> list = new ArrayList<UnableDomOrForeignResidentDocHolderNatType>();
		public static final Map<Integer, UnableDomOrForeignResidentDocHolderNatType> lookup = new HashMap<Integer, UnableDomOrForeignResidentDocHolderNatType>();

		static {
			for (UnableDomOrForeignResidentDocHolderNatType s : EnumSet.allOf(UnableDomOrForeignResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private UnableDomOrForeignResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static UnableDomOrForeignResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
