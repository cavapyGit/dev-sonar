package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequesterType;
import com.pradera.model.accounts.type.HolderBlockMotiveType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderUnBlockMotiveType;





// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_REQUEST database table.
 * 
 */

@NamedQueries({
	@NamedQuery(name = HolderRequest.HOLDERREQUEST_SEARCH_BY_ID, query = "Select h From HolderRequest h WHERE h.idHolderRequestPk = :idHolderRequestPrm")
})
@Entity
@Table(name="HOLDER_REQUEST")
public class HolderRequest implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant HOLDERREQUEST_SEARCH_BY_ID. */
	public static final String HOLDERREQUEST_SEARCH_BY_ID = "HolderRequest.searchRequestById";

	/** The id holder request pk. */
	@Id
	@SequenceGenerator(name="HOLDER_REQUEST_IDHOLDERREQUESTPK_GENERATOR", sequenceName="SQ_ID_HOLDER_REQUEST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_REQUEST_IDHOLDERREQUESTPK_GENERATOR")
	@Column(name="ID_HOLDER_REQUEST_PK")
	private Long idHolderRequestPk;

    /** The action date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="ACTION_DATE")
	private Date actionDate;

	/** The action motive. */
	@Column(name="ACTION_MOTIVE")
	private Integer actionMotive;

	/** The action other motive. */
	@Column(name="ACTION_OTHER_MOTIVE")
	private String actionOtherMotive;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The request motive. */
	@Column(name="REQUEST_MOTIVE")
	private Integer requestMotive;

 
	/** The request number. */
	@Column(name="REQUEST_NUMBER")
	private Long requestNumber;

	/** The request other motive. */
	@Column(name="REQUEST_OTHER_MOTIVE")
	private String requestOtherMotive;

	/** The request type. */
	@Column(name="REQUEST_TYPE")
	private Integer requestType;

	/** The state holder request. */
	@Column(name="STATE_HOLDER_REQUEST")
	private Integer stateHolderRequest;
	
	/** The confirm date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;
	
	/** The confirm user. */
	@Column(name="CONFIRM_USER")
	private String confirmUser;
	
	/** The reject date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REJECT_DATE")
	private Date rejectDate;
	
	/** The reject user. */
	@Column(name="REJECT_USER") 
	private String rejectUser;
	
	/** The annul date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="ANNUL_DATE")
	private Date annulDate;
	
	/** The annul user. */
	@Column(name="ANNUL_USER")
	private String annulUser;
	
	/** The approve date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="APPROVE_DATE")
	private Date approveDate;
	
	/** The approve user. */
	@Column(name="APPROVE_USER")
	private String approveUser;
	
	/** The requester type. */
	@Column(name="REQUESTER_TYPE")
	private Integer requesterType;
	
	
	/** The holder histories. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="holderRequest",fetch=FetchType.LAZY)
	private List<HolderHistory> holderHistories;
	
	/** The legal representative histories. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="holderRequest",fetch=FetchType.LAZY)
	private List<LegalRepresentativeHistory> legalRepresentativeHistories;

	/** The holder req file histories. */
	//bi-directional many-to-one association to HolderFile
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="holderRequest")
	private List<HolderReqFileHistory> holderReqFileHistories;	

	/** The holder history states. */
	//bi-directional many-to-one association to HolderHistoryState
	@OneToMany(cascade=CascadeType.ALL,mappedBy="holderRequest",fetch=FetchType.LAZY)
	private List<HolderHistoryState> holderHistoryStates;

	/** The holder. */
	//bi-directional many-to-one association to Holder
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;

	/** The participant. */
	//bi-directional many-to-one association to Participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

    /** The holder history. */
    @Transient
    private HolderHistory holderHistory;
    
    /** The state description. */
    @Transient
    private String stateDescription;
    
    /** The request type description. */
    @Transient
    private String requestTypeDescription;
    
    /** The request motive description. */
    @Transient
    private String requestMotiveDescription;
    
    /** The participant description. */
    @Transient
    private String participantDescription;
    
    
    /** The requester type description. */
    @Transient 
    private String requesterTypeDescription;
    
    /**
     * Instantiates a new holder request.
     */
    public HolderRequest() {
    }

	/**
	 * Gets the id holder request pk.
	 *
	 * @return the id holder request pk
	 */
	public Long getIdHolderRequestPk() {
		return this.idHolderRequestPk;
	}

	/**
	 * Sets the id holder request pk.
	 *
	 * @param idHolderRequestPk the new id holder request pk
	 */
	public void setIdHolderRequestPk(Long idHolderRequestPk) {
		this.idHolderRequestPk = idHolderRequestPk;
	}

	/**
	 * Gets the action date.
	 *
	 * @return the action date
	 */
	public Date getActionDate() {
		return this.actionDate;
	}

	/**
	 * Sets the action date.
	 *
	 * @param actionDate the new action date
	 */
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 * Gets the action motive.
	 *
	 * @return the action motive
	 */
	public Integer getActionMotive() {
		return this.actionMotive;
	}

	/**
	 * Sets the action motive.
	 *
	 * @param actionMotive the new action motive
	 */
	public void setActionMotive(Integer actionMotive) {
		this.actionMotive = actionMotive;
	}

	/**
	 * Gets the action other motive.
	 *
	 * @return the action other motive
	 */
	public String getActionOtherMotive() {
		return this.actionOtherMotive;
	}

	/**
	 * Sets the action other motive.
	 *
	 * @param actionOtherMotive the new action other motive
	 */
	public void setActionOtherMotive(String actionOtherMotive) {
		this.actionOtherMotive = actionOtherMotive;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the request motive.
	 *
	 * @return the request motive
	 */
	public Integer getRequestMotive() {
		return this.requestMotive;
	}

	/**
	 * Sets the request motive.
	 *
	 * @param requestMotive the new request motive
	 */
	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return this.requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the request other motive.
	 *
	 * @return the request other motive
	 */
	public String getRequestOtherMotive() {
		return this.requestOtherMotive;
	}

	/**
	 * Sets the request other motive.
	 *
	 * @param requestOtherMotive the new request other motive
	 */
	public void setRequestOtherMotive(String requestOtherMotive) {
		this.requestOtherMotive = requestOtherMotive;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return this.requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the state holder request.
	 *
	 * @return the state holder request
	 */
	public Integer getStateHolderRequest() {
		return stateHolderRequest;
	}

	/**
	 * Sets the state holder request.
	 *
	 * @param stateHolderRequest the new state holder request
	 */
	public void setStateHolderRequest(Integer stateHolderRequest) {
		this.stateHolderRequest = stateHolderRequest;
	}

	
	
	/**
	 * Gets the holder history states.
	 *
	 * @return the holder history states
	 */
	public List<HolderHistoryState> getHolderHistoryStates() {
		return this.holderHistoryStates;
	}

	/**
	 * Sets the holder history states.
	 *
	 * @param holderHistoryStates the new holder history states
	 */
	public void setHolderHistoryStates(List<HolderHistoryState> holderHistoryStates) {
		this.holderHistoryStates = holderHistoryStates;
	}
		
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return this.holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
		

	/**
	 * Gets the holder req file histories.
	 *
	 * @return the holder req file histories
	 */
	public List<HolderReqFileHistory> getHolderReqFileHistories() {
		return holderReqFileHistories;
	}

	/**
	 * Sets the holder req file histories.
	 *
	 * @param holderReqFileHistories the new holder req file histories
	 */
	public void setHolderReqFileHistories(
			List<HolderReqFileHistory> holderReqFileHistories) {
		this.holderReqFileHistories = holderReqFileHistories;
	}

	

	/**
	 * Gets the legal representative histories.
	 *
	 * @return the legal representative histories
	 */
	public List<LegalRepresentativeHistory> getLegalRepresentativeHistories() {
		return legalRepresentativeHistories;
	}

	/**
	 * Sets the legal representative histories.
	 *
	 * @param legalRepresentativeHistories the new legal representative histories
	 */
	public void setLegalRepresentativeHistories(
			List<LegalRepresentativeHistory> legalRepresentativeHistories) {
		this.legalRepresentativeHistories = legalRepresentativeHistories;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
				
		return stateDescription;		
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the confirm user.
	 *
	 * @return the confirm user
	 */
	public String getConfirmUser() {
		return confirmUser;
	}

	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the new confirm user
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * Gets the reject date.
	 *
	 * @return the reject date
	 */
	public Date getRejectDate() {
		return rejectDate;
	}

	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the new reject date
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	/**
	 * Gets the reject user.
	 *
	 * @return the reject user
	 */
	public String getRejectUser() {
		return rejectUser;
	}

	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the new reject user
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	/**
	 * Gets the annul date.
	 *
	 * @return the annul date
	 */
	public Date getAnnulDate() {
		return annulDate;
	}

	/**
	 * Sets the annul date.
	 *
	 * @param annulDate the new annul date
	 */
	public void setAnnulDate(Date annulDate) {
		this.annulDate = annulDate;
	}

	/**
	 * Gets the annul user.
	 *
	 * @return the annul user
	 */
	public String getAnnulUser() {
		return annulUser;
	}

	/**
	 * Sets the annul user.
	 *
	 * @param annulUser the new annul user
	 */
	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}

	/**
	 * Gets the request type description.
	 *
	 * @return the request type description
	 */
	public String getRequestTypeDescription() {
		return HolderRequestType.get(requestType).getValue();
	}

	/**
	 * Sets the request type description.
	 *
	 * @param requestTypeDescription the new request type description
	 */
	public void setRequestTypeDescription(String requestTypeDescription) {
		this.requestTypeDescription = requestTypeDescription;
	}

	/**
	 * Gets the request motive description.
	 *
	 * @return the request motive description
	 */
	public String getRequestMotiveDescription() {
		if(HolderRequestType.BLOCK.getCode().equals(requestType)){
			
			if(requestMotive.equals(HolderBlockMotiveType.OTHERS_MOTIVES.getCode())){
				requestMotiveDescription = requestOtherMotive;
			}
			else{
			requestMotiveDescription = HolderBlockMotiveType.get(requestMotive).getValue();
			}
		}
		else if(HolderRequestType.UNBLOCK.getCode().equals(requestType)){
			
			if(requestMotive.equals(HolderUnBlockMotiveType.OTHERS_MOTIVES.getCode())){
				requestMotiveDescription = requestOtherMotive;
			}
			else{
			requestMotiveDescription = HolderUnBlockMotiveType.get(requestMotive).getValue();
			}
		}
		else if(HolderRequestType.CREATION.getCode().equals(requestType)){
			
			if(requestMotive.equals(HolderUnBlockMotiveType.OTHERS_MOTIVES.getCode())){
				requestMotiveDescription = requestOtherMotive;
			}
			else{
			requestMotiveDescription = HolderUnBlockMotiveType.get(requestMotive).getValue();
			}
		}
		
		return requestMotiveDescription;
	}

	/**
	 * Sets the request motive description.
	 *
	 * @param requestMotiveDescription the new request motive description
	 */
	public void setRequestMotiveDescription(String requestMotiveDescription) {
		this.requestMotiveDescription = requestMotiveDescription;
	}
	
	/**
	 * Gets the approve date.
	 *
	 * @return the approve date
	 */
	public Date getApproveDate() {
		return approveDate;
	}

	/**
	 * Sets the approve date.
	 *
	 * @param approveDate the new approve date
	 */
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	/**
	 * Gets the approve user.
	 *
	 * @return the approve user
	 */
	public String getApproveUser() {
		return approveUser;
	}

	/**
	 * Sets the approve user.
	 *
	 * @param approveUser the new approve user
	 */
	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}
	
	

	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
        
		String participantDescription;
		
		if(participant!=null){		
			participantDescription = participant.getMnemonic() +" "+participant.getIdParticipantPk();
        }
		else{
			participantDescription = " ";
		}
		
		return participantDescription;
	}

	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}
	
	

	/**
	 * Gets the requester type.
	 *
	 * @return the requester type
	 */
	public Integer getRequesterType() {
		return requesterType;
	}

	/**
	 * Sets the requester type.
	 *
	 * @param requesterType the new requester type
	 */
	public void setRequesterType(Integer requesterType) {
		this.requesterType = requesterType;
	}
	
	

	/**
	 * Gets the requester type description.
	 *
	 * @return the requester type description
	 */
	public String getRequesterTypeDescription() {
		
		if(requesterType!=null){
		requesterTypeDescription = HolderAccountRequesterType.get(requesterType).getValue();
		}
		else{
			requesterTypeDescription = " ";
		}
		
		return requesterTypeDescription;
	}

	/**
	 * Sets the requester type description.
	 *
	 * @param requesterTypeDescription the new requester type description
	 */
	public void setRequesterTypeDescription(String requesterTypeDescription) {
		this.requesterTypeDescription = requesterTypeDescription;
	}
	
	

	/**
	 * Gets the holder histories.
	 *
	 * @return the holder histories
	 */
	public List<HolderHistory> getHolderHistories() {
		return holderHistories;
	}

	/**
	 * Sets the holder histories.
	 *
	 * @param holderHistories the new holder histories
	 */
	public void setHolderHistories(List<HolderHistory> holderHistories) {
		this.holderHistories = holderHistories;
	}
	
	

	/**
	 * Gets the holder history.
	 *
	 * @return the holder history
	 */
	public HolderHistory getHolderHistory() {
		return holderHistory;
	}

	/**
	 * Sets the holder history.
	 *
	 * @param holderHistory the new holder history
	 */
	public void setHolderHistory(HolderHistory holderHistory) {
		this.holderHistory = holderHistory;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null){
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
			if(loggerUser.getAuditTime() != null){
				lastModifyDate = loggerUser.getAuditTime();
			} else {
				lastModifyDate = objLoggerUser.getAuditTime();
			}
			if(loggerUser.getIpAddress() != null) {
				lastModifyIp = loggerUser.getIpAddress();
			} else {
				lastModifyIp = objLoggerUser.getIpAddress();
			}
			if(loggerUser.getUserName() != null) {
				lastModifyUser = loggerUser.getUserName();
			} else {
				lastModifyUser = objLoggerUser.getUserName();
			}			
		} else {			
			lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();       
        detailsMap.put("holderReqFileHistories", holderReqFileHistories);
	    detailsMap.put("legalRepresentativeHistories", legalRepresentativeHistories);
        detailsMap.put("holderHistoryStates",holderHistoryStates);
        detailsMap.put("holderHistories",holderHistories);
        return detailsMap;
	}
}