package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PepChargeType;
import com.pradera.model.accounts.type.PepMotiveType;
import com.pradera.model.generalparameter.GeographicLocation;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PepPerson.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Entity
@Table(name="PEP_PERSON")
public class PepPerson implements Serializable,Auditable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id pep person pk. */
	@Id
	@SequenceGenerator(name="PEP_PERSON_IDPEPPERSONPK_GENERATOR", sequenceName="SQ_ID_PEP_PERSON_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PEP_PERSON_IDPEPPERSONPK_GENERATOR")
	@Column(name="ID_PEP_PERSON_PK")
	private Long idPepPersonPk;

	/** The begining period. */
	@Temporal(TemporalType.DATE)
	@Column(name="BEGINING_PERIOD")
	private Date beginingPeriod;

	/** The category. */
	@Column(name="CATEGORY")
	private Integer category;

	/** The comments. */
	@Column(name="COMMENTS")
	private String comments;

	/** The ending period. */
	@Temporal(TemporalType.DATE)
	@Column(name="ENDING_PERIOD")
	private Date endingPeriod;

	/** The first name. */
	@Column(name="FIRST_NAME")
	private String firstName;
	
	/** The last name. */
	@Column(name="LAST_NAME")
	private String lastName;

	/** The holder. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;

	/** The legal representative. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LEGAL_REPRESENTATIVE_FK",referencedColumnName="ID_LEGAL_REPRESENTATIVE_PK")
	private LegalRepresentative legalRepresentative;


	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The role. */
	@Column(name="\"ROLE\"")
	private Integer role;


	/** The id country. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_GEOGRAPHIC_LOCATION_FK")
	private GeographicLocation geographicLocation;

	/** The motive description. */
	@Transient
	private String motiveDescription;

	/** The charge description. */
	@Transient
	private String  chargeDescription;
	
	/** The notification date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NOTIFICATION_DATE")
	private Date notificationDate;
	
	/** The expedition place. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COUNTRY_IDENTITY")
	private GeographicLocation expeditionPlace;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;
	
	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;
	
	/** The classification. */
	@Column(name="PEP_CLASSIFICATION")
	private Integer classification;
	
	@Column(name="GRADE")
	private Integer grade;
	
	@Column(name="PEP_RELATED_NAME")
	private String pepRelatedName;

	/** The document type description. */
	@Transient
	private String documentTypeDescription;
	
	/** The classification desc. */
	@Transient
	private String classificationDesc;
	
	/**
	 * Gets the document type description.
	 *
	 * @return the document type description
	 */
	public String getDocumentTypeDescription() {
		if(documentType!=null && documentType!=0){
			documentTypeDescription= DocumentType.get(documentType).getValue();
		}else{
			documentTypeDescription="";
		}
		
		return documentTypeDescription;
	}

	/**
	 * Sets the document type description.
	 *
	 * @param documentTypeDescription the new document type description
	 */
	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	/**
	 * Gets the charge description.
	 *
	 * @return the charge description
	 */
	public String getChargeDescription() {

		if(role!=null){
			this.chargeDescription = PepChargeType.get(role).getValue();
		}

		return chargeDescription;
	}

	/**
	 * Sets the charge description.
	 *
	 * @param chargeDescription the new charge description
	 */
	public void setChargeDescription(String chargeDescription) {
		if(role!=null){
			this.chargeDescription = PepChargeType.get(role).getValue();
		}
		this.chargeDescription = chargeDescription;
	}

	/**
	 * Gets the motive description.
	 *
	 * @return the motive description
	 */
	public String getMotiveDescription() {
		motiveDescription = PepMotiveType.get(category).getValue();	
		
		return motiveDescription;
	}

	/**
	 * Sets the motive description.
	 *
	 * @param motiveDescription the new motive description
	 */
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}

	/**
	 * Instantiates a new pep person.
	 */
	public PepPerson() {
	}

	/**
	 * Gets the id pep person pk.
	 *
	 * @return the id pep person pk
	 */
	public Long getIdPepPersonPk() {
		return this.idPepPersonPk;
	}

	/**
	 * Sets the id pep person pk.
	 *
	 * @param idPepPersonPk the new id pep person pk
	 */
	public void setIdPepPersonPk(Long idPepPersonPk) {
		this.idPepPersonPk = idPepPersonPk;
	}

	/**
	 * Gets the begining period.
	 *
	 * @return the begining period
	 */
	public Date getBeginingPeriod() {
		return this.beginingPeriod;
	}

	/**
	 * Sets the begining period.
	 *
	 * @param beginingPeriod the new begining period
	 */
	public void setBeginingPeriod(Date beginingPeriod) {
		this.beginingPeriod = beginingPeriod;
	}

	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public Integer getCategory() {
		return this.category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(Integer category) {
		this.category = category;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return this.comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the ending period.
	 *
	 * @return the ending period
	 */
	public Date getEndingPeriod() {
		return this.endingPeriod;
	}

	/**
	 * Sets the ending period.
	 *
	 * @param endingPeriod the new ending period
	 */
	public void setEndingPeriod(Date endingPeriod) {
		this.endingPeriod = endingPeriod;
	}



	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return this.holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return this.role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Gets the legal representative.
	 *
	 * @return the legal representative
	 */
	public LegalRepresentative getLegalRepresentative() {
		return legalRepresentative;
	}

	/**
	 * Sets the legal representative.
	 *
	 * @param legalRepresentative the new legal representative
	 */
	public void setLegalRepresentative(LegalRepresentative legalRepresentative) {
		this.legalRepresentative = legalRepresentative;
	}

	/**
	 * Gets the geographic location.
	 *
	 * @return the geographic location
	 */
	public GeographicLocation getGeographicLocation() {
		return geographicLocation;
	}

	/**
	 * Sets the geographic location.
	 *
	 * @param geographicLocation the new geographic location
	 */
	public void setGeographicLocation(GeographicLocation geographicLocation) {
		this.geographicLocation = geographicLocation;
	}
	
	/**
	 * Gets the notification date.
	 *
	 * @return the notification date
	 */
	public Date getNotificationDate() {
		return notificationDate;
	}

	/**
	 * Sets the notification date.
	 *
	 * @param notificationDate the new notification date
	 */
	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp =  loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
			if(loggerUser.getAuditTime() != null) {
				lastModifyDate = loggerUser.getAuditTime();
			} else {
				lastModifyDate = objLoggerUser.getAuditTime();
			}
			if(loggerUser.getIpAddress() != null) {
				lastModifyIp = loggerUser.getIpAddress();
			} else {
				lastModifyIp = objLoggerUser.getIpAddress();
			}
			if(loggerUser.getUserName() != null) {
				lastModifyUser = loggerUser.getUserName();
			} else {
				lastModifyUser = objLoggerUser.getUserName();
			}
		} else {
			lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the expedition place.
	 *
	 * @return the expedition place
	 */
	public GeographicLocation getExpeditionPlace() {
		return expeditionPlace;
	}

	/**
	 * Sets the expedition place.
	 *
	 * @param expeditionPlace the new expedition place
	 */
	public void setExpeditionPlace(GeographicLocation expeditionPlace) {
		this.expeditionPlace = expeditionPlace;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the classification.
	 *
	 * @return the classification
	 */
	public Integer getClassification() {
		return classification;
	}

	/**
	 * Sets the classification.
	 *
	 * @param classification the new classification
	 */
	public void setClassification(Integer classification) {
		this.classification = classification;
	}

	/**
	 * Gets the classification desc.
	 *
	 * @return the classification desc
	 */
	public String getClassificationDesc() {
		return classificationDesc;
	}

	/**
	 * Sets the classification desc.
	 *
	 * @param classificationDesc the new classification desc
	 */
	public void setClassificationDesc(String classificationDesc) {
		this.classificationDesc = classificationDesc;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getPepRelatedName() {
		return pepRelatedName;
	}

	public void setPepRelatedName(String pepRelatedName) {
		this.pepRelatedName = pepRelatedName;
	}
	
}