package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeLessFatherDocRepreNatType {
		
			
		CI(Integer.valueOf(279),"FOTOCOPIA CEDULA DE IDENTIDAD"), 
		PASS(Integer.valueOf(280),"FOTOCOPIA PASAPORTE"),
		DIO(Integer.valueOf(282),"FOTOCOPIA DEL DOCUMENTO DE IDENTIDAD DE PAIS DE ORIGEN"),
		DJCP(Integer.valueOf(284),"DECLARACION JURADA SOBRE CONDICION DE PEP"),
	    CN(Integer.valueOf(285),"CERTIFICADO DE NACIMIENTO DEL TITULAR A QUIEN REPRESENTA");
		
		
		private Integer code;
		private String value;
		

		public static final List<AgeLessFatherDocRepreNatType> list = new ArrayList<AgeLessFatherDocRepreNatType>();
		public static final Map<Integer, AgeLessFatherDocRepreNatType> lookup = new HashMap<Integer, AgeLessFatherDocRepreNatType>();

		static {
			for (AgeLessFatherDocRepreNatType s : EnumSet.allOf(AgeLessFatherDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeLessFatherDocRepreNatType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeLessFatherDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
