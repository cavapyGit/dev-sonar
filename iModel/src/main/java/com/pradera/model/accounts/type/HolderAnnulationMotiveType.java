package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
public enum HolderAnnulationMotiveType {
	
	// MOTIVIOS PARA LA ANULACION DE CUI
	REGULATORY_ENTE_REQUEST(Integer.valueOf(2490),"A SOLICITUD ENTE REGULADOR"),
	EDV_REQUEST(Integer.valueOf(2491),"A SOLICITUD DE LA EDV"),
	PARTICIPANT_REQUEST(Integer.valueOf(2492),"A SOLICITUD DEL PARTICIPANTE"),
	DEATH_HOLDER(Integer.valueOf(2493),"FALLECIMIENTO"),
	JUDICAL_ORDER(Integer.valueOf(2494),"POR ORDEN JUDICIAL"),
	OTHER_MOTIVES(Integer.valueOf(2495),"OTROS MOTIVOS"),
	HOLDER_REQUEST(Integer.valueOf(2815),"A SOLICITUD DEL TITULAR"),
	
	;
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new holder state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private HolderAnnulationMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the holder state type
	 */
	public static HolderAnnulationMotiveType get(Integer code) {
		return lookup.get(code);
	}

	/** The Constant list. */
	public static final List<HolderAnnulationMotiveType> list = new ArrayList<HolderAnnulationMotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAnnulationMotiveType> lookup = new HashMap<Integer, HolderAnnulationMotiveType>();
	static {
		for (HolderAnnulationMotiveType s : EnumSet.allOf(HolderAnnulationMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}