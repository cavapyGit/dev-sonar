package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PnaPersonInformationSourceType;
import com.pradera.model.accounts.type.PnaStateType;
import com.pradera.model.generalparameter.GeographicLocation;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PnaPerson.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Entity
@Table(name="PNA_PERSON")
public class PnaPerson implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id pna person pk. */
	@Id
	@SequenceGenerator(name="PNA_PERSON_IDPNAPERSONPK_GENERATOR", sequenceName="SQ_ID_PNA_PERSON_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PNA_PERSON_IDPNAPERSONPK_GENERATOR")
	@Column(name="ID_PNA_PERSON_PK")
	private Long idPnaPersonPk;

	/** The comment. */
	@Column(name="COMMENTS")
	private String comment;

	/** The first name. */
	@NotNull(message="adm.assetlaundry.register.requiredNamefield")
	@Column(name="FIRST_NAME")
	private String firstName;
	
	/** The last name. */
	@NotNull(message="adm.assetlaundry.register.requiredNamefield")
	@Column(name="LAST_NAME")
	private String lastName;

	/** The country. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COUNTRY")
	private GeographicLocation country;

	/** The information source. */
	@Column(name="INFORMATION_SOURCE")
	private Integer informationSource;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The motive. */
	@Column(name="MOTIVE")
	private String motive;

	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	/** The notification date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NOTIFICATION_DATE")
	private Date notificationDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state. */
	@Column(name="PNA_PERSON_STATE")
	private Integer state;
	
	/** The expedition place. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COUNTRY_IDENTITY")
	private GeographicLocation expeditionPlace;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;
	
	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The state description. */
	@Transient
	private String stateDescription;

	/** The information source description. */
	@Transient
	private String informationSourceDescription;

	/** The document type description. */
	@Transient
	private String documentTypeDescription;
	
	/**
	 * Gets the document type description.
	 *
	 * @return the document type description
	 */
	public String getDocumentTypeDescription() {
		if(documentType!=null && documentType!=0){
			documentTypeDescription= DocumentType.get(documentType).getValue();
		}else{
			documentTypeDescription="";
		}
		
		return documentTypeDescription;
	}

	/**
	 * Sets the document type description.
	 *
	 * @param documentTypeDescription the new document type description
	 */
	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}
	
	/**
	 * Gets the information source description.
	 *
	 * @return the information source description
	 */
	public String getInformationSourceDescription() {
		if(informationSource!=null){
			this.informationSourceDescription = PnaPersonInformationSourceType.get(informationSource).getValue();
		}
		return informationSourceDescription;
	}

	/**
	 * Sets the information source description.
	 *
	 * @param informationSourceDescription the new information source description
	 */
	public void setInformationSourceDescription(String informationSourceDescription) {
		if(informationSource!=null){
			this.informationSourceDescription = PnaPersonInformationSourceType.get(informationSource).getValue();
		}
		this.informationSourceDescription = informationSourceDescription;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
		this.stateDescription = PnaStateType.get(state).getValue();

	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Instantiates a new pna person.
	 */
	public PnaPerson() {
	}

	/**
	 * Gets the id pna person pk.
	 *
	 * @return the id pna person pk
	 */
	public Long getIdPnaPersonPk() {
		return this.idPnaPersonPk;
	}

	/**
	 * Sets the id pna person pk.
	 *
	 * @param idPnaPersonPk the new id pna person pk
	 */
	public void setIdPnaPersonPk(Long idPnaPersonPk) {
		this.idPnaPersonPk = idPnaPersonPk;
	}

	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		return this.comment;
	}

	/**
	 * Sets the comment.
	 *
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}


	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public GeographicLocation getCountry() {
		return this.country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(GeographicLocation country) {
		this.country = country;
	}

	/**
	 * Gets the information source.
	 *
	 * @return the information source
	 */
	public Integer getInformationSource() {
		return this.informationSource;
	}

	/**
	 * Sets the information source.
	 *
	 * @param informationSource the new information source
	 */
	public void setInformationSource(Integer informationSource) {
		this.informationSource = informationSource;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public String getMotive() {
		return this.motive;
	}

	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(String motive) {
		this.motive = motive;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return idPnaPersonPk + ", " +
				registryDate + "," + motive +", " + comment+"";


	}

	//	/* (non-Javadoc)
	//	 * @see java.lang.Object#toString()
	//	 */
	//	@Override
	//	public String toString() {
	//		return "HolderAccount [idHolderAccountPk=" + idHolderAccountPk
	//				+ ", accountDescription=" + accountDescription
	//				+ ", accountGroup=" + accountGroup + ", accountNumber="
	//				+ accountNumber + ", accountType=" + accountType
	//				+ ", alternateCode=" + alternateCode + ", closingDate="
	//				+ closingDate + ", participant=" + participant
	//				+ ", lastModifyApp=" + lastModifyApp + ", lastModifyDate="
	//				+ lastModifyDate + ", lastModifyIp=" + lastModifyIp
	//				+ ", lastModifyUser=" + lastModifyUser + ", openingDate="
	//				+ openingDate + ", registryDate=" + registryDate
	//				+ ", registryUser=" + registryUser + ", stateAccount="
	//				+ stateAccount + ", stateAccountName=" + stateAccountName
	//				+ ", selected=" + selected + ", holderAccountBanks="
	//				+ (Validations.validateIsNotNull(holderAccountBanks)?holderAccountBanks:null) + ", holderAccountDetails="
	//				+ (Validations.validateIsNotNull(holderAccountDetails)?holderAccountDetails:null);
	//	}
	//	
	//	/**
	//	 * To string only fields.
	//	 *
	//	 * @return the string
	//	 */
	//	public String toStringOnlyFields() {
	//		return "HolderAccount [idHolderAccountPk=" + idHolderAccountPk
	//				+ ", accountDescription=" + accountDescription
	//				+ ", accountGroup=" + accountGroup + ", accountNumber="
	//				+ accountNumber + ", accountType=" + accountType
	//				+ ", alternateCode=" + alternateCode + ", closingDate="
	//				+ closingDate + ", participant=" + participant
	//				+ ", lastModifyApp=" + lastModifyApp + ", lastModifyDate="
	//				+ lastModifyDate + ", lastModifyIp=" + lastModifyIp
	//				+ ", lastModifyUser=" + lastModifyUser + ", openingDate="
	//				+ openingDate + ", registryDate=" + registryDate
	//				+ ", registryUser=" + registryUser + ", stateAccount="
	//				+ stateAccount + ", stateAccountName=" + stateAccountName
	//				+ ", selected=" + selected;
	//	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO review casuisticas no contempladas para mejorar
		if (loggerUser != null) {
			lastModifyApp =  loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}

	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Gets the notification date.
	 *
	 * @return the notification date
	 */
	public Date getNotificationDate() {
		return notificationDate;
	}

	/**
	 * Sets the notification date.
	 *
	 * @param notificationDate the new notification date
	 */
	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}


	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the expedition place.
	 *
	 * @return the expedition place
	 */
	public GeographicLocation getExpeditionPlace() {
		return expeditionPlace;
	}

	/**
	 * Sets the expedition place.
	 *
	 * @param expeditionPlace the new expedition place
	 */
	public void setExpeditionPlace(GeographicLocation expeditionPlace) {
		this.expeditionPlace = expeditionPlace;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

}