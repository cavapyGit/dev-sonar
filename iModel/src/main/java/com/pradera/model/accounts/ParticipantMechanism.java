package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.MechanismModality;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PARTICIPANT_MECHANISM database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_MECHANISM")
@NamedQueries({
	@NamedQuery(name = ParticipantMechanism.PARTICIPANT_MECHAN_STATE, 
			query = "SELECT nvl(count(pm.idParticipantMechanismPk),0)  " +
					"FROM ParticipantMechanism pm " +
					"WHERE pm.participant.idParticipantPk=:idParticipantParam AND " +
					"	   pm.mechanismModality.id.idNegotiationMechanismPk=:idNegotiationMechanismParam AND" +
					" 	   pm.mechanismModality.id.idNegotiationModalityPk=:idNegotiationModalityParam AND " +
					"	   pm.stateParticipantMechanism = :idStateParam ")
})
public class ParticipantMechanism implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String PARTICIPANT_MECHAN_STATE= "partMechanism.State";

	/** The id participant mechanism pk. */
	@Id
	@SequenceGenerator(name="PARTICIPANT_MECHANISM_IDPARTICIPANTMECHANISMPK_GENERATOR", sequenceName="SQ_ID_PARTICIPANT_MECHANISM_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_MECHANISM_IDPARTICIPANTMECHANISMPK_GENERATOR")
	@Column(name="ID_PARTICIPANT_MECHANISM_PK")
	private Long idParticipantMechanismPk;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state participant mechanism. */
	@Column(name="STATE_PARTICIPANT_MECHANISM")
	private Integer stateParticipantMechanism;
	
	 @Column(name="IND_PURCHASE_ROLE")
	 private Integer indPurchaseRole;
	 
	 @Column(name="IND_SALE_ROLE")
	 private Integer indSaleRole;
	
	/** The mechanism modality. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK"),
		@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK")
		})
	private MechanismModality mechanismModality;

	//bi-directional many-to-one association to Participant
    /** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;

    /**
     * The Constructor.
     */
    public ParticipantMechanism() {
    }

	/**
	 * Gets the id participant mechanism pk.
	 *
	 * @return the id participant mechanism pk
	 */
	public Long getIdParticipantMechanismPk() {
		return idParticipantMechanismPk;
	}

	/**
	 * Sets the id participant mechanism pk.
	 *
	 * @param idParticipantMechanismPk the id participant mechanism pk
	 */
	public void setIdParticipantMechanismPk(Long idParticipantMechanismPk) {
		this.idParticipantMechanismPk = idParticipantMechanismPk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state participant mechanism.
	 *
	 * @return the state participant mechanism
	 */
	public Integer getStateParticipantMechanism() {
		return stateParticipantMechanism;
	}

	/**
	 * Sets the state participant mechanism.
	 *
	 * @param stateParticipantMechanism the state participant mechanism
	 */
	public void setStateParticipantMechanism(Integer stateParticipantMechanism) {
		this.stateParticipantMechanism = stateParticipantMechanism;
	}

	/**
	 * Gets the mechanism modality.
	 *
	 * @return the mechanism modality
	 */
	public MechanismModality getMechanismModality() {
		return mechanismModality;
	}

	/**
	 * Sets the mechanism modality.
	 *
	 * @param mechanismModality the mechanism modality
	 */
	public void setMechanismModality(MechanismModality mechanismModality) {
		this.mechanismModality = mechanismModality;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	
	
	public Integer getIndPurchaseRole() {
		return indPurchaseRole;
	}

	public void setIndPurchaseRole(Integer indPurchaseRole) {
		this.indPurchaseRole = indPurchaseRole;
	}

	public Integer getIndSaleRole() {
		return indSaleRole;
	}

	public void setIndSaleRole(Integer indSaleRole) {
		this.indSaleRole = indSaleRole;
	}

	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
	/**
	 * @param idParticipantMechanismPk
	 * @param stateParticipantMechanism
	 */
	public ParticipantMechanism(Long idParticipantMechanismPk,Integer stateParticipantMechanism) {
		this.idParticipantMechanismPk = idParticipantMechanismPk;
		this.stateParticipantMechanism = stateParticipantMechanism;
	}
}