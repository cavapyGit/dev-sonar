package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.Bank;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the INSTITUTION_BANK_ACCOUNT database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/12/2013
 */
@Entity
@Table(name="PARTICIPANT_BANK_ACCOUNT")
public class ParticipantBankAccount implements Serializable,Auditable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 360537844772101812L;

	/** The id part bank account pk. */
	@Id
	@SequenceGenerator(name="PART_BANK_ACCOUNT_GENERATOR", sequenceName="SQ_ID_PART_BANK_ACCOUNT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PART_BANK_ACCOUNT_GENERATOR")
	@NotNull
	@Column(name="ID_PART_BANK_ACCOUNT_PK")
	private Long idPartBankAccountPk;

	/** The bank. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BANK_FK")
	private Bank bank;

	/** The bank acount type. */
	@Column(name="BANK_ACOUNT_TYPE")
	private Integer bankAcountType;

	/** The currency. */
	@Column(name="CURRENCY")
	private Integer currency;

	/** The account number. */
	@Column(name="ACCOUNT_NUMBER")
	private String accountNumber;

	/** The account state. */
	@Column(name="ACCOUNT_STATE")
	private Integer accountState;

	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="IND_INSTITUTION_BANK_ACCOUNT")
	private Integer indInstitutionBankAccount;
	
	@Column(name="IND_HOLDER_ACCOUNT_BANK")
	private Integer indHolderAccountBank;
	
	/** The bank acount type description. */
	@Transient
	private String bankAcountTypeDescription;
	
	/** The currency description. */
	@Transient
	private String currencyDescription;
	

    /**
     * Instantiates a new participant bank account.
     */
    public ParticipantBankAccount() {
    }
    
    public String getIndInstitutionBankAccountDescription(){
    	if(Validations.validateIsNotNull(indInstitutionBankAccount)){
    		return BooleanType.get(indInstitutionBankAccount).getValue();
    	} else {
    		return null;
    	}
    }
    
    public String getIndHolderAccountBankDescription(){
    	if(Validations.validateIsNotNull(indHolderAccountBank)){
    		return BooleanType.get(indHolderAccountBank).getValue();
    	} else {
    		return null;
    	}
    }
    
    public String getCurrencyAndNumber(){
    	StringBuilder sbBuilder = new StringBuilder();
    	if(Validations.validateIsNotNull(currency)){
    		sbBuilder.append(currency);
    	}
    	
    	sbBuilder.append("-");
    	
    	if(Validations.validateIsNotNull(accountNumber)){
    		sbBuilder.append(accountNumber);
    	}
    	
    	return sbBuilder.toString();
    }


	
	/**
	 * Gets the id part bank account pk.
	 *
	 * @return the id part bank account pk
	 */
	public Long getIdPartBankAccountPk() {
		return idPartBankAccountPk;
	}



	/**
	 * Sets the id part bank account pk.
	 *
	 * @param idPartBankAccountPk the new id part bank account pk
	 */
	public void setIdPartBankAccountPk(Long idPartBankAccountPk) {
		this.idPartBankAccountPk = idPartBankAccountPk;
	}



	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public Bank getBank() {
		return bank;
	}



	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(Bank bank) {
		this.bank = bank;
	}



	/**
	 * Gets the bank acount type.
	 *
	 * @return the bank acount type
	 */
	public Integer getBankAcountType() {
		return bankAcountType;
	}



	/**
	 * Sets the bank acount type.
	 *
	 * @param bankAcountType the new bank acount type
	 */
	public void setBankAcountType(Integer bankAcountType) {
		this.bankAcountType = bankAcountType;
	}



	/**
	 * Gets the account state.
	 *
	 * @return the account state
	 */
	public Integer getAccountState() {
		return accountState;
	}



	/**
	 * Sets the account state.
	 *
	 * @param accountState the new account state
	 */
	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}



	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	
	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	
	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	
	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	
	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	
	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	
	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	

	/**
	 * Gets the bank acount type description.
	 *
	 * @return the bank acount type description
	 */
	public String getBankAcountTypeDescription() {
		return bankAcountTypeDescription;
	}



	/**
	 * Sets the bank acount type description.
	 *
	 * @param bankAcountTypeDescription the new bank acount type description
	 */
	public void setBankAcountTypeDescription(String bankAcountTypeDescription) {
		this.bankAcountTypeDescription = bankAcountTypeDescription;
	}



	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}



	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}		

	public Integer getIndInstitutionBankAccount() {
		return indInstitutionBankAccount;
	}

	public void setIndInstitutionBankAccount(Integer indInstitutionBankAccount) {
		this.indInstitutionBankAccount = indInstitutionBankAccount;
	}

	public Integer getIndHolderAccountBank() {
		return indHolderAccountBank;
	}

	public void setIndHolderAccountBank(Integer indHolderAccountBank) {
		this.indHolderAccountBank = indHolderAccountBank;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {		
        return null;
	}
}