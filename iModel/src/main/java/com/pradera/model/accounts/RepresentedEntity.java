package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.type.RepresentativeClassType;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class RepresentedEntity.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@Entity
@Table(name="REPRESENTED_ENTITY")
public class RepresentedEntity implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id represented entity pk. */
	@Id
	@SequenceGenerator(name="REPRESENTED_ENTITY_IDREPRESENTEDENTITYPK_GENERATOR", sequenceName="SQ_ID_REPRESENTED_ENTITY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPRESENTED_ENTITY_IDREPRESENTEDENTITYPK_GENERATOR")
	@Column(name="ID_REPRESENTED_ENTITY_PK")
	private Long idRepresentedEntityPk;

	/** The id holder fk. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;

	//bi-directional many-to-one association to Issuer
    /** The issuer. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK",referencedColumnName="ID_ISSUER_PK")
	private Issuer issuer;

	/** The id participant fk. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
    private Date registryDate;
	
	/** The representative class. */
	@Column(name="REPRESENTATIVE_CLASS")
	private Integer representativeClass;

	/** The state. */
	@Column(name="STATE_REPRESENTED")
	private Integer stateRepresented;

	//bi-directional many-to-one association to LegalRepresentative
	/** The legal representative. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LEGAL_REPRESENTATIVE_FK",referencedColumnName="ID_LEGAL_REPRESENTATIVE_PK")
	private LegalRepresentative legalRepresentative;

	/** The representative class description. */
	@Transient
	private String representativeClassDescription;
    /**
     * The Constructor.
     */
    public RepresentedEntity() {
    }
    
    /**
     * Gets the representative class description.
     *
     * @return the representative class description
     */
    public String getRepresentativeClassDescription(){
    	String representativeClassDescription = null;
    	if(representativeClass != null) {
    		representativeClassDescription = RepresentativeClassType.get(representativeClass).getValue();
    	}
    	return representativeClassDescription;
    }

	/**
	 * Gets the id represented entity pk.
	 *
	 * @return the id represented entity pk
	 */
	public Long getIdRepresentedEntityPk() {
		return this.idRepresentedEntityPk;
	}

	/**
	 * Sets the id represented entity pk.
	 *
	 * @param idRepresentedEntityPk the id represented entity pk
	 */
	public void setIdRepresentedEntityPk(Long idRepresentedEntityPk) {
		this.idRepresentedEntityPk = idRepresentedEntityPk;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the representative class.
	 *
	 * @return the representative class
	 */
	public Integer getRepresentativeClass() {
		return this.representativeClass;
	}

	/**
	 * Sets the representative class.
	 *
	 * @param representativeClass the representative class
	 */
	public void setRepresentativeClass(Integer representativeClass) {
		this.representativeClass = representativeClass;
	}

	
	/**
	 * Gets the state represented.
	 *
	 * @return the state represented
	 */
	public Integer getStateRepresented() {
		return stateRepresented;
	}

	/**
	 * Sets the state represented.
	 *
	 * @param stateRepresented the new state represented
	 */
	public void setStateRepresented(Integer stateRepresented) {
		this.stateRepresented = stateRepresented;
	}

	/**
	 * Gets the legal representative.
	 *
	 * @return the legal representative
	 */
	public LegalRepresentative getLegalRepresentative() {
		return this.legalRepresentative;
	}

	/**
	 * Sets the legal representative.
	 *
	 * @param legalRepresentative the legal representative
	 */
	public void setLegalRepresentative(LegalRepresentative legalRepresentative) {
		this.legalRepresentative = legalRepresentative;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	
	
	/**
	 * Sets the representative class description.
	 *
	 * @param representativeClassDescription the new representative class description
	 */
	public void setRepresentativeClassDescription(
			String representativeClassDescription) {
		this.representativeClassDescription = representativeClassDescription;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
        if (loggerUser != null) {
        	if(loggerUser.getIdPrivilegeOfSystem() != null) {
        		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        	} else {
        		lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
        	}
            if(loggerUser.getAuditTime() != null) {
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null){
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}