package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum UnableDocRepreNatType {
		
		POR(Integer.valueOf(298),"PODER OTORGADO COMO REPRESENTANTE",Integer.valueOf(188)),
		CE(Integer.valueOf(299),"CEDULA DE IDENTIDAD",Integer.valueOf(188)), 
		PAS(Integer.valueOf(300),"PASAPORTE",Integer.valueOf(188)),
		LIC(Integer.valueOf(301),"LICENCIA DE CONDUCIR",Integer.valueOf(188)),
		DIO(Integer.valueOf(302),"DOCUMENTO DE IDENTIDAD DE PAIS ORIGEN",Integer.valueOf(188)),
		RNC(Integer.valueOf(303),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(188)), 
		DJCP(Integer.valueOf(304),"DECLARACION JURADA SOBRE CONDICION DE PEP",Integer.valueOf(188)),
	    OC(Integer.valueOf(305),"ORDEN DE CUSTODIA",Integer.valueOf(188)),
		CATL(Integer.valueOf(306),"DOCUMENTO DE ACREDITACION COMO TUTOR LEGAL",Integer.valueOf(188)),
		CDRL(Integer.valueOf(307),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(188));	
		
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<UnableDocRepreNatType> list = new ArrayList<UnableDocRepreNatType>();
		public static final Map<Integer, UnableDocRepreNatType> lookup = new HashMap<Integer, UnableDocRepreNatType>();

		static {
			for (UnableDocRepreNatType s : EnumSet.allOf(UnableDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private UnableDocRepreNatType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static UnableDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
