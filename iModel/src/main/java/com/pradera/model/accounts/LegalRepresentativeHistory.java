package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeClassType;
import com.pradera.model.issuancesecuritie.IssuerRequest;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the LEGAL_REPRESENTATIVE_CREATION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/03/2013
 */
@Entity
@Table(name="LEGAL_REPRESENTATIVE_HISTORY")
public class LegalRepresentativeHistory implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id representative history pk. */
	@Id
	@SequenceGenerator(name="LEGAL_REPRESENTATIVE_HISTORY_IDREPREHISTORYPK_GENERATOR", sequenceName="SQ_ID_REPRE_HISTORY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LEGAL_REPRESENTATIVE_HISTORY_IDREPREHISTORYPK_GENERATOR")
	@Column(name="ID_REPRESENTATIVE_HISTORY_PK")
	private Long idRepresentativeHistoryPk;
    
	
	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	
	
	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	
	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private Integer economicActivity;

	
	/** The economic sector. */
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;

	/** The email. */	
	private String email;

	
	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;

	
	/** The first last name. */
	@Column(name="FIRST_LAST_NAME")	
	private String firstLastName;

	
	/** The holder request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_REQUEST_FK",referencedColumnName="ID_HOLDER_REQUEST_PK")
	private HolderRequest holderRequest;
	
	/** The participant request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_REQUEST_FK")
	private ParticipantRequest participantRequest;
	
	/** The issuer request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_REQUEST_FK")
	private IssuerRequest issuerRequest;
	
	/** The ind residence. */
	@Column(name="IND_RESIDENCE")
	private Integer indResidence;

	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    
	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	
	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	
	/** The legal address. */
	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;

	
	/** The legal department. */
	@Column(name="LEGAL_DEPARTMENT")
	private Integer legalDepartment;

	
	/** The legal district. */
	@Column(name="LEGAL_DISTRICT")
	private Integer legalDistrict;

	
	/** The legal province. */
	@Column(name="LEGAL_PROVINCE")
	private Integer legalProvince;

	
	/** The legal residence country. */
	@Column(name="LEGAL_RESIDENCE_COUNTRY")
	private Integer legalResidenceCountry;

	
	/** The mobile number. */
	@Column(name="MOBILE_NUMBER")
	private String mobileNumber;

	
	/** The name. */
	@Column(name="NAME")   	
	private String name;

	
	/** The nationality. */
	private Integer nationality;

	
	/** The person type. */
	@Column(name="PERSON_TYPE")
	private Integer personType;

	
	/** The home phone number. */
	@Column(name="HOME_PHONE_NUMBER")
	private String homePhoneNumber;
	
	
	/** The office phone number. */
	@Column(name="OFFICE_PHONE_NUMBER")
	private String officePhoneNumber;

	
	/** The postal address. */
	@Column(name="POSTAL_ADDRESS")
	private String postalAddress;

	
	/** The postal department. */
	@Column(name="POSTAL_DEPARTMENT")
	private Integer postalDepartment;

	
	/** The postal district. */
	@Column(name="POSTAL_DISTRICT")
	private Integer postalDistrict;

	
	/** The postal province. */
	@Column(name="POSTAL_PROVINCE")
	private Integer postalProvince;

	
	/** The postal residence country. */
	@Column(name="POSTAL_RESIDENCE_COUNTRY")
	private Integer postalResidenceCountry;

	
	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	
	/** The representative description. */
	@Column(name="REPRESENTATIVE_DESCRIPTION")
	private String representativeDescription;

	
	/** The second document number. */
	@Column(name="SECOND_DOCUMENT_NUMBER")
	private String secondDocumentNumber;

	
	/** The second document type. */
	@Column(name="SECOND_DOCUMENT_TYPE")
	private Integer secondDocumentType;

	
	/** The second last name. */
	@Column(name="SECOND_LAST_NAME")
	private String secondLastName;


	/** The second nationality. */
	@Column(name="SECOND_NATIONALITY")
	private Integer secondNationality;

	
	/** The State repre history. */
	@Column(name="STATE_REPRE_HISTORY")
	private  Integer stateRepreHistory;
	
	
	/** The representative class. */
	@Column(name="REPRESENTATIVE_CLASS")
	private Integer representativeClass;
	
	/** The birth date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="BIRTH_DATE")	
	private Date birthDate;
	
	
	/** The sex. */
	@Column(name="SEX")
	private Integer sex;
	
	
	/** The full name. */
	@Column(name="FULL_NAME")	
	private String fullName;
	
	
	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;
	
	
	/** The repre history type. */
	@Column(name="REPRE_HISTORY_TYPE")
	private Integer repreHistoryType;
	
	
	/** The ind pep. */
	@Column(name="IND_PEP")
	private Integer indPEP;
	
	
	/** The category. */
	@Column(name="CATEGORY")
    private Integer category;
	
	
	/** The role. */
	@Column(name="ROLE")
	private Integer role;
	
	
	/** The comments. */
	@Column(name="COMMENTS")
	private String comments;
	
	/** The beginning period. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="BEGINNING_PERIOD")
	private Date  beginningPeriod;
	
	/** The ending period. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ENDING_PERIOD")
	private Date endingPeriod;
	
	@Column(name="DOCUMENT_SOURCE")
	private Integer documentSource;
	
	@Column(name="DOCUMENT_ISSUANCE_DATE")
	private Integer documentIssuanceDate;
	
	@Column(name="AUTHORITY_LEGAL_NUMBER")
    private String authorityLegalNumber;
	
	/** The representative type description. */
	@Transient
	private String representativeTypeDescription;
	
	/** The document type description. */
	@Transient
	private String documentTypeDescription;
	
	/** The participant description. */
	@Transient
	private String representativeDescriptionAux;

	/** The representative class description. */
	@Transient
	private String representativeClassDescription;
	
	/** The represente file history. */
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="legalRepresentativeHistory")
	private List<RepresentativeFileHistory> representeFileHistory;
	
	@Transient
	private boolean flagLegalImport;
	
	@Transient
	private boolean flagActiveFilesByHolderImport;

	@Transient
	private String columnsValid;
	
	
   	public String getColumnsValid() {
   		columnsValid =  "LegalRepresentativeHistory [documentNumber=" + documentNumber
				+ ", representativeTypeDescription="
				+ representativeTypeDescription + ", documentTypeDescription="
				+ documentTypeDescription + ", representativeDescriptionAux="
				+ representativeDescriptionAux
				+ ", representativeClassDescription="
				+ representativeClassDescription + "]";
   		return columnsValid;
	}
   	
	public void setColumnsValid(String columnsValid) {
		this.columnsValid = columnsValid;
	}



	/**
     * Instantiates a new legal representative history.
     */
    public LegalRepresentativeHistory() {
    	setIndPEP(BooleanType.NO.getCode());
    }

    /**
     * Gets the person type description.
     *
     * @return the person type description
     */
    public String getPersonTypeDescription(){
    	String personTypeDescription = null;
    	if(personType != null) {
    		personTypeDescription = PersonType.get(personType).getValue();
    	}
    	return personTypeDescription;
    }
    
    /**
     * Gets the representative class description.
     *
     * @return the representative class description
     */
    public String getRepresentativeClassDescription(){
    	this.representativeClassDescription = null;
    	if(representativeClass != null) {
    		representativeClassDescription = RepresentativeClassType.get(representativeClass).getValue();
    	}
    	return representativeClassDescription;
    }
    
    /**
     * Gets the document type description.
     *
     * @return the document type description
     */
    public String getDocumentTypeDescription() {
    	String documentTypeDescription = null;
    	if(documentType != null) {
    		documentTypeDescription = DocumentType.get(documentType).getValue();
    	}
    	return documentTypeDescription;
    }
    
    /**
     * Gets the document type and number.
     *
     * @return the document type and number
     */
    public String getDocumentTypeAndNumber(){
    	if(documentNumber != null && getDocumentTypeDescription() != null){
    		StringBuilder sb = new StringBuilder();
    		sb.append(getDocumentTypeDescription());
    		sb.append("-");
    		sb.append(documentNumber);
    		return sb.toString();
    	}
    	return null;
    }
	
	/**
	 * Gets the id representative history pk.
	 *
	 * @return the id representative history pk
	 */
	public Long getIdRepresentativeHistoryPk() {
		return idRepresentativeHistoryPk;
	}


	/**
	 * Sets the id representative history pk.
	 *
	 * @param idRepresentativeHistoryPk the new id representative history pk
	 */
	public void setIdRepresentativeHistoryPk(Long idRepresentativeHistoryPk) {
		this.idRepresentativeHistoryPk = idRepresentativeHistoryPk;
	}


	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return this.documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public Integer getEconomicActivity() {
		return this.economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the new economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return this.economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return this.faxNumber;
	}

	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the new fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Gets the first last name.
	 *
	 * @return the first last name
	 */
	public String getFirstLastName() {
		return this.firstLastName;
	}

	/**
	 * Sets the first last name.
	 *
	 * @param firstLastName the new first last name
	 */
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	

	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return holderRequest;
	}



	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}



	/**
	 * Gets the ind residence.
	 *
	 * @return the ind residence
	 */
	public Integer getIndResidence() {
		return this.indResidence;
	}

	/**
	 * Sets the ind residence.
	 *
	 * @param indResidence the new ind residence
	 */
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the legal address.
	 *
	 * @return the legal address
	 */
	public String getLegalAddress() {
		return this.legalAddress;
	}

	/**
	 * Sets the legal address.
	 *
	 * @param legalAddress the new legal address
	 */
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	/**
	 * Gets the legal department.
	 *
	 * @return the legal department
	 */
	public Integer getLegalDepartment() {
		return this.legalDepartment;
	}

	/**
	 * Sets the legal department.
	 *
	 * @param legalDepartment the new legal department
	 */
	public void setLegalDepartment(Integer legalDepartment) {
		this.legalDepartment = legalDepartment;
	}

	/**
	 * Gets the legal district.
	 *
	 * @return the legal district
	 */
	public Integer getLegalDistrict() {
		return this.legalDistrict;
	}

	/**
	 * Sets the legal district.
	 *
	 * @param legalDistrict the new legal district
	 */
	public void setLegalDistrict(Integer legalDistrict) {
		this.legalDistrict = legalDistrict;
	}

	/**
	 * Gets the legal province.
	 *
	 * @return the legal province
	 */
	public Integer getLegalProvince() {
		return this.legalProvince;
	}

	/**
	 * Sets the legal province.
	 *
	 * @param legalProvince the new legal province
	 */
	public void setLegalProvince(Integer legalProvince) {
		this.legalProvince = legalProvince;
	}

	/**
	 * Gets the legal residence country.
	 *
	 * @return the legal residence country
	 */
	public Integer getLegalResidenceCountry() {
		return this.legalResidenceCountry;
	}

	/**
	 * Sets the legal residence country.
	 *
	 * @param legalResidenceCountry the new legal residence country
	 */
	public void setLegalResidenceCountry(Integer legalResidenceCountry) {
		this.legalResidenceCountry = legalResidenceCountry;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return this.mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the nationality.
	 *
	 * @return the nationality
	 */
	public Integer getNationality() {
		return this.nationality;
	}

	/**
	 * Sets the nationality.
	 *
	 * @param nationality the new nationality
	 */
	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	/**
	 * Gets the person type.
	 *
	 * @return the person type
	 */
	public Integer getPersonType() {
		return this.personType;
	}

	/**
	 * Sets the person type.
	 *
	 * @param personType the new person type
	 */
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	

	/**
	 * Gets the home phone number.
	 *
	 * @return the home phone number
	 */
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}



	/**
	 * Sets the home phone number.
	 *
	 * @param homePhoneNumber the new home phone number
	 */
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}



	/**
	 * Gets the office phone number.
	 *
	 * @return the office phone number
	 */
	public String getOfficePhoneNumber() {
		return officePhoneNumber;
	}



	/**
	 * Sets the office phone number.
	 *
	 * @param officePhoneNumber the new office phone number
	 */
	public void setOfficePhoneNumber(String officePhoneNumber) {
		this.officePhoneNumber = officePhoneNumber;
	}



	/**
	 * Gets the postal address.
	 *
	 * @return the postal address
	 */
	public String getPostalAddress() {
		return this.postalAddress;
	}

	/**
	 * Sets the postal address.
	 *
	 * @param postalAddress the new postal address
	 */
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	/**
	 * Gets the postal department.
	 *
	 * @return the postal department
	 */
	public Integer getPostalDepartment() {
		return this.postalDepartment;
	}

	/**
	 * Sets the postal department.
	 *
	 * @param postalDepartment the new postal department
	 */
	public void setPostalDepartment(Integer postalDepartment) {
		this.postalDepartment = postalDepartment;
	}

	/**
	 * Gets the postal district.
	 *
	 * @return the postal district
	 */
	public Integer getPostalDistrict() {
		return this.postalDistrict;
	}

	/**
	 * Sets the postal district.
	 *
	 * @param postalDistrict the new postal district
	 */
	public void setPostalDistrict(Integer postalDistrict) {
		this.postalDistrict = postalDistrict;
	}

	/**
	 * Gets the postal province.
	 *
	 * @return the postal province
	 */
	public Integer getPostalProvince() {
		return this.postalProvince;
	}

	/**
	 * Sets the postal province.
	 *
	 * @param postalProvince the new postal province
	 */
	public void setPostalProvince(Integer postalProvince) {
		this.postalProvince = postalProvince;
	}

	/**
	 * Gets the postal residence country.
	 *
	 * @return the postal residence country
	 */
	public Integer getPostalResidenceCountry() {
		return this.postalResidenceCountry;
	}

	/**
	 * Sets the postal residence country.
	 *
	 * @param postalResidenceCountry the new postal residence country
	 */
	public void setPostalResidenceCountry(Integer postalResidenceCountry) {
		this.postalResidenceCountry = postalResidenceCountry;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the representative description.
	 *
	 * @return the representative description
	 */
	public String getRepresentativeDescription() {
		return this.representativeDescription;
	}

	/**
	 * Sets the representative description.
	 *
	 * @param representativeDescription the new representative description
	 */
	public void setRepresentativeDescription(String representativeDescription) {
		this.representativeDescription = representativeDescription;
	}

	/**
	 * Gets the second document number.
	 *
	 * @return the second document number
	 */
	public String getSecondDocumentNumber() {
		return this.secondDocumentNumber;
	}

	/**
	 * Sets the second document number.
	 *
	 * @param secondDocumentNumber the new second document number
	 */
	public void setSecondDocumentNumber(String secondDocumentNumber) {
		this.secondDocumentNumber = secondDocumentNumber;
	}

	/**
	 * Gets the second document type.
	 *
	 * @return the second document type
	 */
	public Integer getSecondDocumentType() {
		return this.secondDocumentType;
	}

	/**
	 * Sets the second document type.
	 *
	 * @param secondDocumentType the new second document type
	 */
	public void setSecondDocumentType(Integer secondDocumentType) {
		this.secondDocumentType = secondDocumentType;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return this.secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
	 * Gets the second nationality.
	 *
	 * @return the second nationality
	 */
	public Integer getSecondNationality() {
		return this.secondNationality;
	}

	/**
	 * Sets the second nationality.
	 *
	 * @param secondNationality the new second nationality
	 */
	public void setSecondNationality(Integer secondNationality) {
		this.secondNationality = secondNationality;
	}

  	

	/**
	 * Gets the state repre history.
	 *
	 * @return the state repre history
	 */
	public Integer getStateRepreHistory() {
		return stateRepreHistory;
	}

	/**
	 * Sets the state repre history.
	 *
	 * @param stateRepreHistory the new state repre history
	 */
	public void setStateRepreHistory(Integer stateRepreHistory) {
		this.stateRepreHistory = stateRepreHistory;
	}

	/**
	 * Gets the representative type description.
	 *
	 * @return the representative type description
	 */
	public String getRepresentativeTypeDescription() {		
		return PersonType.get(personType).getValue();		
	}

	/**
	 * Sets the representative type description.
	 *
	 * @param representativeTypeDescription the new representative type description
	 */
	public void setRepresentativeTypeDescription(String representativeTypeDescription){
		this.representativeTypeDescription = representativeTypeDescription;
	}
	
	



	/**
	 * Sets the representative class description.
	 *
	 * @param representativeClassDescription the new representative class description
	 */
	public void setRepresentativeClassDescription(
			String representativeClassDescription) {
		this.representativeClassDescription = representativeClassDescription;
	}

	/**
 * Gets the representative class.
 *
 * @return the representative class
 */
public Integer getRepresentativeClass() {
		return representativeClass;
	}

	/**
	 * Sets the representative class.
	 *
	 * @param representativeClass the new representative class
	 */
	public void setRepresentativeClass(Integer representativeClass) {
		this.representativeClass = representativeClass;
	}

	/**
	 * Gets the birth date.
	 *
	 * @return the birth date
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * Sets the birth date.
	 *
	 * @param birthDate the new birth date
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Gets the sex.
	 *
	 * @return the sex
	 */
	public Integer getSex() {
		return sex;
	}

	/**
	 * Sets the sex.
	 *
	 * @param sex the new sex
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return registryType;
	}



	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}



	/**
	 * Gets the repre history type.
	 *
	 * @return the repre history type
	 */
	public Integer getRepreHistoryType() {
		return repreHistoryType;
	}



	/**
	 * Sets the repre history type.
	 *
	 * @param repreHistoryType the new repre history type
	 */
	public void setRepreHistoryType(Integer repreHistoryType) {
		this.repreHistoryType = repreHistoryType;
	}



	/**
	 * Gets the ind pep.
	 *
	 * @return the ind pep
	 */
	public Integer getIndPEP() {
		return indPEP;
	}



	/**
	 * Sets the ind pep.
	 *
	 * @param indPEP the new ind pep
	 */
	public void setIndPEP(Integer indPEP) {
		this.indPEP = indPEP;
	}



	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public Integer getCategory() {
		return category;
	}



	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(Integer category) {
		this.category = category;
	}



	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}



	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}



	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}



	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}



	/**
	 * Gets the beginning period.
	 *
	 * @return the beginning period
	 */
	public Date getBeginningPeriod() {
		return beginningPeriod;
	}



	/**
	 * Sets the beginning period.
	 *
	 * @param beginningPeriod the new beginning period
	 */
	public void setBeginningPeriod(Date beginningPeriod) {
		this.beginningPeriod = beginningPeriod;
	}



	/**
	 * Gets the ending period.
	 *
	 * @return the ending period
	 */
	public Date getEndingPeriod() {
		return endingPeriod;
	}



	/**
	 * Sets the ending period.
	 *
	 * @param endingPeriod the new ending period
	 */
	public void setEndingPeriod(Date endingPeriod) {
		this.endingPeriod = endingPeriod;
	}



	/**
	 * Gets the represente file history.
	 *
	 * @return the represente file history
	 */
	public List<RepresentativeFileHistory> getRepresenteFileHistory() {
		return representeFileHistory;
	}



	/**
	 * Sets the represente file history.
	 *
	 * @param representeFileHistory the new represente file history
	 */
	public void setRepresenteFileHistory(
			List<RepresentativeFileHistory> representeFileHistory) {
		this.representeFileHistory = representeFileHistory;
	}


	public Integer getDocumentSource() {
		return documentSource;
	}

	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}

	/**
	 * Sets the document type description.
	 *
	 * @param documentTypeDescription the new document type description
	 */
	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}
	
	
	/**
	 * Gets the representative description aux.
	 *
	 * @return the representative description aux
	 */
	public String getRepresentativeDescriptionAux() {
		
		if(personType.equals(PersonType.NATURAL.getCode())){
			if(secondLastName != null){
			representativeDescriptionAux = firstLastName+" "+secondLastName+","+name;
			}
			else{
			representativeDescriptionAux = firstLastName+","+name;					
			}
		}
		else if(personType.equals(PersonType.JURIDIC.getCode())){
			representativeDescriptionAux = fullName;
		}
		
		return representativeDescriptionAux;
	}

	/**
	 * Sets the representative description aux.
	 *
	 * @param representativeDescriptionAux the new representative description aux
	 */
	public void setRepresentativeDescriptionAux(String representativeDescriptionAux) {
		this.representativeDescriptionAux = representativeDescriptionAux;
	}	

	/**
	 * Gets the participant request.
	 *
	 * @return the participant request
	 */
	public ParticipantRequest getParticipantRequest() {
		return participantRequest;
	}

	/**
	 * Sets the participant request.
	 *
	 * @param participantRequest the new participant request
	 */
	public void setParticipantRequest(ParticipantRequest participantRequest) {
		this.participantRequest = participantRequest;
	}

	/**
	 * Gets the issuer request.
	 *
	 * @return the issuer request
	 */
	public IssuerRequest getIssuerRequest() {
		return issuerRequest;
	}

	/**
	 * Sets the issuer request.
	 *
	 * @param issuerRequest the new issuer request
	 */
	public void setIssuerRequest(IssuerRequest issuerRequest) {
		this.issuerRequest = issuerRequest;
	}
	
	

	public boolean isFlagLegalImport() {
		return flagLegalImport;
	}

	public void setFlagLegalImport(boolean flagLegalImport) {
		this.flagLegalImport = flagLegalImport;
	}

	public boolean isFlagActiveFilesByHolderImport() {
		return flagActiveFilesByHolderImport;
	}

	public void setFlagActiveFilesByHolderImport(
			boolean flagActiveFilesByHolderImport) {
		this.flagActiveFilesByHolderImport = flagActiveFilesByHolderImport;
	}
	
	

	public Integer getDocumentIssuanceDate() {
		return documentIssuanceDate;
	}

	public void setDocumentIssuanceDate(Integer documentIssuanceDate) {
		this.documentIssuanceDate = documentIssuanceDate;
	}
	
	

	public String getAuthorityLegalNumber() {
		return authorityLegalNumber;
	}

	public void setAuthorityLegalNumber(String authorityLegalNumber) {
		this.authorityLegalNumber = authorityLegalNumber;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();				
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null){
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
            if(loggerUser.getAuditTime() != null){
            	lastModifyDate = loggerUser.getAuditTime();
			}  else{
				lastModifyDate = objLoggerUser.getAuditTime();
			}
            if( loggerUser.getIpAddress() != null){
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null){
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();       
        detailsMap.put("representeFileHistory",representeFileHistory);     
        return detailsMap;
		
		
	}
	
	



	

	
	
	

}