package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeMajorDomOrForeignResidentDocHolderNatType {
		
			
		CE(Integer.valueOf(869),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(870),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(871),"PASAPORTE"), 
		LIC(Integer.valueOf(872),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(873),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(874),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(875),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(876),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(877),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(878),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(879),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(880),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(881),"DECRETO"),		
	    RCF(Integer.valueOf(882),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(883),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(884),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    		
		private Integer code;
		private String value;
		
		

		public static final List<AgeMajorDomOrForeignResidentDocHolderNatType> list = new ArrayList<AgeMajorDomOrForeignResidentDocHolderNatType>();
		public static final Map<Integer, AgeMajorDomOrForeignResidentDocHolderNatType> lookup = new HashMap<Integer, AgeMajorDomOrForeignResidentDocHolderNatType>();

		static {
			for (AgeMajorDomOrForeignResidentDocHolderNatType s : EnumSet.allOf(AgeMajorDomOrForeignResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeMajorDomOrForeignResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeMajorDomOrForeignResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
