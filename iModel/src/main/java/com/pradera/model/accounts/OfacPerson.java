package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.type.PersonType;


/**
 * The persistent class for the OFAC_PERSON database table.
 * 
 */
@Entity
@Table(name="OFAC_PERSON")
public class OfacPerson implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	//@SequenceGenerator(name="OFAC_PERSON_IDOFACPERSONPK_GENERATOR", sequenceName="SQ_ID_OFAC_PERSON_PK",initialValue=1,allocationSize=1)
	//@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OFAC_PERSON_IDOFACPERSONPK_GENERATOR")
	@Column(name="ID_OFAC_PERSON_PK")
	private Long idOfacPersonPk;

	@Column(name="COMMENTS")
	private String comment;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OFAC_CODE")
	private String ofacCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="RESIDENCE_COUNTRY")
	private String residenceCountry;

	@Column(name="PERSON_TYPE")
	private Integer personType;
	
	@Column(name="FULL_NAME")
	private String fullName;

	@Transient
	private String entityDescription;
	
	@Transient
	private String description;



	public String getDescription() {
		StringBuilder sb = new StringBuilder();
		if(Validations.validateIsNotNull(firstName)){
			sb.append(firstName);
			sb.append(" ");			
		}
		if(Validations.validateIsNotNull(lastName)){
			sb.append(lastName);
		}
		if(Validations.validateIsNotNull(fullName)){
			sb.append(fullName);
		}
		description = sb.toString();
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OfacPerson() {
	}

	public String getEntityDescription() {
		if(personType!=null){
			entityDescription = PersonType.get(personType).getValue();
		}
		return entityDescription;
	}

	public void setEntityDescription(String entityDescription) {
		this.entityDescription = entityDescription;
	}


	public Long getIdOfacPersonPk() {
		return this.idOfacPersonPk;
	}

	public void setIdOfacPersonPk(Long idOfacPersonPk) {
		this.idOfacPersonPk = idOfacPersonPk;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getOfacCode() {
		return this.ofacCode;
	}

	public void setOfacCode(String ofacCode) {
		this.ofacCode = ofacCode;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public String getResidenceCountry() {
		return this.residenceCountry;
	}

	public void setResidenceCountry(String residenceCountry) {
		this.residenceCountry = residenceCountry;
	}

	private OfacPerson(String ofacCode) {
		super();
		this.ofacCode = ofacCode;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO review casuisticas no contempladas para mejorar
		if (loggerUser != null) {
			lastModifyApp =  loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}

	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Integer getPersonType() {
		return personType;
	}

	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

}