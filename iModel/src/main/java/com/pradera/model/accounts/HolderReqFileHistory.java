package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class HolderReqFileHistory.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@Entity
@Table(name="HOLDER_REQ_FILE_HISTORY")
public class HolderReqFileHistory implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder req file his pk. */
	@Id
	@SequenceGenerator(name="HOLDER_REQ_FILE_H_IDHOLDERFILEPK_GENERATOR", sequenceName="SQ_ID_HOLDER_REQ_FILE_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_REQ_FILE_H_IDHOLDERFILEPK_GENERATOR")
	@Column(name="ID_HOLDER_REQ_FILE_HIS_PK")   
	private Long idHolderReqFileHisPk;

	/** The description. */
	private String description;

    /** The file holder req. */
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_FILE")
	private byte[] fileHolderReq;

	/** The filename. */
	private String filename;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state file. */
	@Column(name="STATE_FILE")
	private Integer stateFile;
	
	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;
	
	/** The holder req file his type. */
	@Column(name="HOLDER_REQ_FILE_HIS_TYPE")
	private Integer holderReqFileHisType;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;
	
	//bi-directional many-to-one association to HolderRequest
    /** The holder request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_REQUEST_FK",referencedColumnName="ID_HOLDER_REQUEST_PK")
	private HolderRequest holderRequest;
    
	/** The was modified. */
	@Transient
	private boolean wasModified;
	
	/** The columns valid. */
	@Transient
	private String columnsValid;
	
    /**
     * Instantiates a new holder req file history.
     */
    public HolderReqFileHistory() {
    }

	
    
    /**
     * Gets the columns valid.
     *
     * @return the columns valid
     */
    public String getColumnsValid() {
    	columnsValid = "HolderReqFileHistory [description=" + description
				+ ", filename=" + filename + "]";
    	return columnsValid;
	}

    /**
     * Sets the columns valid.
     *
     * @param columnsValid the new columns valid
     */
    public void setColumnsValid(String columnsValid) {
		this.columnsValid = columnsValid;
	}

    


	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return this.filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return this.holderRequest;
	}

	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}



	/**
	 * Gets the id holder req file his pk.
	 *
	 * @return the id holder req file his pk
	 */
	public Long getIdHolderReqFileHisPk() {
		return idHolderReqFileHisPk;
	}



	/**
	 * Sets the id holder req file his pk.
	 *
	 * @param idHolderReqFileHisPk the new id holder req file his pk
	 */
	public void setIdHolderReqFileHisPk(Long idHolderReqFileHisPk) {
		this.idHolderReqFileHisPk = idHolderReqFileHisPk;
	}



	/**
	 * Gets the file holder req.
	 *
	 * @return the file holder req
	 */
	public byte[] getFileHolderReq() {
		return fileHolderReq;
	}



	/**
	 * Sets the file holder req.
	 *
	 * @param fileHolderReq the new file holder req
	 */
	public void setFileHolderReq(byte[] fileHolderReq) {
		this.fileHolderReq = fileHolderReq;
	}



	/**
	 * Gets the state file.
	 *
	 * @return the state file
	 */
	public Integer getStateFile() {
		return stateFile;
	}



	/**
	 * Sets the state file.
	 *
	 * @param stateFile the new state file
	 */
	public void setStateFile(Integer stateFile) {
		this.stateFile = stateFile;
	}



	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return registryType;
	}



	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}



	/**
	 * Gets the holder req file his type.
	 *
	 * @return the holder req file his type
	 */
	public Integer getHolderReqFileHisType() {
		return holderReqFileHisType;
	}



	/**
	 * Sets the holder req file his type.
	 *
	 * @param holderReqFileHisType the new holder req file his type
	 */
	public void setHolderReqFileHisType(Integer holderReqFileHisType) {
		this.holderReqFileHisType = holderReqFileHisType;
	}



	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}



	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	
	

    /**
     * Checks if is was modified.
     *
     * @return true, if is was modified
     */
    public boolean isWasModified() {
		return wasModified;
	}

	/**
	 * Sets the was modified.
	 *
	 * @param wasModified the new was modified
	 */
	public void setWasModified(boolean wasModified) {
		this.wasModified = wasModified;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
        if (loggerUser != null) {
        	if(loggerUser.getIdPrivilegeOfSystem() != null){
        		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        	} else {
        		lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
        	}
            if(loggerUser.getAuditTime() != null){
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null){
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }



	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
}