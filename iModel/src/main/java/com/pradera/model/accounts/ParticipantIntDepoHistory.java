package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PARTICIPANT_INT_DEPO_HISTORY database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/04/2013
 */
@Entity
@Table(name="PARTICIPANT_INT_DEPO_HISTORY")
public class ParticipantIntDepoHistory implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id particip int hist pk. */
	@Id
	@SequenceGenerator(name="PARTICIPANT_INT_DEPO_HISTORY_ID_PARTICIP_INT_HIST_PK_GENERATOR", sequenceName="SQ_ID_PARTICIP_INT_HIST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_INT_DEPO_HISTORY_ID_PARTICIP_INT_HIST_PK_GENERATOR")
	@Column(name="ID_PARTICIP_INT_HIST_PK")
	private Long idParticipIntHistPk;	

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state int depositary. */
	@Column(name="STATE_INT_DEPOSITARY")
	private Integer stateIntDepositary;
	
	/** The ind new. */
	@Column(name="IND_NEW")    
    private Integer indNew;
	
	/** The international depository. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERNATIONAL_DEPOSITORY_FK", referencedColumnName="ID_INTERNATIONAL_DEPOSITORY_PK")
	private InternationalDepository internationalDepository;
	
	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;
	
	/** The participant request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_REQUEST_FK", referencedColumnName="ID_PARTICIPANT_REQUEST_PK")
	private ParticipantRequest participantRequest;


	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}



	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}



	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}



	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}



	/**
	 * Gets the state int depositary.
	 *
	 * @return the state int depositary
	 */
	public Integer getStateIntDepositary() {
		return stateIntDepositary;
	}



	/**
	 * Sets the state int depositary.
	 *
	 * @param stateIntDepositary the state int depositary
	 */
	public void setStateIntDepositary(Integer stateIntDepositary) {
		this.stateIntDepositary = stateIntDepositary;
	}



	/**
	 * Gets the international depository.
	 *
	 * @return the international depository
	 */
	public InternationalDepository getInternationalDepository() {
		return internationalDepository;
	}



	/**
	 * Sets the international depository.
	 *
	 * @param internationalDepository the international depository
	 */
	public void setInternationalDepository(
			InternationalDepository internationalDepository) {
		this.internationalDepository = internationalDepository;
	}



	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}



	/**
	 * Sets the participant.
	 *
	 * @param participant the participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}


	/**
	 * Gets the participant request.
	 *
	 * @return the participant request
	 */
	public ParticipantRequest getParticipantRequest() {
		return participantRequest;
	}



	/**
	 * Sets the participant request.
	 *
	 * @param participantRequest the participant request
	 */
	public void setParticipantRequest(ParticipantRequest participantRequest) {
		this.participantRequest = participantRequest;
	}



	/**
	 * Gets the id particip int hist pk.
	 *
	 * @return the id particip int hist pk
	 */
	public Long getIdParticipIntHistPk() {
		return idParticipIntHistPk;
	}



	/**
	 * Sets the id particip int hist pk.
	 *
	 * @param idParticipIntHistPk the new id particip int hist pk
	 */
	public void setIdParticipIntHistPk(Long idParticipIntHistPk) {
		this.idParticipIntHistPk = idParticipIntHistPk;
	}		
		
	
	/**
	 * Gets the ind new.
	 *
	 * @return the ind new
	 */
	public Integer getIndNew() {
		return indNew;
	}



	/**
	 * Sets the ind new.
	 *
	 * @param indNew the new ind new
	 */
	public void setIndNew(Integer indNew) {
		this.indNew = indNew;
	}



	/**
	 * Sets the audit.
	 *
	 * @param loggerUser the audit
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}