package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantFileType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public enum ParticipantFileForeignType {

	/** The certificationvalidity. */
	CERTIFICATIONVALIDITY(Integer.valueOf(17),"CERTIFICADO DE VIGENCIA"),

	/** The bylawsfile. */
	BYLAWSFILE(Integer.valueOf(18),"ESTATUTOS SOCIALES VIGENTES"),

	/** The shareholderslistfile. */
	SHAREHOLDERSLISTFILE(Integer.valueOf(19),"LISTADO ACCIONISTAS DE LA SOCIEDAD"),

	/** The certificationadmcouncil. */
	CERTIFICATIONADMCOUNCIL(Integer.valueOf(20),"CERTIFICACIÓN DEL CONSEJO DE ADMINISTRACIÓN."),	

	/** The representativecertificationfile. */
	REPRESENTATIVECERTIFICATIONFILE(Integer.valueOf(21),"CERTIFICACIÓN REPRESENTANTE"),

	/** The representativepassportfile. */
	REPRESENTATIVEPASSPORTFILE(Integer.valueOf(22),"CÉDULA O PASAPORTE REPRESENTANTE"),

	/** The certificationauthorityfile. */
	CERTIFICATIONAUTHORITYFILE(Integer.valueOf(23),"CERTIFICACIÓN DE AUTORIZACIÓN PARA OPERAR.(EXCEPTO BCRD,MH)");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant file type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ParticipantFileForeignType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<ParticipantFileForeignType> list = new ArrayList<ParticipantFileForeignType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantFileForeignType> lookup = new HashMap<Integer, ParticipantFileForeignType>();
	static {
		for (ParticipantFileForeignType s : EnumSet.allOf(ParticipantFileForeignType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant file foreign type
	 */
	public static ParticipantFileForeignType get(Integer code) {
		return lookup.get(code);
	}

}
