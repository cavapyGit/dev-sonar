package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum StateHistoryStateHolderType {
	
	REGISTERED(Integer.valueOf(528),"REGISTRADO"),
	BLOCKED(Integer.valueOf(529),"BLOQUEADO"),
	TRANSFERED(Integer.valueOf(1374),"TRANSFERIDO");
	
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private StateHistoryStateHolderType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static StateHistoryStateHolderType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<StateHistoryStateHolderType> list = new ArrayList<StateHistoryStateHolderType>();
	public static final Map<Integer, StateHistoryStateHolderType> lookup = new HashMap<Integer, StateHistoryStateHolderType>();
	static {
		for (StateHistoryStateHolderType s : EnumSet.allOf(StateHistoryStateHolderType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
