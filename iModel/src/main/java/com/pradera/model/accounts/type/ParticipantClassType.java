package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum ParticipantClassType {
	
	AGENCIAS_BOLSA(Integer.valueOf(28),"AGENCIAS DE BOLSA"),
	BANCO_CENTRAL_DE_BOLIVIA(Integer.valueOf(30),"BANCO CENTRAL DE BOLIVIA"),
	SOCIEDADES_ADMINISTRADORAS_DE_FONDOS_DE_INVERSION(Integer.valueOf(31),"SOCIEDADES ADMINISTRADORAS DE FONDOS DE INVERSION"),
	BANCOS_COMERCIALES(Integer.valueOf(32),"BANCOS COMERCIALES"),
	FONDOS_FINANCIEROS_PRIVADOS(Integer.valueOf(33),"FONDOS FINANCIEROS PRIVADOS"),
	MUTUALES(Integer.valueOf(34),"MUTUALES"),
	COOPERATIVAS(Integer.valueOf(35),"COOPERATIVAS"),
	AFP(Integer.valueOf(36),"ADMINISTRADORAS DE FONDOS DE PENSIONES"),
	ASEGURADORAS(Integer.valueOf(263),"ASEGURADORAS"),
	ENTIDADES_EXTRANJERAS_CUSTODIA_DE_VALORES(Integer.valueOf(264),"ENTIDADES EXTRANJERAS QUE OFREZCAN SERVICIOS DE CUSTODIA DE VALORES"),
	ENTIDADES_DE_DEPOSITO_DE_VALORES(Integer.valueOf(1961),"ENTIDADES DE DEPOSITO DE VALORES");
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private ParticipantClassType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<ParticipantClassType> list = new ArrayList<ParticipantClassType>();
	public static final Map<Integer, ParticipantClassType> lookup = new HashMap<Integer, ParticipantClassType>();
	static {
		for (ParticipantClassType s : EnumSet.allOf(ParticipantClassType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ParticipantClassType get(Integer code) {
		return lookup.get(code);
	}

}
