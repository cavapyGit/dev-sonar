package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum HolderBlockMotiveType {
	
	DECEASE(Integer.valueOf(506),"FALLECIMIENTO"),
	ASFI_REQUESTER(Integer.valueOf(507),"A SOLICITUD DE CNV"),
	WARRANT(Integer.valueOf(508),"POR ORDEN JUDICIAL"),
	EDV_REQUESTER(Integer.valueOf(509),"A SOLICITUD DE CAVAPY"),
	PARTICIPANT_REQUESTER(Integer.valueOf(1772),"A SOLICITUD DEL PARTICIPANTE"),
	OTHERS_MOTIVES(Integer.valueOf(1773),"OTROS MOTIVOS"),
	TITULAR_REQUESTER(Integer.valueOf(2807),"A SOLICITUD DEL TITULAR"),
	DEPOSITOR_REQUESTER(Integer.valueOf(2806),"A SOLICITUD DEL DEPOSITANTE");
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private HolderBlockMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static HolderBlockMotiveType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<HolderBlockMotiveType> list = new ArrayList<HolderBlockMotiveType>();
	public static final Map<Integer, HolderBlockMotiveType> lookup = new HashMap<Integer, HolderBlockMotiveType>();
	static {
		for (HolderBlockMotiveType s : EnumSet.allOf(HolderBlockMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
