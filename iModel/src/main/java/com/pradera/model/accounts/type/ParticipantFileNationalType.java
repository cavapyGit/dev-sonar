package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantFileType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public enum ParticipantFileNationalType {
	
	/** The commercialregisterfile. */
	COMMERCIALREGISTERFILE(Integer.valueOf(9),"CERTIFICADO DE REGISTRO MERCANTIL"),

	/** The bylawsfile. */
	BYLAWSFILE(Integer.valueOf(10),"ESTATUTOS SOCIALES VIGENTES"),

	/** The shareholderslistfile. */
	SHAREHOLDERSLISTFILE(Integer.valueOf(11),"LISTADO ACCIONISTAS DE LA SOCIEDAD"),

	/** The generalassemblyfile. */
	GENERALASSEMBLYFILE(Integer.valueOf(12),"ACTA ASAMBLEA GENERAL ACCIONISTAS"),

	/** The registrationrncfile. */
	REGISTRATIONRNCFILE(Integer.valueOf(13),"CONSTANCIA INSCRIPCIÓN RNC"),

	/** The representativecertificationfile. */
	REPRESENTATIVECERTIFICATIONFILE(Integer.valueOf(14),"CERTIFICACIÓN REPRESENTANTE"),

	/** The representativepassportfile. */
	REPRESENTATIVEPASSPORTFILE(Integer.valueOf(15),"CÉDULA O PASAPORTE REPRESENTANTE"),
	
	/** The certificationauthorityfile. */
	CERTIFICATIONAUTHORITYFILE(Integer.valueOf(16),"CERTIFICACIÓN DE AUTORIZACIÓN PARA OPERAR.(EXCEPTO BCRD,MH)");		
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant file type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ParticipantFileNationalType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<ParticipantFileNationalType> list = new ArrayList<ParticipantFileNationalType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantFileNationalType> lookup = new HashMap<Integer, ParticipantFileNationalType>();
	static {
		for (ParticipantFileNationalType s : EnumSet.allOf(ParticipantFileNationalType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant file national type
	 */
	public static ParticipantFileNationalType get(Integer code) {
		return lookup.get(code);
	}

}
