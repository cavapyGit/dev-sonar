package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum UnableDomNoResidentDocHolderNatType {
		
		CE(Integer.valueOf(981),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(982),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(983),"PASAPORTE"), 
		LIC(Integer.valueOf(984),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(985),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(986),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(987),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(988),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(989),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(990),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(991),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(992),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(993),"DECRETO"),		
	    RCF(Integer.valueOf(994),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(995),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(996),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    private Integer code;
		private String value;
		
		public static final List<UnableDomNoResidentDocHolderNatType> list = new ArrayList<UnableDomNoResidentDocHolderNatType>();
		public static final Map<Integer, UnableDomNoResidentDocHolderNatType> lookup = new HashMap<Integer, UnableDomNoResidentDocHolderNatType>();

		static {
			for (UnableDomNoResidentDocHolderNatType s : EnumSet.allOf(UnableDomNoResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private UnableDomNoResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static UnableDomNoResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
