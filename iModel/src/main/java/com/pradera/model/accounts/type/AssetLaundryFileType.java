package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AssetLaundryFileType {

	OFAC(new Integer(460),"OFAC"),
	PNA_PEP(new Integer(459),"PEP_PNA");

	private String value;
	private Integer code;

	public static final List<AssetLaundryFileType> list= new ArrayList<AssetLaundryFileType>();
	public static final Map<Integer, AssetLaundryFileType> lookup = new HashMap<Integer,AssetLaundryFileType >();

	static{
		for(AssetLaundryFileType type: EnumSet.allOf(AssetLaundryFileType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	private AssetLaundryFileType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static AssetLaundryFileType get(Integer code)
	{
		return lookup.get(code);
	}


}
