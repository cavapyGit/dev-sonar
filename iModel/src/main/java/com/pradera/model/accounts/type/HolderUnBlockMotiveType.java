package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum HolderUnBlockMotiveType {
	PARTICIPANT_REQUESTER(Integer.valueOf(1774),"A SOLICITUD DEL PARTICIPANTE"),
	OTHERS_MOTIVES(Integer.valueOf(1775),"OTROS MOTIVOS"),
	DECEASE(Integer.valueOf(510),"FALLECIMIENTO"),
	ASFI_REQUESTER(Integer.valueOf(511),"A SOLICITUD DE CNV"),	
	WARRANT(Integer.valueOf(512),"POR ORDEN JUDICIAL"),
	EDV_REQUESTER(Integer.valueOf(513),"A SOLICITUD DE CAVAPY"),
	TITULAR_REQUESTER(Integer.valueOf(2809),"A SOLICITUD DEL TITULAR"),
	DEPOSITOR_REQUESTER(Integer.valueOf(2808),"A SOLICITUD DEL DEPOSITANTE");
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private HolderUnBlockMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static HolderUnBlockMotiveType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<HolderUnBlockMotiveType> list = new ArrayList<HolderUnBlockMotiveType>();
	public static final Map<Integer, HolderUnBlockMotiveType> lookup = new HashMap<Integer, HolderUnBlockMotiveType>();
	static {
		for (HolderUnBlockMotiveType s : EnumSet.allOf(HolderUnBlockMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
