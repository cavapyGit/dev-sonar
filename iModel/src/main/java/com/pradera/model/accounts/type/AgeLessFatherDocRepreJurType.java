package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeLessFatherDocRepreJurType {
		
		
		RNC(Integer.valueOf(362),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(195)),
		CDRL(Integer.valueOf(363),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(195)),	
		CRMF(Integer.valueOf(364),"CERTIFICADO DEL REGISTRO MERCANTIL DEL FIDUCIARIO",Integer.valueOf(195)), 
		DEFF(Integer.valueOf(365),"DOCUMENTO DE EJERCICIO DE FUNCIONES DEL FIDUCIARIO",Integer.valueOf(195));
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<AgeLessFatherDocRepreJurType> list = new ArrayList<AgeLessFatherDocRepreJurType>();
		public static final Map<Integer, AgeLessFatherDocRepreJurType> lookup = new HashMap<Integer, AgeLessFatherDocRepreJurType>();

		static {
			for (AgeLessFatherDocRepreJurType s : EnumSet.allOf(AgeLessFatherDocRepreJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeLessFatherDocRepreJurType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeLessFatherDocRepreJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
