package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeLessNationalOrForeignResidentDocHolderNatType {
		

		CI(Integer.valueOf(917),"FOTOCOPIA CEDULA DE IDENTIDAD"),
		DIO(Integer.valueOf(921),"FOTOCOPIA DEL DOCUMENTO DE IDENTIDAD DE PAIS DE ORIGEN"),
		CDN(Integer.valueOf(922),"CERTIFICADO DE NACIMIENTO DE TITULAR A QUIEN REPRESENTA CDN (ORIGINAL O FOTOCOPIA)"),
		FCASC(Integer.valueOf(924),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(925),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(926),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES");	
	    	
	    		
		private Integer code;
		private String value;
		
		public static final List<AgeLessNationalOrForeignResidentDocHolderNatType> list = new ArrayList<AgeLessNationalOrForeignResidentDocHolderNatType>();
		public static final Map<Integer, AgeLessNationalOrForeignResidentDocHolderNatType> lookup = new HashMap<Integer, AgeLessNationalOrForeignResidentDocHolderNatType>();

		static {
			for (AgeLessNationalOrForeignResidentDocHolderNatType s : EnumSet.allOf(AgeLessNationalOrForeignResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeLessNationalOrForeignResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeLessNationalOrForeignResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
