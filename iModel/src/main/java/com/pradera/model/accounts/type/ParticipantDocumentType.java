package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantDocumentType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public enum ParticipantDocumentType {
	
	/** The rnc. */
	NIT(Integer.valueOf(386),"NIT"),
	
	/** The dio. */
	DIO(Integer.valueOf(390),"DIO");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;		
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant document type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ParticipantDocumentType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<ParticipantDocumentType> list = new ArrayList<ParticipantDocumentType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantDocumentType> lookup = new HashMap<Integer, ParticipantDocumentType>();
	static {
		for (ParticipantDocumentType s : EnumSet.allOf(ParticipantDocumentType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant document type
	 */
	public static ParticipantDocumentType get(Integer code) {
		return lookup.get(code);
	}

}
