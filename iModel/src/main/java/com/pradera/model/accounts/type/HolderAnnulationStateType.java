package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
public enum HolderAnnulationStateType {
	
	// ESTADOS DE UNA SOLICITUD DE ANULACION DE CUI
	REGISTERED(Integer.valueOf(2484),"REGISTRADO"),
	APPROVED(Integer.valueOf(2485),"APROBADO"),
	ANNULATED(Integer.valueOf(2486),"ANULADO"),
	REVIEWED(Integer.valueOf(2487),"REVISADO"),
	CONFIRMED(Integer.valueOf(2488),"CONFIRMADO"),
	REJECTED(Integer.valueOf(2489),"RECHAZADO"),
	
	;
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new holder state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private HolderAnnulationStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the holder state type
	 */
	public static HolderAnnulationStateType get(Integer code) {
		return lookup.get(code);
	}

	/** The Constant list. */
	public static final List<HolderAnnulationStateType> list = new ArrayList<HolderAnnulationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAnnulationStateType> lookup = new HashMap<Integer, HolderAnnulationStateType>();
	static {
		for (HolderAnnulationStateType s : EnumSet.allOf(HolderAnnulationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}