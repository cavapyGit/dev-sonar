package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AnnularMotivesHolderRequestType {
		
		DATA_WONG(Integer.valueOf(484),"DOCUMENTACION INSUFICIENTE O INCORRECTA"), 
		LACK_SUPPORT_DOCUMENTS_ATTACHED(Integer.valueOf(485),"DATOS CARGADOS INCORRECTOS"), 
		OTHER_MOTIVES(Integer.valueOf(486),"OTROS");
		
		
		private Integer code;
		private String value;
		

		public static final List<AnnularMotivesHolderRequestType> list = new ArrayList<AnnularMotivesHolderRequestType>();
		public static final Map<Integer, AnnularMotivesHolderRequestType> lookup = new HashMap<Integer, AnnularMotivesHolderRequestType>();

		static {
			for (AnnularMotivesHolderRequestType s : EnumSet.allOf(AnnularMotivesHolderRequestType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AnnularMotivesHolderRequestType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AnnularMotivesHolderRequestType get(Integer code) {
			return lookup.get(code);
		}

		
}
