package com.pradera.model.accounts;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantIntDepositaryPK.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@Embeddable
public class ParticipantIntDepositaryPK implements Serializable {
	//default serial version id, required for serializable classes.
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant pk. */
	@Column(name="ID_PARTICIPANT_PK")
	private Long idParticipantPk;

	/** The id international depository pk. */
	@Column(name="ID_INTERNATIONAL_DEPOSITORY_PK")
	private Long idInternationalDepositoryPk;

    /**
     * The Constructor.
     */
    public ParticipantIntDepositaryPK() {
    }

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}


	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}


	/**
	 * Gets the id international depository pk.
	 *
	 * @return the id international depository pk
	 */
	public Long getIdInternationalDepositoryPk() {
		return idInternationalDepositoryPk;
	}


	/**
	 * Sets the id international depository pk.
	 *
	 * @param idInternationalDepositoryPk the id international depository pk
	 */
	public void setIdInternationalDepositoryPk(Long idInternationalDepositoryPk) {
		this.idInternationalDepositoryPk = idInternationalDepositoryPk;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ParticipantIntDepositaryPK)) {
			return false;
		}
		ParticipantIntDepositaryPK castOther = (ParticipantIntDepositaryPK)other;
		return 
			(this.idParticipantPk.longValue() == castOther.idParticipantPk.longValue())
			&& (this.idInternationalDepositoryPk.longValue() == castOther.idInternationalDepositoryPk.longValue());

    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idParticipantPk ^ (this.idParticipantPk >>> 32)));
		hash = hash * prime + ((int) (this.idInternationalDepositoryPk ^ (this.idInternationalDepositoryPk >>> 32)));
		
		return hash;
    }
}