package com.pradera.model.accounts.holderaccounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.Holder;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT_DET_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
@Entity
@Table(name="HOLDER_ACCOUNT_DET_REQUEST")
public class HolderAccountDetRequest implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder accounts request detail pk. */
	@Id
	@SequenceGenerator(name="SQ_HOLD_ACCOUNT_REQ_DET_PK", sequenceName="SQ_ID_HOLD_ACCOUNT_REQ_DET_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLD_ACCOUNT_REQ_DET_PK")
	@Column(name="ID_HOLDER_ACC_DET_REQ_PK")  
	private Long idHolderAccReqDetHysPk;
	
	/** The holder account request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_REQ_FK")
	private HolderAccountRequest holderAccountRequest;	
	
	/** The holder. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	/** The ind representative. */
	@Column(name="IND_REPRESENTATIVE")
	private Integer indRepresentative;
	
	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;        
    
	/** The have old or new. */
	@Column(name="IND_OLD_NEW")
	private Integer indOldNew;
	
	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

    /**
     * Instantiates a new holder account req det hy.
     */
    public HolderAccountDetRequest() {
    }

	/**
	 * Gets the id holder acc req det hys pk.
	 *
	 * @return the id holder acc req det hys pk
	 */
	public Long getIdHolderAccReqDetHysPk() {
		return this.idHolderAccReqDetHysPk;
	}

	/**
	 * Sets the id holder acc req det hys pk.
	 *
	 * @param idHolderAccReqDetHysPk the new id holder acc req det hys pk
	 */
	public void setIdHolderAccReqDetHysPk(Long idHolderAccReqDetHysPk) {
		this.idHolderAccReqDetHysPk = idHolderAccReqDetHysPk;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param idHolderFk the new holder
	 */
	public void setHolder(Holder idHolderFk) {
		this.holder = idHolderFk;
	}

	/**
	 * Gets the ind representative.
	 *
	 * @return the ind representative
	 */
	public Integer getIndRepresentative() {
		return indRepresentative;
	}
	
	/**
	 * Checks if is representative.
	 *
	 * @return true, if is representative
	 */
	public boolean isRepresentative() {
		return Validations.validateIsNullOrEmpty(this.getIndRepresentative())?false:this.getIndRepresentative()==1?true:false;
	}

	/**
	 * Sets the ind representative.
	 *
	 * @param indRepresentative the new ind representative
	 */
	public void setIndRepresentative(Integer indRepresentative) {
		this.indRepresentative = indRepresentative;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the holder account request.
	 *
	 * @return the holder account request 
	 */
	public HolderAccountRequest getHolderAccountRequest() {
		return holderAccountRequest;
	}

	/**
	 * Sets the holder account request.
	 *
	 * @param holderAccountRequest the new holder account request
	 */
	public void setHolderAccountRequest(HolderAccountRequest holderAccountRequest) {
		this.holderAccountRequest = holderAccountRequest;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
            if(loggerUser.getAuditTime() != null) {
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null) {
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Gets the old or new.
	 *
	 * @return the old or new
	 */
	public Integer getIndOldNew() {
		return indOldNew;
	}

	/**
	 * Sets the old or new.
	 *
	 * @param indOldNew the new old or new
	 */
	public void setIndOldNew(Integer indOldNew) {
		this.indOldNew = indOldNew;
	}	
	
}