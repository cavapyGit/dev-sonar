package com.pradera.model.accounts.holderaccounts;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.constants.Constants;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.GeographicLocation;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the BANK database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03/03/2013
 */
@NamedQueries({
	@NamedQuery(name = Bank.BAN_LIST_BY_STATE , query = "Select distinct b From Bank b WHERE b.state = :state order by b.description asc"),
	@NamedQuery(name = Bank.BANK_REQUEST_STATE, query = "SELECT new com.pradera.model.accounts.holderaccounts.Bank(I.idBankPk,I.state) From Bank I WHERE I.idBankPk = :idBankPkParam")
})
@Entity
@Table(name = "BANK")
public class Bank implements Serializable,Auditable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -965976616989120316L;
	
	/** The Constant BAN_LIST_BY_STATE. */
	public final static String BAN_LIST_BY_STATE = "Bank.searchBanksByState";

	/** The Constant BANK_REQUEST_STATE. */
	public static final String  BANK_REQUEST_STATE= "Bank.searchBankState";
	
	/** The id bank pk. */
	@Id
	@SequenceGenerator(name = "BANK_IDBANKPK_GENERATOR", sequenceName = "SQ_BANK_IDBANKPK_GENERATOR", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BANK_IDBANKPK_GENERATOR")
	@Column(name="ID_BANK_PK")
	private Long idBankPk;

	/** The description. */
	@Column(name="DESCRIPTION")
	private String description;

	/** The state. */
	@Column(name="STATE")
	private Integer state;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Long documentType;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	/** The bank type. */
	@Column(name="BANK_TYPE")
	private Integer bankType;

	/** The bank origin. */
	@Column(name="BANK_ORIGIN")
	private Integer bankOrigin;

	/** The bic code. */
	@Column(name="BIC_CODE")
	private String bicCode;

	/** The country. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "COUNTRY")
	private GeographicLocation country;

	/** The department. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "DEPARTMENT")
	private GeographicLocation department;

	/** The province. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "PROVINCE")
	private GeographicLocation province;

	/** The district. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "DISTRICT")
	private GeographicLocation district;

	/** The address. */
	@Column(name="ADDRESS")
	private String address;

	/** The mnemonic. */
	@Column(name="MNEMONIC")
	private String mnemonic;

	/** The phone number. */
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;

	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;

	/** The contact name. */
	@Column(name="CONTACT_NAME")
	private String contactName;

	/** The contact phone number. */
	@Column(name="CONTACT_PHONE_NUMBER")
	private String contactPhoneNumber;

	/** The contact email. */
	@Column(name="CONTACT_EMAIL")
	private String contactEmail;

	/** The observations. */
	@Column(name="COMMENTS")
	private String observations;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	/** The website. */
	@Column(name="WEBSITE")
	private String website;

	/** The aba rtn code. */
	@Column(name="CODE_ABA_RTN")
	private String abaRtnCode;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BLOCK_DATE")
	private Date blockDate;

	/** The reject user. */
	@Column(name = "BLOCK_USER")
	private String blockUser;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UNBLOCK_DATE")
	private Date unblockDate;

	/** The reject user. */
	@Column(name = "UNBLOCK_USER")
	private String unblockUser;
	
	/** The reject motive. */
	@Column(name="BLOCK_MOTIVE")
	private Integer blockMotive;
	
	/** The annulment other motive. */
	@Column(name = "BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/** The reject motive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/** The annulment other motive. */
	@Column(name = "UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;
	
	@Column(name="ID_BCB_CODE")
	private Long IdBcbCode;
	
	/** The holder account req bank hy. */
	@OneToMany(mappedBy="bank",cascade = CascadeType.ALL)
	private List<HolderAccountBankRequest> holderAccountReqBankHy;

	/** The holder account bank. */
	@OneToMany(mappedBy="bank",cascade = CascadeType.ALL)
	private List<HolderAccountBank> holderAccountBank;

	/** The clien bank. */
	@OneToMany(mappedBy="bank",cascade = CascadeType.ALL)
	private List<InstitutionBankAccount> bank;

	/** The provider bank. */
	@OneToMany(mappedBy="providerBank",cascade = CascadeType.ALL)
	private List<InstitutionBankAccount> providerBank;

	/** The funds operation. */
	@OneToMany(mappedBy="bank")
	private List<FundsOperation> fundsOperation;

	/** The state Description. */
	@Transient
	private String stateDescription;
	
	/** The document Type Description. */
	@Transient
	private String documentTypeDescription;
	
	/** The bank Type Description. */
	@Transient
	private String bankTypeDescription;
    /**
     * Instantiates a new bank.
     *
     * @param idBankPk the id bank pk
     */
    public Bank(Long idBankPk) {
    	this.idBankPk = idBankPk;
    }
    
    /**
     * Instantiates a new bank.
     */
    public Bank(){}

	public Bank(Long idBankPk, Integer state) {
		this.idBankPk = idBankPk;
		this.state = state;
	}

	/**
	 * Gets the id bank pk.
	 *
	 * @return the id bank pk
	 */
	public Long getIdBankPk() {
		return idBankPk;
	}

	/**
	 * Sets the id bank pk.
	 *
	 * @param idBankPk the new id bank pk
	 */
	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Long getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Long documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}

	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}


	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public GeographicLocation getCountry() {
		return country;
	}


	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(GeographicLocation country) {
		this.country = country;
	}


	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public GeographicLocation getDepartment() {
		return department;
	}


	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(GeographicLocation department) {
		this.department = department;
	}


	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public GeographicLocation getProvince() {
		return province;
	}


	/**
	 * Sets the province.
	 *
	 * @param province the new province
	 */
	public void setProvince(GeographicLocation province) {
		this.province = province;
	}


	/**
	 * Gets the district.
	 *
	 * @return the district
	 */
	public GeographicLocation getDistrict() {
		return district;
	}


	/**
	 * Sets the district.
	 *
	 * @param district the new district
	 */
	public void setDistrict(GeographicLocation district) {
		this.district = district;
	}


	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}


	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}


	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}


	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}


	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}


	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber the new phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return faxNumber;
	}


	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the new fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}


	/**
	 * Gets the contact name.
	 *
	 * @return the contact name
	 */
	public String getContactName() {
		return contactName;
	}


	/**
	 * Sets the contact name.
	 *
	 * @param contactName the new contact name
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	/**
	 * Gets the contact phone number.
	 *
	 * @return the contact phone number
	 */
	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}


	/**
	 * Sets the contact phone number.
	 *
	 * @param contactPhoneNumber the new contact phone number
	 */
	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}


	/**
	 * Gets the contact email.
	 *
	 * @return the contact email
	 */
	public String getContactEmail() {
		return contactEmail;
	}


	/**
	 * Sets the contact email.
	 *
	 * @param contactEmail the new contact email
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}


	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}


	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}


	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}


	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}


	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}


	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}


	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}


	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}


	/**
	 * Gets the holder account req bank hy.
	 *
	 * @return the holder account req bank hy
	 */
	public List<HolderAccountBankRequest> getHolderAccountReqBankHy() {
		return holderAccountReqBankHy;
	}


	/**
	 * Sets the holder account req bank hy.
	 *
	 * @param holderAccountReqBankHy the new holder account req bank hy
	 */
	public void setHolderAccountReqBankHy(
			List<HolderAccountBankRequest> holderAccountReqBankHy) {
		this.holderAccountReqBankHy = holderAccountReqBankHy;
	}


	/**
	 * Gets the holder account bank.
	 *
	 * @return the holder account bank
	 */
	public List<HolderAccountBank> getHolderAccountBank() {
		return holderAccountBank;
	}


	/**
	 * Sets the holder account bank.
	 *
	 * @param holderAccountBank the new holder account bank
	 */
	public void setHolderAccountBank(List<HolderAccountBank> holderAccountBank) {
		this.holderAccountBank = holderAccountBank;
	}





	public List<InstitutionBankAccount> getBank() {
		return bank;
	}

	public void setBank(List<InstitutionBankAccount> bank) {
		this.bank = bank;
	}

	/**
	 * Gets the provider bank.
	 *
	 * @return the provider bank
	 */
	public List<InstitutionBankAccount> getProviderBank() {
		return providerBank;
	}


	/**
	 * Sets the provider bank.
	 *
	 * @param providerBank the new provider bank
	 */
	public void setProviderBank(List<InstitutionBankAccount> providerBank) {
		this.providerBank = providerBank;
	}

	/**
	 * Gets the funds operation.
	 *
	 * @return the funds operation
	 */
	public List<FundsOperation> getFundsOperation() {
		return fundsOperation;
	}

	/**
	 * Sets the funds operation.
	 *
	 * @param fundsOperation the new funds operation
	 */
	public void setFundsOperation(List<FundsOperation> fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * Sets the website.
	 *
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("providerBank", (List<? extends Auditable>) providerBank);
		detailsMap.put("bank", (List<? extends Auditable>) bank);
		detailsMap.put("holderAccountBank", holderAccountBank);
		detailsMap.put("holderAccountReqBankHy", holderAccountReqBankHy);
        return detailsMap;
	}

	/**
	 * Gets the bank type.
	 *
	 * @return the bank type
	 */
	public Integer getBankType() {
		return bankType;
	}

	/**
	 * Sets the bank type.
	 *
	 * @param bankType the new bank type
	 */
	public void setBankType(Integer bankType) {
		this.bankType = bankType;
	}

	/**
	 * Gets the bank origin.
	 *
	 * @return the bank origin
	 */
	public Integer getBankOrigin() {
		return bankOrigin;
	}

	/**
	 * Sets the bank origin.
	 *
	 * @param bankOrigin the new bank origin
	 */
	public void setBankOrigin(Integer bankOrigin) {
		this.bankOrigin = bankOrigin;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the aba rtn code.
	 *
	 * @return the aba rtn code
	 */
	public String getAbaRtnCode() {
		return abaRtnCode;
	}

	/**
	 * Sets the aba rtn code.
	 *
	 * @param abaRtnCode the new aba rtn code
	 */
	public void setAbaRtnCode(String abaRtnCode) {
		this.abaRtnCode = abaRtnCode;
	}

	/**
	 * Get State Description
	 * @return
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Set  State Description
	 * @param stateDescription  State Description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Get Document Type Description
	 * @return Document Type Description
	 */
	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}

	/**
	 * Set Document Type Description
	 * @param documentTypeDescription Document Type Description
	 */
	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	public Date getBlockDate() {
		return blockDate;
	}

	public void setBlockDate(Date blockDate) {
		this.blockDate = blockDate;
	}

	public String getBlockUser() {
		return blockUser;
	}

	public void setBlockUser(String blockUser) {
		this.blockUser = blockUser;
	}

	public Date getUnblockDate() {
		return unblockDate;
	}

	public void setUnblockDate(Date unblockDate) {
		this.unblockDate = unblockDate;
	}

	public String getUnblockUser() {
		return unblockUser;
	}

	public void setUnblockUser(String unblockUser) {
		this.unblockUser = unblockUser;
	}

	public Integer getBlockMotive() {
		return blockMotive;
	}

	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}

	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}

	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}

	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}

	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}

	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}

	public String getBankTypeDescription() {
		return bankTypeDescription;
	}

	public void setBankTypeDescription(String bankTypeDescription) {
		this.bankTypeDescription = bankTypeDescription;
	}
	
	public String getDisplayMnemonicDescription(){
		String displayMnemonicCode="";
		if(Validations.validateIsNotNull(mnemonic) && Validations.validateIsNotNull(description)){
			StringBuffer sb = new StringBuffer();
    		sb.append(mnemonic).append(Constants.DASH).append(description);
    		displayMnemonicCode =  sb.toString();
		}
		return displayMnemonicCode;
	}

	public Long getIdBcbCode() {
		return IdBcbCode;
	}

	public void setIdBcbCode(Long idBcbCode) {
		IdBcbCode = idBcbCode;
	}


}