package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * The Enum ParticipantBlockUnblockRequestType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 13/03/2013
 */
public enum ParticipantRequestStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(1),"REGISTRADO"),
	
	/** The rejected. */
	REJECTED(Integer.valueOf(4),"RECHAZADO"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(5),"CONFIRMADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ParticipantRequestStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<ParticipantRequestStateType> list = new ArrayList<ParticipantRequestStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantRequestStateType> lookup = new HashMap<Integer, ParticipantRequestStateType>();
	static {
		for (ParticipantRequestStateType s : EnumSet.allOf(ParticipantRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant block unblock request type
	 */
	public static ParticipantRequestStateType get(Integer code) {
		return lookup.get(code);
	}

}
