package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum ForeignNoResidentDocHolderJurType {
		
		CE(Integer.valueOf(1029),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(1030),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(1031),"PASAPORTE"), 
		LIC(Integer.valueOf(1032),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(1033),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(1034),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(1035),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(1036),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(1037),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(1038),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(1039),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(1040),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(1041),"DECRETO"),		
	    RCF(Integer.valueOf(1042),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(1043),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(1044),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    private Integer code;
		private String value;
		
		public static final List<ForeignNoResidentDocHolderJurType> list = new ArrayList<ForeignNoResidentDocHolderJurType>();
		public static final Map<Integer, ForeignNoResidentDocHolderJurType> lookup = new HashMap<Integer, ForeignNoResidentDocHolderJurType>();

		static {
			for (ForeignNoResidentDocHolderJurType s : EnumSet.allOf(ForeignNoResidentDocHolderJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private ForeignNoResidentDocHolderJurType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static ForeignNoResidentDocHolderJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
