package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum JuridicDocRepreNatType {
		
		POR(Integer.valueOf(308),"PODER OTORGADO COMO REPRESENTANTE",Integer.valueOf(189)),
		CE(Integer.valueOf(309),"CEDULA DE IDENTIDAD",Integer.valueOf(189)), 
		PAS(Integer.valueOf(310),"PASAPORTE",Integer.valueOf(189)),
		LIC(Integer.valueOf(311),"LICENCIA DE CONDUCIR",Integer.valueOf(189)),
		DIO(Integer.valueOf(312),"DOCUMENTO DE IDENTIDAD DE PAIS ORIGEN",Integer.valueOf(189)),
		RNC(Integer.valueOf(313),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(189)), 
		DJCP(Integer.valueOf(314),"DECLARACION JURADA SOBRE CONDICION DE PEP",Integer.valueOf(189)),
	    OC(Integer.valueOf(315),"ORDEN DE CUSTODIA",Integer.valueOf(189)),
		CATL(Integer.valueOf(316),"DOCUMENTO DE ACREDITACION COMO TUTOR LEGAL",Integer.valueOf(189)),
		CDRL(Integer.valueOf(317),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(189));	
		
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<JuridicDocRepreNatType> list = new ArrayList<JuridicDocRepreNatType>();
		public static final Map<Integer, JuridicDocRepreNatType> lookup = new HashMap<Integer, JuridicDocRepreNatType>();

		static {
			for (JuridicDocRepreNatType s : EnumSet.allOf(JuridicDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private JuridicDocRepreNatType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static JuridicDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
