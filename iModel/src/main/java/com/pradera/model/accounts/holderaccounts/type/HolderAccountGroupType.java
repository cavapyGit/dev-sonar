/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public enum HolderAccountGroupType {
	ISSUER(new Integer(461),"EMISOR"),
	INVERSTOR(new Integer(462),"INVERSIONISTA");
	
	private Integer code;
	private String value;
	public static final List<HolderAccountGroupType> list = new ArrayList<HolderAccountGroupType>();
	public static final Map<Integer, HolderAccountGroupType> lookup = new HashMap<Integer, HolderAccountGroupType>();
	public static List<HolderAccountGroupType> listSomeElements(HolderAccountGroupType... transferSecuritiesTypeParams){
		List<HolderAccountGroupType> retorno = new ArrayList<HolderAccountGroupType>();
		for(HolderAccountGroupType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountGroupType s : EnumSet.allOf(HolderAccountGroupType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private HolderAccountGroupType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	public static HolderAccountGroupType get(Integer codigo) {
		return lookup.get(codigo);
	}
}