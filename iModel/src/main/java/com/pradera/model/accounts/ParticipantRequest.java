package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionBankAccountHistory;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PARTICIPANT_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 13/03/2013
 */
@Entity
@Table(name="PARTICIPANT_REQUEST")
public class ParticipantRequest implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant request pk. */
	@Id
	@SequenceGenerator(name="PARTICIPANT_REQUEST_IDPARTICIPANTREQUESTPK_GENERATOR", sequenceName="SQ_ID_PARTICIPANT_REQUEST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_REQUEST_IDPARTICIPANTREQUESTPK_GENERATOR")
	@Column(name="ID_PARTICIPANT_REQUEST_PK")
	private Long idParticipantRequestPk;

    /** The confirmation date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="CONFIRMATION_DATE")
	private Date confirmationDate;

	/** The confirmation user. */
	@Column(name="CONFIRMATION_USER")
	private String confirmationUser;	

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The observations. */
	@Column(name="COMMENTS")
	private String observations;

    /** The registry date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The reject other motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;

    /** The rejected date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REJECTED_DATE")
	private Date rejectedDate;

	/** The rejected motive. */
	@Column(name="REJECTED_MOTIVE")
	private Integer rejectedMotive;

	/** The rejected user. */
	@Column(name="REJECTED_USER")
	private String rejectedUser;

	/** The request motive. */
	@Column(name="REQUEST_MOTIVE")
	private Integer requestMotive;

	/** The request number. */
	@Column(name="REQUEST_NUMBER")
	private Long requestNumber;

	/** The request type. */
	@Column(name="REQUEST_TYPE")
	private Integer requestType;

	/** The state request. */
	@Column(name="STATE_REQUEST")
	private Integer stateRequest;
	
	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;
	
	/** The participant history states. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participantRequest")
	private List<ParticipantHistoryState> participantHistoryStates;
	
	/** The participant files. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participantRequest")
	private List<ParticipantFile> participantFiles;
	
	/** The participant int depo histories. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participantRequest")
	private List<ParticipantIntDepoHistory> participantIntDepoHistories;
	
	/** The participant mechanism histories. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participantRequest")
	private List<ParticipantMechanismHistory> participantMechanismHistories;
	
	/** The participant file histories. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participantRequest")
	private List<ParticipantFileHistory> participantFileHistories;
	
	/** The participant files. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participant")
	private List<InstitutionBankAccountHistory> institutionBankAccountHistories;
	
	/** The request motive description. */
	@Transient
	private String requestMotiveDescription;
	
	/** The request type description. */
	@Transient
	private String requestTypeDescription;
	
	/** The participant request state description. */
	@Transient
	private String participantRequestStateDescription;
	
	/** The modify bank bic code. */
	@Transient
	private boolean modifyBankBicCode;
	
	/** The modify international bank accounts. */
	@Transient
	private boolean modifyInternationalBankAccounts;
	
	/** The modify holder account bank. */
	@Transient
	private boolean modifyHolderAccountBank;
	
	@Transient
	private boolean indHolderWillBeCreated;
	
	@Transient
	private List<HolderFile> holderFiles;
	
    /**
     * The Constructor.
     */
    public ParticipantRequest() {
    }
    
 
    
    /**
     * Gets the request motive description.
     *
     * @return the request motive description
     */
    public String getRequestMotiveDescription() {
		return requestMotiveDescription;
	}



	/**
	 * Sets the request motive description.
	 *
	 * @param requestMotiveDescription the new request motive description
	 */
	public void setRequestMotiveDescription(String requestMotiveDescription) {
		this.requestMotiveDescription = requestMotiveDescription;
	}
	

	/**
	 * Gets the request type description.
	 *
	 * @return the request type description
	 */
	public String getRequestTypeDescription() {
		return requestTypeDescription;
	}



	/**
	 * Sets the request type description.
	 *
	 * @param requestTypeDescription the new request type description
	 */
	public void setRequestTypeDescription(String requestTypeDescription) {
		this.requestTypeDescription = requestTypeDescription;
	}



	/**
	 * Gets the participant request state description.
	 *
	 * @return the participant request state description
	 */
	public String getParticipantRequestStateDescription() {
		return participantRequestStateDescription;
	}



	/**
	 * Sets the participant request state description.
	 *
	 * @param participantRequestStateDescription the new participant request state description
	 */
	public void setParticipantRequestStateDescription(
			String participantRequestStateDescription) {
		this.participantRequestStateDescription = participantRequestStateDescription;
	}



	/**
	 * Gets the id participant request pk.
	 *
	 * @return the id participant request pk
	 */
	public Long getIdParticipantRequestPk() {
		return idParticipantRequestPk;
	}

	/**
	 * Sets the id participant request pk.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 */
	public void setIdParticipantRequestPk(Long idParticipantRequestPk) {
		this.idParticipantRequestPk = idParticipantRequestPk;
	}

	/**
	 * Gets the confirmation date.
	 *
	 * @return the confirmation date
	 */
	public Date getConfirmationDate() {
		return confirmationDate;
	}

	/**
	 * Sets the confirmation date.
	 *
	 * @param confirmationDate the confirmation date
	 */
	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return confirmationUser;
	}

	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}

	/**
	 * Sets the observations.
	 *
	 * @param observations the observations
	 */
	public void setObservations(String observations) {
		if(Validations.validateIsNotNullAndNotEmpty(observations)){
			observations = observations.toUpperCase();
		}
		this.observations = observations;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/**
	 * Gets the rejected date.
	 *
	 * @return the rejected date
	 */
	public Date getRejectedDate() {
		return rejectedDate;
	}

	/**
	 * Sets the rejected date.
	 *
	 * @param rejectedDate the rejected date
	 */
	public void setRejectedDate(Date rejectedDate) {
		this.rejectedDate = rejectedDate;
	}

	/**
	 * Gets the rejected motive.
	 *
	 * @return the rejected motive
	 */
	public Integer getRejectedMotive() {
		return rejectedMotive;
	}

	/**
	 * Sets the rejected motive.
	 *
	 * @param rejectedMotive the rejected motive
	 */
	public void setRejectedMotive(Integer rejectedMotive) {
		this.rejectedMotive = rejectedMotive;
	}

	/**
	 * Gets the rejected user.
	 *
	 * @return the rejected user
	 */
	public String getRejectedUser() {
		return rejectedUser;
	}

	/**
	 * Sets the rejected user.
	 *
	 * @param rejectedUser the rejected user
	 */
	public void setRejectedUser(String rejectedUser) {
		this.rejectedUser = rejectedUser;
	}

	/**
	 * Gets the request motive.
	 *
	 * @return the request motive
	 */
	public Integer getRequestMotive() {
		return requestMotive;
	}

	/**
	 * Sets the request motive.
	 *
	 * @param requestMotive the request motive
	 */
	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the state request.
	 *
	 * @return the state request
	 */
	public Integer getStateRequest() {
		return stateRequest;
	}

	/**
	 * Sets the state request.
	 *
	 * @param stateRequest the state request
	 */
	public void setStateRequest(Integer stateRequest) {
		this.stateRequest = stateRequest;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the participant files.
	 *
	 * @return the participant files
	 */
	public List<ParticipantFile> getParticipantFiles() {
		return participantFiles;
	}

	/**
	 * Sets the participant files.
	 *
	 * @param participantFiles the participant files
	 */
	public void setParticipantFiles(List<ParticipantFile> participantFiles) {
		this.participantFiles = participantFiles;
	}

	/**
	 * Gets the participant history states.
	 *
	 * @return the participant history states
	 */
	public List<ParticipantHistoryState> getParticipantHistoryStates() {
		return participantHistoryStates;
	}

	/**
	 * Sets the participant history states.
	 *
	 * @param participantHistoryStates the participant history states
	 */
	public void setParticipantHistoryStates(
			List<ParticipantHistoryState> participantHistoryStates) {
		this.participantHistoryStates = participantHistoryStates;
	}

	/**
	 * Gets the participant int depo histories.
	 *
	 * @return the participant int depo histories
	 */
	public List<ParticipantIntDepoHistory> getParticipantIntDepoHistories() {
		return participantIntDepoHistories;
	}

	/**
	 * Sets the participant int depo histories.
	 *
	 * @param participantIntDepoHistories the new participant int depo histories
	 */
	public void setParticipantIntDepoHistories(
			List<ParticipantIntDepoHistory> participantIntDepoHistories) {
		this.participantIntDepoHistories = participantIntDepoHistories;
	}

	/**
	 * Gets the participant mechanism histories.
	 *
	 * @return the participant mechanism histories
	 */
	public List<ParticipantMechanismHistory> getParticipantMechanismHistories() {
		return participantMechanismHistories;
	}

	/**
	 * Sets the participant mechanism histories.
	 *
	 * @param participantMechanismHistories the new participant mechanism histories
	 */
	public void setParticipantMechanismHistories(
			List<ParticipantMechanismHistory> participantMechanismHistories) {
		this.participantMechanismHistories = participantMechanismHistories;
	}	
	
	/**
	 * Gets the participant file histories.
	 *
	 * @return the participant file histories
	 */
	public List<ParticipantFileHistory> getParticipantFileHistories() {
		return participantFileHistories;
	}

	/**
	 * Sets the participant file histories.
	 *
	 * @param participantFileHistories the new participant file histories
	 */
	public void setParticipantFileHistories(
			List<ParticipantFileHistory> participantFileHistories) {
		this.participantFileHistories = participantFileHistories;
	}	
	
	public List<InstitutionBankAccountHistory> getInstitutionBankAccountHistories() {
		return institutionBankAccountHistories;
	}



	public void setInstitutionBankAccountHistories(List<InstitutionBankAccountHistory> institutionBankAccountHistories) {
		this.institutionBankAccountHistories = institutionBankAccountHistories;
	}

	/**
	 * Checks if is modify bank bic code.
	 *
	 * @return true, if is modify bank bic code
	 */
	public boolean isModifyBankBicCode() {
		return modifyBankBicCode;
	}



	/**
	 * Sets the modify bank bic code.
	 *
	 * @param modifyBankBicCode the new modify bank bic code
	 */
	public void setModifyBankBicCode(boolean modifyBankBicCode) {
		this.modifyBankBicCode = modifyBankBicCode;
	}



	/**
	 * Checks if is modify international bank accounts.
	 *
	 * @return true, if is modify international bank accounts
	 */
	public boolean isModifyInternationalBankAccounts() {
		return modifyInternationalBankAccounts;
	}



	/**
	 * Sets the modify international bank accounts.
	 *
	 * @param modifyInternationalBankAccounts the new modify international bank accounts
	 */
	public void setModifyInternationalBankAccounts(
			boolean modifyInternationalBankAccounts) {
		this.modifyInternationalBankAccounts = modifyInternationalBankAccounts;
	}



	/**
	 * Checks if is modify holder account bank.
	 *
	 * @return true, if is modify holder account bank
	 */
	public boolean isModifyHolderAccountBank() {
		return modifyHolderAccountBank;
	}



	/**
	 * Sets the modify holder account bank.
	 *
	 * @param modifyHolderAccountBank the new modify holder account bank
	 */
	public void setModifyHolderAccountBank(boolean modifyHolderAccountBank) {
		this.modifyHolderAccountBank = modifyHolderAccountBank;
	}
	
	public boolean isIndHolderWillBeCreated() {
		return indHolderWillBeCreated;
	}



	public void setIndHolderWillBeCreated(boolean indHolderWillBeCreated) {
		this.indHolderWillBeCreated = indHolderWillBeCreated;
	}		
	
	public List<HolderFile> getHolderFiles() {
		return holderFiles;
	}


	public void setHolderFiles(List<HolderFile> holderFiles) {
		this.holderFiles = holderFiles;
	}



	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("participantHistoryStates", participantHistoryStates);
        detailsMap.put("participantFiles", participantFiles);
        detailsMap.put("participantIntDepoHistories", participantIntDepoHistories);
        detailsMap.put("participantMechanismHistories", participantMechanismHistories);
        detailsMap.put("participantFileHistories", participantFileHistories);
        detailsMap.put("institutionBankAccountHistories", institutionBankAccountHistories);
        return detailsMap;
	}
	
	

}