package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum StatalEntityDocRepreJurType {
		
		
		RNC(Integer.valueOf(378),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(199)),
		CDRL(Integer.valueOf(379),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(199)),	
		CRMF(Integer.valueOf(380),"CERTIFICADO DEL REGISTRO MERCANTIL DEL FIDUCIARIO",Integer.valueOf(199)), 
		DEFF(Integer.valueOf(381),"DOCUMENTO DE EJERCICIO DE FUNCIONES DEL FIDUCIARIO",Integer.valueOf(199));
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<StatalEntityDocRepreJurType> list = new ArrayList<StatalEntityDocRepreJurType>();
		public static final Map<Integer, StatalEntityDocRepreJurType> lookup = new HashMap<Integer, StatalEntityDocRepreJurType>();

		static {
			for (StatalEntityDocRepreJurType s : EnumSet.allOf(StatalEntityDocRepreJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private StatalEntityDocRepreJurType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static StatalEntityDocRepreJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
