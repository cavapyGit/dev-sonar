package com.pradera.model.accounts.type;


import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum PnaPersonInformationSourceType {
	
	TV(Integer.valueOf(436),"TV"), 
	PRENSA(Integer.valueOf(437),"PRENSA"),
	RADIO(Integer.valueOf(438),"RADIO");
 	

	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private PnaPersonInformationSourceType(int code,String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<PnaPersonInformationSourceType> list = new ArrayList<PnaPersonInformationSourceType>();
	public static final Map<Integer, PnaPersonInformationSourceType> lookup = new HashMap<Integer, PnaPersonInformationSourceType>();
	static {
		for (PnaPersonInformationSourceType s : EnumSet.allOf(PnaPersonInformationSourceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static PnaPersonInformationSourceType get(Integer code)
	{
		return lookup.get(code);
	}

}
