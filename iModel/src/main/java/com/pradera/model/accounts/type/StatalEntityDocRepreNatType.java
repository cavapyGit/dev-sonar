package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum StatalEntityDocRepreNatType {
		
		POR(Integer.valueOf(318),"PODER OTORGADO COMO REPRESENTANTE",Integer.valueOf(190)),
		CE(Integer.valueOf(319),"CEDULA DE IDENTIDAD",Integer.valueOf(190)), 
		PAS(Integer.valueOf(320),"PASAPORTE",Integer.valueOf(190)),
		LIC(Integer.valueOf(321),"LICENCIA DE CONDUCIR",Integer.valueOf(190)),
		DIO(Integer.valueOf(322),"DOCUMENTO DE IDENTIDAD DE PAIS ORIGEN",Integer.valueOf(190)),
		RNC(Integer.valueOf(323),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(190)), 
		DJCP(Integer.valueOf(324),"DECLARACION JURADA SOBRE CONDICION DE PEP",Integer.valueOf(190)),
	    OC(Integer.valueOf(325),"ORDEN DE CUSTODIA",Integer.valueOf(190)),
		CATL(Integer.valueOf(326),"DOCUMENTO DE ACREDITACION COMO TUTOR LEGAL",Integer.valueOf(190)),
		CDRL(Integer.valueOf(327),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(190));	
		
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<StatalEntityDocRepreNatType> list = new ArrayList<StatalEntityDocRepreNatType>();
		public static final Map<Integer, StatalEntityDocRepreNatType> lookup = new HashMap<Integer, StatalEntityDocRepreNatType>();

		static {
			for (StatalEntityDocRepreNatType s : EnumSet.allOf(StatalEntityDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private StatalEntityDocRepreNatType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static StatalEntityDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
