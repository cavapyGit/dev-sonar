package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * 
 * The Enum HolderStateType.
 *
 * @author rlarico.
 * @version 1.0 , 03/03/2020
 */
public enum HolderAnnulationCancelType {
	
	// TIPOS DE CANCELACION EN UNA SOLICTUD DE ANULACION DE CUI
	CANCEL_PARTICIPANT_REQUEST(Integer.valueOf(2502),"A SOLICITUD DEL PARTICIPANTE"),
	CANCEL_INCORRECT_DATA(Integer.valueOf(2503),"DATOS INCORRECTOS"),
	CANCEL_LACK_FILE_BACKUPS(Integer.valueOf(2504),"FALTA DOCUMENTOS DE RESPALDO"),
	CANCEL_OTHER_MOTIVES(Integer.valueOf(2505),"OTROS MOTIVOS"),
	
	;
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new holder state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private HolderAnnulationCancelType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the holder state type
	 */
	public static HolderAnnulationCancelType get(Integer code) {
		return lookup.get(code);
	}

	/** The Constant list. */
	public static final List<HolderAnnulationCancelType> list = new ArrayList<HolderAnnulationCancelType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAnnulationCancelType> lookup = new HashMap<Integer, HolderAnnulationCancelType>();
	static {
		for (HolderAnnulationCancelType s : EnumSet.allOf(HolderAnnulationCancelType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}