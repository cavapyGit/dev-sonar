/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountStatusType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
public enum HolderAccountStateDetailType {
	
	/** The registered. */
	REGISTERED(new Integer(1174),"REGISTERED"),
	
	/** The deleted. */
	DELETED(new Integer(1175),"DELETED");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<HolderAccountStateDetailType> list = new ArrayList<HolderAccountStateDetailType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountStateDetailType> lookup = new HashMap<Integer, HolderAccountStateDetailType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountStateDetailType> listSomeElements(HolderAccountStateDetailType... transferSecuritiesTypeParams){
		List<HolderAccountStateDetailType> retorno = new ArrayList<HolderAccountStateDetailType>();
		for(HolderAccountStateDetailType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountStateDetailType s : EnumSet.allOf(HolderAccountStateDetailType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account status type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountStateDetailType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account status type
	 */
	public static HolderAccountStateDetailType get(Integer codigo) {
		return lookup.get(codigo);
	}
}