package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum RequestParticipantFileType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/04/2013
 */
public enum RequestParticipantFileType {
	
	/** The register. */
	REGISTER(Integer.valueOf(828),"REGISTRO"),
	
	/** The block. */
	BLOCK(Integer.valueOf(829),"BLOQUEO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new request participant file type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private RequestParticipantFileType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<RequestParticipantFileType> list = new ArrayList<RequestParticipantFileType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RequestParticipantFileType> lookup = new HashMap<Integer, RequestParticipantFileType>();
	static {
		for (RequestParticipantFileType s : EnumSet.allOf(RequestParticipantFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
