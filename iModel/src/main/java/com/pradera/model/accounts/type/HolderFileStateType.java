package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantFileStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/04/2013
 */
public enum HolderFileStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(1093),"REGISTRADO"),
	
	/** The deleted. */
	DELETED(Integer.valueOf(1094),"ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant file state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private HolderFileStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<HolderFileStateType> list = new ArrayList<HolderFileStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderFileStateType> lookup = new HashMap<Integer, HolderFileStateType>();
	static {
		for (HolderFileStateType s : EnumSet.allOf(HolderFileStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
