package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum FiduciaryDocRepreJurType {
		
		
		RNC(Integer.valueOf(382),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(200)),
		CDRL(Integer.valueOf(383),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(200)),	
		CRMF(Integer.valueOf(384),"CERTIFICADO DEL REGISTRO MERCANTIL DEL FIDUCIARIO",Integer.valueOf(200)), 
		DEFF(Integer.valueOf(385),"DOCUMENTO DE EJERCICIO DE FUNCIONES DEL FIDUCIARIO",Integer.valueOf(200));
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<FiduciaryDocRepreJurType> list = new ArrayList<FiduciaryDocRepreJurType>();
		public static final Map<Integer, FiduciaryDocRepreJurType> lookup = new HashMap<Integer, FiduciaryDocRepreJurType>();

		static {
			for (FiduciaryDocRepreJurType s : EnumSet.allOf(FiduciaryDocRepreJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private FiduciaryDocRepreJurType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static FiduciaryDocRepreJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
