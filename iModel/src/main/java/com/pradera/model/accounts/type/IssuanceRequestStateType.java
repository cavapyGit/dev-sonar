package com.pradera.model.accounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum IssuanceRequestStateType {
	
	REGISTERED(Integer.valueOf(2370),"REGISTRADO"),
	REVIEWED(Integer.valueOf(2371),"REVISADO"),
	APPROBED(Integer.valueOf(2372),"APROBADO"),
	CONFIRMED(Integer.valueOf(2373),"CONFIRMADO"),
	REJECTED(Integer.valueOf(2374),"RECHAZADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuanceRequestStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<IssuanceRequestStateType> list = new ArrayList<IssuanceRequestStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, IssuanceRequestStateType> lookup = new HashMap<Integer, IssuanceRequestStateType>();
	static {
		for (IssuanceRequestStateType s : EnumSet.allOf(IssuanceRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant block unblock request type
	 */
	public static IssuanceRequestStateType get(Integer code) {
		return lookup.get(code);
	}
}
