package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * 
 * The Enum HolderAnnulationFileType.
 *
 * @author rlarico
 * @version 1.0 , 03/03/2020
 */
public enum HolderAnnulationFileType {
	
	// TIPOS DE ARCHIVO SEGUN EL MOTIVO DE LA ANULACION DE CUI
	RESOLUTION_REGULATORY_ENTE(Integer.valueOf(2496),"RESOLUCION - ENTE REGULADOR"),
	RESOLUTION_EDV(Integer.valueOf(2497),"RESOLUCION EDV"),
	ORDER_EXPRESS_PARTICIPANT(Integer.valueOf(2498),"ORDEN EXPRESA - PARTICIPANTE"),
	DEATH_CERTIFICATE(Integer.valueOf(2499),"ACTA DE DEFUNCION"),
	COMPETENT_ENTITY_ORDER(Integer.valueOf(2500),"ORDEN DE LA ENTIDAD COMPETENTE"),
	OTHER_MOTIVES(Integer.valueOf(2501),"OTROS MOTIVOS"),
	ORDER_EXPRESS_HOLDER(Integer.valueOf(2817),"ORDEN EXPRESA - TITULAR"),
	
	;
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new holder state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private HolderAnnulationFileType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the holder state type
	 */
	public static HolderAnnulationFileType get(Integer code) {
		return lookup.get(code);
	}

	/** The Constant list. */
	public static final List<HolderAnnulationFileType> list = new ArrayList<HolderAnnulationFileType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAnnulationFileType> lookup = new HashMap<Integer, HolderAnnulationFileType>();
	static {
		for (HolderAnnulationFileType s : EnumSet.allOf(HolderAnnulationFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}