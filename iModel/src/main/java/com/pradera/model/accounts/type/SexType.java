package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum SexType {
	
	MALE(Integer.valueOf(1),"MASCULINO"), 
	FEMALE(Integer.valueOf(2),"FEMENINO"),
	POR_COMPLETAR(Integer.valueOf(3),"POR COMPLETAR");
	

	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private SexType(int code,String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<SexType> list = new ArrayList<SexType>();
	public static final Map<Integer, SexType> lookup = new HashMap<Integer, SexType>();
	static {
		for (SexType s : EnumSet.allOf(SexType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
