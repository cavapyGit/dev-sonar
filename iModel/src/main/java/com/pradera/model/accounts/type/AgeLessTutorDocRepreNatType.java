package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeLessTutorDocRepreNatType {
		
		
		CI(Integer.valueOf(289),"FOTOCOPIA CEDULA DE IDENTIDAD"), 
		PASS(Integer.valueOf(290),"FOTOCOPIA PASAPORTE"),
		DIO(Integer.valueOf(292),"FOTOCOPIA DEL DOCUMENTO DE IDENTIDAD DE PAIS DE ORIGEN"),
		DJCP(Integer.valueOf(294),"DECLARACION JURADA SOBRE CONDICION DE PEP"),
	    CATC(Integer.valueOf(296),"DESIGNACION DE AUTORIDAD COMPETENTE QUE ENCOMIENDE LA TUTELA O CURATELA");
		
		
		private Integer code;
		private String value;
		

		public static final List<AgeLessTutorDocRepreNatType> list = new ArrayList<AgeLessTutorDocRepreNatType>();
		public static final Map<Integer, AgeLessTutorDocRepreNatType> lookup = new HashMap<Integer, AgeLessTutorDocRepreNatType>();

		static {
			for (AgeLessTutorDocRepreNatType s : EnumSet.allOf(AgeLessTutorDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeLessTutorDocRepreNatType(Integer code, String value) {
			this.code = code;
			this.value = value;		
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeLessTutorDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
