package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeLessTutorDocRepreJurType {
				
		RNC(Integer.valueOf(366),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(196)),
		CDRL(Integer.valueOf(367),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(196)),	
		CRMF(Integer.valueOf(368),"CERTIFICADO DEL REGISTRO MERCANTIL DEL FIDUCIARIO",Integer.valueOf(196)), 
		DEFF(Integer.valueOf(369),"DOCUMENTO DE EJERCICIO DE FUNCIONES DEL FIDUCIARIO",Integer.valueOf(196));
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<AgeLessTutorDocRepreJurType> list = new ArrayList<AgeLessTutorDocRepreJurType>();
		public static final Map<Integer, AgeLessTutorDocRepreJurType> lookup = new HashMap<Integer, AgeLessTutorDocRepreJurType>();

		static {
			for (AgeLessTutorDocRepreJurType s : EnumSet.allOf(AgeLessTutorDocRepreJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeLessTutorDocRepreJurType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeLessTutorDocRepreJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
