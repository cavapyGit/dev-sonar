package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum BlockedUnBlockedRequestStateType {
	
	BLOCKED(Integer.valueOf(1),"BLOCKEADO"),
	UNBLOCKED(Integer.valueOf(2),"DESBLOQUEADO");
	
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private BlockedUnBlockedRequestStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static BlockedUnBlockedRequestStateType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<BlockedUnBlockedRequestStateType> list = new ArrayList<BlockedUnBlockedRequestStateType>();
	public static final Map<Integer, BlockedUnBlockedRequestStateType> lookup = new HashMap<Integer, BlockedUnBlockedRequestStateType>();
	static {
		for (BlockedUnBlockedRequestStateType s : EnumSet.allOf(BlockedUnBlockedRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
