package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum AgeMajorDomNoResidentDocHolderNatType {
		
		CE(Integer.valueOf(885),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(886),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(887),"PASAPORTE"), 
		LIC(Integer.valueOf(888),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(889),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(890),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(891),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(892),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(893),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(894),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(895),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(896),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(897),"DECRETO"),		
	    RCF(Integer.valueOf(898),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(899),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(900),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    		
		private Integer code;
		private String value;
		
		

		public static final List<AgeMajorDomNoResidentDocHolderNatType> list = new ArrayList<AgeMajorDomNoResidentDocHolderNatType>();
		public static final Map<Integer, AgeMajorDomNoResidentDocHolderNatType> lookup = new HashMap<Integer, AgeMajorDomNoResidentDocHolderNatType>();

		static {
			for (AgeMajorDomNoResidentDocHolderNatType s : EnumSet.allOf(AgeMajorDomNoResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private AgeMajorDomNoResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static AgeMajorDomNoResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
