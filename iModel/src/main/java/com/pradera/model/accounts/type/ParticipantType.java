package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum ParticipantType {
	
	DIRECT(Integer.valueOf(26),"DIRECTO"),
	INDIRECT(Integer.valueOf(27),"INDIRECTO");

	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private ParticipantType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<ParticipantType> list = new ArrayList<ParticipantType>();
	public static final Map<Integer, ParticipantType> lookup = new HashMap<Integer, ParticipantType>();
	static {
		for (ParticipantType s : EnumSet.allOf(ParticipantType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ParticipantType get(Integer code) {
		return lookup.get(code);
	}

}
