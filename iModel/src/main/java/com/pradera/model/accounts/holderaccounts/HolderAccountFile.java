package com.pradera.model.accounts.holderaccounts;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT_FILE database table.
 * 
 */
@Entity
@Table(name="HOLDER_ACCOUNT_FILE")
public class HolderAccountFile implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder account file pk. */
	@Id
	@SequenceGenerator(name="SQ_HOLDER_ACCOUNT_FILE_PK", sequenceName="SQ_ID_HOLDER_ACCOUNT_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLDER_ACCOUNT_FILE_PK")
	@Column(name="ID_HOLDER_ACCOUNT_FILE_PK")
	private Long idHolderAccountFilePk;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;
	
	/** The ind main. */
	@Column(name="IND_MAIN")
	private Integer indMain;

    /** The file holder account. */
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_FILE")
	private byte[] documentFile;

	/** The filename. */
	private String filename;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional many-to-one association to HolderAccount
	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	/** The description. */
	@Transient
	private String documentTypeDesc;
	
	@Transient
	private Integer stateDocumentFile;

    /**
     * The Constructor.
     */
    public HolderAccountFile() {
    }

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the file holder account.
	 *
	 * @return the file holder account
	 */
	public byte[] getDocumentFile() {
		return this.documentFile;
	}

	/**
	 * Sets the file holder account.
	 *
	 * @param fileHolderAccount the file holder account
	 */
	public void setDocumentFile(byte[] fileHolderAccount) {
		this.documentFile = fileHolderAccount;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return this.filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Sets the id holder account file pk.
	 *
	 * @param idHolderAccountFilePk the id holder account file pk
	 */
	public void setIdHolderAccountFilePk(Long idHolderAccountFilePk) {
		this.idHolderAccountFilePk = idHolderAccountFilePk;
	}

	/**
	 * Gets the id holder account file pk.
	 *
	 * @return the id holder account file pk
	 */
	public Long getIdHolderAccountFilePk() {
		return idHolderAccountFilePk;
	}

	/**
	 * Gets the ind main.
	 *
	 * @return the ind main
	 */
	public Integer getIndMain() {
		return indMain;
	}

	/**
	 * Sets the ind main.
	 *
	 * @param indMain the ind main
	 */
	public void setIndMain(Integer indMain) {
		this.indMain = indMain;
	}
	
	public Integer getStateDocumentFile() {
		return stateDocumentFile;
	}

	public void setStateDocumentFile(Integer stateDocumentFile) {
		this.stateDocumentFile = stateDocumentFile;
	}
	
	public boolean isAnnulate(){
    	return Validations.validateIsNullOrEmpty(this.getStateDocumentFile())?false:this.getStateDocumentFile().equals(HolderAccountStateBankType.REGISTERED.getCode())?true:false;
    }

	@Override
	public String toString() {
		return "HolderAccountFile [idHolderAccountFilePk="
				+ idHolderAccountFilePk + ", documentType=" + documentType
				+ ", indMain=" + indMain + ", filename=" + filename
				+ ", lastModifyApp=" + lastModifyApp + ", lastModifyDate="
				+ lastModifyDate + ", lastModifyIp=" + lastModifyIp
				+ ", lastModifyUser=" + lastModifyUser + ", registryDate="
				+ registryDate + ", registryUser=" + registryUser+ "]";
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDocumentTypeDesc() {
		return documentTypeDesc;
	}

	public void setDocumentTypeDesc(String documentTypeDesc) {
		this.documentTypeDesc = documentTypeDesc;
	}
	
	
}