package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum JuridicDocRepreJurType {
		
		RNC(Integer.valueOf(374),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(198)),
		CDRL(Integer.valueOf(375),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(198)),	
		CRMF(Integer.valueOf(376),"CERTIFICADO DEL REGISTRO MERCANTIL DEL FIDUCIARIO",Integer.valueOf(198)), 
		DEFF(Integer.valueOf(377),"DOCUMENTO DE EJERCICIO DE FUNCIONES DEL FIDUCIARIO",Integer.valueOf(198));
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<JuridicDocRepreJurType> list = new ArrayList<JuridicDocRepreJurType>();
		public static final Map<Integer, JuridicDocRepreJurType> lookup = new HashMap<Integer, JuridicDocRepreJurType>();

		static {
			for (JuridicDocRepreJurType s : EnumSet.allOf(JuridicDocRepreJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private JuridicDocRepreJurType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static JuridicDocRepreJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
