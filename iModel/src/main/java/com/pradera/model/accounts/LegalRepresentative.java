package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PersonType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the LEGAL_REPRESENTATIVE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/02/2013
 */
@Entity
@Table(name="LEGAL_REPRESENTATIVE")
public class LegalRepresentative implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id representative pk. */
	@Id
	@SequenceGenerator(name="LEGAL_REPRESENTATIVE_IDLEGALREPRESENTATIVEPK_GENERATOR", sequenceName="SQ_ID_LEGAL_REPRESENTATIVE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LEGAL_REPRESENTATIVE_IDLEGALREPRESENTATIVEPK_GENERATOR")
	@Column(name="ID_LEGAL_REPRESENTATIVE_PK")
	private Long idLegalRepresentativePk;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private Integer economicActivity;

	/** The economic sector. */
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;
    
	/** The email. */
	@Column(name="EMAIL")
	private String email;

	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;

	/** The first last name. */
	@Column(name="FIRST_LAST_NAME")
	private String firstLastName;

	/** The full name. */
	@Column(name="FULL_NAME")
	private String fullName;

	/** The ind residence. */
	@Column(name="IND_RESIDENCE")
	private Integer indResidence;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The legal address. */
	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;

	/** The legal department. */
	@Column(name="LEGAL_DEPARTMENT")
	private Integer legalDepartment;

	/** The legal district. */
	@Column(name="LEGAL_DISTRICT")
	private Integer legalDistrict;

	/** The legal province. */
	@Column(name="LEGAL_PROVINCE")
	private Integer legalProvince;

	/** The legal residence country. */
	@Column(name="LEGAL_RESIDENCE_COUNTRY")
	private Integer legalResidenceCountry;

	/** The mobile number. */
	@Column(name="MOBILE_NUMBER")
	private String mobileNumber;

	/** The name. */
	@Column(name="NAME")
	private String name;

	/** The nationality. */
	@Column(name="NATIONALITY")
	@NotNull
	private Integer nationality;

	/** The person type. */
	@Column(name="PERSON_TYPE")
	@NotNull
	private Integer personType;

	/** The home phone number. */
	@Column(name="HOME_PHONE_NUMBER")
	private String homePhoneNumber;

	/** The office phone number. */
	@Column(name="OFFICE_PHONE_NUMBER")
	private String officePhoneNumber;
	
	/** The postal address. */
	@Column(name="POSTAL_ADDRESS")
	private String postalAddress;

	/** The postal department. */
	@Column(name="POSTAL_DEPARTMENT")
	private Integer postalDepartment;

	/** The postal district. */
	@Column(name="POSTAL_DISTRICT")
	private Integer postalDistrict;

	/** The postal province. */
	@Column(name="POSTAL_PROVINCE")
	private Integer postalProvince;

	/** The postal residence country. */
	@Column(name="POSTAL_RESIDENCE_COUNTRY")
	private Integer postalResidenceCountry;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The second document number. */
	@Column(name="SECOND_DOCUMENT_NUMBER")
	private String secondDocumentNumber;

	/** The second document type. */
	@Column(name="SECOND_DOCUMENT_TYPE")
	private Integer secondDocumentType;

	/** The second last name. */
	@Column(name="SECOND_LAST_NAME")
	private String secondLastName;

	/** The second nationality. */
	@Column(name="SECOND_NATIONALITY")
	private Integer secondNationality;

	/** The state. */
	@Column(name="\"STATE\"")
	private Integer state;
	
	/** The sex. */
	@Column(name="SEX")
	private Integer sex;
	
	/** The birth date. */
	@Column(name="BIRTH_DATE")
	private Date birthDate;
	
	/** The registry date. */
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	//bi-directional many-to-one association to RepresentedEntity
	/** The represented entities. */
	@OneToMany(mappedBy="legalRepresentative",fetch=FetchType.LAZY)
	private List<RepresentedEntity> representedEntities;
	
	/** The legal representative file. */
	@OneToMany(mappedBy="legalRepresentative",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<LegalRepresentativeFile> legalRepresentativeFile;

	/** The document source. */
	@Column(name="DOCUMENT_SOURCE")
	private Integer documentSource;
	
	/** The document issuance date. */
	@Column(name="DOCUMENT_ISSUANCE_DATE")
	private Integer documentIssuanceDate;
	
	/** The authority legal number. */
	@Column(name="AUTHORITY_LEGAL_NUMBER")
    private String authorityLegalNumber;
	
	/** The representative type description. */
	@Transient
	private String representativeTypeDescription;
	
	/** The document type description. */
	@Transient
	private String documentTypeDescription;

	/** The representative class description. */
	@Transient
	private String representativeClassDescription;
	
	/** The representative description aux. */
	@Transient
	private String representativeDescriptionAux;
	
	/** The representative class aux. */
	@Transient
	private Integer representativeClassAux;
	
	/** The holderfk. */
	@Transient
	private Long holderfk;
	
    /**
     * Gets the holderfk.
     *
     * @return the holderfk
     */
    public Long getHolderfk() {
		return holderfk;
	}

	/**
	 * Sets the holderfk.
	 *
	 * @param holderfk the new holderfk
	 */
	public void setHolderfk(Long holderfk) {
		this.holderfk = holderfk;
	}

	/**
     * The Constructor.
     */
    public LegalRepresentative() {
    }
    
    /**
     * Gets the person type description.
     *
     * @return the person type description
     */
    public String getPersonTypeDescription(){
    	String personTypeDescription = null;
    	if(personType != null) {
    		personTypeDescription = PersonType.get(personType).getValue();
    	}
    	return personTypeDescription;
    }    
    
    /**
     * Gets the document type description.
     *
     * @return the document type description
     */
    public String getDocumentTypeDescription() {
    	String documentTypeDescription = null;
    	if(documentType != null) {
    		documentTypeDescription = DocumentType.get(documentType).getValue();
    	}
    	return documentTypeDescription;
    }
    
    /**
     * Gets the document type and number.
     *
     * @return the document type and number
     */
    public String getDocumentTypeAndNumber(){
    	if(documentNumber != null && getDocumentTypeDescription() != null){
    		StringBuilder sb = new StringBuilder();
    		sb.append(getDocumentTypeDescription());
    		sb.append("-");
    		sb.append(documentNumber);
    		return sb.toString();
    	}
    	return null;
    }


	

	/**
	 * Gets the id legal representative pk.
	 *
	 * @return the id legal representative pk
	 */
	public Long getIdLegalRepresentativePk() {
		return idLegalRepresentativePk;
	}



	/**
	 * Sets the id legal representative pk.
	 *
	 * @param idLegalRepresentativePk the new id legal representative pk
	 */
	public void setIdLegalRepresentativePk(Long idLegalRepresentativePk) {
		this.idLegalRepresentativePk = idLegalRepresentativePk;
	}



	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return this.documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public Integer getEconomicActivity() {
		return this.economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return this.economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return this.faxNumber;
	}

	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Gets the first last name.
	 *
	 * @return the first last name
	 */
	public String getFirstLastName() {
		return this.firstLastName;
	}

	/**
	 * Sets the first last name.
	 *
	 * @param firstLastName the first last name
	 */
	public void setFirstLastName(String firstLastName) {
		if(Validations.validateIsNotNull(firstLastName)){
			firstLastName = firstLastName.toUpperCase();
		}
		this.firstLastName = firstLastName;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return this.fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the full name
	 */
	public void setFullName(String fullName) {
		if(Validations.validateIsNotNull(fullName)){
			fullName = fullName.toUpperCase();
		}
		this.fullName = fullName;
	}

	/**
	 * Gets the ind residence.
	 *
	 * @return the ind residence
	 */
	public Integer getIndResidence() {
		return this.indResidence;
	}

	/**
	 * Sets the ind residence.
	 *
	 * @param indResidence the ind residence
	 */
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the legal address.
	 *
	 * @return the legal address
	 */
	public String getLegalAddress() {
		return this.legalAddress;
	}

	/**
	 * Sets the legal address.
	 *
	 * @param legalAddress the legal address
	 */
	public void setLegalAddress(String legalAddress) {
		if(Validations.validateIsNotNull(legalAddress)){
			legalAddress = legalAddress.toUpperCase();
		}
		this.legalAddress = legalAddress;
	}

	/**
	 * Gets the legal department.
	 *
	 * @return the legal department
	 */
	public Integer getLegalDepartment() {
		return this.legalDepartment;
	}

	/**
	 * Sets the legal department.
	 *
	 * @param legalDepartment the legal department
	 */
	public void setLegalDepartment(Integer legalDepartment) {
		this.legalDepartment = legalDepartment;
	}

	/**
	 * Gets the legal district.
	 *
	 * @return the legal district
	 */
	public Integer getLegalDistrict() {
		return this.legalDistrict;
	}

	/**
	 * Sets the legal district.
	 *
	 * @param legalDistrict the legal district
	 */
	public void setLegalDistrict(Integer legalDistrict) {
		this.legalDistrict = legalDistrict;
	}

	/**
	 * Gets the legal province.
	 *
	 * @return the legal province
	 */
	public Integer getLegalProvince() {
		return this.legalProvince;
	}

	/**
	 * Sets the legal province.
	 *
	 * @param legalProvince the legal province
	 */
	public void setLegalProvince(Integer legalProvince) {
		this.legalProvince = legalProvince;
	}

	/**
	 * Gets the legal residence country.
	 *
	 * @return the legal residence country
	 */
	public Integer getLegalResidenceCountry() {
		return this.legalResidenceCountry;
	}

	/**
	 * Sets the legal residence country.
	 *
	 * @param legalResidenceCountry the legal residence country
	 */
	public void setLegalResidenceCountry(Integer legalResidenceCountry) {
		this.legalResidenceCountry = legalResidenceCountry;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return this.mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		if(Validations.validateIsNotNull(name)){
			name = name.toUpperCase();
		}
		this.name = name;
	}

	/**
	 * Gets the nationality.
	 *
	 * @return the nationality
	 */
	public Integer getNationality() {
		return this.nationality;
	}

	/**
	 * Sets the nationality.
	 *
	 * @param nationality the nationality
	 */
	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	/**
	 * Gets the person type.
	 *
	 * @return the person type
	 */
	public Integer getPersonType() {
		return this.personType;
	}

	/**
	 * Sets the person type.
	 *
	 * @param personType the person type
	 */
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}	

	/**
	 * Gets the home phone number.
	 *
	 * @return the home phone number
	 */
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	/**
	 * Sets the home phone number.
	 *
	 * @param homePhoneNumber the home phone number
	 */
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	/**
	 * Gets the postal address.
	 *
	 * @return the postal address
	 */
	public String getPostalAddress() {
		return this.postalAddress;
	}

	/**
	 * Sets the postal address.
	 *
	 * @param postalAddress the postal address
	 */
	public void setPostalAddress(String postalAddress) {
		if(Validations.validateIsNotNull(postalAddress)){
			postalAddress = postalAddress.toUpperCase();
		}
		this.postalAddress = postalAddress;
	}

	/**
	 * Gets the postal department.
	 *
	 * @return the postal department
	 */
	public Integer getPostalDepartment() {
		return this.postalDepartment;
	}

	/**
	 * Sets the postal department.
	 *
	 * @param postalDepartment the postal department
	 */
	public void setPostalDepartment(Integer postalDepartment) {
		this.postalDepartment = postalDepartment;
	}

	/**
	 * Gets the postal district.
	 *
	 * @return the postal district
	 */
	public Integer getPostalDistrict() {
		return this.postalDistrict;
	}

	/**
	 * Sets the postal district.
	 *
	 * @param postalDistrict the postal district
	 */
	public void setPostalDistrict(Integer postalDistrict) {
		this.postalDistrict = postalDistrict;
	}

	/**
	 * Gets the postal province.
	 *
	 * @return the postal province
	 */
	public Integer getPostalProvince() {
		return this.postalProvince;
	}

	/**
	 * Sets the postal province.
	 *
	 * @param postalProvince the postal province
	 */
	public void setPostalProvince(Integer postalProvince) {
		this.postalProvince = postalProvince;
	}

	/**
	 * Gets the postal residence country.
	 *
	 * @return the postal residence country
	 */
	public Integer getPostalResidenceCountry() {
		return postalResidenceCountry;
	}

	/**
	 * Sets the postal residence country.
	 *
	 * @param postalResidenceCountry the postal residence country
	 */
	public void setPostalResidenceCountry(Integer postalResidenceCountry) {
		this.postalResidenceCountry = postalResidenceCountry;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the second document number.
	 *
	 * @return the second document number
	 */
	public String getSecondDocumentNumber() {
		return this.secondDocumentNumber;
	}

	/**
	 * Sets the second document number.
	 *
	 * @param secondDocumentNumber the second document number
	 */
	public void setSecondDocumentNumber(String secondDocumentNumber) {
		this.secondDocumentNumber = secondDocumentNumber;
	}

	/**
	 * Gets the second document type.
	 *
	 * @return the second document type
	 */
	public Integer getSecondDocumentType() {
		return this.secondDocumentType;
	}

	/**
	 * Sets the second document type.
	 *
	 * @param secondDocumentType the second document type
	 */
	public void setSecondDocumentType(Integer secondDocumentType) {
		this.secondDocumentType = secondDocumentType;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return this.secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the second last name
	 */
	public void setSecondLastName(String secondLastName) {
		if(Validations.validateIsNotNull(secondLastName)){
			secondLastName = secondLastName.toUpperCase();
		}
		this.secondLastName = secondLastName;
	}

	/**
	 * Gets the second nationality.
	 *
	 * @return the second nationality
	 */
	public Integer getSecondNationality() {
		return this.secondNationality;
	}

	/**
	 * Sets the second nationality.
	 *
	 * @param secondNationality the second nationality
	 */
	public void setSecondNationality(Integer secondNationality) {
		this.secondNationality = secondNationality;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the represented entities.
	 *
	 * @return the represented entities
	 */
	public List<RepresentedEntity> getRepresentedEntities() {
		return this.representedEntities;
	}

	/**
	 * Sets the represented entities.
	 *
	 * @param representedEntities the represented entities
	 */
	public void setRepresentedEntities(List<RepresentedEntity> representedEntities) {
		this.representedEntities = representedEntities;
	}

	/**
	 * Gets the sex.
	 *
	 * @return the sex
	 */
	public Integer getSex() {
		return sex;
	}

	/**
	 * Sets the sex.
	 *
	 * @param sex the sex
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * Gets the birth date.
	 *
	 * @return the birth date
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * Sets the birth date.
	 *
	 * @param birthDate the birth date
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Gets the office phone number.
	 *
	 * @return the office phone number
	 */
	public String getOfficePhoneNumber() {
		return officePhoneNumber;
	}

	/**
	 * Sets the office phone number.
	 *
	 * @param officePhoneNumber the office phone number
	 */
	public void setOfficePhoneNumber(String officePhoneNumber) {
		this.officePhoneNumber = officePhoneNumber;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the legal representative file.
	 *
	 * @return the legal representative file
	 */
	public List<LegalRepresentativeFile> getLegalRepresentativeFile() {
		return legalRepresentativeFile;
	}

	/**
	 * Sets the legal representative file.
	 *
	 * @param legalRepresentativeFile the new legal representative file
	 */
	public void setLegalRepresentativeFile(
			List<LegalRepresentativeFile> legalRepresentativeFile) {
		this.legalRepresentativeFile = legalRepresentativeFile;
	}

	/**
	 * Gets the representative type description.
	 *
	 * @return the representative type description
	 */
	public String getRepresentativeTypeDescription() {
		if(personType!=null){
			representativeTypeDescription = PersonType.get(personType).getValue();	
		}
		
		return representativeTypeDescription;
	}

	/**
	 * Sets the representative type description.
	 *
	 * @param representativeTypeDescription the new representative type description
	 */
	public void setRepresentativeTypeDescription(
			String representativeTypeDescription) {
		this.representativeTypeDescription = representativeTypeDescription;
	}

	/**
	 * Sets the document type description.
	 *
	 * @param documentTypeDescription the new document type description
	 */
	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}
	
	
	
	/**
	 * Gets the representative class description.
	 *
	 * @return the representative class description
	 */
	public String getRepresentativeClassDescription() {
		return representativeClassDescription;
	}

	/**
	 * Sets the representative class description.
	 *
	 * @param representativeClassDescription the new representative class description
	 */
	public void setRepresentativeClassDescription(
			String representativeClassDescription) {
		this.representativeClassDescription = representativeClassDescription;
	}
	
	

	/**
	 * Gets the representative description aux.
	 *
	 * @return the representative description aux
	 */
	public String getRepresentativeDescriptionAux() {
		if(personType!=null && personType.equals(PersonType.NATURAL.getCode())){
			if(secondLastName != null){
			representativeDescriptionAux = firstLastName+" "+secondLastName+","+name;
			}
			else{
			representativeDescriptionAux = firstLastName+","+name;					
			}
		}
		else if(personType!=null && personType.equals(PersonType.JURIDIC.getCode())){
			representativeDescriptionAux = fullName;
		}
		
		return representativeDescriptionAux;
	}

	/**
	 * Sets the representative description aux.
	 *
	 * @param representativeDescriptionAux the new representative description aux
	 */
	public void setRepresentativeDescriptionAux(String representativeDescriptionAux) {
		this.representativeDescriptionAux = representativeDescriptionAux;
	}
	
	

	/**
	 * Gets the representative class aux.
	 *
	 * @return the representative class aux
	 */
	public Integer getRepresentativeClassAux() {
		return representativeClassAux;
	}

	/**
	 * Sets the representative class aux.
	 *
	 * @param representativeClassAux the new representative class aux
	 */
	public void setRepresentativeClassAux(Integer representativeClassAux) {
		this.representativeClassAux = representativeClassAux;
	}
	
	/**
	 * Gets the document source.
	 *
	 * @return the document source
	 */
	public Integer getDocumentSource() {
		return documentSource;
	}

	/**
	 * Sets the document source.
	 *
	 * @param documentSource the new document source
	 */
	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}

	
	/**
	 * Gets the document issuance date.
	 *
	 * @return the document issuance date
	 */
	public Integer getDocumentIssuanceDate() {
		return documentIssuanceDate;
	}

	/**
	 * Sets the document issuance date.
	 *
	 * @param documentIssuanceDate the new document issuance date
	 */
	public void setDocumentIssuanceDate(Integer documentIssuanceDate) {
		this.documentIssuanceDate = documentIssuanceDate;
	}

	/**
	 * Gets the authority legal number.
	 *
	 * @return the authority legal number
	 */
	public String getAuthorityLegalNumber() {
		return authorityLegalNumber;
	}

	/**
	 * Sets the authority legal number.
	 *
	 * @param authorityLegalNumber the new authority legal number
	 */
	public void setAuthorityLegalNumber(String authorityLegalNumber) {
		this.authorityLegalNumber = authorityLegalNumber;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();			
        if (loggerUser != null) {
        	if(loggerUser.getIdPrivilegeOfSystem() != null) {
        		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        	} else {
        		lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
        	}
            if(loggerUser.getAuditTime() != null) {
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null) {
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }            
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("legalRepresentativeFile",legalRepresentativeFile);
               
        return detailsMap;
	}
	
}