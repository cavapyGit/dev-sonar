package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * This Enum contains the Economic Sectors.
 */
public enum EconomicSectorType {
	
	/** The non financial public sector. */
	NON_FINANCIAL_PUBLIC_SECTOR(Integer.valueOf(38),"SECTOR PUBLICO"),
	
	/** The financial public sector. */
	FINANCIAL_PUBLIC_SECTOR(Integer.valueOf(39),"SECTOR PRIVADO"),
	
	/** The non financial private sector. */
	NON_FINANCIAL_PRIVATE_SECTOR(Integer.valueOf(40),"SECTOR PRIVADO NO FINANCIERO"),
	
	MIXTO(Integer.valueOf(1874),"MIXTO");
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new economic sector type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private EconomicSectorType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<EconomicSectorType> list = new ArrayList<EconomicSectorType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, EconomicSectorType> lookup = new HashMap<Integer, EconomicSectorType>();
	static {
		for (EconomicSectorType s : EnumSet.allOf(EconomicSectorType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the economic sector type
	 */
	public static EconomicSectorType get(Integer code) {
		return lookup.get(code);
	}

}
