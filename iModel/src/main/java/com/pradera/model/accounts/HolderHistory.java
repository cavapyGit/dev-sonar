package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.type.PersonType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_CREATION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/03/2013
 */
@Entity
@Table(name="HOLDER_HISTORY")
public class HolderHistory implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder history pk. */
	@Id
	@SequenceGenerator(name="HOLDER_HISTORY_IDHOLDERHISTORYPK_GENERATOR", sequenceName="SQ_ID_HOLDER_HISTORY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_HISTORY_IDHOLDERHISTORYPK_GENERATOR")
	@Column(name="ID_HOLDER_HISTORY_PK")  
	private Long idHolderHistoryPk;
	
	/** The Juridic class. */
	@Column(name="JURIDIC_CLASS")
	private Integer JuridicClass;

    /** The birth date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="BIRTH_DATE")
   
	private Date birthDate;
    
    /** The document number. */
    @Column(name="DOCUMENT_NUMBER")   
	private String documentNumber;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private Integer economicActivity;
	
	@Column(name="INVESTOR_TYPE")
	private Integer investorType;

	/** The economic sector. */
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;

	/** The email. */
	@Column(name="EMAIL")
	private String email;

	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;

	/** The first last name. */
	@Column(name="FIRST_LAST_NAME")
	private String firstLastName;
	
	/** The full name. */
	@Column(name="FULL_NAME")
	private String fullName;

	/** The holder type. */
	@Column(name="HOLDER_TYPE")
	private Integer holderType;
	
	/** The holder history type. */
	@Column(name="HOLDER_HISTORY_TYPE")
	private Integer holderHistoryType;
	
	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;

	/** The ind disabled. */
	@Column(name="IND_DISABLED")
	private Integer indDisabled;

	/** The ind residence. */
	@Column(name="IND_RESIDENCE")
	private Integer indResidence;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The legal address. */
	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;

	/** The legal department. */
	@Column(name="LEGAL_DEPARTMENT")
	private Integer legalDepartment;

	/** The legal district. */
	@Column(name="LEGAL_DISTRICT")
	private Integer legalDistrict;

	/** The legal province. */
	@Column(name="LEGAL_PROVINCE")
	private Integer legalProvince;

	/** The legal residence country. */
	@Column(name="LEGAL_RESIDENCE_COUNTRY")
	private Integer legalResidenceCountry;

	/** The mobile number. */
	@Column(name="MOBILE_NUMBER")
	private String mobileNumber;

	/** The name. */
	@Column(name="NAME")
	private String name;

	/** The nationality. */
	@Column(name="Nationality")
	private Integer nationality;

	/** The home phone number. */
	@Column(name="HOME_PHONE_NUMBER")
	private String homePhoneNumber;
	
	/** The office phone number. */
	@Column(name="OFFICE_PHONE_NUMBER")
	private String officePhoneNumber;

	/** The postal address. */
	@Column(name="POSTAL_ADDRESS")
	private String postalAddress;

	/** The postal department. */
	@Column(name="POSTAL_DEPARTMENT")
	private Integer postalDepartment;

	/** The postal district. */
	@Column(name="POSTAL_DISTRICT")
	private Integer postalDistrict;

	/** The postal province. */
	@Column(name="POSTAL_PROVINCE")
	private Integer postalProvince;

	/** The postal residence country. */
	@Column(name="POSTAL_RESIDENCE_COUNTRY")
	private Integer postalResidenceCountry;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The second document number. */
	@Column(name="SECOND_DOCUMENT_NUMBER")
	private String secondDocumentNumber;

	/** The second document type. */
	@Column(name="SECOND_DOCUMENT_TYPE")
	private Integer secondDocumentType;

	/** The second last name. */
	@Column(name="SECOND_LAST_NAME")
	private String secondLastName;

	/** The second nationality. */
	@Column(name="SECOND_NATIONALITY")
	private Integer secondNationality;

	/** The sex. */
	private Integer sex;

	/** The state holder history. */
	@Column(name="STATE_HOLDER_HISTORY")
	private Integer stateHolderHistory;
	
	/** The category. */
	@Column(name="CATEGORY")
	private Integer category;
	
	/** The role. */
	@Column(name="ROLE")
	private Integer role;
	
	/** The comments. */
	@Column(name="COMMENTS")
	private String comments;
	
	/** The beginning period. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="BEGINNING_PERIOD")
	private Date beginningPeriod;
	
	/** The ending period. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="ENDING_PERIOD")
	private Date endingPeriod;
	
	/** The ind minor. */
	@Column(name="IND_MINOR")
	private Integer indMinor;
	
	/** The ind pep. */
	@Column(name="IND_PEP")
	private Integer indPEP;
	
	
	/** The holder request. */
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_REQUEST_FK",referencedColumnName="ID_HOLDER_REQUEST_PK")
	private HolderRequest holderRequest;
	
	/** The document source. */
	@Column(name="DOCUMENT_SOURCE")
	private Integer documentSource;

	/** The document issuance date. */
	@Column(name="DOCUMENT_ISSUANCE_DATE")
	private Integer documentIssuanceDate;
	/** The type document description. */
	
	@Column(name="RELATED_CUI")
    private Long relatedCui;
    
    /** The fund administrator. */
    @Column(name="FUND_ADMINISTRATOR")
    private String fundAdministrator;
    
    /** The fund type. */
    @Column(name="FUND_TYPE")
    private String fundType;
    
    /** The transfer number. */
    @Column(name="TRANSFER_NUMBER")
    private String transferNumber;
	
    /** The mnemonic. */
    @Column(name="MNEMONIC")
	private String mnemonic;

    /** The married last name. */
    @Column(name="MARRIED_LAST_NAME")
    private String marriedLastName;
    
    /** The civil status. */
    @Column(name="CIVIL_STATUS")
    private Integer civilStatus;
    
    /** The main activity. */
    @Column(name="MAIN_ACTIVITY")
    private String mainActivity;
    
    /** The job source business name. */
    @Column(name="JOB_SOURCE_BUSINESS_NAME")
    private String jobSourceBusinessName;
    
//    @Column(name="JOB_SOURCE_ECONOMIC_ACTIVITY")
//    private String jobSourceEconomicActivity;
/** The job date admission. */
//    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="JOB_DATE_ADMISSION")
    private Date jobDateAdmission;
	
    /** The appointment. */
    @Column(name="APPOINTMENT")
    private String appointment;
	
    /** The job address. */
    @Column(name="JOB_ADDRESS")
    private String jobAddress;
    
//    @Column(name="OFFICE_EMAIL")
//    private String officeEmail;
/** The total income. */
//    
    @Column(name="TOTAL_INCOME")
    private Integer totalIncome;
    
//    @Column(name="OTHER_INCOME")
//    private BigDecimal otherIncome;
/** The per ref full name. */
//  
    @Column(name="PER_REF_FULL_NAME")
    private String perRefFullName;
    
    /** The per ref land line. */
    @Column(name="PER_REF_LANDLINE")
    private String perRefLandLine;
    
    /** The per ref cell phone. */
    @Column(name="PER_REF_CELLPHONE")
    private String perRefCellPhone;
    
    /** The comer ref business name. */
    @Column(name="COMER_REF_BUSINESS_NAME")
    private String comerRefBusinessName;
    
    /** The comer ref land line. */
    @Column(name="COMER_REF_LANDLINE")
    private String comerRefLandLine;

//    @Column(name="IND_ENTITY_PUBLIC_APPOINTMENT")
//    private Integer indEntityPublicAppointment;

//    @Column(name="ENTITY_PUBLIC")
//    private String entityPublic;
    
    /** The entity public appointment. */
@Column(name="ENTITY_PUBLIC_APPOINTMENT")
    private String entityPublicAppointment;
    
//    @Temporal(TemporalType.TIMESTAMP)
//	@Column(name="ENTITY_PUBLIC_DATE_INITIAL")
//    private Date entityPublicDateInitial;
//    
//    @Temporal(TemporalType.TIMESTAMP)
//	@Column(name="ENTITY_PUBLIC_DATE_END")
//    private Date entityPublicDateEnd;
    
    /** The nit natural person. */
@Column(name="NIT_NATURAL_PERSON")
    private Integer nitNaturalPerson;
    
    /** The website. */
    @Column(name="WEBSITE")
	private String website;
    
    /** The fund company enrollment. */
    @Column(name="FUNDCOMPANY_ENROLLMENT")
    private String fundCompanyEnrollment;
    
    /** The constitution date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="CONSTITUTION_DATE")
    private Date constitutionDate;
    
    /** The file photo. */
    @Lob()
   	@Column(name="FILE_PHOTO",updatable=true)
   	private byte[] filePhoto;
    
    /** The file signing. */
    @Lob()
   	@Column(name="FILE_SIGNING",updatable=true)
   	private byte[] fileSigning;
    
    /** The ind can Negotiate. */
	@Column(name="IND_CAN_NEGOTIATE")
	private Integer indCanNegotiate;
	
	/** The ind sirtex neg. */
	@Column(name="IND_SIRTEX_NEG")
	private Integer indSirtexNeg;
	
	/** The type document description. */
    @Transient
	private String typeDocumentDescription;
	
	/** The indicator disabled. */
	@Transient
	private boolean indicatorDisabled;
	
	/** The indicator minor. */
	@Transient
	private boolean indicatorMinor;
	
	 /** The description holder. */
 	@Transient
	 private String descriptionHolder;
	 
	 /** The indicator pep. */
 	@Transient
	 private boolean indicatorPep;
 	
 	@Column(name="IND_FATCA")
 	private Integer indFatca;
 	
 	@Column(name="FILE_PHOTO_NAME")
 	private String filePhotoName;
 	
 	@Column(name="GRADE")
 	private Integer grade;
 	
 	@Column(name="PEP_RELATED_NAME")
 	private String pepRelatedName;
// 	@Transient
// 	private boolean indicatorEntityPublicAppointment;
	
	
	
    /**
     * Gets the id holder history pk.
     *
     * @return the id holder history pk
     */
    public Long getIdHolderHistoryPk() {
		return idHolderHistoryPk;
	}





	/**
	 * Sets the id holder history pk.
	 *
	 * @param idHolderHistoryPk the new id holder history pk
	 */
	public void setIdHolderHistoryPk(Long idHolderHistoryPk) {
		this.idHolderHistoryPk = idHolderHistoryPk;
	}

	/**
	 * Gets the birth date.
	 *
	 * @return the birth date
	 */
	public Date getBirthDate() {
		return this.birthDate;
	}

	/**
	 * Sets the birth date.
	 *
	 * @param birthDate the new birth date
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return this.documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public Integer getInvestorType() {
		return investorType;
	}

	public void setInvestorType(Integer investorType) {
		this.investorType = investorType;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public Integer getEconomicActivity() {
		return this.economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the new economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return this.economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return this.faxNumber;
	}

	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the new fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Gets the first last name.
	 *
	 * @return the first last name
	 */
	public String getFirstLastName() {
		return this.firstLastName;
	}

	/**
	 * Sets the first last name.
	 *
	 * @param firstLastName the new first last name
	 */
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return this.fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Gets the holder type.
	 *
	 * @return the holder type
	 */
	public Integer getHolderType() {
		return this.holderType;
	}

	/**
	 * Sets the holder type.
	 *
	 * @param holderType the new holder type
	 */
	public void setHolderType(Integer holderType) {
		this.holderType = holderType;
	}

	/**
	 * Gets the ind disabled.
	 *
	 * @return the ind disabled
	 */
	public Integer getIndDisabled() {	
		
		if(indDisabled != null && indDisabled.equals(BooleanType.YES.getCode())){
			indicatorDisabled = true;
		}else{
			indicatorDisabled = false;
		}
		
		return this.indDisabled;
	}

	/**
	 * Sets the ind disabled.
	 *
	 * @param indDisabled the new ind disabled
	 */
	public void setIndDisabled(Integer indDisabled) {
		this.indDisabled = indDisabled;
	}

	/**
	 * Gets the ind residence.
	 *
	 * @return the ind residence
	 */
	public Integer getIndResidence() {
		return this.indResidence;
	}

	/**
	 * Sets the ind residence.
	 *
	 * @param indResidence the new ind residence
	 */
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the legal address.
	 *
	 * @return the legal address
	 */
	public String getLegalAddress() {
		return this.legalAddress;
	}

	/**
	 * Sets the legal address.
	 *
	 * @param legalAddress the new legal address
	 */
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	/**
	 * Gets the legal department.
	 *
	 * @return the legal department
	 */
	public Integer getLegalDepartment() {
		return this.legalDepartment;
	}

	/**
	 * Sets the legal department.
	 *
	 * @param legalDepartment the new legal department
	 */
	public void setLegalDepartment(Integer legalDepartment) {
		this.legalDepartment = legalDepartment;
	}

	/**
	 * Gets the legal district.
	 *
	 * @return the legal district
	 */
	public Integer getLegalDistrict() {
		return this.legalDistrict;
	}

	/**
	 * Sets the legal district.
	 *
	 * @param legalDistrict the new legal district
	 */
	public void setLegalDistrict(Integer legalDistrict) {
		this.legalDistrict = legalDistrict;
	}

	/**
	 * Gets the legal province.
	 *
	 * @return the legal province
	 */
	public Integer getLegalProvince() {
		return this.legalProvince;
	}

	/**
	 * Sets the legal province.
	 *
	 * @param legalProvince the new legal province
	 */
	public void setLegalProvince(Integer legalProvince) {
		this.legalProvince = legalProvince;
	}

	/**
	 * Gets the legal residence country.
	 *
	 * @return the legal residence country
	 */
	public Integer getLegalResidenceCountry() {
		return this.legalResidenceCountry;
	}

	/**
	 * Sets the legal residence country.
	 *
	 * @param legalResidenceCountry the new legal residence country
	 */
	public void setLegalResidenceCountry(Integer legalResidenceCountry) {
		this.legalResidenceCountry = legalResidenceCountry;
	}

	

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the nationality.
	 *
	 * @return the nationality
	 */
	public Integer getNationality() {
		return this.nationality;
	}

	/**
	 * Sets the nationality.
	 *
	 * @param nationality the new nationality
	 */
	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}



	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}



	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}



	/**
	 * Gets the home phone number.
	 *
	 * @return the home phone number
	 */
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}



	/**
	 * Sets the home phone number.
	 *
	 * @param homePhoneNumber the new home phone number
	 */
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}



	/**
	 * Gets the postal address.
	 *
	 * @return the postal address
	 */
	public String getPostalAddress() {
		return this.postalAddress;
	}

	/**
	 * Sets the postal address.
	 *
	 * @param postalAddress the new postal address
	 */
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	/**
	 * Gets the postal department.
	 *
	 * @return the postal department
	 */
	public Integer getPostalDepartment() {
		return this.postalDepartment;
	}

	/**
	 * Sets the postal department.
	 *
	 * @param postalDepartment the new postal department
	 */
	public void setPostalDepartment(Integer postalDepartment) {
		this.postalDepartment = postalDepartment;
	}

	/**
	 * Gets the postal district.
	 *
	 * @return the postal district
	 */
	public Integer getPostalDistrict() {
		return this.postalDistrict;
	}

	/**
	 * Sets the postal district.
	 *
	 * @param postalDistrict the new postal district
	 */
	public void setPostalDistrict(Integer postalDistrict) {
		this.postalDistrict = postalDistrict;
	}

	/**
	 * Gets the postal province.
	 *
	 * @return the postal province
	 */
	public Integer getPostalProvince() {
		return this.postalProvince;
	}

	/**
	 * Sets the postal province.
	 *
	 * @param postalProvince the new postal province
	 */
	public void setPostalProvince(Integer postalProvince) {
		this.postalProvince = postalProvince;
	}

	/**
	 * Gets the postal residence country.
	 *
	 * @return the postal residence country
	 */
	public Integer getPostalResidenceCountry() {
		return this.postalResidenceCountry;
	}

	/**
	 * Sets the postal residence country.
	 *
	 * @param postalResidenceCountry the new postal residence country
	 */
	public void setPostalResidenceCountry(Integer postalResidenceCountry) {
		this.postalResidenceCountry = postalResidenceCountry;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the second document number.
	 *
	 * @return the second document number
	 */
	public String getSecondDocumentNumber() {
		return this.secondDocumentNumber;
	}

	/**
	 * Sets the second document number.
	 *
	 * @param secondDocumentNumber the new second document number
	 */
	public void setSecondDocumentNumber(String secondDocumentNumber) {
		this.secondDocumentNumber = secondDocumentNumber;
	}

	/**
	 * Gets the second document type.
	 *
	 * @return the second document type
	 */
	public Integer getSecondDocumentType() {
		return this.secondDocumentType;
	}

	/**
	 * Sets the second document type.
	 *
	 * @param secondDocumentType the new second document type
	 */
	public void setSecondDocumentType(Integer secondDocumentType) {
		this.secondDocumentType = secondDocumentType;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return this.secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
	 * Gets the second nationality.
	 *
	 * @return the second nationality
	 */
	public Integer getSecondNationality() {
		return this.secondNationality;
	}

	/**
	 * Sets the second nationality.
	 *
	 * @param secondNationality the new second nationality
	 */
	public void setSecondNationality(Integer secondNationality) {
		this.secondNationality = secondNationality;
	}

	/**
	 * Gets the sex.
	 *
	 * @return the sex
	 */
	public Integer getSex() {
		return this.sex;
	}

	/**
	 * Sets the sex.
	 *
	 * @param sex the new sex
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	

	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return this.holderRequest;
	}

	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}

	

	/**
	 * Gets the type document description.
	 *
	 * @return the type document description
	 */
	public String getTypeDocumentDescription() {
		
		return typeDocumentDescription;
	}

	/**
	 * Sets the type document description.
	 *
	 * @param typeDocumentDescription the new type document description
	 */
	public void setTypeDocumentDescription(String typeDocumentDescription) {
		this.typeDocumentDescription = typeDocumentDescription;
	}


	/**
	 * Gets the juridic class.
	 *
	 * @return the juridic class
	 */
	public Integer getJuridicClass() {
		return JuridicClass;
	}

	/**
	 * Sets the juridic class.
	 *
	 * @param juridicClass the new juridic class
	 */
	public void setJuridicClass(Integer juridicClass) {
		JuridicClass = juridicClass;
	}

	/**
	 * Gets the state holder history.
	 *
	 * @return the state holder history
	 */
	public Integer getStateHolderHistory() {
		return stateHolderHistory;
	}

	/**
	 * Sets the state holder history.
	 *
	 * @param stateHolderHistory the new state holder history
	 */
	public void setStateHolderHistory(Integer stateHolderHistory) {
		this.stateHolderHistory = stateHolderHistory;
	}

	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public Integer getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(Integer category) {
		this.category = category;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	

	/**
	 * Gets the beginning period.
	 *
	 * @return the beginning period
	 */
	public Date getBeginningPeriod() {
		return beginningPeriod;
	}



	/**
	 * Sets the beginning period.
	 *
	 * @param beginningPeriod the new beginning period
	 */
	public void setBeginningPeriod(Date beginningPeriod) {
		this.beginningPeriod = beginningPeriod;
	}



	/**
	 * Gets the ending period.
	 *
	 * @return the ending period
	 */
	public Date getEndingPeriod() {
		return endingPeriod;
	}

	/**
	 * Sets the ending period.
	 *
	 * @param endingPeriod the new ending period
	 */
	public void setEndingPeriod(Date endingPeriod) {
		this.endingPeriod = endingPeriod;
	}

	/**
	 * Gets the ind minor.
	 *
	 * @return the ind minor
	 */
	public Integer getIndMinor() {		
		
		return indMinor;
	}

	/**
	 * Sets the ind minor.
	 *
	 * @param indMinor the new ind minor
	 */
	public void setIndMinor(Integer indMinor) {
		this.indMinor = indMinor;
	}

	/**
	 * Checks if is indicator disabled.
	 *
	 * @return true, if is indicator disabled
	 */
	public boolean isIndicatorDisabled() {
		return indicatorDisabled;
	}
	
	/**
	 * Gets the office phone number.
	 *
	 * @return the office phone number
	 */
	public String getOfficePhoneNumber() {
		return officePhoneNumber;
	}



	/**
	 * Sets the office phone number.
	 *
	 * @param officePhoneNumber the new office phone number
	 */
	public void setOfficePhoneNumber(String officePhoneNumber) {
		this.officePhoneNumber = officePhoneNumber;
	}
	
	
	



	/**
	 * Gets the holder history type.
	 *
	 * @return the holder history type
	 */
	public Integer getHolderHistoryType() {
		return holderHistoryType;
	}





	/**
	 * Sets the holder history type.
	 *
	 * @param holderHistoryType the new holder history type
	 */
	public void setHolderHistoryType(Integer holderHistoryType) {
		this.holderHistoryType = holderHistoryType;
	}





	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return registryType;
	}





	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	
	




	/**
	 * Gets the ind pep.
	 *
	 * @return the ind pep
	 */
	public Integer getIndPEP() {	
		
		return indPEP;
	}





	/**
	 * Sets the ind pep.
	 *
	 * @param indPEP the new ind pep
	 */
	public void setIndPEP(Integer indPEP) {
		this.indPEP = indPEP;
	}


	

	/**
	 * Gets the document source.
	 *
	 * @return the document source
	 */
	public Integer getDocumentSource() {
		return documentSource;
	}





	/**
	 * Sets the document source.
	 *
	 * @param documentSource the new document source
	 */
	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}





	/**
	 * Sets the indicator disabled.
	 *
	 * @param indicatorDisabled the new indicator disabled
	 */
	public void setIndicatorDisabled(boolean indicatorDisabled) {
		this.indicatorDisabled = indicatorDisabled;
		
		if(indicatorDisabled){
			indDisabled=BooleanType.YES.getCode();
		}else{
			indDisabled=BooleanType.NO.getCode();
		}
	}

	/**
	 * Checks if is indicator minor.
	 *
	 * @return true, if is indicator minor
	 */
	public boolean isIndicatorMinor() {
		return indicatorMinor;
	}

	/**
	 * Sets the indicator minor.
	 *
	 * @param indicatorMinor the new indicator minor
	 */
	public void setIndicatorMinor(boolean indicatorMinor) {
		this.indicatorMinor = indicatorMinor;
		
		if(indicatorMinor){
			indMinor=BooleanType.YES.getCode();
		}else{
			indMinor=BooleanType.NO.getCode();
		}
	}





	/**
	 * Gets the description holder.
	 *
	 * @return the description holder
	 */
	public String getDescriptionHolder() {

		if(holderType.equals(PersonType.NATURAL.getCode())){
			if(secondLastName!=null){
				descriptionHolder = firstLastName+" "+secondLastName+","+name;
			}
			else{
				descriptionHolder = firstLastName+","+name;
			}
		 
		}
		else if(holderType.equals(PersonType.JURIDIC.getCode())){
		 descriptionHolder = fullName;	
		}
		
		return descriptionHolder;

	}





	/**
	 * Sets the description holder.
	 *
	 * @param descriptionHolder the new description holder
	 */
	public void setDescriptionHolder(String descriptionHolder) {
		this.descriptionHolder = descriptionHolder;
	}





	/**
	 * Checks if is indicator pep.
	 *
	 * @return true, if is indicator pep
	 */
	public boolean isIndicatorPep() {
		return indicatorPep;
	}





	/**
	 * Sets the indicator pep.
	 *
	 * @param indicatorPep the new indicator pep
	 */
	public void setIndicatorPep(boolean indicatorPep) {
		this.indicatorPep = indicatorPep;
		
		if(indicatorPep){
			indPEP = BooleanType.YES.getCode();
		}else{
		    indPEP = BooleanType.NO.getCode();	
		}
		
	}
	
	

	/**
	 * Gets the document issuance date.
	 *
	 * @return the document issuance date
	 */
	public Integer getDocumentIssuanceDate() {
		return documentIssuanceDate;
	}





	/**
	 * Sets the document issuance date.
	 *
	 * @param documentIssuanceDate the new document issuance date
	 */
	public void setDocumentIssuanceDate(Integer documentIssuanceDate) {
		this.documentIssuanceDate = documentIssuanceDate;
	}
	
	
	
	





	/**
	 * Gets the related cui.
	 *
	 * @return the related cui
	 */
	public Long getRelatedCui() {
		return relatedCui;
	}





	/**
	 * Sets the related cui.
	 *
	 * @param relatedCui the new related cui
	 */
	public void setRelatedCui(Long relatedCui) {
		this.relatedCui = relatedCui;
	}





	/**
	 * Gets the fund administrator.
	 *
	 * @return the fund administrator
	 */
	public String getFundAdministrator() {
		return fundAdministrator;
	}





	/**
	 * Sets the fund administrator.
	 *
	 * @param fundAdministrator the new fund administrator
	 */
	public void setFundAdministrator(String fundAdministrator) {
		this.fundAdministrator = fundAdministrator;
	}





	/**
	 * Gets the fund type.
	 *
	 * @return the fund type
	 */
	public String getFundType() {
		return fundType;
	}





	/**
	 * Sets the fund type.
	 *
	 * @param fundType the new fund type
	 */
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	/**
	 * Gets the transfer number.
	 *
	 * @return the transfer number
	 */
	public String getTransferNumber() {
		return transferNumber;
	}

	/**
	 * Sets the transfer number.
	 *
	 * @param transferNumber the new transfer number
	 */
	public void setTransferNumber(String transferNumber) {
		this.transferNumber = transferNumber;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();
        if (loggerUser != null) {
        	if(loggerUser.getIdPrivilegeOfSystem() != null) {
        		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        	} else {
        		lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
        	}
            if(loggerUser.getAuditTime() != null){
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getAuditTime() != null){
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getAuditTime() != null){
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }            
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }


	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}





	/**
	 * Gets the married last name.
	 *
	 * @return the married last name
	 */
	public String getMarriedLastName() {
		return marriedLastName;
	}





	/**
	 * Sets the married last name.
	 *
	 * @param marriedLastName the new married last name
	 */
	public void setMarriedLastName(String marriedLastName) {
		this.marriedLastName = marriedLastName;
	}





	/**
	 * Gets the civil status.
	 *
	 * @return the civil status
	 */
	public Integer getCivilStatus() {
		return civilStatus;
	}





	/**
	 * Sets the civil status.
	 *
	 * @param civilStatus the new civil status
	 */
	public void setCivilStatus(Integer civilStatus) {
		this.civilStatus = civilStatus;
	}





	/**
	 * Gets the main activity.
	 *
	 * @return the main activity
	 */
	public String getMainActivity() {
		return mainActivity;
	}





	/**
	 * Sets the main activity.
	 *
	 * @param mainActivity the new main activity
	 */
	public void setMainActivity(String mainActivity) {
		this.mainActivity = mainActivity;
	}





	/**
	 * Gets the job source business name.
	 *
	 * @return the job source business name
	 */
	public String getJobSourceBusinessName() {
		return jobSourceBusinessName;
	}





	/**
	 * Sets the job source business name.
	 *
	 * @param jobSourceBusinessName the new job source business name
	 */
	public void setJobSourceBusinessName(String jobSourceBusinessName) {
		this.jobSourceBusinessName = jobSourceBusinessName;
	}

	/**
	 * Gets the job date admission.
	 *
	 * @return the job date admission
	 */
	public Date getJobDateAdmission() {
		return jobDateAdmission;
	}

	/**
	 * Sets the job date admission.
	 *
	 * @param jobDateAdmission the new job date admission
	 */
	public void setJobDateAdmission(Date jobDateAdmission) {
		this.jobDateAdmission = jobDateAdmission;
	}


	/**
	 * Gets the appointment.
	 *
	 * @return the appointment
	 */
	public String getAppointment() {
		return appointment;
	}

	/**
	 * Sets the appointment.
	 *
	 * @param appointment the new appointment
	 */
	public void setAppointment(String appointment) {
		this.appointment = appointment;
	}


	/**
	 * Gets the job address.
	 *
	 * @return the job address
	 */
	public String getJobAddress() {
		return jobAddress;
	}


	/**
	 * Sets the job address.
	 *
	 * @param jobAddress the new job address
	 */
	public void setJobAddress(String jobAddress) {
		this.jobAddress = jobAddress;
	}


	/**
	 * Gets the total income.
	 *
	 * @return the total income
	 */
	public Integer getTotalIncome() {
		return totalIncome;
	}

	/**
	 * Sets the total income.
	 *
	 * @param totalIncome the new total income
	 */
	public void setTotalIncome(Integer totalIncome) {
		this.totalIncome = totalIncome;
	}

	/**
	 * Gets the per ref full name.
	 *
	 * @return the per ref full name
	 */
	public String getPerRefFullName() {
		return perRefFullName;
	}

	/**
	 * Sets the per ref full name.
	 *
	 * @param perRefFullName the new per ref full name
	 */
	public void setPerRefFullName(String perRefFullName) {
		this.perRefFullName = perRefFullName;
	}

	/**
	 * Gets the per ref land line.
	 *
	 * @return the per ref land line
	 */
	public String getPerRefLandLine() {
		return perRefLandLine;
	}

	/**
	 * Sets the per ref land line.
	 *
	 * @param perRefLandLine the new per ref land line
	 */
	public void setPerRefLandLine(String perRefLandLine) {
		this.perRefLandLine = perRefLandLine;
	}

	/**
	 * Gets the per ref cell phone.
	 *
	 * @return the per ref cell phone
	 */
	public String getPerRefCellPhone() {
		return perRefCellPhone;
	}

	/**
	 * Sets the per ref cell phone.
	 *
	 * @param perRefCellPhone the new per ref cell phone
	 */
	public void setPerRefCellPhone(String perRefCellPhone) {
		this.perRefCellPhone = perRefCellPhone;
	}


	/**
	 * Gets the comer ref business name.
	 *
	 * @return the comer ref business name
	 */
	public String getComerRefBusinessName() {
		return comerRefBusinessName;
	}


	/**
	 * Sets the comer ref business name.
	 *
	 * @param comerRefBusinessName the new comer ref business name
	 */
	public void setComerRefBusinessName(String comerRefBusinessName) {
		this.comerRefBusinessName = comerRefBusinessName;
	}


	/**
	 * Gets the comer ref land line.
	 *
	 * @return the comer ref land line
	 */
	public String getComerRefLandLine() {
		return comerRefLandLine;
	}


	/**
	 * Sets the comer ref land line.
	 *
	 * @param comerRefLandLine the new comer ref land line
	 */
	public void setComerRefLandLine(String comerRefLandLine) {
		this.comerRefLandLine = comerRefLandLine;
	}


	/**
	 * Gets the entity public appointment.
	 *
	 * @return the entity public appointment
	 */
	public String getEntityPublicAppointment() {
		return entityPublicAppointment;
	}


	/**
	 * Sets the entity public appointment.
	 *
	 * @param entityPublicAppointment the new entity public appointment
	 */
	public void setEntityPublicAppointment(String entityPublicAppointment) {
		this.entityPublicAppointment = entityPublicAppointment;
	}


//	public boolean isIndicatorEntityPublicAppointment() {
//		return indicatorEntityPublicAppointment;
//	}





	/**
 * Gets the website.
 *
 * @return the website
 */
public String getWebsite() {
		return website;
	}

	/**
	 * Sets the website.
	 *
	 * @param website the new website
	 */
	public void setWebsite(String website) {
		this.website = website;
	}





	/**
	 * Gets the nit natural person.
	 *
	 * @return the nit natural person
	 */
	public Integer getNitNaturalPerson() {
		return nitNaturalPerson;
	}





	/**
	 * Sets the nit natural person.
	 *
	 * @param nitNaturalPerson the new nit natural person
	 */
	public void setNitNaturalPerson(Integer nitNaturalPerson) {
		this.nitNaturalPerson = nitNaturalPerson;
	}





	/**
	 * Gets the fund company enrollment.
	 *
	 * @return the fund company enrollment
	 */
	public String getFundCompanyEnrollment() {
		return fundCompanyEnrollment;
	}


	/**
	 * Sets the fund company enrollment.
	 *
	 * @param fundCompanyEnrollment the new fund company enrollment
	 */
	public void setFundCompanyEnrollment(String fundCompanyEnrollment) {
		this.fundCompanyEnrollment = fundCompanyEnrollment;
	}

	/**
	 * Gets the constitution date.
	 *
	 * @return the constitution date
	 */
	public Date getConstitutionDate() {
		return constitutionDate;
	}

	/**
	 * Sets the constitution date.
	 *
	 * @param constitutionDate the new constitution date
	 */
	public void setConstitutionDate(Date constitutionDate) {
		this.constitutionDate = constitutionDate;
	}

	/**
	 * Gets the file photo.
	 *
	 * @return the filePhoto
	 */
	public byte[] getFilePhoto() {
		return filePhoto;
	}

	/**
	 * Sets the file photo.
	 *
	 * @param filePhoto the filePhoto to set
	 */
	public void setFilePhoto(byte[] filePhoto) {
		this.filePhoto = filePhoto;
	}

	/**
	 * Gets the file signing.
	 *
	 * @return the fileSigning
	 */
	public byte[] getFileSigning() {
		return fileSigning;
	}

	/**
	 * Sets the file signing.
	 *
	 * @param fileSigning the fileSigning to set
	 */
	public void setFileSigning(byte[] fileSigning) {
		this.fileSigning = fileSigning;
	}
	
	public Integer getIndCanNegotiate() {
			return indCanNegotiate;
		}

	public void setIndCanNegotiate(Integer indCanNegotiate) {
		this.indCanNegotiate = indCanNegotiate;
	}
	
	/**
	 * sirtex negotiate CUI
	 * **/
	public Integer getIndSirtexNeg() {
		return indSirtexNeg;
	}

	public void setIndSirtexNeg(Integer indSirtexNeg) {
		this.indSirtexNeg = indSirtexNeg;
	}

	/**
	 * @return the indFatca
	 */
	public Integer getIndFatca() {
		return indFatca;
	}

	/**
	 * @param indFatca the indFatca to set
	 */
	public void setIndFatca(Integer indFatca) {
		this.indFatca = indFatca;
	}
	
	/**
	 * @return the filePhotoName
	 */
	public String getFilePhotoName() {
		return filePhotoName;
	}

	/**
	 * @param filePhotoName the filePhotoName to set
	 */
	public void setFilePhotoName(String filePhotoName) {
		this.filePhotoName = filePhotoName;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getPepRelatedName() {
		return pepRelatedName;
	}

	public void setPepRelatedName(String pepRelatedName) {
		this.pepRelatedName = pepRelatedName;
	}
	
}