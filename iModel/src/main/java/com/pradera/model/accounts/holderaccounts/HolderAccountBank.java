package com.pradera.model.accounts.holderaccounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.generalparameter.type.CurrencyType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT_BANK database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/02/2013
 */
@Entity
@Table(name="HOLDER_ACCOUNT_BANK")
public class HolderAccountBank implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder account bank pk. */
	@Id
	@SequenceGenerator(name="SQ_HOLDER_ACCOUNT_BANK_PK", sequenceName="SQ_ID_HOLDER_ACCOUNT_BANK_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLDER_ACCOUNT_BANK_PK")
	@Column(name="ID_HOLDER_ACCOUNT_BANK_PK")
	private Long idHolderAccountBankPk;

	/** The bank account number. */
	@Column(name="BANK_ACCOUNT_NUMBER")
	private String bankAccountNumber;

	/** The bank account type. */
	@Column(name="BANK_ACCOUNT_TYPE")
	private Integer bankAccountType;

	/** The currency. */
	private Integer currency;

	/** The bank. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BANK_FK")
	private Bank bank;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state account bank. */
	@Column(name="STATE_ACCOUNT_BANK")
	private Integer stateAccountBank;
	
	/** The description bank. */
	@Column(name="DESCRIPTION")
	private String descriptionBank;	
	
	/** The have bic. */
	@Column(name="HAVE_BIC")
	private Integer haveBic;

	/** HOLDERS. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;

	//bi-directional many-to-one association to HolderAccount
	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@Column(name="CODIGO_SWIFT")
	private String swiftCode;
	
	@Transient
	private Integer originalStateAccountBank;
	
    /**
     * Instantiates a new holder account bank.
     */
    public HolderAccountBank() {
    }
    
    /**
     * Gets the bank account type description.
     *
     * @return the bank account type description
     */
    public String getBankAccountTypeDescription() {
		return HolderAccountBankType.lookup.get(this.getBankAccountType()).getValue();
	}
    
    /**
     * Gets the currency description.
     *
     * @return the currency description
     */
    public String getCurrencyDescription() {
		return CurrencyType.lookup.get(this.getCurrency()).getCodeIso();
	}
    
    /**
     * Gets the state description.
     *
     * @return the state description
     */
    public String getStateDescription() {
		return HolderAccountStateBankType.lookup.get(this.getStateAccountBank()).getDescription();
	}
    
    /**
     * Checks if is annulate.
     *
     * @return true, if is annulate
     */
    public boolean isAnnulate(){
    	return Validations.validateIsNullOrEmpty(this.getStateAccountBank())?false:this.getStateAccountBank().equals(HolderAccountStateBankType.REGISTERED.getCode())?true:false;
    }	

	/**
	 * Gets the have bic.
	 *
	 * @return the have bic
	 */
	public Integer getHaveBic() {
		return haveBic;
	}

	/**
	 * Sets the have bic.
	 *
	 * @param haveBic the new have bic
	 */
	public void setHaveBic(Integer haveBic) {
		this.haveBic = haveBic;
	}

	/**
	 * Gets the description bank.
	 *
	 * @return the description bank
	 */
	public String getDescriptionBank() {
		return descriptionBank;
	}

	/**
	 * Sets the description bank.
	 *
	 * @param descriptionBank the new description bank
	 */
	public void setDescriptionBank(String descriptionBank) {
		this.descriptionBank = descriptionBank;
	}

//	/**
//	 * Gets the doc designation.
//	 *
//	 * @return the doc designation
//	 */
//	public byte[] getDocDesignation() {
//		return docDesignation;
//	}
//
//	/**
//	 * Sets the doc designation.
//	 *
//	 * @param docDesignation the new doc designation
//	 */
//	public void setDocDesignation(byte[] docDesignation) {
//		this.docDesignation = docDesignation;
//	}

	/**
	 * Gets the id holder account bank pk.
	 *
	 * @return the id holder account bank pk
	 */
	public Long getIdHolderAccountBankPk() {
		return this.idHolderAccountBankPk;
	}

	/**
	 * Sets the id holder account bank pk.
	 *
	 * @param idHolderAccountBankPk the new id holder account bank pk
	 */
	public void setIdHolderAccountBankPk(Long idHolderAccountBankPk) {
		this.idHolderAccountBankPk = idHolderAccountBankPk;
	}

	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return this.bankAccountNumber;
	}

	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	/**
	 * Gets the bank account type.
	 *
	 * @return the bank account type
	 */
	public Integer getBankAccountType() {
		return this.bankAccountType;
	}

	/**
	 * Sets the bank account type.
	 *
	 * @param bankAccountType the new bank account type
	 */
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return this.currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public Bank getBank() {
		return bank;
	}

	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(Bank bank) {
		this.bank = bank;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state account bank.
	 *
	 * @return the state account bank
	 */
	public Integer getStateAccountBank() {
		return this.stateAccountBank;
	}

	/**
	 * Sets the state account bank.
	 *
	 * @param stateAccountBank the new state account bank
	 */
	public void setStateAccountBank(Integer stateAccountBank) {
		this.stateAccountBank = stateAccountBank;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return this.holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the swiftCode
	 */
	public String getSwiftCode() {
		return swiftCode;
	}

	/**
	 * @param swiftCode the swiftCode to set
	 */
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public Integer getOriginalStateAccountBank() {
		return originalStateAccountBank;
	}

	public void setOriginalStateAccountBank(Integer originalStateAccountBank) {
		this.originalStateAccountBank = originalStateAccountBank;
	}
	
}