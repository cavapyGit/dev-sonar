package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum UnableForeignNoResidentDocHolderNatType {
		
		CE(Integer.valueOf(997),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(998),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(999),"PASAPORTE"), 
		LIC(Integer.valueOf(1000),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(1001),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(1002),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(1003),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(1004),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(1005),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(1006),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(1007),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(1008),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(1009),"DECRETO"),		
	    RCF(Integer.valueOf(1010),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(1011),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(1012),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    private Integer code;
		private String value;
		
		public static final List<UnableForeignNoResidentDocHolderNatType> list = new ArrayList<UnableForeignNoResidentDocHolderNatType>();
		public static final Map<Integer, UnableForeignNoResidentDocHolderNatType> lookup = new HashMap<Integer, UnableForeignNoResidentDocHolderNatType>();

		static {
			for (UnableForeignNoResidentDocHolderNatType s : EnumSet.allOf(UnableForeignNoResidentDocHolderNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private UnableForeignNoResidentDocHolderNatType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static UnableForeignNoResidentDocHolderNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
