package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum IssuerDocRepreNatType {
		
		POR(Integer.valueOf(338),"PODER OTORGADO COMO REPRESENTANTE",Integer.valueOf(192)),
		CI(Integer.valueOf(339),"CEDULA DE IDENTIDAD",Integer.valueOf(192)), 
		PAS(Integer.valueOf(340),"PASAPORTE",Integer.valueOf(192)),
		LIC(Integer.valueOf(341),"LICENCIA DE CONDUCIR",Integer.valueOf(192)),
		DIO(Integer.valueOf(342),"DOCUMENTO DE IDENTIDAD DE PAIS ORIGEN",Integer.valueOf(192)),
		RNC(Integer.valueOf(343),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(192)), 
		DJCP(Integer.valueOf(344),"DECLARACION JURADA SOBRE CONDICION DE PEP",Integer.valueOf(192)),
	    OC(Integer.valueOf(345),"ORDEN DE CUSTODIA",Integer.valueOf(192)),
		CATL(Integer.valueOf(346),"DOCUMENTO DE ACREDITACION COMO TUTOR LEGAL",Integer.valueOf(192)),
		CDRL(Integer.valueOf(347),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(192));	
		
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<IssuerDocRepreNatType> list = new ArrayList<IssuerDocRepreNatType>();
		public static final Map<Integer, IssuerDocRepreNatType> lookup = new HashMap<Integer, IssuerDocRepreNatType>();

		static {
			for (IssuerDocRepreNatType s : EnumSet.allOf(IssuerDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private IssuerDocRepreNatType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static IssuerDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
