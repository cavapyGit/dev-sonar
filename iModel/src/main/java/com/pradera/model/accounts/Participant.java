package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.constants.Constants;
import com.pradera.model.corporatives.stockcalculation.validation.StockByParticipant;
import com.pradera.model.custody.channelopening.ChannelOpening;
import com.pradera.model.custody.participantunion.ParticipantUnionOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegParticipaStruct;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PARTICIPANT database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/01/2013
 */

@Entity
@Cacheable(false)
@NamedQueries({
	@NamedQuery(name = Participant.PARTICIPANT, query = "Select distinct p From Participant p WHERE p.state = :state and p.accountClass = :accountClass order by p.mnemonic asc"),
	@NamedQuery(name = Participant.PARTICIPANT_STATE, query = "SELECT new com.pradera.model.accounts.Participant(p.idParticipantPk, p.state) FROM Participant p WHERE p.idParticipantPk = :idParticipantPkParam "),
	@NamedQuery(name = Participant.PARTICIPANT_MNEMONIC, query = "SELECT new com.pradera.model.accounts.Participant(p.idParticipantPk, p.state) FROM Participant p WHERE p.mnemonic = :idMnemonicParam "),
	@NamedQuery(name = Participant.PARTICIPANT_ID_BCB_CODE, query = "SELECT new com.pradera.model.accounts.Participant(p.idParticipantPk, p.mnemonic, p.state) FROM Participant p WHERE p.idBcbCode = :idBcbCode ")
	
})
public class Participant implements Serializable, Auditable  {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant PARTICIPANT. */
	public static final String PARTICIPANT = "Participant.searchAllParticipant";
	
	/** The Constant PARTICIPANT_STATE. */
	public static final String PARTICIPANT_STATE = "Participant.searchParticipantState";
	
	public static final String PARTICIPANT_MNEMONIC = "Participant.searchParticipantMnemonic";
	
	/** Constante ID_BCB_CODE */
	public static final String PARTICIPANT_ID_BCB_CODE = "Participant.searchParticipantIdBcbCode";

	/** The id participant pk. */
	@Id
	@SequenceGenerator(name="PARTICIPANT_IDPARTICIPANTPK_GENERATOR", sequenceName="SQ_ID_PARTICIPANT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_IDPARTICIPANTPK_GENERATOR")
	@Column(name="ID_PARTICIPANT_PK")
	@NotNull(groups={StockByParticipant.class})
	private Long idParticipantPk;

	/** The account class. */
	@Column(name="ACCOUNT_CLASS")
	private Integer accountClass;

	/** The account type. */
	@Column(name="ACCOUNT_TYPE")
	private Integer accountType;

	/** The address. */
	@Column(name="ADDRESS")
	private String address;

	/** The comments. */
	@Column(name="COMMENTS")
	private String comments;

	/** The commercial register. */
	@Column(name="COMMERCIAL_REGISTER")
	private String commercialRegister;

	/** The contact name. */
	@Column(name="CONTACT_NAME")
	private String contactName;

	/** The contact phone. */
	@Column(name="CONTACT_PHONE")
	private String contactPhone;

    /** The contract date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CONTRACT_DATE")    
	private Date contractDate;

	/** The contract number. */
	@Column(name="CONTRACT_NUMBER")
	private String contractNumber;

    /** The creation date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CREATION_DATE")  
	private Date creationDate;

    /** The currency. */
    @Column(name="CURRENCY")
	private Integer currency;

    /** The description. */
    @Column(name="DESCRIPTION")
	private String description;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private Integer economicActivity;
	
	@Column(name="INVESTOR_TYPE")
	private Integer investorType;

	/** The economic sector. */
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;

	/** The email. */
	@Column(name="EMAIL")
	private String email;

	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The mnemonic. */
	@Column(name="MNEMONIC")
	private String mnemonic;

	/** The phone number. */
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The residence country. */
	@Column(name="RESIDENCE_COUNTRY")
	private Integer residenceCountry;

	/** The social capital. */
	@Column(name="SOCIAL_CAPITAL")
	private Double socialCapital;

	/** The state. */
	@Column(name="STATE")
	private Integer state;

	/** The superintendent resolution. */
	@Column(name="SUPERINTENDENT_RESOLUTION")
	private String superintendentResolution;

    /** The superintendent resolution date. */
    @Temporal( TemporalType.DATE)
	@Column(name="SUPERINTENDENT_RESOLUTION_DATE")
	private Date superintendentResolutionDate;

    /** The website. */
    @Column(name="WEBSITE")
	private String website;

    
    /** The bic code. */
    @Column(name="BIC_CODE")
	private String bicCode;

    /** The id use type. */
    @Column(name="ID_USE_TYPE")
    private Integer idUseType;

    /** The ind participant depository. */
    @Column(name="IND_PARTICIPANT_DEPOSITORY")
    private Integer indParticipantDepository;

    /** The indicator of issuer or matrix account  */
    @Column(name="ACCOUNTS_STATE")
    private Integer accountsState;
    
    /** The indicator of issuer or matrix account payment status */
    @Column(name="PAYMENT_STATE")
    private Integer paymentState;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idParticipantFk", fetch = FetchType.LAZY)
    private List<ChannelOpening> channelOpeningList;

    public Integer getAccountsState() {
		return accountsState;
	}

	public void setAccountsState(Integer accountsState) {
		this.accountsState = accountsState;
	}	

	public Integer getPaymentState() {
		return paymentState;
	}

	public void setPaymentState(Integer paymentState) {
		this.paymentState = paymentState;
	}

	//bi-directional many-to-one association to Holder
	/** The holders. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participant")
	private List<Holder> holders;
	
	/** The holder. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	
	/** The issuer. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK", referencedColumnName="ID_ISSUER_PK")
	private Issuer issuer;

	//bi-directional many-to-one association to HolderAccount
	/** The holder accounts. */
	@OneToMany(mappedBy="participant")
	private List<com.pradera.model.accounts.holderaccounts.HolderAccount> holderAccounts;
	
	//bi-directional many-to-one association to HolderAccountBalance
	/** The holder account balances. */
	@OneToMany(mappedBy="participant")
	private List<HolderAccountBalance> holderAccountBalances;

	/*
	//bi-directional many-to-one association to HolderAccountMovement
	@OneToMany(mappedBy="participant")
	private List<HolderAccountMovement> holderAccountMovements;
	*/
	
	//bi-directional many-to-one association to HolderAccountRequest
	/** The holder account requests. */
	@OneToMany(mappedBy="participant")
	private List<com.pradera.model.accounts.holderaccounts.HolderAccountRequest> holderAccountRequestHy;

	//bi-directional many-to-one association to HolderRequest
	/** The holder requests. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participant",fetch=FetchType.LAZY)
	private List<HolderRequest> holderRequests;

	//bi-directional many-to-one association to Participant
    /** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ORIGIN_PARTICIPANT_FK")
	private Participant participantOrigin;

	//bi-directional many-to-one association to Participant
	/** The participants. */
	@OneToMany(mappedBy="participantOrigin")
	private List<Participant> participants;

	//bi-directional many-to-one association to ParticipantHistoryState
	/** The participant history states. */
	@OneToMany(mappedBy="participant")
	private List<ParticipantHistoryState> participantHistoryStates;

	//bi-directional many-to-one association to ParticipantMechanism
	/** The participant mechanisms. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participant")
	private List<ParticipantMechanism> participantMechanisms;

	//bi-directional many-to-one association to ParticipantRequest
	/** The participant requests. */
	@OneToMany(mappedBy="participant")
	private List<ParticipantRequest> participantRequests;

	//bi-directional many-to-one association to ParticipantUnionOperation
	/** The participant union operations. */
	@OneToMany(mappedBy="sourceParticipant")
	private List<ParticipantUnionOperation> participantUnionOperations;
	
	/** The placement seg participa struct list. */
	@OneToMany(mappedBy="participant")
	private List<PlacementSegParticipaStruct> placementSegParticipaStructList;
	
	/** The department. */
	@Column(name="DEPARTMENT")	
	private Integer department;
	
	/** The province. */
	@Column(name="PROVINCE")
	private Integer province;
	
	/** The district. */
	@Column(name="DISTRICT")
	private Integer district;
	
	/** The participant files. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participant")
	private List<ParticipantFile> participantFiles;
	
	/** The participant files. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participant")
	private List<InstitutionBankAccount> institutionBankAccounts;
	
	/** The participant int depositaries. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participant")
	private List<ParticipantIntDepositary> participantIntDepositaries;
	
	/** The ind pay commission. */
	@Column(name="IND_PAY_COMMISSION")	
	private Integer indPayCommission;
	
	/** The ind depositary. */
	@Column(name="IND_DEPOSITARY")	
	private Integer indDepositary;
	
	/** The ind Managed Third. */
	@Column(name="IND_MANAGED_THIRD")	
	private Integer indManagedThird;

	/** The contact email. */
	@Column(name="CONTACT_EMAIL")	
	private String contactEmail;
	
	/** The represented entities. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="participant")
	private List<RepresentedEntity> representedEntities;
	
	/** The institution cash account. */
	@OneToMany(mappedBy="participant")
	private List<InstitutionCashAccount> institutionCashAccount;

	/** The institution bank account. */
	@OneToMany(mappedBy="participant")
	private List<InstitutionBankAccount> institutionBankAccount;

	/** The role participant. */
	@Column(name="ROLE_PARTICIPANT")	
	private Integer roleParticipant;
	
	/** The ind settlement incharge. */
	@Column(name="IND_SETTLEMENT_INCHARGE")	
	private Integer indSettlementIncharge;
	
	@Column(name="DOCUMENT_SOURCE")
	private Integer documentSource;
	
	@Column(name="ID_BCB_CODE")
	private String idBcbCode;
//	private Long idBcbCode;

	/** The complete description. */
	@Transient
	private String completeDescription;
	
	/** The residence country description. */
	@Transient
	private String residenceCountryDescription;
	
	/** The department description. */
	@Transient
	private String departmentDescription;
	
	/** The privince description. */
	@Transient
	private String provinceDescription;
	
	/** The district description. */
	@Transient
	private String districtDescription;
	
	/** The display description code. */
	@Transient
	private String displayDescriptionCode;
	@Transient
	private String displayCodeMnemonic;
	/** The ind holder will be created. */
	@Transient
	private String displayMnemonicCode;
	@Transient
	private boolean indHolderWillBeCreated;
	
	/** The participant bank accounts. */
	@Transient
	private List<ParticipantBankAccount> participantBankAccounts;
	
	/** The state description. */
	@Transient
	private String stateDescription;
	
	/** The account type description. */
	@Transient
	private String accountTypeDescription;
	
	/** The account class description. */
	@Transient
	private String accountClassDescription;
	
	/** The document type description. */
	@Transient
	private String documentTypeDescription;
	
	/** The full document. */
	@Transient
	private String fullDocument;
	
	/** The role participant description. */
	@Transient
	private String roleParticipantDescription;
	
	/** The economic activity description. */
	@Transient
	private String economicActivityDescription;
	
	/** The economic sector description. */
	@Transient
	private String economicSectorDescription;
	
	@Transient
	private List<Integer> lstParticipantStates;
	
    /**
     * Instantiates a new participant.
     */
    public Participant() {
    	indParticipantDepository = BooleanType.NO.getCode();
    	indPayCommission = BooleanType.NO.getCode();
    	indDepositary = BooleanType.YES.getCode();
    	indManagedThird = BooleanType.NO.getCode();
    	indSettlementIncharge = BooleanType.NO.getCode();
    }
    
    /**
     * Instantiates a new participant.
     *
     * @param mnemonic the mnemonic
     */
    public Participant(String mnemonic) {
		super();
		this.mnemonic = mnemonic;
	}

	/**
	 * Instantiates a new participant.
	 *
	 * @param idParticipantPk the id participant pk
	 */
	public Participant(Long idParticipantPk) {
    	this.idParticipantPk = idParticipantPk;
    }
    
    /**
     * Instantiates a new participant.
     *
     * @param idParticipantPk the id participant pk
     * @param description the description
     * @param mnemonic the mnemonic
     */
	public Participant(Long idParticipantPk, String description, String mnemonic) {
		this.idParticipantPk = idParticipantPk;
		this.description = description;
		this.mnemonic = mnemonic;
	}
	
	/**
	 * Instantiates a new participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param description the description
	 * @param mnemonic the mnemonic
	 * @param state the state
	 */
	public Participant(Long idParticipantPk, String description, String mnemonic, Integer state) {
		this.idParticipantPk = idParticipantPk;
		this.description = description;
		this.mnemonic = mnemonic;
		this.state = state;
	}
	
	/**
	 * Instantiates a new participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param mnemonic the mnemonic
	 */
	public Participant(Long idParticipantPk, String mnemonic) {
		this.idParticipantPk = idParticipantPk;
		this.mnemonic = mnemonic;
	}
	
	/**
	 * 
	 * @param idParticipantPk
	 * @param mnemonic
	 * @param state
	 */
	public Participant(Long idParticipantPk, String mnemonic, Integer state) {
		this.idParticipantPk = idParticipantPk;
		this.mnemonic = mnemonic;
		this.state = state;
	}
	
	/**
	 * Gets the ind swift description.
	 *
	 * @return the ind swift description
	 */
	public String getIndSwiftDescription(){
    	if(Validations.validateIsNotNull(bicCode)){
    		if(Validations.validateIsNotNullAndNotEmpty(bicCode) &&
    				bicCode.length() == 11){
//				if(bicCode.substring(7, 8).matches(GeneralConstants.REGEX_CHAR)) {
//					return BooleanType.YES.getValue();
//				} else if(bicCode.substring(7, 8).matches(GeneralConstants.REGEX_NUMBER)) {
//					return BooleanType.NO.getValue();
//				} else {
//					return null;
//				}
    			return null;
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }

    
    /**
     * Gets the ind pay commission description.
     *
     * @return the ind pay commission description
     */
    public String getIndPayCommissionDescription(){
    	if(Validations.validateIsNotNull(indPayCommission)){
    		return BooleanType.get(indPayCommission).getValue();
    	} else {
    		return null;
    	}
    }    
    
    /**
     * Gets the ind cavapy description.
     *
     * @return the ind cavapy description
     */
    public String getIndDepositaryDescription(){
    	if(Validations.validateIsNotNull(indDepositary)){
    		return BooleanType.get(indDepositary).getValue();
    	} else {
    		return null;
    	}
    } 
    
    /**
     * Gets the ind Managed Third description.
     *
     * @return the ind Managed Third description
     */
    public String getIndManagedThirdDescription(){
    	if(Validations.validateIsNotNull(indManagedThird)){
    		return BooleanType.get(indManagedThird).getValue();
    	} else {
    		return null;
    	}
    }
    
    public String getIndSettlementInchargeDescription(){
    	if(Validations.validateIsNotNull(indSettlementIncharge)){
    		return BooleanType.get(indSettlementIncharge).getValue();
    	} else {
    		return null;
    	}
    }    
    
    /**
     * Gets the display description code.
     *
     * @return the display description code
     */
    public String getDisplayDescriptionCode(){
    	if(Validations.validateIsNullOrEmpty(displayDescriptionCode)){
	    	if(Validations.validateIsNotNullAndPositive(idParticipantPk) && Validations.validateIsNotNullAndNotEmpty(description) && Validations.validateIsNotNullAndNotEmpty(mnemonic)){
	    		StringBuffer sb = new StringBuffer();
	    		sb.append(mnemonic).append(Constants.DASH).append(idParticipantPk).append(Constants.DASH).append(description);
	    		displayDescriptionCode = sb.toString();
	    	}
    	}
    	return displayDescriptionCode;
    }
    
    public String getDisplayMnemonicCode(){
    	if(Validations.validateIsNullOrEmpty(displayMnemonicCode)){
	    	if(Validations.validateIsNotNullAndPositive(idParticipantPk) && Validations.validateIsNotNullAndNotEmpty(mnemonic)){
	    		StringBuffer sb = new StringBuffer();
	    		sb.append(mnemonic).append(Constants.DASH).append(idParticipantPk);
	    		displayMnemonicCode = sb.toString();
	    	}
    	}
    	return displayMnemonicCode;
    }
    
    public void setDisplayMnemonicCode(String displayMnemonicCode){
    	this.displayMnemonicCode = displayMnemonicCode;
    }
    
    public void setDisplayCodeMnemonic(String displayCodeMnemonic){
    	this.displayCodeMnemonic = displayCodeMnemonic;
    }
    
    /**
     * Gets the code and mnemonic.
     *
     * @return the code and mnemonic
     */
    public String getDisplayCodeMnemonic(){
    	if(Validations.validateIsNullOrEmpty(displayCodeMnemonic)){
    		if(Validations.validateIsNotNullAndPositive(idParticipantPk) && Validations.validateIsNotNullAndNotEmpty(mnemonic)){
    			StringBuffer sb = new StringBuffer();
        		sb.append(mnemonic);
        		sb.append(Constants.DASH);
        		sb.append(idParticipantPk);    		
        		displayCodeMnemonic = sb.toString();
        	}
    	}
    	return displayCodeMnemonic;
    }

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return this.idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the account class.
	 *
	 * @return the account class
	 */
	public Integer getAccountClass() {
		return this.accountClass;
	}

	/**
	 * Sets the account class.
	 *
	 * @param accountClass the new account class
	 */
	public void setAccountClass(Integer accountClass) {
		this.accountClass = accountClass;
	}

	/**
	 * Gets the account type.
	 *
	 * @return the account type
	 */
	public Integer getAccountType() {
		return this.accountType;
	}

	/**
	 * Sets the account type.
	 *
	 * @param accountType the new account type
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		if(address != null) {
			address = address.toUpperCase();
		}
		this.address = address;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return this.comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		if(comments != null) {
			comments = comments.toUpperCase();
		}
		this.comments = comments;
	}

	/**
	 * Gets the commercial register.
	 *
	 * @return the commercial register
	 */
	public String getCommercialRegister() {
		return this.commercialRegister;
	}

	/**
	 * Sets the commercial register.
	 *
	 * @param commercialRegister the new commercial register
	 */
	public void setCommercialRegister(String commercialRegister) {
		if(commercialRegister != null) {
			commercialRegister = commercialRegister.toUpperCase();
		}
		this.commercialRegister = commercialRegister;
	}

	/**
	 * Gets the contact name.
	 *
	 * @return the contact name
	 */
	public String getContactName() {
		return this.contactName;
	}

	/**
	 * Sets the contact name.
	 *
	 * @param contactName the new contact name
	 */
	public void setContactName(String contactName) {
		if(contactName != null) {
			contactName = contactName.toUpperCase();
		}
		this.contactName = contactName;
	}

	/**
	 * Gets the contact phone.
	 *
	 * @return the contact phone
	 */
	public String getContactPhone() {
		return this.contactPhone;
	}

	/**
	 * Sets the contact phone.
	 *
	 * @param contactPhone the new contact phone
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	/**
	 * Gets the contract date.
	 *
	 * @return the contract date
	 */
	public Date getContractDate() {
		return this.contractDate;
	}

	/**
	 * Sets the contract date.
	 *
	 * @param contractDate the new contract date
	 */
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	/**
	 * Gets the contract number.
	 *
	 * @return the contract number
	 */
	public String getContractNumber() {
		return this.contractNumber;
	}

	/**
	 * Sets the contract number.
	 *
	 * @param contractNumber the new contract number
	 */
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return this.currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description != null) {
			description = description.toUpperCase();
		}
		this.description = description;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return this.documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public Integer getEconomicActivity() {
		return this.economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the new economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return this.economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return this.faxNumber;
	}

	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the new fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public List<ChannelOpening> getChannelOpeningList() {
		return channelOpeningList;
	}

	public void setChannelOpeningList(List<ChannelOpening> channelOpeningList) {
		this.channelOpeningList = channelOpeningList;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return this.mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		if(mnemonic != null) {
			mnemonic = mnemonic.toUpperCase();
		}
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber the new phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the residence country.
	 *
	 * @return the residence country
	 */
	public Integer getResidenceCountry() {
		return this.residenceCountry;
	}

	/**
	 * Sets the residence country.
	 *
	 * @param residenceCountry the new residence country
	 */
	public void setResidenceCountry(Integer residenceCountry) {
		this.residenceCountry = residenceCountry;
	}

	/**
	 * Gets the social capital.
	 *
	 * @return the social capital
	 */
	public Double getSocialCapital() {
		return this.socialCapital;
	}

	/**
	 * Sets the social capital.
	 *
	 * @param socialCapital the new social capital
	 */
	public void setSocialCapital(Double socialCapital) {
		this.socialCapital = socialCapital;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the superintendent resolution.
	 *
	 * @return the superintendent resolution
	 */
	public String getSuperintendentResolution() {
		return this.superintendentResolution;
	}

	/**
	 * Sets the superintendent resolution.
	 *
	 * @param superintendentResolution the new superintendent resolution
	 */
	public void setSuperintendentResolution(String superintendentResolution) {
		this.superintendentResolution = superintendentResolution;
	}

	/**
	 * Gets the superintendent resolution date.
	 *
	 * @return the superintendent resolution date
	 */
	public Date getSuperintendentResolutionDate() {
		return this.superintendentResolutionDate;
	}

	/**
	 * Sets the superintendent resolution date.
	 *
	 * @param superintendentResolutionDate the new superintendent resolution date
	 */
	public void setSuperintendentResolutionDate(Date superintendentResolutionDate) {
		this.superintendentResolutionDate = superintendentResolutionDate;
	}

	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite() {
		return this.website;
	}

	/**
	 * Sets the website.
	 *
	 * @param website the new website
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * Gets the holders.
	 *
	 * @return the holders
	 */
	public List<Holder> getHolders() {
		return this.holders;
	}

	/**
	 * Sets the holders.
	 *
	 * @param holders the new holders
	 */
	public void setHolders(List<Holder> holders) {
		this.holders = holders;
	}
	
	/**
	 * Gets the holder accounts.
	 *
	 * @return the holder accounts
	 */
	public List<com.pradera.model.accounts.holderaccounts.HolderAccount> getHolderAccounts() {
		return this.holderAccounts;
	}

	/**
	 * Sets the holder accounts.
	 *
	 * @param holderAccounts the new holder accounts
	 */
	public void setHolderAccounts(List<com.pradera.model.accounts.holderaccounts.HolderAccount> holderAccounts) {
		this.holderAccounts = holderAccounts;
	}
	
	/**
	 * Gets the holder account balances.
	 *
	 * @return the holder account balances
	 */
	public List<HolderAccountBalance> getHolderAccountBalances() {
		return this.holderAccountBalances;
	}

	/**
	 * Sets the holder account balances.
	 *
	 * @param holderAccountBalances the new holder account balances
	 */
	public void setHolderAccountBalances(List<HolderAccountBalance> holderAccountBalances) {
		this.holderAccountBalances = holderAccountBalances;
	}
	
//	/**
//	 * Gets the holder account movements.
//	 *
//	 * @return the holder account movements
//	 */
//	public List<HolderAccountMovement> getHolderAccountMovements() {
//		return this.holderAccountMovements;
//	}
//
//	/**
//	 * Sets the holder account movements.
//	 *
//	 * @param holderAccountMovements the new holder account movements
//	 */
//	public void setHolderAccountMovements(List<HolderAccountMovement> holderAccountMovements) {
//		this.holderAccountMovements = holderAccountMovements;
//	}
	
	/**
	 * Gets the holder account requests.
	 *
	 * @return the holder account requests
	 */
	public List<com.pradera.model.accounts.holderaccounts.HolderAccountRequest> getHolderAccountRequestHy() {
		return this.holderAccountRequestHy;
	}

	/**
	 * Sets the holder account requests.
	 *
	 * @param holderAccountRequests the new holder account requests
	 */
	public void setHolderAccountRequestHy(List<com.pradera.model.accounts.holderaccounts.HolderAccountRequest> holderAccountRequests) {
		this.holderAccountRequestHy = holderAccountRequests;
	}
	
	/**
	 * Gets the holder requests.
	 *
	 * @return the holder requests
	 */
	public List<HolderRequest> getHolderRequests() {
		return this.holderRequests;
	}

	/**
	 * Sets the holder requests.
	 *
	 * @param holderRequests the new holder requests
	 */
	public void setHolderRequests(List<HolderRequest> holderRequests) {
		this.holderRequests = holderRequests;
	}
	
	
	/**
	 * Gets the participants.
	 *
	 * @return the participants
	 */
	public List<Participant> getParticipants() {
		return this.participants;
	}

	/**
	 * Sets the participants.
	 *
	 * @param participants the new participants
	 */
	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}
	
	/**
	 * Gets the participant history states.
	 *
	 * @return the participant history states
	 */
	public List<ParticipantHistoryState> getParticipantHistoryStates() {
		return this.participantHistoryStates;
	}

	/**
	 * Sets the participant history states.
	 *
	 * @param participantHistoryStates the new participant history states
	 */
	public void setParticipantHistoryStates(List<ParticipantHistoryState> participantHistoryStates) {
		this.participantHistoryStates = participantHistoryStates;
	}
	
	/**
	 * Gets the participant mechanisms.
	 *
	 * @return the participant mechanisms
	 */
	public List<ParticipantMechanism> getParticipantMechanisms() {
		return this.participantMechanisms;
	}

	/**
	 * Sets the participant mechanisms.
	 *
	 * @param participantMechanisms the new participant mechanisms
	 */
	public void setParticipantMechanisms(List<ParticipantMechanism> participantMechanisms) {
		this.participantMechanisms = participantMechanisms;
	}
	
	/**
	 * Gets the participant requests.
	 *
	 * @return the participant requests
	 */
	public List<ParticipantRequest> getParticipantRequests() {
		return this.participantRequests;
	}

	/**
	 * Sets the participant requests.
	 *
	 * @param participantRequests the new participant requests
	 */
	public void setParticipantRequests(List<ParticipantRequest> participantRequests) {
		this.participantRequests = participantRequests;
	}
	
	/**
	 * Gets the participant union operations.
	 *
	 * @return the participant union operations
	 */
	public List<ParticipantUnionOperation> getParticipantUnionOperations() {
		return this.participantUnionOperations;
	}

	/**
	 * Sets the participant union operations.
	 *
	 * @param participantUnionOperations the new participant union operations
	 */
	public void setParticipantUnionOperations(List<ParticipantUnionOperation> participantUnionOperations) {
		this.participantUnionOperations = participantUnionOperations;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public Integer getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(Integer department) {
		this.department = department;
	}

	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public Integer getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 *
	 * @param province the new province
	 */
	public void setProvince(Integer province) {
		this.province = province;
	}

	/**
	 * Gets the district.
	 *
	 * @return the district
	 */
	public Integer getDistrict() {
		return district;
	}

	/**
	 * Sets the district.
	 *
	 * @param district the new district
	 */
	public void setDistrict(Integer district) {
		this.district = district;
	}

	/**
	 * Gets the participant files.
	 *
	 * @return the participant files
	 */
	public List<ParticipantFile> getParticipantFiles() {
		return participantFiles;
	}

	/**
	 * Sets the participant files.
	 *
	 * @param participantFiles the new participant files
	 */
	public void setParticipantFiles(List<ParticipantFile> participantFiles) {
		this.participantFiles = participantFiles;
	}
	
	public List<InstitutionBankAccount> getInstitutionBankAccounts() {
		return institutionBankAccounts;
	}

	public void setInstitutionBankAccounts(List<InstitutionBankAccount> institutionBankAccounts) {
		this.institutionBankAccounts = institutionBankAccounts;
	}

	/**
	 * Gets the participant int depositaries.
	 *
	 * @return the participant int depositaries
	 */
	public List<ParticipantIntDepositary> getParticipantIntDepositaries() {
		return participantIntDepositaries;
	}

	/**
	 * Sets the participant int depositaries.
	 *
	 * @param participantIntDepositaries the participant int depositaries
	 */
	public void setParticipantIntDepositaries(
			List<ParticipantIntDepositary> participantIntDepositaries) {
		this.participantIntDepositaries = participantIntDepositaries;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the participant origin.
	 *
	 * @return the participant origin
	 */
	public Participant getParticipantOrigin() {
		return participantOrigin;
	}

	/**
	 * Sets the participant origin.
	 *
	 * @param participantOrigin the new participant origin
	 */
	public void setParticipantOrigin(Participant participantOrigin) {
		this.participantOrigin = participantOrigin;
	}

	/**
	 * Gets the ind pay commission.
	 *
	 * @return the ind pay commission
	 */
	public Integer getIndPayCommission() {
		return indPayCommission;
	}

	/**
	 * Sets the ind pay commission.
	 *
	 * @param indPayCommission the new ind pay commission
	 */
	public void setIndPayCommission(Integer indPayCommission) {
		this.indPayCommission = indPayCommission;
	}
	
	/**
	 * Gets the ind cavapy.
	 *
	 * @return the ind cavapy
	 */
	public Integer getIndDepositary() {
		return indDepositary;
	}

	/**
	 * Sets the ind cavapy.
	 *
	 * @param indDepositary the new ind cavapy
	 */
	public void setIndDepositary(Integer indDepositary) {
		this.indDepositary = indDepositary;
	}
	
	/**
	 * Gets the ind Managed Third.
	 *
	 * @return the ind Managed Third
	 */
	public Integer getIndManagedThird() {
		return indManagedThird;
	}
	
	/**
	 * Sets the ind Managed Third.
	 *
	 * @param indManagedThird the new Managed Third
	 */
	public void setIndManagedThird(Integer indManagedThird) {
		this.indManagedThird = indManagedThird;
	}

	/**
	 * Gets the contact email.
	 *
	 * @return the contact email
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * Sets the contact email.
	 *
	 * @param contactEmail the new contact email
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the represented entities.
	 *
	 * @return the represented entities
	 */
	public List<RepresentedEntity> getRepresentedEntities() {
		return representedEntities;
	}

	/**
	 * Sets the represented entities.
	 *
	 * @param representedEntities the new represented entities
	 */
	public void setRepresentedEntities(List<RepresentedEntity> representedEntities) {
		this.representedEntities = representedEntities;
	}

	/**
	 * Gets the role participant.
	 *
	 * @return the role participant
	 */
	public Integer getRoleParticipant() {
		return roleParticipant;
	}

	/**
	 * Sets the role participant.
	 *
	 * @param roleParticipant the new role participant
	 */
	public void setRoleParticipant(Integer roleParticipant) {
		this.roleParticipant = roleParticipant;
	}
	
	/**
	 * Gets the complete description.
	 *
	 * @return the complete description
	 */
	public String getCompleteDescription() {
		return completeDescription;
	}

	/**
	 * Sets the complete description.
	 *
	 * @param completeDescription the new complete description
	 */
	public void setCompleteDescription(String completeDescription) {
		this.completeDescription = completeDescription;
	}

	/**
	 * Gets the institution cash account.
	 *
	 * @return the institution cash account
	 */
	public List<InstitutionCashAccount> getInstitutionCashAccount() {
		return institutionCashAccount;
	}

	/**
	 * Sets the institution cash account.
	 *
	 * @param institutionCashAccount the new institution cash account
	 */
	public void setInstitutionCashAccount(
			List<InstitutionCashAccount> institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}

	/**
	 * Gets the institution bank account.
	 *
	 * @return the institution bank account
	 */
	public List<InstitutionBankAccount> getInstitutionBankAccount() {
		return institutionBankAccount;
	}

	/**
	 * Sets the institution bank account.
	 *
	 * @param institutionBankAccount the new institution bank account
	 */
	public void setInstitutionBankAccount(
			List<InstitutionBankAccount> institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}
	
	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}

	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}

	
	
	/**
	 * Gets the id use type.
	 *
	 * @return the id use type
	 */
	public Integer getIdUseType() {
		return idUseType;
	}

	/**
	 * Sets the id use type.
	 *
	 * @param idUseType the new id use type
	 */
	public void setIdUseType(Integer idUseType) {
		this.idUseType = idUseType;
	}
	
	/**
	 * Gets the placement seg participa struct list.
	 *
	 * @return the placement seg participa struct list
	 */
	public List<PlacementSegParticipaStruct> getPlacementSegParticipaStructList() {
		return placementSegParticipaStructList;
	}

	/**
	 * Sets the placement seg participa struct list.
	 *
	 * @param placementSegParticipaStructList the new placement seg participa struct list
	 */
	public void setPlacementSegParticipaStructList(
			List<PlacementSegParticipaStruct> placementSegParticipaStructList) {
		this.placementSegParticipaStructList = placementSegParticipaStructList;
	}

	/**
	 * Gets the residence country description.
	 *
	 * @return the residence country description
	 */
	public String getResidenceCountryDescription() {
		return residenceCountryDescription;
	}

	/**
	 * Sets the residence country description.
	 *
	 * @param residenceCountryDescription the new residence country description
	 */
	public void setResidenceCountryDescription(String residenceCountryDescription) {
		this.residenceCountryDescription = residenceCountryDescription;
	}

	/**
	 * Gets the province description.
	 *
	 * @return the province description
	 */
	public String getProvinceDescription() {
		return provinceDescription;
	}

	/**
	 * Sets the province description.
	 *
	 * @param provinceDescription the new province description
	 */
	public void setProvinceDescription(String provinceDescription) {
		this.provinceDescription = provinceDescription;
	}

	/**
	 * Gets the district description.
	 *
	 * @return the district description
	 */
	public String getDistrictDescription() {
		return districtDescription;
	}

	/**
	 * Sets the district description.
	 *
	 * @param districtDescription the new district description
	 */
	public void setDistrictDescription(String districtDescription) {
		this.districtDescription = districtDescription;
	}

	/**
	 * Gets the ind participant depository.
	 *
	 * @return the ind participant depository
	 */
	public Integer getIndParticipantDepository() {
		return indParticipantDepository;
	}

	/**
	 * Sets the ind participant depository.
	 *
	 * @param indParticipantDepository the new ind participant depository
	 */
	public void setIndParticipantDepository(Integer indParticipantDepository) {
		this.indParticipantDepository = indParticipantDepository;
	}		
	
	/**
	 * Checks if is ind holder will be created.
	 *
	 * @return true, if is ind holder will be created
	 */
	public boolean isIndHolderWillBeCreated() {
		return indHolderWillBeCreated;
	}

	/**
	 * Sets the ind holder will be created.
	 *
	 * @param indHolderWillBeCreated the new ind holder will be created
	 */
	public void setIndHolderWillBeCreated(boolean indHolderWillBeCreated) {
		this.indHolderWillBeCreated = indHolderWillBeCreated;
	}

	/**
	 * Gets the participant bank accounts.
	 *
	 * @return the participant bank accounts
	 */
	public List<ParticipantBankAccount> getParticipantBankAccounts() {
		return participantBankAccounts;
	}

	/**
	 * Sets the participant bank accounts.
	 *
	 * @param participantBankAccounts the new participant bank accounts
	 */
	public void setParticipantBankAccounts(
			List<ParticipantBankAccount> participantBankAccounts) {
		this.participantBankAccounts = participantBankAccounts;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("participantFiles", participantFiles);
        detailsMap.put("institutionBankAccounts", institutionBankAccounts);
        detailsMap.put("holders", holders);
        detailsMap.put("participantMechanisms", participantMechanisms);
        detailsMap.put("participantIntDepositaries", participantIntDepositaries);

        return detailsMap;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idParticipantPk == null) ? 0 : idParticipantPk.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Participant other = (Participant) obj;
		if (idParticipantPk == null) {
			if (other.idParticipantPk != null)
				return false;
		} else if (!idParticipantPk.equals(other.idParticipantPk))
			return false;
		return true;
	}

	/**
	 * Instantiates a new participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param state the state
	 */
	public Participant(Long idParticipantPk, Integer state) {
		this.idParticipantPk = idParticipantPk;
		this.state = state;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the account type description.
	 *
	 * @return the account type description
	 */
	public String getAccountTypeDescription() {
		return accountTypeDescription;
	}

	/**
	 * Sets the account type description.
	 *
	 * @param accountTypeDescription the new account type description
	 */
	public void setAccountTypeDescription(String accountTypeDescription) {
		this.accountTypeDescription = accountTypeDescription;
	}

	/**
	 * Gets the account class description.
	 *
	 * @return the account class description
	 */
	public String getAccountClassDescription() {
		return accountClassDescription;
	}

	/**
	 * Sets the account class description.
	 *
	 * @param accountClassDescription the new account class description
	 */
	public void setAccountClassDescription(String accountClassDescription) {
		this.accountClassDescription = accountClassDescription;
	}

	/**
	 * Gets the document type description.
	 *
	 * @return the document type description
	 */
	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}

	/**
	 * Sets the document type description.
	 *
	 * @param documentTypeDescription the new document type description
	 */
	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	/**
	 * Gets the full document.
	 *
	 * @return the full document
	 */
	public String getFullDocument() {
		return fullDocument;
	}

	/**
	 * Sets the full document.
	 *
	 * @param fullDocument the new full document
	 */
	public void setFullDocument(String fullDocument) {
		this.fullDocument = fullDocument;
	}

	/**
	 * Gets the ind settlement incharge.
	 *
	 * @return the ind settlement incharge
	 */
	public Integer getIndSettlementIncharge() {
		return indSettlementIncharge;
	}

	/**
	 * Sets the ind settlement incharge.
	 *
	 * @param indSettlementIncharge the new ind settlement incharge
	 */
	public void setIndSettlementIncharge(Integer indSettlementIncharge) {
		this.indSettlementIncharge = indSettlementIncharge;
	}

	/**
	 * Gets the department description.
	 *
	 * @return the department description
	 */
	public String getDepartmentDescription() {
		return departmentDescription;
	}

	/**
	 * Sets the department description.
	 *
	 * @param departmentDescription the new department description
	 */
	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}

	/**
	 * Gets the role participant description.
	 *
	 * @return the role participant description
	 */
	public String getRoleParticipantDescription() {
		return roleParticipantDescription;
	}

	/**
	 * Sets the role participant description.
	 *
	 * @param roleParticipantDescription the new role participant description
	 */
	public void setRoleParticipantDescription(String roleParticipantDescription) {
		this.roleParticipantDescription = roleParticipantDescription;
	}

	/**
	 * Gets the economic activity description.
	 *
	 * @return the economic activity description
	 */
	public String getEconomicActivityDescription() {
		return economicActivityDescription;
	}

	/**
	 * Sets the economic activity description.
	 *
	 * @param economicActivityDescription the new economic activity description
	 */
	public void setEconomicActivityDescription(String economicActivityDescription) {
		this.economicActivityDescription = economicActivityDescription;
	}

	/**
	 * Gets the economic sector description.
	 *
	 * @return the economic sector description
	 */
	public String getEconomicSectorDescription() {
		return economicSectorDescription;
	}

	/**
	 * Sets the economic sector description.
	 *
	 * @param economicSectorDescription the new economic sector description
	 */
	public void setEconomicSectorDescription(String economicSectorDescription) {
		this.economicSectorDescription = economicSectorDescription;
	}

	public List<Integer> getLstParticipantStates() {
		return lstParticipantStates;
	}

	public void setLstParticipantStates(List<Integer> lstParticipantStates) {
		this.lstParticipantStates = lstParticipantStates;
	}

	public Integer getDocumentSource() {
		return documentSource;
	}

	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}

	/**
	 * @return the idBcbCode
	 */
	public String getIdBcbCode() {
//	public Long getIdBcbCode() {
		return idBcbCode;
	}

	/**
	 * @param idBcbCode the idBcbCode to set
	 */
	public void setIdBcbCode(String idBcbCode) {
//	public void setIdBcbCode(Long idBcbCode) {
		this.idBcbCode = idBcbCode;
	}

	public Integer getInvestorType() {
		return investorType;
	}

	public void setInvestorType(Integer investorType) {
		this.investorType = investorType;
	}
	
}