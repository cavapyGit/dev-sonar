package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum HolderReqFileHisType {
	
	CREATION(Integer.valueOf(1099),"CREACION"),
	MODIFICATION(Integer.valueOf(1100),"MODIFICACION");
	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private HolderReqFileHisType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<HolderReqFileHisType> list = new ArrayList<HolderReqFileHisType>();
	public static final Map<Integer, HolderReqFileHisType> lookup = new HashMap<Integer, HolderReqFileHisType>();
	static {
		for (HolderReqFileHisType s : EnumSet.allOf(HolderReqFileHisType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
