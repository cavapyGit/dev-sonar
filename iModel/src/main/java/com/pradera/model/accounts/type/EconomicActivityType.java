package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * This Enum contains the Economic Activities.
 */
public enum EconomicActivityType {
	
	/** The ECONOMI c_ activit y_1. */
	ADMINISTRACION_CENTRAL(Integer.valueOf(41),"AGRICULTURA, SILVICULTURA, CAZA Y PESCA"),
	
	/** The ECONOMI c_ activit y_2. */
	ADMINISTRADORAS_FONDOS_PENSIONES(Integer.valueOf(42),"ADMINISTRADORAS DE FONDOS DE PENSIONES"),
	
	/** The ECONOMI c_ activit y_3. */
	AGENCIAS_BOLSA(Integer.valueOf(43),"AGENCIAS DE BOLSA"),
	
	/** The ECONOMI c_ activit y_4. */
	MUNICIPIOS_GOBIERNOS_LOCALES(Integer.valueOf(44),"ELECTRICIDAD, GAS Y AGUA"),
	
	/** The ECONOMI c_ activit y_5. */
	BANCOS(Integer.valueOf(45),"BANCOS"),
	
	/** The ECONOMI c_ activit y_6. */
	BANCOS_MULTIPLES(Integer.valueOf(46),"EXTRACCION DE MINAS Y CANTERAS"),
	
	/** The ECONOMI c_ activit y_7. */
	BANCOS_AHORRO_CREDITOS_FOMENTO(Integer.valueOf(47),"INDUSTRIAS MANUFACTURERAS"),
	
	/** The ECONOMI c_ activit y_13. */
	COMPANIAS_SEGURO(Integer.valueOf(48),"AGRICULTURA, SILVICULTURA, CAZA Y PESCA"),
	
	/** The ECONOMI c_ activit y_9. */
	ASOCIACIONES_AHORROS_PRESTAMOS(Integer.valueOf(49),"SERVICIOS BANCARIOS IMPUTADOS"),
	
	/** The ECONOMI c_ activit y_10. */
	COOPERATIVAS(Integer.valueOf(50),"COOPERATIVAS"),
	
	/** The ECONOMI c_ activit y_11. */
	ENTIDADES_PUBLICAS_INTERMEDIACION_FINANCIERA(Integer.valueOf(51),"SERVICIOS DE ADMINISTRACION PUBLICA"),
	
	/** The ECONOMI c_ activit y_12. */
	RESTO_SOCIEDADES_DEPOSITOS(Integer.valueOf(52),"TRANSPORTE, ALMACENAMIENTO Y COMUNICACIONES"),
	
	/** The ECONOMI c_ activit y_8. */
	CORPORACIONES_CREDITO_FINANCIERAS(Integer.valueOf(53),"RESTAURANTES Y HOTELES"),
	
	/** The ECONOMI c_ activit y_14. */
	AFP(Integer.valueOf(54),"COMERCIO"),
	
	/** The ECONOMI c_ activit y_15. */
	FONDOS_INVERSION_ABIERTOS(Integer.valueOf(55),"FONDOS DE INVERSION ABIERTOS"),
	
	/** The ECONOMI c_ activit y_16. */
	FONDOS_INVERSION_CERRADOS(Integer.valueOf(56),"FONDOS_DE_INVERSION_CERRADOS"),
	
	/** The ECONOMI c_ activit y_18. */
	FONDOS_INVERSION_MIXTOS(Integer.valueOf(57),"FONDOS DE INVERSION MIXTOS"),
	
	/** The ECONOMI c_ activit y_19. */
	FONDOS_FINANCIEROS_PRIVADOS(Integer.valueOf(58),"FONDOS FINANCIEROS PRIVADOS"),	
	
	/** The ECONOMI c_ activit y_21. */
	EMPRESAS_PRIVADAS(Integer.valueOf(59),"INDUSTRIAS MANUFACTURERAS"),
	
	/** The ECONOMI c_ activit y_22. */
	HOGARES_MICROEMPRESAS(Integer.valueOf(60),"RESTAURANTES Y HOTELES"),
	
	/** The ECONOMI c_ activit y_23. */
	MUTUALES(Integer.valueOf(61),"MUTUALES"),
		
	/** The ECONOMI c_ activit y_24. */
	ORGANISMO_NO_GUBERNAMENTAL_NTERMEDIACION_FINANCIERA(Integer.valueOf(62),"ORGANISMO NO GUBERNAMENTAL DE INTERMEDIACI�N FINANCIERA"),
	
	/** The ECONOMI c_ activit y_25. */
	SECTOR_PUBLICO_NO_FINANCIERO(Integer.valueOf(63),"SERVICIOS DE ADMINISTRACION PUBLICA"),
	
	/** The ECONOMI c_ activit y1_26. */
	SECTOR_FINANCIERO(Integer.valueOf(64),"TRANSPORTE, ALMACENAMIENTO Y COMUNICACIONES"),
	
	/** The ECONOMI c_ activit y_27. */
	SECTOR_PRIVADO_NO_FINANCIERO(Integer.valueOf(65),"SECTOR PRIVADO NO FINANCIERO"),
	
	PERSONAS_NATURALES(Integer.valueOf(1877),"PERSONAS NATURALES"),
	
	SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION(Integer.valueOf(1879),"SOCIEDADES ADMINISTRADORAS DE FONDOS DE INVERSION"),
	
	SOCIEDADES_TITULARIZACION(Integer.valueOf(1880),"SOCIEDADES DE TITULACION"),
	
	OTROS_SERVICIOS_FINANCIEROS(Integer.valueOf(1876),"OTROS SERVICIOS FINANCIEROS"),
	
	PATRIMONIO_AUTONOMO_TITULARIZACION(Integer.valueOf(2157),"PATRIMONIO AUTONOMO TITULARIZACION");
	
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new economic sector type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private EconomicActivityType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<EconomicActivityType> list = new ArrayList<EconomicActivityType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, EconomicActivityType> lookup = new HashMap<Integer, EconomicActivityType>();
	static {
		for (EconomicActivityType s : EnumSet.allOf(EconomicActivityType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the economic sector type
	 */
	public static EconomicActivityType get(Integer code) {
		return lookup.get(code);
	}

}
