package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum TrustDocHolderJurType {
		
		
		CE(Integer.valueOf(1061),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(1062),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(1063),"PASAPORTE"), 
		LIC(Integer.valueOf(1064),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(1065),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(1066),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(1067),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(1068),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(1069),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(1070),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(1071),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(1072),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(1073),"DECRETO"),		
	    RCF(Integer.valueOf(1074),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(1075),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(1076),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    private Integer code;
		private String value;
		
		public static final List<TrustDocHolderJurType> list = new ArrayList<TrustDocHolderJurType>();
		public static final Map<Integer, TrustDocHolderJurType> lookup = new HashMap<Integer, TrustDocHolderJurType>();

		static {
			for (TrustDocHolderJurType s : EnumSet.allOf(TrustDocHolderJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private TrustDocHolderJurType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static TrustDocHolderJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
