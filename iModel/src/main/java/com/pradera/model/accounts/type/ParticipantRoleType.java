package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum ParticipantRoleType {
	
	NORMAL(Integer.valueOf(589),"NORMAL"),
	EMISOR(Integer.valueOf(590),"EMISOR"),
	SPECIAL(Integer.valueOf(591),"ESPECIAL");

	
	private Integer code;
	private String value;	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private ParticipantRoleType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<ParticipantRoleType> list = new ArrayList<ParticipantRoleType>();
	public static final Map<Integer, ParticipantRoleType> lookup = new HashMap<Integer, ParticipantRoleType>();
	static {
		for (ParticipantRoleType s : EnumSet.allOf(ParticipantRoleType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ParticipantRoleType get(Integer code) {
		return lookup.get(code);
	}

}
