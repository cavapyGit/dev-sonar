package com.pradera.model.accounts;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "HOLDER_ANNULATION")
@NamedQueries({
    @NamedQuery(name = "HolderAnnulation.findAll", query = "SELECT h FROM HolderAnnulation h"),
    @NamedQuery(name = "HolderAnnulation.findByIdAnnulationHolderPk", query = "SELECT h FROM HolderAnnulation h WHERE h.idAnnulationHolderPk = :idAnnulationHolderPk"),
    @NamedQuery(name = "HolderAnnulation.findByMotiveAnnulation", query = "SELECT h FROM HolderAnnulation h WHERE h.motiveAnnulation = :motiveAnnulation"),
    @NamedQuery(name = "HolderAnnulation.findByObservationAnnulation", query = "SELECT h FROM HolderAnnulation h WHERE h.observationAnnulation = :observationAnnulation"),
    @NamedQuery(name = "HolderAnnulation.findByFileTypeAnnulation", query = "SELECT h FROM HolderAnnulation h WHERE h.fileTypeAnnulation = :fileTypeAnnulation"),
    @NamedQuery(name = "HolderAnnulation.findByFileName", query = "SELECT h FROM HolderAnnulation h WHERE h.fileName = :fileName"),
    @NamedQuery(name = "HolderAnnulation.findByStateAnnulation", query = "SELECT h FROM HolderAnnulation h WHERE h.stateAnnulation = :stateAnnulation"),
    @NamedQuery(name = "HolderAnnulation.findByRegistryDate", query = "SELECT h FROM HolderAnnulation h WHERE h.registryDate = :registryDate"),
    @NamedQuery(name = "HolderAnnulation.findByLastModifyUser", query = "SELECT h FROM HolderAnnulation h WHERE h.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "HolderAnnulation.findByLastModifyDate", query = "SELECT h FROM HolderAnnulation h WHERE h.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "HolderAnnulation.findByLastModifyIp", query = "SELECT h FROM HolderAnnulation h WHERE h.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "HolderAnnulation.findByLastModifyApp", query = "SELECT h FROM HolderAnnulation h WHERE h.lastModifyApp = :lastModifyApp")})
public class HolderAnnulation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_HOLDER_ANNULATION", sequenceName="SQ_HOLDER_ANNULATION",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLDER_ANNULATION")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ANNULATION_HOLDER_PK", nullable = false)
    private Long idAnnulationHolderPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MOTIVE_ANNULATION", nullable = false)
    private Integer motiveAnnulation;
    @Size(max = 1000)
    @Column(name = "OBSERVATION_ANNULATION", length = 1000)
    private String observationAnnulation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_TYPE_ANNULATION", nullable = false)
    private Integer fileTypeAnnulation;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "FILE_ATTACHED", nullable = false)
    private byte[] fileAttached;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 265)
    @Column(name = "FILE_NAME", nullable = false, length = 265)
    private String fileName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE_ANNULATION", nullable = false)
    private Integer stateAnnulation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Column(name = "REQUEST_CANCEL_TYPE")
    private Integer requestCancelType;
    @Size(max = 500)
    @Column(name = "REQUEST_CANCEL_DESCRIPTION", length = 500)
    private String requestCancelDescription;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Size(max = 20)
    @Column(name = "LAST_MODIFY_IP", length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAnnulationHolderFk", fetch = FetchType.LAZY)
    private List<HolderAnnulationHistory> holderAnnulationHistoryList;
    @JoinColumn(name = "ID_PARTICIPANT_FK", referencedColumnName = "ID_PARTICIPANT_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Participant idParticipantFk;
    @JoinColumn(name = "ID_HOLDER_FK", referencedColumnName = "ID_HOLDER_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Holder idHolderFk;

    public HolderAnnulation() {
    }

    public HolderAnnulation(Long idAnnulationHolderPk) {
        this.idAnnulationHolderPk = idAnnulationHolderPk;
    }

    public HolderAnnulation(Long idAnnulationHolderPk, Integer motiveAnnulation, Integer fileTypeAnnulation, byte[] fileAttached, String fileName, Integer stateAnnulation, Date registryDate, String lastModifyUser, Date lastModifyDate, Long lastModifyApp) {
        this.idAnnulationHolderPk = idAnnulationHolderPk;
        this.motiveAnnulation = motiveAnnulation;
        this.fileTypeAnnulation = fileTypeAnnulation;
        this.fileAttached = fileAttached;
        this.fileName = fileName;
        this.stateAnnulation = stateAnnulation;
        this.registryDate = registryDate;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdAnnulationHolderPk() {
        return idAnnulationHolderPk;
    }

    public void setIdAnnulationHolderPk(Long idAnnulationHolderPk) {
        this.idAnnulationHolderPk = idAnnulationHolderPk;
    }

    public List<HolderAnnulationHistory> getHolderAnnulationHistoryList() {
		return holderAnnulationHistoryList;
	}

	public void setHolderAnnulationHistoryList(List<HolderAnnulationHistory> holderAnnulationHistoryList) {
		this.holderAnnulationHistoryList = holderAnnulationHistoryList;
	}

	public Integer getMotiveAnnulation() {
        return motiveAnnulation;
    }

    public void setMotiveAnnulation(Integer motiveAnnulation) {
        this.motiveAnnulation = motiveAnnulation;
    }

    public String getObservationAnnulation() {
        return observationAnnulation;
    }

    public void setObservationAnnulation(String observationAnnulation) {
        this.observationAnnulation = observationAnnulation;
    }

    public Integer getFileTypeAnnulation() {
        return fileTypeAnnulation;
    }

    public void setFileTypeAnnulation(Integer fileTypeAnnulation) {
        this.fileTypeAnnulation = fileTypeAnnulation;
    }


    public byte[] getFileAttached() {
		return fileAttached;
	}

	public void setFileAttached(byte[] fileAttached) {
		this.fileAttached = fileAttached;
	}

	public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getStateAnnulation() {
        return stateAnnulation;
    }

    public void setStateAnnulation(Integer stateAnnulation) {
        this.stateAnnulation = stateAnnulation;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public Integer getRequestCancelType() {
		return requestCancelType;
	}

	public void setRequestCancelType(Integer requestCancelType) {
		this.requestCancelType = requestCancelType;
	}

	public String getRequestCancelDescription() {
		return requestCancelDescription;
	}

	public void setRequestCancelDescription(String requestCancelDescription) {
		this.requestCancelDescription = requestCancelDescription;
	}

	public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public Participant getIdParticipantFk() {
        return idParticipantFk;
    }

    public void setIdParticipantFk(Participant idParticipantFk) {
        this.idParticipantFk = idParticipantFk;
    }

    public Holder getIdHolderFk() {
        return idHolderFk;
    }

    public void setIdHolderFk(Holder idHolderFk) {
        this.idHolderFk = idHolderFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAnnulationHolderPk != null ? idAnnulationHolderPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HolderAnnulation)) {
            return false;
        }
        HolderAnnulation other = (HolderAnnulation) object;
        if ((this.idAnnulationHolderPk == null && other.idAnnulationHolderPk != null) || (this.idAnnulationHolderPk != null && !this.idAnnulationHolderPk.equals(other.idAnnulationHolderPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.HolderAnnulation[ idAnnulationHolderPk=" + idAnnulationHolderPk + " ]";
    }
    
}
