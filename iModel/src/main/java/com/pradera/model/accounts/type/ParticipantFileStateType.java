package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantFileStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/04/2013
 */
public enum ParticipantFileStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(816),"REGISTRADO"),
	
	/** The deleted. */
	DELETED(Integer.valueOf(817),"ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant file state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ParticipantFileStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<ParticipantFileStateType> list = new ArrayList<ParticipantFileStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantFileStateType> lookup = new HashMap<Integer, ParticipantFileStateType>();
	static {
		for (ParticipantFileStateType s : EnumSet.allOf(ParticipantFileStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
