package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum PepMotiveType {
		LINK_FOR_FAMILY(Integer.valueOf(120),"VINCULADO"),
		AFFINITY(Integer.valueOf(577),"DIRECTO"),
		OWN(Integer.valueOf(803),"PROPIO");
		

		
		private Integer code;
		private String value;
		

		public static final List<PepMotiveType> list = new ArrayList<PepMotiveType>();
		public static final Map<Integer, PepMotiveType> lookup = new HashMap<Integer, PepMotiveType>();

		static {
			for (PepMotiveType s : EnumSet.allOf(PepMotiveType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private PepMotiveType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static PepMotiveType get(Integer code) {
			return lookup.get(code);
		}

		
}
