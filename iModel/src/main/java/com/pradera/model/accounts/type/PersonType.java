package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum PersonType {
		
		NATURAL(Integer.valueOf(96),Integer.valueOf(1), "FISICA"),
		JURIDIC(Integer.valueOf(394),Integer.valueOf(2),"JURIDICO");		
		
		private Integer code;
		private Integer interfaceCode;
		private String value;
		

		public static final List<PersonType> list = new ArrayList<PersonType>();
		public static final Map<Integer, PersonType> lookup = new HashMap<Integer, PersonType>();

		static {
			for (PersonType s : EnumSet.allOf(PersonType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private PersonType(Integer code, Integer interfaceCode, String value) {
			this.code = code;
			this.interfaceCode = interfaceCode;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static PersonType get(Integer code) {
			return lookup.get(code);
		}

		public Integer getInterfaceCode() {
			return interfaceCode;
		}

		public void setInterfaceCode(Integer interfaceCode) {
			this.interfaceCode = interfaceCode;
		}

		
}
