package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum MechanismModalityStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/04/2013
 */
public enum MechanismModalityStateType {
	
	/** The registered. */
	ACTIVE(Integer.valueOf(820),"ACTIVO"),
	
	/** The innactive. */
	INNACTIVE(Integer.valueOf(821),"INACTIVO");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;		
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new mechanism modality state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private MechanismModalityStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<MechanismModalityStateType> list = new ArrayList<MechanismModalityStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, MechanismModalityStateType> lookup = new HashMap<Integer, MechanismModalityStateType>();
	static {
		for (MechanismModalityStateType s : EnumSet.allOf(MechanismModalityStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
