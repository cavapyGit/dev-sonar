package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.type.StateHistoryStateHolderType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_HISTORY_STATE database table.
 * 
 */
@Entity
@Table(name="HOLDER_HISTORY_STATE")
public class HolderHistoryState implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id hol history state pk. */
	@Id
	@SequenceGenerator(name="HOLDER_HISTORY_STATE_IDHOLHISTORYSTATEPK_GENERATOR", sequenceName="SQ_ID_HOL_HISTORY_STATE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_HISTORY_STATE_IDHOLHISTORYSTATEPK_GENERATOR")
	@Column(name="ID_HOL_HISTORY_STATE_PK")
	private Long idHolHistoryStatePk;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The motive. */
	private Integer motive;

	/** The new state. */
	@Column(name="NEW_STATE")
	private Integer newState;

	/** The old state. */
	@Column(name="OLD_STATE")
	private Integer oldState;

	/** The other motive. */
	@Column(name="OTHER_MOTIVE")
	private String otherMotive;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

    /** The update state date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_STATE_DATE")
	private Date updateStateDate;

	/** The holder. */
	//bi-directional many-to-one association to Holder
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;

	/** The holder request. */
	//bi-directional many-to-one association to HolderRequest
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_REQUEST_FK",referencedColumnName="ID_HOLDER_REQUEST_PK")
	private HolderRequest holderRequest;
    
    /** The old state description. */
    @Transient
    private String oldStateDescription;
    
    /** The new state description. */
    @Transient
    private String newStateDescription;

    /**
     * Instantiates a new holder history state.
     */
    public HolderHistoryState() {
    }

	/**
	 * Gets the id hol history state pk.
	 *
	 * @return the id hol history state pk
	 */
	public Long getIdHolHistoryStatePk() {
		return this.idHolHistoryStatePk;
	}

	/**
	 * Sets the id hol history state pk.
	 *
	 * @param idHolHistoryStatePk the new id hol history state pk
	 */
	public void setIdHolHistoryStatePk(Long idHolHistoryStatePk) {
		this.idHolHistoryStatePk = idHolHistoryStatePk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public Integer getMotive() {
		return this.motive;
	}

	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	/**
	 * Gets the new state.
	 *
	 * @return the new state
	 */
	public Integer getNewState() {
		return this.newState;
	}

	/**
	 * Sets the new state.
	 *
	 * @param newState the new new state
	 */
	public void setNewState(Integer newState) {
		this.newState = newState;
	}

	/**
	 * Gets the old state.
	 *
	 * @return the old state
	 */
	public Integer getOldState() {
		return this.oldState;
	}

	/**
	 * Sets the old state.
	 *
	 * @param oldState the new old state
	 */
	public void setOldState(Integer oldState) {
		this.oldState = oldState;
	}

	/**
	 * Gets the other motive.
	 *
	 * @return the other motive
	 */
	public String getOtherMotive() {
		return this.otherMotive;
	}

	/**
	 * Sets the other motive.
	 *
	 * @param otherMotive the new other motive
	 */
	public void setOtherMotive(String otherMotive) {
		this.otherMotive = otherMotive;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the update state date.
	 *
	 * @return the update state date
	 */
	public Date getUpdateStateDate() {
		return this.updateStateDate;
	}

	/**
	 * Sets the update state date.
	 *
	 * @param updateStateDate the new update state date
	 */
	public void setUpdateStateDate(Date updateStateDate) {
		this.updateStateDate = updateStateDate;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return this.holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return this.holderRequest;
	}

	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}

	/**
	 * Gets the old state description.
	 *
	 * @return the old state description
	 */
	public String getOldStateDescription() {
		return oldStateDescription =	StateHistoryStateHolderType.get(oldState).getValue();
	}

	/**
	 * Sets the old state description.
	 *
	 * @param oldStateDescription the new old state description
	 */
	public void setOldStateDescription(String oldStateDescription) {
		this.oldStateDescription = oldStateDescription;
	}

	/**
	 * Gets the new state description.
	 *
	 * @return the new state description
	 */
	public String getNewStateDescription() {
		return newStateDescription = StateHistoryStateHolderType.get(newState).getValue();
	}

	/**
	 * Sets the new state description.
	 *
	 * @param newStateDescription the new new state description
	 */
	public void setNewStateDescription(String newStateDescription) {
		this.newStateDescription = newStateDescription;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();
        if (loggerUser != null) {
        	if(loggerUser.getIdPrivilegeOfSystem() != null) {
        		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        	} else {
        		lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
        	}
            if(loggerUser.getAuditTime() != null) {
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null) {
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }
        } else {
        	lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
        }
    }
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}