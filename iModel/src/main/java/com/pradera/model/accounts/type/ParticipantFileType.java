package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum ParticipantFileType {
		
		
		DEPOSITARY_REGISTRATION_REQUEST(Integer.valueOf(9));
		
		/** The code. */
		private Integer code;

		
		/** The Constant list. */
		public static final List<ParticipantFileType> list = new ArrayList<ParticipantFileType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, ParticipantFileType> lookup = new HashMap<Integer, ParticipantFileType>();

		static {
			for (ParticipantFileType s : EnumSet.allOf(ParticipantFileType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		/**
		 * Instantiates a new document type.
		 *
		 * @param code the code
		 * @param value the value
		 * @param abrev the abrev
		 */
		private ParticipantFileType(Integer code) {
			this.code = code;			
		}
		
		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}
		
		
		/**
		 * Gets the description.
		 *
		 * @param locale the locale
		 * @return the description
		 */
		public String getDescription(Locale locale) {
			return null;
		}
		
		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the document type
		 */
		public static ParticipantFileType get(Integer code) {
			return lookup.get(code);
		}
}