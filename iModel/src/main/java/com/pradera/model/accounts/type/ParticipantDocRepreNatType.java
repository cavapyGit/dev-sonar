package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum ParticipantDocRepreNatType {
		
		POR(Integer.valueOf(328),"PODER OTORGADO COMO REPRESENTANTE",Integer.valueOf(191)),
		CI(Integer.valueOf(329),"CEDULA DE IDENTIDAD",Integer.valueOf(191)), 
		PAS(Integer.valueOf(330),"PASAPORTE",Integer.valueOf(191)),
		LIC(Integer.valueOf(331),"LICENCIA DE CONDUCIR",Integer.valueOf(191)),
		DIO(Integer.valueOf(332),"DOCUMENTO DE IDENTIDAD DE PAIS ORIGEN",Integer.valueOf(191)),
		RNC(Integer.valueOf(333),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(191)), 
		DJCP(Integer.valueOf(334),"DECLARACION JURADA SOBRE CONDICION DE PEP",Integer.valueOf(191)),
	    OC(Integer.valueOf(335),"ORDEN DE CUSTODIA",Integer.valueOf(191)),
		CATL(Integer.valueOf(336),"DOCUMENTO DE ACREDITACION COMO TUTOR LEGAL",Integer.valueOf(191)),
		CDRL(Integer.valueOf(337),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(191));	
		
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<ParticipantDocRepreNatType> list = new ArrayList<ParticipantDocRepreNatType>();
		public static final Map<Integer, ParticipantDocRepreNatType> lookup = new HashMap<Integer, ParticipantDocRepreNatType>();

		static {
			for (ParticipantDocRepreNatType s : EnumSet.allOf(ParticipantDocRepreNatType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private ParticipantDocRepreNatType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static ParticipantDocRepreNatType get(Integer code) {
			return lookup.get(code);
		}

		
}
