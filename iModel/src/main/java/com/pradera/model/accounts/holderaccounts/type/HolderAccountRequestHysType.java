/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountRequestHysType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/03/2013
 */
public enum HolderAccountRequestHysType {
	
	/** The create. */
	CREATE(new Integer(472),"CREACION","Creado"),
	
	/** The modify. */
	MODIFY(new Integer(473),"MODIFICACION","Modificado"),
	
	/** The block. */
	BLOCK(new Integer(474),"BLOQUEO","Bloqueado"),
	
	/** The unblock. */
	UNBLOCK(new Integer(475),"DESBLOQUEO","Desbloqueado"),
	
	/** The close. */
	CLOSE(new Integer(476),"CIERRE","Cerrado");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The optional value. */
	private String optionalValue;
	
	/** The Constant list. */
	public static final List<HolderAccountRequestHysType> list = new ArrayList<HolderAccountRequestHysType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountRequestHysType> lookup = new HashMap<Integer, HolderAccountRequestHysType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountRequestHysType> listSomeElements(HolderAccountRequestHysType... transferSecuritiesTypeParams){
		List<HolderAccountRequestHysType> retorno = new ArrayList<HolderAccountRequestHysType>();
		for(HolderAccountRequestHysType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountRequestHysType s : EnumSet.allOf(HolderAccountRequestHysType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account request hys type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountRequestHysType(Integer codigo, String valor, String optionalValue) {
		this.code = codigo;
		this.value = valor;
		this.optionalValue = optionalValue;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account request hys type
	 */
	public static HolderAccountRequestHysType get(Integer codigo) {
		return lookup.get(codigo);
	}

	/**
	 * Gets the optional value.
	 *
	 * @return the optional value
	 */
	public String getOptionalValue() {
		return optionalValue;
	}

	/**
	 * Sets the optional value.
	 *
	 * @param optionalValue the new optional value
	 */
	public void setOptionalValue(String optionalValue) {
		this.optionalValue = optionalValue;
	}
}