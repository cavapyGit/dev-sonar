package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PARTICIPANT_REQUEST_HISTORY database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/03/2013
 */
@Entity
@Table(name="PARTICIPANT_REQUEST_HISTORY")
public class ParticipantRequestHistory implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant req hist pk. */
	@Id
	@SequenceGenerator(name="PARTICIPANT_REQUEST_HISTORY_IDPARTICIPANTREQHISTPK_GENERATOR", sequenceName="SQ_ID_PARTICIPANT_REQ_HIST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_REQUEST_HISTORY_IDPARTICIPANTREQHISTPK_GENERATOR")
	@Column(name="ID_PARTICIPANT_REQ_HIST_PK")
	private Long idParticipantReqHistPk;

	/** The account class. */
	@Column(name="ACCOUNT_CLASS")
	private Integer accountClass;

	/** The account type. */
	@Column(name="ACCOUNT_TYPE")
	private Integer accountType;

	/** The address. */
	private String address;

	/** The comments. */
	private String comments;

	/** The commercial register. */
	@Column(name="COMMERCIAL_REGISTER")
	private String commercialRegister;

	/** The contact name. */
	@Column(name="CONTACT_NAME")
	private String contactName;

	/** The contact phone. */
	@Column(name="CONTACT_PHONE")
	private String contactPhone;

    /** The contract date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CONTRACT_DATE")
	private Date contractDate;

	/** The contract number. */
	@Column(name="CONTRACT_NUMBER")
	private String contractNumber;

    /** The creation date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	/** The currency. */
	private Integer currency;

	/** The department. */
	private Integer department;

	/** The description. */
	private String description;

	/** The district. */
	private Integer district;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private Integer economicActivity;
	
	@Column(name="INVESTOR_TYPE")
	private Integer investorType;

	/** The economic sector. */
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;

	/** The email. */
	private String email;

	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;

//	/** The holder. */
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_HOLDER_FK")
//	private Holder holder;
//
//	/** The issuer. */
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_ISSUER_FK")
//	private Issuer issuer;
	
	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	/** The participant request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_REQUEST_FK")
	private ParticipantRequest participantRequest;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The mnemonic. */
	private String mnemonic;

	/** The participant state. */
	@Column(name="PARTICIPANT_STATE")
	private Integer participantState;

	/** The phone number. */
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;

	/** The province. */
	private Integer province;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The residence country. */
	@Column(name="RESIDENCE_COUNTRY")
	private Integer residenceCountry;

	/** The social capital. */
	@Column(name="SOCIAL_CAPITAL")
	private Double socialCapital;

	/** The superintendent resolution. */
	@Column(name="SUPERINTENDENT_RESOLUTION")
	private String superintendentResolution;

    /** The superintendent resolution date. */
    @Temporal( TemporalType.DATE)
	@Column(name="SUPERINTENDENT_RESOLUTION_DATE")
	private Date superintendentResolutionDate;

	/** The website. */
	private String website;
	
	/** The role participant. */
	@Column(name="ROLE_PARTICIPANT")	
	private Integer roleParticipant;
	
	/** The contact email. */
	@Column(name="CONTACT_EMAIL")	
	private String contactEmail;
	
	/** The ind pay commission. */
	@Column(name="IND_PAY_COMMISSION")	
	private Integer indPayCommission;
	
	/** The ind depositary. */
	@Column(name="IND_DEPOSITARY")	
	private Integer indDepositary;
	
	/** The ind Managed Third. */
	@Column(name="IND_MANAGED_THIRD")	
	private Integer indManagedThird;
	
	/** The bic code. */
    @Column(name="BIC_CODE")
	private String bicCode;
    
    /** The ind settlement incharge. */
	@Column(name="IND_SETTLEMENT_INCHARGE")	
	private Integer indSettlementIncharge;
    
    /** The ind new. */
    @Column(name="IND_NEW")    
    private Integer indNew;
    
    @Column(name="DOCUMENT_SOURCE")
	private Integer documentSource;
    
    /** The bcb code. */
    @Column(name="ID_BCB_CODE")
	private String idBcbCode;
    
    /** The indicator of issuer or matrix account  */
    @Column(name="ACCOUNTS_STATE")
    private Integer accountsState;
    
    /** The issuer. */
	@Column(name="ID_ISSUER_FK")
	private String issuerFk;

	/** The residence country description. */
	@Transient
	private String residenceCountryDescription;
	
	/** The department description. */
	@Transient
	private String departmentDescription;
	
	/** The privince description. */
	@Transient
	private String provinceDescription;
	
	/** The district description. */
	@Transient
	private String districtDescription;
	
	/** The account type description. */
	@Transient
	private String accountTypeDescription;
	
	/** The account class description. */
	@Transient
	private String accountClassDescription;
	
	/** The document type description. */
	@Transient
	private String documentTypeDescription;
	
	/** The full document. */
	@Transient
	private String fullDocument;
	
	/** The role participant description. */
	@Transient
	private String roleParticipantDescription;
	
	/** The economic activity description. */
	@Transient
	private String economicActivityDescription;
	
	@Transient
	private String investorTypeDescription;
	
	/** The economic sector description. */
	@Transient
	private String economicSectorDescription;
	
	@Transient
	private Issuer issuer;
	
	
	/**
	 * 	bcb code
	 */
	public String getIdBcbCode() {
		return idBcbCode;
	}

	public void setIdBcbCode(String idBcbCode) {
		this.idBcbCode = idBcbCode;
	}	

	public Integer getAccountsState() {
		return accountsState;
	}

	public void setAccountsState(Integer accountsState) {
		this.accountsState = accountsState;
	}

	/**
     * Instantiates a new participant request history.
     */
    public ParticipantRequestHistory() {
    }
    
    /**
     * Gets the ind swift description.
     *
     * @return the ind swift description
     */
    public String getIndSwiftDescription(){
    	if(Validations.validateIsNotNull(bicCode)){
    		if(Validations.validateIsNotNullAndNotEmpty(bicCode) &&
    				bicCode.length() == 11){
//				if(bicCode.substring(7, 8).matches(GeneralConstants.REGEX_CHAR)) {
//					return BooleanType.YES.getValue();
//				} else if(bicCode.substring(7, 8).matches(GeneralConstants.REGEX_NUMBER)) {
//					return BooleanType.NO.getValue();
//				} else {
//					return null;
//				}
    			return null;
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the ind pay commission description.
     *
     * @return the ind pay commission description
     */
    public String getIndPayCommissionDescription(){
    	if(Validations.validateIsNotNull(indPayCommission)){
    		return BooleanType.get(indPayCommission).getValue();
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the ind cavapy description.
     *
     * @return the ind cavapy description
     */
    public String getIndDepositaryDescription(){
    	if(Validations.validateIsNotNull(indDepositary)){
    		return BooleanType.get(indDepositary).getValue();
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the ind Managed Third description.
     *
     * @return the ind Managed Third description
     */
    public String getIndManagedThirdDescription(){
    	if(Validations.validateIsNotNull(indManagedThird)){
    		return BooleanType.get(indManagedThird).getValue();
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the ind settlement incharge description.
     *
     * @return the ind settlement incharge description
     */
    public String getIndSettlementInchargeDescription(){
    	if(Validations.validateIsNotNull(indSettlementIncharge)){
    		return BooleanType.get(indSettlementIncharge).getValue();
    	} else {
    		return null;
    	}
    }    

	/**
	 * Gets the id participant req hist pk.
	 *
	 * @return the id participant req hist pk
	 */
	public Long getIdParticipantReqHistPk() {
		return idParticipantReqHistPk;
	}

	/**
	 * Sets the id participant req hist pk.
	 *
	 * @param idParticipantReqHistPk the new id participant req hist pk
	 */
	public void setIdParticipantReqHistPk(Long idParticipantReqHistPk) {
		this.idParticipantReqHistPk = idParticipantReqHistPk;
	}

	/**
	 * Gets the account class.
	 *
	 * @return the account class
	 */
	public Integer getAccountClass() {
		return accountClass;
	}

	/**
	 * Sets the account class.
	 *
	 * @param accountClass the new account class
	 */
	public void setAccountClass(Integer accountClass) {
		this.accountClass = accountClass;
	}

	/**
	 * Gets the account type.
	 *
	 * @return the account type
	 */
	public Integer getAccountType() {
		return accountType;
	}

	/**
	 * Sets the account type.
	 *
	 * @param accountType the new account type
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the commercial register.
	 *
	 * @return the commercial register
	 */
	public String getCommercialRegister() {
		return commercialRegister;
	}

	/**
	 * Sets the commercial register.
	 *
	 * @param commercialRegister the new commercial register
	 */
	public void setCommercialRegister(String commercialRegister) {
		this.commercialRegister = commercialRegister;
	}

	/**
	 * Gets the contact name.
	 *
	 * @return the contact name
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * Sets the contact name.
	 *
	 * @param contactName the new contact name
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	/**
	 * Gets the contact phone.
	 *
	 * @return the contact phone
	 */
	public String getContactPhone() {
		return contactPhone;
	}

	/**
	 * Sets the contact phone.
	 *
	 * @param contactPhone the new contact phone
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	/**
	 * Gets the contract date.
	 *
	 * @return the contract date
	 */
	public Date getContractDate() {
		return contractDate;
	}

	/**
	 * Sets the contract date.
	 *
	 * @param contractDate the new contract date
	 */
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	/**
	 * Gets the contract number.
	 *
	 * @return the contract number
	 */
	public String getContractNumber() {
		return contractNumber;
	}

	/**
	 * Sets the contract number.
	 *
	 * @param contractNumber the new contract number
	 */
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public Integer getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(Integer department) {
		this.department = department;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the district.
	 *
	 * @return the district
	 */
	public Integer getDistrict() {
		return district;
	}

	/**
	 * Sets the district.
	 *
	 * @param district the new district
	 */
	public void setDistrict(Integer district) {
		this.district = district;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public Integer getEconomicActivity() {
		return economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the new economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the new fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
//	public Holder getHolder() {
//		return holder;
//	}
//
//	/**
//	 * Sets the holder.
//	 *
//	 * @param holder the new holder
//	 */
//	public void setHolder(Holder holder) {
//		this.holder = holder;
//	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
//	public Issuer getIssuer() {
//		return issuer;
//	}
//
//	/**
//	 * Sets the issuer.
//	 *
//	 * @param issuer the new issuer
//	 */
//	public void setIssuer(Issuer issuer) {
//		this.issuer = issuer;
//	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the participant request.
	 *
	 * @return the participant request
	 */
	public ParticipantRequest getParticipantRequest() {
		return participantRequest;
	}

	/**
	 * Sets the participant request.
	 *
	 * @param participantRequest the new participant request
	 */
	public void setParticipantRequest(ParticipantRequest participantRequest) {
		this.participantRequest = participantRequest;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the participant state.
	 *
	 * @return the participant state
	 */
	public Integer getParticipantState() {
		return participantState;
	}

	/**
	 * Sets the participant state.
	 *
	 * @param participantState the new participant state
	 */
	public void setParticipantState(Integer participantState) {
		this.participantState = participantState;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber the new phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public Integer getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 *
	 * @param province the new province
	 */
	public void setProvince(Integer province) {
		this.province = province;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return registryType;
	}

	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the residence country.
	 *
	 * @return the residence country
	 */
	public Integer getResidenceCountry() {
		return residenceCountry;
	}

	/**
	 * Sets the residence country.
	 *
	 * @param residenceCountry the new residence country
	 */
	public void setResidenceCountry(Integer residenceCountry) {
		this.residenceCountry = residenceCountry;
	}

	/**
	 * Gets the social capital.
	 *
	 * @return the social capital
	 */
	public Double getSocialCapital() {
		return socialCapital;
	}

	/**
	 * Sets the social capital.
	 *
	 * @param socialCapital the new social capital
	 */
	public void setSocialCapital(Double socialCapital) {
		this.socialCapital = socialCapital;
	}

	/**
	 * Gets the superintendent resolution.
	 *
	 * @return the superintendent resolution
	 */
	public String getSuperintendentResolution() {
		return superintendentResolution;
	}

	/**
	 * Sets the superintendent resolution.
	 *
	 * @param superintendentResolution the new superintendent resolution
	 */
	public void setSuperintendentResolution(String superintendentResolution) {
		this.superintendentResolution = superintendentResolution;
	}

	/**
	 * Gets the superintendent resolution date.
	 *
	 * @return the superintendent resolution date
	 */
	public Date getSuperintendentResolutionDate() {
		return superintendentResolutionDate;
	}

	/**
	 * Sets the superintendent resolution date.
	 *
	 * @param superintendentResolutionDate the new superintendent resolution date
	 */
	public void setSuperintendentResolutionDate(Date superintendentResolutionDate) {
		this.superintendentResolutionDate = superintendentResolutionDate;
	}

	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * Sets the website.
	 *
	 * @param website the new website
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * Gets the role participant.
	 *
	 * @return the role participant
	 */
	public Integer getRoleParticipant() {
		return roleParticipant;
	}

	/**
	 * Sets the role participant.
	 *
	 * @param roleParticipant the new role participant
	 */
	public void setRoleParticipant(Integer roleParticipant) {
		this.roleParticipant = roleParticipant;
	}

	/**
	 * Gets the contact email.
	 *
	 * @return the contact email
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * Sets the contact email.
	 *
	 * @param contactEmail the new contact email
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * Gets the ind pay commission.
	 *
	 * @return the ind pay commission
	 */
	public Integer getIndPayCommission() {
		return indPayCommission;
	}

	/**
	 * Sets the ind pay commission.
	 *
	 * @param indPayCommission the new ind pay commission
	 */
	public void setIndPayCommission(Integer indPayCommission) {
		this.indPayCommission = indPayCommission;
	}
	
	/**
	 * Gets the ind cavapy.
	 *
	 * @return the ind cavapy
	 */
	public Integer getIndDepositary() {
		return indDepositary;
	}

	/**
	 * Sets the ind cavapy.
	 *
	 * @param indPayCommission the new ind cavapy
	 */
	public void setIndDepositary(Integer indDepositary) {
		this.indDepositary = indDepositary;
	}
	
	/**
	 * Gets the ind Managed Third.
	 *
	 * @return the ind Managed Third
	 */
	public Integer getIndManagedThird() {
		return indManagedThird;
	}
	
	/**
	 * Sets the ind Managed Third.
	 *
	 * @param indManagedThird the new Managed Third
	 */
	public void setIndManagedThird(Integer indManagedThird) {
		this.indManagedThird = indManagedThird;
	}

	/**
	 * Gets the residence country description.
	 *
	 * @return the residence country description
	 */
	public String getResidenceCountryDescription() {
		return residenceCountryDescription;
	}

	/**
	 * Sets the residence country description.
	 *
	 * @param residenceCountryDescription the new residence country description
	 */
	public void setResidenceCountryDescription(String residenceCountryDescription) {
		this.residenceCountryDescription = residenceCountryDescription;
	}

	/**
	 * Gets the province description.
	 *
	 * @return the province description
	 */
	public String getProvinceDescription() {
		return provinceDescription;
	}

	/**
	 * Sets the province description.
	 *
	 * @param provinceDescription the new province description
	 */
	public void setProvinceDescription(String provinceDescription) {
		this.provinceDescription = provinceDescription;
	}

	/**
	 * Gets the district description.
	 *
	 * @return the district description
	 */
	public String getDistrictDescription() {
		return districtDescription;
	}

	/**
	 * Sets the district description.
	 *
	 * @param districtDescription the new district description
	 */
	public void setDistrictDescription(String districtDescription) {
		this.districtDescription = districtDescription;
	}	
	
	/**
	 * Gets the bic code.
	 *
	 * @return the bic code
	 */
	public String getBicCode() {
		return bicCode;
	}

	/**
	 * Sets the bic code.
	 *
	 * @param bicCode the new bic code
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}

	/**
	 * Gets the ind new.
	 *
	 * @return the ind new
	 */
	public Integer getIndNew() {
		return indNew;
	}

	/**
	 * Sets the ind new.
	 *
	 * @param indNew the new ind new
	 */
	public void setIndNew(Integer indNew) {
		this.indNew = indNew;
	}	

	/**
	 * Gets the ind settlement incharge.
	 *
	 * @return the ind settlement incharge
	 */
	public Integer getIndSettlementIncharge() {
		return indSettlementIncharge;
	}

	/**
	 * Sets the ind settlement incharge.
	 *
	 * @param indSettlementIncharge the new ind settlement incharge
	 */
	public void setIndSettlementIncharge(Integer indSettlementIncharge) {
		this.indSettlementIncharge = indSettlementIncharge;
	}		

	/**
	 * Gets the department description.
	 *
	 * @return the department description
	 */
	public String getDepartmentDescription() {
		return departmentDescription;
	}

	/**
	 * Sets the department description.
	 *
	 * @param departmentDescription the new department description
	 */
	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}		

	/**
	 * Gets the account type description.
	 *
	 * @return the account type description
	 */
	public String getAccountTypeDescription() {
		return accountTypeDescription;
	}

	/**
	 * Sets the account type description.
	 *
	 * @param accountTypeDescription the new account type description
	 */
	public void setAccountTypeDescription(String accountTypeDescription) {
		this.accountTypeDescription = accountTypeDescription;
	}

	/**
	 * Gets the account class description.
	 *
	 * @return the account class description
	 */
	public String getAccountClassDescription() {
		return accountClassDescription;
	}

	/**
	 * Sets the account class description.
	 *
	 * @param accountClassDescription the new account class description
	 */
	public void setAccountClassDescription(String accountClassDescription) {
		this.accountClassDescription = accountClassDescription;
	}

	/**
	 * Gets the document type description.
	 *
	 * @return the document type description
	 */
	public String getDocumentTypeDescription() {
		return documentTypeDescription;
	}

	/**
	 * Sets the document type description.
	 *
	 * @param documentTypeDescription the new document type description
	 */
	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	/**
	 * Gets the full document.
	 *
	 * @return the full document
	 */
	public String getFullDocument() {
		return fullDocument;
	}

	/**
	 * Sets the full document.
	 *
	 * @param fullDocument the new full document
	 */
	public void setFullDocument(String fullDocument) {
		this.fullDocument = fullDocument;
	}

	/**
	 * Gets the role participant description.
	 *
	 * @return the role participant description
	 */
	public String getRoleParticipantDescription() {
		return roleParticipantDescription;
	}

	/**
	 * Sets the role participant description.
	 *
	 * @param roleParticipantDescription the new role participant description
	 */
	public void setRoleParticipantDescription(String roleParticipantDescription) {
		this.roleParticipantDescription = roleParticipantDescription;
	}

	/**
	 * Gets the economic activity description.
	 *
	 * @return the economic activity description
	 */
	public String getEconomicActivityDescription() {
		return economicActivityDescription;
	}

	/**
	 * Sets the economic activity description.
	 *
	 * @param economicActivityDescription the new economic activity description
	 */
	public void setEconomicActivityDescription(String economicActivityDescription) {
		this.economicActivityDescription = economicActivityDescription;
	}

	/**
	 * Gets the economic sector description.
	 *
	 * @return the economic sector description
	 */
	public String getEconomicSectorDescription() {
		return economicSectorDescription;
	}

	/**
	 * Sets the economic sector description.
	 *
	 * @param economicSectorDescription the new economic sector description
	 */
	public void setEconomicSectorDescription(String economicSectorDescription) {
		this.economicSectorDescription = economicSectorDescription;
	}
	
	public Integer getDocumentSource() {
		return documentSource;
	}

	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}
	
	public String getIssuerFk() {
		return issuerFk;
	}

	public void setIssuerFk(String issuerFk) {
		this.issuerFk = issuerFk;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	public Integer getInvestorType() {
		return investorType;
	}

	public void setInvestorType(Integer investorType) {
		this.investorType = investorType;
	}

	public String getInvestorTypeDescription() {
		return investorTypeDescription;
	}

	public void setInvestorTypeDescription(String investorTypeDescription) {
		this.investorTypeDescription = investorTypeDescription;
	}

	/**
	 * Sets the audit.
	 *
	 * @param loggerUser the audit
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
    
}