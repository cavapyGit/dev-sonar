package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PARTICIPANT_INT_DEPOSITARY database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_INT_DEPOSITARY")
public class ParticipantIntDepositary implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@EmbeddedId
	private ParticipantIntDepositaryPK id;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state int depositary. */
	@Column(name="STATE_INT_DEPOSITARY")
	private Integer stateIntDepositary;
	
	/** The international depository. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERNATIONAL_DEPOSITORY_PK", referencedColumnName="ID_INTERNATIONAL_DEPOSITORY_PK", insertable=false,updatable=false)
	private InternationalDepository internationalDepository;
	
	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_PK", referencedColumnName="ID_PARTICIPANT_PK", insertable=false,updatable=false)
	private Participant participant;

	@PrePersist
	public void prePersist(){
		if(Validations.validateIsNull(id)){
			id = new ParticipantIntDepositaryPK();
		}
		if(Validations.validateIsNotNull(participant) && Validations.validateIsNotNull(internationalDepository)){
			id.setIdParticipantPk(participant.getIdParticipantPk());
			id.setIdInternationalDepositoryPk(internationalDepository.getIdInternationalDepositoryPk());
		}
	}
	
	
    /**
     * The Constructor.
     */
    public ParticipantIntDepositary() {
    }

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public ParticipantIntDepositaryPK getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 */
	public void setId(ParticipantIntDepositaryPK id) {
		this.id = id;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state int depositary.
	 *
	 * @return the state int depositary
	 */
	public Integer getStateIntDepositary() {
		return stateIntDepositary;
	}

	/**
	 * Sets the state int depositary.
	 *
	 * @param stateIntDepositary the state int depositary
	 */
	public void setStateIntDepositary(Integer stateIntDepositary) {
		this.stateIntDepositary = stateIntDepositary;
	}

	/**
	 * Gets the international depository.
	 *
	 * @return the international depository
	 */
	public InternationalDepository getInternationalDepository() {
		return internationalDepository;
	}

	/**
	 * Sets the international depository.
	 *
	 * @param internationalDepository the international depository
	 */
	public void setInternationalDepository(
			InternationalDepository internationalDepository) {
		this.internationalDepository = internationalDepository;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}	
	
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

}