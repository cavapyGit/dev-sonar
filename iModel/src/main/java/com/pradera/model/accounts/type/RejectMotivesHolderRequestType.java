package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum RejectMotivesHolderRequestType {
		
		DATA_WONG(Integer.valueOf(480),"DOCUMENTACION INSUFICIENTE O INCORRECTA"), 
		LACK_SUPPORT_DOCUMENTS_ATTACHED	(Integer.valueOf(481),"DATOS CARGADOS INCORRECTOS"), 
		OTHER_MOTIVES(Integer.valueOf(482),"A SOLICITUD DEL PARTICIPANTE");
		
		
		private Integer code;
		private String value;
		

		public static final List<RejectMotivesHolderRequestType> list = new ArrayList<RejectMotivesHolderRequestType>();
		public static final Map<Integer, RejectMotivesHolderRequestType> lookup = new HashMap<Integer, RejectMotivesHolderRequestType>();

		static {
			for (RejectMotivesHolderRequestType s : EnumSet.allOf(RejectMotivesHolderRequestType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private RejectMotivesHolderRequestType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static RejectMotivesHolderRequestType get(Integer code) {
			return lookup.get(code);
		}

		
}
