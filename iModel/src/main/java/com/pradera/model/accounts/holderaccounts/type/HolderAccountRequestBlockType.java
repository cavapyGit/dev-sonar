/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.accounts.holderaccounts.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
public enum HolderAccountRequestBlockType {
	/** The natural. */
	PARTICIPANT(new Integer(106),"PARTICIPANTE"),
	
	/** The juridic. */
	SUPER_INTENDENCE(new Integer(107),"SUPERINTENDENCIA"),
	
	/** The ownership. */
	INVERTIONIST(new Integer(108),"INVERSIONISTA");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<HolderAccountRequestBlockType> list = new ArrayList<HolderAccountRequestBlockType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderAccountRequestBlockType> lookup = new HashMap<Integer, HolderAccountRequestBlockType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<HolderAccountRequestBlockType> listSomeElements(HolderAccountRequestBlockType... transferSecuritiesTypeParams){
		List<HolderAccountRequestBlockType> retorno = new ArrayList<HolderAccountRequestBlockType>();
		for(HolderAccountRequestBlockType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (HolderAccountRequestBlockType s : EnumSet.allOf(HolderAccountRequestBlockType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private HolderAccountRequestBlockType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static HolderAccountRequestBlockType get(Integer codigo) {
		return lookup.get(codigo);
	}
}