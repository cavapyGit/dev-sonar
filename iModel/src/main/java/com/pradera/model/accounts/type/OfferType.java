package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * This Enum contains the Economic Sectors.
 */
public enum OfferType {
	
	/** Valores de Oferta Publica. */
	PUBLIC_OFFER(Integer.valueOf(2450),"EMISOR VALORES DE OFERTA PUBLICA"),
	
	/** Valores de Oferta Privada. */
	PRIVATE_OFFER(Integer.valueOf(2451),"EMISOR VALORES DE OFERTA PRIVADA"),
	
	/** Valores de Oferta Publica y Privada. */
	PUBLIC_PRIVATE_OFFER(Integer.valueOf(2452),"EMISOR VALORES DE OFERTA PUBLICA Y PRIVADA");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new economic sector type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private OfferType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<OfferType> list = new ArrayList<OfferType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, OfferType> lookup = new HashMap<Integer, OfferType>();
	static {
		for (OfferType s : EnumSet.allOf(OfferType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the economic sector type
	 */
	public static OfferType get(Integer code) {
		return lookup.get(code);
	}

}
