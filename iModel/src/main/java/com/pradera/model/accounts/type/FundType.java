package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum FundType {
	
	FCC(Integer.valueOf(1), "FONDOS DE CAPITALIZACIÓN COMUNITARIO"),
	FCI(Integer.valueOf(2), "FONDOS DE CAPITALIZACIÓN INDIVIDUAL");
	
	private Integer code;
	private String value;
	
	private FundType(Integer code, String value) {
		this.code=code;
		this.value=value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public static final List<FundType> list = new ArrayList<FundType>();
	public static final Map<Integer, FundType> lookup = new HashMap<Integer, FundType>();
	static {
		for (FundType s : EnumSet.allOf(FundType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static FundType get(Integer code) {
		return lookup.get(code);
	}
}