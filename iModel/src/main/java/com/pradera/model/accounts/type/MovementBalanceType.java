package com.pradera.model.accounts.type;

public enum MovementBalanceType {
	
	BLOCK_BALANCE_BAN(new Long(300101), "AFECTACION POR BLOQUEO DE SALDO EN EMBARGO"),
	BLOCK_BALANCE_OTHERS(new Long(300104), "AFECTACION POR BLOQUEO DE SALDO EN OTROS"),
	BLOCK_BALANCE_PAWN(new Long(300098), "AFECTACION POR BLOQUEO DE SALDO EN GRAVAMEN");
	
	
	private Long code;
	
	private String descripcion;

	private MovementBalanceType(Long code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
