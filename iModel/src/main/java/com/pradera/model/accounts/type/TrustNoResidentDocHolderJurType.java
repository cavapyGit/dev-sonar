package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum TrustNoResidentDocHolderJurType {
		
		CE(Integer.valueOf(1077),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(1078),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(1079),"PASAPORTE"), 
		LIC(Integer.valueOf(1080),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(1081),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(1082),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(1083),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(1084),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(1085),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(1086),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(1087),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(1088),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(1089),"DECRETO"),		
	    RCF(Integer.valueOf(1090),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(1091),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(1092),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    private Integer code;
		private String value;
		
		public static final List<TrustNoResidentDocHolderJurType> list = new ArrayList<TrustNoResidentDocHolderJurType>();
		public static final Map<Integer, TrustNoResidentDocHolderJurType> lookup = new HashMap<Integer, TrustNoResidentDocHolderJurType>();

		static {
			for (TrustNoResidentDocHolderJurType s : EnumSet.allOf(TrustNoResidentDocHolderJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private TrustNoResidentDocHolderJurType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static TrustNoResidentDocHolderJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
