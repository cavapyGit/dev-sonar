package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * The Enum ParticipantBlockUnblockRequestType.
 */
public enum ParticipantRequestType {
	/* Types for Block, Unblock and Annulment Participant */
	/** The block. */
	BLOCK(Integer.valueOf(552),"BLOQUEO"),
	
	/** The unblock. */
	UNBLOCK(Integer.valueOf(553),"DESBLOQUEO"),
	
	/** The annulment. */
	ANNULMENT(Integer.valueOf(554),"ANULACION"),
	
	/* Type for Participant modification */
	/** The modification. */
	MODIFICATION(Integer.valueOf(696),"MODIFICACION");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ParticipantRequestType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<ParticipantRequestType> list = new ArrayList<ParticipantRequestType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantRequestType> lookup = new HashMap<Integer, ParticipantRequestType>();
	static {
		for (ParticipantRequestType s : EnumSet.allOf(ParticipantRequestType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant block unblock request type
	 */
	public static ParticipantRequestType get(Integer code) {
		return lookup.get(code);
	}

}
