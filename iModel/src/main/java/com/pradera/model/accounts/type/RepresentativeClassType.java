package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum RepresentativeClassType {
		
		FATHER(Integer.valueOf(83),"PADRE"),
		TUTOR_LEGAL(Integer.valueOf(84),"TUTOR LEGAL"),	
		LEGAL(Integer.valueOf(85),"LEGAL"),
		FIDUCIARY(Integer.valueOf(444),"FIDUCIARIO"),
		AUTHORIZED(Integer.valueOf(2829), "AUTORIZADO");
		
		
		private Integer code;
		private String value;
		

		public static final List<RepresentativeClassType> list = new ArrayList<RepresentativeClassType>();
		public static final Map<Integer, RepresentativeClassType> lookup = new HashMap<Integer, RepresentativeClassType>();

		static {
			for (RepresentativeClassType s : EnumSet.allOf(RepresentativeClassType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private RepresentativeClassType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static RepresentativeClassType get(Integer code) {
			return lookup.get(code);
		}

		
}
