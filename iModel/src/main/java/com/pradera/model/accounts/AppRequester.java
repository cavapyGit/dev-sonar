package com.pradera.model.accounts;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;

/**
* <ul><li>Copyright EDV 2020.</li></ul> 
* The Class Applicant.
* @author RCHIARA.
*/
@Entity
@Table(name="APP_REQUESTER")
public class AppRequester implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="APPREQUESTERPK_GEN",sequenceName="SECID_APPREQUESTERPK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="APPREQUESTERPK_GEN")
	@Column(name="ID_APP_REQUESTER_PK")
	private Long idAppRequesterPk;

	@Column(name="NAME_REQUESTER")
	private String nameRequester;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp =  loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
			if(loggerUser.getAuditTime() != null) {
				lastModifyDate = loggerUser.getAuditTime();
			} else {
				lastModifyDate = objLoggerUser.getAuditTime();
			}
			if(loggerUser.getIpAddress() != null) {
				lastModifyIp = loggerUser.getIpAddress();
			} else {
				lastModifyIp = objLoggerUser.getIpAddress();
			}
			if(loggerUser.getUserName() != null) {
				lastModifyUser = loggerUser.getUserName();
			} else {
				lastModifyUser = objLoggerUser.getUserName();
			}
		} else {
			lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdAppRequesterPk() {
		return idAppRequesterPk;
	}

	public void setIdAppRequesterPk(Long idAppRequesterPk) {
		this.idAppRequesterPk = idAppRequesterPk;
	}

	public String getNameRequester() {
		return nameRequester;
	}

	public void setNameRequester(String nameRequester) {
		this.nameRequester = nameRequester;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
}
