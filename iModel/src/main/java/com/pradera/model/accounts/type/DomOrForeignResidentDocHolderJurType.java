package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum DomOrForeignResidentDocHolderJurType {
		
		CE(Integer.valueOf(1013),"CEDULA DE IDENTIDAD"),
		NUI(Integer.valueOf(1014),"NUMERO UNICO DE IDENTIFICACION"),	
		PASS(Integer.valueOf(1015),"PASAPORTE"), 
		LIC(Integer.valueOf(1016),"LICENCIA DE CONDUCIR"),
		DIPO(Integer.valueOf(1017),"DOCUMENTO DE IDENTIDAD DEL PAIS ORIGEN"),
		ACTN(Integer.valueOf(1018),"ACTA DE NACIMIENTO"),
		RNC(Integer.valueOf(1019),"REGISTRO NACIONAL DE CONTRIBUYENTE"),
		FCASC(Integer.valueOf(1020),"FORMULARIO CONOZCA A SU CLIENTE"),		
	    DJCP(Integer.valueOf(1021),"DECLARACION JURADA CONDICION PEP"),
	    CMAMCV(Integer.valueOf(1022),"CONTRATO DE MANDATO DE APERTURA Y MOVIMIENTO DE CUENTAS DE VALORES"),		
	    CRM(Integer.valueOf(1023),"CERTIFICACION DE REGISTRO MERCANTIL"),
	    CI(Integer.valueOf(1024),"CERTIFICACION DE INCORPORACION"),
	    DECT(Integer.valueOf(1025),"DECRETO"),		
	    RCF(Integer.valueOf(1026),"REGISTRO DE CONSTITUCION DEL FIDEICOMISO"),		
	    CAOCCRT(Integer.valueOf(1027),"COPIA CERTIFICADA DEL ACTA DEL ORGANO CORPORATIVO"),
	    CICGSOLP(Integer.valueOf(1028),"CERTIFICADO DE INCUMBENCY Y CERTIFICADO DE GOOD STANDING");
	    		
	    private Integer code;
		private String value;
		
		public static final List<DomOrForeignResidentDocHolderJurType> list = new ArrayList<DomOrForeignResidentDocHolderJurType>();
		public static final Map<Integer, DomOrForeignResidentDocHolderJurType> lookup = new HashMap<Integer, DomOrForeignResidentDocHolderJurType>();

		static {
			for (DomOrForeignResidentDocHolderJurType s : EnumSet.allOf(DomOrForeignResidentDocHolderJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private DomOrForeignResidentDocHolderJurType(Integer code, String value) {
			this.code = code;
			this.value = value;						
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static DomOrForeignResidentDocHolderJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
