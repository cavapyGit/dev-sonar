package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum UnableDocRepreJurType {
		
		
		RNC(Integer.valueOf(370),"REGISTRO NACIONAL DEL CONTRIBUYENTE",Integer.valueOf(197)),
		CDRL(Integer.valueOf(371),"CERTIFICACION QUE DESIGNA AL REPRESENTANTE LEGAL",Integer.valueOf(197)),	
		CRMF(Integer.valueOf(372),"CERTIFICADO DEL REGISTRO MERCANTIL DEL FIDUCIARIO",Integer.valueOf(197)), 
		DEFF(Integer.valueOf(373),"DOCUMENTO DE EJERCICIO DE FUNCIONES DEL FIDUCIARIO",Integer.valueOf(197));
		
		
		private Integer code;
		private String value;
		private Integer header;
		

		public static final List<UnableDocRepreJurType> list = new ArrayList<UnableDocRepreJurType>();
		public static final Map<Integer, UnableDocRepreJurType> lookup = new HashMap<Integer, UnableDocRepreJurType>();

		static {
			for (UnableDocRepreJurType s : EnumSet.allOf(UnableDocRepreJurType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private UnableDocRepreJurType(Integer code, String value,Integer header) {
			this.code = code;
			this.value = value;			
			this.header = header;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
			
		public Integer getHeader() {
			return header;
		}

		public String getDescription(Locale locale) {
			return null;
		}
		
		public static UnableDocRepreJurType get(Integer code) {
			return lookup.get(code);
		}

		
}
