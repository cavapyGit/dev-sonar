package com.pradera.model.accounts.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	public enum IssuerRequestType {
		
		BLOCK (Integer.valueOf(661),"BLOQUEO"), 
		UNBLOCK (Integer.valueOf(662),"DESBLOQUEO"),
		MODIFICATION (Integer.valueOf(1303),"MODIFICACION");
		
		private Integer code;
		private String value;
		

		public static final List<IssuerRequestType> list = new ArrayList<IssuerRequestType>();
		public static final Map<Integer, IssuerRequestType> lookup = new HashMap<Integer, IssuerRequestType>();

		static {
			for (IssuerRequestType s : EnumSet.allOf(IssuerRequestType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private IssuerRequestType(Integer code, String value) {
			this.code = code;
			this.value = value;			
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getDescription(Locale locale) {
			return null;
		}
		
		public static IssuerRequestType get(Integer code) {
			return lookup.get(code);
		}

		
}
