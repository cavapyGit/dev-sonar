package com.pradera.model.notification.type;

import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum NotificationStatus.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/11/2012
 */
public enum NotificationStatusType {

	/** The registered. */
	REGISTERED(Integer.valueOf(1447)),
	/** The readed. */
	DELIVERED(Integer.valueOf(1448));
	
	/** The code. */
	private Integer code;
	
	/**
	 * Instantiates a new notification status.
	 *
	 * @param code the code
	 */
	private NotificationStatusType(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/** The Constant lookup. */
	private static final Map<Integer, NotificationStatusType> lookup = new HashMap<Integer, NotificationStatusType>();
    static {
        for (NotificationStatusType d : NotificationStatusType.values())
            lookup.put(d.getCode(), d);
    }
    
    /**
     * Gets the.
     *
     * @param code the code
     * @return the notification status
     */
    public static NotificationStatusType get(Integer code) {
        return lookup.get(code);
    }
	
	
}
