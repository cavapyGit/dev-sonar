package com.pradera.model.notification;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * Copyright PraderaTechnologies 2013.
 * The persistent class for the DESTINATION_NOTIFICATION database table.
 * The Class DestinationNotification.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/02/2013
 */

@Entity
@Table(name="DESTINATION_NOTIFICATION")
public class DestinationNotification implements Serializable,Auditable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	

	/** The DESTINATION_NOTIFICATION table pk.*/
	@Id
	@SequenceGenerator(name="DESTI_NOTIFICATION_PK_GENERATOR", sequenceName="SQ_ID_DESTINATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DESTI_NOTIFICATION_PK_GENERATOR")
	@Column(name="ID_DESTINATION_PK")
	private Long idDestinationPk;
	
	/** The ProcessNotification Foreign Key. */
	@ManyToOne
	@JoinColumn(name = "ID_PROCESS_NOTIFICATION_FK",referencedColumnName="ID_PROCESS_NOTIFICATION_PK")
	private ProcessNotification processNotification;
	
	/** The FullName. */
	@Column(name="FULL_NAME")
	private String fullName;
	
	/** The Email. */
	@Column(name="EMAIL")
	private String email;
	
	/** The MobileNumber. */
	@Column(name="MOBILE_NUMBER")
	private String mobileNumber;
	
	/** The state. */
	@Column(name = "IND_STATUS")
	private Integer indStatus;
	
	/** The idUserAccount. */
	@Column(name = "ID_USER_ACCOUNT")
	private String idUserAccount;
	
	/** The institutionType. */
	@Column(name="INSTITUTION_TYPE")
	private Integer institutionType;
	
	/** The institution. */
	@Column(name="INSTITUTION")
	private String institution;
	
	/** The lastModifyApp. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The lastModifyIp. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The lastModifyUser. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The lastModifyDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Transient
	private boolean checked;
	
	@Transient
	private Integer tempId;
	
	
	 /**
 	 * Gets the id destination pk.
 	 *
 	 * @return the id destination pk
 	 */
 	public Long getIdDestinationPk() {
		return idDestinationPk;
	}
	
	/**
	 * Sets the id destination pk.
	 *
	 * @param idDestinationPk the new id destination pk
	 */
	public void setIdDestinationPk(Long idDestinationPk) {
		this.idDestinationPk = idDestinationPk;
	}
	
	/**
	 * Gets the process notification.
	 *
	 * @return the process notification
	 */
	public ProcessNotification getProcessNotification() {
		return processNotification;
	}
	
	/**
	 * Sets the process notification.
	 *
	 * @param processNotification the new process notification
	 */
	public void setProcessNotification(ProcessNotification processNotification) {
		this.processNotification = processNotification;
	}
	
	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	
	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	/**
	 * Gets the ind status.
	 *
	 * @return the ind status
	 */
	public Integer getIndStatus() {
		return indStatus;
	}
	
	/**
	 * Sets the ind status.
	 *
	 * @param indStatus the new ind status
	 */
	public void setIndStatus(Integer indStatus) {
		this.indStatus = indStatus;
	}
	
	/**
	 * Gets the id user account.
	 *
	 * @return the id user account
	 */
	public String getIdUserAccount() {
		return idUserAccount;
	}
	
	/**
	 * Sets the id user account.
	 *
	 * @param idUserAccount the new id user account
	 */
	public void setIdUserAccount(String idUserAccount) {
		this.idUserAccount = idUserAccount;
	}
	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	
	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	
	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	
	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	
	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	
	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	
	
	/**
	 * Get the tempId
	 * 
	 * @return the tempId
	 */
	public Integer getTempId() {
		return tempId;
	}

	/**
	 * Set the tempId
	 * 
	 * @param tempId the tempId to set
	 */
	public void setTempId(Integer tempId) {
		this.tempId = tempId;
	}

		/* (non-Javadoc)
		 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
		 */
		@Override
	    public void setAudit(LoggerUser loggerUser) {
	        // TODO review casuisticas no contempladas para mejorar
	        if (loggerUser != null) {
	            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
	            lastModifyDate = loggerUser.getAuditTime();
	            lastModifyIp = loggerUser.getIpAddress();
	            lastModifyUser = loggerUser.getUserName();
	        }
	    }
		/* (non-Javadoc)
		 * @see com.pradera.commons.audit.Auditable#getListForAudit()
		 */
		@Override
		public Map<String, List<? extends Auditable>> getListForAudit() {
	        return null;
		}

		public boolean isChecked() {
			return indStatus.equals(BooleanType.YES.getCode());
		}

		public void setChecked(boolean checked) {
			this.checked = checked;
			if(this.checked){
				indStatus = BooleanType.YES.getCode();
			} else {
				indStatus = BooleanType.NO.getCode();
			}
		}

		public Integer getInstitutionType() {
			return institutionType;
		}

		public void setInstitutionType(Integer institutionType) {
			this.institutionType = institutionType;
		}

		public String getInstitution() {
			return institution;
		}

		public void setInstitution(String institution) {
			this.institution = institution;
		}
	
	
}

