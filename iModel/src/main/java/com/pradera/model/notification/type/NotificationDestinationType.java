package com.pradera.model.notification.type;

import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum NotificationDestinationType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/07/2013
 */
public enum NotificationDestinationType {
	
	/** The executor. */
	EXECUTOR(Integer.valueOf(246)),
	
	/** The dynamic. */
	DYNAMIC(Integer.valueOf(1441)),
	
	/** The manual. */
	MANUAL(Integer.valueOf(1442)),
	
	/** The manual. */
	SUPERVISOR(Integer.valueOf(2320)),

	HOLDER_ACCOUNT(Integer.valueOf(2849)),

	FREE(Integer.valueOf(0)),
	;
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	private static final Map<Integer, NotificationDestinationType> lookup = new HashMap<Integer, NotificationDestinationType>();
    static {
        for (NotificationDestinationType d : NotificationDestinationType.values())
            lookup.put(d.getCode(), d);
    }

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Instantiates a new notification destination type.
	 *
	 * @param code the code
	 */
	private NotificationDestinationType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the notification destination type
	 */
	public static NotificationDestinationType get(Integer code){
		return lookup.get(code);
	}
	
}
