package com.pradera.model.notification;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.process.BusinessProcess;

/**
 * Copyright PraderaTechnologies 2015.
 * The persistent class for the NOTIFICATION_CONFIGURATION database table.
 * The Class NotificationConfiguration.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/07/2015
 */

@Entity
@Table(name = "NOTIFICATION_CONFIGURATION")
public class NotificationConfiguration implements Serializable ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The NOTIFICATION_CONFIGURATION table pk.*/
	@Id
	@SequenceGenerator(name="NOTIFICATION_CONFIG_PK_GENERATOR", sequenceName="SQ_ID_NOTIFICATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NOTIFICATION_CONFIG_PK_GENERATOR")
	@Column(name = "ID_NOTIFICATION_PK")
	private Long idNotificationPk;
	
	/** The parameter label. */
	@Column(name = "PARAM_LABEL")
	private String paramLabel;
	
	/** The parameter value. */
	@Column(name = "PARAM_VALUE")
	private String paramValue;
	
	/** The Business Process Foreign Key. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_BUSINESS_PROCESS_FK")
	private BusinessProcess businessProcess;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	

	/**
	 * @return the idNotificationPk
	 */
	public Long getIdNotificationPk() {
		return idNotificationPk;
	}

	/**
	 * @param idNotificationPk the idNotificationPk to set
	 */
	public void setIdNotificationPk(Long idNotificationPk) {
		this.idNotificationPk = idNotificationPk;
	}

	/**
	 * @return the paramLabel
	 */
	public String getParamLabel() {
		return paramLabel;
	}

	/**
	 * @param paramLabel the paramLabel to set
	 */
	public void setParamLabel(String paramLabel) {
		this.paramLabel = paramLabel;
	}

	/**
	 * @return the paramValue
	 */
	public String getParamValue() {
		return paramValue;
	}

	/**
	 * @param paramValue the paramValue to set
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	/**
	 * @return the businessProcess
	 */
	public BusinessProcess getBusinessProcess() {
		return businessProcess;
	}

	/**
	 * @param businessProcess the businessProcess to set
	 */
	public void setBusinessProcess(BusinessProcess businessProcess) {
		this.businessProcess = businessProcess;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
