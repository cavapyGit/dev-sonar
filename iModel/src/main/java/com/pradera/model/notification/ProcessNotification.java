package com.pradera.model.notification;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * Copyright PraderaTechnologies 2013.
 * The persistent class for the PROCESS_NOTIFICATION database table.
 * The Class ProcessNotification.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/02/2013
 */

@Entity
@Table(name = "PROCESS_NOTIFICATION")
public class ProcessNotification implements Serializable ,Auditable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Default Constructor.
	 */
	public ProcessNotification() {
	}

	/** The PROCESS_NOTIFICATION table pk.*/
	@Id
	@SequenceGenerator(name="NOTIFICATION_PK_GENERATOR", sequenceName="SQ_ID_PROCESS_NOTIFICATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NOTIFICATION_PK_GENERATOR")
	@Column(name = "ID_PROCESS_NOTIFICATION_PK")
	private Long idProcessNotificationPk;
	
	@Version
	private Long version;

	/** The Business Process Foreign Key. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_BUSINESS_PROCESS_FK")
	private BusinessProcess businessProcess;

	/** The idNotificationType. */
	@Column(name = "NOTIFICATION_TYPE")
	private Integer notificationType;
	
	/** The destination type. */
	@Column(name="DESTINATION_TYPE")
	private Integer destinationType;
	
	/** The institution type. */
	@Column(name="INSTITUTION_TYPE")
	private Integer institutionType;
	
	/** The user responsability. */
	@Column(name="USER_RESPONSABILITY")
	private Integer userResponsability;

	/** The description. */
	@Column(name = "DESCRIPTION")
	private String description;

	/** The state. */
	@Column(name = "IND_STATUS")
	private Integer indStatus;

	/** The notificationMessage. */
	@Column(name = "NOTIFICATION_MESSAGE")
	private String notificationMessage;

	/** The notificationMessage. */
	@Column(name = "NOTIFICATION_SUBJECT")
	private String notificationSubject;

	/** The lastModifyApp. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The lastModifyIp. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The lastModifyUser. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The lastModifyDate. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	// bi-directional many-to-one association to ProcessLogger
	/** The destination notifications. */
	@OneToMany(mappedBy = "processNotification", cascade = { CascadeType.ALL })
	private List<DestinationNotification> destinationNotifications;
		
	@Column(name="SYSTEM_PROFILE")
	private Integer systemProfile;	
	
	/** The user priority. */
	@Column(name="PRIORITY")
	private Integer priority;
	
	@Transient
	private String profileDescription;	
	
	/** The state description. */
	@Transient	
	private String stateDescription;
	
	/** The notification type description. */
	@Transient	
	private String notificationTypeDescription;
	
	@Transient
	private String destinationTypeDescription;
	@Transient
	private Long destinationSel;
	@Transient
    private boolean blExist;
	
	/**
	 * Gets the institution description.
	 *
	 * @return the institution description
	 */
	public String getInstitutionDescription(){
		String result = null;
//		if(institutionType != null && InstitutionType.get(institutionType) != null){
//			result = InstitutionType.get(institutionType).getValue();
//		}
		return result;
	}

	 /**
 	 * Gets the destination type.
 	 *
 	 * @return the destination type
 	 */
 	public Integer getDestinationType() {
		return destinationType;
	}

	/**
	 * Sets the destination type.
	 *
	 * @param destinationType the new destination type
	 */
	public void setDestinationType(Integer destinationType) {
		this.destinationType = destinationType;
	}

	/**
 	 * Gets the id process notification pk.
 	 *
 	 * @return the id process notification pk
 	 */
 	public Long getIdProcessNotificationPk() {
		return idProcessNotificationPk;
	}
	
	/**
	 * Sets the id process notification pk.
	 *
	 * @param idProcessNotificationPk the new id process notification pk
	 */
	public void setIdProcessNotificationPk(Long idProcessNotificationPk) {
		this.idProcessNotificationPk = idProcessNotificationPk;
	}
	
	/**
	 * Gets the business process.
	 *
	 * @return the business process
	 */
	public BusinessProcess getBusinessProcess() {
		return businessProcess;
	}
	
	/**
	 * Sets the business process.
	 *
	 * @param businessProcess the new business process
	 */
	public void setBusinessProcess(BusinessProcess businessProcess) {
		this.businessProcess = businessProcess;
	}
	
	/**
	 * Gets the notification type.
	 *
	 * @return the notification type
	 */
	public Integer getNotificationType() {
		return notificationType;
	}
	
	/**
	 * Sets the notification type.
	 *
	 * @param notificationType the new notification type
	 */
	public void setNotificationType(Integer notificationType) {
		this.notificationType = notificationType;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the ind status.
	 *
	 * @return the ind status
	 */
	public Integer getIndStatus() {
		return indStatus;
	}
	
	/**
	 * Sets the ind status.
	 *
	 * @param indStatus the new ind status
	 */
	public void setIndStatus(Integer indStatus) {
		this.indStatus = indStatus;
	}
	
	/**
	 * Gets the notification message.
	 *
	 * @return the notification message
	 */
	public String getNotificationMessage() {
		return notificationMessage;
	}
	
	/**
	 * Sets the notification message.
	 *
	 * @param notificationMessage the new notification message
	 */
	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}
	
	/**
	 * Gets the notification subject.
	 *
	 * @return the notification subject
	 */
	public String getNotificationSubject() {
		return notificationSubject;
	}
	
	/**
	 * Sets the notification subject.
	 *
	 * @param notificationSubject the new notification subject
	 */
	public void setNotificationSubject(String notificationSubject) {
		this.notificationSubject = notificationSubject;
	}
	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	
	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	
	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	
	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	
	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	
	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	
	/**
	 * Gets the destination notifications.
	 *
	 * @return the destination notifications
	 */
	public List<DestinationNotification> getDestinationNotifications() {
		return destinationNotifications;
	}
	
	/**
	 * Sets the destination notifications.
	 *
	 * @param destinationNotifications the new destination notifications
	 */
	public void setDestinationNotifications(
			List<DestinationNotification> destinationNotifications) {
		this.destinationNotifications = destinationNotifications;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	/**
	 * Sets the audit.
	 *
	 * @param loggerUser the new audit
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	/**
	 * Gets the list for audit.
	 *
	 * @return the list for audit
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>(); 
		detailsMap.put("destinationNotifications", destinationNotifications);
        return detailsMap;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the stateDescription
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the stateDescription to set
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription =stateDescription;
	}

	/**
	 * Gets the notification type description.
	 *
	 * @return the notificationTypeDescription
	 */
	public String getNotificationTypeDescription() {
		return notificationTypeDescription;
	}

	/**
	 * Sets the notification type description.
	 *
	 * @param notificationTypeDescription the notificationTypeDescription to set
	 */
	public void setNotificationTypeDescription(String notificationTypeDescription) {
		this.notificationTypeDescription = notificationTypeDescription;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the user responsability.
	 *
	 * @return the user responsability
	 */
	public Integer getUserResponsability() {
		return userResponsability;
	}

	/**
	 * Sets the user responsability.
	 *
	 * @param userResponsability the new user responsability
	 */
	public void setUserResponsability(Integer userResponsability) {
		this.userResponsability = userResponsability;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getDestinationTypeDescription() {
		return destinationTypeDescription;
	}

	public void setDestinationTypeDescription(String destinationTypeDescription) {
		this.destinationTypeDescription = destinationTypeDescription;
	}

	public Long getDestinationSel() {
		return destinationSel;
	}

	public void setDestinationSel(Long destinationSel) {
		this.destinationSel = destinationSel;
	}

	public boolean isBlExist() {
		return blExist;
	}

	public void setBlExist(boolean blExist) {
		this.blExist = blExist;
	}

	public Integer getSystemProfile() {
		return systemProfile;
	}
	
	public void setSystemProfile(Integer systemProfile) {
		this.systemProfile = systemProfile;
	}
		
	public String getProfileDescription() {
		return profileDescription;
	}

	public void setProfileDescription(String profileDescription) {
		this.profileDescription = profileDescription;
	}

	/**
	 * @return the priority
	 */
	public Integer getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idProcessNotificationPk == null) ? 0
						: idProcessNotificationPk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProcessNotification other = (ProcessNotification) obj;
		if (idProcessNotificationPk == null) {
			if (other.idProcessNotificationPk != null)
				return false;
		} else if (!idProcessNotificationPk
				.equals(other.idProcessNotificationPk))
			return false;
		return true;
	}
}
