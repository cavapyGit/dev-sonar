package com.pradera.model.notification;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;


/**
 * Copyright PraderaTechnologies 2013.
 * The persistent class for the NOTIFICATION_LOGGER database table.
 * The Class NotificationLogger.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/02/2013
 */

@Entity
@Table(name = "NOTIFICATION_LOGGER")
public class NotificationLogger implements Serializable, Auditable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 */
	public NotificationLogger() {
	}

	/** The NOTIFICATION_LOGGER table pk.*/
	@Id
	@SequenceGenerator(name="NOTIFICATION_LOG_PK_GENERATOR", sequenceName="SQ_ID_NOTIFICATION_LOGGER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NOTIFICATION_LOG_PK_GENERATOR")
	@Column(name = "ID_NOTIFICATION_LOGGER_PK")
	private Long idNotificationLoggerPk;

	/** The DestinationNotification Foreign Key. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_DESTINATION_FK")
	private DestinationNotification destinationNotification;

	/** The BusinessProcess Foreign Key. */
	@ManyToOne
	@JoinColumn(name = "ID_BUSINESS_PROCESS_FK")
	private BusinessProcess businessProcess;
	
	/** The process logger. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCESS_LOGGER_FK")
	private ProcessLogger processLogger;

	/** The lastModifyDate. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	/** The delivery date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DELIVERY_DATE")
	private Date deliveryDate;
	
	/** The ind delivered. */
	@Column(name="IND_DELIVERED")
	private Integer indDelivered;
	
	/** The notification type. */
	@Column(name="NOTIFICATION_TYPE")
	private Integer notificationType;
	
	/** The notification subject. */
	@Column(name="NOTIFICATION_SUBJECT")
	private String notificationSubject;
	
	/** The notificationMessage. */
	@Column(name="NOTIFICATION_MESSAGE")
	private String notificationMessage;
	
	/** The dest user name. */
	@Column(name="DESTINATION_USER_NAME")
	private String destUserName;
	
	/** The destinationEmail. */
	@Column(name="DESTINATION_EMAIL")
	private String destEmail;
	
	/** The destnMobileNumber. */
	@Column(name="DESTINATION_MOBILE_NUMBER")
	private String destMobileNumber;
	
	/** The destnFullName. */
	@Column(name="DESTINATION_FULL_NAME")
	private String destFullName;
	
	/** The source user name. */
	@Column(name="SOURCE_USER_NAME")
	private String sourceUserName;
	
	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The lastModifyUser. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The lastModifyDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	

	/**
	 * Gets the id notification logger pk.
	 *
	 * @return the id notification logger pk
	 */
	public Long getIdNotificationLoggerPk() {
		return idNotificationLoggerPk;
	}

	/**
	 * Sets the id notification logger pk.
	 *
	 * @param idNotificationLoggerPk the new id notification logger pk
	 */
	public void setIdNotificationLoggerPk(Long idNotificationLoggerPk) {
		this.idNotificationLoggerPk = idNotificationLoggerPk;
	}

	/**
	 * Gets the destination notification.
	 *
	 * @return the destination notification
	 */
	public DestinationNotification getDestinationNotification() {
		return destinationNotification;
	}

	/**
	 * Sets the destination notification.
	 *
	 * @param destinationNotification the new destination notification
	 */
	public void setDestinationNotification(
			DestinationNotification destinationNotification) {
		this.destinationNotification = destinationNotification;
	}

	/**
	 * Gets the business process.
	 *
	 * @return the business process
	 */
	public BusinessProcess getBusinessProcess() {
		return businessProcess;
	}

	/**
	 * Sets the business process.
	 *
	 * @param businessProcess the new business process
	 */
	public void setBusinessProcess(BusinessProcess businessProcess) {
		this.businessProcess = businessProcess;
	}

	/**
	 * Gets the process logger.
	 *
	 * @return the process logger
	 */
	public ProcessLogger getProcessLogger() {
		return processLogger;
	}

	/**
	 * Sets the process logger.
	 *
	 * @param processLogger the new process logger
	 */
	public void setProcessLogger(ProcessLogger processLogger) {
		this.processLogger = processLogger;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the delivery date.
	 *
	 * @return the delivery date
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * Sets the delivery date.
	 *
	 * @param deliveryDate the new delivery date
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * Gets the ind delivered.
	 *
	 * @return the ind delivered
	 */
	public Integer getIndDelivered() {
		return indDelivered;
	}

	/**
	 * Sets the ind delivered.
	 *
	 * @param indDelivered the new ind delivered
	 */
	public void setIndDelivered(Integer indDelivered) {
		this.indDelivered = indDelivered;
	}

	/**
	 * Gets the notification type.
	 *
	 * @return the notification type
	 */
	public Integer getNotificationType() {
		return notificationType;
	}

	/**
	 * Sets the notification type.
	 *
	 * @param notificationType the new notification type
	 */
	public void setNotificationType(Integer notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * Gets the notification subject.
	 *
	 * @return the notification subject
	 */
	public String getNotificationSubject() {
		return notificationSubject;
	}

	/**
	 * Sets the notification subject.
	 *
	 * @param notificationSubject the new notification subject
	 */
	public void setNotificationSubject(String notificationSubject) {
		this.notificationSubject = notificationSubject;
	}

	/**
	 * Gets the notification message.
	 *
	 * @return the notification message
	 */
	public String getNotificationMessage() {
		return notificationMessage;
	}

	/**
	 * Sets the notification message.
	 *
	 * @param notificationMessage the new notification message
	 */
	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	/**
	 * Gets the dest user name.
	 *
	 * @return the dest user name
	 */
	public String getDestUserName() {
		return destUserName;
	}

	/**
	 * Sets the dest user name.
	 *
	 * @param destUserName the new dest user name
	 */
	public void setDestUserName(String destUserName) {
		this.destUserName = destUserName;
	}

	/**
	 * Gets the dest email.
	 *
	 * @return the dest email
	 */
	public String getDestEmail() {
		return destEmail;
	}

	/**
	 * Sets the dest email.
	 *
	 * @param destEmail the new dest email
	 */
	public void setDestEmail(String destEmail) {
		this.destEmail = destEmail;
	}

	/**
	 * Gets the dest mobile number.
	 *
	 * @return the dest mobile number
	 */
	public String getDestMobileNumber() {
		return destMobileNumber;
	}

	/**
	 * Sets the dest mobile number.
	 *
	 * @param destMobileNumber the new dest mobile number
	 */
	public void setDestMobileNumber(String destMobileNumber) {
		this.destMobileNumber = destMobileNumber;
	}

	/**
	 * Gets the dest full name.
	 *
	 * @return the dest full name
	 */
	public String getDestFullName() {
		return destFullName;
	}

	/**
	 * Sets the dest full name.
	 *
	 * @param destFullName the new dest full name
	 */
	public void setDestFullName(String destFullName) {
		this.destFullName = destFullName;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the source user name.
	 *
	 * @return the source user name
	 */
	public String getSourceUserName() {
		return sourceUserName;
	}

	/**
	 * Sets the source user name.
	 *
	 * @param sourceUserName the new source user name
	 */
	public void setSourceUserName(String sourceUserName) {
		this.sourceUserName = sourceUserName;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	/**
	 * Sets the audit.
	 *
	 * @param loggerUser the new audit
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
			lastModifyApp=loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate=loggerUser.getAuditTime();
			lastModifyIp=loggerUser.getIpAddress();
			lastModifyUser=loggerUser.getUserName();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	/**
	 * Gets the list for audit.
	 *
	 * @return the list for audit
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return "NotificationLogger [idNotificationLoggerPk=" + idNotificationLoggerPk + ", destinationNotification="
				+ destinationNotification + ", businessProcess=" + businessProcess + ", processLogger=" + processLogger
				+ ", registryDate=" + registryDate + ", deliveryDate=" + deliveryDate + ", indDelivered=" + indDelivered
				+ ", notificationType=" + notificationType + ", notificationSubject=" + notificationSubject
				+ ", notificationMessage=" + notificationMessage + ", destUserName=" + destUserName + ", destEmail="
				+ destEmail + ", destMobileNumber=" + destMobileNumber + ", destFullName=" + destFullName
				+ ", sourceUserName=" + sourceUserName + ", lastModifyIp=" + lastModifyIp + ", lastModifyApp="
				+ lastModifyApp + ", lastModifyUser=" + lastModifyUser + ", lastModifyDate=" + lastModifyDate + "]";
	}

}
