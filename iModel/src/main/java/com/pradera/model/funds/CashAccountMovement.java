package com.pradera.model.funds;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.MovementType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the CASH_ACCOUNT_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="CASH_ACCOUNT_MOVEMENT")
public class CashAccountMovement implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CASH_ACCOUNT_MOVEMENT_GENERATOR", sequenceName="SQ_ID_CASH_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CASH_ACCOUNT_MOVEMENT_GENERATOR")
	@Column(name="ID_CASH_ACCOUNT_MOVEMENT_PK")
	private Long idCashAccountMovementPk;

	@Column(name="CURRENCY")
	private Integer currency;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MOVEMENT_AMOUNT")
	private BigDecimal movementAmount;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;
    
    @Column(name="MOVEMENT_CLASS")
    private Integer movementClass;

	//bi-directional many-to-one association to FundsOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_FK")
	private FundsOperation fundsOperation;

	//bi-directional many-to-one association to InstitutionCashAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_CASH_ACCOUNT_FK")
	private InstitutionCashAccount institutionCashAccount;

    
    
    public CashAccountMovement() {
    }

	public Long getIdCashAccountMovementPk() {
		return this.idCashAccountMovementPk;
	}

	public void setIdCashAccountMovementPk(Long idCashAccountMovementPk) {
		this.idCashAccountMovementPk = idCashAccountMovementPk;
	}

	public Integer getCurrency() {
		return this.currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public MovementType getMovementType() {
		return movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getMovementAmount() {
		return this.movementAmount;
	}

	public void setMovementAmount(BigDecimal movementAmount) {
		this.movementAmount = movementAmount;
	}

	public Date getMovementDate() {
		return this.movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public FundsOperation getFundsOperation() {
		return this.fundsOperation;
	}

	public void setFundsOperation(FundsOperation fundsOperation) {
		this.fundsOperation = fundsOperation;
	}
	
	public InstitutionCashAccount getInstitutionCashAccount() {
		return this.institutionCashAccount;
	}

	public void setInstitutionCashAccount(InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}

	public Integer getMovementClass() {
		return movementClass;
	}

	public void setMovementClass(Integer movementClass) {
		this.movementClass = movementClass;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		
        return detailsMap;
	}
	
}