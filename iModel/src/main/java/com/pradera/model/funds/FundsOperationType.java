package com.pradera.model.funds;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.funds.type.AccountCashFundsType;


/**
 * The persistent class for the FUNDS_OPERATION_TYPE database table.
 * 
 */
@Entity
@Cacheable(true)
@Table(name="FUNDS_OPERATION_TYPE")
public class FundsOperationType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_FUNDS_OPERATION_TYPE_PK")
	private Long idFundsOperationTypePk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RELATED_OPERATION_TYPE_FK")
	private FundsOperationType fundsOperationType;

	@Column(name="NAME")
	private String name;
	
	@Column(name="SHORT_NAME")
	private String shortName;

	@Column(name="OPERATION_CLASS")
	private Integer operationClass;

	@Column(name="OPERATION_GROUP")
	private Integer operationGroup;

	@Column(name="CASH_ACCOUNT_TYPE")
	private Integer cashAccountType;

	@Column(name="EXCECUTION_TYPE")
	private Long excecutionType;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Transient
	private String cashAccountTypeDesc;
	
    public FundsOperationType(Long idFundsOperationTypePk) {
    	this.idFundsOperationTypePk=idFundsOperationTypePk;
    }
    
    public FundsOperationType() {
    }


	public Long getIdFundsOperationTypePk() {
		return this.idFundsOperationTypePk;
	}

	public void setIdFundsOperationTypePk(Long idFundsOperationTypePk) {
		this.idFundsOperationTypePk = idFundsOperationTypePk;
	}

	public Long getExcecutionType() {
		return this.excecutionType;
	}

	public void setExcecutionType(Long excecutionType) {
		this.excecutionType = excecutionType;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FundsOperationType getFundsOperationType() {
		return this.fundsOperationType;
	}

	public void setFundsOperationType(FundsOperationType fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}

	public Integer getOperationClass() {
		return operationClass;
	}

	public void setOperationClass(Integer operationClass) {
		this.operationClass = operationClass;
	}

	public Integer getOperationGroup() {
		return operationGroup;
	}

	public void setOperationGroup(Integer operationGroup) {
		this.operationGroup = operationGroup;
	}


	public Integer getCashAccountType() {
		return cashAccountType;
	}

	public void setCashAccountType(Integer cashAccountType) {
		this.cashAccountType = cashAccountType;
	}

	public String getCashAccountTypeDesc() {
		if(Validations.validateIsNotNullAndPositive(cashAccountType)){
    		return AccountCashFundsType.get(cashAccountType).getValue();
    	} else {
    		return "" ;
    	}
		
	}

	public void setCashAccountTypeDesc(String cashAccountTypeDesc) {
		this.cashAccountTypeDesc = cashAccountTypeDesc;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	
}