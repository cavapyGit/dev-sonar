package com.pradera.model.funds;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

@Entity
@Table(name="CASH_ACCOUNT_DETAIL")
public class CashAccountDetail implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_CASH_ACCOUNT_DETAIL_PK")
	@SequenceGenerator(name="CASH_ACCOUNT_DETAIL_GENERATOR", sequenceName="SQ_ID_CASH_ACCOUNT_DETAIL" ,initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CASH_ACCOUNT_DETAIL_GENERATOR")
	private Long idCashAccountDetail;

	@Column(name="BANK_ACCOUNT_CLASS")
	private Integer bankAccountClass;

	@Column(name="IND_RECEPTION")
	private Integer indReception;

	@Column(name="IND_SENDING")
	private Integer indSending;

	@Column(name="ACCOUNT_STATE")
	private Integer accountState;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to CorporativeOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_CASH_ACCOUNT_FK")
	private InstitutionCashAccount institutionCashAccount;

	//bi-directional many-to-one association to CorporativeOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_BANK_ACCOUNT_FK")
	private InstitutionBankAccount institutionBankAccount;

    public CashAccountDetail() {
    	
    }

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
               new HashMap<String, List<? extends Auditable>>();  
		
       return detailsMap;
	}


	public Long getIdCashAccountDetail() {
		return idCashAccountDetail;
	}




	public void setIdCashAccountDetail(Long idCashAccountDetail) {
		this.idCashAccountDetail = idCashAccountDetail;
	}




	public Integer getBankAccountClass() {
		return bankAccountClass;
	}




	public void setBankAccountClass(Integer bankAccountClass) {
		this.bankAccountClass = bankAccountClass;
	}




	public Integer getIndReception() {
		return indReception;
	}




	public void setIndReception(Integer indReception) {
		this.indReception = indReception;
	}




	public Integer getIndSending() {
		return indSending;
	}




	public void setIndSending(Integer indSending) {
		this.indSending = indSending;
	}




	public Integer getAccountState() {
		return accountState;
	}




	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}




	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}




	public void setInstitutionCashAccount(
			InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}




	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}




	public void setInstitutionBankAccount(
			InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}

}