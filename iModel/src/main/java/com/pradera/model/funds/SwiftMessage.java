package com.pradera.model.funds;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.math.BigDecimal;
import java.sql.Blob;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the SWIFT_MESSAGE database table.
 * 
 */
@Entity
@Table(name="SWIFT_MESSAGE")
public class SwiftMessage implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SWIFT_MESSAGE_GENERATOR", sequenceName="SQ_ID_SWIFT_MESSAGE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SWIFT_MESSAGE_GENERATOR")
	@Column(name="ID_SWIFT_MESSAGE_PK")
	private Long idSwiftMessagePk;

	@Column(name="IND_INPUT_OUTPUT")
	private Integer indInputOutput;

	@Column(name="MESSAGE_NAME")
	private String messageName;

	@Lob()
	@Column(name="\"MESSAGE\"")
	private byte[] message;

	
	@Column(name="MESSAGE_TYPE")
	private Integer messageType;

	@Column(name="\"STATE\"")
	private Integer state;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to FundsOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_FK")
	private FundsOperation fundsOperation;

    
    
    public SwiftMessage() {
    }



	public Long getIdSwiftMessagePk() {
		return idSwiftMessagePk;
	}



	public void setIdSwiftMessagePk(Long idSwiftMessagePk) {
		this.idSwiftMessagePk = idSwiftMessagePk;
	}



	public Integer getIndInputOutput() {
		return indInputOutput;
	}



	public void setIndInputOutput(Integer indInputOutput) {
		this.indInputOutput = indInputOutput;
	}



	public Integer getMessageType() {
		return messageType;
	}



	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}



	public Integer getState() {
		return state;
	}



	public void setState(Integer state) {
		this.state = state;
	}



	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	public String getLastModifyIp() {
		return lastModifyIp;
	}



	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	public String getLastModifyUser() {
		return lastModifyUser;
	}



	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	public FundsOperation getFundsOperation() {
		return fundsOperation;
	}



	public void setFundsOperation(FundsOperation fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	public String getMessageName() {
		return messageName;
	}

	public void setMessageName(String messageName) {
		this.messageName = messageName;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
	public byte[] getMessage() {
		return message;
	}



	public void setMessage(byte[] message) {
		this.message = message;
	}
}