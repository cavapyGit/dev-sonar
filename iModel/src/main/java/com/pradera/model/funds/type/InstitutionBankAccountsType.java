package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum InstitutionBankAccountsType {
		
	ISSUER(Integer.valueOf(1895),"EMISOR"),
	PARTICIPANT(Integer.valueOf(1896),"DEPOSITANTE"),
	BANK(Integer.valueOf(1902),"BANCO"),
	STATE(Integer.valueOf(1898),"ESTADO");
	
	/** The Constant list. */
	public static final List<InstitutionBankAccountsType> list = new ArrayList<InstitutionBankAccountsType>();
	
	/** The Constant listExternalInstitutions. */
	public static final List<InstitutionBankAccountsType> listExternalInstitutions = new ArrayList<InstitutionBankAccountsType>();
	
	/** The Constant listInternalInstitutions. */
	public static final List<InstitutionBankAccountsType> listInternalInstitutions = new ArrayList<InstitutionBankAccountsType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, InstitutionBankAccountsType> lookup = new HashMap<Integer, InstitutionBankAccountsType>();
	static {
		for (InstitutionBankAccountsType s : EnumSet.allOf(InstitutionBankAccountsType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InstitutionBankAccountsType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static InstitutionBankAccountsType get(Integer code) {
		return lookup.get(code);
	}
}
