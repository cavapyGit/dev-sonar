package com.pradera.model.funds.type;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum FundsType.
 */
public enum FundsType {

	/*HARD CODE PARAMETERS*/
	PARAMETER_CENTRALIZING_ACCOUNT("1"),
	PARAMETER_SETTLER_ACCOUNT("3"),
	PARAMETER_BENEFIT_ACCOUNT("4"),
	PARAMETER_GUARANTEE_ACCOUNT("5"),
	PARAMETER_RATE_ACCOUNT("6"),
	PARAMETER_INT_OPERATION_ACCOUNT("7"),
	PARAMETER_RETURN_ACCOUNT("8"),
	PARAMETER_TAX_ACCOUNT("9"),

	IND_FUND_OPERATION_CLASS_RECEPTION(Integer.valueOf(1)),
	IND_FUND_OPERATION_CLASS_SEND(Integer.valueOf(2)),
	IND_FUND_OPERATION_CLASS_TRANSFER(Integer.valueOf(0)),

	IND_ACTIVATE_CASH_ACCOUNT("1"),
	IND_DISACTIVATE_CASH_ACCOUNT("2"),

	CASH_ACCOUNT_IND_BCRD_RELATION(Integer.valueOf(1)),
	CASH_ACCOUNT_IND_BCRD_NO_RELATION(Integer.valueOf(0)),

	CASH_ACCOUNT_SCHEMA_GROSS(Long.valueOf(1)),
	CASH_ACCOUNT_SCHEMA_NET(Long.valueOf(2)),

	CASH_ACCOUNT_SITUATION_OPEN(Long.valueOf(1)),
	CASH_ACCOUNT_SITUATION_CLOSE(Long.valueOf(0)),

	IND_GROSS_SETTLEMENT(BigDecimal.valueOf(1)),
	IND_NET_SETTLEMENT(BigDecimal.valueOf(2)),

	DEFAULT_AMOUNT(BigDecimal.valueOf(0)),
	SELECTED_ELEMENT(Long.valueOf(1)),

	IND_RECEPTION_PROCESS(Integer.valueOf(1)),
	IND_RETIREMENT_PROCESS(Integer.valueOf(2)),

	INDICATOR_FUND_PROCESS_TYPE_AUTOMATIC(Integer.valueOf(1)),
	INDICATOR_FUND_PROCESS_TYPE_MANUAL(Integer.valueOf(0)),

	IND_REGISTER_TYPE_MESSAGE(Integer.valueOf(1)),
	IND_REGISTER_TYPE_FILE(Integer.valueOf(2)),

	IND_MESSAGE_TYPE_INPUT(Integer.valueOf(1)),
	IND_MESSAGE_TYPE_OUTPUT(Integer.valueOf(2)),

	IND_FUNCTIONAL_VALIDATIONS_OBSERVED(1),
	IND_FUNCTIONAL_VALIDATIONS_AUTOMATIC_DEVOLUTION(2),
	IND_FUNCTIONAL_VALIDATIONS_SUCCESSFUL(3),

	IND_SETTLEMENT_SCHEMA_GROSS("1"),
	IND_SETTLEMENT_SCHEMA_NET("2"),

	IND_ROLE_BUY(Integer.valueOf(1)),
	IND_ROLE_SELL(Integer.valueOf(2)),

	IND_AUTOMATIC_RETURN(Integer.valueOf(1)),
	IND_OBSERVED_OPERATION(Integer.valueOf(2)),

	IND_AUTOMATIC_DEPOSIT_TYPE_SUCCESS(Integer.valueOf(1)),
	IND_AUTOMATIC_DEPOSIT_TYPE_OBSERVED_OPERATION(Integer.valueOf(2)),
	IND_AUTOMATIC_DEPOSIT_TYPE_AUTOMATIC_RETURN(Integer.valueOf(3)),

	IND_CASH_ACCOUNT_ORIGIN(Integer.valueOf(1)),
	IND_CASH_ACCOUNT_DESTINY(Integer.valueOf(2)),

	IND_HAS_INCHARGE(Integer.valueOf(1)),

	IND_MODALITY_NOT_MANAGE_TERM(Integer.valueOf(0)),
	IND_MODALITY_MANAGE_TERM(Integer.valueOf(1)),

	IND_CASH_PART(Integer.valueOf(1)),
	IND_TERM_PART(Integer.valueOf(2)),

	DEFAULT_COMBO_VALUE(Integer.valueOf(0)),
	DEFAULT_HASH_MAP_KEY(Integer.valueOf(0)),
	SHOW_SECOND_SCREEN(Integer.valueOf(2)),
	SHOW_SCREEN(Integer.valueOf(1)),
	NOT_SHOW_SCREEN(Integer.valueOf(0)),

	EQUAL_DATE(Integer.valueOf(0)),
	GREATER_DATE(Integer.valueOf(1)),
	LESS_DATE(Integer.valueOf(-1)),

	IND_DEPOSITARY_PARTICIPANT(Integer.valueOf(1)),

	COMBOBOX_INDICATOR_PARAMETER(Integer.valueOf(0)),
	COMBOBOX_INDICATOR_PARTICIPANT(Integer.valueOf(1)),
	COMBOBOX_INDICATOR_ISSUER(Integer.valueOf(2)),
	COMBOBOX_INDICATOR_BANKS(Integer.valueOf(3)),
	COMBOBOX_INDICATOR_MECHANISM(Integer.valueOf(4)),
	COMBOBOX_INDICATOR_MODALITY_GROUP(Integer.valueOf(5)),
	COMBOBOX_INDICATOR_INTERNATIONAL_PARTICIPANT(Integer.valueOf(6)),

	MAP_DESCRIPTION_CURRENCY(Integer.valueOf(1)),
	MAP_DESCRIPTION_MECHANISM(Integer.valueOf(2)),
	MAP_DESCRIPTION_MODALITY_GROUP(Integer.valueOf(3)),
	MAP_DESCRIPTION_PARTICIPANT(Integer.valueOf(4)),
	MAP_DESCRIPTION_STATE(Integer.valueOf(5)),
	MAP_DESCRIPTION_ACCOUNT_STATE(Integer.valueOf(6)),

	/*CONSTANTS SEND - RECEPTION FUNDS*/
	TRN_E000148("E000148"),
	TRN_E000149("E000149"),
	TRN_E000339("E000339"),
	TRN_E000340("E000340"),
	TRN_E000341("E000341"),
	TRN_E000341DEV("E000341.DEV"),
	TRN_E000342("E000342"),
	TRN_E000342DEV("E000342.DEV"),
	TRN_E000399("E000399"),
	TRN_E000400("E000400"),
	TRN_E000900("E000900"),
	TRN_E000901("E000901"),


	//FUNDS_OPERATION_TYPE
	FUNDS_OPERATION_TYPE_BENEFIT_RESERVATION_DEPOSIT(Long.valueOf(40020)),
	FUNDS_OPERATION_TYPE_BENEFIT_REPAYMENT_DEPOSIT(Long.valueOf(40021)),

	SWIFT_OPERATION_TYPE_RETIREMENT(Integer.valueOf(1)),
	SWIFT_OPERATION_TYPE_DEPOSIT(Integer.valueOf(2)),

	PARAMETER_OPERATION_CLASS_DEPOSIT(Integer.valueOf(1)),
	PARAMETER_OPERATION_CLASS_RETIREMENT(Integer.valueOf(2)),
	PARAMETER_OPERATION_CLASS_TRANSFERENCE(Integer.valueOf(3)),

	PARAMETER_PROCESS_TYPE_MANUAL(Integer.valueOf(1)),
	PARAMETER_PROCESS_TYPE_AUTOMATIC(Integer.valueOf(2)),

	PARAMETER_CEVALDOM_PARTICIPANT(Integer.valueOf(1)),

	/*SWIFT INDICATORS*/
	SWIFT_INDICATOR_GROSS_SETTLEMENT("1"),
	SWIFT_INDICATOR_NET_SETTLEMENT("2"),

	/*SETTLEMENT INDICATORS*/
	INDICATOR_CASH_PART(Integer.valueOf(1)),
	INDICATOR_TERM_PART(Integer.valueOf(2)),
	INDICATOR_ROLE_BUYER(Integer.valueOf(1)),
	INDICATOR_ROLE_SELLER(Integer.valueOf(2)),

	/*DESCRIPTIONS*/
	DESCRIPTION_PARAMETER_CEVALDOM("CEVALDOM"),
	DESCRIPTION_PARAMETER_BCRD("BCRD"),
	DESCRIPTION_PESOS_DOMINICANOS("PESOS DOMINICANOS"),
	DESCRIPTION_DOLARES_AMERICANOS("DOLARES AMERICANOS"),
	DESCRIPTION_NO("NO"),
	DESCRIPTION_SI("SI"),
	MESSAGE_VALIDATION_USE_TYPE_RECEPTION("No es posible registrar 2 cuentas bancarias asociadas a Recepción"),
	MESSAGE_VALIDATION_USE_TYPE_SENT("No es posible registrar 2 cuentas bancarias asociadas a Envío"),
	MESSAGE_VALIDATION_USE_TYPE_ACCOUNT("No es posible registrar más de 2 cuentas bancarias"),
	MESSAGE_GROSS_SETTLEMENT("LIQUIDACIÓN BRUTA"),
	MESSAGE_NET_SETTLEMENT("LIQUIDACIÓN NETA"),
	VALIDATION_MESSAGE_ACTIVE_ACCOUNT("activateAccount.show()"),
	VALIDATION_MESSAGE_DONT_ACTIVE_ACCOUNT("invalidActivateAccount.show()"),
	VALIDATION_MESSAGE_DISACTIVE_ACCOUNT("disAcc.show()"),
	VALIDATION_MESSAGE_DONT_DISACTIVE_ACCOUNT("invalidDisactivateAccount.show()"),
	DESCRIPTION_SHORT_CURRENCY_DOLLAR("USD"),
	DESCRIPTION_SHORT_CURRENCY_LOCAL("DOP"),
	EMPTY_STRING(""),
	REGISTER_STATE("REGISTRADA"),
	CASH_ACCOUNT_LIQUID("CUENTA DE LIQUIDACION"),
	CASH_ACCOUNT_BENEFIT("CUENTA DE COBRO DE DERECHOS"),
	CASH_ACCOUNT_GRANTIS("CUENTA DE GARANTIAS"),
	CASH_ACCOUNT_INTER("CUENTA DE OPERACIONES INTERNACIONALES"),
	DOC_TYPE_RNC_DESC("RNC - REGISTRO NACIONAL CONTRIBUYENTE"),
	DOC_TYPE_RNC("RNC"),
	DOC_TYPE_DIO("DIO");

	/** The Constant list. */
	private static final List<FundsType> list=new ArrayList<FundsType>();
	
	/** The Constant map. */
	private static final Map<Integer, FundsType> lookup=new HashMap<Integer, FundsType>();
	
	static{
		for(FundsType issu : FundsType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}

	private Long lngCode;
	private Integer code;
	private String strCode;
	private String value;
	private BigDecimal bdCode;


	private FundsType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	private FundsType(String code){
		this.strCode=code;
	}
	private FundsType(Long code){
		this.lngCode=code;
	}
	private FundsType(BigDecimal code){
		this.bdCode=code;
	}
	private FundsType(Integer code){
		this.code=code;
	}


	public static FundsType get(Integer code) {
		return lookup.get(code);
	}


	public Integer getCode(){
		return code;
	}
	public String getValue(){
		return value;
	}
	public String getStrCode() {
		return strCode;
	}
	public BigDecimal getBdCode() {
		return bdCode;
	}
	public Long getLngCode() {
		return lngCode;
	}
}