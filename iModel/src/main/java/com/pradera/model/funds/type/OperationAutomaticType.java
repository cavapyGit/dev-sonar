package com.pradera.model.funds.type;

public enum OperationAutomaticType {
	OPERATION_MANUAL(new Integer(0)),
	OPERATION_ATOMATIC(new Integer(1)),
	OPERATION_URL_ACCUSE_PAYMENT(new Integer(2)),
	;

	private OperationAutomaticType(Integer code){
		this.code = code;
	}
	
	private Integer code;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
}
