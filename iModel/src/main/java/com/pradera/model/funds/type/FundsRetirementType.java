package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum FundsRetirementType {
	BY_TOTAL(1455,"POR TOTAL"),	
	BY_OVERPAID(1454,"POR EXCEDENTE");
	
	/** The Constant list. */
	public static final List<FundsRetirementType> list = new ArrayList<FundsRetirementType>();

	public static final Map<Integer, FundsRetirementType> lookup = new HashMap<Integer, FundsRetirementType>();
	static {
		for (FundsRetirementType s : EnumSet.allOf(FundsRetirementType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	

	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}
	

	public String getValue() {
		return value;
	}
	

	public void setValue(String value) {
		this.value = value;
	}

	private FundsRetirementType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	

	public static FundsRetirementType get(Integer code) {
		return lookup.get(code);
	}
}
