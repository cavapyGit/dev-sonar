package com.pradera.model.funds.type;

public enum SearchOperationType {
	REFERENCE_PAYMENT(new Integer(1452)),
	OPERATION_INFO(new Integer(1453));
	
	private SearchOperationType(Integer code){
		this.code = code;
	}
	
	private Integer code;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
}
