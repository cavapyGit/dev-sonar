package com.pradera.model.funds.type;

public enum OperationClassType {
	RECEPTION_FUND(new Integer(1)),
	SEND_FUND(new Integer(2));
	
	private OperationClassType(Integer code){
		this.code = code;
	}
	
	private Integer code;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
}
