package com.pradera.model.funds;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantRequest;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.issuancesecuritie.Issuer;


/**
 * The persistent class for the INSTITUTION_BANK_ACCOUNT database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name = InstitutionBankAccountHistory.BANK_ACCOUNT_STATE, query = "SELECT new com.pradera.model.funds.InstitutionBankAccount(I.idInstitutionBankAccountPk,I.state) From InstitutionBankAccount I WHERE I.idInstitutionBankAccountPk = :idBankAccountPkParam"),
	@NamedQuery(name = InstitutionBankAccountHistory.INSTITUTION_BANK_ACCOUNT_HISTORY_FOR_ID_PARTICIPANT_CURRENCY_STATE, query = "SELECT new com.pradera.model.funds.InstitutionBankAccount(I.idInstitutionBankAccountPk,I.state) From InstitutionBankAccount I join I.participant p WHERE p.idParticipantPk = :idParticipantPk and I.currency = :currency and I.state = :state ")
})
@Entity
@Table(name="INSTITUTION_BANK_ACC_HISTORY")
public class InstitutionBankAccountHistory implements Serializable,Auditable {

	private static final long serialVersionUID = 1L;

	/** The Constant BANK_REQUEST_STATE. */
	public static final String  BANK_ACCOUNT_STATE= "InstitutionBankAccountHistory.searchBankAccountsState";
	/** Constante BANK ACCOUNT por id_codigo_BCB*/
	public static final String  INSTITUTION_BANK_ACCOUNT_HISTORY_FOR_ID_PARTICIPANT_CURRENCY_STATE= "InstitutionBankAccountHistory.searchBankAccountsIdBcbCode";
	
	@Id
	@SequenceGenerator(name="INST_BANK_ACCOUNT_HISTORY_GENERATOR", sequenceName="SQ_ID_INST_BANK_ACC_HISTORY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INST_BANK_ACCOUNT_HISTORY_GENERATOR")
	@Column(name="ID_INST_BANK_ACC_HISTORY_PK")
	private long idInstBankAccHistoryPk;
	
	@Column(name="ID_INSTITUTION_BANK_ACCOUNT_FK")
	private Long idInstitutionBankAccountFk;
	
	/** The participant request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_REQUEST_FK", referencedColumnName="ID_PARTICIPANT_REQUEST_PK")
	private ParticipantRequest participantRequest;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROVIDER_BANK_FK",referencedColumnName="ID_BANK_PK")
	private Bank providerBank;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BANK_FK",referencedColumnName="ID_BANK_PK")
	private Bank bank;

	@Column(name="BANK_ACOUNT_TYPE")
	private Long bankAcountType;

	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="ACCOUNT_NUMBER")
	private String accountNumber;

	@Column(name="STATE")
	private Integer state;
	
	/** The ind new. */
	@Column(name="IND_NEW")    
    private Integer indNew;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK",referencedColumnName="ID_ISSUER_PK")
	private Issuer issuer;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
		
	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BLOCK_DATE")
	private Date blockDate;

	/** The reject user. */
	@Column(name = "BLOCK_USER")
	private String blockUser;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UNBLOCK_DATE")
	private Date unblockDate;

	/** The reject user. */
	@Column(name = "UNBLOCK_USER")
	private String unblockUser;
	
	/** The reject motive. */
	@Column(name="BLOCK_MOTIVE")
	private Integer blockMotive;
	
	/** The annulment other motive. */
	@Column(name = "BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/** The reject motive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/** The annulment other motive. */
	@Column(name = "UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;
	
	@Column(name="ID_BCB_CODE")
	private Long IdBcbCode;
	
	//bi-directional many-to-one association to CorporativeProcessResult
	/** The corporative process results. */
	@OneToMany(mappedBy="institutionBankAccount")
	private List<CashAccountDetail> cashAccountDetailResults;
	
	@Transient
	private Integer institutionType;
	
	@Transient
	private Integer bankType;
	
	@Transient
	private String descriptionInstitution;

	@Transient
	private String descriptionBankClient;
	
	@Transient
	private String descriptionBankAcountType;
	
	@Transient
	private String descriptionCurrency;
	
	@Transient
	private String stateDescription;
	
	@Transient
	private String bicCodeBank;
	
    public InstitutionBankAccountHistory() {
    }

    
	public InstitutionBankAccountHistory(Long idInstitutionBankAccountFk, Integer state) {
		this.idInstitutionBankAccountFk = idInstitutionBankAccountFk;
		this.state = state;
	}
	
	public long getIdInstBankAccHistoryPk() {
		return idInstBankAccHistoryPk;
	}


	public void setIdInstBankAccHistoryPk(long idInstBankAccHistoryPk) {
		this.idInstBankAccHistoryPk = idInstBankAccHistoryPk;
	}


	public ParticipantRequest getParticipantRequest() {
		return participantRequest;
	}


	public void setParticipantRequest(ParticipantRequest participantRequest) {
		this.participantRequest = participantRequest;
	}


	public Integer getIndNew() {
		return indNew;
	}


	public void setIndNew(Integer indNew) {
		this.indNew = indNew;
	}


	public Long getIdInstitutionBankAccountFk() {
		return idInstitutionBankAccountFk;
	}
	public void setIdInstitutionBankAccountFk(Long idInstitutionBankAccountFk) {
		this.idInstitutionBankAccountFk = idInstitutionBankAccountFk;
	}
	public Bank getProviderBank() {
		return providerBank;
	}
	public void setProviderBank(Bank providerBank) {
		this.providerBank = providerBank;
	}

	public Bank getBank() {
		return bank;
	}


	public void setBank(Bank bank) {
		this.bank = bank;
	}


	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}	
	public Long getBankAcountType() {
		return bankAcountType;
	}
	public void setBankAcountType(Long bankAcountType) {
		this.bankAcountType = bankAcountType;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Integer getInstitutionType() {
		return institutionType;
	}

	public String getRegistryUser() {
		return registryUser;
	}


	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}


	public Date getRegistryDate() {
		return registryDate;
	}


	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}


	public Date getBlockDate() {
		return blockDate;
	}


	public void setBlockDate(Date blockDate) {
		this.blockDate = blockDate;
	}


	public String getBlockUser() {
		return blockUser;
	}


	public void setBlockUser(String blockUser) {
		this.blockUser = blockUser;
	}


	public Date getUnblockDate() {
		return unblockDate;
	}


	public void setUnblockDate(Date unblockDate) {
		this.unblockDate = unblockDate;
	}


	public String getUnblockUser() {
		return unblockUser;
	}


	public void setUnblockUser(String unblockUser) {
		this.unblockUser = unblockUser;
	}


	public Integer getBlockMotive() {
		return blockMotive;
	}


	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}


	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}


	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}


	public Integer getUnblockMotive() {
		return unblockMotive;
	}


	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}


	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}


	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}


	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}


	public Integer getBankType() {
		return bankType;
	}


	public void setBankType(Integer bankType) {
		this.bankType = bankType;
	}


	public String getDescriptionBankAcountType() {
		return descriptionBankAcountType;
	}


	public void setDescriptionBankAcountType(String descriptionBankAcountType) {
		this.descriptionBankAcountType = descriptionBankAcountType;
	}


	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}


	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}


	public String getStateDescription() {
		return stateDescription;
	}


	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}


	public String getDescriptionInstitution() {
		return descriptionInstitution;
	}


	public void setDescriptionInstitution(String descriptionInstitution) {
		this.descriptionInstitution = descriptionInstitution;
	}


	public String getDescriptionBankClient() {
		return descriptionBankClient;
	}


	public void setDescriptionBankClient(String descriptionBankClient) {
		this.descriptionBankClient = descriptionBankClient;
	}


	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		
        return detailsMap;
	}


	public List<CashAccountDetail> getCashAccountDetailResults() {
		return cashAccountDetailResults;
	}


	public void setCashAccountDetailResults(
			List<CashAccountDetail> cashAccountDetailResults) {
		this.cashAccountDetailResults = cashAccountDetailResults;
	}


	public String getBicCodeBank() {
		return bicCodeBank;
	}


	public void setBicCodeBank(String bicCodeBank) {
		this.bicCodeBank = bicCodeBank;
	}


	public Long getIdBcbCode() {
		return IdBcbCode;
	}


	public void setIdBcbCode(Long idBcbCode) {
		IdBcbCode = idBcbCode;
	}
	
}