package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum BeginEndDaySituationType {
	
	CLOSE(Integer.valueOf(0),"CERRADO"),
	OPEN(Integer.valueOf(1),"ABIERTO");

	/** The Constant list. */
	public static final List<BeginEndDaySituationType> list = new ArrayList<BeginEndDaySituationType>();
	
	/** The Constant listExternalInstitutions. */
	public static final List<BeginEndDaySituationType> listExternalInstitutions = new ArrayList<BeginEndDaySituationType>();
	
	/** The Constant listInternalInstitutions. */
	public static final List<BeginEndDaySituationType> listInternalInstitutions = new ArrayList<BeginEndDaySituationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BeginEndDaySituationType> lookup = new HashMap<Integer, BeginEndDaySituationType>();
	static {
		for (BeginEndDaySituationType s : EnumSet.allOf(BeginEndDaySituationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BeginEndDaySituationType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static BeginEndDaySituationType get(Integer code) {
		return lookup.get(code);
	}
}
