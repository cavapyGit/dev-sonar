package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BankOriginType {

	/** The origin national. */
	NATIONAL(Integer.valueOf(699),"NACIONAL"),
	
	/** The origin foreign. */
	FOREIGN(Integer.valueOf(700),"EXTRANJERO");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;		
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant document type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BankOriginType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<BankOriginType> list = new ArrayList<BankOriginType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BankOriginType> lookup = new HashMap<Integer, BankOriginType>();
	static {
		for (BankOriginType s : EnumSet.allOf(BankOriginType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant document type
	 */
	public static BankOriginType get(Integer code) {
		return lookup.get(code);
	}
}
