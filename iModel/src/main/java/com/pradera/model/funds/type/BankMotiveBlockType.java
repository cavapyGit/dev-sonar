package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BankMotiveBlockType {
	
	OTHER(Integer.valueOf(1893),"OTROS");
	
	/** The Constant lookup. */
	public static final Map<Integer,BankMotiveBlockType> lookup=new HashMap<Integer, BankMotiveBlockType>();
	
	/** The Constant list. */
	public static final List<BankMotiveBlockType> list=new ArrayList<BankMotiveBlockType>();

	static{
		for(BankMotiveBlockType s:BankMotiveBlockType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BankMotiveBlockType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static BankMotiveBlockType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
}
