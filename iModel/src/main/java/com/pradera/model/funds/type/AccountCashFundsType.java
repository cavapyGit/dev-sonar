package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum AccountCashFundsType {
	
	CENTRALIZING(Integer.valueOf(648),"CUENTA CENTRALIZADORA"),
	SETTLEMENT(Integer.valueOf(649),"CUENTA DE LIQUIDACION"),
	BENEFIT(Integer.valueOf(650),"CUENTA DE PAGO DE DERECHOS"),
	GUARANTEES(Integer.valueOf(651),"CUENTA DEL FONDO DE GARANTIAS"),
	RATES(Integer.valueOf(652),"CUENTA DE TARIFAS"),
	INTERNATIONAL_OPERATIONS(Integer.valueOf(653),"CUENTA DE OPERACIONES INTERNACIONALES"),
	RETURN(Integer.valueOf(654),"CUENTA DE DEVOLUCIONES"),
	TAX(Integer.valueOf(655),"CUENTA DE IMPUESTOS"),
	SPECIAL_PAYMENT(Integer.valueOf(1702),"CUENTA DE PAGOS ESPECIALES"),
	CENTRALIZING_SETTLEMENT(Integer.valueOf(2900),"CUENTA CENTRALIZADORA LIQUIDACION"),
	CENTRALIZING_BENEFIT(Integer.valueOf(2901),"CUENTA CENTRALIZADORA PAGO DE DERECHOS"),
	CENTRALIZING_GUARANTEES(Integer.valueOf(2902),"CUENTA CENTRALIZADORA FONDO DE GARANTIA"),
	CENTRALIZING_RATES(Integer.valueOf(2903),"CUENTA CENTRALIZADORA TARIFAS"),
	CENTRALIZING_INTERNATIONAL_OPERATIONS(Integer.valueOf(2904),"CUENTA CENTRALIZADORA OPERACIONES INTERNACIONALES"),
	CENTRALIZING_RETURN(Integer.valueOf(2905),"CUENTA CENTRALIZADORA DEVOLUCIONES"),
	CENTRALIZING_TAX(Integer.valueOf(2915),"CUENTA CENTRALIZADORA IMPUESTOS");
	
	/** The Constant list. */
	public static final List<AccountCashFundsType> list = new ArrayList<AccountCashFundsType>();
	
	/** The Constant listExternalInstitutions. */
	public static final List<AccountCashFundsType> listExternalInstitutions = new ArrayList<AccountCashFundsType>();
	
	/** The Constant listInternalInstitutions. */
	public static final List<AccountCashFundsType> listInternalInstitutions = new ArrayList<AccountCashFundsType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountCashFundsType> lookup = new HashMap<Integer, AccountCashFundsType>();
	static {
		for (AccountCashFundsType s : EnumSet.allOf(AccountCashFundsType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountCashFundsType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static AccountCashFundsType get(Integer code) {
		return lookup.get(code);
	}
}
