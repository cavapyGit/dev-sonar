package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum FundsOperationGroupType {
	GROUP_OPERATION_CENTRALACCOUNT(Integer.valueOf(1224),"GRUPO DE OEPRACION CUENTA CENTRALIZADORA"),
	GUARANTEE_FUND_OPERATION(Integer.valueOf(703),"OPERACION DE FONDOS DE GARANTIAS"),
	RATE_FUND_OPERATION(Integer.valueOf(704),"OPERACION DE FONDOS DE TARIFAS"),
	INTERNATIONAL_FUND_OPERATION(Integer.valueOf(705),"OPERACION DE FONDOS DE OPERACIONES INTERNACIONALES"),
	RETURN_FUND_OPERATION(Integer.valueOf(706),"OPERACION DE FONDOS DE DEVOLUCIONES"),
	SETTELEMENT_FUND_OPERATION(Integer.valueOf(708),"OPERACION DE FONDOS DE LIQUIDACION"),
	BENEFIT_FUND_OPERATION(Integer.valueOf(709),"OPERACION DE FONDOS DE COBRO DE DERECHOS"),
	TAX_FUND_OPERATION(Integer.valueOf(710),"OPERACION DE FONDOS DE IMPUESTOS"),
	SPECIAL_OPERATION(Integer.valueOf(1703),"OPERACION DE FONDOS ESPECIALES");

	private Integer code;
	private String value;

	public static final List<FundsOperationGroupType> list = new ArrayList<FundsOperationGroupType>();
	public static final Map<Integer, FundsOperationGroupType> lookup = new HashMap<Integer, FundsOperationGroupType>();

	/**
	 * List some elements.
	 *
	 * @param fundsOperationGrouptype 
	 * 
	 * @return the list
	 */
	public static List<FundsOperationGroupType> listSomeElements(FundsOperationGroupType... fundsOperationGrouptype){
		List<FundsOperationGroupType> fundsGroupType = new ArrayList<FundsOperationGroupType>();
		for(FundsOperationGroupType fundsOperationGroupType: fundsOperationGrouptype){
			fundsGroupType.add(fundsOperationGroupType);
		}
		return fundsGroupType;
	}
	
	static {
		for (FundsOperationGroupType s : EnumSet.allOf(FundsOperationGroupType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	private FundsOperationGroupType(Integer code, String value) {
		this.code = code;
		this.value = value;	
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getValue() {
		return value;
	}	
	
	

	public String getDescription(Locale locale) {
		return null;
	}
	
	public static FundsOperationGroupType get(Integer code) {
		return lookup.get(code);
	}
}
