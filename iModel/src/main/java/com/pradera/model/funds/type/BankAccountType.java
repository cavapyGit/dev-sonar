package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BankAccountType {

	SAVINGS(Integer.valueOf(453),"CUENTA DE AHORROS"),
	CURRENT(Integer.valueOf(454),"CUENTA CORRIENTE");
	
	/** The Constant list. */
	public static final List<BankAccountType> list = new ArrayList<BankAccountType>();
	
	/** The Constant listExternalInstitutions. */
	public static final List<BankAccountType> listExternalInstitutions = new ArrayList<BankAccountType>();
	
	/** The Constant listInternalInstitutions. */
	public static final List<BankAccountType> listInternalInstitutions = new ArrayList<BankAccountType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BankAccountType> lookup = new HashMap<Integer, BankAccountType>();
	static {
		for (BankAccountType s : EnumSet.allOf(BankAccountType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BankAccountType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static BankAccountType get(Integer code) {
		return lookup.get(code);
	}
}
