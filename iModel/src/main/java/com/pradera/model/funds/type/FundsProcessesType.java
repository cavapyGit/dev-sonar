package com.pradera.model.funds.type;

public enum FundsProcessesType {
	MANUAL(new Integer(1794)),
	AUTOMATIC(new Integer(1795));
	
	private FundsProcessesType(Integer code){
		this.code = code;
	}
	
	private Integer code;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
}
