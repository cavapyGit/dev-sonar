package com.pradera.model.funds;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="LBTR_OPERATION")
public class LbtrOperation implements Serializable,Auditable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@SequenceGenerator(name="SQ_ID_LBTR_OPERATION_PK", sequenceName="SQ_ID_LBTR_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_LBTR_OPERATION_PK")
	@Column(name="ID_LBTR_OPERATION_PK")
	private Long idLbtrOperationPk;
	
	@Column(name="TRN")
	private String trn;
	
	@Column(name="AMOUNT")
	private BigDecimal amount;
	
	@Column(name="CURRENCY")
	private String currency;
	
	@Column(name="OMT")
	private String omt; 
	
	@Column(name="PRIORITY_CODE")
	private String priorityCode; 
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate; 
	
	@Column(name="DEBIT_PARTY")
	private String debitParty; 
	
	@Column(name="CREDIT_PARTY")
	private String creditParty;
	
	@Column(name="OPERATION_STATE")
	private String operationState;
	
	@Column(name="SUBMITTER")
	private String submitter;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RECEIPT_TIME")
	private Date receitTime;
	
	@Column(name="TRN_MODIFIED")
	private String trnModified;
	
	@Column(name="IND_EXPIRATION")
	private Integer indExpiration;
		
	@Temporal(TemporalType.DATE)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	
	@Transient
	private String lbtrFileName;
	
	@Transient
	private Boolean oneDayBefore;
	
	@Transient
	private Boolean expire;
	
	public Boolean getOneDayBefore() {
		return oneDayBefore;
	}

	public void setOneDayBefore(Boolean oneDayBefore) {
		this.oneDayBefore = oneDayBefore;
	}

	public String getLbtrFileName() {
		return lbtrFileName;
	}

	public void setLbtrFileName(String lbtrFileName) {
		this.lbtrFileName = lbtrFileName;
	}

	public Long getIdLbtrOperationPk() {
		return idLbtrOperationPk;
	}

	public void setIdLbtrOperationPk(Long idLbtrOperationPk) {
		this.idLbtrOperationPk = idLbtrOperationPk;
	}

	public String getTrn() {
		return trn;
	}

	public void setTrn(String trn) {
		this.trn = trn;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOmt() {
		return omt;
	}

	public void setOmt(String omt) {
		this.omt = omt;
	}

	public String getPriorityCode() {
		return priorityCode;
	}

	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getDebitParty() {
		return debitParty;
	}

	public void setDebitParty(String debitParty) {
		this.debitParty = debitParty;
	}
	public String getOperationState() {
		return operationState;
	}

	public void setOperationState(String operationState) {
		this.operationState = operationState;
	}

	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public Date getReceitTime() {
		return receitTime;
	}

	public void setReceitTime(Date receitTime) {
		this.receitTime = receitTime;
	}

	public String getTrnModified() {
		return trnModified;
	}

	public void setTrnModified(String trnModified) {
		this.trnModified = trnModified;
	}

	public Integer getIndExpiration() {
		return indExpiration;
	}

	public Boolean getExpire() {
		return expire;
	}

	public void setExpire(Boolean expire) {
		if(expire)
			this.setIndExpiration(BooleanType.YES.getCode());
		else
			this.setIndExpiration(BooleanType.NO.getCode());
		this.expire = expire;
	}

	public void setIndExpiration(Integer indExpiration) {
		this.indExpiration = indExpiration;
	}

	public String getCreditParty() {
		return creditParty;
	}

	public void setCreditParty(String creditParty) {
		this.creditParty = creditParty;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
