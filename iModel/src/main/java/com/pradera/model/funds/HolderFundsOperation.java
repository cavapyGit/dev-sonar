package com.pradera.model.funds;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.corporatives.HolderCashMovement;

/**
 * Entity implementation class for Entity: HolderFundsOperation
 *
 */
@Entity
@Table(name="HOLDER_FUNDS_OPERATION")
public class HolderFundsOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="SQ_HOLDER_FUNDS_OPERATION", sequenceName="SQ_ID_HOLDER_FUNDS_OPER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_HOLDER_FUNDS_OPERATION")
	@Column(name="ID_HOLDER_FUNDS_OPERATION_PK")
	private Long idHolderFundsOperationPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_FK")	
	private FundsOperation fundsOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_CASH_MOVEMENT_FK")	
	private HolderCashMovement relatedHolderCashMovement;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_TYPE_FK")
	private FundsOperationType fundsOperationType;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_BANK_FK")
	private HolderAccountBank holderAccountBank;	
		
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@Column(name="FULL_NAME_HOLDER")
	private String fullNameHolder;
	
	@Column(name="HOLDER_DOCUMENT_NUMBER")
	private String holderDocumentNumber;
	
	@Column(name="HOLDER_OPERATION_AMOUNT")
	private BigDecimal holderOperationAmount;
	
	@Column(name="HOLDER_OPERATION_STATE")
	private Integer holderOperationState;
	
	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="BANK_ACCOUNT_TYPE")
	private Integer bankAccountType;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@OneToMany(mappedBy="holderFundsOperation",cascade=CascadeType.ALL)
	private List<HolderFundsDetail> holderFundsDetails;
	
	@Transient
	private boolean selected;
	@Transient
	private String currencyDescription;
	@Transient
	private String bankDescription;
	
	
	public Long getIdHolderFundsOperationPk() {
		return idHolderFundsOperationPk;
	}

	public void setIdHolderFundsOperationPk(Long idHolderFundsOperationPk) {
		this.idHolderFundsOperationPk = idHolderFundsOperationPk;
	}

	public FundsOperation getFundsOperation() {
		return fundsOperation;
	}

	public void setFundsOperation(FundsOperation fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public String getFullNameHolder() {
		return fullNameHolder;
	}

	public void setFullNameHolder(String fullNameHolder) {
		this.fullNameHolder = fullNameHolder;
	}

	public String getHolderDocumentNumber() {
		return holderDocumentNumber;
	}

	public void setHolderDocumentNumber(String holderDocumentNumber) {
		this.holderDocumentNumber = holderDocumentNumber;
	}

	public BigDecimal getHolderOperationAmount() {
		return holderOperationAmount;
	}

	public void setHolderOperationAmount(BigDecimal holderOperationAmount) {
		this.holderOperationAmount = holderOperationAmount;
	}

	public Integer getHolderOperationState() {
		return holderOperationState;
	}

	public void setHolderOperationState(Integer holderOperationState) {
		this.holderOperationState = holderOperationState;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Integer getBankAccountType() {
		return bankAccountType;
	}

	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO review 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("holderFundsDetails", (List<? extends Auditable>) holderFundsDetails);
        return detailsMap;
	}

	public List<HolderFundsDetail> getHolderFundsDetails() {
		return holderFundsDetails;
	}

	public void setHolderFundsDetails(List<HolderFundsDetail> holderFundsDetails) {
		this.holderFundsDetails = holderFundsDetails;
	}

	public HolderCashMovement getRelatedHolderCashMovement() {
		return relatedHolderCashMovement;
	}

	public void setRelatedHolderCashMovement(HolderCashMovement relatedHolderCashMovement) {
		this.relatedHolderCashMovement = relatedHolderCashMovement;
	}

	public FundsOperationType getFundsOperationType() {
		return fundsOperationType;
	}

	public void setFundsOperationType(FundsOperationType fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}

	public HolderAccountBank getHolderAccountBank() {
		return holderAccountBank;
	}

	public void setHolderAccountBank(HolderAccountBank holderAccountBank) {
		this.holderAccountBank = holderAccountBank;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getCurrencyDescription() {
		return currencyDescription;
	}

	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	public String getBankDescription() {
		return bankDescription;
	}

	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
}
