package com.pradera.model.funds.type;

public enum FundOpeGroupEntityRelationType {
	EDV(new Integer(1444)),
	PARTICIPANT(new Integer(1463)),
	ISSUER(new Integer(1462));
	
	private FundOpeGroupEntityRelationType(Integer code){
		this.code = code;
	}
	
	private Integer code;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	
}
