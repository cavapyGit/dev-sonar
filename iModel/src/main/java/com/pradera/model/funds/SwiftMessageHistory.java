package com.pradera.model.funds;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.math.BigDecimal;
import java.sql.Blob;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the SWIFT_MESSAGE_HISTORY database table.
 * 
 */
@Entity
@Table(name="SWIFT_MESSAGE_HISTORY")
public class SwiftMessageHistory implements Auditable, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SWIFT_MESSAGE_HIST_GENERATOR", sequenceName="SQ_ID_SWIFT_MESSAGE_HIST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SWIFT_MESSAGE_HIST_GENERATOR")
	@Column(name="ID_SWIFT_MESSAGE_HIS_PK")
	private Long idSwiftMessageHisPk;

    @Lob()
	@Column(name="FILE_MESSAGE")
	private Blob fileMessage;

	@Column(name="FILE_NAME")
	private String fileName;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="HISTORY_DATE")
	private Date historyDate;

	@Column(name="IND_INPUT_OUTPUT")
	private Integer indInputOutput;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="SOURCE_URL")
	private String sourceUrl;

	@Column(name="TARGET_URL")
	private String targetUrl;


    public SwiftMessageHistory() {
    }

	public Long getIdSwiftMessageHisPk() {
		return this.idSwiftMessageHisPk;
	}

	public void setIdSwiftMessageHisPk(Long idSwiftMessageHisPk) {
		this.idSwiftMessageHisPk = idSwiftMessageHisPk;
	}
	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getHistoryDate() {
		return this.historyDate;
	}

	public void setHistoryDate(Date historyDate) {
		this.historyDate = historyDate;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getSourceUrl() {
		return this.sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getTargetUrl() {
		return this.targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

	public Blob getFileMessage() {
		return fileMessage;
	}

	public void setFileMessage(Blob fileMessage) {
		this.fileMessage = fileMessage;
	}

	public Integer getIndInputOutput() {
		return indInputOutput;
	}

	public void setIndInputOutput(Integer indInputOutput) {
		this.indInputOutput = indInputOutput;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}