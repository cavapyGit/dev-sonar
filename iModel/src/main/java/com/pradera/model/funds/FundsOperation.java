package com.pradera.model.funds;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.corporatives.BenefitPaymentAllocation;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.settlement.SettlementOperation;
import com.pradera.model.settlement.SettlementProcess;


/**
 * The persistent class for the FUNDS_OPERATION database table.
 * 
 */
@Entity
@Table(name="FUNDS_OPERATION")
public class FundsOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FUNDS_OPERATION_GENERATOR", sequenceName="SQ_ID_FUNDS_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FUNDS_OPERATION_GENERATOR")
	@Column(name="ID_FUNDS_OPERATION_PK")
	private Long idFundsOperationPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RELATED_FUNDS_OPERATION_FK")
	private FundsOperation fundsOperation;

	@Column(name="BANK_REFERENCE")
	private String bankReference;

	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="FUNDS_OPERATION_CLASS")
	private Integer fundsOperationClass;

	@Column(name="FUNDS_OPERATION_GROUP")
	private Integer fundsOperationGroup;

	@Column(name="FUNDS_OPERATION_TYPE")
	private Long fundsOperationType;

	@Column(name="PAYMENT_REFERENCE")
	private String paymentReference;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BANK_FK")
	private Bank bank;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BENEFIT_PAYMENT_FK")
	private BenefitPaymentAllocation benefitPaymentAllocation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_GUARANTEE_OPERATION_FK")
	private GuaranteeOperation guaranteeOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
	private MechanismOperation mechanismOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_OPERATION_FK")
	private SettlementOperation settlementOperation;
	
	@OneToMany(mappedBy="fundsOperation")
	private List<FundsInternationalOperation> lstFundsInternationalOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_PROCESS_FK")
	private SettlementProcess settlementProcess;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_CASH_ACCOUNT_FK")
	private InstitutionCashAccount institutionCashAccount; 
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="COMMENTS")
	private Integer observation;

	@Column(name="OPERATION_AMOUNT")
	private BigDecimal operationAmount;

	@Column(name="IND_AUTOMATIC")
	private Integer indAutomatic;

	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="OPERATION_STATE")
	private Integer operationState;
	
	@Column(name="IND_CENTRALIZED")
	private Integer indCentralized;
	
	@Column(name="OPERATION_PART")
	private Integer operationPart = Integer.valueOf(0);
	
	@Column(name="BANK_ACCOUNT_NUMBER")
	private String bankAccountNumber;

	@Column(name="FUNDS_OPERATION_NUMBER")
	private Integer fundOperationNumber;

	@Column(name="SENDER_BIC_CODE")
	private String senderBicCode;
	
	@Column(name="IND_BCRD_PAYBACK")
	private Integer indBcrdPayback;
	
	@Column(name="IND_TRANSFER_CENTRALIZING")
	private Integer indTransferCentralizing;
	
	@Column(name="RETIREMENT_TYPE")
	private Integer retirementType;
	
	
	@Column(name="IND_BANK_RETIREMENT")
	private Integer indBankRetirement;
	
	@Column(name="IND_BANK_RETIREMENT_CONFIRM")
	private Integer indBankRetirementConfirm;

	@Column(name="BANK_RETIREMENT_TYPE")
	private Integer bankRetirementType;
	
	@Column(name="BANK_RETIREMENT_DATE")
	private Date bankRetirementDate;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;
	
	
	//bi-directional many-to-one association to CashAccountMovement
	@OneToMany(mappedBy="fundsOperation")
	private List<CashAccountMovement> cashAccountMovements;

	//bi-directional many-to-one association to SwiftMessage
	@OneToMany(mappedBy="fundsOperation")
	private List<SwiftMessage> swiftMessages;
	
	@OneToMany(mappedBy="fundsOperation",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<HolderFundsOperation> holderFundsOperations;
	
	@OneToMany(mappedBy="fundsOperation",cascade=CascadeType.ALL,fetch=FetchType.LAZY)	
	private List<HolderCashMovement> holderCashMovement;
	
	// referencia para la busqueda para la recepcion de fondos
	// Operacion E73 y E74
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_BANK_ACCOUNT_FK")
	private InstitutionBankAccount institutionBankAccount;
	
	@Column(name="BANK_MOVEMENT")
	private String bankMovement;
	
	@Transient
	private String fundsOpeGroupDescription;
	@Transient
	private String fundsOpeTypeDescription;
	@Transient
	private String currencyDescription;
	@Transient
	private String operationStateDescription;
	@Transient
	private Date initialDate;
	@Transient
	private Date endDate;
	@Transient
	private Integer indAutomaticDevolution;
	@Transient
	private HolderFundsOperation holderFundsOperation;
	@Transient
	private String withdrawlSwiftBic;
	@Transient
	private String holderDescription;
	@Transient
	private String corporativeDescription;
	@Transient
	private boolean indSendLip;
	
	@Transient
	private boolean blTransferCentralizing;
	
	public FundsOperation(Long idFundsOperationPk) {
		super();
		this.idFundsOperationPk = idFundsOperationPk;
	}
	public FundsOperation() {
		indBcrdPayback = new Integer(0);
    }

	public String getBankReference() {
		return this.bankReference;
	}

	public void setBankReference(String bankReference) {
		this.bankReference = bankReference;
	}


	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public BenefitPaymentAllocation getBenefitPaymentAllocation() {
		return benefitPaymentAllocation;
	}

	public void setBenefitPaymentAllocation(BenefitPaymentAllocation benefitPaymentAllocation) {
		this.benefitPaymentAllocation = benefitPaymentAllocation;
	}

	public GuaranteeOperation getGuaranteeOperation() {
		return guaranteeOperation;
	}

	public void setGuaranteeOperation(GuaranteeOperation guaranteeOperation) {
		this.guaranteeOperation = guaranteeOperation;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public List<HolderCashMovement> getHolderCashMovement() {
		return holderCashMovement;
	}

	public void setHolderCashMovement(List<HolderCashMovement> holderCashMovement) {
		this.holderCashMovement = holderCashMovement;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getObservation() {
		return observation;
	}

	public void setObservation(Integer observation) {
		this.observation = observation;
	}

	public BigDecimal getOperationAmount() {
		return operationAmount;
	}

	public void setOperationAmount(BigDecimal operationAmount) {
		this.operationAmount = operationAmount;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public SettlementProcess getSettlementProcess() {
		return settlementProcess;
	}

	public void setSettlementProcess(SettlementProcess settlementProcess) {
		this.settlementProcess = settlementProcess;
	}

	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}

	public void setInstitutionCashAccount(
			InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}

	public Long getIdFundsOperationPk() {
		return idFundsOperationPk;
	}

	public void setIdFundsOperationPk(Long idFundsOperationPk) {
		this.idFundsOperationPk = idFundsOperationPk;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Integer getFundsOperationClass() {
		return fundsOperationClass;
	}

	public void setFundsOperationClass(Integer fundsOperationClass) {
		this.fundsOperationClass = fundsOperationClass;
	}

	public Integer getFundsOperationGroup() {
		return fundsOperationGroup;
	}

	public void setFundsOperationGroup(Integer fundsOperationGroup) {
		this.fundsOperationGroup = fundsOperationGroup;
	}

	
	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public List<CashAccountMovement> getCashAccountMovements() {
		return cashAccountMovements;
	}

	public void setCashAccountMovements(
			List<CashAccountMovement> cashAccountMovements) {
		this.cashAccountMovements = cashAccountMovements;
	}

	public List<SwiftMessage> getSwiftMessages() {
		return swiftMessages;
	}

	public void setSwiftMessages(List<SwiftMessage> swiftMessages) {
		this.swiftMessages = swiftMessages;
	}

	public Long getFundsOperationType() {
		return fundsOperationType;
	}

	public void setFundsOperationType(Long fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}
	public FundsOperation getFundsOperation() {
		return fundsOperation;
	}
	public void setFundsOperation(FundsOperation fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	public Integer getIndAutomatic() {
		return indAutomatic;
	}

	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	}
	
	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}
	public void setInstitutionBankAccount(
			InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("cashAccountMovements", (List<? extends Auditable>) cashAccountMovements);
		detailsMap.put("swiftMessages", (List<? extends Auditable>) swiftMessages);
		detailsMap.put("holderFundsOperations", (List<? extends Auditable>) holderFundsOperations);
		detailsMap.put("holderCashMovement", (List<? extends Auditable>) holderCashMovement);
        return detailsMap;
	}

	public String getFundsOpeGroupDescription() {
		return fundsOpeGroupDescription;
	}

	public void setFundsOpeGroupDescription(String fundsOpeGroupDescription) {
		this.fundsOpeGroupDescription = fundsOpeGroupDescription;
	}

	public String getFundsOpeTypeDescription() {
		if(fundsOperationType != null){
			return ParameterFundsOperationType.get(fundsOperationType).getDescription();
		}else{
			return "";
		}
	}
	
	public String getFundsProcessDescription(){
		if(Validations.validateIsNotNullAndNotEmpty(indAutomatic)){//TODO
			return BooleanType.YES.getBinaryValue().equals(indAutomatic)?"AUTOMATICO":"MANUAL";
		}else
			return "";		
	}

	public void setFundsOpeTypeDescription(String fundsOpeTypeDescription) {
		this.fundsOpeTypeDescription = fundsOpeTypeDescription;
	}

	public String getCurrencyDescription() {
		return currencyDescription;
	}

	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	public String getOperationStateDescription() {
		return operationStateDescription;
	}

	public void setOperationStateDescription(String operationStateDescription) {
		this.operationStateDescription = operationStateDescription;
	}

	public List<FundsInternationalOperation> getLstFundsInternationalOperation() {
		return lstFundsInternationalOperation;
	}

	public void setLstFundsInternationalOperation(
			List<FundsInternationalOperation> lstFundsInternationalOperation) {
		this.lstFundsInternationalOperation = lstFundsInternationalOperation;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getOperationPart() {
		return operationPart;
	}

	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	public Integer getIndCentralized() {
		return indCentralized;
	}

	public void setIndCentralized(Integer indCentralized) {
		this.indCentralized = indCentralized;
	}

	public HolderFundsOperation getHolderFundsOperation() {
		return holderFundsOperation;
	}

	public void setHolderFundsOperation(HolderFundsOperation holderFundsOperation) {
		this.holderFundsOperation = holderFundsOperation;
	}
	public Integer getIndAutomaticDevolution() {
		return indAutomaticDevolution;
	}
	public void setIndAutomaticDevolution(Integer indAutomaticDevolution) {
		this.indAutomaticDevolution = indAutomaticDevolution;
	}
	public String getWithdrawlSwiftBic() {
		return withdrawlSwiftBic;
	}
	public void setWithdrawlSwiftBic(String withdrawlSwiftBic) {
		this.withdrawlSwiftBic = withdrawlSwiftBic;
	}
	public String getHolderDescription() {
		return holderDescription;
	}
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}
	public String getCorporativeDescription() {
		return corporativeDescription;
	}
	public List<HolderFundsOperation> getHolderFundsOperations() {
		return holderFundsOperations;
	}

	public void setHolderFundsOperations(
			List<HolderFundsOperation> holderFundsOperations) {
		this.holderFundsOperations = holderFundsOperations;
	}

	public void setCorporativeDescription(String corporativeDescription) {
		this.corporativeDescription = corporativeDescription;
	}
	public String getPaymentReference() {
		return paymentReference;
	}
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}
	public Integer getFundOperationNumber() {
		return fundOperationNumber;
	}
	public void setFundOperationNumber(Integer fundOperationNumber) {
		this.fundOperationNumber = fundOperationNumber;
	}
	public String getSenderBicCode() {
		return senderBicCode;
	}
	public void setSenderBicCode(String senderBicCode) {
		this.senderBicCode = senderBicCode;
	}

	public Integer getRetirementType() {
		return retirementType;
	}

	/**
	 * @return the indBcrdPayback
	 */
	public Integer getIndBcrdPayback() {
		return indBcrdPayback;
	}

	/**
	 * @param indBcrdPayback the indBcrdPayback to set
	 */
	public void setIndBcrdPayback(Integer indBcrdPayback) {
		this.indBcrdPayback = indBcrdPayback;
	}

	public void setRetirementType(Integer retirementType) {
		this.retirementType = retirementType;
	}
	
	public SettlementOperation getSettlementOperation() {
		return settlementOperation;
	}

	public void setSettlementOperation(SettlementOperation settlementOperation) {
		this.settlementOperation = settlementOperation;
	}
	/**
	 * @return the indSendLip
	 */
	public boolean isIndSendLip() {
		return indSendLip;
	}
	/**
	 * @param indSendLip the indSendLip to set
	 */
	public void setIndSendLip(boolean indSendLip) {
		this.indSendLip = indSendLip;
	}
	public Integer getIndTransferCentralizing() {
		return indTransferCentralizing;
	}
	public void setIndTransferCentralizing(Integer indTransferCentralizing) {
		this.indTransferCentralizing = indTransferCentralizing;
	}
	public boolean isBlTransferCentralizing() {
		return blTransferCentralizing;
	}
	public void setBlTransferCentralizing(boolean blTransferCentralizing) {
		this.blTransferCentralizing = blTransferCentralizing;
	}
	public String getBankMovement() {
		return bankMovement;
	}
	public void setBankMovement(String bankMovement) {
		this.bankMovement = bankMovement;
	}
	public Integer getIndBankRetirement() {
		return indBankRetirement;
	}
	public void setIndBankRetirement(Integer indBankRetirement) {
		this.indBankRetirement = indBankRetirement;
	}
	public Integer getIndBankRetirementConfirm() {
		return indBankRetirementConfirm;
	}
	public void setIndBankRetirementConfirm(Integer indBankRetirementConfirm) {
		this.indBankRetirementConfirm = indBankRetirementConfirm;
	}
	public Integer getBankRetirementType() {
		return bankRetirementType;
	}
	public void setBankRetirementType(Integer bankRetirementType) {
		this.bankRetirementType = bankRetirementType;
	}
	public Date getBankRetirementDate() {
		return bankRetirementDate;
	}
	public void setBankRetirementDate(Date bankRetirementDate) {
		this.bankRetirementDate = bankRetirementDate;
	}
	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}
	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}
	
}