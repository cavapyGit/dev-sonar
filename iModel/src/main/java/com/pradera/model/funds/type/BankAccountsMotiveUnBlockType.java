package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BankAccountsMotiveUnBlockType {

OTHER(Integer.valueOf(1901),"OTROS");
	
	/** The Constant lookup. */
	public static final Map<Integer,BankAccountsMotiveUnBlockType> lookup=new HashMap<Integer, BankAccountsMotiveUnBlockType>();
	
	/** The Constant list. */
	public static final List<BankAccountsMotiveUnBlockType> list=new ArrayList<BankAccountsMotiveUnBlockType>();

	static{
		for(BankAccountsMotiveUnBlockType s:BankAccountsMotiveUnBlockType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BankAccountsMotiveUnBlockType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static BankAccountsMotiveUnBlockType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
}
