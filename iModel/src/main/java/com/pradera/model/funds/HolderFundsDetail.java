package com.pradera.model.funds;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.HolderCashMovement;

/**
 * Entity implementation class for Entity: HolderFundsDetail
 *
 */
@Entity
@Table(name="HOLDER_FUNDS_DETAIL")
public class HolderFundsDetail implements Serializable,Auditable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOLDER_FUNDS_DETAIL_SK", sequenceName="SQ_ID_HOLDER_FUNDS_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_FUNDS_DETAIL_SK")
	@Column(name="ID_HOLDER_FUNDS_DETAIL_PK")
	private Long idHolderFundsDetailPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FUNDS_OPERATION_FK")
	private HolderFundsOperation holderFundsOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_CASH_MOVEMENT_FK")
	private HolderCashMovement holderCashMovement;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	public Long getIdHolderFundsDetailPk() {
		return idHolderFundsDetailPk;
	}

	public void setIdHolderFundsDetailPk(Long idHolderFundsDetailPk) {
		this.idHolderFundsDetailPk = idHolderFundsDetailPk;
	}

	public HolderFundsOperation getHolderFundsOperation() {
		return holderFundsOperation;
	}

	public void setHolderFundsOperation(HolderFundsOperation holderFundsOperation) {
		this.holderFundsOperation = holderFundsOperation;
	}

	public HolderCashMovement getHolderCashMovement() {
		return holderCashMovement;
	}

	public void setHolderCashMovement(HolderCashMovement holderCashMovement) {
		this.holderCashMovement = holderCashMovement;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO review 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
