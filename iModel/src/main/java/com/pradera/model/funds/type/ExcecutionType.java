package com.pradera.model.funds.type;

public enum ExcecutionType {
	MANUAL_FUND(new Long(1)),
	AUTOMATIC_FUND(new Long(2));
	
	private ExcecutionType(Long code){
		this.code = code;
	}
	
	private Long code;

	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
}
