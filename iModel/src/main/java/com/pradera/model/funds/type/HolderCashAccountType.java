package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum HolderCashAccountType{
	SAVINGS(Integer.valueOf(1308),"AHORROS"),
	
	CURRENT(Integer.valueOf(1307),"CORRIENTE");
	
	/** The Constant list. */
	public static final List<HolderCashAccountType> list = new ArrayList<HolderCashAccountType>();
	
	/** The Constant listExternalInstitutions. */
	public static final List<HolderCashAccountType> listExternalInstitutions = new ArrayList<HolderCashAccountType>();
	
	/** The Constant listInternalInstitutions. */
	public static final List<HolderCashAccountType> listInternalInstitutions = new ArrayList<HolderCashAccountType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderCashAccountType> lookup = new HashMap<Integer, HolderCashAccountType>();
	static {
		for (HolderCashAccountType s : EnumSet.allOf(HolderCashAccountType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private HolderCashAccountType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static HolderCashAccountType get(Integer code) {
		return lookup.get(code);
	}
}