package com.pradera.model.funds;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the LIP_MESSAGE_HISTORY database table.
 * 
 */
@Entity
@Table(name="LIP_MESSAGE_HISTORY")
public class LipMessageHistory implements Serializable, Auditable{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	@Id
	@SequenceGenerator(name="LIP_MESSAGE_HISTORY_GENERATOR", sequenceName="SQ_ID_LIP_MESSAGE_HISTORY", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LIP_MESSAGE_HISTORY_GENERATOR")
	@Column(name="ID_LIP_MESSAGE_HISTORY_PK")
	private Long idLipMessageHistoryPk;
	
	@Column(name="PARTICIPANT_SOURCE_CODE")
	private Long participantSourceCode;
	
	@Column(name="PART_SOURCE_DESCRIPTION")
	private String partSourceDescription;
	
	@Column(name="DEPARTAMENT_CODE_SOURCE")
	private String departamentCodeSource;
	
	@Column(name="PARTICIPANT_TARGET_CODE")
	private Long participantTargetCode;
	
	@Column(name="PART_TARGET_DESCRIPTION")
	private String partTargetDescription;
	
	@Column(name="DEPARTAMENT_CODE_TARGET")
	private String departamentCodeTarget;	
	
	@Column(name="ACCOUNT_NUMBER")
	private String accountNumber;
	
	@Column(name="CURRENCY")
	private Integer currency;
	
	@Column(name="OPERATION_TYPE")
	private String operationType;
	
	@Column(name="OPER_TYPE_DESCRIPTION")
	private String operTypeDescription;	
	
	@Column(name="REQUEST_NUMBER")
	private Long requestNumber;
	
	@Column(name="TRANSACTION_AMOUNT")
	private BigDecimal transactionAmount;
	
	@Column(name="OPERATION_DATE")
	private Date operationDate;
	
	@Column(name="DESCRIPTION")
	private String description;	
	
	@Column(name="IND_RECEP_FUNDS")
	private Integer indRecepFunds = ComponentConstant.ZERO;
	
	@Column(name="OPERATION_HOUR")
	private String operationHour;	
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;	
	
	public LipMessageHistory() {
		super();
	}

	/**
	 * @return the idLipMessageHistoryPk
	 */
	public Long getIdLipMessageHistoryPk() {
		return idLipMessageHistoryPk;
	}

	/**
	 * @param idLipMessageHistoryPk the idLipMessageHistoryPk to set
	 */
	public void setIdLipMessageHistoryPk(Long idLipMessageHistoryPk) {
		this.idLipMessageHistoryPk = idLipMessageHistoryPk;
	}

	/**
	 * @return the participantSourceCode
	 */
	public Long getParticipantSourceCode() {
		return participantSourceCode;
	}

	/**
	 * @param participantSourceCode the participantSourceCode to set
	 */
	public void setParticipantSourceCode(Long participantSourceCode) {
		this.participantSourceCode = participantSourceCode;
	}

	/**
	 * @return the partSourceDescription
	 */
	public String getPartSourceDescription() {
		return partSourceDescription;
	}

	/**
	 * @param partSourceDescription the partSourceDescription to set
	 */
	public void setPartSourceDescription(String partSourceDescription) {
		this.partSourceDescription = partSourceDescription;
	}

	/**
	 * @return the departamentCodeSource
	 */
	public String getDepartamentCodeSource() {
		return departamentCodeSource;
	}

	/**
	 * @param departamentCodeSource the departamentCodeSource to set
	 */
	public void setDepartamentCodeSource(String departamentCodeSource) {
		this.departamentCodeSource = departamentCodeSource;
	}

	/**
	 * @return the participantTargetCode
	 */
	public Long getParticipantTargetCode() {
		return participantTargetCode;
	}

	/**
	 * @param participantTargetCode the participantTargetCode to set
	 */
	public void setParticipantTargetCode(Long participantTargetCode) {
		this.participantTargetCode = participantTargetCode;
	}

	/**
	 * @return the partTargetDescription
	 */
	public String getPartTargetDescription() {
		return partTargetDescription;
	}

	/**
	 * @param partTargetDescription the partTargetDescription to set
	 */
	public void setPartTargetDescription(String partTargetDescription) {
		this.partTargetDescription = partTargetDescription;
	}

	/**
	 * @return the departamentCodeTarget
	 */
	public String getDepartamentCodeTarget() {
		return departamentCodeTarget;
	}

	/**
	 * @param departamentCodeTarget the departamentCodeTarget to set
	 */
	public void setDepartamentCodeTarget(String departamentCodeTarget) {
		this.departamentCodeTarget = departamentCodeTarget;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the operTypeDescription
	 */
	public String getOperTypeDescription() {
		return operTypeDescription;
	}

	/**
	 * @param operTypeDescription the operTypeDescription to set
	 */
	public void setOperTypeDescription(String operTypeDescription) {
		this.operTypeDescription = operTypeDescription;
	}

	/**
	 * @return the requestNumber
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the transactionAmount
	 */
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}

	/**
	 * @param transactionAmount the transactionAmount to set
	 */
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	/**
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the indRecepFunds
	 */
	public Integer getIndRecepFunds() {
		return indRecepFunds;
	}

	/**
	 * @param indRecepFunds the indRecepFunds to set
	 */
	public void setIndRecepFunds(Integer indRecepFunds) {
		this.indRecepFunds = indRecepFunds;
	}

	/**
	 * @return the operationHour
	 */
	public String getOperationHour() {
		return operationHour;
	}

	/**
	 * @param operationHour the operationHour to set
	 */
	public void setOperationHour(String operationHour) {
		this.operationHour = operationHour;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
