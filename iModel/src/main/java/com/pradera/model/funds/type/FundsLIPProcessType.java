package com.pradera.model.funds.type;

public enum FundsLIPProcessType {
	ENVIO(new String("E03"),"ENVIO DE FONDOS LIP E03"),
	ENVIO_E74(new String("E74"),"ENVIO DE FONDOS LIP E74"),
	RECEPCION(new String("E02"),"RECEPTION DE FONDOS LIP E02"),
	RECEPCION_E73(new String("E73"),"RECEPTION DE FONDOS LIP E73");
	
	private FundsLIPProcessType(String code,String value){
		this.code = code;
		this.value = value;
	}
	
	private String code;
	private String value;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
