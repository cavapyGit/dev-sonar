package com.pradera.model.funds.constant;

public class FundsAutomaticConstants 
{
	public static final String SETTLEMENT_TYPE_GROSS= "B";
	public static final String DEPARTMENT_DEFECT_BCB= "200";
	public static final String CREDIT_ENTITY= "N";
	public static final String PARTICIPANT_ADITIONAL_INFORMATION_CODE= "PART_INDIRECTO";
	public static final String USER_DEFECT_BCB= "CCACERES";
	
	public static final String RETIREMENT_COLLECTION_ECONOMIC = "RETIRO CDDE";
}
