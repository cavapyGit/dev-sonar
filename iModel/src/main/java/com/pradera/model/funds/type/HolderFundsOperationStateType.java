package com.pradera.model.funds.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum HolderFundsOperationStateType {
	REGISTERED(Integer.valueOf(1504),"REGISTRADA"),		
	CONFIRMED(Integer.valueOf(1505),"CONFIRMADA"),		
	REJECTED(Integer.valueOf(1506),"RECHAZADA");
	
	/** The Constant list. */
	public static final List<HolderFundsOperationStateType> list = new ArrayList<HolderFundsOperationStateType>();
	
	/** The Constant listExternalInstitutions. */
	public static final List<HolderFundsOperationStateType> listExternalInstitutions = new ArrayList<HolderFundsOperationStateType>();
	
	/** The Constant listInternalInstitutions. */
	public static final List<HolderFundsOperationStateType> listInternalInstitutions = new ArrayList<HolderFundsOperationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolderFundsOperationStateType> lookup = new HashMap<Integer, HolderFundsOperationStateType>();
	static {
		for (HolderFundsOperationStateType s : EnumSet.allOf(HolderFundsOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private HolderFundsOperationStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static HolderFundsOperationStateType get(Integer code) {
		return lookup.get(code);
	}
}
