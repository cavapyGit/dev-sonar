package com.pradera.model.funds;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;


/**
 * The persistent class for the INSTITUTION_CASH_ACCOUNT database table.
 * 
 */
@Entity
@Table(name="INSTITUTION_CASH_ACCOUNT")
public class InstitutionCashAccount implements Serializable,Auditable {

	private static final long serialVersionUID = 5283307942361241058L;

	@Id
	@NotNull
	@SequenceGenerator(name="INST_CASH_ACCOUNT_GENERATOR", sequenceName="SQ_ID_INST_CASH_ACCOUNT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INST_CASH_ACCOUNT_GENERATOR")
	@Column(name="ID_INSTITUTION_CASH_ACCOUNT_PK")
	private Long idInstitutionCashAccountPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REFERENCE_CASH_ACCOUNT_FK")
	private InstitutionCashAccount institutionCashAccount;

	@Column(name="ACCOUNT_TYPE")
	private Integer accountType;

	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="TOTAL_AMOUNT")
	private BigDecimal totalAmount;

	@Column(name="AVAILABLE_AMOUNT")
	private BigDecimal availableAmount;

	@Column(name="DEPOSIT_AMOUNT")
	private BigDecimal depositAmount;

	@Column(name="RETIREMENT_AMOUNT")
	private BigDecimal retirementAmount;

	@Column(name="MARGIN_AMOUNT")
	private BigDecimal marginAmount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="ACCOUNT_STATE")
	private Integer accountState;

	@Column(name="SETTLEMENT_SCHEMA")
	private Integer settlementSchema;

	@ManyToOne(fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	@JoinColumn(name="ID_MODALITY_GROUP_FK")
	private ModalityGroup modalityGroup;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK")
	private NegotiationMechanism negotiationMechanism;
	
	@Column(name="IND_RELATED_BCRD")
	private Integer indRelatedBcrd;

	@Column(name="IND_AUTOMATIC_PROCESS")
	private Integer indAutomaticProcess;
	
	@Column(name="SITUATION")
	private Integer situation;

	@Column(name="ACCOUNT_CLASS")
	private Integer accountClass;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	//bi-directional many-to-one association to CorporativeProcessResult
	/** The corporative process results. */
	@OneToMany(mappedBy="institutionCashAccount")
	private List<CashAccountDetail> cashAccountDetailResults;
	
	/** The holder account balances. */
	@OneToMany(mappedBy = "institutionCashAccount")
	private List<FundsOperation> fundsOperation;
	
	@Column(name="IND_SEND_SETTLEMENT_FUNDS")
	private Integer indSendSettlementFunds;
	
	@Column(name="IND_SETTLEMENT_PROCESS")
	private Integer indSettlementProcess;

	@Transient
	private String accountTypeDescription;
	@Transient
	private String currencyTypeDescription;
	@Transient
	private String  settlementSchemaDescription;	
	@Transient
	private String  situationDescription;
	@Transient
	private boolean blSelected;
	@Transient
	private String bankDescription;
	@Transient
	private String bankAccount;
	@Transient
	private BigDecimal fundsOperationAmount;
	@Transient
	private String indCentralizer;
	@Transient
	private String accountNumber;
	@Version
	private Long version;	
	
    public InstitutionCashAccount() {
    	
    }
    public String getAccountTypeDescription(){
    	if(Validations.validateIsNotNullAndPositive(accountType)){
    		return  accountTypeDescription;
    	} else {
    		return "";
    	}
    }
	public Long getIdInstitutionCashAccountPk() {
		return idInstitutionCashAccountPk;
	}
	public void setIdInstitutionCashAccountPk(Long idInstitutionCashAccountPk) {
		this.idInstitutionCashAccountPk = idInstitutionCashAccountPk;
	}
	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}
	public void setInstitutionCashAccount(
			InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public BigDecimal getAvailableAmount() {
		return availableAmount;
	}
	public void setAvailableAmount(BigDecimal availableAmount) {
		this.availableAmount = availableAmount;
	}
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}
	public BigDecimal getRetirementAmount() {
		return retirementAmount;
	}
	public void setRetirementAmount(BigDecimal retirementAmount) {
		this.retirementAmount = retirementAmount;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public Integer getSituation() {
		return situation;
	}
	public void setSituation(Integer situation) {
		this.situation = situation;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public ModalityGroup getModalityGroup() {
		return modalityGroup;
	}
	public void setModalityGroup(ModalityGroup modalityGroup) {
		this.modalityGroup = modalityGroup;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}
	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}

	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	public Integer getAccountState() {
		return accountState;
	}


	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}
	public Integer getSettlementSchema() {
		return settlementSchema;
	}
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	public Integer getIndRelatedBcrd() {
		return indRelatedBcrd;
	}
	public void setIndRelatedBcrd(Integer indRelatedBcrd) {
		this.indRelatedBcrd = indRelatedBcrd;
	}
	public Integer getAccountClass() {
		return accountClass;
	}
	public void setAccountClass(Integer accountClass) {
		this.accountClass = accountClass;
	}


	public BigDecimal getMarginAmount() {
		return marginAmount;
	}


	public void setMarginAmount(BigDecimal marginAmount) {
		this.marginAmount = marginAmount;
	}
	
	public List<FundsOperation> getFundsOperation() {
		return fundsOperation;
	}
	public void setFundsOperation(List<FundsOperation> fundsOperation) {
		this.fundsOperation = fundsOperation;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		
        return detailsMap;
	}


	public Integer getAccountType() {
		return accountType;
	}


	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public void setAccountTypeDescription(String accountTypeDescription) {
		this.accountTypeDescription = accountTypeDescription;
	}
	public String getSettlementSchemaDescription() {
		return settlementSchemaDescription;
	}
	public void setSettlementSchemaDescription(
			String settlementSchemaDescription) {
		this.settlementSchemaDescription = settlementSchemaDescription;
	}
	public String getSituationDescription() {
		return situationDescription;
	}
	public void setSituationDescription(String situationDescription) {
		this.situationDescription = situationDescription;
	}
	public String getCurrencyTypeDescription() {
		if(Validations.validateIsNotNull(currency)){
			return CurrencyType.get(currency).getCodeIso();
		}
		return currencyTypeDescription;
	}
	public void setCurrencyTypeDescription(String currencyTypeDescription) {
		this.currencyTypeDescription = currencyTypeDescription;
	}
	public boolean isBlSelected() {
		return blSelected;
	}
	public void setBlSelected(boolean blSelected) {
		this.blSelected = blSelected;
	}
	public String getBankDescription() {
		return bankDescription;
	}
	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public BigDecimal getFundsOperationAmount() {
		return fundsOperationAmount;
	}
	public void setFundsOperationAmount(BigDecimal fundsOperationAmount) {
		this.fundsOperationAmount = fundsOperationAmount;
	}
	public String getIndCentralizer() {
		return indCentralizer;
	}
	public void setIndCentralizer(String indCentralizer) {
		this.indCentralizer = indCentralizer;
	}
	public Integer getIndAutomaticProcess() {
		return indAutomaticProcess;
	}
	public void setIndAutomaticProcess(Integer indAutomaticProcess) {
		this.indAutomaticProcess = indAutomaticProcess;
	}
	
	public Integer getIndSendSettlementFunds() {
		return indSendSettlementFunds;
	}
	public void setIndSendSettlementFunds(Integer indSendSettlementFunds) {
		this.indSendSettlementFunds = indSendSettlementFunds;
	}
	public List<CashAccountDetail> getCashAccountDetailResults() {
		return cashAccountDetailResults;
	}
	public void setCashAccountDetailResults(
			List<CashAccountDetail> cashAccountDetailResults) {
		this.cashAccountDetailResults = cashAccountDetailResults;
	}
	public Integer getIndSettlementProcess() {
		return indSettlementProcess;
	}
	public void setIndSettlementProcess(Integer indSettlementProcess) {
		this.indSettlementProcess = indSettlementProcess;
	}
	
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}	
	
}