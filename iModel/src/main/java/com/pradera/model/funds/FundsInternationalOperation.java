package com.pradera.model.funds;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.InternationalOperation;

@Entity
@Table(name="FUNDS_INTERNATIONAL_OPERATION")
public class FundsInternationalOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="FUNDS_INTERNATIONAL_OPERATION_IDFUNDSINTERNATIONALOPERATIONPK_GENERATOR", sequenceName="SQ_ID_FUNDS_INTER_OPER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FUNDS_INTERNATIONAL_OPERATION_IDFUNDSINTERNATIONALOPERATIONPK_GENERATOR")
	@Column(name="ID_FUNDS_INTER_OPER_PK")
	private Long idFundsInterOperPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_FK")
	private FundsOperation fundsOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERNATIONAL_OPERATION_FK")
	private InternationalOperation internationalOperation;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	public FundsOperation getFundsOperation() {
		return fundsOperation;
	}

	public void setFundsOperation(FundsOperation fundsOperation) {
		this.fundsOperation = fundsOperation;
	}

	public InternationalOperation getInternationalOperation() {
		return internationalOperation;
	}

	public void setInternationalOperation(
			InternationalOperation internationalOperation) {
		this.internationalOperation = internationalOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		 // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdFundsInterOperPk() {
		return idFundsInterOperPk;
	}

	public void setIdFundsInterOperPk(Long idFundsInterOperPk) {
		this.idFundsInterOperPk = idFundsInterOperPk;
	}
	
	
}
