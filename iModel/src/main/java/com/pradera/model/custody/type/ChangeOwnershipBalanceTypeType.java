/**@author nmolina
 * 
 */

package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public enum ChangeOwnershipBalanceTypeType {
		
	AVAILABLE_BALANCE(new Integer(862),"SALDO DISPONIBLE"),
	BLOCKED_BALANCE(new Integer(863),"SALDO BLOQUEADO");
	
	private Integer code;
	private String value;
	public static final List<ChangeOwnershipBalanceTypeType> list = new ArrayList<ChangeOwnershipBalanceTypeType>();
	
	public static final Map<Integer, ChangeOwnershipBalanceTypeType> lookup = new HashMap<Integer, ChangeOwnershipBalanceTypeType>();
	
	public static List<ChangeOwnershipBalanceTypeType> listSomeElements(ChangeOwnershipBalanceTypeType... transferSecuritiesTypeParams){
		List<ChangeOwnershipBalanceTypeType> retorno = new ArrayList<ChangeOwnershipBalanceTypeType>();
		for(ChangeOwnershipBalanceTypeType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ChangeOwnershipBalanceTypeType s : EnumSet.allOf(ChangeOwnershipBalanceTypeType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private ChangeOwnershipBalanceTypeType(Integer ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}