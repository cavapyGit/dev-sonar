package com.pradera.model.custody.transfersecurities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_CODE database table.
 * 
 */
@Entity
@Table(name="TOTAL_SECURITY_CLASS_BALANCE")
@NamedQuery(name="TotalSecurityClassBalance.findAll", query="SELECT v FROM TotalSecurityClassBalance v")
public class TotalSecurityClassBalance implements Serializable,Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TOTAL_SECURITY_CLASS_BALANCE_IDTOTALSECCLASSBALANCEPK_GENERATOR", sequenceName="SQ_TOTAL_SEC_CLASS_BALANCE_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOTAL_SECURITY_CLASS_BALANCE_IDTOTALSECCLASSBALANCEPK_GENERATOR")
	
	@Column(name="ID_TOTAL_SEC_CLASS_BALANCE_PK")
	private Long idTotalSecClassBalancePk;

	@Column(name="PROCESS_DATE")
	private Date processDate;

	@Column(name="SECURITY_CLASS")
	private Integer securityClass;
	
	@Column(name="CURRENCY")
	private Integer currency;
	
	@Column(name="YEAR_TOTAL_BALANCE")
	private BigDecimal yearTotalBalance;
	
	@Column(name="YEAR_TOTAL_AMOUNT")
	private BigDecimal yearTotalAmount;
	
	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;
	
	@Column(name="TOTAL_AMOUNT")
	private BigDecimal totalAmount;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;


	public TotalSecurityClassBalance() {
	}

	/**
	 * @return the idTotalSecClassBalancePk
	 */
	public Long getIdTotalSecClassBalancePk() {
		return idTotalSecClassBalancePk;
	}

	/**
	 * @param idTotalSecClassBalancePk the idTotalSecClassBalancePk to set
	 */
	public void setIdTotalSecClassBalancePk(Long idTotalSecClassBalancePk) {
		this.idTotalSecClassBalancePk = idTotalSecClassBalancePk;
	}

	/**
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * @return the stockQuantity
	 */
	
	
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * @return the yearTotalBalance
	 */
	public BigDecimal getYearTotalBalance() {
		return yearTotalBalance;
	}

	/**
	 * @param yearTotalBalance the yearTotalBalance to set
	 */
	public void setYearTotalBalance(BigDecimal yearTotalBalance) {
		this.yearTotalBalance = yearTotalBalance;
	}

	/**
	 * @return the yearTotalAmount
	 */
	public BigDecimal getYearTotalAmount() {
		return yearTotalAmount;
	}

	/**
	 * @param yearTotalAmount the yearTotalAmount to set
	 */
	public void setYearTotalAmount(BigDecimal yearTotalAmount) {
		this.yearTotalAmount = yearTotalAmount;
	}

	/**
	 * @return the totalBalance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	/**
	 * @param totalBalance the totalBalance to set
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}



	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}



	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}



	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}