package com.pradera.model.custody.securitiesretirement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the RETIREMENT_OPERATION database table.
 * 
 */
@Entity
@Table(name="RETIREMENT_OPERATION")
public class RetirementOperation implements Serializable, Auditable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long idRetirementOperationPk; 
	
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_RETIREMENT_OPERATION_PK")
	private CustodyOperation custodyOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK")	
	private Security security;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK", referencedColumnName="ID_PHYSICAL_CERTIFICATE_PK")	
	private PhysicalCertificate physicalCertificate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK")	
	private Security targetSecurity;
	
	@Column(name="RETIREMENT_TYPE")
	private Integer retirementType;
	
	@Column(name="IND_RETIREMENT_EXPIRATION")
	private Integer indRetirementExpiration;
		
	@Column(name="MOTIVE")
	private Integer motive;

	@Column(name="STATE")
	private Integer state;

	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;
	
	@Column(name="IND_WITHDRAWAL_BCB")
	private Integer indWithdrawalBCB;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ANNOTATION_OPERATION_FK", referencedColumnName="ID_ANNOTATION_OPERATION_PK")	
	private AccountAnnotationOperation accountAnnotationOperation;
	
	@OneToMany(mappedBy="retirementOperation")
	private List<CorporativeOperation> lstCorporativeOperation;

	//bi-directional many-to-one association to RetirementDetail
	@OneToMany(mappedBy="retirementOperation")
	private List<RetirementDetail> retirementDetails;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_BUYER_FK")
	private Participant participantBuyer;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_BUYER_FK")
	private HolderAccount holderAccountBuyer;

	@Column(name="CASH_PRICE")
	private BigDecimal cashPrice;

	@Column(name="AMOUNT_RATE")
	private BigDecimal amountRate;
	
	/** The reject motive. */
	@Transient
	private Integer rejectMotive;
	
	/** The reject motive other. */
	@Transient
	private String rejectMotiveOther;

	@Transient
	private Integer currency;

	@Transient
	private Issuer issuer;

	@Transient
	private Date corporativePaymentDate;
	
	@Transient
	private Participant participantSeller;
	
	@Transient
	private HolderAccount holderAccountSeller;
	
	@Transient
	private PayrollHeader payrollHeader;
	

    public RetirementOperation() {
    	super();
    	this.indWithdrawalBCB= BooleanType.NO.getCode();
    	this.indRetirementExpiration= BooleanType.NO.getCode();
    }

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getMotive() {
		return this.motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public List<RetirementDetail> getRetirementDetails() {
		return this.retirementDetails;
	}

	public void setRetirementDetails(List<RetirementDetail> retirementDetails) {
		this.retirementDetails = retirementDetails;
	}

	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public Long getIdRetirementOperationPk() {
		return idRetirementOperationPk;
	}

	public void setIdRetirementOperationPk(Long idRetirementOperationPk) {
		this.idRetirementOperationPk = idRetirementOperationPk;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(custodyOperation != null)
            custodyOperation.setAudit(loggerUser);                        
        }
		
	}

	@Override
	 public Map<String, List<? extends Auditable>> getListForAudit() {
	 	HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("retirementDetails", retirementDetails);
        return detailsMap;
	 }

	public List<CorporativeOperation> getLstCorporativeOperation() {
		return lstCorporativeOperation;
	}

	public void setLstCorporativeOperation(
			List<CorporativeOperation> lstCorporativeOperation) {
		this.lstCorporativeOperation = lstCorporativeOperation;
	}
	
	/*Start Motive*/
	
	public boolean isRedemptionFixedIncomeMotive(){
		if(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode().equals(getMotive())){
			return true;
		}
		return false;
	}
	
	public boolean isRedemptionVariableIncomeMotive(){
		if(RetirementOperationMotiveType.REDEMPTION_VARIABLE_INCOME.getCode().equals(getMotive())){
			return true;
		}
		return false;
	}
	
	public boolean isReversionMotive(){
		if(RetirementOperationMotiveType.REVERSION.getCode().equals(getMotive())){
			return true;
		}
		return false;
	}
	
	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Security getTargetSecurity() {
		return targetSecurity;
	}

	public void setTargetSecurity(Security targetSecurity) {
		this.targetSecurity = targetSecurity;
	}

	public Integer getRetirementType() {
		return retirementType;
	}

	public void setRetirementType(Integer retirementType) {
		this.retirementType = retirementType;
	}

	public Integer getIndWithdrawalBCB() {
		return indWithdrawalBCB;
	}

	public void setIndWithdrawalBCB(Integer indWithdrawalBCB) {
		this.indWithdrawalBCB = indWithdrawalBCB;
	}

	public AccountAnnotationOperation getAccountAnnotationOperation() {
		return accountAnnotationOperation;
	}

	public void setAccountAnnotationOperation(
			AccountAnnotationOperation accountAnnotationOperation) {
		this.accountAnnotationOperation = accountAnnotationOperation;
	}

	/**
	 * @return the rejectMotive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * @param rejectMotive the rejectMotive to set
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * @return the rejectMotiveOther
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	/**
	 * @param rejectMotiveOther the rejectMotiveOther to set
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * @return the indRetirementExpiration
	 */
	public Integer getIndRetirementExpiration() {
		return indRetirementExpiration;
	}

	/**
	 * @param indRetirementExpiration the indRetirementExpiration to set
	 */
	public void setIndRetirementExpiration(Integer indRetirementExpiration) {
		this.indRetirementExpiration = indRetirementExpiration;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Date getCorporativePaymentDate() {
		return corporativePaymentDate;
	}

	public void setCorporativePaymentDate(Date corporativePaymentDate) {
		this.corporativePaymentDate = corporativePaymentDate;
	}

	public Participant getParticipantBuyer() {
		return participantBuyer;
	}

	public void setParticipantBuyer(Participant participantBuyer) {
		this.participantBuyer = participantBuyer;
	}

	public HolderAccount getHolderAccountBuyer() {
		return holderAccountBuyer;
	}

	public void setHolderAccountBuyer(HolderAccount holderAccountBuyer) {
		this.holderAccountBuyer = holderAccountBuyer;
	}

	public BigDecimal getCashPrice() {
		return cashPrice;
	}

	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	public BigDecimal getAmountRate() {
		return amountRate;
	}

	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}

	public Participant getParticipantSeller() {
		return participantSeller;
	}

	public void setParticipantSeller(Participant participantSeller) {
		this.participantSeller = participantSeller;
	}

	public HolderAccount getHolderAccountSeller() {
		return holderAccountSeller;
	}

	public void setHolderAccountSeller(HolderAccount holderAccountSeller) {
		this.holderAccountSeller = holderAccountSeller;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public PayrollHeader getPayrollHeader() {
		return payrollHeader;
	}

	public void setPayrollHeader(PayrollHeader payrollHeader) {
		this.payrollHeader = payrollHeader;
	}

	@Override
	public String toString() {
		return "RetirementOperation [idRetirementOperationPk=" + idRetirementOperationPk 
				+ ", retirementType=" + retirementType
				+ ", indRetirementExpiration=" + indRetirementExpiration + ", motive=" + motive + ", state=" + state
				+ ", totalBalance=" + totalBalance + ", cashPrice=" + cashPrice + ", amountRate=" + amountRate +"]";
	}
	
}