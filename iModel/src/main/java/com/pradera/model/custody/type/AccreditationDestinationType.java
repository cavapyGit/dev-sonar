package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AccreditationDestinationType {
	HOLDER(new Integer(645),"TITULAR"),
	MESSENGER(new Integer(646),"MENSAJERO");
	
	private Integer code;
	private String value;
	public static final List<AccreditationDestinationType> list = new ArrayList<AccreditationDestinationType>();
	
	public static final Map<Integer, AccreditationDestinationType> lookup = new HashMap<Integer, AccreditationDestinationType>();
	public static List<AccreditationDestinationType> listSomeElements(AccreditationDestinationType... accreditationDestinationType){
		List<AccreditationDestinationType> retorno = new ArrayList<AccreditationDestinationType>();
		for(AccreditationDestinationType accreditationDestinationTypeO: accreditationDestinationType){
			retorno.add(accreditationDestinationTypeO);
		}
		return retorno;
	}
	static {
		for (AccreditationDestinationType s : EnumSet.allOf(AccreditationDestinationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private AccreditationDestinationType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static AccreditationDestinationType get(Integer code) {
		return lookup.get(code);
	}
}
