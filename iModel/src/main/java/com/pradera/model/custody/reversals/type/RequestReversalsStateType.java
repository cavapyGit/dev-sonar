package com.pradera.model.custody.reversals.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RequestReversalsStateType {
	
	/** The registered. */
	REGISTERED(new Integer(173)),
	
	APPROVED(new Integer(2182)),
	
	ANNULED(new Integer(2183)),
	
	/** The confirmed. */
	CONFIRMED(new Integer(1208)),
	
	/** The rejected. */
	REJECTED(new Integer(1209));
	
	/** The code. */
	private Integer code;
	

	private RequestReversalsStateType(Integer code){
		this.code = code;
	}
		
	/** The Constant list. */
	public static final List<RequestReversalsStateType> list = new ArrayList<RequestReversalsStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RequestReversalsStateType> lookup = new HashMap<Integer, RequestReversalsStateType>();
	
	static {
		for (RequestReversalsStateType s : EnumSet.allOf(RequestReversalsStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static RequestReversalsStateType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}	
}
