package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RectificationOperationStateType {
	 
		REGISTERED(new Integer(1449),"REGISTRADO"),
	    CONFIRMED(new Integer(1450),"CONFIRMADO"),
		REJECTED(new Integer(1451),"RECHAZADO");
  		
		private String value;
		private Integer code;
		
		public static final List<RectificationOperationStateType> list= new ArrayList<RectificationOperationStateType>();
 		public static final Map<Integer, RectificationOperationStateType> lookup = new HashMap<Integer, RectificationOperationStateType >();
		
		static{
			for(RectificationOperationStateType type: EnumSet.allOf(RectificationOperationStateType.class))
			{
				list.add(type);
				lookup.put(type.getCode(),type);
			}
			
			
		}
		
		
		private RectificationOperationStateType(Integer code, String value) {
			this.value = value;
			this.code = code;
		}


		public String getValue() {
			return value;
		}


	    public Integer getCode() {
			return code;
		}
		
		public static RectificationOperationStateType get(Integer code)
		{
			return lookup.get(code);
		}


		
		
		
		

	}
