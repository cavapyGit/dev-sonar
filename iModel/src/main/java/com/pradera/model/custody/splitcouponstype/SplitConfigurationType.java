package com.pradera.model.custody.splitcouponstype;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum SplitCouponStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public enum SplitConfigurationType {
	
	/** The max day split. */
	MAX_DAY_SPLIT(new Integer(2335), "MAX DIAS PARA PODER DESPRENDER");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new split coupon state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SplitConfigurationType(Integer code, String value){
		this.code = code;
		this.value = value;
	}
		
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
