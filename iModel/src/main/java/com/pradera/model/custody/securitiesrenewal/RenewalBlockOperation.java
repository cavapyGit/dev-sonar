package com.pradera.model.custody.securitiesrenewal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;

@Entity
@Table(name="RENEWAL_BLOCK_OPERATION")
public class RenewalBlockOperation implements Serializable,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The id renewal block operation pk. */
	@Id
	@SequenceGenerator(name="SQ_ID_RENEWAL_BLOCK", sequenceName="SQ_ID_RENEWAL_BLOCK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_RENEWAL_BLOCK")
	@Column(name="ID_RENEWAL_BLOCK_PK")
	private Long idRenewalBlockPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RENEWAL_DETAIL_FK", referencedColumnName="ID_RENEWAL_DETAIL_PK")
	private RenewalOperationDetail renewalOperationDetail;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK", referencedColumnName="ID_BLOCK_OPERATION_PK")
	private BlockOperation blockOperation;
	
	/** The indicator source target */
	@Column(name="IND_SOURCE_TARGET")
	private Integer indSourceTarget;
	
	/** The block quantity */
	@Column(name="BLOCK_QUANTITY")
	private BigDecimal blockQuantity;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The renewal marketfact operation. */
    @OneToMany(mappedBy="renewalBlockOperation",fetch=FetchType.LAZY)
    private List<RenewalMarketfactOperation> renewalMarketfactOperation;

	/**
	 * @return the idRenewalBlockPk
	 */
	public Long getIdRenewalBlockPk() {
		return idRenewalBlockPk;
	}

	/**
	 * @param idRenewalBlockPk the idRenewalBlockPk to set
	 */
	public void setIdRenewalBlockPk(Long idRenewalBlockPk) {
		this.idRenewalBlockPk = idRenewalBlockPk;
	}

	/**
	 * @return the renewalOperationDetail
	 */
	public RenewalOperationDetail getRenewalOperationDetail() {
		return renewalOperationDetail;
	}

	/**
	 * @param renewalOperationDetail the renewalOperationDetail to set
	 */
	public void setRenewalOperationDetail(
			RenewalOperationDetail renewalOperationDetail) {
		this.renewalOperationDetail = renewalOperationDetail;
	}

	/**
	 * @return the blockOperation
	 */
	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	/**
	 * @param blockOperation the blockOperation to set
	 */
	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	/**
	 * @return the indSourceTarget
	 */
	public Integer getIndSourceTarget() {
		return indSourceTarget;
	}

	/**
	 * @param indSourceTarget the indSourceTarget to set
	 */
	public void setIndSourceTarget(Integer indSourceTarget) {
		this.indSourceTarget = indSourceTarget;
	}

	/**
	 * @return the blockQuantity
	 */
	public BigDecimal getBlockQuantity() {
		return blockQuantity;
	}

	/**
	 * @param blockQuantity the blockQuantity to set
	 */
	public void setBlockQuantity(BigDecimal blockQuantity) {
		this.blockQuantity = blockQuantity;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the renewalMarketfactOperation
	 */
	public List<RenewalMarketfactOperation> getRenewalMarketfactOperation() {
		return renewalMarketfactOperation;
	}

	/**
	 * @param renewalMarketfactOperation the renewalMarketfactOperation to set
	 */
	public void setRenewalMarketfactOperation(
			List<RenewalMarketfactOperation> renewalMarketfactOperation) {
		this.renewalMarketfactOperation = renewalMarketfactOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            this.lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            this.lastModifyDate = loggerUser.getAuditTime();
            this.lastModifyIp = loggerUser.getIpAddress();
            this.lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		 HashMap<String,List<? extends Auditable>> detailsMap = 
	                new HashMap<String, List<? extends Auditable>>();
	        detailsMap.put("renewalMarketfactOperation", renewalMarketfactOperation);
	        return detailsMap;
	}

}
