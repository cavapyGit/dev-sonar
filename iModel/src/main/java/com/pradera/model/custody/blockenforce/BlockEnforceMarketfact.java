package com.pradera.model.custody.blockenforce;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.HolderMarketFactBalance;


/**
 * The persistent class for the BLOCK_ENFORCE_MARKETFACT database table.
 * 
 */
@Entity
@Table(name="BLOCK_ENFORCE_MARKETFACT")
@NamedQuery(name="BlockEnforceMarketfact.findAll", query="SELECT b FROM BlockEnforceMarketfact b")
public class BlockEnforceMarketfact implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BLOCK_ENFORCE_MARKETFACT_IDENFORCEMARKETFACTPK_GENERATOR", sequenceName="SQ_ID_ENFORCE_MARKETFACT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_ENFORCE_MARKETFACT_IDENFORCEMARKETFACTPK_GENERATOR")
	@Column(name="ID_ENFORCE_MARKETFACT_PK")
	private Long idEnforceMarketfactPk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;

	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

	@Column(name="MARKET_QUANTITY")
	private BigDecimal marketQuantity;

	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;

	//bi-directional many-to-one association to BlockEnforceDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ENFORCE_DETAIL_FK")
	private BlockEnforceDetail blockEnforceDetail;
	
	@Transient
	private HolderMarketFactBalance holderMarketFactBalance;

	public BlockEnforceMarketfact() {
	}

	public Long getIdEnforceMarketfactPk() {
		return this.idEnforceMarketfactPk;
	}

	public void setIdEnforceMarketfactPk(Long idEnforceMarketfactPk) {
		this.idEnforceMarketfactPk = idEnforceMarketfactPk;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMarketDate() {
		return this.marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getMarketQuantity() {
		return this.marketQuantity;
	}

	public void setMarketQuantity(BigDecimal marketQuantity) {
		this.marketQuantity = marketQuantity;
	}

	public BigDecimal getMarketRate() {
		return this.marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BlockEnforceDetail getBlockEnforceDetail() {
		return this.blockEnforceDetail;
	}

	public void setBlockEnforceDetail(BlockEnforceDetail blockEnforceDetail) {
		this.blockEnforceDetail = blockEnforceDetail;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public HolderMarketFactBalance getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	public void setHolderMarketFactBalance(
			HolderMarketFactBalance holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

}