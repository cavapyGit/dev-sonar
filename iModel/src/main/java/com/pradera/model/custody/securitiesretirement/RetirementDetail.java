package com.pradera.model.custody.securitiesretirement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the RETIREMENT_DETAIL database table.
 * 
 */
@Entity
@Table(name="RETIREMENT_DETAIL")
public class RetirementDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id retirement detail pk. */
	@Id
	@SequenceGenerator(name="SQ_ID_RETIREMENT_DETAIL_PK", sequenceName="SQ_ID_RETIREMENT_DETAIL_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_RETIREMENT_DETAIL_PK")
	@Column(name="ID_RETIREMENT_DETAIL_PK")
	private Long idRetirementDetailPk;

	/** The account balance. */
	@Column(name="AVAILABLE_BALANCE")	
	private BigDecimal availableBalance;
	
	@Column(name="PAWN_BALANCE")	
	private BigDecimal pawnBalance;
	
	@Column(name="OTHER_BLOCK_BALANCE")	
	private BigDecimal otherBlockBalance;
	
	@Column(name="BAN_BALANCE")	
	private BigDecimal banBalance;

	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")	
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK") 	
	private Participant participant;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK", referencedColumnName="ID_PHYSICAL_CERTIFICATE_PK") 
	private PhysicalCertificate physicalCertificate;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to RetirementOperation
    /** The retirement operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RETIREMENT_OPERATION_FK", referencedColumnName="ID_RETIREMENT_OPERATION_PK")
	private RetirementOperation retirementOperation;
	
	/** The retirement marketfacts. */
    @OneToMany(mappedBy="retirementDetail",fetch=FetchType.LAZY)
    private List<RetirementBlockDetail> retirementBlockDetails;

    /** The retirement marketfacts. */
    @OneToMany(mappedBy="retirementDetail",fetch=FetchType.LAZY)
    private List<RetirementMarketfact> retirementMarketfacts;
    
    public BigDecimal getTotalBalance(){
    	if(availableBalance == null) {
    		availableBalance = BigDecimal.ZERO;
    	}
    	if(banBalance==null) {
    		banBalance = BigDecimal.ZERO;
    	}
    	if(pawnBalance==null) {
    		pawnBalance = BigDecimal.ZERO;
    	}
    	if(otherBlockBalance==null) {
    		otherBlockBalance = BigDecimal.ZERO;
    	}
    	return availableBalance.add(banBalance).add(pawnBalance).add(otherBlockBalance);
    }
    
    @Transient
	private String descriptionHolder;
	
	@Transient
	private Issuer issuer;
	
	@Transient
	private Security security;

	@Transient
	private Boolean blCheck;

	@Transient
	private CorporativeOperation corporativeOperation;

	@Transient
	private BigDecimal amountRate;
	
	@Transient
	private BigDecimal cashPrice;

	@Transient
	private Participant participantBuyer;

	@Transient
	private BigDecimal totalBalanceAnnotation;
    
	@Transient
	private String annotationFromTo;
	
    /**
     * Instantiates a new retirement detail.
     */
    public RetirementDetail() {
    	availableBalance = BigDecimal.ZERO;
    	banBalance = BigDecimal.ZERO;
    	pawnBalance = BigDecimal.ZERO;
    	otherBlockBalance = BigDecimal.ZERO;
    }

	/**
	 * Gets the id retirement detail pk.
	 *
	 * @return the id retirement detail pk
	 */
	public Long getIdRetirementDetailPk() {
		return this.idRetirementDetailPk;
	}

	/**
	 * Sets the id retirement detail pk.
	 *
	 * @param idRetirementDetailPk the new id retirement detail pk
	 */
	public void setIdRetirementDetailPk(Long idRetirementDetailPk) {
		this.idRetirementDetailPk = idRetirementDetailPk;
	}


	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public String getDescriptionHolder() {
		return descriptionHolder;
	}

	public void setDescriptionHolder(String descriptionHolder) {
		this.descriptionHolder = descriptionHolder;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return this.holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the retirement operation.
	 *
	 * @return the retirement operation
	 */
	public RetirementOperation getRetirementOperation() {
		return this.retirementOperation;
	}

	/**
	 * Sets the retirement operation.
	 *
	 * @param retirementOperation the new retirement operation
	 */
	public void setRetirementOperation(RetirementOperation retirementOperation) {
		this.retirementOperation = retirementOperation;
	}
	
	/**
	 * Gets the retirement marketfacts.
	 *
	 * @return the retirement marketfacts
	 */
	public List<RetirementMarketfact> getRetirementMarketfacts() {
		return retirementMarketfacts;
	}

	/**
	 * Sets the retirement marketfacts.
	 *
	 * @param retirementMarketfacts the new retirement marketfacts
	 */
	public void setRetirementMarketfacts(
			List<RetirementMarketfact> retirementMarketfacts) {
		this.retirementMarketfacts = retirementMarketfacts;
	}
	
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub

		if (loggerUser != null) {
            this.lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            this.lastModifyDate = loggerUser.getAuditTime();
            this.lastModifyIp = loggerUser.getIpAddress();
            this.lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
        HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("retirementMarketfacts", retirementMarketfacts);
        return detailsMap;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public List<RetirementBlockDetail> getRetirementBlockDetails() {
		return retirementBlockDetails;
	}

	public void setRetirementBlockDetails(
			List<RetirementBlockDetail> retirementBlockDetails) {
		this.retirementBlockDetails = retirementBlockDetails;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Boolean getBlCheck() {
		return blCheck;
	}

	public void setBlCheck(Boolean blCheck) {
		this.blCheck = blCheck;
	}

	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	public BigDecimal getCashPrice() {
		return cashPrice;
	}

	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	public Participant getParticipantBuyer() {
		return participantBuyer;
	}

	public void setParticipantBuyer(Participant participantBuyer) {
		this.participantBuyer = participantBuyer;
	}

	public BigDecimal getAmountRate() {
		return amountRate;
	}

	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}

	public BigDecimal getTotalBalanceAnnotation() {
		return totalBalanceAnnotation;
	}

	public void setTotalBalanceAnnotation(BigDecimal totalBalanceAnnotation) {
		this.totalBalanceAnnotation = totalBalanceAnnotation;
	}

	public String getAnnotationFromTo() {
		return annotationFromTo;
	}

	public void setAnnotationFromTo(String annotationFromTo) {
		this.annotationFromTo = annotationFromTo;
	}

	@Override
	public String toString() {
		return "RetirementDetail [idRetirementDetailPk=" + idRetirementDetailPk + ", availableBalance="
				+ availableBalance + ", pawnBalance=" + pawnBalance + ", otherBlockBalance=" + otherBlockBalance
				+ ", banBalance=" + banBalance + ", totalBalance= "+getTotalBalance()+"]";
	}
	
}