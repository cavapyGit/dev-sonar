package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum PetitionerType {
	/** The systems. */
	PARTICIPANT(Integer.valueOf(224),"DEPOSITANTE"),
	HOLDER(Integer.valueOf(440),"TITULAR"),
	INHERITOR(Integer.valueOf(441),"HEREDERO"),
	ATTORNEY(Integer.valueOf(442),"APODERADO"),
	CREDITOR(Integer.valueOf(443),"ACREEDOR"),
	ISSUER(Integer.valueOf(1737),"EMISOR");
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<PetitionerType> list = new ArrayList<PetitionerType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, PetitionerType> lookup = new HashMap<Integer, PetitionerType>();

	static {
		for (PetitionerType s : EnumSet.allOf(PetitionerType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PetitionerType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return null;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static PetitionerType get(Integer code) {
		return lookup.get(code);
	}
}
