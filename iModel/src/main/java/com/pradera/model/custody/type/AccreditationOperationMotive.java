package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AccreditationOperationMotive {
	
	ECONOMIC(new Integer(2021),"DERECHOS ECONOMICOS"),
	POLITICAL(new Integer(2022),"DERECHOS POLITICOS"),
	PROPERTY(new Integer(2023),"DERECHO DE PROPIEDAD"),
	EARLY_REDEMPTION(new Integer(2024),"REDENCION ANTICIPADA");
	
	private Integer code;
	private String value;
	public static final List<AccreditationOperationMotive> list = new ArrayList<AccreditationOperationMotive>();
	
	public static final Map<Integer, AccreditationOperationMotive> lookup = new HashMap<Integer, AccreditationOperationMotive>();
	public static List<AccreditationOperationMotive> listSomeElements(AccreditationOperationMotive... transferSecuritiesTypeParams){
		List<AccreditationOperationMotive> retorno = new ArrayList<AccreditationOperationMotive>();
		for(AccreditationOperationMotive TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (AccreditationOperationMotive s : EnumSet.allOf(AccreditationOperationMotive.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private AccreditationOperationMotive(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
