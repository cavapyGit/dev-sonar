package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ParticipantUnionOperationRejectMotiveType {
	ORDER_OF_THE_SUPERINTENDENT(new Integer(1340),"ORDER OF THE SUPERINTENDENT"),
	INCOMPLETE_DOCUMENTS(new Integer(1341),"INCOMPLETE DOCUMENTS"),
	COURT_ORDER(new Integer(1342),"COURT ORDER"),
	OTHER(new Integer(1343),"OTHER");
	private Integer code;
	private String value;
	public static final List<ParticipantUnionOperationRejectMotiveType> list = new ArrayList<ParticipantUnionOperationRejectMotiveType>();
	
	public static final Map<Integer, ParticipantUnionOperationRejectMotiveType> lookup = new HashMap<Integer, ParticipantUnionOperationRejectMotiveType>();
	public static List<AccreditationPaymentWayType> listSomeElements(AccreditationPaymentWayType... transferSecuritiesTypeParams){
		List<AccreditationPaymentWayType> retorno = new ArrayList<AccreditationPaymentWayType>();
		for(AccreditationPaymentWayType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ParticipantUnionOperationRejectMotiveType s : EnumSet.allOf(ParticipantUnionOperationRejectMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private ParticipantUnionOperationRejectMotiveType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
