package com.pradera.model.custody.rectification;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.issuancesecuritie.Security;

/**
 * Entity implementation class for Entity: RectificationDetail
 *
 */
@Entity
@Table(name="RECTIFICATION_DETAIL")
public class RectificationDetail implements Serializable,Auditable {

	private static final long serialVersionUID = 1L;


	@Id
	@SequenceGenerator(name="RECTIFICATION_REQUESTPK_GENERATOR", sequenceName="SQ_ID_RECTIFICACION_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RECTIFICATION_REQUESTPK_GENERATOR")
	@Column(name="ID_RECTIFICATION_DETAIL_PK")
	private Long idRectificationDetailPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RECTIFICATION_OPERATION_FK")
	private RectificationOperation rectificationOperation;

	//bi-directional many-to-one association to holderAccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	//bi-directional many-to-one association to holderAccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

	//bi-directional many-to-one association to holderAccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="QUANTITY")
	private BigDecimal quantity;

	@Column(name="IND_BLOCK_DETAIL")
	private Integer indBlockDetail = ComponentConstant.ZERO;
	
	@Column(name="IND_SOURCE_DESTINATION")
	private Long indSourceDestination;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	//bi-directional many-to-one association to BlockOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;
    
	//bi-directional many-to-one association to BlockOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RECTIFICATION_DETAIL_FK")
	private RectificationDetail refRectificationDetail;
    
    @OneToMany(mappedBy="refRectificationDetail",fetch=FetchType.LAZY)
    private List<RectificationDetail> rectificationDetails;
    
    @OneToMany(mappedBy="rectificationDetail",fetch=FetchType.LAZY)
    private List<RectificationMarketFact> rectificationMarketFacts;
	
    @Transient
    private Long certificateNumber;
    
    @Transient
    private String indSourceDestinationDescription;
    
    public RectificationDetail getRefRectificationDetail() {
		return refRectificationDetail;
	}

	public void setRefRectificationDetail(RectificationDetail refRectificatioDetail) {
		this.refRectificationDetail = refRectificatioDetail;
	}

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	


	public RectificationOperation getRectificationOperation() {
		return rectificationOperation;
	}
	public void setRectificationOperation(
			RectificationOperation rectificationOperation) {
		this.rectificationOperation = rectificationOperation;
	}
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	public Security getSecurities() {
		return securities;
	}
	public void setSecurities(Security securities) {
		this.securities = securities;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public Long getIndSourceDestination() {
		return indSourceDestination;
	}
	public void setIndSourceDestination(Long indSourceDestination) {
		this.indSourceDestination = indSourceDestination;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public RectificationDetail() {
		super();
	}   
	public Long getIdRectificationDetailPk() {
		return this.idRectificationDetailPk;
	}

	public void setIdRectificationDetailPk(Long idRectificationDetailPk) {
		this.idRectificationDetailPk = idRectificationDetailPk;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {
	    if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser =loggerUser.getUserName();           
        }		
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public String getIndSourceDestinationDescription() {
		if(indSourceDestination.equals(ComponentConstant.SOURCE)){
			indSourceDestinationDescription = "ORIGEN";
		}
		else if(indSourceDestination.equals(ComponentConstant.TARGET)){
			indSourceDestinationDescription = "DESTINO";
		}
		
		return indSourceDestinationDescription;
	}

	public void setIndSourceDestinationDescription(
			String indSourceDestinationDescription) {
		this.indSourceDestinationDescription = indSourceDestinationDescription;
	}

	public List<RectificationDetail> getRectificationDetails() {
		return rectificationDetails;
	}

	public void setRectificationDetails(
			List<RectificationDetail> rectificationDetails) {
		this.rectificationDetails = rectificationDetails;
	}

	public List<RectificationMarketFact> getRectificationMarketFacts() {
		return rectificationMarketFacts;
	}

	public void setRectificationMarketFacts(
			List<RectificationMarketFact> rectificationMarketFacts) {
		this.rectificationMarketFacts = rectificationMarketFacts;
	}

	public Integer getIndBlockDetail() {
		return indBlockDetail;
	}

	public void setIndBlockDetail(Integer indBlockDetail) {
		this.indBlockDetail = indBlockDetail;
	}
		
}
