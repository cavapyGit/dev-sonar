package com.pradera.model.custody.blockoperation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="UNBLOCK_REQUEST_FILE")
public class UnblockRequestFile implements Serializable, Auditable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="UNBLOCK_REQUEST_FILE_IDUNBLOCKREQUESTFILEPK_GENERATOR", sequenceName="SQ_ID_UNBLOCK_REQUEST_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UNBLOCK_REQUEST_FILE_IDUNBLOCKREQUESTFILEPK_GENERATOR")
	@Column(name="ID_UNBLOCK_REQUEST_FILE_PK")
	private Long idUnblockRequestFilePk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_UNBLOCK_REQUEST_FK", referencedColumnName="ID_UNBLOCK_REQUEST_PK")
	private UnblockRequest unblockRequest;
	
	@Column(name="FILENAME")
	private String fileName;
	
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;
		
	@Lob()
	@Column(name="DOCUMENT_FILE")
	private byte[] documentFile;
	
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	public void setAudit(LoggerUser loggerUser) {
		if(Validations.validateIsNotNullAndNotEmpty(loggerUser)){
			this.lastModifyUser = loggerUser.getUserName();
			this.lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			this.lastModifyDate = loggerUser.getAuditTime();
			this.lastModifyIp = loggerUser.getIpAddress();
		}
		
	}
	
	public Long getIdUnblockRequestFilePk() {
		return idUnblockRequestFilePk;
	}
	public void setIdUnblockRequestFilePk(Long idUnblockRequestFilePk) {
		this.idUnblockRequestFilePk = idUnblockRequestFilePk;
	}
	public UnblockRequest getUnblockRequest() {
		return unblockRequest;
	}
	public void setUnblockRequest(UnblockRequest unblockRequest) {
		this.unblockRequest = unblockRequest;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getDocumentFile() {
		return documentFile;
	}
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}
	public String getRegistryUser() {
		return registryUser;
	}
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	public Date getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	
	
	public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {		
		return null;
	}
	
}
