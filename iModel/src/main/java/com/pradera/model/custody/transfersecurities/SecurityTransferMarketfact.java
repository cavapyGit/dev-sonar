package com.pradera.model.custody.transfersecurities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SECURITY_TRANSFER_OPERATION database table.
 * 
 */
@Entity
@Table(name="SECURITY_TRANSFER_MARKETFACT")
public class SecurityTransferMarketfact implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id transfer marketfact pk. */
	@Id
	@SequenceGenerator(name="CUSTODY_OPERATION_IDCUSTODYOPERATIONPK_GENERATOR", sequenceName="SQ_ID_CUSTODY_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CUSTODY_OPERATION_IDCUSTODYOPERATIONPK_GENERATOR")//ACORDARME PEDIR LA SECUENCIAS PARA ESTA TABLA
	@Column(name="ID_SEC_TRANSFER_MARKETFACT_PK")
	private Long IdSecTansferMarketfactPk;
	
	/** The security transfer operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRANSFER_OPERATION_FK")
	private SecurityTransferOperation securityTransferOperation;

	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;

    /** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	/** The quantity operation. */
	@Column(name="QUANTITY_OPERATION")
	private BigDecimal quantityOperation;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Transient
    private Integer correlativeOperation;

	
	public Long getIdSecTansferMarketfactPk() {
		return IdSecTansferMarketfactPk;
	}

	public void setIdSecTansferMarketfactPk(Long idSecTansferMarketfactPk) {
		IdSecTansferMarketfactPk = idSecTansferMarketfactPk;
	}

	public SecurityTransferOperation getSecurityTransferOperation() {
		return securityTransferOperation;
	}

	public void setSecurityTransferOperation(
			SecurityTransferOperation securityTransferOperation) {
		this.securityTransferOperation = securityTransferOperation;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getQuantityOperation() {
		return quantityOperation;
	}

	public void setQuantityOperation(BigDecimal quantityOperation) {
		this.quantityOperation = quantityOperation;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getCorrelativeOperation() {
		return correlativeOperation;
	}

	public void setCorrelativeOperation(Integer correlativeOperation) {
		this.correlativeOperation = correlativeOperation;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}
}