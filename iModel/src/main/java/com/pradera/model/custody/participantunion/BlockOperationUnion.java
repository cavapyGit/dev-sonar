package com.pradera.model.custody.participantunion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;


/**
 * The persistent class for the BLOCK_OPERACION_UNION database table.
 * 
 */
@Entity
@Table(name="BLOCK_OPERATION_UNION")
public class BlockOperationUnion implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BLOCK_OPERATION_UNION_GENERATOR", sequenceName="SQ_ID_BLOCK_OPER_UNION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_OPERATION_UNION_GENERATOR")
	@Column(name="ID_BLOCK_OPERATION_UNION_PK")
	private Long idBlockOperationUnionPk;

	//bi-directional many-to-one association to blockOperationDetail
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="ORIGINAL_BLOCK_BALANCE")
	private BigDecimal originalBlockBalance;

	//bi-directional many-to-one association to ParticipantUnionOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PART_UNION_OPERATION_FK")
	private ParticipantUnionOperation participantUnionOperation;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_REF_BLOCK_OPERATION_FK")
    private BlockOperation refBlockOperation;
    
    public BlockOperationUnion() {
    }
    
    /**
	 * @return the refBlockOperation
	 */
	public BlockOperation getRefBlockOperation() {
		return refBlockOperation;
	}

	/**
	 * @param refBlockOperation the refBlockOperation to set
	 */
	public void setRefBlockOperation(BlockOperation refBlockOperation) {
		this.refBlockOperation = refBlockOperation;
	}

	public Long getIdBlockOperationUnionPk() {
		return this.idBlockOperationUnionPk;
	}

	public void setIdBlockOperationUnionPk(Long idBlockOperationUnionPk) {
		this.idBlockOperationUnionPk = idBlockOperationUnionPk;
	}

	

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getOriginalBlockBalance() {
		return this.originalBlockBalance;
	}

	public void setOriginalBlockBalance(BigDecimal originalBlockBalance) {
		this.originalBlockBalance = originalBlockBalance;
	}

	public ParticipantUnionOperation getParticipantUnionOperation() {
		return this.participantUnionOperation;
	}

	public void setParticipantUnionOperation(ParticipantUnionOperation participantUnionOperation) {
		this.participantUnionOperation = participantUnionOperation;
	}

	public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
        }
    }


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}