package com.pradera.model.custody.affectation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AffectationFormType {
	EXACT_AMOUNT(new Integer(203)),
	CASH_VALUATION(new Integer(1173));
	
	private Integer code;
	
	private AffectationFormType(Integer code){
		this.code = code;
	}
		
	/** The Constant list. */
	public static final List<AffectationFormType> list = new ArrayList<AffectationFormType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AffectationFormType> lookup = new HashMap<Integer, AffectationFormType>();
	
	static {
		for (AffectationFormType s : EnumSet.allOf(AffectationFormType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static AffectationFormType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}
