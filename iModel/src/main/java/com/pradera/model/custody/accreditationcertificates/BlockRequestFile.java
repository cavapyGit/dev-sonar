package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="BLOCK_REQUEST_FILE")
@NamedQueries({
	@NamedQuery(name = BlockRequestFile.FIND_DATA_AND_INFO_BY_FK, query = " select new BlockRequestFile(brf.idBlockRequestFilePk,brf.fileName,brf.documentFile,brf.documentType) from BlockRequestFile brf where brf.blockRequest.idBlockRequestPk = :idBlockRequestPk"),
})
public class BlockRequestFile implements Serializable, Auditable{
	
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_DATA_AND_INFO_BY_FK = "findDataAndInfoByFk";
	
	@Id
	@SequenceGenerator(name="BLOCK_REQUEST_FILE_IDBLOCKREQUESTFILEPK_GENERATOR", sequenceName="SQ_ID_BLOCK_REQUEST_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_REQUEST_FILE_IDBLOCKREQUESTFILEPK_GENERATOR")
	@Column(name="ID_BLOCK_REQUEST_FILE_PK")
	private Long idBlockRequestFilePk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_REQUEST_FK", referencedColumnName="ID_BLOCK_REQUEST_PK")
	private BlockRequest blockRequest;
	
	@Column(name="FILENAME")
	private String fileName;
	
	@Lob()
	@Column(name="DOCUMENT_FILE")
	@Basic(fetch=FetchType.LAZY)
	private byte[] documentFile;
	
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;
	
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	public BlockRequestFile() {
		// TODO Auto-generated constructor stub
	}
	
	public BlockRequestFile(Long idBlockRequestFilePk, String fileName,
			byte[] documentFile, Integer documentType) {
		super();
		this.idBlockRequestFilePk = idBlockRequestFilePk;
		this.fileName = fileName;
		this.documentFile = documentFile;
		this.documentType = documentType;
	}

	public void setAudit(LoggerUser loggerUser) {
		if(Validations.validateIsNotNullAndNotEmpty(loggerUser)){
			this.lastModifyUser = loggerUser.getUserName();
			this.lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			this.lastModifyDate = loggerUser.getAuditTime();
			this.lastModifyIp = loggerUser.getIpAddress();	
		}
	}
	
	public Long getIdBlockRequestFilePk() {
		return idBlockRequestFilePk;
	}
	public void setIdBlockRequestFilePk(Long idBlockRequestFilePk) {
		this.idBlockRequestFilePk = idBlockRequestFilePk;
	}
	public BlockRequest getBlockRequest() {
		return blockRequest;
	}
	public void setBlockRequest(BlockRequest blockRequest) {
		this.blockRequest = blockRequest;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getDocumentFile() {
		return documentFile;
	}
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}
	public String getRegistryUser() {
		return registryUser;
	}
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	public Date getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	
	public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {		
		return null;
	}	
	
	
}
