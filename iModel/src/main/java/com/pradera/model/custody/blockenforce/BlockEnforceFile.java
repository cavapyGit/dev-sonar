package com.pradera.model.custody.blockenforce;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the BLOCK_ENFORCE_FILE database table.
 * 
 */
@Entity
@Table(name="BLOCK_ENFORCE_FILE")
@NamedQuery(name="BlockEnforceFile.findAll", query="SELECT b FROM BlockEnforceFile b")
public class BlockEnforceFile implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BLOCK_ENFORCE_FILE_IDENFORCEFILEPK_GENERATOR", sequenceName="SQ_ID_ENFORCE_FILE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_ENFORCE_FILE_IDENFORCEFILEPK_GENERATOR")
	@Column(name="ID_ENFORCE_FILE_PK")
	private long idEnforceFilePk;

	@Basic(fetch=FetchType.LAZY)
	@Lob()
	@Column(name="ENFORCE_FILE")
	private byte[] enforceFile;

	@Column(name="FILE_NAME")
	private String fileName;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to BlockEnforceOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ENFORCE_OPERATION_FK")
	private BlockEnforceOperation blockEnforceOperation;
	
	@Transient
	private Long fileSize;

	public BlockEnforceFile() {
	}

	public long getIdEnforceFilePk() {
		return this.idEnforceFilePk;
	}

	public void setIdEnforceFilePk(long idEnforceFilePk) {
		this.idEnforceFilePk = idEnforceFilePk;
	}

	public byte[] getEnforceFile() {
		return this.enforceFile;
	}

	public void setEnforceFile(byte[] enforceFile) {
		this.enforceFile = enforceFile;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BlockEnforceOperation getBlockEnforceOperation() {
		return this.blockEnforceOperation;
	}

	public void setBlockEnforceOperation(BlockEnforceOperation blockEnforceOperation) {
		this.blockEnforceOperation = blockEnforceOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
}