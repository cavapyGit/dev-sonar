package com.pradera.model.custody.affectation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * The Enum LevelAffectationType.
 */
public enum LevelAffectationType {
	
	/** The block. */
	BLOCK(new Integer(1119),"BLOQUEO"),
	
	/** The reblock. */
	REBLOCK(new Integer(1120),"REBLOQUEO");
	
	/** The code. */
	private Integer code;
	
	private String value;
	
	/**
	 * Instantiates a new level affectation type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private LevelAffectationType(Integer code,String value) {
		this.code = code;	
		this.value = value;	
	}

	/** The Constant list. */
	public static final List<LevelAffectationType> list = new ArrayList<LevelAffectationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, LevelAffectationType> lookup = new HashMap<Integer, LevelAffectationType>();
	
	static {
		for (LevelAffectationType s : EnumSet.allOf(LevelAffectationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the level affectation type
	 */
	public static LevelAffectationType get(Integer code) {
		return lookup.get(code);
	}
	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
