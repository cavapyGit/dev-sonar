package com.pradera.model.custody.reversals.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum ReversalsActionType {
	REVERSALS(new Integer(1)),	
	SEARCH_REQUEST(new Integer(2));
	
	private Integer code;
	
	private ReversalsActionType(Integer code) {
		this.code = code;
	}
	
	public static final List<ReversalsActionType> list = new ArrayList<ReversalsActionType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ReversalsActionType> lookup = new HashMap<Integer, ReversalsActionType>();
	
	static {
		for (ReversalsActionType s : EnumSet.allOf(ReversalsActionType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
