/**@author nmolina
 * 
 */

package com.pradera.model.custody.coupongrant.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CouponTransferStateType {
		
	REGISTERED(new Integer(1719),"REGISTRADO"),
	REJECTED(new Integer(1721),"RECHAZADO"),
	CONFIRMED(new Integer(1720),"CONFIRMADO");
	
	private Integer code;
	private String value;
	
	public static final List<CouponTransferStateType> list = new ArrayList<CouponTransferStateType>();
	
	public static final Map<Integer, CouponTransferStateType> lookup = new HashMap<Integer, CouponTransferStateType>();
	
	public static List<CouponTransferStateType> listSomeElements(CouponTransferStateType... transferSecuritiesTypeParams){
		List<CouponTransferStateType> retorno = new ArrayList<CouponTransferStateType>();
		for(CouponTransferStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (CouponTransferStateType s : EnumSet.allOf(CouponTransferStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private CouponTransferStateType(Integer ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}