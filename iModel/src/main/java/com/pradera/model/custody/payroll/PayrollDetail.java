
package com.pradera.model.custody.payroll;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.physical.CertificateOperation;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;

@Entity
@Table(name="PAYROLL_DETAIL")
public class PayrollDetail implements Serializable, Auditable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="PAYROLL_HEADER_IDPAYROLLDETAILPK_GENERATOR", sequenceName="SQ_PAYROLL_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAYROLL_HEADER_IDPAYROLLDETAILPK_GENERATOR")
	@Column(name="ID_PAYROLL_DETAIL_PK")
	private Long idPayrollDetailPk;
	
	@ManyToOne
	@JoinColumn(name="ID_PAYROLL_HEADER_FK")
	private PayrollHeader payrollHeader;
	
	@ManyToOne
	@JoinColumn(name="ID_ANNOTATION_OPERATION_FK")
	private AccountAnnotationOperation accountAnnotationOperation;
	
	@ManyToOne
	@JoinColumn(name="ID_ANNOTATION_CUPON_FK")
	private AccountAnnotationCupon accountAnnotationCupon;

	@ManyToOne
	@JoinColumn(name="ID_RETIREMENT_DETAIL_FK")
	private RetirementDetail retirementDetail;
	
	@ManyToOne
	@JoinColumn(name="ID_RETIREMENT_OPERATION_FK")
	private RetirementOperation retirementOperation;
	
	@ManyToOne
	@JoinColumn(name="ID_CERTIFICATE_OPERATION_FK")
	private CertificateOperation certificateOperation;
	
	@ManyToOne
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;
	
	@ManyToOne
	@JoinColumn(name="ID_EMPLOYEE_FK")
	private Employees employees;
	
	@ManyToOne
	@JoinColumn(name="ID_SECOND_EMPLOYEE_FK")
	private Employees secondEmployees;
	
	@Column(name="PAYROLL_STATE")
	private Integer payrollState;
	
	@Column(name="FILENAME")
	private String fileName;
	
	@Column(name="PAYROLL_QUANTITY")
	private BigDecimal payrollQuantity;
	
	@Lob()
	@Column(name="DOCUMENT_FILE")
	@Basic(fetch=FetchType.LAZY)
	private byte[] documentFile;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="BRANCH_OFFICE")
	private Integer branchOffice;

	@Column(name="IND_CUPON")
	private Integer indCupon = 0;
	
	@Column(name="CUPON_NUMBER")
	private Integer cuponNumber;
	
	@Column(name="SERIAL_CODE")
	private String serialCode;
	
	@Column(name="CURRENT_VAULT")
	private Integer currentVault;
	
	@Column(name="IND_SECURITY_CREATED")
	private Integer indSecurityCreated;
	
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;

	@Column(name="REJECT_MOTIVE_OTHER")
	private String rejectMotiveOther;
	
	@Column(name="IND_REENTER")
	private Integer indReenter;

    @Temporal(TemporalType.DATE)
	@Column(name="LAST_REENTER_DATE")
	private Date lastReenterDate;
	
	@Transient
	private boolean blSelected;
	
	public Long getIdPayrollDetailPk() {
		return idPayrollDetailPk;
	}
	public void setIdPayrollDetailPk(Long idPayrollDetailPk) {
		this.idPayrollDetailPk = idPayrollDetailPk;
	}
	public PayrollHeader getPayrollHeader() {
		return payrollHeader;
	}
	public void setPayrollHeader(PayrollHeader payrollHeader) {
		this.payrollHeader = payrollHeader;
	}
	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}
	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}
	public Integer getPayrollState() {
		return payrollState;
	}
	public void setPayrollState(Integer payrollState) {
		this.payrollState = payrollState;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}	
	public boolean isBlSelected() {
		return blSelected;
	}
	public void setBlSelected(boolean blSelected) {
		this.blSelected = blSelected;
	}	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getDocumentFile() {
		return documentFile;
	}
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}
	public Employees getEmployees() {
		return employees;
	}
	public void setEmployees(Employees employees) {
		this.employees = employees;
	}
	public Employees getSecondEmployees() {
		return secondEmployees;
	}
	public void setSecondEmployees(Employees secondEmployees) {
		this.secondEmployees = secondEmployees;
	}
	public Integer getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}
	public BigDecimal getPayrollQuantity() {
		return payrollQuantity;
	}
	public void setPayrollQuantity(BigDecimal payrollQuantity) {
		this.payrollQuantity = payrollQuantity;
	}
	public AccountAnnotationOperation getAccountAnnotationOperation() {
		return accountAnnotationOperation;
	}
	public void setAccountAnnotationOperation(
			AccountAnnotationOperation accountAnnotationOperation) {
		this.accountAnnotationOperation = accountAnnotationOperation;
	}
	public RetirementOperation getRetirementOperation() {
		return retirementOperation;
	}
	public void setRetirementOperation(RetirementOperation retirementOperation) {
		this.retirementOperation = retirementOperation;
	}	
	public CertificateOperation getCertificateOperation() {
		return certificateOperation;
	}
	public void setCertificateOperation(CertificateOperation certificateOperation) {
		this.certificateOperation = certificateOperation;
	}
	public Integer getIndCupon() {
		return indCupon;
	}
	public void setIndCupon(Integer indCupon) {
		this.indCupon = indCupon;
	}
	public Integer getCuponNumber() {
		return cuponNumber;
	}
	public void setCuponNumber(Integer cuponNumber) {
		this.cuponNumber = cuponNumber;
	}
	public String getSerialCode() {
		return serialCode;
	}
	public void setSerialCode(String serialCode) {
		this.serialCode = serialCode;
	}
	public Integer getCurrentVault() {
		return currentVault;
	}
	public void setCurrentVault(Integer current_vault) {
		this.currentVault = current_vault;
	}
	public Integer getIndSecurityCreated() {
		return indSecurityCreated;
	}
	public void setIndSecurityCreated(Integer indSecurityCreated) {
		this.indSecurityCreated = indSecurityCreated;
	}
	public Integer getRejectMotive() {
		return rejectMotive;
	}
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}
	public Integer getIndReenter() {
		return indReenter;
	}
	public void setIndReenter(Integer indReenter) {
		this.indReenter = indReenter;
	}
	public Date getLastReenterDate() {
		return lastReenterDate;
	}
	public void setLastReenterDate(Date lastReenterDate) {
		this.lastReenterDate = lastReenterDate;
	}
	public AccountAnnotationCupon getAccountAnnotationCupon() {
		return accountAnnotationCupon;
	}
	public void setAccountAnnotationCupon(AccountAnnotationCupon accountAnnotationCupon) {
		this.accountAnnotationCupon = accountAnnotationCupon;
	}
	public RetirementDetail getRetirementDetail() {
		return retirementDetail;
	}
	public void setRetirementDetail(RetirementDetail retirementDetail) {
		this.retirementDetail = retirementDetail;
	}
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null){
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
