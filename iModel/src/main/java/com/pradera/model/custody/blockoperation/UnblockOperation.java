package com.pradera.model.custody.blockoperation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.blockenforce.BlockEnforceDetail;
import com.pradera.model.custody.transfersecurities.CustodyOperation;


/**
 * The persistent class for the UNBLOCK_OPERATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="UNBLOCK_OPERATION")
public class UnblockOperation implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id	
	private Long idUnblockOperationPk;

	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_UNBLOCK_OPERATION_PK")
	private CustodyOperation custodyOperation;
	
	
	//bi-directional many-to-one association to blockOperationDetail
    @ManyToOne
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="UNBLOCK_QUANTITY")
	private BigDecimal unblockQuantity;
	
	@Column(name="DOCUMENT_NUMBER")
	private Long documentNumber;
	
	@Column(name="UNBLOCK_STATE")
	private Integer unblockState;

	//bi-directional many-to-one association to UnblockOperation
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_UNBLOCK_REQUEST_FK",referencedColumnName="ID_UNBLOCK_REQUEST_PK")
	private UnblockRequest unblockRequest;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_ENFORCE_DETAIL_FK",referencedColumnName="ID_ENFORCE_DETAIL_PK")
    private BlockEnforceDetail blockEnforceDetail;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="unblockOperation", fetch=FetchType.LAZY)
    private List<UnblockMarketFactOperation> unblockMarketFactOperations;

    public UnblockOperation() {
    	
    }

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public UnblockRequest getUnblockRequest() {
		return unblockRequest;
	}

	public void setUnblockRequest(UnblockRequest unblockRequest) {
		this.unblockRequest = unblockRequest;
	}

	public Long getIdUnblockOperationPk() {
		return idUnblockOperationPk;
	}

	public void setIdUnblockOperationPk(Long idUnblockOperationPk) {
		this.idUnblockOperationPk = idUnblockOperationPk;
	}

	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public Long getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(Long documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Integer getUnblockState() {
		return unblockState;
	}

	public void setUnblockState(Integer unblockState) {
		this.unblockState = unblockState;
	}

	public BigDecimal getUnblockQuantity() {
		return unblockQuantity;
	}

	public void setUnblockQuantity(BigDecimal unblockQuantity) {
		this.unblockQuantity = unblockQuantity;
	}
	
	public List<UnblockMarketFactOperation> getUnblockMarketFactOperations() {
		return unblockMarketFactOperations;
	}

	public void setUnblockMarketFactOperations(
			List<UnblockMarketFactOperation> unblockMarketFactOperations) {
		this.unblockMarketFactOperations = unblockMarketFactOperations;
	}

	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(custodyOperation != null){
            	custodyOperation.setAudit(loggerUser);
            }
            if(blockOperation != null){
            	blockOperation.setAudit(loggerUser);
            }
        }
    }

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("unblockMarketFactOperations", unblockMarketFactOperations);
        return detailsMap;
	}

	public BlockEnforceDetail getBlockEnforceDetail() {
		return blockEnforceDetail;
	}

	public void setBlockEnforceDetail(BlockEnforceDetail blockEnforceDetail) {
		this.blockEnforceDetail = blockEnforceDetail;
	}
	
}