package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PartUnionOperationMotiveType {
	FUSION(new Integer(198),""),	
	BUY_AND_SELL_CLIENTS(new Integer(809),""),	
	SUPERINTENDENT_ORDER(new Integer(810),""),
	OTHERS(new Integer(1438),"")
	;
	
	private Integer code;
	private String value;
	
	public static final List<PartUnionOperationMotiveType> list = new ArrayList<PartUnionOperationMotiveType>();
	
	public static final Map<Integer, PartUnionOperationMotiveType> lookup = new HashMap<Integer, PartUnionOperationMotiveType>();
	
	public static List<PartUnionOperationMotiveType> listSomeElements(PartUnionOperationMotiveType... transferSecuritiesTypeParams){
		List<PartUnionOperationMotiveType> retorno = new ArrayList<PartUnionOperationMotiveType>();
		for(PartUnionOperationMotiveType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (PartUnionOperationMotiveType s : EnumSet.allOf(PartUnionOperationMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	private PartUnionOperationMotiveType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
