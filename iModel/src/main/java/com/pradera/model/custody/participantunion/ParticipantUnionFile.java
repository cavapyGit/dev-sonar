package com.pradera.model.custody.participantunion;



import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.type.ParticipantUnionFileType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PARTICIPANT_FILE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@Entity
@Table(name="PARTICIPANT_UNION_FILE")
public class ParticipantUnionFile implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant file pk. */
	@Id
	@SequenceGenerator(name="sq_ID_PART_UNION_FILE_PK", sequenceName="sq_ID_PART_UNION_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sq_ID_PART_UNION_FILE_PK")
	@Column(name="ID_PARTICIPANT_UNION_FILE_PK")
	private Long idParticipantUnionFilePk;

	/** The description. */
	private String description;

    /** The file. */
    @Lob()
	@Column(name="DOCUMENT_FILE")
	private byte[] file;

	/** The filename. */
	private String filename;
	
	/** The participant. */	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PART_UNION_OPERATION_FK")    
	private ParticipantUnionOperation participantUnionOperation;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	private Integer stateFile;
	
	/** The request file type. */
	@Column(name="REQUEST_FILE_TYPE")
	private Integer requestFileType;
	
	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;
	
	@Transient
	private Integer index;
	
	@Transient
	private String fileTypeDescription;

	@Transient
	private Long fileSize;
	

	@Transient
	private String fileDocumentType;
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {		
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description != null) {
			description = description.toUpperCase();
		}
		this.description = description;
	}

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public byte[] getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 *
	 * @param file the new file
	 */
	public void setFile(byte[] file) {
		this.file = file;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public ParticipantUnionOperation getParticipant() {
		return participantUnionOperation;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setparticipantUnionOperation(ParticipantUnionOperation participant) {
		this.participantUnionOperation = participant;
	}


	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state file.
	 *
	 * @return the state file
	 */
	public Integer getStateFile() {
		return stateFile;
	}

	/**
	 * Sets the state file.
	 *
	 * @param stateFile the new state file
	 */
	public void setStateFile(Integer stateFile) {
		this.stateFile = stateFile;
	}

	/**
	 * Gets the request file type.
	 *
	 * @return the request file type
	 */
	public Integer getRequestFileType() {
		return requestFileType;
	}

	/**
	 * Sets the request file type.
	 *
	 * @param requestFileType the new request file type
	 */
	public void setRequestFileType(Integer requestFileType) {
		this.requestFileType = requestFileType;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}


	
	@Override
    public void setAudit(LoggerUser loggerUser) {        // 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	public Long getIdParticipantUnionFilePk() {
		return idParticipantUnionFilePk;
	}

	public void setIdParticipantUnionFilePk(Long idParticipantUnionFilePk) {
		this.idParticipantUnionFilePk = idParticipantUnionFilePk;
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public ParticipantUnionOperation getParticipantUnionOperation() {
		return participantUnionOperation;
	}

	public void setParticipantUnionOperation(
			ParticipantUnionOperation participantUnionOperation) {
		this.participantUnionOperation = participantUnionOperation;
	}

	public String getFileTypeDescription() {
		return fileTypeDescription;
	}

	public void setFileTypeDescription(String fileTypeDescription) {
		this.fileTypeDescription = fileTypeDescription;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileDocumentType() {
		return fileDocumentType;
	}

	public void setFileDocumentType(String fileDocumentType) {
		this.fileDocumentType = fileDocumentType;
	}

}