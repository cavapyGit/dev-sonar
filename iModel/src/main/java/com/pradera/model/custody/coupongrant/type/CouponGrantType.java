/**@author nmolina
 * 
 */

package com.pradera.model.custody.coupongrant.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CouponGrantType {
		
	GRANT(new Integer(1707),"CESION"),
	TRANSFER(new Integer(1708),"TRASPASO");
	
	private Integer code;
	private String value;
	public static final List<CouponGrantType> list = new ArrayList<CouponGrantType>();
	
	public static final Map<Integer, CouponGrantType> lookup = new HashMap<Integer, CouponGrantType>();
	
	public static List<CouponGrantType> listSomeElements(CouponGrantType... transferSecuritiesTypeParams){
		List<CouponGrantType> retorno = new ArrayList<CouponGrantType>();
		for(CouponGrantType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (CouponGrantType s : EnumSet.allOf(CouponGrantType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private CouponGrantType(Integer ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}