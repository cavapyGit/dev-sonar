package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SecuritiesTransferMotiveType {
	
	INCONSISTENT(Integer.valueOf(197),"INCONGRUENCIA DE DATOS"),
	
	EXCEEDED_TIME_ALLOWED(Integer.valueOf(1735),"SOLICITUD EXCEDIO TIEMPO PERMITIDO"),
	
	OTHERS(Integer.valueOf(1718),"OTROS");
	
	public static final List<SecuritiesTransferMotiveType> list = new ArrayList<SecuritiesTransferMotiveType>();
	public static final Map<Integer, SecuritiesTransferMotiveType> lookup = new HashMap<Integer, SecuritiesTransferMotiveType>();
	
	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private SecuritiesTransferMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	static {
        for (SecuritiesTransferMotiveType d : SecuritiesTransferMotiveType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
	
	public static SecuritiesTransferMotiveType get(Integer code) {
		return lookup.get(code);
	}
	
}