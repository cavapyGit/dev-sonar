package com.pradera.model.custody.payroll;

public enum ControlType {
	MICROFILMACION(new Integer(1)),
	COORDINACION(new Integer(2)),
	ANALISIS(new Integer(3)),
	;
		
	private Integer code;
	
	private ControlType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
