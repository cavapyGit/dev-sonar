package com.pradera.model.custody.payroll;

public enum PayrollHeaderStateType {
	GENERATE(new Integer(2713)),//2335
	ANNULATE(new Integer(2714)),//2336
	PROCESSED(new Integer(2728)),//2355
	FINISHED(new Integer(2729)),//2356
	REJECT(new Integer(2835)),
	;
	
	private Integer code;
	
	private PayrollHeaderStateType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	
}