package com.pradera.model.custody.physical;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "PHYSICAL_CERT_FILE")
@NamedQueries({
    @NamedQuery(name = "PhysicalCertFile.findAll", query = "SELECT p FROM PhysicalCertFile p"),
    @NamedQuery(name = "PhysicalCertFile.findByIdCertFilePk", query = "SELECT p FROM PhysicalCertFile p WHERE p.idCertFilePk = :idCertFilePk"),
    @NamedQuery(name = "PhysicalCertFile.findByDescription", query = "SELECT p FROM PhysicalCertFile p WHERE p.description = :description"),
    @NamedQuery(name = "PhysicalCertFile.findByFilename", query = "SELECT p FROM PhysicalCertFile p WHERE p.filename = :filename"),
    @NamedQuery(name = "PhysicalCertFile.findByRegistryUser", query = "SELECT p FROM PhysicalCertFile p WHERE p.registryUser = :registryUser"),
    @NamedQuery(name = "PhysicalCertFile.findByRegistryDate", query = "SELECT p FROM PhysicalCertFile p WHERE p.registryDate = :registryDate"),
    @NamedQuery(name = "PhysicalCertFile.findByLastModifyUser", query = "SELECT p FROM PhysicalCertFile p WHERE p.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "PhysicalCertFile.findByLastModifyDate", query = "SELECT p FROM PhysicalCertFile p WHERE p.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "PhysicalCertFile.findByLastModifyIp", query = "SELECT p FROM PhysicalCertFile p WHERE p.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "PhysicalCertFile.findByLastModifyApp", query = "SELECT p FROM PhysicalCertFile p WHERE p.lastModifyApp = :lastModifyApp")})
public class PhysicalCertFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_PHYSICAL_CERT_FILE", sequenceName="SQ_PHYSICAL_CERT_FILE",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_PHYSICAL_CERT_FILE")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CERT_FILE_PK", nullable = false)
    private Long idCertFilePk;
    @Column(name="DOCUMENT_TYPE")
	private Integer documentType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "DESCRIPTION", nullable = false, length = 150)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "FILENAME", nullable = false, length = 150)
    private String filename;
    @Lob
    @Column(name = "DOCUMENT_FILE")
    private Serializable documentFile;
    @Size(max = 20)
    @Column(name = "REGISTRY_USER", length = 20)
    private String registryUser;
    @Column(name = "REGISTRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private long lastModifyApp;
    @JoinColumn(name = "ID_PHYSICAL_CERTIFICATE_FK", referencedColumnName = "ID_PHYSICAL_CERTIFICATE_PK")
    @ManyToOne(fetch = FetchType.LAZY)
    private PhysicalCertificate idPhysicalCertificateFk;

    public PhysicalCertFile() {
    }

    public PhysicalCertFile(Long idCertFilePk) {
        this.idCertFilePk = idCertFilePk;
    }

    public PhysicalCertFile(Long idCertFilePk, String description, String filename, String lastModifyUser, Date lastModifyDate, String lastModifyIp, long lastModifyApp) {
        this.idCertFilePk = idCertFilePk;
        this.description = description;
        this.filename = filename;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdCertFilePk() {
        return idCertFilePk;
    }

    public void setIdCertFilePk(Long idCertFilePk) {
        this.idCertFilePk = idCertFilePk;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilename() {
        return filename;
    }

    public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public void setFilename(String filename) {
        this.filename = filename;
    }

    public Serializable getDocumentFile() {
        return documentFile;
    }

    public void setDocumentFile(Serializable documentFile) {
        this.documentFile = documentFile;
    }

    public String getRegistryUser() {
        return registryUser;
    }

    public void setRegistryUser(String registryUser) {
        this.registryUser = registryUser;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public PhysicalCertificate getIdPhysicalCertificateFk() {
        return idPhysicalCertificateFk;
    }

    public void setIdPhysicalCertificateFk(PhysicalCertificate idPhysicalCertificateFk) {
        this.idPhysicalCertificateFk = idPhysicalCertificateFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCertFilePk != null ? idCertFilePk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PhysicalCertFile)) {
            return false;
        }
        PhysicalCertFile other = (PhysicalCertFile) object;
        if ((this.idCertFilePk == null && other.idCertFilePk != null) || (this.idCertFilePk != null && !this.idCertFilePk.equals(other.idCertFilePk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.PhysicalCertFile[ idCertFilePk=" + idCertFilePk + " ]";
    }
    
}

