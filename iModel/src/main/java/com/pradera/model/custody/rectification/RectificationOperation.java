package com.pradera.model.custody.rectification;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.RectificationMotiveType;
import com.pradera.model.custody.type.RectificationOperationStateType;
import com.pradera.model.custody.type.RectificationOperationType;


/**
 * The persistent class for the RECTIFICATION_OPERATION database table.
 * 
 */
@Entity
@Table(name="RECTIFICATION_OPERATION")
public class RectificationOperation implements Serializable,Auditable {
	
	
	public static final String RECTIFICATION_OPERATION_STATE="rectification.searchState";
	private static final long serialVersionUID = 1L;

	@Id
	private Long idRectificationOperationPk;

	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_RECTIFICATION_OPERATION_PK")
	private CustodyOperation custodyOperation;	

	//bi-directional many-to-one association to holderAccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RECTIFIED_OPERATION_FK")
	private CustodyOperation rectifiedOperation;

	@Column(name="REQUEST_NUMBER")
	private Long requestNumber;

	@Column(name="REQUEST_MOTIVE")
	private Integer requestMotive;

	@Column(name="REQUEST_TYPE")
	private Integer requestType;

	@Column(name="REQUEST_STATE")
	private Integer requestState;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@OneToMany(mappedBy="rectificationOperation")
	private List<RectificationDetail> rectificationDetail;
	
	@Column(name="COMMENTS")
	private String otherMotive;

	public String getOtherMotive() {
		return otherMotive;
	}

	public void setOtherMotive(String otherMotive) {
		this.otherMotive = otherMotive;
	}

	public RectificationOperation(Long idRectificationOperationPk,
			Long requestNumber, Integer requestType, Integer requestState) {
		super();
		this.idRectificationOperationPk = idRectificationOperationPk;
	    //this.requestNumber = requestNumber;
		this.rectifiedOperation = new CustodyOperation(requestNumber);
		this.requestType = requestType;
		this.requestState = requestState;
	}



	@Transient
	private String requestTypeDescription;

	@Transient
	private String requestStateDescription;

	@Transient
	private BigDecimal retirementTotalBalance;

	public BigDecimal getRetirementTotalBalance() {
		BigDecimal sum = BigDecimal.ZERO;
		for(RectificationDetail detail: rectificationDetail){
			sum = sum.add(detail.getQuantity());
		}
		retirementTotalBalance = sum;
		
		return retirementTotalBalance;
	}

	public void setRetirementTotalBalance(BigDecimal retirementTotalBalance) {
		this.retirementTotalBalance = retirementTotalBalance;
	}

	public String getRequestTypeDescription() {
		if(requestType!=null){
			requestTypeDescription = RectificationOperationType.get(requestType).getValue();
		}
		else{
			requestTypeDescription = "";
		}
		return requestTypeDescription;
	}

	public void setRequestTypeDescription(String requestTypeDescription) {
		this.requestTypeDescription = requestTypeDescription;
	}

	public String getRequestStateDescription() {
		if(requestState!=null){
			requestStateDescription = RectificationOperationStateType.get(requestState).getValue();
		}
		else{
			requestStateDescription = "";
		}
		return requestStateDescription;
	}

	public void setRequestStateDescription(String requestStateDescription) {
		this.requestStateDescription = requestStateDescription;
	}

	public String getRequestMotiveDescription() {
		if(requestMotive!=null && requestMotive.equals(RectificationMotiveType.OTHERS.getCode())){
			requestMotiveDescription = otherMotive;
		}
		else if(requestMotive!=null && !requestMotive.equals(RectificationMotiveType.OTHERS.getCode())){
			requestMotiveDescription = RectificationMotiveType.get(requestMotive).getValue();
		}
		else{
			requestMotiveDescription = "";
		}
		return requestMotiveDescription;
	}

	public void setRequestMotiveDescription(String requestMotiveDescription) {
		this.requestMotiveDescription = requestMotiveDescription;
	}




	@Transient
	private String requestMotiveDescription;


	public RectificationOperation() {
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public Long getIdRectificationOperationPk() {
		return idRectificationOperationPk;
	}

	public void setIdRectificationOperationPk(Long idRectificationOperationPk) {
		this.idRectificationOperationPk = idRectificationOperationPk;
	}


	public CustodyOperation getRectifiedOperation() {
		return rectifiedOperation;
	}

	public void setRectifiedOperation(CustodyOperation rectifiedOperation) {
		this.rectifiedOperation = rectifiedOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}




	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser =loggerUser.getUserName();
			if(custodyOperation != null){
				custodyOperation.setAudit(loggerUser);
			}


		}

	}

	public Long getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}



	public Integer getRequestType() {
		return requestType;
	}

	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	public Integer getRequestState() {
		return requestState;
	}

	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}



	public Integer getRequestMotive() {
		return requestMotive;
	}

	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}


	public List<RectificationDetail> getRectificationDetail() {
		return rectificationDetail;
	}

	public void setRectificationDetail(List<RectificationDetail> rectificationDetail) {
		this.rectificationDetail = rectificationDetail;
	}

	public List<RectificationDetail> getRectificationDetailBySecurityCode(){
		List<RectificationDetail> returnRectificationDetail = new ArrayList<RectificationDetail>();
		Map<String,RectificationDetail> mapRectification = new HashMap<String, RectificationDetail>();

		for(RectificationDetail detail: getRectificationDetail()){
			if(detail.getIndSourceDestination().equals(ComponentConstant.TARGET)){
				mapRectification.put(detail.getSecurities().getIdSecurityCodePk(), detail);
			}
			if(detail.getIndSourceDestination().equals(ComponentConstant.SOURCE)){
				returnRectificationDetail.add(detail);
			}
		}
		
		for(Map.Entry<String, RectificationDetail> entry: mapRectification.entrySet()){
			returnRectificationDetail.add(entry.getValue());
		}	
		return returnRectificationDetail;

	}
	
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();    
		return detailsMap;
	}

}
