package com.pradera.model.custody.physical;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Security;

@Entity
@Table(name = "CERTIFICATE_OPERATION")
public class CertificateOperation implements Serializable, Auditable{

	private static final long serialVersionUID = 1L;

	@Id
	private Long idCertificateOperationPk;
	
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_CERTIFICATE_OPERATION_PK")
	private CustodyOperation custodyOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	@Column(name = "CERTIFICATE_OPERATION_TYPE")
	private Integer certificateOperationType;
	
	@Column(name = "BRANCH_OFFICE")
	private Integer branchOffice;
	
	@Column(name = "BRANCH_OFFICE_TARGET")
	private Integer branchOfficeTarget;
	
	@Column(name = "VAULT_LOCATION_TARGET")
	private Integer vaultLocationTarget;
	
	@Column(name = "FRACTION_TYPE")
	private Integer fractionType;
	
	@Column(name = "GLOBALIZE_QUANTITY")
	private BigDecimal globalizeQuantity;
	
	@Column(name = "OPERATION_STATE")
	private Integer operationState;
	
    @Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private String operationTypeDesc;
	@Transient
	private String operationStateDesc;
	@Transient
	private String vaultLocationTargetDesc;
	
	public Long getIdCertificateOperationPk() {
		return idCertificateOperationPk;
	}

	public void setIdCertificateOperationPk(Long idCertificateOperationPk) {
		this.idCertificateOperationPk = idCertificateOperationPk;
	}

	public Integer getCertificateOperationType() {
		return certificateOperationType;
	}

	public void setCertificateOperationType(Integer certificateOperationType) {
		this.certificateOperationType = certificateOperationType;
	}

	public Integer getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public Integer getBranchOfficeTarget() {
		return branchOfficeTarget;
	}

	public void setBranchOfficeTarget(Integer branchOfficeTarget) {
		this.branchOfficeTarget = branchOfficeTarget;
	}

	public Integer getVaultLocationTarget() {
		return vaultLocationTarget;
	}

	public void setVaultLocationTarget(Integer vaultLocationTarget) {
		this.vaultLocationTarget = vaultLocationTarget;
	}

	public Integer getFractionType() {
		return fractionType;
	}

	public void setFractionType(Integer fractionType) {
		this.fractionType = fractionType;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public BigDecimal getGlobalizeQuantity() {
		return globalizeQuantity;
	}

	public void setGlobalizeQuantity(BigDecimal globalizeQuantity) {
		this.globalizeQuantity = globalizeQuantity;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}
	
	public String getVaultLocationTargetDesc() {
		return vaultLocationTargetDesc;
	}

	public void setVaultLocationTargetDesc(String vaultLocationTargetDesc) {
		this.vaultLocationTargetDesc = vaultLocationTargetDesc;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null){
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(custodyOperation != null){
            	custodyOperation.setAudit(loggerUser);
            }
		}	
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getOperationTypeDesc() {
		return operationTypeDesc;
	}

	public void setOperationTypeDesc(String operationTypeDesc) {
		this.operationTypeDesc = operationTypeDesc;
	}

	public String getOperationStateDesc() {
		return operationStateDesc;
	}

	public void setOperationStateDesc(String operationStateDesc) {
		this.operationStateDesc = operationStateDesc;
	}
	
}
