package com.pradera.model.custody.splitcoupons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="SPLIT_COUPON_MARKETFACT")
public class SplitCouponMarketFact implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="SPLITCOUPONMARKETFACTPK_GENERATOR", sequenceName="SQ_ID_SPLIT_MARKETFACT_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SPLITCOUPONMARKETFACTPK_GENERATOR")	
	@Column(name="ID_SPLIT_MARKETFACT_PK")
	private Long idSplitMarketFactPk;

	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Column(name="OPERATION_QUANTITY")
	private BigDecimal operationQuantity;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SPLIT_OPERATION_FK",referencedColumnName="ID_SPLIT_OPERATION_PK")
	private SplitCouponRequest splitCouponRequest;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	public Long getIdSplitMarketFactPk() {
		return idSplitMarketFactPk;
	}

	public void setIdSplitMarketFactPk(Long idSplitMarketFactPk) {
		this.idSplitMarketFactPk = idSplitMarketFactPk;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public SplitCouponRequest getSplitCouponRequest() {
		return splitCouponRequest;
	}

	public void setSplitCouponRequest(SplitCouponRequest splitCouponRequest) {
		this.splitCouponRequest = splitCouponRequest;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getOperationQuantity() {
		return operationQuantity;
	}

	public void setOperationQuantity(BigDecimal operationQuantity) {
		this.operationQuantity = operationQuantity;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(Validations.validateIsNotNullAndNotEmpty(loggerUser)){
			 lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
	         lastModifyDate = loggerUser.getAuditTime();
	         lastModifyIp = loggerUser.getIpAddress();
	         lastModifyUser =loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
