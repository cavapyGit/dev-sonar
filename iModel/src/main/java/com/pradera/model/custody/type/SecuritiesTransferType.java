package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SecuritiesTransferType {
	MISMO_PARTICIPANTE(Integer.valueOf(196),"MISMO PARTICIPANTE"),
	ORIGEN_DESTINO(Integer.valueOf(525),"ORIGEN DESTINO"),
	DESTINO_ORIGEN(Integer.valueOf(526),"DESTINO ORIGEN");
	
	public static final List<SecuritiesTransferType> list = new ArrayList<SecuritiesTransferType>();
	public static final Map<Integer, SecuritiesTransferType> lookup = new HashMap<Integer, SecuritiesTransferType>();
	
	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private SecuritiesTransferType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	static {
        for (SecuritiesTransferType d : SecuritiesTransferType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
	
	public static SecuritiesTransferType get(Integer code) {
		return lookup.get(code);
	}
	
}