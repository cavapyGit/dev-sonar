package com.pradera.model.custody.splitcouponstype;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum SplitCouponStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public enum SplitCouponStateType {
	
	/** The registered. */
	REGISTERED(new Integer(1742), "REGISTRADO"),
	
	/** The approved. */
	APPROVED(new Integer(1743), "APROBADO"),		
	
	/** The annulled. */
	ANNULLED(new Integer(1744), "ANULADO"),
	
	/** The confirmed. */
	CONFIRMED(new Integer(1746), "CONFIRMADO"),
	
	/** The rejected. */
	REJECTED(new Integer(1745), "RECHAZADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new split coupon state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SplitCouponStateType(Integer code, String value){
		this.code = code;
		this.value = value;
	}
		
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
