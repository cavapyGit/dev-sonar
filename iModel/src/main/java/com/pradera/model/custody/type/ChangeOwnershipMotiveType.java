/**@author nmolina
 * 
 */

package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
public enum ChangeOwnershipMotiveType {
		
	SUCESION(new Integer(733),"SUCESION, DIVISION Y PARTICION DE MASA HEREDITARIA",
			ParameterOperationType.CHANGE_OWNERSHIP_ENTIRE_STATE.getCode()),
	DIVISION_PATRIMONIO(new Integer(734),"DIVISION Y PARTICION DEL PATRIMONIO DE SOCIEDAD CONYUGAL",
			ParameterOperationType.CHANGE_OWNERSHIP_MARRIAGE_PARTNERSHIP_EQUITY.getCode()),
	DIVISION_COPROPIEDAD(new Integer(735),"DIVISION Y PARTICION DE LA COPROPIEDAD",
			ParameterOperationType.CHANGE_OWNERSHIP_COOWNERSHIP_DIVISION_PARTITION.getCode()),
	DONACION(new Integer(736),"DONACION",
			ParameterOperationType.CHANGE_OWNERSHIP_DONATION.getCode()),
	FUSION(new Integer(737),"FUSION O ESCISION",
			ParameterOperationType.CHANGE_OWNERSHIP_MERGER_SPLIT.getCode()),
	DACION_PAGO(new Integer(738),"DACION EN PAGO",
			ParameterOperationType.CHANGE_OWNERSHIP_NONRECOURSE_DEBT.getCode()),
	PERMUTA(new Integer(739),"PERMUTA",
			ParameterOperationType.CHANGE_OWNERSHIP_SWAP.getCode()),
	MANDATO_JUDICIAL(new Integer(740),"MANDATO JUDICIAL",
			ParameterOperationType.CHANGE_OWNERSHIP_WRIT.getCode()),
	CAMBIO_RAZON_SOCIAL(new Integer(741),"CAMBIO DE RAZON SOCIAL O DENOMINACION", 
			ParameterOperationType.CHANGE_OWNERSHIP_DENOMINATION.getCode()),
	OTHERS(new Integer(742),"OTROS",
			ParameterOperationType.CHANGE_OWNERSHIP_OTHERS.getCode()),

			// ISSUE 1379
	EXCHANGE_SECURITIES(new Integer(1),"INTERCAMBIO DE VALORES",
			ParameterOperationType.TRANSFER_FOR_EXCHANGE_SECURITIES.getCode()),
			
			;
	
	private Integer code;
	private String value;
	private Long parameterOperationType;
	
	public static final List<ChangeOwnershipMotiveType> list = new ArrayList<ChangeOwnershipMotiveType>();
	
	public static final Map<Integer, ChangeOwnershipMotiveType> lookup = new HashMap<Integer, ChangeOwnershipMotiveType>();
	
	public static List<ChangeOwnershipMotiveType> listSomeElements(ChangeOwnershipMotiveType... transferSecuritiesTypeParams){
		List<ChangeOwnershipMotiveType> retorno = new ArrayList<ChangeOwnershipMotiveType>();
		for(ChangeOwnershipMotiveType changeOwnershipMotiveType: transferSecuritiesTypeParams){
			retorno.add(changeOwnershipMotiveType);
		}
		return retorno;
	}
	
	public static ChangeOwnershipMotiveType get(Integer code) {
		return lookup.get(code);
	}
	
	static {
		for (ChangeOwnershipMotiveType s : EnumSet.allOf(ChangeOwnershipMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private ChangeOwnershipMotiveType(Integer ordinal, String name, Long parameterOperationType) {
		this.code = ordinal;
		this.value = name;
		this.setParameterOperationType(parameterOperationType);
	}
	/**
	 * @return the parameterOperationType
	 */
	public Long getParameterOperationType() {
		return parameterOperationType;
	}
	/**
	 * @param parameterOperationType the parameterOperationType to set
	 */
	public void setParameterOperationType(Long parameterOperationType) {
		this.parameterOperationType = parameterOperationType;
	}
}