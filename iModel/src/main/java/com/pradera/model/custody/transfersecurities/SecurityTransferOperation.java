package com.pradera.model.custody.transfersecurities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SECURITY_TRANSFER_OPERATION database table.
 * 
 */
@Entity
@Table(name="SECURITY_TRANSFER_OPERATION")
public class SecurityTransferOperation implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id transfer operation pk. */
	@Id
	private Long idTransferOperationPk;
	
	/** The custody operation. */
	@MapsId
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRANSFER_OPERATION_PK")
	private CustodyOperation custodyOperation;

	//bi-directional many-to-one association to securities
    /** The securities. */
	@ManyToOne
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;
	
	@Transient
	private Holder holder;

	//bi-directional many-to-one association to sourceHolderAccount
    /** The holder account for search. */
	@Transient
	private HolderAccount holderAccount;
    
    //bi-directional many-to-one association to sourceHolderAccount
    /** The source holder account. */
    @ManyToOne
	@JoinColumn(name="ID_SOURCE_HOLDER_ACCOUNT_FK")
	private HolderAccount sourceHolderAccount;

    //bi-directional many-to-one association to sourceParticipant
    /** The participant for search. */
    @Transient
	private Participant participant;
    
    //bi-directional many-to-one association to sourceParticipant
    /** The source participant. */
    @ManyToOne
	@JoinColumn(name="ID_SOURCE_PARTICIPANT_FK")
	private Participant sourceParticipant;

    //bi-directional many-to-one association to targetHolderAccount
    /** The target holder account. */
    @ManyToOne
	@JoinColumn(name="ID_TARGET_HOLDER_ACCOUNT_FK")
	private HolderAccount targetHolderAccount;
    
    //bi-directional many-to-one association to targetParticipant
    /** The target participant. */
    @ManyToOne
	@JoinColumn(name="ID_TARGET_PARTICIPANT_FK")
	private Participant targetParticipant;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The quantity operation. */
	@Column(name="QUANTITY_OPERATION")
	private BigDecimal quantityOperation;

	/** The transfer type. */
	@Column(name="TRANSFER_TYPE")
	private Integer transferType;

	/** The ind block transfer. */
	@Column(name="IND_BLOCK_TRANSFER")
	private Integer indBlockTransfer;
	
	/** The state. */
	@Column(name="\"STATE\"")
	private Integer state;
	
	@OneToMany(mappedBy="securityTransferOperation")
	private List<SecurityTransferMarketfact> securityTransferOperationnMarketFact;

	
	/** The state description. */
	@Transient
	private String stateDescription;
	
    /** The version. */
    @Version
    private Long version;
    
    @Transient
    private boolean selected;

    @Transient
    private boolean checkForReview;

    @Transient
    private boolean enabledForReview;

    @Transient
    private boolean flagTargetSource;
    
    @Transient
    private List<HolderMarketFactBalance> lstHolderMarketfactBalance;

    @Transient
    private List<SecurityTransferMarketfact> lstsecurityTransferMarketfact;

    @Transient
    private Map<Integer, String> MapSecurityTransferState = new HashMap<Integer,String>();
    
    @Transient
    private Integer correlativeOperation;

    @Transient
    private String styleClassButton;
    
    @Transient
	private Integer securityClass;
//    @Transient
//    private List<HolderAccountHelperResultTO> lstSourceAccountTo;
//    
//    @Transient
//    private List<HolderAccountHelperResultTO> lstTargetAccountTo;
    
	
	// bi-directional many-to-one association to BlockOperationTransfer
	/** The block operation transfer. */
	@OneToMany(mappedBy = "securityTransferOperation")
	private List<BlockOperationTransfer> blockOperationTransfer;
	
	
    /**
     * Instantiates a new security transfer operation.
     */
    public SecurityTransferOperation() {
    }


	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the new securities
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	/**
	 * Gets the source holder account.
	 *
	 * @return the source holder account
	 */
	public HolderAccount getSourceHolderAccount() {
		return sourceHolderAccount;
	}

	/**
	 * Sets the source holder account.
	 *
	 * @param sourceHolderAccount the new source holder account
	 */
	public void setSourceHolderAccount(HolderAccount sourceHolderAccount) {
		this.sourceHolderAccount = sourceHolderAccount;
	}

	/**
	 * Gets the source participant.
	 *
	 * @return the source participant
	 */
	public Participant getSourceParticipant() {
		return sourceParticipant;
	}

	/**
	 * Sets the source participant.
	 *
	 * @param sourceParticipant the new source participant
	 */
	public void setSourceParticipant(Participant sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}

	/**
	 * Gets the target holder account.
	 *
	 * @return the target holder account
	 */
	public HolderAccount getTargetHolderAccount() {
		return targetHolderAccount;
	}

	/**
	 * Sets the target holder account.
	 *
	 * @param targetHolderAccount the new target holder account
	 */
	public void setTargetHolderAccount(HolderAccount targetHolderAccount) {
		this.targetHolderAccount = targetHolderAccount;
	}

	/**
	 * Gets the target participant.
	 *
	 * @return the target participant
	 */
	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	/**
	 * Sets the target participant.
	 *
	 * @param targetParticipant the new target participant
	 */
	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the quantity operation.
	 *
	 * @return the quantity operation
	 */
	public BigDecimal getQuantityOperation() {
		return this.quantityOperation;
	}

	/**
	 * Sets the quantity operation.
	 *
	 * @param quantityOperation the new quantity operation
	 */
	public void setQuantityOperation(BigDecimal quantityOperation) {
		this.quantityOperation = quantityOperation;
	}

	/**
	 * Gets the transfer type.
	 *
	 * @return the transfer type
	 */
	public Integer getTransferType() {
		return this.transferType;
	}

	/**
	 * Sets the transfer type.
	 *
	 * @param transferType the new transfer type
	 */
	public void setTransferType(Integer transferType) {
		this.transferType = transferType;
	}

	/**
	 * Gets the custody operation.
	 *
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation() {
		return this.custodyOperation;
	}

	/**
	 * Sets the custody operation.
	 *
	 * @param custodyOperation the new custody operation
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}


	/**
	 * Gets the id transfer operation pk.
	 *
	 * @return the id transfer operation pk
	 */
	public Long getIdTransferOperationPk() {
		return idTransferOperationPk;
	}


	/**
	 * Sets the id transfer operation pk.
	 *
	 * @param idTransferOperationPk the new id transfer operation pk
	 */
	public void setIdTransferOperationPk(Long idTransferOperationPk) {
		this.idTransferOperationPk = idTransferOperationPk;
	}


	/**
	 * Gets the block operation transfer.
	 *
	 * @return the block operation transfer
	 */
	public List<BlockOperationTransfer> getBlockOperationTransfer() {
		return blockOperationTransfer;
	}


	/**
	 * Sets the block operation transfer.
	 *
	 * @param blockOperationTransfer the new block operation transfer
	 */
	public void setBlockOperationTransfer(
			List<BlockOperationTransfer> blockOperationTransfer) {
		this.blockOperationTransfer = blockOperationTransfer;
	}
	
	
	/**
	 * Gets the ind block transfer.
	 *
	 * @return the ind block transfer
	 */
	public Integer getIndBlockTransfer() {
		return indBlockTransfer;
	}


	/**
	 * Sets the ind block transfer.
	 *
	 * @param indBlockTransfer the new ind block transfer
	 */
	public void setIndBlockTransfer(Integer indBlockTransfer) {
		this.indBlockTransfer = indBlockTransfer;
	}


	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}


	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}		

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Integer getCorrelativeOperation() {
		return correlativeOperation;
	}

	public void setCorrelativeOperation(Integer correlativeOperation) {
		this.correlativeOperation = correlativeOperation;
	}

	public List<HolderMarketFactBalance> getLstHolderMarketfactBalance() {
		return lstHolderMarketfactBalance;
	}

	public void setLstHolderMarketfactBalance(
			List<HolderMarketFactBalance> lstHolderMarketfactBalance) {
		this.lstHolderMarketfactBalance = lstHolderMarketfactBalance;
	}

	public boolean isCheckForReview() {
		return checkForReview;
	}

	public void setCheckForReview(boolean checkForReview) {
		this.checkForReview = checkForReview;
	}

	public boolean isEnabledForReview() {
		return enabledForReview;
	}

	public void setEnabledForReview(boolean enabledForReview) {
		this.enabledForReview = enabledForReview;
	}

	public boolean isFlagTargetSource() {
		return flagTargetSource;
	}

	public void setFlagTargetSource(boolean flagTargetSource) {
		this.flagTargetSource = flagTargetSource;
	}
	
	public List<SecurityTransferMarketfact> getLstsecurityTransferMarketfact() {
		return lstsecurityTransferMarketfact;
	}

	public void setLstsecurityTransferMarketfact(
			List<SecurityTransferMarketfact> lstsecurityTransferMarketfact) {
		this.lstsecurityTransferMarketfact = lstsecurityTransferMarketfact;
	}

//	public List<HolderAccountHelperResultTO> getLstSourceAccountTo() {
//		return lstSourceAccountTo;
//	}
//
//	public void setLstSourceAccountTo(
//			List<HolderAccountHelperResultTO> lstSourceAccountTo) {
//		this.lstSourceAccountTo = lstSourceAccountTo;
//	}
//
//	public List<HolderAccountHelperResultTO> getLstTargetAccountTo() {
//		return lstTargetAccountTo;
//	}


//	public void setLstTargetAccountTo(
//			List<HolderAccountHelperResultTO> lstTargetAccountTo) {
//		this.lstTargetAccountTo = lstTargetAccountTo;
//	}


	public String getStyleClassButton() {
		return styleClassButton;
	}


	public Map<Integer, String> getMapSecurityTransferState() {
		return MapSecurityTransferState;
	}

	public void setMapSecurityTransferState(
			Map<Integer, String> mapSecurityTransferState) {
		MapSecurityTransferState = mapSecurityTransferState;
	}

	public void setStyleClassButton(String styleClassButton) {
		this.styleClassButton = styleClassButton;
	}


	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	


	public List<SecurityTransferMarketfact> getSecurityTransferOperationnMarketFact() {
		return securityTransferOperationnMarketFact;
	}


	public void setSecurityTransferOperationnMarketFact(
			List<SecurityTransferMarketfact> securityTransferOperationnMarketFact) {
		this.securityTransferOperationnMarketFact = securityTransferOperationnMarketFact;
	}


	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(custodyOperation!=null){
    			custodyOperation.setLastModifyApp(lastModifyApp);
    			custodyOperation.setLastModifyDate(lastModifyDate);
    			custodyOperation.setLastModifyIp(lastModifyIp);
    			custodyOperation.setLastModifyUser(lastModifyUser);
    		}
        }
		
	}


	public Integer getSecurityClass() {
		return securityClass;
	}


	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	

}