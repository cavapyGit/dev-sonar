package com.pradera.model.custody.affectation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AffectationStateType estado de las afectaciones
 */
public enum AffectationStateType {
	
	/** The registrado. */
	REGISTRED(new Integer(1140), "REGISTRADO"),	

	BLOCKED(new Integer(1141), "BLOQUEADO"),
		
	UNBLOCKED(new Integer(1143), "DESBLOQUEADO"),
	
	/** The anulada. */
	ANNULLED(new Integer(1145), "ANULADO");
	
	/** The code. */
	private Integer code;
	private String value;
	
	

	/**
	 * Instantiates a new affectation state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AffectationStateType(Integer code, String value){
		this.code = code;
		this.value = value;
	}
		
	/** The Constant list. */
	public static final List<AffectationStateType> list = new ArrayList<AffectationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AffectationStateType> lookup = new HashMap<Integer, AffectationStateType>();
	
	static {
		for (AffectationStateType s : EnumSet.allOf(AffectationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static AffectationStateType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}	
	
}
