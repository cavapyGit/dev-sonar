package com.pradera.model.custody.securitiesannotation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.stockcalculation.validation.StockByParticipant;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.SecuritiesManager;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the ACCOUNT_ANNOTATION_CUPON database table.
 * 
 */
@Entity
@Table(name="ACCOUNT_ANNOTATION_CUPON")
public class AccountAnnotationCupon implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCOUNT_ANNOTATION_CUPONPK_GENERATOR", sequenceName="SQ_ACCOUNT_ANNOTATION_CUPON_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNT_ANNOTATION_CUPONPK_GENERATOR")
	@Column(name="ID_ANNOTATION_CUPON_PK")
	@NotNull(groups={StockByParticipant.class})
	private Long idAccountAnnotationCuponPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ANNOTATION_OPERATION_FK")
	private AccountAnnotationOperation accountAnnotationOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;
    
    @JoinColumn(name = "ID_ACC_ANNOTATION_FILE_FK", referencedColumnName = "ID_FILE_PK")
	@ManyToOne(fetch = FetchType.LAZY)
    private AccAnnotationOperationFile idAccAnnotationFileFk;

	@Column(name="CERTIFICATE_NUMBER")
	private String certificateNumber;
	
	@Column(name="IND_ELECTRONIC_CUPON")
	private Integer indElectronicCupon;

	@Column(name="SERIAL_NUMBER")
	private String serialNumber;

	@Column(name="COUPON_NUMBER")
	private Integer cuponNumber;

    @Temporal( TemporalType.DATE)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

    @Temporal( TemporalType.DATE)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

    @Temporal( TemporalType.DATE)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

	@Column(name="PAYMENT_DAYS")
	private Integer paymentDays;
	
	@Column(name = "INTEREST_FACTOR")
	private BigDecimal interestFactor;
	
	@Column(name = "INTEREST_RATE")
	private BigDecimal interestRate;
	
	@Column(name = "CUPON_AMOUNT")
	private BigDecimal cuponAmount;
	
	@Column(name = "STATE_ANNOTATION")
	private Integer stateAnnotation;

	@Type(type = "org.hibernate.type.NumericBooleanType")
	@Column(name = "STATE_CUPON")
	private Boolean stateCupon;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROGRAM_INTEREST_FK")
	private ProgramInterestCoupon programInterestCoupon;

	@Column(name = "SECURITY_NOMINAL_VALUE")
	private BigDecimal securityNominalValue;

	@Column(name = "SECURITY_TOTAL_BALANCE")
	private BigDecimal securityTotalBalance;
	
	@Transient
	private Issuer issuer;

	@Transient
	private Integer securityCurrency;
	
	
	@Transient
	private String messagesError;
	

	public Long getIdAccountAnnotationCuponPk() {
		return idAccountAnnotationCuponPk;
	}

	public void setIdAccountAnnotationCuponPk(Long idAccountAnnotationCuponPk) {
		this.idAccountAnnotationCuponPk = idAccountAnnotationCuponPk;
	}

	public AccountAnnotationOperation getAccountAnnotationOperation() {
		return accountAnnotationOperation;
	}

	public void setAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation) {
		this.accountAnnotationOperation = accountAnnotationOperation;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public AccAnnotationOperationFile getIdAccAnnotationFileFk() {
		return idAccAnnotationFileFk;
	}

	public void setIdAccAnnotationFileFk(AccAnnotationOperationFile idAccAnnotationFileFk) {
		this.idAccAnnotationFileFk = idAccAnnotationFileFk;
	}

	public Integer getIndElectronicCupon() {
		return indElectronicCupon;
	}

	public void setIndElectronicCupon(Integer indElectronicCupon) {
		this.indElectronicCupon = indElectronicCupon;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getCuponNumber() {
		return cuponNumber;
	}

	public void setCuponNumber(Integer cuponNumber) {
		this.cuponNumber = cuponNumber;
	}

	public Date getBeginingDate() {
		return beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Integer getPaymentDays() {
		return paymentDays;
	}

	public void setPaymentDays(Integer paymentDays) {
		this.paymentDays = paymentDays;
	}

	public BigDecimal getInterestFactor() {
		return interestFactor;
	}

	public void setInterestFactor(BigDecimal interestFactor) {
		this.interestFactor = interestFactor;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public BigDecimal getCuponAmount() {
		return cuponAmount;
	}

	public void setCuponAmount(BigDecimal cuponAmount) {
		this.cuponAmount = cuponAmount;
	}

	public Integer getStateAnnotation() {
		return stateAnnotation;
	}

	public void setStateAnnotation(Integer stateAnnotation) {
		this.stateAnnotation = stateAnnotation;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public ProgramInterestCoupon getProgramInterestCoupon() {
		return programInterestCoupon;
	}

	public void setProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon) {
		this.programInterestCoupon = programInterestCoupon;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public Boolean getStateCupon() {
		return stateCupon;
	}

	public void setStateCupon(Boolean stateCupon) {
		this.stateCupon = stateCupon;
	}

	public String getMessagesError() {
		return messagesError;
	}

	public void setMessagesError(String messagesError) {
		this.messagesError = messagesError;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Integer getSecurityCurrency() {
		return securityCurrency;
	}

	public void setSecurityCurrency(Integer securityCurrency) {
		this.securityCurrency = securityCurrency;
	}

	public BigDecimal getSecurityNominalValue() {
		return securityNominalValue;
	}

	public void setSecurityNominalValue(BigDecimal securityNominalValue) {
		this.securityNominalValue = securityNominalValue;
	}

	public BigDecimal getSecurityTotalBalance() {
		return securityTotalBalance;
	}

	public void setSecurityTotalBalance(BigDecimal securityTotalBalance) {
		this.securityTotalBalance = securityTotalBalance;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
	    if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser =loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}