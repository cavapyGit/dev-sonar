/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.custody.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public enum CustodyOperationStateType {
	
	REGISTERED(new Integer(1913),"REGISTRADO"),
	APPROVED(new Integer(1914),"APROBADO"),
	ANNULLED(new Integer(1915),"ANULADO"),
	CONFIRMED(new Integer(1916),"CONFIRMADO"),
	REJECTED(new Integer(1917),"RECHAZADO"),
	BLOCKED(new Integer(1918),"BLOQUEADO"),
	AUTHORIZED(new Integer(1919),"AUTORIZADO"),
	REVISED(new Integer(1920),"REVISADO"),
	APPLIED(new Integer(1921),"APLICADO"),
	RECTIFIED(new Integer(2273),"RECTIFICADO");
	
	
	private Integer code;
	private String value;
	public static final List<CustodyOperationStateType> list = new ArrayList<CustodyOperationStateType>();
	public static final Map<Integer, CustodyOperationStateType> lookup = new HashMap<Integer, CustodyOperationStateType>();
	public static List<CustodyOperationStateType> listSomeElements(CustodyOperationStateType... transferSecuritiesTypeParams){
		List<CustodyOperationStateType> retorno = new ArrayList<CustodyOperationStateType>();
		for(CustodyOperationStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (CustodyOperationStateType s : EnumSet.allOf(CustodyOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private CustodyOperationStateType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	public static CustodyOperationStateType get(Integer codigo) {
		return lookup.get(codigo);
	}
}