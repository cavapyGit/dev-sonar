package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BenefitPaymentType {

	PDD(Integer.valueOf(2908),"PDD"),
	PDI(Integer.valueOf(2907),"PDI");

	private Integer code;
	private String value;


	/** The Constant list. */
	public static final List<BenefitPaymentType> list = new ArrayList<BenefitPaymentType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BenefitPaymentType> lookup = new HashMap<Integer, BenefitPaymentType>();
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<BenefitPaymentType> listSomeElements(BenefitPaymentType... paymentTypes){
		List<BenefitPaymentType> retorno = new ArrayList<BenefitPaymentType>();
		for(BenefitPaymentType paymentType: paymentTypes){
			retorno.add(paymentType);
		}
		return retorno;
	}
	static {
		for (BenefitPaymentType s : EnumSet.allOf(BenefitPaymentType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Instantiates a new transfer securities estatus type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 * @param accion the accion
	 */
	private BenefitPaymentType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the transfer securities estatus type
	 */
	public static BenefitPaymentType get(Integer codigo) {
		return lookup.get(codigo);
	}
}
