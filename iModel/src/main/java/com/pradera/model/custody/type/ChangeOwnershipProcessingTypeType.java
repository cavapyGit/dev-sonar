/**@author nmolina
 * 
 */

package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public enum ChangeOwnershipProcessingTypeType {
		
	NORMAL_OPERATION(new Integer(1203),"OPERACION NORMAL"),
	PENDING_BALANCE_TRANSFER(new Integer(1204),"TRANSFERENCIA DE SALDOS PENDIENTES");
	
	private Integer code;
	private String value;
	public static final List<ChangeOwnershipProcessingTypeType> list = new ArrayList<ChangeOwnershipProcessingTypeType>();
	
	public static final Map<Integer, ChangeOwnershipProcessingTypeType> lookup = new HashMap<Integer, ChangeOwnershipProcessingTypeType>();
	
	public static List<ChangeOwnershipProcessingTypeType> listSomeElements(ChangeOwnershipProcessingTypeType... transferSecuritiesTypeParams){
		List<ChangeOwnershipProcessingTypeType> retorno = new ArrayList<ChangeOwnershipProcessingTypeType>();
		for(ChangeOwnershipProcessingTypeType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ChangeOwnershipProcessingTypeType s : EnumSet.allOf(ChangeOwnershipProcessingTypeType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private ChangeOwnershipProcessingTypeType(Integer ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}