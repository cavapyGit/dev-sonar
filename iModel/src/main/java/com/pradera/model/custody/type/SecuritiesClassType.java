package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum SecuritiesClassType {
	ACCIONES_COMUNES(new Integer(126),"deposito.security.class.common.shares"),
	ACCIONES_DE_INVERSION(new Integer(406),"deposito.security.class.invest.shares"),
	ACCIONES_COMUNES_PREFERENTES(new Integer(407),"deposito.security.class.preferd.shares"),
	BONOS_HIPOTECARIOS(new Integer(408),"deposito.security.class.mortgage.bonds"),
	BONOS_SUBORDINADOS(new Integer(409),"deposito.security.class.subordinate.bonds"),
	BONOS_SOBERANO(new Integer(410),"deposito.security.class.sovergn.bonds"),
	BONOS_SECTOR_PUBLICO(new Integer(411),"deposito.security.class.publicsector.bonds"),
	BONOS_DE_TITULIZACION(new Integer(412),"deposito.security.class.securitisation.bonds"),
	BONOS_CORPORATIVOS(new Integer(413),"deposito.security.class.corporate.bonds"),
	BONOS_ESTRUCTURADOS(new Integer(414),"deposito.security.class.structred.bonds"),
	BONOS_CONVERTIBLES_EN_ACCIONES(new Integer(415),"deposito.security.class.convert.bonds"),
	BONOS_DE_ARRENDAMIENTO_FINANCIERO(new Integer(416),"deposito.security.class.leasing.bonds"),
	LETRAS_HIPOTECARIAS(new Integer(417),"deposito.security.class.mortgageo.bonds"),
	TITULOS_DE_CREDITO_HIPOTECARIO_NEGOCIABLE(new Integer(418),"deposito.security.class.mortgagesec.negotiable"),
	PAPELES_COMERCIALES(new Integer(419),"deposito.security.class.comercial.paper"),
	CERTIFICADOS_DE_DEPOSITOS_BANCARIOS(new Integer(420),"deposito.security.class.certificate.deposit"),
	LETRAS(new Integer(421),"deposito.security.class.letters"),
	PAGARES(new Integer(422),"deposito.security.class.notes"),
	FACTURAS_CONFORMADAS(new Integer(423),"deposito.security.class.accepted.invoices"),
	WARRANT(new Integer(424),"deposito.security.class.warrant"),
	CUOTA_DE_PARTICIPACION_EN_FONDOS_DE_INVERSION(new Integer(425),"deposito.security.class.part.funds"),
	CUOTA_DE_PARTICIPACION_EN_FONDOS_MUTUOS(new Integer(426),"deposito.security.class.share.mutualfunds"),
	CERTIFICADOS_DE_SUSCRIPCION_PREFERENTE(new Integer(427),"deposito.security.class.certificate.emptive"),
	AMERICAN_DEPOSITARY_RECEIPT(new Integer(428),"deposito.security.class.american.dreceipt"),
	AMERICAN_DEPOSITARY_SHARES(new Integer(429),"deposito.security.class.american.dshares");
	
	
	
	private Integer code;
	private String value;

	public static final List<SecuritiesClassType> list = new ArrayList<SecuritiesClassType>();
	public static final Map<Integer, SecuritiesClassType> lookup = new HashMap<Integer, SecuritiesClassType>();

	static {
		for (SecuritiesClassType s : EnumSet.allOf(SecuritiesClassType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	private SecuritiesClassType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getValue() {
		return value;
	}
	
	
	
	public static SecuritiesClassType get(Integer code) {
		return lookup.get(code);
	}
}
