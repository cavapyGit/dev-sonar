package com.pradera.model.custody.blockenforce.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AffectationStateType estado de las solicitudes de afectacion.
 */
public enum BlockEnforceRejectMotiveType {
	
	PARTICIPANT_REQUESTER(Integer.valueOf(2039)),
	INCORRECT_DATA(Integer.valueOf(2040)),
	OTHERS_MOTIVES(Integer.valueOf(2041));
	
	
	/** The code. */
	private Integer code;
	
	
	/**
	 * Instantiates a new affectation state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BlockEnforceRejectMotiveType(Integer code){
		this.code = code;
	}
		
	/** The Constant list. */
	public static final List<BlockEnforceRejectMotiveType> list = new ArrayList<BlockEnforceRejectMotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BlockEnforceRejectMotiveType> lookup = new HashMap<Integer, BlockEnforceRejectMotiveType>();
	
	static {
		for (BlockEnforceRejectMotiveType s : EnumSet.allOf(BlockEnforceRejectMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static BlockEnforceRejectMotiveType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}
