package com.pradera.model.custody.physical;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.HolderAccount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the PHYSICAL_CERTIFICATE_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="PHYSICAL_CERTIFICATE_MOVEMENT")
public class PhysicalCertificateMovement implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PHYSICAL_CERTIFICATE_MOVEMENT_GENERATOR", sequenceName="SQ_ID_CERTIFICATE_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PHYSICAL_CERTIFICATE_MOVEMENT_GENERATOR")
	@Column(name="ID_CERTIFICATE_MOVEMENT_PK")
	private Long idCertificateMovementPk;

	@ManyToOne
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount HolderAccount;

	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="CERTIFICATE_QUANTITY")
	private BigDecimal certificateQuantity;
	
	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	//bi-directional many-to-one association to PhysicalCertificate
    @ManyToOne
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;

	//bi-directional many-to-one association to PhysicalCertificateOperation
    @ManyToOne
	@JoinColumn(name="ID_PHYSICAL_OPERATION_FK", referencedColumnName="ID_PHYSICAL_OPERATION_PK")
	private PhysicalCertificateOperation physicalCertificateOperation;
    
	
	public HolderAccount getHolderAccount() {
		return HolderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		HolderAccount = holderAccount;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * @param idCertificateMovementPk the idCertificateMovementPk to set
	 */
	public void setIdCertificateMovementPk(Long idCertificateMovementPk) {
		this.idCertificateMovementPk = idCertificateMovementPk;
	}

	public PhysicalCertificateMovement() {
    }

	public long getIdCertificateMovementPk() {
		return this.idCertificateMovementPk;
	}

	public void setIdCertificateMovementPk(long idCertificateMovementPk) {
		this.idCertificateMovementPk = idCertificateMovementPk;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMovementDate() {
		return this.movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return this.physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}
	
	public PhysicalCertificateOperation getPhysicalCertificateOperation() {
		return this.physicalCertificateOperation;
	}

	public void setPhysicalCertificateOperation(PhysicalCertificateOperation physicalCertificateOperation) {
		this.physicalCertificateOperation = physicalCertificateOperation;
	}
	
	public BigDecimal getCertificateQuantity() {
		return certificateQuantity;
	}

	public void setCertificateQuantity(BigDecimal certificateQuantity) {
		this.certificateQuantity = certificateQuantity;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}