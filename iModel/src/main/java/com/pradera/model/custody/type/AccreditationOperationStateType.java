package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum AccreditationOperationStateType {
	REGISTRED(new Integer(455),"REGISTRADO"),
	CONFIRMED(new Integer(456),"CONFIRMADO"),
	UNBLOCKED(new Integer(458),"VENCIDO"),
	CANCELED(new Integer(1371),"CANCELADO"),
	REJECTED(new Integer(457),"RECHAZADO");
	
	private Integer code;
	private String value;
	public static final List<AccreditationOperationStateType> list = new ArrayList<AccreditationOperationStateType>();
	
	public static final Map<Integer, AccreditationOperationStateType> lookup = new HashMap<Integer, AccreditationOperationStateType>();
	public static List<AccreditationOperationStateType> listSomeElements(AccreditationOperationStateType... transferSecuritiesTypeParams){
		List<AccreditationOperationStateType> retorno = new ArrayList<AccreditationOperationStateType>();
		for(AccreditationOperationStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (AccreditationOperationStateType s : EnumSet.allOf(AccreditationOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private AccreditationOperationStateType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	public static AccreditationOperationStateType get(Integer code) {
		return lookup.get(code);
	}
}
