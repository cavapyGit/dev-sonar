package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RetirementOperationRedemptionType {
	
	TOTAL(new Integer(2174),"TOTAL"),
	PARTIAL(new Integer(2175),"PARCIAL");
	
	private Integer code;
	private String value;
	public static final List<RetirementOperationRedemptionType> list = new ArrayList<RetirementOperationRedemptionType>();
	
	public static final Map<Integer, RetirementOperationRedemptionType> lookup = new HashMap<Integer, RetirementOperationRedemptionType>();
	public static List<RetirementOperationRedemptionType> listSomeElements(RetirementOperationRedemptionType... transferSecuritiesTypeParams){
		List<RetirementOperationRedemptionType> retorno = new ArrayList<RetirementOperationRedemptionType>();
		for(RetirementOperationRedemptionType removeSecurityMotiveType: transferSecuritiesTypeParams){
			retorno.add(removeSecurityMotiveType);
		}
		return retorno;
	}
	static {
		for (RetirementOperationRedemptionType s : EnumSet.allOf(RetirementOperationRedemptionType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public static RetirementOperationRedemptionType get(Integer code){
		return lookup.get(code);
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private RetirementOperationRedemptionType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
