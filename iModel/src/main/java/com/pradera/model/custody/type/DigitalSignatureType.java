package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum DigitalSignatureType {

	FIRST_SIGNATURE(new Integer(2318),"PRIMERA FIRMA"),
	SECOND_SIGNATURE(new Integer(2319),"SEGUNDA FIRMA"),
	FIRST_P12_CONTAINER_SIGNATURE(new Integer(2444),"FIRMA A"),
	SECOND_P12_CONTAINER_SIGNATURE(new Integer(2446),"FIRMA B"),;
	
	private Integer code;
	private String value;
	public static final List<DigitalSignatureType> list = new ArrayList<DigitalSignatureType>();
	
	public static final Map<Integer, DigitalSignatureType> lookup = new HashMap<Integer, DigitalSignatureType>();
	public static List<DigitalSignatureType> listSomeElements(DigitalSignatureType... digitalSignatureTypeParams){
		List<DigitalSignatureType> retorno = new ArrayList<DigitalSignatureType>();
		for(DigitalSignatureType diditalSignatureType: digitalSignatureTypeParams){
			retorno.add(diditalSignatureType);
		}
		return retorno;
	}
	static {
		for (DigitalSignatureType s : EnumSet.allOf(DigitalSignatureType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	private DigitalSignatureType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public static DigitalSignatureType get(Integer code) {
		return lookup.get(code);
	}
}
