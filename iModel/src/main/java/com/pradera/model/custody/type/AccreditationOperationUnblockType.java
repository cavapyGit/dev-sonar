package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AccreditationOperationUnblockType {
	DESIST_GUARANTEES(new Integer(1655),"POR DESISTIR GARANTIAS"),
	SALES_OF_SECURITIES(new Integer(1656),"POR VENTAS DE VALORES"),
	CHANGE_OF_OWNERSHIP(new Integer(1657),"POR CAMBIO DE TITULARIDAD"),
	OTHERS(new Integer(1658),"OTROS");
	
	private Integer code;
	private String value;
	public static final List<AccreditationOperationUnblockType> list = new ArrayList<AccreditationOperationUnblockType>();
	
	public static final Map<Integer, AccreditationOperationUnblockType> lookup = new HashMap<Integer, AccreditationOperationUnblockType>();
	public static List<AccreditationOperationUnblockType> listSomeElements(AccreditationOperationUnblockType... AccreditationOperationUnblockType){
		List<AccreditationOperationUnblockType> retorno = new ArrayList<AccreditationOperationUnblockType>();
		for(AccreditationOperationUnblockType AccreditationOperationUnblockTypeO: AccreditationOperationUnblockType){
			retorno.add(AccreditationOperationUnblockTypeO);
		}
		return retorno;
	}
	static {
		for (AccreditationOperationUnblockType s : EnumSet.allOf(AccreditationOperationUnblockType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private AccreditationOperationUnblockType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static AccreditationOperationUnblockType get(Integer code) {
		return lookup.get(code);
	}
}
