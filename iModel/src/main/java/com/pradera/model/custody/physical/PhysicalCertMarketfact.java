package com.pradera.model.custody.physical;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PHYSICAL_CERT_MARKETFACT database table.
 * 
 */
@Entity
@Table(name="PHYSICAL_CERT_MARKETFACT")
public class PhysicalCertMarketfact implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id transfer marketfact pk. */
	@Id
	@SequenceGenerator(name="CUSTODY_OPERATION_IDPHYSICALCERTMARKETFACT_GENERATOR", sequenceName="SQ_ID_PHYSICAL_CERT_MARKETFACT",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CUSTODY_OPERATION_IDPHYSICALCERTMARKETFACT_GENERATOR")
	@Column(name="ID_PHYSICAL_CERT_MARKETFACT_PK")
	private Long IdPhysicalCertMarketfactPk;
	
	/** The security transfer operation. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate ;

	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;

    /** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	/** The quantity operation. */
	@Column(name="QUANTITY")
	private BigDecimal quantity;

	/** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="IND_ACTIVE")
	private Integer indActive = ComponentConstant.ONE ;

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Long getIdPhysicalCertMarketfactPk() {
		return IdPhysicalCertMarketfactPk;
	}

	public void setIdPhysicalCertMarketfactPk(Long idPhysicalCertMarketfactPk) {
		IdPhysicalCertMarketfactPk = idPhysicalCertMarketfactPk;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	public Integer getIndActive() {
		return indActive;
	}

	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}
}