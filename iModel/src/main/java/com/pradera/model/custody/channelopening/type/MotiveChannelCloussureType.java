package com.pradera.model.custody.channelopening.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum MotiveChannelCloussureType {
	
	CIERRE_A_SOLICITUD_PARTICIPANTE(Integer.valueOf(2482),"A SOLICITUD DEL PARTICIPANTE"),
	CIERRE_OTROS(Integer.valueOf(2483),"OTROS MOTIVOS PARA EL CIERRE DE CANAL"),
	;
	
	/** The Constant list. */
	public static final List<MotiveChannelCloussureType> list = new ArrayList<MotiveChannelCloussureType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, MotiveChannelCloussureType> lookup = new HashMap<Integer, MotiveChannelCloussureType>();
	static {
		for (MotiveChannelCloussureType s : EnumSet.allOf(MotiveChannelCloussureType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private MotiveChannelCloussureType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static MotiveChannelCloussureType get(Integer code) {
		return lookup.get(code);
	}
}
