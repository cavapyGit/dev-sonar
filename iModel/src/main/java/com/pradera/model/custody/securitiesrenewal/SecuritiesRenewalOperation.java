/**
 * 
 */
package com.pradera.model.custody.securitiesrenewal;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Security;

/**
 * The persistent class for the SECURITIES_RENEWAL_OPERATION database table.
 * 
 */
@Entity
@Table(name="SECURITIES_RENEWAL_OPERATION")
public class SecuritiesRenewalOperation implements Serializable, Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long idSecuritiesRenewalPk;

	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_SECURITIES_RENEWAL_PK")
	private CustodyOperation custodyOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOURCE_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK")	
	private Security sourceSecurity;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK")	
	private Security targetSecurity;
	
	@Column(name="MOTIVE")
	private Integer motive;
	
	@Column(name="OPERATION_STATE")
	private Integer operationState;
	
	@Column(name="SECURITY_EXPIRATION")
	private Integer securityExpiration;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@OneToMany(mappedBy="securitiesRenewalOperation")
	private List<RenewalOperationDetail> renewalOperationDetail;

	public SecuritiesRenewalOperation() {
		super();
	}

	/**
	 * @return the idSecuritiesRenewalPk
	 */
	public Long getIdSecuritiesRenewalPk() {
		return idSecuritiesRenewalPk;
	}

	/**
	 * @param idSecuritiesRenewalPk the idSecuritiesRenewalPk to set
	 */
	public void setIdSecuritiesRenewalPk(Long idSecuritiesRenewalPk) {
		this.idSecuritiesRenewalPk = idSecuritiesRenewalPk;
	}

	/**
	 * @return the custodyOperation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * @param custodyOperation the custodyOperation to set
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * @return the sourceSecurity
	 */
	public Security getSourceSecurity() {
		return sourceSecurity;
	}

	/**
	 * @param sourceSecurity the sourceSecurity to set
	 */
	public void setSourceSecurity(Security sourceSecurity) {
		this.sourceSecurity = sourceSecurity;
	}

	/**
	 * @return the targetSecurity
	 */
	public Security getTargetSecurity() {
		return targetSecurity;
	}

	/**
	 * @param targetSecurity the targetSecurity to set
	 */
	public void setTargetSecurity(Security targetSecurity) {
		this.targetSecurity = targetSecurity;
	}

	/**
	 * @return the motive
	 */
	public Integer getMotive() {
		return motive;
	}

	/**
	 * @param motive the motive to set
	 */
	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	/**
	 * @return the operationState
	 */
	public Integer getOperationState() {
		return operationState;
	}

	/**
	 * @param operationState the operationState to set
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}
	
	/**
	 * @return the securityExpiration
	 */
	public Integer getSecurityExpiration() {
		return securityExpiration;
	}

	/**
	 * @param securityExpiration the securityExpiration to set
	 */
	public void setSecurityExpiration(Integer securityExpiration) {
		this.securityExpiration = securityExpiration;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the renewalOperationDetail
	 */
	public List<RenewalOperationDetail> getRenewalOperationDetail() {
		return renewalOperationDetail;
	}

	/**
	 * @param renewalOperationDetail the renewalOperationDetail to set
	 */
	public void setRenewalOperationDetail(
			List<RenewalOperationDetail> renewalOperationDetail) {
		this.renewalOperationDetail = renewalOperationDetail;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(custodyOperation != null)
            custodyOperation.setAudit(loggerUser);                        
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("renewalOperationDetail", renewalOperationDetail);
        return detailsMap;
	}

}
