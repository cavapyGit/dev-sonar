package com.pradera.model.custody.physical;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the CERTIFICATE_SITUATION_HISTORY database table.
 * 
 */
@Entity
@Table(name="CERTIFICATE_SITUATION_HISTORY")
public class CertificateSituationHistory implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CERTIFICATE_SITUATION_HISTORY_GENERATOR", sequenceName="SQ_ID_CERTIFICATE_HISTORY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CERTIFICATE_SITUATION_HISTORY_GENERATOR")
	@Column(name="ID_SITUATION_HISTORY_PK")
	private Long idSituationHistoryPk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="NEW_SITUATION")
	private Integer newSituation;

	@Column(name="OLD_SITUATION")
	private Integer oldSituation;

	@Column(name="REGISTER_USER")
	private String registerUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="SITUATION_DATE")
	private Date situationDate;

	//bi-directional many-to-one association to PhysicalCertificate
    @ManyToOne
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;

    public CertificateSituationHistory() {
    }

	public Long getIdSituationHistoryPk() {
		return this.idSituationHistoryPk;
	}

	public void setIdSituationHistoryPk(Long idSituationHistoryPk) {
		this.idSituationHistoryPk = idSituationHistoryPk;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getNewSituation() {
		return this.newSituation;
	}

	public void setNewSituation(Integer newSituation) {
		this.newSituation = newSituation;
	}


	/**
	 * @return the oldSituation
	 */
	public Integer getOldSituation() {
		return oldSituation;
	}

	/**
	 * @param oldSituation the oldSituation to set
	 */
	public void setOldSituation(Integer oldSituation) {
		this.oldSituation = oldSituation;
	}

	public String getRegisterUser() {
		return this.registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getSituationDate() {
		return this.situationDate;
	}

	public void setSituationDate(Date situationDate) {
		this.situationDate = situationDate;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return this.physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
	
}