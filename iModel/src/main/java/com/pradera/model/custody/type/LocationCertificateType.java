/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.custody.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public enum LocationCertificateType {
	CEV_OPERATIONS(new Integer(1),"CEVALDON OPERACIONES"),
	CEV_VAULT(new Integer(2),"CEVALDOM BOVEDA"),
	CEV_MESSENGER(new Integer(3),"CEVALDOM MENSAJERO");
	
	private Integer code;
	private String value;
	public static final List<LocationCertificateType> list = new ArrayList<LocationCertificateType>();
	public static final Map<Integer, LocationCertificateType> lookup = new HashMap<Integer, LocationCertificateType>();
	public static List<LocationCertificateType> listSomeElements(LocationCertificateType... transferSecuritiesTypeParams){
		List<LocationCertificateType> retorno = new ArrayList<LocationCertificateType>();
		for(LocationCertificateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (LocationCertificateType s : EnumSet.allOf(LocationCertificateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private LocationCertificateType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	public static LocationCertificateType get(Integer codigo) {
		return lookup.get(codigo);
	}
}