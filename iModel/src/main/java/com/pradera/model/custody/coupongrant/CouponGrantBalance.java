package com.pradera.model.custody.coupongrant;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the CHANGE_OWNERSHIP_OPERATION database table.
 * 
 */
@Entity
@Table(name="COUPON_GRANT_BALANCE")
public class CouponGrantBalance implements Serializable,Auditable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="COUPON_GRANT_BALANCE_GENERATOR", sequenceName="SQ_ID_COUPON_GRANT_BALANCE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COUPON_GRANT_BALANCE_GENERATOR")
	@Column(name="ID_COUPON_GRANT_BALANCE_PK")
	private Long idCouponGrantBalancePk;
	
	//bi-directional many-to-one association to sourceHolderAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
    //bi-directional many-to-one association to security
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;
    
	//bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
    
    //bi-directional many-to-one association to programInterestCoupon
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_PROGRAM_INTEREST_FK")
  	private ProgramInterestCoupon programInterestCoupon;

    @Column(name="GRANTED_BALANCE")
	private BigDecimal grantedBalance;
    
    @Column(name="BALANCE_TYPE")
	private Long balanceType;
    
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	public CouponGrantBalance() {
		this.grantedBalance= BigDecimal.ZERO;
    }

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        return null;
	}

	/**
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * @param participant the participant to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the programInterestCoupon
	 */
	public ProgramInterestCoupon getProgramInterestCoupon() {
		return programInterestCoupon;
	}

	/**
	 * @param programInterestCoupon the programInterestCoupon to set
	 */
	public void setProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon) {
		this.programInterestCoupon = programInterestCoupon;
	}

	/**
	 * @return the grantedBalance
	 */
	public BigDecimal getGrantedBalance() {
		return grantedBalance;
	}

	/**
	 * @param grantedBalance the grantedBalance to set
	 */
	public void setGrantedBalance(BigDecimal grantedBalance) {
		this.grantedBalance = grantedBalance;
	}

	/**
	 * @return the balanceType
	 */
	public Long getBalanceType() {
		return balanceType;
	}

	/**
	 * @param balanceType the balanceType to set
	 */
	public void setBalanceType(Long balanceType) {
		this.balanceType = balanceType;
	}

	/**
	 * @return the idCouponGrantBalancePk
	 */
	public Long getIdCouponGrantBalancePk() {
		return idCouponGrantBalancePk;
	}

	/**
	 * @param idCouponGrantBalancePk the idCouponGrantBalancePk to set
	 */
	public void setIdCouponGrantBalancePk(Long idCouponGrantBalancePk) {
		this.idCouponGrantBalancePk = idCouponGrantBalancePk;
	}

	/**
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

}