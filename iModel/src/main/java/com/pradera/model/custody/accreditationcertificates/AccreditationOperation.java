package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationDestinationType;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.PetitionerType;
import com.pradera.model.issuancesecuritie.Security;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccreditationOperation.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/04/2013
 */
@Entity
@Table(name="ACCREDITATION_OPERATION")
public class AccreditationOperation implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id accreditation operation pk. */
	@Id		
	private Long idAccreditationOperationPk;
	
	/** The custody operation. */
	@MapsId
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_ACCREDITATION_OPERATION_PK")
	private CustodyOperation custodyOperation;

    /** The accreditation certificate. */
    @Lob
    @Basic(fetch=FetchType.LAZY)
	@Column(name="ACCREDITATION_CERTIFICATE")
	private byte[] accreditationCertificate;

    /** The certification file. */
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="CERTIFICATION_FILE")
	private byte[] certificationFile;

	/** The certification type. */
	@Column(name="CERTIFICATION_TYPE")
	private Integer certificationType;

    /** The expiration date. */
    @Temporal( TemporalType.DATE)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;
    
    @Temporal(TemporalType.DATE)
    @Column(name="ISSUANCE_DATE")
    private Date issuanceDate;
    
    @Column(name="VALIDITY_DAYS")
    private Integer validityDays;
	/** The holder. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	/** The participant petitioner. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_PETITIONER_FK", referencedColumnName="ID_PARTICIPANT_PK") 
	private Participant participantPetitioner;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The observations. */
	@Column(name="COMMENTS")
	private String observations;

	/** The petitioner type. */
	@Column(name="PETITIONER_TYPE")
	private Integer petitionerType;

    /** The register date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;
    
    /** The way of payment. */
    @Column(name="WAY_OF_PAYMENT")
    private Integer wayOfPayment;
    
    /** The ind delivered. */
    @Column(name="IND_DELIVERED")
    private Integer indDelivered;
    /** The ind delivered. */
    @Column(name="IND_GENERATED")
    private Integer indGenerated;
    
    /** The destination certificate. */
    @Column(name="DESTINATION_CERTIFICATE")
    private Integer destinationCertificate;
    
    /** The id signature certificate fk. */
    @Column(name="ID_SIGNATURE_CERTIFICATE_FK")
    private Integer idSignatureCertificateFk;
    
    /** The id reject motive fk. */
    @Column(name="ID_REJECT_MOTIVE")
    private Integer idRejectMotive;
    
    /** The reject motive other. */
    @Column(name="REJECT_MOTIVE_OTHER")
    private String rejectMotiveOther;
    
    @Column(name="CERTIFICATE_NUMBER")
	private Integer certificateNumber;
    
    
	@Temporal(TemporalType.DATE)
   	@Column(name="DELIVERY_DATE")
   	private Date deliveryDate;
      
	@Temporal(TemporalType.DATE)
   	@Column(name="GENERATED_DATE")
   	private Date generatedDate;
    /** The end date. */
    @Transient
    private Date endDate;
    
    /** The is selected. */
    @Transient
	private Boolean isSelected;
    @Transient
	private Boolean isGenerated;

	
    /** The accreditation state. */
	@Column(name="ACCREDITATION_STATE")
	private Integer accreditationState;
	
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	@Column(name="UNBLOCK_OBSERVATION")
	private String unblockObservation;
	
	@Column(name="SUSTENT_FILE_NAME")
	private String sustentFileName;
	
	@Column(name="MOTIVE")
	private Integer motive;
	
	@Column(name="CAT_TYPE")
	private Integer catType;
	
	/** The accreditation state. */
	@Column(name="SECURITY_CODE_CAT")
	private String securityCodeCat;

    @Transient
	private String strGenerated;
    @Transient
	private String strDelivered;
    
    @Transient
    private Security security;
	
    @Transient
    private String  formatSecurities;
    
    @Temporal(TemporalType.TIMESTAMP)
   	@Column(name="REAL_EXPIRATION_DATE")
   	private Date realExpirationDate;
    
	public String getStrGenerated() {
		return strGenerated;
	}
	public void setStrGenerated(String strGenerated) {
		this.strGenerated = strGenerated;
	}
	public String getStrDelivered() {
		return strDelivered;
	}
	public void setStrDelivered(String strDelivered) {
		this.strDelivered = strDelivered;
	}
	public Integer getUnblockMotive() {
		return unblockMotive;
	}
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}
	public String getUnblockObservation() {
		return unblockObservation;
	}
	public void setUnblockObservation(String unblockObservation) {
		this.unblockObservation = unblockObservation;
	}
	//bi-directional many-to-one association to AccreditationDetail
	/** The accreditation details. */
	@OneToMany(mappedBy="accreditationOperation", cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<AccreditationDetail> accreditationDetails;
	
	@OneToMany(mappedBy="accreditationOperation", cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<AccreditationDeliveryInformation> listAccreditationDeliveryInformation;
	
	@Transient
	private Long idBlockEntity;
	@Transient
	private String blockEntityName;
	
	@Version
    private Long version;
	
    /**
     * Instantiates a new accreditation operation.
     */
    public AccreditationOperation() {
    }
    public Integer getIndGenerated() {
		return indGenerated;
	}

	public void setIndGenerated(Integer indGenerated) {
		this.indGenerated = indGenerated;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	/**
	 * Gets the accreditation certificate.
	 *
	 * @return the accreditation certificate
	 */
	public byte[] getAccreditationCertificate() {
		return this.accreditationCertificate;
	}

	/**
	 * Sets the accreditation certificate.
	 *
	 * @param accreditationCertificate the new accreditation certificate
	 */
	public void setAccreditationCertificate(byte[] accreditationCertificate) {
		this.accreditationCertificate = accreditationCertificate;
	}

	/**
	 * Gets the certification file.
	 *
	 * @return the certification file
	 */
	public byte[] getCertificationFile() {
		return this.certificationFile;
	}

	/**
	 * Sets the certification file.
	 *
	 * @param certificationFile the new certification file
	 */
	public void setCertificationFile(byte[] certificationFile) {
		this.certificationFile = certificationFile;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return this.expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return this.observations;
	}

	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return this.registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the participant petitioner.
	 *
	 * @return the participant petitioner
	 */
	public Participant getParticipantPetitioner() {
		return participantPetitioner;
	}

	/**
	 * Sets the participant petitioner.
	 *
	 * @param participantPetitioner the new participant petitioner
	 */
	public void setParticipantPetitioner(Participant participantPetitioner) {
		this.participantPetitioner = participantPetitioner;
	}

	/**
	 * Gets the accreditation details.
	 *
	 * @return the accreditation details
	 */
	public List<AccreditationDetail> getAccreditationDetails() {
		return accreditationDetails;
	}

	/**
	 * Sets the accreditation details.
	 *
	 * @param accreditationDetails the new accreditation details
	 */
	public void setAccreditationDetails(
			List<AccreditationDetail> accreditationDetails) {
		this.accreditationDetails = accreditationDetails;
	}

	/**
	 * Gets the certification type.
	 *
	 * @return the certification type
	 */
	public Integer getCertificationType() {
		return certificationType;
	}

	/**
	 * Sets the certification type.
	 *
	 * @param certificationType the new certification type
	 */
	public void setCertificationType(Integer certificationType) {
		this.certificationType = certificationType;
	}

	/**
	 * Gets the petitioner type.
	 *
	 * @return the petitioner type
	 */
	public Integer getPetitionerType() {
		return petitionerType;
	}

	public String getSustentFileName() {
		return sustentFileName;
	}
	public void setSustentFileName(String sustentFileName) {
		this.sustentFileName = sustentFileName;
	}
	/**
	 * Sets the petitioner type.
	 *
	 * @param petitionerType the new petitioner type
	 */
	public void setPetitionerType(Integer petitionerType) {
		this.petitionerType = petitionerType;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the custody operation.
	 *
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * Sets the custody operation.
	 *
	 * @param custodyOperation the new custody operation
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * Gets the id accreditation operation pk.
	 *
	 * @return the id accreditation operation pk
	 */
	public Long getIdAccreditationOperationPk() {
		return idAccreditationOperationPk;
	}

	/**
	 * Sets the id accreditation operation pk.
	 *
	 * @param idAccreditationOperationPk the new id accreditation operation pk
	 */
	public void setIdAccreditationOperationPk(Long idAccreditationOperationPk) {
		this.idAccreditationOperationPk = idAccreditationOperationPk;
	}

	/**
	 * Gets the way of payment.
	 *
	 * @return the way of payment
	 */
	public Integer getWayOfPayment() {
		return wayOfPayment;
	}

	/**
	 * Sets the way of payment.
	 *
	 * @param wayOfPayment the new way of payment
	 */
	public void setWayOfPayment(Integer wayOfPayment) {
		this.wayOfPayment = wayOfPayment;
	}

	/**
	 * Gets the ind delivered.
	 * 
	 *
	 * @return the ind delivered
	 */
	public Integer getIndDelivered() {
		return indDelivered;
	}

	/**
	 * Sets the ind delivered.
	 *
	 * @param indDelivered the new ind delivered
	 */
	public void setIndDelivered(Integer indDelivered) {
		this.indDelivered = indDelivered;
	}

	/**
	 * Gets the destination certificate.
	 *
	 * @return the destination certificate
	 */
	public Integer getDestinationCertificate() {
		return destinationCertificate;
	}

	/**
	 * Sets the destination certificate.
	 *
	 * @param destinationCertificate the new destination certificate
	 */
	public void setDestinationCertificate(Integer destinationCertificate) {
		this.destinationCertificate = destinationCertificate;
	}

	/**
	 * Gets the accreditation state.
	 *
	 * @return the accreditation state
	 */
	public Integer getAccreditationState() {
		return accreditationState;
	}

	/**
	 * Sets the accreditation state.
	 *
	 * @param accreditationState the new accreditation state
	 */
	public void setAccreditationState(Integer accreditationState) {
		this.accreditationState = accreditationState;
	}

	/**
	 * Gets the checks if is selected.
	 *
	 * @return the checks if is selected
	 */
	public Boolean getIsSelected() {
		return isSelected;
	}

	/**
	 * Sets the checks if is selected.
	 *
	 * @param isSelected the new checks if is selected
	 */
	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	/**
	 * Gets the id signature certificate fk.
	 *
	 * @return the id signature certificate fk
	 */
	public Integer getIdSignatureCertificateFk() {
		return idSignatureCertificateFk;
	}

	/**
	 * Sets the id signature certificate fk.
	 *
	 * @param idSignatureCertificateFk the new id signature certificate fk
	 */
	public void setIdSignatureCertificateFk(Integer idSignatureCertificateFk) {
		this.idSignatureCertificateFk = idSignatureCertificateFk;
	}

	/**
	 * Gets the id reject motive fk.
	 *
	 * @return the id reject motive fk
	 */
	public Integer getIdRejectMotive() {
		return idRejectMotive;
	}

	/**
	 * Sets the id reject motive fk.
	 *
	 * @param idRejectMotiveFk the new id reject motive fk
	 */
	public void setIdRejectMotive(Integer idRejectMotive) {
		this.idRejectMotive = idRejectMotive;
	}

	/**
	 * Gets the reject motive other.
	 *
	 * @return the reject motive other
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the new reject motive other
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}
	public String getPetitionerTypeDesc(){
		if(this.petitionerType!=null)
			return PetitionerType.get(petitionerType).getValue();
		else return "";
	}
	public String getStateTypeDesc(){
		if(this.accreditationState!=null)
			return AccreditationOperationStateType.get(accreditationState).getValue();
		else return "";
	}
	public String getDescriptionDestinationType(){
    	if(Validations.validateIsNotNullAndPositive(destinationCertificate)){
    		return AccreditationDestinationType.get(destinationCertificate).getValue();
    	} else {
    		return "" ;
    	}
    }
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(custodyOperation != null){
            	custodyOperation.setAudit(loggerUser);
            }
        }
	}
	
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("accreditationDetails", accreditationDetails);
        detailsMap.put("listAccreditationDeliveryInformation",listAccreditationDeliveryInformation);
        return detailsMap;
	}


	public Date getGeneratedDate() {
		return generatedDate;
	}
	public void setGeneratedDate(Date generatedDate) {
		this.generatedDate = generatedDate;
	}
	public Boolean getIsGenerated() {
		return isGenerated;
	}
	public void setIsGenerated(Boolean isGenerated) {
		this.isGenerated = isGenerated;
	}
	public Long getIdBlockEntity() {
		return idBlockEntity;
	}
	public void setIdBlockEntity(Long idBlockEntity) {
		this.idBlockEntity = idBlockEntity;
	}
	public String getBlockEntityName() {
		return blockEntityName;
	}
	public void setBlockEntityName(String blockEntityName) {
		this.blockEntityName = blockEntityName;
	}
	public Integer getMotive() {
		return motive;
	}
	public void setMotive(Integer motive) {
		this.motive = motive;
	}
	public Security getSecurity() {
		return security;
	}
	public void setSecurity(Security security) {
		this.security = security;
	}
	public String getFormatSecurities() {
		return formatSecurities;
	}
	public void setFormatSecurities(String formatSecurities) {
		this.formatSecurities = formatSecurities;
	}
	public List<AccreditationDeliveryInformation> getListAccreditationDeliveryInformation() {
		return listAccreditationDeliveryInformation;
	}
	public void setListAccreditationDeliveryInformation(
			List<AccreditationDeliveryInformation> listAccreditationDeliveryInformation) {
		this.listAccreditationDeliveryInformation = listAccreditationDeliveryInformation;
	}
	public Date getIssuanceDate() {
		return issuanceDate;
	}
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	public Integer getValidityDays() {
		return validityDays;
	}
	public void setValidityDays(Integer validityDays) {
		this.validityDays = validityDays;
	}
	public Integer getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(Integer certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public Date getRealExpirationDate() {
		return realExpirationDate;
	}
	public void setRealExpirationDate(Date realExpirationDate) {
		this.realExpirationDate = realExpirationDate;
	}
	/**
	 * @return the securityCodeCat
	 */
	public String getSecurityCodeCat() {
		return securityCodeCat;
	}
	/**
	 * @param securityCodeCat the securityCodeCat to set
	 */
	public void setSecurityCodeCat(String securityCodeCat) {
		this.securityCodeCat = securityCodeCat;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public Integer getCatType() {
		return catType;
	}
	public void setCatType(Integer catType) {
		this.catType = catType;
	}
	
	
	
	
}
