package com.pradera.model.custody.channelopening.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum ChannelOpeningStateType {
	
	REGISTRADO(Integer.valueOf(2472),"ESTADO REGISTRADO DE UNA APERTURA DE CANAL"),
	APROBADO(Integer.valueOf(2473),"ESTADO APROBADO DE UNA APERTURA DE CANAL"),
	ANULADO(Integer.valueOf(2474),"ESTADO ANULADO DE UNA APERTURA DE CANAL"),
	CONFIRMADO(Integer.valueOf(2475),"ESTADO CONFIRMADO DE UNA APERTURA DE CANAL"),
	RECHAZADO(Integer.valueOf(2476),"ESTADO RECHAZADO DE UNA APERTURA DE CANAL"),
	VENCIDO(Integer.valueOf(2477),"ESTADO VENCIDO DE UNA APERTURA DE CANAL"),

	;
	
	/** The Constant list. */
	public static final List<ChannelOpeningStateType> list = new ArrayList<ChannelOpeningStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ChannelOpeningStateType> lookup = new HashMap<Integer, ChannelOpeningStateType>();
	static {
		for (ChannelOpeningStateType s : EnumSet.allOf(ChannelOpeningStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ChannelOpeningStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static ChannelOpeningStateType get(Integer code) {
		return lookup.get(code);
	}
}
