package com.pradera.model.custody.blockenforce;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.transfersecurities.CustodyOperation;


/**
 * The persistent class for the BLOCK_ENFORCE_OPERATION database table.
 * 
 */
@Entity
@Table(name="BLOCK_ENFORCE_OPERATION")
@NamedQuery(name="BlockEnforceOperation.findAll", query="SELECT b FROM BlockEnforceOperation b")
public class BlockEnforceOperation implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long idEnforceOperationPk;
	
	@MapsId
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_ENFORCE_OPERATION_PK")
	private CustodyOperation custodyOperation;

	@Column(name="ENFORCE_FORM")
	private Integer enforceForm;

	@Column(name="ENFORCE_TYPE")
	private Integer enforceType;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_PARTICIPANT_FK")
	private Participant participantTarget;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OBSERVATIONS")
	private String observations;

	@Column(name="OPERATION_NUMBER")
	private Long operationNumber;

	@Column(name="OPERATION_STATE")
	private Integer operationState;

	@Temporal(TemporalType.DATE)
	@Column(name="RESOLUTION_DATE")
	private Date resolutionDate;

	@Column(name="RESOLUTION_NUMBER")
	private String resolutionNumber;

	//bi-directional many-to-one association to BlockEnforceDetail
	@OneToMany(mappedBy="blockEnforceOperation",fetch=FetchType.LAZY)
	private List<BlockEnforceDetail> blockEnforceDetails;

	//bi-directional many-to-one association to BlockEnforceFile
	@OneToMany(mappedBy="blockEnforceOperation",fetch=FetchType.LAZY)
	private List<BlockEnforceFile> blockEnforceFiles;

	//bi-directional many-to-one association to BlockEntity
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_ENTITY_FK")
	private BlockEntity blockEntity;

	//bi-directional many-to-one association to Holder
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOURCE_HOLDER_FK")
	private Holder holderSource;

	//bi-directional many-to-one association to Holder
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_HOLDER_FK")
	private Holder holderTarget;

	//bi-directional many-to-one association to HolderAccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@Column(name="IND_BLOCKENTITY_CREDITOR")
	private Integer indBlockEntityCreditor;
	
	@Transient
	private Boolean isSelected;
	
	@Transient
	private byte[] file;
	
	@Transient
	private String fileName;
	
	@Transient
	/** The show motive text. */
	private boolean showMotiveText;

	@Transient
	/** The request motive. */
	private Integer requestMotive;

	@Transient
	/** The request other motive. */
	private String requestOtherMotive;
	
	@Transient
	private String stateDescription;

	public BlockEnforceOperation() {
	}

	public Long getIdEnforceOperationPk() {
		return this.idEnforceOperationPk;
	}

	public void setIdEnforceOperationPk(Long idEnforceOperationPk) {
		this.idEnforceOperationPk = idEnforceOperationPk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getObservations() {
		return this.observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public Date getResolutionDate() {
		return this.resolutionDate;
	}

	public void setResolutionDate(Date resolutionDate) {
		this.resolutionDate = resolutionDate;
	}

	public String getResolutionNumber() {
		return this.resolutionNumber;
	}

	public void setResolutionNumber(String resolutionNumber) {
		this.resolutionNumber = resolutionNumber;
	}

	public List<BlockEnforceDetail> getBlockEnforceDetails() {
		return this.blockEnforceDetails;
	}

	public void setBlockEnforceDetails(List<BlockEnforceDetail> blockEnforceDetails) {
		this.blockEnforceDetails = blockEnforceDetails;
	}

	public BlockEnforceDetail addBlockEnforceDetail(BlockEnforceDetail blockEnforceDetail) {
		getBlockEnforceDetails().add(blockEnforceDetail);
		blockEnforceDetail.setBlockEnforceOperation(this);

		return blockEnforceDetail;
	}

	public BlockEnforceDetail removeBlockEnforceDetail(BlockEnforceDetail blockEnforceDetail) {
		getBlockEnforceDetails().remove(blockEnforceDetail);
		blockEnforceDetail.setBlockEnforceOperation(null);

		return blockEnforceDetail;
	}

	public List<BlockEnforceFile> getBlockEnforceFiles() {
		return this.blockEnforceFiles;
	}

	public void setBlockEnforceFiles(List<BlockEnforceFile> blockEnforceFiles) {
		this.blockEnforceFiles = blockEnforceFiles;
	}

	public BlockEnforceFile addBlockEnforceFile(BlockEnforceFile blockEnforceFile) {
		getBlockEnforceFiles().add(blockEnforceFile);
		blockEnforceFile.setBlockEnforceOperation(this);

		return blockEnforceFile;
	}

	public BlockEnforceFile removeBlockEnforceFile(BlockEnforceFile blockEnforceFile) {
		getBlockEnforceFiles().remove(blockEnforceFile);
		blockEnforceFile.setBlockEnforceOperation(null);

		return blockEnforceFile;
	}

	public BlockEntity getBlockEntity() {
		return this.blockEntity;
	}

	public void setBlockEntity(BlockEntity blockEntity) {
		this.blockEntity = blockEntity;
	}

	public CustodyOperation getCustodyOperation() {
		return this.custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public HolderAccount getHolderAccount() {
		return this.holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Holder getHolderSource() {
		return holderSource;
	}

	public void setHolderSource(Holder holderSource) {
		this.holderSource = holderSource;
	}

	public Holder getHolderTarget() {
		return holderTarget;
	}

	public void setHolderTarget(Holder holderTarget) {
		this.holderTarget = holderTarget;
	}

	public Integer getIndBlockEntityCreditor() {
		return indBlockEntityCreditor;
	}

	public void setIndBlockEntityCreditor(Integer indBlockEntityCreditor) {
		this.indBlockEntityCreditor = indBlockEntityCreditor;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Participant getParticipantTarget() {
		return participantTarget;
	}

	public void setParticipantTarget(Participant participantTarget) {
		this.participantTarget = participantTarget;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Long getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("blockEnforceDetails", blockEnforceDetails);
		detailsMap.put("blockEnforceFiles", blockEnforceFiles);
		return detailsMap;
	}

	public Integer getEnforceForm() {
		return enforceForm;
	}

	public void setEnforceForm(Integer enforceForm) {
		this.enforceForm = enforceForm;
	}

	public Integer getEnforceType() {
		return enforceType;
	}

	public void setEnforceType(Integer enforceType) {
		this.enforceType = enforceType;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}

	public Integer getRequestMotive() {
		return requestMotive;
	}

	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	public String getRequestOtherMotive() {
		return requestOtherMotive;
	}

	public void setRequestOtherMotive(String requestOtherMotive) {
		this.requestOtherMotive = requestOtherMotive;
	}

	public String getStateDescription() {
		return stateDescription;
	}

	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	
	
	
}