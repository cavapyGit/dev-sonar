package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum SituationType {
	/*
	 lbl.accreditation.certificate.petitioner_type.participant=PARTICIPANTE
	 location.cevaldom.oficina=CEVALDOM OFICINA
	 location.cevaldom.boveda=CEVALDOM BOVEDA
	 location.cevaldom.mensajero=CEVALDOM MENSAJERO
	 location.bcrd.boveda=BCRD BOVEDA
	*/
	PARTICIPANT(new Integer(607)),//PARTICIPANTE
	DEPOSITARY_OPERATIONS(new Integer(608)),//DEPOSITARIA OPERACIONES	- EDV
	DEPOSITARY_VAULT(new Integer(609)),//BOVEDA DEPOSITO - EDV BOVEDA
	DEPOSITARY_MESSENGER(new Integer(610)),//MENSAJERO DEPOSITO
	CUSTODY_VAULT(new Integer(611)),
	OTHER (new Integer(0));//BOVEDA BANCO CUSTODIA
	

	private Integer code;
	
	public static final List<SituationType> list = new ArrayList<SituationType>();
	public static final Map<Integer, SituationType> lookup = new HashMap<Integer, SituationType>();

	static {
		for (SituationType s : EnumSet.allOf(SituationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	private SituationType(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public static SituationType get(Integer code) {
		return lookup.get(code);
	}


}
