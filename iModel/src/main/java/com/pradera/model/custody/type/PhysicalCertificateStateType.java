package com.pradera.model.custody.type;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum PhysicalCertificateStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Feb 11, 2014
 */
public enum PhysicalCertificateStateType {

	REGISTERED(Integer.valueOf(597),"REGISTRADO"),

	APPROVED(Integer.valueOf(598),"APROBADO"),

	ANNULED(Integer.valueOf(599),"ANULADO"),

	REJECTED(Integer.valueOf(600),"RECHAZADO"),

	CONFIRMED(Integer.valueOf(601),"CONFIRMADO"),

	AUTHORIZED(Integer.valueOf(602),"AUTORIZADO"),

	RETIREMENT_REQUEST(Integer.valueOf(2844),"SOLICITUD RETIRO"),

	RETIREMENT(Integer.valueOf(2843),"RETIRADA");
 
	/** The Constant list. */
	public static final List<PhysicalCertificateStateType> list=new ArrayList<PhysicalCertificateStateType>();

	/** The Constant lookup. */
	public static final Map<Integer, PhysicalCertificateStateType> lookup=new HashMap<Integer, PhysicalCertificateStateType>();

	static {
		for(PhysicalCertificateStateType e : PhysicalCertificateStateType.values()){
			lookup.put(e.getCode(), e);
			list.add( e );
		}
	}

	/** The code. */
	private Integer  code;

	/** The value. */
	private String value;


	/**
	 * Instantiates a new security state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PhysicalCertificateStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the security state type
	 */
	public static PhysicalCertificateStateType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
