package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TransferSecuritiesStateType {

	REGISTRADO(new Integer(166),"REGISTRADO"),
	APROBADO(new Integer(746),"APROBADO"),
	ANULADO(new Integer(747),"ANULADO"),
	REVISADO(new Integer(748),"REVISADO"),
	CONFIRMADO(new Integer(749),"CONFIRMADO"),
	RECHAZADO(new Integer(750),"RECHAZADO"),
	RECTIFIED(new Integer(2277),"RECTIFICADO");
	
	private Integer code;
	private String value;
	
	public static final List<TransferSecuritiesStateType> list = new ArrayList<TransferSecuritiesStateType>();
	
	public static final Map<Integer, TransferSecuritiesStateType> lookup = new HashMap<Integer, TransferSecuritiesStateType>();
	
	public static List<TransferSecuritiesStateType> listSomeElements(TransferSecuritiesStateType... transferSecuritiesTypeParams){
		List<TransferSecuritiesStateType> retorno = new ArrayList<TransferSecuritiesStateType>();
		for(TransferSecuritiesStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (TransferSecuritiesStateType s : EnumSet.allOf(TransferSecuritiesStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	private TransferSecuritiesStateType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
