/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.custody.securitiesannotation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "ACC_ANNOTATION_OPERATION_FILE")
@NamedQueries({
    @NamedQuery(name = "AccAnnotationOperationFile.findAll", query = "SELECT a FROM AccAnnotationOperationFile a"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByIdFilePk", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.idFilePk = :idFilePk"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByFileName", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.fileName = :fileName"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByFileType", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.fileType = :fileType"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByRegistryUser", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.registryUser = :registryUser"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByRegistryDate", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.registryDate = :registryDate"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByRegistryIp", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.registryIp = :registryIp"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByAttachedName", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.attachedName = :attachedName"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByAttachedType", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.attachedType = :attachedType"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByRegistryUserAtt", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.registryUserAtt = :registryUserAtt"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByRegistryDateAtt", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.registryDateAtt = :registryDateAtt"),
    @NamedQuery(name = "AccAnnotationOperationFile.findByRegistryIpAtt", query = "SELECT a FROM AccAnnotationOperationFile a WHERE a.registryIpAtt = :registryIpAtt")})
public class AccAnnotationOperationFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
	@SequenceGenerator(name="SQ_ID_ANNOTATION_FILE_PK", sequenceName="SQ_ID_ANNOTATION_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_ANNOTATION_FILE_PK")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_FILE_PK", nullable = false)
    private Long idFilePk;
    @Size(max = 100)
    @Column(name = "FILE_NAME", length = 100)
    private String fileName;
    @Size(max = 20)
    @Column(name = "FILE_TYPE", length = 20)
    private String fileType;
    @Lob
    @Column(name = "FILE_CONTENT")
    private byte[] fileContent;
    @Size(max = 20)
    @Column(name = "REGISTRY_USER", length = 20)
    private String registryUser;
    @Column(name = "REGISTRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Size(max = 20)
    @Column(name = "REGISTRY_IP", length = 20)
    private String registryIp;
    @Size(max = 100)
    @Column(name = "ATTACHED_NAME", length = 100)
    private String attachedName;
    @Size(max = 20)
    @Column(name = "ATTACHED_TYPE", length = 20)
    private String attachedType;
    @Lob
    @Column(name = "ATTACHED_CONTENT")
    private byte[] attachedContent;
    @Size(max = 20)
    @Column(name = "REGISTRY_USER_ATT", length = 20)
    private String registryUserAtt;
    @Column(name = "REGISTRY_DATE_ATT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDateAtt;
    @Size(max = 20)
    @Column(name = "REGISTRY_IP_ATT", length = 20)
    private String registryIpAtt;
    @OneToMany(mappedBy = "idAccAnnotationFileFk", fetch = FetchType.LAZY)
    private List<AccountAnnotationOperation> accountAnnotationOperationList;
    
	/** true: registro masivo mediante archivo Excel false: registro simple */
	@Transient
	private boolean massiveRegister;
	
	@Transient
	private String nameFileExcel;
	@Transient
	private String nameFileAttached;

	public AccAnnotationOperationFile() {
		this.massiveRegister = false;
	}

    public AccAnnotationOperationFile(Long idFilePk) {
        this.idFilePk = idFilePk;
    }

    public Long getIdFilePk() {
        return idFilePk;
    }

    public void setIdFilePk(Long idFilePk) {
        this.idFilePk = idFilePk;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

	/** true: registro masivo mediante archivo Excel false: registro simple
	 * 
	 * @return */
	public boolean isMassiveRegister() {
		return massiveRegister;
	}

	/** true: registro masivo mediante archivo Excel false: registro simple
	 * 
	 * @param massiveRegister */
	public void setMassiveRegister(boolean massiveRegister) {
		this.massiveRegister = massiveRegister;
	}

	public String getFileType() {
        return fileType;
    }

    public String getNameFileExcel() {
		return nameFileExcel;
	}

	public void setNameFileExcel(String nameFileExcel) {
		this.nameFileExcel = nameFileExcel;
	}

	public String getNameFileAttached() {
		return nameFileAttached;
	}

	public void setNameFileAttached(String nameFileAttached) {
		this.nameFileAttached = nameFileAttached;
	}

	public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Serializable getFileContent() {
        return fileContent;
    }

    public String getRegistryUser() {
        return registryUser;
    }

    public void setRegistryUser(String registryUser) {
        this.registryUser = registryUser;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public String getRegistryIp() {
        return registryIp;
    }

    public void setRegistryIp(String registryIp) {
        this.registryIp = registryIp;
    }

    public String getAttachedName() {
        return attachedName;
    }

    public void setAttachedName(String attachedName) {
        this.attachedName = attachedName;
    }

    public String getAttachedType() {
        return attachedType;
    }

    public void setAttachedType(String attachedType) {
        this.attachedType = attachedType;
    }

    public Serializable getAttachedContent() {
        return attachedContent;
    }

    public String getRegistryUserAtt() {
        return registryUserAtt;
    }

    public void setRegistryUserAtt(String registryUserAtt) {
        this.registryUserAtt = registryUserAtt;
    }

    public Date getRegistryDateAtt() {
        return registryDateAtt;
    }

    public void setRegistryDateAtt(Date registryDateAtt) {
        this.registryDateAtt = registryDateAtt;
    }

    public String getRegistryIpAtt() {
        return registryIpAtt;
    }

    public void setRegistryIpAtt(String registryIpAtt) {
        this.registryIpAtt = registryIpAtt;
    }

    public List<AccountAnnotationOperation> getAccountAnnotationOperationList() {
        return accountAnnotationOperationList;
    }

    public void setAccountAnnotationOperationList(List<AccountAnnotationOperation> accountAnnotationOperationList) {
        this.accountAnnotationOperationList = accountAnnotationOperationList;
    }

    public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public void setAttachedContent(byte[] attachedContent) {
		this.attachedContent = attachedContent;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idFilePk != null ? idFilePk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccAnnotationOperationFile)) {
            return false;
        }
        AccAnnotationOperationFile other = (AccAnnotationOperationFile) object;
        if ((this.idFilePk == null && other.idFilePk != null) || (this.idFilePk != null && !this.idFilePk.equals(other.idFilePk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.AccAnnotationOperationFile[ idFilePk=" + idFilePk + " ]";
    }
}