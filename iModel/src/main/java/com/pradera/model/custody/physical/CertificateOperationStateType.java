package com.pradera.model.custody.physical;

public enum CertificateOperationStateType  {
	REGISTERED(new Integer(2361)),
	REJECTED(new Integer(2362)),
	CONFIRMED(new Integer(2363)),
	;	
	
	private CertificateOperationStateType(Integer code){
		this.code = code;
	} 
	
	private Integer code;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
