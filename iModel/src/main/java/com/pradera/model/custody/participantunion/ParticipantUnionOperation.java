package com.pradera.model.custody.participantunion;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.transfersecurities.CustodyOperation;

/**
 * The persistent class for the PARTICIPANT_UNION_OPERATION database table.
 * 
 */
@Entity
@Table(name = "PARTICIPANT_UNION_OPERATION")
public class ParticipantUnionOperation implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long idPartUnionOperationPk;
	
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "ID_PART_UNION_OPERATION_PK")
	private CustodyOperation custodyOperation;

	// bi-directional many-to-one association to sourceParticipant
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_SOURCE_PARTICIPANT_FK")
	private Participant sourceParticipant;

	// bi-directional many-to-one association to targetParticipant
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_TARGET_PARTICIPANT_FK")
	private Participant targetParticipant;

	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	private Integer motive;
	@Column(name="COMMENTS")
	private String observations;

	@Column(name = "\"STATE\"")
	private Integer state;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UNION_DATE")
	private Date unionDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "COUNCIL_RESOLUTION_DATE")
	private Date councilResolutionDate;
	

	@Temporal(TemporalType.DATE)
	@Column(name = "SUPER_RESOLUTION_DATE")
	private Date superResolutionDate;
	
	// bi-directional many-to-one association to BlockOperacionUnion
	@OneToMany(mappedBy = "participantUnionOperation", cascade=CascadeType.ALL)
	private List<BlockOperationUnion> blockOperacionUnions;

	// bi-directional many-to-one association to ParticipantUnionBalance
	@OneToMany(mappedBy = "participantUnionOperation", cascade=CascadeType.ALL)
	private List<ParticipantUnionBalance> participantUnionBalances;

	@OneToMany(mappedBy="participantUnionOperation", cascade=CascadeType.PERSIST)
	private List<ParticipantUnionFile> participantUnionFile;

	@Transient
	String stateDescription;
	
	@Transient
	String motiveDescription;

	public ParticipantUnionOperation() {
	}
	
	public Long getIdPartUnionOperationPk() {
		return idPartUnionOperationPk;
	}

	public void setIdPartUnionOperationPk(Long idPartUnionOperationPk) {
		this.idPartUnionOperationPk = idPartUnionOperationPk;
	}


	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public Participant getSourceParticipant() {
		return sourceParticipant;
	}

	public void setSourceParticipant(Participant originParticipant) {
		this.sourceParticipant = originParticipant;
	}

	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getMotive() {
		return this.motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public String getObservations() {
		return this.observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getUnionDate() {
		return this.unionDate;
	}

	public void setUnionDate(Date unionDate) {
		this.unionDate = unionDate;
	}

	public List<BlockOperationUnion> getBlockOperacionUnions() {
		return this.blockOperacionUnions;
	}

	public void setBlockOperacionUnions(
			List<BlockOperationUnion> blockOperacionUnions) {
		this.blockOperacionUnions = blockOperacionUnions;
	}

	public List<ParticipantUnionBalance> getParticipantUnionBalances() {
		return this.participantUnionBalances;
	}

	public void setParticipantUnionBalances(
			List<ParticipantUnionBalance> participantUnionBalances) {
		this.participantUnionBalances = participantUnionBalances;
	}

	public List<ParticipantUnionFile> getParticipantUnionFile() {
		return participantUnionFile;
	}

	public void setParticipantUnionFile(List<ParticipantUnionFile> participantUnionFile) {
		this.participantUnionFile = participantUnionFile;
	}
	public Date getCouncilResolutionDate() {
		return councilResolutionDate;
	}

	public void setCouncilResolutionDate(Date councilResolutionDate) {
		this.councilResolutionDate = councilResolutionDate;
	}

	public Date getSuperResolutionDate() {
		return superResolutionDate;
	}

	public void setSuperResolutionDate(Date superResolutionDate) {
		this.superResolutionDate = superResolutionDate;
	}
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(custodyOperation != null){
            	custodyOperation.setAudit(loggerUser);
            }
        }
	}

	@Override
	 public Map<String, List<? extends Auditable>> getListForAudit() {
	  HashMap<String,List<? extends Auditable>> detailsMap = 
	                new HashMap<String, List<? extends Auditable>>();
	        detailsMap.put("participantUnionFile", participantUnionFile);
	        return detailsMap;
	 }

	public String getStateDescription() {
		return stateDescription;
	}

	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	public String getMotiveDescription() {
		return motiveDescription;
	}

	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}
	
	
}