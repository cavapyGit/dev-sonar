package com.pradera.model.custody.channelopening;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "CHANNEL_OPENING_HISTORY")
@NamedQueries({
    @NamedQuery(name = "ChannelOpeningHistory.findAll", query = "SELECT c FROM ChannelOpeningHistory c"),
    @NamedQuery(name = "ChannelOpeningHistory.findByIdChannelOpeningHisPk", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.idChannelOpeningHisPk = :idChannelOpeningHisPk"),
    @NamedQuery(name = "ChannelOpeningHistory.findByIdParticipantFk", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.idParticipantFk = :idParticipantFk"),
    @NamedQuery(name = "ChannelOpeningHistory.findByDateChannelOpening", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.dateChannelOpening = :dateChannelOpening"),
    @NamedQuery(name = "ChannelOpeningHistory.findByInitialHour", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.initialHour = :initialHour"),
    @NamedQuery(name = "ChannelOpeningHistory.findByFinalHour", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.finalHour = :finalHour"),
    @NamedQuery(name = "ChannelOpeningHistory.findByMotiveChannelOpening", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.motiveChannelOpening = :motiveChannelOpening"),
    @NamedQuery(name = "ChannelOpeningHistory.findByObservationChannelOpening", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.observationChannelOpening = :observationChannelOpening"),
    @NamedQuery(name = "ChannelOpeningHistory.findByRegistryDate", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.registryDate = :registryDate"),
    @NamedQuery(name = "ChannelOpeningHistory.findByMotiveChannelClosure", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.motiveChannelClosure = :motiveChannelClosure"),
    @NamedQuery(name = "ChannelOpeningHistory.findByObservationChannelClosure", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.observationChannelClosure = :observationChannelClosure"),
    @NamedQuery(name = "ChannelOpeningHistory.findByFileName", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.fileName = :fileName"),
    @NamedQuery(name = "ChannelOpeningHistory.findByOperation", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.operation = :operation"),
    @NamedQuery(name = "ChannelOpeningHistory.findByLastModifyUser", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "ChannelOpeningHistory.findByLastModifyDate", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "ChannelOpeningHistory.findByLastModifyIp", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "ChannelOpeningHistory.findByLastModifyApp", query = "SELECT c FROM ChannelOpeningHistory c WHERE c.lastModifyApp = :lastModifyApp")})
public class ChannelOpeningHistory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_CHANNEL_OPENING_HIS", sequenceName="SQ_CHANNEL_OPENING_HIS",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_CHANNEL_OPENING_HIS")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CHANNEL_OPENING_HIS_PK", nullable = false)
    private Long idChannelOpeningHisPk;
    @Column(name = "ID_PARTICIPANT_FK")
    private Long idParticipantFk;
    @Column(name = "DATE_CHANNEL_OPENING")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateChannelOpening;
    @Column(name = "INITIAL_HOUR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date initialHour;
    @Column(name = "FINAL_HOUR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finalHour;
    @Column(name = "MOTIVE_CHANNEL_OPENING")
    private Integer motiveChannelOpening;
    @Size(max = 1000)
    @Column(name = "OBSERVATION_CHANNEL_OPENING", length = 1000)
    private String observationChannelOpening;
    @Column(name = "REGISTRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Column(name = "MOTIVE_CHANNEL_CLOSURE")
    private Integer motiveChannelClosure;
    @Size(max = 1000)
    @Column(name = "OBSERVATION_CHANNEL_CLOSURE", length = 1000)
    private String observationChannelClosure;
    @Lob
    @Column(name = "FILE_ATTACHED")
    private Serializable fileAttached;
    @Size(max = 256)
    @Column(name = "FILE_NAME", length = 256)
    private String fileName;
    @Column(name = "OPERATION")
    private Integer operation;
    @Size(max = 20)
    @Column(name = "LAST_MODIFY_USER", length = 20)
    private String lastModifyUser;
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Size(max = 20)
    @Column(name = "LAST_MODIFY_IP", length = 20)
    private String lastModifyIp;
    @Column(name = "LAST_MODIFY_APP")
    private Long lastModifyApp;
    @JoinColumn(name = "ID_CHANNEL_OPENING_FK", referencedColumnName = "ID_CHANNEL_OPENING_PK")
    @ManyToOne(fetch = FetchType.LAZY)
    private ChannelOpening idChannelOpeningFk;

    public ChannelOpeningHistory() {
    }

    public ChannelOpeningHistory(Long idChannelOpeningHisPk) {
        this.idChannelOpeningHisPk = idChannelOpeningHisPk;
    }

    public Long getIdChannelOpeningHisPk() {
        return idChannelOpeningHisPk;
    }

    public void setIdChannelOpeningHisPk(Long idChannelOpeningHisPk) {
        this.idChannelOpeningHisPk = idChannelOpeningHisPk;
    }

    public Long getIdParticipantFk() {
        return idParticipantFk;
    }

    public void setIdParticipantFk(Long idParticipantFk) {
        this.idParticipantFk = idParticipantFk;
    }

    public Date getDateChannelOpening() {
        return dateChannelOpening;
    }

    public void setDateChannelOpening(Date dateChannelOpening) {
        this.dateChannelOpening = dateChannelOpening;
    }

    public Date getInitialHour() {
        return initialHour;
    }

    public void setInitialHour(Date initialHour) {
        this.initialHour = initialHour;
    }

    public Date getFinalHour() {
        return finalHour;
    }

    public void setFinalHour(Date finalHour) {
        this.finalHour = finalHour;
    }

    public Integer getMotiveChannelOpening() {
        return motiveChannelOpening;
    }

    public void setMotiveChannelOpening(Integer motiveChannelOpening) {
        this.motiveChannelOpening = motiveChannelOpening;
    }

    public String getObservationChannelOpening() {
        return observationChannelOpening;
    }

    public void setObservationChannelOpening(String observationChannelOpening) {
        this.observationChannelOpening = observationChannelOpening;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public Integer getMotiveChannelClosure() {
        return motiveChannelClosure;
    }

    public void setMotiveChannelClosure(Integer motiveChannelClosure) {
        this.motiveChannelClosure = motiveChannelClosure;
    }

    public String getObservationChannelClosure() {
        return observationChannelClosure;
    }

    public void setObservationChannelClosure(String observationChannelClosure) {
        this.observationChannelClosure = observationChannelClosure;
    }

    public Serializable getFileAttached() {
        return fileAttached;
    }

    public void setFileAttached(Serializable fileAttached) {
        this.fileAttached = fileAttached;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getOperation() {
        return operation;
    }

    public void setOperation(Integer operation) {
        this.operation = operation;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public ChannelOpening getIdChannelOpeningFk() {
        return idChannelOpeningFk;
    }

    public void setIdChannelOpeningFk(ChannelOpening idChannelOpeningFk) {
        this.idChannelOpeningFk = idChannelOpeningFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idChannelOpeningHisPk != null ? idChannelOpeningHisPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChannelOpeningHistory)) {
            return false;
        }
        ChannelOpeningHistory other = (ChannelOpeningHistory) object;
        if ((this.idChannelOpeningHisPk == null && other.idChannelOpeningHisPk != null) || (this.idChannelOpeningHisPk != null && !this.idChannelOpeningHisPk.equals(other.idChannelOpeningHisPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.ChannelOpeningHistory[ idChannelOpeningHisPk=" + idChannelOpeningHisPk + " ]";
    }
    
}
