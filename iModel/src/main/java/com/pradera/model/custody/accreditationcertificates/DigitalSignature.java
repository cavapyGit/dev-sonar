package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="DIGITAL_SIGNATURE")
public class DigitalSignature implements Serializable, Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="DIGITAL_SIGNATURE_GENERATOR", sequenceName="SQ_ID_DIGITAL_SIGNATURE", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DIGITAL_SIGNATURE_GENERATOR")
	@Column(name="ID_DIGITAL_SIGNATURE_PK")
	private Long idDigitalSignaturePk;
	
	/** The signature type. */
	@NotNull
	@Column(name="TYPE_SIGNATURE")
	private Integer typeSignature;
	
	/** The active signature. */
	@NotNull
	@Column(name="ACTIVE_SIGNATURE")
	private Integer activeSignature;
	
	/** The office. */
	@Column(name="OFFICE")
	private String office;
	
	/** The name. */
	@Column(name="NAME")
	private String name;
	
	/** The first last name. */
	@Column(name="FIRST_LAST_NAME")
	private String firstLastName;

	/** The second last name. */
	@Column(name="SECOND_LAST_NAME")
	private String secondLastName;
	
	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;
	
	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;
	
	/** The legal address. */
	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;
	
	/** The phone number. */
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;
	
	@Column(name="PASSWORD_SIGNATURE")
	private String passwordSignature;
	
	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;
	
	/** The signature. */
	@Lob()
   	@Column(name="SIGNATURE", updatable=true)
   	private byte[] signature;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Transient
	private String descTypeSignature;
	
	@Transient
	private String descActiveSignature;
	
	
	/** The constructor. */
	public DigitalSignature() {
		super();
	}

	/**
	 * @return the idDigitalSignaturePk
	 */
	public Long getIdDigitalSignaturePk() {
		return idDigitalSignaturePk;
	}

	/**
	 * @param idDigitalSignaturePk the idDigitalSignaturePk to set
	 */
	public void setIdDigitalSignaturePk(Long idDigitalSignaturePk) {
		this.idDigitalSignaturePk = idDigitalSignaturePk;
	}

	/**
	 * @return the typeSignature
	 */
	public Integer getTypeSignature() {
		return typeSignature;
	}

	/**
	 * @param typeSignature the typeSignature to set
	 */
	public void setTypeSignature(Integer typeSignature) {
		this.typeSignature = typeSignature;
	}

	/**
	 * @return the activeSignature
	 */
	public Integer getActiveSignature() {
		return activeSignature;
	}

	/**
	 * @param activeSignature the activeSignature to set
	 */
	public void setActiveSignature(Integer activeSignature) {
		this.activeSignature = activeSignature;
	}

	/**
	 * @return the office
	 */
	public String getOffice() {
		return office;
	}

	/**
	 * @param office the office to set
	 */
	public void setOffice(String office) {
		this.office = office;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the firstLastName
	 */
	public String getFirstLastName() {
		return firstLastName;
	}

	/**
	 * @param firstLastName the firstLastName to set
	 */
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	/**
	 * @return the secondLastName
	 */
	public String getSecondLastName() {
		return secondLastName;
	}

	/**
	 * @param secondLastName the secondLastName to set
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
	 * @return the documentType
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * @return the documentNumber
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * @param documentNumber the documentNumber to set
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * @return the legalAddress
	 */
	public String getLegalAddress() {
		return legalAddress;
	}

	/**
	 * @param legalAddress the legalAddress to set
	 */
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @param faxNumber the faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * @return the signature
	 */
	public byte[] getSignature() {
		return signature;
	}

	/**
	 * @param signature the signature to set
	 */
	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the descTypeSignature
	 */
	public String getDescTypeSignature() {
		return descTypeSignature;
	}

	/**
	 * @param descTypeSignature the descTypeSignature to set
	 */
	public void setDescTypeSignature(String descTypeSignature) {
		this.descTypeSignature = descTypeSignature;
	}

	/**
	 * @return the descActiveSignature
	 */
	public String getDescActiveSignature() {
		return descActiveSignature;
	}

	/**
	 * @param descActiveSignature the descActiveSignature to set
	 */
	public void setDescActiveSignature(String descActiveSignature) {
		this.descActiveSignature = descActiveSignature;
	}
	

	public String getPasswordSignature() {
		return passwordSignature;
	}

	public void setPasswordSignature(String passwordSignature) {
		this.passwordSignature = passwordSignature;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
