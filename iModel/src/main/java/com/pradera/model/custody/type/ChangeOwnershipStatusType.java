/**@author nmolina
 * 
 */

package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public enum ChangeOwnershipStatusType {
		
	REGISTRADO(new Integer(864),"REGISTRADO"),
	CONFIRMADO(new Integer(865),"CONFIRMADO"),
	RECHAZADO(new Integer(866),"RECHAZADO"),
	ANULADO(new Integer(867),"ANULADO"),
	RECTIFIED(new Integer(2274),"RECTIFICADO");
	
	private Integer code;
	private String value;
	public static final List<ChangeOwnershipStatusType> list = new ArrayList<ChangeOwnershipStatusType>();
	
	public static final Map<Integer, ChangeOwnershipStatusType> lookup = new HashMap<Integer, ChangeOwnershipStatusType>();
	
	public static List<ChangeOwnershipStatusType> listSomeElements(ChangeOwnershipStatusType... transferSecuritiesTypeParams){
		List<ChangeOwnershipStatusType> retorno = new ArrayList<ChangeOwnershipStatusType>();
		for(ChangeOwnershipStatusType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ChangeOwnershipStatusType s : EnumSet.allOf(ChangeOwnershipStatusType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private ChangeOwnershipStatusType(Integer ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}