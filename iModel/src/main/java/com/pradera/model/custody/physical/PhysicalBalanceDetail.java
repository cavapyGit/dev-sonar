package com.pradera.model.custody.physical;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the PHYSICAL_BALANCE_DETAIL database table.
 * 
 */
@Entity
@Table(name="PHYSICAL_BALANCE_DETAIL")
public class PhysicalBalanceDetail implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PHYSICAL_BALANCE_DETAIL_GENERATOR", sequenceName="SQ_ID_PHYSICAL_BALANCE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PHYSICAL_BALANCE_DETAIL_GENERATOR")
	@Column(name="ID_PHYSICAL_BALANCE_DETAIL_PK")
	private Long idPhysicalBalanceDetailPk;

	@ManyToOne
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@ManyToOne
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

	@Column(name="\"STATE\"")
	private Integer state;
	
	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="CERTIFICATE_QUANTITY")
	private BigDecimal certificateQuantity;
	
	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;
	
	//bi-directional many-to-one association to PhysicalCertificate
    @ManyToOne
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;
    
    @Temporal(TemporalType.TIMESTAMP)
   	@Column(name="DEPOSIT_DATE")
   	private Date depositeDate;
    
    @Temporal(TemporalType.TIMESTAMP)
   	@Column(name="RETIREMENT_DATE")
   	private Date retiroDate;
    
	/**
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public PhysicalBalanceDetail() {
    }


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	
	public PhysicalCertificate getPhysicalCertificate() {
		return this.physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public Date getDepositeDate() {
		return depositeDate;
	}

	public void setDepositeDate(Date depositeDate) {
		this.depositeDate = depositeDate;
	}

	public Date getRetiroDate() {
		return retiroDate;
	}

	public void setRetiroDate(Date retiroDate) {
		this.retiroDate = retiroDate;
	}

	public BigDecimal getCertificateQuantity() {
		return certificateQuantity;
	}

	public void setCertificateQuantity(BigDecimal certificateQuantity) {
		this.certificateQuantity = certificateQuantity;
	}
	
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Long getIdPhysicalBalanceDetailPk() {
		return idPhysicalBalanceDetailPk;
	}

	public void setIdPhysicalBalanceDetailPk(Long idPhysicalBalanceDetailPk) {
		this.idPhysicalBalanceDetailPk = idPhysicalBalanceDetailPk;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}