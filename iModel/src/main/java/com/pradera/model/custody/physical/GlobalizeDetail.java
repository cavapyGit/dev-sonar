package com.pradera.model.custody.physical;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name = "GLOBALIZE_DETAIL")
public class GlobalizeDetail implements Serializable, Auditable{

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "GLOBALIZE_DETAIL_PK", sequenceName = "SQ_GLOBALIZE_DETAIL_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GLOBALIZE_DETAIL_PK")
	@Column(name = "ID_GLOBALIZE_DETAIL_PK")
	private Long idGlobalizeDetailPk;

	@ManyToOne
	@JoinColumn(name="ID_CERTIFICATE_OPERATION_FK")
	private CertificateOperation certificateOperation;
	
	@ManyToOne
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;
	
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
		
	public Long getIdGlobalizeDetailPk() {
		return idGlobalizeDetailPk;
	}

	public void setIdGlobalizeDetailPk(Long idGlobalizeDetailPk) {
		this.idGlobalizeDetailPk = idGlobalizeDetailPk;
	}

	public CertificateOperation getCertificateOperation() {
		return certificateOperation;
	}

	public void setCertificateOperation(CertificateOperation certificateOperation) {
		this.certificateOperation = certificateOperation;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null){
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
