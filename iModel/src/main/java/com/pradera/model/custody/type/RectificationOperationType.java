package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RectificationOperationType {


	DESMATERIALIZATION_CERTIFICATE(new Integer(1456),"DESMATERIALIZACION DE VALORES"),
	SECURITIES_RETIREMENT(new Integer(1457),"RETIRO DE VALORES"),
	TRANSFER_SECURITIE_AVAILABLE(new Integer(1458),"TRASPASO DE VALORES DISPONIBLES"),
	TRANSFER_SECURITIE_BLOCK(new Integer(1459),"TRASPASO DE VALORES BLOQUEADOS"),
	CHANGE_OWNERSHIP_AVIALBLE(new Integer(1460),"CAMBIO DE TITULARIDAD DE VALORES DISPONIBLES"),
	CHANGE_OWNERCSHIP_BLOCK(new Integer(1461),"CAMBIO DE TITULARIDAD DE VALORES BLOQUEADOS");

	private Integer code;
	private String value;
	
	public static final List<RectificationOperationType> list = new ArrayList<RectificationOperationType>();
	public static final Map<Integer, RectificationOperationType> lookup = new HashMap<Integer, RectificationOperationType>();
	public static List<RectificationOperationType> listSomeElements(RectificationOperationType... transferSecuritiesTypeParams){
		List<RectificationOperationType> retorno = new ArrayList<RectificationOperationType>();
		for(RectificationOperationType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (RectificationOperationType s : EnumSet.allOf(RectificationOperationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private RectificationOperationType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	public static RectificationOperationType get(Integer codigo) {
		return lookup.get(codigo);
	}

}
