/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.custody.channelopening;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.pradera.model.accounts.Participant;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "CHANNEL_OPENING")
@NamedQueries({
    @NamedQuery(name = "ChannelOpening.findAll", query = "SELECT c FROM ChannelOpening c"),
    @NamedQuery(name = "ChannelOpening.findByIdChannelOpeningPk", query = "SELECT c FROM ChannelOpening c WHERE c.idChannelOpeningPk = :idChannelOpeningPk"),
    @NamedQuery(name = "ChannelOpening.findByDateChannelOpening", query = "SELECT c FROM ChannelOpening c WHERE c.dateChannelOpening = :dateChannelOpening"),
    @NamedQuery(name = "ChannelOpening.findByInitialHour", query = "SELECT c FROM ChannelOpening c WHERE c.initialHour = :initialHour"),
    @NamedQuery(name = "ChannelOpening.findByFinalHour", query = "SELECT c FROM ChannelOpening c WHERE c.finalHour = :finalHour"),
    @NamedQuery(name = "ChannelOpening.findByMotiveChannelOpening", query = "SELECT c FROM ChannelOpening c WHERE c.motiveChannelOpening = :motiveChannelOpening"),
    @NamedQuery(name = "ChannelOpening.findByObservationChannelOpening", query = "SELECT c FROM ChannelOpening c WHERE c.observationChannelOpening = :observationChannelOpening"),
    @NamedQuery(name = "ChannelOpening.findByRegistryDate", query = "SELECT c FROM ChannelOpening c WHERE c.registryDate = :registryDate"),
    @NamedQuery(name = "ChannelOpening.findByMotiveChannelClosure", query = "SELECT c FROM ChannelOpening c WHERE c.motiveChannelClosure = :motiveChannelClosure"),
    @NamedQuery(name = "ChannelOpening.findByObservationChannelClosure", query = "SELECT c FROM ChannelOpening c WHERE c.observationChannelClosure = :observationChannelClosure"),
    @NamedQuery(name = "ChannelOpening.findByState", query = "SELECT c FROM ChannelOpening c WHERE c.state = :state"),
    @NamedQuery(name = "ChannelOpening.findByLastModifyUser", query = "SELECT c FROM ChannelOpening c WHERE c.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "ChannelOpening.findByLastModifyDate", query = "SELECT c FROM ChannelOpening c WHERE c.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "ChannelOpening.findByLastModifyIp", query = "SELECT c FROM ChannelOpening c WHERE c.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "ChannelOpening.findByLastModifyApp", query = "SELECT c FROM ChannelOpening c WHERE c.lastModifyApp = :lastModifyApp"),
    @NamedQuery(name = "ChannelOpening.findByFileName", query = "SELECT c FROM ChannelOpening c WHERE c.fileName = :fileName")})
public class ChannelOpening implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_CHANNEL_OPENING", sequenceName="SQ_CHANNEL_OPENING",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_CHANNEL_OPENING")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CHANNEL_OPENING_PK", nullable = false)
    private Long idChannelOpeningPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_CHANNEL_OPENING", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateChannelOpening;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INITIAL_HOUR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date initialHour;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FINAL_HOUR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date finalHour;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MOTIVE_CHANNEL_OPENING", nullable = false)
    private Integer motiveChannelOpening;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "OBSERVATION_CHANNEL_OPENING", nullable = false, length = 1000)
    private String observationChannelOpening;
    @Column(name = "REGISTRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Column(name = "MOTIVE_CHANNEL_CLOSURE")
    private Integer motiveChannelClosure;
    @Size(max = 1000)
    @Column(name = "OBSERVATION_CHANNEL_CLOSURE", length = 1000)
    private String observationChannelClosure;
    @Lob
    @Column(name = "FILE_ATTACHED")
    private Serializable fileAttached;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE_CHANNEL", nullable = false)
    private Integer state;
    @Size(max = 20)
    @Column(name = "LAST_MODIFY_USER", length = 20)
    private String lastModifyUser;
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Size(max = 20)
    @Column(name = "LAST_MODIFY_IP", length = 20)
    private String lastModifyIp;
    @Column(name = "LAST_MODIFY_APP")
    private Long lastModifyApp;
    @Size(max = 256)
    @Column(name = "FILE_NAME", length = 256)
    private String fileName;
    @JoinColumn(name = "ID_PARTICIPANT_FK", referencedColumnName = "ID_PARTICIPANT_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Participant idParticipantFk;
    @OneToMany(mappedBy = "idChannelOpeningFk", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ChannelOpeningHistory> channelOpeningHistoryList;

    public ChannelOpening() {
    }

    public ChannelOpening(Long idChannelOpeningPk) {
        this.idChannelOpeningPk = idChannelOpeningPk;
    }

    public ChannelOpening(Long idChannelOpeningPk, Date dateChannelOpening, Date initialHour, Date finalHour, Integer motiveChannelOpening, String observationChannelOpening, Integer state) {
        this.idChannelOpeningPk = idChannelOpeningPk;
        this.dateChannelOpening = dateChannelOpening;
        this.initialHour = initialHour;
        this.finalHour = finalHour;
        this.motiveChannelOpening = motiveChannelOpening;
        this.observationChannelOpening = observationChannelOpening;
        this.state = state;
    }

    public Long getIdChannelOpeningPk() {
        return idChannelOpeningPk;
    }

    public void setIdChannelOpeningPk(Long idChannelOpeningPk) {
        this.idChannelOpeningPk = idChannelOpeningPk;
    }

    public Date getDateChannelOpening() {
        return dateChannelOpening;
    }

    public void setDateChannelOpening(Date dateChannelOpening) {
        this.dateChannelOpening = dateChannelOpening;
    }

    public Date getInitialHour() {
        return initialHour;
    }

    public void setInitialHour(Date initialHour) {
        this.initialHour = initialHour;
    }

    public Date getFinalHour() {
        return finalHour;
    }

    public void setFinalHour(Date finalHour) {
        this.finalHour = finalHour;
    }

    public Integer getMotiveChannelOpening() {
        return motiveChannelOpening;
    }

    public void setMotiveChannelOpening(Integer motiveChannelOpening) {
        this.motiveChannelOpening = motiveChannelOpening;
    }

    public String getObservationChannelOpening() {
        return observationChannelOpening;
    }

    public void setObservationChannelOpening(String observationChannelOpening) {
        this.observationChannelOpening = observationChannelOpening;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public Integer getMotiveChannelClosure() {
        return motiveChannelClosure;
    }

    public void setMotiveChannelClosure(Integer motiveChannelClosure) {
        this.motiveChannelClosure = motiveChannelClosure;
    }

    public String getObservationChannelClosure() {
        return observationChannelClosure;
    }

    public void setObservationChannelClosure(String observationChannelClosure) {
        this.observationChannelClosure = observationChannelClosure;
    }

    public Serializable getFileAttached() {
        return fileAttached;
    }

    public void setFileAttached(Serializable fileAttached) {
        this.fileAttached = fileAttached;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Participant getIdParticipantFk() {
        return idParticipantFk;
    }

    public void setIdParticipantFk(Participant idParticipantFk) {
        this.idParticipantFk = idParticipantFk;
    }

    public List<ChannelOpeningHistory> getChannelOpeningHistoryList() {
		return channelOpeningHistoryList;
	}

	public void setChannelOpeningHistoryList(List<ChannelOpeningHistory> channelOpeningHistoryList) {
		this.channelOpeningHistoryList = channelOpeningHistoryList;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idChannelOpeningPk != null ? idChannelOpeningPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChannelOpening)) {
            return false;
        }
        ChannelOpening other = (ChannelOpening) object;
        if ((this.idChannelOpeningPk == null && other.idChannelOpeningPk != null) || (this.idChannelOpeningPk != null && !this.idChannelOpeningPk.equals(other.idChannelOpeningPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.ChannelOpening[ idChannelOpeningPk=" + idChannelOpeningPk + " ]";
    }
    
}
