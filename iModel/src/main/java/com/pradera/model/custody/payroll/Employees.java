package com.pradera.model.custody.payroll;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.constants.Constants;

@Entity
@Table(name="EMPLOYEES")
public class Employees implements Serializable, Auditable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="EMPLOYEES_IDEMPLOYESPK_GENERATOR", sequenceName="SQ_EMPLOYEES_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EMPLOYEES_IDEMPLOYESPK_GENERATOR")
	@Column(name="ID_EMPLOYEE_PK")
	private Long idEmployeePk;
	
	@Column(name="EMPLOYEE_NAMES")
	private String employeeNames;
	
	@Column(name="EMPLOYEE_LAST_NAMES")
	private String employeeLastNames;
	
	@Column(name="ROL_TYPE")
	private Integer rolType;
	
	@Column(name="EMPLOYEE_STATE")
	private Integer employeeState;
	
	@Column(name="BRANCH_OFFICE")
	private Integer branchOffice;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="ID_USER_FK")
	private Long idUserFk;
	
	public String getFullName(){
		return employeeLastNames.concat(Constants.COMMA).concat(employeeNames);
	}	
	public Long getIdEmployeePk() {
		return idEmployeePk;
	}
	public void setIdEmployeePk(Long idEmployeePk) {
		this.idEmployeePk = idEmployeePk;
	}
	public String getEmployeeNames() {
		return employeeNames;
	}
	public void setEmployeeNames(String employeeNames) {
		this.employeeNames = employeeNames;
	}
	public String getEmployeeLastNames() {
		return employeeLastNames;
	}
	public void setEmployeeLastNames(String employeeLastNames) {
		this.employeeLastNames = employeeLastNames;
	}
	public Integer getRolType() {
		return rolType;
	}
	public void setRolType(Integer rolType) {
		this.rolType = rolType;
	}
	public Integer getEmployeeState() {
		return employeeState;
	}
	public void setEmployeeState(Integer employeeState) {
		this.employeeState = employeeState;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}	
	public Long getIdUserFk() {
		return idUserFk;
	}
	public void setIdUserFk(Long idUserFk) {
		this.idUserFk = idUserFk;
	}
	public Integer getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null){
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
