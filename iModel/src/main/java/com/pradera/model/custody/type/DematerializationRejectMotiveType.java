package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum DematerializationRejectMotiveType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public enum DematerializationRejectMotiveType {
	
	/** The dematerialization duplicity. issue 1263 ANTES: DUPLICIDAD DE DESMTERIALIZACION */
	DEMATERIALIZATION_DUPLICITY(new Integer(1213),"A SOLICITUD DEL PARTICIPANTE"),
	
	/** The lost securities. issue 1263 INHABILITADO*/
	LOST_SECURITIES(new Integer(1214),"EXTRAVIO DE TITULOSN"),
	
	/** The name of other owner. issue 1263 ANTES: NOMBRE DE OTRO TITULAR*/
	NAME_OF_OTHER_OWNER(new Integer(1215),"DATOS INCORRECTOS"),
	
	/** The others. */
	OTHERS(new Integer(1216) , "OTROS");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<DematerializationRejectMotiveType> list = new ArrayList<DematerializationRejectMotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, DematerializationRejectMotiveType> lookup = new HashMap<Integer, DematerializationRejectMotiveType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<DematerializationRejectMotiveType> listSomeElements(DematerializationRejectMotiveType... transferSecuritiesTypeParams){
		List<DematerializationRejectMotiveType> retorno = new ArrayList<DematerializationRejectMotiveType>();
		for(DematerializationRejectMotiveType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (DematerializationRejectMotiveType s : EnumSet.allOf(DematerializationRejectMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new dematerialization reject motive type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private DematerializationRejectMotiveType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the dematerialization reject motive type
	 */
	public static DematerializationRejectMotiveType get(Integer codigo) {
		return lookup.get(codigo);
	}
}