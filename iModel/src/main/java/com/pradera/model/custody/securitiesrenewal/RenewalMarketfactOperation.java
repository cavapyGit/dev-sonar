package com.pradera.model.custody.securitiesrenewal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="RENEWAL_MARKETFACT_OPERATION")
public class RenewalMarketfactOperation implements Serializable, Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The id renewal marketfact */
	@Id
	@SequenceGenerator(name="SQ_ID_RENEWAL_MARKETFACT", sequenceName="SQ_ID_RENEWAL_MARKETFACT", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_RENEWAL_MARKETFACT")
	@Column(name="ID_RENEWAL_MARKETFACT_PK")
	private Long idRenewalMarketfactPk;
	
	/** The retirement detail. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RENEWAL_DETAIL_FK")
	private RenewalOperationDetail renewalOperationDetail;
	
	/** The retirement detail. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RENEWAL_BLOCK_FK")
	private RenewalBlockOperation renewalBlockOperation;

	/** The market date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MARKET_DATE")
	private Date marketDate;
	
	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	/** The market price. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	/** The operation quantity. */
	@Column(name="OPERATION_QUANTITY")
	private BigDecimal operationQuantity;
	
	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	public RenewalMarketfactOperation() {
		
	}

	/**
	 * @return the idRenewalMarketfactPk
	 */
	public Long getIdRenewalMarketfactPk() {
		return idRenewalMarketfactPk;
	}

	/**
	 * @param idRenewalMarketfactPk the idRenewalMarketfactPk to set
	 */
	public void setIdRenewalMarketfactPk(Long idRenewalMarketfactPk) {
		this.idRenewalMarketfactPk = idRenewalMarketfactPk;
	}

	/**
	 * @return the renewalOperationDetail
	 */
	public RenewalOperationDetail getRenewalOperationDetail() {
		return renewalOperationDetail;
	}

	/**
	 * @param renewalOperationDetail the renewalOperationDetail to set
	 */
	public void setRenewalOperationDetail(
			RenewalOperationDetail renewalOperationDetail) {
		this.renewalOperationDetail = renewalOperationDetail;
	}

	/**
	 * @return the renewalBlockOperation
	 */
	public RenewalBlockOperation getRenewalBlockOperation() {
		return renewalBlockOperation;
	}

	/**
	 * @param renewalBlockOperation the renewalBlockOperation to set
	 */
	public void setRenewalBlockOperation(RenewalBlockOperation renewalBlockOperation) {
		this.renewalBlockOperation = renewalBlockOperation;
	}

	/**
	 * @return the marketDate
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * @param marketDate the marketDate to set
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * @return the marketRate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * @param marketRate the marketRate to set
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * @return the marketPrice
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * @param marketPrice the marketPrice to set
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * @return the operationQuantity
	 */
	public BigDecimal getOperationQuantity() {
		return operationQuantity;
	}

	/**
	 * @param operationQuantity the operationQuantity to set
	 */
	public void setOperationQuantity(BigDecimal operationQuantity) {
		this.operationQuantity = operationQuantity;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if(loggerUser!=null){
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
