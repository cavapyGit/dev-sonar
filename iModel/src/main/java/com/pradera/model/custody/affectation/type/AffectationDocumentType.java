package com.pradera.model.custody.affectation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * The Enum AffectationType.
 */
public enum AffectationDocumentType {	
	
	SUPPORT_DOCUMENT(new Integer(1922),"DOCUMENTO DE SUSTENTO");

	private Integer code;
	private String value;
	public static final List<AffectationDocumentType> list = new ArrayList<AffectationDocumentType>();
	
	public static final Map<Integer, AffectationDocumentType> lookup = new HashMap<Integer, AffectationDocumentType>();
	public static List<AffectationDocumentType> listSomeElements(AffectationDocumentType... AffectationType){
		List<AffectationDocumentType> retorno = new ArrayList<AffectationDocumentType>();
		for(AffectationDocumentType AffectationTypeO: AffectationType){
			retorno.add(AffectationTypeO);
		}
		return retorno;
	}
	static {
		for (AffectationDocumentType s : EnumSet.allOf(AffectationDocumentType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private AffectationDocumentType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static AffectationDocumentType get(Integer code) {
		return lookup.get(code);
	}
}