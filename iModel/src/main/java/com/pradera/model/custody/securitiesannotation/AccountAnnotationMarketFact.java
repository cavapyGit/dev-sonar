package com.pradera.model.custody.securitiesannotation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="ACCOUNT_ANNOTATION_MARKETFACT")
public class AccountAnnotationMarketFact implements Serializable, Auditable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PK_GENERATOR_ACCOUNT_ANNOTATION_MARKET_FACT", sequenceName="SQ_ID_ANNOTATION_MARKETFACT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PK_GENERATOR_ACCOUNT_ANNOTATION_MARKET_FACT")
	@Column(name="ID_ANNOTATION_MARKETFACT_PK")
	private Long idAnnotationMarketFactPk;
	
	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;
	
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="OPERATION_QUANTITY")
	private BigDecimal operationQuantity;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ANNOTATION_OPERATION_FK",referencedColumnName="ID_ANNOTATION_OPERATION_PK")
	private AccountAnnotationOperation accountAnnotationOperation;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(Validations.validateIsNotNullAndNotEmpty(loggerUser)){
			 lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
	         lastModifyDate = loggerUser.getAuditTime();
	         lastModifyIp = loggerUser.getIpAddress();
	         lastModifyUser =loggerUser.getUserName();
		}	
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdAnnotationMarketFactPk() {
		return idAnnotationMarketFactPk;
	}

	public void setIdAnnotationMarketFactPk(Long idAnnotationMarketFactPk) {
		this.idAnnotationMarketFactPk = idAnnotationMarketFactPk;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public AccountAnnotationOperation getAccountAnnotationOperation() {
		return accountAnnotationOperation;
	}

	public void setAccountAnnotationOperation(
			AccountAnnotationOperation accountAnnotationOperation) {
		this.accountAnnotationOperation = accountAnnotationOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getOperationQuantity() {
		return operationQuantity;
	}

	public void setOperationQuantity(BigDecimal operationQuantity) {
		this.operationQuantity = operationQuantity;
	}
}
