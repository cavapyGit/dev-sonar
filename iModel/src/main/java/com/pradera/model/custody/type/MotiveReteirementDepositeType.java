/*
 * 
 */
package com.pradera.model.custody.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum TransferSecuritiesEstatusType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/11/2012
 */
public enum MotiveReteirementDepositeType {
	
	/** The registered. */
	REQUEST_HOLDER(new Integer(1),"SOLICITUD TITULAR"),
	
	/** The approved. */
	VENCEMENT( new Integer(2),"VENCIMIENTO");
	
	/** The codigo. */
	private Integer code;
	
	/** The valor. */
	private String value;
	
	/** The accion. */
	
	private String icon;
	
	/** The Constant list. */
	public static final List<MotiveReteirementDepositeType> list = new ArrayList<MotiveReteirementDepositeType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, MotiveReteirementDepositeType> lookup = new HashMap<Integer, MotiveReteirementDepositeType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<MotiveReteirementDepositeType> listSomeElements(MotiveReteirementDepositeType... transferSecuritiesTypeParams){
		List<MotiveReteirementDepositeType> retorno = new ArrayList<MotiveReteirementDepositeType>();
		for(MotiveReteirementDepositeType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (MotiveReteirementDepositeType s : EnumSet.allOf(MotiveReteirementDepositeType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * Instantiates a new transfer securities estatus type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 * @param accion the accion
	 */
	private MotiveReteirementDepositeType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the transfer securities estatus type
	 */
	public static MotiveReteirementDepositeType get(Integer codigo) {
		return lookup.get(codigo);
	}
}