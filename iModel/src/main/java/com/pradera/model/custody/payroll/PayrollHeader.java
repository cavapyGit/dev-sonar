package com.pradera.model.custody.payroll;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuer;

@Entity
@Table(name="PAYROLL_HEADER")
public class PayrollHeader implements Serializable, Auditable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="PAYROLL_HEADER_IDPAYROLLHEADERPK_GENERATOR", sequenceName="SQ_PAYROLL_HEADER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAYROLL_HEADER_IDPAYROLLHEADERPK_GENERATOR")
	@Column(name="ID_PAYROLL_HEADER_PK")
	private Long idPayrollHeaderPk;
	
	@ManyToOne
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;
	
	@Column(name="PAYROLL_TYPE")
	private Integer payrollType;
	
	@Column(name="PAYROLL_NUMBER")
	private Long payrollNumber;
	
	@Column(name="PAYROLL_STATE")
	private Integer payrollState;
	
	@Column(name="TOTAL_STOCK")
	private BigDecimal totalStock;
	
	@Column(name="REGISTER_DATE")
	private Date registerDate;
	
	@Column(name="REGISTER_USER")
	private String registerUser;
	
	@Column(name="BRANCH_OFFICE")
	private Integer branchOffice;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="IND_APPROVE")
	private Integer indApprove;
	
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;

	@Column(name="IND_DELIVERY")
	private Integer indDelivery;

	@Column(name="DELIVERY_ADDRESS")
	private String deliveryAddress;

	@Column(name="CONTACT_NAME")
	private String contactName;

	@Column(name="MOBILE_CONTACT")
	private String mobileContact;
	
	@Lob()
	@Column(name="SEND_ACUSE_FILE")
	@Basic(fetch=FetchType.LAZY)
	private byte[]  sendAcuseFile;
	
	@Lob()
	@Column(name="RECEPTION_ACUSE_FILE")
	@Basic(fetch=FetchType.LAZY)
	private byte[] receptionAcuseFile;
	
	@Column(name="IND_GENERATE_SEND_ACUSE")
	private Integer indGenerateSendAcuse = 0;
	
	@Column(name="IND_GENERATE_RECEPTION_ACUSE")
	private Integer indGenerateReceptionAcuse = 0;
	
	
	@OneToMany(mappedBy = "payrollHeader", cascade=CascadeType.ALL)
	private List<PayrollDetail> lstPayrollDetail;
	
	@Transient
	private String payrollTypeDesc;
	@Transient
	private String stateDesc;
	@Transient
	private String rejectMotiveDesc;
	@Transient
	private Date searchDate;

	@Transient
	private Integer retirementMotive;
	
	public Long getIdPayrollHeaderPk() {
		return idPayrollHeaderPk;
	}

	public void setIdPayrollHeaderPk(Long idPayrollHeaderPk) {
		this.idPayrollHeaderPk = idPayrollHeaderPk;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Integer getPayrollType() {
		return payrollType;
	}

	public void setPayrollType(Integer payrollType) {
		this.payrollType = payrollType;
	}

	public Long getPayrollNumber() {
		return payrollNumber;
	}

	public void setPayrollNumber(Long payrollNumber) {
		this.payrollNumber = payrollNumber;
	}

	public Integer getPayrollState() {
		return payrollState;
	}

	public void setPayrollState(Integer payrollState) {
		this.payrollState = payrollState;
	}

	public BigDecimal getTotalStock() {
		return totalStock;
	}

	public void setTotalStock(BigDecimal totalStock) {
		this.totalStock = totalStock;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	public List<PayrollDetail> getLstPayrollDetail() {
		return lstPayrollDetail;
	}

	public void setLstPayrollDetail(List<PayrollDetail> lstPayrollDetail) {
		this.lstPayrollDetail = lstPayrollDetail;
	}

	public String getPayrollTypeDesc() {
		return payrollTypeDesc;
	}

	public void setPayrollTypeDesc(String payrollTypeDesc) {
		this.payrollTypeDesc = payrollTypeDesc;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

		
	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Integer getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}

	public Integer getIndApprove() {
		return indApprove;
	}

	public void setIndApprove(Integer indApprove) {
		this.indApprove = indApprove;
	}

	public Integer getRejectMotive() {
		return rejectMotive;
	}

	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	public String getRejectMotiveDesc() {
		return rejectMotiveDesc;
	}

	public void setRejectMotiveDesc(String rejectMotiveDesc) {
		this.rejectMotiveDesc = rejectMotiveDesc;
	}

	public Date getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(Date searchDate) {
		this.searchDate = searchDate;
	}

	public Integer getIndDelivery() {
		return indDelivery;
	}

	public void setIndDelivery(Integer indDelivery) {
		this.indDelivery = indDelivery;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getMobileContact() {
		return mobileContact;
	}

	public void setMobileContact(String mobileContact) {
		this.mobileContact = mobileContact;
	}

	public Integer getRetirementMotive() {
		return retirementMotive;
	}

	public void setRetirementMotive(Integer retirementMotive) {
		this.retirementMotive = retirementMotive;
	}

	public byte[] getSendAcuseFile() {
		return sendAcuseFile;
	}

	public void setSendAcuseFile(byte[] sendAcuseFile) {
		this.sendAcuseFile = sendAcuseFile;
	}

	public byte[] getReceptionAcuseFile() {
		return receptionAcuseFile;
	}

	public void setReceptionAcuseFile(byte[] receptionAcuseFile) {
		this.receptionAcuseFile = receptionAcuseFile;
	}

	public Integer getIndGenerateSendAcuse() {
		return indGenerateSendAcuse;
	}

	public void setIndGenerateSendAcuse(Integer indGenerateSendAcuse) {
		this.indGenerateSendAcuse = indGenerateSendAcuse;
	}

	public Integer getIndGenerateReceptionAcuse() {
		return indGenerateReceptionAcuse;
	}

	public void setIndGenerateReceptionAcuse(Integer indGenerateReceptionAcuse) {
		this.indGenerateReceptionAcuse = indGenerateReceptionAcuse;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null){
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

}
