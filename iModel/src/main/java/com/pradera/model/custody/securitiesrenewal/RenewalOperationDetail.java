package com.pradera.model.custody.securitiesrenewal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;

/**
 * The persistent class for the RENEWAL_OPERATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="RENEWAL_OPERATION_DETAIL")
public class RenewalOperationDetail implements Serializable, Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="SQ_ID_RENEWAL_DETAIL", sequenceName="SQ_ID_RENEWAL_DETAIL", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_RENEWAL_DETAIL")
	@Column(name="ID_RENEWAL_DETAIL_PK")
	private long idRenewalDetailPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITIES_RENEWAL_FK", referencedColumnName="ID_SECURITIES_RENEWAL_PK")
	private SecuritiesRenewalOperation securitiesRenewalOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")	
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK") 	
	private Participant participant;
	
	/** The total balance. */
	@Column(name="TOTAL_BALANCE")	
	private BigDecimal totalBalance;
	
	/** The available balance. */
	@Column(name="AVAILABLE_BALANCE")	
	private BigDecimal availableBalance;
	
	/** The pawn balance. */
	@Column(name="PAWN_BALANCE")	
	private BigDecimal pawnBalance;
	
	/** The other block balance. */
	@Column(name="OTHER_BLOCK_BALANCE")	
	private BigDecimal otherBlockBalance;
	
	/** The ban balance. */
	@Column(name="BAN_BALANCE")	
	private BigDecimal banBalance;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The renewal block operation. */
    @OneToMany(mappedBy="renewalOperationDetail",fetch=FetchType.LAZY)
    private List<RenewalBlockOperation> renewalBlockOperation;
    
    /** The renewal marketfact operation. */
    @OneToMany(mappedBy="renewalOperationDetail",fetch=FetchType.LAZY)
    private List<RenewalMarketfactOperation> renewalMarketfactOperation;
    
    public BigDecimal getTotalBalance(){
    	return availableBalance.add(banBalance).add(pawnBalance).add(otherBlockBalance);
    }
    
	public RenewalOperationDetail() {
		this.availableBalance = BigDecimal.ZERO;
		this.pawnBalance = BigDecimal.ZERO;
		this.otherBlockBalance = BigDecimal.ZERO;
		this.banBalance = BigDecimal.ZERO;
	}

	/**
	 * @return the idRenewalDetailPk
	 */
	public long getIdRenewalDetailPk() {
		return idRenewalDetailPk;
	}

	/**
	 * @param idRenewalDetailPk the idRenewalDetailPk to set
	 */
	public void setIdRenewalDetailPk(long idRenewalDetailPk) {
		this.idRenewalDetailPk = idRenewalDetailPk;
	}

	/**
	 * @return the securitiesRenewalOperation
	 */
	public SecuritiesRenewalOperation getSecuritiesRenewalOperation() {
		return securitiesRenewalOperation;
	}

	/**
	 * @param securitiesRenewalOperation the securitiesRenewalOperation to set
	 */
	public void setSecuritiesRenewalOperation(
			SecuritiesRenewalOperation securitiesRenewalOperation) {
		this.securitiesRenewalOperation = securitiesRenewalOperation;
	}

	/**
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * @param participant the participant to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * @return the availableBalance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * @param availableBalance the availableBalance to set
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * @return the pawnBalance
	 */
	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	/**
	 * @param pawnBalance the pawnBalance to set
	 */
	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	/**
	 * @return the otherBlockBalance
	 */
	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	/**
	 * @param otherBlockBalance the otherBlockBalance to set
	 */
	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	/**
	 * @return the banBalance
	 */
	public BigDecimal getBanBalance() {
		return banBalance;
	}

	/**
	 * @param banBalance the banBalance to set
	 */
	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the renewalBlockOperation
	 */
	public List<RenewalBlockOperation> getRenewalBlockOperation() {
		return renewalBlockOperation;
	}

	/**
	 * @param renewalBlockOperation the renewalBlockOperation to set
	 */
	public void setRenewalBlockOperation(
			List<RenewalBlockOperation> renewalBlockOperation) {
		this.renewalBlockOperation = renewalBlockOperation;
	}

	/**
	 * @return the renewalMarketfactOperation
	 */
	public List<RenewalMarketfactOperation> getRenewalMarketfactOperation() {
		return renewalMarketfactOperation;
	}

	/**
	 * @param renewalMarketfactOperation the renewalMarketfactOperation to set
	 */
	public void setRenewalMarketfactOperation(
			List<RenewalMarketfactOperation> renewalMarketfactOperation) {
		this.renewalMarketfactOperation = renewalMarketfactOperation;
	}

	/**
	 * @param totalBalance the totalBalance to set
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            this.lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            this.lastModifyDate = loggerUser.getAuditTime();
            this.lastModifyIp = loggerUser.getIpAddress();
            this.lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		 HashMap<String,List<? extends Auditable>> detailsMap = 
	                new HashMap<String, List<? extends Auditable>>();
	        detailsMap.put("renewalMarketfactOperation", renewalMarketfactOperation);
	        return detailsMap;
	}

}
