package com.pradera.model.custody.transfersecurities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_CODE database table.
 * 
 */
@Entity
@Table(name="DAILY_SECURITY_CLASS_BALANCE")
@NamedQuery(name="DailySecurityClassBalance.findAll", query="SELECT v FROM DailySecurityClassBalance v")
public class DailySecurityClassBalance implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DAILY_SECURITY_CLASS_BALANCE_IDDAILYSECCLASSBALANCEPK_GENERATOR", sequenceName="SQ_DAILY_SEC_CLASS_BALANCE_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DAILY_SECURITY_CLASS_BALANCE_IDDAILYSECCLASSBALANCEPK_GENERATOR")
	
	@Column(name="ID_DAILY_SEC_CLASS_BALANCE_PK")
	private Long idDailySecClassBalancePk;

	@Column(name="PROCESS_DATE")
	private Date processDate;

	@Column(name="SECURITY_CLASS")
	private Integer securityClass;
	
	@Column(name="CURRENCY")
	private Integer currency;
	
	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	@Column(name="NOMINAL_AMOUNT")
	private BigDecimal nominalAmount;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	public DailySecurityClassBalance() {
	}

	/**
	 * @return the idDailySecClassBalancePk
	 */
	public Long getIdDailySecClassBalancePk() {
		return idDailySecClassBalancePk;
	}

	/**
	 * @param idDailySecClassBalancePk the idDailySecClassBalancePk to set
	 */
	public void setIdDailySecClassBalancePk(Long idDailySecClassBalancePk) {
		this.idDailySecClassBalancePk = idDailySecClassBalancePk;
	}

	/**
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * @return the stockQuantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * @return the nominalAmount
	 */
	public BigDecimal getNominalAmount() {
		return nominalAmount;
	}

	/**
	 * @param nominalAmount the nominalAmount to set
	 */
	public void setNominalAmount(BigDecimal nominalAmount) {
		this.nominalAmount = nominalAmount;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}