package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RetirementOperationStateType {
	REGISTERED(new Integer(162),"REGISTRADO"),
	APPROVED(new Integer(531),"APROBADO"),
	ANNULLED(new Integer(1142),"ANULADO"),
	REJECTED(new Integer(1292),"RECHAZADO"),
	CONFIRMED(new Integer(1144),"CONFIRMADO"),
	RECTIFIED(new Integer(2276),"RECTIFICADO"),
	REVERSED(new Integer(2278),"REVERTIDO");
	
	private Integer code;
	private String value;
	public static final List<RetirementOperationStateType> list = new ArrayList<RetirementOperationStateType>();
	
	public static final Map<Integer, RetirementOperationStateType> lookup = new HashMap<Integer, RetirementOperationStateType>();
	public static List<RetirementOperationStateType> listSomeElements(RetirementOperationStateType... transferSecuritiesTypeParams){
		List<RetirementOperationStateType> retorno = new ArrayList<RetirementOperationStateType>();
		for(RetirementOperationStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (RetirementOperationStateType s : EnumSet.allOf(RetirementOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public static RetirementOperationStateType get(Integer code){
		return lookup.get(code);
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private RetirementOperationStateType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
