package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RectificationMotiveType {


	WRONG_ENTERED_DATA(new Integer(1489),"ERROR EN EL REGISTRO DE LA SOLICITUD"),
	PARTICIPANT_REQUESTER(new Integer(1490),"A SOLICITUD DEL PARTICIPANTE"),
	OTHERS(new Integer(1778),"OTROS");

	private String value;
	private Integer code;

	public static final List<RectificationMotiveType> list= new ArrayList<RectificationMotiveType>();
	public static final Map<Integer, RectificationMotiveType> lookup = new HashMap<Integer, RectificationMotiveType >();

	static{
		for(RectificationMotiveType type: EnumSet.allOf(RectificationMotiveType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);
		}


	}


	private RectificationMotiveType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static RectificationMotiveType get(Integer code)
	{
		return lookup.get(code);
	}







}
