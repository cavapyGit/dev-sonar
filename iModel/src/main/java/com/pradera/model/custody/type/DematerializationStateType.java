package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum DematerializationStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public enum DematerializationStateType {
	
	/** The reigstered. */
	REIGSTERED(new Integer(219),"REGISTRADO"),
    
    /** The confirmed. */
    CONFIRMED(new Integer(858),"CONFIRMADO"),
	
	/** The rejected. */
	REJECTED(new Integer(859),"RECHAZADO"),
	
	/** The approved. */
	APPROVED(new Integer(860),"APROBADO"),
    
    /** The cancelled. */
    ANNULLED(new Integer(861),"ANULADO"),
    
    REVIEWED(new Integer(1923),"REVISADO"),
	
	CERTIFIED(new Integer(1924),"APROBADO"),
    
    CANCELLED(new Integer(1925),"CANCELADO"),
    
	RECTIFIED(new Integer(2275),"RECTIFICADO"),
	
	REVERSED(new Integer(2552),"REVERTIDO");
	
	/** The value. */
	private String value;
	
	/** The code. */
	private Integer code;
	
	/** The Constant list. */
	public static final List<DematerializationStateType> list= new ArrayList<DematerializationStateType>();
	
	/** The Constant listNotOfac. */
	public static final List<DematerializationStateType> listNotOfac = new ArrayList<DematerializationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, DematerializationStateType> lookup = new HashMap<Integer, DematerializationStateType >();
	
	static{
		for(DematerializationStateType type: EnumSet.allOf(DematerializationStateType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);
		}
		
		
	}
	
	
	/**
	 * Instantiates a new dematerialization state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private DematerializationStateType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}


    /**
     * Gets the code.
     *
     * @return the code
     */
    public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the dematerialization state type
	 */
	public static DematerializationStateType get(Integer code)
	{
		return lookup.get(code);
	}


	
	
	
	

}
