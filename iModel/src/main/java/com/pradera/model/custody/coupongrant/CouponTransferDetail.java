package com.pradera.model.custody.coupongrant;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the CHANGE_OWNERSHIP_OPERATION database table.
 * 
 */
@Entity
@Table(name="COUPON_TRANSFER_DETAIL")
public class CouponTransferDetail implements Serializable,Auditable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="COUPON_TRANSFER_DETAIL_GENERATOR", sequenceName="SQ_ID_COUPON_TRANSFER_DET_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COUPON_TRANSFER_DETAIL_GENERATOR")
	@Column(name="ID_COUPON_TRANSFER_DETAIL_PK")
	private long idCouponTransferDetailPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COUPON_TRANSFER_OPER_FK")
	private CouponTransferOperation couponTransferOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COUPON_GRANT_OPERATION_FK")
	private CouponGrantOperation couponGrantOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEW_GRANT_OPERATION_FK")
	private CouponGrantOperation newCouponGrantOperation;
	
	@Column(name="TRANSFERED_BALANCE")
	private BigDecimal transferedBalance;

	@Column(name="IND_SOURCE_TARGET")
	private Integer indSourceTarget;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	public CouponTransferDetail() {

    }

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        return null;
	}
	
	/**
	 * @return the newCouponGrantOperation
	 */
	public CouponGrantOperation getNewCouponGrantOperation() {
		return newCouponGrantOperation;
	}

	/**
	 * @param newCouponGrantOperation the newCouponGrantOperation to set
	 */
	public void setNewCouponGrantOperation(
			CouponGrantOperation newCouponGrantOperation) {
		this.newCouponGrantOperation = newCouponGrantOperation;
	}

	/**
	 * @return the idCouponTransferDetailPk
	 */
	public long getIdCouponTransferDetailPk() {
		return idCouponTransferDetailPk;
	}

	/**
	 * @param idCouponTransferDetailPk the idCouponTransferDetailPk to set
	 */
	public void setIdCouponTransferDetailPk(long idCouponTransferDetailPk) {
		this.idCouponTransferDetailPk = idCouponTransferDetailPk;
	}

	/**
	 * @return the couponTransferOperation
	 */
	public CouponTransferOperation getCouponTransferOperation() {
		return couponTransferOperation;
	}

	/**
	 * @param couponTransferOperation the couponTransferOperation to set
	 */
	public void setCouponTransferOperation(
			CouponTransferOperation couponTransferOperation) {
		this.couponTransferOperation = couponTransferOperation;
	}

	/**
	 * @return the couponGrantOperation
	 */
	public CouponGrantOperation getCouponGrantOperation() {
		return couponGrantOperation;
	}

	/**
	 * @param couponGrantOperation the couponGrantOperation to set
	 */
	public void setCouponGrantOperation(CouponGrantOperation couponGrantOperation) {
		this.couponGrantOperation = couponGrantOperation;
	}

	/**
	 * @return the transferedBalance
	 */
	public BigDecimal getTransferedBalance() {
		return transferedBalance;
	}

	/**
	 * @param transferedBalance the transferedBalance to set
	 */
	public void setTransferedBalance(BigDecimal transferedBalance) {
		this.transferedBalance = transferedBalance;
	}

	/**
	 * @return the indSourceTarget
	 */
	public Integer getIndSourceTarget() {
		return indSourceTarget;
	}

	/**
	 * @param indSourceTarget the indSourceTarget to set
	 */
	public void setIndSourceTarget(Integer indSourceTarget) {
		this.indSourceTarget = indSourceTarget;
	}


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

}