package com.pradera.model.custody.type;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum PhysicalCertificateStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Feb 11, 2014
 */
public enum PhysicalCertificateOperationType {

	DEPOSIT(Integer.valueOf(673),"DEPÓSITO"),

	RETIREMENT(Integer.valueOf(674),"RETIRO");
 
	/** The Constant list. */
	public static final List<PhysicalCertificateOperationType> list=new ArrayList<PhysicalCertificateOperationType>();

	/** The Constant lookup. */
	public static final Map<Integer, PhysicalCertificateOperationType> lookup=new HashMap<Integer, PhysicalCertificateOperationType>();

	static {
		for(PhysicalCertificateOperationType e : PhysicalCertificateOperationType.values()){
			lookup.put(e.getCode(), e);
			list.add( e );
		}
	}

	/** The code. */
	private Integer  code;

	/** The value. */
	private String value;


	/**
	 * Instantiates a new security state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PhysicalCertificateOperationType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the security state type
	 */
	public static PhysicalCertificateOperationType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
