package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SecuritiesRenewalMotiveType {
	
	SECURITIES_RENEWAL_DPF(new Integer(2280),"RENOVACION DE VALORES DPF");
	
	public static final List<SecuritiesRenewalMotiveType> list = new ArrayList<SecuritiesRenewalMotiveType>();
	public static final Map<Integer, SecuritiesRenewalMotiveType> lookup = new HashMap<Integer, SecuritiesRenewalMotiveType>();
	
	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private SecuritiesRenewalMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	static {
        for (SecuritiesRenewalMotiveType d : SecuritiesRenewalMotiveType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
	
	public static SecuritiesRenewalMotiveType get(Integer code) {
		return lookup.get(code);
	}
	
}
