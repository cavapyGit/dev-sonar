package com.pradera.model.custody.affectation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ReversalMotiveRejectType {
	MOTIVE_1(new Integer(1813)),
	OTHER(new Integer(1814));
	
	private Integer code;
	
	private ReversalMotiveRejectType(Integer code){
		this.code = code;
	}
		
	/** The Constant list. */
	public static final List<ReversalMotiveRejectType> list = new ArrayList<ReversalMotiveRejectType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ReversalMotiveRejectType> lookup = new HashMap<Integer, ReversalMotiveRejectType>();
	
	static {
		for (ReversalMotiveRejectType s : EnumSet.allOf(ReversalMotiveRejectType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static ReversalMotiveRejectType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}
