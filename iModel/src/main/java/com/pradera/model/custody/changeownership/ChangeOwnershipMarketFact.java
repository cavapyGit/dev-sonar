package com.pradera.model.custody.changeownership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the BLOCK_CHANGE_OWNERSHIP database table.
 * 
 */
@Entity
@Table(name="CHANGE_OWNERSHIP_MARKETFACT")
public class ChangeOwnershipMarketFact implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CHANGE_OWNERSHIP_MARKETFACT_GENERATOR", sequenceName="SQ_ID_CHNGOWNER_MARKETFACT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CHANGE_OWNERSHIP_MARKETFACT_GENERATOR")
	@Column(name="ID_OWNERSHIP_MARKETFACT_PK")
	private Long idOwnershipMarketfactPk;

	@Column(name="MARKET_BALANCE")
	private BigDecimal marketBalance;
	
//	@Column(name="PAWN_BALANCE")
//	private BigDecimal pawnBalance;
//	
//	@Column(name="BAN_BALANCE")
//	private BigDecimal banBalance;
//	
//	@Column(name="OTHER_BLOCK_BALANCE")
//	private BigDecimal otherBlockBalance;

	@Column(name="MARKET_DATE")
	@Temporal( TemporalType.DATE)
	private Date marketDate;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

    //bi-directional many-to-one association to ChangeOwnershipDetail
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANGE_OWNERSHIP_DETAIL_FK")
	private ChangeOwnershipDetail changeOwnershipDetail;
    
    //bi-directional many-to-one association to BlockChangeOwnership
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_CHANGE_OWNERSHIP_FK")
	private BlockChangeOwnership blockChangeOwnership;
    
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
    
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	
	
	public ChangeOwnershipMarketFact() {
		super();
		this.marketBalance = BigDecimal.ZERO;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idOwnershipMarketfactPk == null) ? 0
						: idOwnershipMarketfactPk.hashCode());
		result = prime * result
				+ ((marketDate == null) ? 0 : marketDate.hashCode());
		result = prime * result
				+ ((marketPrice == null) ? 0 : marketPrice.hashCode());
		result = prime * result
				+ ((marketRate == null) ? 0 : marketRate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeOwnershipMarketFact other = (ChangeOwnershipMarketFact) obj;
		if (idOwnershipMarketfactPk == null) {
			if (other.idOwnershipMarketfactPk != null)
				return false;
		} else if (!idOwnershipMarketfactPk
				.equals(other.idOwnershipMarketfactPk))
			return false;
		if (marketDate == null) {
			if (other.marketDate != null)
				return false;
		} else if (!marketDate.equals(other.marketDate))
			return false;
		if (marketPrice == null) {
			if (other.marketPrice != null)
				return false;
		} else if (!marketPrice.equals(other.marketPrice))
			return false;
		if (marketRate == null) {
			if (other.marketRate != null)
				return false;
		} else if (!marketRate.equals(other.marketRate))
			return false;
		return true;
	}

	public BlockChangeOwnership getBlockChangeOwnership() {
		return blockChangeOwnership;
	}

	public void setBlockChangeOwnership(BlockChangeOwnership blockChangeOwnership) {
		this.blockChangeOwnership = blockChangeOwnership;
	}

	public Long getIdOwnershipMarketfactPk() {
		return idOwnershipMarketfactPk;
	}

	public void setIdOwnershipMarketfactPk(Long idOwnershipMarketfactPk) {
		this.idOwnershipMarketfactPk = idOwnershipMarketfactPk;
	}

	public ChangeOwnershipDetail getChangeOwnershipDetail() {
		return changeOwnershipDetail;
	}
	
	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public void setChangeOwnershipDetail(ChangeOwnershipDetail changeOwnershipDetail) {
		this.changeOwnershipDetail = changeOwnershipDetail;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
	    if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal getMarketBalance() {
		return marketBalance;
	}

	public void setMarketBalance(BigDecimal marketBalance) {
		this.marketBalance = marketBalance;
	}
	    
	
	
	
}