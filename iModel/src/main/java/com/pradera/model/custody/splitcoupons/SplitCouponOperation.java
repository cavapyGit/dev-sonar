package com.pradera.model.custody.splitcoupons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;

@Entity
@Table(name="SPLIT_COUPON_DETAIL")
public class SplitCouponOperation implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="SPLITCOUPONDETAILPK_GENERATOR", sequenceName="SQ_ID_SPLIT_COUPON_DETAIL_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SPLITCOUPONDETAILPK_GENERATOR")	
	@Column(name="ID_SPLIT_DETAIL_PK")
	private Long idSplitDetailPk;
		
	@Column(name="OPERATION_QUANTITY")
	private BigDecimal operationQuantity;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SPLIT_OPERATION_FK", referencedColumnName = "ID_SPLIT_OPERATION_PK")
	private SplitCouponRequest splitCouponRequest;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROGRAM_INTEREST_FK", referencedColumnName = "ID_PROGRAM_INTEREST_PK")
	private ProgramInterestCoupon programInterestCoupon;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	public BigDecimal getOperationQuantity() {
		return operationQuantity;
	}

	public void setOperationQuantity(BigDecimal operationQuantity) {
		this.operationQuantity = operationQuantity;
	}

	public ProgramInterestCoupon getProgramInterestCoupon() {
		return programInterestCoupon;
	}

	public void setProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon) {
		this.programInterestCoupon = programInterestCoupon;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Long getIdSplitDetailPk() {
		return idSplitDetailPk;
	}

	public void setIdSplitDetailPk(Long idSplitDetailPk) {
		this.idSplitDetailPk = idSplitDetailPk;
	}

	public SplitCouponRequest getSplitCouponRequest() {
		return splitCouponRequest;
	}

	public void setSplitCouponRequest(SplitCouponRequest splitCouponRequest) {
		this.splitCouponRequest = splitCouponRequest;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (Validations.validateIsNotNullAndNotEmpty(loggerUser)) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();                       
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

}
