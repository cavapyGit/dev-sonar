package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum UnblockAccreditationOperationStateType {
	REGISTERED(new Integer(2312),"REGISTRADO"),
	CONFIRMED(new Integer(2313),"CONFIRMADO"),
	APPROVED(new Integer(2314),"APROBADO"),
	ANNULLED(new Integer(2315),"ANULADO"),
	REJECTED(new Integer(2316),"RECHAZADO");
	
	private Integer code;
	private String value;
	public static final List<UnblockAccreditationOperationStateType> list = new ArrayList<UnblockAccreditationOperationStateType>();
	
	public static final Map<Integer, UnblockAccreditationOperationStateType> lookup = new HashMap<Integer, UnblockAccreditationOperationStateType>();
	public static List<UnblockAccreditationOperationStateType> listSomeElements(UnblockAccreditationOperationStateType... transferSecuritiesTypeParams){
		List<UnblockAccreditationOperationStateType> retorno = new ArrayList<UnblockAccreditationOperationStateType>();
		for(UnblockAccreditationOperationStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (UnblockAccreditationOperationStateType s : EnumSet.allOf(UnblockAccreditationOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private UnblockAccreditationOperationStateType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	public static UnblockAccreditationOperationStateType get(Integer code) {
		return lookup.get(code);
	}
}
