package com.pradera.model.custody.transfersecurities;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the BLOCK_OPERATION_TRANSFER database table.
 * 
 */
@Entity
@Table(name="BLOCK_OPERATION_TRANSFER")
public class BlockOperationTransfer implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ID_BLOCK_OPERATION_TRANSFER_PK_GENERATOR", sequenceName="SQ_ID_BLOCK_OPERATION_TRANS_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_BLOCK_OPERATION_TRANSFER_PK_GENERATOR")	
	@Column(name="ID_BLOCK_OPERATION_TRANSFER_PK")
	private Long idBlockOperationTransferPk;

	@ManyToOne
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

	@ManyToOne
	@JoinColumn(name="ID_TRANSFER_OPERATION_FK")
	private SecurityTransferOperation securityTransferOperation;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="ORIGINAL_BLOCK_BALANCE")
	private BigDecimal originalBlockBalance;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REF_BLOCK_OPERATION_FK")
	private BlockOperation refBlockOperation;

	public BlockOperationTransfer() {
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getOriginalBlockBalance() {
		return this.originalBlockBalance;
	}

	public void setOriginalBlockBalance(BigDecimal originalBlockBalance) {
		this.originalBlockBalance = originalBlockBalance;
	}

	public Long getIdBlockOperationTransferPk() {
		return idBlockOperationTransferPk;
	}

	public void setIdBlockOperationTransferPk(Long idBlockOperationTransferPk) {
		this.idBlockOperationTransferPk = idBlockOperationTransferPk;
	}

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	public SecurityTransferOperation getSecurityTransferOperation() {
		return securityTransferOperation;
	}

	public void setSecurityTransferOperation(
			SecurityTransferOperation securityTransferOperation) {
		this.securityTransferOperation = securityTransferOperation;
	}

	public BlockOperation getRefBlockOperation() {
		return refBlockOperation;
	}

	public void setRefBlockOperation(BlockOperation refBlockOperation) {
		this.refBlockOperation = refBlockOperation;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}