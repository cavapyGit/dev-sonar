package com.pradera.model.custody.affectation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * The Enum AffectationType.
 */
public enum AffectationType {	
	
	/** The pawn. */
	PAWN(new Integer(1115), "GRAVAMEN"),
	
	/** The reserve. */
	RESERVE(new Integer(1116),"ENCAJE"),
	
	/** The others. */
	OTHERS(new Integer(1118), "OTROS"),
	
	/** The ban. */
	BAN(new Integer(1117),"EMBARGO"),
	
	OPPOSITION(new Integer(1146),"OPOSICION");

	private Integer code;
	private String value;
	public static final List<AffectationType> list = new ArrayList<AffectationType>();
	
	public static final Map<Integer, AffectationType> lookup = new HashMap<Integer, AffectationType>();
	public static List<AffectationType> listSomeElements(AffectationType... AffectationType){
		List<AffectationType> retorno = new ArrayList<AffectationType>();
		for(AffectationType AffectationTypeO: AffectationType){
			retorno.add(AffectationTypeO);
		}
		return retorno;
	}
	static {
		for (AffectationType s : EnumSet.allOf(AffectationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private AffectationType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static AffectationType get(Integer code) {
		return lookup.get(code);
	}

}
