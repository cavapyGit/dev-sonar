package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the BLOCK_OPERATION_CERTIFICATION database table.
 * 
 */
@Entity
@Table(name="BLOCK_OPERATION_CERTIFICATION")
public class BlockOperationCertification implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BLOCK_OPERATION_CERTIFICATION_BLOCKOPERATIONCERTPK_GENERATOR", sequenceName="SQ_ID_BLOCKOPERATION_CERT", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_OPERATION_CERTIFICATION_BLOCKOPERATIONCERTPK_GENERATOR")
	@Column(name="BLOCK_OPERATION_CERT_PK")
	private Long blockOperationCertPk;

	@Column(name="BLOCK_BALANCE_CERTIFICATION")
	private BigDecimal blockBalanceCertification;

	@Column(name="BLOCK_LEVEL")
	private Integer blockLevel;

	@Column(name="BLOCK_TYPE")
	private Integer blockType;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to AccreditationDetail
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCREDITATION_DETAIL_FK")
	private AccreditationDetail accreditationDetail;

	public Integer getBlockLevel() {
		return blockLevel;
	}

	public void setBlockLevel(Integer blockLevel) {
		this.blockLevel = blockLevel;
	}

	public Integer getBlockType() {
		return blockType;
	}

	public void setBlockType(Integer blockType) {
		this.blockType = blockType;
	}

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	
    public BlockOperationCertification() {
    }

	public Long getBlockOperationCertPk() {
		return blockOperationCertPk;
	}

	public void setBlockOperationCertPk(Long blockOperationCertPk) {
		this.blockOperationCertPk = blockOperationCertPk;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public AccreditationDetail getAccreditationDetail() {
		return this.accreditationDetail;
	}

	public void setAccreditationDetail(AccreditationDetail accreditationDetail) {
		this.accreditationDetail = accreditationDetail;
	}

	public BigDecimal getBlockBalanceCertification() {
		return blockBalanceCertification;
	}

	public void setBlockBalanceCertification(BigDecimal blockBalanceCertification) {
		this.blockBalanceCertification = blockBalanceCertification;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();            
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}