package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ChangeOwnershipRejectMotiveType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/03/2013
 */
public enum ChangeOwnershipRejectMotiveType {
		
	/** The MOTIV e_ rejec t_1. */
	MOTIVE_REJECT_1(Integer.valueOf(1356),"ERROR EN DATOS"),
	
	/** The MOTIV e_ rejec t_2. */
	MOTIVE_REJECT_2(Integer.valueOf(1357),"ERROR EN MOTIVO"),
	
	/** The MOTIV e_ rejec t_3. */
	MOTIVE_REJECT_3(Integer.valueOf(1358),"INCUMPLIMIENTO DE REQUISITOS"),
	
	/** The other motive reject. */
	OTHER_MOTIVE_REJECT(Integer.valueOf(1359),"OTROS");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ChangeOwnershipRejectMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<ChangeOwnershipRejectMotiveType> list = new ArrayList<ChangeOwnershipRejectMotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ChangeOwnershipRejectMotiveType> lookup = new HashMap<Integer, ChangeOwnershipRejectMotiveType>();
	static {
		for (ChangeOwnershipRejectMotiveType s : EnumSet.allOf(ChangeOwnershipRejectMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant block motive type
	 */
	public static ChangeOwnershipRejectMotiveType get(Integer code) {
		return lookup.get(code);
	}

}
