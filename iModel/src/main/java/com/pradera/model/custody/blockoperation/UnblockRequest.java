package com.pradera.model.custody.blockoperation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;


/**
 * The persistent class for the UNBLOCK_OPERATION database table.
 * 
 */
@Entity
@Table(name="UNBLOCK_REQUEST")
public class UnblockRequest implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="UNBLOCK_REQUEST_IDUNBLOCKREQUESTPK_GENERATOR", sequenceName="SQ_ID_UNBLOCK_REQUEST_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UNBLOCK_REQUEST_IDUNBLOCKREQUESTPK_GENERATOR")	
	@Column(name="ID_UNBLOCK_REQUEST_PK")
	private Long idUnblockRequestPk;	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")	
	private Participant participant;
	
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CONFIRMATION_DATE")
	private Date confirmationDate;

	@Column(name = "CONFIRMATION_USER")
	private String confirmationUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTRY_DATE")
	private Date registryDate;

	@Column(name = "REGISTRY_USER")
	private String registryUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REJECT_DATE")
	private Date rejectDate;

	@Column(name = "REJECT_USER")
	private String rejectUser;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MOTIVE")
	private Integer motive;

	@Column(name="UNBLOCK_REQUEST_STATE")
	private Integer unblockRequestState;

	@Column(name="REQUEST_QUANTITY")
	private BigDecimal requestQuantity;

	// bi-directional many-to-one association to BlockOperation
	@OneToMany(mappedBy = "unblockRequest", cascade=CascadeType.ALL)
	private List<UnblockOperation> unblockOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_HOLDER_FK")
	private Holder holder;
	
	@Column(name = "APPROVE_USER")
	private String approveUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "APPROVE_DATE")
	private Date approveDate;
	
	@Column(name = "ANNUL_USER")
	private String annulUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ANNUL_DATE")
	private Date annulDate;
	
	
	@OneToMany(mappedBy = "unblockRequest", cascade=CascadeType.ALL)
	private List<UnblockRequestFile> unblockRequestFile;
	
    public UnblockRequest() {
    }

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	
	public Long getIdUnblockRequestPk() {
		return idUnblockRequestPk;
	}

	public void setIdUnblockRequestPk(Long idUnblockRequestPk) {
		this.idUnblockRequestPk = idUnblockRequestPk;
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getConfirmationUser() {
		return confirmationUser;
	}

	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	

	public Integer getUnblockRequestState() {
		return unblockRequestState;
	}

	public void setUnblockRequestState(Integer unblockRequestState) {
		this.unblockRequestState = unblockRequestState;
	}

	public List<UnblockOperation> getUnblockOperation() {
		return unblockOperation;
	}

	public void setUnblockOperation(List<UnblockOperation> unblockOperation) {
		this.unblockOperation = unblockOperation;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public BigDecimal getRequestQuantity() {
		return requestQuantity;
	}

	public void setRequestQuantity(BigDecimal requestQuantity) {
		this.requestQuantity = requestQuantity;
	}
	
	public List<UnblockRequestFile> getUnblockRequestFile() {
		return unblockRequestFile;
	}

	public void setUnblockRequestFile(List<UnblockRequestFile> unblockRequestFile) {
		this.unblockRequestFile = unblockRequestFile;
	}

	public String getApproveUser() {
		return approveUser;
	}

	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getAnnulUser() {
		return annulUser;
	}

	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}

	public Date getAnnulDate() {
		return annulDate;
	}

	public void setAnnulDate(Date annulDate) {
		this.annulDate = annulDate;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(Validations.validateIsNotNull(this.getUnblockOperation())){
            	for (UnblockOperation unblockOperationTmp : this.getUnblockOperation()) {
            		unblockOperationTmp.setAudit(loggerUser);
				}
            }
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		   HashMap<String,List<? extends Auditable>> detailsMap =
	                new HashMap<String, List<? extends Auditable>>();
	        detailsMap.put("unblockOperation", unblockOperation);
	        detailsMap.put("unblockRequestFile", unblockRequestFile);
	        return detailsMap;
	}
	
	
	
}