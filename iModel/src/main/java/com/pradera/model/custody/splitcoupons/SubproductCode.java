package com.pradera.model.custody.splitcoupons;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="SUBPRODUCT_CODE")
public class SubproductCode implements Serializable,Auditable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SUBPRODUCT_CODE_IDSUBPRODUCTCODEPK_GENERATOR", sequenceName="SQ_ID_SUBPRODUCT_CODE_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SUBPRODUCT_CODE_IDSUBPRODUCTCODEPK_GENERATOR")	
	@Column(name="ID_SUBPRODUCT_CODE_PK")
	private Long idSubprodcutCodePk;	
	
	@Column(name="FIRST_LETTER_CODE")
	private String firstLetterCode;
	
	@Column(name="SECOND_LETTER_CODE")
	private String secondLetterCode;
	
	@Column(name="THIRD_LETTER_CODE")
	private String thirdLetterCode;
	
	@Column(name="FOURTH_LETTER_CODE")
	private String fourthLetterCode;
	
	@Column(name="COUPONS_RELATED")
	private String couponsRelated;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	
	public Long getIdSubprodcutCodePk() {
		return idSubprodcutCodePk;
	}

	public void setIdSubprodcutCodePk(Long idSubprodcutCodePk) {
		this.idSubprodcutCodePk = idSubprodcutCodePk;
	}

	public String getFirstLetterCode() {
		return firstLetterCode;
	}

	public void setFirstLetterCode(String firstLetterCode) {
		this.firstLetterCode = firstLetterCode;
	}

	public String getSecondLetterCode() {
		return secondLetterCode;
	}

	public void setSecondLetterCode(String secondLetterCode) {
		this.secondLetterCode = secondLetterCode;
	}

	public String getThirdLetterCode() {
		return thirdLetterCode;
	}

	public void setThirdLetterCode(String thirdLetterCode) {
		this.thirdLetterCode = thirdLetterCode;
	}

	public String getFourthLetterCode() {
		return fourthLetterCode;
	}

	public void setFourthLetterCode(String fourthLetterCode) {
		this.fourthLetterCode = fourthLetterCode;
	}

	public String getCouponsRelated() {
		return couponsRelated;
	}

	public void setCouponsRelated(String couponsRelated) {
		this.couponsRelated = couponsRelated;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(Validations.validateIsNotNullAndNotEmpty(loggerUser)){
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
