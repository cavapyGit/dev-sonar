package com.pradera.model.custody.changeownership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;


/**
 * The persistent class for the BLOCK_CHANGE_OWNERSHIP database table.
 * 
 */
@Entity
@Table(name="BLOCK_CHANGE_OWNERSHIP")
public class BlockChangeOwnership implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BLOCK_CHANGE_OWNERSHIP_GENERATOR", sequenceName="SQ_ID_BLK_CHANGE_OWNERSHIP_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_CHANGE_OWNERSHIP_GENERATOR")
	@Column(name="ID_BLOCK_CHANGE_OWNERSHIP_PK")
	private Long idBlockChangeOwnershipPk;

	@Column(name="BLOCK_QUANTITY")
	private BigDecimal blockQuantity;

	//bi-directional many-to-one association to blockOperationDetail
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

    //bi-directional many-to-one association to unblockOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REF_BLOCK_OPERATION_FK")
	private BlockOperation refBlockOperation;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to ChangeOwnershipDetail
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANGE_OWNERSHIP_DETAIL_FK")
	private ChangeOwnershipDetail changeOwnershipDetail;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK",referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;
	
	@OneToMany(mappedBy="blockChangeOwnership",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<ChangeOwnershipMarketFact> changeOwnershipMarketFacts;
    
    @Transient
	private Integer affectationType;
	    
    public BlockChangeOwnership() {
    }

	public Long getIdBlockChangeOwnershipPk() {
		return this.idBlockChangeOwnershipPk;
	}

	public void setIdBlockChangeOwnershipPk(Long idBlockChangeOwnershipPk) {
		this.idBlockChangeOwnershipPk = idBlockChangeOwnershipPk;
	}

	public BigDecimal getBlockQuantity() {
		return this.blockQuantity;
	}

	public void setBlockQuantity(BigDecimal blockQuantity) {
		this.blockQuantity = blockQuantity;
	}

	
	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}


	public BlockOperation getRefBlockOperation() {
		return refBlockOperation;
	}

	public void setRefBlockOperation(BlockOperation refBlockOperation) {
		this.refBlockOperation = refBlockOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public ChangeOwnershipDetail getChangeOwnershipDetail() {
		return this.changeOwnershipDetail;
	}

	public void setChangeOwnershipDetail(ChangeOwnershipDetail changeOwnershipDetail) {
		this.changeOwnershipDetail = changeOwnershipDetail;
	}
	
	public Integer getAffectationType() {
		return affectationType;
	}

	public void setAffectationType(Integer affectationType) {
		this.affectationType = affectationType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("changeOwnershipMarketFacts", changeOwnershipMarketFacts);
		return detailsMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((blockOperation == null) ? 0 : blockOperation.hashCode());
		result = prime
				* result
				+ ((idBlockChangeOwnershipPk == null) ? 0
						: idBlockChangeOwnershipPk.hashCode());
		result = prime
				* result
				+ ((refBlockOperation == null) ? 0 : refBlockOperation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockChangeOwnership other = (BlockChangeOwnership) obj;
		if (blockOperation == null) {
			if (other.blockOperation != null)
				return false;
		} else if (!blockOperation.equals(other.blockOperation))
			return false;
		if (idBlockChangeOwnershipPk == null) {
			if (other.idBlockChangeOwnershipPk != null)
				return false;
		} else if (!idBlockChangeOwnershipPk
				.equals(other.idBlockChangeOwnershipPk))
			return false;
		if (refBlockOperation == null) {
			if (other.refBlockOperation != null)
				return false;
		} else if (!refBlockOperation.equals(other.refBlockOperation))
			return false;
		return true;
	}

	public List<ChangeOwnershipMarketFact> getChangeOwnershipMarketFacts() {
		return changeOwnershipMarketFacts;
	}

	public void setChangeOwnershipMarketFacts(
			List<ChangeOwnershipMarketFact> changeOwnershipMarketFacts) {
		this.changeOwnershipMarketFacts = changeOwnershipMarketFacts;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
}