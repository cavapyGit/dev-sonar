package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AccreditationOperationRejectMotiveType {
	INCORRECT_DOCUMENTATION(new Integer(684),"DOCUMENTACION INCORRECTA"),
	INCORRECT_DATE_UPLOADED(new Integer(685),"DATOS CARGADOS INCORRECTOS"),
	INCORRECT_PARTICIPANT(new Integer(686),"PARTICIPANTE INCORRECTO"),
	INCORRECT_ISSUANCE(new Integer(687),"EMISON INCORRECTA"),
	EXCEEDED_TIME_ALLOWED(new Integer(1531),"SOLICITUD EXCEDIA TIEMPO PERMITIDO"),
	OTHERS(new Integer(698),"OTROS");
	
	private Integer code;
	private String value;
	public static final List<AccreditationOperationRejectMotiveType> list = new ArrayList<AccreditationOperationRejectMotiveType>();
	
	public static final Map<Integer, AccreditationOperationRejectMotiveType> lookup = new HashMap<Integer, AccreditationOperationRejectMotiveType>();
	public static List<AccreditationOperationRejectMotiveType> listSomeElements(AccreditationOperationRejectMotiveType... AccreditationOperationRejectMotiveType){
		List<AccreditationOperationRejectMotiveType> retorno = new ArrayList<AccreditationOperationRejectMotiveType>();
		for(AccreditationOperationRejectMotiveType AccreditationOperationRejectMotiveTypeO: AccreditationOperationRejectMotiveType){
			retorno.add(AccreditationOperationRejectMotiveTypeO);
		}
		return retorno;
	}
	static {
		for (AccreditationOperationRejectMotiveType s : EnumSet.allOf(AccreditationOperationRejectMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private AccreditationOperationRejectMotiveType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static AccreditationOperationRejectMotiveType get(Integer code) {
		return lookup.get(code);
	}
}
