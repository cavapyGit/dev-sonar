package com.pradera.model.custody.physical;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.securitiesannotation.AccAnnotationOperationFile;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the PHYSICAL_CERTIFICATE database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name = PhysicalCertificate.PHYSICAL_CERTIFICATE_LIST, query = "Select distinct p From PhysicalCertificate p WHERE p.state = :state")
})
@Entity
@Table(name="PHYSICAL_CERTIFICATE")
public class PhysicalCertificate implements Serializable,Auditable,Cloneable{

	private static final long serialVersionUID = 1L;
	
	public final static String PHYSICAL_CERTIFICATE_LIST = "PhysicalCertificate.searchAllCertificate";

	@Id
	@SequenceGenerator(name="PHYSICAL_CERTIFICATE_GENERATOR", sequenceName="SQ_ID_PHYSICAL_CERTIFICATE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PHYSICAL_CERTIFICATE_GENERATOR")
	@Column(name="ID_PHYSICAL_CERTIFICATE_PK")
	private Long idPhysicalCertificatePk;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

	@Column(name="CERTIFICATE_QUANTITY")
	private BigDecimal certificateQuantity;
	
	@Column(name="CERTIFICATE_NUMBER")
	private String certificateNumber;
	
	@Column(name="SERIAL_NUMBER")
	private String serialNumber;

	@Column(name="CURRENCY")
	private Integer currency;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="ISSUE_DATE")
	private Date issueDate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

	@Column(name="REGISTER_USER")
	private String registerUser;

	@Column(name="SECURITY_CLASS")
	private Integer securityClass;

	@Column(name="SITUATION")
	private Integer situation;

	@Column(name="\"STATE\"")
	private Integer state;
	
	@Column(name="INSTRUMENT_TYPE")
	private Integer instrumentType;
	
	@Column(name="PAYMENT_PERIODICITY")
	private Integer paymentPeriodicity;
	
	@Column(name="INTEREST_RATE")
	private BigDecimal interestRate;

	@Column(name = "BRANCH_OFFICE")
	private Integer branchOffice;

	@Column(name = "VAULT_LOCATION")
	private Integer vaultLocation;
	
	@Column(name="ID_PENDING_SECURITY_CODE_PK")
	private String idPendingSecurityCodePk;
	
	@Column(name="PHYSICAL_CERTIFICATE_REFERENCE")
	private Long PhysicalCertificateReference;

	@Column(name="ID_PROGRAM_INTEREST_FK")
	private Long idProgramInterestFk;

	@Column(name="IND_ELECTRONIC_CUPON")
	private Integer indElectroniCupon;

	@Column(name="IND_CUPON")
	private Integer indCupon = 0;
	
	@Column(name="CUPON_NUMBER")
	private Integer cuponNumber;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

	@Column(name="NOMINAL_AMOUNT")
	private BigDecimal nominalAmount;

	@Column(name="MOTIVE")
	private Integer motive;
	
	@Transient
	private Boolean blIsCupon;
	
	@Transient
	private String certificateFull;
	
	//@Temporal( TemporalType.TIMESTAMP)
	//@Column(name="CERTIFICATE_DATE")
	@Transient
	private Date certificateDate;
	
	//@Column(name="IND_EXISTS_SECURITY")
	@Transient
	private Integer indExistsSecurity;

	@Transient
	private Boolean blCheck;
	
	public Date getCertificateDate() {
		return certificateDate;
	}

	public void setCertificateDate(Date certificateDate) {
		this.certificateDate = certificateDate;
	}
	
	/** issue 895: se adiciona este elemento para los valores que NO exiten y fueron registrados en el DEPOSITO DE TITULOS FISICOS */
	@Transient
	private String idSecurityCodePk;

	@Transient
	private String stateType;
	
	@Transient
	private String situationType;
	
	@Transient
	private String instrumentTypeName;
	
	@Transient
	private String currencyType;
	
	@Transient
	private String classType;
	
	@Transient
	private String issuerName;
	
	@Transient
	private String paymentType;
	
	@Transient
	private Long requestNumber;

	@Transient
	private Integer couponCount;
	
	@ManyToOne
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;
	
	@Transient
	private String currencyFullDesc;

	@Transient
	private boolean blSelected;

	@Transient
	private String vaultLocationDesc;
	
	@Transient
	private HolderAccount holderAccount;
	
	@Transient
	private Participant participant;
	
	//bi-directional many-to-one association to CertificateSituationHistory
	@OneToMany(mappedBy="physicalCertificate",cascade=CascadeType.PERSIST)
	private List<CertificateSituationHistory> certificateSituationHistories;

	//bi-directional many-to-one association to PhysicalBalanceDetail
	@OneToMany(mappedBy="physicalCertificate")
	private List<PhysicalBalanceDetail> physicalBalanceDetails;

	//bi-directional many-to-one association to PhysicalCertificateMovement
	@OneToMany(mappedBy="physicalCertificate",cascade=CascadeType.PERSIST)
	private List<PhysicalCertificateMovement> physicalCertificateMovements;

	//bi-directional many-to-one association to PhysicalOperationDetail
	@OneToMany(mappedBy="physicalCertificate",cascade={CascadeType.ALL})
	private List<PhysicalOperationDetail> physicalOperationDetails;

	//bi-directional many-to-one association to PhysicalOperationDetail
	@OneToMany(mappedBy="physicalCertificate",cascade={CascadeType.ALL})
	private List<PhysicalCertMarketfact> physicalCertMarketfacts;
	/*
	@OneToMany(mappedBy = "idPhysicalCertificateFk", cascade={CascadeType.ALL}, fetch = FetchType.LAZY)
    private List<PhysicalCertFile> physicalCertFileList;
	
	@JoinColumn(name = "ID_PHYSICAL_CERTIFICATE_FILE_FK", referencedColumnName = "ID_PHYSICAL_CERTIFICATE_FILE_PK")
	@ManyToOne(fetch = FetchType.LAZY)
    private PhysicalCertificateFile idPhysicalCertificateFileFk;
	*/
	public Long getIdPhysicalCertificatePk() {
		return idPhysicalCertificatePk;
	}

	public void setIdPhysicalCertificatePk(Long idPhysicalCertificatePk) {
		this.idPhysicalCertificatePk = idPhysicalCertificatePk;
	}

	public Security getSecurities() {
		return securities;
	}
	/*
	public List<PhysicalCertFile> getPhysicalCertFileList() {
		return physicalCertFileList;
	}

	public void setPhysicalCertFileList(List<PhysicalCertFile> physicalCertFileList) {
		this.physicalCertFileList = physicalCertFileList;
	}
	*/
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public BigDecimal getCertificateQuantity() {
		return certificateQuantity;
	}

	public void setCertificateQuantity(BigDecimal certificateQuantity) {
		this.certificateQuantity = certificateQuantity;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public Integer getIndExistsSecurity() {
		return indExistsSecurity;
	}

	public void setIndExistsSecurity(Integer indExistsSecurity) {
		this.indExistsSecurity = indExistsSecurity;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Integer getSituation() {
		return situation;
	}

	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public Integer getPaymentPeriodicity() {
		return paymentPeriodicity;
	}

	public void setPaymentPeriodicity(Integer paymentPeriodicity) {
		this.paymentPeriodicity = paymentPeriodicity;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public String getStateType() {
		return stateType;
	}

	public void setStateType(String stateType) {
		this.stateType = stateType;
	}

	public String getSituationType() {
		return situationType;
	}

	public void setSituationType(String situationType) {
		this.situationType = situationType;
	}

	public String getInstrumentTypeName() {
		return instrumentTypeName;
	}

	public void setInstrumentTypeName(String instrumentTypeName) {
		this.instrumentTypeName = instrumentTypeName;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Long getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	public Integer getCouponCount() {
		return couponCount;
	}

	public void setCouponCount(Integer couponCount) {
		this.couponCount = couponCount;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public List<CertificateSituationHistory> getCertificateSituationHistories() {
		return certificateSituationHistories;
	}

	public void setCertificateSituationHistories(
			List<CertificateSituationHistory> certificateSituationHistories) {
		this.certificateSituationHistories = certificateSituationHistories;
	}

	public List<PhysicalBalanceDetail> getPhysicalBalanceDetails() {
		return physicalBalanceDetails;
	}

	public void setPhysicalBalanceDetails(
			List<PhysicalBalanceDetail> physicalBalanceDetails) {
		this.physicalBalanceDetails = physicalBalanceDetails;
	}

	public List<PhysicalCertificateMovement> getPhysicalCertificateMovements() {
		return physicalCertificateMovements;
	}

	public void setPhysicalCertificateMovements(
			List<PhysicalCertificateMovement> physicalCertificateMovements) {
		this.physicalCertificateMovements = physicalCertificateMovements;
	}

	public List<PhysicalOperationDetail> getPhysicalOperationDetails() {
		return physicalOperationDetails;
	}

	public void setPhysicalOperationDetails(
			List<PhysicalOperationDetail> physicalOperationDetails) {
		this.physicalOperationDetails = physicalOperationDetails;
	}
	
	public List<PhysicalCertMarketfact> getPhysicalCertMarketfacts() {
		return physicalCertMarketfacts;
	}

	public void setPhysicalCertMarketfacts(
			List<PhysicalCertMarketfact> physicalCertMarketfacts) {
		this.physicalCertMarketfacts = physicalCertMarketfacts;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("certificateSituationHistories", certificateSituationHistories);
		detailsMap.put("physicalBalanceDetails", physicalBalanceDetails);
		detailsMap.put("physicalCertificateMovements", physicalCertificateMovements);
		detailsMap.put("physicalOperationDetails", physicalOperationDetails);
		detailsMap.put("physicalCertMarketfacts", physicalCertMarketfacts);
		
        return detailsMap;
	}
	
	public String getCurrencyFullDesc() {
		return currencyFullDesc;
	}

	public void setCurrencyFullDesc(String currencyFullDesc) {
		this.currencyFullDesc = currencyFullDesc;
	}
	/*
	public PhysicalCertificateFile getIdPhysicalCertificateFileFk() {
		return idPhysicalCertificateFileFk;
	}

	public void setIdPhysicalCertificateFileFk(PhysicalCertificateFile idPhysicalCertificateFileFk) {
		this.idPhysicalCertificateFileFk = idPhysicalCertificateFileFk;
	}*/

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Integer getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}

	public Integer getVaultLocation() {
		return vaultLocation;
	}

	public void setVaultLocation(Integer vaultLocation) {
		this.vaultLocation = vaultLocation;
	}

	public boolean isBlSelected() {
		return blSelected;
	}

	public void setBlSelected(boolean blSelected) {
		this.blSelected = blSelected;
	}

	public String getVaultLocationDesc() {
		return vaultLocationDesc;
	}

	public void setVaultLocationDesc(String vaultLocationDesc) {
		this.vaultLocationDesc = vaultLocationDesc;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getIdPendingSecurityCodePk() {
		return idPendingSecurityCodePk;
	}

	public void setIdPendingSecurityCodePk(String idPendingSecurityCodePk) {
		this.idPendingSecurityCodePk = idPendingSecurityCodePk;
	}

	public Long getPhysicalCertificateReference() {
		return PhysicalCertificateReference;
	}

	public void setPhysicalCertificateReference(Long physicalCertificateReference) {
		PhysicalCertificateReference = physicalCertificateReference;
	}

	public Long getIdProgramInterestFk() {
		return idProgramInterestFk;
	}

	public void setIdProgramInterestFk(Long idProgramInterestFk) {
		this.idProgramInterestFk = idProgramInterestFk;
	}

	public Integer getIndElectroniCupon() {
		return indElectroniCupon;
	}

	public void setIndElectroniCupon(Integer indElectroniCupon) {
		this.indElectroniCupon = indElectroniCupon;
	}

	public Integer getIndCupon() {
		return indCupon;
	}

	public void setIndCupon(Integer indCupon) {
		this.indCupon = indCupon;
	}

	public Integer getCuponNumber() {
		return cuponNumber;
	}

	public void setCuponNumber(Integer cuponNumber) {
		this.cuponNumber = cuponNumber;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getStateDescription() {
		String description = "";
		if(state!=null) {
			if(PhysicalCertificateStateType.get(state)!=null) {
				description = PhysicalCertificateStateType.get(state).getValue();
			}
		}
		return description;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Date getBeginingDate() {
		return beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}
	
	public boolean getIndCuponFlag() {
		if(indCupon != null && indCupon == 1) {
			return true;
		}
		return false;
	}

	public BigDecimal getNominalAmount() {
		return nominalAmount;
	}

	public void setNominalAmount(BigDecimal nominalAmount) {
		this.nominalAmount = nominalAmount;
	}

	public Boolean getBlCheck() {
		return blCheck;
	}

	public void setBlCheck(Boolean blCheck) {
		this.blCheck = blCheck;
	}

	public Boolean getBlIsCupon() {
		if(blIsCupon == null) {
			if(indCupon != null && indCupon == 1) {
				return true;
			}
			return false;
		}
		return blIsCupon;
	}

	public void setBlIsCupon(Boolean blIsCupon) {
		this.blIsCupon = blIsCupon;
	}

	public String getCertificateFull() {
		if(certificateFull == null) {
			if(cuponNumber != null) {
				return serialNumber+""+certificateNumber+"-"+cuponNumber;
			}
			return serialNumber+""+certificateNumber;
		}
		return certificateFull;
	}

	public void setCertificateFull(String certificateFull) {
		this.certificateFull = certificateFull;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	@Override
	public PhysicalCertificate clone() throws CloneNotSupportedException {
		return (PhysicalCertificate)super.clone();
	}

	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	@Override
	public String toString() {
		return "PhysicalCertificate [idPhysicalCertificatePk=" + idPhysicalCertificatePk + ", securities=" + securities
				+ ", certificateQuantity=" + certificateQuantity + ", certificateNumber=" + certificateNumber
				+ ", serialNumber=" + serialNumber + ", currency=" + currency + ", expirationDate=" + expirationDate
				+ ", issueDate=" + issueDate + ", lastModifyApp=" + lastModifyApp + ", lastModifyDate=" + lastModifyDate
				+ ", lastModifyIp=" + lastModifyIp + ", lastModifyUser=" + lastModifyUser + ", nominalValue="
				+ nominalValue + ", registerDate=" + registerDate + ", registerUser=" + registerUser
				+ ", securityClass=" + securityClass + ", situation=" + situation + ", state=" + state
				+ ", instrumentType=" + instrumentType + ", paymentPeriodicity=" + paymentPeriodicity
				+ ", interestRate=" + interestRate + ", branchOffice=" + branchOffice + ", vaultLocation="
				+ vaultLocation + ", idPendingSecurityCodePk=" + idPendingSecurityCodePk
				+ ", PhysicalCertificateReference=" + PhysicalCertificateReference + ", idProgramInterestFk="
				+ idProgramInterestFk + ", indElectroniCupon=" + indElectroniCupon + ", indCupon=" + indCupon
				+ ", cuponNumber=" + cuponNumber + ", paymentDate=" + paymentDate + ", registryDate=" + registryDate
				+ ", beginingDate=" + beginingDate + ", nominalAmount=" + nominalAmount + ", motive=" + motive
				+ ", blIsCupon=" + blIsCupon + ", certificateFull=" + certificateFull + ", certificateDate="
				+ certificateDate + ", indExistsSecurity=" + indExistsSecurity + ", blCheck=" + blCheck
				+ ", idSecurityCodePk=" + idSecurityCodePk + ", stateType=" + stateType + ", situationType="
				+ situationType + ", instrumentTypeName=" + instrumentTypeName + ", currencyType=" + currencyType
				+ ", classType=" + classType + ", issuerName=" + issuerName + ", paymentType=" + paymentType
				+ ", requestNumber=" + requestNumber + ", couponCount=" + couponCount + ", issuer=" + issuer
				+ "]";
	}
	
}