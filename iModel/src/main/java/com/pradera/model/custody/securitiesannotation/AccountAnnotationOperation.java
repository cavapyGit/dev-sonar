package com.pradera.model.custody.securitiesannotation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.SecuritiesManager;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;


/**
 * The persistent class for the ACCOUNT_ANNOTATION_OPERATION database table.
 * 
 */
@Entity
@Table(name="ACCOUNT_ANNOTATION_OPERATION")
public class AccountAnnotationOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long idAnnotationOperationPk;
	
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_ANNOTATION_OPERATION_PK")
	private CustodyOperation custodyOperation;

	@Column(name="CERTIFICATE_NUMBER")
	private String certificateNumber;

    @Temporal( TemporalType.DATE)
	@Column(name="EXPEDITION_DATE")
	private Date expeditionDate;

    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK",referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccount;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PLACEMENT_PARTICIPANT_FK",referencedColumnName="ID_PARTICIPANT_PK")
	private Participant placementParticipant;

    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITIES_MANAGER_FK")
	private SecuritiesManager securitiesManager;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;
    
    @JoinColumn(name = "ID_ACC_ANNOTATION_FILE_FK", referencedColumnName = "ID_FILE_PK")
	@ManyToOne(fetch = FetchType.LAZY)
    private AccAnnotationOperationFile idAccAnnotationFileFk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MOTIVE")
	private Integer motive;

	@Column(name="SERIAL_NUMBER")
	private String serialNumber;

	@Column(name="STATE_ANNOTATION")
	private Integer state;

	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;

	@Column(name="CERTIFICATE_FROM")
	private Integer certificateFrom;

	@Column(name="CERTIFICATE_TO")
	private Integer certificateTo;

    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_BENEF_FK",referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccountBenef;

    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_DEBTOR_FK",referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccountDebtor;
    
	@Transient
	private String stateDescription;
	
	@Transient
	private String motiveDescription;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Column(name="MOTIVE_REJECT")
	private Integer motiveReject;
	
	@OneToMany(mappedBy="accountAnnotationOperation", fetch = FetchType.LAZY)
	private List<AccountAnnotationMarketFact> accountAnnotationMarketFact;
	
	@Column(name="CREDITOR_SOURCE")
	private String creditorSource;
	
	@Column(name="ALTERNATIVE_CODE")
	private String alternativeCode;
	
	@Column(name="IND_PRIMARY_SETTLEMENT")
	private Integer indPrimarySettlement;
	
	@Column(name="IND_LIEN")
	private Integer indLien;
	
	@Column(name="DEPARTMENT")
	private Integer department;
	
	@Column(name="PROVINCE")
	private Integer province;
	
	@Column(name="MUNICIPALITY")
	private Integer municipality;
	
	@Column(name="OBLIGATION_NUMBER")
	private String obligationNumber;
	
	@Column(name="OPERATION_NUMBER")
	private Long operationNumber;

	@Column(name = "SECURITY_CLASS")
	private Integer securityClass;
	
	@Column(name = "SECURITY_YIELD_RATE")
	private BigDecimal securityYieldRate;
	
	@Column(name = "SECURITY_EXPIRATION_DATE")
	private Date securityExpirationDate;
	
	@Column(name = "SECURITY_CURRENCY")
	private Integer securityCurrency;

	@Transient
	private String securityCurrencyDesc;
	
	@Column(name = "SECURITY_INTEREST_TYPE")
	private Integer securityInterestType;

	@Transient
	private String securityInterestTypeDesc;
	
	@Column(name = "SECURITY_INTEREST_RATE")
	private BigDecimal securityInterestRate;
	
	@Column(name = "IND_SERIALIZABLE")
	private Integer indSerializable = BooleanType.NO.getCode();
	
	@Column(name = "SECURITY_NOMINAL_VALUE")
	private BigDecimal securityNominalValue;

	@Column(name="BRANCH_OFFICE")
	private Integer branchOffice;

	@Column(name="IND_ELECTRONIC_CUPON")
	private Integer indElectronicCupon;		

	@Column(name="IND_DEMATERIALIZATION")
	private Integer indDematerialization;
	
	@Column(name="IND_IMMBOLIZATION")
	private Integer indImmobilization;

	@Column(name="PERIODICITY")
	private Integer periodicity;

	@Transient
	private String periodicityDesc;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="COUPON_FIRST_EXPIRATION_DATE")
	private Date couponFirstExpirationDate;

	@OneToMany(mappedBy="accountAnnotationOperation")
	private List<AccountAnnotationCupon> accountAnnotationCupons;
	
	@Temporal(TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

	@Temporal(TemporalType.DATE)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;
	

    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_SELLER_FK",referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccountSeller;

	@Column(name="CASH_PRICE")
	private BigDecimal cashPrice;

	@Column(name="AMOUNT_RATE")
	private BigDecimal amountRate;

	@Column(name="ANNOTATION_ISIN_CODE")
	private String annotationIsinCode;

	@Column(name="IND_APPROVE")
	private Integer indApprove;

	@Column(name="IND_TAX_EXONERATION")
	private Integer indTaxExoneration;	

	@Column(name="IND_GUARDA")
	private Integer indGuarda;	

	@Column(name="REFERENCE_PART_CODE")
	private String referencePartCode;
	
	/** The operation price. */
	@Column(name = "OPERATION_PRICE")
	private BigDecimal operationPrice;

	@Column(name = "IND_EXPIRATION_CUPONS")
	private Integer indExpirationCupons;

	@Transient
	private String indGuardaDesc;

	@Transient
	private String indTaxExonerationDesc;
	
	@Transient
	private String indElectronicCuponDesc;
	
	@Transient
	private String indApproveDesc;
	
	@Transient
	private Holder holder;
	
	/** The show motive text. */
	@Transient
	private boolean showMotiveText;
	
	@Transient
	private String motiveDescReject;
	
	@Transient
	private Issuance issuance;
	
	@Transient
	private String msgMassiveErrors;

	@Transient
	private String securityClassDescription;

	@Transient
	private PayrollHeader payrollHeader;

	@Transient	
	List<SecuritySerialRange> lstSecuritySerialRange;
	
	public AccountAnnotationOperation() {
    }
  
	public String getMotiveDescription() {
		return motiveDescription;
	}
	
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}

	public String getStateDescription() {		
		return stateDescription;
	}

	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	public AccAnnotationOperationFile getIdAccAnnotationFileFk() {
		return idAccAnnotationFileFk;
	}

	public void setIdAccAnnotationFileFk(AccAnnotationOperationFile idAccAnnotationFileFk) {
		this.idAccAnnotationFileFk = idAccAnnotationFileFk;
	}

	public String getCertificateNumber() {
		return this.certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public Date getExpeditionDate() {
		return this.expeditionDate;
	}

	public void setExpeditionDate(Date expeditionDate) {
		this.expeditionDate = expeditionDate;
	}

	public HolderAccount getHolderAccount() {
		return this.holderAccount;
	}

	public void setHolderAccount(HolderAccount idHolderAccountFk) {
		this.holderAccount = idHolderAccountFk;
	}

	public Security getSecurity() {
		return this.security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getMotive() {
		return this.motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public Long getIdAnnotationOperationPk() {
		return idAnnotationOperationPk;
	}

	public void setIdAnnotationOperationPk(Long idAnnotationOperationPk) {
		this.idAnnotationOperationPk = idAnnotationOperationPk;
	}
	
	public Integer getMotiveReject() {
		return motiveReject;
	}


	public void setMotiveReject(Integer motiveReject) {
		this.motiveReject = motiveReject;
	}


	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	@Override
	public void setAudit(LoggerUser loggerUser) {
	    if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser =loggerUser.getUserName();
            if(holderAccount!=null){            	
            	holderAccount.setAudit(loggerUser);
            }
            if(custodyOperation != null){
            	custodyOperation.setAudit(loggerUser);
            }            
            if(security!=null){
            	security.setLastModifyApp(lastModifyApp);
            	security.setLastModifyDate(lastModifyDate);
            	security.setLastModifyIp(lastModifyIp);
            	security.setLastModifyUser(lastModifyUser);
            }
        }
		
	}

	public List<AccountAnnotationMarketFact> getAccountAnnotationMarketFact() {
		return accountAnnotationMarketFact;
	}


	public void setAccountAnnotationMarketFact(
			List<AccountAnnotationMarketFact> accountAnnotationMarketFact) {
		this.accountAnnotationMarketFact = accountAnnotationMarketFact;
	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("accountAnnotationMarketFact", accountAnnotationMarketFact);
		return detailsMap;
	}

	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}

	public String getMotiveDescReject() {
		return motiveDescReject;
	}

	public void setMotiveDescReject(String motiveDescReject) {
		this.motiveDescReject = motiveDescReject;
	}

	public String getCreditorSource() {
		return creditorSource;
	}

	public void setCreditorSource(String creditorSource) {
		this.creditorSource = creditorSource;
	}

	public String getAlternativeCode() {
		return alternativeCode;
	}

	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}

	public Integer getIndPrimarySettlement() {
		return indPrimarySettlement;
	}

	public void setIndPrimarySettlement(Integer indPrimarySettlement) {
		this.indPrimarySettlement = indPrimarySettlement;
	}

	public Integer getIndLien() {
		return indLien;
	}

	public void setIndLien(Integer indLien) {
		this.indLien = indLien;
	}

	public Integer getDepartment() {
		return department;
	}

	public void setDepartment(Integer department) {
		this.department = department;
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getMunicipality() {
		return municipality;
	}

	public void setMunicipality(Integer municipality) {
		this.municipality = municipality;
	}
	
	public String getObligationNumber() {
		return obligationNumber;
	}

	public void setObligationNumber(String obligationNumber) {
		this.obligationNumber = obligationNumber;
	}

	public Participant getPlacementParticipant() {
		return placementParticipant;
	}

	public void setPlacementParticipant(Participant placementParticipant) {
		this.placementParticipant = placementParticipant;
	}
	
	public Long getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public SecuritiesManager getSecuritiesManager() {
		return securitiesManager;
	}

	public void setSecuritiesManager(SecuritiesManager securitiesManager) {
		this.securitiesManager = securitiesManager;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public BigDecimal getSecurityYieldRate() {
		return securityYieldRate;
	}

	public void setSecurityYieldRate(BigDecimal securityYieldRate) {
		this.securityYieldRate = securityYieldRate;
	}

	public Date getSecurityExpirationDate() {
		return securityExpirationDate;
	}

	public void setSecurityExpirationDate(Date securityExpirationDate) {
		this.securityExpirationDate = securityExpirationDate;
	}

	public Integer getSecurityCurrency() {
		return securityCurrency;
	}

	public void setSecurityCurrency(Integer securityCurrency) {
		this.securityCurrency = securityCurrency;
	}

	public Integer getSecurityInterestType() {
		return securityInterestType;
	}

	public void setSecurityInterestType(Integer securityInterestType) {
		this.securityInterestType = securityInterestType;
	}

	public BigDecimal getSecurityInterestRate() {
		return securityInterestRate;
	}

	public void setSecurityInterestRate(BigDecimal securityInterestRate) {
		this.securityInterestRate = securityInterestRate;
	}

	public Integer getIndSerializable() {
		return indSerializable;
	}

	public void setIndSerializable(Integer indSerializable) {
		this.indSerializable = indSerializable;
	}

	public BigDecimal getSecurityNominalValue() {
		return securityNominalValue;
	}

	public void setSecurityNominalValue(BigDecimal securityNominalValue) {
		this.securityNominalValue = securityNominalValue;
	}

	public Integer getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public Integer getIndElectronicCupon() {
		return indElectronicCupon;
	}

	public void setIndElectronicCupon(Integer indElectronicCupon) {
		this.indElectronicCupon = indElectronicCupon;
	}

	public List<AccountAnnotationCupon> getAccountAnnotationCupons() {
		return accountAnnotationCupons;
	}

	public void setAccountAnnotationCupons(List<AccountAnnotationCupon> accountAnnotationCupons) {
		this.accountAnnotationCupons = accountAnnotationCupons;
	}

	public Issuance getIssuance() {
		return issuance;
	}

	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}

	public Integer getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(Integer periodicity) {
		this.periodicity = periodicity;
	}

	public Date getCouponFirstExpirationDate() {
		return couponFirstExpirationDate;
	}

	public void setCouponFirstExpirationDate(Date couponFirstExpirationDate) {
		this.couponFirstExpirationDate = couponFirstExpirationDate;
	}

	public Integer getIndDematerialization() {
		return indDematerialization;
	}

	public void setIndDematerialization(Integer indDematerialization) {
		this.indDematerialization = indDematerialization;
	}

	public Integer getIndImmobilization() {
		return indImmobilization;
	}

	public void setIndImmobilization(Integer indImmobilization) {
		this.indImmobilization = indImmobilization;
	}

	public String getMsgMassiveErrors() {
		return msgMassiveErrors;
	}

	public void setMsgMassiveErrors(String msgMassiveErrors) {
		this.msgMassiveErrors = msgMassiveErrors;
	}

	public String getSecurityClassDescription() {
		return securityClassDescription;
	}

	public void setSecurityClassDescription(String securityClassDescription) {
		this.securityClassDescription = securityClassDescription;
	}

	public PayrollHeader getPayrollHeader() {
		return payrollHeader;
	}

	public void setPayrollHeader(PayrollHeader payrollHeader) {
		this.payrollHeader = payrollHeader;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Date getBeginingDate() {
		return beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public HolderAccount getHolderAccountSeller() {
		return holderAccountSeller;
	}

	public void setHolderAccountSeller(HolderAccount holderAccountSeller) {
		this.holderAccountSeller = holderAccountSeller;
	}

	public BigDecimal getCashPrice() {
		return cashPrice;
	}

	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	public BigDecimal getAmountRate() {
		return amountRate;
	}

	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}

	public String getAnnotationIsinCode() {
		return annotationIsinCode;
	}

	public void setAnnotationIsinCode(String annotationIsinCode) {
		this.annotationIsinCode = annotationIsinCode;
	}

	public String getSecurityCurrencyDesc() {
		return securityCurrencyDesc;
	}

	public void setSecurityCurrencyDesc(String securityCurrencyDesc) {
		this.securityCurrencyDesc = securityCurrencyDesc;
	}

	public String getSecurityInterestTypeDesc() {
		return securityInterestTypeDesc;
	}

	public void setSecurityInterestTypeDesc(String securityInterestTypeDesc) {
		this.securityInterestTypeDesc = securityInterestTypeDesc;
	}

	public String getPeriodicityDesc() {
		return periodicityDesc;
	}

	public void setPeriodicityDesc(String periodicityDesc) {
		this.periodicityDesc = periodicityDesc;
	}

	public String getIndElectronicCuponDesc() {
		return indElectronicCuponDesc;
	}

	public void setIndElectronicCuponDesc(String indElectronicCuponDesc) {
		this.indElectronicCuponDesc = indElectronicCuponDesc;
	}

	public Integer getIndApprove() {
		return indApprove;
	}

	public void setIndApprove(Integer indApprove) {
		this.indApprove = indApprove;
	}
	

	public Integer getCertificateFrom() {
		return certificateFrom;
	}

	public void setCertificateFrom(Integer certificateFrom) {
		this.certificateFrom = certificateFrom;
	}

	public Integer getCertificateTo() {
		return certificateTo;
	}

	public void setCertificateTo(Integer certificateTo) {
		this.certificateTo = certificateTo;
	}

	public String getIndApproveDesc() {
		if(indApprove!= null && indApprove == 1) {
			indApproveDesc = "SI";
		}else {
			indApproveDesc =  "NO";
		}
		return indApproveDesc;
	}

	public Integer getIndTaxExoneration() {
		return indTaxExoneration;
	}

	public void setIndTaxExoneration(Integer indTaxExoneration) {
		this.indTaxExoneration = indTaxExoneration;
	}

	public String getIndTaxExonerationDesc() {
		return indTaxExonerationDesc;
	}

	public void setIndTaxExonerationDesc(String indTaxExonerationDesc) {
		this.indTaxExonerationDesc = indTaxExonerationDesc;
	}

	public Integer getIndGuarda() {
		return indGuarda;
	}

	public void setIndGuarda(Integer indGuarda) {
		this.indGuarda = indGuarda;
	}

	public String getIndGuardaDesc() {
		return indGuardaDesc;
	}

	public void setIndGuardaDesc(String indGuardaDesc) {
		this.indGuardaDesc = indGuardaDesc;
	}

	public String getReferencePartCode() {
		return referencePartCode;
	}

	public void setReferencePartCode(String referencePartCode) {
		this.referencePartCode = referencePartCode;
	}

	public List<SecuritySerialRange> getLstSecuritySerialRange() {
		return lstSecuritySerialRange;
	}

	public void setLstSecuritySerialRange(List<SecuritySerialRange> lstSecuritySerialRange) {
		this.lstSecuritySerialRange = lstSecuritySerialRange;
	}
	
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}

	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}

	public HolderAccount getHolderAccountBenef() {
		return holderAccountBenef;
	}

	public void setHolderAccountBenef(HolderAccount holderAccountBenef) {
		this.holderAccountBenef = holderAccountBenef;
	}

	public void setIndApproveDesc(String indApproveDesc) {
		this.indApproveDesc = indApproveDesc;
	}

	public HolderAccount getHolderAccountDebtor() {
		return holderAccountDebtor;
	}

	public void setHolderAccountDebtor(HolderAccount holderAccountDebtor) {
		this.holderAccountDebtor = holderAccountDebtor;
	}

	public Integer getIndExpirationCupons() {
		return indExpirationCupons;
	}

	public void setIndExpirationCupons(Integer indExpirationCupons) {
		this.indExpirationCupons = indExpirationCupons;
	}

	@Override
	public String toString() {
		return "AccountAnnotationOperation [idAnnotationOperationPk=" + idAnnotationOperationPk 
				+ ", certificateNumber=" + certificateNumber + ", expeditionDate=" + expeditionDate
				+ ", motive=" + motive + ", serialNumber=" + serialNumber + ", state=" + state
				+ ", totalBalance=" + totalBalance + ", certificateFrom=" + certificateFrom 
				+ ", certificateTo="+ certificateTo + ", registryDate=" + registryDate
				+ ", beginingDate=" + beginingDate + ", paymentDate=" + paymentDate 
				+ ", cashPrice=" + cashPrice + ", amountRate=" + amountRate
				+ ", indApprove=" + indApprove + ", indGuarda=" + indGuarda 
				+ "]";
	}
	
}