package com.pradera.model.custody.changeownership;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the CHANGE_OWNERSHIP_FILE database table.
 * 
 */
@Entity
@Table(name="CHANGE_OWNERSHIP_FILE")
public class ChangeOwnershipFile implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CHANGE_OWNERSHIP_FILE_GENERATOR", sequenceName="SQ_ID_CHANGE_OWNERSHIP_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CHANGE_OWNERSHIP_FILE_GENERATOR")
	@Column(name="ID_CHANGE_OWNERSHIP_FILE_PK")
	private Long idChangeOwnershipFilePk;

	@Column(name="FILE_NAME")
	private String fileName;

	@Column(name="FILE_TYPE")
	private Integer fileType;

    @Lob()
    @Column(name="DOCUMENT_FILE")
    @Basic(fetch=FetchType.LAZY)
	private byte[] documentFile;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to ChangeOwnershipOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANGE_OWNERSHIP_FK",referencedColumnName="ID_CHANGE_OWNERSHIP_PK")
	private ChangeOwnershipOperation changeOwnershipOperation;

    @Transient
    private String fileTypeDescription;
    @Transient
    private String fileFormat;
    @Transient
    private Long fileSize;
    
    public ChangeOwnershipFile() {
    
    }
    
    
    public ChangeOwnershipFile(Long idChangeOwnershipFilePk, String fileName,
			Integer fileType, String fileTypeDescription) {
		this.idChangeOwnershipFilePk = idChangeOwnershipFilePk;
		this.fileName = fileName;
		this.fileType = fileType;
		this.fileTypeDescription = fileTypeDescription;
	}
    
	public ChangeOwnershipFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	public Long getIdChangeOwnershipFilePk() {
		return this.idChangeOwnershipFilePk;
	}

	public void setIdChangeOwnershipFilePk(Long idChangeOwnershipFilePk) {
		this.idChangeOwnershipFilePk = idChangeOwnershipFilePk;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getFileType() {
		return this.fileType;
	}

	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}

	public byte[] getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public ChangeOwnershipOperation getChangeOwnershipOperation() {
		return this.changeOwnershipOperation;
	}

	public void setChangeOwnershipOperation(ChangeOwnershipOperation changeOwnershipOperation) {
		this.changeOwnershipOperation = changeOwnershipOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
	    if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}


	public void setFileTypeDescription(String fileTypeDescription) {
		this.fileTypeDescription = fileTypeDescription;
	}


	public String getFileTypeDescription() {
		return fileTypeDescription;
	}


	public Long getFileSize() {
		return fileSize;
	}


	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}


	public String getFileFormat() {
		return fileFormat;
	}


	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
	
}