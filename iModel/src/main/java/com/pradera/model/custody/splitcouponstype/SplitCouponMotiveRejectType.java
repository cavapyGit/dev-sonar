package com.pradera.model.custody.splitcouponstype;

public enum SplitCouponMotiveRejectType {
		
	OTHERS_MOTIVES(new Integer(424)),
	TO_REQUEST_PARTICIPANT(new Integer(423)),
	REQUEST_WITH_WRONG_DATA(new Integer(422));
	
	private Integer code;
	
	private SplitCouponMotiveRejectType(Integer code){
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
