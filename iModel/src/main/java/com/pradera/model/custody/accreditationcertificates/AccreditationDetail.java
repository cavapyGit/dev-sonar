package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;





/**
 * The persistent class for the ACCREDITATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="ACCREDITATION_DETAIL")
public class AccreditationDetail implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCREDITATION_DETAIL_IDACCREDITATIONDETAILPK_GENERATOR", sequenceName="SQ_ID_ACCREDITATION_DETAIL", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCREDITATION_DETAIL_IDACCREDITATIONDETAILPK_GENERATOR")
	@Column(name="ID_ACCREDITATION_DETAIL_PK")
	private Long idAccreditationDetailPk;

	@Column(name="AVAILABLE_BALANCE")
	private BigDecimal availableBalance;

	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")	
	private Participant idParticipantFk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OTHER_BALANCE")
	private BigDecimal otherBalance;

	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance;

	@Column(name="REPORTING_BALANCE")
	private BigDecimal reportingBalance;
	
	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;

	@Column(name="ROLE")
	private Integer role;

	//bi-directional many-to-one association to AccreditationOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCREDITATION_OPERATION_FK")
	private AccreditationOperation accreditationOperation;

	//bi-directional many-to-one association to BlockOperationCertification
	@OneToMany(fetch=FetchType.LAZY, mappedBy="accreditationDetail", cascade=CascadeType.ALL)
	private List<BlockOperationCertification> blockOperationCertifications;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="accreditationDetailFk", cascade=CascadeType.ALL)
	private List<AccreditationMarketFact> accreditationMarketFactList;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="accreditationDetailFk", cascade=CascadeType.ALL)
	private List<ReportAccreditation> accreditationReported;

    public AccreditationDetail() {
    }

	public Long getIdAccreditationDetailPk() {
		return this.idAccreditationDetailPk;
	}

	public void setIdAccreditationDetailPk(Long idAccreditationDetailPk) {
		this.idAccreditationDetailPk = idAccreditationDetailPk;
	}

	public BigDecimal getAvailableBalance() {
		return this.availableBalance;
	}

	public void setAvailableBalance(BigDecimal Long1) {
		this.availableBalance = Long1;
	}

	public BigDecimal getBanBalance() {
		return this.banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public Participant getIdParticipantFk() {
		return idParticipantFk;
	}

	public void setIdParticipantFk(Participant idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getOtherBalance() {
		return otherBalance;
	}

	public void setOtherBalance(BigDecimal otherBalance) {
		this.otherBalance = otherBalance;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public AccreditationOperation getAccreditationOperation() {
		return this.accreditationOperation;
	}

	public void setAccreditationOperation(AccreditationOperation accreditationOperation) {
		this.accreditationOperation = accreditationOperation;
	}
	
	public List<BlockOperationCertification> getBlockOperationCertifications() {
		return this.blockOperationCertifications;
	}

	public void setBlockOperationCertifications(List<BlockOperationCertification> blockOperationCertifications) {
		this.blockOperationCertifications = blockOperationCertifications;
	}
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();     
            if(Validations.validateListIsNotNullAndNotEmpty(blockOperationCertifications)){
            	for (BlockOperationCertification blockOpeCert : blockOperationCertifications) {
            			blockOpeCert.setAudit(loggerUser);
				}
            }
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("blockOperationCertifications", blockOperationCertifications);
        detailsMap.put("accreditationMarketFactList", accreditationMarketFactList);
        detailsMap.put("accreditationReported", accreditationReported);
        return detailsMap;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public List<AccreditationMarketFact> getAccreditationMarketFactList() {
		return accreditationMarketFactList;
	}

	public void setAccreditationMarketFactList(
			List<AccreditationMarketFact> accreditationMarketFactList) {
		this.accreditationMarketFactList = accreditationMarketFactList;
	}

	public List<ReportAccreditation> getAccreditationReported() {
		return accreditationReported;
	}

	public void setAccreditationReported(
			List<ReportAccreditation> accreditationReported) {
		this.accreditationReported = accreditationReported;
	}

	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	
	
}