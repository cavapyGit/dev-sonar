package com.pradera.model.custody.securitiesretirement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the RETIREMENT_DETAIL database table.
 * 
 */
@Entity
@Table(name="RETIREMENT_BLOCK_DETAIL")
public class RetirementBlockDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id retirement detail pk. */
	@Id
	@SequenceGenerator(name="SQ_ID_RETIREMENT_BLOCK_DETAIL_PK", sequenceName="SQ_ID_RETIREMENT_BLOCK_DET_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_RETIREMENT_BLOCK_DETAIL_PK")
	@Column(name="ID_RETIREMENT_BLOCK_DET_PK")
	private Long idRetirementBlockDetPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RETIREMENT_DETAIL_FK", referencedColumnName="ID_RETIREMENT_DETAIL_PK")
	private RetirementDetail retirementDetail;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK", referencedColumnName="ID_BLOCK_OPERATION_PK")
	private BlockOperation blockOperation;
	
	@Column(name="BLOCK_QUANTITY")
	private BigDecimal blockQuantity;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The retirement marketfacts. */
    @OneToMany(mappedBy="retirementBlockDetail",fetch=FetchType.LAZY)
    private List<RetirementMarketfact> retirementMarketfacts;
    
    public Long getIdRetirementBlockDetPk() {
		return idRetirementBlockDetPk;
	}

	public void setIdRetirementBlockDetPk(Long idRetirementBlockDetPk) {
		this.idRetirementBlockDetPk = idRetirementBlockDetPk;
	}

	public RetirementDetail getRetirementDetail() {
		return retirementDetail;
	}

	public void setRetirementDetail(RetirementDetail retirementDetail) {
		this.retirementDetail = retirementDetail;
	}

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}
    
    /**
     * Instantiates a new retirement detail.
     */
    public RetirementBlockDetail() {
    }
 
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/**
	 * Gets the retirement marketfacts.
	 *
	 * @return the retirement marketfacts
	 */
	public List<RetirementMarketfact> getRetirementMarketfacts() {
		return retirementMarketfacts;
	}

	/**
	 * Sets the retirement marketfacts.
	 *
	 * @param retirementMarketfacts the new retirement marketfacts
	 */
	public void setRetirementMarketfacts(
			List<RetirementMarketfact> retirementMarketfacts) {
		this.retirementMarketfacts = retirementMarketfacts;
	}
	
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub

		if (loggerUser != null) {
            this.lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            this.lastModifyDate = loggerUser.getAuditTime();
            this.lastModifyIp = loggerUser.getIpAddress();
            this.lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
        HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("retirementMarketfacts", retirementMarketfacts);
        return detailsMap;
	}

	public BigDecimal getBlockQuantity() {
		return blockQuantity;
	}

	public void setBlockQuantity(BigDecimal blockQuantity) {
		this.blockQuantity = blockQuantity;
	}

}