package com.pradera.model.custody.type;


import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum DematerializationCertificateMotiveType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public enum DematerializationCertificateMotiveType {
	
	
	/** The dematerialization. */
	DEMATERIALIZATION(new Integer(812),"INGRESO POR INMOVILIZACION"),
	GUARDA_MANAGED(new Integer(2840),"INGRESO POR GUARDA ADMINISTRADA"),
	GUARDA_EXCLUSIVE(new Integer(2841),"INGRESO POR GUARDA EXCLUSIVA"),
	DEMATERIALIZATION_OTC(new Integer(2165),"INGRESO POR OTC"),
	

	/** The suscription. */
	SUSCRIPTION(new Integer(220),"INGRESO POR SUSCRIPCION PREFERENTE"),
	
	DONATION(new Integer(813),"INGRESO POR DONACION"),
	
	DEMATERIALIZATION_PHISYCAL(new Integer(2357),"DESMATERIALIZACION"),
	
	BANK_SHARES_PRIMARY_COLOCATION_PURCHASE(new Integer(2032),"INGRESO POR COMPRA DE ACCIONES BANCARIAS EN COLOCACION PRIMARIA"),
	
	DIRECT_PURCHASE_BCB(new Integer(2162),"INGRESO POR VENTA DIRECTA BIC"),
	
	DIRECT_PURCHASE_TGN(new Integer(2163),"INGRESO POR TESORO DIRECTO TGN"),
	
	AUCTION_PURCHASE(new Integer(2164),"INGRESO POR COMPRA EN SUBASTA"),
	
	MONEY_PURCHASE(new Integer(2165),"INGRESO POR COMPRA EN MESA DE DINERO"),
	
	DIRECT_PURCHASE_PRIVATE(new Integer(2166),"INGRESO POR ADQUISICION DIRECTA PRIVADA"),
	
	INCOME_PURCHASE_PRIVATE_STOCK(new Integer(2559),"INGRESO POR COMPRA DE ACCIONES PRIVADAS"),
	
	INCOME_PURCHASE_PRIMARY(new Integer(2908),"INGRESO POR COMPRA EN PRIMARIO"),
	
	;
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<DematerializationCertificateMotiveType> list = new ArrayList<DematerializationCertificateMotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, DematerializationCertificateMotiveType> lookup = new HashMap<Integer, DematerializationCertificateMotiveType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<DematerializationCertificateMotiveType> listSomeElements(DematerializationCertificateMotiveType... transferSecuritiesTypeParams){
		List<DematerializationCertificateMotiveType> retorno = new ArrayList<DematerializationCertificateMotiveType>();
		for(DematerializationCertificateMotiveType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (DematerializationCertificateMotiveType s : EnumSet.allOf(DematerializationCertificateMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new dematerialization certificate motive type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private DematerializationCertificateMotiveType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the dematerialization certificate motive type
	 */
	public static DematerializationCertificateMotiveType get(Integer codigo) {
		return lookup.get(codigo);
	}
}