package com.pradera.model.custody.affectation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AffectationMotiveRejectType {
	MOTIVE_1(new Integer(1811)),
	OTHER(new Integer(1812));
	
	private Integer code;
	
	private AffectationMotiveRejectType(Integer code){
		this.code = code;
	}
		
	/** The Constant list. */
	public static final List<AffectationMotiveRejectType> list = new ArrayList<AffectationMotiveRejectType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AffectationMotiveRejectType> lookup = new HashMap<Integer, AffectationMotiveRejectType>();
	
	static {
		for (AffectationMotiveRejectType s : EnumSet.allOf(AffectationMotiveRejectType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static AffectationMotiveRejectType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}
