package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum ReasonRetirementType {
	
	ERROR_EN_REGISTRO_DE_SOLICITUD(new Integer(724),"deposito.reason.err.regstration.application.wrong"),
	CUENTA_TITULAR_INCORRECTA(new Integer(725),"deposito.reason.err.holder.account.wrong"),	
	OTRO(new Integer(726),"deposito.reason.others");
	
	
	private Integer code;
	private String value;

	public static final List<ReasonRetirementType> list = new ArrayList<ReasonRetirementType>();
	public static final Map<Integer, ReasonRetirementType> lookup = new HashMap<Integer, ReasonRetirementType>();

	static {
		for (ReasonRetirementType s : EnumSet.allOf(ReasonRetirementType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	private ReasonRetirementType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getValue() {
		return value;
	}
	
	
	
	public static ReasonRetirementType get(Integer code) {
		return lookup.get(code);
	}

}
