package com.pradera.model.custody.splitcoupons;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Security;

@Entity
@Table(name="SPLIT_COUPON_OPERATION")
public class SplitCouponRequest implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long idSplitOperationPk;
	
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_SPLIT_OPERATION_PK")
	private CustodyOperation custodyOperation;	

	@Column(name="REQUEST_STATE")
	private Integer requestState;
	
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName = "ID_PARTICIPANT_PK")	
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK", referencedColumnName = "ID_HOLDER_PK")	
	private Holder holder;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK", referencedColumnName = "ID_HOLDER_ACCOUNT_PK")	
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK", referencedColumnName = "ID_SECURITY_CODE_PK")	
	private Security security;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SUBPRODUCT_CODE_FK", referencedColumnName = "ID_SECURITY_CODE_PK")	
	private Security securityProduct;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@OneToMany(mappedBy = "splitCouponRequest")
	private List<SplitCouponOperation> splitCouponOperations;
	
	@Transient
	private String requestStateDescription;
	
	@Column(name="FILE_NAME")
	private String fileName;
	
	@Lob()
	@Column(name="DOCUMENT_FILE")
	private byte[] documentFile;
	
	@Column(name="MOTIVE_ANNUL_REJECT")
	private Integer motiveAnnulReject;
	
	@Column(name="COMMENTS")
	private String comments;
	
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public Integer getRequestState() {
		return requestState;
	}

	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public String getRequestStateDescription() {
		return requestStateDescription;
	}

	public void setRequestStateDescription(String requestStateDescription) {
		this.requestStateDescription = requestStateDescription;
	}

	public Long getIdSplitOperationPk() {
		return idSplitOperationPk;
	}

	public void setIdSplitOperationPk(Long idSplitOperationPk) {
		this.idSplitOperationPk = idSplitOperationPk;
	}

	public List<SplitCouponOperation> getSplitCouponOperations() {
		return splitCouponOperations;
	}

	public void setSplitCouponOperations(
			List<SplitCouponOperation> splitCouponOperations) {
		this.splitCouponOperations = splitCouponOperations;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (Validations.validateIsNotNullAndNotEmpty(loggerUser)) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(Validations.validateIsNotNullAndNotEmpty(custodyOperation))
            	custodyOperation.setAudit(loggerUser);
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	public Integer getMotiveAnnulReject() {
		return motiveAnnulReject;
	}

	public void setMotiveAnnulReject(Integer motiveAnnulReject) {
		this.motiveAnnulReject = motiveAnnulReject;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Security getSecurityProduct() {
		return securityProduct;
	}

	public void setSecurityProduct(Security securityProduct) {
		this.securityProduct = securityProduct;
	}
}
