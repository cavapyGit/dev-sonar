package com.pradera.model.custody.changeownership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.issuancesecuritie.Security;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChangeOwnershipDetail.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Entity
@Table(name="CHANGE_OWNERSHIP_DETAIL")
public class ChangeOwnershipDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id change ownership detail pk. */
	@Id
	@SequenceGenerator(name="CHANGE_OWNERSHIP_DETAIL_GENERATOR", sequenceName="SQ_ID_CHANGE_OWNERSHIP_DET_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CHANGE_OWNERSHIP_DETAIL_GENERATOR")
	@Column(name="ID_CHANGE_OWNERSHIP_DETAIL_PK")
	private Long idChangeOwnershipDetailPk;

	/** The available balance. */
	@Column(name="AVAILABLE_BALANCE")
	private BigDecimal availableBalance;

	/** The ban balance. */
	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance;
	
	/** The other block balance. */
	@Column(name="OTHER_BLOCK_BALANCE")
	private BigDecimal otherBlockBalance;

	/** The pawn balance. */
	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance;
	
	/** The reserve balance. */
	@Column(name="RESERVE_BALANCE")
	private BigDecimal reserveBalance;
	
	/** The opposition balance. */
	@Column(name="OPPOSITION_BALANCE")
	private BigDecimal oppositionBalance;

	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK",referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccount;

	/** The security. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",referencedColumnName="ID_SECURITY_CODE_PK")
	private Security security;

	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;

	/** The ind origin target. */
	@Column(name="IND_ORIGIN_TARGET")
	private Long indOriginTarget;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The block change ownerships. */
	@OneToMany(mappedBy="changeOwnershipDetail",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<BlockChangeOwnership> blockChangeOwnerships;

	/** The change ownership operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANGE_OWNERSHIP_FK",referencedColumnName="ID_CHANGE_OWNERSHIP_PK")
	private ChangeOwnershipOperation changeOwnershipOperation;
    
	/** The target change ownership details. */
	@OneToMany(mappedBy="sourceChangeOwnershipDetail",cascade=CascadeType.PERSIST)
	private List<ChangeOwnershipDetail> targetChangeOwnershipDetails;
	
	/** The change ownership market facts. */
	@OneToMany(mappedBy="changeOwnershipDetail",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<ChangeOwnershipMarketFact> changeOwnershipMarketFacts;
	
	/** The source change ownership detail. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANGE_OWNERSHIP_DETAIL_FK",referencedColumnName="ID_CHANGE_OWNERSHIP_DETAIL_PK")
	private ChangeOwnershipDetail sourceChangeOwnershipDetail;
	
	/** The selected. */
	@Transient
	private boolean selected;
		
	/**
	 * Instantiates a new change ownership detail.
	 */
	public ChangeOwnershipDetail() {
		this.availableBalance = BigDecimal.ZERO;
		this.banBalance = BigDecimal.ZERO;
		this.otherBlockBalance = BigDecimal.ZERO;
		this.reserveBalance = BigDecimal.ZERO;
		this.oppositionBalance = BigDecimal.ZERO;
		this.pawnBalance = BigDecimal.ZERO;
    }
	
	/**
	 * Gets the id change ownership detail pk.
	 *
	 * @return the id change ownership detail pk
	 */
	public Long getIdChangeOwnershipDetailPk() {
		return idChangeOwnershipDetailPk;
	}

	/**
	 * Sets the id change ownership detail pk.
	 *
	 * @param idChangeOwnershipDetailPk the new id change ownership detail pk
	 */
	public void setIdChangeOwnershipDetailPk(Long idChangeOwnershipDetailPk) {
		this.idChangeOwnershipDetailPk = idChangeOwnershipDetailPk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the ind origin target.
	 *
	 * @return the ind origin target
	 */
	public Long getIndOriginTarget() {
		return indOriginTarget;
	}

	/**
	 * Sets the ind origin target.
	 *
	 * @param indOriginTarget the new ind origin target
	 */
	public void setIndOriginTarget(Long indOriginTarget) {
		this.indOriginTarget = indOriginTarget;
	}


	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * Gets the ban balance.
	 *
	 * @return the ban balance
	 */
	public BigDecimal getBanBalance() {
		return banBalance;
	}

	/**
	 * Sets the ban balance.
	 *
	 * @param banBalance the new ban balance
	 */
	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	/**
	 * Gets the other block balance.
	 *
	 * @return the other block balance
	 */
	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	/**
	 * Sets the other block balance.
	 *
	 * @param otherBlockBalance the new other block balance
	 */
	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	/**
	 * Gets the pawn balance.
	 *
	 * @return the pawn balance
	 */
	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	/**
	 * Sets the pawn balance.
	 *
	 * @param pawnBalance the new pawn balance
	 */
	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	/**
	 * Gets the block change ownerships.
	 *
	 * @return the block change ownerships
	 */
	public List<BlockChangeOwnership> getBlockChangeOwnerships() {
		return this.blockChangeOwnerships;
	}

	/**
	 * Sets the block change ownerships.
	 *
	 * @param blockChangeOwnerships the new block change ownerships
	 */
	public void setBlockChangeOwnerships(List<BlockChangeOwnership> blockChangeOwnerships) {
		this.blockChangeOwnerships = blockChangeOwnerships;
	}
	
	/**
	 * Gets the change ownership operation.
	 *
	 * @return the change ownership operation
	 */
	public ChangeOwnershipOperation getChangeOwnershipOperation() {
		return this.changeOwnershipOperation;
	}

	/**
	 * Sets the change ownership operation.
	 *
	 * @param changeOwnershipOperation the new change ownership operation
	 */
	public void setChangeOwnershipOperation(ChangeOwnershipOperation changeOwnershipOperation) {
		this.changeOwnershipOperation = changeOwnershipOperation;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	

	/**
	 * Gets the target change ownership details.
	 *
	 * @return the target change ownership details
	 */
	public List<ChangeOwnershipDetail> getTargetChangeOwnershipDetails() {
		return targetChangeOwnershipDetails;
	}

	/**
	 * Sets the target change ownership details.
	 *
	 * @param targetChangeOwnershipDetails the new target change ownership details
	 */
	public void setTargetChangeOwnershipDetails(
			List<ChangeOwnershipDetail> targetChangeOwnershipDetails) {
		this.targetChangeOwnershipDetails = targetChangeOwnershipDetails;
	}

	/**
	 * Gets the source change ownership detail.
	 *
	 * @return the source change ownership detail
	 */
	public ChangeOwnershipDetail getSourceChangeOwnershipDetail() {
		return sourceChangeOwnershipDetail;
	}

	/**
	 * Sets the source change ownership detail.
	 *
	 * @param sourceChangeOwnershipDetail the new source change ownership detail
	 */
	public void setSourceChangeOwnershipDetail(
			ChangeOwnershipDetail sourceChangeOwnershipDetail) {
		this.sourceChangeOwnershipDetail = sourceChangeOwnershipDetail;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((holderAccount == null) ? 0 : holderAccount.hashCode());
		result = prime
				* result
				+ (int) (idChangeOwnershipDetailPk ^ (idChangeOwnershipDetailPk >>> 32));
		result = prime * result
				+ ((participant == null) ? 0 : participant.hashCode());
		result = prime * result
				+ ((security == null) ? 0 : security.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeOwnershipDetail other = (ChangeOwnershipDetail) obj;
		if (holderAccount == null) {
			if (other.holderAccount != null)
				return false;
		} else if (!holderAccount.equals(other.holderAccount))
			return false;
//		if (!idChangeOwnershipDetailPk.equals(other.idChangeOwnershipDetailPk))
//			return false;
		if (participant == null) {
			if (other.participant != null)
				return false;
		} else if (!participant.equals(other.participant))
			return false;
		if (security == null) {
			if (other.security != null)
				return false;
		} else if (!security.equals(other.security))
			return false;
		return true;
	}

//	public List<ChangeOwnershipDetail> getAsList(){
//		List<ChangeOwnershipDetail> result = new ArrayList<ChangeOwnershipDetail>();
//		result.add(this);
//		return result;
//	}
//	
	/**
 * Gets the targets as list.
 *
 * @return the targets as list
 */
public List<ChangeOwnershipDetail> getTargetsAsList(){
		if(targetChangeOwnershipDetails!=null &&  Hibernate.isInitialized(targetChangeOwnershipDetails)){
			List<ChangeOwnershipDetail> result = new ArrayList<ChangeOwnershipDetail>();
			result.addAll(targetChangeOwnershipDetails);
			return result;
		}else{
			return null;
		}
	}
	
	/**
	 * Gets the total avail balance from targets.
	 *
	 * @return the total avail balance from targets
	 */
	public BigDecimal getTotalAvailBalanceFromTargets(){
		BigDecimal targetTotal = new BigDecimal(0);
		if(targetChangeOwnershipDetails!=null){
			for(ChangeOwnershipDetail tgtDetail : targetChangeOwnershipDetails){
				targetTotal = targetTotal.add(
						tgtDetail.getAvailableBalance()==null?new BigDecimal(0):tgtDetail.getAvailableBalance());
			}
		}
		return targetTotal;
	}
	
	/**
	 * Gets the total block balance.
	 *
	 * @return the total block balance
	 */
	public BigDecimal getTotalBlockBalance(){
		BigDecimal srcBlockTotal = new BigDecimal(0);
		srcBlockTotal=srcBlockTotal.add(getBanBalance());
		srcBlockTotal=srcBlockTotal.add(getOppositionBalance());
		srcBlockTotal=srcBlockTotal.add(getOtherBlockBalance());
		srcBlockTotal=srcBlockTotal.add(getPawnBalance());
		srcBlockTotal=srcBlockTotal.add(getReserveBalance());
		return srcBlockTotal;
	}
	
	/**
	 * Gets the total block balance from targets.
	 *
	 * @return the total block balance from targets
	 */
	public BigDecimal getTotalBlockBalanceFromTargets(){
		BigDecimal targetTotal = new BigDecimal(0);
		if(blockChangeOwnerships!=null){
			for(BlockChangeOwnership blockDetail : blockChangeOwnerships){
				targetTotal = targetTotal.add(
						blockDetail.getBlockQuantity()==null?new BigDecimal(0):blockDetail.getBlockQuantity());
			}
		}
		return targetTotal;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
	    if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap =
                new HashMap<String, List<? extends Auditable>>();
      detailsMap.put("targetChangeOwnershipDetails", targetChangeOwnershipDetails);
      detailsMap.put("changeOwnershipMarketFacts", changeOwnershipMarketFacts);
      detailsMap.put("blockChangeOwnerships", blockChangeOwnerships);
      return detailsMap;
	}

	/**
	 * Group block details by affec and request.
	 *
	 * @return the map
	 */
	public Map<Integer, Map<BlockRequest, List<BlockChangeOwnership>>> groupBlockDetailsByAffecAndRequest(){
		
		Map<Integer, Map<BlockRequest, List<BlockChangeOwnership>>> mapByAffec = 
				new HashMap<Integer, Map<BlockRequest,List<BlockChangeOwnership>>>();
		
		for(BlockChangeOwnership blockDetail : blockChangeOwnerships){
			
			Map<BlockRequest, List<BlockChangeOwnership>> mapByRequestPk = 
					mapByAffec.get(blockDetail.getBlockOperation().getBlockRequest().getBlockType());
			
			if (mapByRequestPk == null){
				mapByRequestPk = new HashMap<BlockRequest, List<BlockChangeOwnership>>();
			}
			
			List<BlockChangeOwnership> currentBlockList  = mapByRequestPk.get(
					blockDetail.getBlockOperation().getBlockRequest());
			
			if(currentBlockList == null){
				currentBlockList = new ArrayList<BlockChangeOwnership>();
			}
			currentBlockList.add(blockDetail);
			mapByRequestPk.put(blockDetail.getBlockOperation().getBlockRequest(), currentBlockList);
			mapByAffec.put(blockDetail.getBlockOperation().getBlockRequest().getBlockType(), mapByRequestPk);
		}
		
		return mapByAffec;
	}
	
	/**
	 * Group block details by affec.
	 *
	 * @return the map
	 */
	public Map<Integer, List<BlockChangeOwnership>> groupBlockDetailsByAffec(){
		Map<Integer, List<BlockChangeOwnership>> blockDetailMap = new HashMap<Integer, List<BlockChangeOwnership>>();

		for(BlockChangeOwnership blockDetail : blockChangeOwnerships){
			List<BlockChangeOwnership> currentBlockList = blockDetailMap.get(blockDetail.getBlockOperation().getBlockRequest().getBlockType());
			if(currentBlockList==null){
				currentBlockList = new ArrayList<BlockChangeOwnership>();
			} 
			currentBlockList.add(blockDetail);
			blockDetailMap.put(
					blockDetail.getBlockOperation().getBlockRequest().getBlockType(),currentBlockList);
		}
		
		return blockDetailMap;
	}

	/**
	 * Gets the reserve balance.
	 *
	 * @return the reserve balance
	 */
	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}

	/**
	 * Sets the reserve balance.
	 *
	 * @param reserveBalance the new reserve balance
	 */
	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	/**
	 * Gets the opposition balance.
	 *
	 * @return the opposition balance
	 */
	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	/**
	 * Sets the opposition balance.
	 *
	 * @param oppositionBalance the new opposition balance
	 */
	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}
	
	/**
	 * Contains block change ownership.
	 *
	 * @param blockChangeOwnership the block change ownership
	 * @return the block change ownership
	 */
	public BlockChangeOwnership containsBlockChangeOwnership(
			BlockChangeOwnership blockChangeOwnership) {
		if(blockChangeOwnerships!=null){
			for(BlockChangeOwnership blockDetail : blockChangeOwnerships){
				if(blockDetail.getBlockOperation().getIdBlockOperationPk().equals(blockChangeOwnership.getBlockOperation().getIdBlockOperationPk()) &&
								blockDetail.getHolderAccount().getIdHolderAccountPk().equals(blockChangeOwnership.getHolderAccount().getIdHolderAccountPk())){
					return blockDetail;
				}
			}
		}
		
		return null;
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * Gets the change ownership market facts.
	 *
	 * @return the change ownership market facts
	 */
	public List<ChangeOwnershipMarketFact> getChangeOwnershipMarketFacts() {
		return changeOwnershipMarketFacts;
	}

	/**
	 * Sets the change ownership market facts.
	 *
	 * @param changeOwnershipMarketFacts the new change ownership market facts
	 */
	public void setChangeOwnershipMarketFacts(
			List<ChangeOwnershipMarketFact> changeOwnershipMarketFacts) {
		this.changeOwnershipMarketFacts = changeOwnershipMarketFacts;
	}

	/**
	 * Contains market fact.
	 *
	 * @param marketFact the market fact
	 * @return the change ownership market fact
	 */
	public ChangeOwnershipMarketFact containsMarketFact(
			ChangeOwnershipMarketFact marketFact) {
		if(changeOwnershipMarketFacts!=null){
			if(changeOwnershipMarketFacts.contains(marketFact)){
				int idx = changeOwnershipMarketFacts.indexOf(marketFact);
				return changeOwnershipMarketFacts.get(idx);
			}
		}
		return null;
	} 
	

}