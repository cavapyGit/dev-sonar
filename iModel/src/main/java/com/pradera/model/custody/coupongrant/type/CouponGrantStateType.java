/**@author nmolina
 * 
 */

package com.pradera.model.custody.coupongrant.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CouponGrantStateType {
		
	REGISTERED(new Integer(1709),"REGISTRADO"),
	REJECTED(new Integer(1710),"RECHAZADO"),
	CONFIRMED(new Integer(1711),"CONFIRMADO"),
	ANNULED(new Integer(1731),"ANULADO");
	
	private Integer code;
	private String value;
	
	public static final List<CouponGrantStateType> list = new ArrayList<CouponGrantStateType>();
	
	public static final Map<Integer, CouponGrantStateType> lookup = new HashMap<Integer, CouponGrantStateType>();
	
	public static List<CouponGrantStateType> listSomeElements(CouponGrantStateType... transferSecuritiesTypeParams){
		List<CouponGrantStateType> retorno = new ArrayList<CouponGrantStateType>();
		for(CouponGrantStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (CouponGrantStateType s : EnumSet.allOf(CouponGrantStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private CouponGrantStateType(Integer ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}