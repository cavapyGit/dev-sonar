package com.pradera.model.custody.physical;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.transfersecurities.CustodyOperation;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the PHYSICAL_CERTIFICATE_OPERATION database table.
 * 
 */
@Entity
@Table(name="PHYSICAL_CERTIFICATE_OPERATION")
public class PhysicalCertificateOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long idPhysicalOperationPk;
	
	@MapsId
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_OPERATION_PK")
	private CustodyOperation custodyOperation;

	@ManyToOne
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@ManyToOne
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="PHYSICAL_OPERATION_TYPE")
	private Long physicalOperationType;
	
	@Column(name="IND_APPLIED")
	private Integer indApplied;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REQUEST_DELIVERY_DATE")
	private Date requestDeliveryDate;

	@Column(name="\"STATE\"")
	private Integer state;

	//bi-directional many-to-one association to PhysicalCertificateMovement
	@OneToMany(mappedBy="physicalCertificateOperation")
	private List<PhysicalCertificateMovement> physicalCertificateMovements;

	//bi-directional many-to-one association to PhysicalOperationDetail
	@OneToMany(mappedBy="physicalCertificateOperation",cascade=CascadeType.ALL)
	private List<PhysicalOperationDetail> physicalOperationDetails;
	
	@Column(name="COMMENTS")
	private String comments; 
	
	// issue 895: columnas adicionas a requerimiento de issue
	@Column(name="NAME_PICKUP_MANAGER")
	private String namePickupManager;
	
	@Column(name="CI_PICKUP_MANAGER")
	private String ciPickupManager;
	
	public Long getIdPhysicalOperationPk() {
		return idPhysicalOperationPk;
	}

	public void setIdPhysicalOperationPk(Long idPhysicalOperationPk) {
		this.idPhysicalOperationPk = idPhysicalOperationPk;
	}

	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public String getNamePickupManager() {
		return namePickupManager;
	}

	public void setNamePickupManager(String namePickupManager) {
		this.namePickupManager = namePickupManager;
	}

	public String getCiPickupManager() {
		return ciPickupManager;
	}

	public void setCiPickupManager(String ciPickupManager) {
		this.ciPickupManager = ciPickupManager;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Long getPhysicalOperationType() {
		return physicalOperationType;
	}

	public void setPhysicalOperationType(Long physicalOperationType) {
		this.physicalOperationType = physicalOperationType;
	}

	public Integer getIndApplied() {
		return indApplied;
	}

	public void setIndApplied(Integer indApplied) {
		this.indApplied = indApplied;
	}

	public Date getRequestDeliveryDate() {
		return requestDeliveryDate;
	}

	public void setRequestDeliveryDate(Date requestDeliveryDate) {
		this.requestDeliveryDate = requestDeliveryDate;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public List<PhysicalCertificateMovement> getPhysicalCertificateMovements() {
		return physicalCertificateMovements;
	}

	public void setPhysicalCertificateMovements(
			List<PhysicalCertificateMovement> physicalCertificateMovements) {
		this.physicalCertificateMovements = physicalCertificateMovements;
	}

	public List<PhysicalOperationDetail> getPhysicalOperationDetails() {
		return physicalOperationDetails;
	}

	public void setPhysicalOperationDetails(
			List<PhysicalOperationDetail> physicalOperationDetails) {
		this.physicalOperationDetails = physicalOperationDetails;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            //custodyOperation.setAudit(loggerUser);
            
            if(custodyOperation != null){
    			custodyOperation.setLastModifyApp(lastModifyApp);
    			custodyOperation.setLastModifyDate(lastModifyDate);
    			custodyOperation.setLastModifyIp(lastModifyIp);
    			custodyOperation.setLastModifyUser(lastModifyUser);
            }
            
            if(physicalOperationDetails != null && physicalOperationDetails.size()>0){
            	
            	for (PhysicalOperationDetail physicalOperationDetail : physicalOperationDetails) {
            		physicalOperationDetail.setLastModifyApp(lastModifyApp);
            		physicalOperationDetail.setLastModifyDate(lastModifyDate);
            		physicalOperationDetail.setLastModifyIp(lastModifyIp);
            		physicalOperationDetail.setLastModifyUser(lastModifyUser);
            
            		if(physicalOperationDetail.getPhysicalCertificate() != null){
            			physicalOperationDetail.getPhysicalCertificate().setLastModifyApp(lastModifyApp);
            			physicalOperationDetail.getPhysicalCertificate().setLastModifyDate(lastModifyDate);
            			physicalOperationDetail.getPhysicalCertificate().setLastModifyIp(lastModifyIp);
            			physicalOperationDetail.getPhysicalCertificate().setLastModifyUser(lastModifyUser);
            		}
            		
				}
            	
            }
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("physicalCertificateMovements", physicalCertificateMovements);
		detailsMap.put("physicalOperationDetails", physicalOperationDetails);
        return detailsMap;
	}
	
	
	
}