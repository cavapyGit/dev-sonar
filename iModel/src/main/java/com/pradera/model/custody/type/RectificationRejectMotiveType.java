package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RectificationRejectMotiveType {


	WRONG_REQUEST_NUMBER(new Integer(1492),"NUERMO DE SOLICITUD ERRONEA"),
	WRONG_DATA(new Integer(11493),"ERROR EN DATOS"),
	OTHERS(new Integer(1494),"OTROS");

	private String value;
	private Integer code;

	public static final List<RectificationRejectMotiveType> list= new ArrayList<RectificationRejectMotiveType>();
	public static final Map<Integer, RectificationRejectMotiveType> lookup = new HashMap<Integer, RectificationRejectMotiveType >();

	static{
		for(RectificationRejectMotiveType type: EnumSet.allOf(RectificationRejectMotiveType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);
		}


	}


	private RectificationRejectMotiveType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static RectificationRejectMotiveType get(Integer code)
	{
		return lookup.get(code);
	}







}
