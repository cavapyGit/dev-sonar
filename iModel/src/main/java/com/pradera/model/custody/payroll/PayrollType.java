package com.pradera.model.custody.payroll;

public enum PayrollType {
	ACCOUNT_ANNOTATION_OPERATION(new Integer(2710)),//OPERACIONES DE ANOTACION EN CUENTA //2332 old
	REMOVE_SECURITIES_OPERATION(new Integer(2711)),//OPERACIONES DE RETIRO DE VALORES //2333 old
	CORPORATIVE_OPERATION(new Integer(2712)),//OPERACIONES DE PROCESOS CORPORATIVOS //2334 old
	CERTIFICATE_OPERATION(new Integer(2733)),//OPERACIONES DE TITULOS REALES //2364 old
	;
	
	private Integer code;
	
	private PayrollType(Integer code) {
		this.code = code;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}	
}
