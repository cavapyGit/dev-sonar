package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCREDITATION_MARKETFACT database table.
 * 
 */
@Entity
@Table(name="ACCREDITATION_MARKETFACT")
public class AccreditationMarketFact implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SQ_ACCRE_MARKET_FACT_PK", sequenceName = "SQ_ID_ACCRE_MARKET_FACT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ACCRE_MARKET_FACT_PK")
	@Column(name="ID_ACCREDITATION_MARKETFACT_PK")
	private Long idAccreditationMarketFactPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCREDITATION_DETAIL_FK", referencedColumnName="ID_ACCREDITATION_DETAIL_PK")
	private AccreditationDetail accreditationDetailFk;
	
	@Column(name="MARKET_QUANTITY")
	private BigDecimal quantityToAccredit;
	
	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="IND_ACTIVE")
	private Integer indActive = ComponentConstant.ONE;
	
	@Transient
	private Long idAccreditationMF;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	public Long getIdAccreditationMarketFactPk() {
		return idAccreditationMarketFactPk;
	}

	public void setIdAccreditationMarketFactPk(Long idAccreditationMarketFactPk) {
		this.idAccreditationMarketFactPk = idAccreditationMarketFactPk;
	}

	public AccreditationDetail getAccreditationDetailFk() {
		return accreditationDetailFk;
	}

	public void setAccreditationDetailFk(AccreditationDetail accreditationDetailFk) {
		this.accreditationDetailFk = accreditationDetailFk;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getQuantityToAccredit() {
		return quantityToAccredit;
	}

	public void setQuantityToAccredit(BigDecimal quantityToAccredit) {
		this.quantityToAccredit = quantityToAccredit;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Long getIdAccreditationMF() {
		return idAccreditationMF;
	}

	public void setIdAccreditationMF(Long idAccreditationMF) {
		this.idAccreditationMF = idAccreditationMF;
	}

	public Integer getIndActive() {
		return indActive;
	}

	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}

}
