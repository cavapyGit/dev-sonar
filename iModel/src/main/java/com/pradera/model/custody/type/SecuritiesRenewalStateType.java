package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SecuritiesRenewalStateType {

	REGISTERED(new Integer(2281),"REGISTRADO"),
	APPROVED(new Integer(2282),"APROBADO"),
	CONFIRMED(new Integer(2283),"CONFIRMADO"),
	REVERSED(new Integer(2284),"REVERTIDO");
	
	private Integer code;
	private String value;
	public static final List<SecuritiesRenewalStateType> list = new ArrayList<SecuritiesRenewalStateType>();
	
	public static final Map<Integer, SecuritiesRenewalStateType> lookup = new HashMap<Integer, SecuritiesRenewalStateType>();
	public static List<SecuritiesRenewalStateType> listSomeElements(SecuritiesRenewalStateType... transferSecuritiesTypeParams){
		List<SecuritiesRenewalStateType> retorno = new ArrayList<SecuritiesRenewalStateType>();
		for(SecuritiesRenewalStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	
	static {
		for (SecuritiesRenewalStateType s : EnumSet.allOf(SecuritiesRenewalStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public static SecuritiesRenewalStateType get(Integer code){
		return lookup.get(code);
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private SecuritiesRenewalStateType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
