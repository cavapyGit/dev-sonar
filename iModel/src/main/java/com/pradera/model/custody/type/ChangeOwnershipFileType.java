/**@author nmolina
 * 
 */

package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.StringType;

public enum ChangeOwnershipFileType {
		
	SUCESION_ACTA_DEFUNCION_TITULAR(new Integer(1179),"ACTA DE DEFUNCION"),
	SUCESION_SENTENCIA_CERTIFICADA_HEREDEROS(new Integer(1180),"SENTENCIA CERTIFICADA"),
	SUCESION_SENTENCIA_PARTICION_MASA_HEREDITARIA(new Integer(1181),"ACUERDO O SENTENCIA DE LA PARTICION"),
	SUCESION_COPIA_CID_HEREDEROS_MAYOR(new Integer(1182),"COPIA DE LAS CEDULAS DE IDENTIDAD"),
	SUCESION_ACTA_NAC_HEREDEROS_MENOR(new Integer(1183),"ACTAS DE NACIMIENTO"),
	SUCESION_PLIEGO_MODIFICACIONES_RECIBO_PAGO(new Integer(1184),"PLIEGO DE MODIFICACIONES Y RECIBO DE PAGO"),
	
	DIVISION_CONYUGAL_ACTA_DIVORCIO(new Integer(1185),"ACTA DE DIVORCIO"),
	DIVISION_CONYUGAL_SENTENCIA_CERTIFICADA(new Integer(1186),"SENTENCIA CERTIFICADA O COMPULSA DEL ACTO NOTARIAL"),
	DIVISION_CONYUGAL_COPIA_CID_CONYUGES(new Integer(1187),"COPIA DEL DOCUMENTO DE IDENTIDAD"),
	
	DIVISION_COPRO_CONVENIO_PARTICION(new Integer(1188),"CONVENIO DE DIVISION Y PARTICION"),
	DIVISION_COPRO_SENTENCIA_CERTIFICADA(new Integer(1189),"SENTENCIA CERTIFICADA"),
	DIVISION_COPRO_COPIA_CID(new Integer(1190),"COPIA DE LAS CEDULAS DE IDENTIDAD"),
	
	DONACION_DOC_ACTA_NOTARIAL(new Integer(1191),"COMPULSA DEL ACTO NOTARIAL"),
	DONACION_RECIBO_PAGO_IMPUESTOS(new Integer(1192),"RECIBO DE PAGO DE IMPUESTOS"),
	DONACION_COPIA_CID_DONANTE_DONATARIO(new Integer(1193),"COPIA DE LOS DOCUMENTOS DE IDENTIDAD"),
	
	FUSION_ACTA_FUSION(new Integer(1194),"ACTA DE LA ASAMBLEA GENERAL EXTRAORDINARIA"),
	FUSION_COPIA_TIT_ENTIDADES(new Integer(1195),"COPIA DE LA TARJETA DE IDENTIFICACION TRIBUTARIA"),
	FUSION_COPIA_CERT_REG_MERCANTIL(new Integer(1196),"COPIA DEL CERTIFICADO DE REGISTRO MERCANTIL"),
	
	DACION_CONVENIO_CEDENTE_CESIONARIO(new Integer(1197),"ORIGINAL DE CONVENIO SUSCRITO LEGALIZADO"),
	DACION_COPIA_CID_CEDENTE_CESIONARIO(new Integer(1198),"COPIA DE LOS DOCUMENTOS DE IDENTIDAD"),
	
	PERMUTA_CONVENIO_TITULARES(new Integer(1199),"ORIGINAL DEL CONVENIO SUSCRITO"),
	PERMUTA_COPIA_CID_CERT_REG_MERCANTIL(new Integer(1200),"COPIA DE LOS DOCUMENTOS DE IDENTIDAD O CERTIFICADO DE REGISTRO MERCANTIL"),
	
	MANDATO_COPIA_CERT_SENTENCIA(new Integer(1201),"COPIA CERTIFICADA DE LA SENTENCIA"),
	MANDATO_COPIA_CID_CERT_REG_MERCANTIL(new Integer(1202),"COPIA DE LOS DOCUMENTOS DE IDENTIDAD O CERTIFICADO DE REGISTRO MERCANTIL"),
	MANDATO_COPIA_CID_PARTES(new Integer(1554),""),
	
	CAMBIO_RAZON_SOCIAL_ESCR_PUBLICA(new Integer(1787),""),
	CAMBIO_RAZON_SOCIAL_RESOLUCION(new Integer(1788),"")
	
	;
	
	private Integer code;
	private String value;
	
	public static final List<ChangeOwnershipFileType> list = new ArrayList<ChangeOwnershipFileType>();
	
	public static final Map<Integer, ChangeOwnershipFileType> lookup = new HashMap<Integer, ChangeOwnershipFileType>();
	
	public static List<ChangeOwnershipFileType> listSomeElements(ChangeOwnershipFileType... transferSecuritiesTypeParams){
		List<ChangeOwnershipFileType> retorno = new ArrayList<ChangeOwnershipFileType>();
		for(ChangeOwnershipFileType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ChangeOwnershipFileType s : EnumSet.allOf(ChangeOwnershipFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private ChangeOwnershipFileType(Integer ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}

}