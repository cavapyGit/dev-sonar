package com.pradera.model.custody.blockentity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.type.BlockEntityStateType;




/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BlockEntity.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Entity
@Table(name="BLOCK_ENTITY")
public class BlockEntity implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id block entity pk. */
	@Id
	@SequenceGenerator(name="BLOCK_ENTITY_IDBLOCKENTITYPK_GENERATOR", sequenceName="SQ_ID_BLOCK_ENTITY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_ENTITY_IDBLOCKENTITYPK_GENERATOR")
	
	@Column(name="ID_BLOCK_ENTITY_PK")  
	private Long idBlockEntityPk;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The email. */
	@Column(name="Email")
	private String email;

	/** The first last name. */
	@Column(name="FIRST_LAST_NAME")
	private String firstLastName;

	/** The full name. */
	@Column(name="FULL_NAME")
	private String fullName;

	/** The person type. */
	@Column(name="PERSON_TYPE")
	private Integer personType;

	/** The home address. */
	@Column(name="HOME_ADDRESS")
	private String homeAddress;

	/** The holder. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;

	/** The ind notification. */
	@Column(name="IND_NOTIFICATION")
	private Integer indNotification;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The name. */
	private String name;

	/** The nationality. */
	private Integer nationality;

	/** The comments. */
	private String comments;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The second last name. */
	@Column(name="SECOND_LAST_NAME")
	private String secondLastName;

	/** The state block entity. */
	@Column(name="STATE_BLOCK_ENTITY")
	private Integer stateBlockEntity;
	
	/** The ind disabled. */
	@Column(name="IND_DISABLED")
	private Integer indDisabled;

	/** The ind residence. */
	@Column(name="IND_RESIDENCE")
	private Integer indResidence;

	/** The ind minor. */
	@Column(name="IND_MINOR")
	private Integer indMinor;

	/** The document source. */
	@Column(name="DOCUMENT_SOURCE")
	private Integer documentSource;
	
	/** The description type document. */
	@Transient
	private String descriptionTypeDocument;
	
	/** The description person. */
	@Transient
	private String descriptionPerson;
	
	/** The state description. */
	@Transient
	private String stateDescription;
	
	/** The block request. */
	@OneToMany(mappedBy="blockEntity",fetch=FetchType.LAZY)
	private List<BlockRequest> blockRequest;
	
    /**
     * Instantiates a new block entity.
     */
    public BlockEntity() {
    }

	/**
	 * Gets the id block entity pk.
	 *
	 * @return the id block entity pk
	 */
	public Long getIdBlockEntityPk() {
		return this.idBlockEntityPk;
	}

	/**
	 * Sets the id block entity pk.
	 *
	 * @param idBlockEntityPk the new id block entity pk
	 */
	public void setIdBlockEntityPk(Long idBlockEntityPk) {
		this.idBlockEntityPk = idBlockEntityPk;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return this.documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the first last name.
	 *
	 * @return the first last name
	 */
	public String getFirstLastName() {
		return this.firstLastName;
	}

	/**
	 * Sets the first last name.
	 *
	 * @param firstLastName the new first last name
	 */
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return this.fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	

	/**
	 * Gets the person type.
	 *
	 * @return the person type
	 */
	public Integer getPersonType() {
		return personType;
	}

	/**
	 * Sets the person type.
	 *
	 * @param personType the new person type
	 */
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	/**
	 * Gets the home address.
	 *
	 * @return the home address
	 */
	public String getHomeAddress() {
		return this.homeAddress;
	}

	/**
	 * Sets the home address.
	 *
	 * @param homeAddress the new home address
	 */
	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}	

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the ind notification.
	 *
	 * @return the ind notification
	 */
	public Integer getIndNotification() {
		return this.indNotification;
	}

	/**
	 * Sets the ind notification.
	 *
	 * @param indNotification the new ind notification
	 */
	public void setIndNotification(Integer indNotification) {
		this.indNotification = indNotification;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the nationality.
	 *
	 * @return the nationality
	 */
	public Integer getNationality() {
		return this.nationality;
	}

	/**
	 * Sets the nationality.
	 *
	 * @param nationality the new nationality
	 */
	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return this.secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
	 * Gets the state block entity.
	 *
	 * @return the state block entity
	 */
	public Integer getStateBlockEntity() {
		return this.stateBlockEntity;
	}

	/**
	 * Sets the state block entity.
	 *
	 * @param stateBlockEntity the new state block entity
	 */
	public void setStateBlockEntity(Integer stateBlockEntity) {
		this.stateBlockEntity = stateBlockEntity;
	}
	
	/**
	 * Gets the description type document.
	 *
	 * @return the description type document
	 */
	public String getDescriptionTypeDocument() {
		return descriptionTypeDocument;
	}

	/**
	 * Sets the description type document.
	 *
	 * @param descriptionTypeDocument the new description type document
	 */
	public void setDescriptionTypeDocument(String descriptionTypeDocument) {
		this.descriptionTypeDocument = descriptionTypeDocument;
	}

	/**
	 * Gets the description person.
	 *
	 * @return the description person
	 */
	public String getDescriptionPerson() {
		if(PersonType.NATURAL.getCode().equals(personType)){
			
			descriptionPerson = null;
			
			if(secondLastName!=null){
				descriptionPerson = firstLastName+" "+secondLastName+" "+name;
			}
			else{
				descriptionPerson = firstLastName+" "+name;
			}
			
			if(descriptionPerson==null){
			   descriptionPerson = fullName;
			}
		}
		else if(PersonType.JURIDIC.getCode().equals(personType)){
			 descriptionPerson = fullName;	
		}
			
		if(descriptionPerson!=null){
			descriptionPerson=descriptionPerson.toUpperCase();
			}
		
		return descriptionPerson;
	}

	/**
	 * Sets the description person.
	 *
	 * @param descriptionPerson the new description person
	 */
	public void setDescriptionPerson(String descriptionPerson) {
		this.descriptionPerson = descriptionPerson;
	}

		
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the document type description.
	 *
	 * @return the document type description
	 */
	public String getDocumentTypeDescription() {
		return DocumentType.lookup.get(this.getDocumentType()).name();
	}
	
	/**
	 * Gets the block request.
	 *
	 * @return the block request
	 */
	public List<BlockRequest> getBlockRequest() {
		return blockRequest;
	}

	/**
	 * Sets the block request.
	 *
	 * @param blockRequest the new block request
	 */
	public void setBlockRequest(List<BlockRequest> blockRequest) {
		this.blockRequest = blockRequest;
	}
	
	

	/**
	 * Gets the ind disabled.
	 *
	 * @return the ind disabled
	 */
	public Integer getIndDisabled() {
		return indDisabled;
	}

	/**
	 * Sets the ind disabled.
	 *
	 * @param indDisabled the new ind disabled
	 */
	public void setIndDisabled(Integer indDisabled) {
		this.indDisabled = indDisabled;
	}

	/**
	 * Gets the ind residence.
	 *
	 * @return the ind residence
	 */
	public Integer getIndResidence() {
		return indResidence;
	}

	/**
	 * Sets the ind residence.
	 *
	 * @param indResidence the new ind residence
	 */
	public void setIndResidence(Integer indResidence) {
		this.indResidence = indResidence;
	}

	/**
	 * Gets the ind minor.
	 *
	 * @return the ind minor
	 */
	public Integer getIndMinor() {
		return indMinor;
	}

	/**
	 * Sets the ind minor.
	 *
	 * @param indMinor the new ind minor
	 */
	public void setIndMinor(Integer indMinor) {
		this.indMinor = indMinor;
	}
	
	/**
	 * Gets the document source.
	 *
	 * @return the document source
	 */
	public Integer getDocumentSource() {
		return documentSource;
	}

	/**
	 * Sets the document source.
	 *
	 * @param documentSource the new document source
	 */
	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	 public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }


	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive(){
		if(Validations.validateIsNotNullAndNotEmpty(stateBlockEntity)){
			if(BlockEntityStateType.ACTIVATED.getCode().equals(stateBlockEntity))
				return true;
			else
				return false;
		}
		return false;
	}
}