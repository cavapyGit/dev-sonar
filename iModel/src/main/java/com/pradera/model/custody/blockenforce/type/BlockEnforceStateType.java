package com.pradera.model.custody.blockenforce.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AffectationStateType estado de las solicitudes de afectacion.
 */
public enum BlockEnforceStateType {
	
	/** The registered. */
	REGISTERED(new Integer(1780)),
	
	/** The confirmed. */
	CONFIRMED(new Integer(1781)),
	
	/** The rejected. */
	REJECTED(new Integer(1782));
	
	/** The code. */
	private Integer code;
	
	
	/**
	 * Instantiates a new affectation state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BlockEnforceStateType(Integer code){
		this.code = code;
	}
		
	/** The Constant list. */
	public static final List<BlockEnforceStateType> list = new ArrayList<BlockEnforceStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BlockEnforceStateType> lookup = new HashMap<Integer, BlockEnforceStateType>();
	
	static {
		for (BlockEnforceStateType s : EnumSet.allOf(BlockEnforceStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static BlockEnforceStateType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}
