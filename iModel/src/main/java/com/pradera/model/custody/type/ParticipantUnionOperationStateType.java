package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum ParticipantUnionOperationStateType.
 */
public enum ParticipantUnionOperationStateType {
	
	/** The registered. */
	REGISTERED(new Integer(1101),""),
	
	/** The preliminary. */
	PRELIMINARY(new Integer(1102),""),
	
	/** The anulled. */
	ANULLED(new Integer(1135),""),
	
	/** The definitive. */
	DEFINITIVE(new Integer(1103),""),

	/** The preeliminary error. */
	PRELIMINARY_ERROR(new Integer(1832),""),

	/** The definitive error */
	DEFINITIVE_ERROR(new Integer(1833),""),
	
	/** The in process */	
	IN_PROCESS(new Integer(1883),"");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<ParticipantUnionOperationStateType> list = new ArrayList<ParticipantUnionOperationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantUnionOperationStateType> lookup = new HashMap<Integer, ParticipantUnionOperationStateType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<ParticipantUnionOperationStateType> listSomeElements(ParticipantUnionOperationStateType... transferSecuritiesTypeParams){
		List<ParticipantUnionOperationStateType> retorno = new ArrayList<ParticipantUnionOperationStateType>();
		for(ParticipantUnionOperationStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ParticipantUnionOperationStateType s : EnumSet.allOf(ParticipantUnionOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new participant union operation state type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private ParticipantUnionOperationStateType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
