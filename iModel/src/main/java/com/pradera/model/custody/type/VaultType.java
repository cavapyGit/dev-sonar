package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockEntityTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/01/2014
 */
public enum VaultType {
	
	/** The block entity. */
	VAULT_CAVAPY(2722,"BOVEDA CAVAPY"),
	
	/** The holder cod. */
	PROVISORY(2723,"BOVEDA PROVISORIA");		
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	

	/** The Constant list. */
	public static final List<VaultType> list = new ArrayList<VaultType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, VaultType> lookup = new HashMap<Integer, VaultType>();

	static {
		for (VaultType s : EnumSet.allOf(VaultType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new block entity search type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private VaultType(Integer code, String value) {
		this.code = code;
		this.value = value;			
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the block entity search type
	 */
	public static VaultType get(Integer code) {
		return lookup.get(code);
	}
}