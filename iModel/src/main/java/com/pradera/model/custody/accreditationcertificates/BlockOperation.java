package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.corporatives.BlockBenefitDetail;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the BLOCK_OPERATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="BLOCK_OPERATION")
public class BlockOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	private Long idBlockOperationPk;
	
	@Column(name="DOCUMENT_NUMBER")
	private Long documentNumber;
	
	@MapsId
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_BLOCK_OPERATION_PK")
	private CustodyOperation custodyOperation;

	@Column(name="BLOCK_STATE")
	private Integer blockState;
	
	@Column(name = "BLOCK_LEVEL")
	private Integer blockLevel;
	
	@Column(name="CURRENT_BLOCK_AMOUNT")
	private BigDecimal currentBlockAmount;
	
	@Column(name="ACTUAL_BLOCK_BALANCE")
	private BigDecimal actualBlockBalance;
	
	@Column(name="ACTUAL_BLOCK_AMOUNT")
	private BigDecimal actualBlockAmount;

	@Column(name="FUTURE_BALANCES")
	private BigDecimal futureBalances;

	@Column(name="FUTURE_BENEFITS")
	private BigDecimal futureBenefits;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REF_BLOCK_OPERATION_FK")
	private BlockOperation blockOperationRef;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="IND_ACCREDITATION")
	private Integer indAccreditation;

	@Column(name="IND_AMORTIZATION")
	private Integer indAmortization;

	@Column(name="IND_CASH_DIVIDEND")
	private Integer indCashDividend;

	@Column(name="IND_INTEREST")
	private Integer indInterest;
	
	@Column(name="IND_RESCUE")
	private Integer indRescue;

	@Column(name="IND_RETURN_CONTRIBUTIONS")
	private Integer indReturnContributions;

	@Column(name="IND_STOCK_DIVIDEND")
	private Integer indStockDividend;

	@Column(name="IND_SUSCRIPTION")
	private Integer indSuscription;

	@Column(name="IND_REGISTERED_DEPOSITARY")
	private Integer indRegisteredDepositary;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
    
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;

	@Column(name="ORIGINAL_BLOCK_AMOUNT")
	private BigDecimal originalBlockAmount;

	@Column(name="ORIGINAL_BLOCK_BALANCE")
	private BigDecimal originalBlockBalance;

	@Column(name="QUOTATION_PRICE")
	private BigDecimal quotationPrice;
	
	//bi-directional many-to-one association to blockOperationRequest
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_REQUEST_FK",referencedColumnName="ID_BLOCK_REQUEST_PK")    
	private BlockRequest blockRequest;

	//bi-directional many-to-one association to HolderAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

    @OneToMany(mappedBy="blockOperation",fetch=FetchType.LAZY)
	private List<BlockBenefitDetail> blockBenefitDetail;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="blockOperation", fetch=FetchType.LAZY)
	private List<BlockMarketFactOperation> blockMarketFactOperations;
    
    @Transient
	@Lob()
	@Column(name="SUPPORT_FILE")
	@Basic(fetch=FetchType.LAZY)
	private byte[] supportFile;
	
	@Transient
	@Column(name = "SUPPORT_FILE_NAME")
	private String supportFileName;

	@Transient
	private BigDecimal copiedActualBlockBalance;
	
	@Transient
	private String blockStateDescription;
	
	@Transient
	private String blockLevelDescription;
	
    @Transient
    private boolean selected;
	
    public BlockOperation() {
    	this.indAccreditation = 0;
    	this.indRegisteredDepositary = BooleanType.NO.getCode();
    }
    
    
	


	public BlockOperation(Long idBlockOperationPk) {
		super();
		this.idBlockOperationPk = idBlockOperationPk;
	}





	public Integer getBlockState() {
		return this.blockState;
	}

	public void setBlockState(Integer blockState) {
		this.blockState = blockState;
	}

	public Long getIdBlockOperationPk() {
		return idBlockOperationPk;
	}

	public void setIdBlockOperationPk(Long idBlockOperationPk) {
		this.idBlockOperationPk = idBlockOperationPk;
	}

	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Integer getIndAccreditation() {
		return this.indAccreditation;
	}

	public void setIndAccreditation(Integer indAccreditation) {
		this.indAccreditation = indAccreditation;
	}

	public Integer getIndAmortization() {
		return this.indAmortization;
	}

	public void setIndAmortization(Integer indAmortization) {
		this.indAmortization = indAmortization;
	}

	public Integer getIndCashDividend() {
		return this.indCashDividend;
	}

	public void setIndCashDividend(Integer indCashDividend) {
		this.indCashDividend = indCashDividend;
	}

	public Integer getIndInterest() {
		return this.indInterest;
	}

	public void setIndInterest(Integer indInterest) {
		this.indInterest = indInterest;
	}

	public Integer getIndReturnContributions() {
		return this.indReturnContributions;
	}

	public void setIndReturnContributions(Integer indReturnContributions) {
		this.indReturnContributions = indReturnContributions;
	}

	public Integer getIndStockDividend() {
		return this.indStockDividend;
	}

	public void setIndStockDividend(Integer indStockDividend) {
		this.indStockDividend = indStockDividend;
	}

	public Integer getIndSuscription() {
		return this.indSuscription;
	}

	public void setIndSuscription(Integer indSuscription) {
		this.indSuscription = indSuscription;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	
	
	public BigDecimal getNominalValue() {
		return nominalValue;
	}



	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	
	
	public BigDecimal getOriginalBlockAmount() {
		return this.originalBlockAmount;
	}

	
	
	public void setOriginalBlockAmount(BigDecimal originalBlockAmount) {
		this.originalBlockAmount = originalBlockAmount;
	}

	
	
	public BigDecimal getOriginalBlockBalance() {
		return this.originalBlockBalance;
	}

	
	
	public void setOriginalBlockBalance(BigDecimal originalBlockBalance) {
		this.originalBlockBalance = originalBlockBalance;
	}



	public BigDecimal getQuotationPrice() {
		return quotationPrice;
	}



	public void setQuotationPrice(BigDecimal quotationPrice) {
		this.quotationPrice = quotationPrice;
	}



	public BlockRequest getBlockRequest() {
		return blockRequest;
	}



	public void setBlockRequest(BlockRequest blockRequest) {
		this.blockRequest = blockRequest;
	}

	public HolderAccount getHolderAccount() {
		return this.holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public boolean getSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public BigDecimal getCurrentBlockAmount() {
		return currentBlockAmount;
	}

	public void setCurrentBlockAmount(BigDecimal currentBlockAmount) {
		this.currentBlockAmount = currentBlockAmount;
	}

	public BigDecimal getFutureBalances() {
		return futureBalances;
	}

	public Long getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(Long documentNumber) {
		this.documentNumber = documentNumber;
	}

	public void setFutureBalances(BigDecimal futureBalances) {
		this.futureBalances = futureBalances;
	}

	public BigDecimal getFutureBenefits() {
		return futureBenefits;
	}

	public void setFutureBenefits(BigDecimal futureBenefits) {
		this.futureBenefits = futureBenefits;
	}

	public Integer getBlockLevel() {
		return blockLevel;
	}

	public void setBlockLevel(Integer blockLevel) {
		this.blockLevel = blockLevel;
	}
	
	public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(custodyOperation != null){
            	custodyOperation.setAudit(loggerUser);
            }
        }
    }

	public BigDecimal getActualBlockBalance() {
		return actualBlockBalance;
	}

	public void setActualBlockBalance(BigDecimal actualBlockBalance) {
		this.actualBlockBalance = actualBlockBalance;
	}

	public BigDecimal getActualBlockAmount() {
		return actualBlockAmount;
	}

	public void setActualBlockAmount(BigDecimal actualBlockAmount) {
		this.actualBlockAmount = actualBlockAmount;
	}

	public BlockOperation getBlockOperationRef() {
		return blockOperationRef;
	}


	public void setBlockOperationRef(BlockOperation blockOperationRef) {
		this.blockOperationRef = blockOperationRef;
	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub  blockOperationCertifications
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("blockMarketFactOperations", blockMarketFactOperations);
        return detailsMap;
	}


	public BigDecimal getCopiedActualBlockBalance() {
		return copiedActualBlockBalance;
	}


	public void setCopiedActualBlockBalance(BigDecimal copiedActualBlockBalance) {
		this.copiedActualBlockBalance = copiedActualBlockBalance;
	}

	public Integer getIndRescue() {
		return indRescue;
	}

	public void setIndRescue(Integer indRescue) {
		this.indRescue = indRescue;
	}

	public String getBlockStateDescription() {
		if(blockState!=null){
			return AffectationStateType.get(blockState).getValue();
		}else{
			return "";
		}		
	}

	public void setBlockStateDescription(String blockStateDescription) {
		this.blockStateDescription = blockStateDescription;
	}
	
    public String getBlockLevelDescription(){
		return blockLevelDescription;

    }

	public void setBlockLevelDescription(String blockLevelDescription) {
		this.blockLevelDescription = blockLevelDescription;
	}

	public List<BlockBenefitDetail> getBlockBenefitDetail() {
		return blockBenefitDetail;
	}

	public void setBlockBenefitDetail(List<BlockBenefitDetail> blockBenefitDetail) {
		this.blockBenefitDetail = blockBenefitDetail;
	}

	public List<BlockMarketFactOperation> getBlockMarketFactOperations() {
		return blockMarketFactOperations;
	}

	public void setBlockMarketFactOperations(
			List<BlockMarketFactOperation> blockMarketFactOperations) {
		this.blockMarketFactOperations = blockMarketFactOperations;
	}

	public byte[] getSupportFile() {
		return supportFile;
	}

	public void setSupportFile(byte[] supportFile) {
		this.supportFile = supportFile;
	}

	public String getSupportFileName() {
		return supportFileName;
	}

	public void setSupportFileName(String supportFileName) {
		this.supportFileName = supportFileName;
	}

	public Integer getIndRegisteredDepositary() {
		return indRegisteredDepositary;
	}

	public void setIndRegisteredDepositary(Integer indRegisteredDepositary) {
		this.indRegisteredDepositary = indRegisteredDepositary;
	}

}