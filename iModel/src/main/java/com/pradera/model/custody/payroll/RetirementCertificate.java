package com.pradera.model.custody.payroll;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;

@Entity
@Table(name="RETIREMENT_CERTIFICATE")
public class RetirementCertificate implements Serializable, Auditable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="RETIREMENT_CERTIFICATE_IDRETIREMENTCERTIFICAPK_GENERATOR", sequenceName="SQ_RETIREMENT_CERTIFICATE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RETIREMENT_CERTIFICATE_IDRETIREMENTCERTIFICAPK_GENERATOR")
	@Column(name="ID_RETIREMENT_CERTIFICATE_PK")
	private Long idRetirementCertificatePk;
	
	@ManyToOne
	@JoinColumn(name="ID_RETIREMENT_OPERATION_FK")
	private RetirementOperation retirementOperation;
	
	@ManyToOne
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	public Long getIdRetirementCertificatePk() {
		return idRetirementCertificatePk;
	}

	public void setIdRetirementCertificatePk(Long idRetirementCertificatePk) {
		this.idRetirementCertificatePk = idRetirementCertificatePk;
	}

	public RetirementOperation getRetirementOperation() {
		return retirementOperation;
	}

	public void setRetirementOperation(RetirementOperation retirementOperation) {
		this.retirementOperation = retirementOperation;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null){
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}


}
