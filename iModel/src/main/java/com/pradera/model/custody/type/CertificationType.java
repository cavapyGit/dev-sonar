package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum CertificationType {
	AVAILABLE_BALANCE(new Integer(223),"SALDO DISPONIBLE"),
	BLOCKED_BALANCE(new Integer(439),"SALDO BLOQUEADO"),
	REPORTED_BALANCE(new Integer(1736), "SALDO MIXTO");
	
	private Integer code;
	private String value;
	public static final List<CertificationType> list = new ArrayList<CertificationType>();
	
	public static final Map<Integer, CertificationType> lookup = new HashMap<Integer, CertificationType>();
	public static List<CertificationType> listSomeElements(CertificationType... transferSecuritiesTypeParams){
		List<CertificationType> retorno = new ArrayList<CertificationType>();
		for(CertificationType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (CertificationType s : EnumSet.allOf(CertificationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private CertificationType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static CertificationType get(Integer code) {
		return lookup.get(code);
	}
}
