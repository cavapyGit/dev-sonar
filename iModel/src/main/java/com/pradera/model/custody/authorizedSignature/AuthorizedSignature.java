package com.pradera.model.custody.authorizedSignature;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the AUTHORIZED_SIGNATURE database table.
 * 
 */
@Entity
@Table(name="AUTHORIZED_SIGNATURE")
@NamedQuery(name="AuthorizedSignature.findAll", query="SELECT a FROM AuthorizedSignature a")
public class AuthorizedSignature implements Serializable,Auditable{

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="AUTHORIZED_SIGNATURE_GENERATOR", sequenceName="SQ_ID_AUTHORIZED_SIGNATURE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUTHORIZED_SIGNATURE_GENERATOR")
	
	@Column(name="ID_AUTHORIZED_SIGNATURE_PK")
	private Long idAuthorizedSignaturePk;
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;
	@Column(name="FIRST_LAST_NAME")
	private String firstLastName;
	@Column(name="SECOND_LAST_NAME")
	private String secondLastName;
	@Column(name="NAME")
	private String name;
	@Column(name="JOB_POSITION")
	private String jobPosition;
	@Column(name="SITUATION")
	private Integer situation;
	@Column(name="SIGNATURE_TYPE")
	private String signatureType;
	@Column(name="INSTITUTION_TYPE")
	private Integer institutionType;
	@Column(name="ID_INSTITUTION_FK")
	private String idInstitutionFk;  
	@Lob
	@Column(name="SIGNATURE_IMG")
	private byte[] signatureImg;
	@Column(name="SIGNATURE_NAME")
	private String signatureName;
	@Column(name="SIGNATURE_SIZE")
	private Long signatureSize;
	@Column(name="SIGNATURE_EXTENSION")
	private String signatureExtension;
	
	@Column(name="FILE_CONTENT")
	private byte[] fileContent;
	@Column(name="FILE_NAME")
	private String fileName;
	@Column(name="FILE_SIZE")
	private Long fileSize;
	@Column(name="FILE_EXTENSION")
	private String fileExtension;
	
    @Column(name="\"STATE\"")
	private Integer state;
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;
	@Column(name="REGISTER_USER")
	private String registerUser;
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CONFIRMATION_DATE")
	private Date confirmationDate;
	@Column(name="CONFIRMATION_USER")
	private String confirmationUser;
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REJECT_DATE")
	private Date rejectDate;
	@Column(name="REJECT_USER")
	private String rejectUser;
	@Column(name="REASON_REJECT")
	private String reasonReject; 
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Transient
	private String keyAuthorizedSignaturePk;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            if(idAuthorizedSignaturePk!=null)
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            if(idAuthorizedSignaturePk!=null)
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdAuthorizedSignaturePk() {
		return idAuthorizedSignaturePk;
	}

	public void setIdAuthorizedSignaturePk(Long idAuthorizedSignaturePk) {
		this.idAuthorizedSignaturePk = idAuthorizedSignaturePk;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public Integer getSituation() {
		return situation;
	}

	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	public String getSignatureType() {
		return signatureType;
	}

	public void setSignatureType(String signatureType) {
		this.signatureType = signatureType;
	}

	public Integer getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	public String getIdInstitutionFk() {
		return idInstitutionFk;
	}

	public void setIdInstitutionFk(String idInstitutionFk) {
		this.idInstitutionFk = idInstitutionFk;
	}

	public byte[] getSignatureImg() {
		return signatureImg;
	}

	public void setSignatureImg(byte[] signatureImg) {
		this.signatureImg = signatureImg;
	}

	public String getSignatureName() {
		return signatureName;
	}

	public void setSignatureName(String signatureName) {
		this.signatureName = signatureName;
	}

	public Long getSignatureSize() {
		return signatureSize;
	}

	public void setSignatureSize(Long signatureSize) {
		this.signatureSize = signatureSize;
	}

	public String getSignatureExtension() {
		return signatureExtension;
	}

	public void setSignatureExtension(String signatureExtension) {
		this.signatureExtension = signatureExtension;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getConfirmationUser() {
		return confirmationUser;
	}

	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	public String getReasonReject() {
		return reasonReject;
	}

	public void setReasonReject(String reasonReject) {
		this.reasonReject = reasonReject;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public String getKeyAuthorizedSignaturePk() {
		return keyAuthorizedSignaturePk;
	}

	public void setKeyAuthorizedSignaturePk(String keyAuthorizedSignaturePk) {
		this.keyAuthorizedSignaturePk = keyAuthorizedSignaturePk;
	}
}