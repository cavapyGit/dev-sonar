package com.pradera.model.custody.participantunion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the PARTICIPANT_UNION_BALANCE database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_UNION_BALANCE")
public class ParticipantUnionBalance implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARTICIPANT_UNION_BALANCE_GENERATOR", sequenceName="SQ_ID_PART_UNION_BALANCE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_UNION_BALANCE_GENERATOR")
	@Column(name="ID_PARTICIPANT_UNION_BALANCE")
	private Long idParticipantUnionBalance;

	@Column(name="AVAILABLE_BALANCE")
	private BigDecimal availableBalance;

	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance;

	//bi-directional many-to-one association to holderAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ORIGIN_HOLDER_ACCOUNT_FK")
	private HolderAccount originHolderAccount;
    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="ID_TARGET_HOLDER_ACCOUNT_FK")
   	private HolderAccount targetHolderAccount;

    //bi-directional many-to-one association to securities
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

    //bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ORIGIN_PARTICIPANT_FK")
	private Participant originParticipant;
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_PARTICIPANT_FK")
	private Participant targetParticipant;

	@Column(name="IND_SOURCE_TARGET")
	private Integer indSourceTarget;

	@Column(name="IND_PROCESSED")
	private Integer indProcessed;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OTHER_BLOCK_BALANCE")
	private BigDecimal otherBlockBalance;

	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance;

	@Column(name="RESERVE_BALANCE")
	private BigDecimal reserveBalance;

	@Column(name="OPPOSITION_BALANCE")
	private BigDecimal opositionBalance;
	
	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;

	//bi-directional many-to-one association to ParticipantUnionOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PART_UNION_OPERATION_FK")
	private ParticipantUnionOperation participantUnionOperation;

    
    
    
    public ParticipantUnionBalance() {
    	indSourceTarget = BooleanType.NO.getCode();
    	indProcessed = BooleanType.NO.getCode();
    }

	public Long getIdParticipantUnionBalance() {
		return this.idParticipantUnionBalance;
	}

	public void setIdParticipantUnionBalance(Long idParticipantUnionBalance) {
		this.idParticipantUnionBalance = idParticipantUnionBalance;
	}

	public BigDecimal getAvailableBalance() {
		return this.availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getBanBalance() {
		return this.banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}


	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public Integer getIndSourceTarget() {
		return indSourceTarget;
	}

	public void setIndSourceTarget(Integer indSourceTarget) {
		this.indSourceTarget = indSourceTarget;
	}

	public Integer getIndProcessed() {
		return this.indProcessed;
	}

	public void setIndProcessed(Integer indProcessed) {
		this.indProcessed = indProcessed;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getOtherBlockBalance() {
		return this.otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getPawnBalance() {
		return this.pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getReserveBalance() {
		return this.reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public ParticipantUnionOperation getParticipantUnionOperation() {
		return this.participantUnionOperation;
	}

	public void setParticipantUnionOperation(ParticipantUnionOperation participantUnionOperation) {
		this.participantUnionOperation = participantUnionOperation;
	}

	public HolderAccount getOriginHolderAccount() {
		return originHolderAccount;
	}

	public void setOriginHolderAccount(HolderAccount originHolderAccount) {
		this.originHolderAccount = originHolderAccount;
	}

	public HolderAccount getTargetHolderAccount() {
		return targetHolderAccount;
	}

	public void setTargetHolderAccount(HolderAccount targetHolderAccount) {
		this.targetHolderAccount = targetHolderAccount;
	}

	public Participant getOriginParticipant() {
		return originParticipant;
	}

	public void setOriginParticipant(Participant originParticipant) {
		this.originParticipant = originParticipant;
	}

	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}

	public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
        }
    }

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal getOpositionBalance() {
		return opositionBalance;
	}

	public void setOpositionBalance(BigDecimal opositionBalance) {
		this.opositionBalance = opositionBalance;
	}
	
}