package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum ReasonType {
	
	ERROR_SOLICITUD_PARTICIPANTE(new Integer(714),"deposito.reason.err.sol.part"),
	ERROR_DATOS_INCORRECTO(new Integer(715),"deposito.reason.err.dat.incorrect"),
//	TIPO_DE_INSTRUMENTO_INCORRECTO(new Integer(716),"deposito.reason.err.instrument.type.wrong"),
//	CLASE_DE_VALOR_INCORRECTA(new Integer(717),"deposito.reason.err.security.class.wrong"),
//	NUMERO_DE_CERTIFICADO_INCORRECTO(new Integer(718),"deposito.reason.err.certificate.number.wrong"),
//	FECHA_DE_EMISION_INCORRECTA(new Integer(719),"deposito.reason.err.issue.date.wrong"),
//	FECHA_DE_VENCIMIENTO_INCORRECTA(new Integer(720),"deposito.reason.err.due.date.wrong"),
//	VALOR_NOMINAL_INCORRECTO(new Integer(721),"deposito.reason.err.nominal.value.wrong"),
//	MONEDA_INCORRECTA(new Integer(722),"deposito.reason.err.currency.value.wrong"),
	OTRO(new Integer(716),"deposito.reason.others");
	
	
	private Integer code;
	private String value;

	public static final List<ReasonType> list = new ArrayList<ReasonType>();
	public static final Map<Integer, ReasonType> lookup = new HashMap<Integer, ReasonType>();

	static {
		for (ReasonType s : EnumSet.allOf(ReasonType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	private ReasonType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getValue() {
		return value;
	}
	
	
	
	public static ReasonType get(Integer code) {
		return lookup.get(code);
	}

}
