package com.pradera.model.custody.rectification;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the RECTIFICATION_OPERATION database table.
 * 
 */
@Entity
@Table(name="RECTIFICATION_MARKETFACT")
public class RectificationMarketFact implements Serializable,Auditable {
	
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RECT_MFACT_GENERATOR", sequenceName="SQ_ID_RECTIFICATION_MFACT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RECT_MFACT_GENERATOR")
	@Column(name="ID_RECTIFICATION_MARKETFACT_PK")
	private Long idRectificationMarketFactPk;	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RECTIFICATION_DETAIL_FK")
	private RectificationDetail rectificationDetail;

	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MARKET_DATE")
	private Date marketDate;

	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

	@Column(name="OPERATION_QUANTITY")
	private BigDecimal operationQuantity;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	
	public RectificationMarketFact() {
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
	    if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser =loggerUser.getUserName();           
        }		
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdRectificationMarketFactPk() {
		return idRectificationMarketFactPk;
	}

	public void setIdRectificationMarketFactPk(Long idRectificationMarketFactPk) {
		this.idRectificationMarketFactPk = idRectificationMarketFactPk;
	}

	public RectificationDetail getRectificationDetail() {
		return rectificationDetail;
	}

	public void setRectificationDetail(RectificationDetail rectificationDetail) {
		this.rectificationDetail = rectificationDetail;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getOperationQuantity() {
		return operationQuantity;
	}

	public void setOperationQuantity(BigDecimal quantity) {
		this.operationQuantity = quantity;
	}


	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	
	
	
}
