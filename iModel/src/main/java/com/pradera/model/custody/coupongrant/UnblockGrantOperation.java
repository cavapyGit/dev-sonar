package com.pradera.model.custody.coupongrant;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the CHANGE_OWNERSHIP_OPERATION database table.
 * 
 */
@Entity
@Table(name="UNBLOCK_GRANT_OPERATION")
public class UnblockGrantOperation implements Serializable,Auditable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long idUnblockGrantOperationPk;
	
	@Version
    private Long version;
	
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_UNBLOCK_GRANT_OPERATION_PK")
	private CustodyOperation custodyOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_COUPON_GRANT_OPERATION_FK")
  	private CouponGrantOperation couponGrantOperation;

	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
  	private CorporativeOperation corporativeOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_PARTICIPANT_FK")
  	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
  	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_SECURITY_CODE_FK")
  	private Security securities;
	
	@Column(name="UNBLOCK_BALANCE")
	private BigDecimal unblockBalance;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	
	public UnblockGrantOperation() {
		super();
	}

	
	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}


	/**
	 * @param version the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}


	/**
	 * @return the custodyOperation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}


	/**
	 * @param custodyOperation the custodyOperation to set
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}


	public Long getIdUnblockGrantOperationPk() {
		return idUnblockGrantOperationPk;
	}

	public void setIdUnblockGrantOperationPk(Long idUnblockGrantOperationPk) {
		this.idUnblockGrantOperationPk = idUnblockGrantOperationPk;
	}

	public CouponGrantOperation getCouponGrantOperation() {
		return couponGrantOperation;
	}

	public void setCouponGrantOperation(CouponGrantOperation couponGrantOperation) {
		this.couponGrantOperation = couponGrantOperation;
	}

	public BigDecimal getUnblockBalance() {
		return unblockBalance;
	}

	public void setUnblockBalance(BigDecimal unblockBalance) {
		this.unblockBalance = unblockBalance;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}


	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}


	public Participant getParticipant() {
		return participant;
	}


	public void setParticipant(Participant participant) {
		this.participant = participant;
	}


	public HolderAccount getHolderAccount() {
		return holderAccount;
	}


	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}


	public Security getSecurities() {
		return securities;
	}


	public void setSecurities(Security securities) {
		this.securities = securities;
	}


	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(custodyOperation!=null){
    			custodyOperation.setLastModifyApp(lastModifyApp);
    			custodyOperation.setLastModifyDate(lastModifyDate);
    			custodyOperation.setLastModifyIp(lastModifyIp);
    			custodyOperation.setLastModifyUser(lastModifyUser);
    		}
        }

	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        return null;
	}

}