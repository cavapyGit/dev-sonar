package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.HolderAccountOperation;

/**
 * The persistent class for the ACCREDITATION_MARKETFACT database table.
 * 
 */
@Entity
@Table(name="REPORT_ACCREDITATION")
public class ReportAccreditation implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SQ_REPORT_ACCREDITATION_PK", sequenceName = "SQ_ID_REPORT_ACCREDITATION", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_REPORT_ACCREDITATION_PK")
	@Column(name="ID_REPORT_ACCREDITATION_PK")
	private Long idReportAccreditationPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCREDITATION_DETAIL_FK", referencedColumnName="ID_ACCREDITATION_DETAIL_PK")
	private AccreditationDetail accreditationDetailFk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK", referencedColumnName="ID_HOLDER_ACCOUNT_OPERATION_PK")
	private HolderAccountOperation holderAccountOperationFk;
	
	@Column(name="REPORT_BALANCE")
	private BigDecimal reportBalance;
	
	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	public AccreditationDetail getAccreditationDetailFk() {
		return accreditationDetailFk;
	}

	public void setAccreditationDetailFk(AccreditationDetail accreditationDetailFk) {
		this.accreditationDetailFk = accreditationDetailFk;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Long getIdReportAccreditationPk() {
		return idReportAccreditationPk;
	}

	public void setIdReportAccreditationPk(Long idReportAccreditationPk) {
		this.idReportAccreditationPk = idReportAccreditationPk;
	}

	public BigDecimal getReportBalance() {
		return reportBalance;
	}

	public void setReportBalance(BigDecimal reportBalance) {
		this.reportBalance = reportBalance;
	}

	public HolderAccountOperation getHolderAccountOperationFk() {
		return holderAccountOperationFk;
	}

	public void setHolderAccountOperationFk(
			HolderAccountOperation holderAccountOperationFk) {
		this.holderAccountOperationFk = holderAccountOperationFk;
	}

}
