package com.pradera.model.custody.channelopening.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum MotiveChannelOpeningType {
	
	REGULARIZACION_DPF(Integer.valueOf(2470),"REGULARIZACION DE DPF REZAGADOS"),
	OTROS(Integer.valueOf(2471),"OTROS MOTIVOS PARA LA APERTURA DE CANAL"),
	
	;
	
	/** The Constant list. */
	public static final List<MotiveChannelOpeningType> list = new ArrayList<MotiveChannelOpeningType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, MotiveChannelOpeningType> lookup = new HashMap<Integer, MotiveChannelOpeningType>();
	static {
		for (MotiveChannelOpeningType s : EnumSet.allOf(MotiveChannelOpeningType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private MotiveChannelOpeningType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static MotiveChannelOpeningType get(Integer code) {
		return lookup.get(code);
	}
}
