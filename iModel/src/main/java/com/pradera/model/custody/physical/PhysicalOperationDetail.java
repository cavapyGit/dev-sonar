package com.pradera.model.custody.physical;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the PHYSICAL_OPERATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="PHYSICAL_OPERATION_DETAIL")
public class PhysicalOperationDetail implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PHYSICAL_OPERATION_DETAIL_GENERATOR", sequenceName="SQ_ID_PHYSICAL_OPERATION",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PHYSICAL_OPERATION_DETAIL_GENERATOR")
	
	@Column(name="ID_PHYSICAL_OPERATION_DETAIL")
	private Long idPhysicalOperationDetail;

	//bi-directional many-to-one association to PhysicalCertificate
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;

	//bi-directional many-to-one association to PhysicalCertificateOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_OPERATION_FK")
	private PhysicalCertificateOperation physicalCertificateOperation;
    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="APPLICATION_DATE")
	private Date applicationDate;

	@Column(name="APPLICATION_USER")
	private String applicationUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="APPROVAL_DATE")
	private Date approvalDate;

	@Column(name="APPROVAL_USER")
	private String approvalUser;
	
	@Column(name="IND_APPLIED")
	private Integer indApplied;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="AUTHORIZATION_DATE")
	private Date authorizationDate;

	@Column(name="AUTHORIZATION_USER")
	private String authorizationUser;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;

	@Column(name="CONFIRM_USER")
	private String confirmUser;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REAL_DELIVERY_DATE")
	private Date realDeliveryDate;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REJECT_DATE")
	private Date rejectDate;

	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;

	@Column(name="REJECT_MOTIVE_OTHER")
	private String rejectMotiveOther;

	@Column(name="REJECT_USER")
	private String rejectUser;

	@Column(name="\"STATE\"")
	private Integer state;
	
	@Column(name="IND_DEMATERIALIZED")
	private Integer indDematerialized;
	
	@Transient
	private String motiveType;
	
	

	public PhysicalOperationDetail() {
		super();
		this.indDematerialized= BooleanType.NO.getCode();
	}

	public Long getIdPhysicalOperationDetail() {
		return idPhysicalOperationDetail;
	}

	public void setIdPhysicalOperationDetail(Long idPhysicalOperationDetail) {
		this.idPhysicalOperationDetail = idPhysicalOperationDetail;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApplicationUser() {
		return applicationUser;
	}

	public void setApplicationUser(String applicationUser) {
		this.applicationUser = applicationUser;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalUser() {
		return approvalUser;
	}

	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	public Integer getIndApplied() {
		return indApplied;
	}

	public void setIndApplied(Integer indApplied) {
		this.indApplied = indApplied;
	}

	public Date getAuthorizationDate() {
		return authorizationDate;
	}

	public void setAuthorizationDate(Date authorizationDate) {
		this.authorizationDate = authorizationDate;
	}

	public String getAuthorizationUser() {
		return authorizationUser;
	}

	public void setAuthorizationUser(String authorizationUser) {
		this.authorizationUser = authorizationUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRealDeliveryDate() {
		return realDeliveryDate;
	}

	public void setRealDeliveryDate(Date realDeliveryDate) {
		this.realDeliveryDate = realDeliveryDate;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public Integer getRejectMotive() {
		return rejectMotive;
	}

	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getMotiveType() {
		return motiveType;
	}

	public void setMotiveType(String motiveType) {
		this.motiveType = motiveType;
	}

	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	public PhysicalCertificateOperation getPhysicalCertificateOperation() {
		return physicalCertificateOperation;
	}

	public void setPhysicalCertificateOperation(
			PhysicalCertificateOperation physicalCertificateOperation) {
		this.physicalCertificateOperation = physicalCertificateOperation;
	}
   
	public Integer getIndDematerialized() {
		return indDematerialized;
	}

	public void setIndDematerialized(Integer indDematerialized) {
		this.indDematerialized = indDematerialized;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(physicalCertificate != null){
            	physicalCertificate.setLastModifyApp(lastModifyApp);
            	physicalCertificate.setLastModifyDate(lastModifyDate);
            	physicalCertificate.setLastModifyIp(lastModifyIp);
            	physicalCertificate.setLastModifyUser(lastModifyUser);
            }
            
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
 		return null;
	}
	
}