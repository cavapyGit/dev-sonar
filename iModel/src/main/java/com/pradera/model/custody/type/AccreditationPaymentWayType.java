package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AccreditationPaymentWayType {
	CASH(new Integer(634),"CONTADO"),
	INVOICED_PARTICIPANT(new Integer(635),"FACTURADO PARTICIPANT"),
	FREE(new Integer(636),"SIN COBRO");
	private Integer code;
	private String value;
	public static final List<AccreditationPaymentWayType> list = new ArrayList<AccreditationPaymentWayType>();
	
	public static final Map<Integer, AccreditationPaymentWayType> lookup = new HashMap<Integer, AccreditationPaymentWayType>();
	public static List<AccreditationPaymentWayType> listSomeElements(AccreditationPaymentWayType... transferSecuritiesTypeParams){
		List<AccreditationPaymentWayType> retorno = new ArrayList<AccreditationPaymentWayType>();
		for(AccreditationPaymentWayType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (AccreditationPaymentWayType s : EnumSet.allOf(AccreditationPaymentWayType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private AccreditationPaymentWayType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
