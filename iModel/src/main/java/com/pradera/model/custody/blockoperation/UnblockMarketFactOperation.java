package com.pradera.model.custody.blockoperation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the BLOCK_OPERATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="UNBLOCK_MARKETFACT_OPERATION")
public class UnblockMarketFactOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "SQ_UNBLOCK_MARK_OPERAT_PK", sequenceName = "SQ_ID_UNBLOCK_MARK_OPERAT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_UNBLOCK_MARK_OPERAT_PK")
	@Column(name="ID_UNBLOCK_MARKETFACT_PK")
	private Long idUnblockMarketFactOperatPk;
//	
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_MARKETFACT_BALANCE_FK")
//	private HolderMarketFactBalance holderMarketFactBalance;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;

	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_UNBLOCK_OPERATION_FK")
	private UnblockOperation unblockOperation;
	
	@Column(name="UNBLOCK_MARKET_STATE")
	private Integer unblockMarketState;
	
	@Column(name="UNBLOCK_QUANTITY")
	private BigDecimal unblockQuantity;
	
	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registryUser;

	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
	
//	public HolderMarketFactBalance getHolderMarketFactBalance() {
//		return holderMarketFactBalance;
//	}
//
//	public void setHolderMarketFactBalance(HolderMarketFactBalance holderMarketFactBalance) {
//		this.holderMarketFactBalance = holderMarketFactBalance;
//	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}


	public UnblockOperation getUnblockOperation() {
		return unblockOperation;
	}

	public void setUnblockOperation(UnblockOperation unblockOperation) {
		this.unblockOperation = unblockOperation;
	}

	public Long getIdUnblockMarketFactOperatPk() {
		return idUnblockMarketFactOperatPk;
	}

	public void setIdUnblockMarketFactOperatPk(Long idUnblockMarketFactOperatPk) {
		this.idUnblockMarketFactOperatPk = idUnblockMarketFactOperatPk;
	}

	public Integer getUnblockMarketState() {
		return unblockMarketState;
	}

	public void setUnblockMarketState(Integer unblockMarketState) {
		this.unblockMarketState = unblockMarketState;
	}

	public BigDecimal getUnblockQuantity() {
		return unblockQuantity;
	}

	public void setUnblockQuantity(BigDecimal unblockQuantity) {
		this.unblockQuantity = unblockQuantity;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}
}