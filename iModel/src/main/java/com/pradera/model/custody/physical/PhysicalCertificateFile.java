/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.custody.physical;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jquino
 */
@Entity
@Table(name = "PHYSICAL_CERTIFICATE_FILE")
@NamedQueries({
    @NamedQuery(name = "PhysicalCertificateFile.findAll", query = "SELECT a FROM PhysicalCertificateFile a"),
    @NamedQuery(name = "PhysicalCertificateFile.findByIdFilePk", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.idPhysicalCertificateFilePk = :idPhysicalCertificateFilePk"),
    @NamedQuery(name = "PhysicalCertificateFile.findByFileName", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.fileName = :fileName"),
    @NamedQuery(name = "PhysicalCertificateFile.findByFileType", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.fileType = :fileType"),
    @NamedQuery(name = "PhysicalCertificateFile.findByRegistryUser", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.registryUser = :registryUser"),
    @NamedQuery(name = "PhysicalCertificateFile.findByRegistryDate", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.registryDate = :registryDate"),
    @NamedQuery(name = "PhysicalCertificateFile.findByRegistryIp", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.registryIp = :registryIp"),
    @NamedQuery(name = "PhysicalCertificateFile.findByAttachedName", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.attachedName = :attachedName"),
    @NamedQuery(name = "PhysicalCertificateFile.findByAttachedType", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.attachedType = :attachedType"),
    @NamedQuery(name = "PhysicalCertificateFile.findByRegistryUserAtt", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.registryUserAtt = :registryUserAtt"),
    @NamedQuery(name = "PhysicalCertificateFile.findByRegistryDateAtt", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.registryDateAtt = :registryDateAtt"),
    @NamedQuery(name = "PhysicalCertificateFile.findByRegistryIpAtt", query = "SELECT a FROM PhysicalCertificateFile a WHERE a.registryIpAtt = :registryIpAtt")})

public class PhysicalCertificateFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
	@SequenceGenerator(name="SQ_ID_PHYSICAL_CERTIFICATE_FILE_PK", sequenceName="SQ_ID_PHYSICAL_CERTIFICATE_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_PHYSICAL_CERTIFICATE_FILE_PK")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PHYSICAL_CERTIFICATE_FILE_PK", nullable = false)
    private Long idPhysicalCertificateFilePk;
    
    @Size(max = 100)
    @Column(name = "FILE_NAME", length = 100)
    private String fileName;
    
    @Size(max = 20)
    @Column(name = "FILE_TYPE", length = 20)
    private String fileType;
    
    @Lob
    @Column(name = "FILE_CONTENT")
    private Serializable fileContent;
    
    @Size(max = 20)
    @Column(name = "REGISTRY_USER", length = 20)
    private String registryUser;
    
    @Column(name = "REGISTRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    
    @Size(max = 20)
    @Column(name = "REGISTRY_IP", length = 20)
    private String registryIp;
    
    @Size(max = 100)
    @Column(name = "ATTACHED_NAME", length = 100)
    private String attachedName;
    
    @Size(max = 20)
    @Column(name = "ATTACHED_TYPE", length = 20)
    private String attachedType;
    
    @Lob
    @Column(name = "ATTACHED_CONTENT")
    private Serializable attachedContent;
    
    @Size(max = 20)
    @Column(name = "REGISTRY_USER_ATT", length = 20)
    private String registryUserAtt;
    
    @Column(name = "REGISTRY_DATE_ATT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDateAtt;
    
    @Size(max = 20)
    @Column(name = "REGISTRY_IP_ATT", length = 20)
    private String registryIpAtt;
    /*
    @OneToMany(mappedBy = "idPhysicalCertificateFileFk", fetch = FetchType.LAZY)
    private List<PhysicalCertificate> physicalCertificateList;
    */
	/** true: registro masivo mediante archivo Excel false: registro simple */
	@Transient
	private boolean massiveRegister;
	
	@Transient
	private String nameFileExcel;
	
	@Transient
	private String nameFileAttached;
	
	
	
	
	public PhysicalCertificateFile() {
		this.massiveRegister = false;
	}

    public PhysicalCertificateFile(Long idFilePk) {
        this.idPhysicalCertificateFilePk = idFilePk;
    }

    public Long getIdPhysicalCertificateFilePk() {
		return idPhysicalCertificateFilePk;
	}

	public void setIdPhysicalCertificateFilePk(Long idPhysicalCertificateFilePk) {
		this.idPhysicalCertificateFilePk = idPhysicalCertificateFilePk;
	}
	/*
	public List<PhysicalCertificate> getPhysicalCertificateList() {
		return physicalCertificateList;
	}

	public void setPhysicalCertificateList(List<PhysicalCertificate> physicalCertificateList) {
		this.physicalCertificateList = physicalCertificateList;
	}
	 */
	public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

	public boolean isMassiveRegister() {
		return massiveRegister;
	}

	public void setMassiveRegister(boolean massiveRegister) {
		this.massiveRegister = massiveRegister;
	}

	public String getFileType() {
        return fileType;
    }

    public String getNameFileExcel() {
		return nameFileExcel;
	}

	public void setNameFileExcel(String nameFileExcel) {
		this.nameFileExcel = nameFileExcel;
	}

	public String getNameFileAttached() {
		return nameFileAttached;
	}

	public void setNameFileAttached(String nameFileAttached) {
		this.nameFileAttached = nameFileAttached;
	}

	public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Serializable getFileContent() {
        return fileContent;
    }

    public void setFileContent(Serializable fileContent) {
        this.fileContent = fileContent;
    }

    public String getRegistryUser() {
        return registryUser;
    }

    public void setRegistryUser(String registryUser) {
        this.registryUser = registryUser;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public String getRegistryIp() {
        return registryIp;
    }

    public void setRegistryIp(String registryIp) {
        this.registryIp = registryIp;
    }

    public String getAttachedName() {
        return attachedName;
    }

    public void setAttachedName(String attachedName) {
        this.attachedName = attachedName;
    }

    public String getAttachedType() {
        return attachedType;
    }

    public void setAttachedType(String attachedType) {
        this.attachedType = attachedType;
    }

    public Serializable getAttachedContent() {
        return attachedContent;
    }

    public void setAttachedContent(Serializable attachedContent) {
        this.attachedContent = attachedContent;
    }

    public String getRegistryUserAtt() {
        return registryUserAtt;
    }

    public void setRegistryUserAtt(String registryUserAtt) {
        this.registryUserAtt = registryUserAtt;
    }

    public Date getRegistryDateAtt() {
        return registryDateAtt;
    }

    public void setRegistryDateAtt(Date registryDateAtt) {
        this.registryDateAtt = registryDateAtt;
    }

    public String getRegistryIpAtt() {
        return registryIpAtt;
    }

    public void setRegistryIpAtt(String registryIpAtt) {
        this.registryIpAtt = registryIpAtt;
    }

}