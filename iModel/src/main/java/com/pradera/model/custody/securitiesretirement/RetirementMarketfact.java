package com.pradera.model.custody.securitiesretirement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * The Class RetirementMarketfact.
 */
@NamedQueries({
	@NamedQuery(name = RetirementMarketfact.RETIREMENT_MARKETFACT_BY_RETIREMENT_DET, query = "Select rmc From RetirementMarketfact rmc WHERE rmc.retirementDetail.idRetirementDetailPk = :idRetiementDetailPrm ")
})
@Entity
@Table(name="RETIREMENT_MARKETFACT")
public class RetirementMarketfact implements Serializable, Auditable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1837572191303314564L;
	
	public static final String RETIREMENT_MARKETFACT_BY_RETIREMENT_DET="RetirementMarketfact.searchByRetirementDet";
	
	/** The id retirement marketfact pk. */
	@Id
	@SequenceGenerator(name="SQ_ID_RETIREMENT_MARKETFACT_PK", sequenceName="SQ_ID_RETIREMENT_MARKETFACT_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_RETIREMENT_MARKETFACT_PK")
	@Column(name="ID_RETIREMENT_MARKETFACT_PK")
	private Long idRetirementMarketfactPk;
	
	/** The market date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MARKET_DATE")
	private Date marketDate;
	
	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	/** The market price. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	/** The operation quantity. */
	@Column(name="OPERATION_QUANTITY")
	private BigDecimal operationQuantity;
	
	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The retirement detail. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RETIREMENT_DETAIL_FK")
	private RetirementDetail retirementDetail;
	
	/** The retirement detail. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RETIREMENT_BLOCK_DET_FK")
	private RetirementBlockDetail retirementBlockDetail;
	

	public RetirementMarketfact(){
		
	}
	
	/**
	 * Gets the id retirement marketfact pk.
	 *
	 * @return the id retirement marketfact pk
	 */
	public Long getIdRetirementMarketfactPk() {
		return idRetirementMarketfactPk;
	}

	/**
	 * Sets the id retirement marketfact pk.
	 *
	 * @param idRetirementMarketfactPk the new id retirement marketfact pk
	 */
	public void setIdRetirementMarketfactPk(Long idRetirementMarketfactPk) {
		this.idRetirementMarketfactPk = idRetirementMarketfactPk;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the operation quantity.
	 *
	 * @return the operation quantity
	 */
	public BigDecimal getOperationQuantity() {
		return operationQuantity;
	}

	/**
	 * Sets the operation quantity.
	 *
	 * @param operationQuantity the new operation quantity
	 */
	public void setOperationQuantity(BigDecimal operationQuantity) {
		this.operationQuantity = operationQuantity;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the retirement detail.
	 *
	 * @return the retirement detail
	 */
	public RetirementDetail getRetirementDetail() {
		return retirementDetail;
	}

	/**
	 * Sets the retirement detail.
	 *
	 * @param retirementDetail the new retirement detail
	 */
	public void setRetirementDetail(RetirementDetail retirementDetail) {
		this.retirementDetail = retirementDetail;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser!=null){
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public RetirementBlockDetail getRetirementBlockDetail() {
		return retirementBlockDetail;
	}

	public void setRetirementBlockDetail(RetirementBlockDetail retirementBlockDetail) {
		this.retirementBlockDetail = retirementBlockDetail;
	}

}
