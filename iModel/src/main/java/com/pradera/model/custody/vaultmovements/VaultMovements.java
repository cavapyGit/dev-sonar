package com.pradera.model.custody.vaultmovements;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.physical.PhysicalCertificate;

@Entity
@Table(name="VAULT_MOVEMENTS")
public class VaultMovements implements Serializable, Auditable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="VAULT_MOVEMENTS_IDVAULTMOVEMENTSPK_GENERATOR", sequenceName="SQ_ID_VAULT_MOVEMENTS",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VAULT_MOVEMENTS_IDVAULTMOVEMENTSPK_GENERATOR")
	@Column(name="ID_VAULT_MOVEMENTS_PK")
	private Long idVaultMovementsPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PHYSICAL_CERTIFICATE_FK")
	private PhysicalCertificate physicalCertificate;
	
	@Column(name="MOVEMENT_TYPE")
	private Integer movementType;
	
	@Column(name="VAULT")
	private Integer vault;
	
	@Column(name="BRANCH_OFFICE")//EN QUE BOVEDA ESTA
	private Integer branchOffice;
	
	@Column(name="MOVEMENT_QUANTITY")
	private BigDecimal movementQuantity;
	
	/** The movement date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/**
	 * @return the idVaultMovementsPk
	 */
	public Long getIdVaultMovementsPk() {
		return idVaultMovementsPk;
	}
	/**
	 * @param idVaultMovementsPk the idVaultMovementsPk to set
	 */
	public void setIdVaultMovementsPk(Long idVaultMovementsPk) {
		this.idVaultMovementsPk = idVaultMovementsPk;
	}
	/**
	 * @return the movementType
	 */
	public Integer getMovementType() {
		return movementType;
	}
	/**
	 * @param movementType the movementType to set
	 */
	public void setMovementType(Integer movementType) {
		this.movementType = movementType;
	}
	/**
	 * @return the vault
	 */
	public Integer getVault() {
		return vault;
	}
	/**
	 * @param vault the vault to set
	 */
	public void setVault(Integer vault) {
		this.vault = vault;
	}
	/**
	 * @return the branchOffice
	 */
	public Integer getBranchOffice() {
		return branchOffice;
	}
	/**
	 * @param branchOffice the branchOffice to set
	 */
	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}
	/**
	 * @return the movementDate
	 */
	public Date getMovementDate() {
		return movementDate;
	}
	/**
	 * @param movementDate the movementDate to set
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}	
	public BigDecimal getMovementQuantity() {
		return movementQuantity;
	}
	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}
	
	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}
	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null){
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
