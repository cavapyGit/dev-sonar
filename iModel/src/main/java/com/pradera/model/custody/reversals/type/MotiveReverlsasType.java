package com.pradera.model.custody.reversals.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum MotiveReverlsasType {
	NORMAL(new Integer(103)),
	EJECUTION(new Integer(707)),
	ASFI_REQUEST(new Integer(1858)),
	INJUNCTION(new Integer(1859)),
	OTHERS(new Integer(1860));
	
	private Integer code;
	
	private MotiveReverlsasType(Integer code){
		this.code = code;
	}

	public static final List<MotiveReverlsasType> list = new ArrayList<MotiveReverlsasType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, MotiveReverlsasType> lookup = new HashMap<Integer, MotiveReverlsasType>();
	
	static {
		for (MotiveReverlsasType s : EnumSet.allOf(MotiveReverlsasType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static MotiveReverlsasType get(Integer code) {
		return lookup.get(code);
	}
	
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	
}
