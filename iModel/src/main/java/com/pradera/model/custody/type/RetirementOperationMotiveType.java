package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RetirementOperationMotiveType {
	REDEMPTION_FIXED_INCOME(new Integer(667),"REDENCION RF"),
	REDEMPTION_VARIABLE_INCOME(new Integer(668),"REDENCION RV"),
	//RESCUE(new Integer(1317),"RESCATE");
	DIRECT_SALE_BCB_REDEMPTION(new Integer(1317), "REDENCION ANTICIPADA VD BIC"),
	DIRECT_SALE_TGN_REDEMPTION(new Integer(1318), "REDENCION ANTICIPADA VD TGN"),
	
	REVERSION(new Integer(2173),"RETIRO DE VALORES FISICOS"),
	RETIREMENT_EXPIRATION_CUPON(new Integer(2830), "RETIRO POR VENCIMIENTO DE CUPON"),
	RETIREMENT_OTC(new Integer(2831), "RETIRO POR OTC"),
	RETIREMENT_GUARD_EXCLUSIVE(new Integer(2852), "RETIRO GUADRA EXCLUSIVA"),
	REVERSION_RV(new Integer(2854),"RETIRO DE VALORES RV");
	
	private Integer code;
	private String value;
	public static final List<RetirementOperationMotiveType> list = new ArrayList<RetirementOperationMotiveType>();
	
	public static final Map<Integer, RetirementOperationMotiveType> lookup = new HashMap<Integer, RetirementOperationMotiveType>();
	public static List<RetirementOperationMotiveType> listSomeElements(RetirementOperationMotiveType... transferSecuritiesTypeParams){
		List<RetirementOperationMotiveType> retorno = new ArrayList<RetirementOperationMotiveType>();
		for(RetirementOperationMotiveType removeSecurityMotiveType: transferSecuritiesTypeParams){
			retorno.add(removeSecurityMotiveType);
		}
		return retorno;
	}
	static {
		for (RetirementOperationMotiveType s : EnumSet.allOf(RetirementOperationMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public static RetirementOperationMotiveType get(Integer code){
		return lookup.get(code);
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private RetirementOperationMotiveType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
