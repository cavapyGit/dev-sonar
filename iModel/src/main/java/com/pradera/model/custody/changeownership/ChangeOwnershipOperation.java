package com.pradera.model.custody.changeownership;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.transfersecurities.CustodyOperation;


/**
 * The persistent class for the CHANGE_OWNERSHIP_OPERATION database table.
 * 
 */
@Entity
@Table(name="CHANGE_OWNERSHIP_OPERATION")
@NamedQueries({
	@NamedQuery(name = ChangeOwnershipOperation.OPERATION_STATE, 
				query = "Select new ChangeOwnershipOperation(op.idChangeOwnershipPk,op.state) From ChangeOwnershipOperation op WHERE op.idChangeOwnershipPk = :idChangeOwnershipPkParam")
})
public class ChangeOwnershipOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	public static final String OPERATION_STATE = "findState";
	
	@Id
	private Long idChangeOwnershipPk;
	
	@Version
    private Long version;
	
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_CHANGE_OWNERSHIP_PK")
	private CustodyOperation custodyOperation;

	//bi-directional many-to-one association to sourceHolder
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOURCE_HOLDER_ACCOUNT_FK")
	private HolderAccount sourceHolderAccount;

    //bi-directional many-to-one association to sourceParticipant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOURCE_PARTICIPANT_FK")
	private Participant sourceParticipant;
    
    //bi-directional many-to-one association to targetParticipant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_PARTICIPANT_FK")
	private Participant targetParticipant;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MOTIVE")
	private Integer motive;

	@Column(name="PROCESSING_TYPE")
	private Integer processingType;

	@Column(name="STATE")
	private Integer state;
	
	@Column(name="BALANCE_TYPE")
	private Integer balanceType;
	
	/** The comments. */
	@Column(name="COMMENTS")
	private String comments;

	//bi-directional many-to-one association to ChangeOwnershipDetail
	@OneToMany(mappedBy="changeOwnershipOperation",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<ChangeOwnershipDetail> changeOwnershipDetails;

	//bi-directional many-to-one association to ChangeOwnershipFile
	@OneToMany(mappedBy="changeOwnershipOperation",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<ChangeOwnershipFile> changeOwnershipFiles;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANGE_OWNERSHIP_FK")
	private ChangeOwnershipOperation changeOwnershipOperationRef;
	
	@Column(name = "IND_LAST_REQUEST")
	private Integer indLastRequest;
	
	@Column(name = "IND_EXCHANGE_SECURITIES")
	private Integer indExchangeSecurities;
	
	@Transient
	private boolean regularizationIndicator;
	
	@Transient
	private String motiveDescription;
	
	@Transient
	private String rejectMotiveDescription;
	
	@Transient
	private String otherRejectMotiveDescription;
	
	@Transient
	private String balanceTypeDescription;
	
	public ChangeOwnershipOperation() {

    }
	
    public ChangeOwnershipOperation(Long idChangeOwnershipPk, Integer state) {
		super();
		this.idChangeOwnershipPk = idChangeOwnershipPk;
		this.state = state;
	}

	/**
	 * @return the motiveDescription
	 */
	public String getMotiveDescription() {
		return motiveDescription;
	}

	/**
	 * @param motiveDescription the motiveDescription to set
	 */
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}

	/**
	 * @return the custodyOperation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * @param custodyOperation the custodyOperation to set
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * @return the sourceHolderAccount
	 */
	public HolderAccount getSourceHolderAccount() {
		return sourceHolderAccount;
	}

	/**
	 * @param sourceHolderAccount the sourceHolderAccount to set
	 */
	public void setSourceHolderAccount(HolderAccount sourceHolderAccount) {
		this.sourceHolderAccount = sourceHolderAccount;
	}

	/**
	 * @return the sourceParticipant
	 */
	public Participant getSourceParticipant() {
		return sourceParticipant;
	}

	/**
	 * @param sourceParticipant the sourceParticipant to set
	 */
	public void setSourceParticipant(Participant sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}

	/**
	 * @return the targetParticipant
	 */
	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	/**
	 * @param targetParticipant the targetParticipant to set
	 */
	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}


	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public List<ChangeOwnershipDetail> getChangeOwnershipDetails() {
		return changeOwnershipDetails;
	}

	public void setChangeOwnershipDetails(
			List<ChangeOwnershipDetail> changeOwnershipDetails) {
		this.changeOwnershipDetails = changeOwnershipDetails;
	}

	/**
	 * @return the changeOwnershipFiles
	 */
	public List<ChangeOwnershipFile> getChangeOwnershipFiles() {
		return changeOwnershipFiles;
	}

	/**
	 * @param changeOwnershipFiles the changeOwnershipFiles to set
	 */
	public void setChangeOwnershipFiles(
			List<ChangeOwnershipFile> changeOwnershipFiles) {
		this.changeOwnershipFiles = changeOwnershipFiles;
	}

	public Long getIdChangeOwnershipPk() {
		return idChangeOwnershipPk;
	}

	public void setIdChangeOwnershipPk(Long idChangeOwnershipPk) {
		this.idChangeOwnershipPk = idChangeOwnershipPk;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public Integer getProcessingType() {
		return processingType;
	}

	public void setProcessingType(Integer processingType) {
		this.processingType = processingType;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(Integer balanceType) {
		this.balanceType = balanceType;
	}
	
//	public List<ChangeOwnershipDetail> getChangeOwnershipDetailsAsList(){
//		List<ChangeOwnershipDetail> returnList = new ArrayList<ChangeOwnershipDetail>();
//		returnList.addAll(changeOwnershipDetails);
//		return returnList;
//	}

	public Integer getIndExchangeSecurities() {
		return indExchangeSecurities;
	}

	public void setIndExchangeSecurities(Integer indExchangeSecurities) {
		this.indExchangeSecurities = indExchangeSecurities;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(custodyOperation!=null){
    			custodyOperation.setLastModifyApp(lastModifyApp);
    			custodyOperation.setLastModifyDate(lastModifyDate);
    			custodyOperation.setLastModifyIp(lastModifyIp);
    			custodyOperation.setLastModifyUser(lastModifyUser);
    		}
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap =
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("changeOwnershipDetails", changeOwnershipDetails);
        detailsMap.put("changeOwnershipFiles", changeOwnershipFiles);
        return detailsMap;

	}

	public Boolean getRegularizationIndicator() {
		return regularizationIndicator;
	}

	public void setRegularizationIndicator(boolean regularizationIndicator) {
		this.regularizationIndicator = regularizationIndicator;
	}

	public ChangeOwnershipOperation getChangeOwnershipOperationRef() {
		return changeOwnershipOperationRef;
	}

	public void setChangeOwnershipOperationRef(
			ChangeOwnershipOperation changeOwnershipOperationRef) {
		this.changeOwnershipOperationRef = changeOwnershipOperationRef;
	}

	public Integer getIndLastRequest() {
		return indLastRequest;
	}

	public void setIndLastRequest(Integer indLastRequest) {
		this.indLastRequest = indLastRequest;
	}

	/**
	 * @return the balanceTypeDescription
	 */
	public String getBalanceTypeDescription() {
		return balanceTypeDescription;
	}

	/**
	 * @param balanceTypeDescription the balanceTypeDescription to set
	 */
	public void setBalanceTypeDescription(String balanceTypeDescription) {
		this.balanceTypeDescription = balanceTypeDescription;
	}

	/**
	 * @return the rejectMotiveDescription
	 */
	public String getRejectMotiveDescription() {
		return rejectMotiveDescription;
	}

	/**
	 * @param rejectMotiveDescription the rejectMotiveDescription to set
	 */
	public void setRejectMotiveDescription(String rejectMotiveDescription) {
		this.rejectMotiveDescription = rejectMotiveDescription;
	}

	/**
	 * @return the otherRejectMotiveDescription
	 */
	public String getOtherRejectMotiveDescription() {
		return otherRejectMotiveDescription;
	}

	/**
	 * @param otherRejectMotiveDescription the otherRejectMotiveDescription to set
	 */
	public void setOtherRejectMotiveDescription(String otherRejectMotiveDescription) {
		this.otherRejectMotiveDescription = otherRejectMotiveDescription;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	
}