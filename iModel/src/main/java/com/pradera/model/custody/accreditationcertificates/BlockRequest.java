package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.blockentity.BlockEntity;

/**
 * The persistent class for the BLOCK_OPERATION database table.
 * 
 */
@Entity
@Table(name = "BLOCK_REQUEST")
public class BlockRequest implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BLOCK_REQUEST_IDBLOCKREQUESTPK_GENERATOR", sequenceName="SQ_ID_BLOCK_REQUEST_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_REQUEST_IDBLOCKREQUESTPK_GENERATOR")	
	@Column(name="ID_BLOCK_REQUEST_PK")
	private Long idBlockRequestPk;	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_ENTITY_FK", referencedColumnName = "ID_BLOCK_ENTITY_PK")	
	private BlockEntity blockEntity;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")	
	private Participant participant;

	@Column(name = "BLOCK_NUMBER")
	private String blockNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "BLOCK_NUMBER_DATE")
	private Date blockNumberDate;

	@Column(name = "BLOCK_TYPE")
	private Integer blockType;

	@Column(name = "BLOCK_FORM")
	private Integer blockForm;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CONFIRMATION_DATE")
	private Date confirmationDate;

	@Column(name = "CONFIRMATION_USER")
	private String confirmationUser;

	@Column(name="BLOCK_REQUEST_STATE")
	private Integer blockRequestState;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "COURT_DEPOSIT_DATE")
	private Date courtDepositDate;

	@Column(name = "CURRENCY")
	private Integer currency;

	@Column(name = "CURRENT_BLOCK_AMOUNT")
	private BigDecimal currentBlockAmount;

	@Column(name = "EXCHANGE_RATE")
	private BigDecimal exchangeRate;

	@Column(name = "IND_ISSUE_REQUEST")
	private Integer indIssueRequest;

	@Column(name = "IND_REQUEST_DECLARATION")
	private Integer indRequestDeclaration;

	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "NOTIFICATION_DATE")
	private Date notificationDate;

	@Column(name = "ORIGIN_BLOCK_AMOUNT")
	private BigDecimal originBlockAmount;

	@Column(name = "PENDING_BLOCK_AMOUNT")
	private BigDecimal pendingBlockAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTRY_DATE")
	private Date registryDate;

	@Column(name = "REGISTRY_USER")
	private String registryUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REJECT_DATE")
	private Date rejectDate;

	@Column(name = "REJECT_USER")
	private String rejectUser;

	@Column(name = "VALORIZATION_TYPE")
	private BigDecimal valorizationType;
	
	@Column(name ="OTHER_BLOCK_MOTIVE")
	private String otherBlockMotive;

	// bi-directional many-to-one association to Holder
	@ManyToOne
	@JoinColumn(name = "ID_HOLDER_FK")
	private Holder holder;
	
	@Column(name = "COMMENTS")
	private String comments;

	@Column(name = "APPROVE_USER")
	private String approveUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "APPROVE_DATE")
	private Date approveDate;
	
	@Column(name = "ANNUL_USER")
	private String annulUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ANNUL_DATE")
	private Date annulDate;
	
	@Column(name = "IND_AUTO_SETTLED")
	private Integer indAutoSettled;
	
	@Column(name = "IND_AUTHORITY_RESTRICTED")
	private Integer indAuthorityRestricted;
	
	@Column(name = "CIRCULAR_NUMBER")
	private String circularNumber;
	
	@Column(name = "FISCAL_ORIGIN")
	private String fiscalOrigin;
	
	@Column(name = "FISCAL_CURRENCY")
	private Integer fiscalCurrency;
	
	@Column(name = "FISCAL_AMOUNT")
	private BigDecimal fiscalAmount;
	
	@Column(name = "FISCAL_CHARGE")
	private String fiscalCharge;
	
	@Column(name = "FISCAL_PROCESS")
	private String fiscalProcess;
	
	@Column(name = "FISCAL_AUTHORITY")
	private String fiscalAuthority;
	
	// bi-directional many-to-one association to BlockOperation
	@OneToMany(mappedBy = "blockRequest", cascade=CascadeType.ALL)
	private List<BlockOperation> blockOperation;
	
	@OneToMany(mappedBy = "blockRequest", cascade=CascadeType.ALL)
	private List<BlockRequestFile> blockRequestFile;
	
	@Transient
	private String blockTypeDescription;
	
	@Transient
	private String blockFormDescription;
	
	@Transient
	private String blockStateDescription;
	
	public BlockRequest() {
	}
	
	public String getBlockTypeDescription() {
		return blockTypeDescription;
	}

	public void setBlockTypeDescription(String blockTypeDescription) {
		this.blockTypeDescription = blockTypeDescription;
	}

	public BlockEntity getBlockEntity() {
		return blockEntity;
	}

	public void setBlockEntity(BlockEntity blockEntity) {
		this.blockEntity = blockEntity;
	}

	public String getBlockNumber() {
		return this.blockNumber;
	}

	public void setBlockNumber(String blockNumber) {
		this.blockNumber = blockNumber;
	}

	public Date getBlockNumberDate() {
		return this.blockNumberDate;
	}

	public void setBlockNumberDate(Date blockNumberDate) {
		this.blockNumberDate = blockNumberDate;
	}

	public Integer getBlockType() {
		return this.blockType;
	}

	public void setBlockType(Integer blockType) {
		this.blockType = blockType;
	}

	public Date getConfirmationDate() {
		return this.confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getConfirmationUser() {
		return this.confirmationUser;
	}

	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	public Date getCourtDepositDate() {
		return this.courtDepositDate;
	}

	public void setCourtDepositDate(Date courtDepositDate) {
		this.courtDepositDate = courtDepositDate;
	}

	public Integer getCurrency() {
		return this.currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public BigDecimal getCurrentBlockAmount() {
		return this.currentBlockAmount;
	}

	public void setCurrentBlockAmount(BigDecimal currentBlockAmount) {
		this.currentBlockAmount = currentBlockAmount;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	//
	// public BigDecimal getIdCustodyOperationPk() {
	// return this.idCustodyOperationPk;
	// }
	//
	// public void setIdCustodyOperationPk(BigDecimal idCustodyOperationPk) {
	// this.idCustodyOperationPk = idCustodyOperationPk;
	// }

	public Integer getIndIssueRequest() {
		return this.indIssueRequest;
	}

	public void setIndIssueRequest(Integer indIssueRequest) {
		this.indIssueRequest = indIssueRequest;
	}

	public Integer getIndRequestDeclaration() {
		return this.indRequestDeclaration;
	}

	public void setIndRequestDeclaration(Integer indRequestDeclaration) {
		this.indRequestDeclaration = indRequestDeclaration;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getNotificationDate() {
		return this.notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public BigDecimal getOriginBlockAmount() {
		return this.originBlockAmount;
	}

	public void setOriginBlockAmount(BigDecimal originBlockAmount) {
		this.originBlockAmount = originBlockAmount;
	}

	public BigDecimal getPendingBlockAmount() {
		return this.pendingBlockAmount;
	}

	public void setPendingBlockAmount(BigDecimal pendingBlockAmount) {
		this.pendingBlockAmount = pendingBlockAmount;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public BigDecimal getValorizationType() {
		return this.valorizationType;
	}

	public void setValorizationType(BigDecimal valorizationType) {
		this.valorizationType = valorizationType;
	}

	public Holder getHolder() {
		return this.holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public List<BlockOperation> getBlockOperation() {
		return this.blockOperation;
	}

	public void setBlockOperation(List<BlockOperation> blockOperation) {
		this.blockOperation = blockOperation;
	}
	
	public Integer getBlockForm() {
		return blockForm;
	}

	public void setBlockForm(Integer blockForm) {
		this.blockForm = blockForm;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	

	public Integer getBlockRequestState() {
		return blockRequestState;
	}

	public void setBlockRequestState(Integer blockRequestState) {
		this.blockRequestState = blockRequestState;
	}

	public Long getIdBlockRequestPk() {
		return idBlockRequestPk;
	}

	public void setIdBlockRequestPk(Long idBlockRequestPk) {
		this.idBlockRequestPk = idBlockRequestPk;
	}

	
	public List<BlockRequestFile> getBlockRequestFile() {
		return blockRequestFile;
	}

	public void setBlockRequestFile(List<BlockRequestFile> blockRequestFile) {
		this.blockRequestFile = blockRequestFile;
	}

	public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	@Override
    public Map<String, List<? extends Auditable>> getListForAudit() {
        HashMap<String,List<? extends Auditable>> detailsMap =
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("blockOperation", blockOperation);
        detailsMap.put("blockRequestFile", blockRequestFile);
        return detailsMap;
    }

	public String getOtherBlockMotive() {
		return otherBlockMotive;
	}

	public void setOtherBlockMotive(String otherBlockMotive) {
		this.otherBlockMotive = otherBlockMotive;
	}

	public String getBlockFormDescription() {
		return blockFormDescription;
	}

	public void setBlockFormDescription(String blockFormDescription) {
		this.blockFormDescription = blockFormDescription;
	}

	public String getBlockStateDescription() {
		return blockStateDescription;
	}

	public void setBlockStateDescription(String blockStateDescription) {
		this.blockStateDescription = blockStateDescription;
	}

	public String getApproveUser() {
		return approveUser;
	}

	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getAnnulUser() {
		return annulUser;
	}

	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}

	public Date getAnnulDate() {
		return annulDate;
	}

	public void setAnnulDate(Date annulDate) {
		this.annulDate = annulDate;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idBlockRequestPk == null) ? 0 : idBlockRequestPk.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockRequest other = (BlockRequest) obj;
		if (idBlockRequestPk == null) {
			if (other.idBlockRequestPk != null)
				return false;
		} else if (!idBlockRequestPk.equals(other.idBlockRequestPk))
			return false;
		return true;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String observations) {
		this.comments = observations;
	}

	public Integer getIndAutoSettled() {
		return indAutoSettled;
	}

	public void setIndAutoSettled(Integer indAutoSettled) {
		this.indAutoSettled = indAutoSettled;
	}

	public Integer getIndAuthorityRestricted() {
		return indAuthorityRestricted;
	}

	public void setIndAuthorityRestricted(Integer indAuthorityRestricted) {
		this.indAuthorityRestricted = indAuthorityRestricted;
	}

	public String getCircularNumber() {
		return circularNumber;
	}

	public void setCircularNumber(String circularNumber) {
		this.circularNumber = circularNumber;
	}

	public String getFiscalOrigin() {
		return fiscalOrigin;
	}

	public void setFiscalOrigin(String fiscalOrigin) {
		this.fiscalOrigin = fiscalOrigin;
	}

	public Integer getFiscalCurrency() {
		return fiscalCurrency;
	}

	public void setFiscalCurrency(Integer fiscalCurrency) {
		this.fiscalCurrency = fiscalCurrency;
	}

	public BigDecimal getFiscalAmount() {
		return fiscalAmount;
	}

	public void setFiscalAmount(BigDecimal fiscalAmount) {
		this.fiscalAmount = fiscalAmount;
	}

	public String getFiscalCharge() {
		return fiscalCharge;
	}

	public void setFiscalCharge(String fiscalCharge) {
		this.fiscalCharge = fiscalCharge;
	}

	public String getFiscalProcess() {
		return fiscalProcess;
	}

	public void setFiscalProcess(String fiscalProcess) {
		this.fiscalProcess = fiscalProcess;
	}

	public String getFiscalAuthority() {
		return fiscalAuthority;
	}

	public void setFiscalAuthority(String fiscalAuthority) {
		this.fiscalAuthority = fiscalAuthority;
	}

}