package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/03/2013
 */
public enum BlockEntityStateType {
	
	/** The registered. */
	ACTIVATED(Integer.valueOf(814),"ACTIVADO"),
	
	/** The blocked. */
	OFF(Integer.valueOf(815),"DESACTIVADO");	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new holder state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BlockEntityStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the holder state type
	 */
	public static BlockEntityStateType get(Integer code) {
		return lookup.get(code);
	}

	/** The Constant list. */
	public static final List<BlockEntityStateType> list = new ArrayList<BlockEntityStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BlockEntityStateType> lookup = new HashMap<Integer, BlockEntityStateType>();
	static {
		for (BlockEntityStateType s : EnumSet.allOf(BlockEntityStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}