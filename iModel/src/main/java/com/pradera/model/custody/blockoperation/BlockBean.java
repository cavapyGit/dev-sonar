package com.pradera.model.custody.blockoperation;

import java.io.Serializable;

public class BlockBean implements Serializable{

	Integer idSecurities;
	String securitiesCode;
	String securitiesName;
	Double accountBalance;
	Double availableBalance;
	Double blockedBalance;
	Double reportantBalance;
	Double quantityBlock;
	Double blockFirstRange;
	Double reBlock;
	private boolean selected;
	
	
	
	public BlockBean(Integer idSecurities, String securitiesCode,
			String securitiesName, Double accountBalance,
			Double availableBalance, Double blockedBalance,
			Double reportantBalance, Double quantityBlock,
			Double blockFirstRange, Double reBlock) {
		super();
		this.idSecurities = idSecurities;
		this.securitiesCode = securitiesCode;
		this.securitiesName = securitiesName;
		this.accountBalance = accountBalance;
		this.availableBalance = availableBalance;
		this.blockedBalance = blockedBalance;
		this.reportantBalance = reportantBalance;
		this.quantityBlock = quantityBlock;
		this.blockFirstRange = blockFirstRange;
		this.reBlock = reBlock;
	}
	public Integer getIdSecurities() {
		return idSecurities;
	}
	public void setIdSecurities(Integer idSecurities) {
		this.idSecurities = idSecurities;
	}
	public String getSecuritiesName() {
		return securitiesName;
	}
	public void setSecuritiesName(String securitiesName) {
		this.securitiesName = securitiesName;
	}
	public Double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(Double accountBalance) {
		this.accountBalance = accountBalance;
	}
	public Double getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(Double availableBalance) {
		this.availableBalance = availableBalance;
	}
	public Double getBlockedBalance() {
		return blockedBalance;
	}
	public void setBlockedBalance(Double blockedBalance) {
		this.blockedBalance = blockedBalance;
	}
	public Double getReportantBalance() {
		return reportantBalance;
	}
	public void setReportantBalance(Double reportantBalance) {
		this.reportantBalance = reportantBalance;
	}
	public Double getQuantityBlock() {
		return quantityBlock;
	}
	public void setQuantityBlock(Double quantityBlock) {
		this.quantityBlock = quantityBlock;
	}
	public Double getBlockFirstRange() {
		return blockFirstRange;
	}
	public void setBlockFirstRange(Double blockFirstRange) {
		this.blockFirstRange = blockFirstRange;
	}
	public Double getReBlock() {
		return reBlock;
	}
	public void setReBlock(Double reBlock) {
		this.reBlock = reBlock;
	}
	public String getSecuritiesCode() {
		return securitiesCode;
	}
	public void setSecuritiesCode(String securitiesCode) {
		this.securitiesCode = securitiesCode;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
	
	
}
