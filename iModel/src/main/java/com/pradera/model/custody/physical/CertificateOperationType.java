package com.pradera.model.custody.physical;

public enum CertificateOperationType {
	TRANSFER(new Integer(2360)),
	GLOBALIZE(new Integer(2357)),
	FRACTION(new Integer(2358)),
	REPLACE(new Integer(2359)),
	;	
	
	private CertificateOperationType(Integer code){
		this.code = code;
	} 
	
	private Integer code;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
