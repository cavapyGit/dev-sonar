package com.pradera.model.custody.transfersecurities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class CustodyOperation.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/04/2013
 */
@Entity
@Table(name="CUSTODY_OPERATION")
public class CustodyOperation implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id custody operation pk. */
	@Id
	@SequenceGenerator(name="CUSTODY_OPERATION_IDCUSTODYOPERATIONPK_GENERATOR", sequenceName="SQ_ID_CUSTODY_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CUSTODY_OPERATION_IDCUSTODYOPERATIONPK_GENERATOR")
	@Column(name="ID_CUSTODY_OPERATION_PK")
	private Long idCustodyOperationPk;
	
    /** The approval date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="APPROVAL_DATE")
	private Date approvalDate;

	/** The approval user. */
	@Column(name="APPROVAL_USER")
	private String approvalUser;
	
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="ANNULATION_DATE")
	private Date annulationDate;

	@Column(name="ANNULATION_USER")
	private String annulationUser;
	
	/** The application date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="APPLICATION_DATE")
	private Date applicationDate;
	
	/** The application user. */
	@Column(name="APPLICATION_USER")
	private String applicationUser;
	
	/** The authorization date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="AUTHORIZATION_DATE")
	private Date authorizationDate;
	
	/** The authorization user. */
	@Column(name="AUTHORIZATION_USER")
	private String authorizationUser;

    /** The confirm date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;

	/** The confirm user. */
	@Column(name="CONFIRM_USER")
	private String confirmUser;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The operation date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;
    
    /** The operation date. */
    @Temporal( TemporalType.DATE)
	@Transient
	private Date operationInit;
    
    /** The operation date. */
    @Temporal( TemporalType.DATE)
	@Transient
	private Date operationEnd;

	/** The operation number. */
	@Column(name="OPERATION_NUMBER")
	private Long operationNumber;

	/** The operation type. */
	@Column(name="OPERATION_TYPE")
	private Long operationType;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

    /** The reject date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REJECT_DATE")
	private Date rejectDate;

    /** The reject motive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;

	/** The reject motive other. */
	@Column(name="REJECT_MOTIVE_OTHER")
	private String rejectMotiveOther;

	/** The reject user. */
	@Column(name="REJECT_USER")
	private String rejectUser;

    /** The review date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REVIEW_DATE")
	private Date reviewDate;

	/** The review user. */
	@Column(name="REVIEW_USER")
	private String reviewUser;

	/** The state. */
	@Column(name="OPERATION_STATE")
	private Integer state;
	
	/** The ind rectification. */
	@Column(name="IND_RECTIFICATION")
	private Integer indRectification;
	
	/** The ind for send webservice. */
	@Column(name="IND_WEBSERVICE")
	private Integer indWebservice;
	
	@Transient
	private String rejectMotiveDesc;


	
    /**
     * Instantiates a new custody operation.
     */
    public CustodyOperation() {
    	indRectification = ComponentConstant.ZERO;
    	indWebservice = ComponentConstant.ZERO;
    }
    
    /**
     * Instantiates a new custody operation.
     * @param idCustodyOperationPk the new id custody operation pk
     */
    public CustodyOperation(Long idCustodyOperationPk){
    	super();
    	this.idCustodyOperationPk=idCustodyOperationPk;
    }

	/**
	 * Gets the id custody operation pk.
	 *
	 * @return the id custody operation pk
	 */
	public Long getIdCustodyOperationPk() {
		return this.idCustodyOperationPk;
	}

	/**
	 * Sets the id custody operation pk.
	 *
	 * @param idCustodyOperationPk the new id custody operation pk
	 */
	public void setIdCustodyOperationPk(Long idCustodyOperationPk) {
		this.idCustodyOperationPk = idCustodyOperationPk;
	}

	/**
	 * Gets the approval date.
	 *
	 * @return the approval date
	 */
	public Date getApprovalDate() {
		return this.approvalDate;
	}

	/**
	 * Sets the approval date.
	 *
	 * @param approvalDate the new approval date
	 */
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	/**
	 * Gets the approval user.
	 *
	 * @return the approval user
	 */
	public String getApprovalUser() {
		return this.approvalUser;
	}

	/**
	 * Sets the approval user.
	 *
	 * @param approvalUser the new approval user
	 */
	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the confirm user.
	 *
	 * @return the confirm user
	 */
	public String getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the new confirm user
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return this.operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return this.operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Long getOperationType() {
		return this.operationType;
	}

	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the reject date.
	 *
	 * @return the reject date
	 */
	public Date getRejectDate() {
		return this.rejectDate;
	}

	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the new reject date
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return this.rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject motive other.
	 *
	 * @return the reject motive other
	 */
	public String getRejectMotiveOther() {
		return this.rejectMotiveOther;
	}

	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the new reject motive other
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * Gets the reject user.
	 *
	 * @return the reject user
	 */
	public String getRejectUser() {
		return this.rejectUser;
	}

	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the new reject user
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	/**
	 * Gets the review date.
	 *
	 * @return the review date
	 */
	public Date getReviewDate() {
		return this.reviewDate;
	}

	/**
	 * Sets the review date.
	 *
	 * @param reviewDate the new review date
	 */
	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	/**
	 * Gets the review user.
	 *
	 * @return the review user
	 */
	public String getReviewUser() {
		return this.reviewUser;
	}

	/**
	 * Sets the review user.
	 *
	 * @param reviewUser the new review user
	 */
	public void setReviewUser(String reviewUser) {
		this.reviewUser = reviewUser;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	
	/**
	 * Gets the application date.
	 *
	 * @return the application date
	 */
	public Date getApplicationDate() {
		return applicationDate;
	}

	/**
	 * Sets the application date.
	 *
	 * @param applicationDate the new application date
	 */
	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * Gets the application user.
	 *
	 * @return the application user
	 */
	public String getApplicationUser() {
		return applicationUser;
	}

	/**
	 * Sets the application user.
	 *
	 * @param applicationUser the new application user
	 */
	public void setApplicationUser(String applicationUser) {
		this.applicationUser = applicationUser;
	}

	/**
	 * Gets the authorization date.
	 *
	 * @return the authorization date
	 */
	public Date getAuthorizationDate() {
		return authorizationDate;
	}

	/**
	 * Sets the authorization date.
	 *
	 * @param authorizationDate the new authorization date
	 */
	public void setAuthorizationDate(Date authorizationDate) {
		this.authorizationDate = authorizationDate;
	}

	/**
	 * Gets the authorization user.
	 *
	 * @return the authorization user
	 */
	public String getAuthorizationUser() {
		return authorizationUser;
	}

	/**
	 * Sets the authorization user.
	 *
	 * @param authorizationUser the new authorization user
	 */
	public void setAuthorizationUser(String authorizationUser) {
		this.authorizationUser = authorizationUser;
	}

	/**
	 * Gets the ind rectification.
	 *
	 * @return the ind rectification
	 */
	public Integer getIndRectification() {
		return indRectification;
	}

	/**
	 * Sets the ind rectification.
	 *
	 * @param indRectification the new ind rectification
	 */
	public void setIndRectification(Integer indRectification) {
		this.indRectification = indRectification;
	}

	/**
	 * @return the indWebservice
	 */
	public Integer getIndWebservice() {
		return indWebservice;
	}

	/**
	 * @param indWebservice the indWebservice to set
	 */
	public void setIndWebservice(Integer indWebservice) {
		this.indWebservice = indWebservice;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Date getOperationInit() {
		return operationInit;
	}

	public void setOperationInit(Date operationInit) {
		this.operationInit = operationInit;
	}

	public Date getOperationEnd() {
		return operationEnd;
	}

	public void setOperationEnd(Date operationEnd) {
		this.operationEnd = operationEnd;
	}

	public String getRejectMotiveDesc() {
		return rejectMotiveDesc;
	}

	public void setRejectMotiveDesc(String rejectMotiveDesc) {
		this.rejectMotiveDesc = rejectMotiveDesc;
	}

	public Date getAnnulationDate() {
		return annulationDate;
	}

	public void setAnnulationDate(Date annulationDate) {
		this.annulationDate = annulationDate;
	}

	public String getAnnulationUser() {
		return annulationUser;
	}

	public void setAnnulationUser(String annulationUser) {
		this.annulationUser = annulationUser;
	}

	@Override
	public String toString() {
		return "CustodyOperation [idCustodyOperationPk=" + idCustodyOperationPk + ", approvalDate=" + approvalDate
				+ ", approvalUser=" + approvalUser + ", annulationDate=" + annulationDate + ", annulationUser="
				+ annulationUser + ", applicationDate=" + applicationDate + ", applicationUser=" + applicationUser
				+ ", authorizationDate=" + authorizationDate + ", authorizationUser=" + authorizationUser
				+ ", confirmDate=" + confirmDate + ", confirmUser=" + confirmUser + ", lastModifyApp=" + lastModifyApp
				+ ", lastModifyDate=" + lastModifyDate + ", lastModifyIp=" + lastModifyIp + ", lastModifyUser="
				+ lastModifyUser + ", operationDate=" + operationDate + ", operationInit=" + operationInit
				+ ", operationEnd=" + operationEnd + ", operationNumber=" + operationNumber + ", operationType="
				+ operationType + ", registryDate=" + registryDate + ", registryUser=" + registryUser + ", rejectDate="
				+ rejectDate + ", rejectMotive=" + rejectMotive + ", rejectMotiveOther=" + rejectMotiveOther
				+ ", rejectUser=" + rejectUser + ", reviewDate=" + reviewDate + ", reviewUser=" + reviewUser
				+ ", state=" + state + ", indRectification=" + indRectification + ", indWebservice=" + indWebservice
				+ ", rejectMotiveDesc=" + rejectMotiveDesc + "]";
	}

}