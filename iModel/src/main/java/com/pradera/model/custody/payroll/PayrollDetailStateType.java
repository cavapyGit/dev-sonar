package com.pradera.model.custody.payroll;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.custody.affectation.type.AffectationStateType;

public enum PayrollDetailStateType {
	GENERATED(new Integer(2715),"GENERADO"),//2337
	RECEIVED(new Integer(2716),"RECIBIDO"),//2338
	MICROFILMACION(new Integer(2720),"MICROFILMACION"),//2340
	ANALISIS2(new Integer(2717),"ANALISIS 2"),//2339	
	ANALISIS1(new Integer(2721),"ANALISIS 1"),//2341
	ANALYZED(new Integer(2701),"ANALIZADO"),//2344	
	VAULT(new Integer(2725),"BOVEDA"),//2352
	ANNULATE(new Integer(2702),"ANULADO"),//2345
	TRANSIT(new Integer(2718),"TRANSITO"),//2353
	DELIVERED(new Integer(2719),"ENVIADO"),//2354
	REJECT(new Integer(2845),"RECHAZADO"),//2354
	;

	private Integer code;
	private String value;
	
	private PayrollDetailStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/** The Constant list. */
	public static final List<PayrollDetailStateType> list = new ArrayList<PayrollDetailStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, PayrollDetailStateType> lookup = new HashMap<Integer, PayrollDetailStateType>();
	
	static {
		for (PayrollDetailStateType s : EnumSet.allOf(PayrollDetailStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
	

	public static PayrollDetailStateType get(Integer code) {
		return lookup.get(code);
	}
	
}
