package com.pradera.model.custody.affectation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AffectationStateType estado de las solicitudes de afectacion.
 */
public enum RequestAffectationStateType {
	
	/** The registered. */
	REGISTERED(new Integer(1121)),
	
	APPROVED(new Integer(2178)),
	
	ANNULED(new Integer(2179)),
	
	/** The confirmed. */
	CONFIRMED(new Integer(1122)),
	
	/** The rejected. */
	REJECTED(new Integer(1123));
	
	/** The code. */
	private Integer code;
	
	
	/**
	 * Instantiates a new affectation state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private RequestAffectationStateType(Integer code){
		this.code = code;
	}
		
	/** The Constant list. */
	public static final List<RequestAffectationStateType> list = new ArrayList<RequestAffectationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RequestAffectationStateType> lookup = new HashMap<Integer, RequestAffectationStateType>();
	
	static {
		for (RequestAffectationStateType s : EnumSet.allOf(RequestAffectationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	
		
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the affectation state type
	 */
	public static RequestAffectationStateType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}
