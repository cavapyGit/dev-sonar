package com.pradera.model.custody.coupongrant;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.MovementType;
import com.pradera.model.funds.CashAccountMovement;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the GUARANTEE_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="COUPON_GRANT_MOVEMENT")
public class CouponGrantMovement implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="COUPON_GRANT_MOVEMENT_GENERATOR", sequenceName="SQ_ID_COUPON_GRANT_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COUPON_GRANT_MOVEMENT_GENERATOR")
	@Column(name="ID_COUPON_GRANT_MOVEMENT_PK")
	private Long idCouponGrantMovementPk;

	//bi-directional many-to-one association to HolderAccountMovement
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_MOVEMENT_FK")
	private HolderAccountMovement holderAccountMovement;

    //bi-directional many-to-one association to guaranteeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COUPON_GRANT_OPERATION_FK")
	private CouponGrantOperation couponGrantOperation;

  //bi-directional many-to-one association to guaranteeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COUPON_TRANSFER_OPER_FK")
	private CouponTransferOperation couponTransferOperation;
    
  //bi-directional many-to-one association to guaranteeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_UNBLOCK_GRANT_OPERATION_FK")
	private UnblockGrantOperation unblockGrantOperation;
    
  //bi-directional many-to-one association to sourceHolderAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
    //bi-directional many-to-one association to security
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;
    
	//bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
    
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	@Column(name="QUANTITY_MOVEMENT")
	private BigDecimal movementQuantity;

	//bi-directional many-to-one association to MovementType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

    //bi-directional many-to-one association to programInterestCoupon
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_PROGRAM_INTEREST_FK")
  	private ProgramInterestCoupon programInterestCoupon;
  	
    
    public CouponGrantMovement() {
    }

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMovementDate() {
		return this.movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public BigDecimal getMovementQuantity() {
		return this.movementQuantity;
	}

	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	public HolderAccountMovement getHolderAccountMovement() {
		return this.holderAccountMovement;
	}

	public void setHolderAccountMovement(HolderAccountMovement holderAccountMovement) {
		this.holderAccountMovement = holderAccountMovement;
	}
	
	public MovementType getMovementType() {
		return this.movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	/**
	 * @return the idCouponGrantMovementPk
	 */
	public Long getIdCouponGrantMovementPk() {
		return idCouponGrantMovementPk;
	}

	/**
	 * @param idCouponGrantMovementPk the idCouponGrantMovementPk to set
	 */
	public void setIdCouponGrantMovementPk(Long idCouponGrantMovementPk) {
		this.idCouponGrantMovementPk = idCouponGrantMovementPk;
	}

	/**
	 * @return the couponGrantOperation
	 */
	public CouponGrantOperation getCouponGrantOperation() {
		return couponGrantOperation;
	}

	/**
	 * @param couponGrantOperation the couponGrantOperation to set
	 */
	public void setCouponGrantOperation(CouponGrantOperation couponGrantOperation) {
		this.couponGrantOperation = couponGrantOperation;
	}

	/**
	 * @return the couponTransferOperation
	 */
	public CouponTransferOperation getCouponTransferOperation() {
		return couponTransferOperation;
	}

	/**
	 * @param couponTransferOperation the couponTransferOperation to set
	 */
	public void setCouponTransferOperation(
			CouponTransferOperation couponTransferOperation) {
		this.couponTransferOperation = couponTransferOperation;
	}

	/**
	 * @return the unblockGrantOperation
	 */
	public UnblockGrantOperation getUnblockGrantOperation() {
		return unblockGrantOperation;
	}

	/**
	 * @param unblockGrantOperation the unblockGrantOperation to set
	 */
	public void setUnblockGrantOperation(UnblockGrantOperation unblockGrantOperation) {
		this.unblockGrantOperation = unblockGrantOperation;
	}

	/**
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * @param participant the participant to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * @return the programInterestCoupon
	 */
	public ProgramInterestCoupon getProgramInterestCoupon() {
		return programInterestCoupon;
	}

	/**
	 * @param programInterestCoupon the programInterestCoupon to set
	 */
	public void setProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon) {
		this.programInterestCoupon = programInterestCoupon;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}
}