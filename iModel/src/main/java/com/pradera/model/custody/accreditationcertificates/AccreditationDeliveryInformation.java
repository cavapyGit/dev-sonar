package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;





/**
 * The persistent class for the ACCREDITATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="ACCR_DELIVERY_INFORMATION")
public class AccreditationDeliveryInformation implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCR_IDDELIVERYPK_GENERATOR", sequenceName="SQ_ID_ACCR_DELIVERY_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCR_IDDELIVERYPK_GENERATOR")
	@Column(name="ID_ACCREDITATION_DELIVERY_PK")
	private Long idAccreditationDeliveryPk;

	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	@Column(name="FULL_NAME")
	private String fullName;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to AccreditationOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCREDITATION_OPERATION_FK")
	private AccreditationOperation accreditationOperation;

    public AccreditationDeliveryInformation(){
    	
    }
    
	public AccreditationDeliveryInformation(Long idAccreditationDeliveryPk,
			String documentNumber, String fullName, Integer lastModifyApp,
			Date lastModifyDate, String lastModifyIp, String lastModifyUser,
			AccreditationOperation accreditationOperation) {
		super();
		this.idAccreditationDeliveryPk = idAccreditationDeliveryPk;
		this.documentNumber = documentNumber;
		this.fullName = fullName;
		this.lastModifyApp = lastModifyApp;
		this.lastModifyDate = lastModifyDate;
		this.lastModifyIp = lastModifyIp;
		this.lastModifyUser = lastModifyUser;
		this.accreditationOperation = accreditationOperation;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public AccreditationOperation getAccreditationOperation() {
		return this.accreditationOperation;
	}

	public void setAccreditationOperation(AccreditationOperation accreditationOperation) {
		this.accreditationOperation = accreditationOperation;
	}
	
	public Long getIdAccreditationDeliveryPk() {
		return idAccreditationDeliveryPk;
	}


	public void setIdAccreditationDeliveryPk(Long idAccreditationDeliveryPk) {
		this.idAccreditationDeliveryPk = idAccreditationDeliveryPk;
	}


	public String getDocumentNumber() {
		return documentNumber;
	}


	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();           
        }
	}
	
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

	
}