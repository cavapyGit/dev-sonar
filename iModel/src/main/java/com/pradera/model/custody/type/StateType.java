package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum StateType .
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.0
 */
	public enum StateType {		

		REGISTERED(Integer.valueOf(597), "deposito.state.registered","added.png"),
		CONFIRMED(Integer.valueOf(601), "deposito.state.confirmed","confirmed.png"),
		REJECTED(Integer.valueOf(600), "deposito.state.reject","denied.png"),
		ANNULLED(Integer.valueOf(599), "deposito.state.cancel","annulled.png"),
		APPROVED(Integer.valueOf(598), "deposito.state.aprove","approbar.png"),
		AUTHORIZED(Integer.valueOf(602), "retiro.state.authorize","approbar.png"),
		APPLY(Integer.valueOf(1111), "retiro.state.authorize","approbar.png"),

		//ADDED ONLY FOR CERTIFICATES (NO REQUEST)
		DEPOSITED(Integer.valueOf(605), "deposito.state.deposited",null),
		RETIRED(Integer.valueOf(606), "deposito.state.retired",null),
		
		//ADDED ONLY CERTIFICATE STATES (physical_certificate_operation)
		REGISTERED_CERTIFICATE(Integer.valueOf(1590), "certificate.state.registered",""),
		INPROCESSED_CERTIFICATE(Integer.valueOf(1591), "certificate.state.inprocessed",""),
		CONFIRMED_CERTIFICATE(Integer.valueOf(1592), "certificate.state.confirmed","");
		
		private Integer code;
		private String value;
		private String icon;

		public static final List<StateType> list = new ArrayList<StateType>();
		public static final Map<Integer, StateType> lookup = new HashMap<Integer, StateType>();

		/**
		 * List some elements.
		 *
		 * @param transferSecuritiesTypeParams the transfer securities type params
		 * @return the list
		 */
		public static List<StateType> listSomeElements(StateType... stateType){
			List<StateType> retorno = new ArrayList<StateType>();
			for(StateType staType: stateType){
				retorno.add(staType);
			}
			return retorno;
		}
		
		static {
			for (StateType s : EnumSet.allOf(StateType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		private StateType(Integer code, String value, String icon) {
			this.code = code;
			this.value = value;	
		    this.icon = icon;
		}
		
		public Integer getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}	
		
		public String getIcon() {
			return icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

		public String getDescription(Locale locale) {
			return this.getValue();
		}
		
		public static StateType get(Integer code) {
			return lookup.get(code);
		}

		
}
