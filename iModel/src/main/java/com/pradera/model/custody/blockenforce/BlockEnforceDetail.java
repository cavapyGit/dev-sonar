package com.pradera.model.custody.blockenforce;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.blockoperation.UnblockOperation;


/**
 * The persistent class for the BLOCK_ENFORCE_DETAIL database table.
 * 
 */
@Entity
@Table(name="BLOCK_ENFORCE_DETAIL")
@NamedQuery(name="BlockEnforceDetail.findAll", query="SELECT b FROM BlockEnforceDetail b")
public class BlockEnforceDetail implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BLOCK_ENFORCE_DETAIL_IDENFORCEDETAILPK_GENERATOR", sequenceName="SQ_ID_ENFORCE_DETAIL_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_ENFORCE_DETAIL_IDENFORCEDETAILPK_GENERATOR")
	@Column(name="ID_ENFORCE_DETAIL_PK")
	private Long idEnforceDetailPk;

	@Column(name="BLOCK_QUANTITY")
	private BigDecimal blockQuantity;

	@Column(name="ENFORCE_QUANTITY")
	private BigDecimal enforceQuantity;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to BlockEnforceOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ENFORCE_OPERATION_FK")
	private BlockEnforceOperation blockEnforceOperation;

	//bi-directional many-to-one association to BlockOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

	//bi-directional many-to-one association to BlockEnforceMarketfact
	@OneToMany(mappedBy="blockEnforceDetail", fetch=FetchType.LAZY)
	private List<BlockEnforceMarketfact> blockEnforceMarketfacts;
	
	/** The unblock operation. */
//	@OneToMany(mappedBy="blockEnforceDetail", fetch=FetchType.LAZY)
//	private List<UnblockOperation> unblockOperation;
	@LazyToOne(LazyToOneOption.NO_PROXY)
	@OneToOne(mappedBy="blockEnforceDetail",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private UnblockOperation unblockOperation;
	
	@Transient
	private Boolean isSelected;
	
	@Transient
	private Boolean marketLook;
	
	public BlockEnforceDetail() {
	}

	public Long getIdEnforceDetailPk() {
		return this.idEnforceDetailPk;
	}

	public void setIdEnforceDetailPk(Long idEnforceDetailPk) {
		this.idEnforceDetailPk = idEnforceDetailPk;
	}

	public BigDecimal getBlockQuantity() {
		return this.blockQuantity;
	}

	public void setBlockQuantity(BigDecimal blockQuantity) {
		this.blockQuantity = blockQuantity;
	}

	public BigDecimal getEnforceQuantity() {
		return this.enforceQuantity;
	}

	public void setEnforceQuantity(BigDecimal enforceQuantity) {
		this.enforceQuantity = enforceQuantity;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BlockEnforceOperation getBlockEnforceOperation() {
		return this.blockEnforceOperation;
	}

	public void setBlockEnforceOperation(BlockEnforceOperation blockEnforceOperation) {
		this.blockEnforceOperation = blockEnforceOperation;
	}

	public BlockOperation getBlockOperation() {
		return this.blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	public List<BlockEnforceMarketfact> getBlockEnforceMarketfacts() {
		return this.blockEnforceMarketfacts;
	}

	public void setBlockEnforceMarketfacts(List<BlockEnforceMarketfact> blockEnforceMarketfacts) {
		this.blockEnforceMarketfacts = blockEnforceMarketfacts;
	}

	public BlockEnforceMarketfact addBlockEnforceMarketfact(BlockEnforceMarketfact blockEnforceMarketfact) {
		getBlockEnforceMarketfacts().add(blockEnforceMarketfact);
		blockEnforceMarketfact.setBlockEnforceDetail(this);
		return blockEnforceMarketfact;
	}

	public BlockEnforceMarketfact removeBlockEnforceMarketfact(BlockEnforceMarketfact blockEnforceMarketfact) {
		getBlockEnforceMarketfacts().remove(blockEnforceMarketfact);
		blockEnforceMarketfact.setBlockEnforceDetail(null);
		return blockEnforceMarketfact;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Boolean getMarketLook() {
		return marketLook;
	}

	public void setMarketLook(Boolean marketLook) {
		this.marketLook = marketLook;
	}

	public UnblockOperation getUnblockOperation() {
		return unblockOperation;
	}

	public void setUnblockOperation(UnblockOperation unblockOperation) {
		this.unblockOperation = unblockOperation;
	}

//	public List<UnblockOperation> getUnblockOperation() {
//		return unblockOperation;
//	}
//
//	public void setUnblockOperation(List<UnblockOperation> unblockOperation) {
//		this.unblockOperation = unblockOperation;
//	}
}