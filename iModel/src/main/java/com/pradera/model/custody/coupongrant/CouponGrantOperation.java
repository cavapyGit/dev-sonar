package com.pradera.model.custody.coupongrant;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the CHANGE_OWNERSHIP_OPERATION database table.
 * 
 */
@Entity
@Table(name="COUPON_GRANT_OPERATION")
public class CouponGrantOperation implements Serializable,Auditable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long idCouponGrantOperationPk;
	
	@Version
    private Long version;
	
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_COUPON_GRANT_OPERATION_PK")
	private CustodyOperation custodyOperation;
	
	//bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOURCE_PARTICIPANT_FK")
	private Participant sourceParticipant;
    
  //bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_PARTICIPANT_FK")
	private Participant targetParticipant;
    
    //bi-directional many-to-one association to security
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	//bi-directional many-to-one association to sourceHolderAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOURCE_HOLDER_ACCOUNT_FK")
	private HolderAccount sourceHolderAccount;
    
    //bi-directional many-to-one association to targetHolderAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_HOLDER_ACCOUNT_FK")
	private HolderAccount targetHolderAccount;
    
    //bi-directional many-to-one association to programInterestCoupon
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_PROGRAM_INTEREST_FK")
  	private ProgramInterestCoupon programInterestCoupon;
	
	@Column(name="INITIAL_BALANCE")
	private BigDecimal initialBalance;

	@Column(name="CURRENT_BALANCE")
	private BigDecimal currentBalance;
	
	@Column(name="OPERATION_STATE")
	private Integer operationState;
	
	@Column(name="TRANSFER_TYPE")
	private Integer transferType;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="MOVING_BALANCE")
	private BigDecimal movedBalance;
	
	@Transient
	private BigDecimal transferBalance;
	
	@Transient
	private boolean selection;

	@Transient
	private String grantTypeDescription;
	
	@Transient
	private String stateDescription;
	
	public CouponGrantOperation() {
		initialBalance = BigDecimal.ZERO;
		currentBalance = BigDecimal.ZERO;
		movedBalance = BigDecimal.ZERO;
    }

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(custodyOperation!=null){
    			custodyOperation.setLastModifyApp(lastModifyApp);
    			custodyOperation.setLastModifyDate(lastModifyDate);
    			custodyOperation.setLastModifyIp(lastModifyIp);
    			custodyOperation.setLastModifyUser(lastModifyUser);
    		}
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        return null;
	}

	/**
	 * @return the idCouponGrantOperationPk
	 */
	public Long getIdCouponGrantOperationPk() {
		return idCouponGrantOperationPk;
	}

	/**
	 * @param idCouponGrantOperationPk the idCouponGrantOperationPk to set
	 */
	public void setIdCouponGrantOperationPk(Long idCouponGrantOperationPk) {
		this.idCouponGrantOperationPk = idCouponGrantOperationPk;
	}

	/**
	 * @return the selection
	 */
	public boolean isSelection() {
		return selection;
	}

	/**
	 * @param selection the selection to set
	 */
	public void setSelection(boolean selection) {
		this.selection = selection;
	}

	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * @return the custodyOperation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * @param custodyOperation the custodyOperation to set
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * @return the sourceParticipant
	 */
	public Participant getSourceParticipant() {
		return sourceParticipant;
	}

	/**
	 * @param sourceParticipant the sourceParticipant to set
	 */
	public void setSourceParticipant(Participant sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}

	/**
	 * @return the targetParticipant
	 */
	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	/**
	 * @param targetParticipant the targetParticipant to set
	 */
	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the sourceHolderAccount
	 */
	public HolderAccount getSourceHolderAccount() {
		return sourceHolderAccount;
	}

	/**
	 * @param sourceHolderAccount the sourceHolderAccount to set
	 */
	public void setSourceHolderAccount(HolderAccount sourceHolderAccount) {
		this.sourceHolderAccount = sourceHolderAccount;
	}

	/**
	 * @return the targetHolderAccount
	 */
	public HolderAccount getTargetHolderAccount() {
		return targetHolderAccount;
	}

	/**
	 * @param targetHolderAccount the targetHolderAccount to set
	 */
	public void setTargetHolderAccount(HolderAccount targetHolderAccount) {
		this.targetHolderAccount = targetHolderAccount;
	}

	/**
	 * @return the programInterestCoupon
	 */
	public ProgramInterestCoupon getProgramInterestCoupon() {
		return programInterestCoupon;
	}

	/**
	 * @param programInterestCoupon the programInterestCoupon to set
	 */
	public void setProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon) {
		this.programInterestCoupon = programInterestCoupon;
	}

	/**
	 * @return the initialBalance
	 */
	public BigDecimal getInitialBalance() {
		return initialBalance;
	}

	/**
	 * @param initialBalance the initialBalance to set
	 */
	public void setInitialBalance(BigDecimal initialBalance) {
		this.initialBalance = initialBalance;
	}

	/**
	 * @return the currentBalance
	 */
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}

	/**
	 * @param currentBalance the currentBalance to set
	 */
	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}

	/**
	 * @return the operationState
	 */
	public Integer getOperationState() {
		return operationState;
	}

	/**
	 * @param operationState the operationState to set
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the movedBalance
	 */
	public BigDecimal getMovedBalance() {
		return movedBalance;
	}

	/**
	 * @param movedBalance the movedBalance to set
	 */
	public void setMovedBalance(BigDecimal movedBalance) {
		this.movedBalance = movedBalance;
	}

	/**
	 * @return the grantTypeDescription
	 */
	public String getGrantTypeDescription() {
		return grantTypeDescription;
	}

	/**
	 * @param grantTypeDescription the grantTypeDescription to set
	 */
	public void setGrantTypeDescription(String grantTypeDescription) {
		this.grantTypeDescription = grantTypeDescription;
	}

	/**
	 * @return the stateDescription
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * @param stateDescription the stateDescription to set
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * @return the transferBalance
	 */
	public BigDecimal getTransferBalance() {
		return transferBalance;
	}

	/**
	 * @param transferBalance the transferBalance to set
	 */
	public void setTransferBalance(BigDecimal transferBalance) {
		this.transferBalance = transferBalance;
	}

	public Integer getTransferType() {
		return transferType;
	}

	public void setTransferType(Integer transferType) {
		this.transferType = transferType;
	}

	
}