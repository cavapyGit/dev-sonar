package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum ParticipantUnionFileType.
 */
public enum ParticipantUnionFileType {
	
	ACT_THE_EXTRAORDINARY_GENERAL_MEETING(new Integer(1395),""),
	
	ID_CARD_COPY_OF_TAX(new Integer(1396),""),
	
	
	CERTIFICATE_COMMERCIAL_COPY(new Integer(1397),""),
	
	OTHER_DOCUMENTS(new Integer(1439),""),
	
	REGULATORY_AGENCY_AUTHORIZATION(new Integer(1398),"");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<ParticipantUnionFileType> list = new ArrayList<ParticipantUnionFileType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantUnionFileType> lookup = new HashMap<Integer, ParticipantUnionFileType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<ParticipantUnionFileType> listSomeElements(ParticipantUnionFileType... transferSecuritiesTypeParams){
		List<ParticipantUnionFileType> retorno = new ArrayList<ParticipantUnionFileType>();
		for(ParticipantUnionFileType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ParticipantUnionFileType s : EnumSet.allOf(ParticipantUnionFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new participant union operation state type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private ParticipantUnionFileType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant block motive type
	 */
	public static ParticipantUnionFileType get(Integer code) {
		return lookup.get(code);
	}
}
