/*
 * 
 */
package com.pradera.model.custody.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum TransferSecuritiesEstatusType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/11/2012
 */
public enum CertificateDepositeEstatusType {
	
	/** The registered. */
	REGISTERED(new Integer(1),"REGISTRADA","VAULTBCRD","registered.png"),
	
	/** The approved. */
	APPROVED( new Integer(2),"APROBADA","VAULTBCRD","confirmed.png"),
	
	/** The revised. */
	AUTHORIZED(new Integer(3),"REVISADA","PARTICIPANT","confirmed.png"),
	
	/** The rejected. */
	//REJECTED(new Integer(4),"RECHAZADA","rejected.png"),
	
	/** The confirmed. */
	CONFIRMED(new Integer(5),"CONFIRMADA","VAULTBCRD","confirmed.png"),
	
	/** The canceled. */
	APPLY(new Integer(6),"APPLY","MESSEMGERCVD","remove.png");
	
	/** The codigo. */
	private Integer code;
	
	/** The valor. */
	private String value;
	
	/** The accion. */
	
	private String icon;
	private String situation;
	/** The Constant list. */
	public static final List<CertificateDepositeEstatusType> list = new ArrayList<CertificateDepositeEstatusType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, CertificateDepositeEstatusType> lookup = new HashMap<Integer, CertificateDepositeEstatusType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<CertificateDepositeEstatusType> listSomeElements(CertificateDepositeEstatusType... transferSecuritiesTypeParams){
		List<CertificateDepositeEstatusType> retorno = new ArrayList<CertificateDepositeEstatusType>();
		for(CertificateDepositeEstatusType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (CertificateDepositeEstatusType s : EnumSet.allOf(CertificateDepositeEstatusType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	

	/**
	 * @return the situation
	 */
	public String getSituation() {
		return situation;
	}

	/**
	 * @param situation the situation to set
	 */
	public void setSituation(String situation) {
		this.situation = situation;
	}

	/**
	 * Instantiates a new transfer securities estatus type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 * @param accion the accion
	 */
	private CertificateDepositeEstatusType(Integer codigo, String valor,String situation, String icon) {
		this.code = codigo;
		this.value = valor;
		this.icon = icon;
		this.situation=situation;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the transfer securities estatus type
	 */
	public static CertificateDepositeEstatusType get(Integer codigo) {
		return lookup.get(codigo);
	}
}