/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.custody.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public enum DematerializationCertificateRequestType {
	PARTICIPANT(new Integer(1),"PARTICIPANTE"),
	ISSUER(new Integer(2),"EMISOR");	
	private Integer code;
	private String value;
	public static final List<DematerializationCertificateRequestType> list = new ArrayList<DematerializationCertificateRequestType>();
	public static final Map<Integer, DematerializationCertificateRequestType> lookup = new HashMap<Integer, DematerializationCertificateRequestType>();
	public static List<DematerializationCertificateRequestType> listSomeElements(DematerializationCertificateRequestType... transferSecuritiesTypeParams){
		List<DematerializationCertificateRequestType> retorno = new ArrayList<DematerializationCertificateRequestType>();
		for(DematerializationCertificateRequestType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (DematerializationCertificateRequestType s : EnumSet.allOf(DematerializationCertificateRequestType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private DematerializationCertificateRequestType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	public static DematerializationCertificateRequestType get(Integer codigo) {
		return lookup.get(codigo);
	}
}