package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PaymentType {

	MENSUAL(Integer.valueOf(147),""),
	BIMESTRAL(Integer.valueOf(536),""),
	TRIMESTRAL(Integer.valueOf(537),""),
	CUATRIMESTRAL(Integer.valueOf(538),""),
	SEMESTRAL(Integer.valueOf(539),""),
	ANUAL(Integer.valueOf(540),"");
	

	private Integer code;
	private String value;


	/** The Constant list. */
	public static final List<PaymentType> list = new ArrayList<PaymentType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, PaymentType> lookup = new HashMap<Integer, PaymentType>();
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<PaymentType> listSomeElements(PaymentType... paymentTypes){
		List<PaymentType> retorno = new ArrayList<PaymentType>();
		for(PaymentType paymentType: paymentTypes){
			retorno.add(paymentType);
		}
		return retorno;
	}
	static {
		for (PaymentType s : EnumSet.allOf(PaymentType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Instantiates a new transfer securities estatus type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 * @param accion the accion
	 */
	private PaymentType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the transfer securities estatus type
	 */
	public static PaymentType get(Integer codigo) {
		return lookup.get(codigo);
	}
}
