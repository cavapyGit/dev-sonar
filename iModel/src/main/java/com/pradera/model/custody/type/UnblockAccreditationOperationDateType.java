package com.pradera.model.custody.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum UnblockAccreditationOperationDateType {
	REGISTRY_DATE(new Integer(1),"FECHA DE REGISTRO"),
	EXPIRATION_DATE(new Integer(2),"FECHA DE VENCIMIENTO");
	
	private Integer code;
	private String value;
	public static final List<UnblockAccreditationOperationDateType> list = new ArrayList<UnblockAccreditationOperationDateType>();
	
	public static final Map<Integer, UnblockAccreditationOperationDateType> lookup = new HashMap<Integer, UnblockAccreditationOperationDateType>();
	public static List<UnblockAccreditationOperationDateType> listSomeElements(UnblockAccreditationOperationDateType... transferSecuritiesTypeParams){
		List<UnblockAccreditationOperationDateType> retorno = new ArrayList<UnblockAccreditationOperationDateType>();
		for(UnblockAccreditationOperationDateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (UnblockAccreditationOperationDateType s : EnumSet.allOf(UnblockAccreditationOperationDateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private UnblockAccreditationOperationDateType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	public static UnblockAccreditationOperationDateType get(Integer code) {
		return lookup.get(code);
	}
}
