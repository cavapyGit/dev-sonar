package com.pradera.model.custody.accreditationcertificates;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.transfersecurities.CustodyOperation;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccreditationOperation.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/04/2013
 */
@Entity
@Table(name="UNBLOCK_ACCREDIT_OPERATION")
public class UnblockAccreditationOperation implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id unblock accreditation pk. */
	@Id
	private Long idUnblockAccreditationPk;
	
	/** The custody operation. */
	@MapsId
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_UNBLOCK_ACCREDITATION_PK")
	private CustodyOperation custodyOperation;

	/** The accreditation operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCREDITATION_OPERATION_FK")
	private AccreditationOperation accreditationOperation;
	
    /** The expiration date. */
    @Temporal( TemporalType.DATE)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;
    
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The ind automatic. */
    @Column(name="IND_AUTOMATIC")
    private Integer indAutomatic;
    
    /** The is selected. */
    @Transient
	private Boolean isAutomatic;
    
    @Version
    private Long version;

	public UnblockAccreditationOperation() {
    }

	/**
	 * @return the idUnblockAccreditationPk
	 */
	public Long getIdUnblockAccreditationPk() {
		return idUnblockAccreditationPk;
	}

	/**
	 * @param idUnblockAccreditationPk the idUnblockAccreditationPk to set
	 */
	public void setIdUnblockAccreditationPk(Long idUnblockAccreditationPk) {
		this.idUnblockAccreditationPk = idUnblockAccreditationPk;
	}

	/**
	 * @return the custodyOperation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * @param custodyOperation the custodyOperation to set
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * @return the accreditationOperation
	 */
	public AccreditationOperation getAccreditationOperation() {
		return accreditationOperation;
	}

	/**
	 * @param accreditationOperation the accreditationOperation to set
	 */
	public void setAccreditationOperation(
			AccreditationOperation accreditationOperation) {
		this.accreditationOperation = accreditationOperation;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the indAutomatic
	 */
	public Integer getIndAutomatic() {
		return indAutomatic;
	}

	/**
	 * @param indAutomatic the indAutomatic to set
	 */
	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	}

	/**
	 * @return the isAutomatic
	 */
	public Boolean getIsAutomatic() {
		return isAutomatic;
	}

	/**
	 * @param isAutomatic the isAutomatic to set
	 */
	public void setIsAutomatic(Boolean isAutomatic) {
		this.isAutomatic = isAutomatic;
	}

	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(custodyOperation != null){
            	custodyOperation.setAudit(loggerUser);
            }
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
		
}
