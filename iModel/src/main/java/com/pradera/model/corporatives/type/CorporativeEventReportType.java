package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Pradera Security
 *
 */
public enum CorporativeEventReportType {
	
	CONTROL_OF_PAYMENTS(new Integer(1477),"CONTROL DE PAGOS"),
	REPURCHASE_BY_MARGIN(new Integer(1478),"POR OPERACIONES DE REPORTO Y MARGEN"),
	BLOCKED_BY_BALANCE(new Integer(1479), "POR SALDO BLOQUEADO"),
	HOLDER_ACCOUNT_FOR_PARTICIPANTS(new Integer(1480), "POR TITULAR CUENTA A PARTICIPANTES"),
	FOR_SECURITY_ISSUER(new Integer(1481), "POR VALOR - EMISOR"),
	SPECIAL_PROCESS_CONTROL_LIST(new Integer(1483), "PROCESOS ESPECIALES LISTADO CONTROL"),
	RETIREMENT_HOLDER_ACCOUNT_FOR_PARTICIPANTS(new Integer(1), "RETIRO POR TITULAR PARTICIPANTE"),
	RETIREMENT_FOR_SECURITY_ISSUER(new Integer(2), "RETIRO POR TITULARES"),
	AMORTIZATION_PAYMENT_SECURITY_ISSUER(new Integer(174), "REPORTE DE PAGO DE AMORTIZACION POR VALOR - EMISOR"),
	INTERES_PAYMENT_SECURITY_ISSUER(new Integer(172), "REPORTE DE PAGO DE INTERES POR VALOR - EMISOR"),
	CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT(new Integer(373), "REPORTE CONSOLIDADO PAGO DE INTERESES PRELIMINAR Y DEFINITIVO POR DEPOSITANTE"),
	CONSOLIDATED_INTEREST_PAYMENT_ISSUER(new Integer(374), "REPORTE CONSOLIDADO PAGO DE INTERESES PRELIMINAR Y DEFINITIVO POR EMISOR"),
	CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT(new Integer(375), "REPORTE CONSOLIDADO PAGO DE AMORTIZACIÓN PRELIMINAR Y DEFINITIVO POR DEPOSITANTE"),
	CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER(new Integer(376), "REPORTE CONSOLIDADO PAGO DE AMORTIZACIÓN PRELIMINAR Y DEFINITIVO POR EMISOR"),
	REMANENT_PAYMENT_ISSUER(new Integer(377), "REPORTE DE PAGO DE REMANENTES EN EFECTIVO POR VALOR - EMISOR");

	private String value;
	private Integer code;

	public static final List<CorporativeEventReportType> list= new ArrayList<CorporativeEventReportType>();
	public static final Map<Integer, CorporativeEventReportType> lookup = new HashMap<Integer,CorporativeEventReportType >();

	static{
		for(CorporativeEventReportType type: EnumSet.allOf(CorporativeEventReportType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}

	private CorporativeEventReportType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static CorporativeEventReportType get(Integer code)
	{
		return lookup.get(code);
	}

}
