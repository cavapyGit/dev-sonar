package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CorporativeSecuritySourceType {

	/** The coupon payment. */
	SOURCE(new Integer(1), "ORIGEN"),

	/** The amortization payment. */
	TARGET(new Integer(2), "DESTINO");

	/** The value. */
	private String value;

	/** The code. */
	private Integer code;

	/** The Constant list. */
	public static final List<CorporativeSecuritySourceType> list= new ArrayList<CorporativeSecuritySourceType>();

	/** The Constant lookup. */
	public static final Map<Integer, CorporativeSecuritySourceType> lookup = new HashMap<Integer,CorporativeSecuritySourceType >();

	static{
		for(CorporativeSecuritySourceType type: EnumSet.allOf(CorporativeSecuritySourceType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}



	private CorporativeSecuritySourceType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}



	public Integer getCode() {
		return code;
	}


	public static CorporativeSecuritySourceType get(Integer code)
	{
		return lookup.get(code);
	}


}



