package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the BENEFIT_PAYMENT_FILE database table.
 * 
 */
@Entity
@Table(name="CUSTODY_COMMISSION_DETAIL")
public class CustodyCommissionDetail implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CUSTODY_COMMISSION_DETAIL_GENERATOR", sequenceName="SQ_CUSTODY_COMMISSION_DETAIL",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CUSTODY_COMMISSION_DETAIL_GENERATOR")
	@Column(name="ID_CUSTODY_COMMISSION_DET_PK")
	private Long idCustodyCommissionDetailPK;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CUSTODY_COMMISSION_FK")
	private CustodyCommission custodyCommission;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@Column(name="POSSESSION_TYPE")
	private Integer possessionType;

	@Column(name="CUSTODY_QUANTITY")
	private BigDecimal custodyQuantity;

	@Column(name="CUSTODY_AMOUNT")
	private BigDecimal custodyAmount;

	@Column(name="COMMISSION_TAX")
	private BigDecimal custodyTax;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;


    public CustodyCommissionDetail() {
    }


	public Long getIdCustodyCommissionDetailPK() {
		return idCustodyCommissionDetailPK;
	}

	public void setIdCustodyCommissionDetailPK(Long idCustodyCommissionDetailPK) {
		this.idCustodyCommissionDetailPK = idCustodyCommissionDetailPK;
	}

	public CustodyCommission getCustodyCommission() {
		return custodyCommission;
	}

	public void setCustodyCommission(CustodyCommission custodyCommission) {
		this.custodyCommission = custodyCommission;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Integer getPossessionType() {
		return possessionType;
	}

	public void setPossessionType(Integer possessionType) {
		this.possessionType = possessionType;
	}

	public BigDecimal getCustodyQuantity() {
		return custodyQuantity;
	}

	public void setCustodyQuantity(BigDecimal custodyQuantity) {
		this.custodyQuantity = custodyQuantity;
	}

	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}

	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}

	public BigDecimal getCustodyTax() {
		return custodyTax;
	}

	public void setCustodyTax(BigDecimal custodyTax) {
		this.custodyTax = custodyTax;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
}