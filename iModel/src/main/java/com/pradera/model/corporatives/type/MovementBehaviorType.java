package com.pradera.model.corporatives.type;



import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum MovementBehaviorType {

	ADD(new Integer(1), "SUMA"),
	SUBSTRACT(new Integer(2), "RESTA");

	private String value;
	private Integer code;

	public static final List<MovementBehaviorType> list= new ArrayList<MovementBehaviorType>();
	public static final Map<Integer, MovementBehaviorType> lookup = new HashMap<Integer,MovementBehaviorType >();

	static{
		for(MovementBehaviorType type: EnumSet.allOf(MovementBehaviorType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	private MovementBehaviorType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static MovementBehaviorType get(Integer code)
	{
		return lookup.get(code);
	}


}
