package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum PayrollStateType.
 */
public enum PayrollStateType {

	 
		/** The coupon payment. */
		REGISTERED(new Integer(2832),"REGISTRADO"),
		CONFIRMED(new Integer(2833),"CONFIRMADO"),
		ANNULLED(new Integer(2834),"ANULADO"),
		GENERATED(new Integer(2835), "EN PROCESO");

		/** The value. */
		private String value;
		
		/** The code. */
		private Integer code;

		/** The Constant list. */
		public static final List<PayrollStateType> list= new ArrayList<PayrollStateType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, PayrollStateType> lookup = new HashMap<Integer,PayrollStateType >();

		static{
			for(PayrollStateType type: EnumSet.allOf(PayrollStateType.class))
			{
				list.add(type);
				lookup.put(type.getCode(),type);

			}

		}


		/**
		 * Instantiates a new accord type.
		 *
		 * @param code the code
		 * @param value the value
		 */
		private PayrollStateType(Integer code, String value) {
			this.value = value;
			this.code = code;
		}


		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}


		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}

		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the accord type
		 */
		public static PayrollStateType get(Integer code)
		{
			return lookup.get(code);
		}


	}
