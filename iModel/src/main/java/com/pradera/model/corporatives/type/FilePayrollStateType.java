package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum PayrollStateType.
 */
public enum FilePayrollStateType {

	 
		/** The coupon payment. */
		REGISTERED(new Integer(2838),"REGISTRADO"),
		PROCESSED(new Integer(2839), "GENERADO");

		/** The value. */
		private String value;
		
		/** The code. */
		private Integer code;

		/** The Constant list. */
		public static final List<FilePayrollStateType> list= new ArrayList<FilePayrollStateType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, FilePayrollStateType> lookup = new HashMap<Integer,FilePayrollStateType >();

		static{
			for(FilePayrollStateType type: EnumSet.allOf(FilePayrollStateType.class))
			{
				list.add(type);
				lookup.put(type.getCode(),type);

			}

		}


		/**
		 * Instantiates a new accord type.
		 *
		 * @param code the code
		 * @param value the value
		 */
		private FilePayrollStateType(Integer code, String value) {
			this.value = value;
			this.code = code;
		}


		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}


		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}

		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the accord type
		 */
		public static FilePayrollStateType get(Integer code)
		{
			return lookup.get(code);
		}


	}
