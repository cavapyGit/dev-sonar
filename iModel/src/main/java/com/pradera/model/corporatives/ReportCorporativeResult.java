package com.pradera.model.corporatives;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the REPORT_CORPORATIVE_RESULT database table.
 * 
 */
/**
 * @author PraderaTechnologies
 *
 */
@Entity
@Table(name="REPORT_CORPORATIVE_RESULT")
public class ReportCorporativeResult implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_REPORT_CORPORATIVE_RESULT")
	@SequenceGenerator(name="REPORT_CORPORATIVE_RESULT_GENERATOR", sequenceName="SQ_ID_REPORT_CORP_RESULT",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPORT_CORPORATIVE_RESULT_GENERATOR")
	private Long idReportCorporativeResult;

	@Column(name="GUARANTEE_BALANCE")
	private BigDecimal guaranteeBalance;
	
	@Column(name="TAX_AMOUNT")
	private BigDecimal taxAmount;
	
	@Column(name="CUSTODY_AMOUNT")
	private BigDecimal custodyAmount;

	@Column(name="GUARANTEE_TYPE")
	private Integer guaranteeType;
	
	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK")
	private HolderAccountOperation holderAccountOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
	private MechanismOperation mechanismOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="IND_ORIGIN_DESTINY")
	private Integer indOriginDestiny;

	@Column(name="IND_PROCESSED")
	private Integer indProcessed;

	@Column(name="IND_REMANENT")
	private Integer indRemanent;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OPERATION_ROLE")
	private Integer operationRole;

	//bi-directional many-to-one association to CorporativeOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;

	//bi-directional many-to-one association to CorporativeProcessResult
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_PROCESS_RESULT")
	private CorporativeProcessResult corporativeProcessResult;

    public ReportCorporativeResult() {
    	this.taxAmount= BigDecimal.ZERO;
    	this.custodyAmount= BigDecimal.ZERO;
    }

	public Long getIdReportCorporativeResult() {
		return this.idReportCorporativeResult;
	}

	public void setIdReportCorporativeResult(Long idReportCorporativeResult) {
		this.idReportCorporativeResult = idReportCorporativeResult;
	}

	public BigDecimal getGuaranteeBalance() {
		return this.guaranteeBalance;
	}

	public void setGuaranteeBalance(BigDecimal guaranteeBalance) {
		this.guaranteeBalance = guaranteeBalance;
	}

	public Integer getGuaranteeType() {
		return this.guaranteeType;
	}

	public void setGuaranteeType(Integer guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	/**
	 * @return the idHolderAccountFk
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * @param idHolderAccountFk the idHolderAccountFk to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}

	
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Integer getIndProcessed() {
		return indProcessed;
	}

	public void setIndProcessed(Integer indProcessed) {
		this.indProcessed = indProcessed;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getOperationRole() {
		return this.operationRole;
	}

	public void setOperationRole(Integer operationRole) {
		this.operationRole = operationRole;
	}

	public CorporativeOperation getCorporativeOperation() {
		return this.corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}
	
	public CorporativeProcessResult getCorporativeProcessResult() {
		return this.corporativeProcessResult;
	}

	public void setCorporativeProcessResult(CorporativeProcessResult corporativeProcessResult) {
		this.corporativeProcessResult = corporativeProcessResult;
	}

	/**
	 * @return the indOriginDestiny
	 */
	public Integer getIndOriginDestiny() {
		return indOriginDestiny;
	}

	/**
	 * @param indOriginDestiny the indOriginDestiny to set
	 */
	public void setIndOriginDestiny(Integer indOriginDestiny) {
		this.indOriginDestiny = indOriginDestiny;
	}

	/**
	 * @return the indRemanent
	 */
	public Integer getIndRemanent() {
		return indRemanent;
	}

	/**
	 * @param indRemanent the indRemanent to set
	 */
	public void setIndRemanent(Integer indRemanent) {
		this.indRemanent = indRemanent;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		
        return detailsMap;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}

	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}
	
}