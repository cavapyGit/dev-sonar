package com.pradera.model.corporatives.stockcalculation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class StockCalculationBalance.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/04/2013
 */
@Entity
@Table(name = "STOCK_CALCULATION_BALANCE")
public class StockCalculationBalance implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id stock calc balance pk. */
	@Id
	@Column(name = "ID_STOCK_CALC_BALANCE_PK")
	private Long idStockCalcBalancePk;

	/** The total balance. */
	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance = new BigDecimal(0);
	
	/** The available balance. */
	@Column(name="AVAILABLE_BALANCE") 
	private BigDecimal availableBalance = new BigDecimal(0);
	
	/** The transit balance. */
	@Column(name="TRANSIT_BALANCE")
	private BigDecimal transitBalance = new BigDecimal(0);
	
	/** The pawn balance. */
	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance = new BigDecimal(0);
	
	/** The ban balance. */
	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance = new BigDecimal(0);
	
	/** The other block balance. */
	@Column(name="OTHER_BLOCK_BALANCE")
	private BigDecimal otherBlockBalance = new BigDecimal(0);
	
	/** The reserve balance. */
	@Column(name="RESERVE_BALANCE")
	private BigDecimal reserveBalance = new BigDecimal(0);
	
	/** The opposition balance. */
	@Column(name="OPPOSITION_BALANCE")
	private BigDecimal oppositionBalance = new BigDecimal(0);
	
	/** The accreditation balance. */
	@Column(name="ACCREDITATION_BALANCE")
	private BigDecimal accreditationBalance = new BigDecimal(0);

	/** The purchase balance. */
	@Column(name="PURCHASE_BALANCE")
	private BigDecimal purchaseBalance = new BigDecimal(0);

	/** The sale balance. */
	@Column(name="SALE_BALANCE")
	private BigDecimal saleBalance = new BigDecimal(0);
	
	/** The reporting balance. */
	@Column(name="REPORTING_BALANCE")
	private BigDecimal reportingBalance = new BigDecimal(0);
	
	/** The reported balance. */
	@Column(name="REPORTED_BALANCE")
	private BigDecimal reportedBalance = new BigDecimal(0);
	
	/** The margin balance. */
	@Column(name="MARGIN_BALANCE")
	private BigDecimal marginBalance = new BigDecimal(0);
	
	/** The borrower balance. */
	@Column(name="BORROWER_BALANCE")
	private BigDecimal borrowerBalance = new BigDecimal(0);

	/** The lender balance. */
	@Column(name="LENDER_BALANCE")
	private BigDecimal lenderBalance = new BigDecimal(0);

	/** The loanable balance. */
	@Column(name="LOANABLE_BALANCE")
	private BigDecimal loanableBalance = new BigDecimal(0);

	/** The custody amount. */
	@Column(name="CUSTODY_AMOUNT")
	private BigDecimal custodyAmount;

	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Long lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	// bi-directional many-to-one association to StockCalculationProcess
		/** The stock calculation process. */
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name = "ID_STOCK_CALCULATION_FK")
		private StockCalculationProcess stockCalculationProcess;
		
		/** The id holder account fk. */
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name = "ID_HOLDER_ACCOUNT_FK")
		private HolderAccount holderAccount;

		/** The id isin code fk. */
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name = "ID_SECURITY_CODE_FK")
		private Security security;

		/** The id participant fk. */
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name = "ID_PARTICIPANT_FK")
		private Participant participant;
	
	/**
	 * Instantiates a new stock calculation balance.
	 */
	public StockCalculationBalance() {
	}

	/**
	 * Gets the id stock calc balance pk.
	 *
	 * @return the id stock calc balance pk
	 */
	public Long getIdStockCalcBalancePk() {
		return idStockCalcBalancePk;
	}

	/**
	 * Sets the id stock calc balance pk.
	 *
	 * @param idStockCalcBalancePk the new id stock calc balance pk
	 */
	public void setIdStockCalcBalancePk(Long idStockCalcBalancePk) {
		this.idStockCalcBalancePk = idStockCalcBalancePk;
	}

	/**
	 * Gets the accreditation balance.
	 *
	 * @return the accreditation balance
	 */
	public BigDecimal getAccreditationBalance() {
		return accreditationBalance;
	}

	/**
	 * Sets the accreditation balance.
	 *
	 * @param accreditationBalance the new accreditation balance
	 */
	public void setAccreditationBalance(BigDecimal accreditationBalance) {
		this.accreditationBalance = accreditationBalance;
	}

	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * Gets the ban balance.
	 *
	 * @return the ban balance
	 */
	public BigDecimal getBanBalance() {
		return banBalance;
	}

	/**
	 * Sets the ban balance.
	 *
	 * @param banBalance the new ban balance
	 */
	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	/**
	 * Gets the borrower balance.
	 *
	 * @return the borrower balance
	 */
	public BigDecimal getBorrowerBalance() {
		return borrowerBalance;
	}

	/**
	 * Sets the borrower balance.
	 *
	 * @param borrowerBalance the new borrower balance
	 */
	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	

	

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the id isin code fk.
	 *
	 * @return the id isin code fk
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the id isin code fk.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the id participant fk.
	 *
	 * @return the id participant fk
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the id participant fk.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the lender balance.
	 *
	 * @return the lender balance
	 */
	public BigDecimal getLenderBalance() {
		return lenderBalance;
	}

	/**
	 * Sets the lender balance.
	 *
	 * @param lenderBalance the new lender balance
	 */
	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	/**
	 * Gets the lenderable balance.
	 *
	 * @return the lenderable balance
	 */
	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	/**
	 * Sets the lenderable balance.
	 *
	 * @param lenderableBalance the new lenderable balance
	 */
	public void setLoanableBalance(BigDecimal lenderableBalance) {
		this.loanableBalance = lenderableBalance;
	}

	/**
	 * Gets the margin balance.
	 *
	 * @return the margin balance
	 */
	public BigDecimal getMarginBalance() {
		return marginBalance;
	}

	/**
	 * Sets the margin balance.
	 *
	 * @param marginBalance the new margin balance
	 */
	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	/**
	 * Gets the other block balance.
	 *
	 * @return the other block balance
	 */
	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	/**
	 * Sets the other block balance.
	 *
	 * @param otherBlockBalance the new other block balance
	 */
	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	/**
	 * Gets the pawn balance.
	 *
	 * @return the pawn balance
	 */
	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	/**
	 * Sets the pawn balance.
	 *
	 * @param pawnBalance the new pawn balance
	 */
	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	/**
	 * Gets the reported balance.
	 *
	 * @return the reported balance
	 */
	public BigDecimal getReportedBalance() {
		return reportedBalance;
	}

	/**
	 * Sets the reported balance.
	 *
	 * @param reportedBalance the new reported balance
	 */
	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	/**
	 * Gets the reporting balance.
	 *
	 * @return the reporting balance
	 */
	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	/**
	 * Sets the reporting balance.
	 *
	 * @param reportingBalance the new reporting balance
	 */
	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	/**
	 * Gets the reserve balance.
	 *
	 * @return the reserve balance
	 */
	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}

	/**
	 * Sets the reserve balance.
	 *
	 * @param reserveBalance the new reserve balance
	 */
	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	

	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	/**
	 * Gets the transit balance.
	 *
	 * @return the transit balance
	 */
	public BigDecimal getTransitBalance() {
		return transitBalance;
	}

	/**
	 * Sets the transit balance.
	 *
	 * @param transitBalance the new transit balance
	 */
	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	/**
	 * Gets the stock calculation process.
	 *
	 * @return the stock calculation process
	 */
	public StockCalculationProcess getStockCalculationProcess() {
		return stockCalculationProcess;
	}

	/**
	 * Sets the stock calculation process.
	 *
	 * @param stockCalculationProcess the new stock calculation process
	 */
	public void setStockCalculationProcess(
			StockCalculationProcess stockCalculationProcess) {
		this.stockCalculationProcess = stockCalculationProcess;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the opposition balance.
	 *
	 * @return the opposition balance
	 */
	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	/**
	 * Sets the opposition balance.
	 *
	 * @param oppositionBalance the new opposition balance
	 */
	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = new Long(loggerUser.getIdPrivilegeOfSystem());
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the custody amount.
	 *
	 * @return the custodyAmount
	 */
	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}

	/**
	 * Sets the custody amount.
	 *
	 * @param custodyAmount the custodyAmount to set
	 */
	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}

	/**
	 * Gets the purchase balance.
	 *
	 * @return the purchase balance
	 */
	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	/**
	 * Sets the purchase balance.
	 *
	 * @param purchaseBalance the new purchase balance
	 */
	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	/**
	 * Gets the sale balance.
	 *
	 * @return the sale balance
	 */
	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	/**
	 * Sets the sale balance.
	 *
	 * @param saleBalance the new sale balance
	 */
	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	
	

}