package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the BENEFIT_PAYMENT_ALLOCATION database table.
 * 
 */
@Entity
@Table(name="BENEFIT_PAYMENT_ALLOCATION")
public class BenefitPaymentAllocation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BENEFIT_PAYMENT_GENERATOR", sequenceName="SQ_ID_BENEFIT_PAYMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BENEFIT_PAYMENT_GENERATOR")
	@Column(name="ID_BENEFIT_PAYMENT_PK")
	private Long idBenefitPaymentPK;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@Column(name="ALLOCATION_PROCESS_STATE")
	private Integer allocationProcessState;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ALLOCATION_PROCESS_DATE")
	private Date allocationProcessDate;

	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="ALLOCATION_PROCESS_AMOUNT")
	private BigDecimal allocationProcessAmount;

	@Column(name="IND_EXCLUDED_COMMISSIONS")
	private Integer indExcludedCommissions;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="ALLOCATION_QUANTITY_PROCESS")
	private BigDecimal allocationQuantityProcess;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_SECURITY_CODE_FK")
    private Security security;

	@Column(name="PAYMENT_TYPE")
	private Integer paymentType;
	
	@OneToMany(mappedBy="benefitPaymentAllocation")
	private List<CorporativeOperation> corporativeOperations;

	@OneToMany(mappedBy="benefitPaymentAllocation")
	private List<FundsOperation> fundsOperation;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_INSTITUTION_CASH_ACCOUNT_FK")
	InstitutionCashAccount institutionCashAccount;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_INSTITUTION_BANK_ACCOUNT_FK")
	InstitutionBankAccount institutionBankAccount;


    public BenefitPaymentAllocation() {
    }
	public Long getIdBenefitPaymentPK() {
		return idBenefitPaymentPK;
	}
	public void setIdBenefitPaymentPK(Long idBenefitPaymentPK) {
		this.idBenefitPaymentPK = idBenefitPaymentPK;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public Integer getIndExcludedCommissions() {
		return indExcludedCommissions;
	}
	public void setIndExcludedCommissions(Integer indExcludedCommissions) {
		this.indExcludedCommissions = indExcludedCommissions;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public List<CorporativeOperation> getCorporativeOperations() {
		return corporativeOperations;
	}
	public void setCorporativeOperations(
			List<CorporativeOperation> corporativeOperations) {
		this.corporativeOperations = corporativeOperations;
	}
	public List<FundsOperation> getFundsOperation() {
		return fundsOperation;
	}
	public void setFundsOperation(List<FundsOperation> fundsOperation) {
		this.fundsOperation = fundsOperation;
	}
	public Integer getAllocationProcessState() {
		return allocationProcessState;
	}
	public void setAllocationProcessState(Integer allocationProcessState) {
		this.allocationProcessState = allocationProcessState;
	}
	public Date getAllocationProcessDate() {
		return allocationProcessDate;
	}
	public void setAllocationProcessDate(Date allocationProcessDate) {
		this.allocationProcessDate = allocationProcessDate;
	}
	public BigDecimal getAllocationProcessAmount() {
		return allocationProcessAmount;
	}
	public void setAllocationProcessAmount(BigDecimal allocationProcessAmount) {
		this.allocationProcessAmount = allocationProcessAmount;
	}
	public BigDecimal getAllocationQuantityProcess() {
		return allocationQuantityProcess;
	}
	public void setAllocationQuantityProcess(BigDecimal allocationQuantityProcess) {
		this.allocationQuantityProcess = allocationQuantityProcess;
	}
	public Security getSecurity() {
		return security;
	}
	public void setSecurity(Security security) {
		this.security = security;
	}
	public Integer getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}
	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}
	public void setInstitutionCashAccount(InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}
	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}
	public void setInstitutionBankAccount(InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {		 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}