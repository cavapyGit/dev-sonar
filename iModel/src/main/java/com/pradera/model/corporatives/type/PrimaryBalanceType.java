package com.pradera.model.corporatives.type;



import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PrimaryBalanceType {

	PRIMARY_BALANCE(new Integer(1574), "SALDO PRIMARIO"),
	SECONDARY_BALANCE(new Integer(1575), "SALDO SECUNDARIO");

	private String value;
	private Integer code;

	public static final List<PrimaryBalanceType> list= new ArrayList<PrimaryBalanceType>();
	public static final Map<Integer, PrimaryBalanceType> lookup = new HashMap<Integer,PrimaryBalanceType >();

	static{
		for(PrimaryBalanceType type: EnumSet.allOf(PrimaryBalanceType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	private PrimaryBalanceType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static PrimaryBalanceType get(Integer code)
	{
		return lookup.get(code);
	}


}
