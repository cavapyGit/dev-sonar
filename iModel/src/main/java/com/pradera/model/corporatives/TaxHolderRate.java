package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: TaxHolderRate
 *
 */
@Entity
@Table(name="TAX_HOLDER_RATE")
public class TaxHolderRate implements Serializable {
	private static final long serialVersionUID = 1L;

	   
	@Id
	@Column(name="ID_TAX_HOLDER_RATE_PK")
	private Long idTaxHolderRatePk;
	
	@Column(name="HOLDER_TYPE")
	private Integer holderType;
	
	@Column(name="NATIONALITY")
	private Integer nationality;	
	
    @Column(name="CORPORATE_EVENT_TYPE")
	private Integer corporateEventType;
	
	@Column(name="TAX_RATE")
	private BigDecimal taxRate;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="LAST_MODIFY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	public TaxHolderRate() {
		super();
	}   
	public Long getIdTaxHolderRatePk() {
		return this.idTaxHolderRatePk;
	}

	public void setIdTaxHolderRatePk(Long idTaxHolderRatePk) {
		this.idTaxHolderRatePk = idTaxHolderRatePk;
	}   
	public Integer getHolderType() {
		return this.holderType;
	}

	public void setHolderType(Integer holderType) {
		this.holderType = holderType;
	}   
	public Integer getNationality() {
		return this.nationality;
	}

	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}   
	public BigDecimal getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(BigDecimal taxRate) {
		this.taxRate = taxRate;
	}   
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}   
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}   
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}   
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	/**
	 * @return the corporateEventType
	 */
	public Integer getCorporateEventType() {
		return corporateEventType;
	}
	
	/**
	 * @param corporateEventType the corporateEventType to set
	 */
	public void setCorporateEventType(Integer corporateEventType) {
		this.corporateEventType = corporateEventType;
	}
   
}
