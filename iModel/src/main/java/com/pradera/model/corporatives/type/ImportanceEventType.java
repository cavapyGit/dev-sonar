package com.pradera.model.corporatives.type;

 


import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum ImportanceEventType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/06/2014
 */
public enum ImportanceEventType {

	/** The action dividends. */
	ACTION_DIVIDENDS(new Integer(228),"DIVIDENDOS EN ACCIONES","StockCorporateProcessExecutor"),
	
	/** The return of contribution. */
	RETURN_OF_CONTRIBUTION(new Integer(1266),"DEVOLUCION DE APORTES","CashCorporateProcessExecutor"),
	
	/** The remanent payment. */
	REMANENT_PAYMENT(new Integer(1267),"PAGO DE REMANENTES","CashCorporateProcessExecutor"),
	
	/** The end preferred suscription. */
	END_PREFERRED_SUSCRIPTION(new Integer(1268),"TERMINO DE SUSCRIPCION PREFERENTE","SubscriptionCorporateProcessExecutor"),
	
	/** The cash dividends. */
	CASH_DIVIDENDS(new Integer(1264),"DIVIDENDOS EN EFECTIVO","CashCorporateProcessExecutor"),
	
	/** The change nominal value no cap var. */
	CHANGE_NOMINAL_VALUE_NO_CAP_VAR(new Integer(1270),"CAMBIO DE VALOR NOMINAL  SIN VARIACION CAPITAL","SpecialCorporateProcessExecutor"),
	
	/** The change nomal value cap var. */
	CHANGE_NOMAL_VALUE_CAP_VAR(new Integer(1269),"CAMBIO DE VALOR NOMINAL CON VARIACION CAPITAL","SpecialCorporateProcessExecutor"),
	
	/** The securities fusion. */
	SECURITIES_FUSION(new Integer(1271),"FUSION DE VALORES","SpecialCorporateProcessExecutor"),
	
	/** The securuties unification. */
	SECURITIES_UNIFICATION(new Integer(1272),"UNIFICACION DE VALORES","SpecialCorporateProcessExecutor"),
	
	/** The capital reduction. */
	CAPITAL_REDUCTION(new Integer(1273),"REDUCCION DE CAPITAL","SpecialCorporateProcessExecutor"),
	
	/** The securities excision. */
	SECURITIES_EXCISION(new Integer(1274),"ESCISION DE VALORES","SplitCorporateProcessExecutor"),
	
	/** The securities exclusion. */
	SECURITIES_EXCLUSION(new Integer(1275),"EXCLUSION DE VALORES","SpecialCorporateProcessExecutor"),
	
	/** The interest payment. */
	INTEREST_PAYMENT(new Integer(1276),"PAGO DE INTERES","CashCorporateProcessExecutor"),
	
	/** The capital amortization. */
	CAPITAL_AMORTIZATION(new Integer(1277),"AMORTIZACION DE CAPITAL","AmortizationCorporateProcessExecutor"),
	
	/** The convertibily bone to action. */
	CONVERTIBILY_BONE_TO_ACTION(new Integer(1278),"CONVERTIBILIDAD DE BONOS EN ACCIONES", "SpecialCorporateProcessExecutor"),
	
	/** The preferred suscription. */
	PREFERRED_SUSCRIPTION(new Integer(1265),"SUSCRIPCION PREFERENTE","SubscriptionCorporateProcessExecutor"),
	
	/** The rescue capital. */
	EARLY_PAYMENT(new Integer(1443),"REDENCION ANTICIPADA","AmortizationCorporateProcessExecutor"),
	
	COMMON_ACTIONS(new Integer(406),"ACCIONES COMUNES",""),
	
	/** The consolidated interest payment participant. */
	CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT(new Integer(2936),"CONSOLIDADO PAGO DE INTERES POR DEPOSITANTE",""),
	
	/** The consolidated interest payment issuer. */
	CONSOLIDATED_INTEREST_PAYMENT_ISSUER(new Integer(2937),"CONSOLIDADO PAGO DE INTERES POR EMISOR",""),
	
	/** The consolidated amortization payment participant. */
	CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT(new Integer(2938),"CONSOLIDADO AMORTIZACION DE CAPITAL POR DEPOSITANTE",""),
	
	/** The consolidated amortization payment issuer. */
	CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER(new Integer(2939),"CONSOLIDADO AMORTIZACION DE CAPITAL POR EMISOR","");
	
	

	/** The value. */
	private String value;
	
	/** The code. */
	private Integer code;
	
	/** The process. */
	private String process;

	/** The Constant list. */
	public static final List<ImportanceEventType> list= new ArrayList<ImportanceEventType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ImportanceEventType> lookup = new HashMap<Integer,ImportanceEventType >();

	static{
		for(ImportanceEventType type: EnumSet.allOf(ImportanceEventType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	/**
	 * Instantiates a new importance event type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param process the process
	 */
	private ImportanceEventType(Integer code, String value, String process) {
		this.value = value;
		this.code = code;
		this.process = process;
	}


	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}


	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the process.
	 *
	 * @return the process
	 */
	public String getProcess() {
		return process;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the importance event type
	 */
	public static ImportanceEventType get(Integer code)
	{
		return lookup.get(code);
	}


}
