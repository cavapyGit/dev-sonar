package com.pradera.model.corporatives.specialpayment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.negotiation.MechanismOperation;

@Entity
@Table(name="SPECIAL_PAYMENT_DETAIL")
public class SpecialPaymentDetail implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id custody operation pk. */
	@Id
	@SequenceGenerator(name="SPECIAL_PAYMENT_DETAIL_IDSPDETAILPK_GENERATOR", sequenceName="SQ_ID_SP_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SPECIAL_PAYMENT_DETAIL_IDSPDETAILPK_GENERATOR")
	@Column(name="ID_SP_DETAIL_PK")
	private Long idSpecialPaymentDetailPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SP_HEADER_FK")
	private SpecialPaymentHeader specialPaymentHeader;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
	private MechanismOperation mechanismOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@Column(name="PAYMENT_AMOUNT")
	private BigDecimal paymentAmount;
	
	@Column(name="RATE_AMOUNT")
	private BigDecimal rateAmount;
	
	@Column(name="IND_PAYED")
	private Integer indPayed;
	
	 /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;	
	
	@Transient
	private boolean blCheck;
	
	public Long getIdSpecialPaymentDetailPk() {
		return idSpecialPaymentDetailPk;
	}

	public void setIdSpecialPaymentDetailPk(Long idSpecialPaymentDetailPk) {
		this.idSpecialPaymentDetailPk = idSpecialPaymentDetailPk;
	}

	public SpecialPaymentHeader getSpecialPaymentHeader() {
		return specialPaymentHeader;
	}

	public void setSpecialPaymentHeader(SpecialPaymentHeader specialPaymentHeader) {
		this.specialPaymentHeader = specialPaymentHeader;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public BigDecimal getRateAmount() {
		return rateAmount;
	}

	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
		
	public Integer getIndPayed() {
		return indPayed;
	}

	public void setIndPayed(Integer indPayed) {
		this.indPayed = indPayed;
	}

	public boolean isBlCheck() {
		return blCheck;
	}

	public void setBlCheck(boolean blCheck) {
		this.blCheck = blCheck;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(Validations.validateIsNotNullAndNotEmpty(loggerUser)){
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
