package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the BLOCK_CORPORATIVE_RESULT database table.
 * 
 */
@Entity
@Table(name="BLOCK_CORPORATIVE_RESULT")
public class BlockCorporativeResult implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_BLOCK_CORPORATIVE_RESULT")
	@SequenceGenerator(name="BLOCK_CORPORATIVE_RESULT_GENERATOR", sequenceName="SQ_ID_BLOCK_CORPORATIVE_RESULT",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_CORPORATIVE_RESULT_GENERATOR")
	private long idBlockCorporativeResult;

	@Column(name="BLOCK_TYPE")
	private Integer blockType;

	@Column(name="BLOCKED_BALANCE")
	private BigDecimal blockedBalance;
	
	@Column(name="TAX_AMOUNT")
	private BigDecimal taxAmount;
	
	@Column(name="CUSTODY_AMOUNT")
	private BigDecimal custodyAmount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="IND_CASH_DIVIDEND")
	private Integer indCashDividend;

	@Column(name="IND_CONTRIBUTION_RETURN")
	private Integer indContributionReturn;

	@Column(name="IND_INTEREST")
	private Integer indInterest;

	@Column(name="IND_ORIGIN_DESTINY")
	private Integer indOriginDestiny;

	@Column(name="IND_PROCESSED")
	private Integer indProcessed;

	@Column(name="IND_REMANENT")
	private Integer indRemanent;

	@Column(name="IND_STOCK_DIVIDEND")
	private Integer indStockDividend;

	@Column(name="IND_SUSCRIPTION")
	private Integer indSuscription;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to CorporativeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;

	//bi-directional many-to-one association to CorporativeProcessResult
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_PROCESS_RESULT")
	private CorporativeProcessResult corporativeProcessResult;
    
    @Transient
    private boolean unblocked; 
    
    @Column(name="IND_AMORTIZATION")
	private Integer indAmortization;
    
    @Column(name="IND_RESCUE")
	private Integer indRescue;

    public BlockCorporativeResult() {
    	unblocked = false;
    	this.taxAmount= BigDecimal.ZERO;
    	this.custodyAmount= BigDecimal.ZERO;
    }

	public long getIdBlockCorporativeResult() {
		return this.idBlockCorporativeResult;
	}

	public void setIdBlockCorporativeResult(long idBlockCorporativeResult) {
		this.idBlockCorporativeResult = idBlockCorporativeResult;
	}

	public BigDecimal getBlockedBalance() {
		return this.blockedBalance;
	}

	public void setBlockedBalance(BigDecimal blockedBalance) {
		this.blockedBalance = blockedBalance;
	}


	public Security getSecurity() {
		return this.security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}


	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public CorporativeOperation getCorporativeOperation() {
		return this.corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}
	
	public CorporativeProcessResult getCorporativeProcessResult() {
		return this.corporativeProcessResult;
	}

	public void setCorporativeProcessResult(CorporativeProcessResult corporativeProcessResult) {
		this.corporativeProcessResult = corporativeProcessResult;
	}

	/**
	 * @return the blockType
	 */
	public Integer getBlockType() {
		return blockType;
	}

	/**
	 * @param blockType the blockType to set
	 */
	public void setBlockType(Integer blockType) {
		this.blockType = blockType; 
	}

	/**
	 * @return the idBlockOperationDetailPk
	 */
	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	/**
	 * @param idBlockOperationDetailPk the idBlockOperationDetailPk to set
	 */
	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	/**
	 * @return the idHolderAccountFk
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * @param idHolderAccountFk the idHolderAccountFk to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * @return the idHolderFk
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * @param idHolderFk the idHolderFk to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * @return the idParticipantFk
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * @param idParticipantFk the idParticipantFk to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * @return the indCashDividend
	 */
	public Integer getIndCashDividend() {
		return indCashDividend;
	}

	/**
	 * @param indCashDividend the indCashDividend to set
	 */
	public void setIndCashDividend(Integer indCashDividend) {
		this.indCashDividend = indCashDividend;
	}

	/**
	 * @return the indContributionReturn
	 */
	public Integer getIndContributionReturn() {
		return indContributionReturn;
	}

	/**
	 * @param indContributionReturn the indContributionReturn to set
	 */
	public void setIndContributionReturn(Integer indContributionReturn) {
		this.indContributionReturn = indContributionReturn;
	}

	/**
	 * @return the indInterest
	 */
	public Integer getIndInterest() {
		return indInterest;
	}

	/**
	 * @param indInterest the indInterest to set
	 */
	public void setIndInterest(Integer indInterest) {
		this.indInterest = indInterest;
	}

	/**
	 * @return the indOriginDestiny
	 */
	public Integer getIndOriginDestiny() {
		return indOriginDestiny;
	}

	/**
	 * @param indOriginDestiny the indOriginDestiny to set
	 */
	public void setIndOriginDestiny(Integer indOriginDestiny) {
		this.indOriginDestiny = indOriginDestiny;
	}

	/**
	 * @return the indProcessed
	 */
	public Integer getIndProcessed() {
		return indProcessed;
	}

	/**
	 * @param indProcessed the indProcessed to set
	 */
	public void setIndProcessed(Integer indProcessed) {
		this.indProcessed = indProcessed;
	}

	/**
	 * @return the indRemanent
	 */
	public Integer getIndRemanent() {
		return indRemanent;
	}

	/**
	 * @param indRemanent the indRemanent to set
	 */
	public void setIndRemanent(Integer indRemanent) {
		this.indRemanent = indRemanent;
	}

	/**
	 * @return the indStockDividend
	 */
	public Integer getIndStockDividend() {
		return indStockDividend;
	}

	/**
	 * @param indStockDividend the indStockDividend to set
	 */
	public void setIndStockDividend(Integer indStockDividend) {
		this.indStockDividend = indStockDividend;
	}

	/**
	 * @return the indSuscription
	 */
	public Integer getIndSuscription() {
		return indSuscription;
	}

	/**
	 * @param indSuscription the indSuscription to set
	 */
	public void setIndSuscription(Integer indSuscription) {
		this.indSuscription = indSuscription;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		
        return detailsMap;
	}

	/**
	 * @return the unblocked
	 */
	public boolean isUnblocked() {
		return unblocked;
	}

	/**
	 * @param unblocked the unblocked to set
	 */
	public void setUnblocked(boolean unblocked) {
		this.unblocked = unblocked;
	}

	/**
	 * @return the indAmortization
	 */
	public Integer getIndAmortization() {
		return indAmortization;
	}

	/**
	 * @param indAmortization the indAmortization to set
	 */
	public void setIndAmortization(Integer indAmortization) {
		this.indAmortization = indAmortization;
	}

	/**
	 * @return the indRescue
	 */
	public Integer getIndRescue() {
		return indRescue;
	}

	/**
	 * @param indRescue the indRescue to set
	 */
	public void setIndRescue(Integer indRescue) {
		this.indRescue = indRescue;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}

	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}
	
	
}