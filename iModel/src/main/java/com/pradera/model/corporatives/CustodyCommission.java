package com.pradera.model.corporatives;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the BENEFIT_PAYMENT_FILE database table.
 * 
 */
@Entity
@Table(name="CUSTODY_COMMISSION")
public class CustodyCommission implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CUSTODY_COMMISSION_GENERATOR", sequenceName="SQ_ID_CUSTODY_COMMISSION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CUSTODY_COMMISSION_GENERATOR")
	@Column(name="ID_CUSTODY_COMMISSION_PK")
	private Long idCustodyCommissionPk;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

	@Column(name="DAY_TYPE")
	private Integer dayType;

	@Column(name="YEAR_TYPE")
	private Integer yearType;

	@Column(name="IND_BISIESTO")
	private Integer indBisiesto;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@OneToMany(mappedBy="custodyCommission")
	private List<CustodyCommissionDetail> custodyCommissionDetail;


    public CustodyCommission() {
    }


    public Long getIdCustodyCommissionPk() {
		return idCustodyCommissionPk;
	}
	public void setIdCustodyCommissionPk(Long idCustodyCommissionPk) {
		this.idCustodyCommissionPk = idCustodyCommissionPk;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public Integer getIndBisiesto() {
		return indBisiesto;
	}
	public void setIndBisiesto(Integer indBisiesto) {
		this.indBisiesto = indBisiesto;
	}
	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}
	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public Integer getDayType() {
		return dayType;
	}
	public void setDayType(Integer dayType) {
		this.dayType = dayType;
	}
	public Integer getYearType() {
		return yearType;
	}
	public void setYearType(Integer yearType) {
		this.yearType = yearType;
	}


	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}


	public List<CustodyCommissionDetail> getCustodyCommissionDetail() {
		return custodyCommissionDetail;
	}
	public void setCustodyCommissionDetail(
			List<CustodyCommissionDetail> custodyCommissionDetail) {
		this.custodyCommissionDetail = custodyCommissionDetail;
	}
}