/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pradera.model.corporatives.stockcalculation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "STOCK_CALCULATION_MARKETFACT")
public class StockCalculationMarketfact implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_STOCK_CALC_MARKETFACT_PK")
    private Long idStockCalcMarketfactPk;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MARKET_RATE")
    private BigDecimal marketRate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MARKET_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date marketDate;
    @Column(name = "MARKET_PRICE")
    private BigDecimal marketPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_BALANCE")
    private BigDecimal totalBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AVAILABLE_BALANCE")
    private BigDecimal availableBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRANSIT_BALANCE")
    private BigDecimal transitBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PAWN_BALANCE")
    private BigDecimal pawnBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BAN_BALANCE")
    private BigDecimal banBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OTHER_BLOCK_BALANCE")
    private BigDecimal otherBlockBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RESERVE_BALANCE")
    private BigDecimal reserveBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACCREDITATION_BALANCE")
    private BigDecimal accreditationBalance;
    @Column(name = "PURCHASE_BALANCE")
    private BigDecimal purchaseBalance;
    @Column(name = "SALE_BALANCE")
    private BigDecimal saleBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REPORTING_BALANCE")
    private BigDecimal reportingBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REPORTED_BALANCE")
    private BigDecimal reportedBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MARGIN_BALANCE")
    private BigDecimal marginBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BORROWER_BALANCE")
    private BigDecimal borrowerBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LENDER_BALANCE")
    private BigDecimal lenderBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LOANABLE_BALANCE")
    private BigDecimal loanableBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OPPOSITION_BALANCE")
    private BigDecimal oppositionBalance;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP")
    private long lastModifyApp;
    @JoinColumn(name = "ID_STOCK_CALCULATION_FK", referencedColumnName = "ID_STOCK_CALCULATION_PK")
    @ManyToOne(optional = false)
    private StockCalculationProcess stockCalculationProcess;
    @JoinColumn(name = "ID_SECURITY_CODE_FK", referencedColumnName = "ID_SECURITY_CODE_PK")
    @ManyToOne(optional = false)
    private Security security;
    @JoinColumn(name = "ID_PARTICIPANT_FK", referencedColumnName = "ID_PARTICIPANT_PK")
    @ManyToOne(optional = false)
    private Participant participant;
    @JoinColumn(name = "ID_HOLDER_ACCOUNT_FK", referencedColumnName = "ID_HOLDER_ACCOUNT_PK")
    @ManyToOne(optional = false)
    private HolderAccount holderAccount;

    public StockCalculationMarketfact() {
    }

    public StockCalculationMarketfact(Long idStockCalcMarketfactPk) {
        this.idStockCalcMarketfactPk = idStockCalcMarketfactPk;
    }

    public StockCalculationMarketfact(Long idStockCalcMarketfactPk, Date marketDate, BigDecimal totalBalance, BigDecimal availableBalance, BigDecimal transitBalance, BigDecimal pawnBalance, BigDecimal banBalance, BigDecimal otherBlockBalance, BigDecimal reserveBalance, BigDecimal accreditationBalance, BigDecimal reportingBalance, BigDecimal reportedBalance, BigDecimal marginBalance, BigDecimal borrowerBalance, BigDecimal lenderBalance, BigDecimal loanableBalance, BigDecimal oppositionBalance, String lastModifyUser, Date lastModifyDate, String lastModifyIp, long lastModifyApp) {
        this.idStockCalcMarketfactPk = idStockCalcMarketfactPk;
        this.marketDate = marketDate;
        this.totalBalance = totalBalance;
        this.availableBalance = availableBalance;
        this.transitBalance = transitBalance;
        this.pawnBalance = pawnBalance;
        this.banBalance = banBalance;
        this.otherBlockBalance = otherBlockBalance;
        this.reserveBalance = reserveBalance;
        this.accreditationBalance = accreditationBalance;
        this.reportingBalance = reportingBalance;
        this.reportedBalance = reportedBalance;
        this.marginBalance = marginBalance;
        this.borrowerBalance = borrowerBalance;
        this.lenderBalance = lenderBalance;
        this.loanableBalance = loanableBalance;
        this.oppositionBalance = oppositionBalance;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdStockCalcMarketfactPk() {
        return idStockCalcMarketfactPk;
    }

    public void setIdStockCalcMarketfactPk(Long idStockCalcMarketfactPk) {
        this.idStockCalcMarketfactPk = idStockCalcMarketfactPk;
    }

    public BigDecimal getMarketRate() {
        return marketRate;
    }

    public void setMarketRate(BigDecimal marketRate) {
        this.marketRate = marketRate;
    }

    public Date getMarketDate() {
        return marketDate;
    }

    public void setMarketDate(Date marketDate) {
        this.marketDate = marketDate;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getTransitBalance() {
        return transitBalance;
    }

    public void setTransitBalance(BigDecimal transitBalance) {
        this.transitBalance = transitBalance;
    }

    public BigDecimal getPawnBalance() {
        return pawnBalance;
    }

    public void setPawnBalance(BigDecimal pawnBalance) {
        this.pawnBalance = pawnBalance;
    }

    public BigDecimal getBanBalance() {
        return banBalance;
    }

    public void setBanBalance(BigDecimal banBalance) {
        this.banBalance = banBalance;
    }

    public BigDecimal getOtherBlockBalance() {
        return otherBlockBalance;
    }

    public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
        this.otherBlockBalance = otherBlockBalance;
    }

    public BigDecimal getReserveBalance() {
        return reserveBalance;
    }

    public void setReserveBalance(BigDecimal reserveBalance) {
        this.reserveBalance = reserveBalance;
    }

    public BigDecimal getAccreditationBalance() {
        return accreditationBalance;
    }

    public void setAccreditationBalance(BigDecimal accreditationBalance) {
        this.accreditationBalance = accreditationBalance;
    }

    public BigDecimal getPurchaseBalance() {
        return purchaseBalance;
    }

    public void setPurchaseBalance(BigDecimal purchaseBalance) {
        this.purchaseBalance = purchaseBalance;
    }

    public BigDecimal getSaleBalance() {
        return saleBalance;
    }

    public void setSaleBalance(BigDecimal saleBalance) {
        this.saleBalance = saleBalance;
    }

    public BigDecimal getReportingBalance() {
        return reportingBalance;
    }

    public void setReportingBalance(BigDecimal reportingBalance) {
        this.reportingBalance = reportingBalance;
    }

    public BigDecimal getReportedBalance() {
        return reportedBalance;
    }

    public void setReportedBalance(BigDecimal reportedBalance) {
        this.reportedBalance = reportedBalance;
    }

    public BigDecimal getMarginBalance() {
        return marginBalance;
    }

    public void setMarginBalance(BigDecimal marginBalance) {
        this.marginBalance = marginBalance;
    }

    public BigDecimal getBorrowerBalance() {
        return borrowerBalance;
    }

    public void setBorrowerBalance(BigDecimal borrowerBalance) {
        this.borrowerBalance = borrowerBalance;
    }

    public BigDecimal getLenderBalance() {
        return lenderBalance;
    }

    public void setLenderBalance(BigDecimal lenderBalance) {
        this.lenderBalance = lenderBalance;
    }

    public BigDecimal getLoanableBalance() {
        return loanableBalance;
    }

    public void setLoanableBalance(BigDecimal loanableBalance) {
        this.loanableBalance = loanableBalance;
    }

    public BigDecimal getOppositionBalance() {
        return oppositionBalance;
    }

    public void setOppositionBalance(BigDecimal oppositionBalance) {
        this.oppositionBalance = oppositionBalance;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }


    public StockCalculationProcess getStockCalculationProcess() {
		return stockCalculationProcess;
	}

	public void setStockCalculationProcess(
			StockCalculationProcess stockCalculationProcess) {
		this.stockCalculationProcess = stockCalculationProcess;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idStockCalcMarketfactPk != null ? idStockCalcMarketfactPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StockCalculationMarketfact)) {
            return false;
        }
        StockCalculationMarketfact other = (StockCalculationMarketfact) object;
        if ((this.idStockCalcMarketfactPk == null && other.idStockCalcMarketfactPk != null) || (this.idStockCalcMarketfactPk != null && !this.idStockCalcMarketfactPk.equals(other.idStockCalcMarketfactPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pradera.model.corporatives.stockcalculation.StockCalculationMarketfact[ idStockCalcMarketfactPk=" + idStockCalcMarketfactPk + " ]";
    }
    
}
