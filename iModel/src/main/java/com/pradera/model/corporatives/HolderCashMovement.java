package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.type.BankAccountType;


/**
 * The persistent class for the BENEFIT_PAYMENT_ALLOCATION database table.
 * 
 */
@Entity
@Table(name="HOLDER_CASH_MOVEMENT")
public class HolderCashMovement implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOLDER_CASH_GENERATOR", sequenceName="SQ_ID_HOLDER_CASH_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_CASH_GENERATOR")
	@Column(name="ID_HOLDER_CASH_MOVEMENT_PK")
	private Long idHolderCashMovementPk;

	@Temporal( TemporalType.DATE)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_TYPE_FK")
	private FundsOperationType fundsOperationType;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BANK_FK")
	private Bank bank;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_BANK_FK")
	private Bank participantBank;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;

	@Column(name="FULL_NAME_HOLDER")
	private String fullName;

	@Column(name="DOCUMENT_HOLDER")
	private String documentHolder;

	@Column(name="HOLDER_AMOUNT")
	private BigDecimal holderAmount;

	@Column(name="CURRENCY")
	private Integer currency;

//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	@Transient
	private BlockOperation blockOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_BANK_FK")
	private HolderAccountBank holderAccountBank;

	@Column(name="BANK_ACCOUNT_NUMBER")
	private String bankAccountNumber;

	@Column(name="BANK_ACCOUNT_TYPE")
	private Integer bankAccountType;

	@Column(name="PROCESS_TYPE")
	private Integer processType;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;


	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="MOVEMENT_STATE")
	private Integer movementState;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_FK")	
	private FundsOperation fundsOperation;
	
	@Column(name="ID_REF_HOLDER_CASH_MOV_FK")
	private Integer idRefHolderCashMov;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_BANK_ACCOUNT_FK")
	private InstitutionBankAccount institutionBankAccount;
	
	@Transient
	private Boolean selected = Boolean.FALSE;
	@Transient
	private String bankAccountTypeDesc;
	@Transient
	private String currencyTypeDescription;
		
    public HolderCashMovement() {
    }

	public Long getIdHolderCashMovementPk() {
		return idHolderCashMovementPk;
	}
	public void setIdHolderCashMovementPk(Long idHolderCashMovementPk) {
		this.idHolderCashMovementPk = idHolderCashMovementPk;
	}

	public Date getMovementDate() {
		return movementDate;
	}
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public FundsOperationType getFundsOperationType() {
		return fundsOperationType;
	}
	public void setFundsOperationType(FundsOperationType fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}
	
//	public BenefitPaymentAllocation getBenefitPayment() {
//		return benefitPayment;
//	}
//	public void setBenefitPayment(BenefitPaymentAllocation benefitPayment) {
//		this.benefitPayment = benefitPayment;
//	}

	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public Holder getHolder() {
		return holder;
	}
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDocumentHolder() {
		return documentHolder;
	}
	public void setDocumentHolder(String documentHolder) {
		this.documentHolder = documentHolder;
	}

	public BigDecimal getHolderAmount() {
		return holderAmount;
	}
	public void setHolderAmount(BigDecimal holderAmount) {
		this.holderAmount = holderAmount;
	}

	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public HolderAccountBank getHolderAccountBank() {
		return holderAccountBank;
	}
	public void setHolderAccountBank(HolderAccountBank holderAccountBank) {
		this.holderAccountBank = holderAccountBank;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public Integer getBankAccountType() {
		return bankAccountType;
	}
	public void setBankAccountType(Integer bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the movementState
	 */
	public Integer getMovementState() {
		return movementState;
	}

	/**
	 * @param movementState the movementState to set
	 */
	public void setMovementState(Integer movementState) {
		this.movementState = movementState;
	}

	public Boolean getSelected() {
		return selected;
	}

	public String getBankAccountTypeDesc() {
		if(bankAccountType != null){
			return BankAccountType.get(bankAccountType).getValue();
		}else{
			return "";
		}
	}

	public void setBankAccountTypeDesc(String bankAccountTypeDesc) {
		this.bankAccountTypeDesc = bankAccountTypeDesc;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCurrencyTypeDescription() {
		return currencyTypeDescription;
	}
	public void setCurrencyTypeDescription(String currencyTypeDescription) {
		this.currencyTypeDescription = currencyTypeDescription;
	}
	public FundsOperation getFundsOperation() {
		return fundsOperation;
	}
	public void setFundsOperation(FundsOperation fundsOperation) {
		this.fundsOperation = fundsOperation;
	}
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	public BlockOperation getBlockOperation() {
		return blockOperation;
	}
	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}
	public Integer getProcessType() {
		return processType;
	}
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}
	public void setInstitutionBankAccount(InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}
	public Bank getParticipantBank() {
		return participantBank;
	}
	public void setParticipantBank(Bank participantBank) {
		this.participantBank = participantBank;
	}
}