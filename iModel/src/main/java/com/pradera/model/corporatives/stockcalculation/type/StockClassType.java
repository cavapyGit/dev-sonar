package com.pradera.model.corporatives.stockcalculation.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum StockClassType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/05/2013
 */
public enum StockClassType {
	
	/** The normal. */
	NORMAL(Integer.valueOf(1328)),
	
	/** The report. */
	REPORT(Integer.valueOf(1329));
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	public static final Map<Integer, StockClassType> lookup = new HashMap<Integer,StockClassType >();
	
	static{
		for(StockClassType type: EnumSet.allOf(StockClassType.class))
		{
			lookup.put(type.getCode(),type);
		}

	}

	/**
	 * Instantiates a new stock class type.
	 *
	 * @param code the code
	 */
	private StockClassType(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the stock class type
	 */
	public StockClassType get(Integer code) {
		return lookup.get(code);
	}
}
