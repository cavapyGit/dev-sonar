package com.pradera.model.corporatives.type;



import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*Master Pk 491*/
public enum CorporativeEventType {

	RIGHTS_TO_DELIVER(new Integer(1844),"ENTREGA DE DERECHOS"), 
	REORGANIZATION(new Integer(1845),"REORGANIZACION"); 

	private String value;
	private Integer code;

	public static final List<CorporativeEventType> list= new ArrayList<CorporativeEventType>();
	public static final Map<Integer, CorporativeEventType> lookup = new HashMap<Integer,CorporativeEventType >();

	static{
		for(CorporativeEventType type: EnumSet.allOf(CorporativeEventType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	private CorporativeEventType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static CorporativeEventType get(Integer code)
	{
		return lookup.get(code);
	}


}
