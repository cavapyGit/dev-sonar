package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the BENEFIT_PAYMENT_ALLOCATION database table.
 * 
 */
@Entity
@Table(name="payroll_consigned_operations")
public class PayrollConsignedOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;	
	
	@Id
	@SequenceGenerator(name="PAYROLL_CONSIGNED_OPERATIONS_GENERATOR", sequenceName="SQ_ID_PAYROLL_CONSIG_OPER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAYROLL_CONSIGNED_OPERATIONS_GENERATOR")
	@Column(name="ID_PAYROLL_CONSIGNED_OPER_PK")
	private Long idPayrollConsignedOperPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;
	
	@Column(name="SITUATION")
	private Integer situation;
	
	@Column(name="STATE")
	private Integer state;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PROCESS_DATE")
	private Date processDate;

	@Column(name="CURRENCY_PAYMENT")
	private Integer currencyPayment;
	
	@Column(name="CURRENCY")
	private String currency;
	
	@Column(name="INVOICE_NUMBER")
	private String invoiceNumber;

	@Column(name="ID_ALLOCATION_DETAIL_PK")
	private Integer idAllocationDetailPk;
	
	@Column(name="ACCOUNT_NUMBER_BANK_SOURCE")
	private String accountNumberBankSource;
	
	@Column(name="ACCOUNT_NUMBER_BANK_TARGET")
	private String accountNumberBankTarget;
	
	@Column(name="CODE_BANK")
	private String codeBank;
	
	@Column(name="DOCUMENT_TYPE_TARGET")
	private String documentTypeTarger;
	
	@Column(name="DOCUMENT_NUMBER_TARGET")
	private String documentNumberTarget;
	
	@Column(name="FULLNAME_TARGET")
	private String fullnameTarget;
	
	@Column(name="PAYMENT_AMOUNT")
	private BigDecimal paymentAmount;
	
	@Column(name="TRANSFERS_MOTIVE")
	private String transferMotive;
	
	@Column(name="EMAIL_TARGET")
	private String emailTarget;
	
	@Column(name="SMS_MESSAGE")
	private String smsMessage;
	
	@Column(name="PAYMENT_NUMBER")
	private Integer paymentNumber;
	
	@Column(name="GENERATED_FRAME")
	private String generatedFrame;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CONSIGNED_DATE")
	private Date consignedDate;

	@Column(name="PAYMENT_REFERENCE")
	private String paymentReference;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private Date initialDate;

	@Transient
	private Date finalDate;
	
    public PayrollConsignedOperation() {
    }
    
    
    
	public Long getIdPayrollConsignedOperPk() {
		return idPayrollConsignedOperPk;
	}



	public void setIdPayrollConsignedOperPk(Long idPayrollConsignedOperPk) {
		this.idPayrollConsignedOperPk = idPayrollConsignedOperPk;
	}



	public Issuer getIssuer() {
		return issuer;
	}



	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}



	public Integer getSituation() {
		return situation;
	}



	public void setSituation(Integer situation) {
		this.situation = situation;
	}



	public Date getProcessDate() {
		return processDate;
	}



	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}



	public String getCurrency() {
		return currency;
	}



	public void setCurrency(String currency) {
		this.currency = currency;
	}



	public Integer getIdAllocationDetailPk() {
		return idAllocationDetailPk;
	}



	public void setIdAllocationDetailPk(Integer idAllocationDetailFk) {
		this.idAllocationDetailPk = idAllocationDetailFk;
	}



	public String getAccountNumberBankSource() {
		return accountNumberBankSource;
	}



	public void setAccountNumberBankSource(String accountNumberBankSource) {
		this.accountNumberBankSource = accountNumberBankSource;
	}



	public String getAccountNumberBankTarget() {
		return accountNumberBankTarget;
	}



	public void setAccountNumberBankTarget(String accountNumberBankTarget) {
		this.accountNumberBankTarget = accountNumberBankTarget;
	}



	public String getCodeBank() {
		return codeBank;
	}



	public void setCodeBank(String codeBank) {
		this.codeBank = codeBank;
	}



	public String getDocumentTypeTarger() {
		return documentTypeTarger;
	}



	public void setDocumentTypeTarger(String documentTypeTarger) {
		this.documentTypeTarger = documentTypeTarger;
	}



	public String getDocumentNumberTarget() {
		return documentNumberTarget;
	}



	public void setDocumentNumberTarget(String documentNumberTarget) {
		this.documentNumberTarget = documentNumberTarget;
	}



	public String getFullnameTarget() {
		return fullnameTarget;
	}



	public void setFullnameTarget(String fullnameTarget) {
		this.fullnameTarget = fullnameTarget;
	}



	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}



	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}



	public String getTransferMotive() {
		return transferMotive;
	}



	public void setTransferMotive(String transferMotive) {
		this.transferMotive = transferMotive;
	}



	public String getEmailTarget() {
		return emailTarget;
	}



	public void setEmailTarget(String emailTarget) {
		this.emailTarget = emailTarget;
	}



	public String getSmsMessage() {
		return smsMessage;
	}



	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}



	public Integer getPaymentNumber() {
		return paymentNumber;
	}



	public void setPaymentNumber(Integer paymentNumber) {
		this.paymentNumber = paymentNumber;
	}



	public String getGeneratedFrame() {
		return generatedFrame;
	}



	public void setGeneratedFrame(String generatedFrame) {
		this.generatedFrame = generatedFrame;
	}



	public Date getConsignedDate() {
		return consignedDate;
	}



	public void setConsignedDate(Date consignedDate) {
		this.consignedDate = consignedDate;
	}



	public Date getRegistryDate() {
		return registryDate;
	}



	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}



	public String getRegistryUser() {
		return registryUser;
	}



	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}



	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	public String getLastModifyIp() {
		return lastModifyIp;
	}



	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	public String getLastModifyUser() {
		return lastModifyUser;
	}



	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	@Override
	public void setAudit(LoggerUser loggerUser) {		 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}



	public Participant getParticipant() {
		return participant;
	}



	public void setParticipant(Participant participant) {
		this.participant = participant;
	}



	public Security getSecurity() {
		return security;
	}



	public void setSecurity(Security security) {
		this.security = security;
	}



	public Integer getState() {
		return state;
	}



	public void setState(Integer state) {
		this.state = state;
	}



	public Integer getCurrencyPayment() {
		return currencyPayment;
	}



	public void setCurrencyPayment(Integer currencyPayment) {
		this.currencyPayment = currencyPayment;
	}



	public String getInvoiceNumber() {
		return invoiceNumber;
	}



	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}



	public Date getInitialDate() {
		return initialDate;
	}



	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}



	public Date getFinalDate() {
		return finalDate;
	}



	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}



	public String getPaymentReference() {
		return paymentReference;
	}



	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}
}