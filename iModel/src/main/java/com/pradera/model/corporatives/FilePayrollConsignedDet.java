package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the BENEFIT_PAYMENT_ALLOCATION database table.
 * 
 */
@Entity
@Table(name="file_payroll_consigned_det")
public class FilePayrollConsignedDet implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;	
		
	@Id
	@SequenceGenerator(name="FILE_PAYROLL_CONSIGNED_GENERATOR", sequenceName="SQ_ID_FPAYROLL_CONSIG_DET_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FILE_PAYROLL_CONSIGNED_GENERATOR")
	@Column(name="ID_FILE_PAYROLL_CONSIG_DET_PK")
	private Long idFilePayrollConsigDetPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PAYROLL_CONSIGNED_OPER_FK")
	private PayrollConsignedOperation payrollConsignedOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FILE_PAYROLL_CONSIGNED_FK",referencedColumnName="ID_FILE_PAYROLL_CONSIGNED_PK")
	private FilePayrollConsigned filePayrollConsigned;
	
	@Column(name="state")
	private Integer state;
	
	@Column(name="generated_frame")
	private String generatedFrame;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
    public FilePayrollConsignedDet() {
    }
    

	public Long getIdFilePayrollConsigDetPk() {
		return idFilePayrollConsigDetPk;
	}




	public void setIdFilePayrollConsigDetPk(Long idFilePayrollConsigDetPk) {
		this.idFilePayrollConsigDetPk = idFilePayrollConsigDetPk;
	}




	public PayrollConsignedOperation getPayrollConsignedOperation() {
		return payrollConsignedOperation;
	}




	public void setPayrollConsignedOperation(PayrollConsignedOperation payrollConsignedOperation) {
		this.payrollConsignedOperation = payrollConsignedOperation;
	}




	public FilePayrollConsigned getFilePayrollConsigned() {
		return filePayrollConsigned;
	}




	public void setFilePayrollConsigned(FilePayrollConsigned filePayrollConsigned) {
		this.filePayrollConsigned = filePayrollConsigned;
	}




	public Integer getState() {
		return state;
	}




	public void setState(Integer state) {
		this.state = state;
	}




	public Date getRegistryDate() {
		return registryDate;
	}



	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}



	public String getRegistryUser() {
		return registryUser;
	}



	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}



	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	public String getLastModifyIp() {
		return lastModifyIp;
	}



	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	public String getLastModifyUser() {
		return lastModifyUser;
	}



	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	@Override
	public void setAudit(LoggerUser loggerUser) {		 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}


	public String getGeneratedFrame() {
		return generatedFrame;
	}


	public void setGeneratedFrame(String generatedFrame) {
		this.generatedFrame = generatedFrame;
	}
}