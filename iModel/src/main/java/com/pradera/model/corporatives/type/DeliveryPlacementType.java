package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum DeliveryPlacementType {

	DEPOSITARY(new Integer(230),"SESION DIRECTORIO"),
	ISSUER(new Integer(1339),"EMISOR");

	private String value;
	private Integer code;

	public static final List<DeliveryPlacementType> list= new ArrayList<DeliveryPlacementType>();
	public static final Map<Integer, DeliveryPlacementType> lookup = new HashMap<Integer,DeliveryPlacementType >();

	static{
		for(DeliveryPlacementType type: EnumSet.allOf(DeliveryPlacementType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	private DeliveryPlacementType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static DeliveryPlacementType get(Integer code)
	{
		return lookup.get(code);
	}
}

