package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the BENEFIT_PAYMENT_ALLOCATION database table.
 * 
 */
@Entity
@Table(name="PAYMENT_ALLOCATION_DETAIL")
public class PaymentAllocationDetail implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BENEFIT_ALLOCATION_DETAIL", sequenceName="SQ_ID_ALLOCATION_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BENEFIT_ALLOCATION_DETAIL")
	@Column(name="ID_ALLOCATION_DETAIL_PK")
	private Long idAllocationDetailPK;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BENEFIT_PAYMENT_FK")
	private BenefitPaymentAllocation benefitPaymentAllocation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BANK_FK")
	private Bank bank;

	@Column(name="TRANSACTION_AMOUNT")
	private BigDecimal transactionAmount;

	@Column(name="TAX_AMOUNT")
	private BigDecimal taxAmount;

	@Column(name="CUSTODY_AMOUNT")
	private BigDecimal custodyAmount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_BANK_FK")
	private HolderAccountBank holderAccountBank;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_CASH_ACCOUNT_FK")
	private InstitutionCashAccount institutionCashAccount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_BANK_ACCOUNT_FK")
	private InstitutionBankAccount institutionBankAccount;

	@Column(name="ACCOUNT_NUMBER")
	private String accountNumber;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="PROCESS_TYPE")
	private Integer processType;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@OneToMany(mappedBy="paymentAllocationDetail",fetch=FetchType.LAZY)
	private List<BlockBenefitDetail> blockBenefitDetail;

    public PaymentAllocationDetail() {
    }


	@Override
	public void setAudit(LoggerUser loggerUser) {		 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdAllocationDetailPK() {
		return idAllocationDetailPK;
	}
	public void setIdAllocationDetailPK(Long idAllocationDetailPK) {
		this.idAllocationDetailPK = idAllocationDetailPK;
	}
	public BenefitPaymentAllocation getBenefitPaymentAllocation() {
		return benefitPaymentAllocation;
	}
	public void setBenefitPaymentAllocation(
			BenefitPaymentAllocation benefitPaymentAllocation) {
		this.benefitPaymentAllocation = benefitPaymentAllocation;
	}
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public Holder getHolder() {
		return holder;
	}
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}
	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}
	public HolderAccountBank getHolderAccountBank() {
		return holderAccountBank;
	}
	public void setHolderAccountBank(HolderAccountBank holderAccountBank) {
		this.holderAccountBank = holderAccountBank;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public Integer getProcessType() {
		return processType;
	}
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}
	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}
	public List<BlockBenefitDetail> getBlockBenefitDetail() {
		return blockBenefitDetail;
	}
	public void setBlockBenefitDetail(List<BlockBenefitDetail> blockBenefitDetail) {
		this.blockBenefitDetail = blockBenefitDetail;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public Security getSecurity() {
		return security;
	}
	public void setSecurity(Security security) {
		this.security = security;
	}
	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}
	public void setInstitutionCashAccount(InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}
	public void setInstitutionBankAccount(InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}
}