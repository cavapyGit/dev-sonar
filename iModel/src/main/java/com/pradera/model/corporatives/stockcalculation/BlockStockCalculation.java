package com.pradera.model.corporatives.stockcalculation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Security;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BlockStockCalculation.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/04/2013
 */
@Entity
@Table(name="BLOCK_STOCK_CALCULATION")
public class BlockStockCalculation implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id block stock calculation pk. */
	@Id
	@Column(name="ID_BLOCK_STOCK_CALCULATION_PK")
	private Long idBlockStockCalculationPk;

	/** The block balance. */
	@Column(name="BLOCK_BALANCE")
	private BigDecimal blockBalance;

	/** The block type. */
	@Column(name="BLOCK_TYPE")
	private Integer blockType;

	/** The id block operation detail fk. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

	/** The custody operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CUSTODY_OPERATION_FK")
	private CustodyOperation custodyOperation;

	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	/** The holder. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;

	/** The security. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	/** The ind cash dividend. */
	@Column(name="IND_CASH_DIVIDEND")
	private Integer indCashDividend;

	/** The ind contribution return. */
	@Column(name="IND_CONTRIBUTION_RETURN")
	private Integer indContributionReturn;

	/** The ind interest. */
	@Column(name="IND_INTEREST")
	private Integer indInterest;

	/** The ind stock dividend. */
	@Column(name="IND_STOCK_DIVIDEND")
	private Integer indStockDividend;

	/** The ind suscription. */
	@Column(name="IND_SUSCRIPTION")
	private Integer indSuscription;

	/** The ind remanent. */
	@Column(name="IND_REMANENT")
	private Integer indRemanent;

	/** The ind amortization. */
	@Column(name="IND_AMORTIZATION")
	private Integer indAmortization;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Long lastModifyApp;

	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to StockCalculationProcess
	/** The stock calculation process. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_STOCK_CALCULATION_FK")
	private StockCalculationProcess stockCalculationProcess;

	@Column(name="IND_RESCUE")
	private Integer indRescue;



	/**
	 * Instantiates a new block stock calculation.
	 */
	public BlockStockCalculation() {
	}

	/**
	 * Gets the id block stock calculation pk.
	 *
	 * @return the id block stock calculation pk
	 */
	public Long getIdBlockStockCalculationPk() {
		return idBlockStockCalculationPk;
	}

	/**
	 * Sets the id block stock calculation pk.
	 *
	 * @param idBlockStockCalculationPk the new id block stock calculation pk
	 */
	public void setIdBlockStockCalculationPk(Long idBlockStockCalculationPk) {
		this.idBlockStockCalculationPk = idBlockStockCalculationPk;
	}

	/**
	 * Gets the block balance.
	 *
	 * @return the block balance
	 */
	public BigDecimal getBlockBalance() {
		return blockBalance;
	}

	/**
	 * Sets the block balance.
	 *
	 * @param blockBalance the new block balance
	 */
	public void setBlockBalance(BigDecimal blockBalance) {
		this.blockBalance = blockBalance;
	}

	/**
	 * Gets the block type.
	 *
	 * @return the block type
	 */
	public Integer getBlockType() {
		return blockType;
	}

	/**
	 * Sets the block type.
	 *
	 * @param blockType the new block type
	 */
	public void setBlockType(Integer blockType) {
		this.blockType = blockType;
	}


	/**
	 * Gets the block operation.
	 *
	 * @return the idBlockOperationDetailFk
	 */
	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	/**
	 * Sets the block operation.
	 *
	 * @param blockOperation the new block operation
	 */
	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	/**
	 * Gets the custody operation.
	 *
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * Sets the custody operation.
	 *
	 * @param custodyOperation the new custody operation
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the ind cash dividend.
	 *
	 * @return the ind cash dividend
	 */
	public Integer getIndCashDividend() {
		return indCashDividend;
	}

	/**
	 * Sets the ind cash dividend.
	 *
	 * @param indCashDividend the new ind cash dividend
	 */
	public void setIndCashDividend(Integer indCashDividend) {
		this.indCashDividend = indCashDividend;
	}

	/**
	 * Gets the ind contribution return.
	 *
	 * @return the ind contribution return
	 */
	public Integer getIndContributionReturn() {
		return indContributionReturn;
	}

	/**
	 * Sets the ind contribution return.
	 *
	 * @param indContributionReturn the new ind contribution return
	 */
	public void setIndContributionReturn(Integer indContributionReturn) {
		this.indContributionReturn = indContributionReturn;
	}

	/**
	 * Gets the ind interest.
	 *
	 * @return the ind interest
	 */
	public Integer getIndInterest() {
		return indInterest;
	}

	/**
	 * Sets the ind interest.
	 *
	 * @param indInterest the new ind interest
	 */
	public void setIndInterest(Integer indInterest) {
		this.indInterest = indInterest;
	}

	/**
	 * Gets the ind stock dividend.
	 *
	 * @return the ind stock dividend
	 */
	public Integer getIndStockDividend() {
		return indStockDividend;
	}

	/**
	 * Sets the ind stock dividend.
	 *
	 * @param indStockDividend the new ind stock dividend
	 */
	public void setIndStockDividend(Integer indStockDividend) {
		this.indStockDividend = indStockDividend;
	}

	/**
	 * Gets the ind suscription.
	 *
	 * @return the ind suscription
	 */
	public Integer getIndSuscription() {
		return indSuscription;
	}

	/**
	 * Sets the ind suscription.
	 *
	 * @param indSuscription the new ind suscription
	 */
	public void setIndSuscription(Integer indSuscription) {
		this.indSuscription = indSuscription;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the ind remanent.
	 *
	 * @return the ind remanent
	 */
	public Integer getIndRemanent() {
		return indRemanent;
	}

	/**
	 * Sets the ind remanent.
	 *
	 * @param indRemanent the new ind remanent
	 */
	public void setIndRemanent(Integer indRemanent) {
		this.indRemanent = indRemanent;
	}

	/**
	 * Gets the ind amortization.
	 *
	 * @return the ind amortization
	 */
	public Integer getIndAmortization() {
		return indAmortization;
	}

	/**
	 * Sets the ind amortization.
	 *
	 * @param indAmortization the new ind amortization
	 */
	public void setIndAmortization(Integer indAmortization) {
		this.indAmortization = indAmortization;
	}

	/**
	 * Gets the stock calculation process.
	 *
	 * @return the stock calculation process
	 */
	public StockCalculationProcess getStockCalculationProcess() {
		return stockCalculationProcess;
	}

	/**
	 * Sets the stock calculation process.
	 *
	 * @param stockCalculationProcess the new stock calculation process
	 */
	public void setStockCalculationProcess(
			StockCalculationProcess stockCalculationProcess) {
		this.stockCalculationProcess = stockCalculationProcess;
	}

	public Integer getIndRescue() {
		return indRescue;
	}

	public void setIndRescue(Integer indRescue) {
		this.indRescue = indRescue;
	}



}