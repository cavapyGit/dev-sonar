package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;

@Entity
@Table(name="BLOCK_BENEFIT_DETAIL")
public class BlockBenefitDetail implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_BLOCK_BENEFIT_DETAIL_PK")
	@SequenceGenerator(name="BLOCK_BENEFIT_DETAIL_GENERATOR", sequenceName="S_BLOCK_BENEFIT_DETAIL" ,initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_BENEFIT_DETAIL_GENERATOR")
	private Long idBlockBenefitDetailPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_ALLOCATION_DETAIL_FK")
	private PaymentAllocationDetail paymentAllocationDetail;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

	@Column(name="GROSS_BLOCK_AMOUNT")
	private BigDecimal grossBlockAmount;

	@Column(name="TAX_AMOUNT")
	private BigDecimal taxAmount;

	@Column(name="CUSTODY_AMOUNT")
	private BigDecimal custodyAmount;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;


    public BlockBenefitDetail() {
    	
    }


	@Override
    public void setAudit(LoggerUser loggerUser) {
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}


	public Long getIdBlockBenefitDetailPk() {
		return idBlockBenefitDetailPk;
	}
	public void setIdBlockBenefitDetailPk(Long idBlockBenefitDetailPk) {
		this.idBlockBenefitDetailPk = idBlockBenefitDetailPk;
	}
	public BigDecimal getGrossBlockAmount() {
		return grossBlockAmount;
	}
	public void setGrossBlockAmount(BigDecimal grossBlockAmount) {
		this.grossBlockAmount = grossBlockAmount;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}
	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public BlockOperation getBlockOperation() {
		return blockOperation;
	}
	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}
	public PaymentAllocationDetail getPaymentAllocationDetail() {
		return paymentAllocationDetail;
	}
	public void setPaymentAllocationDetail(
			PaymentAllocationDetail paymentAllocationDetail) {
		this.paymentAllocationDetail = paymentAllocationDetail;
	}
}