package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AccordType.
 */
public enum FilePayrollType {

		/** The coupon payment. */
		PAGO_DERECHO(new Integer(2944),"PAGO DERECHO"),
		LIQUIDACION(new Integer(2945),"LIQUIDACION"),
		;

		/** The value. */
		private String value;
		
		/** The code. */
		private Integer code;

		/** The Constant list. */
		public static final List<FilePayrollType> list= new ArrayList<FilePayrollType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, FilePayrollType> lookup = new HashMap<Integer,FilePayrollType >();

		static{
			for(FilePayrollType type: EnumSet.allOf(FilePayrollType.class))
			{
				list.add(type);
				lookup.put(type.getCode(),type);

			}

		}


		/**
		 * Instantiates a new accord type.
		 *
		 * @param code the code
		 * @param value the value
		 */
		private FilePayrollType(Integer code, String value) {
			this.value = value;
			this.code = code;
		}


		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}


		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}

		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the accord type
		 */
		public static FilePayrollType get(Integer code)
		{
			return lookup.get(code);
		}


	}
