package com.pradera.model.corporatives.stockcalculation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportStockCalculation.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/04/2013
 */
@Entity
@Table(name="REPORT_STOCK_CALCULATION")
public class ReportStockCalculation implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id report stock calculation pk. */
	@Id
	@Column(name="ID_REPORT_STOCK_CALCULATION_PK")
	private Long idReportStockCalculationPk;

	/** The balance type. */
	@Column(name="GUARANTEE_TYPE")
	private Integer guaranteeType;

	/** The guarantee balance. */
	@Column(name="GUARANTEE_BALANCE")
	private BigDecimal guaranteeBalance;

	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	/** The holder account operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK")
	private HolderAccountOperation holderAccountOperation;

	/** The security. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	/** The id mechanism operation fk. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
	private MechanismOperation mechanismOperation;

	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Long lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The operation role. */
	@Column(name="OPERATION_ROLE")
	private Integer operationRole;

	//bi-directional many-to-one association to StockCalculationProcess
    /** The stock calculation process. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_STOCK_CALCULATION_FK")
	private StockCalculationProcess stockCalculationProcess;

    /**
     * Instantiates a new report stock calculation.
     */
    public ReportStockCalculation() {
    }

	/**
	 * Gets the id report stock calculation pk.
	 *
	 * @return the id report stock calculation pk
	 */
	public Long getIdReportStockCalculationPk() {
		return idReportStockCalculationPk;
	}

	/**
	 * Sets the id report stock calculation pk.
	 *
	 * @param idReportStockCalculationPk the new id report stock calculation pk
	 */
	public void setIdReportStockCalculationPk(Long idReportStockCalculationPk) {
		this.idReportStockCalculationPk = idReportStockCalculationPk;
	}

	

	public Integer getGuaranteeType() {
		return guaranteeType;
	}

	public void setGuaranteeType(Integer guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	/**
	 * Gets the guarantee balance.
	 *
	 * @return the guarantee balance
	 */
	public BigDecimal getGuaranteeBalance() {
		return guaranteeBalance;
	}

	/**
	 * Sets the guarantee balance.
	 *
	 * @param guaranteeBalance the new guarantee balance
	 */
	public void setGuaranteeBalance(BigDecimal guaranteeBalance) {
		this.guaranteeBalance = guaranteeBalance;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}

	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	

	/**
	 * @return the idMechanismOperationFk
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	/**
	 * @param idMechanismOperationFk the idMechanismOperationFk to set
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the operation role.
	 *
	 * @return the operation role
	 */
	public Integer getOperationRole() {
		return operationRole;
	}

	/**
	 * Sets the operation role.
	 *
	 * @param operationRole the new operation role
	 */
	public void setOperationRole(Integer operationRole) {
		this.operationRole = operationRole;
	}

	/**
	 * Gets the stock calculation process.
	 *
	 * @return the stock calculation process
	 */
	public StockCalculationProcess getStockCalculationProcess() {
		return stockCalculationProcess;
	}

	/**
	 * Sets the stock calculation process.
	 *
	 * @param stockCalculationProcess the new stock calculation process
	 */
	public void setStockCalculationProcess(
			StockCalculationProcess stockCalculationProcess) {
		this.stockCalculationProcess = stockCalculationProcess;
	}

	
	
}