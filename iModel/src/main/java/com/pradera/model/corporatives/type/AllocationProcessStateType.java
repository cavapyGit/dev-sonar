package com.pradera.model.corporatives.type;



import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AllocationProcessStateType {

	PRELIMINAR(new Integer(1712),"PRELIMINAR"),
	DEFINITIVE(new Integer(1713),"DEFINITIVO");

	private String value;
	private Integer code;

	public static final List<AllocationProcessStateType> list= new ArrayList<AllocationProcessStateType>();
	public static final Map<Integer, AllocationProcessStateType> lookup = new HashMap<Integer,AllocationProcessStateType >();

	static{
		for(AllocationProcessStateType type: EnumSet.allOf(AllocationProcessStateType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	private AllocationProcessStateType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static AllocationProcessStateType get(Integer code)
	{
		return lookup.get(code);
	}


}
