package com.pradera.model.corporatives.type;



import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AllocationProcessType {

	DIRECT_PAYMENT(new Integer(1714),"TITULAR - PAGO DIRECTO"),
	INDIRECT_PAYMENT(new Integer(1715),"DEPOSITANTE - PAGO INDIRECTO"),
	BLOCK_BALANCE_RETENTION(new Integer(1723), "RENTENCION SALDO BLOQUEADO"),

	NO_BANK_ACCOUNT_RETENTION(new Integer(1724), "RENTENCION SIN CUENTA BANCARIA"),
	HOLDER_ACCOUNT_RETENTION(new Integer(1716), "RENTENCION CUENTA BLOQUEADA"),
	RNT_RETENTION(new Integer(1717), "RENTENCION CUI BLOQUEADO");

	private String value;
	private Integer code;

	public static final List<AllocationProcessType> list= new ArrayList<AllocationProcessType>();
	public static final Map<Integer, AllocationProcessType> lookup = new HashMap<Integer,AllocationProcessType >();

	static{
		for(AllocationProcessType type: EnumSet.allOf(AllocationProcessType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	private AllocationProcessType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static AllocationProcessType get(Integer code)
	{
		return lookup.get(code);
	}


}
