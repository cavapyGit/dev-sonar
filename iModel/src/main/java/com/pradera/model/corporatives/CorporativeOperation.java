package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.type.AccordType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeSituationType;
import com.pradera.model.corporatives.type.DeliveryPlacementType;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the CORPORATIVE_OPERATION database table.
 * 
 */
@Entity
@Table(name="CORPORATIVE_OPERATION")
@NamedQueries({
	@NamedQuery(name = CorporativeOperation.CORPORATIVE_OPERATION_VALIDATION_PARAMETER, query = "SELECT new com.pradera.model.corporatives.CorporativeOperation(co.idCorporativeOperationPk,co.cutoffDate,co.registryDate,co.securities.idSecurityCodePk) FROM CorporativeOperation co WHERE co.corporativeEventType.corporativeEventType =:eventParam AND co.state  not in :stateList"),
	@NamedQuery(name = CorporativeOperation.CORPORATIVE_OPERATION_VALIDATION_PARAMETER_PAYMENT_REMANENT, query = "SELECT new com.pradera.model.corporatives.CorporativeOperation(co.idCorporativeOperationPk,co.cutoffDate,co.registryDate,co.securities.idSecurityCodePk) FROM CorporativeOperation co WHERE co.corporativeEventType.corporativeEventType =:eventParam AND co.state !=:firstState"),
	@NamedQuery(name = CorporativeOperation.CORPORATIVE_OPERATION_VALIDATION_PARAMETER_DEMATERIALIZATION, query = "SELECT new com.pradera.model.corporatives.CorporativeOperation(co.idCorporativeOperationPk,co.state,co.securities.idSecurityCodePk) FROM CorporativeOperation co WHERE co.corporativeEventType.corporativeEventType in :eventTypeList AND co.state in :stateList"),
	@NamedQuery(name = CorporativeOperation.CORPORATIVE_OPERATION_HOLDER_PROCESS,query=" select substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), sum(scb.totalBalance) "
			+ "from StockCalculationBalance scb, HolderAccount ha where scb.stockCalculationProcess.idStockCalculationPk = :stockprocessid and scb.holderAccount.idHolderAccountPk = ha.idHolderAccountPk group by substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,1)-locate('(',ha.alternateCode,1))) "),
			
	@NamedQuery(name = CorporativeOperation.CORPORATIVE_OPERATION_BY_RETIREMENT,query=" Select c from CorporativeOperation c inner join fetch c.corporativeEventType where c.retirementOperation.idRetirementOperationPk=:idRetirementOperationPrm")
})
public class CorporativeOperation implements Serializable,Auditable {



	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String CORPORATIVE_OPERATION_VALIDATION_PARAMETER = "CorporativeOperation.searchValidationParameters";
	public static final String CORPORATIVE_OPERATION_VALIDATION_PARAMETER_PAYMENT_REMANENT = "Corporativeoperation.searchValidationsParametersPaymentRemanent";
	public static final String CORPORATIVE_OPERATION_VALIDATION_PARAMETER_DEMATERIALIZATION="CorporativeOperation.searchValidationsParameterDematerialization";
	public static final String CORPORATIVE_OPERATION_HOLDER_PROCESS="CorporativeOperation.getHoldersProcessCorporate";
	public static final String CORPORATIVE_OPERATION_BY_RETIREMENT="CorporativeOperation.getCorporativeOperationByRetirement";

	/** The id corporative operation pk. */
	@Id
	@Column(name="ID_CORPORATIVE_OPERATION_PK")
	@SequenceGenerator(name="CORPORATIVE_OPERATION_GENERATOR", sequenceName="SQ_ID_CORPORATIVE_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CORPORATIVE_OPERATION_GENERATOR")
	private Long idCorporativeOperationPk;

	/** The id stock calculation fk. */
	@Column(name="ID_STOCK_CALCULATION")
	private Long idStockCalculation;
	
	/** The accord date. */
	@Temporal( TemporalType.DATE)
	@Column(name="ACCORD_DATE")
	private Date accordDate; 

	/** The accord type. */
	@Column(name="ACCORD_TYPE")
	private Integer accordType;

	/** The commission amount. */
	@Column(name="COMMISSION_AMOUNT")
	private BigDecimal commissionAmount;

	/** The corporative event type. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CORPORATIVE_EVENT_TYPE")
	private CorporativeEventType corporativeEventType;

	/** The creation date. */
	@Temporal( TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	/** The currency. */
	@Column(name="CURRENCY")
	private Integer currency;

	/** The custody amount. */
	@Column(name="CUSTODY_AMOUNT")
	private BigDecimal custodyAmount;

	/** The cutoff date. */
	@Temporal( TemporalType.DATE)
	@Column(name="CUTOFF_DATE")
	private Date cutoffDate;

	/** The delivery date. */
	@Temporal( TemporalType.DATE)
	@Column(name="DELIVERY_DATE")
	private Date deliveryDate;

	/** The delivery factor. */
	@Column(name="DELIVERY_FACTOR")
	private BigDecimal deliveryFactor;

	/** The delivery placement. */
	@Column(name="DELIVERY_PLACEMENT")
	private Integer deliveryPlacement;

	//bi-directional many-to-one association to issuer
	/** The issuer. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	//bi-directional many-to-one association to securities
	/** The securities. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ORIGIN_SECURITY_CODE_FK")
	private Security securities;

	/** The target security. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_SECURITY_CODE_FK")
	private Security targetSecurity;

	/** The ind affect balance. */
	@Column(name="IND_AFFECT_BALANCE")
	private Integer indAffectBalance;

	/** The ind excluded commissions. */
	@Column(name="IND_EXCLUDED_COMMISSIONS")
	private Integer indExcludedCommissions;

	/** The ind remanent. */
	@Column(name="IND_REMANENT")
	private Integer indRemanent;

	/** The ind round. */
	@Column(name="IND_ROUND")
	private Integer indRound;

	/** The ind tax. */
	@Column(name="IND_TAX")
	private Integer indTax;
	
	@Column(name="IND_PENALITY")
	private Integer indPenality = BooleanType.NO.getCode();
	
	@Column(name="PENALITY_RATE")
	private BigDecimal penalityRate;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The new circulation balance. */
	@Column(name="NEW_CIRCULATION_BALANCE")
	private BigDecimal newCirculationBalance;

	/** The new nominal value. */
	@Column(name="NEW_NOMINAL_VALUE")
	private BigDecimal newNominalValue;

	/** The new share capital. */
	@Column(name="NEW_SHARE_CAPITAL")
	private BigDecimal newShareCapital;

	/** The new share balance. */
	@Column(name="NEW_SHARE_BALANCE")
	private BigDecimal newShareBalance;

	/** The old share balance. */
	@Column(name="OLD_SHARE_BALANCE")
	private BigDecimal oldShareBalance;

	/** The old circulation balance. */
	@Column(name="OLD_CIRCULATION_BALANCE")
	private BigDecimal oldCirculationBalance;

	/** The old nominal value. */
	@Column(name="OLD_NOMINAL_VALUE")
	private BigDecimal oldNominalValue;

	/** The old share capital. */
	@Column(name="OLD_SHARE_CAPITAL")
	private BigDecimal oldShareCapital;

	/** The origin ratio. */
	@Column(name="ORIGIN_RATIO")
	private BigDecimal originRatio;

	/** The payment amount. */
	@Column(name="PAYMENT_AMOUNT")
	private BigDecimal paymentAmount;
	
	/** The payment amount. */
	@Column(name="ISSUER_CONFIRMED_AMOUNT")
	private BigDecimal issuerConfirmedAmount;

	/** The reception date. */
	@Temporal( TemporalType.DATE)
	@Column(name="RECEPTION_DATE")
	private Date receptionDate;

	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The retirement factor. */
	@Column(name="RETIREMENT_FACTOR")
	private BigDecimal retirementFactor;

	/** The situation. */
	@Column(name="SITUATION")
	private Integer situation;

	/** The state. */
	@Column(name="\"STATE\"")
	private Integer state;

	/** The target ratio. */
	@Column(name="TARGET_RATIO")
	private BigDecimal targetRatio;

	/** The tax amount. */
	@Column(name="TAX_AMOUNT")
	private BigDecimal taxAmount;

	/** The tax factor. */
	@Column(name="TAX_FACTOR")
	private BigDecimal taxFactor;

	/** The variation amount. */
	@Column(name="VARIATION_AMOUNT")
	private BigDecimal variationAmount;

	@Column(name="IND_PAYED")
	private Integer indPayed;

	/** The securities variation amount. */
	@Column(name=" VARIATION_BALANCE")
	private BigDecimal securitiesVariationAmount;

    /** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	
	/** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="EXCHANGE_DATE_RATE")
	private Date exchangeDateRate;

    /** The exchange amount. */
	@Column(name="EXCHANGE_AMOUNT")
	private BigDecimal exchangeAmount;
	
	/** The market rate. */
	@Column(name="CURRENCY_PAYMENT")
	private Integer currencyPayment;
	
	//bi-directional many-to-one association to BlockCorporativeResult
	/** The block corporative results. */
	@OneToMany(mappedBy="corporativeOperation")
	private List<BlockCorporativeResult> blockCorporativeResults;

	//bi-directional many-to-one association to CapitalIncreaseMotive
	/** The capital increase motives. */
	@OneToMany(mappedBy="corporativeOperation",cascade=CascadeType.PERSIST,orphanRemoval=true)
	private List<CapitalIncreaseMotive> capitalIncreaseMotives;
	
	@OneToMany(mappedBy="corporativeOperation",cascade=CascadeType.PERSIST,orphanRemoval=true)
	private List<SecuritiesCorporative> securitiesCorporatives;	

	//bi-directional many-to-one association to BenefitPaymentAllocation
	/** The benefit payment allocation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BENEFIT_PAYMENT_FK")
	private BenefitPaymentAllocation benefitPaymentAllocation;

	//bi-directional many-to-one association to CorporativeProcessResult
	/** The corporative process results. */
	@OneToMany(mappedBy="corporativeOperation")
	private List<CorporativeProcessResult> corporativeProcessResults;

	@OneToMany(mappedBy="corporativeOperation")
	private List<CorporativeProcessResult> corporativeResultsDetail;

	//bi-directional many-to-one association to ReportCorporativeResult
	/** The report corporative results. */
	@OneToMany(mappedBy="corporativeOperation")
	private List<ReportCorporativeResult> reportCorporativeResults;
	/** The holder account adjustments. */
	@OneToMany(mappedBy="corporativeOperation")
	private List<HolderAccountAdjusment> holderAccountAdjusments;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RETIREMENT_OPERATION_FK")
	private RetirementOperation retirementOperation;	

	/** The block corporative results. */
	@OneToMany(mappedBy="corporativeOperation")
	private List<CustodyCommission> custodyCommissionList;

	/** The program interest coupon. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROGRAM_INTEREST_FK")
	private ProgramInterestCoupon programInterestCoupon;
	
	/** The program amortization coupon. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROGRAM_AMORTIZATION_FK")
	private ProgramAmortizationCoupon programAmortizationCoupon;
	
	@Column(name="PRELIMINARY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date preliminaryDate;	

	@Column(name="DEFINITIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date definitiveDate;

	/** The ind round value. */
	@Transient
	private boolean indRoundValue;
	
	/** The ind remanent value. */
	@Transient
	private boolean indRemanentValue;
	
	/** The ind tax factor. */
	@Transient
	private boolean indTaxValue;

	/** The exchange rela target security. */
	@Transient
	private BigDecimal exchangeRelaTargetSecurity;

	/** The exchange rela source security. */
	@Transient
	private BigDecimal exchangeRelaSourceSecurity;
	
	@Transient
	private BigDecimal amountCoupon;
	
	@Transient
	private String eventTypeDescription;
	
	@Transient
	private String stateDescription;
	
	@Transient
	private String currencyTypeDescription;
	
	@Transient
	private String currencyPaymentDescription;
	
	@Transient
	private Long idParticipantPk;
	
	@Transient
	Integer indOriginDestiny;

	@Transient
	private boolean indElectronicCupon;
	
	@Temporal( TemporalType.DATE)
	@Column(name="FIRST_START_DATE")
	private Date firstStartDate; 
	
	@Temporal( TemporalType.DATE)
	@Column(name="FIRST_END_DATE")
	private Date firstEndDate; 

	@Temporal( TemporalType.DATE)
	@Column(name="SECOND_START_DATE")
	private Date secondStartDate; 
	
	@Temporal( TemporalType.DATE)
	@Column(name="SECOND_END_DATE")
	private Date secondEndDate; 

	@Temporal( TemporalType.DATE)
	@Column(name="THIRD_START_DATE")
	private Date thirdStartDate; 
	
	@Temporal( TemporalType.DATE)
	@Column(name="THIRD_END_DATE")
	private Date thirdEndDate; 
	
	@Column(name="NEGOTIATION_RANGE")
	private Long negotiationRange;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Transient
	private String idDescription;
	
	@Transient
	private boolean indAdjusmentPending;
	
	@Transient
	private List<CapitalIncreaseMotive> tempCapitalIncreaeMotive;
	
	public String getCurrencyTypeDescription() {
		currencyTypeDescription = CurrencyType.get(currency).getValue();
		return currencyTypeDescription;
	}
	public String getCurrencyPaymentDescription() {
		currencyPaymentDescription = CurrencyType.get(currencyPayment).getValue();
		return currencyPaymentDescription;
	}
	public void setCurrencyPaymentDescription(String currencyPaymentDescription) {
		this.currencyPaymentDescription = currencyPaymentDescription;
	}
	public void setCurrencyTypeDescription(String currencyTypeDescription) {
		this.currencyTypeDescription = currencyTypeDescription;
	}

	
	
	public List<CapitalIncreaseMotive> getTempCapitalIncreaeMotive() {
		return tempCapitalIncreaeMotive;
	}

	public void setTempCapitalIncreaeMotive(
			List<CapitalIncreaseMotive> tempCapitalIncreaeMotive) {
		this.tempCapitalIncreaeMotive = tempCapitalIncreaeMotive;
	}
	
	public CorporativeOperation(Long idCorporativeOperationPk, Date cutoffDate,
			Date registryDate,String idSecurityCodePk) {
		super();
		this.idCorporativeOperationPk = idCorporativeOperationPk;
		this.cutoffDate = cutoffDate;
		this.registryDate = registryDate;
		this.securities = new Security(idSecurityCodePk);
		this.indAffectBalance = BooleanType.NO.getCode();
	}
	
	public CorporativeOperation(Long idCorporativeOperationPk, Integer state,String idSecurityCodePk) {
		super();
		this.idCorporativeOperationPk = idCorporativeOperationPk;
		this.state = state;
  		this.securities = new Security(idSecurityCodePk);
  		this.indAffectBalance = BooleanType.NO.getCode();
	}

	public String getIdDescription() {
		StringBuilder sbDescription = new StringBuilder();
		if(idCorporativeOperationPk!=null){
		sbDescription.append(idCorporativeOperationPk + " - ");
//		sbDescription.append(ImportanceEventType.get(corporativeEventType).getValue());
		idDescription = sbDescription.toString();
		}else{
			idDescription = "";
		}
 		return idDescription;
	}

	public void setIdDescription(String idDescription) {
		this.idDescription = idDescription;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Instantiates a new corporative operation.
	 */
	public CorporativeOperation() {
		indAffectBalance = BooleanType.NO.getCode();
		corporativeEventType=new CorporativeEventType();
	}

	public CorporativeOperation(Long idCorporativeOperation) {
		super();
		this.idCorporativeOperationPk= idCorporativeOperation;
	}
	/**
	 * Gets the id corporative operation pk.
	 *
	 * @return the id corporative operation pk
	 */
	public Long getIdCorporativeOperationPk() {
		return this.idCorporativeOperationPk;
	}

	/**
	 * Sets the id corporative operation pk.
	 *
	 * @param idCorporativeOperationPk the new id corporative operation pk
	 */
	public void setIdCorporativeOperationPk(Long idCorporativeOperationPk) {
		this.idCorporativeOperationPk = idCorporativeOperationPk;
	}

	/**
	 * Gets the accord date.
	 *
	 * @return the accord date
	 */
	public Date getAccordDate() {
		return this.accordDate;
	}

	/**
	 * Sets the accord date.
	 *
	 * @param accordDate the new accord date
	 */
	public void setAccordDate(Date accordDate) {
		this.accordDate = accordDate;
	}


	/**
	 * Gets the commission amount.
	 *
	 * @return the commission amount
	 */
	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}

	/**
	 * Sets the commission amount.
	 *
	 * @param commissionAmount the new commission amount
	 */
	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	/**
	 * Gets the custody amount.
	 *
	 * @return the custody amount
	 */
	public BigDecimal getCustodyAmount() {
		return this.custodyAmount;
	}

	/**
	 * Sets the custody amount.
	 *
	 * @param custodyAmount the new custody amount
	 */
	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}

	/**
	 * Gets the cutoff date.
	 *
	 * @return the cutoff date
	 */
	public Date getCutoffDate() {
		return this.cutoffDate;
	}

	/**
	 * Sets the cutoff date.
	 *
	 * @param cutoffDate the new cutoff date
	 */
	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}

	/**
	 * Gets the delivery date.
	 *
	 * @return the delivery date
	 */
	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	/**
	 * Sets the delivery date.
	 *
	 * @param deliveryDate the new delivery date
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * Gets the delivery factor.
	 *
	 * @return the delivery factor
	 */
	public BigDecimal getDeliveryFactor() {
		return this.deliveryFactor;
	}

	/**
	 * Sets the delivery factor.
	 *
	 * @param deliveryFactor the new delivery factor
	 */
	public void setDeliveryFactor(BigDecimal deliveryFactor) {
		this.deliveryFactor = deliveryFactor;
	}

	/**
	 * Gets the delivery placement.
	 *
	 * @return the delivery placement
	 */
	public Integer getDeliveryPlacement() {
		return this.deliveryPlacement;
	}

	/**
	 * Sets the delivery placement.
	 *
	 * @param deliveryPlacement the new delivery placement
	 */
	public void setDeliveryPlacement(Integer deliveryPlacement) {
		this.deliveryPlacement = deliveryPlacement;
	}



	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the new securities
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	/**
	 * Gets the target security.
	 *
	 * @return the target security
	 */
	public Security getTargetSecurity() {
		return targetSecurity;
	}

	/**
	 * Sets the target security.
	 *
	 * @param targetSecurity the new target security
	 */
	public void setTargetSecurity(Security targetSecurity) {
		this.targetSecurity = targetSecurity;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the new circulation balance.
	 *
	 * @return the new circulation balance
	 */
	public BigDecimal getNewCirculationBalance() {
		return this.newCirculationBalance;
	}

	/**
	 * Sets the new circulation balance.
	 *
	 * @param newCirculationBalance the new new circulation balance
	 */
	public void setNewCirculationBalance(BigDecimal newCirculationBalance) {
		this.newCirculationBalance = newCirculationBalance;
	}

	/**
	 * Gets the new nominal value.
	 *
	 * @return the new nominal value
	 */
	public BigDecimal getNewNominalValue() {
		return this.newNominalValue;
	}

	/**
	 * Sets the new nominal value.
	 *
	 * @param newNominalValue the new new nominal value
	 */
	public void setNewNominalValue(BigDecimal newNominalValue) {
		this.newNominalValue = newNominalValue;
	}

	/**
	 * Gets the new share capital.
	 *
	 * @return the new share capital
	 */
	public BigDecimal getNewShareCapital() {
		return this.newShareCapital;
	}

	/**
	 * Sets the new share capital.
	 *
	 * @param newShareCapital the new new share capital
	 */
	public void setNewShareCapital(BigDecimal newShareCapital) {
		this.newShareCapital = newShareCapital;
	}

	/**
	 * Gets the old circulation balance.
	 *
	 * @return the old circulation balance
	 */
	public BigDecimal getOldCirculationBalance() {
		return this.oldCirculationBalance;
	}

	/**
	 * Sets the old circulation balance.
	 *
	 * @param oldCirculationBalance the new old circulation balance
	 */
	public void setOldCirculationBalance(BigDecimal oldCirculationBalance) {
		this.oldCirculationBalance = oldCirculationBalance;
	}

	/**
	 * Gets the old nominal value.
	 *
	 * @return the old nominal value
	 */
	public BigDecimal getOldNominalValue() {
		return this.oldNominalValue;
	}

	/**
	 * Sets the old nominal value.
	 *
	 * @param oldNominalValue the new old nominal value
	 */
	public void setOldNominalValue(BigDecimal oldNominalValue) {
		this.oldNominalValue = oldNominalValue;
	}

	/**
	 * Gets the old share capital.
	 *
	 * @return the old share capital
	 */
	public BigDecimal getOldShareCapital() {
		return this.oldShareCapital;
	}

	/**
	 * Sets the old share capital.
	 *
	 * @param oldShareCapital the new old share capital
	 */
	public void setOldShareCapital(BigDecimal oldShareCapital) {
		this.oldShareCapital = oldShareCapital;
	}

	/**
	 * Gets the origin ratio.
	 *
	 * @return the origin ratio
	 */
	public BigDecimal getOriginRatio() {
		return this.originRatio;
	}

	/**
	 * Sets the origin ratio.
	 *
	 * @param originRatio the new origin ratio
	 */
	public void setOriginRatio(BigDecimal originRatio) {
		this.originRatio = originRatio;
	}

	/**
	 * Gets the payment amount.
	 *
	 * @return the payment amount
	 */
	public BigDecimal getPaymentAmount() {
		return this.paymentAmount;
	}

	/**
	 * Sets the payment amount.
	 *
	 * @param paymentAmount the new payment amount
	 */
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * Gets the reception date.
	 *
	 * @return the reception date
	 */
	public Date getReceptionDate() {
		return this.receptionDate;
	}

	/**
	 * Sets the reception date.
	 *
	 * @param receptionDate the new reception date
	 */
	public void setReceptionDate(Date receptionDate) {
		this.receptionDate = receptionDate;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the retirement factor.
	 *
	 * @return the retirement factor
	 */
	public BigDecimal getRetirementFactor() {
		return this.retirementFactor;
	}

	/**
	 * Sets the retirement factor.
	 *
	 * @param retirementFactor the new retirement factor
	 */
	public void setRetirementFactor(BigDecimal retirementFactor) {
		this.retirementFactor = retirementFactor;
	}


	/**
	 * Gets the accord type.
	 *
	 * @return the accord type
	 */
	public Integer getAccordType() {
		return accordType;
	}

	/**
	 * Sets the accord type.
	 *
	 * @param accordType the new accord type
	 */
	public void setAccordType(Integer accordType) {
		this.accordType = accordType;
	}

	

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the ind affect balance.
	 *
	 * @return the ind affect balance
	 */
	public Integer getIndAffectBalance() {
		return indAffectBalance;
	}

	/**
	 * Sets the ind affect balance.
	 *
	 * @param indAffectBalance the new ind affect balance
	 */
	public void setIndAffectBalance(Integer indAffectBalance) {
		this.indAffectBalance = indAffectBalance;
	}

	/**
	 * Gets the ind excluded commissions.
	 *
	 * @return the ind excluded commissions
	 */
	public Integer getIndExcludedCommissions() {
		return indExcludedCommissions;
	}

	/**
	 * Sets the ind excluded commissions.
	 *
	 * @param indExcludedCommissions the new ind excluded commissions
	 */
	public void setIndExcludedCommissions(Integer indExcludedCommissions) {
		this.indExcludedCommissions = indExcludedCommissions;
	}

	/**
	 * Gets the ind remanent.
	 *
	 * @return the ind remanent
	 */
	public Integer getIndRemanent() {
		return indRemanent;
	}

	/**
	 * Sets the ind remanent.
	 *
	 * @param indRemanent the new ind remanent
	 */
	public void setIndRemanent(Integer indRemanent) {
		this.indRemanent = indRemanent;
	}

	/**
	 * Gets the ind round.
	 *
	 * @return the ind round
	 */
	public Integer getIndRound() {
		return indRound;
	}

	/**
	 * Sets the ind round.
	 *
	 * @param indRound the new ind round
	 */
	public void setIndRound(Integer indRound) {
		this.indRound = indRound;
	}

	/**
	 * Gets the ind tax.
	 *
	 * @return the ind tax
	 */
	public Integer getIndTax() {
		return indTax;
	}

	/**
	 * Sets the ind tax.
	 *
	 * @param indTax the new ind tax
	 */
	public void setIndTax(Integer indTax) {
		this.indTax = indTax;
	}

	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the target ratio.
	 *
	 * @return the target ratio
	 */
	public BigDecimal getTargetRatio() {
		return this.targetRatio;
	}

	/**
	 * Sets the target ratio.
	 *
	 * @param targetRatio the new target ratio
	 */
	public void setTargetRatio(BigDecimal targetRatio) {
		this.targetRatio = targetRatio;
	}

	/**
	 * Gets the tax amount.
	 *
	 * @return the tax amount
	 */
	public BigDecimal getTaxAmount() {
		return this.taxAmount;
	}

	/**
	 * Sets the tax amount.
	 *
	 * @param taxAmount the new tax amount
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * Gets the tax factor.
	 *
	 * @return the tax factor
	 */
	public BigDecimal getTaxFactor() {
		return this.taxFactor;
	}

	/**
	 * Sets the tax factor.
	 *
	 * @param taxFactor the new tax factor
	 */
	public void setTaxFactor(BigDecimal taxFactor) {
		this.taxFactor = taxFactor;
	}

	/**
	 * Gets the variation amount.
	 *
	 * @return the variation amount
	 */
	public BigDecimal getVariationAmount() {
		return this.variationAmount;
	}

	/**
	 * Sets the variation amount.
	 *
	 * @param variationAmount the new variation amount
	 */
	public void setVariationAmount(BigDecimal variationAmount) {
		this.variationAmount = variationAmount;
	}

	/**
	 * Sets the block corporative results.
	 *
	 * @param blockCorporativeResults the new block corporative results
	 */
	public void setBlockCorporativeResults(List<BlockCorporativeResult> blockCorporativeResults) {
		this.blockCorporativeResults = blockCorporativeResults;
	}

	/**
	 * Gets the block corporative results.
	 *
	 * @return the block corporative results
	 */
	public List<BlockCorporativeResult> getBlockCorporativeResults() {
		return blockCorporativeResults;
	}
	/**
	 * Gets the capital increase motives.
	 *
	 * @return the capital increase motives
	 */
	public List<CapitalIncreaseMotive> getCapitalIncreaseMotives() {
		return capitalIncreaseMotives;
	}

	/**
	 * Sets the capital increase motives.
	 *
	 * @param capitalIncreaseMotives the new capital increase motives
	 */
	public void setCapitalIncreaseMotives(List<CapitalIncreaseMotive> capitalIncreaseMotives) {
		this.capitalIncreaseMotives = capitalIncreaseMotives;
	}
	

	/**
	 * Gets the benefit payment allocation.
	 *
	 * @return the benefit payment allocation
	 */
	public BenefitPaymentAllocation getBenefitPaymentAllocation() {
		return this.benefitPaymentAllocation;
	}

	/**
	 * Sets the benefit payment allocation.
	 *
	 * @param benefitPaymentAllocation the new benefit payment allocation
	 */
	public void setBenefitPaymentAllocation(BenefitPaymentAllocation benefitPaymentAllocation) {
		this.benefitPaymentAllocation = benefitPaymentAllocation;
	}
	
	/**
	 * Gets the corporative process results.
	 *
	 * @return the corporative process results
	 */
	public List<CorporativeProcessResult> getCorporativeProcessResults() {
		return corporativeProcessResults;
	}

	/**
	 * Sets the corporative process results.
	 *
	 * @param corporativeProcessResults the new corporative process results
	 */
	public void setCorporativeProcessResults(List<CorporativeProcessResult> corporativeProcessResults) {
		this.corporativeProcessResults = corporativeProcessResults;
	}
	/**
	 * Gets the report corporative results.
	 *
	 * @return the report corporative results
	 */
	public List<ReportCorporativeResult> getReportCorporativeResults() {
		return reportCorporativeResults;
	}

	/**
	 * Sets the report corporative results.
	 *
	 * @param reportCorporativeResults the new report corporative results
	 */
	public void setReportCorporativeResults(List<ReportCorporativeResult> reportCorporativeResults) {
		this.reportCorporativeResults = reportCorporativeResults;
	}
	
	/**
	 * Get the holder Account Adjustments 
	 * 
	 * @return the holderAccountAdjusments
	 */
	public List<HolderAccountAdjusment> getHolderAccountAdjusments() {
		return holderAccountAdjusments;
	}

	/**
	 * Set the holder Account Adjustments 
	 * 
	 * @param holderAccountAdjusments the holderAccountAdjusments to set
	 */
	public void setHolderAccountAdjusments(List<HolderAccountAdjusment> holderAccountAdjusments) {
		this.holderAccountAdjusments = holderAccountAdjusments;
	}

	/**
	 * Checks if is ind round value.
	 *
	 * @return the indRoundValue
	 */
	public boolean isIndRoundValue() {
		return indRoundValue;
	}

	/**
	 * Sets the ind round value.
	 *
	 * @param indRoundValue the indRoundValue to set
	 */
	public void setIndRoundValue(boolean indRoundValue) {
		this.indRoundValue = indRoundValue;

	}

	/**
	 * Gets the new share balance.
	 *
	 * @return the new share balance
	 */
	public BigDecimal getNewShareBalance() {
		return newShareBalance;
	}

	/**
	 * Sets the new share balance.
	 *
	 * @param newShareBalance the new new share balance
	 */
	public void setNewShareBalance(BigDecimal newShareBalance) {
		this.newShareBalance = newShareBalance;
	}

	/**
	 * Gets the old share balance.
	 *
	 * @return the old share balance
	 */
	public BigDecimal getOldShareBalance() {
		return oldShareBalance;
	}

	/**
	 * Sets the old share balance.
	 *
	 * @param oldShareBalance the new old share balance
	 */
	public void setOldShareBalance(BigDecimal oldShareBalance) {
		this.oldShareBalance = oldShareBalance;
	}


	/**
	 * Gets the exchange rela target security.
	 *
	 * @return the exchange rela target security
	 */
	public BigDecimal getExchangeRelaTargetSecurity() {
		return exchangeRelaTargetSecurity;
	}

	/**
	 * Sets the exchange rela target security.
	 *
	 * @param exchangeRelaTargetSecurity the new exchange rela target security
	 */
	public void setExchangeRelaTargetSecurity(BigDecimal exchangeRelaTargetSecurity) {
		this.exchangeRelaTargetSecurity = exchangeRelaTargetSecurity;
	}

	/**
	 * Gets the exchange rela source security.
	 *
	 * @return the exchange rela source security
	 */
	public BigDecimal getExchangeRelaSourceSecurity() {
		return exchangeRelaSourceSecurity;
	}

	/**
	 * Sets the exchange rela source security.
	 *
	 * @param exchangeRelaSourceSecurity the new exchange rela source security
	 */
	public void setExchangeRelaSourceSecurity(BigDecimal exchangeRelaSourceSecurity) {
		this.exchangeRelaSourceSecurity = exchangeRelaSourceSecurity;
	}
	
	

 	/**
	  * Gets the securities variation amount.
	  *
	  * @return the securities variation amount
	  */
	 public BigDecimal getSecuritiesVariationAmount() {
		return securitiesVariationAmount;
	}

	/**
	 * Sets the securities variation amount.
	 *
	 * @param securitiesVariationAmount the new securities variation amount
	 */
	public void setSecuritiesVariationAmount(BigDecimal securitiesVariationAmount) {
		this.securitiesVariationAmount = securitiesVariationAmount;
	}
	
	
	/**
	 * Checks if is ind remanent value.
	 *
	 * @return true, if is ind remanent value
	 */
	public boolean isIndRemanentValue() {
		return indRemanentValue;
	}

	/**
	 * Sets the ind remanent value.
	 *
	 * @param indRemanentValue the new ind remanent value
	 */
	public void setIndRemanentValue(boolean indRemanentValue) {
		this.indRemanentValue = indRemanentValue;
	}
	/**
	 * Checks if is ind tax value.
	 *
	 * @return true, if is ind tax value
	 */
	public boolean isIndTaxValue() {
		return indTaxValue;
	}

	/**
	 * Sets the ind tax value.
	 *
	 * @param indTaxValue the new ind tax value
	 */
	public void setIndTaxValue(boolean indTaxValue) {
		this.indTaxValue = indTaxValue;
	}
	
	/**
	 * Gets the first start date.
	 *
	 * @return the first start date
	 */
	public Date getFirstStartDate() {
		return firstStartDate;
	}

	/**
	 * Sets the first start date.
	 *
	 * @param firstStartDate the new first start date
	 */
	public void setFirstStartDate(Date firstStartDate) {
		this.firstStartDate = firstStartDate;
	}

	/**
	 * Gets the first end date.
	 *
	 * @return the first end date
	 */
	public Date getFirstEndDate() {
		return firstEndDate;
	}

	public void setFirstEndDate(Date firstEndDate) {
		this.firstEndDate = firstEndDate;
	}

	public Date getSecondStartDate() {
		return secondStartDate;
	}

	public void setSecondStartDate(Date secondStartDate) {
		this.secondStartDate = secondStartDate;
	}

	public Date getSecondEndDate() {
		return secondEndDate;
	}

	public void setSecondEndDate(Date secondEndDate) {
		this.secondEndDate = secondEndDate;
	}

	public Date getThirdStartDate() {
		return thirdStartDate;
	}

	public void setThirdStartDate(Date thirdStartDate) {
		this.thirdStartDate = thirdStartDate;
	}

	public Date getThirdEndDate() {
		return thirdEndDate;
	}

	public void setThirdEndDate(Date thirdEndDate) {
		this.thirdEndDate = thirdEndDate;
	}

	public Long getNegotiationRange() {
		return negotiationRange;
	}

	public void setNegotiationRange(Long negotiationRange) {
		this.negotiationRange = negotiationRange;
	}
	

	/**
	 * Gets the event type description.
	 *
	 * @return the event type description
	 */
	public String getEventTypeDescription() {
		if(corporativeEventType!=null){
//		eventTypeDescription = ImportanceEventType.get(corporativeEventType).getValue();
		}
		return eventTypeDescription;
	}

	/**
	 * Sets the event type description.
	 *
	 * @param eventTypeDescription the new event type description
	 */
	public void setEventTypeDescription(String eventTypeDescription) {
		this.eventTypeDescription = eventTypeDescription;
	}
	
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
//		stateDescription = CorporateProcessStateType.get(state).getValue();
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	
	/**
	 * Gets the state program interest coupon.
	 *
	 * @return the program interest coupon
	 */
	public ProgramInterestCoupon getProgramInterestCoupon() {
		return programInterestCoupon;
	}

	/**
	 * Sets the program interest coupon.
	 *
	 * @param programInterestCoupon the new program interest coupon
	 */
	public void setProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon) {
		this.programInterestCoupon = programInterestCoupon;
	}
	
	
	/**
	 * Gets the state program amortization coupon.
	 *
	 * @return the program amortization coupon
	 */
	public ProgramAmortizationCoupon getProgramAmortizationCoupon() {
		return programAmortizationCoupon;
	}

	/**
	 * Sets the program amortization coupon.
	 *
	 * @param programAmortizationCoupon the new program amortization coupon
	 */
	public void setProgramAmortizationCoupon(
			ProgramAmortizationCoupon programAmortizationCoupon) {
		this.programAmortizationCoupon = programAmortizationCoupon;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {		 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("blockCorporativeResults", blockCorporativeResults);
		detailsMap.put("reportCorporativeResults", reportCorporativeResults);
		detailsMap.put("capitalIncreaseMotives", capitalIncreaseMotives); 
		detailsMap.put("holderAccountAdjusments", holderAccountAdjusments);
		detailsMap.put("corporativeProcessResults", corporativeProcessResults);
		detailsMap.put("securitiesCorporatives",securitiesCorporatives);
        return detailsMap;
	}

	public RetirementOperation getRetirementOperation() {
		return retirementOperation;
	}

	public void setRetirementOperation(RetirementOperation retirementOperation) {
		this.retirementOperation = retirementOperation;
	}

	/**
	 * Gets the securities corporatives.
	 *
	 * @return the securities corporatives
	 */
	public List<SecuritiesCorporative> getSecuritiesCorporatives() {
		return securitiesCorporatives;
	}

	/**
	 * Sets the securities corporatives.
	 *
	 * @param securitiesCorporatives the new securities corporatives
	 */
	public void setSecuritiesCorporatives(
			List<SecuritiesCorporative> securitiesCorporatives) {
		this.securitiesCorporatives = securitiesCorporatives;
	}

	/**
	 * @return the issuerConfirmedAmount
	 */
	public BigDecimal getIssuerConfirmedAmount() {
		return issuerConfirmedAmount;
	}

	/**
	 * @param issuerConfirmedAmount the issuerConfirmedAmount to set
	 */
	public void setIssuerConfirmedAmount(BigDecimal issuerConfirmedAmount) {
		this.issuerConfirmedAmount = issuerConfirmedAmount;
	}

	public Integer getIndPayed() {
		return indPayed;
	}

	public void setIndPayed(Integer indPayed) {
		this.indPayed = indPayed;
	}
	
	public void setDataFromProgramIntCoupon(ProgramInterestCoupon programInterestCoupon, CorporativeEventType corporativeEventType, Security security, LoggerUser loggerUser){
		this.setAudit(loggerUser);
		this.setProgramInterestCoupon( programInterestCoupon );
		
		this.setCorporativeEventType( corporativeEventType );
		
		this.setIssuer( security.getIssuer() );
		this.setRegistryDate( programInterestCoupon.getRegistryDate() );
		this.setCutoffDate( programInterestCoupon.getCutoffDate() );
		this.setDeliveryDate( programInterestCoupon.getPaymentDate() );
	    this.setAccordDate( security.getRegistryDate() ); 
		this.setAccordType( AccordType.COUPON_PAYMENT.getCode()  );
		this.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode()  );
		
		if(security.isFixedInterestType())
			this.setDeliveryFactor( programInterestCoupon.getInterestRate() );
		else
			this.setDeliveryFactor( null );
		
		this.setIndRound( BooleanType.NO.getCode() );
		
		if(security.getIndTaxExempt().equals( BooleanType.YES.getCode() ) )
			this.setIndTax( BooleanType.NO.getCode()  );
		if(security.getIndTaxExempt().equals( BooleanType.NO.getCode() ) )
			this.setIndTax( BooleanType.YES.getCode()  );		
		
		this.setIndRemanent( BooleanType.NO.getCode() );
		this.setIndExcludedCommissions( BooleanType.NO.getCode() );
		this.setCreationDate( programInterestCoupon.getCorporativeDate() );
		this.setSecurities( security );
		this.setCurrency( programInterestCoupon.getCurrency() );
		if(this.getCurrency().equals(CurrencyType.UFV.getCode()) || 
				this.getCurrency().equals(CurrencyType.DMV.getCode())){
			this.setExchangeDateRate(this.getDeliveryDate());
			this.setCurrencyPayment(programInterestCoupon.getCurrencyPayment());
		}
		
		if(programInterestCoupon.getStateProgramInterest()!= null && 
		   programInterestCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.EXPIRED.getCode()) &&
		   idCorporativeOperationPk == null){
			this.setState( CorporateProcessStateType.DEFINITIVE.getCode());
			this.setSituation(CorporativeSituationType.ISSUER_PAID.getCode());
		}else{
			this.setState( CorporateProcessStateType.REGISTERED.getCode() );
		}
		
		this.setRegistryUser( loggerUser.getUserName() );
		this.setIndPayed( BooleanType.NO.getCode() );
	}
	public void setDataFromProgramAmortCoupon(ProgramAmortizationCoupon programAmortizationCoupon, 
											  CorporativeEventType corporativeEventType,
											  Security security,
											  LoggerUser loggerUser){
		this.setAudit(loggerUser);
		this.setProgramAmortizationCoupon( programAmortizationCoupon );
		
		this.setCorporativeEventType( corporativeEventType  );
		
		this.setIssuer(security.getIssuer());
		this.setSecurities( security );
		this.setAccordType( AccordType.AMORTIZATION_PAYMENT.getCode() );
		this.setAccordDate( security.getRegistryDate() );
		
		if(programAmortizationCoupon.getStateProgramAmortization()!= null && 
		   programAmortizationCoupon.getStateProgramAmortization().equals(ProgramScheduleStateType.EXPIRED.getCode()) &&
		   idCorporativeOperationPk == null){
			this.setState(CorporateProcessStateType.DEFINITIVE.getCode());
			this.setSituation(CorporativeSituationType.ISSUER_PAID.getCode());
		}else{
			this.setState(CorporateProcessStateType.REGISTERED.getCode());
		}
		
		this.setRetirementFactor(BigDecimal.valueOf(100));
		if(CapitalPaymentModalityType.AT_MATURITY.getCode().equals(security.getCapitalPaymentModality())){
			this.setIndAffectBalance(Integer.valueOf(1));
		}else if(CapitalPaymentModalityType.PARTIAL.getCode().equals(security.getCapitalPaymentModality())
				&& programAmortizationCoupon.getUltimateAmortization().equals(BooleanType.YES.getBooleanValue())){
			this.setIndAffectBalance(Integer.valueOf(1));
		}else{
			this.setIndAffectBalance(Integer.valueOf(0));
		}		
		
		this.setRegistryUser( loggerUser.getUserName() );		
		this.setRegistryDate( programAmortizationCoupon.getRegistryDate() );
		this.setCreationDate( programAmortizationCoupon.getCorporativeDate() );
		
		this.setCutoffDate( programAmortizationCoupon.getCutoffDate() );
		this.setDeliveryDate( programAmortizationCoupon.getPaymentDate() );
		
		this.setCurrency(programAmortizationCoupon.getCurrency() );
		
		if(this.getCurrency().equals(CurrencyType.UFV.getCode()) || 
				this.getCurrency().equals(CurrencyType.DMV.getCode())){
			this.setExchangeDateRate(this.getDeliveryDate());
			this.setCurrencyPayment(programAmortizationCoupon.getCurrencyPayment());
		}	
		
		this.setDeliveryPlacement( DeliveryPlacementType.DEPOSITARY.getCode() );
		this.setDeliveryFactor( programAmortizationCoupon.getAmortizationFactor() );
		this.setIndRound( BooleanType.NO.getCode() );
		
		if(security.getIndTaxExempt().equals( BooleanType.YES.getCode() ) )
			this.setIndTax( BooleanType.NO.getCode()  );
		if(security.getIndTaxExempt().equals( BooleanType.NO.getCode() ) )
			this.setIndTax( BooleanType.YES.getCode()  );
		
		this.setIndRemanent(BooleanType.NO.getCode());
		this.setIndExcludedCommissions( BooleanType.NO.getCode() );
		this.setOldShareCapital( security.getShareCapital() );
		this.setNewShareCapital( security.getShareCapital().multiply( BigDecimal.valueOf(100).subtract(programAmortizationCoupon.getAmortizationFactor())  ) );
		this.setVariationAmount( security.getShareCapital().subtract( this.getNewShareCapital()  ) );
		this.setIndPayed(BooleanType.NO.getCode());
		
		/*for first coupon start*/
		this.setOldNominalValue( security.getInitialNominalValue() );
		this.setNewNominalValue( security.getInitialNominalValue() );
		/*for first coupon end*/
		
//		this.setRetirementFactor(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
//		this.setIndAffectBalance( GeneralConstants.ONE_VALUE_INTEGER );

	}
	
	public BigDecimal calculateAmortizationNominalValues(BigDecimal initialNominalValue,Boolean isAtMaturityAmortPaymentModality, BigDecimal lastNewNominalValue){
		if(isAtMaturityAmortPaymentModality){
			this.setNewNominalValue( BigDecimal.ZERO );
			return null;
		}else{
			if(lastNewNominalValue!=null){
				this.setOldNominalValue( lastNewNominalValue );
			}
			BigDecimal sustrac=this.getDeliveryFactor().multiply(initialNominalValue, MathContext.DECIMAL128).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128);
			this.setNewNominalValue( this.getOldNominalValue().subtract( sustrac  )  );
			
			return this.getNewNominalValue();
		}
	}
	
	public Date getPreliminaryDate() {
		return preliminaryDate;
	}

	public void setPreliminaryDate(Date preliminaryDate) {
		this.preliminaryDate = preliminaryDate;
	}
	
	public Date getDefinitiveDate() {
		return definitiveDate;
	}

	public void setDefinitiveDate(Date definitiveDate) {
		this.definitiveDate = definitiveDate;
	}

	public Integer getIndPenality() {
		return indPenality;
	}

	public void setIndPenality(Integer indPenality) {
		this.indPenality = indPenality;
	}

	public BigDecimal getPenalityRate() {
		return penalityRate;
	}

	public void setPenalityRate(BigDecimal penalityRate) {
		this.penalityRate = penalityRate;
	}

	public CorporativeEventType getCorporativeEventType() {
		return corporativeEventType;
	}

	public void setCorporativeEventType(CorporativeEventType corporativeEventType) {
		this.corporativeEventType = corporativeEventType;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Date getExchangeDateRate() {
		return exchangeDateRate;
	}

	public void setExchangeDateRate(Date exchangeDateRate) {
		this.exchangeDateRate = exchangeDateRate;
	}

	public Integer getCurrencyPayment() {
		return currencyPayment;
	}

	public void setCurrencyPayment(Integer currencyPayment) {
		this.currencyPayment = currencyPayment;
	}
	public BigDecimal getExchangeAmount() {
		return exchangeAmount;
	}
	public void setExchangeAmount(BigDecimal exchangeAmount) {
		this.exchangeAmount = exchangeAmount;
	}
	public Integer getIndOriginDestiny() {
		return indOriginDestiny;
	}
	public void setIndOriginDestiny(Integer indOriginDestiny) {
		this.indOriginDestiny = indOriginDestiny;
	}
	public BigDecimal getAmountCoupon() {
		return amountCoupon;
	}
	public void setAmountCoupon(BigDecimal amountCoupon) {
		this.amountCoupon = amountCoupon;
	}
	public Long getIdStockCalculation() {
		return idStockCalculation;
	}
	public void setIdStockCalculation(Long idStockCalculation) {
		this.idStockCalculation = idStockCalculation;
	}
	public boolean isIndAdjusmentPending() {
		return indAdjusmentPending;
	}
	public void setIndAdjusmentPending(boolean indAdjusmentPending) {
		this.indAdjusmentPending = indAdjusmentPending;
	}
	public boolean isIndElectronicCupon() {
		return indElectronicCupon;
	}
	public void setIndElectronicCupon(boolean indElectronicCupon) {
		this.indElectronicCupon = indElectronicCupon;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	
}