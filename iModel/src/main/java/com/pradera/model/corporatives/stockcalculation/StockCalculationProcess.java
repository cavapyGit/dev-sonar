package com.pradera.model.corporatives.stockcalculation;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.corporatives.stockcalculation.validation.StockByHolder;
import com.pradera.model.corporatives.stockcalculation.validation.StockByParticipant;
import com.pradera.model.corporatives.stockcalculation.validation.StockBySecurity;
import com.pradera.model.issuancesecuritie.Security;
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class StockCalculationProcess.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/04/2013
 */
@Entity
@Table(name = "STOCK_CALCULATION_PROCESS")
@NamedNativeQuery(name = StockCalculationProcess.STORED,
query = "{ call PKG_STOCK_CALCULATION.SP_MAIN_STOCK_CALCULATION(:id) }",
resultClass=StockCalculationProcess.class, 
hints = {@QueryHint(name = "org.hibernate.callable", value = "true") })
public class StockCalculationProcess implements Serializable,Auditable {


	public static final String STORED = "StockCalculationProcess.stored";
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3139626986265790064L;

	/** The id stock calculation pk. */
	@Id
	@SequenceGenerator(name="STOCK_CALCULATION_PROCESS_GENERATOR", sequenceName="SQ_ID_STOCK_CALCULATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="STOCK_CALCULATION_PROCESS_GENERATOR")
    @Column(name = "ID_STOCK_CALCULATION_PK")
    private Long idStockCalculationPk;

    /** The creation date. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATION_DATE")
    @NotNull
    private Date creationDate;

    /** The cutoff date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "CUTOFF_DATE")
    @NotNull
    private Date cutoffDate;
    
    /** The registry date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "REGISTRY_DATE")
    @NotNull
    private Date registryDate;
    
    /** The registry user. */
    @Column(name="REGISTRY_USER")
    @NotNull
	private String registryUser;

    /** The id holder fk. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_HOLDER_FK")
    private Holder holder;

    /** The id isin code fk. */
    @ManyToOne(fetch = FetchType.LAZY,optional = true)
    @JoinColumn(name = "ID_SECURITY_CODE_FK")
    private Security security;

    /** The id participant fk. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PARTICIPANT_FK")
    private Participant participant;

    /** The ind automactic. */
    @Column(name = "IND_AUTOMACTIC")
    @NotNull
    private Integer indAutomactic;

    /** The last modify app. */
    @Column(name = "LAST_MODIFY_APP")
    @NotNull
    private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_MODIFY_DATE")
    @NotNull
    private Date lastModifyDate;

    /** The last modify ip. */
    @Column(name = "LAST_MODIFY_IP")
    @NotNull
    private String lastModifyIp;

    /** The last modify user. */
    @Column(name = "LAST_MODIFY_USER")
    @NotNull
    private String lastModifyUser;

    /** The stock state. */
    @Column(name = "STOCK_STATE")
    @NotNull
    private Integer stockState;

    /** The stock class. */
    @Column(name = "STOCK_CLASS")
    @NotNull
    private Integer stockClass;

    /** The stock type. */
    @Column(name = "STOCK_TYPE")
    @NotNull
    private Integer stockType;
    
    /** The monthly type. */
    @Column(name="MONTHLY_TYPE")
    private Long monthlyType;

    //bi-directional many-to-one association to BlockStockCalculation
    /** The block stock calculations. */
    @OneToMany(mappedBy = "stockCalculationProcess")
    private List<BlockStockCalculation> blockStockCalculations;

    //bi-directional many-to-one association to ReportStockCalculation
    /** The report stock calculations. */
    @OneToMany(mappedBy = "stockCalculationProcess")
    private List<ReportStockCalculation> reportStockCalculations;

    //bi-directional many-to-one association to StockCalculationBalance
    /** The stock calculation balances. */
    @OneToMany(mappedBy = "stockCalculationProcess",cascade = CascadeType.ALL)
    private List<StockCalculationBalance> stockCalculationBalances;

    //bi-directional many-to-one association to StockCalculationProcess
    /** The stock calculation process. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_HISTORY_STOCK_PROCESS")
    private StockCalculationProcess stockCalculationProcess;

    //bi-directional many-to-one association to StockCalculationProcess
    /** The stock calculation processes. */
    @OneToMany(mappedBy = "stockCalculationProcess")
    private Set<StockCalculationProcess> stockCalculationProcesses;
    
    @Transient
    private Integer reportType;
    
    @Transient
    private String idIssuerPk;

	/**
	 * Gets the id stock calculation pk.
	 *
	 * @return the id stock calculation pk
	 */
	public Long getIdStockCalculationPk() {
		return idStockCalculationPk;
	}

	/**
	 * Sets the id stock calculation pk.
	 *
	 * @param idStockCalculationPk the new id stock calculation pk
	 */
	public void setIdStockCalculationPk(Long idStockCalculationPk) {
		this.idStockCalculationPk = idStockCalculationPk;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the cutoff date.
	 *
	 * @return the cutoff date
	 */
	public Date getCutoffDate() {
		return cutoffDate;
	}

	/**
	 * Sets the cutoff date.
	 *
	 * @param cutoffDate the new cutoff date
	 */
	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the id holder fk.
	 *
	 * @return the id holder fk
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the id holder fk.
	 *
	 * @param idHolderFk the new id holder fk
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the id isin code fk.
	 *
	 * @return the id isin code fk
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the id isin code fk.
	 *
	 * @param idIsinCodeFk the new id isin code fk
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the id participant fk.
	 *
	 * @return the id participant fk
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the id participant fk.
	 *
	 * @param idParticipantFk the new id participant fk
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the ind automactic.
	 *
	 * @return the ind automactic
	 */
	public Integer getIndAutomactic() {
		return indAutomactic;
	}

	/**
	 * Sets the ind automactic.
	 *
	 * @param indAutomactic the new ind automactic
	 */
	public void setIndAutomactic(Integer indAutomactic) {
		this.indAutomactic = indAutomactic;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the stock state.
	 *
	 * @return the stock state
	 */
	public Integer getStockState() {
		return stockState;
	}

	/**
	 * Sets the stock state.
	 *
	 * @param stockState the new stock state
	 */
	public void setStockState(Integer stockState) {
		this.stockState = stockState;
	}

	/**
	 * Gets the stock class.
	 *
	 * @return the stock class
	 */
	public Integer getStockClass() {
		return stockClass;
	}

	/**
	 * Sets the stock class.
	 *
	 * @param stockClass the new stock class
	 */
	public void setStockClass(Integer stockClass) {
		this.stockClass = stockClass;
	}

	/**
	 * Gets the stock type.
	 *
	 * @return the stock type
	 */
	public Integer getStockType() {
		return stockType;
	}

	/**
	 * Sets the stock type.
	 *
	 * @param stockType the new stock type
	 */
	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the block stock calculations.
	 *
	 * @return the block stock calculations
	 */
	public List<BlockStockCalculation> getBlockStockCalculations() {
		return blockStockCalculations;
	}

	/**
	 * Sets the block stock calculations.
	 *
	 * @param blockStockCalculations the new block stock calculations
	 */
	public void setBlockStockCalculations(
			List<BlockStockCalculation> blockStockCalculations) {
		this.blockStockCalculations = blockStockCalculations;
	}

	/**
	 * Gets the report stock calculations.
	 *
	 * @return the report stock calculations
	 */
	public List<ReportStockCalculation> getReportStockCalculations() {
		return reportStockCalculations;
	}

	/**
	 * Sets the report stock calculations.
	 *
	 * @param reportStockCalculations the new report stock calculations
	 */
	public void setReportStockCalculations(
			List<ReportStockCalculation> reportStockCalculations) {
		this.reportStockCalculations = reportStockCalculations;
	}

	/**
	 * Gets the stock calculation balances.
	 *
	 * @return the stock calculation balances
	 */
	public List<StockCalculationBalance> getStockCalculationBalances() {
		return stockCalculationBalances;
	}

	/**
	 * Sets the stock calculation balances.
	 *
	 * @param stockCalculationBalances the new stock calculation balances
	 */
	public void setStockCalculationBalances(
			List<StockCalculationBalance> stockCalculationBalances) {
		this.stockCalculationBalances = stockCalculationBalances;
	}

	/**
	 * Gets the stock calculation process.
	 *
	 * @return the stock calculation process
	 */
	public StockCalculationProcess getStockCalculationProcess() {
		return stockCalculationProcess;
	}

	/**
	 * Sets the stock calculation process.
	 *
	 * @param stockCalculationProcess the new stock calculation process
	 */
	public void setStockCalculationProcess(
			StockCalculationProcess stockCalculationProcess) {
		this.stockCalculationProcess = stockCalculationProcess;
	}

	/**
	 * Gets the stock calculation processes.
	 *
	 * @return the stock calculation processes
	 */
	public Set<StockCalculationProcess> getStockCalculationProcesses() {
		return stockCalculationProcesses;
	}

	/**
	 * Sets the stock calculation processes.
	 *
	 * @param stockCalculationProcesses the new stock calculation processes
	 */
	public void setStockCalculationProcesses(
			Set<StockCalculationProcess> stockCalculationProcesses) {
		this.stockCalculationProcesses = stockCalculationProcesses;
	}

	/**
	 * Gets the monthly type.
	 *
	 * @return the monthly type
	 */
	public Long getMonthlyType() {
		return monthlyType;
	}

	/**
	 * Sets the monthly type.
	 *
	 * @param monthlyType the new monthly type
	 */
	public void setMonthlyType(Long monthlyType) {
		this.monthlyType = monthlyType;
	}

	public Integer getReportType() {
		return reportType;
	}

	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}
	
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
				new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("stockCalculationBalances", stockCalculationBalances);
		return detailsMap;
	}

   

    
}
