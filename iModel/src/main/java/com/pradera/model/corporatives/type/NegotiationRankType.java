package com.pradera.model.corporatives.type;

 
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum NegotiationRankType {
	
	ONE(new Long(1)),
	TWO(new Long(2)),
	THREE(new Long(3));
   
	
	private Long code;
	
	public static final List<NegotiationRankType> list= new ArrayList<NegotiationRankType>();
	public static final List<NegotiationRankType> listNotOfac = new ArrayList<NegotiationRankType>();
	public static final Map<Long, NegotiationRankType> lookup = new HashMap<Long, NegotiationRankType >();
	
	static{
		for(NegotiationRankType type: EnumSet.allOf(NegotiationRankType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);
		}
		
		
	}
	
	
	private NegotiationRankType(Long code) {
 		this.code = code;
	}


	 

    public Long getCode() {
		return code;
	}
	
	public static NegotiationRankType get(Long code)
	{
		return lookup.get(code);
	}


	
	
	
	

}
