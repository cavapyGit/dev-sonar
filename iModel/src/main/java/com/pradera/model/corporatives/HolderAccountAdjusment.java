package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

/**
 * @author PraderaTechnologies
 * 
 */
@Entity
@Table(name = "HOLDER_ACCOUNT_ADJUSMENT")
public class HolderAccountAdjusment implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "HOLDER_ACCOUNT_ADJUSTMENT_GENERATOR", sequenceName = "SQ_HOLDER_ACC_ADJUSMENT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HOLDER_ACCOUNT_ADJUSTMENT_GENERATOR")
	@Column(name = "ID_HOLDER_ACCOUNT_ADJUSMENT_PK")
	private Long idHolderAccountAdjusmentPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;

	@Column(name = "OLD_BENEFIT_AMOUNT")
	private BigDecimal oldBenefitAmount;

	@Column(name = "OLD_TAX")
	private BigDecimal oldTax;

	@Column(name = "OLD_COMMISSIONS")
	private BigDecimal oldCommisiones;

	@Column(name = "NEW_BENEFIT_AMOUNT")
	private BigDecimal newBenefitAmount;

	@Column(name = "NEW_TAX")
	private BigDecimal newTax;

	@Column(name = "NEW_COMMISSIONS")
	private BigDecimal newCommissiones;

	@Column(name = "ADJUSMENT_STATE")
	private Integer adjustmentState;

	@Column(name = "IND_DELIVER_BENEFIT")
	private Integer indDeliverBenefit;

	@Column(name = "IND_COLLECT_TAX")
	private Integer indCollectTax;

	@Column(name = "IND_COLLECT_COMMISSIONS")
	private Integer indCollectCommissiones;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SECURITY_CODE_FK")
	private Security security;
	
	/**
	 * @return the idHolderAccountAdjusmentPk
	 */
	public Long getIdHolderAccountAdjusmentPk() {
		return idHolderAccountAdjusmentPk;
	}

	/**
	 * @param idHolderAccountAdjusmentPk
	 *            the idHolderAccountAdjusmentPk to set
	 */
	public void setIdHolderAccountAdjusmentPk(Long idHolderAccountAdjusmentPk) {
		this.idHolderAccountAdjusmentPk = idHolderAccountAdjusmentPk;
	}

	/**
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * @param holderAccount
	 *            the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * @return the corporativeOperation
	 */
	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	/**
	 * @param corporativeOperation
	 *            the corporativeOperation to set
	 */
	public void setCorporativeOperation(
			CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	/**
	 * @return the oldBenefitAmount
	 */
	public BigDecimal getOldBenefitAmount() {
		return oldBenefitAmount;
	}

	/**
	 * @param oldBenefitAmount
	 *            the oldBenefitAmount to set
	 */
	public void setOldBenefitAmount(BigDecimal oldBenefitAmount) {
		this.oldBenefitAmount = oldBenefitAmount;
	}

	/**
	 * @return the oldTax
	 */
	public BigDecimal getOldTax() {
		return oldTax;
	}

	/**
	 * @param oldTax
	 *            the oldTax to set
	 */
	public void setOldTax(BigDecimal oldTax) {
		this.oldTax = oldTax;
	}

	/**
	 * @return the oldCommisiones
	 */
	public BigDecimal getOldCommisiones() {
		return oldCommisiones;
	}

	/**
	 * @param oldCommisiones
	 *            the oldCommisiones to set
	 */
	public void setOldCommisiones(BigDecimal oldCommisiones) {
		this.oldCommisiones = oldCommisiones;
	}

	/**
	 * @return the newBenefitAmount
	 */
	public BigDecimal getNewBenefitAmount() {
		return newBenefitAmount;
	}

	/**
	 * @param newBenefitAmount
	 *            the newBenefitAmount to set
	 */
	public void setNewBenefitAmount(BigDecimal newBenefitAmount) {
		this.newBenefitAmount = newBenefitAmount;
	}

	/**
	 * @return the newTax
	 */
	public BigDecimal getNewTax() {
		return newTax;
	}

	/**
	 * @param newTax
	 *            the newTax to set
	 */
	public void setNewTax(BigDecimal newTax) {
		this.newTax = newTax;
	}

	/**
	 * @return the newCommissiones
	 */
	public BigDecimal getNewCommissiones() {
		return newCommissiones;
	}

	/**
	 * @param newCommissiones
	 *            the newCommissiones to set
	 */
	public void setNewCommissiones(BigDecimal newCommissiones) {
		this.newCommissiones = newCommissiones;
	}

	/**
	 * @return the adjustmentState
	 */
	public Integer getAdjustmentState() {
		return adjustmentState;
	}

	/**
	 * @param adjustmentState
	 *            the adjustmentState to set
	 */
	public void setAdjustmentState(Integer adjustmentState) {
		this.adjustmentState = adjustmentState;
	}

	/**
	 * @return the indDeliverBenefit
	 */
	public Integer getIndDeliverBenefit() {
		return indDeliverBenefit;
	}

	/**
	 * @param indDeliverBenefit
	 *            the indDeliverBenefit to set
	 */
	public void setIndDeliverBenefit(Integer indDeliverBenefit) {
		this.indDeliverBenefit = indDeliverBenefit;
	}

	/**
	 * @return the indCollectTax
	 */
	public Integer getIndCollectTax() {
		return indCollectTax;
	}

	/**
	 * @param indCollectTax
	 *            the indCollectTax to set
	 */
	public void setIndCollectTax(Integer indCollectTax) {
		this.indCollectTax = indCollectTax;
	}

	/**
	 * @return the indCollectCommissiones
	 */
	public Integer getIndCollectCommissiones() {
		return indCollectCommissiones;
	}

	/**
	 * @param indCollectCommissiones
	 *            the indCollectCommissiones to set
	 */
	public void setIndCollectCommissiones(Integer indCollectCommissiones) {
		this.indCollectCommissiones = indCollectCommissiones;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser
	 *            the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate
	 *            the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp
	 *            the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp
	 *            the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
}
