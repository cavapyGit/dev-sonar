package com.pradera.model.corporatives.specialpayment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;

@Entity
@Table(name="SPECIAL_PAYMENT_HEADER")
public class SpecialPaymentHeader implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id custody operation pk. */
	@Id
	@SequenceGenerator(name="SPECIAL_PAYMENT_HEADER_IDSPHEADERPK_GENERATOR", sequenceName="SQ_ID_SP_HEADER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SPECIAL_PAYMENT_HEADER_IDSPHEADERPK_GENERATOR")
	@Column(name="ID_SP_HEADER_PK")
	private Long idSpecialPaymentHeaderPk;

	@Column(name="PAYMENT_DATE")
	private Date paymentDate;
	
	@Column(name="CURRENCY")
	private Integer currency;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="SPH_STATE")
	private Integer sphState;
	
	 /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@OneToMany(mappedBy = "specialPaymentHeader", cascade=CascadeType.ALL)
	private List<SpecialPaymentDetail> specialPaymentDetail;
	
	@Transient
	private String currencyDesc;
	@Transient
	private String stateDesc;
	
	
	public Long getIdSpecialPaymentHeaderPk() {
		return idSpecialPaymentHeaderPk;
	}

	public void setIdSpecialPaymentHeaderPk(Long idSpecialPaymentHeaderPk) {
		this.idSpecialPaymentHeaderPk = idSpecialPaymentHeaderPk;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getSphState() {
		return sphState;
	}

	public void setSphState(Integer sphState) {
		this.sphState = sphState;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	public List<SpecialPaymentDetail> getSpecialPaymentDetail() {
		return specialPaymentDetail;
	}

	public void setSpecialPaymentDetail(
			List<SpecialPaymentDetail> specialPaymentDetail) {
		this.specialPaymentDetail = specialPaymentDetail;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(Validations.validateIsNotNullAndNotEmpty(loggerUser)){
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
