package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

@Entity
@Table(name="CORPORATIVE_PROCESS_RESULT")
public class CorporativeProcessResult implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_CORPORATIVE_PROCESS_RESULT")
	@SequenceGenerator(name="CORPORATIVE_PROCESS_RESULT_GENERATOR", sequenceName="SQ_ID_CORP_PROCESS_RESULT" ,initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CORPORATIVE_PROCESS_RESULT_GENERATOR")
	private Long idCorporativeProcessResult;

	@Column(name="ACREDITATION_BALANCE")
	private BigDecimal acreditationBalance;

	@Column(name="AVAILABLE_BALANCE")
	private BigDecimal availableBalance;

	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance;

	@Column(name="BORROWER_BALANCE")
	private BigDecimal borrowerBalance;

	@Column(name="BUY_BALANCE")
	private BigDecimal buyBalance;

	@Column(name="OPPOSITION_BALANCE")
	private BigDecimal oppositionBalance;

	@Column(name="CUSTODY_AMOUNT")
	private BigDecimal custodyAmount;
		
	@Column(name="PENALITY_AMOUNT")
	private BigDecimal penalityAmount = BigDecimal.ZERO;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SECURITY_CODE_FK")
	private Security security;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="IND_ADJUSTMENT")
	private Integer indAdjustment;

	@Column(name="IND_ORIGIN_TARGET")
	private Integer indOriginTarget;

	@Column(name="IND_PROCESSED")
	private Integer indProcessed;

	@Column(name="IND_REMANENT")
	private Integer indRemanent;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="LENDER_BALANCE")
	private BigDecimal lenderBalance;

	@Column(name="LOANABLE_BALANCE")
	private BigDecimal loanableBalance;

	@Column(name="MARGIN_BALANCE")
	private BigDecimal marginBalance;

	@Column(name="OTHER_BLOCK_BALANCE")
	private BigDecimal otherBlockBalance;

	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance;

	@Column(name="REPORTED_BALANCE")
	private BigDecimal reportedBalance;

	@Column(name="REPORTING_BALANCE")
	private BigDecimal reportingBalance;

	@Column(name="RESERVE_BALANCE")
	private BigDecimal reserveBalance;

	@Column(name="SELL_BALANCE")
	private BigDecimal sellBalance;

	@Column(name="TAX_AMOUNT")
	private BigDecimal taxAmount;

	/** The exchange amount. */
	@Column(name="EXCHANGE_AMOUNT")
	private BigDecimal exchangeAmount;
	
	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;

	@Column(name="TRANSIT_BALANCE")
	private BigDecimal transitBalance;
	
	@Column(name="IND_AVAILABLE")
	private Integer indAvailable;
	
	@Column(name="IND_BAN_BALANCE")
	private Integer indBanBalance;
	
	@Column(name="IND_PAWN_BALANCE")
	private Integer indPawnBalance;
	
	@Column(name="IND_OTHER_BLOCK_BALANCE")
	private Integer indOtherBlockBalance;

	//bi-directional many-to-one association to BlockCorporativeResult
	@OneToMany(mappedBy="corporativeProcessResult",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<BlockCorporativeResult> blockCorporativeResults;

	//bi-directional many-to-one association to CorporativeOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;

	//bi-directional many-to-one association to ReportCorporativeResult
	@OneToMany(mappedBy="corporativeProcessResult",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<ReportCorporativeResult> reportCorporativeResults;

    public CorporativeProcessResult() {
    	this.taxAmount= BigDecimal.ZERO;
    	this.custodyAmount= BigDecimal.ZERO;
    	this.exchangeAmount = BigDecimal.ZERO;
    	this.indAvailable = ComponentConstant.ZERO;
    	this.indBanBalance = ComponentConstant.ZERO;
    	this.indPawnBalance = ComponentConstant.ZERO;
    	this.indOtherBlockBalance = ComponentConstant.ZERO;
    }

	public CorporativeProcessResult(CorporativeProcessResult objCorporativeProcessResult) {
		super();		
		this.acreditationBalance = objCorporativeProcessResult.getAcreditationBalance();
		this.availableBalance = objCorporativeProcessResult.getAvailableBalance();
		this.banBalance = objCorporativeProcessResult.getBanBalance();
		this.borrowerBalance = objCorporativeProcessResult.getBorrowerBalance();
		this.buyBalance = objCorporativeProcessResult.getBuyBalance();
		this.oppositionBalance = objCorporativeProcessResult.getOppositionBalance();
		this.custodyAmount = objCorporativeProcessResult.getCustodyAmount();
		this.penalityAmount = objCorporativeProcessResult.getPenalityAmount();
		this.holderAccount = objCorporativeProcessResult.getHolderAccount();
		this.security = objCorporativeProcessResult.getSecurity();
		this.participant = objCorporativeProcessResult.getParticipant();
		this.indAdjustment = objCorporativeProcessResult.getIndAdjustment();
		this.indOriginTarget = objCorporativeProcessResult.getIndOriginTarget();
		this.indProcessed = objCorporativeProcessResult.getIndProcessed();
		this.indRemanent = objCorporativeProcessResult.getIndRemanent();
		this.lenderBalance = objCorporativeProcessResult.getLenderBalance();
		this.loanableBalance = objCorporativeProcessResult.getLoanableBalance();
		this.marginBalance = objCorporativeProcessResult.getMarginBalance();
		this.otherBlockBalance = objCorporativeProcessResult.getOtherBlockBalance();
		this.pawnBalance = objCorporativeProcessResult.getPawnBalance();
		this.reportedBalance = objCorporativeProcessResult.getReportedBalance();
		this.reportingBalance = objCorporativeProcessResult.getReportingBalance();
		this.reserveBalance = objCorporativeProcessResult.getReserveBalance();
		this.sellBalance = objCorporativeProcessResult.getSellBalance();
		this.taxAmount = objCorporativeProcessResult.getTaxAmount();
		this.exchangeAmount = objCorporativeProcessResult.getExchangeAmount();
		this.totalBalance = objCorporativeProcessResult.getTotalBalance();
		this.transitBalance = objCorporativeProcessResult.getTransitBalance();
		this.corporativeOperation = objCorporativeProcessResult.getCorporativeOperation();
		//agregando indicadores para issue
		this.indAvailable = objCorporativeProcessResult.getIndAvailable();
		this.indPawnBalance = objCorporativeProcessResult.getIndPawnBalance();
		this.indBanBalance = objCorporativeProcessResult.getIndBanBalance();
		this.indOtherBlockBalance = objCorporativeProcessResult.getIndOtherBlockBalance();
	}

	public Long getIdCorporativeProcessResult() {
		return this.idCorporativeProcessResult;
	}

	public void setIdCorporativeProcessResult(Long idCorporativeProcessResult) {
		this.idCorporativeProcessResult = idCorporativeProcessResult;
	}

	public BigDecimal getAcreditationBalance() {
		return this.acreditationBalance;
	}

	public void setAcreditationBalance(BigDecimal acreditationBalance) {
		this.acreditationBalance = acreditationBalance;
	}

	public BigDecimal getAvailableBalance() {
		return this.availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getBanBalance() {
		return this.banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getBorrowerBalance() {
		return this.borrowerBalance;
	}

	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	public BigDecimal getBuyBalance() {
		return this.buyBalance;
	}

	public void setBuyBalance(BigDecimal buyBalance) {
		this.buyBalance = buyBalance;
	}

	public BigDecimal getOppositionBalance() {
		return this.oppositionBalance;
	}

	public void setOppositionBalance(BigDecimal cessionBalance) {
		this.oppositionBalance = cessionBalance;
	}

	public BigDecimal getCustodyAmount() {
		return this.custodyAmount;
	}

	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getLenderBalance() {
		return this.lenderBalance;
	}

	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	public BigDecimal getLoanableBalance() {
		return this.loanableBalance;
	}

	public void setLoanableBalance(BigDecimal lenderableBalance) {
		this.loanableBalance = lenderableBalance;
	}

	public BigDecimal getMarginBalance() {
		return this.marginBalance;
	}

	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	public BigDecimal getOtherBlockBalance() {
		return this.otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getPawnBalance() {
		return this.pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getReportedBalance() {
		return this.reportedBalance;
	}

	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	public BigDecimal getReportingBalance() {
		return this.reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getReserveBalance() {
		return this.reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getSellBalance() {
		return this.sellBalance;
	}

	public void setSellBalance(BigDecimal sellBalance) {
		this.sellBalance = sellBalance;
	}

	public BigDecimal getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getTransitBalance() {
		return this.transitBalance;
	}

	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}
	
	/**
	 * @return the blockCorporativeResults
	 */
	public List<BlockCorporativeResult> getBlockCorporativeResults() {
		return blockCorporativeResults;
	}

	/**
	 * @param blockCorporativeResults the blockCorporativeResults to set
	 */
	public void setBlockCorporativeResults(
			List<BlockCorporativeResult> blockCorporativeResults) {
		this.blockCorporativeResults = blockCorporativeResults;
	}

	public CorporativeOperation getCorporativeOperation() {
		return this.corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	/**
	 * @return the reportCorporativeResults
	 */
	public List<ReportCorporativeResult> getReportCorporativeResults() {
		return reportCorporativeResults;
	}

	/**
	 * @param reportCorporativeResults the reportCorporativeResults to set
	 */
	public void setReportCorporativeResults(
			List<ReportCorporativeResult> reportCorporativeResults) {
		this.reportCorporativeResults = reportCorporativeResults;
	}

	/**
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * @return the idIsinCodeFk
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param idIsinCodeFk the idIsinCodeFk to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the idParticipantFk
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * @param idParticipantFk the idParticipantFk to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * @return the indAdjustment
	 */
	public Integer getIndAdjustment() {
		return indAdjustment;
	}

	/**
	 * @param indAdjustment the indAdjustment to set
	 */
	public void setIndAdjustment(Integer indAdjustment) {
		this.indAdjustment = indAdjustment;
	}

	/**
	 * @return the indOriginTarget
	 */
	public Integer getIndOriginTarget() {
		return indOriginTarget;
	}

	/**
	 * @param indOriginTarget the indOriginTarget to set
	 */
	public void setIndOriginTarget(Integer indOriginTarget) {
		this.indOriginTarget = indOriginTarget;
	}

	/**
	 * @return the indProcessed
	 */
	public Integer getIndProcessed() {
		return indProcessed;
	}

	/**
	 * @param indProcessed the indProcessed to set
	 */
	public void setIndProcessed(Integer indProcessed) {
		this.indProcessed = indProcessed;
	}

	/**
	 * @return the indRemanent
	 */
	public Integer getIndRemanent() {
		return indRemanent;
	}

	/**
	 * @param indRemanent the indRemanent to set
	 */
	public void setIndRemanent(Integer indRemanent) {
		this.indRemanent = indRemanent;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("blockCorporativeResults", blockCorporativeResults);
		detailsMap.put("reportCorporativeResults", reportCorporativeResults);
        return detailsMap;
	}

	public BigDecimal getPenalityAmount() {
		return penalityAmount;
	}

	public void setPenalityAmount(BigDecimal penalityAmount) {
		this.penalityAmount = penalityAmount;
	}

	public BigDecimal getExchangeAmount() {
		return exchangeAmount;
	}

	public void setExchangeAmount(BigDecimal exchangeAmount) {
		this.exchangeAmount = exchangeAmount;
	}

	/**
	 * @return the indAvailable
	 */
	public Integer getIndAvailable() {
		return indAvailable;
	}

	/**
	 * @param indAvailable the indAvailable to set
	 */
	public void setIndAvailable(Integer indAvailable) {
		this.indAvailable = indAvailable;
	}

	/**
	 * @return the indBanBalance
	 */
	public Integer getIndBanBalance() {
		return indBanBalance;
	}

	/**
	 * @param indBanBalance the indBanBalance to set
	 */
	public void setIndBanBalance(Integer indBanBalance) {
		this.indBanBalance = indBanBalance;
	}

	/**
	 * @return the indPawnBalance
	 */
	public Integer getIndPawnBalance() {
		return indPawnBalance;
	}

	/**
	 * @param indPawnBalance the indPawnBalance to set
	 */
	public void setIndPawnBalance(Integer indPawnBalance) {
		this.indPawnBalance = indPawnBalance;
	}

	/**
	 * @return the indOtherBlockBalance
	 */
	public Integer getIndOtherBlockBalance() {
		return indOtherBlockBalance;
	}

	/**
	 * @param indOtherBlockBalance the indOtherBlockBalance to set
	 */
	public void setIndOtherBlockBalance(Integer indOtherBlockBalance) {
		this.indOtherBlockBalance = indOtherBlockBalance;
	}
	
	
}