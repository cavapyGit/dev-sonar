package com.pradera.model.corporatives.specialpayment.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SpecialPaymentStateType {
	REGISTERED_STATE (new Integer(1700), "REGISTRADO"),
	PAYED_STATE (new Integer(1701), "PAGADO");
	
	private Integer code;
	private String value;
	public static final List<SpecialPaymentStateType> list = new ArrayList<SpecialPaymentStateType>();
	
	
	private SpecialPaymentStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	/** Adding Enum to  McnStateOperationType List. */
	public static final Map<Integer, SpecialPaymentStateType> lookup = new HashMap<Integer, SpecialPaymentStateType>();
	static {
		for (SpecialPaymentStateType s : EnumSet.allOf(SpecialPaymentStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static SpecialPaymentStateType get(Integer code) {
		return lookup.get(code);
	}
}
