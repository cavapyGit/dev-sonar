package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;

/**
 * @author PraderaSofware
 *
 */
@Entity
@Table(name = "SECURITIES_CORPORATIVE")
public class SecuritiesCorporative implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID_SECURITIES_CORPORATIVE_PK")
	@SequenceGenerator(name = "SECURITIES_CORPORATIVE_GENERATOR", sequenceName = "SQ_ID_SECURITIES_CORP_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SECURITIES_CORPORATIVE_GENERATOR")
	private Long idSecuritiesCorporativePk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;
	
	@Column(name="DELIVERY_FACTOR")
	private BigDecimal deliveryFactor;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_APP")
	private long lastModifyApp;
	
	@Column(name="OLD_SHARE_CAPITAL")
	private BigDecimal oldShareCapital;
	
	@Column(name="OLD_SHARE_BALANCE")
	private BigDecimal oldShareBalance;
	
	@Column(name="OLD_CIRCULATION_BALANCE")
	private BigDecimal oldCirculationBalance;
	
	@Column(name="NEW_SHARE_CAPITAL")
	private BigDecimal newShareCapital;
	
	@Column(name="NEW_SHARE_BALANCE")
	private BigDecimal newShareBalance;


	@Column(name="NEW_CIRCULATION_BALANCE")	
	private BigDecimal newCirculationBalance;
	
	@Column(name="VARIATION_AMOUNT")	
	private BigDecimal variationAmount;
	
	@Column(name="ORIGIN_RATIO")
	private BigDecimal originRatio;
	
	/** The target ratio. */
	@Column(name="TARGET_RATIO")
	private BigDecimal targetRatio;
	
	/** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Transient
	private Boolean indSelected = Boolean.FALSE;
	
	@Transient
	private BigDecimal roundedTotal;
	

	/**
	 * @return the idSecuritiesCorporativePk
	 */
	public Long getIdSecuritiesCorporativePk() {
		return idSecuritiesCorporativePk;
	}

	/**
	 * @param idSecuritiesCorporativePk the idSecuritiesCorporativePk to set
	 */
	public void setIdSecuritiesCorporativePk(Long idSecuritiesCorporativePk) {
		this.idSecuritiesCorporativePk = idSecuritiesCorporativePk;
	}

	/**
	 * @return the corporativeOperation
	 */
	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	/**
	 * @param corporativeOperation the corporativeOperation to set
	 */
	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyApp
	 */
	public long getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		 if (loggerUser != null) {
	            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
	            lastModifyDate = loggerUser.getAuditTime();
	            lastModifyIp = loggerUser.getIpAddress();
	            lastModifyUser = loggerUser.getUserName();
	        }		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public BigDecimal getOldShareCapital() {
		return oldShareCapital;
	}

	public void setOldShareCapital(BigDecimal oldShareCapital) {
		this.oldShareCapital = oldShareCapital;
	}

	public BigDecimal getOldShareBalance() {
		return oldShareBalance;
	}

	public void setOldShareBalance(BigDecimal oldShareBalance) {
		this.oldShareBalance = oldShareBalance;
	}

	public BigDecimal getOldCirculationBalance() {
		return oldCirculationBalance;
	}

	public void setOldCirculationBalance(BigDecimal oldCirculationBalance) {
		this.oldCirculationBalance = oldCirculationBalance;
	}

	public BigDecimal getNewShareCapital() {
		return newShareCapital;
	}

	public void setNewShareCapital(BigDecimal newShareCapital) {
		this.newShareCapital = newShareCapital;
	}

	public BigDecimal getNewShareBalance() {
		return newShareBalance;
	}

	public void setNewShareBalance(BigDecimal newShareBalance) {
		this.newShareBalance = newShareBalance;
	}

	public BigDecimal getNewCirculationBalance() {
		return newCirculationBalance;
	}

	public void setNewCirculationBalance(BigDecimal newCirculationBalance) {
		this.newCirculationBalance = newCirculationBalance;
	}

	public BigDecimal getVariationAmount() {
		return variationAmount;
	}

	public void setVariationAmount(BigDecimal variationAmount) {
		this.variationAmount = variationAmount;
	}
	
	public Boolean getIndSelected() {
		return indSelected;
	}

	public void setIndSelected(Boolean indSelected) {
		this.indSelected = indSelected;
	}

	public BigDecimal getDeliveryFactor() {
		return deliveryFactor;
	}

	public void setDeliveryFactor(BigDecimal deliveryFactor) {
		this.deliveryFactor = deliveryFactor;
	}

	public BigDecimal getOriginRatio() {
		return originRatio;
	}

	public void setOriginRatio(BigDecimal originRatio) {
		this.originRatio = originRatio;
	}

	public BigDecimal getTargetRatio() {
		return targetRatio;
	}

	public void setTargetRatio(BigDecimal targetRatio) {
		this.targetRatio = targetRatio;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getRoundedTotal() {
		return roundedTotal;
	}

	public void setRoundedTotal(BigDecimal roundedTotal) {
		this.roundedTotal = roundedTotal;
	}

}
