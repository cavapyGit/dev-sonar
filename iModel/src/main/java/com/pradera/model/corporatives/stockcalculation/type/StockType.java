package com.pradera.model.corporatives.stockcalculation.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum StockType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/05/2013
 */
public enum StockType {
	
	/** The security. */
	SECURITY(Integer.valueOf(205)),
	
	/** The participant. */
	PARTICIPANT(Integer.valueOf(1322)),
	
	/** The holder. */
	HOLDER(Integer.valueOf(1323)),
	
	/** The monthly. */
	MONTHLY(Integer.valueOf(1324)),
	
	
	CORPORATIVE(Integer.valueOf(1626)),
	
	MARKET_FACT(Integer.valueOf(2036)),
	
	CONSOLIDATED_PARTICIPANT(Integer.valueOf(2934)),
	
	CONSOLIDATED_ISSUER(Integer.valueOf(2935))
	;
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	public static final Map<Integer, StockType> lookup = new HashMap<Integer,StockType >();
	
	static{
		for(StockType type: EnumSet.allOf(StockType.class))
		{
			lookup.put(type.getCode(),type);
		}

	}

	/**
	 * Instantiates a new stock type.
	 *
	 * @param code the code
	 */
	private StockType(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the stock type
	 */
	public static StockType get(Integer code)
	{
		return lookup.get(code);
	}
	
	
}
