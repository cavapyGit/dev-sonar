package com.pradera.model.corporatives;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.model.corporatives.type.ImportanceEventType;


/**
 * The persistent class for the CORPORATIVE_EVENT_TYPE database table.
 * 
 */
@Entity
@Table(name="CORPORATIVE_EVENT_TYPE")
@NamedQuery(name="CorporativeEventType.findAll", query="SELECT c FROM CorporativeEventType c")
public class CorporativeEventType implements Serializable, Cloneable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_CORPORATIVE_EVENT_TYPE_PK")
	private Long idCorporativeEventTypePk;

	@Column(name="DELIVERY_ENTITY")
	private Long deliveryEntity;

	private String description;

	@Column(name="IND_CASH_PROCESS", insertable=false)
	private Integer indCashProcess;

	@Column(name="IND_FATCA")
	private Integer indFatca;

	@Column(name="IND_STOCK_PROCESS")
	private Integer indStockProcess;

	@Column(name="IND_TAX")
	private Integer indTax;

	@Column(name="INSTRUMENT_TYPE")
	private Integer instrumentType;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="SOURCE_SECURITIES_NUMBER")
	private Integer sourceSecuritiesNumber;

	@Column(name="STOCK_CALCULATION_TYPE")
	private Integer stockCalculationType;

	@Column(name="TARGET_SECURITIES_NUMBER")
	private Integer targetSecuritiesNumber;
	
	@Column(name="CORPORATIVE_EVENT_TYPE")
	private Integer corporativeEventType;
	
	@Column(name="CORPORATIVE_EVENT_CLASS")
	private Integer corporativeEventClass;

	public CorporativeEventType() {
	}
	
	public CorporativeEventType(Integer corporativeEventType, Integer instrumentType, Integer corporativeEventClass) {
		this.setCorporativeEventType(corporativeEventType);
		this.setInstrumentType(instrumentType);
		this.setCorporativeEventClass(corporativeEventClass);
	}

	public Long getIdCorporativeEventTypePk() {
		return this.idCorporativeEventTypePk;
	}

	public void setIdCorporativeEventTypePk(Long idCorporativeEventTypePk) {
		this.idCorporativeEventTypePk = idCorporativeEventTypePk;
	}

	public Long getDeliveryEntity() {
		return this.deliveryEntity;
	}

	public void setDeliveryEntity(Long deliveryEntity) {
		this.deliveryEntity = deliveryEntity;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIndCashProcess() {
		return this.indCashProcess;
	}

	public void setIndCashProcess(Integer indCashProcess) {
		this.indCashProcess = indCashProcess;
	}

	public Integer getIndFatca() {
		return this.indFatca;
	}

	public void setIndFatca(Integer indFatca) {
		this.indFatca = indFatca;
	}

	public Integer getIndStockProcess() {
		return this.indStockProcess;
	}

	public void setIndStockProcess(Integer indStockProcess) {
		this.indStockProcess = indStockProcess;
	}

	public Integer getIndTax() {
		return this.indTax;
	}

	public void setIndTax(Integer indTax) {
		this.indTax = indTax;
	}

	public Integer getInstrumentType() {
		return this.instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getSourceSecuritiesNumber() {
		return this.sourceSecuritiesNumber;
	}

	public void setSourceSecuritiesNumber(Integer sourceSecuritiesNumber) {
		this.sourceSecuritiesNumber = sourceSecuritiesNumber;
	}

	public Integer getStockCalculationType() {
		return this.stockCalculationType;
	}

	public void setStockCalculationType(Integer stockCalculationType) {
		this.stockCalculationType = stockCalculationType;
	}

	public Integer getTargetSecuritiesNumber() {
		return this.targetSecuritiesNumber;
	}

	public void setTargetSecuritiesNumber(Integer targetSecuritiesNumber) {
		this.targetSecuritiesNumber = targetSecuritiesNumber;
	}

	public Integer getCorporativeEventType() {
		return corporativeEventType;
	}

	public void setCorporativeEventType(Integer corporativeEventType) {
		this.corporativeEventType = corporativeEventType;
	}
	
	public Integer getCorporativeEventClass() {
		return corporativeEventClass;
	}

	public void setCorporativeEventClass(Integer corporativeEventClass) {
		this.corporativeEventClass = corporativeEventClass;
	}

	public CorporativeEventType clone() throws CloneNotSupportedException{
		return (CorporativeEventType) super.clone();
	}
	
	public boolean isEndPreferredSuscriptionEventType(){
		if(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode().equals( getCorporativeEventType() )){
			return true;
		}
		return false;
	}
	
	public boolean isCommonActionsEventType(){
		if(ImportanceEventType.COMMON_ACTIONS.getCode().equals( getCorporativeEventType() )){
			return true;
		}
		return false;
	}
	
	public boolean isPreferredSuscriptionEventType(){
		if(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(getCorporativeEventType())){
			return  true;
		}
		return false;
	}

}