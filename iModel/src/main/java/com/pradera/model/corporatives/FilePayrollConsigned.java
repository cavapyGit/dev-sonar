package com.pradera.model.corporatives;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuer;


/**
 * The persistent class for the BENEFIT_PAYMENT_ALLOCATION database table.
 * 
 */
@Entity
@Table(name="file_payroll_consigned")
public class FilePayrollConsigned implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;	
	
	@Id
	@SequenceGenerator(name="PAYROLL_CONSIGNED_OPERATIONS_GENERATOR", sequenceName="SQ_ID_FILE_PAYROLL_CONSIG_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAYROLL_CONSIGNED_OPERATIONS_GENERATOR")
	@Column(name="ID_FILE_PAYROLL_CONSIGNED_PK")
	private Long idFilePayrollConsignedPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PROCESS_DATE")
	private Date processDate;

	@Column(name="CURRENCY")
	private Integer currency;
	
	@Column(name="STATE")
	private Integer state;
	
	@Column(name="NAME_FILE")
	private String nameFile;

	@Column(name="OPERATION_TOTAL")
	private Integer operationTotal;
	
	@Column(name="OPERATION_PROCESSED")
	private Integer operationProcessed;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="file_payroll")
	private byte[] file;
	
	/** The placement segment detail list. */
	@OneToMany(mappedBy="filePayrollConsigned",cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	private List<FilePayrollConsignedDet> filePayrollConsignedDets;

	
	@Transient
	private Date initialDate;
	
	@Transient
	private Date finalDate;
	
	@Transient
	private Integer filePayrollType;
	
    public FilePayrollConsigned() {
    }
    
    
    
	public Long getIdFilePayrollConsignedPk() {
		return idFilePayrollConsignedPk;
	}



	public void setIdFilePayrollConsignedPk(Long idFilePayrollConsignedPk) {
		this.idFilePayrollConsignedPk = idFilePayrollConsignedPk;
	}



	public Issuer getIssuer() {
		return issuer;
	}



	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}



	public Date getProcessDate() {
		return processDate;
	}



	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}



	public String getNameFile() {
		return nameFile;
	}



	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}



	public Integer getOperationTotal() {
		return operationTotal;
	}



	public void setOperationTotal(Integer operationTotal) {
		this.operationTotal = operationTotal;
	}



	public Integer getOperationProcessed() {
		return operationProcessed;
	}



	public void setOperationProcessed(Integer operationProcessed) {
		this.operationProcessed = operationProcessed;
	}



	public Date getRegistryDate() {
		return registryDate;
	}



	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}



	public String getRegistryUser() {
		return registryUser;
	}



	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}



	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	public String getLastModifyIp() {
		return lastModifyIp;
	}



	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	public String getLastModifyUser() {
		return lastModifyUser;
	}



	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	@Override
	public void setAudit(LoggerUser loggerUser) {		 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}



	public Integer getCurrency() {
		return currency;
	}



	public void setCurrency(Integer currency) {
		this.currency = currency;
	}



	public Date getInitialDate() {
		return initialDate;
	}



	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}



	public Date getFinalDate() {
		return finalDate;
	}



	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}



	public List<FilePayrollConsignedDet> getFilePayrollConsignedDets() {
		return filePayrollConsignedDets;
	}



	public void setFilePayrollConsignedDets(List<FilePayrollConsignedDet> filePayrollConsignedDets) {
		this.filePayrollConsignedDets = filePayrollConsignedDets;
	}



	public Integer getState() {
		return state;
	}



	public void setState(Integer state) {
		this.state = state;
	}



	public byte[] getFile() {
		return file;
	}



	public void setFile(byte[] file) {
		this.file = file;
	}



	public Integer getFilePayrollType() {
		return filePayrollType;
	}



	public void setFilePayrollType(Integer filePayrollType) {
		this.filePayrollType = filePayrollType;
	}



	public Participant getParticipant() {
		return participant;
	}



	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	
}