package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.arkin.model.generalparameter.type.FundOperationStateType;

public enum OperationMovementSourceTarget {

	BOTH(Integer.valueOf(0), "AMBAS CUENTAS"),
	MANAGER(Integer.valueOf(1), "CUENTA ADMINISTRADORA"),
	MANAGED(Integer.valueOf(2), "CUENTA ADMINISTRADA");
			
	/** The code. */
	private Integer code;
	
	private String descripcion;

	private OperationMovementSourceTarget(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
		
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public static OperationMovementSourceTarget get(Integer code) {
		return lookup.get(code);
	}

	public static final List<OperationMovementSourceTarget> list = new ArrayList<OperationMovementSourceTarget>();
	public static final Map<Integer, OperationMovementSourceTarget> lookup = new HashMap<Integer, OperationMovementSourceTarget>();
	static {
		for (OperationMovementSourceTarget s : EnumSet.allOf(OperationMovementSourceTarget.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
