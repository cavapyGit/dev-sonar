package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AccordType.
 */
public enum CorporativeSituationType {

	 
		IDEPOSITARY(new Integer(226), "CONFIRMADO POR LA DEPOSITARIA"),
		ISSUER(new Integer(1472), "CONFIRMADO POR EMISOR"),
		PAID(new Integer(1488), "PAGADO"),
		ISSUER_PAID(new Integer(1706), "PAGADO EMISOR");

		/** The value. */
		private String value;
		
		/** The code. */
		private Integer code;

		/** The Constant list. */
		public static final List<CorporativeSituationType> list= new ArrayList<CorporativeSituationType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, CorporativeSituationType> lookup = new HashMap<Integer,CorporativeSituationType >();

		static{
			for(CorporativeSituationType type: EnumSet.allOf(CorporativeSituationType.class))
			{
				list.add(type);
				lookup.put(type.getCode(),type);

			}

		}


		/**
		 * Instantiates a new accord type.
		 *
		 * @param code the code
		 * @param value the value
		 */
		private CorporativeSituationType(Integer code, String value) {
			this.value = value;
			this.code = code;
		}


		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}


		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}

		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the accord type
		 */
		public static CorporativeSituationType get(Integer code)
		{
			return lookup.get(code);
		}


	}
