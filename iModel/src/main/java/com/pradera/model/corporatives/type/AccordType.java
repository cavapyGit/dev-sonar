package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AccordType.
 */
public enum AccordType {

	 
		/** The coupon payment. */
		COUPON_PAYMENT(new Integer(1399), "PAGO DE CUPON"),
		
		/** The amortization payment. */
		AMORTIZATION_PAYMENT(new Integer(1400), "PAGO DE AMORTIZACION");

		/** The value. */
		private String value;
		
		/** The code. */
		private Integer code;

		/** The Constant list. */
		public static final List<AccordType> list= new ArrayList<AccordType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, AccordType> lookup = new HashMap<Integer,AccordType >();

		static{
			for(AccordType type: EnumSet.allOf(AccordType.class))
			{
				list.add(type);
				lookup.put(type.getCode(),type);

			}

		}


		/**
		 * Instantiates a new accord type.
		 *
		 * @param code the code
		 * @param value the value
		 */
		private AccordType(Integer code, String value) {
			this.value = value;
			this.code = code;
		}


		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}


		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}

		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the accord type
		 */
		public static AccordType get(Integer code)
		{
			return lookup.get(code);
		}


	}
