package com.pradera.model.corporatives.specialpayment.type;

public enum SpecialPaymentOutPutStateType {
	INCORRECT(Integer.valueOf(0),"INCORRECTO"),
	CORRECT(Integer.valueOf(1),"CORRECTO");
	
	private Integer code;
	private String value;
	
	private SpecialPaymentOutPutStateType(Integer code, String value){
		this.code = code;
		this.value = value;
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
