package com.pradera.model.corporatives.stockcalculation.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum StockStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/05/2013
 */
public enum StockStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(1325)),
	
	/** The processing. */
	PROCESSING(Integer.valueOf(1326)),
	
	/** The finished. */
	FINISHED(Integer.valueOf(1327)),
	
	/**The Error*/
	ERROR(Integer.valueOf(2347))
	;
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	public static final Map<Integer, StockStateType> lookup = new HashMap<Integer,StockStateType >();
	
	static{
		for(StockStateType type: EnumSet.allOf(StockStateType.class))
		{
			lookup.put(type.getCode(),type);
		}

	}

	/**
	 * Instantiates a new stock state type.
	 *
	 * @param code the code
	 */
	private StockStateType(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the stock state type
	 */
	public StockStateType get(Integer code) {
		return lookup.get(code);
	}
	
}
