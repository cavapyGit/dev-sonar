package com.pradera.model.corporatives.type;



import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CorporateProcessStateType {

	REGISTERED(new Integer(1309),"REGISTRADO"),
	MODIFIED(new Integer(1310),"MODIFICADO"),
	ANNULLED(new Integer(1311), "ANULADO"),
	PRELIMINARY(new Integer(1312), "PRELIMINAR"),
	PRELIMINARY_IN_PROCESS(new Integer(1313), "PRELIMINAR EN PROCESO"),
	DEFINITIVE(new Integer(1314), "DEFINITIVO"),
	DEFINITIVE_IN_PROCESS(new Integer(1315), "DEFINITIVO EN PROCESO"),
	DEFINITIVE_FAIL(new Integer(1316), "DEFINITIVO FALLO"),
	PRELIMINARY_FAIL(new Integer(2309), "PRELIMINAR FALLO");

	private String value;
	private Integer code;

	public static final List<CorporateProcessStateType> list= new ArrayList<CorporateProcessStateType>();
	public static final Map<Integer, CorporateProcessStateType> lookup = new HashMap<Integer,CorporateProcessStateType >();

	static{
		for(CorporateProcessStateType type: EnumSet.allOf(CorporateProcessStateType.class))
		{
			list.add(type);
			lookup.put(type.getCode(),type);

		}

	}


	private CorporateProcessStateType(Integer code, String value) {
		this.value = value;
		this.code = code;
	}


	public String getValue() {
		return value;
	}


	public Integer getCode() {
		return code;
	}

	public static CorporateProcessStateType get(Integer code)
	{
		return lookup.get(code);
	}


}
