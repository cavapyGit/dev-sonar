package com.pradera.model.corporatives;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="CORPORATIVE_RESULT_DETAIL")
public class CorporativeResultDetail implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_CORPORATIVE_PROCESS_RESULT")
	private Long idCorporativeProcessResultPk;

	@MapsId
	@OneToOne(cascade=CascadeType.PERSIST,fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_PROCESS_RESULT")
	private CorporativeProcessResult corporativeProcessResult;

	@Column(name="AVAILABLE_TAX_AMOUNT")
	private BigDecimal availableTaxAmount;
	
	@Column(name="AVAILABLE_CUSTODY_AMOUNT")
	private BigDecimal availableCustodyAmount;
	
	@Column(name="PAWN_TAX_AMOUNT")
	private BigDecimal pawnTaxAmount;
	
	@Column(name="PAWN_CUSTODY_AMOUNT")
	private BigDecimal pawnCustodyAmount;
	
	@Column(name="BAN_TAX_AMOUNT")
	private BigDecimal banTaxAmount;
	
	@Column(name="BAN_CUSTODY_AMOUNT")
	private BigDecimal banCustodyAmount;
	
	@Column(name="OTHER_TAX_AMOUNT")
	private BigDecimal otherTaxAmount;
	
	@Column(name="OTHER_CUSTODY_AMOUNT")
	private BigDecimal otherCustodyAmount;
	
	@Column(name="RESERVE_TAX_AMOUNT")
	private BigDecimal reserveTaxAmount;
	
	@Column(name="RESERVE_CUSTODY_AMOUNT")
	private BigDecimal reserveCustodyAmount;
	
	@Column(name="OPPOSITION_TAX_AMOUNT")
	private BigDecimal oppositionTaxAmount;
	
	@Column(name="OPPOSITION_CUSTODY_AMOUNT")
	private BigDecimal oppositionCustodyAmount;
	
	@Column(name="REPORTING_TAX_AMOUNT")
	private BigDecimal reportingTaxAmount;

	@Column(name="REPORTING_CUSTODY_AMOUNT")
	private BigDecimal reportingCustodyAmount;

	@Column(name="REPORTED_TAX_AMOUNT")
	private BigDecimal reportedTaxAmount;

	@Column(name="REPORTED_CUSTODY_AMOUNT")
	private BigDecimal reportedCustodyAmount;
	
	@Column(name="MARGIN_TAX_AMOUNT")
	private BigDecimal MarginTaxAmount;

	@Column(name="MARGIN_CUSTODY_AMOUNT")
	private BigDecimal MarginCustodyAmount;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;


	public CorporativeResultDetail() {
		super();
		this.availableCustodyAmount= BigDecimal.ZERO;
		this.availableTaxAmount= BigDecimal.ZERO;
		this.banCustodyAmount= BigDecimal.ZERO;
		this.banTaxAmount= BigDecimal.ZERO;
		this. pawnCustodyAmount= BigDecimal.ZERO;
		this.pawnTaxAmount= BigDecimal.ZERO;
		this.otherCustodyAmount= BigDecimal.ZERO;
		this.otherTaxAmount= BigDecimal.ZERO;
		this.reserveCustodyAmount= BigDecimal.ZERO;
		this.reserveTaxAmount= BigDecimal.ZERO;
		this.oppositionCustodyAmount= BigDecimal.ZERO;
		this.oppositionTaxAmount= BigDecimal.ZERO;
		this.MarginCustodyAmount= BigDecimal.ZERO;
		this.MarginTaxAmount= BigDecimal.ZERO;
		this.reportedCustodyAmount= BigDecimal.ZERO;
		this.reportedTaxAmount= BigDecimal.ZERO;
		this.reportingCustodyAmount= BigDecimal.ZERO;
		this.reportingTaxAmount= BigDecimal.ZERO;
	}

	public Long getIdCorporativeProcessResultPk() {
		return idCorporativeProcessResultPk;
	}

	public void setIdCorporativeProcessResultPk(Long idCorporativeProcessResultPk) {
		this.idCorporativeProcessResultPk = idCorporativeProcessResultPk;
	}

	public CorporativeProcessResult getCorporativeProcessResult() {
		return corporativeProcessResult;
	}

	public void setCorporativeProcessResult(
			CorporativeProcessResult corporativeProcessResult) {
		this.corporativeProcessResult = corporativeProcessResult;
	}

	public BigDecimal getAvailableTaxAmount() {
		return availableTaxAmount;
	}

	public void setAvailableTaxAmount(BigDecimal availableTaxAmount) {
		this.availableTaxAmount = availableTaxAmount;
	}

	public BigDecimal getAvailableCustodyAmount() {
		return availableCustodyAmount;
	}

	public void setAvailableCustodyAmount(BigDecimal availableCustodyAmount) {
		this.availableCustodyAmount = availableCustodyAmount;
	}

	public BigDecimal getPawnTaxAmount() {
		return pawnTaxAmount;
	}

	public void setPawnTaxAmount(BigDecimal pawnTaxAmount) {
		this.pawnTaxAmount = pawnTaxAmount;
	}

	public BigDecimal getPawnCustodyAmount() {
		return pawnCustodyAmount;
	}

	public void setPawnCustodyAmount(BigDecimal pawnCustodyAmount) {
		this.pawnCustodyAmount = pawnCustodyAmount;
	}

	public BigDecimal getBanTaxAmount() {
		return banTaxAmount;
	}

	public void setBanTaxAmount(BigDecimal banTaxAmount) {
		this.banTaxAmount = banTaxAmount;
	}

	public BigDecimal getBanCustodyAmount() {
		return banCustodyAmount;
	}

	public void setBanCustodyAmount(BigDecimal banCustodyAmount) {
		this.banCustodyAmount = banCustodyAmount;
	}

	public BigDecimal getOtherTaxAmount() {
		return otherTaxAmount;
	}

	public void setOtherTaxAmount(BigDecimal otherTaxAmount) {
		this.otherTaxAmount = otherTaxAmount;
	}

	public BigDecimal getOtherCustodyAmount() {
		return otherCustodyAmount;
	}

	public void setOtherCustodyAmount(BigDecimal otherCustodyAmount) {
		this.otherCustodyAmount = otherCustodyAmount;
	}

	public BigDecimal getReserveTaxAmount() {
		return reserveTaxAmount;
	}

	public void setReserveTaxAmount(BigDecimal reserveTaxAmount) {
		this.reserveTaxAmount = reserveTaxAmount;
	}

	public BigDecimal getReserveCustodyAmount() {
		return reserveCustodyAmount;
	}

	public void setReserveCustodyAmount(BigDecimal reserveCustodyAmount) {
		this.reserveCustodyAmount = reserveCustodyAmount;
	}

	public BigDecimal getOppositionTaxAmount() {
		return oppositionTaxAmount;
	}

	public void setOppositionTaxAmount(BigDecimal oppositionTaxAmount) {
		this.oppositionTaxAmount = oppositionTaxAmount;
	}

	public BigDecimal getOppositionCustodyAmount() {
		return oppositionCustodyAmount;
	}

	public void setOppositionCustodyAmount(BigDecimal oppositionCustodyAmount) {
		this.oppositionCustodyAmount = oppositionCustodyAmount;
	}

	public BigDecimal getReportingTaxAmount() {
		return reportingTaxAmount;
	}

	public void setReportingTaxAmount(BigDecimal reportingTaxAmount) {
		this.reportingTaxAmount = reportingTaxAmount;
	}

	public BigDecimal getReportingCustodyAmount() {
		return reportingCustodyAmount;
	}

	public void setReportingCustodyAmount(BigDecimal reportingCustodyAmount) {
		this.reportingCustodyAmount = reportingCustodyAmount;
	}

	public BigDecimal getReportedTaxAmount() {
		return reportedTaxAmount;
	}

	public void setReportedTaxAmount(BigDecimal reportedTaxAmount) {
		this.reportedTaxAmount = reportedTaxAmount;
	}

	public BigDecimal getReportedCustodyAmount() {
		return reportedCustodyAmount;
	}

	public void setReportedCustodyAmount(BigDecimal reportedCustodyAmount) {
		this.reportedCustodyAmount = reportedCustodyAmount;
	}

	public BigDecimal getMarginTaxAmount() {
		return MarginTaxAmount;
	}

	public void setMarginTaxAmount(BigDecimal marginTaxAmount) {
		MarginTaxAmount = marginTaxAmount;
	}

	public BigDecimal getMarginCustodyAmount() {
		return MarginCustodyAmount;
	}

	public void setMarginCustodyAmount(BigDecimal marginCustodyAmount) {
		MarginCustodyAmount = marginCustodyAmount;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(corporativeProcessResult!=null){
            	corporativeProcessResult.setLastModifyApp(lastModifyApp);
            	corporativeProcessResult.setLastModifyDate(lastModifyDate);
            	corporativeProcessResult.setLastModifyIp(lastModifyIp);
            	corporativeProcessResult.setLastModifyUser(lastModifyUser);
    		}
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}
	
}
