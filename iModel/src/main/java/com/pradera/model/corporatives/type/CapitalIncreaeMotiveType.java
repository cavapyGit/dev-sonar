package com.pradera.model.corporatives.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CapitalIncreaeMotiveType {
	
 
	     REVALUATION_EXCEDENT(new Integer(1296),"EXCEDENTE DE REVALUACION"),
		CAPITAL_REEXPRESION(new Integer(231),"REEXPRESION DE CAPITAL"),
		RESERVE_CAPITALIZATION(new Integer(1297),"CAPITALIZACION DE RESERVAS"),
		UTILITIES_CAPITALIZATION(new Integer(1298),"CAPITALIZACION DE UTILIDADES"),
		FUSION(new Integer(1299),"FUSION"),
		EXPOSITION_RESULT(new Integer(1300),"RESULT POR EXPOSICION A LA INFLACION"),
		OTHERS(new Integer(1301),"OTROS"),
		VOLUNTIER_REVALUATION(new Integer(1302),"REVALUACION VOLUNTARIA");

		private String value;
		private Integer code;

		public static final List<CapitalIncreaeMotiveType> list= new ArrayList<CapitalIncreaeMotiveType>();
		public static final Map<Integer, CapitalIncreaeMotiveType> lookup = new HashMap<Integer,CapitalIncreaeMotiveType >();

		static{
			for(CapitalIncreaeMotiveType type: EnumSet.allOf(CapitalIncreaeMotiveType.class))
			{
				list.add(type);
				lookup.put(type.getCode(),type);

			}

		}


		private CapitalIncreaeMotiveType(Integer code, String value) {
			this.value = value;
			this.code = code;
		}


		public String getValue() {
			return value;
		}


		public Integer getCode() {
			return code;
		}

		public static CapitalIncreaeMotiveType get(Integer code)
		{
			return lookup.get(code);
		}


	}



