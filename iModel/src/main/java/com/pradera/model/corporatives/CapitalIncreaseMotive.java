package com.pradera.model.corporatives;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the CAPITAL_INCREASE_MOTIVE database table.
 * 
 */
@Entity
@Table(name="CAPITAL_INCREASE_MOTIVE")
public class CapitalIncreaseMotive implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_CAPITAL_INCREASE_PK")
	@SequenceGenerator(name="CAPITAL_INCREASE_GENERATOR", sequenceName="SQ_ID_CAPITAL_INCREASE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CAPITAL_INCREASE_GENERATOR")
	private Long idCapitalIncreasePk;

	@Column(name="INCREASE_MOTIVE")
	private Integer increaseMotive;

	@Column(name="INCREASE_QUANTITY")
	private BigDecimal increaseQuantity;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to CorporativeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK",referencedColumnName="ID_CORPORATIVE_OPERATION_PK")
	private CorporativeOperation corporativeOperation;

    public CapitalIncreaseMotive() {
    }

	public Long getIdCapitalIncreasePk() {
		return this.idCapitalIncreasePk;
	}

	public void setIdCapitalIncreasePk(Long idCapitalIncreasePk) {
		this.idCapitalIncreasePk = idCapitalIncreasePk;
	}

	public Integer getIncreaseMotive() {
		return this.increaseMotive;
	}

	public void setIncreaseMotive(Integer increaseMotive) {
		this.increaseMotive = increaseMotive;
	}

	public BigDecimal getIncreaseQuantity() {
		return this.increaseQuantity;
	}

	public void setIncreaseQuantity(BigDecimal increaseQuantity) {
		this.increaseQuantity = increaseQuantity;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public CorporativeOperation getCorporativeOperation() {
		return this.corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {		 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		
        return detailsMap;
	}



}