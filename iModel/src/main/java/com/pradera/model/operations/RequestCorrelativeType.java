package com.pradera.model.operations;

import java.util.ArrayList;
import java.util.List;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum RequestCorrelativeType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/10/2013
 */
public enum RequestCorrelativeType {
	
	/** The request operation register rnt. */
	REQUEST_OPERATION_REGISTER_RNT("SQ_REQ_REGISTER_RNT"),
	
	/** The request operation modification rnt. */
	REQUEST_OPERATION_MODIFICATION_RNT("SQ_REQ_MODIFICATION_RNT"),
	
	/** The request operation block rnt. */
	REQUEST_OPERATION_BLOCK_UNBLOCK_RNT("SQ_REQ_BLOCK_RNT"),
	
	/** The request operation register account. */
	REQUEST_OPERATION_REGISTER_ACCOUNT("SQ_REQ_REGISTER_ACCOUNT"),
	
	/** The request operation modification account. */
	REQUEST_OPERATION_MODIFICATION_ACCOUNT("SQ_REQ_MODIFICATION_ACCOUNT"),
	
	/** The request operation block account. */
	REQUEST_OPERATION_BLOCK_UNBLOC_CLOSE_ACCOUNT("SQ_REQ_BLOCK_ACCOUNT"),
	
	/** The request operation block participant. */
	REQUEST_OPERATION_BLOCK_UNBLOCK_ANNUL_PARTICIPANT("SQ_REQ_BLOCK_PARTICIPANT"),
	
	/** The request operation unblock participant. */
	REQUEST_OPERATION_MODIFICATION_PARTICIPANT("SQ_REQ_MODIFI_PARTICIPANT"),
	
	/** The request operation block issuer. */
	REQUEST_OPERATION_BLOCK_UNBLOCK_ISSUER("SQ_REQ_BLOCK_ISSUER"),
	
	/** The request operation unblock issuer. */
	REQUEST_OPERATION_MODIFICATION_ISSUER("SQ_REQ_MODIFI_ISSUER"),
	
	/** The request operation transfer securities available. */
	REQUEST_OPERATION_TRANSFER_SECURITIES_AVAILABLE("SQ_REQ_TRANS_SEC_AVAILABLE"),
	
	/** The request operation transfer securities blocked. */
	REQUEST_OPERATION_TRANSFER_SECURITIES_BLOCKED("SQ_REQ_TRANS_SEC_BLOCKED"),
	
	/** The request operation participant unification. */
	REQUEST_OPERATION_PARTICIPANT_UNIFICATION("SQ_REQ_PARTICIPANT_UNIFICATION"),
	
	/** The request operation physical securities deposit. */
	REQUEST_OPERATION_PHYSICAL_SECURITIES_DEPOSIT("SQ_REQ_PHYSICAL_SEC_DEPOSIT"),
	
	/** The request operation physical securities withdrawal. */
	REQUEST_OPERATION_PHYSICAL_SECURITIES_WITHDRAWAL("SQ_REQ_PHYSICAL_SEC_WITHDRAWAL"),
	
	/** The request operation dematerialization. */
	REQUEST_OPERATION_DEMATERIALIZATION("SQ_REQ_DEMATERIALIZATION"),
	
	/** The request operation securities withdrawal. */
	REQUEST_OPERATION_SECURITIES_WITHDRAWAL("SQ_REQ_SECURITIES_WITHDRAWAL"),
	
	/** The request operation blocking. */
	REQUEST_OPERATION_BLOCKING("SQ_REQ_BLOCKING"),
	
	/** The request operation unblocking. */
	REQUEST_OPERATION_UNBLOCKING("SQ_REQ_UNBLOCKING"),
	
	/** The request operation accreditation certificate. */
	REQUEST_OPERATION_ACCREDITATION_CERTIFICATE("SQ_REQ_ACCREDITATION_CERTIF"),
	
	/** The request unblock operation accreditation certificate. */
	REQUEST_UNBLOCK_OPERATION_ACCREDITATION_CERTIFICATE("SQ_ID_UNBLOCK_ACCREDIT_OP"),
	
	/** The request operation change owner. */
	REQUEST_OPERATION_CHANGE_OWNER("SQ_REQ_CHANGE_OWNER"),
	
	/** The request operation rectification. */
	REQUEST_OPERATION_RECTIFICATION("SQ_REQ_RECTIFICATION"),
	
	/** The request operation otc. */
	REQUEST_OPERATION_OTC("SQ_REQ_OTC"),
	
	/** The request operation international. */
	REQUEST_OPERATION_INTERNATIONAL("SQ_REQ_INTERNATIONAL"),
	
	/** The request grant operation. */
	REQUEST_COUPON_GRANT_OPERATION("SQ_REQ_COUPON_GRANT"),
	
	/** The request transfer operation. */
	REQUEST_COUPON_TRANSFER_OPERATION("SQ_REQ_COUPON_TRANSFER"),
	
	/** The request transfer operation. */
	REQUEST_UNBLOCK_GRANT_OPERATION("SQ_REQ_UNBLOCK_GRANT"),
	
	/** The request transfer operation. */
	REQUEST_PARTICIPANT_PENALTY("SQ_REQ_PARTICIPANT_PENALTY"),
	
	/** The request operation securities renewal operation. */
	REQUEST_OPERATION_SECURITIES_RENEWAL("SQ_ID_SECURITIES_RENEWAL"),
	
	ACCOUNT_ID_HOLDER_PK("SQ_ID_HOLDER_PK"),
	
	ACCOUNT_ID_HOLDER_PK_AB("SQ_ID_HOLDER_PK_AB"),
	
	ACCOUNT_ID_HOLDER_PK_SAFI("SQ_ID_HOLDER_PK_SAFI"),
	
	ACCOUNT_ID_HOLDER_PK_FIC("SQ_ID_HOLDER_PK_FIC"),
	
	ACCOUNT_ID_HOLDER_PK_FI("SQ_ID_HOLDER_PK_FI"),
	
	ACCOUNT_ID_HOLDER_PK_AFP("SQ_ID_HOLDER_PK_AFP"),
	
	ACCOUNT_ID_HOLDER_PK_B("SQ_ID_HOLDER_PK_B"),
	
	ACCOUNT_ID_HOLDER_PK_MFC("SQ_ID_HOLDER_PK_MFC"),
	
	ACCOUNT_ID_HOLDER_PK_CS("SQ_ID_HOLDER_PK_CS");

	/** The value. */
	private String value;
	
	/** The Constant lookup. */

	public static final List<RequestCorrelativeType> list = new ArrayList<RequestCorrelativeType>();
    static {
        for (RequestCorrelativeType d : RequestCorrelativeType.values())
        	list.add(d);
    }

	/**
	 * Instantiates a new request correlative type.
	 *
	 * @param value the value
	 */
	private RequestCorrelativeType(String value) {
		this.value =value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}	
}
