package com.pradera.model.paymentagent.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PaymentAgentStateType {
	
	REGISTERED_PAYER_AGENT(Integer.valueOf(2372),"REGISTRADO"),
	CONFIRMED_PAYER_AGENT(Integer.valueOf(2373),"CONFIRMADO"),
	BLOCKED_PAYER_AGENT(Integer.valueOf(2375),"BLOQUEADO"),
	REJECTED_PAYER_AGENT(Integer.valueOf(2374),"RECHAZADO");
	
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PaymentAgentStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<PaymentAgentStateType> list = new ArrayList<PaymentAgentStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, PaymentAgentStateType> lookup = new HashMap<Integer, PaymentAgentStateType>();
	static {
		for (PaymentAgentStateType s : EnumSet.allOf(PaymentAgentStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant block unblock request type
	 */
	public static PaymentAgentStateType get(Integer code) {
		return lookup.get(code);
	}
}
