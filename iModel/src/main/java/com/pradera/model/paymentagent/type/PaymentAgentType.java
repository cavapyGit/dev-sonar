package com.pradera.model.paymentagent.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PaymentAgentType {
	//paga con lip
	PAYMENT_LIP(new Integer(1), "CONTACTO AGENTE PAGADOR PAGA CON LIP"),
	//paga sin lip
	PAYMENT_NO_LIP(new Integer(0),"CONTACTO AGENTE PAGADOR NO PAGA CON LIP"),
	
	//id de agente pagador juridico BCB
	PAYMNET_AGENT_BCB(new Integer(16),"BIC"),
	//id de agente pagador juridico SOMA
	PAYMNET_AGENT_SOMA(new Integer(158),"SOMA-BIC"),
	//id de agente pagador juridico SOSP 
	PAYMNET_AGENT_SOSP(new Integer(159),"SOSP-BIC"),
	
	;
	
	private Integer code;
	private String description;
	
	public static final List<PaymentAgentType> lst=new ArrayList<PaymentAgentType>();
	public static final Map<Integer, PaymentAgentType> lookup=new HashMap<Integer, PaymentAgentType>();
	static{
		for(PaymentAgentType x:EnumSet.allOf(PaymentAgentType.class)){
			lst.add(x);
			lookup.put(x.getCode(), x);
		}
	}
	
	//constructor
	private PaymentAgentType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
