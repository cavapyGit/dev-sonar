package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TradeSettlementRequestStateType {
	
	REGISTERED(Integer.valueOf(2192),"REGISTRADO"),
	APPROVE(Integer.valueOf(2194),"APROBADO"),
	ANNULATE(Integer.valueOf(2193),"ANNULADO"),
	REJECT(Integer.valueOf(2196),"RECHAZADO"),
	REVIEW(Integer.valueOf(2195),"REVISADO"),
	CONFIRM(Integer.valueOf(2197),"CONFIRMADO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;
	private TradeSettlementRequestStateType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static final List<TradeSettlementRequestStateType> list = new ArrayList<TradeSettlementRequestStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, TradeSettlementRequestStateType> lookup = new HashMap<Integer, TradeSettlementRequestStateType>();
	static {
		for (TradeSettlementRequestStateType s : EnumSet.allOf(TradeSettlementRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static TradeSettlementRequestStateType get(Integer codigo) {
		return lookup.get(codigo);
	}

}
