package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.ModalityGroup;

@Entity
@Table(name="CHAINED_HOLDER_OPERATION")
public class ChainedHolderOperation implements Serializable, Auditable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="CHAINED_OPERATION_GENERATOR", sequenceName="SQ_ID_CHAINED_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CHAINED_OPERATION_GENERATOR")
	@Column(name="ID_CHAINED_HOLDER_OPERATION_PK")
	private Long idChainedHolderOperationPk;
	
	@Column(name="CHAIN_STATE")
	private Integer chaintState;
	
	@Column(name="STOCK_REFERENCE")
	private Long stockReference;
	
	@Column(name="REGISTER_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;
	
	@Column(name="REGISTER_USER")
	private String registerUser;
	
	@Column(name="CONFIRM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date confirmDate;
	
	@Column(name="CONFIRM_USER")
	private String confirmyUser;
	
	@Column(name="REJECT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date rejectDate;
	
	@Column(name="REJECT_USER")
	private String rejectUser;
	
	@Column(name="CANCEL_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date cancelDate;
	
	@Column(name="CANCEL_USER")
	private String cancelUser;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MODALITY_GROUP_FK")	
	private ModalityGroup modalityGroup;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")	
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")	
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")	
	private Security security;
	
	@Temporal( TemporalType.DATE)
	@Column(name="SETTLEMENT_DATE")
	private Date settlementDate;
	
	@Column(name="CURRENCY")
	private Integer currency;
	
	@Column(name="IND_EXTENDED")
	private Integer indExtended;
	
	@Column(name="PURCHASE_QUANTITY")
	private BigDecimal purchaseQuantity;
	
	@Column(name="SALE_QUANTITY")
	private BigDecimal saleQuantity;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_REF_CHAINED_OPERATION_FK")
	private ChainedHolderOperation refChainedHolderOperation;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="IND_AUTOMATIC")
	private Integer indAutomatic;
	
	@Transient
	private String desCurrency;
	
	@Transient
	private String desChainState;
	
	@Transient
	private String desIndExtended;
	
	
	public Long getIdChainedHolderOperationPk() {
		return idChainedHolderOperationPk;
	}

	public void setIdChainedHolderOperationPk(Long idChainedHolderOperationPk) {
		this.idChainedHolderOperationPk = idChainedHolderOperationPk;
	}

	public Integer getChaintState() {
		return chaintState;
	}

	public void setChaintState(Integer chaintState) {
		this.chaintState = chaintState;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmyUser() {
		return confirmyUser;
	}

	public void setConfirmyUser(String confirmyUser) {
		this.confirmyUser = confirmyUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Date getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public BigDecimal getPurchaseQuantity() {
		return purchaseQuantity;
	}

	public void setPurchaseQuantity(BigDecimal purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}

	public BigDecimal getSaleQuantity() {
		return saleQuantity;
	}

	public void setSaleQuantity(BigDecimal saleQuantity) {
		this.saleQuantity = saleQuantity;
	}

	public ChainedHolderOperation getRefChainedHolderOperation() {
		return refChainedHolderOperation;
	}

	public void setRefChainedHolderOperation(
			ChainedHolderOperation refChainedHolderOperation) {
		this.refChainedHolderOperation = refChainedHolderOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Long getStockReference() {
		return stockReference;
	}

	public void setStockReference(Long stockReference) {
		this.stockReference = stockReference;
	}

	public ModalityGroup getModalityGroup() {
		return modalityGroup;
	}

	public void setModalityGroup(ModalityGroup modalityGroup) {
		this.modalityGroup = modalityGroup;
	}

	
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	
	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Integer getIndExtended() {
		return indExtended;
	}

	public void setIndExtended(Integer indExtended) {
		this.indExtended = indExtended;
	}

	public String getDesCurrency() {
		return desCurrency;
	}

	public void setDesCurrency(String desCurrency) {
		this.desCurrency = desCurrency;
	}

	public String getDesChainState() {
		return desChainState;
	}

	public void setDesChainState(String desChainState) {
		this.desChainState = desChainState;
	}

	public String getDesIndExtended() {
		return desIndExtended;
	}

	public void setDesIndExtended(String desIndExtended) {
		this.desIndExtended = desIndExtended;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancelUser() {
		return cancelUser;
	}

	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}

	public Integer getIndAutomatic() {
		return indAutomatic;
	}

	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        lastModifyDate = loggerUser.getAuditTime();
        lastModifyIp = loggerUser.getIpAddress();
        lastModifyUser = loggerUser.getUserName();
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
}
