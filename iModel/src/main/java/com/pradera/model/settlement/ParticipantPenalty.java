package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;


/**
 * The persistent class for the PARTICIPANT_PENALTY database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_PENALTY")
@NamedQuery(name="ParticipantPenalty.findAll", query="SELECT p FROM ParticipantPenalty p")
public class ParticipantPenalty implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARTICIPANT_PENALTY_IDPARTICIPANTPENALTYPK_GENERATOR", sequenceName="SQ_ID_PARTICIPANT_PENALTY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_PENALTY_IDPARTICIPANTPENALTYPK_GENERATOR")
	@Column(name="ID_PARTICIPANT_PENALTY_PK", unique=true, nullable=false, precision=10)
	private Long idParticipantPenaltyPk;

	@Column(name="RESOLUTION_NUMBER", length=20)
	private String resolutionNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="PENALTY_APPEAL_DATE")
	private Date penaltyAppealDate;

	@Temporal(TemporalType.DATE)
	@Column(name="SETTLEMENT_DATE")
	private Date settlementDate;

	@Temporal(TemporalType.DATE)
	@Column(name="PENALTY_EMITION_DATE", nullable=false)
	private Date penaltyEmitionDate;

	@Temporal(TemporalType.DATE)
	@Column(name="PENALTY_EXPIRE_DATE", nullable=false)
	private Date penaltyExpireDate;

	@Column(name="PENALTY_MOTIVE", nullable=false)
	private Integer penaltyMotive;
	
	@Column(name="PENALTY_MOTIVE_OTHER")
	private String penaltyMotiveOther;
	
	@Column(name="PENALTY_LEVEL")
	private Integer penaltyLevel;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Column(name="PENALTY_AMOUNT")
	private BigDecimal penaltyAmount;
	
	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="PENALTY_PERIOD", nullable=false)
	private Integer penaltyPeriod;

	@Column(name="PENALTY_STATE", nullable=false)
	private Integer penaltyState;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE", nullable=false)
	private Date registerDate;

	@Column(name="REGISTER_USER", length=20, nullable=false)
	private String registerUser;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", nullable=false)
	private Participant participant;
	
	@OneToMany(mappedBy="participantPenalty",fetch=FetchType.LAZY)
	private List<ParticipantPenaltyOperation> listParticipantPenaltyOperation;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	public ParticipantPenalty() {
	}

	public Long getIdParticipantPenaltyPk() {
		return idParticipantPenaltyPk;
	}

	public void setIdParticipantPenaltyPk(Long idParticipantPenaltyPk) {
		this.idParticipantPenaltyPk = idParticipantPenaltyPk;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getPenaltyAppealDate() {
		return penaltyAppealDate;
	}

	public void setPenaltyAppealDate(Date penaltyAppealDate) {
		this.penaltyAppealDate = penaltyAppealDate;
	}

	public Date getPenaltyEmitionDate() {
		return penaltyEmitionDate;
	}

	public void setPenaltyEmitionDate(Date penaltyEmitionDate) {
		this.penaltyEmitionDate = penaltyEmitionDate;
	}

	public Date getPenaltyExpireDate() {
		return penaltyExpireDate;
	}

	public void setPenaltyExpireDate(Date penaltyExpireDate) {
		this.penaltyExpireDate = penaltyExpireDate;
	}

	public Integer getPenaltyMotive() {
		return penaltyMotive;
	}

	public void setPenaltyMotive(Integer penaltyMotive) {
		this.penaltyMotive = penaltyMotive;
	}

	public Integer getPenaltyPeriod() {
		return penaltyPeriod;
	}

	public void setPenaltyPeriod(Integer penaltyPeriod) {
		this.penaltyPeriod = penaltyPeriod;
	}

	public Integer getPenaltyState() {
		return penaltyState;
	}

	public void setPenaltyState(Integer penaltyState) {
		this.penaltyState = penaltyState;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	public String getPenaltyMotiveOther() {
		return penaltyMotiveOther;
	}

	public void setPenaltyMotiveOther(String penaltyMotiveOther) {
		this.penaltyMotiveOther = penaltyMotiveOther;
	}
	
	public List<ParticipantPenaltyOperation> getListParticipantPenaltyOperation() {
		return listParticipantPenaltyOperation;
	}

	public void setListParticipantPenaltyOperation(
			List<ParticipantPenaltyOperation> listParticipantPenaltyOperation) {
		this.listParticipantPenaltyOperation = listParticipantPenaltyOperation;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		return detailsMap;
	}

	public String getResolutionNumber() {
		return resolutionNumber;
	}

	public void setResolutionNumber(String resolutionNumber) {
		this.resolutionNumber = resolutionNumber;
	}

	public BigDecimal getPenaltyAmount() {
		return penaltyAmount;
	}

	public void setPenaltyAmount(BigDecimal penaltyAmount) {
		this.penaltyAmount = penaltyAmount;
	}

	public Date getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getPenaltyLevel() {
		return penaltyLevel;
	}

	public void setPenaltyLevel(Integer penaltyLevel) {
		this.penaltyLevel = penaltyLevel;
	}
}