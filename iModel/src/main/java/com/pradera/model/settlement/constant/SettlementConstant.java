package com.pradera.model.settlement.constant;

public class SettlementConstant {

	/* Parameters to unfulfillment process */
	public static final String CLOSSING_ASSIGNMENT_PROCESS = "CLOSSING_ASSIGNMENT_PROCESS";
	public static final String ASSIGNMENT_UNFULFILLMENT = "ASSIGNMENT_UNFULFILLMENT";
	public static final String CLOSSING_SETTLEMENT_PROCESS = "CLOSSING_SETTLEMENT_PROCESS";
	public static final String MECHANISM_PARAMETER = "MECHANISM_PARAMETER";
	public static final String MODALITY_GROUP_PARAMETER = "MODALITY_GROUP_PARAMETER";
	public static final String MODALITY_PARAMETER = "MODALITY_PARAMETER";
	public static final String CURRENCY_PARAMETER = "CURRENCY_PARAMETER";
	public static final String ID_SETTLEMENT_PROCESS_PARAMETER = "ID_SETTLEMENT_PROCESS_PARAMETER";
	public static final String OPERATION_PART_PARAMETER = "OPERATION_PART_PARAMETER";
	public static final String SCHEDULE_TYPE = "SCHEDULE_TYPE";
	public static final Integer DAILY_EXCHANGE_RATE_SOURCE_INFO = Integer.valueOf(212);
	
	public static final Integer NEGATIVE_POSITION= new Integer(-1);
	public static final Integer POSITIVE_POSITION= new Integer(1);
	public static final Integer NEUTRAL_POSITION= new Integer(0);
	
	public static final String ID_PARTICIPANT= "ID_PARTICIPANT";
	
	public static final String MSG_REASSIGNMENT_REQUEST = "REASIGNACION DE CUENTAS";
	public static final String MSG_CHAINED_HOLDER_OPERATION = "ENCADENAMIENTO DE OPERACIONES";
	public static final String MSG_FUNDS_OPERATION_BOB = "DEPOSITO FONDOS BOB";
	public static final String MSG_FUNDS_OPERATION_USD = "DEPOSITO FONDOS USD";
	public static final String MSG_FUNDS_OPERATION = "DEPOSITO FONDOS";
}
