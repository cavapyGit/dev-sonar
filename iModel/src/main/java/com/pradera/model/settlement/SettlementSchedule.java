package com.pradera.model.settlement;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
/**
 * The persistent class for the SETTLEMENT_PROCESS database table.
 * 
 */
@Entity
@Table(name="SETTLEMENT_SCHEDULE")
public class SettlementSchedule implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SETTLEMENT_SCHED_GENERATOR", sequenceName="SQ_ID_SETTLEMENT_SCHEDULE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SETTLEMENT_SCHED_GENERATOR")
	@Column(name="ID_SETTLEMENT_SCHEDULE_PK")
	private Long idSettlementSchedulePk;

	@Temporal(TemporalType.TIME)
	@Column(name="START_DATE")
	private Date startDate;

	@Temporal(TemporalType.TIME)
	@Column(name="END_DATE")
	private Date endDate;

	@Column(name="EXECUTION_TIMES")
	private Integer executionTimes;
	
	@Column(name="IND_SANCTION")
	private Integer indSanction;
	
	@Column(name="IND_EXTEND")
	private Integer indExtend;
	
	@Column(name="IND_EXTEND_SETTLEMENT")
	private Integer indExtendSettlement;
	
	@Column(name="IND_SETTLEMENT")
	private Integer indSettlement;

	@Column(name="IND_UNFULFILLMENT")
	private Integer indUnfulfillment;
	
	@Column(name="SCHEDULE_TYPE")
	private Integer scheduleType;
	
	@Column(name="ID_SCHEDULE_REF_FK")
	private Integer scheduleTypeRef;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private String scheduleTypeDescription;
	
	public String getScheduleTypeDescription() {
		return scheduleTypeDescription;
	}

	public void setScheduleTypeDescription(String scheduleTypeDescription) {
		this.scheduleTypeDescription = scheduleTypeDescription;
	}

	public Long getIdSettlementSchedulePk() {
		return idSettlementSchedulePk;
	}

	public void setIdSettlementSchedulePk(Long idSettlementSchedulePk) {
		this.idSettlementSchedulePk = idSettlementSchedulePk;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getExecutionTimes() {
		return executionTimes;
	}

	public void setExecutionTimes(Integer executionTimes) {
		this.executionTimes = executionTimes;
	}

	public Integer getIndSanction() {
		return indSanction;
	}

	public void setIndSanction(Integer indSanction) {
		this.indSanction = indSanction;
	}

	public Integer getIndExtend() {
		return indExtend;
	}

	public void setIndExtend(Integer indExtend) {
		this.indExtend = indExtend;
	}

	public Integer getIndSettlement() {
		return indSettlement;
	}

	public void setIndSettlement(Integer indSettlement) {
		this.indSettlement = indSettlement;
	}

	public Integer getIndUnfulfillment() {
		return indUnfulfillment;
	}

	public void setIndUnfulfillment(Integer indUnfulfillment) {
		this.indUnfulfillment = indUnfulfillment;
	}

	public Integer getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem()!=null){
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			}else{
				lastModifyApp = BooleanType.NO.getCode();
			}
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}

	public Integer getScheduleTypeRef() {
		return scheduleTypeRef;
	}

	public void setScheduleTypeRef(Integer scheduleTypeRef) {
		this.scheduleTypeRef = scheduleTypeRef;
	}

	public Integer getIndExtendSettlement() {
		return indExtendSettlement;
	}

	public void setIndExtendSettlement(Integer indExtendSettlement) {
		this.indExtendSettlement = indExtendSettlement;
	}

}