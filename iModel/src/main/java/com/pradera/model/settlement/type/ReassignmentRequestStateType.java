package com.pradera.model.settlement.type;


public enum ReassignmentRequestStateType {

	REGISTERED (Integer.valueOf(2159)), 
	REJECTED (Integer.valueOf(2160)),
	CONFIRMED (Integer.valueOf(2161)),
	AUTHORIZED (Integer.valueOf(2365));
	
	/** The code. */
	private Integer code;

	private ReassignmentRequestStateType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
