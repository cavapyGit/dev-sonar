package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the PARTICIPANT_PENALTY database table.
 * 
 */
@Entity
@Table(name="PENALTY_LEVEL")
public class PenaltyLevel implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PENALTY_LEVEL_GENERATOR", sequenceName="SQ_ID_PENALTY_LEVEL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PENALTY_LEVEL_GENERATOR")
	@Column(name="ID_PENALTY_LEVEL_PK")
	private Long idPenaltyLevelPk;

	@Column(name="PENALTY_NUMBER")
	private Integer penaltyNumber;
	
	@Column(name="CURRENCY")
	private Integer currency;
	
	@Column(name="PENALTY_MOTIVE")
	private Integer penaltyMotive;
	
	@Column(name="PENALTY_AMOUNT")
	private BigDecimal penaltyAmount;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	public PenaltyLevel() {
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	public Long getIdPenaltyLevelPk() {
		return idPenaltyLevelPk;
	}

	public void setIdPenaltyLevelPk(Long idPenaltyLevelPk) {
		this.idPenaltyLevelPk = idPenaltyLevelPk;
	}

	public Integer getPenaltyNumber() {
		return penaltyNumber;
	}

	public void setPenaltyNumber(Integer penaltyNumber) {
		this.penaltyNumber = penaltyNumber;
	}

	public Integer getPenaltyMotive() {
		return penaltyMotive;
	}

	public void setPenaltyMotive(Integer penaltyMotive) {
		this.penaltyMotive = penaltyMotive;
	}

	public BigDecimal getPenaltyAmount() {
		return penaltyAmount;
	}

	public void setPenaltyAmount(BigDecimal penaltyAmount) {
		this.penaltyAmount = penaltyAmount;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		return detailsMap;
	}
}