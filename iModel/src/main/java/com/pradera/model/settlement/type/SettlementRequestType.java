package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum SettlementRequestType {

	SETTLEMENT_CURRENCY_EXCHANGE (Integer.valueOf(2017),"MONEDA DISTINTA"), // MONEDA DISTINTA
	SETTLEMENT_EXTENSION (Integer.valueOf(2018),"SELID"), //SELID
	SETTLEMENT_ANTICIPATION (Integer.valueOf(2019),"SELAR"), //SELAR
	SETTLEMENT_TYPE_EXCHANGE (Integer.valueOf(2020),"SELVE"), //SELVE
	SETTLEMENT_CURRENCY_EXCHANGE_REVERSION (Integer.valueOf(2208),"REVERSION MONEDA DISTINTA"), // REVERSION MONEDA DISTINTA
	SETTLEMENT_SPECIAL_TYPE_EXCHANGE (Integer.valueOf(2257),"CTL"); // CTL
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;	
		
	private SettlementRequestType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public static final List<SettlementRequestType> list = new ArrayList<SettlementRequestType>();
	public static final Map<Integer, SettlementRequestType> lookup = new HashMap<Integer, SettlementRequestType>();
	static {
		for (SettlementRequestType s : EnumSet.allOf(SettlementRequestType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
