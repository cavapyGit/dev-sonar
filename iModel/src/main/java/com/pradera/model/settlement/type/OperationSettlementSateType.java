package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum OperationSettlementSateType {

	WITHDRAWN_BY_SECURITIES (new Integer(1286), "RETIRADA POR VALORES"),
	SETTLEMENT_PENDIENT(new Integer(1533), "LIQUIDACION PENDIENTE"),
	WITHDRAWN_BY_FUNDS (new Integer(1287), "RETIRADA POR FONDOS"),
	WITHDRAWN_BY_GUARANTEES (new Integer(1638), "RETIRADA POR GARANTIAS"),
	SETTLED (new Integer(1288), "LIQUIDADA"),
	SETTLEMENT_ERROR (new Integer(1289), "ERROR EN LIQUIDACION");
	
	
	/** The code. */
	private Integer code;
	
	private String description;

	private OperationSettlementSateType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<OperationSettlementSateType> list = new ArrayList<OperationSettlementSateType>();
	public static final Map<Integer, OperationSettlementSateType> lookup = new HashMap<Integer, OperationSettlementSateType>();
	static {
		for (OperationSettlementSateType    s : EnumSet.allOf(OperationSettlementSateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the operation settlement sate type
	 */
	public static OperationSettlementSateType get(Integer code) {
		return lookup.get(code);
	}
}
