package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.NegotiationMechanism;
/**
 * The persistent class for the SETTLEMENT_PROCESS database table.
 * 
 */
@Entity
@Table(name="SETTLEMENT_PROCESS")
public class SettlementProcess implements Serializable, Auditable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SETTLEMENT_PROCESS_GENERATOR", sequenceName="SQ_ID_SETTLEMENT_PROCESS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SETTLEMENT_PROCESS_GENERATOR")
	@Column(name="ID_SETTLEMENT_PROCESS_PK")
	private Long idSettlementProcessPk;

	@Column(name="CANCELED_OPERATIONS")
	private Long canceledOperations;

	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="EXCHANGE_RATE")
	private BigDecimal exchangeRate;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MODALITY_GROUP_FK")
	private ModalityGroup modalityGroup;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK")
	private NegotiationMechanism negotiationMechanism;

	@Column(name="PROCESS_NUMBER")
	private Long processNumber;

	@Column(name="PROCESS_TYPE")
	private Integer processType;

	@Column(name="TOTAL_OPERATION")
	private Long totalOperation;
	
	@Column(name="PENDING_OPERATIONS")
	private Long pendingOperations;
	
	@Column(name="SETTLED_OPERATIONS")
	private Long settledOperations;
	
	@Column(name="REMOVED_OPERATIONS")
	private Long removedOperations;

    @Temporal( TemporalType.DATE)
	@Column(name="SETTLEMENT_DATE")
	private Date settlementDate;

	@Column(name="SETTLEMENT_SCHEMA")
	private Integer settlementSchema;

	@Column(name="PROCESS_STATE")
	private Integer processState;
	
	@Column(name="SCHEDULE_TYPE")
	private Integer scheduleType;
	
	//bi-directional many-to-one association to OperationSettlement
	@OneToMany(mappedBy="settlementProcess")
	private List<OperationSettlement> operationSettlements;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The participant pos. */
	@OneToMany(mappedBy="settlementProcess",fetch=FetchType.LAZY)
	private List<ParticipantPosition> participantPositions;
	
	@Transient
	private List<SettlementOperation> totalSettlementOperations;
	
	@Transient
	private List<SettlementOperation> pendingSettlementOperations;
	
	@Transient
	private List<SettlementOperation> settledSettlementOperations;
	
	@Transient
	private List<SettlementOperation> removedSettlementOperations;
	
	@Transient
	private String processTypeDescription;
	
	@Transient
	private String scheduleTypeDescription;
	
	@Transient
	private String stateDescription;
	
	@Transient
	private String currencyDescription;
	
	@Transient
	private Long participantCode;
	
    public SettlementProcess() {
    	removedOperations = 0L;
    }

	public Long getIdSettlementProcessPk() {
		return this.idSettlementProcessPk;
	}

	public void setIdSettlementProcessPk(Long idSettlementProcessPk) {
		this.idSettlementProcessPk = idSettlementProcessPk;
	}

	public Long getCanceledOperations() {
		return this.canceledOperations;
	}

	public void setCanceledOperations(Long canceledOperations) {
		this.canceledOperations = canceledOperations;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public ModalityGroup getModalityGroup() {
		return modalityGroup;
	}

	public void setModalityGroup(ModalityGroup modalityGroup) {
		this.modalityGroup = modalityGroup;
	}

	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}

	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Long getPendingOperations() {
		return pendingOperations;
	}

	public void setPendingOperations(Long pendingOperations) {
		this.pendingOperations = pendingOperations;
	}

	public Long getProcessNumber() {
		return processNumber;
	}

	public void setProcessNumber(Long processNumber) {
		this.processNumber = processNumber;
	}

	public Integer getProcessType() {
		return processType;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	public Long getSettledOperations() {
		return settledOperations;
	}

	public void setSettledOperations(Long settledOperations) {
		this.settledOperations = settledOperations;
	}

	public Date getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getSettlementSchema() {
		return settlementSchema;
	}

	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Long getTotalOperation() {
		return totalOperation;
	}

	public void setTotalOperation(Long totalOperation) {
		this.totalOperation = totalOperation;
	}

	public List<OperationSettlement> getOperationSettlements() {
		return operationSettlements;
	}

	public void setOperationSettlements(
			List<OperationSettlement> operationSettlements) {
		this.operationSettlements = operationSettlements;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("operationSettlements", operationSettlements);
        detailsMap.put("participantPositions", participantPositions);
        return detailsMap;
	}

	/**
	 * Instantiates a new settlement process.
	 *
	 * @param idSettlementProcessPk the id settlement process pk
	 * @param totalOperation the total operation
	 */
	public SettlementProcess(Long idSettlementProcessPk, Long totalOperation) {
		this.idSettlementProcessPk = idSettlementProcessPk;
		this.totalOperation = totalOperation;
	}
	
	/**
	 * Instantiates a new settlement process.
	 *
	 * @param idSettlementProcessPk the id settlement process pk
	 */
	public SettlementProcess(Long idSettlementProcessPk) {
		this.idSettlementProcessPk = idSettlementProcessPk;
	}

	/**
	 * Gets the participant positions.
	 *
	 * @return the participant positions
	 */
	public List<ParticipantPosition> getParticipantPositions() {
		return participantPositions;
	}

	/**
	 * Sets the participant positions.
	 *
	 * @param participantPositions the new participant positions
	 */
	public void setParticipantPositions(
			List<ParticipantPosition> participantPositions) {
		this.participantPositions = participantPositions;
	}
	
	public SettlementProcess clone() throws CloneNotSupportedException {
        return (SettlementProcess) super.clone();
    }

	public Long getRemovedOperations() {
		return removedOperations;
	}

	public void setRemovedOperations(Long removedOperations) {
		this.removedOperations = removedOperations;
	}

	public String getStateDescription() {
		return stateDescription;
	}

	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	public List<SettlementOperation> getTotalSettlementOperations() {
		return totalSettlementOperations;
	}

	public void setTotalSettlementOperations(
			List<SettlementOperation> totalSettlementOperations) {
		this.totalSettlementOperations = totalSettlementOperations;
	}

	public List<SettlementOperation> getPendingSettlementOperations() {
		return pendingSettlementOperations;
	}

	public void setPendingSettlementOperations(
			List<SettlementOperation> pendingSettlementOperations) {
		this.pendingSettlementOperations = pendingSettlementOperations;
	}

	public List<SettlementOperation> getSettledSettlementOperations() {
		return settledSettlementOperations;
	}

	public void setSettledSettlementOperations(
			List<SettlementOperation> settledSettlementOperations) {
		this.settledSettlementOperations = settledSettlementOperations;
	}

	public List<SettlementOperation> getRemovedSettlementOperations() {
		return removedSettlementOperations;
	}

	public void setRemovedSettlementOperations(
			List<SettlementOperation> removedSettlementOperations) {
		this.removedSettlementOperations = removedSettlementOperations;
	}

	public SettlementProcess(Long idSettlementProcessPk, Integer processType,
			Integer processState) {
		super();
		this.idSettlementProcessPk = idSettlementProcessPk;
		this.processType = processType;
		this.processState = processState;
	}

	/**
	 * Gets the process type description.
	 *
	 * @return the process type description
	 */
	public String getProcessTypeDescription() {
		return processTypeDescription;
	}

	/**
	 * Sets the process type description.
	 *
	 * @param processTypeDescription the new process type description
	 */
	public void setProcessTypeDescription(String processTypeDescription) {
		this.processTypeDescription = processTypeDescription;
	}

	public Integer getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getScheduleTypeDescription() {
		return scheduleTypeDescription;
	}

	public void setScheduleTypeDescription(String scheduleTypeDescription) {
		this.scheduleTypeDescription = scheduleTypeDescription;
	}

	/**
	 * @return the currencyDescription
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}

	/**
	 * @param currencyDescription the currencyDescription to set
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	public Long getParticipantCode() {
		return participantCode;
	}

	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}
	
}