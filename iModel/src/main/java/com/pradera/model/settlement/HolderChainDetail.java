package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="HOLDER_CHAIN_DETAIL")
public class HolderChainDetail implements Serializable,Auditable, Cloneable{

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOLDER_CHAIN_DETAIL_GENERATOR", sequenceName="SQ_ID_HOLDER_CHAIN_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_CHAIN_DETAIL_GENERATOR")
	@Column(name="ID_HOLDER_CHAIN_DETAIL_PK")
	private Long idHolderChainDetailPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHAINED_HOLDER_OPERATION_FK")
	private ChainedHolderOperation chainedHolderOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCOUNT_MARKETFACT_FK")
	private SettlementAccountMarketfact settlementAccountMarketfact;

//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_SETTLEMENT_ACCOUNT_FK")
//	private SettlementAccountOperation settlementAccountOperation;
	
	@Column(name="CHAINED_QUANTITY")
	private BigDecimal chainedQuantity; 
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	public HolderChainDetail() {
		super();
	}

	public Long getIdHolderChainDetailPk() {
		return idHolderChainDetailPk;
	}

	public void setIdHolderChainDetailPk(Long idHolderChainDetailPk) {
		this.idHolderChainDetailPk = idHolderChainDetailPk;
	}

	public ChainedHolderOperation getChainedHolderOperation() {
		return chainedHolderOperation;
	}

	public void setChainedHolderOperation(
			ChainedHolderOperation chainedHolderOperation) {
		this.chainedHolderOperation = chainedHolderOperation;
	}

//	public SettlementAccountOperation getSettlementAccountOperation() {
//		return settlementAccountOperation;
//	}
//
//	public void setSettlementAccountOperation(
//			SettlementAccountOperation settlementAccountOperation) {
//		this.settlementAccountOperation = settlementAccountOperation;
//	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getChainedQuantity() {
		return chainedQuantity;
	}

	public void setChainedQuantity(BigDecimal chainedQuantity) {
		this.chainedQuantity = chainedQuantity;
	}

	public SettlementAccountMarketfact getSettlementAccountMarketfact() {
		return settlementAccountMarketfact;
	}

	public void setSettlementAccountMarketfact(
			SettlementAccountMarketfact settlementAccountMarketfact) {
		this.settlementAccountMarketfact = settlementAccountMarketfact;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        lastModifyDate = loggerUser.getAuditTime();
        lastModifyIp = loggerUser.getIpAddress();
        lastModifyUser = loggerUser.getUserName();
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public HolderChainDetail clone() throws CloneNotSupportedException {
        return (HolderChainDetail) super.clone();
    }
}
