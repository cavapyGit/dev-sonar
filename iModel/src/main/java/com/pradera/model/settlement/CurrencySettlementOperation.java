package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


@Entity 
@Table(name = "CURRENCY_SETTLEMENT_OPERATION")
public class CurrencySettlementOperation implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "CURRENCY_SETTLEMENT_DETAILPK_GENERATOR", sequenceName = "SQ_ID_CURRENCY_SETTLE_DET_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CURRENCY_SETTLEMENT_DETAILPK_GENERATOR")
	@Column(name = "ID_CURRENCY_SETTLE_OPER_PK")
	private Long idCurrencySettlementOperPk;
	
	//bi-directional many-to-one association to currency request source
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_REQUEST_FK", referencedColumnName="ID_SETTLEMENT_REQUEST_PK")
	private SettlementRequest settlementRequest;
    
  //bi-directional many-to-one association to mechanism operation source
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_OPERATION_FK")
    private SettlementOperation settlementOperation;
    
    /** The operation part. */
    @Column(name="OPERATION_PART")
    private Integer operationPart;
    
    /** The settlement currency. */
    @Column(name="SETTLEMENT_CURRENCY")
    private Integer settlementCurrency;
    
    /** The settlement currency. */
    @Column(name="EXCHANGE_RATE")
    private BigDecimal exchangeRate;
    
    /** The settlement currency. */
    @Column(name="INITIAL_SETTLEMENT_CURRENCY")
    private Integer initialSettlementCurrency;
    
    /** The settlement price. */
    @Column(name="INITIAL_SETTLEMENT_PRICE")
    private BigDecimal initialSettlementPrice;
    
    /** The settlement amount. */
    @Column(name="INITIAL_SETTLEMENT_AMOUNT")
    private BigDecimal initialSettlementAmount;
    
    /** The settlement price. */
    @Column(name="SETTLEMENT_PRICE")
    private BigDecimal settlementPrice;
    
    /** The settlement amount. */
    @Column(name="SETTLEMENT_AMOUNT")
    private BigDecimal settlementAmount;
    
    /** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Transient
	private String descriptionCurrency;
	
	public CurrencySettlementOperation() {
		
	}

	public Long getIdCurrencySettlementOperPk() {
		return idCurrencySettlementOperPk;
	}

	public void setIdCurrencySettlementOperPk(Long idCurrencySettlementOperPk) {
		this.idCurrencySettlementOperPk = idCurrencySettlementOperPk;
	}

	public SettlementRequest getSettlementRequest() {
		return settlementRequest;
	}

	public void setSettlementRequest(
			SettlementRequest settlementRequest) {
		this.settlementRequest = settlementRequest;
	}

	public SettlementOperation getSettlementOperation() {
		return settlementOperation;
	}

	public void setSettlementOperation(SettlementOperation settlementOperation) {
		this.settlementOperation = settlementOperation;
	}

	public Integer getOperationPart() {
		return operationPart;
	}

	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	public Integer getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
	public BigDecimal getSettlementPrice() {
		return settlementPrice;
	}

	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}

	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
		
        return detailsMap;
	}

	public Integer getInitialSettlementCurrency() {
		return initialSettlementCurrency;
	}

	public void setInitialSettlementCurrency(Integer initialSettlementCurrency) {
		this.initialSettlementCurrency = initialSettlementCurrency;
	}

	public BigDecimal getInitialSettlementPrice() {
		return initialSettlementPrice;
	}

	public void setInitialSettlementPrice(BigDecimal initialSettlementPrice) {
		this.initialSettlementPrice = initialSettlementPrice;
	}

	public BigDecimal getInitialSettlementAmount() {
		return initialSettlementAmount;
	}

	public void setInitialSettlementAmount(BigDecimal initialSettlementAmount) {
		this.initialSettlementAmount = initialSettlementAmount;
	}   
}
