package com.pradera.model.settlement;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the PARTICIPANT_PENALTY_OPERATION database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_PENALTY_OPERATION")
@NamedQuery(name="ParticipantPenaltyOperation.findAll", query="SELECT p FROM ParticipantPenaltyOperation p")
public class ParticipantPenaltyOperation implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARTICIPANT_PENALTY_OPERATION_IDPARTICIPANTPENALTYOPEPK_GENERATOR", sequenceName="SQ_ID_PART_PENALTY_OPE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_PENALTY_OPERATION_IDPARTICIPANTPENALTYOPEPK_GENERATOR")
	@Column(name="ID_PARTICIPANT_PENALTY_OPE_PK", unique=true, nullable=false, precision=10)
	private Long idParticipantPenaltyOpePk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="OPERATION_STATE")
	private Integer operationState;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP", length=20)
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER", length=20)
	private String lastModifyUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registerDate;

	@Column(name="REGISTRY_USER", length=20)
	private String registerUser;

	//bi-directional many-to-one association to MechanismOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_OPERATION_FK", nullable=false)
	private SettlementOperation settlementOperation;

	//bi-directional many-to-one association to ParticipantPenalty
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_PENALTY_FK", nullable=false)
	private ParticipantPenalty participantPenalty;

	public ParticipantPenaltyOperation() {
		
	}

	public Long getIdParticipantPenaltyOpePk() {
		return idParticipantPenaltyOpePk;
	}

	public void setIdParticipantPenaltyOpePk(Long idParticipantPenaltyOpePk) {
		this.idParticipantPenaltyOpePk = idParticipantPenaltyOpePk;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public SettlementOperation getSettlementOperation() {
		return settlementOperation;
	}

	public void setSettlementOperation(SettlementOperation settlementOperation) {
		this.settlementOperation = settlementOperation;
	}

	public ParticipantPenalty getParticipantPenalty() {
		return participantPenalty;
	}

	public void setParticipantPenalty(ParticipantPenalty participantPenalty) {
		this.participantPenalty = participantPenalty;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		return detailsMap;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	
}