package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ChainedOperationStateType {

	/**the PARTIAL*/
	REGISTERED (Integer.valueOf(1957), "REGISTRADA"),
	/**the FINAL*/
	REJECTED (Integer.valueOf(1958), "RECHAZADA"),
	
	CONFIRMED (Integer.valueOf(1959), "CONFIRMADA"),
	
	CANCELLED (Integer.valueOf(1960), "CANCELADA");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ChainedOperationStateType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static ChainedOperationStateType get(Integer code) {
		return lookup.get(code);
	}
	
	public static final List<ChainedOperationStateType> list = new ArrayList<ChainedOperationStateType>();
	public static final Map<Integer, ChainedOperationStateType> lookup = new HashMap<Integer, ChainedOperationStateType>();
	static {
		for (ChainedOperationStateType s : EnumSet.allOf(ChainedOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
