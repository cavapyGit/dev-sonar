package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum ReassignmentRequestMotiveType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/10/2015
 */
public enum ReassignmentRequestMotiveType {
	
	/** The modify holder account. */
	MODIFY_HOLDER_ACCOUNT(Integer.valueOf(2336), "MODIFICACION DE CUENTAS TITULAR"),
	
	/** The modify market fact. */
	MODIFY_MARKETFACT(Integer.valueOf(2337), "MODIFICACION HECHOS DE MERCADO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;
	
	/**
	 * Instantiates a new reassignment request motive type.
	 *
	 * @param code the code
	 * @param description the description
	 */
	private ReassignmentRequestMotiveType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/** The Constant list. */
	public static final List<ReassignmentRequestMotiveType> list = new ArrayList<ReassignmentRequestMotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ReassignmentRequestMotiveType> lookup = new HashMap<Integer, ReassignmentRequestMotiveType>();
	static {
		for (ReassignmentRequestMotiveType s : EnumSet.allOf(ReassignmentRequestMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the reassignment request motive type
	 */
	public static ReassignmentRequestMotiveType get(Integer code) {
		return lookup.get(code);
	}
}
