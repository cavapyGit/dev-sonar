package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CurrencySettlementMotiveRejectType {

	ERROR(Integer.valueOf(1870),"ERROR EN EL REGISTRO DE LA SOLICITUD"),
	
	WITHDRAWAL(Integer.valueOf(1871),"DESISTIMIENTO DE LA SOLICITUD"),
	
	OTHER(Integer.valueOf(1843),"OTROS");
	
	/** The Constant lookup. */
	public static final Map<Integer,CurrencySettlementMotiveRejectType> lookup=new HashMap<Integer, CurrencySettlementMotiveRejectType>();
	
	/** The Constant list. */
	public static final List<CurrencySettlementMotiveRejectType> list=new ArrayList<CurrencySettlementMotiveRejectType>();

	static{
		for(CurrencySettlementMotiveRejectType s:CurrencySettlementMotiveRejectType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private CurrencySettlementMotiveRejectType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static CurrencySettlementMotiveRejectType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
	
}
