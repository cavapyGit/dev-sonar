package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SETTLEMENT_DATE_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/08/2013
 */
@Entity
@Table(name = "SETTLEMENT_DATE_OPERATION")
public class SettlementDateOperation implements Serializable, Auditable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id settlement date request pk. */
	@Id
	@SequenceGenerator(name = "SETTLEMENT_DATE_OPER_GENERATOR", sequenceName = "SQ_ID_SETTLEMENT_DATE_OPER_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SETTLEMENT_DATE_OPER_GENERATOR")
	@Column(name = "ID_SETTLEMENT_DATE_OPER_PK")
	private Long idSettlementDateOperationPk;

	@Temporal(TemporalType.DATE)
	@Column(name = "SETTLEMENT_DATE")
	private Date settlementDate;

	@Column(name = "SETTLEMENT_AMOUNT")
	private BigDecimal settlementAmount;

	@Column(name = "SETTLEMENT_PRICE")
	private BigDecimal settlementPrice;

	@Column(name = "SETTLEMENT_AMOUNT_RATE")
	private BigDecimal settlementAmountRate;

	@Column(name = "SETTLEMENT_DAYS")
	private Long settlementDays;

	@Column(name = "SETTLEMENT_CURRENCY")
	private Integer settlementCurrency;

	@Column(name = "STOCK_QUANTITY")
	private BigDecimal stockQuantity;

	@Column(name = "OPERATION_PART")
	private Integer operationPart;

	@Column(name = "SETTLEMENT_TYPE")
	private Integer settlementType;

	@Column(name = "INITIAL_SETTLEMENT_TYPE")
	private Integer initialSettlementType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SETTLEMENT_REQUEST_FK", referencedColumnName = "ID_SETTLEMENT_REQUEST_PK")
	private SettlementRequest settlementRequest;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SETTLEMENT_OPERATION_FK")
	private SettlementOperation settlementOperation;

	@Transient
	private List<SettlementDateAccount> settlementDateAccounts;

	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	@Transient
	private boolean selected;

	@Transient
	private BigDecimal oldSettlementPrice;
	
	@Transient
	private Integer settlementSchema;	
	
	
	public SettlementDateOperation() {
		super();
	}
	
	public SettlementDateOperation(Long idSettlementDateOperationPk) {
		this.idSettlementDateOperationPk = idSettlementDateOperationPk;
	}

	public Long getIdSettlementDateOperationPk() {
		return idSettlementDateOperationPk;
	}

	public void setIdSettlementDateOperationPk(Long idSettlementDateOperationPk) {
		this.idSettlementDateOperationPk = idSettlementDateOperationPk;
	}

	public Date getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public BigDecimal getSettlementPrice() {
		return settlementPrice;
	}

	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

	public BigDecimal getSettlementAmountRate() {
		return settlementAmountRate;
	}

	public void setSettlementAmountRate(BigDecimal settlementAmountRate) {
		this.settlementAmountRate = settlementAmountRate;
	}

	public Long getSettlementDays() {
		return settlementDays;
	}

	public void setSettlementDays(Long settlementDays) {
		this.settlementDays = settlementDays;
	}

	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Integer getOperationPart() {
		return operationPart;
	}

	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	public SettlementRequest getSettlementRequest() {
		return settlementRequest;
	}

	public void setSettlementRequest(SettlementRequest settlementRequest) {
		this.settlementRequest = settlementRequest;
	}

	// public MechanismOperation getMechanismOperation() {
	// return mechanismOperation;
	// }
	//
	// public void setMechanismOperation(MechanismOperation mechanismOperation)
	// {
	// this.mechanismOperation = mechanismOperation;
	// }

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public List<SettlementDateAccount> getSettlementDateAccounts() {
		return settlementDateAccounts;
	}

	public void setSettlementDateAccounts(
			List<SettlementDateAccount> settlementDateAccounts) {
		this.settlementDateAccounts = settlementDateAccounts;
	}

	public Integer getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public Integer getSettlementType() {
		return settlementType;
	}

	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}

	public SettlementOperation getSettlementOperation() {
		return settlementOperation;
	}

	public void setSettlementOperation(SettlementOperation settlementOperation) {
		this.settlementOperation = settlementOperation;
	}

	public Integer getInitialSettlementType() {
		return initialSettlementType;
	}

	public void setInitialSettlementType(Integer initialSettlementType) {
		this.initialSettlementType = initialSettlementType;
	}

	public BigDecimal getOldSettlementPrice() {
		return oldSettlementPrice;
	}

	public void setOldSettlementPrice(BigDecimal oldSettlementPrice) {
		this.oldSettlementPrice = oldSettlementPrice;
	}

	/**
	 * @return the settlementSchema
	 */
	public Integer getSettlementSchema() {
		return settlementSchema;
	}

	/**
	 * @param settlementSchema the settlementSchema to set
	 */
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

}