package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.negotiation.type.ParticipantRoleType;

public enum OperationPartType {

	/**the CASH_PART - parte contado*/
	CASH_PART  (Integer.valueOf(1), "PARTE CONTADO"),
	/**the TERM_PART - parte a plazo*/
	TERM_PART (Integer.valueOf(2), "PARTE A PLAZO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private OperationPartType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<OperationPartType> list = new ArrayList<OperationPartType>();
	public static final Map<Integer, OperationPartType> lookup = new HashMap<Integer, OperationPartType>();
	static {
		for (OperationPartType s : EnumSet.allOf(OperationPartType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static OperationPartType get(Integer codigo) {
		return lookup.get(codigo);
	}
	
}
