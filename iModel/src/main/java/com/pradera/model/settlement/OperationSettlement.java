package com.pradera.model.settlement;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.MechanismOperation;


/**
 * The persistent class for the OPERATION_SETTLEMENT database table.
 * 
 */
@Entity
@Table(name="OPERATION_SETTLEMENT")
public class OperationSettlement implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="OPERATION_SETTLEMENT_GENERATOR", sequenceName="SQ_ID_OPER_SETTLEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OPERATION_SETTLEMENT_GENERATOR")
	@Column(name="ID_OPERATION_SETTLEMENT_PK")
	private Long idOperationSettlementPk;

	//bi-directional many-to-one association to mechanismOperation
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
  	private MechanismOperation mechanismOperation;
	
	//bi-directional many-to-one association to mechanismOperation
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_SETTLEMENT_OPERATION_FK")
  	private SettlementOperation settlementOperation;

	@Column(name="OPERATION_STATE")
	private Integer operationState;
	
	//bi-directional many-to-one association to SettlementProcess
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLMENT_PROCESS_FK")
	private SettlementProcess settlementProcess;

    @Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
    
	@Transient
	private Integer initialState;
    
    public OperationSettlement() {
    }

	public Long getIdOperationSettlementPk() {
		return this.idOperationSettlementPk;
	}

	public void setIdOperationSettlementPk(Long idOperationSettlementPk) {
		this.idOperationSettlementPk = idOperationSettlementPk;
	}

	
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public SettlementProcess getSettlementProcess() {
		return this.settlementProcess;
	}

	public void setSettlementProcess(SettlementProcess settlementProcess) {
		this.settlementProcess = settlementProcess;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        lastModifyDate = loggerUser.getAuditTime();
        lastModifyIp = loggerUser.getIpAddress();
        lastModifyUser = loggerUser.getUserName();
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	public SettlementOperation getSettlementOperation() {
		return settlementOperation;
	}

	public void setSettlementOperation(SettlementOperation settlementOperation) {
		this.settlementOperation = settlementOperation;
	}

	public Integer getInitialState() {
		return initialState;
	}

	public void setInitialState(Integer initialState) {
		this.initialState = initialState;
	}

}