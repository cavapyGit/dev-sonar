package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SirtexTermSettlementType {
	
	MIN_DAYS (Integer.valueOf(2190), "NRO DIAS MINIMO"),
	MAX_DAYS (Integer.valueOf(2191), "NRO DIAS MAXIMO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;
	private SirtexTermSettlementType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static final List<SirtexTermSettlementType> list = new ArrayList<SirtexTermSettlementType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SirtexTermSettlementType> lookup = new HashMap<Integer, SirtexTermSettlementType>();
	static {
		for (SirtexTermSettlementType s : EnumSet.allOf(SirtexTermSettlementType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static SirtexTermSettlementType get(Integer codigo) {
		return lookup.get(codigo);
	}

}
