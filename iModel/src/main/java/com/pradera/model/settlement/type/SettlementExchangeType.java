package com.pradera.model.settlement.type;


public enum SettlementExchangeType {

	DVP_TO_FOP (Integer.valueOf(2037)), 
	FOP_TO_DVP (Integer.valueOf(2038));
	
	/** The code. */
	private Integer code;

	private SettlementExchangeType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
