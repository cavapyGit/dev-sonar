package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SettlementProcessStateType {

	/**the IN_PROCESS*/
	IN_PROCESS  (Integer.valueOf(1281), "EN PROCESO"),
	/**the STOPPED*/
	STOPPED (Integer.valueOf(1282), "DETENIDO"),
	/**the STARTED*/
	STARTED (Integer.valueOf(1283), "INICIADO"),
	/**the FINISHED*/
	FINISHED (Integer.valueOf(1284), "TERMINADO"),
	/**the WAITING*/
	WAITING (Integer.valueOf(1285), "EN ESPERA"),
	
	ERROR (Integer.valueOf(2253), "ERROR");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private SettlementProcessStateType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<SettlementProcessStateType> list = new ArrayList<SettlementProcessStateType>();
	public static final Map<Integer, SettlementProcessStateType> lookup = new HashMap<Integer, SettlementProcessStateType>();
	static {
		for (SettlementProcessStateType s : EnumSet.allOf(SettlementProcessStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the settlement process state type
	 */
	public static SettlementProcessStateType get(Integer code) {
		return lookup.get(code);
	}	
}
