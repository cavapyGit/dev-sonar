package com.pradera.model.settlement.type;


public enum SettlementDateOperationStateType {

	REGISTERED (Integer.valueOf(2036)),
	SETTLEMENT_PENDING (Integer.valueOf(1230)),
	SETTLED (Integer.valueOf(1231)),
	CANCELLED (Integer.valueOf(1476)),
	REFERENTIAL (Integer.valueOf(1));
	
	/** The code. */
	private Integer code;

	private SettlementDateOperationStateType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
