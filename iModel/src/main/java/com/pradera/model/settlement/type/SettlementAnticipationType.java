package com.pradera.model.settlement.type;


public enum SettlementAnticipationType {

	SETTLEMENT_PARTIAL (Integer.valueOf(1230)), 
	SETTLEMENT_TOTAL (Integer.valueOf(1231));
	
	/** The code. */
	private Integer code;

	private SettlementAnticipationType(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
