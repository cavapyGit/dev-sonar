package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.negotiation.MechanismOperation;

/**
 * The persistent class for the PARTICIPANT_POSITION database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_POSITION")
public class ParticipantPosition implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARTICIPANT_POSITION_GENERATOR", sequenceName="SQ_ID_PARTICIPANT_POSITION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_POSITION_GENERATOR")
	@Column(name="ID_PARTICIPANT_POSITION_PK")
	private Long idParticipantPositionPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_SETTLEMENT_PROCESS_FK")
	private SettlementProcess settlementProcess;
	
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="SELLER_POSITION")
	private BigDecimal sellerPosition;
	
	@Column(name="BUYER_POSITION")
	private BigDecimal buyerPosition;
	
	@Column(name="NET_POSITION")
	private BigDecimal netPosition;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="IND_PROCESS")
	private Integer indProcess = ComponentConstant.ZERO;
	
	@Column(name="IND_SENT_FUNDS")
	private Integer indSentFunds = ComponentConstant.ZERO;
	
	@Column(name="IND_AUTOMATIC_SENT")
	private Integer indAutomaticSent = ComponentConstant.ZERO;
	
	/** The mechanism operations. */
	@Transient
	private List<MechanismOperation> mechanismOperations;

	public ParticipantPosition() {
		super();
	}

	public ParticipantPosition(Long idParticipantPositionPk) {
		super();
		this.idParticipantPositionPk = idParticipantPositionPk;
	}

	public Long getIdParticipantPositionPk() {
		return idParticipantPositionPk;
	}

	public void setIdParticipantPositionPk(Long idParticipantPositionPk) {
		this.idParticipantPositionPk = idParticipantPositionPk;
	}

	public SettlementProcess getSettlementProcess() {
		return settlementProcess;
	}

	public void setSettlementProcess(SettlementProcess settlementProcess) {
		this.settlementProcess = settlementProcess;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public BigDecimal getSellerPosition() {
		return sellerPosition;
	}

	public void setSellerPosition(BigDecimal sellerPosition) {
		this.sellerPosition = sellerPosition;
	}

	public BigDecimal getBuyerPosition() {
		return buyerPosition;
	}

	public void setBuyerPosition(BigDecimal buyerPosition) {
		this.buyerPosition = buyerPosition;
	}

	public BigDecimal getNetPosition() {
		return netPosition;
	}

	public void setNetPosition(BigDecimal netPosition) {
		this.netPosition = netPosition;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		return detailsMap;
	}
	/**
	 * Instantiates a new participant position.
	 *
	 * @param idParticipant the id participant
	 * @param description the description
	 * @param mnemonic the mnemonic
	 */
	public ParticipantPosition(Long idParticipant, String description, String mnemonic) {
		this.participant = new Participant(idParticipant,description,mnemonic);
	}
	/**
	 * Gets the ind process.
	 *
	 * @return the ind process
	 */
	public Integer getIndProcess() {
		return indProcess;
	}
	/**
	 * Sets the ind process.
	 *
	 * @param indProcess the new ind process
	 */
	public void setIndProcess(Integer indProcess) {
		this.indProcess = indProcess;
	}

	/**
	 * Gets the mechanism operations.
	 *
	 * @return the mechanism operations
	 */
	public List<MechanismOperation> getMechanismOperations() {
		return mechanismOperations;
	}

	/**
	 * Sets the mechanism operations.
	 *
	 * @param mechanismOperations the new mechanism operations
	 */
	public void setMechanismOperations(List<MechanismOperation> mechanismOperations) {
		this.mechanismOperations = mechanismOperations;
	}

	public Integer getIndSentFunds() {
		return indSentFunds;
	}

	public void setIndSentFunds(Integer indSentFunds) {
		this.indSentFunds = indSentFunds;
	}

	public Integer getIndAutomaticSent() {
		return indAutomaticSent;
	}

	public void setIndAutomaticSent(Integer indAutomaticSent) {
		this.indAutomaticSent = indAutomaticSent;
	}
	
	
	
}