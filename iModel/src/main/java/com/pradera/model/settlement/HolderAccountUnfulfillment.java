package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.guarantees.AccountOperationValorization;


/**
 * The persistent class for the HOLDER_ACCOUNT_UNFULFILLMENT database table.
 * 
 */
@Entity
@Table(name="HOLDER_ACCOUNT_UNFULFILLMENT")
public class HolderAccountUnfulfillment implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCOUNT_UNFULFILLMENT_GENERATOR", sequenceName="SQ_ID_ACCOUNT_UNFULFILLMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNT_UNFULFILLMENT_GENERATOR")
	@Column(name="ID_ACCOUNT_UNFULFILLMENT_PK")
	private Long idAccountUnfulfillmentPk;

	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNT_VALORIZATION_FK")
	private AccountOperationValorization accountOperationValorization;

//	@ManyToOne(fetch=FetchType.LAZY)
//  	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK")
//	private HolderAccountOperation holderAccountOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_ACCOUNT_FK")
	private SettlementAccountOperation settlementAccountOperation;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="UNFULFILLED_AMOUNT")
	private BigDecimal unfulfilledAmount;

	@Column(name="UNFULFILLED_QUANTITY")
	private BigDecimal unfulfilledQuantity;

	//bi-directional many-to-one association to OperationUnfulfillment
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OPERATION_UNFULFILLMENT_FK")
	private OperationUnfulfillment operationUnfulfillment;

    public HolderAccountUnfulfillment() {
    }

	public Long getIdAccountUnfulfillmentPk() {
		return this.idAccountUnfulfillmentPk;
	}

	public void setIdAccountUnfulfillmentPk(Long idAccountUnfulfillmentPk) {
		this.idAccountUnfulfillmentPk = idAccountUnfulfillmentPk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getUnfulfilledAmount() {
		return this.unfulfilledAmount;
	}

	public void setUnfulfilledAmount(BigDecimal unfulfilledAmount) {
		this.unfulfilledAmount = unfulfilledAmount;
	}

	public BigDecimal getUnfulfilledQuantity() {
		return this.unfulfilledQuantity;
	}

	public void setUnfulfilledQuantity(BigDecimal unfulfilledQuantity) {
		this.unfulfilledQuantity = unfulfilledQuantity;
	}

	public OperationUnfulfillment getOperationUnfulfillment() {
		return this.operationUnfulfillment;
	}

	public void setOperationUnfulfillment(OperationUnfulfillment operationUnfulfillment) {
		this.operationUnfulfillment = operationUnfulfillment;
	}

	public AccountOperationValorization getAccountOperationValorization() {
		return accountOperationValorization;
	}

	public void setAccountOperationValorization(
			AccountOperationValorization accountOperationValorization) {
		this.accountOperationValorization = accountOperationValorization;
	}

	public SettlementAccountOperation getSettlementAccountOperation() {
		return settlementAccountOperation;
	}

	public void setSettlementAccountOperation(
			SettlementAccountOperation settlementAccountOperation) {
		this.settlementAccountOperation = settlementAccountOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		return detailsMap;
	}
	
}