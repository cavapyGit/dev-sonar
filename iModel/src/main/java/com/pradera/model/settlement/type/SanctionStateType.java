package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SanctionStateType {
	
	REGISTERED (Integer.valueOf(1820), "REGISTRADO"),
	DELETED (Integer.valueOf(1821), "ANULADO"),
	DELETED_BY_APELATION (Integer.valueOf(1822), "REVOCADO"),
	CONFIRMED (Integer.valueOf(1823), "CONFIRMADO"),
	MODIFIED (Integer.valueOf(1861), "MODIFICADO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;
	private SanctionStateType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static final List<SanctionStateType> list = new ArrayList<SanctionStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SanctionStateType> lookup = new HashMap<Integer, SanctionStateType>();
	static {
		for (SanctionStateType s : EnumSet.allOf(SanctionStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static SanctionStateType get(Integer codigo) {
		return lookup.get(codigo);
	}

}
