package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SETTLEMENT_DATE_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/08/2013
 */
@Entity
@Table(name="SETTLEMENT_DATE_ACCOUNT")
public class SettlementDateAccount implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id settlement date request pk. */
	@Id
	@SequenceGenerator(name="SETTLEMENT_DATE_ACCOUNT_GENERATOR", sequenceName="SQ_ID_SETTLEMENT_DATE_ACC_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SETTLEMENT_DATE_ACCOUNT_GENERATOR")
	@Column(name="ID_SETTLEMENT_DATE_ACCOUNT_PK")
	private Long idSettlementDateAccountPk;

	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	@Column(name="SETTLEMENT_AMOUNT")
	private BigDecimal settlementAmount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_DATE_OPER_FK")
	private SettlementDateOperation settlementDateOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_ACCOUNT_FK")
    private SettlementAccountOperation settlementAccountOperation;
	
	@OneToMany(fetch=FetchType.LAZY ,mappedBy = "settlementDateAccount",cascade=CascadeType.PERSIST)
	private List<SettlementDateMarketfact> settlementDateMarketfacts;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private boolean selected;
	
	@Transient
	private boolean isTotalPrepaid;
	
	
	
	public boolean getSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Long getIdSettlementDateAccountPk() {
		return idSettlementDateAccountPk;
	}

	public void setIdSettlementDateAccountPk(Long idSettlementDateAccountPk) {
		this.idSettlementDateAccountPk = idSettlementDateAccountPk;
	}

	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public SettlementDateOperation getSettlementDateOperation() {
		return settlementDateOperation;
	}

	public void setSettlementDateOperation(
			SettlementDateOperation settlementDateOperation) {
		this.settlementDateOperation = settlementDateOperation;
	}

//	public HolderAccountOperation getHolderAccountOperation() {
//		return holderAccountOperation;
//	}
//
//	public void setHolderAccountOperation(
//			HolderAccountOperation holderAccountOperation) {
//		this.holderAccountOperation = holderAccountOperation;
//	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap =
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("settlementDateMarketfacts", settlementDateMarketfacts);
		return detailsMap;
	}

	public SettlementAccountOperation getSettlementAccountOperation() {
		return settlementAccountOperation;
	}

	public void setSettlementAccountOperation(
			SettlementAccountOperation settlementAccountOperation) {
		this.settlementAccountOperation = settlementAccountOperation;
	}

	public List<SettlementDateMarketfact> getSettlementDateMarketfacts() {
		return settlementDateMarketfacts;
	}

	public void setSettlementDateMarketfacts(
			List<SettlementDateMarketfact> settlementDateMarketfacts) {
		this.settlementDateMarketfacts = settlementDateMarketfacts;
	}

	public boolean isTotalPrepaid() {
		return isTotalPrepaid;
	}

	public void setTotalPrepaid(boolean isTotalPrepaid) {
		this.isTotalPrepaid = isTotalPrepaid;
	}

}