package com.pradera.model.settlement;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
/**
 * The persistent class for the CURRENCY_SETTLEMENT_REQUEST database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name = SettlementRequest.CURRENCY_SETTLEMENT_REQUEST, query = "Select distinct SettlementRequest From SettlementRequest settlementRequest WHERE settlementRequest.requestState = :state"),
	@NamedQuery(name = SettlementRequest.CURRENCY_SETTLEMENT_REQUEST_STATE, query = "SELECT new com.pradera.model.settlement.SettlementRequest(I.idSettlementRequestPk,I.requestState) From SettlementRequest I WHERE I.idSettlementRequestPk = :idSettlementRequestPkParam")
})
@Entity 
@Table(name = "SETTLEMENT_REQUEST")
public class SettlementRequest implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	/** The Constant CURRENCY_SETTLEMENT_REQUEST. */
	public static final String  CURRENCY_SETTLEMENT_REQUEST= "CurrencySettlementRequest.searchAllCurrencySettlementRequest";
	
	/** The Constant CURRENCY_SETTLEMENT_REQUEST_STATE. */
	public static final String  CURRENCY_SETTLEMENT_REQUEST_STATE= "CurrencySettlementRequest.searchCurrencySettlementRequestState";
	
	@Id
	@SequenceGenerator(name = "CURRENCY_SETTLEMENT_REQUESTPK_GENERATOR", sequenceName = "SQ_ID_SETTLEMENT_REQUEST_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CURRENCY_SETTLEMENT_REQUESTPK_GENERATOR")
	@Column(name = "ID_SETTLEMENT_REQUEST_PK")
	private Long idSettlementRequestPk;		
	
	//bi-directional many-to-one association to Participant source
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOURCE_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant sourceParticipant;
    
  //bi-directional many-to-one association to Participant target
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TARGET_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant targetParticipant;
        
    /** The participant role. */
	@Column(name = "PARTICIPANT_ROLE")
	private Integer participantRole;
	
	/** The request State. */
    @Column(name="REQUEST_STATE")
    private Integer requestState;
    
    /** The request State. */
    @Column(name="REQUEST_TYPE")
    private Integer requestType;
    
    @Column(name = "ANTICIPATION_TYPE")
	private Integer anticipationType;
    
    /** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTER_DATE")
	private Date registerDate;

	/** The registry user. */
	@Column(name = "REGISTER_USER")
	private String registerUser;
	
	/** The approval date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "APPROVAL_DATE")
	private Date approvalDate;

	/** The approval user. */
	@Column(name = "APPROVAL_USER")
	private String approvalUser;
	
	/** The annul date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ANNUL_DATE")
	private Date annulDate;

	/** The annul user. */
	@Column(name = "ANNUL_USER")
	private String annulUser;
	
	/** The review date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REVIEW_DATE")
	private Date reviewDate;

	/** The review user. */
	@Column(name = "REVIEW_USER")
	private String reviewUser;
	
	/** The confirm date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;

	/** The confirm user. */
	@Column(name = "CONFIRM_USER")
	private String confirmUser;
	
	/** The authorize date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AUTHORIZE_DATE")
	private Date authorizeDate;

	/** The authorize user. */
	@Column(name = "AUTHORIZE_USER")
	private String authorizeUser;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REJECT_DATE")
	private Date rejectDate;

	/** The reject user. */
	@Column(name = "REJECT_USER")
	private String rejectUser;
	
	/** The annulment motive. */
	@Column(name="ANNUL_MOTIVE")
	private Integer annulMotive;
	
	/** The reject motive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/** The annulment other motive. */
	@Column(name = "ANNUL_OTHER_MOTIVE")
	private String annulOtherMotive;

	/** The reject other motive. */
	@Column(name = "REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;
	
	/** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private String requestStateDescription;
	
	@Transient
	private String requestTypeDescription;
	
	// bi-directional many-to-one association to Security
	/** The currency settlement detail. */
	@OneToMany(fetch=FetchType.LAZY ,mappedBy = "settlementRequest")
	private List<CurrencySettlementOperation> lstCurrencySettlementDetail;
	
	// bi-directional many-to-one association to Security
	/** The currency settlement detail. */
	@OneToMany(fetch=FetchType.LAZY ,mappedBy = "settlementRequest")
	private List<SettlementDateOperation> lstSettlementDateOperations;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_SETTLEMENT_REQ_FK", referencedColumnName="ID_TRADE_SETTLEMENT_REQ_PK")
	private TradeSettlementRequest tradeSettlementRequest;
	
	public SettlementRequest() {
		
	}	
	
	public SettlementRequest(Long idSettlementRequestPk,
			Integer requestState) {
		this.idSettlementRequestPk = idSettlementRequestPk;
		this.requestState = requestState;
	}

	public Long getIdSettlementRequestPk() {
		return idSettlementRequestPk;
	}

	public void setIdSettlementRequestPk(Long idSettlementRequestPk) {
		this.idSettlementRequestPk = idSettlementRequestPk;
	}
	
	public Participant getSourceParticipant() {
		return sourceParticipant;
	}

	public void setSourceParticipant(Participant sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}

	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	public void setTargetParticipant(Participant participantTarget) {
		this.targetParticipant = participantTarget;
	}

	public Integer getRequestState() {
		return requestState;
	}

	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalUser() {
		return approvalUser;
	}

	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	public Date getAnnulDate() {
		return annulDate;
	}

	public void setAnnulDate(Date annulDate) {
		this.annulDate = annulDate;
	}

	public String getAnnulUser() {
		return annulUser;
	}

	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getReviewUser() {
		return reviewUser;
	}

	public void setReviewUser(String reviewUser) {
		this.reviewUser = reviewUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	public Integer getAnnulMotive() {
		return annulMotive;
	}

	public void setAnnulMotive(Integer annulMotive) {
		this.annulMotive = annulMotive;
	}

	public Integer getRejectMotive() {
		return rejectMotive;
	}

	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	public String getAnnulOtherMotive() {
		return annulOtherMotive;
	}

	public void setAnnulOtherMotive(String annulOtherMotive) {
		this.annulOtherMotive = annulOtherMotive;
	}

	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public Integer getRequestType() {
		return requestType;
	}

	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	public Date getAuthorizeDate() {
		return authorizeDate;
	}

	public void setAuthorizeDate(Date authorizeDate) {
		this.authorizeDate = authorizeDate;
	}

	public String getRequestStateDescription() {
		return requestStateDescription;
	}

	public void setRequestStateDescription(String requestStateDescription) {
		this.requestStateDescription = requestStateDescription;
	}

	public String getRequestTypeDescription() {
		return requestTypeDescription;
	}

	public void setRequestTypeDescription(String requestTypeDescription) {
		this.requestTypeDescription = requestTypeDescription;
	}

	public String getAuthorizeUser() {
		return authorizeUser;
	}

	public void setAuthorizeUser(String authorizeUser) {
		this.authorizeUser = authorizeUser;
	}

	public Integer getParticipantRole() {
		return participantRole;
	}

	public void setParticipantRole(Integer participantRole) {
		this.participantRole = participantRole;
	}

	public List<CurrencySettlementOperation> getLstCurrencySettlementDetail() {
		return lstCurrencySettlementDetail;
	}

	public void setLstCurrencySettlementDetail(
			List<CurrencySettlementOperation> lstCurrencySettlementDetail) {
		this.lstCurrencySettlementDetail = lstCurrencySettlementDetail;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
		 detailsMap.put("currencySettlementDetail", lstCurrencySettlementDetail);
        return detailsMap;
	}

	public List<SettlementDateOperation> getLstSettlementDateOperations() {
		return lstSettlementDateOperations;
	}

	public void setLstSettlementDateOperations(
			List<SettlementDateOperation> lstSettlementDateOperations) {
		this.lstSettlementDateOperations = lstSettlementDateOperations;
	}

	public Integer getAnticipationType() {
		return anticipationType;
	}

	public void setAnticipationType(Integer anticipationType) {
		this.anticipationType = anticipationType;
	}

	public TradeSettlementRequest getTradeSettlementRequest() {
		return tradeSettlementRequest;
	}

	public void setTradeSettlementRequest(
			TradeSettlementRequest tradeSettlementRequest) {
		this.tradeSettlementRequest = tradeSettlementRequest;
	}
}
