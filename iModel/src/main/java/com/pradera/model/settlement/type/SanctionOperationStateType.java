package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SanctionOperationStateType {
	
	REGISTERED (Integer.valueOf(2144), "REGISTRADO"),
	DELETED (Integer.valueOf(2145), "ELIMINADO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;
	private SanctionOperationStateType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static final List<SanctionOperationStateType> list = new ArrayList<SanctionOperationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SanctionOperationStateType> lookup = new HashMap<Integer, SanctionOperationStateType>();
	static {
		for (SanctionOperationStateType s : EnumSet.allOf(SanctionOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static SanctionOperationStateType get(Integer codigo) {
		return lookup.get(codigo);
	}

}
