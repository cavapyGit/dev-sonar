package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;


/**
 * The persistent class for the OPERATION_UNFULFILLMENT database table.
 * 
 */
@Entity
@Table(name="OPERATION_UNFULFILLMENT")
public class OperationUnfulfillment implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="OPERATION_UNFULFILLMENT_GENERATOR", sequenceName="SQ_ID_OPER_UNFULFILLMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OPERATION_UNFULFILLMENT_GENERATOR")
	@Column(name="ID_OPERATION_UNFULFILLMENT_PK")
	private Long idOperationUnfulfillmentPk;

	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_AFFECTED_PARTICIPANT_FK")
	private Participant affectedParticipant;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_OPERATION_FK")
	private SettlementOperation settlementOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_UNFULFILLED_PARTICIPANT_FK")
	private Participant unfulfilledParticipant;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OPERATION_PART")
	private Integer operationPart;

    @Temporal( TemporalType.DATE)
	@Column(name="SETTLEMENT_DATE")
	private Date settlementDate;

	@Column(name="UNFULFILLED_AMOUNT")
	private BigDecimal unfulfilledAmount;

	@Column(name="UNFULFILLED_QUANTITY")
	private BigDecimal unfulfilledQuantity;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UNFULFILLMENT_DATE")
	private Date unfulfillmentDate;

	@Column(name="UNFULFILLMENT_REASON")
	private Long unfulfillmentReason;

	@Column(name="UNFULFILLMENT_TYPE")
	private Long unfulfillmentType;

	//bi-directional many-to-one association to HolderAccountUnfulfillment
	@OneToMany(mappedBy="operationUnfulfillment")
	private List<HolderAccountUnfulfillment> holderAccountUnfulfillments;

    public OperationUnfulfillment() {
    }

	public Long getIdOperationUnfulfillmentPk() {
		return this.idOperationUnfulfillmentPk;
	}

	public void setIdOperationUnfulfillmentPk(Long idOperationUnfulfillmentPk) {
		this.idOperationUnfulfillmentPk = idOperationUnfulfillmentPk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getSettlementDate() {
		return this.settlementDate;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public BigDecimal getUnfulfilledAmount() {
		return this.unfulfilledAmount;
	}

	public void setUnfulfilledAmount(BigDecimal unfulfilledAmount) {
		this.unfulfilledAmount = unfulfilledAmount;
	}

	public BigDecimal getUnfulfilledQuantity() {
		return this.unfulfilledQuantity;
	}

	public void setUnfulfilledQuantity(BigDecimal unfulfilledQuantity) {
		this.unfulfilledQuantity = unfulfilledQuantity;
	}

	public Date getUnfulfillmentDate() {
		return this.unfulfillmentDate;
	}

	public void setUnfulfillmentDate(Date unfulfillmentDate) {
		this.unfulfillmentDate = unfulfillmentDate;
	}

	public List<HolderAccountUnfulfillment> getHolderAccountUnfulfillments() {
		return this.holderAccountUnfulfillments;
	}

	public void setHolderAccountUnfulfillments(List<HolderAccountUnfulfillment> holderAccountUnfulfillments) {
		this.holderAccountUnfulfillments = holderAccountUnfulfillments;
	}

	public Participant getAffectedParticipant() {
		return affectedParticipant;
	}

	public void setAffectedParticipant(Participant affectedParticipant) {
		this.affectedParticipant = affectedParticipant;
	}

	public SettlementOperation getSettlementOperation() {
		return settlementOperation;
	}

	public void setSettlementOperation(SettlementOperation settlementOperation) {
		this.settlementOperation = settlementOperation;
	}

	public Participant getUnfulfilledParticipant() {
		return unfulfilledParticipant;
	}

	public void setUnfulfilledParticipant(Participant unfulfilledParticipant) {
		this.unfulfilledParticipant = unfulfilledParticipant;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getOperationPart() {
		return operationPart;
	}

	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	public Long getUnfulfillmentReason() {
		return unfulfillmentReason;
	}

	public void setUnfulfillmentReason(Long unfulfillmentReason) {
		this.unfulfillmentReason = unfulfillmentReason;
	}

	public Long getUnfulfillmentType() {
		return unfulfillmentType;
	}

	public void setUnfulfillmentType(Long unfulfillmentType) {
		this.unfulfillmentType = unfulfillmentType;
	}
	
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
//		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("holderAccountUnfulfillments", holderAccountUnfulfillments);
		return detailsMap;
	}
	
}