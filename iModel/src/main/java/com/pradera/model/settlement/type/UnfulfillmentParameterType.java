package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum UnfulfillmentParameterType {

	PURCHASE_UNFULFILLMENT_TYPE  (new Long(1232), "INCUMPLIMIENTO EN COMPRA"),
	SALE_UNFULFILLMENT_TYPE (new Long(1233), "INCUMPLIMIENTO EN VENTA"),
	/*******/
	ASSIGNMENT_UNFULFILLMENT_MOTIVE (new Long(1234), "ASIGNACION"),
	FUNDS_UNFULFILLMENT_MOTIVE (new Long(1235), "FONDOS"),
	STOCKS_UNFULFILLMENT_MOTIVE (new Long(1236), "VALORES"),
	GUARANTEES_UNFULFILLMENT_MOTIVE (new Long(1237), "GARANTIAS");
	
	
	/** The code. */
	private Long code;
	
	private String description;

	private UnfulfillmentParameterType(Long code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<UnfulfillmentParameterType> list = new ArrayList<UnfulfillmentParameterType>();
	public static final Map<Long, UnfulfillmentParameterType> lookup = new HashMap<Long, UnfulfillmentParameterType>();
	static {
		for (UnfulfillmentParameterType s : EnumSet.allOf(UnfulfillmentParameterType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static UnfulfillmentParameterType get(Long code){
		return lookup.get(code);
	}
	
}
