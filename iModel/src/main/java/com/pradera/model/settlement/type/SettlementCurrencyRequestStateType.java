package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SettlementCurrencyRequestStateType {

	REGISTERED (Integer.valueOf(1835),"REGISTRADO"),
	APPROVED  (Integer.valueOf(1836),"APROBADO"),
	CANCELLED (Integer.valueOf(1837),"ANULADO"),
	REVISED (Integer.valueOf(1838),"REVISADO"),
	CONFIRMED (Integer.valueOf(1840),"CONFIRMADO"),	
	REJECTED (Integer.valueOf(1839),"RECHAZADO"),
	AUTHORIZED (Integer.valueOf(1841),"AUTORIZADO");
	
	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private SettlementCurrencyRequestStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<SettlementCurrencyRequestStateType> list = new ArrayList<SettlementCurrencyRequestStateType>();
	public static final Map<Integer, SettlementCurrencyRequestStateType> lookup = new HashMap<Integer, SettlementCurrencyRequestStateType>();
	static {
        for (SettlementCurrencyRequestStateType d : SettlementCurrencyRequestStateType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
}
