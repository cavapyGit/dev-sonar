package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RemoveSettlementMotiveType {

	FUNDS (Long.valueOf(1534), "RETIRO POR FONDOS"),
	STOCKS (Long.valueOf(1535), "RETIRO POR VALORES"),
	CEVALDOM (Long.valueOf(1536), "RETIRO POR CEVALDOM"),
	PARTICIPANT (Long.valueOf(1704), "RETIRO POR PARTICIPANTE"),
	BLOCKED_ISSUER (Long.valueOf(1537), "EMISOR BLOQUEADO"),
	BLOCKED_ISSUANCE (Long.valueOf(1538), "EMISION BLOQUEADA"),
	BLOCKED_SECURITIES (Long.valueOf(1539), "VALOR BLOQUEADO"),
	BLOCKED_PARTICIPANT (Long.valueOf(1540), "PARTICIPANTE BLOQUEADO"),
	BLOCKED_HOLDER (Long.valueOf(1541), "TITULAR BLOQUEADO"),
	BLOCKED_HOLDER_ACCOUNT (Long.valueOf(1542), "CUENTA TITULAR BLOQUEADA");
	
	/** The code. */
	private Long code;
	/** The description. */
	private String description;

	private RemoveSettlementMotiveType(Long code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<RemoveSettlementMotiveType> list = new ArrayList<RemoveSettlementMotiveType>();
	public static final Map<Long, RemoveSettlementMotiveType> lookup = new HashMap<Long, RemoveSettlementMotiveType>();
	static {
		for (RemoveSettlementMotiveType s : EnumSet.allOf(RemoveSettlementMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public static RemoveSettlementMotiveType get(Long codigo) {
		return lookup.get(codigo);
	}	
}
