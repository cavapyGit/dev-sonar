package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum SettlementDateRequestStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/09/2013
 */
public enum SettlementDateRequestStateType {

	/** the REGISTERED. - REGISTRADA */
	REGISTERED (Integer.valueOf(1227), "REGISTRADA"),
	/** the REJECTED. - RECHAZADA */
	REJECTED (Integer.valueOf(1228), "RECHAZADA"),
	/** the CONFIRMED. - CONFIRMADA */
	CONFIRMED (Integer.valueOf(1229), "CONFIRMADA"),
	/** The approved. - APROBADA */
	APPROVED(Integer.valueOf(1474), "APROBADA"),
	/** The annulate. - ANULADA */
	ANNULATE(Integer.valueOf(1475), "ANULADA"),
	/** The authorize. - AUTORIZADA */
	AUTHORIZE(Integer.valueOf(1543), "AUTORIZADO"),
	/** The review. - REVISADA */
	REVIEW(Integer.valueOf(1482), "REVISADA");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	/**
	 * Instantiates a new settlement date request state type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private SettlementDateRequestStateType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param descripcion the new description
	 */
	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	/** The Constant list. */
	public static final List<SettlementDateRequestStateType> list = new ArrayList<SettlementDateRequestStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SettlementDateRequestStateType> lookup = new HashMap<Integer, SettlementDateRequestStateType>();
	static {
		for (SettlementDateRequestStateType s : EnumSet.allOf(SettlementDateRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static SettlementDateRequestStateType get(Integer requestState) {
		return lookup.get(requestState);
	}
	
}
