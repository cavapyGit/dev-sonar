package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TradeSettlementRequestType {
	
	RENOVATION (Integer.valueOf(2180), "RENOVACION"),
	COMPULSORY_PURCHASE (Integer.valueOf(2181), "COMPRA FORZOSA");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;
	private TradeSettlementRequestType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static final List<TradeSettlementRequestType> list = new ArrayList<TradeSettlementRequestType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, TradeSettlementRequestType> lookup = new HashMap<Integer, TradeSettlementRequestType>();
	static {
		for (TradeSettlementRequestType s : EnumSet.allOf(TradeSettlementRequestType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static TradeSettlementRequestType get(Integer codigo) {
		return lookup.get(codigo);
	}

}
