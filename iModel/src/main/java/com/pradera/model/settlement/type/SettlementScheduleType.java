package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SettlementScheduleType {

	DEFERRED (Integer.valueOf(1938), "LIQUIDACION_DIFERIDA"),
	
	FIRST_SETTLEMENT (Integer.valueOf(1939), "ETAPA_1"),
	
	SECOND_SETTLEMENT (Integer.valueOf(1940), "ETAPA_2"),
	
	MELOR (Integer.valueOf(1941), "MELOR"),
	
	MELID (Integer.valueOf(1942), "MELID");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private SettlementScheduleType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<SettlementScheduleType> list = new ArrayList<SettlementScheduleType>();
	public static final Map<Integer, SettlementScheduleType> lookup = new HashMap<Integer, SettlementScheduleType>();
	static {
		for (SettlementScheduleType s : EnumSet.allOf(SettlementScheduleType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
