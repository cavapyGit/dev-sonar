package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum SettlementDateRequestStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/09/2013
 */
public enum MonitoringSettCountType {

	TOTAL_OPERATION (Long.valueOf(1227), "REGISTRADA"),
	PENDIENT_OPERATION (Long.valueOf(1228), "RECHAZADA"),
	REMOVED_OPERATION (Long.valueOf(1229), "CONFIRMADA"),
	SETTLEMENT_OPERATION(Long.valueOf(1474), "APROBADA"),
	CANCEL_OPERATION(Long.valueOf(1475), "ANULADA");
	
	/** The code. */
	private Long code;
	/** The description. */
	private String description;

	/**
	 * Instantiates a new settlement date request state type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private MonitoringSettCountType(Long code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param descripcion the new description
	 */
	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	/** The Constant list. */
	public static final List<MonitoringSettCountType> list = new ArrayList<MonitoringSettCountType>();
	
	/** The Constant lookup. */
	public static final Map<Long, MonitoringSettCountType> lookup = new HashMap<Long, MonitoringSettCountType>();
	static {
		for (MonitoringSettCountType s : EnumSet.allOf(MonitoringSettCountType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static MonitoringSettCountType get(Long codigo) {
		return lookup.get(codigo);
	}
	
}
