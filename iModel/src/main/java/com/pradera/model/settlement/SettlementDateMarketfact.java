package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SETTLEMENT_DATE_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/08/2013
 */
@Entity
@Table(name="SETTLEMENT_DATE_MARKETFACT")
public class SettlementDateMarketfact implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id settlement date request pk. */
	@Id
	@SequenceGenerator(name="SETTLEMENT_DATE_MARKETFACT_GENERATOR", sequenceName="SQ_ID_SETT_DATE_MKTFACT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SETTLEMENT_DATE_MARKETFACT_GENERATOR")
	@Column(name="ID_SETT_DATE_MARKETFACT_PK")
	private Long idSettlementDateMarketfactPk;

	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Column(name="MARKET_QUANTITY")
	private BigDecimal marketQuantity;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_DATE_ACCOUNT_FK")
	private SettlementDateAccount settlementDateAccount;
	
	public Long getIdSettlementDateMarketfactPk() {
		return idSettlementDateMarketfactPk;
	}

	public void setIdSettlementDateMarketfactPk(Long idSettlementDateMarketfactPk) {
		this.idSettlementDateMarketfactPk = idSettlementDateMarketfactPk;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getMarketQuantity() {
		return marketQuantity;
	}

	public void setMarketQuantity(BigDecimal marketQuantity) {
		this.marketQuantity = marketQuantity;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public SettlementDateAccount getSettlementDateAccount() {
		return settlementDateAccount;
	}

	public void setSettlementDateAccount(SettlementDateAccount settlementDateAccount) {
		this.settlementDateAccount = settlementDateAccount;
	}

	
}