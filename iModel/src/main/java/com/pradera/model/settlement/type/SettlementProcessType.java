package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SettlementProcessType {

	/**the PARTIAL*/
	PARTIAL (Integer.valueOf(1279), "PARCIAL"),
	/**the FINAL*/
	FINAL (Integer.valueOf(1280), "FINAL");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private SettlementProcessType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<SettlementProcessType> list = new ArrayList<SettlementProcessType>();
	public static final Map<Integer, SettlementProcessType> lookup = new HashMap<Integer, SettlementProcessType>();
	static {
		for (SettlementProcessType s : EnumSet.allOf(SettlementProcessType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
