package com.pradera.model.settlement.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SanctionReasonType {
	
	MELOR_FUNDS (Integer.valueOf(1824), "MELOR POR FONDOS"),
	MELOR_STOCKS (Integer.valueOf(1825), "MELOR POR VALORES"),
	MELID_FUNDS (Integer.valueOf(1826), "MELID POR FONDOS"),
	MELID_STOCKS (Integer.valueOf(1827), "MELID POR VALORES"),
	UNFULFILLMENT_FUNDS (Integer.valueOf(1828), "MELID POR FONDOS"),
	UNFULFILLMENT_STOCK (Integer.valueOf(1829), "MELID POR VALORES"),
	OTHER (Integer.valueOf(1834), "OTROS");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;
	private SanctionReasonType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static final List<SanctionReasonType> list = new ArrayList<SanctionReasonType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SanctionReasonType> lookup = new HashMap<Integer, SanctionReasonType>();
	static {
		for (SanctionReasonType s : EnumSet.allOf(SanctionReasonType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static SanctionReasonType get(Integer codigo) {
		return lookup.get(codigo);
	}
	

}
