package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.HolderAccountOperation;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SETTLEMENT_DATE_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/08/2013
 */
@Entity
@Table(name="SETTLEMENT_ACCOUNT_OPERATION")
public class SettlementAccountOperation implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id settlement date request pk. */
	@Id
	@SequenceGenerator(name="SETTLEMENT_ACCOUNT_OPERATION_GENERATOR", sequenceName="SQ_ID_SETT_ACCOUNT_OPER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SETTLEMENT_ACCOUNT_OPERATION_GENERATOR")
	@Column(name="ID_SETTLEMENT_ACCOUNT_PK")
	private Long idSettlementAccountPk;
	
	/** The operation state. */
	@Column(name="OPERATION_STATE")
	private Integer operationState;
	
	/** The initial stock quantity. */
	@Column(name="INITIAL_STOCK_QUANTITY")
	private BigDecimal initialStockQuantity;
	
	/** The stock quantity. */
	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	/** The settled quantity. */
	@Column(name="SETTLED_QUANTITY")
	private BigDecimal settledQuantity;
	
	/** The role. */
	@Column(name="ROLE")
	private Integer role;
	
	/** The settlement amount. */
	@Column(name="SETTLEMENT_AMOUNT")
	private BigDecimal settlementAmount;
	
	/** The funds reference. */
	@Column(name="FUNDS_REFERENCE")
	private Long fundsReference;

	/** The stock reference. */
	@Column(name="STOCK_REFERENCE")
	private Long stockReference;
	
	/** The stock block date. */
	@Temporal(TemporalType.DATE)
	@Column(name="STOCK_BLOCK_DATE")
	private Date stockBlockDate;
	
    /** The ind unfulfilled. */
    @Column(name="IND_UNFULFILLED")
    private Integer indUnfulfilled;
    
    /** The holder account operation. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK")
    private HolderAccountOperation holderAccountOperation;
    
    /** The settlement account operation. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_ACCOUNT_FK")
    private SettlementAccountOperation settlementAccountOperation;
    
    /** The settlement operation. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_OPERATION_FK")
	private SettlementOperation settlementOperation;
    
    /** The settlement account marketfacts. */
    @OneToMany(mappedBy="settlementAccountOperation")
	private List<SettlementAccountMarketfact> settlementAccountMarketfacts;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The chained quantity. */
	@Column(name="CHAINED_QUANTITY")
	private BigDecimal chainedQuantity;
	
	/** The ind fund reference. */
    @Transient
    private String indFundReference;
    
    /** The fund reference desc. */
    @Transient
    private String fundReferenceDesc;
    
    /** The ind stock reference. */
    @Transient
    private String indStockReference;
    
    /** The stock reference desc. */
    @Transient
    private String stockReferenceDesc;
    
    /** The state description. */
    @Transient
    private String stateDescription;
    
    /** The is selected. */
    @Transient
    private boolean isSelected;
	
    /** The des role. */
    @Transient
    private String desRole;
    
    /** The indicator fund reference. */
    @Transient
    private String indChained;
    
    /** The indicator fund reference. */
    @Transient
    private String chainedDesc;
    
    
	/**
	 * Instantiates a new settlement account operation.
	 *
	 * @param idSettlementAccountPk the id settlement account pk
	 */
	public SettlementAccountOperation(Long idSettlementAccountPk) {
		super();
		this.idSettlementAccountPk = idSettlementAccountPk;
	}
	
	/**
	 * Instantiates a new settlement account operation.
	 */
	public SettlementAccountOperation() {
		super();
		indUnfulfilled = ComponentConstant.ZERO;
		stockQuantity = BigDecimal.ZERO;
		initialStockQuantity = BigDecimal.ZERO;
		chainedQuantity = BigDecimal.ZERO;
		settledQuantity = BigDecimal.ZERO;
	}
	
	/**
	 * Gets the id settlement account pk.
	 *
	 * @return the id settlement account pk
	 */
	public Long getIdSettlementAccountPk() {
		return idSettlementAccountPk;
	}

	/**
	 * Sets the id settlement account pk.
	 *
	 * @param idSettlementAccountPk the new id settlement account pk
	 */
	public void setIdSettlementAccountPk(Long idSettlementAccountPk) {
		this.idSettlementAccountPk = idSettlementAccountPk;
	}

	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Integer getOperationState() {
		return operationState;
	}

	/**
	 * Gets the settlement operation.
	 *
	 * @return the settlement operation
	 */
	public SettlementOperation getSettlementOperation() {
		return settlementOperation;
	}

	/**
	 * Sets the settlement operation.
	 *
	 * @param settlementOperation the new settlement operation
	 */
	public void setSettlementOperation(SettlementOperation settlementOperation) {
		this.settlementOperation = settlementOperation;
	}

	/**
	 * Sets the operation state.
	 *
	 * @param operationState the new operation state
	 */
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlement amount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}



	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the new settlement amount
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}



	/**
	 * Gets the funds reference.
	 *
	 * @return the funds reference
	 */
	public Long getFundsReference() {
		return fundsReference;
	}



	/**
	 * Sets the funds reference.
	 *
	 * @param fundsReference the new funds reference
	 */
	public void setFundsReference(Long fundsReference) {
		this.fundsReference = fundsReference;
	}



	/**
	 * Gets the stock reference.
	 *
	 * @return the stock reference
	 */
	public Long getStockReference() {
		return stockReference;
	}



	/**
	 * Sets the stock reference.
	 *
	 * @param stockReference the new stock reference
	 */
	public void setStockReference(Long stockReference) {
		this.stockReference = stockReference;
	}



	/**
	 * Gets the stock block date.
	 *
	 * @return the stock block date
	 */
	public Date getStockBlockDate() {
		return stockBlockDate;
	}



	/**
	 * Sets the stock block date.
	 *
	 * @param stockBlockDate the new stock block date
	 */
	public void setStockBlockDate(Date stockBlockDate) {
		this.stockBlockDate = stockBlockDate;
	}



	/**
	 * Gets the ind unfulfilled.
	 *
	 * @return the ind unfulfilled
	 */
	public Integer getIndUnfulfilled() {
		return indUnfulfilled;
	}



	/**
	 * Sets the ind unfulfilled.
	 *
	 * @param indUnfulfilled the new ind unfulfilled
	 */
	public void setIndUnfulfilled(Integer indUnfulfilled) {
		this.indUnfulfilled = indUnfulfilled;
	}



	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}



	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}


	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the settlement account operation.
	 *
	 * @return the settlement account operation
	 */
	public SettlementAccountOperation getSettlementAccountOperation() {
		return settlementAccountOperation;
	}

	/**
	 * Sets the settlement account operation.
	 *
	 * @param settlementAccountOperation the new settlement account operation
	 */
	public void setSettlementAccountOperation(
			SettlementAccountOperation settlementAccountOperation) {
		this.settlementAccountOperation = settlementAccountOperation;
	}

	/**
	 * Gets the settlement account marketfacts.
	 *
	 * @return the settlement account marketfacts
	 */
	public List<SettlementAccountMarketfact> getSettlementAccountMarketfacts() {
		return settlementAccountMarketfacts;
	}

	/**
	 * Sets the settlement account marketfacts.
	 *
	 * @param settlementAccountMarketfacts the new settlement account marketfacts
	 */
	public void setSettlementAccountMarketfacts(
			List<SettlementAccountMarketfact> settlementAccountMarketfacts) {
		this.settlementAccountMarketfacts = settlementAccountMarketfacts;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Gets the initial stock quantity.
	 *
	 * @return the initial stock quantity
	 */
	public BigDecimal getInitialStockQuantity() {
		return initialStockQuantity;
	}

	/**
	 * Sets the initial stock quantity.
	 *
	 * @param initialStockQuantity the new initial stock quantity
	 */
	public void setInitialStockQuantity(BigDecimal initialStockQuantity) {
		this.initialStockQuantity = initialStockQuantity;
	}

	/**
	 * Gets the chained quantity.
	 *
	 * @return the chained quantity
	 */
	public BigDecimal getChainedQuantity() {
		return chainedQuantity;
	}

	/**
	 * Sets the chained quantity.
	 *
	 * @param chainedQuantity the new chained quantity
	 */
	public void setChainedQuantity(BigDecimal chainedQuantity) {
		this.chainedQuantity = chainedQuantity;
	}

	/**
	 * Gets the ind fund reference.
	 *
	 * @return the ind fund reference
	 */
	public String getIndFundReference() {
		return indFundReference;
	}

	/**
	 * Sets the ind fund reference.
	 *
	 * @param indFundReference the new ind fund reference
	 */
	public void setIndFundReference(String indFundReference) {
		this.indFundReference = indFundReference;
	}

	/**
	 * Gets the fund reference desc.
	 *
	 * @return the fund reference desc
	 */
	public String getFundReferenceDesc() {
		return fundReferenceDesc;
	}

	/**
	 * Sets the fund reference desc.
	 *
	 * @param fundReferenceDesc the new fund reference desc
	 */
	public void setFundReferenceDesc(String fundReferenceDesc) {
		this.fundReferenceDesc = fundReferenceDesc;
	}

	/**
	 * Gets the ind stock reference.
	 *
	 * @return the ind stock reference
	 */
	public String getIndStockReference() {
		return indStockReference;
	}

	/**
	 * Sets the ind stock reference.
	 *
	 * @param indStockReference the new ind stock reference
	 */
	public void setIndStockReference(String indStockReference) {
		this.indStockReference = indStockReference;
	}

	/**
	 * Gets the stock reference desc.
	 *
	 * @return the stock reference desc
	 */
	public String getStockReferenceDesc() {
		return stockReferenceDesc;
	}

	/**
	 * Sets the stock reference desc.
	 *
	 * @param stockReferenceDesc the new stock reference desc
	 */
	public void setStockReferenceDesc(String stockReferenceDesc) {
		this.stockReferenceDesc = stockReferenceDesc;
	}

	/**
	 * Gets the settled quantity.
	 *
	 * @return the settled quantity
	 */
	public BigDecimal getSettledQuantity() {
		return settledQuantity;
	}

	/**
	 * Sets the settled quantity.
	 *
	 * @param settledQuantity the new settled quantity
	 */
	public void setSettledQuantity(BigDecimal settledQuantity) {
		this.settledQuantity = settledQuantity;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param isSelected the new selected
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	/**
	 * Gets the des role.
	 *
	 * @return the des role
	 */
	public String getDesRole() {
		return desRole;
	}

	/**
	 * Sets the des role.
	 *
	 * @param desRole the new des role
	 */
	public void setDesRole(String desRole) {
		this.desRole = desRole;
	}
	
	/**
	 * Gets the row key.
	 *
	 * @return the row key
	 */
	public int getRowKey(){
    	return this.hashCode();
    }

	/**
	 * @return the indChained
	 */
	public String getIndChained() {
		return indChained;
	}

	/**
	 * @param indChained the indChained to set
	 */
	public void setIndChained(String indChained) {
		this.indChained = indChained;
	}

	/**
	 * @return the chainedDesc
	 */
	public String getChainedDesc() {
		return chainedDesc;
	}

	/**
	 * @param chainedDesc the chainedDesc to set
	 */
	public void setChainedDesc(String chainedDesc) {
		this.chainedDesc = chainedDesc;
	}

}