package com.pradera.model.settlement;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.negotiation.TradeRequest;
/**
 * The persistent class for the CURRENCY_SETTLEMENT_REQUEST database table.
 * 
 */
@Entity 
@Table(name = "TRADE_SETTLEMENT_REQUEST")
public class TradeSettlementRequest implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "SQ_ID_TRADE_SETTLEMENT_REQ_PK_", sequenceName = "SQ_ID_TRADE_SETTLEMENT_REQ_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_TRADE_SETTLEMENT_REQ_PK_")
	@Column(name = "ID_TRADE_SETTLEMENT_REQ_PK")
	private Long idTradeSettlementRequestPk;
	
	//bi-directional many-to-one association to Participant source
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_SOURCE_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant sourceParticipant;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_REQUEST_FK", referencedColumnName="ID_TRADE_REQUEST_PK")
	private TradeRequest tradeRequest;
    
  //bi-directional many-to-one association to Participant target
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_TARGET_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant targetParticipant;
	
	/** The request State. */
    @Column(name="REQUEST_STATE")
    private Integer requestState;
    
    /** The request State. */
    @Column(name="REQUEST_TYPE")
    private Integer requestType;
    
    /** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTRY_DATE")
	private Date registerDate;

	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registerUser;
	
	/** The approval date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "APPROVAL_DATE")
	private Date approvalDate;

	/** The approval user. */
	@Column(name = "APPROVAL_USER")
	private String approvalUser;
	
	/** The annul date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ANNUL_DATE")
	private Date annulDate;

	/** The annul user. */
	@Column(name = "ANNUL_USER")
	private String annulUser;
	
	/** The review date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REVIEW_DATE")
	private Date reviewDate;

	/** The review user. */
	@Column(name = "REVIEW_USER")
	private String reviewUser;
	
	/** The confirm date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;

	/** The confirm user. */
	@Column(name = "CONFIRM_USER")
	private String confirmUser;
	
	/** The authorize date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AUTHORIZE_DATE")
	private Date authorizeDate;

	/** The authorize user. */
	@Column(name = "AUTHORIZE_USER")
	private String authorizeUser;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REJECT_DATE")
	private Date rejectDate;

	/** The reject user. */
	@Column(name = "REJECT_USER")
	private String rejectUser;
	
	/** The annulment motive. */
	@Column(name="ANNUL_MOTIVE")
	private Integer annulMotive;
	
	/** The reject motive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/** The annulment other motive. */
	@Column(name = "ANNUL_OTHER_MOTIVE")
	private String annulOtherMotive;

	/** The reject other motive. */
	@Column(name = "REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;
	
	/** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	// bi-directional many-to-one association to Security
	/** The currency settlement detail. */
	@OneToMany(fetch=FetchType.LAZY ,mappedBy = "tradeSettlementRequest")
	private List<SettlementRequest> lstSettlementRequest;
	
	public TradeSettlementRequest() {
		
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getIdTradeSettlementRequestPk() {
		return idTradeSettlementRequestPk;
	}

	public void setIdTradeSettlementRequestPk(Long idTradeSettlementRequestPk) {
		this.idTradeSettlementRequestPk = idTradeSettlementRequestPk;
	}

	public Participant getSourceParticipant() {
		return sourceParticipant;
	}

	public void setSourceParticipant(Participant sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}

	public TradeRequest getTradeRequest() {
		return tradeRequest;
	}
	
	public List<TradeRequest> getTradeRequestLst(){
		return Arrays.asList(tradeRequest);
	}

	public void setTradeRequest(TradeRequest tradeRequest) {
		this.tradeRequest = tradeRequest;
	}

	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}

	public Integer getRequestState() {
		return requestState;
	}

	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	public Integer getRequestType() {
		return requestType;
	}

	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalUser() {
		return approvalUser;
	}

	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	public Date getAnnulDate() {
		return annulDate;
	}

	public void setAnnulDate(Date annulDate) {
		this.annulDate = annulDate;
	}

	public String getAnnulUser() {
		return annulUser;
	}

	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getReviewUser() {
		return reviewUser;
	}

	public void setReviewUser(String reviewUser) {
		this.reviewUser = reviewUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getAuthorizeDate() {
		return authorizeDate;
	}

	public void setAuthorizeDate(Date authorizeDate) {
		this.authorizeDate = authorizeDate;
	}

	public String getAuthorizeUser() {
		return authorizeUser;
	}

	public void setAuthorizeUser(String authorizeUser) {
		this.authorizeUser = authorizeUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	public Integer getAnnulMotive() {
		return annulMotive;
	}

	public void setAnnulMotive(Integer annulMotive) {
		this.annulMotive = annulMotive;
	}

	public Integer getRejectMotive() {
		return rejectMotive;
	}

	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	public String getAnnulOtherMotive() {
		return annulOtherMotive;
	}

	public void setAnnulOtherMotive(String annulOtherMotive) {
		this.annulOtherMotive = annulOtherMotive;
	}

	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public List<SettlementRequest> getLstSettlementRequest() {
		return lstSettlementRequest;
	}

	public void setLstSettlementRequest(List<SettlementRequest> lstSettlementRequest) {
		this.lstSettlementRequest = lstSettlementRequest;
	}	
}
