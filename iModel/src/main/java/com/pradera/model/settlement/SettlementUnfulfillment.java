package com.pradera.model.settlement;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
/**
 * 
 */

@Entity 
@Table(name = "SETTLEMENT_UNFULFILLMENT")
public class SettlementUnfulfillment implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "SETTLEMENT_UNFULFILLMENTPK_GENERATOR", sequenceName = "SQ_ID_SETTLEMENT_UNFULFILLMENT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SETTLEMENT_UNFULFILLMENTPK_GENERATOR")
	@Column(name = "ID_SETTLEMENT_UNFULFILLMENT_PK")
	private Long idSettlementUnfulfillmentPk;		
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OPERATION_UNFULFILLMENT_FK", referencedColumnName="ID_OPERATION_UNFULFILLMENT_PK")
    private OperationUnfulfillment operationUnfulfillment;
    
	/** The request State. */
    @Column(name="REQUEST_STATE")
    private Integer requestState;
    
	/** The request State. */
    @Column(name="IND_BUYER_SELLER")
    private Integer indBuyerSeller;
      
    /** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTER_DATE")
	private Date registerDate;
	
	/** The registry user. */
	@Column(name = "REGISTER_USER")
	private String registerUser;
	
	/** The approval date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "APPROVAL_DATE")
	private Date approvalDate;

	/** The approval user. */
	@Column(name = "APPROVAL_USER")
	private String approvalUser;
	
	/** The annul date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ANNUL_DATE")
	private Date annulDate;

	/** The annul user. */
	@Column(name = "ANNUL_USER")
	private String annulUser;
	
	/** The review date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REVIEW_DATE")
	private Date reviewDate;

	/** The review user. */
	@Column(name = "REVIEW_USER")
	private String reviewUser;
	
	/** The confirm date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;

	/** The confirm user. */
	@Column(name = "CONFIRM_USER")
	private String confirmUser;
	
	/** The authorize date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AUTHORIZE_DATE")
	private Date authorizeDate;

	/** The authorize user. */
	@Column(name = "AUTHORIZE_USER")
	private String authorizeUser;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REJECT_DATE")
	private Date rejectDate;

	/** The reject user. */
	@Column(name = "REJECT_USER")
	private String rejectUser;
	
    /** The certification file. */
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="UNFULFILLMENT_FILE")
	private byte[] unfulfillmentFile;
	
	
	/** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private String requestStateDescription;
	
	
	public SettlementUnfulfillment() {
		
	}	
	
	public SettlementUnfulfillment(Long idSettlementUnfulfillmentPk,
			Integer requestState) {
		this.idSettlementUnfulfillmentPk = idSettlementUnfulfillmentPk;
		this.requestState = requestState;
	}

	public Long getIdSettlementUnfulfillmentPk() {
		return idSettlementUnfulfillmentPk;
	}

	public void setIdSettlementUnfulfillmentPk(Long idSettlementUnfulfillmentPk) {
		this.idSettlementUnfulfillmentPk = idSettlementUnfulfillmentPk;
	}
	
	public Integer getRequestState() {
		return requestState;
	}

	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalUser() {
		return approvalUser;
	}

	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	public Date getAnnulDate() {
		return annulDate;
	}

	public void setAnnulDate(Date annulDate) {
		this.annulDate = annulDate;
	}

	public String getAnnulUser() {
		return annulUser;
	}

	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getReviewUser() {
		return reviewUser;
	}

	public void setReviewUser(String reviewUser) {
		this.reviewUser = reviewUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}


	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	public Date getAuthorizeDate() {
		return authorizeDate;
	}

	public void setAuthorizeDate(Date authorizeDate) {
		this.authorizeDate = authorizeDate;
	}

	public String getRequestStateDescription() {
		return requestStateDescription;
	}

	public void setRequestStateDescription(String requestStateDescription) {
		this.requestStateDescription = requestStateDescription;
	}

	public String getAuthorizeUser() {
		return authorizeUser;
	}

	public void setAuthorizeUser(String authorizeUser) {
		this.authorizeUser = authorizeUser;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public OperationUnfulfillment getOperationUnfulfillment() {
		return operationUnfulfillment;
	}

	public void setOperationUnfulfillment(OperationUnfulfillment operationUnfulfillment) {
		this.operationUnfulfillment = operationUnfulfillment;
	}

	public Integer getIndBuyerSeller() {
		return indBuyerSeller;
	}

	public void setIndBuyerSeller(Integer indBuyerSeller) {
		this.indBuyerSeller = indBuyerSeller;
	}

	public byte[] getUnfulfillmentFile() {
		return unfulfillmentFile;
	}

	public void setUnfulfillmentFile(byte[] unfulfillmentFile) {
		this.unfulfillmentFile = unfulfillmentFile;
	}
	
	
}
