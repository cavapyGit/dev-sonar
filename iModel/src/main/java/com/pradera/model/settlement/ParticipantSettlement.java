package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;


/**
 * The persistent class for the PARTICIPANT_OPERATION database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_SETTLEMENT")
public class ParticipantSettlement implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARTICIPANT_SETTLEMENT_GENERATOR", sequenceName="SQ_ID_PART_SETTLEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_SETTLEMENT_GENERATOR")
	@Column(name="ID_PARTICIPANT_SETTLEMENT_PK")
	private Long idParticipantSettlementPk;

	@Column(name="FUNDS_REFERENCE")
	private Long fundsReference;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="OPERATION_STATE")
	private Integer operationState;

	@Column(name="IND_INCHARGE")
	private Integer indIncharge;
	
	@Column(name="ROLE")
	private Integer role;

	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	@Column(name="SETTLEMENT_CURRENCY")
	private Integer settlementCurrency;
	
	@Column(name="SETTLEMENT_AMOUNT")
	private BigDecimal settlementAmount;
	
	@Column(name="DEPOSITED_AMOUNT")
	private BigDecimal depositedAmount;
	
	//bi-directional many-to-one association to MechanismOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_OPERATION_FK")
	private SettlementOperation settlementOperation;
    
	@Column(name="IND_PAYED_BACK")
	private Integer indPayedBack;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private BigDecimal unpaidAmount;
	
	@Transient
	private BigDecimal overpaidAmount;
	
    public ParticipantSettlement() {
    	indPayedBack = ComponentConstant.ZERO;
    	indIncharge = ComponentConstant.ZERO;
    	depositedAmount = BigDecimal.ZERO;
    }

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	public Integer getIndIncharge() {
		return indIncharge;
	}

	public void setIndIncharge(Integer indIncharge) {
		this.indIncharge = indIncharge;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Long getFundsReference() {
		return fundsReference;
	}

	public void setFundsReference(Long fundsReference) {
		this.fundsReference = fundsReference;
	}

	public BigDecimal getDepositedAmount() {
		return depositedAmount;
	}

	public void setDepositedAmount(BigDecimal depositedAmount) {
		this.depositedAmount = depositedAmount;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Integer getIndPayedBack() {
		return indPayedBack;
	}

	public void setIndPayedBack(Integer indPayedBack) {
		this.indPayedBack = indPayedBack;
	}

	public Integer getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public Long getIdParticipantSettlementPk() {
		return idParticipantSettlementPk;
	}

	public void setIdParticipantSettlementPk(Long idParticipantSettlementPk) {
		this.idParticipantSettlementPk = idParticipantSettlementPk;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public SettlementOperation getSettlementOperation() {
		return settlementOperation;
	}

	public void setSettlementOperation(SettlementOperation settlementOperation) {
		this.settlementOperation = settlementOperation;
	}

	public BigDecimal getUnpaidAmount() {
		return unpaidAmount;
	}

	public void setUnpaidAmount(BigDecimal unpaidAmount) {
		this.unpaidAmount = unpaidAmount;
	}

	public BigDecimal getOverpaidAmount() {
		return overpaidAmount;
	}

	public void setOverpaidAmount(BigDecimal overpaidAmount) {
		this.overpaidAmount = overpaidAmount;
	}	
}