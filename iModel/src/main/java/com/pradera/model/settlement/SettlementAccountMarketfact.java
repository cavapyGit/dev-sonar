package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SETTLEMENT_DATE_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/08/2013
 */
@Entity
@Table(name="SETTLEMENT_ACCOUNT_MARKETFACT")
public class SettlementAccountMarketfact implements Serializable,Auditable, Cloneable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id settlement date request pk. */
	@Id
	@SequenceGenerator(name="SETTLEMENT_ACCOUNT_MARKETFACT_GENERATOR", sequenceName="SQ_ID_SETT_ACC_MKTFACT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SETTLEMENT_ACCOUNT_MARKETFACT_GENERATOR")
	@Column(name="ID_ACCOUNT_MARKETFACT_PK")
	private Long idSettAccountMarketfactPk;

	/** The market date. */
	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;
	
	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	/** The market price. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	/** The market quantity. */
	@Column(name="MARKET_QUANTITY")
	private BigDecimal marketQuantity;
	
	/** The chained quantity. */
	@Column(name="CHAINED_QUANTITY")
	private BigDecimal chainedQuantity;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The ind active. */
	@Column(name="IND_ACTIVE")
	private Integer indActive = ComponentConstant.ONE;
	
	/** The original quantity. */
	@Transient
	private BigDecimal originalQuantity;
	
	/** The selected. */
	@Transient
	private boolean selected;
	
	/** The settlement account operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_ACCOUNT_FK")
	private SettlementAccountOperation settlementAccountOperation;	
	
	/**
	 * Instantiates a new settlement account marketfact.
	 */
	public SettlementAccountMarketfact() {
		super();
		this.chainedQuantity= BigDecimal.ZERO;
	}

	/**
	 * Gets the id sett account marketfact pk.
	 *
	 * @return the id sett account marketfact pk
	 */
	public Long getIdSettAccountMarketfactPk() {
		return idSettAccountMarketfactPk;
	}

	/**
	 * Sets the id sett account marketfact pk.
	 *
	 * @param idSettAccountMarketfactPk the new id sett account marketfact pk
	 */
	public void setIdSettAccountMarketfactPk(Long idSettAccountMarketfactPk) {
		this.idSettAccountMarketfactPk = idSettAccountMarketfactPk;
	}

	/**
	 * Gets the settlement account operation.
	 *
	 * @return the settlement account operation
	 */
	public SettlementAccountOperation getSettlementAccountOperation() {
		return settlementAccountOperation;
	}

	/**
	 * Sets the settlement account operation.
	 *
	 * @param settlementAccountOperation the new settlement account operation
	 */
	public void setSettlementAccountOperation(
			SettlementAccountOperation settlementAccountOperation) {
		this.settlementAccountOperation = settlementAccountOperation;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market quantity.
	 *
	 * @return the market quantity
	 */
	public BigDecimal getMarketQuantity() {
		return marketQuantity;
	}

	/**
	 * Sets the market quantity.
	 *
	 * @param marketQuantity the new market quantity
	 */
	public void setMarketQuantity(BigDecimal marketQuantity) {
		this.marketQuantity = marketQuantity;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the ind active.
	 *
	 * @return the ind active
	 */
	public Integer getIndActive() {
		return indActive;
	}

	/**
	 * Sets the ind active.
	 *
	 * @param indActive the new ind active
	 */
	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}

	/**
	 * Gets the original quantity.
	 *
	 * @return the original quantity
	 */
	public BigDecimal getOriginalQuantity() {
		return originalQuantity;
	}

	/**
	 * Sets the original quantity.
	 *
	 * @param originalQuantity the new original quantity
	 */
	public void setOriginalQuantity(BigDecimal originalQuantity) {
		this.originalQuantity = originalQuantity;
	}

	/**
	 * Gets the chained quantity.
	 *
	 * @return the chained quantity
	 */
	public BigDecimal getChainedQuantity() {
		return chainedQuantity;
	}

	/**
	 * Sets the chained quantity.
	 *
	 * @param chainedQuantity the new chained quantity
	 */
	public void setChainedQuantity(BigDecimal chainedQuantity) {
		this.chainedQuantity = chainedQuantity;
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public SettlementAccountMarketfact clone() throws CloneNotSupportedException {
        return (SettlementAccountMarketfact) super.clone();
    }

	
}