package com.pradera.model.settlement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SETTLEMENT_DATE_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/08/2013
 */
@Entity
@Table(name="SETTLEMENT_OPERATION")
public class SettlementOperation implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id settlement date request pk. */
	@Id
	@SequenceGenerator(name="SETTLEMENT_OPERATION_GENERATOR", sequenceName="SQ_ID_SETTLEMENT_OPER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SETTLEMENT_OPERATION_GENERATOR")
	@Column(name="ID_SETTLEMENT_OPERATION_PK")
	private Long idSettlementOperationPk;
	
	@Column(name="OPERATION_STATE")
	private Integer operationState;
	
	@Column(name="OPERATION_PART")
	private Integer operationPart;
	
	@Column(name="INITIAL_STOCK_QUANTITY")
	private BigDecimal initialStockQuantity;

	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	@Column(name="SETTLED_QUANTITY")
	private BigDecimal settledQuantity;
	
	@Temporal( TemporalType.DATE)
	@Column(name="SETTLEMENT_DATE")
	private Date settlementDate;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REAL_SETTLEMENT_DATE")
	private Date realSettlementDate;
	
	@Column(name="INITIAL_SETTLEMENT_PRICE")
	private BigDecimal initialSettlementPrice;
	
	@Column(name="SETTLEMENT_PRICE")
	private BigDecimal settlementPrice;
	
	@Column(name="SETTLEMENT_AMOUNT")
	private BigDecimal settlementAmount;
	
	@Column(name="SETTLEMENT_CURRENCY")
	private Integer settlementCurrency;
	
	@Column(name="INITIAL_SETTLEMENT_CURRENCY")
	private Integer initialSettlementCurrency;
	
	@Column(name="SETTLEMENT_DAYS")
	private Long settlementDays;
	
	@Column(name="AMOUNT_RATE")
	private BigDecimal amountRate;
	
	@Column(name="EXCHANGE_RATE")
	private BigDecimal exchangeRate;
	
	@Column(name="SETTLEMENT_TYPE")
	private Integer settlementType;
	
	@Column(name="SETTLEMENT_SCHEMA")
	private Integer settlementSchema;
	
	@Column(name="FUNDS_REFERENCE")
	private Long fundsReference;

	@Column(name="STOCK_REFERENCE")
	private Long stockReference;
	
	@Column(name="MARGIN_REFERENCE")
	private Long marginReference;
	
	@Temporal(TemporalType.DATE)
	@Column(name="STOCK_BLOCK_DATE")
	private Date stockBlockDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="STOCK_DELIVERY_DATE")
	private Date stockDeliveryDate;
	
	@Column(name="IND_PARTIAL")
	private Integer indPartial;
	
	@Column(name="IND_PREPAID")
	private Integer indPrepaid;
	
	@Column(name="IND_EXTENDED")
	private Integer indExtended;
	
	@Column(name="IND_CURRENCY_EXCHANGE")
	private Integer indCurrencyExchange;
	
	@Column(name="IND_SETTLEMENT_EXCHANGE")
	private Integer indSettlementExchange;
	
	@Column(name="IND_REMOVE_SETTLEMENT")
    private Integer indRemoveSettlement;
    
    @Column(name="IND_REENTER_SETTLEMENT")
    private Integer indReenterSettlement;
    
    @Column(name="IND_UNFULFILLED")
    private Integer indUnfulfilled;
    
    @Column(name="IND_DEMATERIALIZATION")
    private Integer indDematerialization;
    
    @Column(name="IND_FORCED_PURCHASE")
    private Integer indForcedPurchase;
    
    @Column(name="IND_SENT_INTERFACE")
    private Integer indSentInterface;
    
    @Column(name="REMOVE_SETTLEMENT_MOTIVE")
    private Integer removeSettlementMotive;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK", referencedColumnName="ID_MECHANISM_OPERATION_PK")
    private MechanismOperation mechanismOperation;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private boolean selected;
	
	@Transient
	private boolean forcedPurchase;
	
	@Transient
	private Long lngForcedPurchase;
	
	@Transient
	private String stateDescription;
	
	@Transient
	private String operationPartDescription;
	
	@Transient
	private String currencyDescription;
	
	@Transient
	private String stockReferenceDesc;
	
	@Transient
	private String fundsReferenceDesc;
	
	@Transient
	private String motiveRemovedOperation;
	
	@Transient
	private List<ParticipantSettlement> participantSettlements;
	
	@Transient
	private List<SettlementAccountOperation> settlementAccountOperations;


	public SettlementOperation() {
		super();
		settledQuantity = BigDecimal.ZERO;
		indPartial = ComponentConstant.ZERO;
		indExtended = ComponentConstant.ZERO;
		indPrepaid = ComponentConstant.ZERO;
		indReenterSettlement = ComponentConstant.ZERO;
		indRemoveSettlement = ComponentConstant.ZERO;
		indUnfulfilled = ComponentConstant.ZERO;
		indCurrencyExchange = ComponentConstant.ZERO;
		indSettlementExchange = ComponentConstant.ZERO;
		indDematerialization = ComponentConstant.ZERO;
		indForcedPurchase = ComponentConstant.ZERO;
		indSentInterface = ComponentConstant.ZERO;
	}
	
	public SettlementOperation(Long idSettlementOperationPk){
		super();
		this.idSettlementOperationPk = idSettlementOperationPk;
	}
	public SettlementOperation(Long idMechanismOperationPk, Long idNegotiationMechanismPk, String mechanismName,
			Long idNegotiationModalityPk, String modalityName,
			Date operationDate, Long operationNumber, Long ballotNumber, Long sequential,
			Long idBuyerParticipantPk, String buyerNemonicParticipant, Long idSellerParticipantPk, String sellerNemonicParticipant,  
			String idSecurityCodePk, Integer operationState, Integer settlementState, BigDecimal stockQuantity,
			BigDecimal settlementAmount, BigDecimal settlementPrice, BigDecimal amountRate, Date settlementDate, Integer operationPart, 
			Integer removeSettlementMotive, Integer indRemoveSettlement, Integer indReenterSettlement, String stateDescription, Integer indSentInterface,
			Long lngForcedPurchase,String motiveRemovedOperation) {
			super();
			mechanismOperation = new MechanismOperation();
			mechanismOperation.setIdMechanismOperationPk(idMechanismOperationPk);
			mechanismOperation.setMechanisnModality(new MechanismModality());
			mechanismOperation.getMechanisnModality().setNegotiationMechanism(new NegotiationMechanism());
			mechanismOperation.getMechanisnModality().getNegotiationMechanism().setIdNegotiationMechanismPk(idNegotiationMechanismPk);
			mechanismOperation.getMechanisnModality().getNegotiationMechanism().setMechanismName(mechanismName);
			mechanismOperation.getMechanisnModality().setNegotiationModality(new NegotiationModality());
			mechanismOperation.getMechanisnModality().getNegotiationModality().setIdNegotiationModalityPk(idNegotiationModalityPk);
			mechanismOperation.getMechanisnModality().getNegotiationModality().setModalityName(modalityName);
			mechanismOperation.setOperationDate(operationDate);
			mechanismOperation.setOperationNumber(operationNumber);
			mechanismOperation.setBallotNumber(ballotNumber);
			mechanismOperation.setSequential(sequential);
			mechanismOperation.setBuyerParticipant(new Participant(idBuyerParticipantPk,buyerNemonicParticipant));
			mechanismOperation.setSellerParticipant(new Participant(idSellerParticipantPk,sellerNemonicParticipant));
			mechanismOperation.setSecurities(new Security(idSecurityCodePk));
			mechanismOperation.setOperationState(operationState);
			this.operationState = settlementState;
			this.stockQuantity = stockQuantity;
			this.settlementAmount = settlementAmount;
			this.settlementPrice = settlementPrice;
			this.amountRate = amountRate;
			this.settlementDate = settlementDate;
			this.operationPart = operationPart;
			this.indRemoveSettlement = indRemoveSettlement;
			this.indReenterSettlement = indReenterSettlement;
			this.stateDescription = stateDescription;
			this.indSentInterface = indSentInterface;
			this.lngForcedPurchase = lngForcedPurchase;
			this.motiveRemovedOperation = motiveRemovedOperation;
		}

	public Date getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public BigDecimal getSettlementPrice() {
		return settlementPrice;
	}

	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

	public BigDecimal getAmountRate() {
		return amountRate;
	}

	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}

	public Long getSettlementDays() {
		return settlementDays;
	}

	public void setSettlementDays(Long settlementDays) {
		this.settlementDays = settlementDays;
	}

	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public Integer getOperationPart() {
		return operationPart;
	}

	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public Long getIdSettlementOperationPk() {
		return idSettlementOperationPk;
	}

	public void setIdSettlementOperationPk(Long idSettlementOperationPk) {
		this.idSettlementOperationPk = idSettlementOperationPk;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public Long getFundsReference() {
		return fundsReference;
	}


	public void setFundsReference(Long fundsReference) {
		this.fundsReference = fundsReference;
	}


	public Long getStockReference() {
		return stockReference;
	}


	public void setStockReference(Long stockReference) {
		this.stockReference = stockReference;
	}


	public Long getMarginReference() {
		return marginReference;
	}


	public void setMarginReference(Long marginReference) {
		this.marginReference = marginReference;
	}


	public Date getStockBlockDate() {
		return stockBlockDate;
	}


	public void setStockBlockDate(Date stockBlockDate) {
		this.stockBlockDate = stockBlockDate;
	}


	public Date getStockDeliveryDate() {
		return stockDeliveryDate;
	}


	public void setStockDeliveryDate(Date stockDeliveryDate) {
		this.stockDeliveryDate = stockDeliveryDate;
	}

	public Integer getIndUnfulfilled() {
		return indUnfulfilled;
	}


	public void setIndUnfulfilled(Integer indUnfulfilled) {
		this.indUnfulfilled = indUnfulfilled;
	}


	public Integer getRemoveSettlementMotive() {
		return removeSettlementMotive;
	}


	public void setRemoveSettlementMotive(Integer removeSettlementMotive) {
		this.removeSettlementMotive = removeSettlementMotive;
	}


	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Integer getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public Integer getSettlementType() {
		return settlementType;
	}

	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}

	public Integer getIndRemoveSettlement() {
		return indRemoveSettlement;
	}

	public void setIndRemoveSettlement(Integer indRemoveSettlement) {
		this.indRemoveSettlement = indRemoveSettlement;
	}

	public Integer getIndReenterSettlement() {
		return indReenterSettlement;
	}

	public void setIndReenterSettlement(Integer indReenterSettlement) {
		this.indReenterSettlement = indReenterSettlement;
	}


	public Integer getIndExtended() {
		return indExtended;
	}


	public void setIndExtended(Integer indExtended) {
		this.indExtended = indExtended;
	}


	public Integer getIndPrepaid() {
		return indPrepaid;
	}


	public void setIndPrepaid(Integer indPrepaid) {
		this.indPrepaid = indPrepaid;
	}


	public String getStateDescription() {
		return stateDescription;
	}


	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	public String getCurrencyDescription() {
		return currencyDescription;
	}

	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	public Integer getIndPartial() {
		return indPartial;
	}

	public void setIndPartial(Integer indPartial) {
		this.indPartial = indPartial;
	}

	public BigDecimal getInitialStockQuantity() {
		return initialStockQuantity;
	}

	public void setInitialStockQuantity(BigDecimal initialStockQuantity) {
		this.initialStockQuantity = initialStockQuantity;
	}

	public Integer getIndCurrencyExchange() {
		return indCurrencyExchange;
	}

	public void setIndCurrencyExchange(Integer indCurrencyExchange) {
		this.indCurrencyExchange = indCurrencyExchange;
	}

	public Integer getIndSettlementExchange() {
		return indSettlementExchange;
	}

	public void setIndSettlementExchange(Integer indSettlementExchange) {
		this.indSettlementExchange = indSettlementExchange;
	}

	public List<ParticipantSettlement> getParticipantSettlements() {
		return participantSettlements;
	}

	public void setParticipantSettlements(
			List<ParticipantSettlement> participantSettlements) {
		this.participantSettlements = participantSettlements;
	}

	public Date getRealSettlementDate() {
		return realSettlementDate;
	}

	public void setRealSettlementDate(Date realSettlementDate) {
		this.realSettlementDate = realSettlementDate;
	}

	public Integer getSettlementSchema() {
		return settlementSchema;
	}

	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	public String getStockReferenceDesc() {
		return stockReferenceDesc;
	}

	public void setStockReferenceDesc(String stockReferenceDesc) {
		this.stockReferenceDesc = stockReferenceDesc;
	}

	public String getFundsReferenceDesc() {
		return fundsReferenceDesc;
	}

	public void setFundsReferenceDesc(String fundsReferenceDesc) {
		this.fundsReferenceDesc = fundsReferenceDesc;
	}

	public List<SettlementAccountOperation> getSettlementAccountOperations() {
		return settlementAccountOperations;
	}

	public void setSettlementAccountOperations(
			List<SettlementAccountOperation> settlementAccountOperations) {
		this.settlementAccountOperations = settlementAccountOperations;
	}

	public BigDecimal getSettledQuantity() {
		return settledQuantity;
	}

	public void setSettledQuantity(BigDecimal settledQuantity) {
		this.settledQuantity = settledQuantity;
	}

	public String getOperationPartDescription() {
		return operationPartDescription;
	}

	public Integer getIndDematerialization() {
		return indDematerialization;
	}

	public void setIndDematerialization(Integer indDematerialization) {
		this.indDematerialization = indDematerialization;
	}

	public void setOperationPartDescription(String operationPartDescription) {
		this.operationPartDescription = operationPartDescription;
	}

	public Integer getIndForcedPurchase() {
		return indForcedPurchase;
	}

	public void setIndForcedPurchase(Integer indForcedPurchase) {
		this.indForcedPurchase = indForcedPurchase;
	}

	public BigDecimal getInitialSettlementPrice() {
		return initialSettlementPrice;
	}

	public void setInitialSettlementPrice(BigDecimal initialSettlementPrice) {
		this.initialSettlementPrice = initialSettlementPrice;
	}

	public Integer getInitialSettlementCurrency() {
		return initialSettlementCurrency;
	}

	public void setInitialSettlementCurrency(Integer initialSettlementCurrency) {
		this.initialSettlementCurrency = initialSettlementCurrency;
	}

	public Integer getIndSentInterface() {
		return indSentInterface;
	}

	public void setIndSentInterface(Integer indSentInterface) {
		this.indSentInterface = indSentInterface;
	}

	public boolean isForcedPurchase() {
		return forcedPurchase;
	}

	public void setForcedPurchase(boolean forcedPurchase) {
		this.forcedPurchase = forcedPurchase;
	}

	public Long getLngForcedPurchase() {
		return lngForcedPurchase;
	}

	public void setLngForcedPurchase(Long lngForcedPurchase) {
		this.lngForcedPurchase = lngForcedPurchase;
	}

	public String getMotiveRemovedOperation() {
		return motiveRemovedOperation;
	}

	public void setMotiveRemovedOperation(String motiveRemovedOperation) {
		this.motiveRemovedOperation = motiveRemovedOperation;
	}

	

}