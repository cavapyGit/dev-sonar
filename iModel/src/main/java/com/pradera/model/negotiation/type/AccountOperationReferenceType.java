package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AccountOperationReferenceType {

	COMPLIANCE_SECURITIES (new Long(1109), "VALORES CONFORMES"),
	AVAILABLE_SECURITIES (new Long(1110), "VALORES DISPONIBLES"),
	DELIVERIED_SECURITIES (new Long(1111), "VALORES ENTREGADOS"),
	REFERENTIAL_SECURITIES (new Long(1484), "VALORES REFERENCIALES"),
	
	DEPOSITED_FUNDS (new Long(1112), "FONDOS DEPOSITADOS"),
	COMPLAINCE_FUNDS (new Long(1113), "FONDOS CONFORMES"),
	SENT_FUNDS (new Long(1114), "FONDOS ENVIADOS"),
	
	COMPLIANCE_MARGIN (new Long(1507), "MARGEN CONFORME"),
	RETURNED_MARGIN (new Long(1508), "MARGEN DEVUELTO");
	
	
	/** The code. */
	private Long code;
	
	private String description;

	private AccountOperationReferenceType(Long code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<AccountOperationReferenceType> list = new ArrayList<AccountOperationReferenceType>();
	public static final Map<Long, AccountOperationReferenceType> lookup = new HashMap<Long, AccountOperationReferenceType>();
	static {
		for (AccountOperationReferenceType s : EnumSet.allOf(AccountOperationReferenceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
