package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * Entity implementation class for Entity: InternationalFileDetail.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/08/2013
 */
@Entity
@Table(name="INTERNATIONAL_FILE_DETAIL")
public class InternationalFileDetail implements Serializable, Auditable  {

	   
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Id international file pk. */
	@Id
	@SequenceGenerator(name = "SQ_INTERNATIONAL_FILE_PK", sequenceName = "SQ_ID_INTERNATIONAL_FILE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_INTERNATIONAL_FILE_PK")
	@Column(name = "ID_INTERNATIONAL_FILE_PK")
	private Long IdInternationalFilePk;
	
	/** The depositary operation file. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_DEPOSITARY_OPER_FILE_FK")
	private DepositaryOperationFile depositaryOperationFile;
	
	/** The international operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERNATIONAL_OPERATION_FK")
	private InternationalOperation internationalOperation;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registy date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registyDate;
    
	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/**
	 * Gets the id international file pk.
	 *
	 * @return the id international file pk
	 */
	public Long getIdInternationalFilePk() {
		return IdInternationalFilePk;
	}

	/**
	 * Sets the id international file pk.
	 *
	 * @param idInternationalFilePk the new id international file pk
	 */
	public void setIdInternationalFilePk(Long idInternationalFilePk) {
		IdInternationalFilePk = idInternationalFilePk;
	}

	/**
	 * Gets the depositary operation file.
	 *
	 * @return the depositary operation file
	 */
	public DepositaryOperationFile getDepositaryOperationFile() {
		return depositaryOperationFile;
	}

	/**
	 * Sets the depositary operation file.
	 *
	 * @param depositaryOperationFile the new depositary operation file
	 */
	public void setDepositaryOperationFile(
			DepositaryOperationFile depositaryOperationFile) {
		this.depositaryOperationFile = depositaryOperationFile;
	}

	/**
	 * Gets the international operation.
	 *
	 * @return the international operation
	 */
	public InternationalOperation getInternationalOperation() {
		return internationalOperation;
	}

	/**
	 * Sets the international operation.
	 *
	 * @param internationalOperation the new international operation
	 */
	public void setInternationalOperation(
			InternationalOperation internationalOperation) {
		this.internationalOperation = internationalOperation;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registy date.
	 *
	 * @return the registy date
	 */
	public Date getRegistyDate() {
		return registyDate;
	}

	/**
	 * Sets the registy date.
	 *
	 * @param registyDate the new registy date
	 */
	public void setRegistyDate(Date registyDate) {
		this.registyDate = registyDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Instantiates a new international file detail.
	 */
	public InternationalFileDetail() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}