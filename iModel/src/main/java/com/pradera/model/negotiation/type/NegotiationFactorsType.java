package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Enum McnCancelReasonType.
 * @author PraderaTechnologies.
 */

public enum NegotiationFactorsType {

	KAPITAL_SOC(new Integer(1509), "CAPITAL SOCIAL");
			
	/** The code. */
	private Integer code;
	/** The descripcion. */
	private String descripcion;
	/** List of McnCancelReasonType. */
	public static final List<NegotiationFactorsType> list = new ArrayList<NegotiationFactorsType>();
	
    /**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/** The Constructor 
     *  @param Long code,String descripcion
     * */
	private NegotiationFactorsType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}


	/** Adding Enum to  McnCancelReasonType List*/
	public static final Map<Integer, NegotiationFactorsType> lookup = new HashMap<Integer, NegotiationFactorsType>();
	static {
		for (NegotiationFactorsType s : EnumSet.allOf(NegotiationFactorsType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
