package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.component.type.ParameterOperationType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum NegotiationModalityType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/04/2013
 */
public enum NegotiationModalityType {

	/** The cash fixed income. */
	CASH_FIXED_INCOME (new Long(1), "CONTADO RENTA FIJA",ParameterOperationType.CASH_FIXED_INCOME.getCode(), ParameterOperationType.CHAIN_CASH_SETTLEMENT.getCode()),
	
	/** The cash equities. */
	CASH_EQUITIES (new Long(2), "CONTADO RENTA VARIABLE",ParameterOperationType.CASH_EQUITIES.getCode(), ParameterOperationType.CHAIN_CASH_SETTLEMENT.getCode()),
	
	/** The report fixed income. - REPORTO RENTA FIJA*/
	REPORT_FIXED_INCOME (new Long(3), "REPORTO RENTA FIJA",ParameterOperationType.REPORTO_FIXED_INCOME.getCode(), ParameterOperationType.CHAIN_REPO_SETTLEMENT.getCode()),
	
	/** The report equities. - REPORTO RENTA VARIABLE*/
	REPORT_EQUITIES (new Long(4), "REPORTO RENTA VARIABLE",ParameterOperationType.REPORTO_EQUITIES.getCode(), ParameterOperationType.CHAIN_REPO_SETTLEMENT.getCode()),
	
	/** The view repo fixed income. */
	VIEW_REPO_FIXED_INCOME (new Long(5), "REPO A LA VISTA RENTA FIJA",ParameterOperationType.VIEW_REPORTO_FIXED_INCOME.getCode(), new Long(0)),
	
	/** The view repo equities. */
	VIEW_REPO_EQUITIES (new Long(6), "REPO A LA VISTA RENTA VARIABLE",ParameterOperationType.VIEW_REPORTO_EQUITIES.getCode(), new Long(0)),
	
	/** The secundary repo fixed income. */
	SECUNDARY_REPO_FIXED_INCOME (new Long(7), "REPO SECUNDARIO RENTA FIJA",ParameterOperationType.SECUNDARY_REPO_FIXED_INCOME.getCode(), new Long(0)),
	
	/** The secundary repo equities. */
	SECUNDARY_REPO_EQUITIES (new Long(8), "REPO SECUNDARIO RENTA VARIABLE",ParameterOperationType.SECUNDARY_REPO_EQUITIES.getCode(), new Long(0)),
	
	/** The repurchase fixed income. */
	REPURCHASE_FIXED_INCOME (new Long(9), "RECOMPRA RENTA FIJA",ParameterOperationType.REPURCHASE_FIXED_INCOME.getCode(), new Long(0)),
	
	/** The repurchase equities. */
	REPURCHASE_EQUITIES (new Long(10), "RECOMPRA RENTA VARIABLE",ParameterOperationType.REPURCHASE_EQUITIES.getCode(), new Long(0)),
	
	/** The coupon split fixed income. */
	COUPON_SPLIT_FIXED_INCOME (new Long(11), "SPLIT DE CUPON RENTA FIJA",ParameterOperationType.COUPON_SPLIT_FIXED_INCOME.getCode(), new Long(0)),
	
	/** The primary placement fixed income. */
	PRIMARY_PLACEMENT_FIXED_INCOME (new Long(12), "COLOCACION PRIMARIA RENTA FIJA",ParameterOperationType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode(), ParameterOperationType.CHAIN_CASH_SETTLEMENT.getCode()),
	
	/** The primary placement equities. */
	PRIMARY_PLACEMENT_EQUITIES (new Long(13), "COLOCACION PRIMARIA RENTA VARIABLE",ParameterOperationType.PRIMARY_PLACEMENT_EQUITIES.getCode(), ParameterOperationType.CHAIN_CASH_SETTLEMENT.getCode()),
	
	/** The swap fixed income. */
	SWAP_FIXED_INCOME (new Long(14), "PERMUTA RENTA FIJA",ParameterOperationType.SWAP_FIXED_INCOME.getCode(), new Long(0)),
	
	/** The swap equities. */
	SWAP_EQUITIES (new Long(15), "PERMUTA RENTA VARIABLE",ParameterOperationType.SWAP_EQUITIES.getCode(), new Long(0)),
	
	/** The securities loan fixed income. - PRESTAMO DE VALORES RENTA FIJA */
	SECURITIES_LOAN_FIXED_INCOME (new Long(16), "PRESTAMO DE VALORES RENTA FIJA",ParameterOperationType.SECURITIES_LOAN_FIXED_INCOME.getCode(), new Long(0)),
	
	/** The securities loan equities. - PRESTAMO DE VALORES RENTA Variable*/
	SECURITIES_LOAN_EQUITIES (new Long(17), "PERSTAMO DE VALORES RENTA VARIABLE",ParameterOperationType.SECURITIES_LOAN_EQUITIES.getCode(), new Long(0)),
	
	/** The foward sale fixed income. */
	FOWARD_SALE_FIXED_INCOME (new Long(18), "VENTA FOWARD RENTA FIJA",ParameterOperationType.FOWARD_SALE_FIXED_INCOME.getCode(), new Long(0)),
	
	/** The foward sale equities. */
	FOWARD_SALE_EQUITIES (new Long(19), "VENTA FOWARD RENTA VARIABLE",ParameterOperationType.FOWARD_SALE_EQUITIES.getCode(), new Long(0)),
	
	/** The foward sale equities. */
	SALE_FIXED_INCOME (new Long(20), "SUBASTA RF",ParameterOperationType.SALE_FIXED_INCOME.getCode(), new Long(0)),
	
	SALE_EQUITIES (new Long(21), "SUBASTA RV",ParameterOperationType.SALE_EQUITIES.getCode(), new Long(0)),
	
	/** The repo reversal fixed income. */
	REPO_REVERSAL_FIXED_INCOME (new Long(22), "REPORTO REVERSO RENTA FIJA",ParameterOperationType.REVERT_REPORT_FIXED_INCOME.getCode(), new Long(0));
	
	/** The code. */
	private Long code;
	
	/** The descripcion. */
	private String descripcion;
	
	/** The instrument type. */
	private Long parameterOperationType;//Me permite relacionar La modalidad con el tipo de Operacion para TRADE_OPERATION

	private Long chainOperationType;
	
	/**
	 * Instantiates a new negotiation modality type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 * @param instrumentType the instrument type
	 */
	private NegotiationModalityType(Long code, String descripcion, Long parameterOperationType, Long chainOperationType) {
		this.code = code;
		this.descripcion = descripcion;
		this.parameterOperationType = parameterOperationType;
		this.chainOperationType = chainOperationType;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	
	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getChainOperationType() {
		return chainOperationType;
	}

	public void setChainOperationType(Long chainOperationType) {
		this.chainOperationType = chainOperationType;
	}

	/** The Constant list. */
	public static final List<NegotiationModalityType> list = new ArrayList<NegotiationModalityType>();
	
	/** The Constant lookup. */
	public static final Map<Long, NegotiationModalityType> lookup = new HashMap<Long, NegotiationModalityType>();
	static {
		for (NegotiationModalityType s : EnumSet.allOf(NegotiationModalityType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the parameter operation type.
	 *
	 * @return the parameter operation type
	 */
	public Long getParameterOperationType() {
		return parameterOperationType;
	}

	/**
	 * Sets the parameter operation type.
	 *
	 * @param parameterOperationType the new parameter operation type
	 */
	public void setParameterOperationType(Long parameterOperationType) {
		this.parameterOperationType = parameterOperationType;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the NegotiationModalityType
	 */
	public static NegotiationModalityType get(Long code) {
		return lookup.get(code);
	}
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<NegotiationModalityType> listSomeElements(NegotiationModalityType... transferSecuritiesTypeParams){
		List<NegotiationModalityType> retorno = new ArrayList<NegotiationModalityType>();
		for(NegotiationModalityType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
}