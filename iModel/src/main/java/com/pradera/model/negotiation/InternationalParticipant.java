package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.InternationalDepository;

/**
 * The persistent class for the INTERNATIONAL_PARTICIPANT database table.
 * 
 */
@Entity
@Table(name = "INTERNATIONAL_PARTICIPANT")
public class InternationalParticipant implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "INTERNATIONAL_PARTICIPANTPK_GENERATOR", sequenceName = "SQ_ID_INTER_PARTICIPANT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INTERNATIONAL_PARTICIPANTPK_GENERATOR")
	@Column(name = "ID_INTER_PARTICIPANT_PK")
	private Long idInterParticipantPk;

	@Column(name = "COUNTRY")
	private Long country;

	@Column(name = "DESCRIPTION")
	private String description;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_INTERNATIONAL_DEPOSITORY_FK")
	private InternationalDepository internationalDepository;

	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
    
	@Column(name="ACCOUNT_NUMBER")
	private Integer accountNumber;

	@Column(name = "PARTICIPANT_STATE")
	private Long state;

	
	@Transient
	private String countryDesc;

	// bi-directional many-to-one association to InternationalOperation
	// @OneToMany(mappedBy="internationalParticipant")
	// private Set<InternationalOperation> internationalOperations;

	public InternationalParticipant() {
	}

	/**
	 * @return the country
	 */
	public Long getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(Long country) {
		this.country = country;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the internationalDepository
	 */
	public InternationalDepository getInternationalDepository() {
		return internationalDepository;
	}

	/**
	 * @param internationalDepository
	 *            the internationalDepository to set
	 */
	public void setInternationalDepository(
			InternationalDepository internationalDepository) {
		this.internationalDepository = internationalDepository;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * @return the accountNumber
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Long getState() {
		return this.state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	/**
	 * @return the idInterParticipantPk
	 */
	public Long getIdInterParticipantPk() {
		return idInterParticipantPk;
	}

	/**
	 * @param idInterParticipantPk
	 *            the idInterParticipantPk to set
	 */
	public void setIdInterParticipantPk(Long idInterParticipantPk) {
		this.idInterParticipantPk = idInterParticipantPk;
	}


	

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the countryDesc
	 */
	public String getCountryDesc() {
		return countryDesc;
	}

	/**
	 * @param countryDesc
	 *            the countryDesc to set
	 */
	public void setCountryDesc(String countryDesc) {
		this.countryDesc = countryDesc;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {		 
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
}