package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the SIRTEX_OPERATION database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = TradeRequest.OPERATION_STATE, query = "SELECT new com.pradera.model.negotiation.TradeRequest(tra.idTradeRequestPk,tra.operationState) FROM TradeRequest tra WHERE tra.idTradeRequestPk = :idTradeRequest")
})
@Table(name="TRADE_REQUEST")
public class TradeRequest implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	public static final String OPERATION_STATE = "findSirtexRequest";
	
	@Id
	@SequenceGenerator(name="TRADE_REQUEST_IDTRADEREQUESTPK_GENERATOR", sequenceName="SQ_ID_TRADE_REQUEST_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRADE_REQUEST_IDTRADEREQUESTPK_GENERATOR")
	@Column(name="ID_TRADE_REQUEST_PK")
	private Long idTradeRequestPk;
	
	@OneToMany(mappedBy="tradeRequest")
	private List<TradeOperation> tradeOperationList;

	@Column(name="AMOUNT_RATE")
	private BigDecimal amountRate;	

	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	@Column(name="CASH_PRICE")
	private BigDecimal cashPrice;

	@Temporal(TemporalType.DATE)
	@Column(name="CASH_SETTLEMENT_DATE")
	private Date cashSettlementDate;

	@Column(name="CASH_SETTLEMENT_DAYS")
	private Long cashSettlementDays;

	@Column(name="EXCHANGE_RATE")
	private BigDecimal exchangeRate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BUYER_PARTICIPANT_FK")
	private Participant buyerParticipant;

	/** The mechanisn modality. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK"),
		@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK")
		})
    private MechanismModality mechanisnModality;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_PARENT_FK")
	private Security securityParent;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SELLER_PARTICIPANT_FK")
	private Participant sellerParticipant;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;

	@Temporal(TemporalType.DATE)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	@Column(name="OPERATION_STATE")
	private Integer operationState;

	@Column(name="REAL_CASH_AMOUNT")
	private BigDecimal realCashAmount;

	@Column(name="REAL_CASH_PRICE")
	private BigDecimal realCashPrice;

	@Temporal(TemporalType.DATE)
	@Column(name="REFERENCE_DATE")
	private Date referenceDate;

	@Temporal(TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="SETTLEMENT_CURRENCY")
	private Integer settlementCurrency;

	@Column(name="SETTLEMENT_SCHEME")
	private Integer settlementScheme;

	@Column(name="SETTLEMENT_TYPE")
	private Integer settlementType;

	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;

	@Column(name="TERM_AMOUNT")
	private BigDecimal termAmount;

	@Column(name="TERM_PRICE")
	private BigDecimal termPrice;

	@Temporal(TemporalType.DATE)
	@Column(name="TERM_SETTLEMENT_DATE")
	private Date termSettlementDate;

	@Column(name="TERM_SETTLEMENT_DAYS")
	private Long termSettlementDays;

	//bi-directional many-to-one association to AccountSirtexOperation
	@OneToMany(mappedBy="tradeRequest")
	private List<AccountTradeRequest> accountSirtexOperations;

	//bi-directional many-to-one association to SirtexOperationMarketfact
	@OneToMany(mappedBy="tradeRequest")
	private List<TradeMarketFact> sirtexOperationMarketfacts;
	
	@OneToMany(mappedBy="tradeRequest",fetch=FetchType.LAZY)
	private List<CouponTradeRequest> couponsTradeRequestList;
	
	@Transient
    private Long originRequest;
	
	@Transient
	private HolderAccount sellerAccount;
	
	public TradeRequest() {
	}

	public BigDecimal getAmountRate() {
		return this.amountRate;
	}

	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}

	public BigDecimal getCashAmount() {
		return this.cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public BigDecimal getCashPrice() {
		return this.cashPrice;
	}

	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	public Date getCashSettlementDate() {
		return this.cashSettlementDate;
	}

	public void setCashSettlementDate(Date cashSettlementDate) {
		this.cashSettlementDate = cashSettlementDate;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getNominalValue() {
		return this.nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public Date getOperationDate() {
		return this.operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public Integer getOperationState() {
		return this.operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public BigDecimal getRealCashAmount() {
		return this.realCashAmount;
	}

	public void setRealCashAmount(BigDecimal realCashAmount) {
		this.realCashAmount = realCashAmount;
	}

	public BigDecimal getRealCashPrice() {
		return this.realCashPrice;
	}

	public void setRealCashPrice(BigDecimal realCashPrice) {
		this.realCashPrice = realCashPrice;
	}

	public Date getReferenceDate() {
		return this.referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getSettlementCurrency() {
		return this.settlementCurrency;
	}

	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public BigDecimal getStockQuantity() {
		return this.stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public BigDecimal getTermAmount() {
		return this.termAmount;
	}

	public void setTermAmount(BigDecimal termAmount) {
		this.termAmount = termAmount;
	}

	public BigDecimal getTermPrice() {
		return this.termPrice;
	}

	public void setTermPrice(BigDecimal termPrice) {
		this.termPrice = termPrice;
	}

	public Date getTermSettlementDate() {
		return this.termSettlementDate;
	}

	public void setTermSettlementDate(Date termSettlementDate) {
		this.termSettlementDate = termSettlementDate;
	}

	public List<AccountTradeRequest> getAccountSirtexOperations() {
		return this.accountSirtexOperations;
	}

	public void setAccountSirtexOperations(List<AccountTradeRequest> accountSirtexOperations) {
		this.accountSirtexOperations = accountSirtexOperations;
	}

	public List<TradeMarketFact> getSirtexOperationMarketfacts() {
		return this.sirtexOperationMarketfacts;
	}

	public void setSirtexOperationMarketfacts(List<TradeMarketFact> sirtexOperationMarketfacts) {
		this.sirtexOperationMarketfacts = sirtexOperationMarketfacts;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Participant getBuyerParticipant() {
		return buyerParticipant;
	}

	public void setBuyerParticipant(Participant buyerParticipant) {
		this.buyerParticipant = buyerParticipant;
	}

	public MechanismModality getMechanisnModality() {
		return mechanisnModality;
	}

	public void setMechanisnModality(MechanismModality mechanisnModality) {
		this.mechanisnModality = mechanisnModality;
	}

	public Participant getSellerParticipant() {
		return sellerParticipant;
	}

	public void setSellerParticipant(Participant sellerParticipant) {
		this.sellerParticipant = sellerParticipant;
	}

	public List<CouponTradeRequest> getCouponsTradeRequestList() {
		return couponsTradeRequestList;
	}

	public void setCouponsTradeRequestList(List<CouponTradeRequest> mechanismOperationCoupons) {
		this.couponsTradeRequestList = mechanismOperationCoupons;
	}

	public Integer getSettlementScheme() {
		return settlementScheme;
	}

	public void setSettlementScheme(Integer settlementScheme) {
		this.settlementScheme = settlementScheme;
	}

	public Integer getSettlementType() {
		return settlementType;
	}

	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}

	public Long getCashSettlementDays() {
		return cashSettlementDays;
	}

	public void setCashSettlementDays(Long cashSettlementDays) {
		this.cashSettlementDays = cashSettlementDays;
	}

	public Long getTermSettlementDays() {
		return termSettlementDays;
	}

	public void setTermSettlementDays(Long termSettlementDays) {
		this.termSettlementDays = termSettlementDays;
	}

	public Long getIdTradeRequestPk() {
		return idTradeRequestPk;
	}

	public void setIdTradeRequestPk(Long idTradeRequestPk) {
		this.idTradeRequestPk = idTradeRequestPk;
	}

	public List<TradeOperation> getTradeOperationList() {
		return tradeOperationList;
	}

	public void setTradeOperationList(List<TradeOperation> tradeOperationList) {
		this.tradeOperationList = tradeOperationList;
	}

	public Long getOriginRequest() {
		return originRequest;
	}

	public void setOriginRequest(Long originRequest) {
		this.originRequest = originRequest;
	}

	public TradeRequest(Long idTradeRequestPk, Integer operationState) {
		super();
		this.idTradeRequestPk = idTradeRequestPk;
		this.operationState = operationState;
	}

	public Security getSecurityParent() {
		return securityParent;
	}

	public void setSecurityParent(Security securityParent) {
		this.securityParent = securityParent;
	}

	public HolderAccount getSellerAccount() {
		return sellerAccount;
	}

	public void setSellerAccount(HolderAccount sellerAccount) {
		this.sellerAccount = sellerAccount;
	}

}