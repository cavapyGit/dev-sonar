/**@author 
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */
package com.pradera.model.negotiation.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountOperationStatusType.
 *
 * @author PraderaTechnologies.
 *
 */
public enum InternationalOperationTransferType {
	
	/** The reception international securities. */
	RECEPTION_INTERNATIONAL_SECURITIES (new Long(30053), "RECEPCION DE VALORES INTERNACIONALES"),
	
	/** The sending international securities. */
	SENDING_INTERNATIONAL_SECURITIES (new Long(30054), "ENVIO DE VALORES INTERNACIONALES");
	
	/** The code. */
	private Long code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<InternationalOperationTransferType> list = new ArrayList<InternationalOperationTransferType>();
	
	/** The Constant lookup. */
	public static final Map<Long, InternationalOperationTransferType> lookup = new HashMap<Long, InternationalOperationTransferType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<InternationalOperationTransferType> listSomeElements(InternationalOperationTransferType... transferSecuritiesTypeParams){
		List<InternationalOperationTransferType> retorno = new ArrayList<InternationalOperationTransferType>();
		for(InternationalOperationTransferType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (InternationalOperationTransferType s : EnumSet.allOf(InternationalOperationTransferType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private InternationalOperationTransferType(Long codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static InternationalOperationTransferType get(Long codigo) {
		return lookup.get(codigo);
	}
}