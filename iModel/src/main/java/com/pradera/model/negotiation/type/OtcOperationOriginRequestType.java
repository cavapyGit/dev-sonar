package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum OtcOperationStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/04/2013
 */
public enum OtcOperationOriginRequestType {

	PURCHASE(new Long(1136),"COMPRA"),
	SALE(new Long(1137),"VENTA"),
	CRUSADE(new Long(1138),"CRUZADA");
			
	/** The code. */
	private Long code;
	
	/** The descripcion. */
	private String descripcion;

	/**
	 * Instantiates a new otc operation state type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private OtcOperationOriginRequestType(Long code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	
	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/** The Constant list. */
	public static final List<OtcOperationOriginRequestType> list = new ArrayList<OtcOperationOriginRequestType>();
	
	/** The Constant lookup. */
	public static final Map<Long, OtcOperationOriginRequestType> lookup = new HashMap<Long, OtcOperationOriginRequestType>();
	static {
		for (OtcOperationOriginRequestType s : EnumSet.allOf(OtcOperationOriginRequestType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * List some elements.
	 *
	 * @param otcOperationOriginRequestType the otc operation origin request type
	 * @return the list
	 */
	public static List<OtcOperationOriginRequestType> listSomeElements(OtcOperationOriginRequestType... otcOperationOriginRequestType){
		List<OtcOperationOriginRequestType> retorno = new ArrayList<OtcOperationOriginRequestType>();
		for(OtcOperationOriginRequestType TransferSecuritiesType: otcOperationOriginRequestType){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
}