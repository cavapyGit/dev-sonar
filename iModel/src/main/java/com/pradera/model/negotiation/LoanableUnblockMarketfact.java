package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the LOANABLE_UNBLOCK_MARKETFACT database table.
 * 
 */
@Entity
@Table(name = "LOANABLE_UNBLOCK_MARKETFACT")
public class LoanableUnblockMarketfact implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "LOANABLE_UNBLOCK_MARKETFACTPK_GENERATOR", sequenceName = "SQ_ID_LOAN_UNBLOCK_MARKET_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOANABLE_UNBLOCK_MARKETFACTPK_GENERATOR")
	@Column(name = "ID_LOAN_UNBLOCK_MARKET_PK")
	private Long idLoanableUnblockOperationPk;
	
	//bi-directional many-to-one association to Loanable Unblock Operation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOANABLE_UNBLOCK_FK",referencedColumnName="ID_LOANABLE_UNBLOCK_PK")
	private LoanableUnblockOperation loanableUnblockOperation;
	
	/** The market Date. */
	@Temporal( TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate; 
	
	/** The market rate. */
    @Column(name="MARKET_RATE")
	private BigDecimal marketRate;
    
    /** The market price. */
    @Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
    
    /** The market Quantity. */
    @Column(name="MARKET_QUANTITY")
	private BigDecimal marketQuantity;
    
    /** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	public LoanableUnblockMarketfact(){
		
	}

	public Long getIdLoanableUnblockOperationPk() {
		return idLoanableUnblockOperationPk;
	}

	public void setIdLoanableUnblockOperationPk(Long idLoanableUnblockOperationPk) {
		this.idLoanableUnblockOperationPk = idLoanableUnblockOperationPk;
	}

	public LoanableUnblockOperation getLoanableUnblockOperation() {
		return loanableUnblockOperation;
	}

	public void setLoanableUnblockOperation(
			LoanableUnblockOperation loanableUnblockOperation) {
		this.loanableUnblockOperation = loanableUnblockOperation;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getMarketQuantity() {
		return marketQuantity;
	}

	public void setMarketQuantity(BigDecimal marketQuantity) {
		this.marketQuantity = marketQuantity;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
       
        return detailsMap;
	}
}
