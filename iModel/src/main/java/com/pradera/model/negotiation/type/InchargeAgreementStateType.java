package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InchargeAgreementStateType {
	REGISTERED (Long.valueOf(2248),"REGISTRADO"),
	CANCELLED (Long.valueOf(2251),"CANCELADO"),
	CONFIRMED (Long.valueOf(2250),"CONFIRMADO"),	
	REJECTED (Long.valueOf(2249),"RECHAZADO");
	
	private Long code;
	private String value;
	
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private InchargeAgreementStateType(Long code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<InchargeAgreementStateType> list = new ArrayList<InchargeAgreementStateType>();
	public static final Map<Long, InchargeAgreementStateType> lookup = new HashMap<Long, InchargeAgreementStateType>();
	static {
        for (InchargeAgreementStateType d : InchargeAgreementStateType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }	
}
