package com.pradera.model.negotiation;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SWAP_OPERATION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/07/2013
 */
@Entity
@Table(name="SWAP_OPERATION")
public class SwapOperation implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id swap operation pk. */
	@Id
	@SequenceGenerator(name="SWAP_OPERATIONPK_GENERATOR", sequenceName="SQ_ID_SWAP_OPERATION",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SWAP_OPERATIONPK_GENERATOR")
	@Column(name="ID_SWAP_OPERATION_PK")
	private Long idSwapOperationPk;

	/** The cash amount. */
	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	/** The cash price. */
	@Column(name="CASH_PRICE")
	private BigDecimal cashPrice;

	/** The currency. */
	@Column(name="CURRENCY")
	private Integer currency;
	
	/** The cash stock reference. */
	@Column(name="CASH_STOCK_REFERENCE")
	private Long cashStockReference;
	
	/** The stock block date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="STOCK_BLOCK_DATE")
	private Date stockBlockDate;
	
	/** The stock delivery date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="STOCK_DELIVERY_DATE")
	private Date stockDeliveryDate;

	/** The securities. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

	/** The mechanism operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
	private MechanismOperation mechanismOperation;

	/** The seller participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SELLER_PARTICIPANT_FK")
	private Participant sellerParticipant;
	
	/** The buyer participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BUYER_PARTICIPANT_FK")
	private Participant buyerParticipant;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The stock quantity. */
	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	/** The stock quantity. */
	@Column(name="REAL_CASH_PRICE")
	private BigDecimal realCashPrice;
	
	/** The real cash amount. */
	@Column(name="REAL_CASH_AMOUNT")
	private BigDecimal realCashAmount;
	
	/** The holder account operations. */
	@OneToMany(mappedBy="swapOperation",cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH})
	private List<HolderAccountOperation> holderAccountOperations;
	
	/** The participant operations. */
	@OneToMany(mappedBy="mechanismOperation",cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH})
	private List<ParticipantOperation> participantOperations;
	
	/** The exchangeRate. */
	@Column(name="EXCHANGE_RATE")
	private BigDecimal exchangeRate= BigDecimal.ONE;
	
    /**
     * Instantiates a new swap operation.
     */
    public SwapOperation() {
    }
    
    public SwapOperation(Security security){
    	this.securities = security;
    }
    
    public SwapOperation(
    		Long idSwapOperationPk,
    		String idIsinCodePk,
    		String securityDescription,
    		Integer securityClass,
    		Integer securityInstrumentType,
    		String idIssuerPk,
    		String issuerBusinessName,
    		BigDecimal secCurrentNominalValue,
    		Integer securityCurrency,
    		String secCurrencyDescription,
    		BigDecimal cashAmount,
    		BigDecimal cashPrice,
    		BigDecimal stockQuantity) {
    	this.idSwapOperationPk = idSwapOperationPk;
    	this.setSecurities(new Security(idIsinCodePk));
    	this.getSecurities().setDescription(securityDescription);
    	this.getSecurities().setSecurityClass(securityClass);
    	this.getSecurities().setInstrumentType(securityInstrumentType);
    	this.getSecurities().setIssuer(new Issuer(idIssuerPk));
    	this.getSecurities().getIssuer().setBusinessName(issuerBusinessName);
    	this.getSecurities().setCurrentNominalValue(secCurrentNominalValue);
    	this.getSecurities().setCurrency(securityCurrency);
    	this.getSecurities().setCurrencyName(secCurrencyDescription);
    	this.cashAmount = cashAmount;
    	this.cashPrice = cashPrice;
    	this.stockQuantity = stockQuantity;
    }

	/**
	 * Gets the id swap operation pk.
	 *
	 * @return the id swap operation pk
	 */
	public Long getIdSwapOperationPk() {
		return this.idSwapOperationPk;
	}

	/**
	 * Sets the id swap operation pk.
	 *
	 * @param idSwapOperationPk the new id swap operation pk
	 */
	public void setIdSwapOperationPk(Long idSwapOperationPk) {
		this.idSwapOperationPk = idSwapOperationPk;
	}

	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return this.cashAmount;
	}

	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	/**
	 * Gets the cash price.
	 *
	 * @return the cash price
	 */
	public BigDecimal getCashPrice() {
		return this.cashPrice;
	}

	/**
	 * Sets the cash price.
	 *
	 * @param cashPrice the new cash price
	 */
	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the new securities
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}



	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return this.stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the cash stock reference.
	 *
	 * @return the cash stock reference
	 */
	public Long getCashStockReference() {
		return cashStockReference;
	}

	/**
	 * Sets the cash stock reference.
	 *
	 * @param cashStockReference the new cash stock reference
	 */
	public void setCashStockReference(Long cashStockReference) {
		this.cashStockReference = cashStockReference;
	}

	/**
	 * Gets the stock block date.
	 *
	 * @return the stock block date
	 */
	public Date getStockBlockDate() {
		return stockBlockDate;
	}

	/**
	 * Sets the stock block date.
	 *
	 * @param stockBlockDate the new stock block date
	 */
	public void setStockBlockDate(Date stockBlockDate) {
		this.stockBlockDate = stockBlockDate;
	}

	/**
	 * Gets the stock delivery date.
	 *
	 * @return the stock delivery date
	 */
	public Date getStockDeliveryDate() {
		return stockDeliveryDate;
	}

	/**
	 * Sets the stock delivery date.
	 *
	 * @param stockDeliveryDate the new stock delivery date
	 */
	public void setStockDeliveryDate(Date stockDeliveryDate) {
		this.stockDeliveryDate = stockDeliveryDate;
	}

	/**
	 * Gets the seller participant.
	 *
	 * @return the seller participant
	 */
	public Participant getSellerParticipant() {
		return sellerParticipant;
	}

	/**
	 * Sets the seller participant.
	 *
	 * @param sellerParticipant the new seller participant
	 */
	public void setSellerParticipant(Participant sellerParticipant) {
		this.sellerParticipant = sellerParticipant;
	}

	/**
	 * Gets the buyer participant.
	 *
	 * @return the buyer participant
	 */
	public Participant getBuyerParticipant() {
		return buyerParticipant;
	}
	/**
	 * Sets the buyer participant.
	 *
	 * @param buyerParticipant the new buyer participant
	 */
	public void setBuyerParticipant(Participant buyerParticipant) {
		this.buyerParticipant = buyerParticipant;
	}
	/**
	 * Gets the holder account operations.
	 *
	 * @return the holder account operations
	 */
	public List<HolderAccountOperation> getHolderAccountOperations() {
		return holderAccountOperations;
	}
	/**
	 * Gets the exchange rate.
	 *
	 * @return the exchange rate
	 */
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}
	/**
	 * Sets the exchange rate.
	 *
	 * @param exchangeRate the new exchange rate
	 */
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	/**
	 * Sets the holder account operations.
	 *
	 * @param holderAccountOperations the new holder account operations
	 */
	public void setHolderAccountOperations(
			List<HolderAccountOperation> holderAccountOperations) {
		this.holderAccountOperations = holderAccountOperations;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("holderAccountOperations", holderAccountOperations);
		detailsMap.put("participantOperations", participantOperations);
		return detailsMap;
	}
	/**
	 * Gets the participant operations.
	 *
	 * @return the participant operations
	 */
	public List<ParticipantOperation> getParticipantOperations() {
		return participantOperations;
	}

	/**
	 * Sets the participant operations.
	 *
	 * @param participantOperations the new participant operations
	 */
	public void setParticipantOperations(
			List<ParticipantOperation> participantOperations) {
		this.participantOperations = participantOperations;
	}
	/**
	 * Gets the real cash price.
	 *
	 * @return the real cash price
	 */
	public BigDecimal getRealCashPrice() {
		return realCashPrice;
	}
	/**
	 * Sets the real cash price.
	 *
	 * @param realCashPrice the new real cash price
	 */
	public void setRealCashPrice(BigDecimal realCashPrice) {
		this.realCashPrice = realCashPrice;
	}
	/**
	 * Gets the real cash amount.
	 *
	 * @return the real cash amount
	 */
	public BigDecimal getRealCashAmount() {
		return realCashAmount;
	}
	/**
	 * Sets the real cash amount.
	 *
	 * @param realCashAmount the new real cash amount
	 */
	public void setRealCashAmount(BigDecimal realCashAmount) {
		this.realCashAmount = realCashAmount;
	}
}