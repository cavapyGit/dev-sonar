package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the LOANABLE_UNBLOCK_OPERATION database table.
 * 
 */
@Entity
@Table(name = "LOANABLE_UNBLOCK_OPERATION")
public class LoanableUnblockOperation implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long idLoanableUnblockOperationPk;
	
	/** The trade operation. */
	@MapsId
	@OneToOne(cascade=CascadeType.PERSIST,fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOANABLE_UNBLOCK_PK")
	private TradeOperation tradeOperation;
	
	//bi-directional many-to-one association to Loanable Securities Operation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOANABLE_SECURITIES_OPER_FK",referencedColumnName="ID_LOANABLE_SECURITIES_OPER_PK")
	private LoanableSecuritiesOperation loanableSecuritiesOperation;
	
	//bi-directional many-to-one association to Loanable Securities Request
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOANABLE_REQUEST_FK",referencedColumnName="ID_LOANABLE_REQUEST_PK")
	private LoanableSecuritiesRequest loanableSecuritiesRequest;
		
	/** The current Quantity. */
    @Column(name="CURRENT_QUANTITY")
	private BigDecimal currentQuantity;
    
    /** The unblock Quantity. */
    @Column(name="UNBLOCK_QUANTITY")
	private BigDecimal unblockQuantity;
    
    /** The unblock Motive. */
    @Column(name="UNBLOCK_MOTIVE")
    private Long unblockMotive;
    
    /** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	// bi-directional many-to-one association to Security
	/** The loanable Unblock Operation. */
	@OneToMany(fetch=FetchType.LAZY ,mappedBy = "loanableUnblockOperation")
	private List<LoanableUnblockMarketfact> lstLoanableUnblockMarketfact;
		
    public LoanableUnblockOperation(){
    	
    }

	public Long getIdLoanableUnblockOperationPk() {
		return idLoanableUnblockOperationPk;
	}

	public void setIdLoanableUnblockOperationPk(Long idLoanableUnblockOperationPk) {
		this.idLoanableUnblockOperationPk = idLoanableUnblockOperationPk;
	}

	public LoanableSecuritiesOperation getLoanableSecuritiesOperation() {
		return loanableSecuritiesOperation;
	}

	public void setLoanableSecuritiesOperation(
			LoanableSecuritiesOperation loanableSecuritiesOperation) {
		this.loanableSecuritiesOperation = loanableSecuritiesOperation;
	}

	public LoanableSecuritiesRequest getLoanableSecuritiesRequest() {
		return loanableSecuritiesRequest;
	}

	public void setLoanableSecuritiesRequest(
			LoanableSecuritiesRequest loanableSecuritiesRequest) {
		this.loanableSecuritiesRequest = loanableSecuritiesRequest;
	}

	public BigDecimal getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(BigDecimal currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public BigDecimal getUnblockQuantity() {
		return unblockQuantity;
	}

	public void setUnblockQuantity(BigDecimal unblockQuantity) {
		this.unblockQuantity = unblockQuantity;
	}

	public Long getUnblockMotive() {
		return unblockMotive;
	}

	public void setUnblockMotive(Long unblockMotive) {
		this.unblockMotive = unblockMotive;
	}
	
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public List<LoanableUnblockMarketfact> getLstLoanableUnblockMarketfact() {
		return lstLoanableUnblockMarketfact;
	}

	public void setLstLoanableUnblockMarketfact(
			List<LoanableUnblockMarketfact> lstLoanableUnblockMarketfact) {
		this.lstLoanableUnblockMarketfact = lstLoanableUnblockMarketfact;
	}

	public TradeOperation getTradeOperation() {
		return tradeOperation;
	}

	public void setTradeOperation(TradeOperation tradeOperation) {
		this.tradeOperation = tradeOperation;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(tradeOperation!=null){
    			tradeOperation.setLastModifyApp(lastModifyApp);
    			tradeOperation.setLastModifyDate(lastModifyDate);
    			tradeOperation.setLastModifyIp(lastModifyIp);
    			tradeOperation.setLastModifyUser(lastModifyUser);
    		}
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
		 detailsMap.put("loanableUnblockMarketfact", lstLoanableUnblockMarketfact);
        return detailsMap;
	}
}
