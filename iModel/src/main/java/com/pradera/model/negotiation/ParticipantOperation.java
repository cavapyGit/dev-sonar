package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;


/**
 * The persistent class for the PARTICIPANT_OPERATION database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_OPERATION")
public class ParticipantOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARTICIPANT_OPERATION_GENERATOR", sequenceName="SQ_ID_PART_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_OPERATION_GENERATOR")
	@Column(name="ID_PARTICIPANT_OPERATION_PK")
	private Long idParticipantOperationPk;

	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	@Column(name="FUNDS_REFERENCE")
	private Long fundsReference;

	@Column(name="STOCK_REFERENCE")
	private Long stockReference;

	//bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="INCHARGE_STATE")
	private Integer inchargeState;

	@Column(name="IND_INCHARGE")
	private Integer indIncharge;

	@Column(name="OPERATION_PART")
	private Integer operationPart;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="PRINCIPAL_REFERENCE")
	private Long principalReference;
	
	@Column(name="MARGIN_REFERENCE")
	private Long marginReference;

	@Column(name="\"ROLE\"")
	private Integer role;

	@Column(name="DEPOSITED_AMOUNT")
	private BigDecimal depositedAmount;

	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	@Column(name="INITIAL_MARGIN_AMOUNT")
	private BigDecimal initialMarginAmount;

	@Column(name="DEPOSITED_MARGIN")
	private BigDecimal depositedMargin;
	
	//bi-directional many-to-one association to MechanismOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
	private MechanismOperation mechanismOperation;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SWAP_OPERATION_FK")
	private SwapOperation swapOperation;
    
    
    @Column(name="INCHARGE_TYPE")
	private Integer inchargeType;
    
    /** The currency. */
	@Column(name="OPERATION_CURRENCY")
	private Integer currency;
    
	
	 /** The blocked stock. */
	@Column(name="BLOCKED_STOCK")
	private BigDecimal blockedStock;

	@Column(name="IND_PAYED_BACK")
	private Integer indPayedBack;
	
	@Column(name="SETTLEMENT_CURRENCY")
	private Integer settlementCurrency;
	
	@Column(name="SETTLEMENT_AMOUNT")
	private BigDecimal settlementAmount;
	
	@Transient
	private boolean blCheck;
	
	@Transient
	private BigDecimal unpaidAmount;
	@Transient
	private BigDecimal overpaidAmount;
	
    public ParticipantOperation() {
    	indPayedBack =0;
    	initialMarginAmount= BigDecimal.ZERO;
    	depositedMargin= BigDecimal.ZERO;
    }

	public Long getIdParticipantOperationPk() {
		return this.idParticipantOperationPk;
	}

	public void setIdParticipantOperationPk(Long idParticipantOperationPk) {
		this.idParticipantOperationPk = idParticipantOperationPk;
	}

	public BigDecimal getCashAmount() {
		return this.cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Integer getInchargeState() {
		return inchargeState;
	}

	public void setInchargeState(Integer inchargeState) {
		this.inchargeState = inchargeState;
	}

	
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Long getMarginReference() {
		return marginReference;
	}

	public void setMarginReference(Long marginReference) {
		this.marginReference = marginReference;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	
	public Integer getIndIncharge() {
		return indIncharge;
	}

	public void setIndIncharge(Integer indIncharge) {
		this.indIncharge = indIncharge;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Integer getInchargeType() {
		return inchargeType;
	}

	public void setInchargeType(Integer inchargeType) {
		this.inchargeType = inchargeType;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Long getFundsReference() {
		return fundsReference;
	}

	public void setFundsReference(Long fundsReference) {
		this.fundsReference = fundsReference;
	}

	public Long getStockReference() {
		return stockReference;
	}

	public void setStockReference(Long stockReference) {
		this.stockReference = stockReference;
	}

	public Integer getOperationPart() {
		return operationPart;
	}

	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	public SwapOperation getSwapOperation() {
		return swapOperation;
	}

	public void setSwapOperation(SwapOperation swapOperation) {
		this.swapOperation = swapOperation;
	}

	public BigDecimal getBlockedStock() {
		return blockedStock;
	}

	public void setBlockedStock(BigDecimal blockedStock) {
		this.blockedStock = blockedStock;
	}

	public BigDecimal getDepositedAmount() {
		return depositedAmount;
	}

	public void setDepositedAmount(BigDecimal depositedAmount) {
		this.depositedAmount = depositedAmount;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isBlCheck() {
		return blCheck;
	}

	public void setBlCheck(boolean blCheck) {
		this.blCheck = blCheck;
	}

	public BigDecimal getUnpaidAmount() {
		return unpaidAmount;
	}

	public void setUnpaidAmount(BigDecimal unpaidAmount) {
		this.unpaidAmount = unpaidAmount;
	}

	public BigDecimal getOverpaidAmount() {
		return overpaidAmount;
	}

	public void setOverpaidAmount(BigDecimal overpaidAmount) {
		this.overpaidAmount = overpaidAmount;
	}

	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Integer getIndPayedBack() {
		return indPayedBack;
	}

	public void setIndPayedBack(Integer indPayedBack) {
		this.indPayedBack = indPayedBack;
	}

	public BigDecimal getInitialMarginAmount() {
		return initialMarginAmount;
	}

	public void setInitialMarginAmount(BigDecimal initialMarginAmount) {
		this.initialMarginAmount = initialMarginAmount;
	}

	public BigDecimal getDepositedMargin() {
		return depositedMargin;
	}

	public void setDepositedMargin(BigDecimal depositedMargin) {
		this.depositedMargin = depositedMargin;
	}

	public Long getPrincipalReference() {
		return principalReference;
	}

	public void setPrincipalReference(Long principalReference) {
		this.principalReference = principalReference;
	}

	public Integer getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(Integer settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}	
}