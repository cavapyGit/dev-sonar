package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.model.accounts.ParticipantMechanism;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the MECHANISM_MODALITY database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/02/2013
 */
@Entity
@Cacheable(true)
@Table(name="MECHANISM_MODALITY")
public class MechanismModality implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@EmbeddedId
	private MechanismModalityPK id;

	/** The ind incharge. */
	@Column(name="IND_INCHARGE")
	private Integer indIncharge;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The settlement schema. */
	@Column(name="SETTLEMENT_SCHEMA")
	private Integer settlementSchema;

	/** The settlement type. */
	@Column(name="SETTLEMENT_TYPE")
	private Integer settlementType;

	/** The state. */
	@Column(name="MECHANISM_MODALITY_STATE")
	private Integer stateMechanismModality;
	
	@Column(name="IND_SECURITIES_CURRENCY")
	private Integer indSecuritiesCurrency;
	
	/** The buyerNumber. */
	@Column(name="BUYERS_NUMBER")
	private Long buyerNumber;
	
	/** The sellerNumber. */
	@Column(name="SELLERS_NUMBER")
	private Long sellerNumber;
	
	@Column(name="IND_MASSIVE_LOAD")
	private Integer indMassiveLoad;
	
	@Column(name="IND_CASH_AUTOMATIC_LOAN")
	private Integer indAutomaticLoan;
	
	@Column(name="IND_CASH_PREPAID")
	private Integer indCashPrepaid;
	
	@Column(name="IND_TERM_PREPAID")
	private Integer indTermPrepaid;
	
	@Column(name="IND_CASH_EXTENDED")
	private Integer indCashExtended;
	
	@Column(name="IND_TERM_EXTENDED")
	private Integer indTermExtended;
	
	@Column(name="IND_RENEWAL")
	private Integer indRenewal;
	
	@Column(name="IND_CASH_CURRENCY_EXCHANGE")
	private Integer indCashCurrencyExchange;
	
	@Column(name="IND_TERM_CURRENCY_EXCHANGE")
	private Integer indTermCurrencyExchange;
	
	/** The cash settlement days. */
	@Column(name="CASH_MIN_SETTLEMENT_DAYS")
	private Integer cashMinSettlementDays;
	
	@Column(name="CASH_MAX_SETTLEMENT_DAYS")
	private Integer cashMaxSettlementDays;
	
	@Column(name="IND_CASH_CALLENDAR")
	private Integer indCashCallendar;
	
	@Column(name="IND_TERM_CALLENDAR")
	private Integer indTermCallendar;
	
	/** The term max settlement days. */
	@Column(name="TERM_MAX_SETTLEMENT_DAYS")
	private Integer termMaxSettlementDays;
	
	/** The term min settlement days. */
	@Column(name="TERM_MIN_SETTLEMENT_DAYS")
	private Integer termMinSettlementDays;
	
	@Column(name="INTERFACE_TRANSFER_CODE")
	private String interfaceTransferCode;
	
	@Column(name = "MAX_DAYS_AMP")
	private Integer maxDaysAmp;
	
	@Column(name = "MASSIVE_MODALITY_NAME")
	private String massiveModalityName;
	
	/** The participant mechanisms. */
	@OneToMany(mappedBy="mechanismModality")
	private List<ParticipantMechanism> participantMechanisms;
	
	@OneToMany(mappedBy="mechanisnModality")
	private List<MechanismOperation> mechanismOperationList;
	
	/** The modality group detail list. */
	@OneToMany(mappedBy="mechanismModality")
	private List<ModalityGroupDetail> modalityGroupDetailList;
	
	/** The negotiation mechanism. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_PK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK", insertable=false,updatable=false)
	private NegotiationMechanism negotiationMechanism;
	
	/** The negotiation modality. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MODALITY_PK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK", insertable=false,updatable=false)
	private NegotiationModality negotiationModality;
	
	@Transient
	private String settlementTypeDescription;
	
	@Transient
	private String settlementSchemaDescription;

    /**
     * The Constructor.
     */
    public MechanismModality() {
    	this.id = new MechanismModalityPK();
    }
    /**
     * Instantiates a new mechanism modality.
     *
     * @param id the id
     */
    public MechanismModality(MechanismModalityPK id) {
		super();
		this.id = id;
		if(id!=null){
			this.negotiationMechanism = new NegotiationMechanism(id.getIdNegotiationMechanismPk());
			this.negotiationModality = new NegotiationModality(id.getIdNegotiationModalityPk());
		}
	}
	/**
	 * Gets the id converted.
	 *
	 * @return the id converted
	 */
	public String getIdConverted(){
		StringBuilder sb = new StringBuilder();
		sb.append(id.getIdNegotiationMechanismPk());
		sb.append("-");
		sb.append(id.getIdNegotiationModalityPk());
		return sb.toString();
	}
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public MechanismModalityPK getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 */
	public void setId(MechanismModalityPK id) {
		this.id = id;
	}

	/**
	 * Gets the ind incharge.
	 *
	 * @return the ind incharge
	 */
	public Integer getIndIncharge() {
		return indIncharge;
	}

	/**
	 * Sets the ind incharge.
	 *
	 * @param indIncharge the ind incharge
	 */
	public void setIndIncharge(Integer indIncharge) {
		this.indIncharge = indIncharge;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

/*	*//**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 *//*
	public String getModalityName() {
		return modalityName;
	}

	*//**
	 * Sets the modality name.
	 *
	 * @param modalityName the modality name
	 *//*
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}
*/

	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlement schema
	 */
	public Integer getSettlementSchema() {
		return settlementSchema;
	}
	
	/**
	 * Gets the settlement schema description.
	 *
	 * @return the settlement schema description
	 */
	public String getSettlementSchemaDescription(){
		return settlementSchemaDescription;
	}

	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the settlement schema
	 */
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	/**
	 * Gets the settlement type.
	 *
	 * @return the settlement type
	 */
	public Integer getSettlementType() {
		return settlementType;
	}
	
	/**
	 * Gets the settlement type description.
	 *
	 * @return the settlement type description
	 */
	public String getSettlementTypeDescription() {
		return settlementTypeDescription;
	}

	/**
	 * Sets the settlement type.
	 *
	 * @param settlementType the settlement type
	 */
	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}

	

	/**
	 * Gets the state mechanism modality.
	 *
	 * @return the state mechanism modality
	 */
	public Integer getStateMechanismModality() {
		return stateMechanismModality;
	}

	/**
	 * Sets the state mechanism modality.
	 *
	 * @param stateMechanismModality the new state mechanism modality
	 */
	public void setStateMechanismModality(Integer stateMechanismModality) {
		this.stateMechanismModality = stateMechanismModality;
	}

	public Integer getIndSecuritiesCurrency() {
		return indSecuritiesCurrency;
	}

	public void setIndSecuritiesCurrency(Integer indSecuritiesCurrency) {
		this.indSecuritiesCurrency = indSecuritiesCurrency;
	}

	/**
	 * @return the buyerNumber
	 */
	public Long getBuyerNumber() {
		return buyerNumber;
	}

	/**
	 * @param buyerNumber the buyerNumber to set
	 */
	public void setBuyerNumber(Long buyerNumber) {
		this.buyerNumber = buyerNumber;
	}

	/**
	 * @return the sellerNumber
	 */
	public Long getSellerNumber() {
		return sellerNumber;
	}

	/**
	 * @param sellerNumber the sellerNumber to set
	 */
	public void setSellerNumber(Long sellerNumber) {
		this.sellerNumber = sellerNumber;
	}

	/**
	 * Gets the participant mechanisms.
	 *
	 * @return the participant mechanisms
	 */
	public List<ParticipantMechanism> getParticipantMechanisms() {
		return participantMechanisms;
	}

	/**
	 * Sets the participant mechanisms.
	 *
	 * @param participantMechanisms the participant mechanisms
	 */
	public void setParticipantMechanisms(
			List<ParticipantMechanism> participantMechanisms) {
		this.participantMechanisms = participantMechanisms;
	}

	/**
	 * Gets the negotiation mechanism.
	 *
	 * @return the negotiation mechanism
	 */
	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}

	/**
	 * Sets the negotiation mechanism.
	 *
	 * @param negotiationMechanism the negotiation mechanism
	 */
	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}
	
	
	
	/**
	 * Gets the negotiation modality.
	 *
	 * @return the negotiation modality
	 */
	public NegotiationModality getNegotiationModality() {
		return negotiationModality;
	}

	/**
	 * Sets the negotiation modality.
	 *
	 * @param negotiationModality the negotiation modality
	 */
	public void setNegotiationModality(NegotiationModality negotiationModality) {
		this.negotiationModality = negotiationModality;
	}
	/**
	 * Gets the modality group detail list.
	 *
	 * @return the modality group detail list
	 */
	public List<ModalityGroupDetail> getModalityGroupDetailList() {
		return modalityGroupDetailList;
	}
	/**
	 * Sets the modality group detail list.
	 *
	 * @param modalityGroupDetailList the new modality group detail list
	 */
	public void setModalityGroupDetailList(List<ModalityGroupDetail> modalityGroupDetailList) {
		this.modalityGroupDetailList = modalityGroupDetailList;
	}
	
	/**
	 * Gets the mechanism operation list.
	 *
	 * @return the mechanism operation list
	 */
	public List<MechanismOperation> getMechanismOperationList() {
		return mechanismOperationList;
	}
	
	/**
	 * Sets the mechanism operation list.
	 *
	 * @param mechanismOperationList the new mechanism operation list
	 */
	public void setMechanismOperationList(
			List<MechanismOperation> mechanismOperationList) {
		this.mechanismOperationList = mechanismOperationList;
	}
	public Integer getIndMassiveLoad() {
		return indMassiveLoad;
	}
	public void setIndMassiveLoad(Integer indMassiveLoad) {
		this.indMassiveLoad = indMassiveLoad;
	}
	public Integer getIndAutomaticLoan() {
		return indAutomaticLoan;
	}
	public void setIndAutomaticLoan(Integer indAutomaticLoan) {
		this.indAutomaticLoan = indAutomaticLoan;
	}
	public Integer getIndCashPrepaid() {
		return indCashPrepaid;
	}
	public void setIndCashPrepaid(Integer indCashPrepaid) {
		this.indCashPrepaid = indCashPrepaid;
	}
	public Integer getIndTermPrepaid() {
		return indTermPrepaid;
	}
	public void setIndTermPrepaid(Integer indTermPrepaid) {
		this.indTermPrepaid = indTermPrepaid;
	}
	public Integer getIndCashExtended() {
		return indCashExtended;
	}
	public void setIndCashExtended(Integer indCashExtended) {
		this.indCashExtended = indCashExtended;
	}
	public Integer getIndTermExtended() {
		return indTermExtended;
	}
	public void setIndTermExtended(Integer indTermExtended) {
		this.indTermExtended = indTermExtended;
	}
	public void setSettlementTypeDescription(String settlementTypeDescription) {
		this.settlementTypeDescription = settlementTypeDescription;
	}
	public void setSettlementSchemaDescription(String settlementSchemaDescription) {
		this.settlementSchemaDescription = settlementSchemaDescription;
	}
	public Integer getIndRenewal() {
		return indRenewal;
	}
	public void setIndRenewal(Integer indRenewal) {
		this.indRenewal = indRenewal;
	}
	public Integer getIndCashCurrencyExchange() {
		return indCashCurrencyExchange;
	}
	public void setIndCashCurrencyExchange(Integer indCashCurrencyExchange) {
		this.indCashCurrencyExchange = indCashCurrencyExchange;
	}
	public Integer getIndTermCurrencyExchange() {
		return indTermCurrencyExchange;
	}
	public void setIndTermCurrencyExchange(Integer indTermCurrencyExchange) {
		this.indTermCurrencyExchange = indTermCurrencyExchange;
	}
	public Integer getCashMinSettlementDays() {
		return cashMinSettlementDays;
	}
	public void setCashMinSettlementDays(Integer cashMinSettlementDays) {
		this.cashMinSettlementDays = cashMinSettlementDays;
	}
	public Integer getCashMaxSettlementDays() {
		return cashMaxSettlementDays;
	}
	public void setCashMaxSettlementDays(Integer cashMaxSettlementDays) {
		this.cashMaxSettlementDays = cashMaxSettlementDays;
	}
	public Integer getIndCashCallendar() {
		return indCashCallendar;
	}
	public void setIndCashCallendar(Integer indCashCallendar) {
		this.indCashCallendar = indCashCallendar;
	}
	public Integer getIndTermCallendar() {
		return indTermCallendar;
	}
	public void setIndTermCallendar(Integer indTermCallendar) {
		this.indTermCallendar = indTermCallendar;
	}
	public Integer getTermMaxSettlementDays() {
		return termMaxSettlementDays;
	}
	public void setTermMaxSettlementDays(Integer termMaxSettlementDays) {
		this.termMaxSettlementDays = termMaxSettlementDays;
	}
	public Integer getTermMinSettlementDays() {
		return termMinSettlementDays;
	}
	public void setTermMinSettlementDays(Integer termMinSettlementDays) {
		this.termMinSettlementDays = termMinSettlementDays;
	}
	public String getInterfaceTransferCode() {
		return interfaceTransferCode;
	}
	public void setInterfaceTransferCode(String interfaceTransferCode) {
		this.interfaceTransferCode = interfaceTransferCode;
	}
	/**
	 * @return the maxDaysAmp
	 */
	public Integer getMaxDaysAmp() {
		return maxDaysAmp;
	}
	/**
	 * @param maxDaysAmp the maxDaysAmp to set
	 */
	public void setMaxDaysAmp(Integer maxDaysAmp) {
		this.maxDaysAmp = maxDaysAmp;
	}
	public String getMassiveModalityName() {
		return massiveModalityName;
	}
	public void setMassiveModalityName(String massiveModalityName) {
		this.massiveModalityName = massiveModalityName;
	}
	
	
}