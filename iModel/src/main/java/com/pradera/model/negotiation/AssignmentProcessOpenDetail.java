/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "ASSIGNMENT_PROCESS_OPEN_DETAIL")
@NamedQueries({
    @NamedQuery(name = "AssignmentProcessOpenDetail.findAll", query = "SELECT a FROM AssignmentProcessOpenDetail a"),
    @NamedQuery(name = "AssignmentProcessOpenDetail.findByIdAssignmentOpenDetailPk", query = "SELECT a FROM AssignmentProcessOpenDetail a WHERE a.idAssignmentOpenDetailPk = :idAssignmentOpenDetailPk"),
    @NamedQuery(name = "AssignmentProcessOpenDetail.findByLastModifyUser", query = "SELECT a FROM AssignmentProcessOpenDetail a WHERE a.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "AssignmentProcessOpenDetail.findByLastModifyDate", query = "SELECT a FROM AssignmentProcessOpenDetail a WHERE a.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "AssignmentProcessOpenDetail.findByLastModifyIp", query = "SELECT a FROM AssignmentProcessOpenDetail a WHERE a.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "AssignmentProcessOpenDetail.findByLastModifyApp", query = "SELECT a FROM AssignmentProcessOpenDetail a WHERE a.lastModifyApp = :lastModifyApp")})
public class AssignmentProcessOpenDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_ASSIGNMENT_OPEN_DETAIL", sequenceName="SQ_ASSIGNMENT_OPEN_DETAIL", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ASSIGNMENT_OPEN_DETAIL")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ASSIGNMENT_OPEN_DETAIL_PK", nullable = false)
    private Long idAssignmentOpenDetailPk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @JoinColumn(name = "ID_ASSIGNMENT_PROCESS_OPEN_FK", referencedColumnName = "ID_ASSIGNMENT_PROCESS_OPEN_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AssignmentProcessOpen idAssignmentProcessOpenFk;
    @JoinColumn(name = "ID_ASSIGMENT_PROCESS_FK", referencedColumnName = "ID_ASSIGNMENT_PROCESS_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AssignmentProcess idAssigmentProcessFk;

    public AssignmentProcessOpenDetail() {
    }

    public AssignmentProcessOpenDetail(Long idAssignmentOpenDetailPk) {
        this.idAssignmentOpenDetailPk = idAssignmentOpenDetailPk;
    }

    public AssignmentProcessOpenDetail(Long idAssignmentOpenDetailPk, String lastModifyUser, Date lastModifyDate, String lastModifyIp, Long lastModifyApp) {
        this.idAssignmentOpenDetailPk = idAssignmentOpenDetailPk;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdAssignmentOpenDetailPk() {
        return idAssignmentOpenDetailPk;
    }

    public void setIdAssignmentOpenDetailPk(Long idAssignmentOpenDetailPk) {
        this.idAssignmentOpenDetailPk = idAssignmentOpenDetailPk;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public AssignmentProcessOpen getIdAssignmentProcessOpenFk() {
        return idAssignmentProcessOpenFk;
    }

    public void setIdAssignmentProcessOpenFk(AssignmentProcessOpen idAssignmentProcessOpenFk) {
        this.idAssignmentProcessOpenFk = idAssignmentProcessOpenFk;
    }

    public AssignmentProcess getIdAssigmentProcessFk() {
        return idAssigmentProcessFk;
    }

    public void setIdAssigmentProcessFk(AssignmentProcess idAssigmentProcessFk) {
        this.idAssigmentProcessFk = idAssigmentProcessFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAssignmentOpenDetailPk != null ? idAssignmentOpenDetailPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssignmentProcessOpenDetail)) {
            return false;
        }
        AssignmentProcessOpenDetail other = (AssignmentProcessOpenDetail) object;
        if ((this.idAssignmentOpenDetailPk == null && other.idAssignmentOpenDetailPk != null) || (this.idAssignmentOpenDetailPk != null && !this.idAssignmentOpenDetailPk.equals(other.idAssignmentOpenDetailPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.AssignmentProcessOpenDetail[ idAssignmentOpenDetailPk=" + idAssignmentOpenDetailPk + " ]";
    }
    
}
