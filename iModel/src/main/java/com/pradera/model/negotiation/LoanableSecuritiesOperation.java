package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

/**
 * The persistent class for the LOANABLE_SECURITIES_OPERATION database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name = LoanableSecuritiesOperation.LOANABLE_SECURITIES_OPERATION, query = "Select distinct loanableSecuritiesOperation From LoanableSecuritiesOperation loanableSecuritiesOperation WHERE loanableSecuritiesOperation.operationState = :state"),
	@NamedQuery(name = LoanableSecuritiesOperation.LOANABLE_SECURITIES_OPERATION_STATE, query = "SELECT new com.pradera.model.negotiation.LoanableSecuritiesOperation(I.idLoanableSecuritiesOperationPk,I.operationState) From LoanableSecuritiesOperation I WHERE I.idLoanableSecuritiesOperationPk = :idLoanableSecuritiesOperationPkParam")
})
@Entity
@Table(name = "LOANABLE_SECURITIES_OPERATION")
public class LoanableSecuritiesOperation implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	/** The Constant LOANABLE_SECURITIES_OPERATION. */
	public static final String  LOANABLE_SECURITIES_OPERATION= "LoanableSecuritiesOperation.searchAllLoanableSecuritiesOperation";
	
	/** The Constant LOANABLE_SECURITIES_OPERATION_STATE. */
	public static final String  LOANABLE_SECURITIES_OPERATION_STATE= "LoanableSecuritiesOperation.searchLoanableSecuritiesOperationState";
	
	@Id
	private Long idLoanableSecuritiesOperationPk;
	
	/** The trade operation. */
	@MapsId
	@OneToOne(cascade=CascadeType.PERSIST,fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOANABLE_SECURITIES_OPER_PK")
	private TradeOperation tradeOperation;
	
	//bi-directional many-to-one association to Loanable Securities Request
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOANABLE_REQUEST_FK",referencedColumnName="ID_LOANABLE_REQUEST_PK")
	private LoanableSecuritiesRequest loanableSecuritiesRequest;
	
	//bi-directional many-to-one association to Security
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",referencedColumnName="ID_SECURITY_CODE_PK")
	private Security security;

	//bi-directional many-to-one association to Participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;
	
    //bi-directional many-to-one association to Holder Account
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK", referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccount;
    
    /** The initial Quantity. */
    @Column(name="INITIAL_QUANTITY")
	private BigDecimal initialQuantity;
    
    /** The operation State. */
    @Column(name="OPERATION_STATE")
    private Long operationState;
    
	/** The interest Rate. */
	@Column(name="INTEREST_RATE")
	private BigDecimal interestRate;
    
	/** The initial Date. */
	@Temporal( TemporalType.DATE)
	@Column(name="INITIAL_DATE")
	private Date initialDate; 
	
	/** The expiration Date. */
	@Temporal( TemporalType.DATE)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate; 
    
	/** The validity Days. */
	@Column(name="VALIDITY_DAYS")
	private Integer validityDays;
    
	/** The current Quantity. */
	@Column(name="CURRENT_QUANTITY")
	private BigDecimal currentQuantity;
	
	/** The interest Rate. */
	@Column(name="LOANED_BALANCE")
	private BigDecimal loanedBalance;
    
	/** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	// bi-directional many-to-one association to Security
	/** The LoanableS ecurities Operation. */
	@OneToMany(fetch=FetchType.LAZY ,mappedBy = "loanableSecuritiesOperation")
	private List<LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfact;
		
	public LoanableSecuritiesOperation() {

	}

	public LoanableSecuritiesOperation(Long idLoanableSecuritiesOperationPk,
			Long operationState) {
		this.idLoanableSecuritiesOperationPk = idLoanableSecuritiesOperationPk;
		this.operationState = operationState;
	}

	public Long getIdLoanableSecuritiesOperationPk() {
		return idLoanableSecuritiesOperationPk;
	}

	public void setIdLoanableSecuritiesOperationPk(
			Long idLoanableSecuritiesOperationPk) {
		this.idLoanableSecuritiesOperationPk = idLoanableSecuritiesOperationPk;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public BigDecimal getInitialQuantity() {
		return initialQuantity;
	}

	public void setInitialQuantity(BigDecimal initialQuantity) {
		this.initialQuantity = initialQuantity;
	}

	public Long getOperationState() {
		return operationState;
	}

	public void setOperationState(Long operationState) {
		this.operationState = operationState;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getValidityDays() {
		return validityDays;
	}

	public void setValidityDays(Integer validityDays) {
		this.validityDays = validityDays;
	}

	public BigDecimal getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(BigDecimal currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
    
	public LoanableSecuritiesRequest getLoanableSecuritiesRequest() {
		return loanableSecuritiesRequest;
	}

	public void setLoanableSecuritiesRequest(
			LoanableSecuritiesRequest loanableSecuritiesRequest) {
		this.loanableSecuritiesRequest = loanableSecuritiesRequest;
	}
	
	public List<LoanableSecuritiesMarketfact> getLstLoanableSecuritiesMarketfact() {
		return lstLoanableSecuritiesMarketfact;
	}

	public void setLstLoanableSecuritiesMarketfact(
			List<LoanableSecuritiesMarketfact> lstLoanableSecuritiesMarketfact) {
		this.lstLoanableSecuritiesMarketfact = lstLoanableSecuritiesMarketfact;
	}

	public TradeOperation getTradeOperation() {
		return tradeOperation;
	}

	public void setTradeOperation(TradeOperation tradeOperation) {
		this.tradeOperation = tradeOperation;
	}

	public BigDecimal getLoanedBalance() {
		return loanedBalance;
	}

	public void setLoanedBalance(BigDecimal loanedBalance) {
		this.loanedBalance = loanedBalance;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(tradeOperation!=null){
    			tradeOperation.setLastModifyApp(lastModifyApp);
    			tradeOperation.setLastModifyDate(lastModifyDate);
    			tradeOperation.setLastModifyIp(lastModifyIp);
    			tradeOperation.setLastModifyUser(lastModifyUser);
    		}
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
		 detailsMap.put("loanableSecuritiesMarketfact", lstLoanableSecuritiesMarketfact);
        return detailsMap;
	}
}
