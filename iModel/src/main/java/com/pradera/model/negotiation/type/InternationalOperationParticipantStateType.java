/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.negotiation.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountOperationStatusType.
 *
 * @author PraderaTechnologies.
 *
 */
public enum InternationalOperationParticipantStateType {
	
	/** The REGISTERED */
	REGISTERED(new Long(1170),""),
	
	/** The REJECTED */
	BLOCKED(new Long(1171),"");
	
	
	
	/** The code. */
	private Long code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<InternationalOperationParticipantStateType> list = new ArrayList<InternationalOperationParticipantStateType>();
	
	/** The Constant lookup. */
	public static final Map<Long, InternationalOperationParticipantStateType> lookup = new HashMap<Long, InternationalOperationParticipantStateType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<InternationalOperationParticipantStateType> listSomeElements(InternationalOperationParticipantStateType... transferSecuritiesTypeParams){
		List<InternationalOperationParticipantStateType> retorno = new ArrayList<InternationalOperationParticipantStateType>();
		for(InternationalOperationParticipantStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (InternationalOperationParticipantStateType s : EnumSet.allOf(InternationalOperationParticipantStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private InternationalOperationParticipantStateType(Long codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static InternationalOperationParticipantStateType get(Long codigo) {
		return lookup.get(codigo);
	}
}