package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.settlement.SettlementAccountMarketfact;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ASSIGNMENT_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/10/2015
 */
@Entity
@Table(name="REASSIGNMENT_REQUEST_DETAIL")
public class ReassignmentRequestDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id reassignment request pk. */
	@Id
	@SequenceGenerator(name="REASSIGMENT_REQUEST_DET_GENERATOR", sequenceName="SQ_ID_REASSIGMENT_REQ_DET_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REASSIGMENT_REQUEST_DET_GENERATOR")
	@Column(name="ID_REASSIGNMENT_REQ_DETAIL_PK")
	private Long idReassignmentRequestPk;
	
	/** The holder account state. */
	@Column(name="HOLDER_ACCOUNT_STATE")
	private Integer holderAccountState;
	
	/** The holder account operation. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK")
	private HolderAccountOperation holderAccountOperation;
	
	/** The reassignment request. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_REASSIGNMENT_REQUEST_FK")
	private ReassignmentRequest reassignmentRequest;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The market date. */
	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;
	
	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	/** The market price. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	/** The market quantity. */
	@Column(name="MARKET_QUANTITY")
	private BigDecimal marketQuantity;
	
	/** The settlement account market fact. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_ACCOUNT_MARKETFACT_FK")
	private SettlementAccountMarketfact settlementAccountMarketfact;
	
	@Column(name="IND_OLD")
	private Integer indOld;
	
	/** The state description. */
	@Transient
	private String stateDescription;
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		return detailsMap;
	}

	/**
	 * Gets the id reassignment request pk.
	 *
	 * @return the id reassignment request pk
	 */
	public Long getIdReassignmentRequestPk() {
		return idReassignmentRequestPk;
	}

	/**
	 * Sets the id reassignment request pk.
	 *
	 * @param idReassignmentRequestPk the new id reassignment request pk
	 */
	public void setIdReassignmentRequestPk(Long idReassignmentRequestPk) {
		this.idReassignmentRequestPk = idReassignmentRequestPk;
	}

	/**
	 * Gets the holder account state.
	 *
	 * @return the holder account state
	 */
	public Integer getHolderAccountState() {
		return holderAccountState;
	}

	/**
	 * Sets the holder account state.
	 *
	 * @param holderAccountState the new holder account state
	 */
	public void setHolderAccountState(Integer holderAccountState) {
		this.holderAccountState = holderAccountState;
	}

	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}

	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}

	/**
	 * Gets the reassignment request.
	 *
	 * @return the reassignment request
	 */
	public ReassignmentRequest getReassignmentRequest() {
		return reassignmentRequest;
	}

	/**
	 * Sets the reassignment request.
	 *
	 * @param reassignmentRequest the new reassignment request
	 */
	public void setReassignmentRequest(ReassignmentRequest reassignmentRequest) {
		this.reassignmentRequest = reassignmentRequest;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the marketDate
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the marketDate to set
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the marketRate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the marketRate to set
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the marketPrice
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the marketPrice to set
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market quantity.
	 *
	 * @return the marketQuantity
	 */
	public BigDecimal getMarketQuantity() {
		return marketQuantity;
	}

	/**
	 * Sets the market quantity.
	 *
	 * @param marketQuantity the marketQuantity to set
	 */
	public void setMarketQuantity(BigDecimal marketQuantity) {
		this.marketQuantity = marketQuantity;
	}

	/**
	 * Gets the settlement account market fact.
	 *
	 * @return the settlement account market fact
	 */
	public SettlementAccountMarketfact getSettlementAccountMarketfact() {
		return settlementAccountMarketfact;
	}

	/**
	 * Sets the settlement account market fact.
	 *
	 * @param settlementAccountMarketfact the new settlement account market fact
	 */
	public void setSettlementAccountMarketfact(
			SettlementAccountMarketfact settlementAccountMarketfact) {
		this.settlementAccountMarketfact = settlementAccountMarketfact;
	}

	/**
	 * Indicador 0 anterior HM, 1 nuevo HM
	 * @return
	 */
	public Integer getIndOld() {
		return indOld;
	}
	
	/**
	 * Indicador 0 anterior HM, 1 nuevo HM
	 * @param indOld
	 */
	public void setIndOld(Integer indOld) {
		this.indOld = indOld;
	}
	
	
	
}