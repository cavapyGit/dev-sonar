package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.guarantees.AccountOperationValorization;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.settlement.type.OperationPartType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT_OPERATION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/09/2013
 */
@Entity
@Table(name="HOLDER_ACCOUNT_OPERATION")
public class HolderAccountOperation implements Serializable,Auditable,Cloneable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder account operation pk. */
	@Id
	@SequenceGenerator(name="HOLDER_OPERATION_GENERATOR", sequenceName="SQ_ID_HOLDER_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_OPERATION_GENERATOR")
	@Column(name="ID_HOLDER_ACCOUNT_OPERATION_PK")
	private Long idHolderAccountOperationPk;
//
	/** The cash amount. */
	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	/** The commission amount. */
	@Column(name="COMMISSION_AMOUNT")
	private BigDecimal commissionAmount;

    /** The confirm date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;

	/** The coverage amount. */
	@Column(name="COVERAGE_AMOUNT")
	private BigDecimal coverageAmount;

	/** The funds reference. */
	@Column(name="FUNDS_REFERENCE")
	private Long fundsReference;

	//bi-directional many-to-one association to inchargeFundsParticipant
    /** The incharge funds participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INCHARGE_FUNDS_PARTICIPANT")
	private Participant inchargeFundsParticipant;

    //bi-directional many-to-one association to inchargeStockParticipant
    /** The incharge stock participant. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INCHARGE_STOCK_PARTICIPANT")
	private Participant inchargeStockParticipant;

	/** The incharge state. */
	@Column(name="INCHARGE_STATE")
	private Integer inchargeState;

	/** The ind automatic asignment. */
	@Column(name="IND_AUTOMATIC_ASIGNMENT")
	private Integer indAutomaticAsignment;

	/** The ind incharge. */
	@Column(name="IND_INCHARGE")
	private Integer indIncharge;

	/** The ind unfulfilled. */
	@Column(name="IND_UNFULFILLED")
	private Integer indUnfulfilled;

	/** The ind unfulfilled. */
	@Column(name="IND_AUTOMATED_INCHARGE")
	private Integer indAutomatedIncharge;
	
	/** The initial margin amount. */
	@Column(name="INITIAL_MARGIN_AMOUNT")
	private BigDecimal initialMarginAmount;

    /** The initial margin date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="INITIAL_MARGIN_DATE")
	private Date initialMarginDate;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The margin reference. */
	@Column(name="MARGIN_REFERENCE")
	private Long marginReference;

	/** The operation part. */
	@Column(name="OPERATION_PART")
	private Integer operationPart;

    /** The register date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;
    
    @Transient
    private String registerUser;

    /** The return guarantees date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="RETURN_GUARANTEES_DATE")
	private Date returnGuaranteesDate;

	/** The role. */
	@Column(name="\"ROLE\"")
	private Integer role;

	/** The holder account state. */
	@Column(name="HOLDER_ACCOUNT_STATE")
	private Integer holderAccountState;

	/** The stock quantity. */
	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	/** The stock reference. */
	@Column(name="STOCK_REFERENCE")
	private Long stockReference;
	
	@Column(name="VALUED_AMOUNT")
	private BigDecimal valuedAmount;
	
	@Column(name="IND_ACCREDITATION")
	private Integer indAccreditation;

	//bi-directional many-to-one association to MechanismOperation
    /** The mechanism operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK", referencedColumnName="ID_MECHANISM_OPERATION_PK")
	private MechanismOperation mechanismOperation;

    /** The swap operation. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SWAP_OPERATION_FK")
	private SwapOperation swapOperation;
    
    /** The holder account. */
    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK" , referencedColumnName="ID_HOLDER_ACCOUNT_PK")
   	private HolderAccount holderAccount;
    
    //bi-directional many-to-one association to assignmentRequest
    /** The assignment request. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ASSIGNMENT_REQUEST_FK")
	private AssignmentRequest assignmentRequest;
    
    /** The ref account operation. */
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_REF_ACCOUNT_OPERATION_FK")
	private HolderAccountOperation refAccountOperation;
    
    @OneToMany(mappedBy="holderAccountOperation")
	private List<AccountOperationValorization> accountOperationValorizations;
    
    @OneToMany(mappedBy="holderAccountOperation")
	private List<AccountOperationMarketFact> accountOperationMarketFacts;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MCN_PROCESS_FILE_FK")
	private McnProcessFile mcnProcessFile;
    
    @Column(name="SETTLEMENT_AMOUNT")
    private BigDecimal settlementAmount;
    
    @Column(name="MOTIVE_REJECT")
    private Integer motiveReject;
    
    @Column(name="COMMENTS")
    private String comments;
    
    @Transient
    private String indMarginReference;
    
    @Transient
    private String marginReferenceDesc;
    
    /** The incharge state description. */
    @Transient
    private String inchargeStateDescription;
    
    /** The id isin code. */
    @Transient
    private String idIsinCode;
    
    @Transient
    private String idIsinCodeSwap;
    
    /** The selected. */
    @Transient
    private boolean isSelected;
    
    @Transient
    private String holderAccountStateDesc;
    
    @Transient
    private Integer indMultipleMarketFact = ComponentConstant.ZERO;
    
    public int getRowKey(){
    	return this.hashCode();
    }
    
    /**
     * Instantiates a new holder account operation.
     */
    public HolderAccountOperation() {
    	super();
    	indIncharge = ComponentConstant.ZERO;
    	indAutomaticAsignment = ComponentConstant.ZERO;
    	indUnfulfilled = ComponentConstant.ZERO;
    	indAccreditation = ComponentConstant.ZERO;
     }

	/**
	 * Gets the id holder account operation pk.
	 *
	 * @return the id holder account operation pk
	 */
	public Long getIdHolderAccountOperationPk() {
		return this.idHolderAccountOperationPk;
	}

	/**
	 * Sets the id holder account operation pk.
	 *
	 * @param idHolderAccountOperationPk the new id holder account operation pk
	 */
	public void setIdHolderAccountOperationPk(Long idHolderAccountOperationPk) {
		this.idHolderAccountOperationPk = idHolderAccountOperationPk;
	}

//	/**
//	 * Gets the cash amount.
//	 *
//	 * @return the cash amount
//	 */
//	public BigDecimal getCashAmount() {
//		return this.cashAmount;
//	}
//
//	/**
//	 * Sets the cash amount.
//	 *
//	 * @param cashAmount the new cash amount
//	 */
//	public void setCashAmount(BigDecimal cashAmount) {
//		this.cashAmount = cashAmount;
//	}

	/**
	 * Gets the commission amount.
	 *
	 * @return the commission amount
	 */
	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}

	/**
	 * Sets the commission amount.
	 *
	 * @param commissionAmount the new commission amount
	 */
	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the coverage amount.
	 *
	 * @return the coverage amount
	 */
	public BigDecimal getCoverageAmount() {
		return this.coverageAmount;
	}

	/**
	 * Sets the coverage amount.
	 *
	 * @param coverageAmount the new coverage amount
	 */
	public void setCoverageAmount(BigDecimal coverageAmount) {
		this.coverageAmount = coverageAmount;
	}

	
	/**
	 * Gets the funds reference.
	 *
	 * @return the funds reference
	 */
	public Long getFundsReference() {
		return fundsReference;
	}

	/**
	 * Sets the funds reference.
	 *
	 * @param fundsReference the new funds reference
	 */
	public void setFundsReference(Long fundsReference) {
		this.fundsReference = fundsReference;
	}

	/**
	 * Gets the incharge funds participant.
	 *
	 * @return the incharge funds participant
	 */
	public Participant getInchargeFundsParticipant() {
		return inchargeFundsParticipant;
	}

	/**
	 * Sets the incharge funds participant.
	 *
	 * @param inchargeFundsParticipant the new incharge funds participant
	 */
	public void setInchargeFundsParticipant(Participant inchargeFundsParticipant) {
		this.inchargeFundsParticipant = inchargeFundsParticipant;
	}

	/**
	 * Gets the incharge stock participant.
	 *
	 * @return the incharge stock participant
	 */
	public Participant getInchargeStockParticipant() {
		return inchargeStockParticipant;
	}

	/**
	 * Sets the incharge stock participant.
	 *
	 * @param inchargeStockParticipant the new incharge stock participant
	 */
	public void setInchargeStockParticipant(Participant inchargeStockParticipant) {
		this.inchargeStockParticipant = inchargeStockParticipant;
	}

	/**
	 * Gets the incharge state.
	 *
	 * @return the incharge state
	 */
	public Integer getInchargeState() {
		return inchargeState;
	}

	/**
	 * Sets the incharge state.
	 *
	 * @param inchargeState the new incharge state
	 */
	public void setInchargeState(Integer inchargeState) {
		this.inchargeState = inchargeState;
	}

	

	/**
	 * Gets the initial margin amount.
	 *
	 * @return the initial margin amount
	 */
	public BigDecimal getInitialMarginAmount() {
		return initialMarginAmount;
	}

	/**
	 * Sets the initial margin amount.
	 *
	 * @param initialMarginAmount the new initial margin amount
	 */
	public void setInitialMarginAmount(BigDecimal initialMarginAmount) {
		this.initialMarginAmount = initialMarginAmount;
	}

	/**
	 * Gets the initial margin date.
	 *
	 * @return the initial margin date
	 */
	public Date getInitialMarginDate() {
		return initialMarginDate;
	}

	/**
	 * Sets the initial margin date.
	 *
	 * @param initialMarginDate the new initial margin date
	 */
	public void setInitialMarginDate(Date initialMarginDate) {
		this.initialMarginDate = initialMarginDate;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the margin reference.
	 *
	 * @return the margin reference
	 */
	public Long getMarginReference() {
		return marginReference;
	}

	/**
	 * Sets the margin reference.
	 *
	 * @param marginReference the new margin reference
	 */
	public void setMarginReference(Long marginReference) {
		this.marginReference = marginReference;
	}

	
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the return guarantees date.
	 *
	 * @return the return guarantees date
	 */
	public Date getReturnGuaranteesDate() {
		return returnGuaranteesDate;
	}

	/**
	 * Sets the return guarantees date.
	 *
	 * @param returnGuaranteesDate the new return guarantees date
	 */
	public void setReturnGuaranteesDate(Date returnGuaranteesDate) {
		this.returnGuaranteesDate = returnGuaranteesDate;
	}

	

	/**
	 * Gets the ind automatic asignment.
	 *
	 * @return the ind automatic asignment
	 */
	public Integer getIndAutomaticAsignment() {
		return indAutomaticAsignment;
	}

	/**
	 * Sets the ind automatic asignment.
	 *
	 * @param indAutomaticAsignment the new ind automatic asignment
	 */
	public void setIndAutomaticAsignment(Integer indAutomaticAsignment) {
		this.indAutomaticAsignment = indAutomaticAsignment;
	}

	/**
	 * Gets the ind incharge.
	 *
	 * @return the ind incharge
	 */
	public Integer getIndIncharge() {
		return indIncharge;
	}

	/**
	 * Sets the ind incharge.
	 *
	 * @param indIncharge the new ind incharge
	 */
	public void setIndIncharge(Integer indIncharge) {
		this.indIncharge = indIncharge;
	}

	/**
	 * Gets the ind unfulfilled.
	 *
	 * @return the ind unfulfilled
	 */
	public Integer getIndUnfulfilled() {
		return indUnfulfilled;
	}

	/**
	 * Sets the ind unfulfilled.
	 *
	 * @param indUnfulfilled the new ind unfulfilled
	 */
	public void setIndUnfulfilled(Integer indUnfulfilled) {
		this.indUnfulfilled = indUnfulfilled;
	}

	/**
	 * Gets the operation part.
	 *
	 * @return the operation part
	 */
	public Integer getOperationPart() {
		return operationPart;
	}

	/**
	 * Sets the operation part.
	 *
	 * @param operationPart the new operation part
	 */
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}
	
	/**
	 * Gets the role position.
	 *
	 * @return the role position
	 */
	public String getRolePosition(){
		return ParticipantRoleType.get(role).getPosition();
	}
	
	/**
	 * Gets the operation part desc.
	 *
	 * @return the operation part desc
	 */
	public String getOperationPartDesc(){
		return OperationPartType.get(operationPart).getDescription();
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Gets the holder account state.
	 *
	 * @return the holder account state
	 */
	public Integer getHolderAccountState() {
		return holderAccountState;
	}

	/**
	 * Sets the holder account state.
	 *
	 * @param state the new holder account state
	 */
	public void setHolderAccountState(Integer state) {
		this.holderAccountState = state;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the stock reference.
	 *
	 * @return the stock reference
	 */
	public Long getStockReference() {
		return stockReference;
	}

	/**
	 * Sets the stock reference.
	 *
	 * @param stockReference the new stock reference
	 */
	public void setStockReference(Long stockReference) {
		this.stockReference = stockReference;
	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @return the mechanism operation
	 */
	public MechanismOperation getMechanismOperation() {
		return this.mechanismOperation;
	}

	/**
	 * Sets the mechanism operation.
	 *
	 * @param mechanismOperation the new mechanism operation
	 */
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the assignment request.
	 *
	 * @return the assignment request
	 */
	public AssignmentRequest getAssignmentRequest() {
		return assignmentRequest;
	}

	/**
	 * Sets the assignment request.
	 *
	 * @param assignmentRequest the new assignment request
	 */
	public void setAssignmentRequest(AssignmentRequest assignmentRequest) {
		this.assignmentRequest = assignmentRequest;
	}

	/**
	 * Gets the swap operation.
	 *
	 * @return the swap operation
	 */
	public SwapOperation getSwapOperation() {
		return swapOperation;
	}

	/**
	 * Sets the swap operation.
	 *
	 * @param swapOperation the new swap operation
	 */
	public void setSwapOperation(SwapOperation swapOperation) {
		this.swapOperation = swapOperation;
	}

	/**
	 * Gets the ref account operation.
	 *
	 * @return the ref account operation
	 */
	public HolderAccountOperation getRefAccountOperation() {
		return refAccountOperation;
	}

	/**
	 * Sets the ref account operation.
	 *
	 * @param refAccountOperation the new ref account operation
	 */
	public void setRefAccountOperation(HolderAccountOperation refAccountOperation) {
		this.refAccountOperation = refAccountOperation;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
        lastModifyDate = loggerUser.getAuditTime();
        lastModifyIp = loggerUser.getIpAddress();
        lastModifyUser = loggerUser.getUserName();
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the register user.
	 *
	 * @return the register user
	 */
	public String getRegisterUser() {
		return registerUser;
	}

	/**
	 * Sets the register user.
	 *
	 * @param registerUser the new register user
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}
	
	/**
	 * Gets the term amount.
	 * metodo para obtener el monto Contado, SOLO para grilla
	 * @return the term amount
	 */
	public BigDecimal getRealTermAmount(){
		if(mechanismOperation.getTermPrice()!=null)
			return stockQuantity.multiply(mechanismOperation.getTermPrice());
		return BigDecimal.ZERO;
	}

	/**
	 * Gets the checks if is selected.
	 *
	 * @return the checks if is selected
	 */
	public boolean getIsSelected() {
		return isSelected;
	}

	/**
	 * Sets the checks if is selected.
	 *
	 * @param isSelected the new checks if is selected
	 */
	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public HolderAccountOperation clone() throws CloneNotSupportedException {
        return (HolderAccountOperation) super.clone();
    }

	/**
	 * Gets the incharge state description.
	 *
	 * @return the incharge state description
	 */
	public String getInchargeStateDescription() {
		return inchargeStateDescription;
	}

	/**
	 * Sets the incharge state description.
	 *
	 * @param inchargeStateDescription the new incharge state description
	 */
	public void setInchargeStateDescription(String inchargeStateDescription) {
		this.inchargeStateDescription = inchargeStateDescription;
	}
	
	/**
	 * Gets the id isin code.
	 *
	 * @return the id isin code
	 */
	public String getIdIsinCode() {
		return idIsinCode;
	}
	
	/**
	 * Sets the id isin code.
	 *
	 * @param idIsinCode the new id isin code
	 */
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}

	/**
	 * @return the accountOperationValorizations
	 */
	public List<AccountOperationValorization> getAccountOperationValorizations() {
		return accountOperationValorizations;
	}

	/**
	 * @param accountOperationValorizations the accountOperationValorizations to set
	 */
	public void setAccountOperationValorizations(
			List<AccountOperationValorization> accountOperationValorizations) {
		this.accountOperationValorizations = accountOperationValorizations;
	}

	/**
	 * @return the valuedAmount
	 */
	public BigDecimal getValuedAmount() {
		return valuedAmount;
	}

	public String getHolderAccountStateDesc() {
		return holderAccountStateDesc;
	}

	public void setHolderAccountStateDesc(String holderAccountStateDesc) {
		this.holderAccountStateDesc = holderAccountStateDesc;
	}

	public Integer getIndAccreditation() {
		return indAccreditation;
	}

	public void setIndAccreditation(Integer indAccreditation) {
		this.indAccreditation = indAccreditation;
	}

	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public List<AccountOperationMarketFact> getAccountOperationMarketFacts() {
		return accountOperationMarketFacts;
	}

	public void setAccountOperationMarketFacts(List<AccountOperationMarketFact> holAccOpeMarketFact) {
		this.accountOperationMarketFacts = holAccOpeMarketFact;
	}

	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public McnProcessFile getMcnProcessFile() {
		return mcnProcessFile;
	}

	public void setMcnProcessFile(McnProcessFile mcnProcessFile) {
		this.mcnProcessFile = mcnProcessFile;
	}

	public Integer getMotiveReject() {
		return motiveReject;
	}

	public void setMotiveReject(Integer motiveReject) {
		this.motiveReject = motiveReject;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getIndMarginReference() {
		return indMarginReference;
	}

	public void setIndMarginReference(String indMarginReference) {
		this.indMarginReference = indMarginReference;
	}

	public String getMarginReferenceDesc() {
		return marginReferenceDesc;
	}

	public void setMarginReferenceDesc(String marginReferenceDesc) {
		this.marginReferenceDesc = marginReferenceDesc;
	}

	public Integer getIndMultipleMarketFact() {
		return indMultipleMarketFact;
	}

	/**
	 * Sets the ind multiple market fact.
	 *
	 * @param indMultipleMarketFact the new ind multiple market fact
	 */
	public void setIndMultipleMarketFact(Integer indMultipleMarketFact) {
		this.indMultipleMarketFact = indMultipleMarketFact;
	}

	public Integer getIndAutomatedIncharge() {
		return indAutomatedIncharge;
	}

	public void setIndAutomatedIncharge(Integer indAutomatedIncharge) {
		this.indAutomatedIncharge = indAutomatedIncharge;
	}
	
}