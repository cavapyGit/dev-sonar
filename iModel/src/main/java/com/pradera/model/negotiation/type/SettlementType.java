/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.negotiation.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum SettlementSchemaType.
 *
 * @author PraderaTechnologies.
 *
 */
public enum SettlementType {
	
	/** The Free of Payment settlement. */
	FOP(new Integer(1),"LIBRE DE PAGO","FOP"), //fop
	
	/** The Delivery vs Payment settlement. dvp*/
	DVP(new Integer(2),"PAGO VS ENTREGA","DVP"),
	
	/** The Delivery vs Delivery. ece*/
	ECE(new Integer(3),"ENTREGA VS ENTREGA","ECE");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	private String mnemonic;
	

	/** The Constant list. */
	public static final List<SettlementType> list = new ArrayList<SettlementType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SettlementType> lookup = new HashMap<Integer, SettlementType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<SettlementType> listSomeElements(SettlementType... transferSecuritiesTypeParams){
		List<SettlementType> retorno = new ArrayList<SettlementType>();
		for(SettlementType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (SettlementType s : EnumSet.allOf(SettlementType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private SettlementType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private SettlementType(Integer codigo, String valor, String mnemonic) {
		this.code = codigo;
		this.value = valor;
		this.mnemonic=mnemonic;
	}	
	
	
	

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static SettlementType get(Integer codigo) {
		return lookup.get(codigo);
	}
}