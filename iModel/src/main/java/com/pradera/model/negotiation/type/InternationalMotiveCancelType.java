package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Enum InternationalOperationStateType.
 * @author PraderaTechnologies.
 */
public enum InternationalMotiveCancelType {
	
	PARTICIPANT_REQUEST(new Integer(1664), "SOLICITUD PARTICIPANTE"),
	FOREIGN_CANCELATION(new Integer(1665), "CANCELACION EXTRANJERO"),
	OTHER(new Integer(1666), "OTROS");

	/** The code. */
	private Integer code;
	/** The descripcion. */
	private String descripcion;
	/** List of McnStateOperationType. */
	public static final List<InternationalMotiveCancelType> list = new ArrayList<InternationalMotiveCancelType>();
	
	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/** The Constructor 
     *  @param Integer code,String descripcion
     */
	private InternationalMotiveCancelType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}
	/** Adding Enum to  McnStateOperationType List*/
	public static final Map<Integer, InternationalMotiveCancelType> lookup = new HashMap<Integer, InternationalMotiveCancelType>();
	static {
		for (InternationalMotiveCancelType s : EnumSet.allOf(InternationalMotiveCancelType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the international operation state type
	 */
	public static InternationalMotiveCancelType get(Integer codigo) {
		return lookup.get(codigo);
	}
}