package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.funds.FundsInternationalOperation;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the INTERNATIONAL_OPERATION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/08/2013
 */
@Entity
@Table(name="INTERNATIONAL_OPERATION")
public class InternationalOperation implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id international operation pk. */
	@Id
	private Long idInternationalOperationPk;

	
	/** The trade operation. */
	@MapsId
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_INTERNATIONAL_OPERATION_PK")
	private TradeOperation tradeOperation;
	
	/** The currency. */
	@Column(name="CURRENCY")
	private Integer currency;
	
	/** The cash amount. */
	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	//bi-directional many-to-one association to holderAccount
    /** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

    //bi-directional many-to-one association to securities
    /** The securities. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

    //bi-directional many-to-one association to localParticipant
    /** The local participant. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOCAL_PARTICIPANT_FK")
	private Participant localParticipant;

    //bi-directional many-to-one association to settlementDepository
    /** The settlement depository. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SETTLEMENT_DEPOSITORY_FK")
	private InternationalDepository settlementDepository;

    //bi-directional many-to-one association to tradeDepository
    /** The trade depository. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_DEPOSITORY_FK")
	private InternationalDepository tradeDepository;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The nominal value. */
	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;

	/** The observations. */
	@Column(name="COMMENTS")
	private String observations;

    /** The operation date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	/** The operation number. */
	@Column(name="OPERATION_NUMBER")
	private Long operationNumber;

	/** The operation price. */
	@Column(name="OPERATION_PRICE")
	private BigDecimal operationPrice;

    /** The real settlement date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REAL_SETTLEMENT_DATE")
	private Date realSettlementDate;

    /** The register date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;
    
    /** The registry user. */
    @Transient
    private Date registryUser;

    /** The settlement date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="SETTLEMENT_DATE")
	private Date settlementDate;

	/** The settlement type. */
	@Column(name="SETTLEMENT_TYPE")
	private Integer settlementType;

	/** The operation state. */
	@Column(name="OPERATION_STATE")
	private Integer operationState;

	/** The stock quantity. */
	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;

	/** The transfer type. */
	@Column(name="TRANSFER_TYPE")
	private Long transferType;         
	
	/** The settlement amount. */
	@Column(name="SETTLEMENT_AMOUNT")
	private BigDecimal settlementAmount;
	
	/** The deposit amount. */
	@Column(name="DEPOSITED_AMOUNT")
	private BigDecimal depositAmount;
	
	//bi-directional many-to-one association to InternationalParticipant
    /** The international participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTER_PARTICIPANT_FK")
	private InternationalParticipant internationalParticipant;
    
	/** The funds reference. */
	@Column(name="FUNDS_REFERENCE")
	private Long fundsReference;

	/** The stock reference. */
	@Column(name="STOCK_REFERENCE")
	private Long stockReference;
	
	/** The lst funds international operation. */
	@OneToMany(mappedBy="internationalOperation")
	private List<FundsInternationalOperation> lstFundsInternationalOperation;
	
	/** The bl check. */
	@Transient
	private boolean blCheck;
	
	/** The international file detail list. */
	@OneToMany(mappedBy="internationalOperation",fetch=FetchType.LAZY)
	private List<InternationalFileDetail> internationalFileDetailList;
	
	/** The international market fact operations. */
	@OneToMany(mappedBy="internationalOperation",fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<InternationalMarketFactOperation> internationalMarketFactOperations;	
	
	/**
	 * Gets the deposit amount.
	 *
	 * @return the depositAmount
	 */
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	/**
	 * Sets the deposit amount.
	 *
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	/**
	 * Gets the settlement amount.
	 *
	 * @return the settlementAmount
	 */
	public BigDecimal getSettlementAmount() {
		return settlementAmount;
	}

	/**
	 * Sets the settlement amount.
	 *
	 * @param settlementAmount the settlementAmount to set
	 */
	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}
	
    /**
     * Instantiates a new international operation.
     */
    public InternationalOperation() {
    }
    
    public InternationalOperation(Long idInternationalOperationPk, Date settlementDate, Integer operationState){
		this.idInternationalOperationPk = idInternationalOperationPk;
		this.settlementDate = settlementDate;
		this.operationState = operationState;
	}

	/**
	 * Gets the id international operation pk.
	 *
	 * @return the id international operation pk
	 */
	public Long getIdInternationalOperationPk() {
		return this.idInternationalOperationPk;
	}

	/**
	 * Sets the id international operation pk.
	 *
	 * @param idInternationalOperationPk the new id international operation pk
	 */
	public void setIdInternationalOperationPk(Long idInternationalOperationPk) {
		this.idInternationalOperationPk = idInternationalOperationPk;
	}

	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return this.cashAmount;
	}

	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the new securities
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	/**
	 * Gets the local participant.
	 *
	 * @return the local participant
	 */
	public Participant getLocalParticipant() {
		return localParticipant;
	}

	/**
	 * Sets the local participant.
	 *
	 * @param localParticipant the new local participant
	 */
	public void setLocalParticipant(Participant localParticipant) {
		this.localParticipant = localParticipant;
	}

	/**
	 * Gets the settlement depository.
	 *
	 * @return the settlement depository
	 */
	public InternationalDepository getSettlementDepository() {
		return settlementDepository;
	}

	/**
	 * Sets the settlement depository.
	 *
	 * @param settlementDepository the new settlement depository
	 */
	public void setSettlementDepository(InternationalDepository settlementDepository) {
		this.settlementDepository = settlementDepository;
	}

	/**
	 * Gets the trade depository.
	 *
	 * @return the trade depository
	 */
	public InternationalDepository getTradeDepository() {
		return tradeDepository;
	}

	/**
	 * Sets the trade depository.
	 *
	 * @param tradeDepository the new trade depository
	 */
	public void setTradeDepository(InternationalDepository tradeDepository) {
		this.tradeDepository = tradeDepository;
	}



	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the nominal value.
	 *
	 * @return the nominal value
	 */
	public BigDecimal getNominalValue() {
		return this.nominalValue;
	}

	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the new nominal value
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return this.observations;
	}

	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return this.operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	
	/**
	 * Gets the operation price.
	 *
	 * @return the operation price
	 */
	public BigDecimal getOperationPrice() {
		return this.operationPrice;
	}

	/**
	 * Sets the operation price.
	 *
	 * @param operationPrice the new operation price
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}

	/**
	 * Gets the real settlement date.
	 *
	 * @return the real settlement date
	 */
	public Date getRealSettlementDate() {
		return this.realSettlementDate;
	}

	/**
	 * Sets the real settlement date.
	 *
	 * @param realSettlementDate the new real settlement date
	 */
	public void setRealSettlementDate(Date realSettlementDate) {
		this.realSettlementDate = realSettlementDate;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return this.registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return this.settlementDate;
	}

	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate the new settlement date
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	
	/**
	 * Gets the stock quantity.
	 *
	 * @return the stockQuantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the international participant.
	 *
	 * @return the international participant
	 */
	public InternationalParticipant getInternationalParticipant() {
		return this.internationalParticipant;
	}

	/**
	 * Sets the international participant.
	 *
	 * @param internationalParticipant the new international participant
	 */
	public void setInternationalParticipant(InternationalParticipant internationalParticipant) {
		this.internationalParticipant = internationalParticipant;
	}
	
	/**
	 * Gets the trade operation.
	 *
	 * @return the trade operation
	 */
	public TradeOperation getTradeOperation() {
		return this.tradeOperation;
	}

	/**
	 * Sets the trade operation.
	 *
	 * @param tradeOperation the new trade operation
	 */
	public void setTradeOperation(TradeOperation tradeOperation) {
		this.tradeOperation = tradeOperation;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the settlement type.
	 *
	 * @return the settlementType
	 */
	public Integer getSettlementType() {
		return settlementType;
	}

	/**
	 * Sets the settlement type.
	 *
	 * @param settlementType the settlementType to set
	 */
	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the operation state.
	 *
	 * @return the operation state
	 */
	public Integer getOperationState() {
		return operationState;
	}

	/**
	 * Sets the operation state.
	 *
	 * @param state the new operation state
	 */
	public void setOperationState(Integer state) {
		this.operationState = state;
	}

	/**
	 * Gets the transfer type.
	 *
	 * @return the transfer type
	 */
	public Long getTransferType() {
		return transferType;
	}

	/**
	 * Sets the transfer type.
	 *
	 * @param transferType the new transfer type
	 */
	public void setTransferType(Long transferType) {
		this.transferType = transferType;
	}
	
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}

	/**
	 * Checks if is bl check.
	 *
	 * @return true, if is bl check
	 */
	public boolean isBlCheck() {
		return blCheck;
	}

	/**
	 * Sets the bl check.
	 *
	 * @param blCheck the new bl check
	 */
	public void setBlCheck(boolean blCheck) {
		this.blCheck = blCheck;
	}

	/**
	 * Gets the transfer type desc.
	 *
	 * @return the transfer type desc
	 */
	public String getTransferTypeDesc() {
		if(Validations.validateIsNotNull(transferType))
			return ParameterOperationType.get(transferType).getDescription();
		else 
			return "";
	}



	/**
	 * Gets the funds reference.
	 *
	 * @return the funds reference
	 */
	public Long getFundsReference() {
		return fundsReference;
	}

	/**
	 * Sets the funds reference.
	 *
	 * @param fundsReference the new funds reference
	 */
	public void setFundsReference(Long fundsReference) {
		this.fundsReference = fundsReference;
	}

	/**
	 * Gets the stock reference.
	* *
	 * @return the stock reference
	 */
	public Long getStockReference() {
		return stockReference;
	}

	/**
	 * Sets the stock reference.
	 *
	 * @param stockReference the new stock reference
	 */
	public void setStockReference(Long stockReference) {
		this.stockReference = stockReference;
	}


	/**
	 * Gets the lst funds international operation.
	 *
	 * @return the lst funds international operation
	 */
	public List<FundsInternationalOperation> getLstFundsInternationalOperation() {
		return lstFundsInternationalOperation;
	}

	/**
	 * Sets the lst funds international operation.
	 *
	 * @param lstFundsInternationalOperation the new lst funds international operation
	 */
	public void setLstFundsInternationalOperation(
			List<FundsInternationalOperation> lstFundsInternationalOperation) {
		this.lstFundsInternationalOperation = lstFundsInternationalOperation;
	}
	

	/**
	 * Gets the international file detail list.
	 *
	 * @return the international file detail list
	 */
	public List<InternationalFileDetail> getInternationalFileDetailList() {
		return internationalFileDetailList;
	}

	/**
	 * Sets the international file detail list.
	 *
	 * @param internationalFileDetailList the new international file detail list
	 */
	public void setInternationalFileDetailList(
			List<InternationalFileDetail> internationalFileDetailList) {
		this.internationalFileDetailList = internationalFileDetailList;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public Date getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(Date registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the international market fact operations.
	 *
	 * @return the international market fact operations
	 */
	public List<InternationalMarketFactOperation> getInternationalMarketFactOperations() {
		return internationalMarketFactOperations;
	}

	/**
	 * Sets the international market fact operations.
	 *
	 * @param internationalMarketFactOperations the new international market fact operations
	 */
	public void setInternationalMarketFactOperations(
			List<InternationalMarketFactOperation> internationalMarketFactOperations) {
		this.internationalMarketFactOperations = internationalMarketFactOperations;
	}
	
	
}