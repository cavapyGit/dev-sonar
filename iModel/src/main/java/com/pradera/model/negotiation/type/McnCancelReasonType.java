package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Enum McnCancelReasonType.
 * @author PraderaTechnologies.
 */

public enum McnCancelReasonType {

	REGISTRATION_ERROR (new Integer(727), "ERROR EN REGISTRO DE OPERACION"),
	UNFULFILLMENT_ASSIGNMENT_ACCOUNTS (new Integer(728), "INCUMPLIMIENTO POR ASIGNACION DE CUENTAS"),
	UNFULFILLMENT_STOCKS (new Integer(729), "INCUMPLIMIENTO POR VALORES"),
	UNFULFILLMENT_FUNDS (new Integer(730), "INCUMPLIMIENTO POR FONDOS"),
	UNFULFILLMENT_GUARANTEES (new Integer(731), "INCUMPLIMIENTO POR GARANTIAS"),
	OTHERS (new Integer(732), "OTROS");
			
	/** The code. */
	private Integer code;
	/** The descripcion. */
	private String descripcion;
	/** List of McnCancelReasonType. */
	public static final List<McnCancelReasonType> list = new ArrayList<McnCancelReasonType>();
	
    /**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/** The Constructor 
     *  @param Long code,String descripcion
     * */
	private McnCancelReasonType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}


	/** Adding Enum to  McnCancelReasonType List*/
	public static final Map<Integer, McnCancelReasonType> lookup = new HashMap<Integer, McnCancelReasonType>();
	static {
		for (McnCancelReasonType s : EnumSet.allOf(McnCancelReasonType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
