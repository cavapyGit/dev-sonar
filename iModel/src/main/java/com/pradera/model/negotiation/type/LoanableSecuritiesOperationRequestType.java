package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum LoanableSecuritiesOperationRequestType {

	ENTRY_REQUEST_TYPE(Long.valueOf(1776),"INGRESO"),
	RETIREMENT_REQUEST_TYPE(Long.valueOf(1777),"RETIRO");
	
	
	private Long code;
	private String value;
	
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private LoanableSecuritiesOperationRequestType(Long code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<LoanableSecuritiesOperationRequestType> list = new ArrayList<LoanableSecuritiesOperationRequestType>();
	public static final Map<Long, LoanableSecuritiesOperationRequestType> lookup = new HashMap<Long, LoanableSecuritiesOperationRequestType>();
	static {
        for (LoanableSecuritiesOperationRequestType d : LoanableSecuritiesOperationRequestType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
}

