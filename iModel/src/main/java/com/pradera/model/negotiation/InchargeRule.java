package com.pradera.model.negotiation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="INCHARGE_RULE")
public class InchargeRule implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID_INCHARGE_RULE_PK")
	private Long idInchargeRulePk;
	
	@Column(name="INCHARGE_ROLE")
	private Integer inchargeRole;
	
	@Column(name="IND_TRADER_PARTICIPANT")
	private Integer indTraderParticipant;
	
	@Column(name="IND_STOCK_PARTICIPANT")
	private Integer indStockParticipant;
	
	@Column(name="IND_FUNDS_PARTICIPANT")
	private Integer indFundsParticipant;

	public Long getIdInchargeRulePk() {
		return idInchargeRulePk;
	}

	public void setIdInchargeRulePk(Long idInchargeRulePk) {
		this.idInchargeRulePk = idInchargeRulePk;
	}

	public Integer getInchargeRole() {
		return inchargeRole;
	}

	public void setInchargeRole(Integer inchargeRole) {
		this.inchargeRole = inchargeRole;
	}

	public Integer getIndTraderParticipant() {
		return indTraderParticipant;
	}

	public void setIndTraderParticipant(Integer indTraderParticipant) {
		this.indTraderParticipant = indTraderParticipant;
	}

	public Integer getIndStockParticipant() {
		return indStockParticipant;
	}

	public void setIndStockParticipant(Integer indStockParticipant) {
		this.indStockParticipant = indStockParticipant;
	}

	public Integer getIndFundsParticipant() {
		return indFundsParticipant;
	}

	public void setIndFundsParticipant(Integer indFundsParticipant) {
		this.indFundsParticipant = indFundsParticipant;
	}
}
