package com.pradera.model.negotiation.constant;

public class NegotiationConstant {
	
	public static final Integer STOCK_INCHARGE = new Integer(1);
	public static final Integer FUNDS_INCHARGE = new Integer(2);

	public static final Integer IND_INCHARGE_SAME_AS_TRADER_TYPE= new Integer(0);
	public static final Integer WORKING_DAYS = new Integer(1);
	public static final Integer CALEDAR_DAYS = new Integer(2);
	public static final Integer WORKING_DAYS_PASS_WEEKENDS = new Integer(3);

	public static final Long CLEARSTREAM = new Long(1);
	
	/* Parameters to update balance process */
	
	public static final String BBV_OPERATION_STREAM = "schema/edv/operbbv.xml";
	public static final String BBV_ASSIGNMENT_STREAM = "schema/edv/assignbbv.xml";
	public static final String MASSIVE_XML_BBV_ROOT_TAG = "ROWSET";
	public static final String MASSIVE_XML_OTC_ROOT_TAG = "ROWSET";
	
	public static final String BVRD_OPERATION_CASH_STREAM = "schema/operbvrd_cash.xml";
	public static final String BVRD_OPERATION_REPORT_STREAM = "schema/operbvrd_report.xml";
	public static final String BCRD_OPERATION_STREAM = "schema/operbcrd.xml";
	public static final String BCRD_OPERATION_RESPONSE_STREAM = "schema/operbcrd_response.xml";
	public static final String OTC_OPERATION_STREAM = "schema/operotc.xml";
	public static final String OTC_OPERATION_INCHARGE_STREAM = "schema/operotc_incharge.xml";
	public static final String ACCEPTED_ASSIGNMENT_STREAM = "schema/edvresponse/assig_response_correct.xml";
	public static final String ACCEPTED_OPERATIONS_STREAM = "schema/edvresponse/oper_response_correct.xml";
	public static final String REJECTED_OPERATIONS_STREAM = "schema/edvresponse/oper_response_incorrect.xml";
	public static final String TEMPORAL_BCRD_DIR = "/tmpBcrdFiles";
	
	
	public static final String INTERN_OPERATION_FILE = "CBL_Securities_Instructions_";
	public static final String INTERN_OPERATION_FILE_EXTENSION = ".csv";
	
	/*parameter to marketfac file*/
	public static final String MARKETFACT_ROOT_TAG = "file";
	public static final String BBV_FIXED_FILE_STREAM = "schema/marketfact/fixedbbv.xml";
	public static final String BBV_VARIABLE_FILE_STREAM = "schema/marketfact/variablebbv.xml";
	public static final String BBV_HISTORY_FILE_STREAM = "schema/marketfact/historybbv.xml";
	
	public static final String HOLDER_MASSIVE_FILE_STREAM = "schema/holder/holderMassiveFile.xml";	
	public static final String HOLDER_MASSIVE_FILE_STREAM_NATURAL = "schema/holder/holderMassiveFileNatural.xml";
	public static final String HOLDER_MASSIVE_FILE_STREAM_JURIDIC = "schema/holder/holderMassiveFileJuridic.xml";
	public static final String HOLDER_MASSIVE_FILE_STREAM_RESPONSE = "schema/holder/holderMassiveResponseFile.xml";
	public static final String HOLDER_MASSIVE_FILE_STREAM_RESPONSE_ERROR = "schema/holder/holderMassiveResponseErrorFile.xml";
}
