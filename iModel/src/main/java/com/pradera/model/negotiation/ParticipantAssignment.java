package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;


/**
 * The persistent class for the PARTICIPANT_ASSIGNMENT database table.
 * 
 */
@Entity
@Table(name="PARTICIPANT_ASSIGNMENT")
public class ParticipantAssignment implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARTICIPANT_ASSIGNMENT_GENERATOR", sequenceName="SQ_ID_PART_ASSIGNMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARTICIPANT_ASSIGNMENT_GENERATOR")
	@Column(name="ID_PARTICIPANT_ASSIGNMENT_PK")
	private Long idParticipantAssignmentPk;

	//bi-directional many-to-one association to assignmentProcess
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_ASSIGNMENT_PROCESS_FK")
	private AssignmentProcess assignmentProcess;

    //bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="ASSIGNMENT_STATE")
	private Integer assignmentState;
	 
	//bi-directional many-to-one association to AssignmentRequest
	@OneToMany(mappedBy="participantAssignment")
	private List<AssignmentRequest> assignmentRequests;
	
	
	
    public ParticipantAssignment() {
    }

	public Long getIdParticipantAssignmentPk() {
		return this.idParticipantAssignmentPk;
	}

	public void setIdParticipantAssignmentPk(Long idParticipantAssignmentPk) {
		this.idParticipantAssignmentPk = idParticipantAssignmentPk;
	}

	
	
	
	public AssignmentProcess getAssignmentProcess() {
		return assignmentProcess;
	}

	public void setAssignmentProcess(AssignmentProcess assignmentProcess) {
		this.assignmentProcess = assignmentProcess;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getAssignmentState() {
		return this.assignmentState;
	}

	public void setAssignmentState(Integer state) {
		this.assignmentState = state;
	}
	public List<AssignmentRequest> getAssignmentRequests() {
		return this.assignmentRequests;
	}

	public void setAssignmentRequests(List<AssignmentRequest> assignmentRequests) {
		this.assignmentRequests = assignmentRequests;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("assignmentRequests", assignmentRequests);
		return detailsMap;
	}
	
}