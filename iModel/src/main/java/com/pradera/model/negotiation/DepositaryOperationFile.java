package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the INTERNATIONAL_OPERATION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/08/2013
 */
@Entity
@Table(name="DEPOSITARY_OPERATION_FILE")
public class DepositaryOperationFile implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id depositary oper file pk. */
	@Id
	@SequenceGenerator(name = "SQ_DEPOSITARY_OPER_FILE_PK", sequenceName = "SQ_ID_DEPOSITARY_OPER_FILE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_DEPOSITARY_OPER_FILE_PK")
	@Column(name = "ID_DEPOSITARY_OPER_FILE_PK")
	private Long idDepositaryOperFilePk;
	
	/** The generation date. */
	@Temporal( TemporalType.DATE)
	@Column(name="GENERATION_DATE")
	private Date generationDate;
	
	/** The total operations. */
	@Column(name="FILE_NAME")
	private String fileName;
	
	/** The total operations. */
	@Column(name="TOTAL_OPERATIONS")
	private Long totalOperations;
	
	/** The depositary file. */
	@Lob()
	@Column(name="DEPOSITARY_FILE")
	@Basic(fetch=FetchType.LAZY)
	private byte[] depositaryFile;
	
    /** The international file detail list. */
    @OneToMany(mappedBy="depositaryOperationFile",cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH},fetch=FetchType.LAZY)
	private List<InternationalFileDetail> internationalFileDetailList;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
	@Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registy date. */
	@Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registyDate;
    
    /** The registry user. */
	@Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_USER")
	private Date registryUser;

	/**
	 * Gets the id depositary oper file pk.
	 *
	 * @return the id depositary oper file pk
	 */
	public Long getIdDepositaryOperFilePk() {
		return idDepositaryOperFilePk;
	}

	/**
	 * Sets the id depositary oper file pk.
	 *
	 * @param idDepositaryOperFilePk the new id depositary oper file pk
	 */
	public void setIdDepositaryOperFilePk(Long idDepositaryOperFilePk) {
		this.idDepositaryOperFilePk = idDepositaryOperFilePk;
	}

	/**
	 * Gets the generation date.
	 *
	 * @return the generation date
	 */
	public Date getGenerationDate() {
		return generationDate;
	}

	/**
	 * Sets the generation date.
	 *
	 * @param generationDate the new generation date
	 */
	public void setGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
	}

	/**
	 * Gets the total operations.
	 *
	 * @return the total operations
	 */
	public Long getTotalOperations() {
		return totalOperations;
	}

	/**
	 * Sets the total operations.
	 *
	 * @param totalOperations the new total operations
	 */
	public void setTotalOperations(Long totalOperations) {
		this.totalOperations = totalOperations;
	}

	/**
	 * Gets the depositary file.
	 *
	 * @return the depositary file
	 */
	public byte[] getDepositaryFile() {
		return depositaryFile;
	}

	/**
	 * Sets the depositary file.
	 *
	 * @param depositaryFile the new depositary file
	 */
	public void setDepositaryFile(byte[] depositaryFile) {
		this.depositaryFile = depositaryFile;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registy date.
	 *
	 * @return the registy date
	 */
	public Date getRegistyDate() {
		return registyDate;
	}

	/**
	 * Sets the registy date.
	 *
	 * @param registyDate the new registy date
	 */
	public void setRegistyDate(Date registyDate) {
		this.registyDate = registyDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public Date getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(Date registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the international file detail.
	 *
	 * @return the international file detail
	 */
	public List<InternationalFileDetail> getInternationalFileDetail() {
		return internationalFileDetailList;
	}

	/**
	 * Sets the international file detail.
	 *
	 * @param internationalFileDetail the new international file detail
	 */
	public void setInternationalFileDetail(List<InternationalFileDetail> internationalFileDetail) {
		this.internationalFileDetailList = internationalFileDetail;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("internationalFileDetailList", internationalFileDetailList);
		return detailsMap;
	}
}