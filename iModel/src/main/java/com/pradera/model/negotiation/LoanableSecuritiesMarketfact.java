package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the LOANABLE_SECURITIES_MARKETFACT database table.
 * 
 */
@Entity
@Table(name = "LOANABLE_SECURITIES_MARKETFACT")
public class LoanableSecuritiesMarketfact implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "LOANABLE_SECURITIES_MARKETFACTPK_GENERATOR", sequenceName = "SQ_ID_LOANABLE_MARKETFACT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOANABLE_SECURITIES_MARKETFACTPK_GENERATOR")
	@Column(name = "ID_LOANABLE_MARKETFACT_PK")
	private Long idLoanableSecuritiesMarketfactPk;
	
	//bi-directional many-to-one association to Loanable Securities Operation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOANABLE_SECURITIES_OPER_FK",referencedColumnName="ID_LOANABLE_SECURITIES_OPER_PK")
	private LoanableSecuritiesOperation loanableSecuritiesOperation;

	/** The market Date. */
	@Temporal( TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate; 
	
	/** The market rate. */
    @Column(name="MARKET_RATE")
	private BigDecimal marketRate;
    
    /** The market price. */
    @Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
    
    /** The last Modify App. */

    @Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="INITIAL_MARKET_QUANTITY")
	private BigDecimal initialMarketQuantity;
	
	@Column(name="CURRENT_MARKET_QUANTITY")
	private BigDecimal currentMarketQuantity;
	
	@Column(name="IND_ACTIVE")
	private Integer indActive = ComponentConstant.ONE;
    
	public LoanableSecuritiesMarketfact(){
		
	}

	public Long getIdLoanableSecuritiesMarketfactPk() {
		return idLoanableSecuritiesMarketfactPk;
	}

	public void setIdLoanableSecuritiesMarketfactPk(
			Long idLoanableSecuritiesMarketfactPk) {
		this.idLoanableSecuritiesMarketfactPk = idLoanableSecuritiesMarketfactPk;
	}

	public LoanableSecuritiesOperation getLoanableSecuritiesOperation() {
		return loanableSecuritiesOperation;
	}

	public void setLoanableSecuritiesOperation(
			LoanableSecuritiesOperation loanableSecuritiesOperation) {
		this.loanableSecuritiesOperation = loanableSecuritiesOperation;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
       
        return detailsMap;
	}

	public BigDecimal getInitialMarketQuantity() {
		return initialMarketQuantity;
	}

	public void setInitialMarketQuantity(BigDecimal initialMarketQuantity) {
		this.initialMarketQuantity = initialMarketQuantity;
	}

	public BigDecimal getCurrentMarketQuantity() {
		return currentMarketQuantity;
	}

	public void setCurrentMarketQuantity(BigDecimal currentMarketQuantity) {
		this.currentMarketQuantity = currentMarketQuantity;
	}

	public Integer getIndActive() {
		return indActive;
	}

	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}
}