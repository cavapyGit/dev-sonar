package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the SIRTEX_OPERATION_MARKETFACT database table.
 * 
 */
@Entity
@Table(name="TRADE_MARKETFACT")
public class TradeMarketFact implements Serializable,Auditable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SIRTEX_OPERATION_MARKETFACT_IDSIRTEXMARKETFACTPK_GENERATOR", sequenceName="SQ_ID_TRADE_MARKETFACT_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SIRTEX_OPERATION_MARKETFACT_IDSIRTEXMARKETFACTPK_GENERATOR")
	@Column(name="ID_TRADE_MARKETFACT_PK")
	private Long idTradeMarketfactPk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;

	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

	@Column(name="MARKET_QUANTITY")
	private BigDecimal marketQuantity;

	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;

	@Temporal(TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional many-to-one association to SirtexOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_REQUEST_FK")
	private TradeRequest tradeRequest;

	public TradeMarketFact() {
	}

	public Long getIdTradeMarketfactPk() {
		return idTradeMarketfactPk;
	}

	public void setIdTradeMarketfactPk(Long idTradeMarketfactPk) {
		this.idTradeMarketfactPk = idTradeMarketfactPk;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMarketDate() {
		return this.marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getMarketQuantity() {
		return this.marketQuantity;
	}

	public void setMarketQuantity(BigDecimal marketQuantity) {
		this.marketQuantity = marketQuantity;
	}

	public BigDecimal getMarketRate() {
		return this.marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}


	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public TradeRequest getTradeRequest() {
		return tradeRequest;
	}

	public void setTradeRequest(TradeRequest tradeRequest) {
		this.tradeRequest = tradeRequest;
	}
}