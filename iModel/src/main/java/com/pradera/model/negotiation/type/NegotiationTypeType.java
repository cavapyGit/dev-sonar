package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum OtcOperationStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/04/2013
 */
public enum NegotiationTypeType {

	/** The CROSS_NEGOTIATION. */
	CROSS_NEGOTIATION(new Long(1), "CRUZADA"),
	
	/** The NORMAL_NEGOTIATION. */
	NORMAL_NEGOTIATION(new Long(2), "NORMAL");
			
	/** The code. */
	private Long code;
	
	/** The descripcion. */
	private String descripcion;

	/**
	 * Instantiates a new otc operation state type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private NegotiationTypeType(Long code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	
	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/** The Constant list. */
	public static final List<NegotiationTypeType> list = new ArrayList<NegotiationTypeType>();
	
	/** The Constant lookup. */
	public static final Map<Long, NegotiationTypeType> lookup = new HashMap<Long, NegotiationTypeType>();
	static {
		for (NegotiationTypeType s : EnumSet.allOf(NegotiationTypeType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}