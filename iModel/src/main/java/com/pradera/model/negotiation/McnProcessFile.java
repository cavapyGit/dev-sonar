package com.pradera.model.negotiation;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.report.ReportLoggerFile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The persistent class for the MCN_PROCESS_FILE database table.
 * 
 */
@Entity
@Table(name="MCN_PROCESS_FILE")
public class McnProcessFile implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PROCESS_FILE_GENERATOR", sequenceName="SQ_ID_PROCESS_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROCESS_FILE_GENERATOR")
	@Column(name="ID_MCN_PROCESS_FILE_PK")
	private Long idMcnProcessFilePk;
	
	@Column(name="SEQUENCE_NUMBER")
	private Long sequenceNumber;

    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="ACCEPTED_FILE")
	private byte[] acceptedFile;
    
    @Lob()
    @Basic(fetch=FetchType.LAZY)
   	@Column(name="PROCESSED_FILE")
   	private byte[] processedFile;
    
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="REJECTED_FILE")
	private byte[] rejectedFile;

	@Column(name="FILE_NAME")
	private String fileName;

	@Column(name="IND_AUTOMACTIC_PROCESS")
	private Integer indAutomacticProcess;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PROCESS_DATE")
	private Date processDate;

	@Column(name="PROCESS_TYPE")
	private Integer processType;

	@Column(name="PROCESS_STATE")
	private Integer processState;
	
	@Column(name="REGISTER_USER")
	private String registerUser;

	@Column(name="ACCEPTED_OPERATIONS")
	private BigDecimal acceptedOperations;
	
	@Column(name="REJECTED_OPERATIONS")
	private BigDecimal rejectedOperations;
	
	@Column(name="TOTAL_OPERATIONS")
	private BigDecimal totalOperations;

	//bi-directional many-to-one association to MechanismOperation
	@OneToMany(mappedBy="mcnProcessFile")
	private Set<MechanismOperation> mechanismOperations;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK",referencedColumnName="ID_NEGOTIATION_MECHANISM_PK")
	private NegotiationMechanism negotiationMechanism;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;
	
	
    public McnProcessFile() {
    	
    }

	/**
	 * @return the idMcnProcessFilePk
	 */
	public Long getIdMcnProcessFilePk() {
		return idMcnProcessFilePk;
	}

	/**
	 * @param idMcnProcessFilePk the idMcnProcessFilePk to set
	 */
	public void setIdMcnProcessFilePk(Long idMcnProcessFilePk) {
		this.idMcnProcessFilePk = idMcnProcessFilePk;
	}

	/**
	 * @return the acceptedFile
	 */
	public byte[] getAcceptedFile() {
		return acceptedFile;
	}

	/**
	 * @param acceptedFile the acceptedFile to set
	 */
	public void setAcceptedFile(byte[] acceptedFile) {
		this.acceptedFile = acceptedFile;
	}

	/**
	 * @return the processedFile
	 */
	public byte[] getProcessedFile() {
		return processedFile;
	}

	/**
	 * @param processedFile the processedFile to set
	 */
	public void setProcessedFile(byte[] processedFile) {
		this.processedFile = processedFile;
	}

	/**
	 * @return the rejectedFile
	 */
	public byte[] getRejectedFile() {
		return rejectedFile;
	}

	/**
	 * @param rejectedFile the rejectedFile to set
	 */
	public void setRejectedFile(byte[] rejectedFile) {
		this.rejectedFile = rejectedFile;
	}

	/**
	 * @return the acceptedOperations
	 */
	public BigDecimal getAcceptedOperations() {
		return acceptedOperations;
	}

	/**
	 * @param acceptedOperations the acceptedOperations to set
	 */
	public void setAcceptedOperations(BigDecimal acceptedOperations) {
		this.acceptedOperations = acceptedOperations;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * @return the indAutomacticProcess
	 */
	public Integer getIndAutomacticProcess() {
		return indAutomacticProcess;
	}

	/**
	 * @param indAutomacticProcess the indAutomacticProcess to set
	 */
	public void setIndAutomacticProcess(Integer indAutomacticProcess) {
		this.indAutomacticProcess = indAutomacticProcess;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * @return the processType
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * @param processType the processType to set
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * @return the registerUser
	 */
	public String getRegisterUser() {
		return registerUser;
	}

	/**
	 * @param registerUser the registerUser to set
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	/**
	 * @return the rejectedOperations
	 */
	public BigDecimal getRejectedOperations() {
		return rejectedOperations;
	}

	/**
	 * @param rejectedOperations the rejectedOperations to set
	 */
	public void setRejectedOperations(BigDecimal rejectedOperations) {
		this.rejectedOperations = rejectedOperations;
	}

	/**
	 * @return the sequenceNumber
	 */
	public Long getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * @param sequenceNumber the sequenceNumber to set
	 */
	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	/**
	 * @return the totalOperations
	 */
	public BigDecimal getTotalOperations() {
		return totalOperations;
	}

	/**
	 * @param totalOperations the totalOperations to set
	 */
	public void setTotalOperations(BigDecimal totalOperations) {
		this.totalOperations = totalOperations;
	}

	/**
	 * @return the mechanismOperations
	 */
	public Set<MechanismOperation> getMechanismOperations() {
		return mechanismOperations;
	}

	/**
	 * @param mechanismOperations the mechanismOperations to set
	 */
	public void setMechanismOperations(Set<MechanismOperation> mechanismOperations) {
		this.mechanismOperations = mechanismOperations;
	}

	/**
	 * @return the negotiationMechanism
	 */
	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}

	/**
	 * @param negotiationMechanism the negotiationMechanism to set
	 */
	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}

	/**
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * @param participant the participant to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}
    
}