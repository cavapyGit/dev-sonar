package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum MechanismOperationStateType.
 * Enum migrated from McnOperationStateType
 * @author PraderaTechnologies.
 * @version 1.0 , 25/07/2013
 */
public enum MechanismOperationStateType {

	/** The registered state. - Registrada */
	REGISTERED_STATE (new Integer(612), "REGISTRADA"),
	
	/** The canceled state. - Cancelada  */
	CANCELED_STATE (new Integer(613), "CANCELADA CONTADO"),
	
	/** The assigned state. - Asignada */
	ASSIGNED_STATE (new Integer(614), "ASIGNADA"),
	
	/** The cash settled. - Liquidada Contado */
	CASH_SETTLED (new Integer(615), "LIQUIDADA CONTADO"),
	
	/** The term settled. - Liquidada Plazp */
	TERM_SETTLED (new Integer(616), "LIQUIDADA PLAZO"),
	
	/** The canceled term state - Cancelada Plazo */
	CANCELED_TERM_STATE (new Integer(1849), "CANCELADA PLAZO"),
	
	/** The canceled term state - Cancelada Plazo */
	PENDING_CANCEL_STATE (new Integer(2678), "PENDIENTE DE CANCELAR");
			
	/** The code. */
	private Integer code;
	
	/** The descripcion. */
	private String value;
	
	/** List of McnStateOperationType. */
	public static final List<MechanismOperationStateType> list = new ArrayList<MechanismOperationStateType>();
	

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private MechanismOperationStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** Adding Enum to  McnStateOperationType List. */
	public static final Map<Integer, MechanismOperationStateType> lookup = new HashMap<Integer, MechanismOperationStateType>();
	static {
		for (MechanismOperationStateType s : EnumSet.allOf(MechanismOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the McnStateOperationType
	 */
	public static MechanismOperationStateType get(Integer code) {
		return lookup.get(code);
	}
}