package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Enum InternationalOperationStateType.
 * @author PraderaTechnologies.
 */
public enum InternationalOperationStateType {
	
	APROBADA (new Integer(1150), "APROBADO"),
	REGISTRADA (new Integer(1148), "REGISTRADO"),
	ANULADA (new Integer(1149), "ANULADO"),
	CONFIRMADA (new Integer(1151), "CONFIRMADO"),
	RECHAZADA  (new Integer(1152), "RECHAZADO"),
	CANCELADA  (new Integer(1153), "CANCELADO"),
	LIQUIDADA (new Integer(1154), "LIQUIDADO");
			
	/** The code. */
	private Integer code;
	/** The descripcion. */
	private String descripcion;
	/** List of McnStateOperationType. */
	public static final List<InternationalOperationStateType> list = new ArrayList<InternationalOperationStateType>();
	
	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/** The Constructor 
     *  @param Long code,String descripcion
     */
	private InternationalOperationStateType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}
	/** Adding Enum to  McnStateOperationType List*/
	public static final Map<Integer, InternationalOperationStateType> lookup = new HashMap<Integer, InternationalOperationStateType>();
	static {
		for (InternationalOperationStateType s : EnumSet.allOf(InternationalOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the international operation state type
	 */
	public static InternationalOperationStateType get(Integer codigo) {
		return lookup.get(codigo);
	}
}