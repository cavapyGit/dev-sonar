package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AssignmentProcessStateType {

	NOTOPENED (new Integer(626), "NO ABIERTO"),
	OPENED (new Integer(627), "ABIERTO"),
	CLOSED (new Integer(628), "CERRADO");
			
	/** The code. */
	private Integer code;
	
	private String descripcion;

	private AssignmentProcessStateType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static final List<AssignmentProcessStateType> list = new ArrayList<AssignmentProcessStateType>();
	public static final Map<Integer, AssignmentProcessStateType> lookup = new HashMap<Integer, AssignmentProcessStateType>();
	static {
		for (AssignmentProcessStateType s : EnumSet.allOf(AssignmentProcessStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
