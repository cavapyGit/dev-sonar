package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the ASSIGNMENT_REQUEST database table.
 * 
 */
@Entity
@Table(name="ASSIGNMENT_REQUEST")
public class AssignmentRequest implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSIGMENT_REQUEST_GENERATOR", sequenceName="SQ_ID_ASSIGMENT_REQUEST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSIGMENT_REQUEST_GENERATOR")
	@Column(name="ID_ASSIGNMENT_REQUEST_PK")
	private Long idAssignmentRequestPk;

    @Temporal( TemporalType.DATE)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;

	@Column(name="CONFIRM_USER")
	private String confirmUser;

	//bi-directional many-to-one association to mechanismOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
	private MechanismOperation mechanismOperation;

    //bi-directional many-to-one association to participantAssignment
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_ASSIGNMENT_FK")
	private ParticipantAssignment participantAssignment;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REQUEST_STATE")
	private Integer requestState;
	
	@Column(name="ROLE")
	private Integer role;

	//bi-directional many-to-one association to HolderAccountOperation
	@OneToMany(mappedBy="assignmentRequest")
	private List<HolderAccountOperation> holderAccountOperations;
	
	
    public AssignmentRequest() {
    }

	public Long getIdAssignmentRequestPk() {
		return this.idAssignmentRequestPk;
	}

	public void setIdAssignmentRequestPk(Long idAssignmentRequestPk) {
		this.idAssignmentRequestPk = idAssignmentRequestPk;
	}

	public Date getConfirmDate() {
		return this.confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmUser() {
		return this.confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public ParticipantAssignment getParticipantAssignment() {
		return participantAssignment;
	}

	public void setParticipantAssignment(ParticipantAssignment participantAssignment) {
		this.participantAssignment = participantAssignment;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Integer getRequestState() {
		return this.requestState;
	}

	public void setRequestState(Integer state) {
		this.requestState = state;
	}
	
	public List<HolderAccountOperation> getHolderAccountOperations() {
		return this.holderAccountOperations;
	}

	public void setHolderAccountOperations(List<HolderAccountOperation> holderAccountOperations) {
		this.holderAccountOperations = holderAccountOperations;
	}

	/**
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(Integer role) {
		this.role = role;
	}
	

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("holderAccountOperations", holderAccountOperations);
		return detailsMap;
	}
}