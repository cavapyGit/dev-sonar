package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum HolderAccountOperationStateType {

	PENDING (new Integer(0), "PENDIENTE"),
	REGISTERED (new Integer(623), "REGISTRADA"),
	CANCELLED (new Integer(624), "ANULADA"),
	CONFIRMED (new Integer(625), "CONFIRMADA"),
	PREPAID (new Integer(1850),"ANTICIPADO"),
	MODIFIED_ACCOUNT_OPERATION_STATE (new Integer(1108), "MODIFICADA"),
	INCHARGE_ACCOUNT_OPERATION_STATE (new Integer(1212), "ENCARGADA"),
	
	REGISTERED_INCHARGE_STATE (new Integer(631), "REGISTRADA"),
	REJECTED_INCHARGE_STATE (new Integer(632), "RECHAZADA"),
	CONFIRMED_INCHARGE_STATE (new Integer(633), "CONFIRMADA");
	
	
	/** The code. */
	private Integer code;
	
	private String descripcion;

	private HolderAccountOperationStateType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
//	private String getDescripcion() {
//		return descripcion;
//	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static final List<HolderAccountOperationStateType> list = new ArrayList<HolderAccountOperationStateType>();
	public static final Map<Integer, HolderAccountOperationStateType> lookup = new HashMap<Integer, HolderAccountOperationStateType>();
	static {
		for (HolderAccountOperationStateType s : EnumSet.allOf(HolderAccountOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
