/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "ASSIGNMENT_PROCESS_OPEN_STATE")
@NamedQueries({
    @NamedQuery(name = "AssignmentProcessOpenState.findAll", query = "SELECT a FROM AssignmentProcessOpenState a"),
    @NamedQuery(name = "AssignmentProcessOpenState.findByIdAssignmentOpenStatePk", query = "SELECT a FROM AssignmentProcessOpenState a WHERE a.idAssignmentOpenStatePk = :idAssignmentOpenStatePk"),
    @NamedQuery(name = "AssignmentProcessOpenState.findByStateRequest", query = "SELECT a FROM AssignmentProcessOpenState a WHERE a.stateRequest = :stateRequest"),
    @NamedQuery(name = "AssignmentProcessOpenState.findByLastModifyUser", query = "SELECT a FROM AssignmentProcessOpenState a WHERE a.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "AssignmentProcessOpenState.findByLastModifyDate", query = "SELECT a FROM AssignmentProcessOpenState a WHERE a.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "AssignmentProcessOpenState.findByLastModifyIp", query = "SELECT a FROM AssignmentProcessOpenState a WHERE a.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "AssignmentProcessOpenState.findByLastModifyApp", query = "SELECT a FROM AssignmentProcessOpenState a WHERE a.lastModifyApp = :lastModifyApp")})
public class AssignmentProcessOpenState implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_ASSIGNMENT_OPEN_STATE", sequenceName="SQ_ASSIGNMENT_OPEN_STATE", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ASSIGNMENT_OPEN_STATE")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ASSIGNMENT_OPEN_STATE_PK", nullable = false)
    private Long idAssignmentOpenStatePk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE_REQUEST", nullable = false)
    private Integer stateRequest;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @JoinColumn(name = "ID_ASSIGNMENT_PROCESS_OPEN_FK", referencedColumnName = "ID_ASSIGNMENT_PROCESS_OPEN_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AssignmentProcessOpen idAssignmentProcessOpenFk;

    public AssignmentProcessOpenState() {
    }

    public AssignmentProcessOpenState(Long idAssignmentOpenStatePk) {
        this.idAssignmentOpenStatePk = idAssignmentOpenStatePk;
    }

    public AssignmentProcessOpenState(Long idAssignmentOpenStatePk, Integer stateRequest, String lastModifyUser, Date lastModifyDate, String lastModifyIp, Long lastModifyApp) {
        this.idAssignmentOpenStatePk = idAssignmentOpenStatePk;
        this.stateRequest = stateRequest;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdAssignmentOpenStatePk() {
        return idAssignmentOpenStatePk;
    }

    public void setIdAssignmentOpenStatePk(Long idAssignmentOpenStatePk) {
        this.idAssignmentOpenStatePk = idAssignmentOpenStatePk;
    }

    public Integer getStateRequest() {
        return stateRequest;
    }

    public void setStateRequest(Integer stateRequest) {
        this.stateRequest = stateRequest;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public AssignmentProcessOpen getIdAssignmentProcessOpenFk() {
        return idAssignmentProcessOpenFk;
    }

    public void setIdAssignmentProcessOpenFk(AssignmentProcessOpen idAssignmentProcessOpenFk) {
        this.idAssignmentProcessOpenFk = idAssignmentProcessOpenFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAssignmentOpenStatePk != null ? idAssignmentOpenStatePk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssignmentProcessOpenState)) {
            return false;
        }
        AssignmentProcessOpenState other = (AssignmentProcessOpenState) object;
        if ((this.idAssignmentOpenStatePk == null && other.idAssignmentOpenStatePk != null) || (this.idAssignmentOpenStatePk != null && !this.idAssignmentOpenStatePk.equals(other.idAssignmentOpenStatePk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.AssignmentProcessOpenState[ idAssignmentOpenStatePk=" + idAssignmentOpenStatePk + " ]";
    }
    
}
