/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.negotiation.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountOperationStatusType.
 *
 * @author PraderaTechnologies.
 *
 */
public enum ParticipantOperationStateType {
	
	/** The CONFIRM */
	CONFIRM(new Integer(1943),"CONFIRMADO"),
	
	/** The PREPAID */
	PREPAID(new Integer(1944),"ANTICIPADO"),
	
	CANCELLED(new Integer(2147),"CANCELADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<ParticipantOperationStateType> list = new ArrayList<ParticipantOperationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantOperationStateType> lookup = new HashMap<Integer, ParticipantOperationStateType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<ParticipantOperationStateType> listSomeElements(ParticipantOperationStateType... transferSecuritiesTypeParams){
		List<ParticipantOperationStateType> retorno = new ArrayList<ParticipantOperationStateType>();
		for(ParticipantOperationStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ParticipantOperationStateType s : EnumSet.allOf(ParticipantOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private ParticipantOperationStateType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static ParticipantOperationStateType get(Integer codigo) {
		return lookup.get(codigo);
	}
}