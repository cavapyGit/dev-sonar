package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;

@NamedQueries({
	@NamedQuery(name = InchargeAgreement.INCHARGE_AGREEMENT, query = "Select distinct inchargeAgreement From InchargeAgreement inchargeAgreement WHERE inchargeAgreement.agreementState = :state"),
	@NamedQuery(name = InchargeAgreement.INCHARGE_AGREEMENT_STATE, query = "SELECT new com.pradera.model.negotiation.InchargeAgreement(I.idInchargeAgreementPk,I.agreementState) From InchargeAgreement I WHERE I.idInchargeAgreementPk = :idInchargeAgreementPkParam")
})
@Entity
@Table(name = "INCHARGE_AGREEMENT")
public class InchargeAgreement implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	public static final String  INCHARGE_AGREEMENT= "InchargeAgreement.searchAllInchargeAgreement";
	
	public static final String  INCHARGE_AGREEMENT_STATE= "InchargeAgreement.searchInchargeAgreementState";
	
	@Id
	@SequenceGenerator(name = "INCHARGE_AGREEMENTPK_GENERATOR", sequenceName = "SQ_ID_INCHARGE_AGREEMENT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INCHARGE_AGREEMENTPK_GENERATOR")
	@Column(name = "ID_INCHARGE_AGREEMENT_PK")
	private Long idInchargeAgreementPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADER_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant traderParticipant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INCHARGE_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant inchargeParticipant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK")
	private NegotiationMechanism negotiationMechanism;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK")
	private NegotiationModality megotiationModality;
	
    @Column(name="AGREEMENT_STATE")
    private Long agreementState;
    
    @Column(name="IND_FUNDS_INCHARGE")
    private Integer indFundsIncharge;
    
    @Column(name="IND_STOCK_INCHARGE")
    private Integer indStockIncharge;
    
    @Column(name="IND_PURCHASE_ROLE")
    private Integer indPurchaseRole;
    
    @Column(name="IND_SALE_ROLE")
    private Integer indSaleRole;
    
    /** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTER_DATE")
	private Date registerDate;

	/** The registry user. */
	@Column(name = "REGISTER_USER")
	private String registerUser;
	
	/** The confirm date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;

	/** The confirm user. */
	@Column(name = "CONFIRM_USER")
	private String confirmUser;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REJECT_DATE")
	private Date rejectDate;

	/** The reject user. */
	@Column(name = "REJECT_USER")
	private String rejectUser;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CANCEL_DATE")
	private Date cancelDate;

	/** The reject user. */
	@Column(name = "CANCEL_USER")
	private String cancelUser;
	
	@Transient
	private String stateName;
	
	@Transient
	private String inchargeTypeName1;
	
	@Transient
	private String inchargeTypeName2;
	
	@Transient
	private String roleName1;
	
	@Transient
	private String roleName2;
	
	@Transient
	private String mnemonicTrader;
	
	@Transient
	private String mnemonicIncharge;
	
	public InchargeAgreement() {
		
	}

	public InchargeAgreement(Long idInchargeAgreementPk, Long agreementState) {
		this.idInchargeAgreementPk = idInchargeAgreementPk;
		this.agreementState = agreementState;
	}

	public Long getIdInchargeAgreementPk() {
		return idInchargeAgreementPk;
	}

	public void setIdInchargeAgreementPk(Long idInchargeAgreementPk) {
		this.idInchargeAgreementPk = idInchargeAgreementPk;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public Participant getTraderParticipant() {
		return traderParticipant;
	}

	public void setTraderParticipant(Participant traderParticipant) {
		this.traderParticipant = traderParticipant;
	}

	public Participant getInchargeParticipant() {
		return inchargeParticipant;
	}

	public void setInchargeParticipant(Participant inchargeParticipant) {
		this.inchargeParticipant = inchargeParticipant;
	}

	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}

	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}

	public NegotiationModality getMegotiationModality() {
		return megotiationModality;
	}

	public void setMegotiationModality(NegotiationModality megotiationModality) {
		this.megotiationModality = megotiationModality;
	}

	public Long getAgreementState() {
		return agreementState;
	}

	public void setAgreementState(Long agreementState) {
		this.agreementState = agreementState;
	}

	public Integer getIndFundsIncharge() {
		return indFundsIncharge;
	}

	public void setIndFundsIncharge(Integer indFundsIncharge) {
		this.indFundsIncharge = indFundsIncharge;
	}

	public Integer getIndStockIncharge() {
		return indStockIncharge;
	}

	public void setIndStockIncharge(Integer indStockIncharge) {
		this.indStockIncharge = indStockIncharge;
	}

	public Integer getIndPurchaseRole() {
		return indPurchaseRole;
	}

	public void setIndPurchaseRole(Integer indPurchaseRole) {
		this.indPurchaseRole = indPurchaseRole;
	}

	public Integer getIndSaleRole() {
		return indSaleRole;
	}

	public void setIndSaleRole(Integer indSaleRole) {
		this.indSaleRole = indSaleRole;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancelUser() {
		return cancelUser;
	}

	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getInchargeTypeName1() {
		return inchargeTypeName1;
	}

	public void setInchargeTypeName1(String inchargeTypeName1) {
		this.inchargeTypeName1 = inchargeTypeName1;
	}

	public String getInchargeTypeName2() {
		return inchargeTypeName2;
	}

	public void setInchargeTypeName2(String inchargeTypeName2) {
		this.inchargeTypeName2 = inchargeTypeName2;
	}

	public String getRoleName1() {
		return roleName1;
	}

	public void setRoleName1(String roleName1) {
		this.roleName1 = roleName1;
	}

	public String getRoleName2() {
		return roleName2;
	}

	public void setRoleName2(String roleName2) {
		this.roleName2 = roleName2;
	}

	public String getMnemonicTrader() {
		return mnemonicTrader;
	}

	public void setMnemonicTrader(String mnemonicTrader) {
		this.mnemonicTrader = mnemonicTrader;
	}

	public String getMnemonicIncharge() {
		return mnemonicIncharge;
	}

	public void setMnemonicIncharge(String mnemonicIncharge) {
		this.mnemonicIncharge = mnemonicIncharge;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}
}
