/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.negotiation.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantRoleType.
 *
 * @author PraderaTechnologies.
 *
 */
public enum ParticipantRoleType {
	
	/** The Gross settlement */
	BUY(new Integer(1),"COMPRA","COMPRADOR"),
	
	/** The Net settlement */
	SELL(new Integer(2),"VENTA","VENDEDOR");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The description. */
	private String position;
	
	/** The Constant list. */
	public static final List<ParticipantRoleType> list = new ArrayList<ParticipantRoleType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ParticipantRoleType> lookup = new HashMap<Integer, ParticipantRoleType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<ParticipantRoleType> listSomeElements(ParticipantRoleType... transferSecuritiesTypeParams){
		List<ParticipantRoleType> retorno = new ArrayList<ParticipantRoleType>();
		for(ParticipantRoleType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (ParticipantRoleType s : EnumSet.allOf(ParticipantRoleType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private ParticipantRoleType(Integer codigo, String valor, String position) {
		this.code = codigo;
		this.value = valor;
		this.position = position;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static ParticipantRoleType get(Integer codigo) {
		return lookup.get(codigo);
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position the new position
	 */
	public void setPosition(String position) {
		this.position = position;
	}
}