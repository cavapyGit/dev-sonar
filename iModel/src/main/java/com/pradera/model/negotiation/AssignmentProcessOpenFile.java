/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "ASSIGNMENT_PROCESS_OPEN_FILE")
@NamedQueries({
    @NamedQuery(name = "AssignmentProcessOpenFile.findAll", query = "SELECT a FROM AssignmentProcessOpenFile a"),
    @NamedQuery(name = "AssignmentProcessOpenFile.findByIdAssignmentOpenFilePk", query = "SELECT a FROM AssignmentProcessOpenFile a WHERE a.idAssignmentOpenFilePk = :idAssignmentOpenFilePk"),
    @NamedQuery(name = "AssignmentProcessOpenFile.findByFilename", query = "SELECT a FROM AssignmentProcessOpenFile a WHERE a.filename = :filename"),
    @NamedQuery(name = "AssignmentProcessOpenFile.findByRegistryDate", query = "SELECT a FROM AssignmentProcessOpenFile a WHERE a.registryDate = :registryDate"),
    @NamedQuery(name = "AssignmentProcessOpenFile.findByLastModifyUser", query = "SELECT a FROM AssignmentProcessOpenFile a WHERE a.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "AssignmentProcessOpenFile.findByLastModifyDate", query = "SELECT a FROM AssignmentProcessOpenFile a WHERE a.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "AssignmentProcessOpenFile.findByLastModifyIp", query = "SELECT a FROM AssignmentProcessOpenFile a WHERE a.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "AssignmentProcessOpenFile.findByLastModifyApp", query = "SELECT a FROM AssignmentProcessOpenFile a WHERE a.lastModifyApp = :lastModifyApp")})
public class AssignmentProcessOpenFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_ASSIGNMENT_OPEN_FILE", sequenceName="SQ_ASSIGNMENT_OPEN_FILE", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ASSIGNMENT_OPEN_FILE")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ASSIGNMENT_OPEN_FILE_PK", nullable = false)
    private Long idAssignmentOpenFilePk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "FILENAME", nullable = false, length = 150)
    private String filename;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "DOCUMENT_FILE", nullable = false)
    private byte[] documentFile;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @JoinColumn(name = "ID_ASSIGNMENT_PROCESS_OPEN_FK", referencedColumnName = "ID_ASSIGNMENT_PROCESS_OPEN_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AssignmentProcessOpen idAssignmentProcessOpenFk;
    
    /** esto es un identificador artifiacial, sirve para la adicon/retiro de archvos */
    @Transient
    private int numFileUploaded;

    public AssignmentProcessOpenFile() {
    }

    public AssignmentProcessOpenFile(Long idAssignmentOpenFilePk) {
        this.idAssignmentOpenFilePk = idAssignmentOpenFilePk;
    }

    public AssignmentProcessOpenFile(Long idAssignmentOpenFilePk, String filename, byte[] documentFile, Date registryDate, String lastModifyUser, Date lastModifyDate, String lastModifyIp, Long lastModifyApp) {
        this.idAssignmentOpenFilePk = idAssignmentOpenFilePk;
        this.filename = filename;
        this.documentFile = documentFile;
        this.registryDate = registryDate;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdAssignmentOpenFilePk() {
        return idAssignmentOpenFilePk;
    }

	public void setIdAssignmentOpenFilePk(Long idAssignmentOpenFilePk) {
        this.idAssignmentOpenFilePk = idAssignmentOpenFilePk;
    }

    public int getNumFileUploaded() {
		return numFileUploaded;
	}

	public void setNumFileUploaded(int numFileUploaded) {
		this.numFileUploaded = numFileUploaded;
	}

	public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }


    public byte[] getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public AssignmentProcessOpen getIdAssignmentProcessOpenFk() {
        return idAssignmentProcessOpenFk;
    }

    public void setIdAssignmentProcessOpenFk(AssignmentProcessOpen idAssignmentProcessOpenFk) {
        this.idAssignmentProcessOpenFk = idAssignmentProcessOpenFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAssignmentOpenFilePk != null ? idAssignmentOpenFilePk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssignmentProcessOpenFile)) {
            return false;
        }
        AssignmentProcessOpenFile other = (AssignmentProcessOpenFile) object;
        if ((this.idAssignmentOpenFilePk == null && other.idAssignmentOpenFilePk != null) || (this.idAssignmentOpenFilePk != null && !this.idAssignmentOpenFilePk.equals(other.idAssignmentOpenFilePk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.AssignmentProcessOpenFile[ idAssignmentOpenFilePk=" + idAssignmentOpenFilePk + " ]";
    }
    
}
