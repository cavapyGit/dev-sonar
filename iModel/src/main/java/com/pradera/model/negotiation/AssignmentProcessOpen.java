/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "ASSIGNMENT_PROCESS_OPEN")
@NamedQueries({
    @NamedQuery(name = "AssignmentProcessOpen.findAll", query = "SELECT a FROM AssignmentProcessOpen a"),
    @NamedQuery(name = "AssignmentProcessOpen.findByIdAssignmentProcessOpenPk", query = "SELECT a FROM AssignmentProcessOpen a WHERE a.idAssignmentProcessOpenPk = :idAssignmentProcessOpenPk"),
    @NamedQuery(name = "AssignmentProcessOpen.findByMotiveOpen", query = "SELECT a FROM AssignmentProcessOpen a WHERE a.motiveOpen = :motiveOpen"),
    @NamedQuery(name = "AssignmentProcessOpen.findByRegistryDate", query = "SELECT a FROM AssignmentProcessOpen a WHERE a.registryDate = :registryDate"),
    @NamedQuery(name = "AssignmentProcessOpen.findByStateRequest", query = "SELECT a FROM AssignmentProcessOpen a WHERE a.stateRequest = :stateRequest"),
    @NamedQuery(name = "AssignmentProcessOpen.findByLastModifyUser", query = "SELECT a FROM AssignmentProcessOpen a WHERE a.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "AssignmentProcessOpen.findByLastModifyDate", query = "SELECT a FROM AssignmentProcessOpen a WHERE a.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "AssignmentProcessOpen.findByLastModifyIp", query = "SELECT a FROM AssignmentProcessOpen a WHERE a.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "AssignmentProcessOpen.findByLastModifyApp", query = "SELECT a FROM AssignmentProcessOpen a WHERE a.lastModifyApp = :lastModifyApp")})
public class AssignmentProcessOpen implements Serializable, Cloneable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_ASSIGNMENT_PROCESS_OPEN", sequenceName="SQ_ASSIGNMENT_PROCESS_OPEN",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ASSIGNMENT_PROCESS_OPEN")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ASSIGNMENT_PROCESS_OPEN_PK", nullable = false)
    private Long idAssignmentProcessOpenPk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "MOTIVE_OPEN", nullable = false, length = 200)
    private String motiveOpen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE_REQUEST", nullable = false)
    private Integer stateRequest;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAssignmentProcessOpenFk", fetch = FetchType.LAZY)
    private List<AssignmentProcessOpenState> assignmentProcessOpenStateList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAssignmentProcessOpenFk", fetch = FetchType.LAZY)
    private List<AssignmentProcessOpenFile> assignmentProcessOpenFileList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAssignmentOpenFk", fetch = FetchType.LAZY)
    private List<MechanismOperationOpen> mechanismOperationOpenList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAssignmentProcessOpenFk", fetch = FetchType.LAZY)
    private List<AssignmentProcessOpenDetail> assignmentProcessOpenDetailList;

    public AssignmentProcessOpen() {
    }

    public AssignmentProcessOpen(Long idAssignmentProcessOpenPk) {
        this.idAssignmentProcessOpenPk = idAssignmentProcessOpenPk;
    }

    public AssignmentProcessOpen(Long idAssignmentProcessOpenPk, String motiveOpen, Date registryDate, Integer stateRequest, String lastModifyUser, Date lastModifyDate, String lastModifyIp, Long lastModifyApp) {
        this.idAssignmentProcessOpenPk = idAssignmentProcessOpenPk;
        this.motiveOpen = motiveOpen;
        this.registryDate = registryDate;
        this.stateRequest = stateRequest;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdAssignmentProcessOpenPk() {
        return idAssignmentProcessOpenPk;
    }

    public void setIdAssignmentProcessOpenPk(Long idAssignmentProcessOpenPk) {
        this.idAssignmentProcessOpenPk = idAssignmentProcessOpenPk;
    }

    public String getMotiveOpen() {
        return motiveOpen;
    }

    public void setMotiveOpen(String motiveOpen) {
        this.motiveOpen = motiveOpen;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public List<AssignmentProcessOpenState> getAssignmentProcessOpenStateList() {
		return assignmentProcessOpenStateList;
	}

	public void setAssignmentProcessOpenStateList(
			List<AssignmentProcessOpenState> assignmentProcessOpenStateList) {
		this.assignmentProcessOpenStateList = assignmentProcessOpenStateList;
	}

	public List<AssignmentProcessOpenFile> getAssignmentProcessOpenFileList() {
		return assignmentProcessOpenFileList;
	}

	public void setAssignmentProcessOpenFileList(
			List<AssignmentProcessOpenFile> assignmentProcessOpenFileList) {
		this.assignmentProcessOpenFileList = assignmentProcessOpenFileList;
	}

	public List<MechanismOperationOpen> getMechanismOperationOpenList() {
		return mechanismOperationOpenList;
	}

	public void setMechanismOperationOpenList(
			List<MechanismOperationOpen> mechanismOperationOpenList) {
		this.mechanismOperationOpenList = mechanismOperationOpenList;
	}

	public Integer getStateRequest() {
        return stateRequest;
    }

    public void setStateRequest(Integer stateRequest) {
        this.stateRequest = stateRequest;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public List<AssignmentProcessOpenDetail> getAssignmentProcessOpenDetailList() {
        return assignmentProcessOpenDetailList;
    }

    public void setAssignmentProcessOpenDetailList(List<AssignmentProcessOpenDetail> assignmentProcessOpenDetailList) {
        this.assignmentProcessOpenDetailList = assignmentProcessOpenDetailList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAssignmentProcessOpenPk != null ? idAssignmentProcessOpenPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssignmentProcessOpen)) {
            return false;
        }
        AssignmentProcessOpen other = (AssignmentProcessOpen) object;
        if ((this.idAssignmentProcessOpenPk == null && other.idAssignmentProcessOpenPk != null) || (this.idAssignmentProcessOpenPk != null && !this.idAssignmentProcessOpenPk.equals(other.idAssignmentProcessOpenPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.AssignmentProcessOpen[ idAssignmentProcessOpenPk=" + idAssignmentProcessOpenPk + " ]";
    }

	@Override
	public AssignmentProcessOpen clone(){
		try {
			return (AssignmentProcessOpen) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
    
    
}
