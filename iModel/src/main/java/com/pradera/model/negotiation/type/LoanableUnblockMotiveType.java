package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum LoanableUnblockMotiveType {
	
	MANUAL_PROCESS(Integer.valueOf(1792),"RETIRO MANUAL DE VALORES PRESTABLES"),
	
	AUTOMATIC_PROCESS(Integer.valueOf(1793),"RETIRO AUTOMATICO DE VALORES PRESTABLES");
	
	/** The Constant lookup. */
	public static final Map<Integer,LoanableUnblockMotiveType> lookup=new HashMap<Integer, LoanableUnblockMotiveType>();
	
	/** The Constant list. */
	public static final List<LoanableUnblockMotiveType> list=new ArrayList<LoanableUnblockMotiveType>();

	static{
		for(LoanableUnblockMotiveType s:LoanableUnblockMotiveType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private LoanableUnblockMotiveType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static LoanableUnblockMotiveType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
}
