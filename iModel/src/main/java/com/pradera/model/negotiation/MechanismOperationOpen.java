/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "MECHANISM_OPERATION_OPEN")
@NamedQueries({
    @NamedQuery(name = "MechanismOperationOpen.findAll", query = "SELECT m FROM MechanismOperationOpen m"),
    @NamedQuery(name = "MechanismOperationOpen.findByIdMechanismOpOpenPk", query = "SELECT m FROM MechanismOperationOpen m WHERE m.idMechanismOpOpenPk = :idMechanismOpOpenPk"),
    @NamedQuery(name = "MechanismOperationOpen.findByRegistryDate", query = "SELECT m FROM MechanismOperationOpen m WHERE m.registryDate = :registryDate"),
    @NamedQuery(name = "MechanismOperationOpen.findByLastModifyUser", query = "SELECT m FROM MechanismOperationOpen m WHERE m.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "MechanismOperationOpen.findByLastModifyDate", query = "SELECT m FROM MechanismOperationOpen m WHERE m.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "MechanismOperationOpen.findByLastModifyIp", query = "SELECT m FROM MechanismOperationOpen m WHERE m.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "MechanismOperationOpen.findByLastModifyApp", query = "SELECT m FROM MechanismOperationOpen m WHERE m.lastModifyApp = :lastModifyApp")})
public class MechanismOperationOpen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_MECHANISM_OPERATION_OPEN", sequenceName="SQ_MECHANISM_OPERATION_OPEN", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_MECHANISM_OPERATION_OPEN")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_MECHANISM_OP_OPEN_PK", nullable = false)
    private Long idMechanismOpOpenPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @JoinColumn(name = "ID_MECHANISM_OPERATION_FK", referencedColumnName = "ID_MECHANISM_OPERATION_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MechanismOperation idMechanismOperationFk;
    @JoinColumn(name = "ID_ASSIGNMENT_OPEN_FK", referencedColumnName = "ID_ASSIGNMENT_PROCESS_OPEN_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AssignmentProcessOpen idAssignmentOpenFk;

    public MechanismOperationOpen() {
    }

    public MechanismOperationOpen(Long idMechanismOpOpenPk) {
        this.idMechanismOpOpenPk = idMechanismOpOpenPk;
    }

    public MechanismOperationOpen(Long idMechanismOpOpenPk, Date registryDate, String lastModifyUser, Date lastModifyDate, String lastModifyIp, Long lastModifyApp) {
        this.idMechanismOpOpenPk = idMechanismOpOpenPk;
        this.registryDate = registryDate;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdMechanismOpOpenPk() {
        return idMechanismOpOpenPk;
    }

    public void setIdMechanismOpOpenPk(Long idMechanismOpOpenPk) {
        this.idMechanismOpOpenPk = idMechanismOpOpenPk;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public MechanismOperation getIdMechanismOperationFk() {
        return idMechanismOperationFk;
    }

    public void setIdMechanismOperationFk(MechanismOperation idMechanismOperationFk) {
        this.idMechanismOperationFk = idMechanismOperationFk;
    }

    public AssignmentProcessOpen getIdAssignmentOpenFk() {
        return idAssignmentOpenFk;
    }

    public void setIdAssignmentOpenFk(AssignmentProcessOpen idAssignmentOpenFk) {
        this.idAssignmentOpenFk = idAssignmentOpenFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMechanismOpOpenPk != null ? idMechanismOpOpenPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MechanismOperationOpen)) {
            return false;
        }
        MechanismOperationOpen other = (MechanismOperationOpen) object;
        if ((this.idMechanismOpOpenPk == null && other.idMechanismOpOpenPk != null) || (this.idMechanismOpOpenPk != null && !this.idMechanismOpOpenPk.equals(other.idMechanismOpOpenPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.MechanismOperationOpen[ idMechanismOpOpenPk=" + idMechanismOpOpenPk + " ]";
    }
    
}
