package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="ACCOUNT_OPERATION_MARKETFACT")
public class AccountOperationMarketFact implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="HOLACCOPEMARKETFACTPK_GENERATOR", sequenceName="SQ_ID_HOL_ACC_OPE_MARKET_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLACCOPEMARKETFACTPK_GENERATOR")	
	@Column(name="ID_ACCOUNT_OPER_MARKFACT_PK")
	private Long idAccountOperMarketFactPk;

	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Column(name="OPERATION_QUANTITY")
	private BigDecimal operationQuantity;
	
	@Column(name="IND_INCHAIN")
    private Integer indInchain = 0;
    
    @Column(name="CHAINED_QUANTITY")
    private BigDecimal chainedQuantity = BigDecimal.ZERO;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK",referencedColumnName="ID_HOLDER_ACCOUNT_OPERATION_PK")
	private HolderAccountOperation holderAccountOperation;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private BigDecimal originalQuantity = BigDecimal.ZERO;
	
	@Transient
	private boolean selected;
	
	public int getRowKey(){
		return this.hashCode();
	}
	
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getOperationQuantity() {
		return operationQuantity;
	}

	public void setOperationQuantity(BigDecimal operationQuantity) {
		this.operationQuantity = operationQuantity;
	}

	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}

	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser!=null){
			 lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
	         lastModifyDate = loggerUser.getAuditTime();
	         lastModifyIp = loggerUser.getIpAddress();
	         lastModifyUser =loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public BigDecimal getOriginalQuantity() {
		return originalQuantity;
	}

	public void setOriginalQuantity(BigDecimal originalQuantity) {
		this.originalQuantity = originalQuantity;
	}

	public Long getIdAccountOperMarketFactPk() {
		return idAccountOperMarketFactPk;
	}

	public void setIdAccountOperMarketFactPk(Long idAccountOperMarketFactPk) {
		this.idAccountOperMarketFactPk = idAccountOperMarketFactPk;
	}

	public Integer getIndInchain() {
		return indInchain;
	}

	public void setIndInchain(Integer indInchain) {
		this.indInchain = indInchain;
	}

	public BigDecimal getChainedQuantity() {
		return chainedQuantity;
	}

	public void setChainedQuantity(BigDecimal chainedQuantity) {
		this.chainedQuantity = chainedQuantity;
	}
}