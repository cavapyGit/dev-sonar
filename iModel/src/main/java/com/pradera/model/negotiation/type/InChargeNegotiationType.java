package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * TYPE creado para manejar los distintos encargos que se manejan en la liquidacion
 * los encargados SIEMPRE seran: Encargo de Fondos Y Encargo de Valores
 * The Enum CashCalendarType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/07/2013
 */
public enum InChargeNegotiationType {

	/** The util days. */
	INCHARGE_STOCK(new Integer(1), "EN CARGO DE VALORES"),
	
	/** The calendar days. */
	INCHARGE_FUNDS(new Integer(2), "EN CARGO DE FONDOS");
	
	/** The code. */
	private Integer code;
	
	/** The descripcion. */
	private String descripcion;

	/**
	 * Instantiates a new cash calendar type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private InChargeNegotiationType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/** The Constant list. */
	public static final List<InChargeNegotiationType> list = new ArrayList<InChargeNegotiationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, InChargeNegotiationType> lookup = new HashMap<Integer, InChargeNegotiationType>();
	static {
		for (InChargeNegotiationType s : EnumSet.allOf(InChargeNegotiationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the cash calendar type
	 */
	public static InChargeNegotiationType get(Integer code){
		return lookup.get(code);
	}
	
	/**
	 * List some elements.
	 *
	 * @param inChargeNegotiationType the in charge negotiation type
	 * @return the list
	 */
	public static List<InChargeNegotiationType> listSomeElements(InChargeNegotiationType... inChargeNegotiationType){
		List<InChargeNegotiationType> retorno = new ArrayList<InChargeNegotiationType>();
		for(InChargeNegotiationType TransferSecuritiesType: inChargeNegotiationType){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
}