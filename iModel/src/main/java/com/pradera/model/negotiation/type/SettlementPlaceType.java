/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.negotiation.type;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum SettlementSchemaType.
 *
 * @author PraderaTechnologies.
 *
 */
public enum SettlementPlaceType {
	

	EDV(new Integer(1928),"EDV"), 

	FIS(new Integer(1929),"FIS");
	
	private SettlementPlaceType(Integer value, String code) {
		this.value = value;
		this.code = code;
	}

	/** The value. */
	private Integer value;
	
	private String code;

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}