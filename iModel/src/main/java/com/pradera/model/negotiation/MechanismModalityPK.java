package com.pradera.model.negotiation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class MechanismModalityPK.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@Embeddable
public class MechanismModalityPK implements Serializable {
	//default serial version id, required for serializable classes.
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id negotiation mechanism pk. */
	@Column(name="ID_NEGOTIATION_MECHANISM_PK")
	private Long idNegotiationMechanismPk;

	/** The id negotiation modality pk. */
	@Column(name="ID_NEGOTIATION_MODALITY_PK")
	private Long idNegotiationModalityPk;

    /**
     * The Constructor.
     */
    public MechanismModalityPK() {
    }	    

	public MechanismModalityPK(Long idNegotiationMechanismPk,
			Long idNegotiationModalityPk) {
		super();
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}



	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}

	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}

	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MechanismModalityPK)) {
			return false;
		}
		MechanismModalityPK castOther = (MechanismModalityPK)other;
		return 
			(this.idNegotiationMechanismPk.longValue() == castOther.idNegotiationMechanismPk.longValue())
			&& (this.idNegotiationModalityPk.longValue() == castOther.idNegotiationModalityPk.longValue());

    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idNegotiationMechanismPk ^ (this.idNegotiationMechanismPk >>> 32)));
		hash = hash * prime + ((int) (this.idNegotiationModalityPk ^ (this.idNegotiationModalityPk >>> 32)));
		
		return hash;
    }
}