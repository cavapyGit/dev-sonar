package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;



/**
 * The persistent class for the ASSIGNMENT_PROCESS database table.
 * 
 */
@Entity
@Table(name="ASSIGNMENT_PROCESS")
public class AssignmentProcess implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSIGMENT_PROCESS_GENERATOR", sequenceName="SQ_ID_ASSIGMENT_PROCESS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSIGMENT_PROCESS_GENERATOR")
	@Column(name="ID_ASSIGNMENT_PROCESS_PK")
	private Long idAssignmentProcessPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK"),
		@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK")
		})
    private MechanismModality mechanisnModality;
	

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

    @Temporal( TemporalType.DATE)
	@Column(name="SETTLEMENT_DATE")
	private Date settlementDate;

	@Column(name="ASSIGNMENT_STATE")
	private Integer assignmentState;

	//bi-directional many-to-one association to MechanismOperation
	@OneToMany(mappedBy="assignmentProcess")
	private List<MechanismOperation> mechanismOperations;
	
	//bi-directional many-to-one association to ParticipantAssignment
	@OneToMany(mappedBy="assignmentProcess")
	private List<ParticipantAssignment> participantAssignments;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idAssigmentProcessFk", fetch = FetchType.LAZY)
    private List<AssignmentProcessOpenDetail> assignmentProcessOpenDetailList;
	
    public AssignmentProcess() {
    }
    
    public AssignmentProcess(Long idAssignmentProcessPk) {
        this.idAssignmentProcessPk = idAssignmentProcessPk;
    }

	public Long getIdAssignmentProcessPk() {
		return this.idAssignmentProcessPk;
	}

	public void setIdAssignmentProcessPk(Long idAssignmentProcessPk) {
		this.idAssignmentProcessPk = idAssignmentProcessPk;
	}
	
	public MechanismModality getMechanisnModality() {
		return mechanisnModality;
	}

	public List<AssignmentProcessOpenDetail> getAssignmentProcessOpenDetailList() {
		return assignmentProcessOpenDetailList;
	}

	public void setAssignmentProcessOpenDetailList(
			List<AssignmentProcessOpenDetail> assignmentProcessOpenDetailList) {
		this.assignmentProcessOpenDetailList = assignmentProcessOpenDetailList;
	}

	public void setMechanisnModality(MechanismModality mechanisnModality) {
		this.mechanisnModality = mechanisnModality;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getSettlementDate() {
		return this.settlementDate;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public Integer getAssignmentState() {
		return this.assignmentState;
	}

	public void setAssignmentState(Integer state) {
		this.assignmentState = state;
	}

	/**
	 * @return the mechanismOperations
	 */
	public List<MechanismOperation> getMechanismOperations() {
		return mechanismOperations;
	}

	/**
	 * @param mechanismOperations the mechanismOperations to set
	 */
	public void setMechanismOperations(List<MechanismOperation> mechanismOperations) {
		this.mechanismOperations = mechanismOperations;
	}

	/**
	 * @return the participantAssignments
	 */
	public List<ParticipantAssignment> getParticipantAssignments() {
		return participantAssignments;
	}

	/**
	 * @param participantAssignments the participantAssignments to set
	 */
	public void setParticipantAssignments(
			List<ParticipantAssignment> participantAssignments) {
		this.participantAssignments = participantAssignments;
	}

	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("mechanismOperations", mechanismOperations);
		detailsMap.put("participantAssignments", participantAssignments);
		return detailsMap;
	}
	
}