/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.negotiation.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum SettlementSchemaType.
 *
 * @author PraderaTechnologies.
 *
 */
public enum SettlementSchemaType {
	
	/** The Gross settlement */
	GROSS(new Integer(1),"LIQUIDACION BRUTA"),
	
	/** The Net settlement */
	NET(new Integer(2),"LIQUIDACION NETA");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<SettlementSchemaType> list = new ArrayList<SettlementSchemaType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SettlementSchemaType> lookup = new HashMap<Integer, SettlementSchemaType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<SettlementSchemaType> listSomeElements(SettlementSchemaType... transferSecuritiesTypeParams){
		List<SettlementSchemaType> retorno = new ArrayList<SettlementSchemaType>();
		for(SettlementSchemaType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (SettlementSchemaType s : EnumSet.allOf(SettlementSchemaType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private SettlementSchemaType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static SettlementSchemaType get(Integer codigo) {
		return lookup.get(codigo);
	}
}