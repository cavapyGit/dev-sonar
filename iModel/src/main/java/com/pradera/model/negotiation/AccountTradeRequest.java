package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;


/**
 * The persistent class for the ACCOUNT_SIRTEX_OPERATION database table.
 * 
 */
@Entity
@Table(name="ACCOUNT_TRADE_REQUEST")
public class AccountTradeRequest implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCOUNT_SIRTEX_OPERATION_IDACCOUNTSIRTEXOPERATIONPK_GENERATOR", sequenceName="SQ_ID_ACC_TRADE_OPERAT_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNT_SIRTEX_OPERATION_IDACCOUNTSIRTEXOPERATIONPK_GENERATOR")
	@Column(name="ID_ACCOUNT_TRADE_PK")
	private Long idAccountTradePk;

	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INCHARGE_STOCK_PARTICIPANT")
	private Participant inchargeStockParticipant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INCHARGE_FUNDS_PARTICIPANT")
	private Participant inchargeFundsParticipant;
	
	@Column(name="IND_INCHARGE")
	private Integer indInCharge;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OPERATION_PART")
	private Integer operationPart;

	@Temporal(TemporalType.DATE)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

	@Column(name="REGISTER_USER")
	private String registerUser;

	@Column(name="\"ROLE\"")
	private Integer role;

	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;

	//bi-directional many-to-one association to AccountSirtexMarkectfact
	@OneToMany(mappedBy="accountTradeRequest")
	private List<AccountTradeMarketFact> accountSirtexMarkectfacts;

	//bi-directional many-to-one association to SirtexOperation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_REQUEST_FK")
	private TradeRequest tradeRequest;

	public AccountTradeRequest() {
	}

	public BigDecimal getCashAmount() {
		return this.cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getOperationPart() {
		return this.operationPart;
	}

	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return this.registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Integer getRole() {
		return this.role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public BigDecimal getStockQuantity() {
		return this.stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public List<AccountTradeMarketFact> getAccountSirtexMarkectfacts() {
		return this.accountSirtexMarkectfacts;
	}

	public void setAccountSirtexMarkectfacts(List<AccountTradeMarketFact> accountSirtexMarkectfacts) {
		this.accountSirtexMarkectfacts = accountSirtexMarkectfacts;
	}

	public AccountTradeMarketFact addAccountSirtexMarkectfact(AccountTradeMarketFact accountSirtexMarkectfact) {
		getAccountSirtexMarkectfacts().add(accountSirtexMarkectfact);

		return accountSirtexMarkectfact;
	}

	public AccountTradeMarketFact removeAccountSirtexMarkectfact(AccountTradeMarketFact accountSirtexMarkectfact) {
		getAccountSirtexMarkectfacts().remove(accountSirtexMarkectfact);

		return accountSirtexMarkectfact;
	}

	public TradeRequest getTradeRequest() {
		return tradeRequest;
	}

	public void setTradeRequest(TradeRequest tradeRequest) {
		this.tradeRequest = tradeRequest;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getInchargeStockParticipant() {
		return inchargeStockParticipant;
	}

	public void setInchargeStockParticipant(Participant inchargeStockParticipant) {
		this.inchargeStockParticipant = inchargeStockParticipant;
	}

	public Participant getInchargeFundsParticipant() {
		return inchargeFundsParticipant;
	}

	public void setInchargeFundsParticipant(Participant inchargeFundsParticipant) {
		this.inchargeFundsParticipant = inchargeFundsParticipant;
	}

	public Integer getIndInCharge() {
		return indInCharge;
	}

	public void setIndInCharge(Integer indInCharge) {
		this.indInCharge = indInCharge;
	}

	public Long getIdAccountTradePk() {
		return idAccountTradePk;
	}

	public void setIdAccountTradePk(Long idAccountTradePk) {
		this.idAccountTradePk = idAccountTradePk;
	}
}