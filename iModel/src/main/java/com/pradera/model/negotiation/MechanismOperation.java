package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.type.NegotiationModalityType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the MECHANISM_OPERATION database table.
 * @author PraderaTechnologies.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = MechanismOperation.OPERATION_STATE, query = "Select new com.pradera.model.negotiation.MechanismOperation(mo.idMechanismOperationPk,mo.operationState) From MechanismOperation mo WHERE mo.idMechanismOperationPk = :idMechanismOperationPkParam"),
	@NamedQuery(name = MechanismOperation.PAYMENT_REFERENCE, query = "Select new com.pradera.model.negotiation.MechanismOperation(mo.idMechanismOperationPk,mo.paymentReference) From MechanismOperation mo WHERE mo.idMechanismOperationPk = :idMechanismOperationPkParam")
})
@Table(name="MECHANISM_OPERATION")
public class MechanismOperation implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2L;
	
	/** The Constant OPERATION_STATE. */
	public static final String OPERATION_STATE ="mechanisOperation.state";

	public static final String PAYMENT_REFERENCE ="mechanisOperation.paymentReference";
	
	public static final String SLASH ="/";

	/** The id mechanism operation pk. */
	@Id
	private Long idMechanismOperationPk;
	
	/** The trade operation. */
	@MapsId
	@OneToOne(cascade=CascadeType.PERSIST,fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_PK")
	private TradeOperation tradeOperation;

    /** The amountRate. */
	@Column(name="AMOUNT_RATE")
	private BigDecimal amountRate;

	@Column(name="REAL_AMOUNT_RATE")
	private BigDecimal realAmountRate;
	
	/** The cashAmount. */
	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	/** The cashPrice. */
	@Column(name="CASH_PRICE")
	private BigDecimal cashPrice;

	/** The cashSettelmentDays. */
	@Column(name="CASH_SETTELMENT_DAYS")
	private Long cashSettlementDays;

	/** The cashSettlementDate. */
	@Temporal( TemporalType.DATE)
	@Column(name="CASH_SETTLEMENT_DATE")
	private Date cashSettlementDate;
	
	/** The operationDate. */
	@Temporal( TemporalType.DATE)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	/** The currency. */
	@Column(name="OPERATION_CURRENCY")
	private Integer currency;

	/** The exchangeRate. */
	@Column(name="EXCHANGE_RATE")
	private BigDecimal exchangeRate;

	@Column(name="CASH_PRICE_PERCENTAGE")
	private BigDecimal cashPricePercentage;

	@Column(name="REAL_CASH_PRICE_PERCENTAGE")
	private BigDecimal realCashPricePercentage;
	
//	/** The extendedDate. */
//	@Temporal( TemporalType.DATE)
//	@Column(name="CASH_EXTENDED_DATE")
//	private Date cashExtendedDate;
	
	/** 
	 * The extendedDate. 
	 * bi-directional many-to-one association to assignmentProcess
	 */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ASSIGNMENT_PROCESS_FK")
	private AssignmentProcess assignmentProcess;

    /**
     *  The buyerParticipant.
     *  bi-directional many-to-one association to buyerParticipant
     * */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BUYER_PARTICIPANT_FK")
	private Participant buyerParticipant;

    /** 
     * The buyerParticipant.
     * bi-directional many-to-one association to securities
     * */    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;
    
    /** The mechanisn modality. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK"),
		@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK")
		})
    private MechanismModality mechanisnModality;
    
    @OneToMany(mappedBy="mechanismOperation",cascade={CascadeType.PERSIST,CascadeType.REFRESH})
	private List<SwapOperation> swapOperations;
    
    /** 
     * The sellerParticipant.
     * bi-directional many-to-one association to sellerParticipant
     * */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SELLER_PARTICIPANT_FK")
	private Participant sellerParticipant;

//    /** The indExtended. */
//    @Column(name="IND_CASH_EXTENDED")
//	private Integer indCashExtended;
//
//    /** The indPrepaid. */
//    @Column(name="IND_CASH_PREPAID")
//	private Integer indCashPrepaid;

    /** The lastModifyApp. */
    @Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The lastModifyDate. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The lastModifyIp. */
    @Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

    /** The lastModifyUser. */
    @Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The marginReference. */
    @Column(name="MARGIN_REFERENCE")
	private Long marginReference;

    /** The nominalValue. */
    @Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;

    /** The operationNumber. */
    @Column(name="OPERATION_NUMBER")
	private Long operationNumber;
    
    @Column(name="BALLOT_NUMBER")
	private Long ballotNumber;
    
    @Column(name="SEQUENTIAL")
	private Long sequential;

    /** The paymentReference. */
    @Column(name="PAYMENT_REFERENCE")
	private String paymentReference;

//    /** The prepaidDate. */
//    @Temporal( TemporalType.DATE)
//	@Column(name="CASH_PREPAID_DATE")
//	private Date cashPrepaidDate;

    /** The realCashAmount. */
    @Column(name="REAL_CASH_AMOUNT")
	private BigDecimal realCashAmount;

    /** The referenceDate. */
    @Temporal( TemporalType.DATE)
	@Column(name="REFERENCE_DATE")
	private Date referenceDate;

    /** The referenceNumber. */
    @Column(name="REFERENCE_NUMBER")
	private String referenceNumber;

    /** The registerDate. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

    /** The registerUser. */
    @Column(name="REGISTER_USER")
	private String registerUser;

    /** The returnGuaranteesDate. */
    @Temporal( TemporalType.DATE)
	@Column(name="RETURN_GUARANTEES_DATE")
	private Date returnGuaranteesDate;

    /** The settlementSchema. */
    @Column(name="SETTLEMENT_SCHEMA")
	private Integer settlementSchema;

    /** The settlementType. */
    @Column(name="SETTLEMENT_TYPE")
	private Integer settlementType;
    
    /** The state. */
    @Column(name="OPERATION_STATE")
	private Integer operationState;

    /** The stockQuantity. */
    @Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;

    /** The termSettlementDays. */
    @Column(name="TERM_SETTLEMENT_DAYS")
	private Long termSettlementDays;

    /** The termAmount. */
    @Column(name="TERM_AMOUNT")
	private BigDecimal termAmount;

    /** The termPrice. */
    @Column(name="TERM_PRICE")
	private BigDecimal termPrice;
    
    @Column(name="COVERAGE_AMOUNT")
	private BigDecimal coverageAmount;

    /** The termSettlementDate. */
    @Temporal( TemporalType.DATE)
	@Column(name="TERM_SETTLEMENT_DATE")
	private Date termSettlementDate;

//    /** The unfulfillmentDate. */
//    @Temporal( TemporalType.DATE)
//	@Column(name="UNFULFILLMENT_CASH_DATE")
//	private Date unfulfillmentCashDate;
//
//    @Temporal( TemporalType.DATE)
//	@Column(name="UNFULFILLMENT_TERM_DATE")
//	private Date unfulfillmentTermDate;
    
    /** The realCashPrice. */
    @Column(name="REAL_CASH_PRICE")
	private BigDecimal realCashPrice;
    
    /** The cancelMotive. */
    @Column(name="CANCEL_MOTIVE")
	private Integer cancelMotive;

    /** The cancelMotive. */
    @Column(name="CANCEL_MOTIVE_OTHER")
	private String cancelMotiveOther;
    
//    @Column(name="IND_TERM_PREPAID")
//    private Integer indTermPrepaid;
    
//    @Column(name="IND_TERM_EXTENDED")
//    private Integer indTermExtended;
    
//    @Column(name="TERM_PREPAID_DATE")
//    private Date termPrepaidDate;
//    
//    @Column(name="TERM_EXTENDED_DATE")
//    private Date termExtendedDate;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MCN_PROCESS_FILE_FK")
	private McnProcessFile mcnProcessFile;

  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_REFERENCE_OPERATION_FK")
  	private MechanismOperation referenceOperation;
	
 	@OneToMany(mappedBy="mechanismOperation",cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH,CascadeType.REMOVE},orphanRemoval=true)
	private List<HolderAccountOperation> holderAccountOperations;
  	
	@OneToMany(mappedBy="mechanismOperation",cascade=CascadeType.PERSIST)
	private List<ParticipantOperation> participantOperations;

	/** the assignmentRequests bi-directional many-to-one association to AssignmentRequest. */
	@OneToMany(mappedBy="mechanismOperation")
	private List<AssignmentRequest> assignmentRequests;
	
	@OneToMany(mappedBy="mechanismOperation",fetch=FetchType.LAZY)
	private List<MechanismOperationMarketFact> mechanismOperationMarketFacts;
	
	@Column(name="IND_CASH_SETTLEMENT_EXCHANGE")
	private Integer indCashSettlementExchange;
	
	@Column(name="CASH_SETTLEMENT_CURRENCY")
	private Integer cashSettlementCurrency;
	     
	@Column(name="CASH_SETTLEMENT_AMOUNT")
	private BigDecimal cashSettlementAmount;
	
	@Column(name="CASH_SETTLEMENT_PRICE")
	private BigDecimal cashSettlementPrice;
	    
	@Column(name="CASH_SETTLEMENT_EXCHANGE_RATE")
	private BigDecimal cashSettlementExChangeRate;
	
	@Column(name="IND_TERM_SETTLEMENT_EXCHANGE")
	private Integer indTermSettlementExchange;
	
	@Column(name="TERM_SETTLEMENT_CURRENCY")
	private Integer termSettlementCurrency;
	     
	@Column(name="TERM_SETTLEMENT_AMOUNT")
	private BigDecimal termSettlementAmount;
	
	@Column(name="TERM_SETTLEMENT_PRICE")
	private BigDecimal termSettlementPrice;
	    
	@Column(name="TERM_SETTLEMENT_EXCHANGE_RATE")
	private BigDecimal termSettlementExChangeRate;
	
	@Column(name="IND_PRINCIPAL_GUARANTEE")
	private Integer indPrincipalGuarantee;
	
	@Column(name="IND_MARGIN_GUARANTEE")
	private Integer indMarginGuarantee;
	
	@Column(name="IND_REPORTING_BALANCE")
	private Integer indReportingBalance;
	
	@Column(name="IND_TERM_SETTLEMENT")
	private Integer indTermSettlement;
	
	@Column(name="IND_CASH_STOCK_BLOCK")
	private Integer indCashStockBlock;
	
	@Column(name="IND_TERM_STOCK_BLOCK")
	private Integer indTermStockBlock;
	
	@Column(name="IND_PRIMARY_PLACEMENT")
	private Integer indPrimaryPlacement;
	
	@Column(name="IND_DPF")
	private Integer indDpf;
	
	@Column(name="WARRANTY_CAUSE")
	private BigDecimal warrantyCause;
	
	@Column(name="TERM_INTEREST_RATE")
	private BigDecimal termInterestRate;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idMechanismOperationFk", fetch = FetchType.LAZY)
    private List<MechanismOperationOpen> mechanismOperationOpenList;
	
	@Transient
    private Long originRequest;
    
    @Transient 
    private String cancelDescription;

    @Transient
    private String stateDescription;
    
    @Transient
    private String currencyDescription;
	
    @Transient
    private BigDecimal quantity;
    
    @Transient
    private BigDecimal tempQuantity;

    @Transient
    private boolean isSelected;
    
    @Transient
	private String operationNumberBallotSequential;
    
    @Transient
  	private String instrumentTypeDescription;
      
	/**
	 * The Default Constructor.
	 */
    public MechanismOperation() {
    	
    }

    
	/**
	 * @param idMechanismOperationPk
	 * @param cashPrice
	 * @param cashSettlementPrice
	 */
	public MechanismOperation(Long idMechanismOperationPk,
			BigDecimal cashPrice, BigDecimal cashSettlementPrice) {
		this.idMechanismOperationPk = idMechanismOperationPk;
		this.cashPrice = cashPrice;
		this.cashSettlementPrice = cashSettlementPrice;
	}


	/**
	 * @param idMechanismOperationPk
	 * @param cashPrice
	 * @param buyerParticipant
	 * @param mechanisnModality
	 * @param sellerParticipant
	 * @param cashSettlementPrice
	 */
	public MechanismOperation(Long idMechanismOperationPk,
			BigDecimal cashPrice, Participant buyerParticipant,
			MechanismModality mechanisnModality, Participant sellerParticipant,
			BigDecimal cashSettlementPrice) {
		this.idMechanismOperationPk = idMechanismOperationPk;
		this.cashPrice = cashPrice;
		this.buyerParticipant = buyerParticipant;
		this.mechanisnModality = mechanisnModality;
		this.sellerParticipant = sellerParticipant;
		this.cashSettlementPrice = cashSettlementPrice;
	}

	public MechanismOperation(Long idMechanismOperationPk) {
		super();
		this.idMechanismOperationPk = idMechanismOperationPk;
	}

	public MechanismOperation(Long idMechanismOperationPk, Integer operationState) {
		super();
		this.idMechanismOperationPk = idMechanismOperationPk;
		this.operationState = operationState;
	}

	public MechanismOperation(Long idMechanismOperationPk, String paymentReference) {
		super();
		this.idMechanismOperationPk = idMechanismOperationPk;
		this.paymentReference = paymentReference;
	}


	public void setOperationNumberBallotSequential(
			String operationNumberBallotSequential) {
		this.operationNumberBallotSequential = operationNumberBallotSequential;
	}
	
	public String getOperationNumberBallotSequential(){
		if(operationNumberBallotSequential == null){
			StringBuilder sbOpNumber = new StringBuilder();
			if(ballotNumber!=null && sequential!=null){
				sbOpNumber.append(this.ballotNumber).append(SLASH).append(this.sequential);
			}else{
				sbOpNumber.append(operationNumber.toString());
			}
			operationNumberBallotSequential = sbOpNumber.toString();
		}
		return operationNumberBallotSequential;
	}
	
	public Boolean isRateVariableModality() {
		if(Validations.validateIsNotNull(this.mechanisnModality) && Validations.validateIsNotNull(this.mechanisnModality.getId())
				&& Validations.validateIsNotNull(this.mechanisnModality.getId().getIdNegotiationModalityPk())
				&& (this.mechanisnModality.getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_EQUITIES.getCode())
						|| this.mechanisnModality.getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.REPORT_EQUITIES.getCode())
						|| this.mechanisnModality.getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode()))) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	public Boolean isRateFixedModality() {
		if(Validations.validateIsNotNull(this.mechanisnModality) && Validations.validateIsNotNull(this.mechanisnModality.getId())
				&& Validations.validateIsNotNull(this.mechanisnModality.getId().getIdNegotiationModalityPk())
				&& ( this.mechanisnModality.getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.CASH_FIXED_INCOME.getCode())
						|| this.mechanisnModality.getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.REPORT_FIXED_INCOME.getCode())
						|| this.mechanisnModality.getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode())) ) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	public Boolean isReportRateFixedModality() {
		if(Validations.validateIsNotNull(this.mechanisnModality) && Validations.validateIsNotNull(this.mechanisnModality.getId())
				&& Validations.validateIsNotNull(this.mechanisnModality.getId().getIdNegotiationModalityPk())
				&& this.mechanisnModality.getId().getIdNegotiationModalityPk().equals(NegotiationModalityType.REPORT_FIXED_INCOME.getCode())) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the amount rate.
	 *
	 * @return the amountRate
	 */
	public BigDecimal getAmountRate() {
		return amountRate;
	}


	/**
	 * Sets the amount rate.
	 *
	 * @param amountRate the amountRate to set
	 */
	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}



	/**
	 * Gets the cash amount.
	 *
	 * @return the cashAmount
	 */
	public BigDecimal getCashAmount() {
		return cashAmount;
	}



	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the cashAmount to set
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}



	/**
	 * Gets the cash price.
	 *
	 * @return the cashPrice
	 */
	public BigDecimal getCashPrice() {
		return cashPrice;
	}



	/**
	 * Sets the cash price.
	 *
	 * @param cashPrice the cashPrice to set
	 */
	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}



	/**
	 * Gets the cash settelment days.
	 *
	 * @return the cashSettelmentDays
	 */
	public Long getCashSettlementDays() {
		return cashSettlementDays;
	}



	/**
	 * Sets the cash settelment days.
	 *
	 * @param cashSettelmentDays the cashSettelmentDays to set
	 */
	public void setCashSettlementDays(Long cashSettelmentDays) {
		this.cashSettlementDays = cashSettelmentDays;
	}



	/**
	 * Gets the cash settlement date.
	 *
	 * @return the cashSettlementDate
	 */
	public Date getCashSettlementDate() {
		return cashSettlementDate;
	}



	/**
	 * Sets the cash settlement date.
	 *
	 * @param cashSettlementDate the cashSettlementDate to set
	 */
	public void setCashSettlementDate(Date cashSettlementDate) {
		this.cashSettlementDate = cashSettlementDate;
	}


	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}



	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}



	/**
	 * Gets the exchange rate.
	 *
	 * @return the exchangeRate
	 */
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}



	/**
	 * Sets the exchange rate.
	 *
	 * @param exchangeRate the exchangeRate to set
	 */
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	/**
	 * Gets the assignment process.
	 *
	 * @return the assignmentProcess
	 */
	public AssignmentProcess getAssignmentProcess() {
		return assignmentProcess;
	}



	/**
	 * Sets the assignment process.
	 *
	 * @param assignmentProcess the assignmentProcess to set
	 */
	public void setAssignmentProcess(AssignmentProcess assignmentProcess) {
		this.assignmentProcess = assignmentProcess;
	}



	/**
	 * Gets the buyer participant.
	 *
	 * @return the buyerParticipant
	 */
	public Participant getBuyerParticipant() {
		return buyerParticipant;
	}



	/**
	 * Sets the buyer participant.
	 *
	 * @param buyerParticipant the buyerParticipant to set
	 */
	public void setBuyerParticipant(Participant buyerParticipant) {
		this.buyerParticipant = buyerParticipant;
	}



	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}



	/**
	 * Sets the securities.
	 *
	 * @param securities the securities to set
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	

	/**
	 * Gets the seller participant.
	 *
	 * @return the sellerParticipant
	 */
	public Participant getSellerParticipant() {
		return sellerParticipant;
	}



	/**
	 * Sets the seller participant.
	 *
	 * @param sellerParticipant the sellerParticipant to set
	 */
	public void setSellerParticipant(Participant sellerParticipant) {
		this.sellerParticipant = sellerParticipant;
	}


	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/**
	 * Gets the margin reference.
	 *
	 * @return the marginReference
	 */
	public Long getMarginReference() {
		return marginReference;
	}



	/**
	 * Sets the margin reference.
	 *
	 * @param marginReference the marginReference to set
	 */
	public void setMarginReference(Long marginReference) {
		this.marginReference = marginReference;
	}



	/**
	 * Gets the nominal value.
	 *
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}



	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}



	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}



	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}



	/**
	 * Gets the operation number.
	 *
	 * @return the operationNumber
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}



	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}



	/**
	 * Gets the payment reference.
	 *
	 * @return the paymentReference
	 */
	public String getPaymentReference() {
		return paymentReference;
	}



	/**
	 * Sets the payment reference.
	 *
	 * @param paymentReference the paymentReference to set
	 */
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}



	/**
	 * Gets the real cash amount.
	 *
	 * @return the realCashAmount
	 */
	public BigDecimal getRealCashAmount() {
		return realCashAmount;
	}



	/**
	 * Sets the real cash amount.
	 *
	 * @param realCashAmount the realCashAmount to set
	 */
	public void setRealCashAmount(BigDecimal realCashAmount) {
		this.realCashAmount = realCashAmount;
	}



	/**
	 * Gets the reference date.
	 *
	 * @return the referenceDate
	 */
	public Date getReferenceDate() {
		return referenceDate;
	}



	/**
	 * Sets the reference date.
	 *
	 * @param referenceDate the referenceDate to set
	 */
	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}



	/**
	 * Gets the reference number.
	 *
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}



	/**
	 * Sets the reference number.
	 *
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}



	/**
	 * Gets the register date.
	 *
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}



	/**
	 * Sets the register date.
	 *
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}



	/**
	 * Gets the register user.
	 *
	 * @return the registerUser
	 */
	public String getRegisterUser() {
		return registerUser;
	}



	/**
	 * Sets the register user.
	 *
	 * @param registerUser the registerUser to set
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}



	/**
	 * Gets the return guarantees date.
	 *
	 * @return the returnGuaranteesDate
	 */
	public Date getReturnGuaranteesDate() {
		return returnGuaranteesDate;
	}



	/**
	 * Sets the return guarantees date.
	 *
	 * @param returnGuaranteesDate the returnGuaranteesDate to set
	 */
	public void setReturnGuaranteesDate(Date returnGuaranteesDate) {
		this.returnGuaranteesDate = returnGuaranteesDate;
	}



	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlementSchema
	 */
	public Integer getSettlementSchema() {
		return settlementSchema;
	}



	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the settlementSchema to set
	 */
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}



	/**
	 * Gets the settlement type.
	 *
	 * @return the settlementType
	 */
	public Integer getSettlementType() {
		return settlementType;
	}



	/**
	 * Sets the settlement type.
	 *
	 * @param settlementType the settlementType to set
	 */
	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stockQuantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}



	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the stockQuantity to set
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}



	/**
	 * Gets the term price.
	 *
	 * @return the termPrice
	 */
	public BigDecimal getTermPrice() {
		return termPrice;
	}



	/**
	 * Sets the term price.
	 *
	 * @param termPrice the termPrice to set
	 */
	public void setTermPrice(BigDecimal termPrice) {
		this.termPrice = termPrice;
	}



	/**
	 * Gets the term settlement date.
	 *
	 * @return the termSettlementDate
	 */
	public Date getTermSettlementDate() {
		return termSettlementDate;
	}



	/**
	 * Sets the term settlement date.
	 *
	 * @param termSettlementDate the termSettlementDate to set
	 */
	public void setTermSettlementDate(Date termSettlementDate) {
		this.termSettlementDate = termSettlementDate;
	}


	/**
	 * Gets the holder account operations.
	 *
	 * @return the holderAccountOperations
	 */
	public List<HolderAccountOperation> getHolderAccountOperations() {
		return holderAccountOperations;
	}



	/**
	 * Sets the holder account operations.
	 *
	 * @param holderAccountOperations the holderAccountOperations to set
	 */
	public void setHolderAccountOperations(
			List<HolderAccountOperation> holderAccountOperations) {
		this.holderAccountOperations = holderAccountOperations;
	}



	/**
	 * Gets the mcn process file.
	 *
	 * @return the mcnProcessFile
	 */
	public McnProcessFile getMcnProcessFile() {
		return mcnProcessFile;
	}



	/**
	 * Sets the mcn process file.
	 *
	 * @param mcnProcessFile the mcnProcessFile to set
	 */
	public void setMcnProcessFile(McnProcessFile mcnProcessFile) {
		this.mcnProcessFile = mcnProcessFile;
	}



	/**
	 * Gets the reference operation.
	 *
	 * @return the referenceOperation
	 */
	public MechanismOperation getReferenceOperation() {
		return referenceOperation;
	}



	/**
	 * Sets the reference operation.
	 *
	 * @param referenceOperation the referenceOperation to set
	 */
	public void setReferenceOperation(MechanismOperation referenceOperation) {
		this.referenceOperation = referenceOperation;
	}



	/**
	 * Gets the trade operation.
	 *
	 * @return the tradeOperation
	 */
	public TradeOperation getTradeOperation() {
		return tradeOperation;
	}



	/**
	 * Sets the trade operation.
	 *
	 * @param tradeOperation the tradeOperation to set
	 */
	public void setTradeOperation(TradeOperation tradeOperation) {
		this.tradeOperation = tradeOperation;
	}



	/**
	 * Gets the participant operations.
	 *
	 * @return the participantOperations
	 */
	public List<ParticipantOperation> getParticipantOperations() {
		return participantOperations;
	}



	/**
	 * Sets the participant operations.
	 *
	 * @param participantOperations the participantOperations to set
	 */
	public void setParticipantOperations(
			List<ParticipantOperation> participantOperations) {
		this.participantOperations = participantOperations;
	}



	/**
	 * Gets the assignment requests.
	 *
	 * @return the assignmentRequests
	 */
	public List<AssignmentRequest> getAssignmentRequests() {
		return assignmentRequests;
	}



	/**
	 * Sets the assignment requests.
	 *
	 * @param assignmentRequests the assignmentRequests to set
	 */
	public void setAssignmentRequests(List<AssignmentRequest> assignmentRequests) {
		this.assignmentRequests = assignmentRequests;
	}

	/**
	 * Gets the id mechanism operation pk.
	 *
	 * @return the id mechanism operation pk
	 */
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}



	/**
	 * Sets the id mechanism operation pk.
	 *
	 * @param idMechanismOperationPk the new id mechanism operation pk
	 */
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}



	/**
	 * Gets the mechanisn modality.
	 *
	 * @return the mechanisn modality
	 */
	public MechanismModality getMechanisnModality() {
		return mechanisnModality;
	}



	/**
	 * Sets the mechanisn modality.
	 *
	 * @param mechanisnModality the new mechanisn modality
	 */
	public void setMechanisnModality(MechanismModality mechanisnModality) {
		this.mechanisnModality = mechanisnModality;
	}
    
    /**
     * Gets the cancel motive.
     *
     * @return the cancelMotive
     */
	public Integer getCancelMotive() {
		return cancelMotive;
	}



	/**
	 * Sets the cancel motive.
	 *
	 * @param cancelMotive the cancelMotive to set
	 */
	public void setCancelMotive(Integer cancelMotive) {
		this.cancelMotive = cancelMotive;
	}



	/**
	 * Gets the cancel motive other.
	 *
	 * @return the cancelMotiveOther
	 */
	public String getCancelMotiveOther() {
		return cancelMotiveOther;
	}



	/**
	 * Sets the cancel motive other.
	 *
	 * @param cancelMotiveOther the cancelMotiveOther to set
	 */
	public void setCancelMotiveOther(String cancelMotiveOther) {
		this.cancelMotiveOther = cancelMotiveOther;
	}

	/**
	 * Gets the real cash price.
	 *
	 * @return the realCashPrice
	 */
	public BigDecimal getRealCashPrice() {
		return realCashPrice;
	}



	/**
	 * Sets the real cash price.
	 *
	 * @param realCashPrice the realCashPrice to set
	 */
	public void setRealCashPrice(BigDecimal realCashPrice) {
		this.realCashPrice = realCashPrice;
	}



	public Long getTermSettlementDays() {
		return termSettlementDays;
	}



	public void setTermSettlementDays(Long termSettlementDays) {
		this.termSettlementDays = termSettlementDays;
	}



	public BigDecimal getTermAmount() {
		return termAmount;
	}



	public void setTermAmount(BigDecimal termAmount) {
		this.termAmount = termAmount;
	}

	/**
	 * Gets the origin request.
	 *
	 * @return the origin request
	 */
	public Long getOriginRequest() {
		return originRequest;
	}

	/**
	 * Sets the origin request.
	 *
	 * @param originRequest the new origin request
	 */
	public void setOriginRequest(Long originRequest) {
		this.originRequest = originRequest;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
            if(tradeOperation!=null){
    			tradeOperation.setLastModifyApp(lastModifyApp);
    			tradeOperation.setLastModifyDate(lastModifyDate);
    			tradeOperation.setLastModifyIp(lastModifyIp);
    			tradeOperation.setLastModifyUser(lastModifyUser);
    		}
        }
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("holderAccountOperations", holderAccountOperations);
        detailsMap.put("swapOperations", (List<? extends Auditable>) swapOperations);
        return detailsMap;
	}

	/**
	 * Gets the swap operations.
	 *
	 * @return the swap operations
	 */
	public List<SwapOperation> getSwapOperations() {
		return swapOperations;
	}



	/**
	 * Sets the swap operations.
	 *
	 * @param swapOperations the new swap operations
	 */
	public void setSwapOperations(List<SwapOperation> swapOperations) {
		this.swapOperations = swapOperations;
	}

	public String getCurrencyDescription() {
		if(Validations.validateIsNotNull(currency))
			return CurrencyType.get(currency).getValue();
			else
			return "";
	}

	/**
	 * @return the cancelDescription
	 */
	public String getCancelDescription() {
		return cancelDescription;
	}
	
	/**
	 * @param cancelDescription the cancelDescription to set
	 */
	public void setCancelDescription(String cancelDescription) {
		this.cancelDescription = cancelDescription;
	}

	/**
	 * @return the stateDescription
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * @param stateDescription the stateDescription to set
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * @return the coverageAmount
	 */
	public BigDecimal getCoverageAmount() {
		return coverageAmount;
	}

	/**
	 * @param coverageAmount the coverageAmount to set
	 */
	public void setCoverageAmount(BigDecimal coverageAmount) {
		this.coverageAmount = coverageAmount;
	}

	public MechanismOperation(Long idMechanismOperationPk, Long operationNumber, Integer operationState) {
		super();
		this.idMechanismOperationPk = idMechanismOperationPk;
		this.operationNumber = operationNumber;
		this.operationState = operationState;
	}

	public Long getBallotNumber() {
		return ballotNumber;
	}

	public void setBallotNumber(Long ballotNumber) {
		this.ballotNumber = ballotNumber;
	}

	public Long getSequential() {
		return sequential;
	}

	public void setSequential(Long sequential) {
		this.sequential = sequential;
	}


	public Integer getIndCashSettlementExchange() {
		return indCashSettlementExchange;
	}

	public void setIndCashSettlementExchange(Integer indCashSettlementEnchange) {
		this.indCashSettlementExchange = indCashSettlementEnchange;
	}

	public Integer getCashSettlementCurrency() {
		return cashSettlementCurrency;
	}

	public void setCashSettlementCurrency(Integer cashSettlementCurrency) {
		this.cashSettlementCurrency = cashSettlementCurrency;
	}

	public BigDecimal getCashSettlementAmount() {
		return cashSettlementAmount;
	}

	public void setCashSettlementAmount(BigDecimal cashSettlementAmount) {
		this.cashSettlementAmount = cashSettlementAmount;
	}

	public Integer getIndTermSettlementExchange() {
		return indTermSettlementExchange;
	}

	public void setIndTermSettlementExchange(Integer indTermSettlementEnchange) {
		this.indTermSettlementExchange = indTermSettlementEnchange;
	}

	public Integer getTermSettlementCurrency() {
		return termSettlementCurrency;
	}

	public void setTermSettlementCurrency(Integer termSettlementCurrency) {
		this.termSettlementCurrency = termSettlementCurrency;
	}

	public BigDecimal getTermSettlementAmount() {
		return termSettlementAmount;
	}

	public void setTermSettlementAmount(BigDecimal termSettlementAmount) {
		this.termSettlementAmount = termSettlementAmount;
	}

	public BigDecimal getCashSettlementExChangeRate() {
		return cashSettlementExChangeRate;
	}

	public void setCashSettlementExChangeRate(BigDecimal cashSettlementExChangeRate) {
		this.cashSettlementExChangeRate = cashSettlementExChangeRate;
	}

	public BigDecimal getTermSettlementExChangeRate() {
		return termSettlementExChangeRate;
	}

	public void setTermSettlementExChangeRate(BigDecimal termSettlementExChangeRate) {
		this.termSettlementExChangeRate = termSettlementExChangeRate;
	}

	public BigDecimal getCashSettlementPrice() {
		return cashSettlementPrice;
	}

	public void setCashSettlementPrice(BigDecimal cashSettlementPrice) {
		this.cashSettlementPrice = cashSettlementPrice;
	}

	public BigDecimal getTermSettlementPrice() {
		return termSettlementPrice;
	}

	public void setTermSettlementPrice(BigDecimal termSettlementPrice) {
		this.termSettlementPrice = termSettlementPrice;
	}

	public List<MechanismOperationMarketFact> getMechanismOperationMarketFacts() {
		return mechanismOperationMarketFacts;
	}

	public void setMechanismOperationMarketFacts(
			List<MechanismOperationMarketFact> mechanismOperationMarketFacts) {
		this.mechanismOperationMarketFacts = mechanismOperationMarketFacts;
	}

	public String getInstrumentTypeDescription() {
		return instrumentTypeDescription;
	}

	public void setInstrumentTypeDescription(String instrumentTypeDescription) {
		this.instrumentTypeDescription = instrumentTypeDescription;
	}

	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	public Integer getIndPrincipalGuarantee() {
		return indPrincipalGuarantee;
	}

	public void setIndPrincipalGuarantee(Integer indPrincipalGuarantee) {
		this.indPrincipalGuarantee = indPrincipalGuarantee;
	}

	public Integer getIndMarginGuarantee() {
		return indMarginGuarantee;
	}

	public void setIndMarginGuarantee(Integer indMarginGuarantee) {
		this.indMarginGuarantee = indMarginGuarantee;
	}

	public Integer getIndReportingBalance() {
		return indReportingBalance;
	}

	public void setIndReportingBalance(Integer indReportingBalance) {
		this.indReportingBalance = indReportingBalance;
	}

	public Integer getIndTermSettlement() {
		return indTermSettlement;
	}

	public void setIndTermSettlement(Integer indTermSettlement) {
		this.indTermSettlement = indTermSettlement;
	}

	public Integer getIndCashStockBlock() {
		return indCashStockBlock;
	}

	public void setIndCashStockBlock(Integer indCashStockBlock) {
		this.indCashStockBlock = indCashStockBlock;
	}

	public Integer getIndTermStockBlock() {
		return indTermStockBlock;
	}

	public void setIndTermStockBlock(Integer indTermStockBlock) {
		this.indTermStockBlock = indTermStockBlock;
	}

	public Integer getIndPrimaryPlacement() {
		return indPrimaryPlacement;
	}

	public void setIndPrimaryPlacement(Integer indPrimaryPlacement) {
		this.indPrimaryPlacement = indPrimaryPlacement;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTempQuantity() {
		return tempQuantity;
	}

	public void setTempQuantity(BigDecimal tempQuantity) {
		this.tempQuantity = tempQuantity;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Integer getIndDpf() {
		return indDpf;
	}

	public void setIndDpf(Integer indDpf) {
		this.indDpf = indDpf;
	}


	public BigDecimal getRealAmountRate() {
		return realAmountRate;
	}


	public void setRealAmountRate(BigDecimal realAmountRate) {
		this.realAmountRate = realAmountRate;
	}


	public BigDecimal getWarrantyCause() {
		return warrantyCause;
	}


	public void setWarrantyCause(BigDecimal warrantyCause) {
		this.warrantyCause = warrantyCause;
	}


	public BigDecimal getTermInterestRate() {
		return termInterestRate;
	}


	public void setTermInterestRate(BigDecimal termInterestRate) {
		this.termInterestRate = termInterestRate;
	}


	public BigDecimal getCashPricePercentage() {
		return cashPricePercentage;
	}


	public void setCashPricePercentage(BigDecimal cashPricePercentage) {
		this.cashPricePercentage = cashPricePercentage;
	}


	public BigDecimal getRealCashPricePercentage() {
		return realCashPricePercentage;
	}


	public void setRealCashPricePercentage(BigDecimal realCashPricePercentage) {
		this.realCashPricePercentage = realCashPricePercentage;
	}
	
}