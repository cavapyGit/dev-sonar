package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum OtcOperationStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/04/2013
 */
//1863
//1864
//1865
//1866
//1867
public enum SirtexOperationStateType {

	/** The sell registered. */
	REGISTERED(new Integer(1862), "REGISTRADA"),
	
	/** The sell aproved. */
	APROVED(new Integer(1863), "APROBADA"),
	
	/** The anulate. */
	ANULATE (new Integer(1864), "ANULADA"),
	
	/** The buy aproved. */
	REVIEWED (new Integer(1865), "REVISADA"),
	
	/** The buy confirmed. */
	CONFIRMED (new Integer(1866), "CONFIRMADA"),
	
	/** The rejected. */
	REJECTED (new Integer(1867), "RECHAZADA"),	
	
	/** The cash settled. */
	CASH_SETTLED (new Integer(615), "LIQUIDADA CONTADO"),
	
	/** The term settled. - Liquidada Plazp */
	TERM_SETTLED (new Integer(616), "LIQUIDADA PLAZO");
			
	/** The code. */
	private Integer code;
	
	/** The descripcion. */
	private String descripcion;

	/**
	 * Instantiates a new otc operation state type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private SirtexOperationStateType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/** The Constant list. */
	public static final List<SirtexOperationStateType> list = new ArrayList<SirtexOperationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SirtexOperationStateType> lookup = new HashMap<Integer, SirtexOperationStateType>();
	static {
		for (SirtexOperationStateType s : EnumSet.allOf(SirtexOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static List<SirtexOperationStateType> listSomeElements(SirtexOperationStateType... transferSecuritiesTypeParams){
		List<SirtexOperationStateType> retorno = new ArrayList<SirtexOperationStateType>();
		for(SirtexOperationStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	
	public static SirtexOperationStateType get(Integer codigo) {
		return lookup.get(codigo);
	}
}