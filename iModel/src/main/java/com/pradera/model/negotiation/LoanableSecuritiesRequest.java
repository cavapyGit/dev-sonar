package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
/**
 * The persistent class for the LOANABLE_SECURITIES_REQUEST database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name = LoanableSecuritiesRequest.LOANABLE_SECURITIES_REQUEST, query = "Select distinct loanableSecuritiesRequest From LoanableSecuritiesRequest loanableSecuritiesRequest WHERE loanableSecuritiesRequest.requestState = :state"),
	@NamedQuery(name = LoanableSecuritiesRequest.LOANABLE_SECURITIES_REQUEST_STATE, query = "SELECT new com.pradera.model.negotiation.LoanableSecuritiesRequest(I.idLoanableSecuritiesRequestPk,I.requestState) From LoanableSecuritiesRequest I WHERE I.idLoanableSecuritiesRequestPk = :idLoanableSecuritiesRequestPkParam")
})
@Entity
@Table(name = "LOANABLE_SECURITIES_REQUEST")
public class LoanableSecuritiesRequest implements Serializable ,Auditable{

	private static final long serialVersionUID = 1L;
	
	/** The Constant LOANABLE_SECURITIES_REQUEST. */
	public static final String  LOANABLE_SECURITIES_REQUEST= "LoanableSecuritiesRequest.searchAllLoanableSecuritiesRequest";
	
	/** The Constant LOANABLE_SECURITIES_REQUEST_STATE. */
	public static final String  LOANABLE_SECURITIES_REQUEST_STATE= "LoanableSecuritiesRequest.searchLoanableSecuritiesRequestState";
	
	@Id
	@SequenceGenerator(name = "LOANABLE_SECURITIES_REQUESTPK_GENERATOR", sequenceName = "SQ_ID_LOANABLE_REQUEST_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOANABLE_SECURITIES_REQUESTPK_GENERATOR")
	@Column(name = "ID_LOANABLE_REQUEST_PK")
	private Long idLoanableSecuritiesRequestPk;
	
	//bi-directional many-to-one association to Holder
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;

	//bi-directional many-to-one association to Participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;
	
    //bi-directional many-to-one association to Holder Account
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK", referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccount;
    
    /** The request Type. */
    @Column(name="REQUEST_TYPE")
    private Long requestType;
    
    /** The request State. */
    @Column(name="REQUEST_STATE")
    private Long requestState;
    
	/** The interest Rate. */
	@Column(name="INTEREST_RATE")
	private BigDecimal interestRate;
	
	/** The validity Days. */
	@Column(name="VALIDITY_DAYS")
	private Integer validityDays;
	
	/** The last Modify App. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last Modify Date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The last Modify Ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTER_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name = "REGISTER_USER")
	private String registryUser;
	
	/** The approval date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "APPROVAL_DATE")
	private Date approvalDate;

	/** The approval user. */
	@Column(name = "APPROVAL_USER")
	private String approvalUser;
	
	/** The annulment date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ANNULMENT_DATE")
	private Date annulmentDate;

	/** The annulment user. */
	@Column(name = "ANNULMENT_USER")
	private String annulmentUser;
	
	/** The confirm date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;

	/** The confirm user. */
	@Column(name = "CONFIRM_USER")
	private String confirmUser;
	
	/** The reject date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REJECT_DATE")
	private Date rejectDate;

	/** The reject user. */
	@Column(name = "REJECT_USER")
	private String rejectUser;
	
	/** The annulment motive. */
	@Column(name="ANNULMENT_MOTIVE")
	private Integer annulmentMotive;
	
	/** The reject motive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/** The annulment other motive. */
	@Column(name = "ANNULMENT_OTHER_MOTIVE")
	private String annulmentOtherMotive;

	/** The reject other motive. */
	@Column(name = "REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;
	
	// bi-directional many-to-one association to Security
	/** The LoanableS ecurities Request. */
	@OneToMany(fetch=FetchType.LAZY ,mappedBy = "loanableSecuritiesRequest")
	private List<LoanableSecuritiesOperation> lstLoanableSecuritiesOperation;
	
	// bi-directional many-to-one association to Security
	/** The Loanable Unblock Operation. */
	@OneToMany(fetch=FetchType.LAZY ,mappedBy = "loanableSecuritiesRequest")
	private List<LoanableUnblockOperation> lstLoanableUnblockOperation;
	
	@Transient
	private String requestNameState;
	
	public LoanableSecuritiesRequest(){
		
	}

	public LoanableSecuritiesRequest(Long idLoanableSecuritiesRequestPk,
			Long requestState) {
		this.idLoanableSecuritiesRequestPk = idLoanableSecuritiesRequestPk;
		this.requestState = requestState;
	}

	public Long getIdLoanableSecuritiesRequestPk() {
		return idLoanableSecuritiesRequestPk;
	}

	public void setIdLoanableSecuritiesRequestPk(Long idLoanableSecuritiesRequestPk) {
		this.idLoanableSecuritiesRequestPk = idLoanableSecuritiesRequestPk;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Long getRequestType() {
		return requestType;
	}

	public void setRequestType(Long requestType) {
		this.requestType = requestType;
	}

	public Long getRequestState() {
		return requestState;
	}

	public void setRequestState(Long requestState) {
		this.requestState = requestState;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getValidityDays() {
		return validityDays;
	}

	public void setValidityDays(Integer validityDays) {
		this.validityDays = validityDays;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public List<LoanableSecuritiesOperation> getLstLoanableSecuritiesOperation() {
		return lstLoanableSecuritiesOperation;
	}

	public void setLstLoanableSecuritiesOperation(
			List<LoanableSecuritiesOperation> lstLoanableSecuritiesOperation) {
		this.lstLoanableSecuritiesOperation = lstLoanableSecuritiesOperation;
	}

	public List<LoanableUnblockOperation> getLstLoanableUnblockOperation() {
		return lstLoanableUnblockOperation;
	}

	public void setLstLoanableUnblockOperation(
			List<LoanableUnblockOperation> lstLoanableUnblockOperation) {
		this.lstLoanableUnblockOperation = lstLoanableUnblockOperation;
	}

	public String getRequestNameState() {
		return requestNameState;
	}

	public void setRequestNameState(String requestNameState) {
		this.requestNameState = requestNameState;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalUser() {
		return approvalUser;
	}

	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	public Date getAnnulmentDate() {
		return annulmentDate;
	}

	public void setAnnulmentDate(Date annulmentDate) {
		this.annulmentDate = annulmentDate;
	}

	public String getAnnulmentUser() {
		return annulmentUser;
	}

	public void setAnnulmentUser(String annulmentUser) {
		this.annulmentUser = annulmentUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	public Integer getAnnulmentMotive() {
		return annulmentMotive;
	}

	public void setAnnulmentMotive(Integer annulmentMotive) {
		this.annulmentMotive = annulmentMotive;
	}

	public Integer getRejectMotive() {
		return rejectMotive;
	}

	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	public String getAnnulmentOtherMotive() {
		return annulmentOtherMotive;
	}

	public void setAnnulmentOtherMotive(String annulmentOtherMotive) {
		this.annulmentOtherMotive = annulmentOtherMotive;
	}

	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
		 detailsMap.put("loanableSecuritiesOperation", lstLoanableSecuritiesOperation);
		 detailsMap.put("loanableUnblockOperation", lstLoanableUnblockOperation);
        return detailsMap;
	}
}
