package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.type.OtcOperationStateType;


/**
 * The persistent class for the TRADE_OPERATION database table.
 * @author PraderaTechnologies.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = TradeOperation.OPERATION_STATE, query = "Select new com.pradera.model.negotiation.TradeOperation(tra.idTradeOperationPk,tra.operationState) From TradeOperation tra WHERE tra.idTradeOperationPk = :idTradeOperation")
})
@Table(name="TRADE_OPERATION")
public class TradeOperation implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String OPERATION_STATE = "tradeOperation.state";

	/** The id trade operation pk. */
	@Id
	@SequenceGenerator(name="TRADE_OPERATION_GENERATOR", sequenceName="SQ_ID_TRADE_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRADE_OPERATION_GENERATOR")
	@Column(name="ID_TRADE_OPERATION_PK")
	private Long idTradeOperationPk;

	/** The aprovalDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="APROVAL_DATE")
	private Date aprovalDate;

	/** The aprovalUser. */
	@Column(name="APROVAL_USER")
	private String aprovalUser;

	/** The confirmDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;

	/** The confirmUser. */
	@Column(name="CONFIRM_USER")
	private String confirmUser;

	/** The lastModifyApp. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The lastModifyDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The lastModifyIp. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The lastModifyUser. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The operationType. */
	@Column(name="OPERATION_TYPE")
	private Long operationType;

	/** The registerDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

	/** The registerUser. */
	@Column(name="REGISTER_USER")
	private String registerUser;

	/** The rejectDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REJECT_DATE")
	private Date rejectDate;

	/** The rejectMotive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;

	/** The rejectMotiveOther. */
	@Column(name="REJECT_MOTIVE_OTHER")
	private String rejectMotiveOther;

	/** The rejectUser. */
	@Column(name="REJECT_USER")
	private String rejectUser;
	
	/** The rejectDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="ANNULATE_DATE")
	private Date annulateDate;

	/** The rejectMotive. */
	@Column(name="ANNULATE_MOTIVE")
	private Integer annulateMotive;

	/** The rejectMotiveOther. */
	@Column(name="ANNULATE_MOTIVE_OTHER")
	private String annulateMotiveOther;

	/** The rejectUser. */
	@Column(name="ANNULATE_USER")
	private String annulateUser;

	/** The reviewDate. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REVIEW_DATE")
	private Date reviewDate;

	/** The reviewUser. */
	@Column(name="REVIEW_USER")
	private String reviewUser;

	/** The state. */
	@Column(name="OPERATION_STATE")
	private Integer operationState;

	@Column(name="AUTHORIZE_USER")
	private String authorizeUser;	
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="AUTHORIZE_DATE")
	private Date authorizeDate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_REQUEST_FK")
	private TradeRequest tradeRequest;
		
	/**
	 * @return the idTradeOperationPk
	 */
	public Long getIdTradeOperationPk() {
		return idTradeOperationPk;
	}


	/**
	 * @param idTradeOperationPk the idTradeOperationPk to set
	 */
	public void setIdTradeOperationPk(Long idTradeOperationPk) {
		this.idTradeOperationPk = idTradeOperationPk;
	}


	/**
	 * @return the aprovalDate
	 */
	public Date getAprovalDate() {
		return aprovalDate;
	}


	/**
	 * @param aprovalDate the aprovalDate to set
	 */
	public void setAprovalDate(Date aprovalDate) {
		this.aprovalDate = aprovalDate;
	}


	/**
	 * @return the aprovalUser
	 */
	public String getAprovalUser() {
		return aprovalUser;
	}


	/**
	 * @param aprovalUser the aprovalUser to set
	 */
	public void setAprovalUser(String aprovalUser) {
		this.aprovalUser = aprovalUser;
	}


	/**
	 * @return the confirmDate
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}


	/**
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}


	/**
	 * @return the confirmUser
	 */
	public String getConfirmUser() {
		return confirmUser;
	}


	/**
	 * @param confirmUser the confirmUser to set
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}


	

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}


	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}


	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}




	/**
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}


	/**
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}


	/**
	 * @return the registerUser
	 */
	public String getRegisterUser() {
		return registerUser;
	}


	/**
	 * @param registerUser the registerUser to set
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}


	/**
	 * @return the rejectDate
	 */
	public Date getRejectDate() {
		return rejectDate;
	}


	/**
	 * @param rejectDate the rejectDate to set
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}


	
	/**
	 * @return the rejectMotiveOther
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}


	/**
	 * @param rejectMotiveOther the rejectMotiveOther to set
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}


	/**
	 * @return the rejectUser
	 */
	public String getRejectUser() {
		return rejectUser;
	}


	/**
	 * @param rejectUser the rejectUser to set
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}


	/**
	 * @return the reviewDate
	 */
	public Date getReviewDate() {
		return reviewDate;
	}


	/**
	 * @param reviewDate the reviewDate to set
	 */
	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}


	/**
	 * @return the reviewUser
	 */
	public String getReviewUser() {
		return reviewUser;
	}


	/**
	 * @param reviewUser the reviewUser to set
	 */
	public void setReviewUser(String reviewUser) {
		this.reviewUser = reviewUser;
	}

	


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	public Long getOperationType() {
		return operationType;
	}


	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}


	public Integer getRejectMotive() {
		return rejectMotive;
	}


	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}


	public Integer getOperationState() {
		return operationState;
	}


	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}


	/** The Default Constructor */
    public TradeOperation() {
    }


	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Gets the otc operation state.
	 *
	 * @return the otc operation state
	 */
	public String getOtcOperationState(){
		return OtcOperationStateType.lookup.get(operationState).getDescripcion();
	}


	public Date getAnnulateDate() {
		return annulateDate;
	}


	public void setAnnulateDate(Date annulateDate) {
		this.annulateDate = annulateDate;
	}


	public Integer getAnnulateMotive() {
		return annulateMotive;
	}


	public void setAnnulateMotive(Integer annulateMotive) {
		this.annulateMotive = annulateMotive;
	}


	public String getAnnulateMotiveOther() {
		return annulateMotiveOther;
	}


	public void setAnnulateMotiveOther(String annulateMotiveOther) {
		this.annulateMotiveOther = annulateMotiveOther;
	}


	public String getAnnulateUser() {
		return annulateUser;
	}


	public void setAnnulateUser(String annulateUser) {
		this.annulateUser = annulateUser;
	}


	public TradeOperation(Long idTradeOperationPk, Integer operationState) {
		super();
		this.idTradeOperationPk = idTradeOperationPk;
		this.operationState = operationState;
	}
	
	public TradeOperation(Long idTradeOperationPk) {
		super();
		this.idTradeOperationPk = idTradeOperationPk;
	}


	public TradeRequest getTradeRequest() {
		return tradeRequest;
	}


	public void setTradeRequest(TradeRequest tradeRequest) {
		this.tradeRequest = tradeRequest;
	}


	public String getAuthorizeUser() {
		return authorizeUser;
	}


	public void setAuthorizeUser(String authorizeUser) {
		this.authorizeUser = authorizeUser;
	}


	public Date getAuthorizeDate() {
		return authorizeDate;
	}


	public void setAuthorizeDate(Date authorizeDate) {
		this.authorizeDate = authorizeDate;
	}
}