package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum CashCalendarType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/07/2013
 */
public enum CashCalendarType {

	/** The util days. */
	UTIL_DAYS(new Integer(1), "DIAS UTILES"),
	
	/** The calendar days. */
	CALENDAR_DAYS(new Integer(2), "DIAS CALENDARIO");
	
	/** The code. */
	private Integer code;
	
	/** The descripcion. */
	private String descripcion;

	/**
	 * Instantiates a new cash calendar type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private CashCalendarType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/** The Constant list. */
	public static final List<CashCalendarType> list = new ArrayList<CashCalendarType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, CashCalendarType> lookup = new HashMap<Integer, CashCalendarType>();
	static {
		for (CashCalendarType s : EnumSet.allOf(CashCalendarType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the cash calendar type
	 */
	public static CashCalendarType get(Integer code){
		return lookup.get(code);
	}
}