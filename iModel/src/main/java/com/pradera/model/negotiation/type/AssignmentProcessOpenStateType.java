package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AssignmentProcessOpenStateType {

	/* issue 1161: ESTADOS DE UNA APERTURA DE CIERRE DE ASIGNACIONES GRAL  */
	REGISTRADO (Integer.valueOf(2521), "REGISTRADO"),
	CONFIRMADO(Integer.valueOf(2522), "CONFIRMADO"),
	RECHAZADO(Integer.valueOf(2523), "RECHAZADO");
			
	/** The code. */
	private Integer code;
	
	private String descripcion;

	private AssignmentProcessOpenStateType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static final List<AssignmentProcessOpenStateType> list = new ArrayList<AssignmentProcessOpenStateType>();
	public static final Map<Integer, AssignmentProcessOpenStateType> lookup = new HashMap<Integer, AssignmentProcessOpenStateType>();
	static {
		for (AssignmentProcessOpenStateType s : EnumSet.allOf(AssignmentProcessOpenStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
