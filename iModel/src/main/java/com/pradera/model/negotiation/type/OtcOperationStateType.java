package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum OtcOperationStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/04/2013
 */
public enum OtcOperationStateType {

	/** The sell registered. */
	REGISTERED(new Integer(1127), "REGISTRADA"),
	
	/** The sell aproved. */
	APROVED(new Integer(1128), "APROBADA"),
	
	/** The anulate. */
	ANULATE (new Integer(1129), "ANULADA"),
	
	/** The buy aproved. */
	REVIEWED (new Integer(1130), "REVISADA"),
	
	/** The buy confirmed. */
	CONFIRMED (new Integer(1131), "CONFIRMADA"),
	
	/** The rejected. */
	REJECTED (new Integer(1132), "RECHAZADA"),
	
	/** The canceled. */
	CANCELED (new Integer(1133), "CANCELADA"),
	
	CASH_CANCELED (new Integer(613), "CANCELADA CONTADO"),
	
	TERM_CANCELED (new Integer(1722), "CANCELADA PLAZO"),
	
	AUTHORIZED (new Integer(2899), "AUTORIZADA");
			
	/** The code. */
	private Integer code;
	
	/** The descripcion. */
	private String descripcion;

	/**
	 * Instantiates a new otc operation state type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private OtcOperationStateType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/** The Constant list. */
	public static final List<OtcOperationStateType> list = new ArrayList<OtcOperationStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, OtcOperationStateType> lookup = new HashMap<Integer, OtcOperationStateType>();
	static {
		for (OtcOperationStateType s : EnumSet.allOf(OtcOperationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static List<OtcOperationStateType> listSomeElements(OtcOperationStateType... transferSecuritiesTypeParams){
		List<OtcOperationStateType> retorno = new ArrayList<OtcOperationStateType>();
		for(OtcOperationStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	
	public static OtcOperationStateType get(Integer codigo) {
		return lookup.get(codigo);
	}
}