package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AssignmentRequestStateType {

	REGISTERED (new Integer(621), "REGISTRADA"),
	CONFIRMED (new Integer(622), "CONFIRMADA"),
	CANCLED (new Integer(713),"ANULADA"),
	PENDING (new Integer(0), "PENDIENTE");
	
	/** The code. */
	private Integer code;
	
	private String descripcion;

	private AssignmentRequestStateType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static final List<AssignmentRequestStateType> list = new ArrayList<AssignmentRequestStateType>();
	public static final Map<Integer, AssignmentRequestStateType> lookup = new HashMap<Integer, AssignmentRequestStateType>();
	static {
		for (AssignmentRequestStateType s : EnumSet.allOf(AssignmentRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
