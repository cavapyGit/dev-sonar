package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the MODALITY_GROUP_DETAIL database table.
 * 
 */
@Entity
@Table(name="MODALITY_GROUP_DETAIL")
public class ModalityGroupDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MODALITY_GROUP_DETAIL_PK")
	private  Long idModalityGroupDetailPK;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MOD_GROUP_STATE")
	private Integer modGroupState;
	
	/** The modalityGroup. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MODALITY_GROUP_FK")
	private ModalityGroup modalityGroup;
	
	/** The negotiation mechanism. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK")
	private NegotiationMechanism negotiationMechanism;
	
	/** The negotiation modality. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK")
	private NegotiationModality negotiationModality;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK",insertable=false,updatable=false),
		@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK",insertable=false,updatable=false)
		})
	private MechanismModality mechanismModality;
	
	
    /**
     * Default Constructor
     */
    public ModalityGroupDetail() {
    }

	public Long getIdModalityGroupDetailPK() {
		return idModalityGroupDetailPK;
	}

	public void setIdModalityGroupDetailPK(Long idModalityGroupDetailPK) {
		this.idModalityGroupDetailPK = idModalityGroupDetailPK;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}


	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}


	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	
	/**
	 * @return the modalityGroup
	 */
	public ModalityGroup getModalityGroup() {
		return modalityGroup;
	}


	/**
	 * @param modalityGroup the modalityGroup to set
	 */
	public void setModalityGroup(ModalityGroup modalityGroup) {
		this.modalityGroup = modalityGroup;
	}


	/**
	 * @return the negotiationMechanism
	 */
	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}


	/**
	 * @param negotiationMechanism the negotiationMechanism to set
	 */
	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}


	/**
	 * @return the negotiationModality
	 */
	public NegotiationModality getNegotiationModality() {
		return negotiationModality;
	}


	/**
	 * @param negotiationModality the negotiationModality to set
	 */
	public void setNegotiationModality(NegotiationModality negotiationModality) {
		this.negotiationModality = negotiationModality;
	}


	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	public Integer getModGroupState() {
		return modGroupState;
	}


	public void setModGroupState(Integer modGroupState) {
		this.modGroupState = modGroupState;
	}


	public MechanismModality getMechanismModality() {
		return mechanismModality;
	}


	public void setMechanismModality(MechanismModality mechanismModality) {
		this.mechanismModality = mechanismModality;
	}
}