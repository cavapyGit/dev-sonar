package com.pradera.model.negotiation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.splitcoupons.SplitCouponOperation;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the HOLDER_ACCOUNT_OPERATION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/09/2013
 */
@Entity
@Table(name="COUPON_TRADE_REQUEST")
public class CouponTradeRequest implements Serializable,Auditable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder account operation pk. */
	@Id
	@SequenceGenerator(name="COUPON_TRADE_REQUEST_GENERATOR", sequenceName="SQ_ID_TRADE_COUPON_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COUPON_TRADE_REQUEST_GENERATOR")
	@Column(name="ID_TRADE_COUPON_PK")
	private Long idTradeCouponPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROGRAM_INTEREST_FK")
	private ProgramInterestCoupon programInterestCoupon;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SPLIT_DETAIL_FK")
	private SplitCouponOperation splitCouponDetail;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_REQUEST_FK")
	private TradeRequest tradeRequest;

	/** The cash amount. */
	@Column(name="ORIGIN_AVAILABLE_BALANCE")
	private BigDecimal originAvailableBalance;

	/** The commission amount. */
	@Column(name="SUPPLIED_QUANTITY")
	private BigDecimal suppliedQuantity;
	
	@Column(name="IND_SUPPLIED_ACCEPTED")
	private Integer indSuppliedAccepted;
	
	@Column(name="IND_IS_CAPITAL")
	private Integer indIsCapital;
	
	@Column(name="ACCEPTED_QUANTITY")
	private BigDecimal acceptedQuantity;
	
	@Column(name="COUPON_NUMBER")
	private Integer couponNumber;

    /** The confirm date. */
    @Temporal( TemporalType.DATE)
	@Column(name="EXPIRED_DATE")
	private Date expiredDate;
    
    @Column(name="DAYS_OF_LIFE")
	private Integer daysOfLife;
    
    @Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;
    
    @Column(name="REAL_CASH_AMOUNT")
	private BigDecimal realCashAmount;
    
    @Column(name="TERM_AMOUNT")
	private BigDecimal termAmount;

    /** The register date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
    
    @Column(name="REGISTRY_USER")
    private String registryUser;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private Integer pendientDays;
//	
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_SECURITY_COUPON_FK")
//	private Security idSecurityCouponFk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK", referencedColumnName="ID_MECHANISM_OPERATION_PK")
	private MechanismOperation mechanismOperation;
	
	@Transient
	private Boolean suppliedOk;
	
	@Transient
	private Integer indSettlementOpe = ComponentConstant.ZERO;
	
	@Transient
	private String securityCouponDescription;
	
	@Transient
	private String securityCouponBcb;
	
	@Transient
	private String couponDescription;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public ProgramInterestCoupon getProgramInterestCoupon() {
		return programInterestCoupon;
	}

	public void setProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon) {
		this.programInterestCoupon = programInterestCoupon;
	}

	public SplitCouponOperation getSplitCouponDetail() {
		return splitCouponDetail;
	}

	public void setSplitCouponDetail(SplitCouponOperation splitCouponDetail) {
		this.splitCouponDetail = splitCouponDetail;
	}

	public BigDecimal getOriginAvailableBalance() {
		return originAvailableBalance;
	}

	public void setOriginAvailableBalance(BigDecimal originAvailableBalance) {
		this.originAvailableBalance = originAvailableBalance;
	}

	public BigDecimal getSuppliedQuantity() {
		return suppliedQuantity;
	}

	public void setSuppliedQuantity(BigDecimal suppliedQuantity) {
		this.suppliedQuantity = suppliedQuantity;
	}

	public BigDecimal getAcceptedQuantity() {
		return acceptedQuantity;
	}

	public void setAcceptedQuantity(BigDecimal acceptedQuantity) {
		this.acceptedQuantity = acceptedQuantity;
	}

	public Integer getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Integer getDaysOfLife() {
		return daysOfLife;
	}

	public void setDaysOfLife(Integer daysOfLife) {
		this.daysOfLife = daysOfLife;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public BigDecimal getRealCashAmount() {
		return realCashAmount;
	}

	public void setRealCashAmount(BigDecimal realCashAmount) {
		this.realCashAmount = realCashAmount;
	}

	public BigDecimal getTermAmount() {
		return termAmount;
	}

	public void setTermAmount(BigDecimal termAmount) {
		this.termAmount = termAmount;
	}

	public String getCouponDescription() {
		return couponDescription;
	}

	public void setCouponDescription(String couponDescription) {
		this.couponDescription = couponDescription;
	}

	public Integer getIndSuppliedAccepted() {
		return indSuppliedAccepted;
	}

	public void setIndSuppliedAccepted(Integer indSuppliedAccepted) {
		this.indSuppliedAccepted = indSuppliedAccepted;
	}

	public Boolean getSuppliedOk() {
		return suppliedOk;
	}

	public void setSuppliedOk(Boolean suppliedOk) {
		this.suppliedOk = suppliedOk;
	}

//	public Security getIdSecurityCouponFk() {
//		return idSecurityCouponFk;
//	}
//
//	public void setIdSecurityCouponFk(Security idSecurityCouponFk) {
//		this.idSecurityCouponFk = idSecurityCouponFk;
//	}

	public Long getIdTradeCouponPk() {
		return idTradeCouponPk;
	}

	public void setIdTradeCouponPk(Long idTradeCouponPk) {
		this.idTradeCouponPk = idTradeCouponPk;
	}

	public TradeRequest getTradeRequest() {
		return tradeRequest;
	}

	public void setTradeRequest(TradeRequest tradeRequest) {
		this.tradeRequest = tradeRequest;
	}

	public String getSecurityCouponDescription() {
		return securityCouponDescription;
	}

	public void setSecurityCouponDescription(String couponNumberDesc) {
		this.securityCouponDescription = couponNumberDesc;
	}

	public Integer getIndIsCapital() {
		return indIsCapital;
	}

	public void setIndIsCapital(Integer indIsCapital) {
		this.indIsCapital = indIsCapital;
	}

	public String getSecurityCouponBcb() {
		return securityCouponBcb;
	}

	public void setSecurityCouponBcb(String securityCouponBcb) {
		this.securityCouponBcb = securityCouponBcb;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public Integer getIndSettlementOpe() {
		return indSettlementOpe;
	}

	public void setIndSettlementOpe(Integer indSettlementOpe) {
		this.indSettlementOpe = indSettlementOpe;
	}

	public Integer getPendientDays() {
		return pendientDays;
	}

	public void setPendientDays(Integer pendientDays) {
		this.pendientDays = pendientDays;
	}
}