package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the NEGOTIATION_MODALITY database table.
 * 
 */
@Entity
@Cacheable(true)
@Table(name="NEGOTIATION_MODALITY")
public class NegotiationModality implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id negotiation modality pk. */
	@Id
	@SequenceGenerator(name="NEGOTIATION_MODALITY_IDNEGOTIATIONMODALITYPK_GENERATOR", sequenceName="SQ_ID_NEGOTIATION_MODALITY_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NEGOTIATION_MODALITY_IDNEGOTIATIONMODALITYPK_GENERATOR")
	@Column(name="ID_NEGOTIATION_MODALITY_PK")
	private Long idNegotiationModalityPk;

	/** The cash settlement days. */
	@Column(name="CASH_MIN_SETTLEMENT_DAYS")
	private Integer cashMinSettlementDays;
	
	@Column(name="CASH_MAX_SETTLEMENT_DAYS")
	private Integer cashMaxSettlementDays;
	
	@Column(name="IND_CASH_CALLENDAR")
	private Integer indCashCallendar;
	
	@Column(name="IND_TERM_CALLENDAR")
	private Integer indTermCallendar;
	
	/** The term max settlement days. */
	@Column(name="TERM_MAX_SETTLEMENT_DAYS")
	private Integer termMaxSettlementDays;
	
	/** The term min settlement days. */
	@Column(name="TERM_MIN_SETTLEMENT_DAYS")
	private Integer termMinSettlementDays;

	@Column(name="IND_MARGIN_GUARANTEE")
	private Integer indMarginGuarantee;
	
	@Column(name="IND_REPORTING_BALANCE")
	private Integer indReportingBalance;
	
	@Column(name="IND_PRINCIPAL_GUARANTEE")
	private Integer indPrincipalGuarantee;
	
	/** The ind term settlement. */
	@Column(name="IND_TERM_SETTLEMENT")
	private Integer indTermSettlement;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The modality code. */
	@Column(name="MODALITY_CODE")
	private String modalityCode;

	/** The modality name. */
	@Column(name="MODALITY_NAME")
	private String modalityName;

	/** The modality state. */
	@Column(name="MODALITY_STATE")
	private Integer modalityState;
	
	/** The term settlement days. */
	@Column(name="INSTRUMENT_TYPE")
	private Integer instrumentType;
	
	/** The mechanism modalities. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="negotiationModality", fetch=FetchType.LAZY)
	private List<MechanismModality> mechanismModalities;

	/** The referenceModality. */
	@ManyToOne(fetch= FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name="ID_REFERENCE_MODALITY_FK")
  	private NegotiationModality referenceModality;

	@Column(name="IND_CASH_STOCK_BLOCK")
	private Integer indCashStockBlock;
	
	@Column(name="IND_TERM_STOCK_BLOCK")
	private Integer indTermStockBlock;
	
	@Column(name="IND_PRIMARY_PLACEMENT")
	private Integer indPrimaryPlacement;
	
	@Transient
	List<NegotiationMechanism> negotiationMechanisms;
	
    /**
	 * @return the referenceModality
	 */
	public NegotiationModality getReferenceModality() {
		return referenceModality;
	}

	/**
	 * @param referenceModality the referenceModality to set
	 */
	public void setReferenceModality(NegotiationModality referenceModality) {
		this.referenceModality = referenceModality;
	}

	/**
	 * @return the termMaxSettlementDays
	 */
	public Integer getTermMaxSettlementDays() {
		return termMaxSettlementDays;
	}

	/**
	 * @param termMaxSettlementDays the termMaxSettlementDays to set
	 */
	public void setTermMaxSettlementDays(Integer termMaxSettlementDays) {
		this.termMaxSettlementDays = termMaxSettlementDays;
	}

	/**
	 * @return the termMinSettlementDays
	 */
	public Integer getTermMinSettlementDays() {
		return termMinSettlementDays;
	}

	/**
	 * @param termMinSettlementDays the termMinSettlementDays to set
	 */
	public void setTermMinSettlementDays(Integer termMinSettlementDays) {
		this.termMinSettlementDays = termMinSettlementDays;
	}

	/**
	 * @return the instrumentType
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * @param instrumentType the instrumentType to set
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
     * The Constructor.
     */
    public NegotiationModality() {
    }
    
    /**
     * Instantiates a new negotiation modality.
     *
     * @param idNegotiationModalityPk the id negotiation modality pk
     */
    public NegotiationModality(Long idNegotiationModalityPk) {
    	this.idNegotiationModalityPk = idNegotiationModalityPk;
    }
    
    public NegotiationModality(Long idNegotiationModalityPk, String modalityName) {
    	this.idNegotiationModalityPk = idNegotiationModalityPk;
    	this.modalityName = modalityName;
    }

	/**
	 * Gets the id negotiation modality pk.
	 *
	 * @return the id negotiation modality pk
	 */
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}

	/**
	 * Sets the id negotiation modality pk.
	 *
	 * @param idNegotiationModalityPk the id negotiation modality pk
	 */
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}

	/**
	 * Gets the cash settlement days.
	 *
	 * @return the cash settlement days
	 */
	public Integer getCashMaxSettlementDays() {
		return cashMaxSettlementDays;
	}

	/**
	 * Sets the cash settlement days.
	 *
	 * @param cashSettlementDays the cash settlement days
	 */
	public void setCashMaxSettlementDays(Integer cashSettlementDays) {
		this.cashMaxSettlementDays = cashSettlementDays;
	}

	/**
	 * Gets the ind term settlement.
	 *
	 * @return the ind term settlement
	 */
	public Integer getIndTermSettlement() {
		return indTermSettlement;
	}

	/**
	 * Sets the ind term settlement.
	 *
	 * @param indTermSettlement the ind term settlement
	 */
	public void setIndTermSettlement(Integer indTermSettlement) {
		this.indTermSettlement = indTermSettlement;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the modality code.
	 *
	 * @return the modality code
	 */
	public String getModalityCode() {
		return modalityCode;
	}

	/**
	 * Sets the modality code.
	 *
	 * @param modalityCode the modality code
	 */
	public void setModalityCode(String modalityCode) {
		this.modalityCode = modalityCode;
	}

	/**
	 * Gets the modality name.
	 *
	 * @return the modality name
	 */
	public String getModalityName() {
		return modalityName;
	}

	/**
	 * Sets the modality name.
	 *
	 * @param modalityName the modality name
	 */
	public void setModalityName(String modalityName) {
		this.modalityName = modalityName;
	}

	/**
	 * Gets the modality state.
	 *
	 * @return the modality state
	 */
	public Integer getModalityState() {
		return modalityState;
	}

	/**
	 * Sets the modality state.
	 *
	 * @param modalityState the modality state
	 */
	public void setModalityState(Integer modalityState) {
		this.modalityState = modalityState;
	}

	/**
	 * Gets the mechanism modalities.
	 *
	 * @return the mechanism modalities
	 */
	public List<MechanismModality> getMechanismModalities() {
		return mechanismModalities;
	}

	/**
	 * Sets the mechanism modalities.
	 *
	 * @param mechanismModalities the mechanism modalities
	 */
	public void setMechanismModalities(List<MechanismModality> mechanismModalities) {
		this.mechanismModalities = mechanismModalities;
	}

	public Integer getCashMinSettlementDays() {
		return cashMinSettlementDays;
	}

	public void setCashMinSettlementDays(Integer cashMinSettlementDays) {
		this.cashMinSettlementDays = cashMinSettlementDays;
	}

	public Integer getIndMarginGuarantee() {
		return indMarginGuarantee;
	}

	public void setIndMarginGuarantee(Integer indMarginGuarantee) {
		this.indMarginGuarantee = indMarginGuarantee;
	}

	public Integer getIndCashCallendar() {
		return indCashCallendar;
	}

	public void setIndCashCallendar(Integer indCashCallendar) {
		this.indCashCallendar = indCashCallendar;
	}

	public Integer getIndTermCallendar() {
		return indTermCallendar;
	}

	public void setIndTermCallendar(Integer indTermCallendar) {
		this.indTermCallendar = indTermCallendar;
	}

	public Integer getIndCashStockBlock() {
		return indCashStockBlock;
	}

	public void setIndCashStockBlock(Integer indCashStockBlock) {
		this.indCashStockBlock = indCashStockBlock;
	}

	public Integer getIndTermStockBlock() {
		return indTermStockBlock;
	}

	public void setIndTermStockBlock(Integer indTermStockBlock) {
		this.indTermStockBlock = indTermStockBlock;
	}

	public List<NegotiationMechanism> getNegotiationMechanisms() {
		return negotiationMechanisms;
	}

	public void setNegotiationMechanisms(
			List<NegotiationMechanism> negotiationMechanisms) {
		this.negotiationMechanisms = negotiationMechanisms;
	}

	public Integer getIndReportingBalance() {
		return indReportingBalance;
	}

	public void setIndReportingBalance(Integer indReportingBalance) {
		this.indReportingBalance = indReportingBalance;
	}

	public Integer getIndPrincipalGuarantee() {
		return indPrincipalGuarantee;
	}

	public void setIndPrincipalGuarantee(Integer indPrincipalGuarantee) {
		this.indPrincipalGuarantee = indPrincipalGuarantee;
	}

	public Integer getIndPrimaryPlacement() {
		return indPrimaryPlacement;
	}

	public void setIndPrimaryPlacement(Integer indPrimaryPlacement) {
		this.indPrimaryPlacement = indPrimaryPlacement;
	}		

}