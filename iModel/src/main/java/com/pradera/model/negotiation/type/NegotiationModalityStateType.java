package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.component.type.ParameterOperationType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum NegotiationModalityStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2013
 */
public enum NegotiationModalityStateType {

	ACTIVE(Integer.valueOf(1),"ACTIVO"),
	INNACTIVE(Integer.valueOf(2),"INACTIVO");

	private Integer code;
	private String value;
	
	public final static List<NegotiationModalityStateType> list=new ArrayList<NegotiationModalityStateType>();
	public final static Map<Integer, NegotiationModalityStateType> lookup= new HashMap<Integer, NegotiationModalityStateType>();
	
	static{
		for(NegotiationModalityStateType s : EnumSet.allOf(NegotiationModalityStateType.class)){
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	private NegotiationModalityStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	

}