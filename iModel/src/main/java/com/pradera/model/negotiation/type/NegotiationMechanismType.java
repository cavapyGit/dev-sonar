package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum OtcOperationStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/04/2013
 */
public enum NegotiationMechanismType {

	/** The bolsa. */
	BOLSA(new Long(1), "BVEDV", "BOLSA", "BOL"),	
	
	/** The hac. */
	HAC(new Long(2),"MH", "TGN", "TGN"),
	
	/** The otc. */
	OTC(new Long(3),"OTC", "OTC", "OTC"),
	
	/** The bc. */
	BC(new Long(4), "BIC", "BIC", "BIC"), 
	
	/** The sirtex. */
	SIRTEX(new Long(5), "SIRTEX", "SIRTEX", "SIRTEX");
			
	/** The code. */
	private Long code;
	
	/** The value. */
	private String value;
	
	/** The descripcion. */
	private String descripcion;
	
	/** The formal desc. */
	private String formalDesc;

	/**
	 * Instantiates a new otc operation state type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param description the description
	 * @param formalDesc the formal desc
	 */
	 private NegotiationMechanismType(Long code, String value, String description, String formalDesc) {
		this.code = code;
		this.value = value;
		this.descripcion = description;
		this.formalDesc = formalDesc;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	
	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the formal desc.
	 *
	 * @return the formal desc
	 */
	public String getFormalDesc() {
		return formalDesc;
	}

	/**
	 * Sets the formal desc.
	 *
	 * @param formalDesc the new formal desc
	 */
	public void setFormalDesc(String formalDesc) {
		this.formalDesc = formalDesc;
	}

	/** The Constant list. */
	public static final List<NegotiationMechanismType> list = new ArrayList<NegotiationMechanismType>();
	
	/** The Constant lookup. */
	public static final Map<Long, NegotiationMechanismType> lookup = new HashMap<Long, NegotiationMechanismType>();
	static {
		for (NegotiationMechanismType s : EnumSet.allOf(NegotiationMechanismType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the negotiation mechanism type
	 */
	public static NegotiationMechanismType get(Long code){
		return lookup.get(code);
	}
}