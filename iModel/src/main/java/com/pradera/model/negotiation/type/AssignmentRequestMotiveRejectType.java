package com.pradera.model.negotiation.type;

public enum AssignmentRequestMotiveRejectType {
	
	REQUEST_WITH_WRONG_DATA(new Integer(2072)),
	OTHERS_MOTIVES(new Integer(2073));
	
	private Integer code;
	
	private AssignmentRequestMotiveRejectType(Integer code){
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
}
