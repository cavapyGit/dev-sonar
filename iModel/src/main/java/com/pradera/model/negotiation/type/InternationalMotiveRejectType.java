package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Enum InternationalOperationStateType.
 * @author PraderaTechnologies.
 */
public enum InternationalMotiveRejectType {
	
	OTRO(new Integer(1156), "OTRO");
			
	/** The code. */
	private Integer code;
	/** The descripcion. */
	private String descripcion;
	/** List of McnStateOperationType. */
	public static final List<InternationalMotiveRejectType> list = new ArrayList<InternationalMotiveRejectType>();
	
	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/** The Constructor 
     *  @param Integer code,String descripcion
     */
	private InternationalMotiveRejectType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}
	/** Adding Enum to  McnStateOperationType List*/
	public static final Map<Integer, InternationalMotiveRejectType> lookup = new HashMap<Integer, InternationalMotiveRejectType>();
	static {
		for (InternationalMotiveRejectType s : EnumSet.allOf(InternationalMotiveRejectType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the international operation state type
	 */
	public static InternationalMotiveRejectType get(Integer codigo) {
		return lookup.get(codigo);
	}
}