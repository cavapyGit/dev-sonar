package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.model.funds.InstitutionCashAccount;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the NEGOTIATION_MECHANISM database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/02/2013
 */
@NamedQueries({
	@NamedQuery(name = NegotiationMechanism.SEARCH_NEGOTIATIONS, query = " Select N From NegotiationMechanism N"),
	@NamedQuery(name = NegotiationMechanism.NEGOTIATION_MECHANISM_BY_STATE, 
			query = "Select distinct NegMech From NegotiationMechanism NegMech Join Fetch NegMech.mechanismModalities MechMod Where NegMech.stateMechanism = :stateMechanismPrm And MechMod.stateMechanismModality = :stateMechanismModalityPrm Order By NegMech.mechanismCode, MechMod.id.idNegotiationModalityPk"),
	@NamedQuery(name = NegotiationMechanism.MECHANISM_BY_STATE, query = " Select N From NegotiationMechanism N where N.stateMechanism=:statePrm")
})
@Entity
@Cacheable(true)
@Table(name="NEGOTIATION_MECHANISM")
public class NegotiationMechanism implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant SEARCH_NEGOTIATIONS. */
	public static final String SEARCH_NEGOTIATIONS = "NegotiationMechanism.searchNegotiations";
	
	/** The Constant NEGOTIATION_MECHANISM_BY_STATE. */
	public static final String NEGOTIATION_MECHANISM_BY_STATE = "NegotiationMechanism.negotiationMechanismsByState";
	
	/** The Constant NEGOTIATION_MECHANISM_BY_STATE. */
	public static final String MECHANISM_BY_STATE = "NegotiationMechanism.MechanismsByState";

	/** The id negotiation mechanism pk. */
	@Id
	@SequenceGenerator(name="NEGOTIATION_MECHANISM_IDNEGOTIATIONMECHANISMPK_GENERATOR", sequenceName="SQ_ID_NEGOTIATION_MECHANISM_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NEGOTIATION_MECHANISM_IDNEGOTIATIONMECHANISMPK_GENERATOR")
	@Column(name="ID_NEGOTIATION_MECHANISM_PK")
	private Long idNegotiationMechanismPk;

	/** The description. */
	@Column(name="DESCRIPTION")
	private String description;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The mechanism code. */
	@Column(name="MECHANISM_CODE")	
	private String mechanismCode;
	
	/** The mechanism name. */
	@Column(name="MECHANISM_NAME")	
	private String mechanismName;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The state mechanism. */
	@Column(name="MECHANISM_STATE")
	private Integer stateMechanism;
	
	/** The ind mcn process. */
	@Column(name="IND_MCN_PROCESS")
	private Integer indMcnProcess;
	
	/** The ind block date. */
	@Column(name="IND_BLOCK_DATE")
	private Integer indBlockDate;	

	//bi-directional many-to-one association to SecurityNegotiationMechanism
	/** The security negotiation mechanisms. */
	@OneToMany(mappedBy="negotiationMechanism")
	private List<InstitutionCashAccount> institutionCashAccount;
	
	/** The mechanism modalities. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="negotiationMechanism", fetch=FetchType.LAZY)
	private List<MechanismModality> mechanismModalities;
	
	/** The selected. */
	@Transient
	private boolean selected;
	/** The disabled */
	@Transient
	private boolean disabled;
	
	/** The selected modalities. */
	@Transient
	private List<String> selectedModalities;

    /**
     * Instantiates a new negotiation mechanism.
     */
    public NegotiationMechanism() {
    }
    
    

	public NegotiationMechanism(Long idNegotiationMechanismPk,
			String description, boolean selected, boolean disabled) {
		super();
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
		this.description = description;
		this.selected = selected;
		this.disabled = disabled;
	}



	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}

	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPk the new id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the mechanism code.
	 *
	 * @return the mechanism code
	 */
	public String getMechanismCode() {
		return mechanismCode;
	}

	/**
	 * Sets the mechanism code.
	 *
	 * @param mechanismCode the new mechanism code
	 */
	public void setMechanismCode(String mechanismCode) {
		this.mechanismCode = mechanismCode;
	}

	/**
	 * Gets the mechanism name.
	 *
	 * @return the mechanism name
	 */
	public String getMechanismName() {
		return mechanismName;
	}

	/**
	 * Sets the mechanism name.
	 *
	 * @param mechanismName the new mechanism name
	 */
	public void setMechanismName(String mechanismName) {
		this.mechanismName = mechanismName;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the state mechanism.
	 *
	 * @return the state mechanism
	 */
	public Integer getStateMechanism() {
		return stateMechanism;
	}

	/**
	 * Sets the state mechanism.
	 *
	 * @param stateMechanism the new state mechanism
	 */
	public void setStateMechanism(Integer stateMechanism) {
		this.stateMechanism = stateMechanism;
	}

	/**
	 * Gets the ind mcn process.
	 *
	 * @return the ind mcn process
	 */
	public Integer getIndMcnProcess() {
		return indMcnProcess;
	}

	/**
	 * Sets the ind mcn process.
	 *
	 * @param indMcnProcess the new ind mcn process
	 */
	public void setIndMcnProcess(Integer indMcnProcess) {
		this.indMcnProcess = indMcnProcess;
	}

	/**
	 * Gets the ind block date.
	 *
	 * @return the ind block date
	 */
	public Integer getIndBlockDate() {
		return indBlockDate;
	}

	/**
	 * Sets the ind block date.
	 *
	 * @param indBlockDate the new ind block date
	 */
	public void setIndBlockDate(Integer indBlockDate) {
		this.indBlockDate = indBlockDate;
	}

	/**
	 * Gets the security negotiation mechanisms.
	 *
	 * @return the security negotiation mechanisms
	 */
//	public List<SecurityNegotiationMechanism> getSecurityNegotiationMechanisms() {
//		return securityNegotiationMechanisms;
//	}

	/**
	 * Sets the security negotiation mechanisms.
	 *
	 * @param securityNegotiationMechanisms the new security negotiation mechanisms
	 */
//	public void setSecurityNegotiationMechanisms(
//			List<SecurityNegotiationMechanism> securityNegotiationMechanisms) {
//		this.securityNegotiationMechanisms = securityNegotiationMechanisms;
//	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * Gets the mechanism modalities.
	 *
	 * @return the mechanism modalities
	 */
	public List<MechanismModality> getMechanismModalities() {
		return mechanismModalities;
	}

	/**
	 * Sets the mechanism modalities.
	 *
	 * @param mechanismModalities the new mechanism modalities
	 */
	public void setMechanismModalities(List<MechanismModality> mechanismModalities) {
		this.mechanismModalities = mechanismModalities;
	}

	/**
	 * Gets the selected modalities.
	 *
	 * @return the selected modalities
	 */
	public List<String> getSelectedModalities() {
		return selectedModalities;
	}

	/**
	 * Sets the selected modalities.
	 *
	 * @param selectedModalities the new selected modalities
	 */
	public void setSelectedModalities(List<String> selectedModalities) {
		this.selectedModalities = selectedModalities;
	}

	public List<InstitutionCashAccount> getInstitutionCashAccount() {
		return institutionCashAccount;
	}

	public void setInstitutionCashAccount(
			List<InstitutionCashAccount> institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	/**
	 * @param idNegotiationMechanismPk
	 * @param mechanismCode
	 * @param mechanismName
	 */
	public NegotiationMechanism(Long idNegotiationMechanismPk,
			String mechanismCode, String mechanismName) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
		this.mechanismCode = mechanismCode;
		this.mechanismName = mechanismName;
	}
	/**
	 * @param idNegotiationMechanismPk
	 */
	public NegotiationMechanism(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}
	
	public NegotiationMechanism(Long idNegotiationMechanismPk, String mechanismName) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
		this.mechanismName = mechanismName;
	}
}