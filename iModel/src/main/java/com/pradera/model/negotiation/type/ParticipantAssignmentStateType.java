package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ParticipantAssignmentStateType {

	NOTOPENED (new Integer(675), "NO ABIERTO"),
	OPENED (new Integer(629), "ABIERTO"),
	CLOSED (new Integer(630), "CERRADO");
			
	/** The code. */
	private Integer code;
	
	private String descripcion;

	private ParticipantAssignmentStateType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static final List<ParticipantAssignmentStateType> list = new ArrayList<ParticipantAssignmentStateType>();
	public static final Map<Integer, ParticipantAssignmentStateType> lookup = new HashMap<Integer, ParticipantAssignmentStateType>();
	static {
		for (ParticipantAssignmentStateType s : EnumSet.allOf(ParticipantAssignmentStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
