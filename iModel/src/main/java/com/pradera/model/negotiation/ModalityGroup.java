package com.pradera.model.negotiation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.settlement.SettlementProcess;


/**
 * The persistent class for the MODALITY_GROUP database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name = ModalityGroup.MODALITY_GROUP_LIST , query = "Select modalityGroup from ModalityGroup modalityGroup")
})

@Entity
@Table(name="MODALITY_GROUP")
public class ModalityGroup implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	public final static String MODALITY_GROUP_LIST = "ModalityGroup.getModalityGroups";
	
	@Id
	@Column(name="ID_MODALITY_GROUP_PK")
	private Long idModalityGroupPk;

	private String description;

	@Column(name="GROUP_NAME")
	private String groupName;

	@Column(name="LAST_MODIFY_APP")
	private Long lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MODALITY_GROUP_CODE")
	private String modalityGroupCode;

	@Column(name="SETTLEMENT_SCHEMA")
	private Integer settlementSchema;

	@Column(name="GROUP_STATE")
	private Integer groupState;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK")
	private NegotiationMechanism negotiationMechanism;
	
	@OneToMany(mappedBy="modalityGroup")
	private List<InstitutionCashAccount> institutionCashAccount;
	
	@OneToMany(mappedBy="modalityGroup")
	private List<ModalityGroupDetail> modalityGroupDetail;
	
	@OneToMany(mappedBy="modalityGroup")
	private List<SettlementProcess> settlementsProcessList;

	@Column(name="IND_SEND_SETTLEMENT_FUNDS")
	private Integer indSendSettlementFunds;
	
	@Column(name="IND_AUTO_SETTLEMENT")
	private Integer indAutoSettlement;
	
	/** 
     *  The participantOperations. 
     *  bi-directional many-to-one association to ParticipantOperation
     *  
     * */
	@Transient
	private List<ParticipantOperation> participantOperations;

    public ModalityGroup() {
    }

	public Long getIdModalityGroupPk() {
		return this.idModalityGroupPk;
	}

	public void setIdModalityGroupPk(Long idModalityGroupPk) {
		this.idModalityGroupPk = idModalityGroupPk;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getModalityGroupCode() {
		return this.modalityGroupCode;
	}

	public void setModalityGroupCode(String modalityGroupCode) {
		this.modalityGroupCode = modalityGroupCode;
	}
	public List<InstitutionCashAccount> getInstitutionCashAccount() {
		return institutionCashAccount;
	}
	public void setInstitutionCashAccount(
			List<InstitutionCashAccount> institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}

	/**
	 * @return the participantOperations
	 */
	public List<ParticipantOperation> getParticipantOperations() {
		return participantOperations;
	}

	/**
	 * @param participantOperations the participantOperations to set
	 */
	public void setParticipantOperations(
			List<ParticipantOperation> participantOperations) {
		this.participantOperations = participantOperations;
	}

	public Integer getSettlementSchema() {
		return settlementSchema;
	}

	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	public Integer getGroupState() {
		return groupState;
	}

	public void setGroupState(Integer groupState) {
		this.groupState = groupState;
	}

	/**
	 * Gets the modality group detail.
	 *
	 * @return the modality group detail
	 */
	public List<ModalityGroupDetail> getModalityGroupDetail() {
		return modalityGroupDetail;
	}
	/**
	 * Sets the modality group detail.
	 *
	 * @param modalityGroupDetail the new modality group detail
	 */
	public void setModalityGroupDetail(List<ModalityGroupDetail> modalityGroupDetail) {
		this.modalityGroupDetail = modalityGroupDetail;
	}
	/**
	 * Gets the settlements process list.
	 *
	 * @return the settlements process list
	 */
	public List<SettlementProcess> getSettlementsProcessList() {
		return settlementsProcessList;
	}
	/**
	 * Sets the settlements process list.
	 *
	 * @param settlementsProcessList the new settlements process list
	 */
	public void setSettlementsProcessList(List<SettlementProcess> settlementsProcessList) {
		this.settlementsProcessList = settlementsProcessList;
	}
	/**
	 * @param idModalityGroupPk
	 */
	public ModalityGroup(Long idModalityGroupPk) {
		this.idModalityGroupPk = idModalityGroupPk;
	}

	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}

	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}

	public Integer getIndSendSettlementFunds() {
		return indSendSettlementFunds;
	}

	public void setIndSendSettlementFunds(Integer indSendSettlementFunds) {
		this.indSendSettlementFunds = indSendSettlementFunds;
	}

	public Integer getIndAutoSettlement() {
		return indAutoSettlement;
	}

	public void setIndAutoSettlement(Integer indAutoSettlement) {
		this.indAutoSettlement = indAutoSettlement;
	}
}