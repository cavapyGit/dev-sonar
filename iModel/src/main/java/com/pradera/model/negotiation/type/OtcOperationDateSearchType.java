package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum OtcOperationStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/04/2013
 */
public enum OtcOperationDateSearchType {

	/** The operation date. */
	OPERATION_DATE(new Integer(1124), "FEC OPERACION"),
	
	/** The cash settlem date. */
	CASH_SETTLEM_DATE(new Integer(1125), "FEC LIQ CONTADO"),
	
	/** The term settlem date. */
	TERM_SETTLEM_DATE(new Integer(1126), "FEC LIQ PLAZO");
			
	/** The code. */
	private Integer code;
	
	/** The descripcion. */
	private String descripcion;

	/**
	 * Instantiates a new otc operation state type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private OtcOperationDateSearchType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/** The Constant list. */
	public static final List<OtcOperationDateSearchType> list = new ArrayList<OtcOperationDateSearchType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, OtcOperationDateSearchType> lookup = new HashMap<Integer, OtcOperationDateSearchType>();
	static {
		for (OtcOperationDateSearchType s : EnumSet.allOf(OtcOperationDateSearchType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static OtcOperationDateSearchType get(Integer code) {
		return lookup.get(code);
	}
}