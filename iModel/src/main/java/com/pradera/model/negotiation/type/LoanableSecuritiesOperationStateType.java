package com.pradera.model.negotiation.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum LoanableSecuritiesOperationStateType {
	
	REGISTERED (Long.valueOf(1739),"REGISTRADO"),
	APPROVED  (Long.valueOf(1740),"APROBADO"),
	CANCELLED (Long.valueOf(1741),"ANULADO"),
	CONFIRMED (Long.valueOf(1784),"CONFIRMADO"),	
	REJECTED (Long.valueOf(1785),"RECHAZADO"),
	EXPIRED (Long.valueOf(1799),"VENCIDO");
	
	private Long code;
	private String value;
	
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private LoanableSecuritiesOperationStateType(Long code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<LoanableSecuritiesOperationStateType> list = new ArrayList<LoanableSecuritiesOperationStateType>();
	public static final Map<Long, LoanableSecuritiesOperationStateType> lookup = new HashMap<Long, LoanableSecuritiesOperationStateType>();
	static {
        for (LoanableSecuritiesOperationStateType d : LoanableSecuritiesOperationStateType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
}
