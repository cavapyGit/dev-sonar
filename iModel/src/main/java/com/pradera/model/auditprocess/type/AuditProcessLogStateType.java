package com.pradera.model.auditprocess.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum AuditProcessLogStateType {
	
	REGISTERED(Integer.valueOf(1),"REGISTRADO"),
	PROCESSING(Integer.valueOf(2),"PROCESANDO"),
	FINISHED(Integer.valueOf(3),"FINALIZADO"),
	ERROR(Integer.valueOf(4),"ERROR");
	
	private Integer code;	
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private AuditProcessLogStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final Map<Integer, AuditProcessLogStateType> lookup = new HashMap<Integer, AuditProcessLogStateType>();
	
	static {
		for (AuditProcessLogStateType s : EnumSet.allOf(AuditProcessLogStateType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	public static AuditProcessLogStateType get(Integer code) {
		return lookup.get(code);
	}
}
