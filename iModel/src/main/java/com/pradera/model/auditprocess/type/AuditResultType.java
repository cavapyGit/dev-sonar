package com.pradera.model.auditprocess.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum AuditResultType {
	BALANCE(new Integer(0),"CUADRADO"),
	UNBALANCE(new Integer(1),"DESCUADRADO");
	
	private Integer code;	
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private AuditResultType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final Map<Integer, AuditResultType> lookup = new HashMap<Integer, AuditResultType>();
	
	static {
		for (AuditResultType s : EnumSet.allOf(AuditResultType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	public static AuditResultType get(Integer code) {
		return lookup.get(code);
	}
}
