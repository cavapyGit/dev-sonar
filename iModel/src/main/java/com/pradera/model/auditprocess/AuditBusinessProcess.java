package com.pradera.model.auditprocess;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="AUDIT_BUSINESS_PROCESS")
public class AuditBusinessProcess implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID_AUDIT_BUSINESS_PROCESS_PK")
	private Long idAuditBusinessProcessPk;
	
	@Column(name="AUDIT_STATE")
	private Integer auditState;
	
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="PROCESS_NAME")
	private String processName;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
    
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;	
	
	public Long getIdAuditBusinessProcessPk() {
		return idAuditBusinessProcessPk;
	}

	public void setIdAuditBusinessProcessPk(Long idAuditBusinessProcessPk) {
		this.idAuditBusinessProcessPk = idAuditBusinessProcessPk;
	}

	public Integer getAuditState() {
		return auditState;
	}

	public void setAuditState(Integer auditState) {
		this.auditState = auditState;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	
}
