package com.pradera.model.auditprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.auditprocess.type.AuditPeriodType;
import com.pradera.model.auditprocess.type.AuditProcessLogStateType;
import com.pradera.model.auditprocess.type.AuditProcessType;
import com.pradera.model.auditprocess.type.AuditResultType;
import com.pradera.model.auditprocess.type.AuditType;

@Entity
@Table(name="AUDIT_PROCESS_LOG")
public class AuditProcessLog implements Serializable,Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="AUDITPROCESSLOG_GENERATOR", sequenceName="SQ_ID_AUDIT_PROCESS_LOG_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUDITPROCESSLOG_GENERATOR")	
	@Column(name="ID_AUDIT_PROCESS_LOG_PK")
	private Long idAuditProcessLogPk;

	@Column(name="PROCESS_STATE")
	private Integer processState;
	
	@Column(name="PERIOD")
	private Integer period;
	
	@Column(name="PROCESS_TYPE")
	private Integer processType;
	
	@Column(name="AUDIT_TYPE")
	private Integer auditType;
	
	@Column(name="AUDIT_DATE")
	@Temporal(TemporalType.DATE)
	private Date auditDate;
	
	@Column(name="START_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime;
	
	@Column(name="FINISH_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date finishTime;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_AUDIT_PROCESS_RULES_FK")
	private AuditProcessRules auditProcessRules;
	
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="REGISTRY_DATE")
	@Temporal(TemporalType.DATE)
	private Date registryDate;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="LAST_MODIFY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@OneToMany(mappedBy="auditProcessLog")
	private List<AuditProcessResult> auditProcessResultList;
	
	@Column(name="TOTAL_AUDIT_ROWS")
	private Long totalAuditRows;
	
	@Column(name="CONSISTENT_ROWS")
	private Long consistentRows;
	
	@Column(name="INCONSISTENT_ROWS")
	private Long inconsistentRows;
	
	@Transient
	private String procedureName;

	@Transient
	private String straightenName;
	
	public AuditProcessLog(){
		super();
	}
	
	public AuditProcessLog(
		Long idAuditProcessLogPk, String straightenName,
		Date startTime, Date finishTime,
		Date registryDate, Integer auditType,
		Integer processType, Integer period,
		Integer processState ,Long inconsistentRows) {
		super();
		this.idAuditProcessLogPk = idAuditProcessLogPk;
		this.straightenName = straightenName;
		this.startTime = startTime;
		this.finishTime = finishTime;
		this.registryDate = registryDate;
		this.auditType = auditType;
		this.processType = processType;
		this.period = period;
		this.processState = processState;
		this.inconsistentRows= inconsistentRows;
	}

	public String getAuditTypeDesc(){
		return AuditType.get(auditType).getValue();
	}
	
	/**
	 * @return the straightenName
	 */
	public String getStraightenName() {
		return straightenName;
	}

	/**
	 * @param straightenName the straightenName to set
	 */
	public void setStraightenName(String straightenName) {
		this.straightenName = straightenName;
	}

	/**
	 * @return the procedureName
	 */
	public String getProcedureName() {
		return procedureName;
	}

	/**
	 * @param procedureName the procedureName to set
	 */
	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}

	/**
	 * @return the auditType
	 */
	public Integer getAuditType() {
		return auditType;
	}

	/**
	 * @param auditType the auditType to set
	 */
	public void setAuditType(Integer auditType) {
		this.auditType = auditType;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public Integer getProcessType() {
		return processType;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	public Long getIdAuditProcessLogPk() {
		return idAuditProcessLogPk;
	}

	public void setIdAuditProcessLogPk(Long idAuditProcessLogPk) {
		this.idAuditProcessLogPk = idAuditProcessLogPk;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public AuditProcessRules getAuditProcessRules() {
		return auditProcessRules;
	}

	public void setAuditProcessRules(AuditProcessRules auditProcessRules) {
		this.auditProcessRules = auditProcessRules;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	
	

	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate the auditDate to set
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public List<AuditProcessResult> getAuditProcessResultList() {
		return auditProcessResultList;
	}

	public void setAuditProcessResultList(
			List<AuditProcessResult> auditProcessResultList) {
		this.auditProcessResultList = auditProcessResultList;
	}
	
	public String getPeriodDesc(){
		return AuditPeriodType.get(period).getValue();
	}
	
	public String getProcessTypeDesc(){
		return AuditProcessType.get(processType).getValue();
	}
	
	public String getProcessStateDesc(){
		return AuditProcessLogStateType.get(processState).getValue();
	}

	public Long getTotalAuditRows() {
		return totalAuditRows;
	}

	public void setTotalAuditRows(Long totalAuditRows) {
		this.totalAuditRows = totalAuditRows;
	}

	public Long getConsistentRows() {
		return consistentRows;
	}

	public void setConsistentRows(Long consistentRows) {
		this.consistentRows = consistentRows;
	}

	public Long getInconsistentRows() {
		return inconsistentRows;
	}

	public void setInconsistentRows(Long inconsistentRows) {
		this.inconsistentRows = inconsistentRows;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
           
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}