package com.pradera.model.auditprocess.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum  StateSapResponseType {
	
	CORRECT(new Integer(2535),"CORRECTO"),
	DUPLICATE(new Integer(2536),"DUPLICADO"),
	ERROR(new Integer(2537),"ERROR"),
	RE_ENVIO(new Integer(2540),"RE ENVIADO");
	
	private Integer code;	
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private StateSapResponseType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final Map<Integer, AuditType> lookup = new HashMap<Integer, AuditType>();
	
	static {
		for (AuditType s : EnumSet.allOf(AuditType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	public static AuditType get(Integer code) {
		return lookup.get(code);
	}
}
