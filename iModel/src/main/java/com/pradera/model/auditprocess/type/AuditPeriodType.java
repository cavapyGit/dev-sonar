package com.pradera.model.auditprocess.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum AuditPeriodType {
	DAILY(new Integer(0),"DIARIO"),
	MONTHLY(new Integer(1),"MENSUAL"),
	ANNUAL(new Integer(2),"ANUAL");
	
	private Integer code;	
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private AuditPeriodType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final Map<Integer, AuditPeriodType> lookup = new HashMap<Integer, AuditPeriodType>();
	
	static {
		for (AuditPeriodType s : EnumSet.allOf(AuditPeriodType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	public static AuditPeriodType get(Integer code) {
		return lookup.get(code);
	}
}
