package com.pradera.model.auditprocess.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum AuditType {
	INTERNAL(new Integer(0),"INTERNO"),
	EXTERNAL(new Integer(1),"EXTERNO");
	
	private Integer code;	
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private AuditType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final Map<Integer, AuditType> lookup = new HashMap<Integer, AuditType>();
	
	static {
		for (AuditType s : EnumSet.allOf(AuditType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	public static AuditType get(Integer code) {
		return lookup.get(code);
	}
}
