package com.pradera.model.auditprocess.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


public enum AuditProcessType {
	MANUAL(new Integer(0),"MANUAL"),
	AUTOMATIC(new Integer(1),"AUTOMATICO");
	
	private Integer code;	
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private AuditProcessType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final Map<Integer, AuditProcessType> lookup = new HashMap<Integer, AuditProcessType>();
	
	static {
		for (AuditProcessType s : EnumSet.allOf(AuditProcessType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	public static AuditProcessType get(Integer code) {
		return lookup.get(code);
	}
}
