package com.pradera.model.auditprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="AUDIT_STRAIGHTEN_BUSINESS")
public class AuditStraightenBusiness implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID_STRAIGHTEN_BUSINESS_PK")
	private Long idStraightenBusinessPk;
	
	@Column(name="AUDIT_STATE")
	private Integer auditState;
	
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="STRAIGHTEN_NAME")
	private String straightenName;
	
	@Column(name="PROCEDURE_NAME")
	private String procedureName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_AUDIT_BUSINESS_PROCESS_FK")
	private AuditBusinessProcess auditBusinessProcess;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
    
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@OneToMany(mappedBy = "auditStraightenProcess", fetch = FetchType.LAZY)
	private List<AuditHeaderResult> auditHeaderResult;
	
	@OneToMany(mappedBy = "auditStraightenBusiness", fetch = FetchType.LAZY)
	private List<AuditProcessRules> auditProcessRules;
	
	@Transient
	private boolean blDaily;
	@Transient
	private boolean blMonthly;
	@Transient
	private boolean blAnnual;
	@Transient
	private Integer processType;
	@Transient
	private Date executeTime;
	@Transient
	private Integer audit;
	@Transient
	private boolean blManual;
	
	public Long getIdStraightenBusinessPk() {
		return idStraightenBusinessPk;
	}

	public void setIdStraightenBusinessPk(Long idStraightenBusinessPk) {
		this.idStraightenBusinessPk = idStraightenBusinessPk;
	}

	public Integer getAuditState() {
		return auditState;
	}

	public void setAuditState(Integer auditState) {
		this.auditState = auditState;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	

	public String getProcedureName() {
		return procedureName;
	}

	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}

	public AuditBusinessProcess getAuditBusinessProcess() {
		return auditBusinessProcess;
	}

	public void setAuditBusinessProcess(AuditBusinessProcess auditBusinessProcess) {
		this.auditBusinessProcess = auditBusinessProcess;
	}

	public boolean isBlDaily() {
		return blDaily;
	}

	public void setBlDaily(boolean blDaily) {
		this.blDaily = blDaily;
	}

	public boolean isBlMonthly() {
		return blMonthly;
	}

	public void setBlMonthly(boolean blMonthly) {
		this.blMonthly = blMonthly;
	}

	public boolean isBlAnnual() {
		return blAnnual;
	}

	public void setBlAnnual(boolean blAnnual) {
		this.blAnnual = blAnnual;
	}

	public Integer getProcessType() {
		return processType;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	public Date getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(Date executeTime) {
		this.executeTime = executeTime;
	}

	public Integer getAudit() {
		return audit;
	}

	public void setAudit(Integer audit) {
		this.audit = audit;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getStraightenName() {
		return straightenName;
	}

	public void setStraightenName(String straightenName) {
		this.straightenName = straightenName;
	}

	public List<AuditHeaderResult> getAuditHeaderResult() {
		return auditHeaderResult;
	}

	public void setAuditHeaderResult(List<AuditHeaderResult> auditHeaderResult) {
		this.auditHeaderResult = auditHeaderResult;
	}

	public boolean isBlManual() {
		return blManual;
	}

	public void setBlManual(boolean blManual) {
		this.blManual = blManual;
	}
	
}
