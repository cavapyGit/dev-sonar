package com.pradera.model.auditprocess;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="AUDIT_HEADER_RESULT")
public class AuditHeaderResult implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID_AUDIT_HEADER_RESULT_PK")
	private Long idAuditHeaderResultPk;
	
	@Column(name="ORDER_HEADER")
	private Integer order;
	
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="HEADER_NAME")
	private String headerName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_STRAIGHTEN_BUSINESS_FK")
	private AuditStraightenBusiness auditStraightenProcess;
	
	@Column(name="COLUMN_NAME")
	private String columnName;
	
	@Column(name="JAVA_ATTRIBUTE")
	private String javaAttribute;

	public Long getIdAuditHeaderResultPk() {
		return idAuditHeaderResultPk;
	}

	public void setIdAuditHeaderResultPk(Long idAuditHeaderResultPk) {
		this.idAuditHeaderResultPk = idAuditHeaderResultPk;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getHeaderName() {
		return headerName;
	}

	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

	public AuditStraightenBusiness getAuditStraightenProcess() {
		return auditStraightenProcess;
	}

	public void setAuditStraightenProcess(
			AuditStraightenBusiness auditStraightenProcess) {
		this.auditStraightenProcess = auditStraightenProcess;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getJavaAttribute() {
		return javaAttribute;
	}

	public void setJavaAttribute(String javaAttribute) {
		this.javaAttribute = javaAttribute;
	}
}
