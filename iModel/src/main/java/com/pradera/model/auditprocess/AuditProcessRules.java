package com.pradera.model.auditprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.auditprocess.type.AuditProcessType;
import com.pradera.model.auditprocess.type.AuditType;

@Entity
@Table(name="AUDIT_PROCESS_RULES")
public class AuditProcessRules implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="AUDITPROCESSRULES_GENERATOR", sequenceName="SQ_ID_AUDIT_PROCESS_RULES_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUDITPROCESSRULES_GENERATOR")	
	@Column(name="ID_AUDIT_PROCESS_RULES_PK")
	private Long idAuditProcessRulesPk;
	
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="PROCESS_STATE")
	private Integer processState;	
	
	@Column(name="EXECUTE_TIME")
	private Date executeTime;
	
	@Column(name="AUDIT_TYPE")
	private Integer auditType;
	
	@Column(name="IND_ANNUAL")
	private Integer indAnnual;
	
	@Column(name="IND_DAILY")
	private Integer indDaily;
	
	@Column(name="IND_MONTHLY")
	private Integer indMonthly;
	
	@Column(name="PROCESS_TYPE")
	private Integer processType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_STRAIGHTEN_BUSINESS_FK")
	private AuditStraightenBusiness auditStraightenBusiness;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_AUDIT_BUSINESS_PROCESS_FK")
	private AuditBusinessProcess auditBusinessProcess;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
    
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;	
	
	@Transient
	private Date nextTimeOut;
	
	/**
	 * @return the nextTimeOut
	 */
	public Date getNextTimeOut() {
		return nextTimeOut;
	}

	/**
	 * @param nextTimeOut the nextTimeOut to set
	 */
	public void setNextTimeOut(Date nextTimeOut) {
		this.nextTimeOut = nextTimeOut;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public AuditStraightenBusiness getAuditStraightenBusiness() {
		return auditStraightenBusiness;
	}

	public void setAuditStraightenBusiness(
			AuditStraightenBusiness auditStraightenBusiness) {
		this.auditStraightenBusiness = auditStraightenBusiness;
	}

	public Long getIdAuditProcessRulesPk() {
		return idAuditProcessRulesPk;
	}

	public void setIdAuditProcessRulesPk(Long idAuditProcessRulesPk) {
		this.idAuditProcessRulesPk = idAuditProcessRulesPk;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Date getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(Date executeTime) {
		this.executeTime = executeTime;
	}

	public Integer getAuditType() {
		return auditType;
	}

	public void setAuditType(Integer auditType) {
		this.auditType = auditType;
	}

	public Integer getIndAnnual() {
		return indAnnual;
	}

	public void setIndAnnual(Integer indAnnual) {
		this.indAnnual = indAnnual;
	}

	public Integer getIndDaily() {
		return indDaily;
	}

	public void setIndDaily(Integer indDaily) {
		this.indDaily = indDaily;
	}

	public Integer getIndMonthly() {
		return indMonthly;
	}

	public void setIndMonthly(Integer indMonthly) {
		this.indMonthly = indMonthly;
	}

	public Integer getProcessType() {
		return processType;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	public AuditBusinessProcess getAuditBusinessProcess() {
		return auditBusinessProcess;
	}

	public void setAuditBusinessProcess(AuditBusinessProcess auditBusinessProcess) {
		this.auditBusinessProcess = auditBusinessProcess;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();            
        }		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getProcessTypeDesc(){
		return AuditProcessType.get(processType).getValue();
	}
	
	public String getAuditTypeDesc(){
		return AuditType.get(auditType).getValue();
	}
}
