package com.pradera.model.auditprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.BenefitPaymentAllocation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismOperation;

@Entity
@Table(name="AUDIT_PROCESS_RESULT")
public class AuditProcessResult implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID_AUDIT_PROCESS_RESULT_PK")
	private Long idAuditProcessResultPk;
	
	@Column(name="AUDIT_PROCESS_DATE")
	private Date auditProcessDate;
	
	@Column(name="AUDIT_BALANCE_TYPE")
	private Integer auditBalanceType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_AUDIT_PROCESS_LOG_FK",insertable=false,updatable=false)
	private AuditProcessLog auditProcessLog;
	
	//bi-directional many-to-one association to HolderAccount
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK",insertable=false,updatable=false)
	private HolderAccount holderAccount;
	
	//bi-directional many-to-one association to Participant
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",insertable=false,updatable=false)
	private Participant participant;
	
	//bi-directional many-to-one association to Security
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",insertable=false,updatable=false)
	private Security security;
	
	@Column(name="ACCREDITATION_BALANCE")
	private BigDecimal accreditationBalance;

	@Column(name="AVAILABLE_BALANCE") 
	private BigDecimal availableBalance ;

	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance ;

	@Column(name="BORROWER_BALANCE")
	private BigDecimal borrowerBalance ;

	@Column(name="PURCHASE_BALANCE")
	private BigDecimal purchaseBalance ;

	@Column(name="LENDER_BALANCE")
	private BigDecimal lenderBalance ;

	@Column(name="LOANABLE_BALANCE")
	private BigDecimal loanableBalance ;
	
	@Column(name="MARGIN_BALANCE")
	private BigDecimal marginBalance ;
	
	@Column(name="OPPOSITION_BALANCE")
	private BigDecimal oppositionBalance ;

	@Column(name="OTHER_BLOCK_BALANCE")
	private BigDecimal otherBlockBalance ;

	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance ;

	@Column(name="REPORTED_BALANCE")
	private BigDecimal reportedBalance ;

	@Column(name="REPORTING_BALANCE")
	private BigDecimal reportingBalance ;

	@Column(name="RESERVE_BALANCE")
	private BigDecimal reserveBalance ;

	@Column(name="SALE_BALANCE")
	private BigDecimal saleBalance ;

	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance ;

	@Column(name="TRANSIT_BALANCE")
	private BigDecimal transitBalance ;
	
	@Column(name="GRANTED_BALANCE")
	private BigDecimal grantedBalance = BigDecimal.ZERO;

	@Column(name="GRANT_BALANCE")
	private BigDecimal grantBalance = BigDecimal.ZERO;
	
	@Column(name="ISSUANCE_CIRCULATION_AMOUNT")
	private BigDecimal issuanceCirculationAmount;
	
	@Column(name="ISSUANCE_AMOUNT")
	private BigDecimal issuanceAmount;
	
	@Column(name="ISSUANCE_PLACED_AMOUNT")
	private BigDecimal issuancePlacedAmount;
	
	@Column(name="SECURITY_CIRCULATION_AMOUNT")
	private BigDecimal securityCirculationAmount;
	
	@Column(name="SECURITY_AMORTIZATION_AMOUN")
	private BigDecimal securityAmortizationAmoun;
	
	@Column(name="SECURITY_CIRCULATION_BALANC")
	private BigDecimal securityCirculationBalanc;
	
	@Column(name="SECURITY_DESMATERIALIZ_BALANC")
	private BigDecimal securityDesmaterializBalanc;
	
	@Column(name="SECURITY_PHYSICAL_BALANCE")
	private BigDecimal securityPhysicalBalance;
	
	@Column(name="OPERATION_PART")
	private Integer operationPart;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK",insertable=false,updatable=false)
	private MechanismOperation mechanismOperation;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_FK",insertable=false,updatable=false)
	private FundsOperation fundsOperation;
	
	@Column(name="FUND_OPERATION_AMOUNT")
	private BigDecimal fundOperationAmount;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_BENEFIT_PAYMENT_FK",insertable=false,updatable=false)
	private BenefitPaymentAllocation benefitPaymentAllocation;
	
	@Column(name="TOTAL_GUARANTEE")
	private BigDecimal totalGuarantee;
	
	@Column(name="PRINCIPAL_GUARANTEE")
	private BigDecimal principalGuarantee;
	
	@Column(name="MARGIN_GUARANTEE")
	private BigDecimal marginGuarantee;
	
	@Column(name="PRINCIPAL_DIVIDENDS_GUARANTEE")
	private BigDecimal principalDividendsGuarantee;
	
	@Column(name="MARGIN_DIVIDENDS_GUARANTEE")
	private BigDecimal marginDividendsGuarantee;
	
	@Column(name="INTEREST_MARGIN_GUARANTEE")
	private BigDecimal interestMarginGuarantee;
	
	@Column(name="OPERATION_AMOUNT")
	private BigDecimal operationAmount;
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The market date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
	
	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
    
	/** The market price. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	public Long getIdAuditProcessResultPk() {
		return idAuditProcessResultPk;
	}

	public void setIdAuditProcessResultPk(Long idAuditProcessResultPk) {
		this.idAuditProcessResultPk = idAuditProcessResultPk;
	}

	public Date getAuditProcessDate() {
		return auditProcessDate;
	}

	public void setAuditProcessDate(Date auditProcessDate) {
		this.auditProcessDate = auditProcessDate;
	}

	public Integer getAuditBalanceType() {
		return auditBalanceType;
	}

	public void setAuditBalanceType(Integer auditBalanceType) {
		this.auditBalanceType = auditBalanceType;
	}

	public AuditProcessLog getAuditProcessLog() {
		return auditProcessLog;
	}

	public void setAuditProcessLog(AuditProcessLog auditProcessLog) {
		this.auditProcessLog = auditProcessLog;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public BigDecimal getAccreditationBalance() {
		return accreditationBalance;
	}

	public void setAccreditationBalance(BigDecimal accreditationBalance) {
		this.accreditationBalance = accreditationBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getBorrowerBalance() {
		return borrowerBalance;
	}

	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	public BigDecimal getLenderBalance() {
		return lenderBalance;
	}

	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}

	public BigDecimal getMarginBalance() {
		return marginBalance;
	}

	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getReportedBalance() {
		return reportedBalance;
	}

	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getTransitBalance() {
		return transitBalance;
	}

	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	public BigDecimal getGrantedBalance() {
		return grantedBalance;
	}

	public void setGrantedBalance(BigDecimal grantedBalance) {
		this.grantedBalance = grantedBalance;
	}

	public BigDecimal getGrantBalance() {
		return grantBalance;
	}

	public void setGrantBalance(BigDecimal grantBalance) {
		this.grantBalance = grantBalance;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getOperationAmount() {
		return operationAmount;
	}

	public void setOperationAmount(BigDecimal operationAmount) {
		this.operationAmount = operationAmount;
	}
}