/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */
package com.pradera.model.auditprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum HolderAccountBankType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/05/2013
 */
public enum AuditStraightenState {
	
	/** The savings. */
	REGISTERED(new Integer(4017),"REGISTRADO");  
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<AuditStraightenState> list = new ArrayList<AuditStraightenState>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AuditStraightenState> lookup = new HashMap<Integer, AuditStraightenState>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<AuditStraightenState> listSomeElements(AuditStraightenState... transferSecuritiesTypeParams){
		List<AuditStraightenState> retorno = new ArrayList<AuditStraightenState>();
		for(AuditStraightenState TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static { 
		for (AuditStraightenState s : EnumSet.allOf(AuditStraightenState.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new holder account bank type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private AuditStraightenState(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account bank type
	 */
	public static AuditStraightenState get(Integer codigo) {
		return lookup.get(codigo);
	}
}