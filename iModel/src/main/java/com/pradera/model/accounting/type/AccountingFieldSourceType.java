package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2013.</li>
* </ul>
* 
* The Enum AccountingFieldSourceType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2013
*/
public enum  AccountingFieldSourceType {
	

	MOVEMENT_DATE(Integer.valueOf(2104),"MOVEMENT_DATE"),
	
	MOVEMENT_TYPE(Integer.valueOf(2105),"MOVEMENT_TYPE"),
	
	VALUE(Integer.valueOf(2106),"VALUE"),
	
	PARTICIPANT(Integer.valueOf(2107),"PARTICIPANT"),

	HOLDER(Integer.valueOf(2108),"HOLDER"),
	
	ISSUER(Integer.valueOf(2109),"ISSUER"),
	
	REFERENCE(Integer.valueOf(2110),"REFERENCE"),
	
	SECURITY(Integer.valueOf(2111),"SECURITY"),
	
	ENTITY_COLLECTION(Integer.valueOf(2112),"ENTITY_COLLECTION"),
	
	ISSUANCE(Integer.valueOf(2140),"ISSUANCE"),
	
	OPERATION_MOVEMENT(Integer.valueOf(2527),"OPERATION_MOVEMENT"),
	


	//TIPOS DE REFERENCIA
	NOMINAL_VALUE(Integer.valueOf(2528),"NOMINAL_VALUE"),
	
	MOVEMENT_QUANTITY(Integer.valueOf(2529),"MOVEMENT_QUANTITY"),
	
	NOT_PLACED(Integer.valueOf(2530),"NOT_PLACED")
	;
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingFieldSourceType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingFieldSourceType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingFieldSourceType> list = new ArrayList<AccountingFieldSourceType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingFieldSourceType> lookup      = new HashMap<Integer, AccountingFieldSourceType>();
	public static final Map<String, AccountingFieldSourceType>  lookupValue = new HashMap<String, AccountingFieldSourceType>();
	
	static {
		for (AccountingFieldSourceType s : EnumSet.allOf(AccountingFieldSourceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
			lookupValue.put(s.getValue(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingFieldSourceType state type
	 */
	public static AccountingFieldSourceType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static AccountingFieldSourceType get(String value) {
		return lookupValue.get(value);
	}
	
}
