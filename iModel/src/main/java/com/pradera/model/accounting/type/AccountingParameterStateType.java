package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum AccountingParameterStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum AccountingParameterStateType {
	
 
	REGISTERED(Integer.valueOf(2118),"REGISTRADO"),
	
 
	ACTIVED(Integer.valueOf(2119),"ACTIVADO"),
	
 
	LOCK(Integer.valueOf(2120),"BLOQUEADO"),
	
	 
	DELETED(Integer.valueOf(2121),"ELIMINADO");
	
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingParameterStateType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingParameterStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingParameterStateType> list = new ArrayList<AccountingParameterStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingParameterStateType> lookup = new HashMap<Integer, AccountingParameterStateType>();
	static {
		for (AccountingParameterStateType s : EnumSet.allOf(AccountingParameterStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingParameterStateType state type
	 */
	public static AccountingParameterStateType get(Integer code) {
		return lookup.get(code);
	}

}
