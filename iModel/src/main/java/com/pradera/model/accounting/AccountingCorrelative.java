package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the ACCOUNTING_CORRELATIVE database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_CORRELATIVE")
public class AccountingCorrelative implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCOUNTING_CORRELATIVEPK_GENERATOR", sequenceName="SQ_ID_ACC_CORRELATIVE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_CORRELATIVEPK_GENERATOR")
	@Column(name="ID_ACCOUNTING_CORRELATIVE_PK")
	private Long idAccountingCorrelativePk;

	@Column(name="DESCRIPTION",length=100)
	private String description;

	@Column(name="CORRELATIVE_CD",length=20)
	private String correlativeCD;
	
	@Column(name="CORRELATIVE_NUMBER",precision=10)
	private Long correlativeNumber;
	
	@Column(name="MONTH",precision=10)
	private Integer month;

	@Column(name="YEAR",precision=10)
	private Integer year;
	
	
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	


	/**
	 * @return the idAccountingCorrelativePk
	 */
	public Long getIdAccountingCorrelativePk() {
		return idAccountingCorrelativePk;
	}

	/**
	 * @param idAccountingCorrelativePk the idAccountingCorrelativePk to set
	 */
	public void setIdAccountingCorrelativePk(Long idAccountingCorrelativePk) {
		this.idAccountingCorrelativePk = idAccountingCorrelativePk;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the correlativeCD
	 */
	public String getCorrelativeCD() {
		return correlativeCD;
	}

	/**
	 * @param correlativeCD the correlativeCD to set
	 */
	public void setCorrelativeCD(String correlativeCD) {
		this.correlativeCD = correlativeCD;
	}

	/**
	 * @return the correlativeNumber
	 */
	public Long getCorrelativeNumber() {
		return correlativeNumber;
	}

	/**
	 * @param correlativeNumber the correlativeNumber to set
	 */
	public void setCorrelativeNumber(Long correlativeNumber) {
		this.correlativeNumber = correlativeNumber;
	}

	/**
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
