package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ACCOUNTING_PROCESS database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-ago-2015
 */
@Entity
@Table(name="ACCOUNTING_PROCESS_SCHEMA")
public class AccountingProcessSchema implements Serializable ,Auditable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 538549331400873660L;
	
	
	/** The id accounting process pk. */
	@Id
	@SequenceGenerator(name="ACCOUNTING_PROCESS_SCHEMA_GENERATION", sequenceName="SQ_ID_ACC_PROCESS_SCHEMA_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_PROCESS_SCHEMA_GENERATION")
	@Column(name="ID_ACCOUNTING_PROCESS_SCH_FK")
	private Long idAccountingProcessSchemaPk;

	
	//bi-directional many-to-one association to AccountingProcess
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_PROCESS_FK",referencedColumnName="ID_ACCOUNTING_PROCESS_PK")
  	private AccountingProcess accountingProcess;

	
	//bi-directional many-to-one association to AccountingSchema
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_SCHEMA_FK",referencedColumnName="ID_ACCOUNTING_SCHEMA_PK")
  	private AccountingSchema accountingSchema;
  	
  	
	@Column(name="START_TYPE", precision=10)
	private Integer startType;

	/** The status. */
	@Column(name="STATUS", precision=10)
	private Integer status;


	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	
	/** The description process type. */
	@Transient	
	private String descriptionProcessType;
	
	
	/** The generado. */
	@Transient
	private Integer generado;
	
	/** The descuadrado. */
	@Transient
	private Integer descuadrado;
	
	/** The errado. */
	@Transient
	private Integer errado;
	
	/** The description status. */
	@Transient
	private String descriptionStatus;
	
	
	
	
	/**
	 * Instantiates a new accounting process.
	 */
	public AccountingProcessSchema() {
		accountingSchema=new AccountingSchema();
		accountingProcess=new AccountingProcess();
	}
	

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();

        return detailsMap;
	}

	/**
	 * @return the idAccountingProcessSchemaPk
	 */
	public Long getIdAccountingProcessSchemaPk() {
		return idAccountingProcessSchemaPk;
	}


	/**
	 * @param idAccountingProcessSchemaPk the idAccountingProcessSchemaPk to set
	 */
	public void setIdAccountingProcessSchemaPk(Long idAccountingProcessSchemaPk) {
		this.idAccountingProcessSchemaPk = idAccountingProcessSchemaPk;
	}


	/**
	 * @return the accountingProcess
	 */
	public AccountingProcess getAccountingProcess() {
		return accountingProcess;
	}


	/**
	 * @param accountingProcess the accountingProcess to set
	 */
	public void setAccountingProcess(AccountingProcess accountingProcess) {
		this.accountingProcess = accountingProcess;
	}


	/**
	 * @return the accountingSchema
	 */
	public AccountingSchema getAccountingSchema() {
		return accountingSchema;
	}


	/**
	 * @param accountingSchema the accountingSchema to set
	 */
	public void setAccountingSchema(AccountingSchema accountingSchema) {
		this.accountingSchema = accountingSchema;
	}


	/**
	 * @return the startType
	 */
	public Integer getStartType() {
		return startType;
	}


	/**
	 * @param startType the startType to set
	 */
	public void setStartType(Integer startType) {
		this.startType = startType;
	}


	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}


	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}


	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	/**
	 * @return the descriptionProcessType
	 */
	public String getDescriptionProcessType() {
		return descriptionProcessType;
	}


	/**
	 * @param descriptionProcessType the descriptionProcessType to set
	 */
	public void setDescriptionProcessType(String descriptionProcessType) {
		this.descriptionProcessType = descriptionProcessType;
	}


	/**
	 * @return the generado
	 */
	public Integer getGenerado() {
		return generado;
	}


	/**
	 * @param generado the generado to set
	 */
	public void setGenerado(Integer generado) {
		this.generado = generado;
	}


	/**
	 * @return the descuadrado
	 */
	public Integer getDescuadrado() {
		return descuadrado;
	}


	/**
	 * @param descuadrado the descuadrado to set
	 */
	public void setDescuadrado(Integer descuadrado) {
		this.descuadrado = descuadrado;
	}


	/**
	 * @return the errado
	 */
	public Integer getErrado() {
		return errado;
	}


	/**
	 * @param errado the errado to set
	 */
	public void setErrado(Integer errado) {
		this.errado = errado;
	}


	/**
	 * @return the descriptionStatus
	 */
	public String getDescriptionStatus() {
		return descriptionStatus;
	}


	/**
	 * @param descriptionStatus the descriptionStatus to set
	 */
	public void setDescriptionStatus(String descriptionStatus) {
		this.descriptionStatus = descriptionStatus;
	}


}
