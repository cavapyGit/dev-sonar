package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
/**
 * The persistent class for the ACCOUNTING_SCHEMA database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_SCHEMA")
public class AccountingSchema implements Serializable ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6783856248032538317L;
	
	@Id
	@SequenceGenerator(name="ACCOUNTING_SCHEMA_GENERATION", sequenceName="SQ_ID_ACCOUNTING_SCHEMA_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_SCHEMA_GENERATION")
	@Column(name="ID_ACCOUNTING_SCHEMA_PK")
	private Long idAccountingSchemaPk;
	
	
	@Column(name="SCHEMA_CODE", length=20)
	private String schemaCode;
	
	@Column(name="DESCRIPTION", length=200)
	private String description;
	
	@Column(name="BRANCH_TYPE", precision=10)
	private Integer branchType;
	
	@Column(name="STATUS", precision=10)
	private Integer status;
	
	@Column(name="AUXILIARY_TYPE", precision=10)
	private Integer auxiliaryType;
	
	@Column(name="ASSOCIATE_CODE", precision=10)
	private Integer associateCode;
	
	@Column(name="START_TYPE", precision=10)
	private Integer startType;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="REGISTER_DATE")
	private Date registerDate;
	  
	@Column(name="REGISTER_USER", length=20)
	private String registerUser;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="UPDATE_DATE")
	private Date updateDate;
	
	@Column(name="UPDATE_USER", length=20)
	private String updateUser;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="CANCEL_DATE")
	private Date cancelDate;
	
	@Column(name="CANCEL_USER", length=20)
	private String cancelUser;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	
	@Transient	
	private String descriptionParameterBranch ;
	@Transient
	private String descriptionParameterStatus;
	@Transient
	private String descriptionAuxiliaryType;
	
	

	public AccountingSchema() {
	}

	/**
	 * 
	 * @param idAccountingSchemaPk
	 * @param schemaCode
	 * @param description
	 * @param branchType
	 * @param status
	 * @param auxiliaryType
	 * @param associateCode
	 * @param descriptionParameterBranch
	 * @param descriptionParameterStatus
	 * @param descriptionAuxiliaryType
	 */
	public AccountingSchema(Long idAccountingSchemaPk, String schemaCode,
			String description, Integer branchType, Integer status, Integer auxiliaryType,
			Integer associateCode,String descriptionParameterBranch,
			String descriptionParameterStatus,  String descriptionAuxiliaryType
			) {
		this.idAccountingSchemaPk = idAccountingSchemaPk;
		this.schemaCode = schemaCode;
		this.description = description;
		this.branchType = branchType;
		this.status = status;
		this.auxiliaryType = auxiliaryType;
		this.associateCode = associateCode;
		this.descriptionParameterBranch = descriptionParameterBranch;
		this.descriptionParameterStatus = descriptionParameterStatus;
		this.descriptionAuxiliaryType = descriptionAuxiliaryType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		 
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the idAccountingSchemaPk
	 */
	public Long getIdAccountingSchemaPk() {
		return idAccountingSchemaPk;
	}

	/**
	 * @param idAccountingSchemaPk the idAccountingSchemaPk to set
	 */
	public void setIdAccountingSchemaPk(Long idAccountingSchemaPk) {
		this.idAccountingSchemaPk = idAccountingSchemaPk;
	}

	/**
	 * @return the schemaCode
	 */
	public String getSchemaCode() {
		return schemaCode;
	}

	/**
	 * @param schemaCode the schemaCode to set
	 */
	public void setSchemaCode(String schemaCode) {
		this.schemaCode = schemaCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the branchType
	 */
	public Integer getBranchType() {
		return branchType;
	}

	/**
	 * @param branchType the branchType to set
	 */
	public void setBranchType(Integer branchType) {
		this.branchType = branchType;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * @return the registerUser
	 */
	public String getRegisterUser() {
		return registerUser;
	}

	/**
	 * @param registerUser the registerUser to set
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param updateUser the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * @return the cancelDate
	 */
	public Date getCancelDate() {
		return cancelDate;
	}

	/**
	 * @param cancelDate the cancelDate to set
	 */
	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	/**
	 * @return the cancelUser
	 */
	public String getCancelUser() {
		return cancelUser;
	}

	/**
	 * @param cancelUser the cancelUser to set
	 */
	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the descriptionParameterBranch
	 */
	public String getDescriptionParameterBranch() {
		return descriptionParameterBranch;
	}

	/**
	 * @param descriptionParameterBranch the descriptionParameterBranch to set
	 */
	public void setDescriptionParameterBranch(String descriptionParameterBranch) {
		this.descriptionParameterBranch = descriptionParameterBranch;
	}

	/**
	 * @return the descriptionParameterStatus
	 */
	public String getDescriptionParameterStatus() {
		return descriptionParameterStatus;
	}

	/**
	 * @param descriptionParameterStatus the descriptionParameterStatus to set
	 */
	public void setDescriptionParameterStatus(String descriptionParameterStatus) {
		this.descriptionParameterStatus = descriptionParameterStatus;
	}

	/**
	 * @return the auxiliaryType
	 */
	public Integer getAuxiliaryType() {
		return auxiliaryType;
	}

	/**
	 * @param auxiliaryType the auxiliaryType to set
	 */
	public void setAuxiliaryType(Integer auxiliaryType) {
		this.auxiliaryType = auxiliaryType;
	}

	public String getDescriptionAuxiliaryType() {
		return descriptionAuxiliaryType;
	}

	public void setDescriptionAuxiliaryType(String descriptionAuxiliaryType) {
		this.descriptionAuxiliaryType = descriptionAuxiliaryType;
	}

	/**
	 * @return the associateCode
	 */
	public Integer getAssociateCode() {
		return associateCode;
	}

	/**
	 * @param associateCode the associateCode to set
	 */
	public void setAssociateCode(Integer associateCode) {
		this.associateCode = associateCode;
	}

	/**
	 * @return the startType
	 */
	public Integer getStartType() {
		return startType;
	}

	/**
	 * @param startType the startType to set
	 */
	public void setStartType(Integer startType) {
		this.startType = startType;
	}

	

}
