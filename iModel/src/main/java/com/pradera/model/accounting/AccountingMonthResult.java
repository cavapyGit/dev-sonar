package com.pradera.model.accounting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCOUNTING_RECEIPT database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_MONTH_RESULT")
public class AccountingMonthResult implements Serializable ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 132562281796286991L;

	@Id
	@SequenceGenerator(name="ACCOUNTING_MONTH_RESULT_SQ", sequenceName="SQ_ID_ACCOUNTING_MON_RESULT_PK", initialValue = 1, allocationSize = 10000)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_MONTH_RESULT_SQ")
	@Column(name="ID_ACCOUNTING_MONTH_RESULT_PK")
	private Long idAccountingMonthResultPk;
	
	//bi-directional many-to-one association to accountingReceipt
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_RECEIPT_FK",referencedColumnName="ID_ACCOUNTING_RECEIPTS_PK")
  	private AccountingReceipt accountingReceipt;
  	
    //bi-directional many-to-one association to AccountingMatrixDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCOUNTING_MATRIX_DETAIL_FK",referencedColumnName="ID_ACCOUNTING_MATRIX_DETAIL_PK")
	private AccountingMatrixDetail accountingMatrixDetail;
	
	@Column(name = "ID_SECURITY_CODE_FK")
	private String idSecurityCodeFk;
  	
  	@Column(name="MOVEMENT_QUANTITY", precision=10)
	private BigDecimal movementQuantity;
  	
  	@Column(name="NOMINAL_VALUE", precision=30, scale=8)
	private BigDecimal nominalValue;
  	
  	@Column(name="AMOUNT", precision=30, scale=8)
	private BigDecimal amount;
  	
  	@Column(name="NOT_PLACED", precision=30, scale=8)
	private BigDecimal notPlaced;
	 
	@Column(name="ID_PARTICIPANT_FK")
	private Long idParticipant ;
	
	@Column(name="ID_HOLDER_FK")
	private Long idHolder ;
	 
	@Column(name="ID_HOLDER_ACCOUNT_FK")
	private Long holderAccount ;
	
	@Column(name="ID_ISSUANCE_CODE_FK")
	private String idIssuance ;
	
	@Column(name="ID_ISSUER_FK")
	private String idIssuer ;

	
  	@Column(name="KEY_ASSOCIATE_CODE")
	private String keyAssociateCode;
	


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;
	
	@Column(name="ENTITY_COLLECTION", precision=10)
	private Integer entityCollection;
	
	@Column(name="MOVEMENT_TYPE", precision=10)
	private Integer movementType;
	
	@Column(name="DYNAMIC_TYPE", precision=10)
	private Integer dynamicType;
	
	@Column(name="PORTFOLIO", precision=10)
	private Integer portfolio;
	
	@Column(name="INSTRUMENT_TYPE", precision=10)
	private Integer instrumentType;
	
	@Column(name="CURRENCY_TYPE", precision=10)
	private Integer currencyType;
	
	
  	  		
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}


	/**
	 * @return the idAccountingMonthResultPk
	 */
	public Long getIdAccountingMonthResultPk() {
		return idAccountingMonthResultPk;
	}


	/**
	 * @param idAccountingMonthResultPk the idAccountingMonthResultPk to set
	 */
	public void setIdAccountingMonthResultPk(Long idAccountingMonthResultPk) {
		this.idAccountingMonthResultPk = idAccountingMonthResultPk;
	}





	/**
	 * @return the accountingReceipt
	 */
	public AccountingReceipt getAccountingReceipt() {
		return accountingReceipt;
	}


	/**
	 * @param accountingReceipt the accountingReceipt to set
	 */
	public void setAccountingReceipt(AccountingReceipt accountingReceipt) {
		this.accountingReceipt = accountingReceipt;
	}


	/**
	 * @return the accountingMatrixDetail
	 */
	public AccountingMatrixDetail getAccountingMatrixDetail() {
		return accountingMatrixDetail;
	}


	/**
	 * @param accountingMatrixDetail the accountingMatrixDetail to set
	 */
	public void setAccountingMatrixDetail(AccountingMatrixDetail accountingMatrixDetail) {
		this.accountingMatrixDetail = accountingMatrixDetail;
	}


	/**
	 * @return the idSecurityCodeFk
	 */
	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}


	/**
	 * @param idSecurityCodeFk the idSecurityCodeFk to set
	 */
	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}





	/**
	 * @return the movementQuantity
	 */
	public BigDecimal getMovementQuantity() {
		return movementQuantity;
	}


	/**
	 * @param movementQuantity the movementQuantity to set
	 */
	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}


	/**
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}


	/**
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}


	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}


	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	/**
	 * @return the notPlaced
	 */
	public BigDecimal getNotPlaced() {
		return notPlaced;
	}


	/**
	 * @param notPlaced the notPlaced to set
	 */
	public void setNotPlaced(BigDecimal notPlaced) {
		this.notPlaced = notPlaced;
	}


	/**
	 * @return the idParticipant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}


	/**
	 * @param idParticipant the idParticipant to set
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}


	/**
	 * @return the idHolder
	 */
	public Long getIdHolder() {
		return idHolder;
	}


	/**
	 * @param idHolder the idHolder to set
	 */
	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}


	/**
	 * @return the holderAccount
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}


	/**
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}





	/**
	 * @return the movementType
	 */
	public Integer getMovementType() {
		return movementType;
	}


	/**
	 * @param movementType the movementType to set
	 */
	public void setMovementType(Integer movementType) {
		this.movementType = movementType;
	}


	/**
	 * @return the dynamicType
	 */
	public Integer getDynamicType() {
		return dynamicType;
	}


	/**
	 * @param dynamicType the dynamicType to set
	 */
	public void setDynamicType(Integer dynamicType) {
		this.dynamicType = dynamicType;
	}


	/**
	 * @return the portfolio
	 */
	public Integer getPortfolio() {
		return portfolio;
	}


	/**
	 * @param portfolio the portfolio to set
	 */
	public void setPortfolio(Integer portfolio) {
		this.portfolio = portfolio;
	}


	/**
	 * @return the instrumentType
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}


	/**
	 * @param instrumentType the instrumentType to set
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}


	/**
	 * @return the currencyType
	 */
	public Integer getCurrencyType() {
		return currencyType;
	}


	/**
	 * @param currencyType the currencyType to set
	 */
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}


	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}


	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	/**
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}


	/**
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}


	/**
	 * @return the movementDate
	 */
	public Date getMovementDate() {
		return movementDate;
	}


	/**
	 * @param movementDate the movementDate to set
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}




	/**
	 * @return the idIssuance
	 */
	public String getIdIssuance() {
		return idIssuance;
	}


	/**
	 * @param idIssuance the idIssuance to set
	 */
	public void setIdIssuance(String idIssuance) {
		this.idIssuance = idIssuance;
	}


	/**
	 * @return the idIssuer
	 */
	public String getIdIssuer() {
		return idIssuer;
	}


	/**
	 * @param idIssuer the idIssuer to set
	 */
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}


	/**
	 * @return the keyAssociateCode
	 */
	public String getKeyAssociateCode() {
		return keyAssociateCode;
	}


	/**
	 * @param keyAssociateCode the keyAssociateCode to set
	 */
	public void setKeyAssociateCode(String keyAssociateCode) {
		this.keyAssociateCode = keyAssociateCode;
	}






	
}
