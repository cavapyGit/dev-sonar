package com.pradera.model.accounting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCOUNTING_RECEIPT_DETAIL database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_RECEIPT_DETAIL")
public class AccountingReceiptDetail implements Serializable ,Auditable, Comparable<AccountingReceiptDetail>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3475493805078420372L;
	
	@Id
	@SequenceGenerator(name="ACCOUNTING_RECEIPT_DETAIL_GENERATION", sequenceName="SQ_ID_ACC_RECEIPT_DETAIL_PK", initialValue = 1, allocationSize = 10000)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_RECEIPT_DETAIL_GENERATION")
	@Column(name="ID_ACCOUNTING_RECEIPT_DET_PK")
	private Long idAccountingReceiptsDetailPk;

	//bi-directional many-to-one association to AccountingReceipt
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_RECEIPTS_FK",referencedColumnName="ID_ACCOUNTING_RECEIPTS_PK")
  	private AccountingReceipt accountingReceipt;
  	
  	
  	@OneToMany(mappedBy="accountingReceiptDetail")
  	private List<AccountingSourceLogger> accountingSourceLogger;
  	
  	
  //bi-directional many-to-one association to AccountingMatrixDetail
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_MATRIX_DETAIL_FK",referencedColumnName="ID_ACCOUNTING_MATRIX_DETAIL_PK")
  	private AccountingMatrixDetail accountingMatrixDetail;

  	@Column(name="NUMBER_RECEIPT_DETAIL", length=20)
	private String numberReceiptDetail;
	
  	@Column(name="ASSOCIATE_CODE", precision=10)
	private Integer associateCode;
  	
  	@Column(name="ANNEX_REF", precision=10)
	private Integer annexRef;
  	
  	@Column(name="AMOUNT", precision=30, scale=8)
	private BigDecimal amount;
  	
  	@Column(name="QUANTITY", precision=10)
	private Integer quantity;
  	
  	@Column(name="GLOSS", length=100)
	private String gloss;
  	
  	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DOCUMENT_DATE")
	private Date documentDate;
	
  	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Transient
	private String keyAssociateCode;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("accountingSourceLogger", accountingSourceLogger);
        return detailsMap;
	}

	/**
	 * @return the idAccountingReceiptsDetailPk
	 */
	public Long getIdAccountingReceiptsDetailPk() {
		return idAccountingReceiptsDetailPk;
	}

	/**
	 * @param idAccountingReceiptsDetailPk the idAccountingReceiptsDetailPk to set
	 */
	public void setIdAccountingReceiptsDetailPk(Long idAccountingReceiptsDetailPk) {
		this.idAccountingReceiptsDetailPk = idAccountingReceiptsDetailPk;
	}

	/**
	 * @return the accountingReceipt
	 */
	public AccountingReceipt getAccountingReceipt() {
		return accountingReceipt;
	}

	/**
	 * @param accountingReceipt the accountingReceipt to set
	 */
	public void setAccountingReceipt(AccountingReceipt accountingReceipt) {
		this.accountingReceipt = accountingReceipt;
	}

	/**
	 * @return the accountingMatrixDetail
	 */
	public AccountingMatrixDetail getAccountingMatrixDetail() {
		return accountingMatrixDetail;
	}

	/**
	 * @param accountingMatrixDetail the accountingMatrixDetail to set
	 */
	public void setAccountingMatrixDetail(
			AccountingMatrixDetail accountingMatrixDetail) {
		this.accountingMatrixDetail = accountingMatrixDetail;
	}

	/**
	 * @return the numberReceiptDetail
	 */
	public String getNumberReceiptDetail() {
		return numberReceiptDetail;
	}

	/**
	 * @param numberReceiptDetail the numberReceiptDetail to set
	 */
	public void setNumberReceiptDetail(String numberReceiptDetail) {
		this.numberReceiptDetail = numberReceiptDetail;
	}

	/**
	 * @return the associateCode
	 */
	public Integer getAssociateCode() {
		return associateCode;
	}

	/**
	 * @param associateCode the associateCode to set
	 */
	public void setAssociateCode(Integer associateCode) {
		this.associateCode = associateCode;
	}

	/**
	 * @return the annexRef
	 */
	public Integer getAnnexRef() {
		return annexRef;
	}

	/**
	 * @param annexRef the annexRef to set
	 */
	public void setAnnexRef(Integer annexRef) {
		this.annexRef = annexRef;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the gloss
	 */
	public String getGloss() {
		return gloss;
	}

	/**
	 * @param gloss the gloss to set
	 */
	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	/**
	 * @return the documentDate
	 */
	public Date getDocumentDate() {
		return documentDate;
	}

	/**
	 * @param documentDate the documentDate to set
	 */
	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the accountingSourceLogger
	 */
	public List<AccountingSourceLogger> getAccountingSourceLogger() {
		return accountingSourceLogger;
	}

	/**
	 * @param accountingSourceLogger the accountingSourceLogger to set
	 */
	public void setAccountingSourceLogger(
			List<AccountingSourceLogger> accountingSourceLogger) {
		this.accountingSourceLogger = accountingSourceLogger;
	}

	/**
	 * @return the keyAssociateCode
	 */
	public String getKeyAssociateCode() {
		return keyAssociateCode;
	}

	/**
	 * @param keyAssociateCode the keyAssociateCode to set
	 */
	public void setKeyAssociateCode(String keyAssociateCode) {
		this.keyAssociateCode = keyAssociateCode;
	}

	@Override
	public int compareTo(AccountingReceiptDetail o) {
		AccountingReceiptDetail accountingReceiptDetail=(AccountingReceiptDetail) o;
		String keyAssociate=accountingReceiptDetail.keyAssociateCode;
		String keyAssociateThis=this.getKeyAssociateCode();
		return keyAssociateThis.compareTo(keyAssociate);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AccountingReceiptDetail ["
				+ (idAccountingReceiptsDetailPk != null ? "idAccountingReceiptsDetailPk="
						+ idAccountingReceiptsDetailPk + ", "
						: "")
				+ (accountingReceipt != null ? "accountingReceipt="
						+ accountingReceipt + ", " : "")
				+ (associateCode != null ? "associateCode=" + associateCode
						+ ", " : "")
				+ (amount != null ? "amount=" + amount + ", " : "")
				+ (quantity != null ? "quantity=" + quantity : "") + "]";
	}

}
