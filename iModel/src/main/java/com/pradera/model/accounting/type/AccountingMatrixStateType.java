package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum AccountingMatrixStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum AccountingMatrixStateType {
	
 
	REGISTERED(Integer.valueOf(2115),"REGISTRADO"),
	
  
	LOCK(Integer.valueOf(2116),"BLOQUEADO"),
	
	 
	CANCELED(Integer.valueOf(2117),"CANCELADO");
	
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingMatrixStateType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingMatrixStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingMatrixStateType> list = new ArrayList<AccountingMatrixStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingMatrixStateType> lookup = new HashMap<Integer, AccountingMatrixStateType>();
	static {
		for (AccountingMatrixStateType s : EnumSet.allOf(AccountingMatrixStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingMatrixStateType state type
	 */
	public static AccountingMatrixStateType get(Integer code) {
		return lookup.get(code);
	}

}
