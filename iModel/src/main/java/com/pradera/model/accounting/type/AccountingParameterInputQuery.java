package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2013.</li>
* </ul>
* 
* The Enum AccountingParameterInputQuery.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2013
*/
public enum  AccountingParameterInputQuery {
	
	DATE(Integer.valueOf(100),"DATE"),

	MNEMONIC(Integer.valueOf(101),"MNEMONIC"),
	
	CURRENCY(Integer.valueOf(102),"CURRENCY"),
	
	INSTRUMENT_TYPE(Integer.valueOf(103),"INSTRUMENT_TYPE");
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingParameterInputQuery state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingParameterInputQuery(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingParameterInputQuery> list = new ArrayList<AccountingParameterInputQuery>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingParameterInputQuery> lookup      = new HashMap<Integer, AccountingParameterInputQuery>();
	public static final Map<String, AccountingParameterInputQuery>  lookupValue = new HashMap<String, AccountingParameterInputQuery>();
	
	static {
		for (AccountingParameterInputQuery s : EnumSet.allOf(AccountingParameterInputQuery.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
			lookupValue.put(s.getValue(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingParameterInputQuery state type
	 */
	public static AccountingParameterInputQuery get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static AccountingParameterInputQuery get(String value) {
		return lookupValue.get(value);
	}
	
}
