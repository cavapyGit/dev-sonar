package com.pradera.model.accounting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCOUNTING_RECEIPT database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_RECEIPT")
public class AccountingReceipt implements Serializable ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 132562281796286991L;

	@Id
	@SequenceGenerator(name="ACCOUNTING_RECEIPT_GENERATION", sequenceName="SQ_ID_ACCOUNTING_RECEIPT_PK", initialValue = 1, allocationSize = 5000)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_RECEIPT_GENERATION")
	@Column(name="ID_ACCOUNTING_RECEIPTS_PK")
	private Long idAccountingReceiptsPk;
	
	//bi-directional many-to-one association to AccountingProcess
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_PROCESS_FK",referencedColumnName="ID_ACCOUNTING_PROCESS_PK")
  	private AccountingProcess accountingProcess;
	
  //bi-directional many-to-one association to AccountingReceiptDetail
  	@OneToMany( mappedBy="accountingReceipt" )
  	private List<AccountingReceiptDetail> accountingReceiptDetail;
	
    //bi-directional many-to-one association to AccountingReceiptDetail
	@OneToMany( mappedBy="accountingReceipt" )
	private List<AccountingMonthResult> accountingMonthResult;
  	
  	@Column(name="NUMBER_RECEIPT", length=20)
	private String numberReceipt;
  	
  	@Column(name="CURRENCY", precision=10)
	private Integer currency;
  	
  	@Column(name="RECEIPT_AMOUNT", precision=30, scale=8)
	private BigDecimal receiptAmount;
  	
  	@Column(name="RECEIPT_QUANTITY", precision=10)
	private Integer receiptQueantity;
  	
  	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="RECEIPT_DATE")
	private Date receiptDate;

  	@Column(name="STATUS", precision=10)
	private Integer status;
  	
  	@Column(name="ASSOCIATE_CODE", precision=10)
	private Integer associateCode;
  	
  	@Column(name="KEY_ASSOCIATE", length=20)
	private String keyAssociate;
  	  		
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("accountingReceiptDetail", accountingReceiptDetail);
        return detailsMap;
	}

	/**
	 * @return the idAccountingReceiptsPk
	 */
	public Long getIdAccountingReceiptsPk() {
		return idAccountingReceiptsPk;
	}

	/**
	 * @param idAccountingReceiptsPk the idAccountingReceiptsPk to set
	 */
	public void setIdAccountingReceiptsPk(Long idAccountingReceiptsPk) {
		this.idAccountingReceiptsPk = idAccountingReceiptsPk;
	}

	/**
	 * @return the accountingProcess
	 */
	public AccountingProcess getAccountingProcess() {
		return accountingProcess;
	}

	/**
	 * @param accountingProcess the accountingProcess to set
	 */
	public void setAccountingProcess(AccountingProcess accountingProcess) {
		this.accountingProcess = accountingProcess;
	}

	/**
	 * @return the numberReceipt
	 */
	public String getNumberReceipt() {
		return numberReceipt;
	}

	/**
	 * @param numberReceipt the numberReceipt to set
	 */
	public void setNumberReceipt(String numberReceipt) {
		this.numberReceipt = numberReceipt;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * @return the receiptAmount
	 */
	public BigDecimal getReceiptAmount() {
		return receiptAmount;
	}

	/**
	 * @param receiptAmount the receiptAmount to set
	 */
	public void setReceiptAmount(BigDecimal receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	/**
	 * @return the receiptQueantity
	 */
	public Integer getReceiptQueantity() {
		return receiptQueantity;
	}

	/**
	 * @param receiptQueantity the receiptQueantity to set
	 */
	public void setReceiptQueantity(Integer receiptQueantity) {
		this.receiptQueantity = receiptQueantity;
	}

	/**
	 * @return the receiptDate
	 */
	public Date getReceiptDate() {
		return receiptDate;
	}

	/**
	 * @param receiptDate the receiptDate to set
	 */
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the accountingReceiptDetail
	 */
	public List<AccountingReceiptDetail> getAccountingReceiptDetail() {
		return accountingReceiptDetail;
	}

	/**
	 * @param accountingReceiptDetail the accountingReceiptDetail to set
	 */
	public void setAccountingReceiptDetail(
			List<AccountingReceiptDetail> accountingReceiptDetail) {
		this.accountingReceiptDetail = accountingReceiptDetail;
	}

	/**
	 * @return the associateCode
	 */
	public Integer getAssociateCode() {
		return associateCode;
	}

	/**
	 * @param associateCode the associateCode to set
	 */
	public void setAssociateCode(Integer associateCode) {
		this.associateCode = associateCode;
	}

	/**
	 * @return the keyAssociate
	 */
	public String getKeyAssociate() {
		return keyAssociate;
	}

	/**
	 * @param keyAssociate the keyAssociate to set
	 */
	public void setKeyAssociate(String keyAssociate) {
		this.keyAssociate = keyAssociate;
	}

	/**
	 * @return the accountingMonthResult
	 */
	public List<AccountingMonthResult> getAccountingMonthResult() {
		return accountingMonthResult;
	}

	/**
	 * @param accountingMonthResult the accountingMonthResult to set
	 */
	public void setAccountingMonthResult(List<AccountingMonthResult> accountingMonthResult) {
		this.accountingMonthResult = accountingMonthResult;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AccountingReceipt ["
				+ (idAccountingReceiptsPk != null ? "idAccountingReceiptsPk="
						+ idAccountingReceiptsPk + ", " : "")
				+ (numberReceipt != null ? "numberReceipt=" + numberReceipt
						: "") + "]";
	}

	
	
}
