package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCOUNTING_FORMULATE database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_FORMULATE")
public class AccountingFormulate implements Serializable ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -576285620651593698L;

	@Id
	@SequenceGenerator(name="ACCOUNTING_FORMULATE_GENERATION", sequenceName="SQ_ID_ACCOUNT_FORMULATE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_FORMULATE_GENERATION")
	@Column(name="ID_ACCOUNTING_FORMULATE_PK")
	private Long idAccountingFormulatePk;
	
	//bi-directional many-to-one association to VariableFormulate
	@OneToMany(mappedBy="accountingFormulate",cascade=CascadeType.ALL)
	private List<VariableFormulate> variableFormulates;
	
	@Column(name="FORMULATE_INPUT", length=200)
	private String formulateInput;
		
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("variableFormulates", variableFormulates);
        return detailsMap;
	}

	/**
	 * @return the idAccountingFormulatePk
	 */
	public Long getIdAccountingFormulatePk() {
		return idAccountingFormulatePk;
	}

	/**
	 * @param idAccountingFormulatePk the idAccountingFormulatePk to set
	 */
	public void setIdAccountingFormulatePk(Long idAccountingFormulatePk) {
		this.idAccountingFormulatePk = idAccountingFormulatePk;
	}

	/**
	 * @return the formulateInput
	 */
	public String getFormulateInput() {
		return formulateInput;
	}

	/**
	 * @param formulateInput the formulateInput to set
	 */
	public void setFormulateInput(String formulateInput) {
		this.formulateInput = formulateInput;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the variableFormulates
	 */
	public List<VariableFormulate> getVariableFormulates() {
		return variableFormulates;
	}

	/**
	 * @param variableFormulates the variableFormulates to set
	 */
	public void setVariableFormulates(List<VariableFormulate> variableFormulates) {
		this.variableFormulates = variableFormulates;
	}

	public VariableFormulate getVariableFormulateByPosition(Integer position){
		
		VariableFormulate variableFormulate = null;
		
					for (VariableFormulate variableFormulateTemp : variableFormulates) {
						if (position.equals(variableFormulateTemp.getPositionSource())){
							
						}
					}
				
		return variableFormulate;
	}

}
