package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum AccountingSourceStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum AccountingSourceStateType {
	
 

	REGISTERED(Integer.valueOf(2131),"REGISTRADO"),
	
	DELETED(Integer.valueOf(2132),"ELIMINADO"),
	
	MODIFIED(Integer.valueOf(2133),"MODIFICADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingSourceStateType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingSourceStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingSourceStateType> list = new ArrayList<AccountingSourceStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingSourceStateType> lookup = new HashMap<Integer, AccountingSourceStateType>();
	static {
		for (AccountingSourceStateType s : EnumSet.allOf(AccountingSourceStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingSourceStateType state type
	 */
	public static AccountingSourceStateType get(Integer code) {
		return lookup.get(code);
	}

}
