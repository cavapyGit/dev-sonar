package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum AccountingMatrixDetailStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum AccountingMatrixDetailStateType {
	
 
	REGISTERED(Integer.valueOf(2113),"REGISTRADO"),
	
	 
	CANCELED(Integer.valueOf(2114),"CANCELADO");
	
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingMatrixDetailStateType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingMatrixDetailStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingMatrixDetailStateType> list = new ArrayList<AccountingMatrixDetailStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingMatrixDetailStateType> lookup = new HashMap<Integer, AccountingMatrixDetailStateType>();
	static {
		for (AccountingMatrixDetailStateType s : EnumSet.allOf(AccountingMatrixDetailStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingMatrixDetailStateType state type
	 */
	public static AccountingMatrixDetailStateType get(Integer code) {
		return lookup.get(code);
	}

}
