package com.pradera.model.accounting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCOUNTING_ACCOUNT database table.
 * 
 */
@Entity
@Cacheable(true)
@Table(name="ACCOUNTING_ACCOUNT")
public class AccountingAccount implements Serializable ,Auditable,Comparable<AccountingAccount> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2103831486247750009L;


	@Id
	@SequenceGenerator(name="ACCOUNTING_ACCOUNT_GENERATION", sequenceName="SQ_ID_ACC_ACCOUNT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_ACCOUNT_GENERATION")
	@Column(name="ID_ACCOUNTING_ACCOUNT_PK")
	private Long idAccountingAccountPk;
	
 
	 @ManyToOne(cascade = { CascadeType.ALL })
	 @JoinColumn(name = "ID_ACCOUNTING_ACCOUNT_FK")
	 private AccountingAccount  accountingAccount;

	 // bi-directional many-to-one association to BusinessProcess
	 /** The business processes. */
	 @OneToMany(mappedBy = "accountingAccount")
	 private List<AccountingAccount> accountingAccounts;
	
	
	@Column(name="ACCOUNT_CODE", length=20)
	private String accountCode;


	@Column(name="DESCRIPTION", length=200)
	private String description;
	
	@Column(name="ACCOUNT_TYPE", precision=10)
	private Integer accountType; 
	
	@Column(name="NATURE_TYPE", precision=10)
	private Integer natureType;

	@Column(name="CURRENCY_TYPE", precision=10)
	private Integer currency;
	
	@Column(name="ID_PORTFOLIO", precision=10)
	private Integer portFolio;
	
	@Column(name="IND_ACCUMULATION")
	private Integer indAccumulation;
	
	@Column(name="IND_BALANCE_CONTROL")
	private Integer indBalanceControl;
	
	@Column(name="IND_DIMENSION")
	private Integer indDimension;
	
	@Column(name="IND_REFERENCE")
	private Integer indReference;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="EXPIRED_DATE")
	private Date expiredDate;
	
	@Column(name="ADJUSTMENT_TYPE", precision=10)
	private Integer adjustmentType;
	
	@Column(name="IND_ENTITY_ACCOUNT")
	private Integer indEntityAccount;
	
	@Column(name="ENTITY_ACCOUNT", precision=10)
	private Integer entityAccount;
	
	
	@Column(name="AUXILIARY_TYPE", precision=10)
	private Integer auxiliaryType;
	
	@Column(name="STATUS", precision=10)
	private Integer status;

	@Column(name="ACCOUNTING_TYPE", precision=10)
	private Integer accountingType;
	
	@Column(name="IND_SUB_ACCOUNT", precision=10)
	private Integer indSubAccount;
	
	@Column(name="INSTRUMENT_TYPE", precision=10)
	private Integer instrumentType;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="REGISTER_DATE")
	private Date registerDate;
	  
	@Column(name="REGISTER_USER", length=20)
	private String registerUser;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="UPDATE_DATE")
	private Date updateDate;
	
	@Column(name="UPDATE_USER", length=20)
	private String updateUser;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="CANCEL_DATE")
	private Date cancelDate;
	
	@Column(name="CANCEL_USER", length=20)
	private String cancelUser;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private Integer branch;
	@Transient
	private String fatherCode;
	@Transient
	private String fatherDescription;
	
	@Transient
	private String descriptionTypeAccount;
	@Transient
	private String descriptionCurrency;
	@Transient
	private String descriptionStateAccount;
	@Transient
	private String descriptionInstrumentType;
	@Transient
	private String descriptionAuxiliaryType;
	@Transient
	private String descriptionNatureType;
	@Transient
	private String descriptionEntityAccount;
	
	@Transient
	private BigDecimal amount;
	@Transient
	private BigDecimal amountAssets;
	@Transient
	private BigDecimal amountDebit;
	@Transient
	private BigDecimal amountUSD;
	@Transient
	private BigDecimal amountBOB;
	@Transient
	private BigDecimal amountMVL;
	@Transient
	private BigDecimal amountEU;
	@Transient
	private BigDecimal amountUFV;
	@Transient
	private BigDecimal amountToBOB;
	@Transient
	private BigDecimal quantity;
	
	/**ID_ACCOUNTING_ACCOUNT_FK*/
	@Transient
	private Long idAccountingAccountFk;
	
	/**
	 * 
	 */
	public AccountingAccount() {
	}
	
	


	/**
	 * Constructor Report Accounting
	 * 13 Fields
	 * @param idAccountingAccountPk
	 * @param accountingAccount
	 * @param accountingAccounts
	 * @param accountCode
	 * @param description
	 * @param natureType
	 * @param currency
	 * @param portFolio
	 * @param indAccumulation
	 * @param status
	 * @param instrumentType
	 */
	public AccountingAccount(Long idAccountingAccountPk,
			AccountingAccount accountingAccount,
			String accountCode,
			String description, Integer natureType, Integer currency,
			Integer portFolio, Integer indAccumulation, 
			Integer indReference, Integer auxiliaryType,
			Integer status, 
			Integer indSubAccount,	Integer instrumentType) {

		this.idAccountingAccountPk = idAccountingAccountPk;
		this.accountingAccount = accountingAccount;
		this.accountCode = accountCode;
		this.description = description;
		this.natureType = natureType;
		this.currency = currency;
		this.portFolio = portFolio;
		this.indAccumulation = indAccumulation;
		this.indReference = indReference;
		this.auxiliaryType = auxiliaryType;
		this.status = status;
		this.indSubAccount = indSubAccount;
		this.instrumentType = instrumentType;
	}




	/** 		
	 *  AccountingAccount full Constructor
	 * 33
	 * @param idAccountingAccountPk
	 * @param accountingAccount
	 * @param accountCode
	 * @param description
	 * @param accountType
	 * @param natureType
	 * @param currency
	 * @param indAccumulation
	 * @param indBalanceControl
	 * @param indDimension
	 * @param expiredDate
	 * @param adjustmentType
	 * @param auxiliaryType
	 * @param status
	 * @param accountingType
	 * @param indSubAccount
	 * @param indEntityAccount
	 * @param entityAccount
	 * @param registerDate
	 * @param registerUser
	 * @param updateDate
	 * @param updateUser
	 * @param cancelDate
	 * @param cancelUser
	 * @param descriptionTypeAccount
	 * @param descriptionAuxiliaryType
	 * @param descriptionNatureType
	 * @param descriptionCurrency
	 * @param descriptionStateAccount
	 * @param descriptionEntityAccount
	 * @param descriptionInstrumentType
	 */
	public AccountingAccount(Long idAccountingAccountPk,
			AccountingAccount accountingAccount, String accountCode,
			String description, Integer accountType, Integer natureType,
			Integer currency, Integer portFolio,
			Integer indAccumulation,
			Integer indBalanceControl, Integer indDimension, 
			Integer indReference,
			Date expiredDate,
			Integer adjustmentType, Integer auxiliaryType, Integer status,
			Integer accountingType, Integer indSubAccount, 
			Integer indEntityAccount, Integer entityAccount,
			Integer instrumentType,
			Date registerDate,
			String registerUser, Date updateDate, String updateUser,
			Date cancelDate, String cancelUser, String descriptionTypeAccount,
			String descriptionAuxiliaryType, String descriptionNatureType,
			String descriptionCurrency, String descriptionStateAccount,
			String descriptionEntityAccount, String descriptionInstrumentType) {
		this.idAccountingAccountPk = idAccountingAccountPk;
		this.accountingAccount = accountingAccount;
		this.accountCode = accountCode;
		this.description = description;
		this.accountType = accountType;
		this.natureType = natureType;
		this.currency = currency;
		this.portFolio= portFolio;
		this.indAccumulation = indAccumulation;
		this.indBalanceControl = indBalanceControl;
		this.indDimension = indDimension;
		this.indReference = indReference;
		this.expiredDate = expiredDate;
		this.adjustmentType = adjustmentType;
		this.auxiliaryType = auxiliaryType;
		this.status = status;
		this.accountingType = accountingType;
		this.indSubAccount = indSubAccount;
		this.indEntityAccount= indEntityAccount;
		this.entityAccount= entityAccount;
		this.instrumentType= instrumentType;
		this.registerDate = registerDate;
		this.registerUser = registerUser;
		this.updateDate = updateDate;
		this.updateUser = updateUser;
		this.cancelDate = cancelDate;
		this.cancelUser = cancelUser;
		this.descriptionTypeAccount = descriptionTypeAccount;
		this.descriptionAuxiliaryType = descriptionAuxiliaryType;
		this.descriptionNatureType = descriptionNatureType;
		this.descriptionCurrency = descriptionCurrency;
		this.descriptionStateAccount = descriptionStateAccount;
		this.descriptionEntityAccount= descriptionEntityAccount;
		this.descriptionInstrumentType=descriptionInstrumentType;
	}




	/**
	 * @param idAccountingAccountPk
	 * @param accountingAccount
	 * @param accountCode
	 * @param description
	 * @param accountType
	 * @param natureType
	 * @param currency
	 * @param indAccumulation
	 * @param indBalanceControl
	 * @param status
	 * @param accountingType
	 * @param indSubAccount
	 * @param registerDate
	 * @param amount 
	 */
	public AccountingAccount(Long idAccountingAccountPk,
			AccountingAccount accountingAccount, String accountCode,
			String description, Integer accountType, Integer natureType,
			Integer currency, Integer indAccumulation,
			Integer indBalanceControl, Integer status, Integer accountingType,
			Integer indSubAccount, Date registerDate,BigDecimal amount) {
		this.idAccountingAccountPk = idAccountingAccountPk;
		this.accountingAccount = accountingAccount;
		this.accountCode = accountCode;
		this.description = description;
		this.accountType = accountType;
		this.natureType = natureType;
		this.currency = currency;
		this.indAccumulation = indAccumulation;
		this.indBalanceControl = indBalanceControl;
		this.status = status;
		this.accountingType = accountingType;
		this.indSubAccount = indSubAccount;
		this.registerDate = registerDate;
		this.amount=amount;
	}




	/**
	 * @param accountCode
	 * @param natureType
	 * @param amount
	 * @param quantity
	 */
	public AccountingAccount(String accountCode, Integer natureType,
			BigDecimal amount, BigDecimal quantity) {
		this.accountCode = accountCode;
		this.natureType = natureType;
		this.amount = amount;
		this.quantity = quantity;
	}




	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {

		return null;
	}


	/**
	 * @return the idAccountingAccountPk
	 */
	public Long getIdAccountingAccountPk() {
		return idAccountingAccountPk;
	}


	/**
	 * @param idAccountingAccountPk the idAccountingAccountPk to set
	 */
	public void setIdAccountingAccountPk(Long idAccountingAccountPk) {
		this.idAccountingAccountPk = idAccountingAccountPk;
	}

 

	/**
	 * @return the accountCode
	 */
	public String getAccountCode() {
		return accountCode;
	}


	/**
	 * @param accountCode the accountCode to set
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the accountType
	 */
	public Integer getAccountType() {
		return accountType;
	}


	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}


	/**
	 * @return the natureType
	 */
	public Integer getNatureType() {
		return natureType;
	}


	/**
	 * @param natureType the natureType to set
	 */
	public void setNatureType(Integer natureType) {
		this.natureType = natureType;
	}


	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}


	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}


	/**
	 * @return the indAccumulation
	 */
	public Integer getIndAccumulation() {
		return indAccumulation;
	}


	/**
	 * @param indAccumulation the indAccumulation to set
	 */
	public void setIndAccumulation(Integer indAccumulation) {
		this.indAccumulation = indAccumulation;
	}


	/**
	 * @return the indBalanceControl
	 */
	public Integer getIndBalanceControl() {
		return indBalanceControl;
	}


	/**
	 * @param indBalanceControl the indBalanceControl to set
	 */
	public void setIndBalanceControl(Integer indBalanceControl) {
		this.indBalanceControl = indBalanceControl;
	}


	/**
	 * @return the indDimension
	 */
	public Integer getIndDimension() {
		return indDimension;
	}


	/**
	 * @param indDimension the indDimension to set
	 */
	public void setIndDimension(Integer indDimension) {
		this.indDimension = indDimension;
	}


	/**
	 * @return the expiredDate
	 */
	public Date getExpiredDate() {
		return expiredDate;
	}


	/**
	 * @param expiredDate the expiredDate to set
	 */
	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}


	/**
	 * @return the adjustmentType
	 */
	public Integer getAdjustmentType() {
		return adjustmentType;
	}


	/**
	 * @param adjustmentType the adjustmentType to set
	 */
	public void setAdjustmentType(Integer adjustmentType) {
		this.adjustmentType = adjustmentType;
	}


	/**
	 * @return the auxiliaryType
	 */
	public Integer getAuxiliaryType() {
		return auxiliaryType;
	}


	/**
	 * @param auxiliaryType the auxiliaryType to set
	 */
	public void setAuxiliaryType(Integer auxiliaryType) {
		this.auxiliaryType = auxiliaryType;
	}


	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}


	/**
	 * @return the accountingType
	 */
	public Integer getAccountingType() {
		return accountingType;
	}


	/**
	 * @param accountingType the accountingType to set
	 */
	public void setAccountingType(Integer accountingType) {
		this.accountingType = accountingType;
	}


	/**
	 * @return the indSubAccount
	 */
	public Integer getIndSubAccount() {
		return indSubAccount;
	}


	/**
	 * @param indSubAccount the indSubAccount to set
	 */
	public void setIndSubAccount(Integer indSubAccount) {
		this.indSubAccount = indSubAccount;
	}


	/**
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}


	/**
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}


	/**
	 * @return the registerUser
	 */
	public String getRegisterUser() {
		return registerUser;
	}


	/**
	 * @param registerUser the registerUser to set
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}


	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}


	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}


	/**
	 * @param updateUser the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	/**
	 * @return the cancelDate
	 */
	public Date getCancelDate() {
		return cancelDate;
	}


	/**
	 * @param cancelDate the cancelDate to set
	 */
	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}


	/**
	 * @return the cancelUser
	 */
	public String getCancelUser() {
		return cancelUser;
	}


	/**
	 * @param cancelUser the cancelUser to set
	 */
	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}


	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}


	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	/**
	 * @return the branch
	 */
	public Integer getBranch() {
		return branch;
	}


	/**
	 * @param branch the branch to set
	 */
	public void setBranch(Integer branch) {
		this.branch = branch;
	}


	public AccountingAccount getAccountingAccount() {
		return accountingAccount;
	}


	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	public List<AccountingAccount> getAccountingAccounts() {
		return accountingAccounts;
	}


	public void setAccountingAccounts(List<AccountingAccount> accountingAccounts) {
		this.accountingAccounts = accountingAccounts;
	}


	public String getFatherCode() {
		return fatherCode;
	}


	public void setFatherCode(String fatherCode) {
		this.fatherCode = fatherCode;
	}


	/**
	 * @return the descriptionTypeAccount
	 */
	public String getDescriptionTypeAccount() {
		return descriptionTypeAccount;
	}


	/**
	 * @param descriptionTypeAccount the descriptionTypeAccount to set
	 */
	public void setDescriptionTypeAccount(String descriptionTypeAccount) {
		this.descriptionTypeAccount = descriptionTypeAccount;
	}


	/**
	 * @return the descriptionCurrency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}


	/**
	 * @param descriptionCurrency the descriptionCurrency to set
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}


	/**
	 * @return the descriptionStateAccount
	 */
	public String getDescriptionStateAccount() {
		return descriptionStateAccount;
	}


	/**
	 * @param descriptionStateAccount the descriptionStateAccount to set
	 */
	public void setDescriptionStateAccount(String descriptionStateAccount) {
		this.descriptionStateAccount = descriptionStateAccount;
	}




	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}




	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getDescriptionAuxiliaryType() {
		return descriptionAuxiliaryType;
	}
	public void setDescriptionAuxiliaryType(String descriptionAuxiliaryType) {
		this.descriptionAuxiliaryType = descriptionAuxiliaryType;
	}
	public String getDescriptionNatureType() {
		return descriptionNatureType;
	}
	public void setDescriptionNatureType(String descriptionNatureType) {
		this.descriptionNatureType = descriptionNatureType;
	}




	public String getFatherDescription() {
		return fatherDescription;
	}




	public void setFatherDescription(String fatherDescription) {
		this.fatherDescription = fatherDescription;
	}
	


	/**
	 * @return the entityAccount
	 */
	public Integer getEntityAccount() {
		return entityAccount;
	}




	/**
	 * @param entityAccount the entityAccount to set
	 */
	public void setEntityAccount(Integer entityAccount) {
		this.entityAccount = entityAccount;
	}




	/**
	 * @return the descriptionEntityAccount
	 */
	public String getDescriptionEntityAccount() {
		return descriptionEntityAccount;
	}




	/**
	 * @param descriptionEntityAccount the descriptionEntityAccount to set
	 */
	public void setDescriptionEntityAccount(String descriptionEntityAccount) {
		this.descriptionEntityAccount = descriptionEntityAccount;
	}




	/**
	 * @return the indEntityAccount
	 */
	public Integer getIndEntityAccount() {
		return indEntityAccount;
	}




	/**
	 * @param indEntityAccount the indEntityAccount to set
	 */
	public void setIndEntityAccount(Integer indEntityAccount) {
		this.indEntityAccount = indEntityAccount;
	}




	/**
	 * @return the instrumentType
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}




	/**
	 * @param instrumentType the instrumentType to set
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}




	/**
	 * @return the descriptionInstrumentType
	 */
	public String getDescriptionInstrumentType() {
		return descriptionInstrumentType;
	}




	/**
	 * @param descriptionInstrumentType the descriptionInstrumentType to set
	 */
	public void setDescriptionInstrumentType(String descriptionInstrumentType) {
		this.descriptionInstrumentType = descriptionInstrumentType;
	}
	

	/**
	 * @return the amountAssets
	 */
	public BigDecimal getAmountAssets() {
		return amountAssets;
	}




	/**
	 * @param amountAssets the amountAssets to set
	 */
	public void setAmountAssets(BigDecimal amountAssets) {
		this.amountAssets = amountAssets;
	}




	/**
	 * @return the amountDebit
	 */
	public BigDecimal getAmountDebit() {
		return amountDebit;
	}




	/**
	 * @param amountDebit the amountDebit to set
	 */
	public void setAmountDebit(BigDecimal amountDebit) {
		this.amountDebit = amountDebit;
	}




	/**
	 * @return the indReference
	 */
	public Integer getIndReference() {
		return indReference;
	}




	/**
	 * @param indReference the indReference to set
	 */
	public void setIndReference(Integer indReference) {
		this.indReference = indReference;
	}




	/**
	 * @return the amountUSD
	 */
	public BigDecimal getAmountUSD() {
		return amountUSD;
	}




	/**
	 * @param amountUSD the amountUSD to set
	 */
	public void setAmountUSD(BigDecimal amountUSD) {
		this.amountUSD = amountUSD;
	}




	/**
	 * @return the amountBOB
	 */
	public BigDecimal getAmountBOB() {
		return amountBOB;
	}




	/**
	 * @param amountBOB the amountBOB to set
	 */
	public void setAmountBOB(BigDecimal amountBOB) {
		this.amountBOB = amountBOB;
	}




	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}




	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}




	/**
	 * @return the amountMVL
	 */
	public BigDecimal getAmountMVL() {
		return amountMVL;
	}




	/**
	 * @param amountMVL the amountMVL to set
	 */
	public void setAmountMVL(BigDecimal amountMVL) {
		this.amountMVL = amountMVL;
	}




	/**
	 * @return the amountEU
	 */
	public BigDecimal getAmountEU() {
		return amountEU;
	}




	/**
	 * @param amountEU the amountEU to set
	 */
	public void setAmountEU(BigDecimal amountEU) {
		this.amountEU = amountEU;
	}




	/**
	 * @return the amountUFV
	 */
	public BigDecimal getAmountUFV() {
		return amountUFV;
	}




	/**
	 * @param amountUFV the amountUFV to set
	 */
	public void setAmountUFV(BigDecimal amountUFV) {
		this.amountUFV = amountUFV;
	}




	/**
	 * @return the amountToBOB
	 */
	public BigDecimal getAmountToBOB() {
		return amountToBOB;
	}




	/**
	 * @param amountToBOB the amountToBOB to set
	 */
	public void setAmountToBOB(BigDecimal amountToBOB) {
		this.amountToBOB = amountToBOB;
	}




	/**
	 * @return the idAccountingAccountFk
	 */
	public Long getIdAccountingAccountFk() {
		return idAccountingAccountFk;
	}




	/**
	 * @param idAccountingAccountFk the idAccountingAccountFk to set
	 */
	public void setIdAccountingAccountFk(Long idAccountingAccountFk) {
		this.idAccountingAccountFk = idAccountingAccountFk;
	}




	/**
	 * @return the portFolio
	 */
	public Integer getPortFolio() {
		return portFolio;
	}




	/**
	 * @param portFolio the portFolio to set
	 */
	public void setPortFolio(Integer portFolio) {
		this.portFolio = portFolio;
	}




	public int compareTo(AccountingAccount o) {
		AccountingAccount accountingAccount= (AccountingAccount)o;
		String accountCode=accountingAccount.getAccountCode();
		String accountThis=this.getAccountCode();
		
		return accountThis.compareTo(accountCode);
	}




	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AccountingAccount ["
				+ (idAccountingAccountPk != null ? "idAccountingAccountPk="
						+ idAccountingAccountPk + ", " : "")
				+ (accountCode != null ? "accountCode=" + accountCode + ", "
						: "")
				+ (amount != null ? "amount=" + amount + ", " : "")
				+ (amountAssets != null ? "amountAssets=" + amountAssets + ", "
						: "")
				+ (amountDebit != null ? "amountDebit=" + amountDebit : "")
				+ "]";
	}


}
