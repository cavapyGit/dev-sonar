package com.pradera.model.accounting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCOUNTING_SOURCE_LOGGER database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_SOURCE_LOGGER")
public class AccountingSourceLogger implements Serializable,Comparable<AccountingSourceLogger> ,Auditable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4002399746208274950L;

	@Id
	@SequenceGenerator(name="ACCOUNTING_SOURCE_LOGGER_GENERATION", sequenceName="SQ_ID_ACC_SOURCE_LOGGER_PK", initialValue = 1, allocationSize = 10000)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_SOURCE_LOGGER_GENERATION")
	@Column(name="ID_ACCOUNTING_SOURCE_LOGGER_PK")
	private Long idAccountingSourceLoggerPk;
	
	//bi-directional many-to-one association to AccountingReceiptDetail
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_RECEIPT_DET_FK",referencedColumnName="ID_ACCOUNTING_RECEIPT_DET_PK")
  	private AccountingReceiptDetail accountingReceiptDetail;
  	
	//bi-directional many-to-one association to AccountingSource
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_SOURCE_FK",referencedColumnName="ID_ACCOUNTING_SOURCE_PK")
  	private AccountingSource accountingSource;

	@Column(name="RESULT_PARTIAL", precision=30, scale=8)
	private BigDecimal resultPartial;
	
  	@Column(name="FORMULATE_INPUT",length=200)
	private String formulateInput;

  	@OneToMany(mappedBy="accountingSourceLogger")
  	private List<AccountingSourceDetail> accountingSourceDetail;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("accountingSourceDetail", accountingSourceDetail);
        return detailsMap;
	}

	/**
	 * @return the idAccountingSourceLoggerPk
	 */
	public Long getIdAccountingSourceLoggerPk() {
		return idAccountingSourceLoggerPk;
	}

	/**
	 * @param idAccountingSourceLoggerPk the idAccountingSourceLoggerPk to set
	 */
	public void setIdAccountingSourceLoggerPk(Long idAccountingSourceLoggerPk) {
		this.idAccountingSourceLoggerPk = idAccountingSourceLoggerPk;
	}

	
	

	public BigDecimal getResultPartial() {
		return resultPartial;
	}

	public void setResultPartial(BigDecimal resultPartial) {
		this.resultPartial = resultPartial;
	}

	/**
	 * @return the accountingSource
	 */
	public AccountingSource getAccountingSource() {
		return accountingSource;
	}

	/**
	 * @param accountingSource the accountingSource to set
	 */
	public void setAccountingSource(AccountingSource accountingSource) {
		this.accountingSource = accountingSource;
	}

	/**
	 * @return the formulateInput
	 */
	public String getFormulateInput() {
		return formulateInput;
	}

	/**
	 * @param formulateInput the formulateInput to set
	 */
	public void setFormulateInput(String formulateInput) {
		this.formulateInput = formulateInput;
	}

	/**
	 * @return the accountingReceiptDetail
	 */
	public AccountingReceiptDetail getAccountingReceiptDetail() {
		return accountingReceiptDetail;
	}

	/**
	 * @param accountingReceiptDetail the accountingReceiptDetail to set
	 */
	public void setAccountingReceiptDetail(
			AccountingReceiptDetail accountingReceiptDetail) {
		this.accountingReceiptDetail = accountingReceiptDetail;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the accountingSourceDetail
	 */
	public List<AccountingSourceDetail> getAccountingSourceDetail() {
		return accountingSourceDetail;
	}

	/**
	 * @param accountingSourceDetail the accountingSourceDetail to set
	 */
	public void setAccountingSourceDetail(
			List<AccountingSourceDetail> accountingSourceDetail) {
		this.accountingSourceDetail = accountingSourceDetail;
	}

	@Override
	public int compareTo(AccountingSourceLogger o) {
		AccountingSourceLogger accountingLogger=(AccountingSourceLogger)o;
		Long idSourceLoggerPk=accountingLogger.getIdAccountingSourceLoggerPk();
		Long idSourceThis=this.getIdAccountingSourceLoggerPk();
		return idSourceThis.compareTo(idSourceLoggerPk);
	}
	

	
}
