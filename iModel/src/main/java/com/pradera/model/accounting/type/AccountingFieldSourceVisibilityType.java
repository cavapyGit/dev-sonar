package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2013.</li>
* </ul>
* 
* The Enum AccountingFieldSourceVisibilityType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2013
*/
public enum  AccountingFieldSourceVisibilityType {
	
	TRUE(Integer.valueOf(1),"TRUE"),
	
	FALSE(Integer.valueOf(0),"FALSE");
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingFieldSourceVisibilityType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingFieldSourceVisibilityType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingFieldSourceVisibilityType> list = new ArrayList<AccountingFieldSourceVisibilityType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingFieldSourceVisibilityType> lookup      = new HashMap<Integer, AccountingFieldSourceVisibilityType>();
	public static final Map<String, AccountingFieldSourceVisibilityType>  lookupValue = new HashMap<String, AccountingFieldSourceVisibilityType>();
	
	static {
		for (AccountingFieldSourceVisibilityType s : EnumSet.allOf(AccountingFieldSourceVisibilityType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
			lookupValue.put(s.getValue(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingFieldSourceVisibilityType state type
	 */
	public static AccountingFieldSourceVisibilityType get(Integer code) {
		return lookup.get(code);
	}
	
	public static AccountingFieldSourceVisibilityType get(String value) {
		return lookupValue.get(value);
	}
	
}
