package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the ACCOUNTING_MATRIX_DETAIL database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_MATRIX_DETAIL")
public class AccountingMatrixDetail implements Serializable ,Auditable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7334146072061057977L;


		@Id
		@SequenceGenerator(name="ACCOUNTING_MATRIX_DETAIL_GENERATION", sequenceName="SQ_ID_ACC_MATRIX_DET_PK", initialValue = 1, allocationSize = 1)
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_MATRIX_DETAIL_GENERATION")
		@Column(name="ID_ACCOUNTING_MATRIX_DETAIL_PK")
		private Long idAccountingMatrixDetailPk;
		
		
		//bi-directional many-to-one association to AccountingMatrix
	  	@ManyToOne(fetch=FetchType.LAZY)
	  	@JoinColumn(name="ID_ACCOUNTING_MATRIX_FK",referencedColumnName="ID_ACCOUNTING_MATRIX_PK")
	  	private AccountingMatrix accountingMatrix;
	  	
	  //bi-directional many-to-one association to AccountingAccount
	  	@ManyToOne(fetch=FetchType.LAZY)
	  	@JoinColumn(name="ID_ACCOUNTING_ACCOUNT_FK",referencedColumnName="ID_ACCOUNTING_ACCOUNT_PK")
	  	private AccountingAccount accountingAccount;
	  	
	  //bi-directional many-to-one association to AccountingSource
	  	@ManyToOne(fetch=FetchType.LAZY)
	  	@JoinColumn(name="ID_ACCOUNTING_SOURCE_FK",referencedColumnName="ID_ACCOUNTING_SOURCE_PK")
	  	private AccountingSource accountingSource;
	  	
	  //bi-directional many-to-one association to AccountingFormulate
	  	@ManyToOne(fetch=FetchType.LAZY)
	  	@JoinColumn(name="ID_ACCOUNTING_FORMULATE_FK",referencedColumnName="ID_ACCOUNTING_FORMULATE_PK")
	  	private AccountingFormulate accountingFormulate;
	  	
	  	@Column(name="DYNAMIC_TYPE", precision=10)
		private Integer dynamicType; 
	  	
	  	@Column(name="ASSOCIATE_CODE", precision=10)
		private Integer associateCode; 
	  	
	  	@Column(name="DOCUMENT_TYPE", precision=10)
		private Integer documentType; 
	  	
		@Column(name="AREA_TYPE", precision=10)
		private Integer areaType;
		
		@Column(name="MATRIX_DETAIL_STATE", precision=10)
		private Integer status;
	  	
	  	@Column(name="LAST_MODIFY_APP")
		private Integer lastModifyApp;

		@Temporal(TemporalType.TIMESTAMP)
		@Column(name="LAST_MODIFY_DATE")
		private Date lastModifyDate;

		@Column(name="LAST_MODIFY_IP")
		private String lastModifyIp;

		@Column(name="LAST_MODIFY_USER")
		private String lastModifyUser;
		
		@Override
		public void setAudit(LoggerUser loggerUser) {
			if (loggerUser != null) {
	            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
	            lastModifyDate = loggerUser.getAuditTime();
	            lastModifyIp = loggerUser.getIpAddress();
	            lastModifyUser = loggerUser.getUserName();
	        }
			
		}

		@Override
		public Map<String, List<? extends Auditable>> getListForAudit() {
			// TODO Auto-generated method stub
			return null;
		}

		/**
		 * @return the idAccountingMatrixDetailPk
		 */
		public Long getIdAccountingMatrixDetailPk() {
			return idAccountingMatrixDetailPk;
		}

		/**
		 * @param idAccountingMatrixDetailPk the idAccountingMatrixDetailPk to set
		 */
		public void setIdAccountingMatrixDetailPk(Long idAccountingMatrixDetailPk) {
			this.idAccountingMatrixDetailPk = idAccountingMatrixDetailPk;
		}

		/**
		 * @return the accountingMatrix
		 */
		public AccountingMatrix getAccountingMatrix() {
			return accountingMatrix;
		}

		/**
		 * @param accountingMatrix the accountingMatrix to set
		 */
		public void setAccountingMatrix(AccountingMatrix accountingMatrix) {
			this.accountingMatrix = accountingMatrix;
		}

		/**
		 * @return the accountingAccount
		 */
		public AccountingAccount getAccountingAccount() {
			return accountingAccount;
		}

		/**
		 * @param accountingAccount the accountingAccount to set
		 */
		public void setAccountingAccount(AccountingAccount accountingAccount) {
			this.accountingAccount = accountingAccount;
		}

		/**
		 * @return the accountingSource
		 */
		public AccountingSource getAccountingSource() {
			return accountingSource;
		}

		/**
		 * @param accountingSource the accountingSource to set
		 */
		public void setAccountingSource(AccountingSource accountingSource) {
			this.accountingSource = accountingSource;
		}

		

		/**
		 * @return the accountingFormulate
		 */
		public AccountingFormulate getAccountingFormulate() {
			return accountingFormulate;
		}

		/**
		 * @param accountingFormulate the accountingFormulate to set
		 */
		public void setAccountingFormulate(AccountingFormulate accountingFormulate) {
			this.accountingFormulate = accountingFormulate;
		}

		/**
		 * @return the dynamicType
		 */
		public Integer getDynamicType() {
			return dynamicType;
		}

		/**
		 * @param dynamicType the dynamicType to set
		 */
		public void setDynamicType(Integer dynamicType) {
			this.dynamicType = dynamicType;
		}

		/**
		 * @return the associateCode
		 */
		public Integer getAssociateCode() {
			return associateCode;
		}

		/**
		 * @param associateCode the associateCode to set
		 */
		public void setAssociateCode(Integer associateCode) {
			this.associateCode = associateCode;
		}

		/**
		 * @return the documentType
		 */
		public Integer getDocumentType() {
			return documentType;
		}

		/**
		 * @param documentType the documentType to set
		 */
		public void setDocumentType(Integer documentType) {
			this.documentType = documentType;
		}

		/**
		 * @return the areaType
		 */
		public Integer getAreaType() {
			return areaType;
		}

		/**
		 * @param areaType the areaType to set
		 */
		public void setAreaType(Integer areaType) {
			this.areaType = areaType;
		}

		/**
		 * @return the lastModifyApp
		 */
		public Integer getLastModifyApp() {
			return lastModifyApp;
		}

		/**
		 * @param lastModifyApp the lastModifyApp to set
		 */
		public void setLastModifyApp(Integer lastModifyApp) {
			this.lastModifyApp = lastModifyApp;
		}

		/**
		 * @return the lastModifyDate
		 */
		public Date getLastModifyDate() {
			return lastModifyDate;
		}

		/**
		 * @param lastModifyDate the lastModifyDate to set
		 */
		public void setLastModifyDate(Date lastModifyDate) {
			this.lastModifyDate = lastModifyDate;
		}

		/**
		 * @return the lastModifyIp
		 */
		public String getLastModifyIp() {
			return lastModifyIp;
		}

		/**
		 * @param lastModifyIp the lastModifyIp to set
		 */
		public void setLastModifyIp(String lastModifyIp) {
			this.lastModifyIp = lastModifyIp;
		}

		/**
		 * @return the lastModifyUser
		 */
		public String getLastModifyUser() {
			return lastModifyUser;
		}

		/**
		 * @param lastModifyUser the lastModifyUser to set
		 */
		public void setLastModifyUser(String lastModifyUser) {
			this.lastModifyUser = lastModifyUser;
		}

		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}
		
		
		
}
