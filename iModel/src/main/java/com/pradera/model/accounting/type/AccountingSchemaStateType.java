package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum AccountingSchemaStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum AccountingSchemaStateType {
	
 
	REGISTERED(Integer.valueOf(2127),"REGISTRADO"),
	
  
	LOCK(Integer.valueOf(2128),"BLOQUEADO"),
	
	 
	CANCELED(Integer.valueOf(2129),"CANCELADO"),
	
	
	WITH_OUT_SOURCE(Integer.valueOf(2130),"SIN FUENTE");
	
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingSchemaStateType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingSchemaStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingSchemaStateType> list = new ArrayList<AccountingSchemaStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingSchemaStateType> lookup = new HashMap<Integer, AccountingSchemaStateType>();
	static {
		for (AccountingSchemaStateType s : EnumSet.allOf(AccountingSchemaStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingSchemaStateType state type
	 */
	public static AccountingSchemaStateType get(Integer code) {
		return lookup.get(code);
	}

}
