package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
/**
 * The persistent class for the ACCOUNTING_PARAMETER database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_PARAMETER")
public class AccountingParameter implements Serializable ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4831942806530074169L;

	
	@Id
	@SequenceGenerator(name="ACCOUNTING_PARAMETER_GENERATION", sequenceName="SQ_ID_ACCOUNTING_PARAMETER_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_PARAMETER_GENERATION")
	@Column(name="ID_ACCOUNTING_PARAMETER_PK")
	private Integer idAccountingParameterPk;
	
	@Column(name="PARAMETER_TYPE", precision=10)
	private Integer parameterType;
	
	@Column(name="NR_CORRELATIVE", precision=10)
	private Integer nrCorrelative;
	
	@Column(name="PARAMETER_NAME", length=200)
	private String parameterName;
	
	@Column(name="DESCRIPTION", length=200)
	private String description;
	
	@Column(name="STATUS", precision=10)
	private Integer status;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="REGISTER_DATE")
	private Date registerDate;
	  
	@Column(name="REGISTER_USER", length=20)
	private String registerUser;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="UPDATE_DATE")
	private Date updateDate;
	
	@Column(name="UPDATE_USER", length=20)
	private String updateUser;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="CANCEL_DATE")
	private Date cancelDate;
	
	@Column(name="CANCEL_USER", length=20)
	private String cancelUser;
	

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	
	
	
	@Transient	
	private String descriptionParameterType;
	@Transient
	private String descriptionParameterStatus;
	@Transient
	private String nameWithDescription;
	
	
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	

	public Integer getIdAccountingParameterPk() {
		return idAccountingParameterPk;
	}

	public void setIdAccountingParameterPk(Integer idAccountingParameterPk) {
		this.idAccountingParameterPk = idAccountingParameterPk;
	}

	public Integer getParameterType() {
		return parameterType;
	}

	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}

	public Integer getNrCorrelative() {
		return nrCorrelative;
	}

	public void setNrCorrelative(Integer nrCorrelative) {
		this.nrCorrelative = nrCorrelative;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancelUser() {
		return cancelUser;
	}

	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getDescriptionParameterType() {
		return descriptionParameterType;
	}

	public void setDescriptionParameterType(String descriptionParameterType) {
		this.descriptionParameterType = descriptionParameterType;
	}

	public String getDescriptionParameterStatus() {
		return descriptionParameterStatus;
	}

	public void setDescriptionParameterStatus(String descriptionParameterStatus) {
		this.descriptionParameterStatus = descriptionParameterStatus;
	}

	public String getNameWithDescription() {
		return nameWithDescription;
	}

	public void setNameWithDescription(String nameWithDescription) {
		this.nameWithDescription = nameWithDescription;
	}

	 
	

}
