package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCOUNTING_SOURCE database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_SOURCE")
@NamedQueries({
	@NamedQuery(name = AccountingSource.ACCOUNTING_SOURCE_CONTENT_BY_PK, query = " select acs.fileXml from AccountingSource acs where acs.idAccountingSourcePk =:idAccountingSourcePk ")
})
public class AccountingSource implements Serializable ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1725482598175899458L;
	public static final String ACCOUNTING_SOURCE_CONTENT_BY_PK = "AccountingSource.getContentByPk";

	@Id
	@SequenceGenerator(name="ACCOUNTING_SOURCE_GENERATION", sequenceName="SQ_ID_ACCOUNTING_SOURCE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_SOURCE_GENERATION")
	@Column(name="ID_ACCOUNTING_SOURCE_PK")
	private Long idAccountingSourcePk;
	
	 /** The File XML. */
    @Lob
    @NotNull
	@Column(name="FILE_XML")
    @Basic(fetch=FetchType.LAZY)
	private byte[] fileXml;
    
    @Column(name="AUXILIARY_TYPE", precision=10)
	private Integer auxiliaryType; 
    
    @Column(name="FILE_SOURCE_NAME", length=100)
    private String fileSourceName;
    
    @Column(name="DESCRIPTION_SOURCE", length=200)
    private String descriptionSource;
    
    @Column(name="STATUS", precision=10)
	private Integer status;
    
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private String descriptionSourceStatus;
	
	@Transient
	private String descriptionAuxiliaryType;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the idAccountingSourcePk
	 */
	public Long getIdAccountingSourcePk() {
		return idAccountingSourcePk;
	}

	/**
	 * @param idAccountingSourcePk the idAccountingSourcePk to set
	 */
	public void setIdAccountingSourcePk(Long idAccountingSourcePk) {
		this.idAccountingSourcePk = idAccountingSourcePk;
	}

	/**
	 * @return the fileXml
	 */
	public byte[] getFileXml() {
		return fileXml;
	}

	/**
	 * @param fileXml the fileXml to set
	 */
	public void setFileXml(byte[] fileXml) {
		this.fileXml = fileXml;
	}

	
	/**
	 * @return the auxiliaryType
	 */
	public Integer getAuxiliaryType() {
		return auxiliaryType;
	}

	/**
	 * @param auxiliaryType the auxiliaryType to set
	 */
	public void setAuxiliaryType(Integer auxiliaryType) {
		this.auxiliaryType = auxiliaryType;
	}

	/**
	 * @return the fileSourceName
	 */
	public String getFileSourceName() {
		return fileSourceName;
	}

	/**
	 * @param fileSourceName the fileSourceName to set
	 */
	public void setFileSourceName(String fileSourceName) {
		this.fileSourceName = fileSourceName;
	}

	/**
	 * @return the descriptionSource
	 */
	public String getDescriptionSource() {
		return descriptionSource;
	}

	/**
	 * @param descriptionSource the descriptionSource to set
	 */
	public void setDescriptionSource(String descriptionSource) {
		this.descriptionSource = descriptionSource;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescriptionSourceStatus() {
		return descriptionSourceStatus;
	}

	public void setDescriptionSourceStatus(String descriptionSourceStatus) {
		this.descriptionSourceStatus = descriptionSourceStatus;
	}

	public String getDescriptionAuxiliaryType() {
		return descriptionAuxiliaryType;
	}

	public void setDescriptionAuxiliaryType(String descriptionAuxiliaryType) {
		this.descriptionAuxiliaryType = descriptionAuxiliaryType;
	}
	
	
}
