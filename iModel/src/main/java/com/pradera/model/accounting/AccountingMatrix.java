package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;



/**
 * The persistent class for the ACCOUNTING_MATRIX database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_MATRIX")
public class AccountingMatrix implements Serializable ,Auditable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3675753329547154326L;

	@Id
	@SequenceGenerator(name="ACCOUNTING_MATRIX_GENERATION", sequenceName="SQ_ID_ACCOUNTING_MATRIX_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_MATRIX_GENERATION")
	@Column(name="ID_ACCOUNTING_MATRIX_PK")
	private Long idAccountingMatrixPk;
	
	//bi-directional many-to-one association to AccountingSchema
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_SCHEMA_FK",referencedColumnName="ID_ACCOUNTING_SCHEMA_PK")
  	private AccountingSchema accountingSchema;
  	
  	@OneToMany(fetch=FetchType.LAZY,mappedBy="accountingMatrix")
  	private List<AccountingMatrixDetail> accountingMatrixDetail;
	
	@Column(name="STATUS", precision=10)
	private Integer status; 

	@Column(name="AUXILIARY_TYPE", precision=10)
	private Integer auxiliaryType; 
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the idAccountingMatrixPk
	 */
	public Long getIdAccountingMatrixPk() {
		return idAccountingMatrixPk;
	}

	/**
	 * @param idAccountingMatrixPk the idAccountingMatrixPk to set
	 */
	public void setIdAccountingMatrixPk(Long idAccountingMatrixPk) {
		this.idAccountingMatrixPk = idAccountingMatrixPk;
	}

	/**
	 * @return the accountingSchema
	 */
	public AccountingSchema getAccountingSchema() {
		return accountingSchema;
	}

	/**
	 * @param accountingSchema the accountingSchema to set
	 */
	public void setAccountingSchema(AccountingSchema accountingSchema) {
		this.accountingSchema = accountingSchema;
	}

	/**
	 * @return the accountingMatrixDetail
	 */
	public List<AccountingMatrixDetail> getAccountingMatrixDetail() {
		return accountingMatrixDetail;
	}

	/**
	 * @param accountingMatrixDetail the accountingMatrixDetail to set
	 */
	public void setAccountingMatrixDetail(
			List<AccountingMatrixDetail> accountingMatrixDetail) {
		this.accountingMatrixDetail = accountingMatrixDetail;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the auxiliaryType
	 */
	public Integer getAuxiliaryType() {
		return auxiliaryType;
	}

	/**
	 * @param auxiliaryType the auxiliaryType to set
	 */
	public void setAuxiliaryType(Integer auxiliaryType) {
		this.auxiliaryType = auxiliaryType;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	
	
}
