package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ACCOUNTING_PROCESS database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-ago-2015
 */
@Entity
@Table(name="ACCOUNTING_PROCESS")
@NamedQueries({
	@NamedQuery(name = AccountingProcess.ACCOUNTING_PROCESS_CONTENT_BY_PK, query = " select acp.fileXml from AccountingProcess acp where acp.idAccountingProcessPk =:idAccountingProcessPk ")
})
public class AccountingProcess implements Serializable ,Auditable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 538549331400873660L;
	
	/** The Constant ACCOUNTING_PROCESS_CONTENT_BY_PK. */
	public static final String ACCOUNTING_PROCESS_CONTENT_BY_PK = "AccountingProcess.getContentByPk";
	
	/** The id accounting process pk. */
	@Id
	@SequenceGenerator(name="ACCOUNTING_PROCESS_GENERATION", sequenceName="SQ_ID_ACCOUNTING_PROCESS_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_PROCESS_GENERATION")
	@Column(name="ID_ACCOUNTING_PROCESS_PK")
	private Long idAccountingProcessPk;

	/** The process type. */
	@Column(name="PROCESS_TYPE", precision=10)
	private Integer processType;
	
	@Column(name="START_TYPE", precision=10)
	private Integer startType;

	/** The status. */
	@Column(name="STATUS", precision=10)
	private Integer status;
	
	/** The accounting receipt. */
	@OneToMany(fetch=FetchType.LAZY,mappedBy="accountingProcess")
	private List<AccountingReceipt> accountingReceipt;
	
	/** The execution date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="EXECUTION_DATE")
	private Date executionDate;
	
	/** The count receipt. 
	 * Number Schemas for Process
	 * */
	@Column(name="COUNT_RECEIPT", precision=10)
	private Integer countReceipt;
	
	/** The operation date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The file xml. */
	@Lob
    @NotNull
	@Column(name="FILE_XML")
    @Basic(fetch=FetchType.LAZY)
	private byte[] fileXml;

	/** The file xml. */
	@Lob
//    @NotNull
	@Column(name="FILE_SAP")
    @Basic(fetch=FetchType.LAZY)
	private byte[] fileSap;
	
	/** The name file. */
	@Column(name="NAME_FILE")
	private String nameFile;
	
	/** The description process type. */
	@Transient	
	private String descriptionProcessType;
	
	/** The calculate date. */
	@Transient	
	private String calculateDate;
	
	/** The generado. */
	@Transient
	private Integer generado;
	
	/** The descuadrado. */
	@Transient
	private Integer descuadrado;
	
	/** The errado. */
	@Transient
	private Integer errado;
	
	/** The description status. */
	@Transient
	private String descriptionStatus;
	
	
	
	
	/**
	 * Instantiates a new accounting process.
	 */
	public AccountingProcess() {
	}

	/**
	 * Instantiates a new accounting process.
	 *
	 * @param idAccountingProcessPk the id accounting process pk
	 * @param processType the process type
	 * @param status the status
	 * @param executionDate the execution date
	 * @param operationDate the operation date
	 * @param descriptionProcessType the description process type
	 */
	public AccountingProcess(Long idAccountingProcessPk, Integer processType,
			Integer status, Date executionDate, Date operationDate,
			String descriptionProcessType) {
		this.idAccountingProcessPk = idAccountingProcessPk;
		this.processType = processType;
		this.status = status;
		this.executionDate = executionDate;
		this.operationDate = operationDate;
		this.descriptionProcessType = descriptionProcessType;
	}

	
	
	/**
	 * Instantiates a new accounting process.
	 *
	 * @param idAccountingProcessPk the id accounting process pk
	 * @param processType the process type
	 * @param status the status
	 * @param executionDate the execution date
	 * @param operationDate the operation date
	 * @param nameFile the name file
	 * @param descriptionProcessType the description process type
	 */
	public AccountingProcess(Long idAccountingProcessPk, Integer processType,
			Integer status, Date executionDate, Date operationDate,
			String nameFile, String descriptionProcessType) {
		this.idAccountingProcessPk = idAccountingProcessPk;
		this.processType = processType;
		this.status = status;
		this.executionDate = executionDate;
		this.operationDate = operationDate;
		this.nameFile = nameFile;
		this.descriptionProcessType = descriptionProcessType;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("accountingReceipt", accountingReceipt);
        return detailsMap;
	}

	/**
	 * Gets the id accounting process pk.
	 *
	 * @return the idAccountingProcessPk
	 */
	public Long getIdAccountingProcessPk() {
		return idAccountingProcessPk;
	}

	/**
	 * Sets the id accounting process pk.
	 *
	 * @param idAccountingProcessPk the idAccountingProcessPk to set
	 */
	public void setIdAccountingProcessPk(Long idAccountingProcessPk) {
		this.idAccountingProcessPk = idAccountingProcessPk;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the processType
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the processType to set
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the execution date.
	 *
	 * @return the executionDate
	 */
	public Date getExecutionDate() {
		return executionDate;
	}

	/**
	 * Sets the execution date.
	 *
	 * @param executionDate the executionDate to set
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the description process type.
	 *
	 * @return the description process type
	 */
	public String getDescriptionProcessType() {
		return descriptionProcessType;
	}

	/**
	 * Sets the description process type.
	 *
	 * @param descriptionProcessType the new description process type
	 */
	public void setDescriptionProcessType(String descriptionProcessType) {
		this.descriptionProcessType = descriptionProcessType;
	}


	
	


	/**
	 * Gets the generado.
	 *
	 * @return the generado
	 */
	public Integer getGenerado() {
		return generado;
	}

	/**
	 * Sets the generado.
	 *
	 * @param generado the generado to set
	 */
	public void setGenerado(Integer generado) {
		this.generado = generado;
	}

	/**
	 * Gets the descuadrado.
	 *
	 * @return the descuadrado
	 */
	public Integer getDescuadrado() {
		return descuadrado;
	}

	/**
	 * Sets the descuadrado.
	 *
	 * @param descuadrado the descuadrado to set
	 */
	public void setDescuadrado(Integer descuadrado) {
		this.descuadrado = descuadrado;
	}

	/**
	 * Gets the errado.
	 *
	 * @return the errado
	 */
	public Integer getErrado() {
		return errado;
	}

	/**
	 * Sets the errado.
	 *
	 * @param errado the errado to set
	 */
	public void setErrado(Integer errado) {
		this.errado = errado;
	}

	/**
	 * Gets the description status.
	 *
	 * @return the descriptionStatus
	 */
	public String getDescriptionStatus() {
		return descriptionStatus;
	}

	/**
	 * Sets the description status.
	 *
	 * @param descriptionStatus the descriptionStatus to set
	 */
	public void setDescriptionStatus(String descriptionStatus) {
		this.descriptionStatus = descriptionStatus;
	}

	/**
	 * Gets the accounting receipt.
	 *
	 * @return the accountingReceipt
	 */
	public List<AccountingReceipt> getAccountingReceipt() {
		return accountingReceipt;
	}

	/**
	 * Sets the accounting receipt.
	 *
	 * @param accountingReceipt the accountingReceipt to set
	 */
	public void setAccountingReceipt(List<AccountingReceipt> accountingReceipt) {
		this.accountingReceipt = accountingReceipt;
	}

	

	/**
	 * Gets the calculate date.
	 *
	 * @return the calculateDate
	 */
	public String getCalculateDate() {
		return calculateDate;
	}

	/**
	 * Sets the calculate date.
	 *
	 * @param calculateDate the calculateDate to set
	 */
	public void setCalculateDate(String calculateDate) {
		this.calculateDate = calculateDate;
	}

	/**
	 * Gets the count receipt.
	 *
	 * @return the countReceipt
	 */
	public Integer getCountReceipt() {
		return countReceipt;
	}

	/**
	 * Sets the count receipt.
	 *
	 * @param countReceipt the countReceipt to set
	 */
	public void setCountReceipt(Integer countReceipt) {
		this.countReceipt = countReceipt;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the file xml.
	 *
	 * @return the fileXml
	 */
	public byte[] getFileXml() {
		return fileXml;
	}

	/**
	 * Sets the file xml.
	 *
	 * @param fileXml the fileXml to set
	 */
	public void setFileXml(byte[] fileXml) {
		this.fileXml = fileXml;
	}

	/**
	 * Gets the name file.
	 *
	 * @return the nameFile
	 */
	public String getNameFile() {
		return nameFile;
	}

	/**
	 * Sets the name file.
	 *
	 * @param nameFile the nameFile to set
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	/**
	 * @return the startType
	 */
	public Integer getStartType() {
		return startType;
	}

	/**
	 * @param startType the startType to set
	 */
	public void setStartType(Integer startType) {
		this.startType = startType;
	}

	/**
	 * @return the fileSap
	 */
	public byte[] getFileSap() {
		return fileSap;
	}

	/**
	 * @param fileSap the fileSap to set
	 */
	public void setFileSap(byte[] fileSap) {
		this.fileSap = fileSap;
	}

	
	
	
}
