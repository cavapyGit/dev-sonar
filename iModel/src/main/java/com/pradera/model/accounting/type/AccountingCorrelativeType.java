package com.pradera.model.accounting.type;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AccountingCorrelativeType {

	
	
	ACCOUNTING_RECEIPT_FUNDS(Integer.valueOf(11),"ACCOUNTING_RECEIPT_FUNDS"),
	
	ACCOUNTING_RECEIPT_SECURITIES(Integer.valueOf(12),"ACCOUNTING_RECEIPT_SECURITIES"),
	
	ACCOUNTING_RECEIPT_ADMIN(Integer.valueOf(13),"ACCOUNTING_RECEIPT_ADMIN");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<AccountingCorrelativeType> list=new ArrayList<AccountingCorrelativeType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, AccountingCorrelativeType> lookup=new HashMap<Integer, AccountingCorrelativeType>();

	static {
		for(AccountingCorrelativeType c : AccountingCorrelativeType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	private AccountingCorrelativeType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
//	public String getDescription(Locale locale) {
//		return PropertiesUtilities.getMessage(locale, this.getValue());
//	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the document type
	 */
	public static AccountingCorrelativeType get(Integer code) {
		return lookup.get(code);
	}
}
