package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2013.</li>
* </ul>
* 
* The Enum AccountingParameterType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2013
*/
public enum  AccountingParameterType {
	
	
	
	SUCURSALES(Integer.valueOf(2092),"SUCURSALES"),
	
	FUENTE_DE_FONDOS(Integer.valueOf(2093),"FUENTE DE FONDOS"),
	
	FUENTE_DE_VALORES(Integer.valueOf(2094),"FUENTE DE VALORES"),
	
	TIPO_DE_CUENTA(Integer.valueOf(2095),"TIPO DE CUENTA"),
	
	NATURALEZA_DE_LA_CUENTA(Integer.valueOf(2096),"NATURALEZA DE LA CUENTA"),
	
	TIPO_DIARIO_AUXILIAR(Integer.valueOf(2097),"TIPO DE DIARIO AUXILIAR"),
	 
	CENTRO_DE_COSTOS(Integer.valueOf(2099),"CENTRO DE COSTOS"),
	
	VARIABLE_DE_FORMULA(Integer.valueOf(2087),"VARIABLE DE FORMULA"),
	
	OPERADOR_DE_FORMULA(Integer.valueOf(2091),"OPERADOR DE FORMULA"),
	
	CODIGO_DE_ASOCIADO(Integer.valueOf(2100),"CODIGO DE ASOCIADO"),
	
	CUARTO_NIVEL(Integer.valueOf(2086),"CUARTO NIVEL"),
	
	TIPO_DE_AJUSTE(Integer.valueOf(2098),"TIPO DE AJUSTE"),
	
	NUMERO_DE_HILOS(Integer.valueOf(2101),"NUMERO DE HILOS"),
	
	ENTIDAD_DE_CUENTA(Integer.valueOf(2134),"ENTIDAD DE LA CUENTA"),

	CONCILIACION_CONTABLE_CARTERA(Integer.valueOf(2285),"CONCILIACION CONTABLE CON CARTERA")
	
	;
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * 
	 * @param code the code
	 * @param value the value
	 */
	private AccountingParameterType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingParameterType> list = new ArrayList<AccountingParameterType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingParameterType> lookup = new HashMap<Integer, AccountingParameterType>();
	static {
		for (AccountingParameterType s : EnumSet.allOf(AccountingParameterType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return  
	 */
	public static AccountingParameterType get(Integer code) {
		return lookup.get(code);
	}
	
	
	

}
