package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum AccountingTypeDailyAuxiliary. TIPO_DIARIO_AUXILIAR
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum AccountingTypeDailyAuxiliary {
	
 

	FONDOS(Integer.valueOf(11),"FONDOS"),
	
	VALORES(Integer.valueOf(12),"VALORES"),
	
	ADMINISTRACION(Integer.valueOf(13),"ADMINISTRACION");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingTypeDailyAuxiliary state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingTypeDailyAuxiliary(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingTypeDailyAuxiliary> list = new ArrayList<AccountingTypeDailyAuxiliary>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingTypeDailyAuxiliary> lookup = new HashMap<Integer, AccountingTypeDailyAuxiliary>();
	static {
		for (AccountingTypeDailyAuxiliary s : EnumSet.allOf(AccountingTypeDailyAuxiliary.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingTypeDailyAuxiliary state type
	 */
	public static AccountingTypeDailyAuxiliary get(Integer code) {
		return lookup.get(code);
	}

}
