package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2013.</li>
* </ul>
* 
* The Enum AccountingAssociateCodeType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2013
*/
public enum  AccountingAssociateCodeType {
	

	PARTICIPANT(Integer.valueOf(59),"PARTICIPANT"),
	
	SECURITY(Integer.valueOf(60),"SECURITY"),
	
	ISSUER(Integer.valueOf(61),"ISSUER"),
	
	PARTICIPANT_SECURITY(Integer.valueOf(62),"PARTICIPANT_SECURITY"),
	
	ISSUER_SECURITY(Integer.valueOf(63),"ISSUER_SECURITY"),	
	
	HOLDER_SECURITY(Integer.valueOf(64),"HOLDER_SECURITY"),
	
	HOLDER(Integer.valueOf(65),"HOLDER"),
	
	PARTICIPANT_SECURITY_MOVEMENT(Integer.valueOf(72),"PARTICIPANT_SECURITY_MOVEMENT");
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingAssociateCodeType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingAssociateCodeType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingAssociateCodeType> list = new ArrayList<AccountingAssociateCodeType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingAssociateCodeType> lookup      = new HashMap<Integer, AccountingAssociateCodeType>();
	public static final Map<String, AccountingAssociateCodeType>  lookupValue = new HashMap<String, AccountingAssociateCodeType>();
	
	static {
		for (AccountingAssociateCodeType s : EnumSet.allOf(AccountingAssociateCodeType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
			lookupValue.put(s.getValue(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingAssociateCodeType state type
	 */
	public static AccountingAssociateCodeType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static AccountingAssociateCodeType get(String value) {
		return lookupValue.get(value);
	}
	
}
