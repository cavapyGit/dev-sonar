package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2013.</li>
* </ul>
* 
* The Enum AccountingBranchType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2013
*/
public enum  AccountingBranchType {
	
	CONTABILIDAD_VALORIZADA_VALORES(Integer.valueOf(1),"CONTABILIDAD DE VALORIZADO DE VALORES"),
	
	CONTABILIDAD_SERVICIOS(Integer.valueOf(2),"CONTABILIDAD DE SERVICIOS"),
	
	CONTABILIDAD_DE_FONDOS_MARGEN(Integer.valueOf(3),"CONTABILIDAD FONDOS DE MARGEN"),
	
	CONTABILIDAD_CANTIDADES_VALORES(Integer.valueOf(4),"CONTABILIDAD EN CANTIDADES DE VALORES"),
	
	CONTABILIDAD_DE_FONDOS_COMPENSACION(Integer.valueOf(5),"CONTABILIDAD FONDOS DE COMPENSACION"),
	
	CONTABILIDAD_DE_FONDOS_COORPERATIVO(Integer.valueOf(6),"CONTABILIDAD FONDOS DE COORPORATIVO");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingBranchType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingBranchType> list = new ArrayList<AccountingBranchType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingBranchType> lookup = new HashMap<Integer, AccountingBranchType>();
	static {
		for (AccountingBranchType s : EnumSet.allOf(AccountingBranchType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant state type
	 */
	public static AccountingBranchType get(Integer code) {
		return lookup.get(code);
	}
}
