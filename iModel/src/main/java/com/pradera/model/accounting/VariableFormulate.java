package com.pradera.model.accounting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ACCOUNTING_SOURCE_DETAIL database table.
 * 
 */
@Entity
@Table(name="VARIABLE_FORMULATE")
public class VariableFormulate implements Serializable ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2090883670282610522L;

	@Id
	@SequenceGenerator(name="VARIABLE_FORMULATE_GENERATION", sequenceName="SQ_ID_VARIABLE_FORMULATE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VARIABLE_FORMULATE_GENERATION")
	@Column(name="ID_VARIABLE_FORMULATE_PK")
	private Long idVariableFormulatePk;
	
	//bi-directional many-to-one association to AccountingFormulate
  	@ManyToOne(fetch=FetchType.LAZY)
  	@JoinColumn(name="ID_ACCOUNTING_FORMULATE_FK",referencedColumnName="ID_ACCOUNTING_FORMULATE_PK")
  	private AccountingFormulate accountingFormulate;
  	
  	@Column(name="POSITION_SOURCE", precision=10)
	private Integer positionSource;
  	
  	@Column(name="VARIABLE_SOURCE", length=20)
	private String variableSource;
  	
  	@Column(name="PARAMETER_FORMULATE", precision=10)
	private Integer parameterFormulate;

  	@Column(name="POSITION_FORMULATE", precision=10)
	private Integer positionFormulate;
  	
  	@Column(name="VALUE_DEFAULT",precision=30, scale=8)
	private BigDecimal valueDefault;
  	
  	@Column(name="POSITION_OFFSET", precision=10)
	private Integer positionOffSet;
	
	@Column(name="SOURCE_TYPE", precision=10)
	private Integer sourceType;
	
	@Column(name="SOURCE_DESCRIPTION")
	private Integer sourceDescription;
  	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the idVariableFormulatePk
	 */
	public Long getIdVariableFormulatePk() {
		return idVariableFormulatePk;
	}

	/**
	 * @param idVariableFormulatePk the idVariableFormulatePk to set
	 */
	public void setIdVariableFormulatePk(Long idVariableFormulatePk) {
		this.idVariableFormulatePk = idVariableFormulatePk;
	}

	/**
	 * @return the accountingFormulate
	 */
	public AccountingFormulate getAccountingFormulate() {
		return accountingFormulate;
	}

	/**
	 * @param accountingFormulate the accountingFormulate to set
	 */
	public void setAccountingFormulate(AccountingFormulate accountingFormulate) {
		this.accountingFormulate = accountingFormulate;
	}

	/**
	 * @return the positionSource
	 */
	public Integer getPositionSource() {
		return positionSource;
	}

	/**
	 * @param positionSource the positionSource to set
	 */
	public void setPositionSource(Integer positionSource) {
		this.positionSource = positionSource;
	}

	/**
	 * @return the variableSource
	 */
	public String getVariableSource() {
		return variableSource;
	}

	/**
	 * @param variableSource the variableSource to set
	 */
	public void setVariableSource(String variableSource) {
		this.variableSource = variableSource;
	}

	/**
	 * @return the parameterFormulate
	 */
	public Integer getParameterFormulate() {
		return parameterFormulate;
	}

	/**
	 * @param parameterFormulate the parameterFormulate to set
	 */
	public void setParameterFormulate(Integer parameterFormulate) {
		this.parameterFormulate = parameterFormulate;
	}

	

	/**
	 * @return the positionFormulate
	 */
	public Integer getPositionFormulate() {
		return positionFormulate;
	}

	/**
	 * @param positionFormulate the positionFormulate to set
	 */
	public void setPositionFormulate(Integer positionFormulate) {
		this.positionFormulate = positionFormulate;
	}


	/**
	 * @return the valueDefault
	 */
	public BigDecimal getValueDefault() {
		return valueDefault;
	}

	/**
	 * @param valueDefault the valueDefault to set
	 */
	public void setValueDefault(BigDecimal valueDefault) {
		this.valueDefault = valueDefault;
	}

	/**
	 * @return the positionOffSet
	 */
	public Integer getPositionOffSet() {
		return positionOffSet;
	}

	/**
	 * @param positionOffSet the positionOffSet to set
	 */
	public void setPositionOffSet(Integer positionOffSet) {
		this.positionOffSet = positionOffSet;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getSourceType() {
		return sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	public Integer getSourceDescription() {
		return sourceDescription;
	}

	public void setSourceDescription(Integer sourceDescription) {
		this.sourceDescription = sourceDescription;
	}
	
	
	

	}
