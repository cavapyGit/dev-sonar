package com.pradera.model.accounting;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the ACCOUNTING_SOURCE_DETAIL database table.
 * 
 */
@Entity
@Table(name="ACCOUNTING_SOURCE_DETAIL")
public class AccountingSourceDetail implements Serializable, Comparable<AccountingSourceDetail> ,Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5570573798439746679L;

	@Id
	@SequenceGenerator(name="ACCOUNTING_SOURCE_DETAIL_GENERATION", sequenceName="SQ_ID_ACC_SOURCE_DETAIL_PK", initialValue = 1, allocationSize = 100000)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNTING_SOURCE_DETAIL_GENERATION")
	@Column(name="ID_ACCOUNTING_SOURCE_DETAIL_PK")
	private Long idAccountingSourceDetailPk;

	//bi-directional many-to-one association to AccountingSourceLogger
  	@ManyToOne
  	@JoinColumn(name="ID_ACCOUNTING_SOURCE_LOGGER_FK",referencedColumnName="ID_ACCOUNTING_SOURCE_LOGGER_PK")
  	private AccountingSourceLogger accountingSourceLogger;
  	
  	@NotNull
  	@Column(name="PARAMETER_NAME", length=50)
	private String parameterName;
	
  	@NotNull
  	@Column(name="PARAMETER_VALUE", length=200)
	private String parameterValue;
  	
  	@Column(name="PARAMETER_TYPE", precision=10)
	private Integer parameterType;

  	@Column(name="PARAMETER_DESCRIPTION", length=200)
	private String parameterDescription;
  	
  	@Column(name="POSICION", precision=10)
	private Integer position;
  	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private Long idAccountingSourceLoggerFk;
	
	
	
	/**
	 * 
	 */
	public AccountingSourceDetail() {
	}

	/**
	 * @param idAccountingSourceDetailPk
	 * @param accountingSourceLogger
	 * @param parameterValue
	 * @param parameterType
	 */
	public AccountingSourceDetail(Long idAccountingSourceDetailPk,
			Long idAccountingSourceLoggerFk,
			String parameterValue, Integer parameterType) {
		this.idAccountingSourceDetailPk = idAccountingSourceDetailPk;
		this.idAccountingSourceLoggerFk = idAccountingSourceLoggerFk;
		this.parameterValue = parameterValue;
		this.parameterType = parameterType;
	}
	
	

	/**
	 * @param idAccountingSourceDetailPk
	 * @param parameterName
	 * @param parameterValue
	 * @param parameterType
	 * @param idAccountingSourceLoggerFk
	 */
	public AccountingSourceDetail(Long idAccountingSourceDetailPk,
			Long idAccountingSourceLoggerFk, String parameterName, 
			String parameterValue, Integer parameterType) {
		this.idAccountingSourceDetailPk = idAccountingSourceDetailPk;
		this.parameterName = parameterName;
		this.parameterValue = parameterValue;
		this.parameterType = parameterType;
		this.idAccountingSourceLoggerFk = idAccountingSourceLoggerFk;
	}

	/**
	 * @param idAccountingSourceDetailPk
	 * @param accountingSourceLogger
	 * @param parameterValue
	 * @param parameterType
	 */
	public AccountingSourceDetail(Long idAccountingSourceDetailPk,
			AccountingSourceLogger accountingSourceLogger,
			String parameterValue, Integer parameterType) {
		this.idAccountingSourceDetailPk = idAccountingSourceDetailPk;
		this.accountingSourceLogger = accountingSourceLogger;
		this.parameterValue = parameterValue;
		this.parameterType = parameterType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the idAccountingSourceDetailPk
	 */
	public Long getIdAccountingSourceDetailPk() {
		return idAccountingSourceDetailPk;
	}

	/**
	 * @param idAccountingSourceDetailPk the idAccountingSourceDetailPk to set
	 */
	public void setIdAccountingSourceDetailPk(Long idAccountingSourceDetailPk) {
		this.idAccountingSourceDetailPk = idAccountingSourceDetailPk;
	}

	/**
	 * @return the accountingSourceLogger
	 */
	public AccountingSourceLogger getAccountingSourceLogger() {
		return accountingSourceLogger;
	}

	/**
	 * @param accountingSourceLogger the accountingSourceLogger to set
	 */
	public void setAccountingSourceLogger(
			AccountingSourceLogger accountingSourceLogger) {
		this.accountingSourceLogger = accountingSourceLogger;
	}

	/**
	 * @return the parameterName
	 */
	public String getParameterName() {
		return parameterName;
	}

	/**
	 * @param parameterName the parameterName to set
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	/**
	 * @return the parameterValue
	 */
	public String getParameterValue() {
		return parameterValue;
	}

	/**
	 * @param parameterValue the parameterValue to set
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	/**
	 * @return the parameterType
	 */
	public Integer getParameterType() {
		return parameterType;
	}

	/**
	 * @param parameterType the parameterType to set
	 */
	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}

	/**
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}

	/**
	 * @return the parameterDescription
	 */
	public String getParameterDescription() {
		return parameterDescription;
	}

	/**
	 * @param parameterDescription the parameterDescription to set
	 */
	public void setParameterDescription(String parameterDescription) {
		this.parameterDescription = parameterDescription;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the idAccountingSourceLoggerFk
	 */
	public Long getIdAccountingSourceLoggerFk() {
		return idAccountingSourceLoggerFk;
	}

	/**
	 * @param idAccountingSourceLoggerFk the idAccountingSourceLoggerFk to set
	 */
	public void setIdAccountingSourceLoggerFk(Long idAccountingSourceLoggerFk) {
		this.idAccountingSourceLoggerFk = idAccountingSourceLoggerFk;
	}

	@Override
	public int compareTo(AccountingSourceDetail o) {
		
		AccountingSourceDetail accountingDetail=(AccountingSourceDetail)o;
		Long idSourceLoggerPk=accountingDetail.getIdAccountingSourceLoggerFk();
		Long idSourceThis=this.getIdAccountingSourceLoggerFk();
		return idSourceThis.compareTo(idSourceLoggerPk);
	}




}
