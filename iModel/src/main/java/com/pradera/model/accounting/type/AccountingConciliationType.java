package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2015.</li>
* </ul>
* 
* The Enum AccountingConciliationType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2013
*/
public enum  AccountingConciliationType {
	

	DEMATERIALIZED_BALANCE_AVAILABLE(Integer.valueOf(73),"DEMATERIALIZED BALANCE AVAILABLE"),
	
	DEMATERIALIZED_BALANCE_REPORTED(Integer.valueOf(74),"DEMATERIALIZED BALANCE REPORTED"),
	
	DEMATERIALIZED_BALANCE_LOCK(Integer.valueOf(75),"DEMATERIALIZED BALANCE LOCK"),
	
	DEMATERIALIZED_BALANCE_LOCK_OTHER(Integer.valueOf(76),"DEMATERIALIZED BALANCE_LOCK OTHER"),
	
	PHYSICAL_BALANCE(Integer.valueOf(77),"PHYSICAL BALANCE"),	
	
	SECURITIES_OUTSTANDING_PLACE(Integer.valueOf(78),"SECURITIES OUTSTANDING PLACE")
	
	;
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingConciliationType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingConciliationType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingConciliationType> list = new ArrayList<AccountingConciliationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingConciliationType> lookup      = new HashMap<Integer, AccountingConciliationType>();
	public static final Map<String, AccountingConciliationType>  lookupValue = new HashMap<String, AccountingConciliationType>();
	
	static {
		for (AccountingConciliationType s : EnumSet.allOf(AccountingConciliationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
			lookupValue.put(s.getValue(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingConciliationType state type
	 */
	public static AccountingConciliationType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static AccountingConciliationType get(String value) {
		return lookupValue.get(value);
	}
	
}
