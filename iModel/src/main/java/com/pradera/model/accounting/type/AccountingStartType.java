package com.pradera.model.accounting.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2013.</li>
* </ul>
* 
* The Enum AccountingAssociateCodeType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2013
*/
public enum  AccountingStartType {
	

	START(Integer.valueOf(79),"INICIO CONTABLE"),
	
	CONTINUE(Integer.valueOf(80),"PROCESO CONTABLE"),
	
	MONTHLY(Integer.valueOf(81),"PROCESO MENSUAL");
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingAssociateCodeType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AccountingStartType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AccountingStartType> list = new ArrayList<AccountingStartType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AccountingStartType> lookup      = new HashMap<Integer, AccountingStartType>();
	public static final Map<String, AccountingStartType>  lookupValue = new HashMap<String, AccountingStartType>();
	
	static {
		for (AccountingStartType s : EnumSet.allOf(AccountingStartType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
			lookupValue.put(s.getValue(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingAssociateCodeType state type
	 */
	public static AccountingStartType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static AccountingStartType get(String value) {
		return lookupValue.get(value);
	}
	
}
