package com.pradera.model.guarantees;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;

@Entity
@Table(name = "SECURITY_GVC_LIST")
public class SecurityGvcList  implements Serializable , Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "SECURITY_GVC_PK_LIST", sequenceName = "SQ_ID_SECURITY_GVC_LIST_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SECURITY_GVC_PK_LIST")
	@Column(name = "ID_SECURITY_GVC_PK")
	private Long idSecurityGvcPk;
	
	@Column(name="SECURITY_GVC_STATE")
	private Integer securityGvcState;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",referencedColumnName="ID_SECURITY_CODE_PK")
	private Security security;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_GROUP_FK",referencedColumnName="ID_GROUP_PK")
	private GvcList gvcList;
	
	/** The lastModifyApp. */
    @Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The lastModifyDate. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

    /** The lastModifyIp. */
    @Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

    /** The lastModifyUser. */
    @Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * @return the idSecurityGvcPk
	 */
	public Long getIdSecurityGvcPk() {
		return idSecurityGvcPk;
	}
	/**
	 * @param idSecurityGvcPk the idSecurityGvcPk to set
	 */
	public void setIdSecurityGvcPk(Long idSecurityGvcPk) {
		this.idSecurityGvcPk = idSecurityGvcPk;
	}
	/**
	 * @return the securityGvcState
	 */
	public Integer getSecurityGvcState() {
		return securityGvcState;
	}
	/**
	 * @param securityGvcState the securityGvcState to set
	 */
	public void setSecurityGvcState(Integer securityGvcState) {
		this.securityGvcState = securityGvcState;
	}
	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	/**
	 * @return the gvcList
	 */
	public GvcList getGvcList() {
		return gvcList;
	}
	/**
	 * @param gvcList the gvcList to set
	 */
	public void setGvcList(GvcList gvcList) {
		this.gvcList = gvcList;
	}
	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	
	
}
