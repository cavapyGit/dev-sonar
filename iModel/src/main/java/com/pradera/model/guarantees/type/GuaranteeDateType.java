package com.pradera.model.guarantees.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum GuaranteeDateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/06/2014
 */
public enum GuaranteeDateType {

	/** The contado. */
	CONTADO (Integer.valueOf(0),"FECHA LIQUIDACION CONTADO"),
	
	/** The plazo. */
	PLAZO (Integer.valueOf(1),"FECHA LIQUIDACION PLAZO");
	
	/** The code. */
	private Integer code;
	
	/** The description. */
	private String description;
	
	/** The Constant list. */
	public static final List<GuaranteeDateType> list = new ArrayList<GuaranteeDateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, GuaranteeDateType> lookup = new HashMap<Integer, GuaranteeDateType>();
	
	static {
		for (GuaranteeDateType s : EnumSet.allOf(GuaranteeDateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the guarantee type
	 */
	public static GuaranteeDateType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Instantiates a new guarantee date type.
	 *
	 * @param code the code
	 * @param description the description
	 */
	private GuaranteeDateType(Integer code, String description){
		this.code = code;
		this.description = description;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}


}