package com.pradera.model.guarantees.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum GuaranteeClassType {

	SECURITIES (Integer.valueOf(1334),"GARANTIA EN VALORES"),
	FUNDS (Integer.valueOf(1335),"GARANTIA EN FONDOS");
	
	private Integer code;
	private String description;

	private GuaranteeClassType(Integer code, String description){
		this.code = code;
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public static final List<GuaranteeClassType> list = new ArrayList<GuaranteeClassType>();
	public static final Map<Integer, GuaranteeClassType> lookup = new HashMap<Integer, GuaranteeClassType>();
	static {
		for (GuaranteeClassType s : EnumSet.allOf(GuaranteeClassType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
