package com.pradera.model.guarantees;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the GUARANTEE_OPERATION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/06/2014
 */
@Entity
@Table(name="GUARANTEE_OPERATION")
public class GuaranteeOperation implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id guarantee operation pk. */
	@Id
	@SequenceGenerator(name="GUARANTEE_OPERATION_GENERATOR", sequenceName="SQ_ID_GUARANTEE_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GUARANTEE_OPERATION_GENERATOR")
	@Column(name="ID_GUARANTEE_OPERATION_PK")
	private Long idGuaranteeOperationPk;

	/** The commission amount. */
	@Column(name="COMMISSION_AMOUNT")
	private BigDecimal commissionAmount;

	/** The currency. */
	@Column(name="CURRENCY")
	private Integer currency;

	/** The funds state. */
	@Column(name="FUNDS_STATE")
	private Integer fundsState;

	/** The guarantee balance. */
	@Column(name="GUARANTEE_BALANCE")
	private BigDecimal guaranteeBalance;

	/** The guarantee class. */
	@Column(name="GUARANTEE_CLASS")
	private Integer guaranteeClass;

	/** The guarantee type. */
	@Column(name="GUARANTEE_TYPE")
	private Integer guaranteeType;

	//bi-directional many-to-one association to holderAccount
    /** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

    //bi-directional many-to-one association to holderAccountOperation
    /** The holder account operation. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK")
	private HolderAccountOperation holderAccountOperation;

    //bi-directional many-to-one association to securities
    /** The securities. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

    //bi-directional many-to-one association to participant
    /** The participant. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	/** The interest amount. */
	@Column(name="INTEREST_AMOUNT")
	private BigDecimal interestAmount;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The operation date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	/** The operation type. */
	@Column(name="OPERATION_TYPE")
	private Long operationType;

	/** The pending amount. */
	@Column(name="PENDING_AMOUNT")
	private BigDecimal pendingAmount;

	/** The quotation price. */
	@Column(name="QUOTATION_PRICE")
	private BigDecimal quotationPrice;

	/** The tax amount. */
	@Column(name="TAX_AMOUNT")
	private BigDecimal taxAmount;

	/** The funds operations. */
	@OneToMany(mappedBy="guaranteeOperation",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<FundsOperation> fundsOperations;
	
	/** The guarantee market fact operations. */
	@OneToMany(mappedBy="guaranteeOperation",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
	private List<GuaranteeMarketFactOperation> guaranteeMarketFactOperations;
	
    /**
     * Instantiates a new guarantee operation.
     */
    public GuaranteeOperation() {
    }

	/**
	 * Gets the id guarantee operation pk.
	 *
	 * @return the id guarantee operation pk
	 */
	public Long getIdGuaranteeOperationPk() {
		return this.idGuaranteeOperationPk;
	}

	/**
	 * Sets the id guarantee operation pk.
	 *
	 * @param idGuaranteeOperationPk the new id guarantee operation pk
	 */
	public void setIdGuaranteeOperationPk(Long idGuaranteeOperationPk) {
		this.idGuaranteeOperationPk = idGuaranteeOperationPk;
	}

	/**
	 * Gets the commission amount.
	 *
	 * @return the commission amount
	 */
	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}

	/**
	 * Sets the commission amount.
	 *
	 * @param commissionAmount the new commission amount
	 */
	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}
	
	/**
	 * Gets the guarantee balance.
	 *
	 * @return the guarantee balance
	 */
	public BigDecimal getGuaranteeBalance() {
		return guaranteeBalance;
	}

	/**
	 * Sets the guarantee balance.
	 *
	 * @param guaranteeBalance the new guarantee balance
	 */
	public void setGuaranteeBalance(BigDecimal guaranteeBalance) {
		this.guaranteeBalance = guaranteeBalance;
	}

	/**
	 * Gets the interest amount.
	 *
	 * @return the interest amount
	 */
	public BigDecimal getInterestAmount() {
		return this.interestAmount;
	}

	/**
	 * Sets the interest amount.
	 *
	 * @param interestAmount the new interest amount
	 */
	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return this.operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the funds state.
	 *
	 * @return the funds state
	 */
	public Integer getFundsState() {
		return fundsState;
	}

	/**
	 * Sets the funds state.
	 *
	 * @param fundsState the new funds state
	 */
	public void setFundsState(Integer fundsState) {
		this.fundsState = fundsState;
	}

	/**
	 * Gets the guarantee class.
	 *
	 * @return the guarantee class
	 */
	public Integer getGuaranteeClass() {
		return guaranteeClass;
	}

	/**
	 * Sets the guarantee class.
	 *
	 * @param guaranteeClass the new guarantee class
	 */
	public void setGuaranteeClass(Integer guaranteeClass) {
		this.guaranteeClass = guaranteeClass;
	}

	/**
	 * Gets the guarantee type.
	 *
	 * @return the guarantee type
	 */
	public Integer getGuaranteeType() {
		return guaranteeType;
	}

	/**
	 * Sets the guarantee type.
	 *
	 * @param guaranteeType the new guarantee type
	 */
	public void setGuaranteeType(Integer guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the holder account operation.
	 *
	 * @return the holder account operation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}

	/**
	 * Sets the holder account operation.
	 *
	 * @param holderAccountOperation the new holder account operation
	 */
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}

	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the new securities
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Long getOperationType() {
		return operationType;
	}

	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}

	/**
	 * Gets the pending amount.
	 *
	 * @return the pending amount
	 */
	public BigDecimal getPendingAmount() {
		return this.pendingAmount;
	}

	/**
	 * Sets the pending amount.
	 *
	 * @param pendingAmount the new pending amount
	 */
	public void setPendingAmount(BigDecimal pendingAmount) {
		this.pendingAmount = pendingAmount;
	}

	/**
	 * Gets the quotation price.
	 *
	 * @return the quotation price
	 */
	public BigDecimal getQuotationPrice() {
		return this.quotationPrice;
	}

	/**
	 * Sets the quotation price.
	 *
	 * @param quotationPrice the new quotation price
	 */
	public void setQuotationPrice(BigDecimal quotationPrice) {
		this.quotationPrice = quotationPrice;
	}

	/**
	 * Gets the tax amount.
	 *
	 * @return the tax amount
	 */
	public BigDecimal getTaxAmount() {
		return this.taxAmount;
	}

	/**
	 * Sets the tax amount.
	 *
	 * @param taxAmount the new tax amount
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	
	/**
	 * Gets the funds operations.
	 *
	 * @return the fundsOperations
	 */
	public List<FundsOperation> getFundsOperations() {
		return fundsOperations;
	}

	/**
	 * Sets the funds operations.
	 *
	 * @param fundsOperations the fundsOperations to set
	 */
	public void setFundsOperations(List<FundsOperation> fundsOperations) {
		this.fundsOperations = fundsOperations;
	}

	/**
	 * Gets the guarantee market fact operations.
	 *
	 * @return the guarantee market fact operations
	 */
	public List<GuaranteeMarketFactOperation> getGuaranteeMarketFactOperations() {
		return guaranteeMarketFactOperations;
	}

	/**
	 * Sets the guarantee market fact operations.
	 *
	 * @param guaranteeMarketFactOperations the new guarantee market fact operations
	 */
	public void setGuaranteeMarketFactOperations(
			List<GuaranteeMarketFactOperation> guaranteeMarketFactOperations) {
		this.guaranteeMarketFactOperations = guaranteeMarketFactOperations;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}



	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("fundsOperations", fundsOperations);
		detailsMap.put("guaranteeMarketFactOperations", guaranteeMarketFactOperations);
        return detailsMap;
	}
}