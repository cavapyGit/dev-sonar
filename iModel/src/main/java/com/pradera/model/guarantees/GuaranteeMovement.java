package com.pradera.model.guarantees;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.MovementType;
import com.pradera.model.funds.CashAccountMovement;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the GUARANTEE_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="GUARANTEE_MOVEMENT")
public class GuaranteeMovement implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GUARANTEE_MOVEMENT_GENERATOR", sequenceName="SQ_ID_GUARANTEE_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GUARANTEE_MOVEMENT_GENERATOR")
	@Column(name="ID_GUARANTEE_MOVEMENT_PK")
	private Long idGuaranteeMovementPk;

	@Column(name="GUARANTEE_CLASS")
	private Integer guaranteeClass;

	@Column(name="GUARANTEE_TYPE")
	private Integer guaranteeType;

	//bi-directional many-to-one association to accountGuaranteeBalance
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCOUNT_GUAR_BALANCE_FK")
	private AccountGuaranteeBalance accountGuaranteeBalance;

    //bi-directional many-to-one association to guaranteeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_GUARANTEE_OPERATION_FK")
	private GuaranteeOperation guaranteeOperation;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	@Column(name="MOVEMENT_QUANTITY")
	private BigDecimal movementQuantity;
	
	@Column(name="CURRENCY")
	private Integer currency;

	//bi-directional many-to-one association to HolderAccountMovement
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_MOVEMENT_FK")
	private HolderAccountMovement holderAccountMovement;

   //bi-directional many-to-one association to CashAccountMovement
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CASH_ACCOUNT_MOVEMENT_FK")
	private CashAccountMovement cashAccountMovement;
    
	//bi-directional many-to-one association to MovementType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

    
    
    public GuaranteeMovement() {
    }

	public Long getIdGuaranteeMovementPk() {
		return this.idGuaranteeMovementPk;
	}

	public void setIdGuaranteeMovementPk(Long idGuaranteeMovementPk) {
		this.idGuaranteeMovementPk = idGuaranteeMovementPk;
	}

	
	public AccountGuaranteeBalance getAccountGuaranteeBalance() {
		return accountGuaranteeBalance;
	}

	public void setAccountGuaranteeBalance(
			AccountGuaranteeBalance accountGuaranteeBalance) {
		this.accountGuaranteeBalance = accountGuaranteeBalance;
	}

	public GuaranteeOperation getGuaranteeOperation() {
		return guaranteeOperation;
	}

	public void setGuaranteeOperation(GuaranteeOperation guaranteeOperation) {
		this.guaranteeOperation = guaranteeOperation;
	}

	
	public Integer getGuaranteeClass() {
		return guaranteeClass;
	}

	public void setGuaranteeClass(Integer guaranteeClass) {
		this.guaranteeClass = guaranteeClass;
	}

	public Integer getGuaranteeType() {
		return guaranteeType;
	}

	public void setGuaranteeType(Integer guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMovementDate() {
		return this.movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public BigDecimal getMovementQuantity() {
		return this.movementQuantity;
	}

	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	public HolderAccountMovement getHolderAccountMovement() {
		return this.holderAccountMovement;
	}

	public void setHolderAccountMovement(HolderAccountMovement holderAccountMovement) {
		this.holderAccountMovement = holderAccountMovement;
	}
	
	public MovementType getMovementType() {
		return this.movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	public CashAccountMovement getCashAccountMovement() {
		return cashAccountMovement;
	}

	public void setCashAccountMovement(CashAccountMovement cashAccountMovement) {
		this.cashAccountMovement = cashAccountMovement;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}
}