package com.pradera.model.guarantees.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum GuaranteeRegistrationType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/06/2014
 */
public enum GuaranteeRegistrationType {

	/** The initial guarantees. */
	INITIAL_GUARANTEES (Integer.valueOf(1),"PRESENTACION DE GARANTIA INICIAL"),
	
	/** The guarantees reposition. */
	GUARANTEES_REPOSITION (Integer.valueOf(2),"REPOSICION DE GARANTIA"),
	
	/** The aditional guarantees. */
	ADITIONAL_GUARANTEES (Integer.valueOf(3),"GARANTIAS ADICIONALES"),
	
	/** The guarantees liberation. */
	GUARANTEES_LIBERATION (Integer.valueOf(4),"RETIRO DE GARANTIAS");
	
	/** The code. */
	private Integer code;
	
	/** The description. */
	private String description;
	
	/** The Constant list. */
	public static final List<GuaranteeRegistrationType> list = new ArrayList<GuaranteeRegistrationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, GuaranteeRegistrationType> lookup = new HashMap<Integer, GuaranteeRegistrationType>();
	
	static {
		for (GuaranteeRegistrationType s : EnumSet.allOf(GuaranteeRegistrationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the guarantee registration type
	 */
	public static GuaranteeRegistrationType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Instantiates a new guarantee registration type.
	 *
	 * @param code the code
	 * @param description the description
	 */
	private GuaranteeRegistrationType(Integer code, String description){
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
