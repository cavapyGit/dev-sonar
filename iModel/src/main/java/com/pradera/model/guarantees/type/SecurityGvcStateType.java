package com.pradera.model.guarantees.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SecurityGvcStateType {

	ENABLED (Integer.valueOf(1),"REGISTRADO"),
	DISABLED (Integer.valueOf(0),"ELIMINADO");
	
	private Integer code;
	private String description;

	private SecurityGvcStateType(Integer code, String description){
		this.code = code;
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public static final List<SecurityGvcStateType> list = new ArrayList<SecurityGvcStateType>();
	public static final Map<Integer, SecurityGvcStateType> lookup = new HashMap<Integer, SecurityGvcStateType>();
	static {
		for (SecurityGvcStateType s : EnumSet.allOf(SecurityGvcStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
