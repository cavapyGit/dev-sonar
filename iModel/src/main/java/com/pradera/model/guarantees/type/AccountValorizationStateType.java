package com.pradera.model.guarantees.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AccountValorizationStateType {

//	COVERED (Integer.valueOf(1),"CUBIERTA"),
//	UNCOVERED (Integer.valueOf(2),"DESCUBIERTA");
	PENDING (Integer.valueOf(1),"PENDIENTE"),
	CANCELLED_BY_REVALORIZATION (Integer.valueOf(2),"ANULADA POR REVALORIZACION"),
	CANCELLED_BY_UNFULFILLMENT (Integer.valueOf(3),"ANULADA POR INCUMPLIMIENTO"),
	FULFILLED (Integer.valueOf(4),"CUMPLIDO");
	
	private Integer code;
	private String description;

	private AccountValorizationStateType(Integer code, String description){
		this.code = code;
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public static final List<AccountValorizationStateType> list = new ArrayList<AccountValorizationStateType>();
	public static final Map<Integer, AccountValorizationStateType> lookup = new HashMap<Integer, AccountValorizationStateType>();
	static {
		for (AccountValorizationStateType s : EnumSet.allOf(AccountValorizationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
