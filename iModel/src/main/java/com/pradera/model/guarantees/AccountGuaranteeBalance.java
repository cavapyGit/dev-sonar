package com.pradera.model.guarantees;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the ACCOUNT_GUARANTEE_BALANCE database table.
 * 
 */
@Entity
@Table(name="ACCOUNT_GUARANTEE_BALANCE")
public class AccountGuaranteeBalance implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GUARANTEE_BALANCE_GENERATOR", sequenceName="SQ_ID_ACCOUNT_GUAR_BALANCE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GUARANTEE_BALANCE_GENERATOR")
	@Column(name="ID_ACCOUNT_GUAR_BALANCE_PK")
	private Long idAccountGuarBalancePk;

	@Column(name="TOTAL_GUARANTEE")
	private BigDecimal totalGuarantee;
	
	@Column(name="PRINCIPAL_GUARANTEE")
	private BigDecimal principalGuarantee;
	
	@Column(name="MARGIN_GUARANTEE")
	private BigDecimal marginGuarantee;
	
	@Column(name="PRINCIPAL_DIVIDENDS_GUARANTEE")
	private BigDecimal principalDividendsGuarantee;
	
	@Column(name="MARGIN_DIVIDENDS_GUARANTEE")
	private BigDecimal marginDividendsGuarantee;

	@Column(name="INTEREST_MARGIN_GUARANTEE")
	private BigDecimal interestMarginGuarantee;
	
	@Column(name="GUARANTEE_CLASS")
	private Integer guaranteeClass;
	
	@Column(name="CURRENCY")
	private Integer currency;


	//bi-directional many-to-one association to holderAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

    //bi-directional many-to-one association to holderAccountOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK")
	private HolderAccountOperation holderAccountOperation;

    //bi-directional many-to-one association to institutionCashAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INSTITUTION_CASH_ACCOUNT_FK")
	private InstitutionCashAccount institutionCashAccount;

    //bi-directional many-to-one association to securities
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

    //bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Version
	private Long version;

	@Transient
	private String guaranteeClassDescription;
	
	@Transient
	private String currencyDescription;
	
	
    public AccountGuaranteeBalance() {
    	this.totalGuarantee = BigDecimal.ZERO;
    	this.principalGuarantee = BigDecimal.ZERO;
    	this.principalDividendsGuarantee = BigDecimal.ZERO;
    	this.marginGuarantee = BigDecimal.ZERO;
    	this.marginDividendsGuarantee = BigDecimal.ZERO;
    	this.interestMarginGuarantee = BigDecimal.ZERO;
    }
    
    public AccountGuaranteeBalance(
    		Long idAccountGuarBalancePk,BigDecimal totalGuarantee,
    		BigDecimal principalGuarantee,BigDecimal marginGuarantee,
    		BigDecimal principalDividendsGuarantee, BigDecimal marginDividendsGuarantee,
    		BigDecimal interestMarginGuarantee, Integer guaranteeClass, 
    		Integer currency, String guaranteeClassDesc , 
    		Long idHolderAccountPk, String alternateCode,
    		Long idParticipantPk, String mnemonic,
    		String idSecurityCodePk	
    		){
    	this.idAccountGuarBalancePk = idAccountGuarBalancePk;
    	this.totalGuarantee = totalGuarantee;
    	this.principalGuarantee = principalGuarantee;
    	this.marginGuarantee = marginGuarantee;
    	this.principalDividendsGuarantee = principalDividendsGuarantee;
    	this.marginDividendsGuarantee = marginDividendsGuarantee;
    	this.interestMarginGuarantee = interestMarginGuarantee;
    	this.guaranteeClass = guaranteeClass;
    	this.guaranteeClassDescription = guaranteeClassDesc;
    	this.guaranteeClass = guaranteeClass;
    	this.holderAccount = new HolderAccount(idHolderAccountPk);
    	this.getHolderAccount().setAlternateCode(alternateCode);
    	this.participant = new Participant(idParticipantPk, mnemonic);
    	this.securities = new Security(idSecurityCodePk);
    }
    
    public AccountGuaranteeBalance(
    		Long idAccountGuarBalancePk,BigDecimal totalGuarantee,
    		BigDecimal principalGuarantee,BigDecimal marginGuarantee,
    		BigDecimal principalDividendsGuarantee, BigDecimal marginDividendsGuarantee,
    		BigDecimal interestMarginGuarantee, Integer guaranteeClass, 
    		Integer currency, String guaranteeClassDesc , 
    		String currencyDescription, Long idInstitutionCashAccountPk		
    		){
    	this.idAccountGuarBalancePk = idAccountGuarBalancePk;
    	this.totalGuarantee = totalGuarantee;
    	this.principalGuarantee = principalGuarantee;
    	this.marginGuarantee = marginGuarantee;
    	this.principalDividendsGuarantee = principalDividendsGuarantee;
    	this.marginDividendsGuarantee = marginDividendsGuarantee;
    	this.interestMarginGuarantee = interestMarginGuarantee;
    	this.guaranteeClass = guaranteeClass;
    	this.guaranteeClassDescription = guaranteeClassDesc;
    	this.currencyDescription = currencyDescription;
    	this.institutionCashAccount = new InstitutionCashAccount();
    	this.institutionCashAccount.setIdInstitutionCashAccountPk(idInstitutionCashAccountPk);
    }

	/**
	 * @return the guaranteeClassDescription
	 */
	public String getGuaranteeClassDescription() {
		return guaranteeClassDescription;
	}

	/**
	 * @param guaranteeClassDescription the guaranteeClassDescription to set
	 */
	public void setGuaranteeClassDescription(String guaranteeClassDescription) {
		this.guaranteeClassDescription = guaranteeClassDescription;
	}

	/**
	 * @return the currencyDescription
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}

	/**
	 * @param currencyDescription the currencyDescription to set
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	public Long getIdAccountGuarBalancePk() {
		return this.idAccountGuarBalancePk;
	}

	public void setIdAccountGuarBalancePk(Long idAccountGuarBalancePk) {
		this.idAccountGuarBalancePk = idAccountGuarBalancePk;
	}

	public Integer getGuaranteeClass() {
		return guaranteeClass;
	}

	public void setGuaranteeClass(Integer guaranteeClass) {
		this.guaranteeClass = guaranteeClass;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}

	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}

	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}

	public void setInstitutionCashAccount(
			InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}

	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public BigDecimal getTotalGuarantee() {
		return totalGuarantee;
	}

	public void setTotalGuarantee(BigDecimal totalGuarantee) {
		this.totalGuarantee = totalGuarantee;
	}

	public BigDecimal getPrincipalGuarantee() {
		return principalGuarantee;
	}

	public void setPrincipalGuarantee(BigDecimal principalGuarantee) {
		this.principalGuarantee = principalGuarantee;
	}

	public BigDecimal getMarginGuarantee() {
		return marginGuarantee;
	}

	public void setMarginGuarantee(BigDecimal marginGuarantee) {
		this.marginGuarantee = marginGuarantee;
	}

	public BigDecimal getPrincipalDividendsGuarantee() {
		return principalDividendsGuarantee;
	}

	public void setPrincipalDividendsGuarantee(
			BigDecimal principalDividendsGuarantee) {
		this.principalDividendsGuarantee = principalDividendsGuarantee;
	}

	public BigDecimal getMarginDividendsGuarantee() {
		return marginDividendsGuarantee;
	}

	public void setMarginDividendsGuarantee(BigDecimal marginDividendsGuarantee) {
		this.marginDividendsGuarantee = marginDividendsGuarantee;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	
	public BigDecimal getInterestMarginGuarantee() {
		return interestMarginGuarantee;
	}

	public void setInterestMarginGuarantee(BigDecimal interestMarginGuarantee) {
		this.interestMarginGuarantee = interestMarginGuarantee;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}