package com.pradera.model.guarantees.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum GuaranteeType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/06/2014
 */
public enum GuaranteeType {

	/** The principal. */
	PRINCIPAL (Integer.valueOf(1330),"GARANTIA PRINCIPAL"),
	
	/** The margin. */
	MARGIN (Integer.valueOf(1331),"GARANTIA MARGEN"),
	
	/** The stock dividends principal. */
	STOCK_DIVIDENDS_PRINCIPAL (Integer.valueOf(1332),"LIBERADA POR PRINCIPAL"),
	
	/** The stock dividends margin. */
	STOCK_DIVIDENDS_MARGIN (Integer.valueOf(1333),"LIBERADA POR MARGEN"),
	
	/** The funds interest margin. */
	FUNDS_INTEREST_MARGIN (Integer.valueOf(0),"INTERESES POR MARGEN");
	
	/** The code. */
	private Integer code;
	
	/** The description. */
	private String description;
	
	/** The Constant list. */
	public static final List<GuaranteeType> list = new ArrayList<GuaranteeType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, GuaranteeType> lookup = new HashMap<Integer, GuaranteeType>();
	static {
		for (GuaranteeType s : EnumSet.allOf(GuaranteeType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the guarantee type
	 */
	public static GuaranteeType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Instantiates a new guarantee type.
	 *
	 * @param code the code
	 * @param description the description
	 */
	private GuaranteeType(Integer code, String description){
		this.code = code;
		this.description = description;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
