package com.pradera.model.guarantees.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum GuaranteeFundsStateType {

	PENDING_REFUNDS (Integer.valueOf(1392),"PENDIENTE DEVOLUCION"),
	REFUNDED_AMOUNT (Integer.valueOf(1393),"FONDOS DEVUELTOS");
	
	private Integer code;
	private String description;

	private GuaranteeFundsStateType(Integer code, String description){
		this.code = code;
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public static final List<GuaranteeFundsStateType> list = new ArrayList<GuaranteeFundsStateType>();
	public static final Map<Integer, GuaranteeFundsStateType> lookup = new HashMap<Integer, GuaranteeFundsStateType>();
	static {
		for (GuaranteeFundsStateType s : EnumSet.allOf(GuaranteeFundsStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
}
