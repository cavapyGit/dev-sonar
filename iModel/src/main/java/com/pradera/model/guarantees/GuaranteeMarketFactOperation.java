package com.pradera.model.guarantees;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class GuaranteeMarketFactOperation.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/06/2014
 */
@Entity
@Table(name="GUARANTEE_MARKETFACT_OPERATION")
public class GuaranteeMarketFactOperation  implements Serializable, Auditable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id inter market fact operation pk. */
	@Id
	@SequenceGenerator(name="SEQUENCE_GUARANTEE_MARKETFACT_OPERATION_PK", sequenceName="SQ_ID_GUARANTEE_MARKETFACT_OPE",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQUENCE_GUARANTEE_MARKETFACT_OPERATION_PK")
	@Column(name = "ID_MARKETFACT_OPERATION_PK")
	private Integer idGuaranteeMarketFactOperationPk;

	/** The market date. */
	@Column(name="MARKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date marketDate;	

	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	/** The market price. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	/** The quantity. */
	@Column(name="QUANTITY")
	private BigDecimal quantity;
    
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The international operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_GUARANTEE_OPERATION_FK")
	private GuaranteeOperation guaranteeOperation;

	/**
	 * Gets the id guarantee market fact operation pk.
	 *
	 * @return the id guarantee market fact operation pk
	 */
	public Integer getIdGuaranteeMarketFactOperationPk() {
		return idGuaranteeMarketFactOperationPk;
	}

	/**
	 * Sets the id guarantee market fact operation pk.
	 *
	 * @param idGuaranteeMarketFactOperationPk the new id guarantee market fact operation pk
	 */
	public void setIdGuaranteeMarketFactOperationPk(
			Integer idGuaranteeMarketFactOperationPk) {
		this.idGuaranteeMarketFactOperationPk = idGuaranteeMarketFactOperationPk;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	/**
	 * Gets the guarantee operation.
	 *
	 * @return the guarantee operation
	 */
	public GuaranteeOperation getGuaranteeOperation() {
		return guaranteeOperation;
	}

	/**
	 * Sets the guarantee operation.
	 *
	 * @param guaranteeOperation the new guarantee operation
	 */
	public void setGuaranteeOperation(GuaranteeOperation guaranteeOperation) {
		this.guaranteeOperation = guaranteeOperation;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        return detailsMap;
	}
}
