package com.pradera.model.guarantees;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.HolderAccountOperation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The persistent class for the ACCOUNT_OPERATION_VALORIZATION database table.
 * 
 */
@Entity
@Table(name="ACCOUNT_OPERATION_VALORIZATION")
public class AccountOperationValorization implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCOUNT_VALORIZATION_GENERATOR", sequenceName="SQ_ID_ACCOUNT_VALORIZATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNT_VALORIZATION_GENERATOR")
	@Column(name="ID_ACCOUNT_VALORIZATION_PK")
	private Long idAccountValorizationPk;

    @Column(name="TOTAL_VALORIZATION")
	private BigDecimal totalValorization;

	@Column(name="PRINCIPAL_VALORIZATION")
	private BigDecimal principalValorization;
	
	@Column(name="MARGIN_VALORIZATION")
	private BigDecimal marginValorization;
	
	@Column(name="MISSING_AMOUNT")
	private BigDecimal missingAmount;

	@Column(name="STATE")
	private Integer state;
	
//	@Column(name="SITUATION")
//	private Integer situation;

    @Temporal( TemporalType.DATE)
	@Column(name="VALORIZATION_DATE")
	private Date valorizationDate;
    
    @Column(name="CURRENCY")
	private Integer currency;

    @Temporal( TemporalType.DATE)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_OPERATION_FK")
	private HolderAccountOperation holderAccountOperation;

	//bi-directional many-to-one association to AccountOperationValorization
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REFERENCE_VALORIZATION_FK")
	private AccountOperationValorization accountOperationValorization;

    public AccountOperationValorization() {
    }

	/**
	 * @return the idAccountValorizationPk
	 */
	public Long getIdAccountValorizationPk() {
		return idAccountValorizationPk;
	}

	
	

//	/**
//	 * @return the situation
//	 */
//	public Integer getSituation() {
//		return situation;
//	}
//
//	/**
//	 * @param situation the situation to set
//	 */
//	public void setSituation(Integer situation) {
//		this.situation = situation;
//	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @param idAccountValorizationPk the idAccountValorizationPk to set
	 */
	public void setIdAccountValorizationPk(Long idAccountValorizationPk) {
		this.idAccountValorizationPk = idAccountValorizationPk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getMarginValorization() {
		return this.marginValorization;
	}

	public void setMarginValorization(BigDecimal marginValorization) {
		this.marginValorization = marginValorization;
	}

	public BigDecimal getMissingAmount() {
		return this.missingAmount;
	}

	public void setMissingAmount(BigDecimal missingAmount) {
		this.missingAmount = missingAmount;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getPrincipalValorization() {
		return this.principalValorization;
	}

	public void setPrincipalValorization(BigDecimal principalValorization) {
		this.principalValorization = principalValorization;
	}

	public BigDecimal getTotalValorization() {
		return this.totalValorization;
	}

	public void setTotalValorization(BigDecimal totalValorization) {
		this.totalValorization = totalValorization;
	}

	public Date getValorizationDate() {
		return this.valorizationDate;
	}

	public void setValorizationDate(Date valorizationDate) {
		this.valorizationDate = valorizationDate;
	}

	public AccountOperationValorization getAccountOperationValorization() {
		return this.accountOperationValorization;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	public void setAccountOperationValorization(AccountOperationValorization accountOperationValorization) {
		this.accountOperationValorization = accountOperationValorization;
	}

	/**
	 * @return the holderAccountOperation
	 */
	public HolderAccountOperation getHolderAccountOperation() {
		return holderAccountOperation;
	}

	/**
	 * @param holderAccountOperation the holderAccountOperation to set
	 */
	public void setHolderAccountOperation(
			HolderAccountOperation holderAccountOperation) {
		this.holderAccountOperation = holderAccountOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}