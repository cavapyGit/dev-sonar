package com.pradera.model.guarantees;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.MechanismModality;

/**
 * @author PraderaTechnologies
 * 
 */

@Entity
@Table(name = "GVC_LIST")
public class GvcList implements Serializable , Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "GVC_PK_LIST", sequenceName = "SQ_ID_GVC_LIST_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GVC_PK_LIST")
	@Column(name = "ID_GROUP_PK")
	private Long idGroupPk;

	@Column(name = "GROUP_PERCENT")
	private BigDecimal groupPercent;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifiedDate;

	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifiedApp;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifiedIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifiedUser;

	@Column(name = "SECURITY_CLASS")
	private Integer securityClass;
	
	@Column(name = "GROUP_DESCRIPTION")
	private String groupDescription;
	
	@Column(name = "GROUP_LIST")
	private Integer groupList;

    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK"),
		@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK")
		})
    private MechanismModality mechanismModality;
    
    @OneToMany(mappedBy="gvcList",fetch=FetchType.LAZY)
	private List<SecurityGvcList> securityGvcLists;

	public GvcList() {

	}

	/**
	 * @return the groupPercent
	 */
	public BigDecimal getGroupPercent() {
		return groupPercent;
	}

	/**
	 * @param groupPercent
	 *            the groupPercent to set
	 */
	public void setGroupPercent(BigDecimal groupPercent) {
		this.groupPercent = groupPercent;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate
	 *            the lastModifiedDate to set
	 */
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the lastModifiedApp
	 */
	public Integer getLastModifiedApp() {
		return lastModifiedApp;
	}

	/**
	 * @param lastModifiedApp
	 *            the lastModifiedApp to set
	 */
	public void setLastModifiedApp(Integer lastModifiedApp) {
		this.lastModifiedApp = lastModifiedApp;
	}

	/**
	 * @return the lastModifiedIp
	 */
	public String getLastModifiedIp() {
		return lastModifiedIp;
	}

	/**
	 * @param lastModifiedIp
	 *            the lastModifiedIp to set
	 */
	public void setLastModifiedIp(String lastModifiedIp) {
		this.lastModifiedIp = lastModifiedIp;
	}

	/**
	 * @return the lastModifiedUser
	 */
	public String getLastModifiedUser() {
		return lastModifiedUser;
	}

	/**
	 * @param lastModifiedUser
	 *            the lastModifiedUser to set
	 */
	public void setLastModifiedUser(String lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	/**
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * @param securityClass
	 *            the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * @return the idGroupPk
	 */
	public Long getIdGroupPk() {
		return idGroupPk;
	}

	/**
	 * @param idGroupPk the idGroupPk to set
	 */
	public void setIdGroupPk(Long idGroupPk) {
		this.idGroupPk = idGroupPk;
	}

	/**
	 * @return the groupDescription
	 */
	public String getGroupDescription() {
		return groupDescription;
	}

	/**
	 * @param groupDescription the groupDescription to set
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	/**
	 * @return the mechanismModality
	 */
	public MechanismModality getMechanismModality() {
		return mechanismModality;
	}

	/**
	 * @param mechanismModality the mechanismModality to set
	 */
	public void setMechanismModality(MechanismModality mechanismModality) {
		this.mechanismModality = mechanismModality;
	}

	/**
	 * @return the securityGvcLists
	 */
	public List<SecurityGvcList> getSecurityGvcLists() {
		return securityGvcLists;
	}

	/**
	 * @param securityGvcLists the securityGvcLists to set
	 */
	public void setSecurityGvcLists(List<SecurityGvcList> securityGvcLists) {
		this.securityGvcLists = securityGvcLists;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifiedApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifiedDate = loggerUser.getAuditTime();
            lastModifiedIp = loggerUser.getIpAddress();
            lastModifiedUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	/**
	 * @return the groupList
	 */
	public Integer getGroupList() {
		return groupList;
	}

	/**
	 * @param groupList the groupList to set
	 */
	public void setGroupList(Integer groupList) {
		this.groupList = groupList;
	}
	
}
