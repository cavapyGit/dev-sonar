package com.pradera.model.report;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.holderaccounts.HolderAccount;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the REPORT_FILTER database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
@Entity
@SequenceGenerator(name = "ReportFilter_generator", sequenceName = "SQ_ID_REPORT_FILTER_PK",initialValue = 1, allocationSize = 1)
@Table(name="REPORT_FILTER")
public class ReportFilter implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id report filter pk. */
	@Id @GeneratedValue(generator = "ReportFilter_generator")
	@Column(name="ID_REPORT_FILTER_PK")
	private Long idReportFilterPk;
	
	/** The report. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REPORT_FK")
	private Report report;
	
	/** The filter label. */
	@Column(name="FILTER_LABEL")
	private String filterLabel;
	
	@Column(name="FILTER_LABEL_ENGLISH")
	private String filterLabelEnglish;
	
	/** The filter type. */
	@Column(name="FILTER_TYPE")
	private Integer filterType;
	
	/** The filter name. */
	@Column(name="FILTER_NAME")
	private String filterName;
	
	/** The screen order. */
	@Column(name="SCREEN_ORDER")
	private Integer screenOrder;
	
	/** The sql list query. */
	@Column(name="SQL_LIST_QUERY")
	private String sqlListQuery;
	
	/** The ind required. */
	@Column(name="IND_REQUIRED")
	private Integer indRequired;
	
	/** The ind validation user. */
	@Column(name="IND_VALIDATION_USER")
	private Integer indValidationUser;
	
	/** The ind viewer. */
	@Column(name="IND_VIEWER")
	private Integer indViewer;
	
	@Column(name="RENDERED")
	private Integer rendered;
	
	@Column(name="RENDERED_INSTITUTION_TYPE")
	private Integer renderedInstitutionType;
	
	/** The max lenght. */
	@Column(name="MAX_LENGHT")
	private Integer maxLenght;

	/** The size input. */
	@Column(name="SIZE_INPUT")
	private Integer sizeInput;
	
	/** The event onblur. */
	@Column(name="EVENT_ONBLUR")
	private String eventOnblur;

	/** The event onchange. */
	@Column(name="EVENT_ONCHANGE")
	private String eventOnchange;

	/** The key filter. */
	@Column(name="KEY_FILTER")
    private String keyFilter;
	
	/** The master table. */
	@Column(name="MASTER_TABLE_FK")
	private Integer masterTable;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The ind break row. */
	@Column(name="IND_BREAK_ROW")
	private Integer indBreakRow;
	
	/** The value. */
	@Transient
	private Object value;
	
	/** The name relation filters. */
	@Transient
	private String nameRelationFilters; 
	
	/** The select menu. */
	@Transient
	private LinkedHashMap<String, Object> selectMenu;
	
	/** The disabled. */
	@Transient
	private boolean disabled;
	@Transient
	private List<HolderAccount> holderAccounts;
	
	@Transient
	private List<String> multipleValue;

   	/**
	    * Instantiates a new report filter.
	    */
	   public ReportFilter() {
    }
	   
	/**
	 * Checks if is validate.
	 *
	 * @return true, if is validate
	 */
	public boolean isValidate(){
		boolean validate = false;
		if(indValidationUser != null){
			if(indValidationUser.equals(BooleanType.YES.getCode())){
				validate = true;
			}
		}
		return validate;
	}
	
	/**
	 * Checks if is required.
	 *
	 * @return true, if is required
	 */
	public boolean isRequired() {
		boolean required = false;
		if (indRequired != null) {
			if(indRequired.equals(BooleanType.YES.getCode())) {
				required = true;
			}
		}
		return required;
	}
	
	/**
	 * Gets the id report filter pk.
	 *
	 * @return the id report filter pk
	 */
	public Long getIdReportFilterPk() {
		return idReportFilterPk;
	}
	
	/**
	 * Sets the id report filter pk.
	 *
	 * @param idReportFilterPk the new id report filter pk
	 */
	public void setIdReportFilterPk(Long idReportFilterPk) {
		this.idReportFilterPk = idReportFilterPk;
	}
	
	/**
	 * Gets the report.
	 *
	 * @return the report
	 */
	public Report getReport() {
		return report;
	}
	
	/**
	 * Sets the report.
	 *
	 * @param report the new report
	 */
	public void setReport(Report report) {
		this.report = report;
	}
	
	/**
	 * Gets the filter label.
	 *
	 * @return the filter label
	 */
	public String getFilterLabel() {
		return filterLabel;
	}
	
	/**
	 * Sets the filter label.
	 *
	 * @param filterLabel the new filter label
	 */
	public void setFilterLabel(String filterLabel) {
		this.filterLabel = filterLabel;
	}
	
	/**
	 * Gets the filter type.
	 *
	 * @return the filter type
	 */
	public Integer getFilterType() {
		return filterType;
	}
	
	/**
	 * Sets the filter type.
	 *
	 * @param filterType the new filter type
	 */
	public void setFilterType(Integer filterType) {
		this.filterType = filterType;
	}
	
	/**
	 * Gets the filter name.
	 *
	 * @return the filter name
	 */
	public String getFilterName() {
		return filterName;
	}
	
	/**
	 * Sets the filter name.
	 *
	 * @param filterName the new filter name
	 */
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	
	/**
	 * Gets the screen order.
	 *
	 * @return the screen order
	 */
	public Integer getScreenOrder() {
		return screenOrder;
	}
	
	/**
	 * Sets the screen order.
	 *
	 * @param screenOrder the new screen order
	 */
	public void setScreenOrder(Integer screenOrder) {
		this.screenOrder = screenOrder;
	}
	
	/**
	 * Gets the sql list query.
	 *
	 * @return the sql list query
	 */
	public String getSqlListQuery() {
		return sqlListQuery;
	}
	
	/**
	 * Sets the sql list query.
	 *
	 * @param sqlListQuery the new sql list query
	 */
	public void setSqlListQuery(String sqlListQuery) {
		this.sqlListQuery = sqlListQuery;
	}
	
	/**
	 * Gets the ind required.
	 *
	 * @return the ind required
	 */
	public Integer getIndRequired() {
		return indRequired;
	}
	
	/**
	 * Sets the ind required.
	 *
	 * @param indRequired the new ind required
	 */
	public void setIndRequired(Integer indRequired) {
		this.indRequired = indRequired;
	}
	
	/**
	 * Gets the ind validation user.
	 *
	 * @return the ind validation user
	 */
	public Integer getIndValidationUser() {
		return indValidationUser;
	}
	
	/**
	 * Sets the ind validation user.
	 *
	 * @param indValidationUser the new ind validation user
	 */
	public void setIndValidationUser(Integer indValidationUser) {
		this.indValidationUser = indValidationUser;
	}
	
	/**
	 * Gets the ind viewer.
	 *
	 * @return the ind viewer
	 */
	public Integer getIndViewer() {
		return indViewer;
	}
	
	/**
	 * Sets the ind viewer.
	 *
	 * @param indViewer the new ind viewer
	 */
	public void setIndViewer(Integer indViewer) {
		this.indViewer = indViewer;
	}
	
	/**
	 * Gets the max lenght.
	 *
	 * @return the max lenght
	 */
	public Integer getMaxLenght() {
		return maxLenght;
	}
	
	/**
	 * Sets the max lenght.
	 *
	 * @param maxLenght the new max lenght
	 */
	public void setMaxLenght(Integer maxLenght) {
		this.maxLenght = maxLenght;
	}
	
	/**
	 * Gets the size input.
	 *
	 * @return the size input
	 */
	public Integer getSizeInput() {
		return sizeInput;
	}
	
	/**
	 * Sets the size input.
	 *
	 * @param sizeInput the new size input
	 */
	public void setSizeInput(Integer sizeInput) {
		this.sizeInput = sizeInput;
	}
	
	/**
	 * Gets the event onblur.
	 *
	 * @return the event onblur
	 */
	public String getEventOnblur() {
		return eventOnblur;
	}
	
	/**
	 * Sets the event onblur.
	 *
	 * @param eventOnblur the new event onblur
	 */
	public void setEventOnblur(String eventOnblur) {
		this.eventOnblur = eventOnblur;
	}
	
	/**
	 * Gets the event onchange.
	 *
	 * @return the event onchange
	 */
	public String getEventOnchange() {
		return eventOnchange;
	}
	
	/**
	 * Sets the event onchange.
	 *
	 * @param eventOnchange the new event onchange
	 */
	public void setEventOnchange(String eventOnchange) {
		this.eventOnchange = eventOnchange;
	}
	
	/**
	 * Gets the key filter.
	 *
	 * @return the key filter
	 */
	public String getKeyFilter() {
		return keyFilter;
	}
	
	/**
	 * Sets the key filter.
	 *
	 * @param keyFilter the new key filter
	 */
	public void setKeyFilter(String keyFilter) {
		this.keyFilter = keyFilter;
	}
	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	
	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	
	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	
	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	
	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	
	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	
	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * Gets the master table.
	 *
	 * @return the master table
	 */
	public Integer getMasterTable() {
		return masterTable;
	}

	/**
	 * Sets the master table.
	 *
	 * @param masterTable the new master table
	 */
	public void setMasterTable(Integer masterTable) {
		this.masterTable = masterTable;
	}

	/**
	 * Gets the select menu.
	 *
	 * @return the select menu
	 */
	public LinkedHashMap<String, Object> getSelectMenu() {
		return selectMenu;
	}

	/**
	 * Sets the select menu.
	 *
	 * @param selectMenu the select menu
	 */
	public void setSelectMenu(LinkedHashMap<String, Object> selectMenu) {
		this.selectMenu = selectMenu;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser!= null){
			lastModifyUser=loggerUser.getUserName();
			lastModifyApp=loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate=loggerUser.getAuditTime();
			lastModifyIp=loggerUser.getIpAddress();
		}
		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the name relation filters.
	 *
	 * @return the name relation filters
	 */
	public String getNameRelationFilters() {
		return nameRelationFilters;
	}

	/**
	 * Sets the name relation filters.
	 *
	 * @param nameRelationFilters the new name relation filters
	 */
	public void setNameRelationFilters(String nameRelationFilters) {
		this.nameRelationFilters = nameRelationFilters;
	}

	/**
	 * Gets the ind break row.
	 *
	 * @return the ind break row
	 */
	public Integer getIndBreakRow() {
		return indBreakRow;
	}

	/**
	 * Sets the ind break row.
	 *
	 * @param indBreakRow the new ind break row
	 */
	public void setIndBreakRow(Integer indBreakRow) {
		this.indBreakRow = indBreakRow;
	}

	/**
	 * Checks if is disabled.
	 *
	 * @return true, if is disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * Sets the disabled.
	 *
	 * @param disabled the new disabled
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getFilterLabelEnglish() {
		return filterLabelEnglish;
	}

	public void setFilterLabelEnglish(String filterLabelEnglish) {
		this.filterLabelEnglish = filterLabelEnglish;
	}

	public List<HolderAccount> getHolderAccounts() {
		return holderAccounts;
	}

	public void setHolderAccounts(List<HolderAccount> holderAccounts) {
		this.holderAccounts = holderAccounts;
	}
	
	public List<String> getMultipleValue() {
		return multipleValue;
	}

	public void setMultipleValue(List<String> multipleValue) {
		this.multipleValue = multipleValue;
	}

	public Integer getRendered() {
		return rendered;
	}

	public void setRendered(Integer rendered) {
		this.rendered = rendered;
	}

	public Integer getRenderedInstitutionType() {
		return renderedInstitutionType;
	}

	public void setRenderedInstitutionType(Integer renderedInstitutionType) {
		this.renderedInstitutionType = renderedInstitutionType;
	}
}