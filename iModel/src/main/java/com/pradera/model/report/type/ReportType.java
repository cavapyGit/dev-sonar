package com.pradera.model.report.type;

import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ReportType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
public enum ReportType {
	
	/** The query. */
	QUERY(Integer.valueOf(1376)),
	
	/** The bean. */
	BEAN(Integer.valueOf(1377));
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	public static final Map<Integer, ReportType> lookup = new HashMap<Integer, ReportType>();
    static {
        for (ReportType d : ReportType.values())
            lookup.put(d.getCode(), d);
    }

	/**
	 * Instantiates a new report type.
	 *
	 * @param code the code
	 */
	private ReportType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the report type
	 */
	public static ReportType get(Integer code){
		return lookup.get(code);
	}
	
}
