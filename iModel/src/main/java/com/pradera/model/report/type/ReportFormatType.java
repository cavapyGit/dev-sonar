package com.pradera.model.report.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ReportFormatType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
public enum ReportFormatType {
	
	/** The pdf. */
	PDF(Integer.valueOf(1389),"pdf"),
	
	/** The xls. */
	XLS(Integer.valueOf(1390),"xls"),
	
	/** The txt. */
	TXT(Integer.valueOf(1391),"txt"),

	/** The xml. */
	XML(Integer.valueOf(1669),"xml"),

	/** The edb. */
	EDB(Integer.valueOf(2203),"EDB"),

	/** The xml. */
	CSV(Integer.valueOf(2293),"csv"),
	
	/** The POI EXCEL**/
	POI(Integer.valueOf(2256),"xls"),
	
	/** The CC. */
	CC(Integer.valueOf(2294),"CC"),
	
	/** The FCC. */
	FCC(Integer.valueOf(2295),"FCC"),
	
	/** The FRD. */
	FRD(Integer.valueOf(2296),"FRD"),
	
	/** The FCI. */
	FCI(Integer.valueOf(2297),"FCI"),
	
	XLSX(Integer.valueOf(2836),"XLSX"),
	
	EXCEL_DELIMITADO(Integer.valueOf(2853),"XLSX");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	
	/** The Constant list. */
	public static final List<ReportFormatType> list = new ArrayList<ReportFormatType>();
	/** The Constant lookup. */
	public static final Map<Integer, ReportFormatType> lookup = new HashMap<Integer, ReportFormatType>();
    static {
        for (ReportFormatType d : ReportFormatType.values()){
        	list.add(d);
            lookup.put(d.getCode(), d);	
        }
    }

	/**
	 * Instantiates a new report format type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ReportFormatType(Integer code,String value) {
		this.code = code;
		this.value= value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param Code the code
	 * @return the report format type
	 */
	public static ReportFormatType get(Integer Code){
		return lookup.get(Code);
	}
}
