package com.pradera.model.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the REPORT_LOGGER_DETAIL database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
@NamedQueries({
	@NamedQuery(name = ReportLoggerDetail.FIND_BY_REPORT_LOGGER,
			query = " select new ReportLoggerDetail(rld.idLoggerDetailPk,rf.filterLabel,rld.filterName,rld.filterValue,rld.filterDescription) from ReportLoggerDetail rld , ReportFilter rf " +
					" where rld.reportLogger.report.idReportPk=rf.report.idReportPk and rld.filterName=rf.filterName" +
					" and rld.reportLogger.idReportLoggerPk =:idReportLoggerPk order by rf.screenOrder ")
})
@Entity
@SequenceGenerator(name = "ReportLoggerDetail_generator", sequenceName = "SQ_ID_LOGGER_DETAIL_PK",initialValue = 1, allocationSize = 1)
@Table(name="REPORT_LOGGER_DETAIL")
public class ReportLoggerDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant FIND_BY_REPORT_LOGGER. */
	public static final String FIND_BY_REPORT_LOGGER = "ReportLoggerDetail.idReportLoggerFk";

	/** The id logger detail pk. */
	@Id @GeneratedValue(generator = "ReportLoggerDetail_generator")
	@Column(name="ID_LOGGER_DETAIL_PK")
	private Long idLoggerDetailPk;
	
	/** The report logger. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REPORT_LOGGER_FK", referencedColumnName="ID_REPORT_LOGGER_PK")
	private ReportLogger reportLogger;
	
	/** The filter name. */
	@Column(name="FILTER_NAME")
	private String filterName;
	
	/** The filter value. */
	@Column(name="FILTER_VALUE")
	private String filterValue;

	/** The filter description. */
	@Column(name="FILTER_DESCRIPTION")
	private String filterDescription;

	/** The ind viewer. */
	@Column(name="IND_VIEWER")
	private Integer indViewer;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The filter label. */
	@Transient
	private String filterLabel;
	
	/**
	 * Instantiates a new report logger detail.
	 */
	public ReportLoggerDetail(){
		
	}
	
	/**
	 * Instantiates a new report logger detail.
	 *
	 * @param idReportFilter the id report filter
	 * @param filterLabel the filter label
	 * @param filterName the filter name
	 * @param filterValue the filter value
	 * @param filterDescription the filter description
	 */
	public ReportLoggerDetail(Long idReportFilter,String filterLabel,String filterName,String filterValue,String filterDescription){
		this.idLoggerDetailPk=idReportFilter;
		this.filterLabel=filterLabel;
		this.filterName=filterName;
		this.filterValue=filterValue;
		this.filterDescription=filterDescription;
	}

	/**
	 * Gets the id logger detail pk.
	 *
	 * @return the id logger detail pk
	 */
	public Long getIdLoggerDetailPk() {
		return idLoggerDetailPk;
	}

	/**
	 * Sets the id logger detail pk.
	 *
	 * @param idLoggerDetailPk the new id logger detail pk
	 */
	public void setIdLoggerDetailPk(Long idLoggerDetailPk) {
		this.idLoggerDetailPk = idLoggerDetailPk;
	}

	/**
	 * Gets the report logger.
	 *
	 * @return the report logger
	 */
	public ReportLogger getReportLogger() {
		return reportLogger;
	}

	/**
	 * Sets the report logger.
	 *
	 * @param reportLogger the new report logger
	 */
	public void setReportLogger(ReportLogger reportLogger) {
		this.reportLogger = reportLogger;
	}

	/**
	 * Gets the filter name.
	 *
	 * @return the filter name
	 */
	public String getFilterName() {
		return filterName;
	}

	/**
	 * Sets the filter name.
	 *
	 * @param filterName the new filter name
	 */
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	/**
	 * Gets the filter value.
	 *
	 * @return the filter value
	 */
	public String getFilterValue() {
		return filterValue;
	}

	/**
	 * Sets the filter value.
	 *
	 * @param filterValue the new filter value
	 */
	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}

	/**
	 * Gets the filter description.
	 *
	 * @return the filter description
	 */
	public String getFilterDescription() {
		return filterDescription;
	}

	/**
	 * Sets the filter description.
	 *
	 * @param filterDescription the new filter description
	 */
	public void setFilterDescription(String filterDescription) {
		this.filterDescription = filterDescription;
	}

	/**
	 * Gets the ind viewer.
	 *
	 * @return the ind viewer
	 */
	public Integer getIndViewer() {
		return indViewer;
	}

	/**
	 * Sets the ind viewer.
	 *
	 * @param indViewer the new ind viewer
	 */
	public void setIndViewer(Integer indViewer) {
		this.indViewer = indViewer;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the filter label.
	 *
	 * @return the filter label
	 */
	public String getFilterLabel() {
		return filterLabel;
	}

	/**
	 * Sets the filter label.
	 *
	 * @param filterLabel the new filter label
	 */
	public void setFilterLabel(String filterLabel) {
		this.filterLabel = filterLabel;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser!=null){
			lastModifyApp=loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate=loggerUser.getAuditTime();
			lastModifyIp=loggerUser.getIpAddress();
			lastModifyUser=loggerUser.getUserName();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
}