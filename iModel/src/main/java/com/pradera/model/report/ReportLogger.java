package com.pradera.model.report;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.generalparameter.externalinterface.ExternalInterfaceSend;


/**
 * The persistent class for the REPORT_LOGGER database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
@NamedQueries({
	@NamedQuery(name = ReportLogger.REPORT_FIND_IDREPORT, query = " select  r from ReportLogger r  join fetch r.reportLoggerDetails where r.report.idReportPk =:idReport  order by r.idReportLoggerPk desc")
	,
	@NamedQuery(name=ReportLogger.REPORTLOGGER__FIND_DETAILS,query="select r from ReportLogger r  left join fetch r.reportLoggerDetails rd where r.idReportLoggerPk = :reportLogger")
})
@Entity
@SequenceGenerator(name = "ReportLogger_generator", sequenceName = "SQ_ID_REPORT_LOGGER_PK",initialValue = 1, allocationSize = 1)
@Table(name="REPORT_LOGGER")
public class ReportLogger implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant REPORTE_FIND_IDREPORT. */
	public static final String REPORT_FIND_IDREPORT = "ReportLogger.idReport";
	
	/** The Constant REPORTLOGGER__FIND_DETAILS. */
	public static final String REPORTLOGGER__FIND_DETAILS ="ReportLogger.details";
	
	/** The id report logger pk. */
	@Id @GeneratedValue(generator = "ReportLogger_generator")
	@Column(name="ID_REPORT_LOGGER_PK")
	private Long idReportLoggerPk;
	
	/** The report. */
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ID_REPORT_FK")
	private Report report;
	
	/** The external interface. */
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_EXTERNAL_INTERFACE_SEND_FK")
	private ExternalInterfaceSend externalInterfaceSend;
	
	/** The report state. */
	@Column(name="REPORT_STATE")
	private Integer reportState;
	
	/** The ind error. */
	@Column(name="IND_ERROR")
	private Integer indError;
	
	/** The error detail. */
	@Column(name="ERROR_DETAIL")
	private String errorDetail;
	
	/** The physical name. */
	@Column(name="PHYSICAL_NAME")
	private String physicalName;
	
	/** The file xml. */
	@Lob
	@Column(name="FILE_XML")
	@Basic(fetch=FetchType.LAZY)
	private byte[] fileXml;
	
	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
    
    /** The end date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="END_DATE")
    private Date endDate;
    
    /** The report format. */
    @Column(name="REPORT_FORMAT")
    private Integer reportFormat;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The report logger details. */
	@OneToMany(mappedBy="reportLogger",cascade=CascadeType.ALL)
	private List<ReportLoggerDetail> reportLoggerDetails;

	/** The report logger files. */
	@OneToMany(mappedBy="reportLogger")
	private List<ReportLoggerFile> reportLoggerFiles;

	/** The report logger users. */
	@OneToMany(mappedBy="reportLogger")
	private List<ReportLoggerUser> reportLoggerUsers;
	
	/** The ind send notification. */
	@Column(name="IND_SEND_NOTIFICATION")
	private Integer indSendNotification;

    /**
     * Instantiates a new report logger.
     */
    public ReportLogger() {

    }
    
    /**
     * Gets the parameters string.
     *
     * @return the parameters string
     */
    public String getParametersString(){
    	StringBuilder parameters = new StringBuilder("{id_report="+report.getIdReportPk()+", ");
    	parameters.append("title="+report.getTitle()+", ");
    	if(reportLoggerDetails != null && !reportLoggerDetails.isEmpty()){
    		for(ReportLoggerDetail reportDetail : reportLoggerDetails){
    			parameters.append(reportDetail.getFilterName()+"="+getStringValue(reportDetail.getFilterValue())+" "+getStringValue(reportDetail.getFilterDescription())+", ");
    		}
    	}
    	parameters.append("}");
    	return parameters.toString();
    }
    
    /**
     * Gets the string value.
     *
     * @param value the value
     * @return the string value
     */
    public String getStringValue(String value){
    	if(value != null){
    		return value.toString();
    	} else {
    		return "";
    	}
    }

	/**
	 * Gets the id report logger pk.
	 *
	 * @return the id report logger pk
	 */
	public Long getIdReportLoggerPk() {
		return idReportLoggerPk;
	}

	/**
	 * Sets the id report logger pk.
	 *
	 * @param idReportLoggerPk the new id report logger pk
	 */
	public void setIdReportLoggerPk(Long idReportLoggerPk) {
		this.idReportLoggerPk = idReportLoggerPk;
	}

	/**
	 * Gets the report.
	 *
	 * @return the report
	 */
	public Report getReport() {
		return report;
	}

	/**
	 * Sets the report.
	 *
	 * @param report the new report
	 */
	public void setReport(Report report) {
		this.report = report;
	}

	/**
	 * Gets the report state.
	 *
	 * @return the report state
	 */
	public Integer getReportState() {
		return reportState;
	}

	/**
	 * Sets the report state.
	 *
	 * @param reportState the new report state
	 */
	public void setReportState(Integer reportState) {
		this.reportState = reportState;
	}

	/**
	 * Gets the ind error.
	 *
	 * @return the ind error
	 */
	public Integer getIndError() {
		return indError;
	}

	/**
	 * Sets the ind error.
	 *
	 * @param indError the new ind error
	 */
	public void setIndError(Integer indError) {
		this.indError = indError;
	}

	/**
	 * Gets the error detail.
	 *
	 * @return the error detail
	 */
	public String getErrorDetail() {
		return errorDetail;
	}

	/**
	 * Sets the error detail.
	 *
	 * @param errorDetail the new error detail
	 */
	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	/**
	 * Gets the physical name.
	 *
	 * @return the physical name
	 */
	public String getPhysicalName() {
		return physicalName;
	}

	/**
	 * Sets the physical name.
	 *
	 * @param physicalName the new physical name
	 */
	public void setPhysicalName(String physicalName) {
		this.physicalName = physicalName;
	}

	/**
	 * Gets the file xml.
	 *
	 * @return the file xml
	 */
	public byte[] getFileXml() {
		return fileXml;
	}

	/**
	 * Sets the file xml.
	 *
	 * @param fileXml the new file xml
	 */
	public void setFileXml(byte[] fileXml) {
		this.fileXml = fileXml;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the report logger details.
	 *
	 * @return the report logger details
	 */
	public List<ReportLoggerDetail> getReportLoggerDetails() {
		return reportLoggerDetails;
	}

	/**
	 * Sets the report logger details.
	 *
	 * @param reportLoggerDetails the new report logger details
	 */
	public void setReportLoggerDetails(List<ReportLoggerDetail> reportLoggerDetails) {
		this.reportLoggerDetails = reportLoggerDetails;
	}

	/**
	 * Gets the report logger files.
	 *
	 * @return the report logger files
	 */
	public List<ReportLoggerFile> getReportLoggerFiles() {
		return reportLoggerFiles;
	}

	/**
	 * Sets the report logger files.
	 *
	 * @param reportLoggerFiles the new report logger files
	 */
	public void setReportLoggerFiles(List<ReportLoggerFile> reportLoggerFiles) {
		this.reportLoggerFiles = reportLoggerFiles;
	}

	/**
	 * Gets the report logger users.
	 *
	 * @return the report logger users
	 */
	public List<ReportLoggerUser> getReportLoggerUsers() {
		return reportLoggerUsers;
	}

	/**
	 * Sets the report logger users.
	 *
	 * @param reportLoggerUsers the new report logger users
	 */
	public void setReportLoggerUsers(List<ReportLoggerUser> reportLoggerUsers) {
		this.reportLoggerUsers = reportLoggerUsers;
	}

	/**
	 * Gets the ind send notification.
	 *
	 * @return the ind send notification
	 */
	public Integer getIndSendNotification() {
		return indSendNotification;
	}

	/**
	 * Sets the ind send notification.
	 *
	 * @param indSendNotification the new ind send notification
	 */
	public void setIndSendNotification(Integer indSendNotification) {
		this.indSendNotification = indSendNotification;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	/**
	 * Sets the audit.
	 *
	 * @param loggerUser the new audit
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser!=null){
			lastModifyApp=loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate=loggerUser.getAuditTime();
			lastModifyIp=loggerUser.getIpAddress();
			lastModifyUser=loggerUser.getUserName();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	/**
	 * Gets the list for audit.
	 *
	 * @return the list for audit
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		Map<String, List<? extends Auditable>> mapList = new HashMap<String, List<? extends Auditable>>();
		mapList.put("reportLoggerDetails", reportLoggerDetails);
		return mapList;
	}

	/**
	 * Gets the external interface send.
	 *
	 * @return the external interface send
	 */
	public ExternalInterfaceSend getExternalInterfaceSend() {
		return externalInterfaceSend;
	}

	/**
	 * Sets the external interface send.
	 *
	 * @param externalInterfaceSend the new external interface send
	 */
	public void setExternalInterfaceSend(ExternalInterfaceSend externalInterfaceSend) {
		this.externalInterfaceSend = externalInterfaceSend;
	}

	/**
	 * Gets the report format.
	 *
	 * @return the report format
	 */
	public Integer getReportFormat() {
		return reportFormat;
	}

	/**
	 * Sets the report format.
	 *
	 * @param reportFormat the new report format
	 */
	public void setReportFormat(Integer reportFormat) {
		this.reportFormat = reportFormat;
	}
}