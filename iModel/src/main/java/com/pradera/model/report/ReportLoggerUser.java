package com.pradera.model.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the REPORT_LOGGER_USER database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
@Entity
@SequenceGenerator(name = "ReportLoggerUser_generator", sequenceName = "SQ_ID_LOGGER_USER_PK",initialValue = 1, allocationSize = 1)
@Table(name="REPORT_LOGGER_USER")
public class ReportLoggerUser implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id logger user. */
	@Id @GeneratedValue(generator = "ReportLoggerUser_generator")
	@Column(name="ID_LOGGER_USER_PK")
	private Long idLoggerUser;
	
	/** The report logger. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REPORT_LOGGER_FK", referencedColumnName="ID_REPORT_LOGGER_PK")
	private ReportLogger reportLogger;

	/** The access user. */
	@Column(name="ACCESS_USER")
	private String accessUser;

    /** The assigned date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="ASSIGNED_DATE")
	private Date assignedDate;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /**
     * Instantiates a new report logger user.
     */
    public ReportLoggerUser() {
    }

	/**
	 * Gets the id logger user.
	 *
	 * @return the id logger user
	 */
	public Long getIdLoggerUser() {
		return idLoggerUser;
	}

	/**
	 * Sets the id logger user.
	 *
	 * @param idLoggerUser the new id logger user
	 */
	public void setIdLoggerUser(Long idLoggerUser) {
		this.idLoggerUser = idLoggerUser;
	}

	/**
	 * Gets the report logger.
	 *
	 * @return the report logger
	 */
	public ReportLogger getReportLogger() {
		return reportLogger;
	}

	/**
	 * Sets the report logger.
	 *
	 * @param reportLogger the new report logger
	 */
	public void setReportLogger(ReportLogger reportLogger) {
		this.reportLogger = reportLogger;
	}

	/**
	 * Gets the access user.
	 *
	 * @return the access user
	 */
	public String getAccessUser() {
		return accessUser;
	}

	/**
	 * Sets the access user.
	 *
	 * @param accessUser the new access user
	 */
	public void setAccessUser(String accessUser) {
		this.accessUser = accessUser;
	}

	/**
	 * Gets the assigned date.
	 *
	 * @return the assigned date
	 */
	public Date getAssignedDate() {
		return assignedDate;
	}

	/**
	 * Sets the assigned date.
	 *
	 * @param assignedDate the new assigned date
	 */
	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser!=null){
			lastModifyApp=loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate=loggerUser.getAuditTime();
			lastModifyIp=loggerUser.getIpAddress();
			lastModifyUser=loggerUser.getUserName();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}