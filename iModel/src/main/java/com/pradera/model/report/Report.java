package com.pradera.model.report;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.process.BusinessProcess;

/**
 * The persistent class for the REPORT database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
@NamedQueries({
	@NamedQuery(name = Report.REPORTE_FIND_ALL, query = " select distinct r from Report r  join fetch r.reportFilters  order by r.idReportPk")
})
@Entity
@SequenceGenerator(name = "Report_generator", sequenceName = "SQ_ID_REPORT_PK",initialValue = 1, allocationSize = 1)
public class Report implements Serializable, Auditable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant REPORTE_FIND_ALL. */
	public static final String REPORTE_FIND_ALL = "Report.findAll";
	
	/** The id report pk. */
	@Id @GeneratedValue(generator = "Report_generator")
	@Column(name="ID_REPORT_PK")
	private Long idReportPk;
	
	/** The name. */
	@NotNull
	private String name;
	
	/** The mnemonico. */
	@NotNull
	private String mnemonico;
	
	/** The title. */
	@NotNull
	private String title;
	
	/** The nr columns. */
	@NotNull
	@Column(name="NR_COLUMNS")
	private Integer nrColumns;

	/** The idepositary module. */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDEPOSITARY_MODULE")
	private BusinessProcess idepositaryModule;
	
	/** The user security type. */
	@Column(name="USER_SECURITY_TYPE")
	private Integer userSecurityType;
	
	/** The report bean name. */
	@NotNull
	@Column(name="REPORT_BEAN_NAME")
	private String reportBeanName;

	/** The report clasification. */
	@Column(name="REPORT_CLASIFICATION")
	private String reportClasification;

	/** The ind pdf format. */
	@Column(name="IND_PDF_FORMAT")
	private Integer indPdfFormat;
	
	/** The ind txt format. */
	@Column(name="IND_TXT_FORMAT")
	private Integer indTxtFormat;
	
	/** The ind xls format. */
	@Column(name="IND_XLS_FORMAT")
	private Integer indXlsFormat;

	/** The ind Accounting format. */
	@Column(name="IND_POI_FORMAT")
	private Integer indPoiFormat;
	
	@Column(name="IND_EXCEL_DELIMITED_FORMAT")
	private Integer indExcelDelimitedFormat;
	
	/** The ind priority. */
	@Column(name="IND_PRIORITY")
	private Integer indPriority;

	/** The ind queue. */
	@Column(name="IND_QUEUE")
	private Integer indQueue;

	/** The report type. */
	@Column(name="REPORT_TYPE")
	private Integer reportType;
	
	/** The template name. */
	@Column(name="TEMPLATE_NAME")
	private String templateName;
	
	/** The datasource xml. */
	@Column(name="DATASOURCE_XML")
	private String datasourceXml;
	
	/** The query report. */
	@Lob()
	@Column(name="QUERY_REPORT")
	private String queryReport;
	
	
	/** The query report. */
	@Column(name="SUB_QUERY_REPORT")
	private String subQueryReport;
	
	/** The onload page. */
	@Column(name="ONLOAD_PAGE")
	private String onloadPage;
	
	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The registry date. */
	@Column(name="REGISTRY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registryDate;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	
	//bi-directional many-to-one association to ReportFilter
	/** The report filters. */
	@OneToMany(mappedBy="report",cascade=CascadeType.ALL)
	@OrderBy("screenOrder")
	private List<ReportFilter> reportFilters;

	/** The report loggers. */
	@OneToMany(mappedBy="report")
	private List<ReportLogger> reportLoggers;
	
	/** The external interfaces. */
	@OneToMany(mappedBy="report")
	private List<ExternalInterface> externalInterfaces;

	/** The pdf type selected. */
	@Transient
	private boolean pdfTypeSelected;
	
	/** The is selected. */
	@Transient
	private boolean excelTypeSelected;
	
    /**
     * Instantiates a new report.
     */
    public Report() {
    }

	

	/**
	 * Gets the id report pk.
	 *
	 * @return the id report pk
	 */
	public Long getIdReportPk() {
		return idReportPk;
	}



	/**
	 * Sets the id report pk.
	 *
	 * @param idReportPk the new id report pk
	 */
	public void setIdReportPk(Long idReportPk) {
		this.idReportPk = idReportPk;
	}



	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * Gets the mnemonico.
	 *
	 * @return the mnemonico
	 */
	public String getMnemonico() {
		return mnemonico;
	}



	/**
	 * Sets the mnemonico.
	 *
	 * @param mnemonico the new mnemonico
	 */
	public void setMnemonico(String mnemonico) {
		this.mnemonico = mnemonico;
	}



	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}



	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}



	/**
	 * Gets the nr columns.
	 *
	 * @return the nr columns
	 */
	public Integer getNrColumns() {
		return nrColumns;
	}



	/**
	 * Sets the nr columns.
	 *
	 * @param nrColumns the new nr columns
	 */
	public void setNrColumns(Integer nrColumns) {
		this.nrColumns = nrColumns;
	}



	/**
	 * Gets the idepositary module.
	 *
	 * @return the idepositary module
	 */
	public BusinessProcess getIdepositaryModule() {
		return idepositaryModule;
	}



	/**
	 * Sets the idepositary module.
	 *
	 * @param idepositaryModule the new idepositary module
	 */
	public void setIdepositaryModule(BusinessProcess idepositaryModule) {
		this.idepositaryModule = idepositaryModule;
	}



	/**
	 * Gets the user security type.
	 *
	 * @return the user security type
	 */
	public Integer getUserSecurityType() {
		return userSecurityType;
	}



	/**
	 * Sets the user security type.
	 *
	 * @param userSecurityType the new user security type
	 */
	public void setUserSecurityType(Integer userSecurityType) {
		this.userSecurityType = userSecurityType;
	}



	/**
	 * Gets the report bean name.
	 *
	 * @return the report bean name
	 */
	public String getReportBeanName() {
		return reportBeanName;
	}



	/**
	 * Sets the report bean name.
	 *
	 * @param reportBeanName the new report bean name
	 */
	public void setReportBeanName(String reportBeanName) {
		this.reportBeanName = reportBeanName;
	}



	/**
	 * Gets the report clasification.
	 *
	 * @return the report clasification
	 */
	public String getReportClasification() {
		return reportClasification;
	}



	/**
	 * Sets the report clasification.
	 *
	 * @param reportClasification the new report clasification
	 */
	public void setReportClasification(String reportClasification) {
		this.reportClasification = reportClasification;
	}



	/**
	 * Gets the ind pdf format.
	 *
	 * @return the ind pdf format
	 */
	public Integer getIndPdfFormat() {
		return indPdfFormat;
	}



	/**
	 * Sets the ind pdf format.
	 *
	 * @param indPdfFormat the new ind pdf format
	 */
	public void setIndPdfFormat(Integer indPdfFormat) {
		this.indPdfFormat = indPdfFormat;
	}



	/**
	 * @return the indPoiFormat
	 */
	public Integer getIndPoiFormat() {
		return indPoiFormat;
	}



	/**
	 * @param indPoiFormat the indPoiFormat to set
	 */
	public void setIndPoiFormat(Integer indPoiFormat) {
		this.indPoiFormat = indPoiFormat;
	}



	/**
	 * Gets the ind txt format.
	 *
	 * @return the ind txt format
	 */
	public Integer getIndTxtFormat() {
		return indTxtFormat;
	}



	/**
	 * Sets the ind txt format.
	 *
	 * @param indTxtFormat the new ind txt format
	 */
	public void setIndTxtFormat(Integer indTxtFormat) {
		this.indTxtFormat = indTxtFormat;
	}



	/**
	 * Gets the ind xls format.
	 *
	 * @return the ind xls format
	 */
	public Integer getIndXlsFormat() {
		return indXlsFormat;
	}



	/**
	 * Sets the ind xls format.
	 *
	 * @param indXlsFormat the new ind xls format
	 */
	public void setIndXlsFormat(Integer indXlsFormat) {
		this.indXlsFormat = indXlsFormat;
	}



	/**
	 * Gets the ind priority.
	 *
	 * @return the ind priority
	 */
	public Integer getIndPriority() {
		return indPriority;
	}



	/**
	 * Sets the ind priority.
	 *
	 * @param indPriority the new ind priority
	 */
	public void setIndPriority(Integer indPriority) {
		this.indPriority = indPriority;
	}



	/**
	 * Gets the ind queue.
	 *
	 * @return the ind queue
	 */
	public Integer getIndQueue() {
		return indQueue;
	}



	/**
	 * Sets the ind queue.
	 *
	 * @param indQueue the new ind queue
	 */
	public void setIndQueue(Integer indQueue) {
		this.indQueue = indQueue;
	}



	/**
	 * Gets the report type.
	 *
	 * @return the report type
	 */
	public Integer getReportType() {
		return reportType;
	}



	/**
	 * Sets the report type.
	 *
	 * @param reportType the new report type
	 */
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}



	/**
	 * Gets the template name.
	 *
	 * @return the template name
	 */
	public String getTemplateName() {
		return templateName;
	}



	/**
	 * Sets the template name.
	 *
	 * @param templateName the new template name
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}



	/**
	 * Gets the datasource xml.
	 *
	 * @return the datasource xml
	 */
	public String getDatasourceXml() {
		return datasourceXml;
	}



	/**
	 * Sets the datasource xml.
	 *
	 * @param datasourceXml the new datasource xml
	 */
	public void setDatasourceXml(String datasourceXml) {
		this.datasourceXml = datasourceXml;
	}



	/**
	 * Gets the query report.
	 *
	 * @return the query report
	 */
	public String getQueryReport() {
		return queryReport;
	}



	/**
	 * Sets the query report.
	 *
	 * @param queryReport the new query report
	 */
	public void setQueryReport(String queryReport) {
		this.queryReport = queryReport;
	}



	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/**
	 * Gets the report filters.
	 *
	 * @return the report filters
	 */
	public List<ReportFilter> getReportFilters() {
		return reportFilters;
	}



	/**
	 * Sets the report filters.
	 *
	 * @param reportFilters the new report filters
	 */
	public void setReportFilters(List<ReportFilter> reportFilters) {
		this.reportFilters = reportFilters;
	}



	/**
	 * Gets the report loggers.
	 *
	 * @return the report loggers
	 */
	public List<ReportLogger> getReportLoggers() {
		return reportLoggers;
	}



	/**
	 * Sets the report loggers.
	 *
	 * @param reportLoggers the new report loggers
	 */
	public void setReportLoggers(List<ReportLogger> reportLoggers) {
		this.reportLoggers = reportLoggers;
	}



	/**
	 * Checks if is pdf type selected.
	 *
	 * @return true, if is pdf type selected
	 */
	public boolean isPdfTypeSelected() {
		return pdfTypeSelected;
	}



	/**
	 * Sets the pdf type selected.
	 *
	 * @param pdfTypeSelected the new pdf type selected
	 */
	public void setPdfTypeSelected(boolean pdfTypeSelected) {
		this.pdfTypeSelected = pdfTypeSelected;
	}



	/**
	 * Checks if is excel type selected.
	 *
	 * @return true, if is excel type selected
	 */
	public boolean isExcelTypeSelected() {
		return excelTypeSelected;
	}



	/**
	 * Sets the excel type selected.
	 *
	 * @param excelTypeSelected the new excel type selected
	 */
	public void setExcelTypeSelected(boolean excelTypeSelected) {
		this.excelTypeSelected = excelTypeSelected;
	}



	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}



	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}



	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}



	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}


	/**
	 * Gets the sub query report.
	 *
	 * @return the subQueryReport
	 */
	public String getSubQueryReport() {
		return subQueryReport;
	}



	/**
	 * Sets the sub query report.
	 *
	 * @param subQueryReport the subQueryReport to set
	 */
	public void setSubQueryReport(String subQueryReport) {
		this.subQueryReport = subQueryReport;
	}
	
	/**
	 * Gets the onload page.
	 *
	 * @return the onload page
	 */
	public String getOnloadPage() {
		return onloadPage;
	}



	/**
	 * Sets the onload page.
	 *
	 * @param onloadPage the new onload page
	 */
	public void setOnloadPage(String onloadPage) {
		this.onloadPage = onloadPage;
	}


	/**
	 * Gets the external interfaces.
	 *
	 * @return the external interfaces
	 */
	public List<ExternalInterface> getExternalInterfaces() {
		return externalInterfaces;
	}



	/**
	 * Sets the external interfaces.
	 *
	 * @param externalInterfaces the new external interfaces
	 */
	public void setExternalInterfaces(List<ExternalInterface> externalInterfaces) {
		this.externalInterfaces = externalInterfaces;
	}

	public Integer getIndExcelDelimitedFormat() {
		return indExcelDelimitedFormat;
	}



	public void setIndExcelDelimitedFormat(Integer indExcelDelimitedFormat) {
		this.indExcelDelimitedFormat = indExcelDelimitedFormat;
	}



	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser!=null) {
			lastModifyApp=loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate=loggerUser.getAuditTime();
			lastModifyIp=loggerUser.getIpAddress();
			lastModifyUser=loggerUser.getUserName();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		Map<String, List<? extends Auditable>> mapList = new HashMap<String, List<? extends Auditable>>();
		mapList.put("reportFilters", reportFilters);
		return mapList;
	}
}