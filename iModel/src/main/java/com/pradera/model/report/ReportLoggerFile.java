package com.pradera.model.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the REPORT_LOGGER_FILE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
@Entity
@SequenceGenerator(name = "ReportLoggerFile_generator", sequenceName = "SQ_ID_LOGGER_FILE_PK",initialValue = 1, allocationSize = 1)
@Table(name="REPORT_LOGGER_FILE")
@NamedQueries({
	@NamedQuery(name = ReportLoggerFile.REPORTE_FIND_BY_FK, query = " select new ReportLoggerFile(rlf.idLoggerFilePk,rlf.nameFile,rlf.fileType,(select p.parameterName from ParameterTable p where p.parameterTablePk = rlf.fileType),rlf.numberPages,dataReportLength) from ReportLoggerFile rlf where rlf.reportLogger.idReportLoggerPk = :reportLogger"),
	@NamedQuery(name = ReportLoggerFile.REPORTE_CONTENT_BY_PK, query = " select rlf from ReportLoggerFile rlf where rlf.idLoggerFilePk =:idReportFilePk "),
	@NamedQuery(name = ReportLoggerFile.REPORTE_DATA_CONTENT_BY_PK, query = " select rlf.dataReport from ReportLoggerFile rlf where rlf.idLoggerFilePk =:idReportFilePk ")
})
public class ReportLoggerFile implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant REPORTE_FIND_BY_FK. */
	public static final String REPORTE_FIND_BY_FK = "ReportLoggerFile.findByFK";
	
	public static final String REPORTE_CONTENT_BY_PK = "ReportLoggerFile.getContentByPk";
	
	public static final String REPORTE_DATA_CONTENT_BY_PK = "ReportLoggerFile.getDataContentByPk";
	
	/** The id logger file pk. */
	@Id @GeneratedValue(generator = "ReportLoggerFile_generator")
	@Column(name="ID_LOGGER_FILE_PK")
	private Long idLoggerFilePk;
	
	/** The report logger. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REPORT_LOGGER_FK", referencedColumnName="ID_REPORT_LOGGER_PK")
	private ReportLogger reportLogger;
	
	/** The name file. */
	@Column(name="NAME_FILE")
	private String nameFile;
	
	/** The file type. */
	@Column(name="FILE_TYPE")
	private Integer fileType;

    /** The data report. */
    @Lob
	@Column(name="DATA_REPORT")
    @Basic(fetch=FetchType.LAZY)
	private byte[] dataReport;

    /** The number pages. */
    @Column(name="NUMBER_PAGES")
	private Integer numberPages;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="DATA_REPORT_LENGTH")
	private BigDecimal dataReportLength;
	
	/** The name file. */
	@Column(name="NAME_TRACE")
	private String nameTrace;
	
	@Transient
	private String fileTypeDescription;
	
	@Transient
	private String dataSizeDescription;

	/**
     * Instantiates a new report logger file.
     */
    public ReportLoggerFile() {
    	super();
    	dataReportLength = BigDecimal.ZERO;
    }
    
	public ReportLoggerFile(byte[] dataReport) {
		super();
		this.dataReport = dataReport;
		this.dataReportLength = BigDecimal.ZERO;
	}

	/**
	 * @param idLoggerFilePk
	 * @param nameFile
	 * @param fileType
	 * @param fileTypeDescription
	 * @param numberPages
	 */
	public ReportLoggerFile(Long idLoggerFilePk, String nameFile,
			Integer fileType, String fileTypeDescription, Integer numberPages,BigDecimal dataReportLength) {
		super();
		this.idLoggerFilePk = idLoggerFilePk;
		this.nameFile = nameFile;
		this.fileType = fileType;
		this.fileTypeDescription = fileTypeDescription;
		this.numberPages = numberPages;
		this.dataReportLength = dataReportLength;
	}
	
    /**
	 * @return the fileTypeDescription
	 */
	public String getFileTypeDescription() {
		return fileTypeDescription;
	}

	/**
	 * @param fileTypeDescription the fileTypeDescription to set
	 */
	public void setFileTypeDescription(String fileTypeDescription) {
		this.fileTypeDescription = fileTypeDescription;
	}

	/**
	 * Gets the id logger file pk.
	 *
	 * @return the id logger file pk
	 */
	public Long getIdLoggerFilePk() {
		return idLoggerFilePk;
	}

	/**
	 * Sets the id logger file pk.
	 *
	 * @param idLoggerFilePk the new id logger file pk
	 */
	public void setIdLoggerFilePk(Long idLoggerFilePk) {
		this.idLoggerFilePk = idLoggerFilePk;
	}

	/**
	 * Gets the report logger.
	 *
	 * @return the report logger
	 */
	public ReportLogger getReportLogger() {
		return reportLogger;
	}

	/**
	 * Sets the report logger.
	 *
	 * @param reportLogger the new report logger
	 */
	public void setReportLogger(ReportLogger reportLogger) {
		this.reportLogger = reportLogger;
	}

	/**
	 * Gets the name file.
	 *
	 * @return the name file
	 */
	public String getNameFile() {
		return nameFile;
	}

	/**
	 * Sets the name file.
	 *
	 * @param nameFile the new name file
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	/**
	 * Gets the file type.
	 *
	 * @return the file type
	 */
	public Integer getFileType() {
		return fileType;
	}

	/**
	 * Sets the file type.
	 *
	 * @param fileType the new file type
	 */
	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}

	/**
	 * Gets the data report.
	 *
	 * @return the data report
	 */
	public byte[] getDataReport() {
		return dataReport;
	}

	/**
	 * Sets the data report.
	 *
	 * @param dataReport the new data report
	 */
	public void setDataReport(byte[] dataReport) {
		this.dataReport = dataReport;
	}

	/**
	 * Gets the number pages.
	 *
	 * @return the number pages
	 */
	public Integer getNumberPages() {
		return numberPages;
	}

	/**
	 * Sets the number pages.
	 *
	 * @param numberPages the new number pages
	 */
	public void setNumberPages(Integer numberPages) {
		this.numberPages = numberPages;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser!=null){
			lastModifyApp=loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate=loggerUser.getAuditTime();
			lastModifyIp=loggerUser.getIpAddress();
			lastModifyUser=loggerUser.getUserName();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal getDataReportLength() {
		return dataReportLength;
	}

	public void setDataReportLength(BigDecimal dataReportLength) {
		this.dataReportLength = dataReportLength;
	}

	public String getDataSizeDescription() {
		return dataSizeDescription;
	}

	public void setDataSizeDescription(String dataSizeDescription) {
		this.dataSizeDescription = dataSizeDescription;
	}

	public String getNameTrace() {
		return nameTrace;
	}

	public void setNameTrace(String nameTrace) {
		this.nameTrace = nameTrace;
	}

}