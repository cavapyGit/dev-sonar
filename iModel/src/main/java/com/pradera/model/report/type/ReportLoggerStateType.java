package com.pradera.model.report.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ReportLoggerStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
public enum ReportLoggerStateType {

	/** The registered. */
	REGISTERED(Integer.valueOf(1385)),
	
	/** The processing. */
	PROCESSING(Integer.valueOf(1386)),
	
	/** The finished. */
	FINISHED(Integer.valueOf(1387)),
	
	/** The failed. */
	FAILED(Integer.valueOf(1388));
	
	/** The code. */
	private Integer code;
	
	private String value;	
	/** The Constant lookup. */
	public static final Map<Integer, ReportLoggerStateType> lookup = new HashMap<Integer, ReportLoggerStateType>();
	public static final List<ReportLoggerStateType> list = new ArrayList<ReportLoggerStateType>();
    static {
        for (ReportLoggerStateType d : ReportLoggerStateType.values()){
        	list.add(d);
            lookup.put(d.getCode(), d);
        }
    }

	/**
	 * Instantiates a new report logger state type.
	 *
	 * @param code the code
	 */
	private ReportLoggerStateType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return code;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private ReportLoggerStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the report logger state type
	 */
	public static ReportLoggerStateType get(Integer code){
		return lookup.get(code);
	}
}
