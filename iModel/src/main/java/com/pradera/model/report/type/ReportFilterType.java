package com.pradera.model.report.type;

import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ReportFilterType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2013
 */
public enum ReportFilterType {

	/** The calendar. */
	CALENDAR(Integer.valueOf(1378)),
	
	/** The text. */
	TEXT(Integer.valueOf(1379)),
	
	/** The combo. */
	COMBO(Integer.valueOf(1380)),
	
	/** The help securities. */
	HELP_SECURITIES(Integer.valueOf(1381)),
	
	/** The help holders. */
	HELP_HOLDERS(Integer.valueOf(1382)),
	
	/** The help accounts. */
	HELP_ACCOUNTS(Integer.valueOf(1383)),
	
	/** The help participants. */
	HELP_PARTICIPANTS(Integer.valueOf(1384)),
	
	/** The combo parameters. */
	COMBO_PARAMETERS(Integer.valueOf(1473)),
	
	/** The help issuers. */
	HELP_ISSUERS(Integer.valueOf(1485)),
	
	/** The help billing. */
	HELP_BILLING(Integer.valueOf(1599)),
	
	/** */
	COMBO_MULTIPLE(Integer.valueOf(2074)),
	
	COMBO_SIMPLE(Integer.valueOf(2240)),
	
	CALENDAR_FUTURE(Integer.valueOf(2272)),
	
	CALENDAR_ALL_DATE(Integer.valueOf(2279)),
	
	OPTIONAL_CALENDAR(Integer.valueOf(2325)),
	
	CALENDAR_DISABLED(Integer.valueOf(2940)),;
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	public static final Map<Integer, ReportFilterType> lookup = new HashMap<Integer, ReportFilterType>();
    static {
        for (ReportFilterType d : ReportFilterType.values())
            lookup.put(d.getCode(), d);
    }


	/**
	 * Instantiates a new report filter type.
	 *
	 * @param code the code
	 */
	private ReportFilterType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the report filter type
	 */
	public static ReportFilterType get(Integer code){
		return lookup.get(code);
	}
}
