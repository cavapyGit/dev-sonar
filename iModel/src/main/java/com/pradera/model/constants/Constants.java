package com.pradera.model.constants;

public class Constants {
	public static final String DASH = " - ";
	public static final String ASTERISK = "* ";
	public static final String BR = "<br/>";
	public static final String COMMA = ", ";
}
