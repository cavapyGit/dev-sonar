package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum ParameterTableStateType {

	REGISTERED(Integer.valueOf(1),"REGISTRADO"),
	BLOCKED(Integer.valueOf(0),"BLOQUEADO");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private ParameterTableStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<ParameterTableStateType> list = new ArrayList<ParameterTableStateType>();
	public static final Map<Integer, ParameterTableStateType> lookup = new HashMap<Integer, ParameterTableStateType>();
	static {
		for (ParameterTableStateType s : EnumSet.allOf(ParameterTableStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public static ParameterTableStateType get(Integer code) {
		return lookup.get(code);
	}
}
