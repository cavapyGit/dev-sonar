package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ExternalInterfaceExecutionType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
public enum InterfacePredicateType {

	/** The column. */
	COLUMN(Integer.valueOf(1594),"COLUMNA DE TABLA"),//campo de tabla
	
	/** The query. */
	QUERY(Integer.valueOf(1595),"SUB CONSULTA"),//subconsulta
	
	/** The value. */
	VALUE(Integer.valueOf(1596),"VALOR"),
	
	/** The user type. */
	USER_TYPE(Integer.valueOf(1597),"TIPO USUARIO");
		
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfacePredicateType> list=new ArrayList<InterfacePredicateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfacePredicateType> lookup=new HashMap<Integer, InterfacePredicateType>();

	static {
		for(InterfacePredicateType c : InterfacePredicateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfacePredicateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the interface predicate type
	 */
	public static InterfacePredicateType get(Integer codigo) {
		return lookup.get(codigo);
	}
}