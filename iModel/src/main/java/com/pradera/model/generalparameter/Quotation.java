package com.pradera.model.generalparameter;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationMechanism;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the QUOTATION database table.
 * 
 */
@Entity
public class Quotation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="QUOTATION_PK_GENERATOR", sequenceName="SQ_ID_QUOTATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="QUOTATION_PK_GENERATOR")
	@Column(name="ID_QUOTATION_PK")
	private Integer idQuotationPk;
	
	@Column(name="AVERAGE_PRICE")
	private BigDecimal averagePrice;

	@Column(name="BUY_PRICE")
	private BigDecimal buyPrice;

	@Column(name="CLOSE_PRICE")
	private BigDecimal closePrice;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	//bi-directional many-to-one association to QuotationFile
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK")
	private NegotiationMechanism negotiationMechanism;

	@NotNull
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@NotNull
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@NotNull
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@NotNull
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OPENING_PRICE")
	private BigDecimal openingPrice;

    @Temporal( TemporalType.DATE)
	@Column(name="QUOTATION_DATE")
	private Date quotationDate;

	@Column(name="SELL_PRICE")
	private BigDecimal sellPrice;
	
	@Column(name="QUOTATION_STATE")
	private Integer quotationState;
	
	@Column(name="REMOVE_MOTIVE")
	private Integer removeMotive;
	
	@Column(name="REMOVE_OTHER_MOTIVE")
	private String removeOtherMotive;

	//bi-directional many-to-one association to QuotationFile
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_QUOTATION_FILE_FK", referencedColumnName="ID_QUOTATION_FILE_PK")
	private QuotationFile quotationFile;

    public Quotation() {
    }

	public long getIdQuotationPk() {
		return this.idQuotationPk;
	}

	public void setIdQuotationPk(Integer idQuotationPk) {
		this.idQuotationPk = idQuotationPk;
	}

	public BigDecimal getAveragePrice() {
		return this.averagePrice;
	}

	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}

	public BigDecimal getBuyPrice() {
		return this.buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getClosePrice() {
		return this.closePrice;
	}

	public void setClosePrice(BigDecimal closePrice) {
		this.closePrice = closePrice;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the negotiationMechanism
	 */
	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}

	/**
	 * @param negotiationMechanism the negotiationMechanism to set
	 */
	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}
	
	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getOpeningPrice() {
		return this.openingPrice;
	}

	public void setOpeningPrice(BigDecimal openingPrice) {
		this.openingPrice = openingPrice;
	}

	public Date getQuotationDate() {
		return this.quotationDate;
	}

	public void setQuotationDate(Date quotationDate) {
		this.quotationDate = quotationDate;
	}

	public BigDecimal getSellPrice() {
		return this.sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}	

	/**
	 * @return the quotationState
	 */
	public Integer getQuotationState() {
		return quotationState;
	}

	/**
	 * @param quotationState the quotationState to set
	 */
	public void setQuotationState(Integer quotationState) {
		this.quotationState = quotationState;
	}

	/**
	 * @return the removeMotive
	 */
	public Integer getRemoveMotive() {
		return removeMotive;
	}

	/**
	 * @param removeMotive the removeMotive to set
	 */
	public void setRemoveMotive(Integer removeMotive) {
		this.removeMotive = removeMotive;
	}

	/**
	 * @return the removeOtherMotive
	 */
	public String getRemoveOtherMotive() {
		return removeOtherMotive;
	}

	/**
	 * @param removeOtherMotive the removeOtherMotive to set
	 */
	public void setRemoveOtherMotive(String removeOtherMotive) {
		this.removeOtherMotive = removeOtherMotive;
	}

	public QuotationFile getQuotationFile() {
		return this.quotationFile;
	}

	public void setQuotationFile(QuotationFile quotationFile) {
		this.quotationFile = quotationFile;
	}
	
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>(); 
        return detailsMap;
	}
	
}