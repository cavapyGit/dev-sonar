package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXT_INTERFACE_SEND_USER database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/12/2013
 */
@Entity
@Table(name="EXT_INTERFACE_SEND_USER")
public class ExtInterfaceSendUser implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interf send pk. */
	@Id
	@SequenceGenerator(name="EXT_INTERFACE_SEND_USER_IDEXTINTERFSENDPK_GENERATOR", sequenceName="SQ_ID_EXT_INTERF_SEND_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXT_INTERFACE_SEND_USER_IDEXTINTERFSENDPK_GENERATOR")
	@Column(name="ID_EXT_INTERF_SEND_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfSendPk;

	/** The error exception. */
	@Column(name="ERROR_EXCEPTION", length=1000)
	private String errorException;

	/** The interface send state. */
	@Column(name="INTERFACE_SEND_STATE", precision=10)
	private Integer interfaceSendState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

	/** The process state. */
	@Column(name="PROCESS_STATE", nullable=false, precision=10)
	private Integer processState;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

    /** The send hour. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="SEND_HOUR", nullable=false)
	private Date sendHour;

	/** The user session. */
	@Column(name="USER_SESSION", nullable=false, length=1000)
	private String userSession;

	//bi-directional many-to-one association to ExternalInterfaceSend
	/** The external interface send. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_SEND_FK", nullable=false)
	private ExternalInterfaceSend externalInterfaceSend;
	
	//bi-directional many-to-one association to ExtInterfaceSendFile
	/** The ext interface send files. */
	@OneToMany(mappedBy="externalInterfaceUser")
	private List<ExtInterfaceSendFile> extInterfaceSendFiles;
	

	/** The institution type. */
	@Column(name="INSTITUTION_TYPE", nullable=false, precision=10)
	private Integer institutionType;
	
	/** The institution code. */
	@Column(name="INSTITUTION_CODE", nullable=false, length=20)
	private String institutionCode;
	
	/** The file content generic. */
	@Lob()
	@Basic(fetch=FetchType.LAZY)
	@Column(name="FILE_CONTENT_GENERIC", nullable=false)
	private byte[] fileContentGeneric;

    /**
     * The Constructor.
     */
    public ExtInterfaceSendUser() {
    }

	/**
	 * Gets the id ext interf send pk.
	 *
	 * @return the id ext interf send pk
	 */
	public Long getIdExtInterfSendPk() {
		return this.idExtInterfSendPk;
	}

	/**
	 * Sets the id ext interf send pk.
	 *
	 * @param idExtInterfSendPk the id ext interf send pk
	 */
	public void setIdExtInterfSendPk(Long idExtInterfSendPk) {
		this.idExtInterfSendPk = idExtInterfSendPk;
	}

	/**
	 * Gets the error exception.
	 *
	 * @return the error exception
	 */
	public String getErrorException() {
		return this.errorException;
	}

	/**
	 * Sets the error exception.
	 *
	 * @param errorException the error exception
	 */
	public void setErrorException(String errorException) {
		this.errorException = errorException;
	}

	/**
	 * Gets the interface send state.
	 *
	 * @return the interface send state
	 */
	public Integer getInterfaceSendState() {
		return this.interfaceSendState;
	}

	/**
	 * Sets the interface send state.
	 *
	 * @param interfaceSendState the interface send state
	 */
	public void setInterfaceSendState(Integer interfaceSendState) {
		this.interfaceSendState = interfaceSendState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the process state.
	 *
	 * @return the process state
	 */
	public Integer getProcessState() {
		return this.processState;
	}

	/**
	 * Sets the process state.
	 *
	 * @param processState the process state
	 */
	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the send hour.
	 *
	 * @return the send hour
	 */
	public Date getSendHour() {
		return this.sendHour;
	}

	/**
	 * Sets the send hour.
	 *
	 * @param sendHour the send hour
	 */
	public void setSendHour(Date sendHour) {
		this.sendHour = sendHour;
	}

	/**
	 * Gets the user session.
	 *
	 * @return the user session
	 */
	public String getUserSession() {
		return this.userSession;
	}

	/**
	 * Sets the user session.
	 *
	 * @param userSession the user session
	 */
	public void setUserSession(String userSession) {
		this.userSession = userSession;
	}

	/**
	 * Gets the external interface send.
	 *
	 * @return the external interface send
	 */
	public ExternalInterfaceSend getExternalInterfaceSend() {
		return this.externalInterfaceSend;
	}

	/**
	 * Sets the external interface send.
	 *
	 * @param externalInterfaceSend the external interface send
	 */
	public void setExternalInterfaceSend(ExternalInterfaceSend externalInterfaceSend) {
		this.externalInterfaceSend = externalInterfaceSend;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("extInterfaceSendFiles", extInterfaceSendFiles);
		return detailsMap;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the institution code.
	 *
	 * @return the institution code
	 */
	public String getInstitutionCode() {
		return institutionCode;
	}

	/**
	 * Sets the institution code.
	 *
	 * @param institutionCode the new institution code
	 */
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	/**
	 * Gets the file content generic.
	 *
	 * @return the file content generic
	 */
	public byte[] getFileContentGeneric() {
		return fileContentGeneric;
	}

	/**
	 * Sets the file content generic.
	 *
	 * @param fileContentGeneric the new file content generic
	 */
	public void setFileContentGeneric(byte[] fileContentGeneric) {
		this.fileContentGeneric = fileContentGeneric;
	}

	public List<ExtInterfaceSendFile> getExtInterfaceSendFiles() {
		return extInterfaceSendFiles;
	}

	public void setExtInterfaceSendFiles(
			List<ExtInterfaceSendFile> extInterfaceSendFiles) {
		this.extInterfaceSendFiles = extInterfaceSendFiles;
	}
}