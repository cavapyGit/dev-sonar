package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum InterfaceQueryStateType.
 */
public enum InterfaceSendType {

	/** The registered. */
	MANUAL(Integer.valueOf(1649),"MANUAL"),
	
	/** The deleted. */
	AUTOMATIC(Integer.valueOf(1650), "AUTOMATICA");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceSendType> list=new ArrayList<InterfaceSendType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceSendType> lookup=new HashMap<Integer, InterfaceSendType>();

	static {
		for(InterfaceSendType c : InterfaceSendType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceSendType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	public static InterfaceSendType get(Integer code) {
		return lookup.get(code);
	}
}
