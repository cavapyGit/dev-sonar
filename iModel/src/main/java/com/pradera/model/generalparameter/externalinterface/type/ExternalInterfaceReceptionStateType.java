package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.List;

public enum ExternalInterfaceReceptionStateType {
	/** state REGISTERED. */
	REGISTERED(Integer.valueOf(2184),"REGISTRADO"),
	
	/** state CONFIRMED. */
	CONFIRMED(Integer.valueOf(2185),"CONFIRMADO"),
	
	/** state REJECTED. */
	REJECTED(Integer.valueOf(2186),"RECHAZADO"),
	
	/** state PENDING. */
	PENDING(Integer.valueOf(2187),"EN PROCESO"),
	
	/** state CORRECT. */
	CORRECT(Integer.valueOf(2188),"PROCESADO"),
	
	/** state FAILED. */
	FAILED(Integer.valueOf(2189),"FALLIDO");
	
	
	/** The Constant list. */
	public static final List<ExternalInterfaceReceptionStateType> list = new ArrayList<ExternalInterfaceReceptionStateType>();
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new external interface state.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ExternalInterfaceReceptionStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	static {
        for (ExternalInterfaceReceptionStateType d : ExternalInterfaceReceptionStateType.values()){
        	list.add(d);
        }
    }

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static List<ExternalInterfaceReceptionStateType> getList() {
		return list;
	}
	
	
}
