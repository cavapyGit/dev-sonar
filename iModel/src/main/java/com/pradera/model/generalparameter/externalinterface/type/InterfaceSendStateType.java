package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum InterfaceQueryStateType.
 */
public enum InterfaceSendStateType {

	/** The registered. */
	REGISTERED(Integer.valueOf(1648),"REGISTRADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceSendStateType> list=new ArrayList<InterfaceSendStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceSendStateType> lookup=new HashMap<Integer, InterfaceSendStateType>();

	static {
		for(InterfaceSendStateType c : InterfaceSendStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceSendStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
