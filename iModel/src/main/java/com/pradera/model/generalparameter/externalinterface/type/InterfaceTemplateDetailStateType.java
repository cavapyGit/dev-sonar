package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterfaceTemplateDetailStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(1568),"REGISTRADO"),
	
	/** The annulled. */
	ELIMINATED (Integer.valueOf(1569), "ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceTemplateDetailStateType> list=new ArrayList<InterfaceTemplateDetailStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceTemplateDetailStateType> lookup=new HashMap<Integer, InterfaceTemplateDetailStateType>();

	static {
		for(InterfaceTemplateDetailStateType c : InterfaceTemplateDetailStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	private InterfaceTemplateDetailStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
