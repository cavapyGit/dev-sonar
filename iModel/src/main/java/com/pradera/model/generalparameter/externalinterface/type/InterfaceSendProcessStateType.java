package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum InterfaceQueryStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/12/2013
 */
public enum InterfaceSendProcessStateType {

	/** The genering. */
	GENERATING(Integer.valueOf(1675),"GENERANDO"),
	
	/** The generated. */
	GENERATED(Integer.valueOf(1676),"GENERADO"),
	
	/** The failed. */
	FAILED(Integer.valueOf(1677),"FALLIDO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceSendProcessStateType> list=new ArrayList<InterfaceSendProcessStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceSendProcessStateType> lookup=new HashMap<Integer, InterfaceSendProcessStateType>();

	static {
		for(InterfaceSendProcessStateType c : InterfaceSendProcessStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceSendProcessStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}