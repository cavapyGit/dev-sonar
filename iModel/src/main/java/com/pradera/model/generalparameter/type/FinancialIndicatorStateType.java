package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum  FinancialIndicatorStateType{

	TODAS(Integer.valueOf(1),"TODAS"),
	VIGENTE(Integer.valueOf(1206),"VIGENTE"),
	VENCIDO(Integer.valueOf(1207),"VENCIDO");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private FinancialIndicatorStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<FinancialIndicatorStateType> list = new ArrayList<FinancialIndicatorStateType>();
	public static final Map<Integer, FinancialIndicatorStateType> lookup = new HashMap<Integer, FinancialIndicatorStateType>();
	static {
		for (FinancialIndicatorStateType f : EnumSet.allOf(FinancialIndicatorStateType.class)) {
			list.add(f);
			lookup.put(f.getCode(), f);
		}
	}

	public static FinancialIndicatorStateType get(Integer code) {
		return lookup.get(code);
	}
}
