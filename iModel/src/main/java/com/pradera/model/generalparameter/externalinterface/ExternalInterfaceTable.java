package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE_TABLE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15-nov-2013
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_TABLE")
public class ExternalInterfaceTable implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interface tab pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_TABLE_IDEXTINTERFACETABPK_GENERATOR", sequenceName="SQ_ID_EXT_INTERFACE_TAB_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_TABLE_IDEXTINTERFACETABPK_GENERATOR")
	@Column(name="ID_EXT_INTERFACE_TAB_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfaceTabPk;

	/** The interface table state. */
	@Column(name="INTERFACE_TABLE_STATE", precision=10)
	private Integer interfaceTableState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	/** The table name. */
	@Column(name="TABLE_NAME", nullable=false, length=50)
	private String tableName;

	//bi-directional many-to-one association to ExternalInterfaceColumn
	/** The external interface columns. */
	@OneToMany(mappedBy="externalInterfaceTable", cascade=CascadeType.ALL)
	private List<ExternalInterfaceColumn> externalInterfaceColumns;

	//bi-directional many-to-one association to ExternalInterfaceFilter
	/** The external interface filters. */
	@OneToMany(mappedBy="externalInterfaceTable", cascade=CascadeType.ALL)
	private List<ExternalInterfaceFilter> externalInterfaceFilters;

	//bi-directional many-to-one association to ExternalInterface
	/** The external interface query. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERFACE_QUERY_FK", nullable=false)
	private ExternalInterfaceQuery externalInterfaceQuery;

	//bi-directional many-to-one association to ExtInterfaceFilterPredicate
	/** The ext interface filter predicates. */
	@OneToMany(mappedBy="externalInterfaceTable", cascade=CascadeType.ALL)
	private List<ExtInterfaceFilterPredicate> extInterfaceFilterPredicates;
	
    /**
     * Instantiates a new external interface table.
     */
    public ExternalInterfaceTable() {
    }

	/**
	 * Gets the id ext interface tab pk.
	 *
	 * @return the id ext interface tab pk
	 */
	public Long getIdExtInterfaceTabPk() {
		return this.idExtInterfaceTabPk;
	}

	/**
	 * Sets the id ext interface tab pk.
	 *
	 * @param idExtInterfaceTabPk the new id ext interface tab pk
	 */
	public void setIdExtInterfaceTabPk(Long idExtInterfaceTabPk) {
		this.idExtInterfaceTabPk = idExtInterfaceTabPk;
	}

	/**
	 * Gets the interface table state.
	 *
	 * @return the interface table state
	 */
	public Integer getInterfaceTableState() {
		return this.interfaceTableState;
	}

	/**
	 * Sets the interface table state.
	 *
	 * @param interfaceTableState the new interface table state
	 */
	public void setInterfaceTableState(Integer interfaceTableState) {
		this.interfaceTableState = interfaceTableState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return this.tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the external interface columns.
	 *
	 * @return the external interface columns
	 */
	public List<ExternalInterfaceColumn> getExternalInterfaceColumns() {
		return this.externalInterfaceColumns;
	}

	/**
	 * Sets the external interface columns.
	 *
	 * @param externalInterfaceColumns the new external interface columns
	 */
	public void setExternalInterfaceColumns(List<ExternalInterfaceColumn> externalInterfaceColumns) {
		this.externalInterfaceColumns = externalInterfaceColumns;
	}
	
	/**
	 * Gets the external interface filters.
	 *
	 * @return the external interface filters
	 */
	public List<ExternalInterfaceFilter> getExternalInterfaceFilters() {
		return this.externalInterfaceFilters;
	}

	/**
	 * Sets the external interface filters.
	 *
	 * @param externalInterfaceFilters the new external interface filters
	 */
	public void setExternalInterfaceFilters(List<ExternalInterfaceFilter> externalInterfaceFilters) {
		this.externalInterfaceFilters = externalInterfaceFilters;
	}
	
	/**
	 * Gets the ext interface filter predicates.
	 *
	 * @return the ext interface filter predicates
	 */
	public List<ExtInterfaceFilterPredicate> getExtInterfaceFilterPredicates() {
		return this.extInterfaceFilterPredicates;
	}

	/**
	 * Sets the ext interface filter predicates.
	 *
	 * @param extInterfaceFilterPredicates the new ext interface filter predicates
	 */
	public void setExtInterfaceFilterPredicates(List<ExtInterfaceFilterPredicate> extInterfaceFilterPredicates) {
		this.extInterfaceFilterPredicates = extInterfaceFilterPredicates;
	}

	/**
	 * Gets the external interface query.
	 *
	 * @return the external interface query
	 */
	public ExternalInterfaceQuery getExternalInterfaceQuery() {
		return externalInterfaceQuery;
	}

	/**
	 * Sets the external interface query.
	 *
	 * @param externalInterfaceQuery the new external interface query
	 */
	public void setExternalInterfaceQuery(
			ExternalInterfaceQuery externalInterfaceQuery) {
		this.externalInterfaceQuery = externalInterfaceQuery;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();		
		
		detailsMap.put("externalInterfaceColumns", externalInterfaceColumns);
		detailsMap.put("externalInterfaceFilters", externalInterfaceFilters);
		detailsMap.put("extInterfaceFilterPredicates", extInterfaceFilterPredicates);
		return detailsMap;
	}

	
}