package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * The Enum SignatureType.
 *
 * @author equinajo.
 * @version 1.0 , 19/08/2020
 */
public enum SignatureType {
	
	IND("INDIVIDUAL"),
	
	CON("CONJUNTA");

	/** The valor. */
	private String value;
	
	/** The Constant list. */
	public static final List<SignatureType> list = new ArrayList<SignatureType>();
	
	/** The Constant lookup. */
	public static final Map<String, SignatureType> lookup = new HashMap<String, SignatureType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<SignatureType> listSomeElements(SignatureType... transferSecuritiesTypeParams){
		List<SignatureType> retorno = new ArrayList<SignatureType>();
		for(SignatureType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (SignatureType s : EnumSet.allOf(SignatureType.class)) {
			list.add(s);
			lookup.put(s.getValue(), s);
		}
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	private SignatureType(String valor) {
		this.value = valor;
	}	
	
	public static SignatureType get(Integer codigo) {
		return lookup.get(codigo);
	}
}
