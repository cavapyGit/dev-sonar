package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ExternalInterfaceExecutionType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
public enum ExternalInterfaceExecutionType {

	/** The automatic. */
	AUTOMATIC(Integer.valueOf(1555),"AUTOMATICA"),
	
	/** The manual. */
	MANUAL(Integer.valueOf(1556),"MANUAL"),
	
	/** The boot. */
	BOTH(Integer.valueOf(1557),"AMBAS");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<ExternalInterfaceExecutionType> list=new ArrayList<ExternalInterfaceExecutionType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, ExternalInterfaceExecutionType> lookup=new HashMap<Integer, ExternalInterfaceExecutionType>();

	static {
		for(ExternalInterfaceExecutionType c : ExternalInterfaceExecutionType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ExternalInterfaceExecutionType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the external interface execution type
	 */
	public static ExternalInterfaceExecutionType get(Integer code) {
		return lookup.get(code);
	}
}