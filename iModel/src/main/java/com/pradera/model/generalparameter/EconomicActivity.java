package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;

@Entity
@NamedQueries({
	@NamedQuery(name = EconomicActivity.FIND_BY_SECTOR, 
			query = "SELECT e FROM EconomicActivity e where e.economicSector = :economicSectorPrm")
})
@Table(name="ECONOMIC_ACTIVITY")
public class EconomicActivity implements Serializable, Auditable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_SECTOR="EconomicActivity.findBySector";
	
	@Id
	@Column(name="ID_ECONOMIC_ACTIVITY_PK")
	private Long idEconomicActivityPk;
	
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;
	
	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private Integer economicActivity;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="LAST_MODIFY_DATE ")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	
	
	
	public Long getIdEconomicActivityPk() {
		return idEconomicActivityPk;
	}

	public void setIdEconomicActivityPk(Long idEconomicActivityPk) {
		this.idEconomicActivityPk = idEconomicActivityPk;
	}

	public Integer getEconomicSector() {
		return economicSector;
	}

	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	public Integer getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
