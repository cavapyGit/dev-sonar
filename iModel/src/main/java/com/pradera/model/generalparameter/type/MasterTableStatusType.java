package com.pradera.model.generalparameter.type;

/*
 * 
 */
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum TransferSecuritiesEstatusType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/11/2012
 */
public enum MasterTableStatusType {
	
	/** The registered. */
	INACTIVE(new Integer(0),"INACTIVO","registered.png"),
	
	/** The confirmed. */
	ACTIVE(new Integer(1),"ACTIVO","confirmed.png");
	
	
	
	
	/** The codigo. */
	private Integer code;
	
	/** The valor. */
	private String value;
	
	/** The accion. */
	
	private String icon;
	
	/** The Constant list. */
	public static final List<MasterTableStatusType> list = new ArrayList<MasterTableStatusType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, MasterTableStatusType> lookup = new HashMap<Integer, MasterTableStatusType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<MasterTableStatusType> listSomeElements(MasterTableStatusType... transferSecuritiesTypeParams){
		List<MasterTableStatusType> retorno = new ArrayList<MasterTableStatusType>();
		for(MasterTableStatusType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (MasterTableStatusType s : EnumSet.allOf(MasterTableStatusType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the accion.
	 *
	 * @return the accion
	 */
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * Sets the accion.
	 *
	 * @param accion the new accion
	 */
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Instantiates a new transfer securities estatus type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 * @param accion the accion
	 */
	private MasterTableStatusType(Integer codigo, String valor, String icon) {
		this.code = codigo;
		this.value = valor;
		this.icon = icon;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the transfer securities estatus type
	 */
	public static MasterTableStatusType get(Integer codigo) {
		return lookup.get(codigo);
	}
}
