package com.pradera.model.generalparameter.externalinterface;


import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class SqlController.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29-nov-2013
 */
public class InterfaceContentData{

	/** The data with out detail. */
	private List<Object[]> dataOfQuery;
	
	/** The detail data. */
	private List<InterfaceContentData> detail;

	/**
	 * Gets the data of query.
	 *
	 * @return the data of query
	 */
	public List<Object[]> getDataOfQuery() {
		return dataOfQuery;
	}

	/**
	 * Sets the data of query.
	 *
	 * @param dataOfQuery the new data of query
	 */
	public void setDataOfQuery(List<Object[]> dataOfQuery) {
		this.dataOfQuery = dataOfQuery;
	}

	/**
	 * Gets the detail.
	 *
	 * @return the detail
	 */
	public List<InterfaceContentData> getDetail() {
		return detail;
	}

	/**
	 * Sets the detail.
	 *
	 * @param detail the new detail
	 */
	public void setDetail(List<InterfaceContentData> detail) {
		this.detail = detail;
	}

	public InterfaceContentData(List<Object[]> dataOfQuery) {
		super();
		this.dataOfQuery = dataOfQuery;
		this.detail = new ArrayList<InterfaceContentData>();
	}

	public InterfaceContentData() {
		// TODO Auto-generated constructor stub
		this.dataOfQuery = new ArrayList<Object[]>();
		this.detail = new ArrayList<InterfaceContentData>();
	}
}