package com.pradera.model.generalparameter.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2015.</li>
 * </ul>
 * 
 * The Enum DailyExchangeRoleType.
 *
 * @author PraderaTechnologies.
 *
 */
public enum DailyExchangeRoleType {
	
	/** The Rate Buy */
	BUY(new Integer(1),"COMPRA","COMPRADOR"),
	
	/** The Rate Sell */
	SELL(new Integer(2),"VENTA","VENDEDOR"),
	
	/** The Rate Reference*/
	REFERENCE(new Integer(3),"REFERENCIA","REFERENCIA"),
	
	/** The Rate Average*/
	AVERAGE(new Integer(4),"PROMEDIO","PROMEDIO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The description. */
	private String position;
	
	/** The Constant list. */
	public static final List<DailyExchangeRoleType> list = new ArrayList<DailyExchangeRoleType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, DailyExchangeRoleType> lookup = new HashMap<Integer, DailyExchangeRoleType>();
	
	/**
	 * List some elements.
	 *
	 * @param dailyExchangeRoleType the transfer securities type params
	 * @return the list
	 */
	public static List<DailyExchangeRoleType> listSomeElements(DailyExchangeRoleType... dailyExchangeRoleType){
		List<DailyExchangeRoleType> retorno = new ArrayList<DailyExchangeRoleType>();
		for(DailyExchangeRoleType TransferSecuritiesType: dailyExchangeRoleType){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (DailyExchangeRoleType s : EnumSet.allOf(DailyExchangeRoleType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a Daily Exchange Role.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private DailyExchangeRoleType(Integer codigo, String valor, String position) {
		this.code = codigo;
		this.value = valor;
		this.position = position;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the holder account type
	 */
	public static DailyExchangeRoleType get(Integer codigo) {
		return lookup.get(codigo);
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position the new position
	 */
	public void setPosition(String position) {
		this.position = position;
	}
}