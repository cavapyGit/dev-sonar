package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE_FILTER database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_FILTER")
public class ExternalInterfaceFilter implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interface filt pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_FILTER_IDEXTINTERFACEFILTPK_GENERATOR", sequenceName="SQ_ID_EXT_INTERFACE_FILT_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_FILTER_IDEXTINTERFACEFILTPK_GENERATOR")
	@Column(name="ID_EXT_INTERFACE_FILT_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfaceFiltPk;

	/** The interface filter column. */
	@Column(name="INTERFACE_FILTER_COLUMN", nullable=false, length=100)
	private String interfaceFilterColumn;

	/** The interface filter state. */
	@Column(name="INTERFACE_FILTER_STATE", precision=10)
	private Integer interfaceFilterState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	/** The start logical operator. */
	@Column(name="START_LOGICAL_OPERATOR", nullable=true, length=10)
	private Integer startLogicalOperator;

	//bi-directional many-to-one association to ExternalInterfaceTable
	/** The external interface table. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_TAB_FK")
	private ExternalInterfaceTable externalInterfaceTable;

	//bi-directional many-to-one association to ExtInterfaceFilterPredicate
	/** The ext interface filter predicates. */
	@OneToMany(mappedBy="externalInterfaceFilter", cascade=CascadeType.ALL)
	private List<ExtInterfaceFilterPredicate> extInterfaceFilterPredicates;
	
	/** The interface filter predicate. */
	@Transient
	private ExtInterfaceFilterPredicate interfaceFilterPredicate;
	
	/** The logical operator. */
	@Transient
	private String logicalOperator;

    /**
     * Instantiates a new external interface filter.
     */
    public ExternalInterfaceFilter() {
    }

	/**
	 * Gets the id ext interface filt pk.
	 *
	 * @return the id ext interface filt pk
	 */
	public Long getIdExtInterfaceFiltPk() {
		return this.idExtInterfaceFiltPk;
	}

	/**
	 * Sets the id ext interface filt pk.
	 *
	 * @param idExtInterfaceFiltPk the new id ext interface filt pk
	 */
	public void setIdExtInterfaceFiltPk(Long idExtInterfaceFiltPk) {
		this.idExtInterfaceFiltPk = idExtInterfaceFiltPk;
	}

	/**
	 * Gets the interface filter column.
	 *
	 * @return the interface filter column
	 */
	public String getInterfaceFilterColumn() {
		return this.interfaceFilterColumn;
	}

	/**
	 * Sets the interface filter column.
	 *
	 * @param interfaceFilterColumn the new interface filter column
	 */
	public void setInterfaceFilterColumn(String interfaceFilterColumn) {
		this.interfaceFilterColumn = interfaceFilterColumn;
	}

	/**
	 * Gets the interface filter state.
	 *
	 * @return the interface filter state
	 */
	public Integer getInterfaceFilterState() {
		return this.interfaceFilterState;
	}

	/**
	 * Sets the interface filter state.
	 *
	 * @param interfaceFilterState the new interface filter state
	 */
	public void setInterfaceFilterState(Integer interfaceFilterState) {
		this.interfaceFilterState = interfaceFilterState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the start logical operator.
	 *
	 * @return the start logical operator
	 */
	public Integer getStartLogicalOperator() {
		return this.startLogicalOperator;
	}

	/**
	 * Sets the start logical operator.
	 *
	 * @param startLogicalOperator the new start logical operator
	 */
	public void setStartLogicalOperator(Integer startLogicalOperator) {
		this.startLogicalOperator = startLogicalOperator;
	}

	/**
	 * Gets the external interface table.
	 *
	 * @return the external interface table
	 */
	public ExternalInterfaceTable getExternalInterfaceTable() {
		return this.externalInterfaceTable;
	}

	/**
	 * Sets the external interface table.
	 *
	 * @param externalInterfaceTable the new external interface table
	 */
	public void setExternalInterfaceTable(ExternalInterfaceTable externalInterfaceTable) {
		this.externalInterfaceTable = externalInterfaceTable;
	}
	
	/**
	 * Gets the ext interface filter predicates.
	 *
	 * @return the ext interface filter predicates
	 */
	public List<ExtInterfaceFilterPredicate> getExtInterfaceFilterPredicates() {
		return this.extInterfaceFilterPredicates;
	}

	/**
	 * Sets the ext interface filter predicates.
	 *
	 * @param extInterfaceFilterPredicates the new ext interface filter predicates
	 */
	public void setExtInterfaceFilterPredicates(List<ExtInterfaceFilterPredicate> extInterfaceFilterPredicates) {
		this.extInterfaceFilterPredicates = extInterfaceFilterPredicates;
	}

	/**
	 * Gets the interface filter predicate.
	 *
	 * @return the interface filter predicate
	 */
	public ExtInterfaceFilterPredicate getInterfaceFilterPredicate() {
		return interfaceFilterPredicate;
	}

	/**
	 * Sets the interface filter predicate.
	 *
	 * @param interfaceFilterPredicate the new interface filter predicate
	 */
	public void setInterfaceFilterPredicate(
			ExtInterfaceFilterPredicate interfaceFilterPredicate) {
		this.interfaceFilterPredicate = interfaceFilterPredicate;
	}

	/**
	 * Gets the logical operator.
	 *
	 * @return the logical operator
	 */
	public String getLogicalOperator() {
		return logicalOperator;
	}

	/**
	 * Sets the logical operator.
	 *
	 * @param logicalOperator the new logical operator
	 */
	public void setLogicalOperator(String logicalOperator) {
		this.logicalOperator = logicalOperator;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();		
		
		detailsMap.put("extInterfaceFilterPredicates", extInterfaceFilterPredicates);
		return detailsMap;
	}
	
}