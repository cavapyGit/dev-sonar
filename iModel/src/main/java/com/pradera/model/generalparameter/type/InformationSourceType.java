package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InformationSourceType {
	
	BCRD(Integer.valueOf(212),"BCRD");
	
	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private InformationSourceType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<InformationSourceType> list = new ArrayList<InformationSourceType>();
	public static final Map<Integer, InformationSourceType> lookup = new HashMap<Integer, InformationSourceType>();
	static {
		for (InformationSourceType s : EnumSet.allOf(InformationSourceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
