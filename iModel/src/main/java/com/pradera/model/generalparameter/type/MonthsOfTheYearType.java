package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project Pradera.
* Copyright 2013.</li>
* </ul>
* 
* The Enum MonthsOfTheYearType.
*
* @author PraderaTechnologies.
* @version 1.0 , 26/11/2013
*/
public enum MonthsOfTheYearType {

	ENERO(Integer.valueOf(1),"ENERO"),
	FEBRERO(Integer.valueOf(2),"FEBRERO"),
	MARZO(Integer.valueOf(3),"MARZO"),
	ABRIL(Integer.valueOf(4),"ABRIL"),
	MAYO(Integer.valueOf(5),"MAYO"),
	JUNIO(Integer.valueOf(6),"JUNIO"),
	JULIO(Integer.valueOf(7),"JULIO"),
	AGOSTO(Integer.valueOf(8),"AGOSTO"),
	SETIEMBRE(Integer.valueOf(9),"SETIEMBRE"),
	OCTUBRE(Integer.valueOf(10),"OCTUBRE"),
	NOVIEMBRE(Integer.valueOf(11),"NOVIEMBRE"),
	DICIEMBRE(Integer.valueOf(12),"DICIEMBRE");
	
	
	
	/** The code. */
	private Integer code;
	/** The value. */
	private String value;		
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	private MonthsOfTheYearType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	
	public static final List<MonthsOfTheYearType> list = new ArrayList<MonthsOfTheYearType>();
	public static final Map<Integer, MonthsOfTheYearType> lookup = new HashMap<Integer, MonthsOfTheYearType>();
	static {
		for (MonthsOfTheYearType p : EnumSet.allOf(MonthsOfTheYearType.class)) {
			list.add(p);
			lookup.put(p.getCode(), p);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the Months Of The Year
	 */
	public static MonthsOfTheYearType get(Integer code) {
		return lookup.get(code);
	}
}
