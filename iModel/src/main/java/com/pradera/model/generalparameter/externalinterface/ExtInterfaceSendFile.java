package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXT_INTERFACE_SEND_FILES database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/12/2013
 */
@Entity
@Table(name="EXT_INTERFACE_SEND_FILES")
public class ExtInterfaceSendFile implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext int send file pk. */
	@Id
	@SequenceGenerator(name="EXT_INTERFACE_SEND_FILES_IDEXTINTSENDFILEPK_GENERATOR", sequenceName="SQ_ID_EXT_INT_SEND_FILE_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXT_INTERFACE_SEND_FILES_IDEXTINTSENDFILEPK_GENERATOR")
	@Column(name="ID_EXT_INT_SEND_FILE_PK", unique=true, nullable=false, precision=10)
	private Long idExtIntSendFilePk;

    /** The file content. */
    @Lob()
	@Basic(fetch=FetchType.LAZY)
	@Column(name="FILE_CONTENT", nullable=false)
	private byte[] fileContent;

	/** The file name. */
	@Column(name="FILE_NAME", nullable=false, length=100)
	private String fileName;

	/** The interface sendfile state. */
	@Column(name="INTERFACE_SENDFILE_STATE", precision=10)
	private Integer interfaceSendfileState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	//bi-directional many-to-one association to ExternalInterfaceSend
	/** The external interface send. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERFACE_SEND_USER_FK", nullable=false)
	private ExtInterfaceSendUser externalInterfaceUser;

    /**
     * The Constructor.
     */
    public ExtInterfaceSendFile() {
    }

	/**
	 * Gets the id ext int send file pk.
	 *
	 * @return the id ext int send file pk
	 */
	public Long getIdExtIntSendFilePk() {
		return this.idExtIntSendFilePk;
	}

	/**
	 * Sets the id ext int send file pk.
	 *
	 * @param idExtIntSendFilePk the id ext int send file pk
	 */
	public void setIdExtIntSendFilePk(Long idExtIntSendFilePk) {
		this.idExtIntSendFilePk = idExtIntSendFilePk;
	}

	/**
	 * Gets the file content.
	 *
	 * @return the file content
	 */
	public byte[] getFileContent() {
		return this.fileContent;
	}

	/**
	 * Sets the file content.
	 *
	 * @param fileContent the file content
	 */
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return this.fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the interface sendfile state.
	 *
	 * @return the interface sendfile state
	 */
	public Integer getInterfaceSendfileState() {
		return this.interfaceSendfileState;
	}

	/**
	 * Sets the interface sendfile state.
	 *
	 * @param interfaceSendfileState the interface sendfile state
	 */
	public void setInterfaceSendfileState(Integer interfaceSendfileState) {
		this.interfaceSendfileState = interfaceSendfileState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	/**
	 * Gets the external interface user.
	 *
	 * @return the external interface user
	 */
	public ExtInterfaceSendUser getExternalInterfaceUser() {
		return externalInterfaceUser;
	}

	/**
	 * Sets the external interface user.
	 *
	 * @param externalInterfaceUser the new external interface user
	 */
	public void setExternalInterfaceUser(ExtInterfaceSendUser externalInterfaceUser) {
		this.externalInterfaceUser = externalInterfaceUser;
	}
}