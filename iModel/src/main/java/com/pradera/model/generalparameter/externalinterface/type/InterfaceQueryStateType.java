package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum InterfaceQueryStateType.
 */
public enum InterfaceQueryStateType {

	/** The registered. */
	REGISTERED(Integer.valueOf(1603),"REGISTRADO"),
	
	/** The deleted. */
	DELETED (Integer.valueOf(1604), "ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceQueryStateType> list=new ArrayList<InterfaceQueryStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceQueryStateType> lookup=new HashMap<Integer, InterfaceQueryStateType>();

	static {
		for(InterfaceQueryStateType c : InterfaceQueryStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceQueryStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
