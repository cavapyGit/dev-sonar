package com.pradera.model.generalparameter.externalinterface.engine;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.type.BooleanType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceTableEngine.
 * class to handle diferents tablew from one owner
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/10/2013
 */
public class InterfaceTableEngine implements Serializable {
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result
				+ ((tableName == null) ? 0 : tableName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InterfaceTableEngine other = (InterfaceTableEngine) obj;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (tableName == null) {
			if (other.tableName != null)
				return false;
		} else if (!tableName.equals(other.tableName))
			return false;
		return true;
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The owner. */
	private String owner;
	
	/** The table name. */
	private String tableName;
	
	/** The num rows. */
	private Long numRows;
	
	/** The ind read ony. */
	private Integer indReadOnly;
	
	/** The columns list. */
	private List<InterfaceColumnEngine> columnsList;
	
	/** The columns selected list. */
	private List<InterfaceColumnEngine> columnsSelectedList;

	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the ind read ony.
	 *
	 * @return the ind read ony
	 */
	public Integer getIndReadOnly() {
		return indReadOnly;
	}

	/**
	 * Sets the ind read ony.
	 *
	 * @param indReadOny the new ind read ony
	 */
	public void setIndReadOnly(Integer indReadOny) {
		this.indReadOnly = indReadOny;
	}

	/**
	 * Instantiates a new interface table engine.
	 *
	 * @param owner the owner
	 * @param tableName the table name
	 */
	public InterfaceTableEngine(String owner, String tableName) {
		super();
		this.tableName = tableName;
		this.owner = owner;
	}
	
	/**
	 * Instantiates a new interface table engine.
	 *
	 * @param owner the owner
	 * @param tableName the table name
	 * @param numRows the num rows
	 * @param indRead the ind read
	 */
	public InterfaceTableEngine(Object owner, Object tableName, Object numRows, Object indRead) {
		super();
		this.tableName = tableName.toString();
		this.owner = owner.toString();
		this.numRows = numRows!=null?new Long(numRows.toString()):0L;
		this.indReadOnly = (Integer) (indRead!=null?indRead.equals(BooleanType.YES.getValue())?BooleanType.YES.getCode():BooleanType.NO.getCode():0);
	}

	/**
	 * Gets the num rows.
	 *
	 * @return the num rows
	 */
	public Long getNumRows() {
		return numRows;
	}

	/**
	 * Sets the num rows.
	 *
	 * @param numRows the new num rows
	 */
	public void setNumRows(Long numRows) {
		this.numRows = numRows;
	}

	/**
	 * Instantiates a new interface table engine.
	 */
	public InterfaceTableEngine() {
		super();
	}
	
	/**
	 * Gets the columns list.
	 *
	 * @return the columns list
	 */
	public List<InterfaceColumnEngine> getColumnsList() {
		return columnsList;
	}

	/**
	 * Sets the columns list.
	 *
	 * @param columnsList the new columns list
	 */
	public void setColumnsList(List<InterfaceColumnEngine> columnsList) {
		this.columnsList = columnsList;
	}

	/**
	 * Gets the columns selected list.
	 *
	 * @return the columns selected list
	 */
	public List<InterfaceColumnEngine> getColumnsSelectedList() {
		return columnsSelectedList;
	}

	/**
	 * Sets the columns selected list.
	 *
	 * @param columnsSelectedList the new columns selected list
	 */
	public void setColumnsSelectedList(
			List<InterfaceColumnEngine> columnsSelectedList) {
		this.columnsSelectedList = columnsSelectedList;
	}
	
	
}