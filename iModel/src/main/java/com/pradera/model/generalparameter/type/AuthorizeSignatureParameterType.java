package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author equinajo.
 * @version 1.0 , 22/08/2020
 */
public enum AuthorizeSignatureParameterType {
	
	MAX_SIZE(Integer.valueOf(2556)),
	
	ALLOWED(Integer.valueOf(2557)),
	
	DIMENSION(Integer.valueOf(2558));
	
	/** The Constant list. */
	public final static List<AuthorizeSignatureParameterType> list=new ArrayList<AuthorizeSignatureParameterType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, AuthorizeSignatureParameterType> lookup=new HashMap<Integer, AuthorizeSignatureParameterType>();
	
	static {
		for(AuthorizeSignatureParameterType c : AuthorizeSignatureParameterType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The lng code. */
	private Long lngCode;
	
	/** The str code. */
	private String strCode;
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param code the code
	 */
	private AuthorizeSignatureParameterType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param lngCode the lng code
	 */
	private AuthorizeSignatureParameterType(Long lngCode){
		this.lngCode = lngCode;
	}
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param code the code
	 */
	private AuthorizeSignatureParameterType(String code){
		this.strCode=code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the master table type
	 */
	public static AuthorizeSignatureParameterType get(Integer codigo) {
        return lookup.get(codigo);
    }

	/**
	 * Gets the.
	 *
	 * @param lngCode the lng code
	 * @return the master table type
	 */
	public static AuthorizeSignatureParameterType get(Long lngCode){
		return lookup.get(new Integer(lngCode.intValue()));
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}

	/**
	 * Gets the lng code.
	 *
	 * @return the lng code
	 */
	public Long getLngCode(){
		return this.lngCode;
	}
	
	/**
	 * Gets the str code.
	 *
	 * @return the strCode
	 */
	public String getStrCode() {
		return strCode;
	}
	
	/**
	 * Sets the str code.
	 *
	 * @param strCode the strCode to set
	 */
	public void setStrCode(String strCode) {
		this.strCode = strCode;
	}
	
}