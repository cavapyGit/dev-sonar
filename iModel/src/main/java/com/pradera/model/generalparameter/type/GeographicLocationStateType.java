package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum GeographicLocationStateType {
	
	REGISTERED(Integer.valueOf(1),"REGISTRADO"),
	BLOCKED(Integer.valueOf(2),"BLOQUEADO");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private GeographicLocationStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<GeographicLocationStateType> list = new ArrayList<GeographicLocationStateType>();
	public static final Map<Integer, GeographicLocationStateType> lookup = new HashMap<Integer, GeographicLocationStateType>();
	static {
		for (GeographicLocationStateType s : EnumSet.allOf(GeographicLocationStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
