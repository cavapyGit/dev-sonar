package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE_EXTENSION database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_EXTENSION")
public class ExternalInterfaceExtension implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id interface extension pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_EXTENSION_IDOPERATIRULEPK_GENERATOR", sequenceName="SQ_ID_EXT_INT_EXT_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_EXTENSION_IDOPERATIRULEPK_GENERATOR")
	@Column(name="ID_OPERATI_RULE_PK", unique=true, nullable=false, precision=10)
	private Long idInterfaceExtensionPk;

	/** The interface extension state. */
	@Column(name="INTERFACE_EXTENSION_STATE", precision=10)
	private Integer interfaceExtensionState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	//bi-directional many-to-one association to ExtensionFileType
	/** The extension file type. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXTENSION_FILE_TYPE_FK", nullable=false)
	private ExtensionFileType extensionFileType;

	//bi-directional many-to-one association to ExternalInterface
	/** The external interface. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_FK", nullable=false)
	private ExternalInterface externalInterface;
	
    /**
     * Instantiates a new external interface extension.
     */
    public ExternalInterfaceExtension() {
    }

	/**
	 * Gets the id interface extension pk.
	 *
	 * @return the id interface extension pk
	 */
	public Long getIdInterfaceExtensionPk() {
		return idInterfaceExtensionPk;
	}

	/**
	 * Sets the id interface extension pk.
	 *
	 * @param idInterfaceExtensionPk the new id interface extension pk
	 */
	public void setIdInterfaceExtensionPk(Long idInterfaceExtensionPk) {
		this.idInterfaceExtensionPk = idInterfaceExtensionPk;
	}

	/**
	 * Gets the interface extension state.
	 *
	 * @return the interface extension state
	 */
	public Integer getInterfaceExtensionState() {
		return this.interfaceExtensionState;
	}

	/**
	 * Sets the interface extension state.
	 *
	 * @param interfaceExtensionState the new interface extension state
	 */
	public void setInterfaceExtensionState(Integer interfaceExtensionState) {
		this.interfaceExtensionState = interfaceExtensionState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}
	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	/**
	 * Gets the extension file type.
	 *
	 * @return the extension file type
	 */
	public ExtensionFileType getExtensionFileType() {
		return this.extensionFileType;
	}
	/**
	 * Sets the extension file type.
	 *
	 * @param extensionFileType the new extension file type
	 */
	public void setExtensionFileType(ExtensionFileType extensionFileType) {
		this.extensionFileType = extensionFileType;
	}
	/**
	 * Gets the external interface.
	 *
	 * @return the external interface
	 */
	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}
	/**
	 * Sets the external interface.
	 *
	 * @param externalInterface the new external interface
	 */
	public void setExternalInterface(ExternalInterface externalInterface) {
		this.externalInterface = externalInterface;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}			
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
}