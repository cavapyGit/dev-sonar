package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the INTERFACE_RECEPTION database table.
 * 
 */
@Entity
@Table(name="INTERFACE_PROCESS")
@NamedQuery(name="InterfaceProcess.findAll", query="SELECT i FROM InterfaceProcess i")
public class InterfaceProcess implements Serializable,Auditable  {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="INTERFACE_RECEPTION_IDINTERFACERECEPTIONPK_GENERATOR", sequenceName="SQ_ID_INTERFACE_RECEPTION_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INTERFACE_RECEPTION_IDINTERFACERECEPTIONPK_GENERATOR")
	@Column(name="ID_INTERFACE_PROCESS_PK", unique=true, nullable=false, precision=10)
	private Long idInterfaceProcessPk;

	@Column(name="DIRECTORY_FILE",length=200)
	private String directoryFile;

	@Column(name="FILE_NAME", nullable=false, length=200)
	private String fileName;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXTENSION_FILE_TYPE_FK", nullable=false)
//	@Column(name="ID_EXTENSION_FILE_TYPE_FK", nullable=false, precision=10)
	private ExtensionFileType idExtensionFileTypeFk;

//	@Column(name="ID_EXTERNAL_INTERFACE_FK", nullable=false, precision=10)
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXTERNAL_INTERFACE_FK", nullable=false)
	private ExternalInterface idExternalInterfaceFk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PROCESS_DATE", nullable=false)
	private Date processDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;
	
	@Column(name="PROCESS_TYPE", nullable=false, precision=10)
	private Integer processType;

	@Column(name="PROCESS_STATE", nullable=false, precision=10)
	private Integer processState;
	
	@Lob()
    @Basic(fetch=FetchType.LAZY)
   	@Column(name="FILE_UPLOAD")
   	private byte[] fileUpload;
	
	@Lob()
    @Basic(fetch=FetchType.LAZY)
   	@Column(name="FILE_RESPONSE")
   	private byte[] fileResponse;
	
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;
	
	public InterfaceProcess() {
	}

	
	
	public InterfaceProcess(Long idInterfaceProcessPk) {
		super();
		this.idInterfaceProcessPk = idInterfaceProcessPk;
	}



	public Long getIdInterfaceProcessPk() {
		return idInterfaceProcessPk;
	}


	public void setIdInterfaceProcessPk(Long idInterfaceProcessPk) {
		this.idInterfaceProcessPk = idInterfaceProcessPk;
	}


	public String getDirectoryFile() {
		return this.directoryFile;
	}

	public void setDirectoryFile(String directoryFile) {
		this.directoryFile = directoryFile;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	public ExtensionFileType getIdExtensionFileTypeFk() {
		return idExtensionFileTypeFk;
	}

	public void setIdExtensionFileTypeFk(ExtensionFileType idExtensionFileTypeFk) {
		this.idExtensionFileTypeFk = idExtensionFileTypeFk;
	}

	public ExternalInterface getIdExternalInterfaceFk() {
		return idExternalInterfaceFk;
	}

	public void setIdExternalInterfaceFk(ExternalInterface idExternalInterfaceFk) {
		this.idExternalInterfaceFk = idExternalInterfaceFk;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date receptionDate) {
		this.processDate = receptionDate;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	public Integer getProcessType() {
		return processType;
	}

	public void setProcessType(Integer receptionType) {
		this.processType = receptionType;
	}

	
	public Integer getProcessState() {
		return processState;
	}


	public void setProcessState(Integer receptionState) {
		this.processState = receptionState;
	}
	
	public byte[] getFileUpload() {
		return fileUpload;
	}


	public void setFileUpload(byte[] fileUpload) {
		this.fileUpload = fileUpload;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}	
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}


	public byte[] getFileResponse() {
		return fileResponse;
	}


	public void setFileResponse(byte[] fileResponse) {
		this.fileResponse = fileResponse;
	}


	

}