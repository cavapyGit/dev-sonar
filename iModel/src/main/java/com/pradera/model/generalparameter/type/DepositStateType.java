

package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum DepositStateType {
       
       REGISTERED(Integer.valueOf(1),"REGISTRADO"),         //REGISTRADO
       REJECTED(Integer.valueOf(2),"RECHAZADO"),                   //RECHAZADO
       CONFIRMED(Integer.valueOf(3),"CONFIRMADO"),                 //CONFIRMADO
       
       ;
       
       private Integer code;
       private String value;

       // constructor
       private DepositStateType(Integer code, String value) {
             this.code = code;
             this.value = value;
       }

       public static final List<DepositStateType> lst = new ArrayList<DepositStateType>();
       public static final Map<Integer, DepositStateType> lookup = new HashMap<Integer, DepositStateType>();
       static {
             for (DepositStateType x : EnumSet.allOf(DepositStateType.class)) {
                    lst.add(x);
                    lookup.put(x.getCode(), x);
             }
       }

       public Integer getCode() {
             return code;
       }

       public void setCode(Integer code) {
             this.code = code;
       }

       public String getValue() {
             return value;
       }

       public void setValue(String value) {
             this.value = value;
       }
}