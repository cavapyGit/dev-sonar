package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum HolidayStateType.
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.0
 */
public enum HolidayStateType {

	
	REGISTERED(Integer.valueOf(1), "REGISTRADO"),
	
	DELETED(Integer.valueOf(0), "ELIMINADO");

	private Integer code;
	private String value;

	public static final List<HolidayStateType> list = new ArrayList<HolidayStateType>();
	public static final Map<Integer, HolidayStateType> lookup = new HashMap<Integer, HolidayStateType>();

	static {
		for (HolidayStateType s : EnumSet.allOf(HolidayStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}		

	private HolidayStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}		

}




