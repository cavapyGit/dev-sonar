package com.pradera.model.generalparameter.externalinterface.engine;

import java.io.Serializable;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceTableEngine.
 * class to handle diferents tablew from one owner
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/10/2013
 */
public class InterfaceDataQuery implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The row data. */
	private List<Object> rowData;
	
	/** The parameters. */
	private List<Object> parameters;
	
	/** The object data list. */
	private List<InterfaceDataQuery> objectDataList;

	/**
	 * Gets the row data.
	 *
	 * @return the row data
	 */
	public List<Object> getRowData() {
		return rowData;
	}

	/**
	 * Sets the row data.
	 *
	 * @param rowData the new row data
	 */
	public void setRowData(List<Object> rowData) {
		this.rowData = rowData;
	}

	/**
	 * Gets the parameters.
	 *
	 * @return the parameters
	 */
	public List<Object> getParameters() {
		return parameters;
	}

	/**
	 * Sets the parameters.
	 *
	 * @param parameters the new parameters
	 */
	public void setParameters(List<Object> parameters) {
		this.parameters = parameters;
	}

	/**
	 * Gets the object data list.
	 *
	 * @return the object data list
	 */
	public List<InterfaceDataQuery> getObjectDataList() {
		return objectDataList;
	}

	/**
	 * Sets the object data list.
	 *
	 * @param objectDataList the new object data list
	 */
	public void setObjectDataList(List<InterfaceDataQuery> objectDataList) {
		this.objectDataList = objectDataList;
	}
}