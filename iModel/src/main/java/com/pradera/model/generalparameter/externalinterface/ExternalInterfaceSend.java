package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.report.ReportLogger;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE_SEND database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/11/2013
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_SEND")
public class ExternalInterfaceSend implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interface send pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_SEND_IDEXTINTERFACESENDPK_GENERATOR", sequenceName="SQ_ID_EXT_INTERFACE_SEND_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_SEND_IDEXTINTERFACESENDPK_GENERATOR")
	@Column(name="ID_EXT_INTERFACE_SEND_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfaceSendPk;

	/** The description. */
	@Column(nullable=false, length=200)
	private String description;

    /** The execution date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXECUTION_DATE", nullable=false)
	private Date executionDate;

    /** The execution hour. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="EXECUTION_HOUR", nullable=false)
	private Date executionHour;

	/** The ext interface send type. */
	@Column(name="EXT_INTERFACE_SEND_TYPE", nullable=false, precision=10)
	private Integer extInterfaceSendType;

	/** The interface send state. */
	@Column(name="INTERFACE_SEND_STATE", precision=10)
	private Integer interfaceSendState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

    /** The real generate date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REAL_GENERATE_DATE", nullable=false)
	private Date realGenerateDate;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	//bi-directional many-to-one association to ExternalInterface
	/** The external interface. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_FK", nullable=false)
	private ExternalInterface externalInterface;

	//bi-directional many-to-one association to ExtInterfaceSendUser
	/** The ext interface send users. */
	@OneToMany(mappedBy="externalInterfaceSend")
	private List<ExtInterfaceSendUser> extInterfaceSendUsers;
	
	/** The report logger list. */
	@OneToMany(mappedBy="externalInterfaceSend")
	private List<ReportLogger> reportLoggerList;
	
	/** The institution request. */
	@Column(name="INSTITUTION_REQUEST", nullable=false, precision=10)
	private String institutionRequest;
	
	/** The total interfaces. */
	@Column(name="INTERFACES_TOTAL", nullable=false, precision=10)
	private Integer totalInterfaces;
	
	/** The generated interfaces. */
	@Column(name="GENERATED_INTERFACES", nullable=false, precision=10)
	private Integer generatedInterfaces;
	
	/** The failed interfaces. */
	@Column(name="FAILED_INTERFACE", nullable=false, precision=10)
	private Integer failedInterfaces;

    /**
     * Instantiates a new external interface send.
     */
    public ExternalInterfaceSend() {
    }

	/**
	 * Gets the id ext interface send pk.
	 *
	 * @return the id ext interface send pk
	 */
	public Long getIdExtInterfaceSendPk() {
		return this.idExtInterfaceSendPk;
	}

	/**
	 * Sets the id ext interface send pk.
	 *
	 * @param idExtInterfaceSendPk the new id ext interface send pk
	 */
	public void setIdExtInterfaceSendPk(Long idExtInterfaceSendPk) {
		this.idExtInterfaceSendPk = idExtInterfaceSendPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the execution date.
	 *
	 * @return the execution date
	 */
	public Date getExecutionDate() {
		return this.executionDate;
	}

	/**
	 * Sets the execution date.
	 *
	 * @param executionDate the new execution date
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * Gets the execution hour.
	 *
	 * @return the execution hour
	 */
	public Date getExecutionHour() {
		return this.executionHour;
	}

	/**
	 * Sets the execution hour.
	 *
	 * @param executionHour the new execution hour
	 */
	public void setExecutionHour(Date executionHour) {
		this.executionHour = executionHour;
	}

	/**
	 * Gets the ext interface send type.
	 *
	 * @return the ext interface send type
	 */
	public Integer getExtInterfaceSendType() {
		return this.extInterfaceSendType;
	}

	/**
	 * Sets the ext interface send type.
	 *
	 * @param extInterfaceSendType the new ext interface send type
	 */
	public void setExtInterfaceSendType(Integer extInterfaceSendType) {
		this.extInterfaceSendType = extInterfaceSendType;
	}

	/**
	 * Gets the interface send state.
	 *
	 * @return the interface send state
	 */
	public Integer getInterfaceSendState() {
		return this.interfaceSendState;
	}

	/**
	 * Sets the interface send state.
	 *
	 * @param interfaceSendState the new interface send state
	 */
	public void setInterfaceSendState(Integer interfaceSendState) {
		this.interfaceSendState = interfaceSendState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the real generate date.
	 *
	 * @return the real generate date
	 */
	public Date getRealGenerateDate() {
		return this.realGenerateDate;
	}

	/**
	 * Sets the real generate date.
	 *
	 * @param realGenerateDate the new real generate date
	 */
	public void setRealGenerateDate(Date realGenerateDate) {
		this.realGenerateDate = realGenerateDate;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the external interface.
	 *
	 * @return the external interface
	 */
	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}

	/**
	 * Sets the external interface.
	 *
	 * @param externalInterface the new external interface
	 */
	public void setExternalInterface(ExternalInterface externalInterface) {
		this.externalInterface = externalInterface;
	}
	
	/**
	 * Gets the ext interface send users.
	 *
	 * @return the ext interface send users
	 */
	public List<ExtInterfaceSendUser> getExtInterfaceSendUsers() {
		return this.extInterfaceSendUsers;
	}

	/**
	 * Sets the ext interface send users.
	 *
	 * @param extInterfaceSendUsers the new ext interface send users
	 */
	public void setExtInterfaceSendUsers(List<ExtInterfaceSendUser> extInterfaceSendUsers) {
		this.extInterfaceSendUsers = extInterfaceSendUsers;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("extInterfaceSendUsers", extInterfaceSendUsers);
		return detailsMap;
	}

	/**
	 * Gets the institution request.
	 *
	 * @return the institution request
	 */
	public String getInstitutionRequest() {
		return institutionRequest;
	}

	/**
	 * Sets the institution request.
	 *
	 * @param institutionRequest the new institution request
	 */
	public void setInstitutionRequest(String institutionRequest) {
		this.institutionRequest = institutionRequest;
	}

	/**
	 * Gets the report logger list.
	 *
	 * @return the report logger list
	 */
	public List<ReportLogger> getReportLoggerList() {
		return reportLoggerList;
	}

	/**
	 * Sets the report logger list.
	 *
	 * @param reportLoggerList the new report logger list
	 */
	public void setReportLoggerList(List<ReportLogger> reportLoggerList) {
		this.reportLoggerList = reportLoggerList;
	}

	/**
	 * Gets the total interfaces.
	 *
	 * @return the total interfaces
	 */
	public Integer getTotalInterfaces() {
		return totalInterfaces;
	}

	/**
	 * Sets the total interfaces.
	 *
	 * @param totalInterfaces the new total interfaces
	 */
	public void setTotalInterfaces(Integer totalInterfaces) {
		this.totalInterfaces = totalInterfaces;
	}

	/**
	 * Gets the generated interfaces.
	 *
	 * @return the generated interfaces
	 */
	public Integer getGeneratedInterfaces() {
		return generatedInterfaces;
	}

	/**
	 * Sets the generated interfaces.
	 *
	 * @param generatedInterfaces the new generated interfaces
	 */
	public void setGeneratedInterfaces(Integer generatedInterfaces) {
		this.generatedInterfaces = generatedInterfaces;
	}

	/**
	 * Gets the failed interfaces.
	 *
	 * @return the failed interfaces
	 */
	public Integer getFailedInterfaces() {
		return failedInterfaces;
	}

	/**
	 * Sets the failed interfaces.
	 *
	 * @param failedInterfaces the new failed interfaces
	 */
	public void setFailedInterfaces(Integer failedInterfaces) {
		this.failedInterfaces = failedInterfaces;
	}
}