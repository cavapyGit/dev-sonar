package com.pradera.model.generalparameter;

import java.io.Serializable;

/**
 * Copyright PraderaTechnologies 2013.
 * The persistent class for the MESSAGE database table.
 * The Class Message is used to display message in Posts screen through ConsultServiceBean.java
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/02/2013
 */

public class Message implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Constructor */
	public Message() {
    }
	/** The idMessage */
	private Integer idMessage;

	/** 
	 *  The strMessageDate 
	 *  Used by ConsultServiceBean.java to display date in String format in Posts Screen .
	 * */
	private String strMessageDate;
	
	/** The messageSubject */
	private String messageSubject;
	
	/** The messageBody */
	private String messageBody;
	
	/** The read */
	private boolean read;
	
	/**
	 * @return the read
	 */
	public boolean isRead() {
		return read;
	}

	/**
	 * @param read the read to set
	 */
	public void setRead(boolean read) {
		this.read = read;
	}

	/**
	 * @return the messageBody
	 */
	public String getMessageBody() {
		return messageBody;
	}

	/**
	 * @param messageBody the messageBody to set
	 */
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	/**
	 * @return the idMessage
	 */
	public Integer getIdMessage() {
		return idMessage;
	}

	/**
	 * @return the strMessageDate
	 */
	public String getStrMessageDate() {
		return strMessageDate;
	}

	/**
	 * @param strMessageDate the strMessageDate to set
	 */
	public void setStrMessageDate(String strMessageDate) {
		this.strMessageDate = strMessageDate;
	}

	/**
	 * @param idMessage the idMessage to set
	 */
	public void setIdMessage(Integer idMessage) {
		this.idMessage = idMessage;
	}

	
	/**
	 * @return the messageSubject
	 */
	public String getMessageSubject() {
		return messageSubject;
	}

	/**
	 * @param messageSubject the messageSubject to set
	 */
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}
}
