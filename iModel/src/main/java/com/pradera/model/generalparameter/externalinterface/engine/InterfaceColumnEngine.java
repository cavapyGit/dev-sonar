package com.pradera.model.generalparameter.externalinterface.engine;

import java.io.Serializable;

import com.pradera.integration.common.type.BooleanType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class InterfaceTableEngine.
 * class to handle diferents tablew from one owner
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/10/2013
 */
public class InterfaceColumnEngine implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The owner. */
	private String owner;
	
	/** The table name. */
	private String tableName;
	
	/** The column name. */
	private String columnName;
	
	/** The data type. */
	private String dataType;
	
	/** The data length. */
	private Integer dataLength;
	
	/** The ind nullable. */
	private Integer indNullable;
	
	/** The is selected. */
	private Boolean isSelected;
	
	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Instantiates a new interface table engine.
	 *
	 * @param owner the owner
	 * @param tableName the table name
	 */
	public InterfaceColumnEngine(String owner, String tableName) {
		super();
		this.tableName = tableName;
		this.owner = owner;
	}
	
	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * Sets the column name.
	 *
	 * @param columnName the new column name
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * Gets the data type.
	 *
	 * @return the data type
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * Sets the data type.
	 *
	 * @param dataType the new data type
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * Gets the data length.
	 *
	 * @return the data length
	 */
	public Integer getDataLength() {
		return dataLength;
	}

	/**
	 * Sets the data length.
	 *
	 * @param dataLength the new data length
	 */
	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}

	/**
	 * Gets the ind nullable.
	 *
	 * @return the ind nullable
	 */
	public Integer getIndNullable() {
		return indNullable;
	}

	/**
	 * Sets the ind nullable.
	 *
	 * @param indNullable the new ind nullable
	 */
	public void setIndNullable(Integer indNullable) {
		this.indNullable = indNullable;
	}

	/**
	 * Instantiates a new interface table engine.
	 */
	public InterfaceColumnEngine() {
		super();
	}

	/**
	 * Instantiates a new interface column engine.
	 *
	 * @param owner the owner
	 * @param tableName the table name
	 * @param columnName the column name
	 * @param dataType the data type
	 * @param dataLength the data length
	 * @param indNullable the ind nullable
	 */
	public InterfaceColumnEngine(Object owner, Object tableName,
			Object columnName, Object dataType, Object dataLength,
			Object indNullable) {
		super();
		this.owner = owner.toString();
		this.tableName = tableName.toString();
		this.columnName = columnName.toString();
		this.dataType = dataType.toString();
		this.dataLength = dataLength!=null?new Integer(dataLength.toString()):0;
		this.indNullable = indNullable!=null?BooleanType.NO.getValue().contains(indNullable.toString())?BooleanType.NO.getCode():BooleanType.YES.getCode():0;
	}

	/**
	 * Gets the checks if is selected.
	 *
	 * @return the checks if is selected
	 */
	public Boolean getIsSelected() {
		return isSelected;
	}

	/**
	 * Sets the checks if is selected.
	 *
	 * @param isSelected the new checks if is selected
	 */
	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
}