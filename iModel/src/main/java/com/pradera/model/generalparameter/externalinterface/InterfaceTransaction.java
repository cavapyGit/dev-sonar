package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.TradeOperation;


/**
 * The persistent class for the INTERFACE_TRANSACTION database table.
 * 
 */
@Entity
@NamedQueries({ 
	@NamedQuery(name="InterfaceTransaction.findAll", query="SELECT i FROM InterfaceTransaction i"),
	@NamedQuery(
			name = InterfaceTransaction.INTERFACE_GET_BY_OPERATION_NUMBER, 
			query = " SELECT i FROM InterfaceTransaction i where i.operationNumber = :operationNumber ")
})
@Table(name="INTERFACE_TRANSACTION")
public class InterfaceTransaction implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;
	
	public static final String INTERFACE_GET_BY_OPERATION_NUMBER ="interface.getbyoperationnumber";

	@Id
	@SequenceGenerator(name="INTERFACE_TRANSACTION_IDINTERFACETRANSACTIONPK_GENERATOR", sequenceName="SQ_ID_INTERFACE_TRANSACTION_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INTERFACE_TRANSACTION_IDINTERFACETRANSACTIONPK_GENERATOR")
	@Column(name="ID_INTERFACE_TRANSACTION_PK", unique=true, nullable=false, precision=10)
	private Long idInterfaceTransactionPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_SECURITY_CODE_FK")
	private Security security;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_CUSTODY_OPERATION_FK")
	private CustodyOperation custodyOperation;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_TRADE_OPERATION_FK")
	private TradeOperation tradeOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_FUNDS_OPERATION_FK")
	private FundsOperation fundsOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERFACE_PROCESS_FK")
	private InterfaceProcess interfaceProcess;
	
	@Column(name="OPERATION_NUMBER")
	private Long operationNumber;

	@Column(name="OBLIGATION_NUMBER")
	private String obligationNumber;
	
	@Column(name="TRANSACTION_STATE")
	private Integer transactionState;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	public InterfaceTransaction() {
	}

	public Long getIdInterfaceTransactionPk() {
		return idInterfaceTransactionPk;
	}

	public void setIdInterfaceTransactionPk(Long idInterfaceTransactionPk) {
		this.idInterfaceTransactionPk = idInterfaceTransactionPk;
	}
	
	public Integer getTransactionState() {
		return transactionState;
	}

	public void setTransactionState(Integer transactionState) {
		this.transactionState = transactionState;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public InterfaceProcess getInterfaceProcess() {
		return interfaceProcess;
	}

	public void setInterfaceProcess(InterfaceProcess interfaceProcess) {
		this.interfaceProcess = interfaceProcess;
	}

	public Long getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public String getObligationNumber() {
		return obligationNumber;
	}

	public void setObligationNumber(String obligationNumber) {
		this.obligationNumber = obligationNumber;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	public TradeOperation getTradeOperation() {
		return tradeOperation;
	}

	public void setTradeOperation(TradeOperation tradeOperation) {
		this.tradeOperation = tradeOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public FundsOperation getFundsOperation() {
		return fundsOperation;
	}

	public void setFundsOperation(FundsOperation fundsOperation) {
		this.fundsOperation = fundsOperation;
	}
	
}