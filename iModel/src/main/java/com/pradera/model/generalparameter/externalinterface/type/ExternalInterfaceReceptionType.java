package com.pradera.model.generalparameter.externalinterface.type;



public enum ExternalInterfaceReceptionType {
	
	WEBSERVICE(new Integer(2176),"WEBSERVICE"),
	MANUAL(new Integer(2177),"MANUAL");
	
	private Integer code;
	private String value;
	
	/**
	 * @return code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	

	private ExternalInterfaceReceptionType(int code, String name) {
		this.code = code;
		this.value = name;
	}
}
