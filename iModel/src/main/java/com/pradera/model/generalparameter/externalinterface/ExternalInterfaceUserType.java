package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE_USER_TYPE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_USER_TYPE")
public class ExternalInterfaceUserType implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interf user pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_USER_TYPE_IDEXTINTERFUSERPK_GENERATOR", sequenceName="SQ_ID_EXT_INTERF_USER_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_USER_TYPE_IDEXTINTERFUSERPK_GENERATOR")
	@Column(name="ID_EXT_INTERF_USER_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfUserPk;

	/** The id user type fk. */
	@Column(name="ID_USER_TYPE_FK", nullable=false, precision=10)
	private Integer idUserTypeFk;

	/** The interface user state. */
	@Column(name="INTERFACE_USER_STATE", precision=10)
	private Integer interfaceUserState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	//bi-directional many-to-one association to ExternalInterface
	/** The external interface. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_FK", nullable=false)
	private ExternalInterface externalInterface;
	
	/** The institution code. */
	@Column(name="INSTITUTION_CODE")
	private String institutionCode;
	
	/** The user type description. */
	@Transient
	private String userTypeDescription;
		
	/** The is selected. */
	@Transient
	private Boolean isSelected;

    /**
     * Instantiates a new external interface user type.
     */
    public ExternalInterfaceUserType() {
    }

	/**
	 * Gets the id ext interf user pk.
	 *
	 * @return the id ext interf user pk
	 */
	public Long getIdExtInterfUserPk() {
		return this.idExtInterfUserPk;
	}

	/**
	 * Sets the id ext interf user pk.
	 *
	 * @param idExtInterfUserPk the new id ext interf user pk
	 */
	public void setIdExtInterfUserPk(Long idExtInterfUserPk) {
		this.idExtInterfUserPk = idExtInterfUserPk;
	}

	/**
	 * Gets the id user type fk.
	 *
	 * @return the id user type fk
	 */
	public Integer getIdUserTypeFk() {
		return this.idUserTypeFk;
	}

	/**
	 * Sets the id user type fk.
	 *
	 * @param idUserTypeFk the new id user type fk
	 */
	public void setIdUserTypeFk(Integer idUserTypeFk) {
		this.idUserTypeFk = idUserTypeFk;
	}

	/**
	 * Gets the interface user state.
	 *
	 * @return the interface user state
	 */
	public Integer getInterfaceUserState() {
		return this.interfaceUserState;
	}

	/**
	 * Sets the interface user state.
	 *
	 * @param interfaceUserState the new interface user state
	 */
	public void setInterfaceUserState(Integer interfaceUserState) {
		this.interfaceUserState = interfaceUserState;
	}
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}
	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}
	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}
	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}
	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	/**
	 * Gets the external interface.
	 *
	 * @return the external interface
	 */
	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}
	/**
	 * Sets the external interface.
	 *
	 * @param externalInterface the new external interface
	 */
	public void setExternalInterface(ExternalInterface externalInterface) {
		this.externalInterface = externalInterface;
	}
	/**
	 * Gets the user type description.
	 *
	 * @return the user type description
	 */
	public String getUserTypeDescription() {
		return userTypeDescription;
	}
	/**
	 * Sets the user type description.
	 *
	 * @param userTypeDescription the new user type description
	 */
	public void setUserTypeDescription(String userTypeDescription) {
		this.userTypeDescription = userTypeDescription;
	}
	/**
	 * Gets the checks if is selected.
	 *
	 * @return the checks if is selected
	 */
	public Boolean getIsSelected() {
		return isSelected;
	}
	/**
	 * Sets the checks if is selected.
	 *
	 * @param isSelected the new checks if is selected
	 */
	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}	
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	/**
	 * Gets the institution code.
	 *
	 * @return the institutionCode
	 */
	public String getInstitutionCode() {
		return institutionCode;
	}

	/**
	 * Sets the institution code.
	 *
	 * @param institutionCode the institutionCode to set
	 */
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	
	
}