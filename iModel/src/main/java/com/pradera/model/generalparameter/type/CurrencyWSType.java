package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum CurrencyType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/05/2013
 */
public enum CurrencyWSType {
	
	/** The bob. */
	UFV(Integer.valueOf(76),"UNIDAD DE FOMENTO DE VIVIENDA","2","UFV"),
	
	/** The usd. */
	USDC(Integer.valueOf(34),"DOLARES AMERICANOS COMPRA","3","USD"),
	
	/** The usd. */
	USDV(Integer.valueOf(35),"DOLARES AMERICANOS VENTA","3","USD"),
	
	/** The eur. */
	EU(Integer.valueOf(53),"EUROS","4","EU"),
	
	/** The dmv. */
	DMV(Integer.valueOf(75),"DOLARES CON MANTENIMIENTO DE VALORES","5","DMV");	
	
	/** The Constant list. */
	public final static List<CurrencyWSType> list=new ArrayList<CurrencyWSType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, CurrencyWSType> lookup=new HashMap<Integer, CurrencyWSType>();
	
	static {
		for(CurrencyWSType c : CurrencyWSType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The code cd. */
	private String codeCd;
	
	/** The code iso. */
	private String codeIso;
	
	/**
	 * Instantiates a new currency type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 * @param codeIso the code iso
	 */
	private CurrencyWSType(Integer code, String value, String codeCd, String codeIso){
		this.code=code;
		this.value=value;
		this.codeCd=codeCd;
		this.codeIso = codeIso;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the currency type
	 */
	public static CurrencyWSType get(Integer code) {
		return lookup.get(code);
	}
	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
	
	/**
	 * Gets the code cd.
	 *
	 * @return the code cd
	 */
	public String getCodeCd(){
		return this.codeCd;
	}

	/**
	 * Gets the code iso.
	 *
	 * @return the code iso
	 */
	public String getCodeIso() {
		return codeIso;
	}

	/**
	 * Sets the code iso.
	 *
	 * @param codeIso the new code iso
	 */
	public void setCodeIso(String codeIso) {
		this.codeIso = codeIso;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Sets the code cd.
	 *
	 * @param codeCd the new code cd
	 */
	public void setCodeCd(String codeCd) {
		this.codeCd = codeCd;
	}
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<CurrencyWSType> listSomeElements(CurrencyWSType... transferSecuritiesTypeParams){
		List<CurrencyWSType> retorno = new ArrayList<CurrencyWSType>();
		for(CurrencyWSType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
}
