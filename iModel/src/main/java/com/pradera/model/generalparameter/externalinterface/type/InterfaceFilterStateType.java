package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterfaceFilterStateType {

	/** The automatic. */
	REGISTERED(Integer.valueOf(1607),"REGISTRADO"),
	
	/** The boot. */
	DELETED(Integer.valueOf(1608),"ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceFilterStateType> list=new ArrayList<InterfaceFilterStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceFilterStateType> lookup=new HashMap<Integer, InterfaceFilterStateType>();

	static {
		for(InterfaceFilterStateType c : InterfaceFilterStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceFilterStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the interface filter state type
	 */
	public static InterfaceFilterStateType get(Integer code) {
		return lookup.get(code);
	}
}