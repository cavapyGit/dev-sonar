package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXT_INTERFACE_FILTER_PREDICATE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@Entity
@Table(name="EXT_INTERFACE_FILTER_PREDICATE")
public class ExtInterfaceFilterPredicate implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interface filt pred pk. */
	@Id
	@SequenceGenerator(name="EXT_INTERFACE_FILTER_PREDICATE_IDEXTINTERFACEFILTPREDPK_GENERATOR", sequenceName="SQ_ID_INTERFACE_FILT_PRED_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXT_INTERFACE_FILTER_PREDICATE_IDEXTINTERFACEFILTPREDPK_GENERATOR")
	@Column(name="ID_EXT_INTERFACE_FILT_PRED_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfaceFiltPredPk;

	/** The comparison operator. */
	@Column(name="COMPARISON_OPERATOR", nullable=false, length=20)
	private Integer comparisonOperator;

	/** The interface predic state. */
	@Column(name="INTERFACE_PREDIC_STATE", precision=10)
	private Integer interfacePredicState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

	/** The predicate type. */
	@Column(name="PREDICATE_TYPE", nullable=false, length=20)
	private Integer predicateType;

	/** The predicate value. */
	@Column(name="PREDICATE_VALUE", length=200)
	private String predicateValue;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	//bi-directional many-to-one association to ExternalInterfaceColumn
	/** The external interface column. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_COL_FK")
	private ExternalInterfaceColumn externalInterfaceColumn;

	//bi-directional many-to-one association to ExternalInterfaceFilter
	/** The external interface filter. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_FILT_FK", nullable=false)
	private ExternalInterfaceFilter externalInterfaceFilter;

	//bi-directional many-to-one association to ExternalInterfaceTable
	/** The external interface table. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_TAB_FK")
	private ExternalInterfaceTable externalInterfaceTable;
	
	/** The predicate type value. */
	@Transient
	private String predicateTypeValue;
	
	/** The comparison operator value. */
	@Transient
	private String comparisonOperatorValue;
	
	/** The predicate value column. */
	@Transient
	private String predicateValueColumn;
	
	/** The detail template parent. */
	@Transient
	private ExtInterfaceTemplateDetail detailTemplateParent;

    /**
     * The Constructor.
     */
    public ExtInterfaceFilterPredicate() {
    }

	/**
	 * Gets the id ext interface filt pred pk.
	 *
	 * @return the id ext interface filt pred pk
	 */
	public Long getIdExtInterfaceFiltPredPk() {
		return this.idExtInterfaceFiltPredPk;
	}

	/**
	 * Sets the id ext interface filt pred pk.
	 *
	 * @param idExtInterfaceFiltPredPk the id ext interface filt pred pk
	 */
	public void setIdExtInterfaceFiltPredPk(Long idExtInterfaceFiltPredPk) {
		this.idExtInterfaceFiltPredPk = idExtInterfaceFiltPredPk;
	}

	/**
	 * Gets the comparison operator.
	 *
	 * @return the comparison operator
	 */
	public Integer getComparisonOperator() {
		return this.comparisonOperator;
	}

	/**
	 * Sets the comparison operator.
	 *
	 * @param comparisonOperator the comparison operator
	 */
	public void setComparisonOperator(Integer comparisonOperator) {
		this.comparisonOperator = comparisonOperator;
	}

	/**
	 * Gets the interface predic state.
	 *
	 * @return the interface predic state
	 */
	public Integer getInterfacePredicState() {
		return this.interfacePredicState;
	}

	/**
	 * Sets the interface predic state.
	 *
	 * @param interfacePredicState the interface predic state
	 */
	public void setInterfacePredicState(Integer interfacePredicState) {
		this.interfacePredicState = interfacePredicState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the predicate type.
	 *
	 * @return the predicate type
	 */
	public Integer getPredicateType() {
		return this.predicateType;
	}

	/**
	 * Sets the predicate type.
	 *
	 * @param predicateType the predicate type
	 */
	public void setPredicateType(Integer predicateType) {
		this.predicateType = predicateType;
	}

	/**
	 * Gets the predicate value.
	 *
	 * @return the predicate value
	 */
	public String getPredicateValue() {
		return this.predicateValue;
	}

	/**
	 * Sets the predicate value.
	 *
	 * @param predicateValue the predicate value
	 */
	public void setPredicateValue(String predicateValue) {
		this.predicateValue = predicateValue;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the external interface column.
	 *
	 * @return the external interface column
	 */
	public ExternalInterfaceColumn getExternalInterfaceColumn() {
		return this.externalInterfaceColumn;
	}

	/**
	 * Sets the external interface column.
	 *
	 * @param externalInterfaceColumn the external interface column
	 */
	public void setExternalInterfaceColumn(ExternalInterfaceColumn externalInterfaceColumn) {
		this.externalInterfaceColumn = externalInterfaceColumn;
	}
	
	/**
	 * Gets the external interface filter.
	 *
	 * @return the external interface filter
	 */
	public ExternalInterfaceFilter getExternalInterfaceFilter() {
		return this.externalInterfaceFilter;
	}

	/**
	 * Sets the external interface filter.
	 *
	 * @param externalInterfaceFilter the external interface filter
	 */
	public void setExternalInterfaceFilter(ExternalInterfaceFilter externalInterfaceFilter) {
		this.externalInterfaceFilter = externalInterfaceFilter;
	}
	
	/**
	 * Gets the external interface table.
	 *
	 * @return the external interface table
	 */
	public ExternalInterfaceTable getExternalInterfaceTable() {
		return this.externalInterfaceTable;
	}

	/**
	 * Sets the external interface table.
	 *
	 * @param externalInterfaceTable the external interface table
	 */
	public void setExternalInterfaceTable(ExternalInterfaceTable externalInterfaceTable) {
		this.externalInterfaceTable = externalInterfaceTable;
	}

	/**
	 * Gets the predicate type value.
	 *
	 * @return the predicate type value
	 */
	public String getPredicateTypeValue() {
		return predicateTypeValue;
	}

	/**
	 * Sets the predicate type value.
	 *
	 * @param predicateTypeValue the new predicate type value
	 */
	public void setPredicateTypeValue(String predicateTypeValue) {
		this.predicateTypeValue = predicateTypeValue;
	}

	/**
	 * Gets the comparison operator value.
	 *
	 * @return the comparison operator value
	 */
	public String getComparisonOperatorValue() {
		return comparisonOperatorValue;
	}

	/**
	 * Sets the comparison operator value.
	 *
	 * @param comparisonOperatorValue the new comparison operator value
	 */
	public void setComparisonOperatorValue(String comparisonOperatorValue) {
		this.comparisonOperatorValue = comparisonOperatorValue;
	}

	/**
	 * Gets the detail template parent.
	 *
	 * @return the detail template parent
	 */
	public ExtInterfaceTemplateDetail getDetailTemplateParent() {
		return detailTemplateParent;
	}

	/**
	 * Sets the detail template parent.
	 *
	 * @param detailTemplateParent the new detail template parent
	 */
	public void setDetailTemplateParent(
			ExtInterfaceTemplateDetail detailTemplateParent) {
		this.detailTemplateParent = detailTemplateParent;
	}

	/**
	 * Gets the predicate value column.
	 *
	 * @return the predicate value column
	 */
	public String getPredicateValueColumn() {
		return predicateValueColumn;
	}

	/**
	 * Sets the predicate value column.
	 *
	 * @param predicateValueColumn the new predicate value column
	 */
	public void setPredicateValueColumn(String predicateValueColumn) {
		this.predicateValueColumn = predicateValueColumn;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
	
}