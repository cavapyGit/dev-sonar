package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterfaceTableStateType {

	/** The automatic. */
	REGISTERED(Integer.valueOf(1605),"REGISTRADO"),
	
	/** The boot. */
	DELETED(Integer.valueOf(1606),"ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<ExtensionFileStateType> list=new ArrayList<ExtensionFileStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, ExtensionFileStateType> lookup=new HashMap<Integer, ExtensionFileStateType>();

	static {
		for(ExtensionFileStateType c : ExtensionFileStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceTableStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}