package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum  IndicatorType{

	INDICE(Integer.valueOf(711),"INDICE"),
	TASA(Integer.valueOf(712),"TASA");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private IndicatorType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<IndicatorType> list = new ArrayList<IndicatorType>();
	public static final Map<Integer, IndicatorType> lookup = new HashMap<Integer, IndicatorType>();
	static {
		for (IndicatorType i : EnumSet.allOf(IndicatorType.class)) {
			list.add(i);
			lookup.put(i.getCode(), i);
		}
	}

}
