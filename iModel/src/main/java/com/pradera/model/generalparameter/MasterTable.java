package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class MasterTable.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@Entity
@Table(name="MASTER_TABLE")
public class MasterTable implements Serializable ,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The master table pk. */
	@Id
	@SequenceGenerator(name="MASTER_TABLE_MASTERTABLEPK_GENERATOR", sequenceName="SQ_MASTER_TABLE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MASTER_TABLE_MASTERTABLEPK_GENERATOR")
	@Column(name="MASTER_TABLE_PK")
	private Integer masterTablePk;

	/** The key size. */
	@Column(name="KEY_SIZE")
	private Integer keySize;

	/** The key type. */
	@Column(name="KEY_TYPE")
	private Integer keyType;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The long description. */
	@Column(name="LONG_DESCRIPTION")
	private String longDescription;

	/** The master table cd. */
	@Column(name="MASTER_TABLE_CD")
	private String masterTableCd;

	/** The name date1. */
	@Column(name="NAME_DATE1")
	private String nameDate1;

	/** The name date2. */
	@Column(name="NAME_DATE2")
	private String nameDate2;

	/** The name decimal amount1. */
	@Column(name="NAME_DECIMAL_AMOUNT1")
	private String nameDecimalAmount1;

	/** The name decimal amount2. */
	@Column(name="NAME_DECIMAL_AMOUNT2")
	private String nameDecimalAmount2;

	/** The name indicator1. */
	@Column(name="NAME_INDICATOR1")
	private String nameIndicator1;

	/** The name indicator2. */
	@Column(name="NAME_INDICATOR2")
	private String nameIndicator2;

	/** The name indicator3. */
	@Column(name="NAME_INDICATOR3")
	private String nameIndicator3;

	/** The name indicator4. */
	@Column(name="NAME_INDICATOR4")
	private String nameIndicator4;

	/** The name indicator5. */
	@Column(name="NAME_INDICATOR5")
	private String nameIndicator5;

	/** The name indicator6. */
	@Column(name="NAME_INDICATOR6")
	private String nameIndicator6;

	/** The name integer. */
	@Column(name="NAME_INTEGER")
	private String nameInteger;

	/** The name long integer. */
	@Column(name="NAME_LONG_INTEGER")
	private String nameLongInteger;

	/** The name observation. */
	@Column(name="NAME_OBSERVATION")
	private String nameObservation;

	/** The name state. */
	@Column(name="NAME_STATE")
	private String nameState;

	/** The name subclasification1. */
	@Column(name="NAME_SUBCLASIFICATION1")
	private String nameSubclasification1;

	/** The name subclasification2. */
	@Column(name="NAME_SUBCLASIFICATION2")
	private String nameSubclasification2;

	/** The name text1. */
	@Column(name="NAME_TEXT1")
	private String nameText1;

	/** The name text2. */
	@Column(name="NAME_TEXT2")
	private String nameText2;

	/** The name text3. */
	@Column(name="NAME_TEXT3")
	private String nameText3;

	/** The register date. */
	@Temporal(TemporalType.DATE)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

	/** The req date1. */
	@Column(name="REQ_DATE1")
	private Integer reqDate1;

	/** The req date2. */
	@Column(name="REQ_DATE2")
	private Integer reqDate2;

	/** The req decimal amount1. */
	@Column(name="REQ_DECIMAL_AMOUNT1")
	private Integer reqDecimalAmount1;

	/** The req decimal amount2. */
	@Column(name="REQ_DECIMAL_AMOUNT2")
	private Integer reqDecimalAmount2;

	/** The req indicator1. */
	@Column(name="REQ_INDICATOR1")
	private Integer reqIndicator1;

	/** The req indicator2. */
	@Column(name="REQ_INDICATOR2")
	private Integer reqIndicator2;

	/** The req indicator3. */
	@Column(name="REQ_INDICATOR3")
	private Integer reqIndicator3;

	/** The req indicator4. */
	@Column(name="REQ_INDICATOR4")
	private Integer reqIndicator4;

	/** The req indicator5. */
	@Column(name="REQ_INDICATOR5")
	private Integer reqIndicator5;

	/** The req indicator6. */
	@Column(name="REQ_INDICATOR6")
	private Integer reqIndicator6;

	/** The req integer. */
	@Column(name="REQ_INTEGER")
	private Integer reqInteger;

	/** The req long integer. */
	@Column(name="REQ_LONG_INTEGER")
	private Integer reqLongInteger;

	/** The req observation. */
	@Column(name="REQ_OBSERVATION")
	private Integer reqObservation;

	/** The req state. */
	@Column(name="REQ_STATE")
	private Integer reqState;

	/** The req subclasification1. */
	@Column(name="REQ_SUBCLASIFICATION1")
	private Integer reqSubclasification1;

	/** The req subclasification2. */
	@Column(name="REQ_SUBCLASIFICATION2")
	private Integer reqSubclasification2;

	/** The req text1. */
	@Column(name="REQ_TEXT1")
	private Integer reqText1;

	/** The req text2. */
	@Column(name="REQ_TEXT2")
	private Integer reqText2;

	/** The req text3. */
	@Column(name="REQ_TEXT3")
	private Integer reqText3;

	/** The short description. */
	@Column(name="SHORT_DESCRIPTION")
	private String shortDescription;

	/** The table name. */
	@Column(name="TABLE_NAME")
	private String tableName;

	//bi-directional many-to-one association to ParameterTable
	/** The parameter tables. */
	@OneToMany(mappedBy="masterTable") 
	private List<ParameterTable> parameterTables;

	//@Transient
	@Column(name="STATE") 
	private Integer state;
	
	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Instantiates a new master table.
	 */
	public MasterTable() {
		registerDate = new Date();
	}

	public MasterTable(Integer masterTablePk) {
		this.registerDate = new Date();
		this.masterTablePk = masterTablePk;
	}

	/**
	 * Gets the master table pk.
	 *
	 * @return the master table pk
	 */
	public Integer getMasterTablePk() {
		return masterTablePk;
	}

	/**
	 * Sets the master table pk.
	 *
	 * @param masterTablePk the new master table pk
	 */
	public void setMasterTablePk(Integer masterTablePk) {
		this.masterTablePk = masterTablePk;
	}

	/**
	 * Gets the key size.
	 *
	 * @return the key size
	 */
	public Integer getKeySize() {
		if(Validations.validateIsNullOrNotPositive(keySize)){
			keySize = null;
		}
		return keySize;
	}

	/**
	 * Sets the key size.
	 *
	 * @param keySize the new key size
	 */
	public void setKeySize(Integer keySize) {
		if(Validations.validateIsNotNullAndPositive(keySize)){
			this.keySize = keySize;
		}else{
			this.keySize = null;
		}
	}

	/**
	 * Gets the key type.
	 *
	 * @return the key type
	 */
	public Integer getKeyType() {
		return keyType;
	}

	/**
	 * Sets the key type.
	 *
	 * @param keyType the new key type
	 */
	public void setKeyType(Integer keyType) {
		this.keyType = keyType;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the long description.
	 *
	 * @return the long description
	 */
	public String getLongDescription() {
		return longDescription;
	}

	/**
	 * Sets the long description.
	 *
	 * @param longDescription the new long description
	 */
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	/**
	 * Gets the master table cd.
	 *
	 * @return the master table cd
	 */
	public String getMasterTableCd() {
		return masterTableCd;
	}

	/**
	 * Sets the master table cd.
	 *
	 * @param masterTableCd the new master table cd
	 */
	public void setMasterTableCd(String masterTableCd) {
		this.masterTableCd = masterTableCd;
	}

	/**
	 * Gets the name date1.
	 *
	 * @return the name date1
	 */
	public String getNameDate1() {
		return nameDate1;
	}

	/**
	 * Sets the name date1.
	 *
	 * @param nameDate1 the new name date1
	 */
	public void setNameDate1(String nameDate1) {
		this.nameDate1 = nameDate1;
	}

	/**
	 * Gets the name date2.
	 *
	 * @return the name date2
	 */
	public String getNameDate2() {
		return nameDate2;
	}

	/**
	 * Sets the name date2.
	 *
	 * @param nameDate2 the new name date2
	 */
	public void setNameDate2(String nameDate2) {
		this.nameDate2 = nameDate2;
	}

	/**
	 * Gets the name decimal amount1.
	 *
	 * @return the name decimal amount1
	 */
	public String getNameDecimalAmount1() {
		return nameDecimalAmount1;
	}

	/**
	 * Sets the name decimal amount1.
	 *
	 * @param nameDecimalAmount1 the new name decimal amount1
	 */
	public void setNameDecimalAmount1(String nameDecimalAmount1) {
		this.nameDecimalAmount1 = nameDecimalAmount1;
	}

	/**
	 * Gets the name decimal amount2.
	 *
	 * @return the name decimal amount2
	 */
	public String getNameDecimalAmount2() {
		return nameDecimalAmount2;
	}

	/**
	 * Sets the name decimal amount2.
	 *
	 * @param nameDecimalAmount2 the new name decimal amount2
	 */
	public void setNameDecimalAmount2(String nameDecimalAmount2) {
		this.nameDecimalAmount2 = nameDecimalAmount2;
	}

	/**
	 * Gets the name indicator1.
	 *
	 * @return the name indicator1
	 */
	public String getNameIndicator1() {
		return nameIndicator1;
	}

	/**
	 * Sets the name indicator1.
	 *
	 * @param nameIndicator1 the new name indicator1
	 */
	public void setNameIndicator1(String nameIndicator1) {
		this.nameIndicator1 = nameIndicator1;
	}

	/**
	 * Gets the name indicator2.
	 *
	 * @return the name indicator2
	 */
	public String getNameIndicator2() {
		return nameIndicator2;
	}

	/**
	 * Sets the name indicator2.
	 *
	 * @param nameIndicator2 the new name indicator2
	 */
	public void setNameIndicator2(String nameIndicator2) {
		this.nameIndicator2 = nameIndicator2;
	}

	/**
	 * Gets the name indicator3.
	 *
	 * @return the name indicator3
	 */
	public String getNameIndicator3() {
		return nameIndicator3;
	}

	/**
	 * Sets the name indicator3.
	 *
	 * @param nameIndicator3 the new name indicator3
	 */
	public void setNameIndicator3(String nameIndicator3) {
		this.nameIndicator3 = nameIndicator3;
	}

	/**
	 * Gets the name indicator4.
	 *
	 * @return the name indicator4
	 */
	public String getNameIndicator4() {
		return nameIndicator4;
	}

	/**
	 * Sets the name indicator4.
	 *
	 * @param nameIndicator4 the new name indicator4
	 */
	public void setNameIndicator4(String nameIndicator4) {
		this.nameIndicator4 = nameIndicator4;
	}

	/**
	 * Gets the name indicator5.
	 *
	 * @return the name indicator5
	 */
	public String getNameIndicator5() {
		return nameIndicator5;
	}

	/**
	 * Sets the name indicator5.
	 *
	 * @param nameIndicator5 the new name indicator5
	 */
	public void setNameIndicator5(String nameIndicator5) {
		this.nameIndicator5 = nameIndicator5;
	}

	/**
	 * Gets the name indicator6.
	 *
	 * @return the name indicator6
	 */
	public String getNameIndicator6() {
		return nameIndicator6;
	}

	/**
	 * Sets the name indicator6.
	 *
	 * @param nameIndicator6 the new name indicator6
	 */
	public void setNameIndicator6(String nameIndicator6) {
		this.nameIndicator6 = nameIndicator6;
	}

	/**
	 * Gets the name integer.
	 *
	 * @return the name integer
	 */
	public String getNameInteger() {
		return nameInteger;
	}

	/**
	 * Sets the name integer.
	 *
	 * @param nameInteger the new name integer
	 */
	public void setNameInteger(String nameInteger) {
		this.nameInteger = nameInteger;
	}

	/**
	 * Gets the name long integer.
	 *
	 * @return the name long integer
	 */
	public String getNameLongInteger() {
		return nameLongInteger;
	}

	/**
	 * Sets the name long integer.
	 *
	 * @param nameLongInteger the new name long integer
	 */
	public void setNameLongInteger(String nameLongInteger) {
		this.nameLongInteger = nameLongInteger;
	}

	/**
	 * Gets the name observation.
	 *
	 * @return the name observation
	 */
	public String getNameObservation() {
		return nameObservation;
	}

	/**
	 * Sets the name observation.
	 *
	 * @param nameObservation the new name observation
	 */
	public void setNameObservation(String nameObservation) {
		this.nameObservation = nameObservation;
	}

	/**
	 * Gets the name state.
	 *
	 * @return the name state
	 */
	public String getNameState() {
		return nameState;
	}

	/**
	 * Sets the name state.
	 *
	 * @param nameState the new name state
	 */
	public void setNameState(String nameState) {
		this.nameState = nameState;
	}

	/**
	 * Gets the name subclasification1.
	 *
	 * @return the name subclasification1
	 */
	public String getNameSubclasification1() {
		return nameSubclasification1;
	}

	/**
	 * Sets the name subclasification1.
	 *
	 * @param nameSubclasification1 the new name subclasification1
	 */
	public void setNameSubclasification1(String nameSubclasification1) {
		this.nameSubclasification1 = nameSubclasification1;
	}

	/**
	 * Gets the name subclasification2.
	 *
	 * @return the name subclasification2
	 */
	public String getNameSubclasification2() {
		return nameSubclasification2;
	}

	/**
	 * Sets the name subclasification2.
	 *
	 * @param nameSubclasification2 the new name subclasification2
	 */
	public void setNameSubclasification2(String nameSubclasification2) {
		this.nameSubclasification2 = nameSubclasification2;
	}

	/**
	 * Gets the name text1.
	 *
	 * @return the name text1
	 */
	public String getNameText1() {
		return nameText1;
	}

	/**
	 * Sets the name text1.
	 *
	 * @param nameText1 the new name text1
	 */
	public void setNameText1(String nameText1) {
		this.nameText1 = nameText1;
	}

	/**
	 * Gets the name text2.
	 *
	 * @return the name text2
	 */
	public String getNameText2() {
		return nameText2;
	}

	/**
	 * Sets the name text2.
	 *
	 * @param nameText2 the new name text2
	 */
	public void setNameText2(String nameText2) {
		this.nameText2 = nameText2;
	}

	/**
	 * Gets the name text3.
	 *
	 * @return the name text3
	 */
	public String getNameText3() {
		return nameText3;
	}

	/**
	 * Sets the name text3.
	 *
	 * @param nameText3 the new name text3
	 */
	public void setNameText3(String nameText3) {
		this.nameText3 = nameText3;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the req date1.
	 *
	 * @return the req date1
	 */
	public Integer getReqDate1() {
		return reqDate1;
	}

	/**
	 * Sets the req date1.
	 *
	 * @param reqDate1 the new req date1
	 */
	public void setReqDate1(Integer reqDate1) {
		this.reqDate1 = reqDate1;
	}

	/**
	 * Gets the req date2.
	 *
	 * @return the req date2
	 */
	public Integer getReqDate2() {
		return reqDate2;
	}

	/**
	 * Sets the req date2.
	 *
	 * @param reqDate2 the new req date2
	 */
	public void setReqDate2(Integer reqDate2) {
		this.reqDate2 = reqDate2;
	}

	/**
	 * Gets the req decimal amount1.
	 *
	 * @return the req decimal amount1
	 */
	public Integer getReqDecimalAmount1() {
		return reqDecimalAmount1;
	}

	/**
	 * Sets the req decimal amount1.
	 *
	 * @param reqDecimalAmount1 the new req decimal amount1
	 */
	public void setReqDecimalAmount1(Integer reqDecimalAmount1) {
		this.reqDecimalAmount1 = reqDecimalAmount1;
	}

	/**
	 * Gets the req decimal amount2.
	 *
	 * @return the req decimal amount2
	 */
	public Integer getReqDecimalAmount2() {
		return reqDecimalAmount2;
	}

	/**
	 * Sets the req decimal amount2.
	 *
	 * @param reqDecimalAmount2 the new req decimal amount2
	 */
	public void setReqDecimalAmount2(Integer reqDecimalAmount2) {
		this.reqDecimalAmount2 = reqDecimalAmount2;
	}

	/**
	 * Gets the req indicator1.
	 *
	 * @return the req indicator1
	 */
	public Integer getReqIndicator1() {
		return reqIndicator1;
	}

	/**
	 * Sets the req indicator1.
	 *
	 * @param reqIndicator1 the new req indicator1
	 */
	public void setReqIndicator1(Integer reqIndicator1) {
		this.reqIndicator1 = reqIndicator1;
	}

	/**
	 * Gets the req indicator2.
	 *
	 * @return the req indicator2
	 */
	public Integer getReqIndicator2() {
		return reqIndicator2;
	}

	/**
	 * Sets the req indicator2.
	 *
	 * @param reqIndicator2 the new req indicator2
	 */
	public void setReqIndicator2(Integer reqIndicator2) {
		this.reqIndicator2 = reqIndicator2;
	}

	/**
	 * Gets the req indicator3.
	 *
	 * @return the req indicator3
	 */
	public Integer getReqIndicator3() {
		return reqIndicator3;
	}

	/**
	 * Sets the req indicator3.
	 *
	 * @param reqIndicator3 the new req indicator3
	 */
	public void setReqIndicator3(Integer reqIndicator3) {
		this.reqIndicator3 = reqIndicator3;
	}

	/**
	 * Gets the req indicator4.
	 *
	 * @return the req indicator4
	 */
	public Integer getReqIndicator4() {
		return reqIndicator4;
	}

	/**
	 * Sets the req indicator4.
	 *
	 * @param reqIndicator4 the new req indicator4
	 */
	public void setReqIndicator4(Integer reqIndicator4) {
		this.reqIndicator4 = reqIndicator4;
	}

	/**
	 * Gets the req indicator5.
	 *
	 * @return the req indicator5
	 */
	public Integer getReqIndicator5() {
		return reqIndicator5;
	}

	/**
	 * Sets the req indicator5.
	 *
	 * @param reqIndicator5 the new req indicator5
	 */
	public void setReqIndicator5(Integer reqIndicator5) {
		this.reqIndicator5 = reqIndicator5;
	}

	/**
	 * Gets the req indicator6.
	 *
	 * @return the req indicator6
	 */
	public Integer getReqIndicator6() {
		return reqIndicator6;
	}

	/**
	 * Sets the req indicator6.
	 *
	 * @param reqIndicator6 the new req indicator6
	 */
	public void setReqIndicator6(Integer reqIndicator6) {
		this.reqIndicator6 = reqIndicator6;
	}

	/**
	 * Gets the req integer.
	 *
	 * @return the req integer
	 */
	public Integer getReqInteger() {
		return reqInteger;
	}

	/**
	 * Sets the req integer.
	 *
	 * @param reqInteger the new req integer
	 */
	public void setReqInteger(Integer reqInteger) {
		this.reqInteger = reqInteger;
	}

	/**
	 * Gets the req long integer.
	 *
	 * @return the req long integer
	 */
	public Integer getReqLongInteger() {
		return reqLongInteger;
	}

	/**
	 * Sets the req long integer.
	 *
	 * @param reqLongInteger the new req long integer
	 */
	public void setReqLongInteger(Integer reqLongInteger) {
		this.reqLongInteger = reqLongInteger;
	}

	/**
	 * Gets the req observation.
	 *
	 * @return the req observation
	 */
	public Integer getReqObservation() {
		return reqObservation;
	}

	/**
	 * Sets the req observation.
	 *
	 * @param reqObservation the new req observation
	 */
	public void setReqObservation(Integer reqObservation) {
		this.reqObservation = reqObservation;
	}

	/**
	 * Gets the req state.
	 *
	 * @return the req state
	 */
	public Integer getReqState() {
		return reqState;
	}

	/**
	 * Sets the req state.
	 *
	 * @param reqState the new req state
	 */
	public void setReqState(Integer reqState) {
		this.reqState = reqState;
	}

	/**
	 * Gets the req subclasification1.
	 *
	 * @return the req subclasification1
	 */
	public Integer getReqSubclasification1() {
		return reqSubclasification1;
	}

	/**
	 * Sets the req subclasification1.
	 *
	 * @param reqSubclasification1 the new req subclasification1
	 */
	public void setReqSubclasification1(Integer reqSubclasification1) {
		this.reqSubclasification1 = reqSubclasification1;
	}

	/**
	 * Gets the req subclasification2.
	 *
	 * @return the req subclasification2
	 */
	public Integer getReqSubclasification2() {
		return reqSubclasification2;
	}

	/**
	 * Sets the req subclasification2.
	 *
	 * @param reqSubclasification2 the new req subclasification2
	 */
	public void setReqSubclasification2(Integer reqSubclasification2) {
		this.reqSubclasification2 = reqSubclasification2;
	}

	/**
	 * Gets the req text1.
	 *
	 * @return the req text1
	 */
	public Integer getReqText1() {
		return reqText1;
	}

	/**
	 * Sets the req text1.
	 *
	 * @param reqText1 the new req text1
	 */
	public void setReqText1(Integer reqText1) {
		this.reqText1 = reqText1;
	}

	/**
	 * Gets the req text2.
	 *
	 * @return the req text2
	 */
	public Integer getReqText2() {
		return reqText2;
	}

	/**
	 * Sets the req text2.
	 *
	 * @param reqText2 the new req text2
	 */
	public void setReqText2(Integer reqText2) {
		this.reqText2 = reqText2;
	}

	/**
	 * Gets the req text3.
	 *
	 * @return the req text3
	 */
	public Integer getReqText3() {
		return reqText3;
	}

	/**
	 * Sets the req text3.
	 *
	 * @param reqText3 the new req text3
	 */
	public void setReqText3(Integer reqText3) {
		this.reqText3 = reqText3;
	}

	/**
	 * Gets the short description.
	 *
	 * @return the short description
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * Sets the short description.
	 *
	 * @param shortDescription the new short description
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the parameter tables.
	 *
	 * @return the parameter tables
	 */
	public List<ParameterTable> getParameterTables() {
		return parameterTables;
	}

	/**
	 * Sets the parameter tables.
	 *
	 * @param parameterTables the new parameter tables
	 */
	public void setParameterTables(List<ParameterTable> parameterTables) {
		this.parameterTables = parameterTables;
	}
	 /**
	  * getStatusDescripction method used for get the status description
	  * @return
	  */
    public String getStatusDescripction() {
    	String description = null;
		if (state != null) {
			try{
			description = com.pradera.model.generalparameter.type.MasterTableStatusType.lookup.get(state).getValue();
			}catch(Exception e){
				
			}
		}
		return description;
    }
    /**
	  * getTypeDescripction method used for get the type description
	  * @return
	  */
    public String getTypeDescripction() {
    	String description = null;
		if (keyType != null) {
			try{
			description = com.pradera.model.generalparameter.type.MasterTableRegType.lookup.get(keyType).getValue();
			}catch(Exception e){
				
			}
		}
		return description;
    }
    
    /* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("parameterTables", parameterTables);
        return detailsMap;
	}
	}