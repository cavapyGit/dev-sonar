package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ExternalInterfaceExecutionType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
public enum ExternalInterfaceStateType {

	/** The automatic. */
	REGISTERED(Integer.valueOf(1562),"REGISTRADO"),
	
	/** The approved. */
	APPROVED(Integer.valueOf(1563),"APROBADO"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(1564),"CONFIRMADO"),
	
	/** The blocked. */
	BLOCKED(Integer.valueOf(1565),"BLOQUEADO"),
	
	/** The annulated. */
	ANNULATED(Integer.valueOf(1566),"ANULADO"),
	
	/** The rejected. */
	REJECTED(Integer.valueOf(1567),"RECHAZADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<ExternalInterfaceStateType> list=new ArrayList<ExternalInterfaceStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, ExternalInterfaceStateType> lookup=new HashMap<Integer, ExternalInterfaceStateType>();

	static {
		for(ExternalInterfaceStateType c : ExternalInterfaceStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ExternalInterfaceStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}