package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum  FinancialIndicatorType{

	INDEX(Integer.valueOf(711),"INDICE"),
	RATE(Integer.valueOf(712),"TASA");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private FinancialIndicatorType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<FinancialIndicatorType> list = new ArrayList<FinancialIndicatorType>();
	public static final Map<Integer, FinancialIndicatorType> lookup = new HashMap<Integer, FinancialIndicatorType>();
	static {
		for (FinancialIndicatorType f : EnumSet.allOf(FinancialIndicatorType.class)) {
			list.add(f);
			lookup.put(f.getCode(), f);
		}
	}

	public static FinancialIndicatorType get(Integer code) {
		return lookup.get(code);
	}
}
