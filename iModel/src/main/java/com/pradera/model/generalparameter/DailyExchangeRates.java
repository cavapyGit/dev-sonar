package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="DAILY_EXCHANGE_RATES")
public class DailyExchangeRates implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="DAILY_EXCHANGERATES_DAILY_EXCHANGE_PK_GENERATOR", sequenceName="sq_dailyexchagerate",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DAILY_EXCHANGERATES_DAILY_EXCHANGE_PK_GENERATOR")
	
	@Column(name="ID_DAILY_EXCHANGE_PK")
	private Integer iddailyExchangepk;
	
	@Temporal( TemporalType.DATE)
	@Column(name="DATE_RATE")
	private Date dateRate;
	
	@Column(name="ID_CURRENCY")
	private Integer idCurrency;
	
	@Transient
	private String currencyDescription;
	
	@Column(name="ID_SOURCE_INFORMATION")
	private Integer idSourceinformation;

	@Column(name="SELL_PRICE")
	private BigDecimal sellPrice;

	@Column(name="BUY_PRICE")
	private BigDecimal buyPrice;

	@Column(name="AVERAGE_PRICE")
	private BigDecimal averagePrice;
	
	@Column(name="IND_REFERENCE_PRICE")
	private Integer indReferencePrice;
	
	@Column(name="REFERENCE_PRICE")
	private BigDecimal referencePrice;

	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	/**
	 * @return the iddailyExchangepk
	 */
	public Integer getIddailyExchangepk() {
		return iddailyExchangepk;
	}
	/**
	 * @param iddailyExchangepk the iddailyExchangepk to set
	 */
	public void setIddailyExchangepk(Integer iddailyExchangepk) {
		this.iddailyExchangepk = iddailyExchangepk;
	}
	/**
	 * @return the dateRate
	 */
	public Date getDateRate() {
		return dateRate;
	}
	/**
	 * @param dateRate the dateRate to set
	 */
	public void setDateRate(Date dateRate) {
		this.dateRate = dateRate;
	}
	/**
	 * @return the idCurrency
	 */
	public Integer getIdCurrency() {
		return idCurrency;
	}

	public Integer getIndReferencePrice() {
		return indReferencePrice;
	}
	public void setIndReferencePrice(Integer indReferencePrice) {
		this.indReferencePrice = indReferencePrice;
	}
	public BigDecimal getReferencePrice() {
		return referencePrice;
	}
	public void setReferencePrice(BigDecimal referencePrice) {
		this.referencePrice = referencePrice;
	}
	/**
	 * @param idCurrency the idCurrency to set
	 */
	public void setIdCurrency(Integer idCurrency) {
		this.idCurrency = idCurrency;
	}
	/**
	 * @return the idSourceinformation
	 */
	public Integer getIdSourceinformation() {
		return idSourceinformation;
	}
	/**
	 * @param idSourceinformation the idSourceinformation to set
	 */
	public void setIdSourceinformation(Integer idSourceinformation) {
		this.idSourceinformation = idSourceinformation;
	}
	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	/**
	 * @return the sellPrice
	 */
	public BigDecimal getSellPrice() {
		return sellPrice;
	}
	/**
	 * @param sellPrice the sellPrice to set
	 */
	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}
	/**
	 * @return the buyPrice
	 */
	public BigDecimal getBuyPrice() {
		return buyPrice;
	}
	/**
	 * @param buyPrice the buyPrice to set
	 */
	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}
	/**
	 * @return the averagePrice
	 */
	public BigDecimal getAveragePrice() {
		return averagePrice;
	}
	/**
	 * @param averagePrice the averagePrice to set
	 */
	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}
    
    public String getCurrencyDescription() {
		return currencyDescription;
	}
    
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
