package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum  PeriodType{

	WEEKLY(Integer.valueOf(751),"SEMANAL"),
	BIWEEKLY(Integer.valueOf(752),"QUINCENAL"),
	MONTHLY(Integer.valueOf(753),"MENSUAL"),
	BIMONTHLY(Integer.valueOf(754),"BIMESTRAL"),
	QUARTERLY(Integer.valueOf(755),"TRIMESTRAL"),
	FOURMONTHLY(Integer.valueOf(756),"CUATRIMESTRAL"),
	BIANNUAL(Integer.valueOf(757),"SEMESTRAL"),
	ANNUAL(Integer.valueOf(758),"ANUAL");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private PeriodType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<PeriodType> list = new ArrayList<PeriodType>();
	public static final Map<Integer, PeriodType> lookup = new HashMap<Integer, PeriodType>();
	public static final Map<String, PeriodType> lookupValue = new HashMap<String, PeriodType>();
	static {
		for (PeriodType p : EnumSet.allOf(PeriodType.class)) {
			list.add(p);
			lookup.put(p.getCode(), p);
			lookupValue.put(p.getValue(), p);
		}
	}
	public static PeriodType get(Integer code) {
		return lookup.get(code);
	}
	
	public static PeriodType getWitgDescription(String description) {
		return lookupValue.get(description);
	}

}
