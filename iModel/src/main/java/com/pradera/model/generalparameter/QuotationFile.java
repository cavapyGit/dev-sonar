package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.NegotiationMechanism;


/**
 * The persistent class for the QUOTATION_FILE database table.
 * 
 */
@Entity
@Table(name="QUOTATION_FILE")
public class QuotationFile implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="QUOTATION_FILE_PK_GENERATOR", sequenceName="SQ_ID_QUOTATION_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="QUOTATION_FILE_PK_GENERATOR")
	@Column(name="ID_QUOTATION_FILE_PK")
	private long idQuotationFilePk;

	@Column(name="FILE_NAME")
	private String fileName;

	//bi-directional many-to-one association to QuotationFile
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK")
	private NegotiationMechanism negotiationMechanism;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.DATE)
	@Column(name="PROCESS_DATE")
	private Date processDate;

    @Lob()
	@Column(name="PROCESSED_FILE")
    @Basic(fetch=FetchType.LAZY)
	private byte[] processedFile;

	@Column(name="TOTAL_RECORDS")
	private BigDecimal totalRecords;

	//bi-directional many-to-one association to Quotation
	@OneToMany(mappedBy="quotationFile")
	private List<Quotation> quotations;

    public QuotationFile() {
    }

	public long getIdQuotationFilePk() {
		return this.idQuotationFilePk;
	}

	public void setIdQuotationFilePk(long idQuotationFilePk) {
		this.idQuotationFilePk = idQuotationFilePk;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public NegotiationMechanism getNegotiationMechanism() {
		return negotiationMechanism;
	}

	public void setNegotiationMechanism(NegotiationMechanism negotiationMechanism) {
		this.negotiationMechanism = negotiationMechanism;
	}

	
	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public byte[] getProcessedFile() {
		return this.processedFile;
	}

	public void setProcessedFile(byte[] processedFile) {
		this.processedFile = processedFile;
	}

	public BigDecimal getTotalRecords() {
		return this.totalRecords;
	}

	public void setTotalRecords(BigDecimal totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<Quotation> getQuotations() {
		return this.quotations;
	}

	public void setQuotations(List<Quotation> quotations) {
		this.quotations = quotations;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>(); 
		detailsMap.put("quotations", quotations);
        return detailsMap;
	}
	
}