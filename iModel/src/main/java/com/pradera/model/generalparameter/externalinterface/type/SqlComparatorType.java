package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum SqlComparetorType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 13-nov-2013
 */
public enum SqlComparatorType {
	
	/** The eq. */
	EQ(Integer.valueOf(1616),"="),

	/** The dif. */
	DIF(Integer.valueOf(1617),"<>"),
	
	/** The in. */
	IN(Integer.valueOf(1618),"IN"),
	
	/** The greather. */
	GREATHER(Integer.valueOf(1619),">"),
	
	/** The greather eq. */
	GREATHER_EQ(Integer.valueOf(1620),">="),
	
	/** The less. */
	LESS(Integer.valueOf(1621),"<"),
	
	/** The less eq. */
	LESS_EQ(Integer.valueOf(1622),"<="),
	
	/** The exists. */
	EXISTS(Integer.valueOf(1623),"EXISTS"),
	
	/** The not in. */
	NOT_IN(Integer.valueOf(1624),"NOT IN"),
	
	/** The between. */
	BETWEEN(Integer.valueOf(1625),"LIKE");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<SqlComparatorType> list=new ArrayList<SqlComparatorType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, SqlComparatorType> lookup=new HashMap<Integer, SqlComparatorType>();

	static {
		for(SqlComparatorType c : SqlComparatorType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SqlComparatorType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the sql comparator type
	 */
	public static SqlComparatorType get(Integer code) {
		return lookup.get(code);
	}
	
}