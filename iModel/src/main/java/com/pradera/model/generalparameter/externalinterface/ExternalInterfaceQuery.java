package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE_QUERY database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_QUERY")
public class ExternalInterfaceQuery implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id interface query pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_QUERY_IDINTERFACEQUERYPK_GENERATOR", sequenceName="SQ_ID_INTERFACE_QUERY_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_QUERY_IDINTERFACEQUERYPK_GENERATOR")
	@Column(name="ID_INTERFACE_QUERY_PK", unique=true, nullable=false, precision=10)
	private Long idInterfaceQueryPk;

	/** The external interface. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_FK", nullable=false)
	private ExternalInterface externalInterface;

	/** The interface template detail. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TEMPLATE_DETAIL_FK", nullable=false)
	private ExtInterfaceTemplateDetail interfaceTemplateDetail;

	/** The interface query state. */
	@Column(name="INTERFACE_QUERY_STATE", nullable=false, precision=10)
	private Integer interfaceQueryState;

	/** The interface template query. */
	@Column(name="INTERFACE_TEMPLATE_QUERY", nullable=false, length=4000)
	private String interfaceTemplateQuery;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;
    
    /** The ind query generated. */
    @Column(name="IND_QUERY_GENERATED", nullable=false, length=1)
    private Integer indQueryGenerated;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

    /** The register date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE", nullable=false)
	private Date registerDate;

	/** The register user. */
	@Column(name="REGISTER_USER", nullable=false, length=20)
	private String registerUser;

	//bi-directional many-to-one association to ExternalInterfaceQuery
	/** The external interface query. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERFACE_QUERY_REF")
	private ExternalInterfaceQuery externalInterfaceQuery;

	//bi-directional many-to-one association to ExternalInterfaceQuery
	/** The external interface queries. */
	@OneToMany(mappedBy="externalInterfaceQuery", cascade=CascadeType.ALL)
	private List<ExternalInterfaceQuery> externalInterfaceQueries;

	//bi-directional many-to-one association to ExternalInterfaceTable
	/** The external interface tables. */
	@OneToMany(mappedBy="externalInterfaceQuery", cascade=CascadeType.ALL)
	private List<ExternalInterfaceTable> externalInterfaceTables;

	/** The external interface filters. */
	@Transient
	private List<ExternalInterfaceFilter> externalInterfaceFilters;
	
	/** The data of query generated. */
	@Transient
	private List<List<Object[]>> dataOfQueryGenerated;
	
    /**
     * The Constructor.
     */
    public ExternalInterfaceQuery() {
    }

	/**
	 * Gets the id interface query pk.
	 *
	 * @return the id interface query pk
	 */
	public Long getIdInterfaceQueryPk() {
		return idInterfaceQueryPk;
	}

	/**
	 * Sets the id interface query pk.
	 *
	 * @param idInterfaceQueryPk the id interface query pk
	 */
	public void setIdInterfaceQueryPk(Long idInterfaceQueryPk) {
		this.idInterfaceQueryPk = idInterfaceQueryPk;
	}

	/**
	 * Gets the external interface.
	 *
	 * @return the external interface
	 */
	public ExternalInterface getExternalInterface() {
		return externalInterface;
	}

	/**
	 * Sets the external interface.
	 *
	 * @param externalInterface the external interface
	 */
	public void setExternalInterface(ExternalInterface externalInterface) {
		this.externalInterface = externalInterface;
	}

	/**
	 * Gets the interface template detail.
	 *
	 * @return the interface template detail
	 */
	public ExtInterfaceTemplateDetail getInterfaceTemplateDetail() {
		return interfaceTemplateDetail;
	}

	/**
	 * Sets the interface template detail.
	 *
	 * @param interfaceTemplateDetail the interface template detail
	 */
	public void setInterfaceTemplateDetail(
			ExtInterfaceTemplateDetail interfaceTemplateDetail) {
		this.interfaceTemplateDetail = interfaceTemplateDetail;
	}

	/**
	 * Gets the interface query state.
	 *
	 * @return the interface query state
	 */
	public Integer getInterfaceQueryState() {
		return interfaceQueryState;
	}

	/**
	 * Sets the interface query state.
	 *
	 * @param interfaceQueryState the interface query state
	 */
	public void setInterfaceQueryState(Integer interfaceQueryState) {
		this.interfaceQueryState = interfaceQueryState;
	}

	/**
	 * Gets the interface template query.
	 *
	 * @return the interface template query
	 */
	public String getInterfaceTemplateQuery() {
		return interfaceTemplateQuery;
	}

	/**
	 * Sets the interface template query.
	 *
	 * @param interfaceTemplateQuery the interface template query
	 */
	public void setInterfaceTemplateQuery(String interfaceTemplateQuery) {
		this.interfaceTemplateQuery = interfaceTemplateQuery;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the register user.
	 *
	 * @return the register user
	 */
	public String getRegisterUser() {
		return registerUser;
	}

	/**
	 * Sets the register user.
	 *
	 * @param registerUser the register user
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	/**
	 * Gets the external interface query.
	 *
	 * @return the external interface query
	 */
	public ExternalInterfaceQuery getExternalInterfaceQuery() {
		return externalInterfaceQuery;
	}

	/**
	 * Sets the external interface query.
	 *
	 * @param externalInterfaceQuery the external interface query
	 */
	public void setExternalInterfaceQuery(ExternalInterfaceQuery externalInterfaceQuery) {
		this.externalInterfaceQuery = externalInterfaceQuery;
	}

	/**
	 * Gets the external interface queries.
	 *
	 * @return the external interface queries
	 */
	public List<ExternalInterfaceQuery> getExternalInterfaceQueries() {
		return externalInterfaceQueries;
	}

	/**
	 * Sets the external interface queries.
	 *
	 * @param externalInterfaceQueries the external interface queries
	 */
	public void setExternalInterfaceQueries(List<ExternalInterfaceQuery> externalInterfaceQueries) {
		this.externalInterfaceQueries = externalInterfaceQueries;
	}

	/**
	 * Gets the external interface tables.
	 *
	 * @return the external interface tables
	 */
	public List<ExternalInterfaceTable> getExternalInterfaceTables() {
		return externalInterfaceTables;
	}

	/**
	 * Sets the external interface tables.
	 *
	 * @param externalInterfaceTables the external interface tables
	 */
	public void setExternalInterfaceTables(List<ExternalInterfaceTable> externalInterfaceTables) {
		this.externalInterfaceTables = externalInterfaceTables;
	}

	/**
	 * Gets the ind query generated.
	 *
	 * @return the ind query generated
	 */
	public Integer getIndQueryGenerated() {
		return indQueryGenerated;
	}

	/**
	 * Sets the ind query generated.
	 *
	 * @param indQueryGenerated the ind query generated
	 */
	public void setIndQueryGenerated(Integer indQueryGenerated) {
		this.indQueryGenerated = indQueryGenerated;
	}

	/**
	 * Gets the external interface filters.
	 *
	 * @return the external interface filters
	 */
	public List<ExternalInterfaceFilter> getExternalInterfaceFilters() {
		return externalInterfaceFilters;
	}

	/**
	 * Sets the external interface filters.
	 *
	 * @param externalInterfaceFilters the new external interface filters
	 */
	public void setExternalInterfaceFilters(
			List<ExternalInterfaceFilter> externalInterfaceFilters) {
		this.externalInterfaceFilters = externalInterfaceFilters;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();		
//		detailsMap.put("externalInterfaceQueries", externalInterfaceQueries);
		detailsMap.put("externalInterfaceTables", externalInterfaceTables);
		return detailsMap;
	}

	/**
	 * Gets the data of query generated.
	 *
	 * @return the data of query generated
	 */
	public List<List<Object[]>> getDataOfQueryGenerated() {
		return dataOfQueryGenerated;
	}

	/**
	 * Sets the data of query generated.
	 *
	 * @param dataOfQueryGenerated the new data of query generated
	 */
	public void setDataOfQueryGenerated(List<List<Object[]>> dataOfQueryGenerated) {
		this.dataOfQueryGenerated = dataOfQueryGenerated;
	}
}