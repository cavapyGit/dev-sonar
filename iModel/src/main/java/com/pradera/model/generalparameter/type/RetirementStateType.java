package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RetirementStateType {
    
    REGISTERED(Integer.valueOf(1),"REGISTRADO"),         //REGISTRADO
    REJECTED(Integer.valueOf(2),"RECHAZADO"),                   //RECHAZADO
    CONFIRMED(Integer.valueOf(3),"CONFIRMADO"),                 //CONFIRMADO
    
    ;
    
    private Integer code;
    private String value;

    // constructor
    private RetirementStateType(Integer code, String value) {
          this.code = code;
          this.value = value;
    }

    public static final List<RetirementStateType> lst = new ArrayList<RetirementStateType>();
    public static final Map<Integer, RetirementStateType> lookup = new HashMap<Integer, RetirementStateType>();
    static {
          for (RetirementStateType x : EnumSet.allOf(RetirementStateType.class)) {
                 lst.add(x);
                 lookup.put(x.getCode(), x);
          }
    }

    public Integer getCode() {
          return code;
    }

    public void setCode(Integer code) {
          this.code = code;
    }

    public String getValue() {
          return value;
    }

    public void setValue(String value) {
          this.value = value;
    }
}