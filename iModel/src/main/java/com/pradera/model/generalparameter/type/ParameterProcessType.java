package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ParameterProcessType {

	SECURITIES_TRANSFER (new Long(836), "TRASPASO DE VALORES"),
	SECURITIES_BLOCK (new Long(837), "BLOQUEO DE VALORES"),
	SECURITIES_UNBLOCK (new Long(838), "DESBLOQUEO DE VALORES"),
	SECURITIES_ACCREDITATION (new Long(839), "ACREDITACION DE VALORES"),
	CHANGE_OWNERSHIP_SECURITIES (new Long(840), "CAMBIO DE TITULARIDAD"),
	UNION_PARTICIPANT (new Long(841), "UNIFICACION DE PARTICIPANTES"),
	SECURITIES_DEMATERIALIZATION (new Long(842), "DESMATERIALIZACION DE VALORES"),
	SECURITIES_RETIREMENT (new Long(843), "RETIRO DE VALORES"),
	RECTIFICATION_OPERATIONS (new Long(844), "RECTIFICACION DE OPERACIONES"),
	PHYSICAL_DEPOSIT_RETIREMENT (new Long(845), "DEPOSITO Y RETIRO DE FISICOS"),
	NEGOTIATION_MECHANISM (new Long(846), "NEGOCIACION DE MECANISMOS"),
	NEGOTIATION_INTERNATIONAL (new Long(847), "NEGOCIACION INTERNACIONAL"),
	REPORT_GUARANTIES (new Long(848), "GARANTIAS DE REPORTO"),
	CORPORATIVE_PROCESSES (new Long(849), "PROCESOS CORPORATIVOS"),
	COUPON_GRANT_BLOCK (new Long(1705), "BLOQUEO DE CESION DE CUPON"),
	COUPON_GRANT_UNBLOCK (new Long(1727), "DESBLOQUEO DE CESION DE CUPON"),
	COUPON_DETACHMENT (new Long(2305), "DESPRENDIMIENTO DE CUPON");
	
	
	private Long code;
	private String description;
	
	
	private ParameterProcessType(Long code, String description) {
		this.code = code;
		this.description = description;
	}


	public Long getCode() {
		return code;
	}


	public void setCode(Long code) {
		this.code = code;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public static final List<ParameterProcessType> list = new ArrayList<ParameterProcessType>();
	public static final Map<Long, ParameterProcessType> lookup = new HashMap<Long, ParameterProcessType>();
	static {
		for (ParameterProcessType s : EnumSet.allOf(ParameterProcessType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
