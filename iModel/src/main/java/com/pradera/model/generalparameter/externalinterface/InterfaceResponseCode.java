package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@Entity
@NamedQueries({
	@NamedQuery(
			name=InterfaceResponseCode.FIND_ALL, 
			query="select irc from InterfaceResponseCode irc")
})
@Table(name="INTERFACE_RESPONSE_CODE")
public class InterfaceResponseCode implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL = "findAll";
	
	/** The id ext interface pk. */
	@Id
	@SequenceGenerator(name="INTERFACE_RESPONSE_CODEPK_GENERATOR", sequenceName="SQ_ID_INTERFACE_RESP_CODE_PK" , initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INTERFACE_RESPONSE_CODEPK_GENERATOR")
	@Column(name="ID_INTERFACE_RESP_CODE_PK")
	private Long idInterfaceRespCodePk;

	/** The description. */
	@Column(name="RESPONSE_DESCRIPTION")
	private String responseDescription;

	/** The execution type. */
	@Column(name="COMMENTS")
	private String comments;

	/** The external interface state. */
	@Column(name="RESPONSE_CODE")
	private String responseCode;
	
	@Column(name="RESPONSE_CODE_DPF")
	private String responseCodeDpf;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false)
	private String lastModifyUser;

    /**
     * The Constructor.
     */
    public InterfaceResponseCode() {
    }

	public Long getIdInterfaceRespCodePk() {
		return idInterfaceRespCodePk;
	}



	public void setIdInterfaceRespCodePk(Long idInterfaceRespCodePk) {
		this.idInterfaceRespCodePk = idInterfaceRespCodePk;
	}



	public String getResponseDescription() {
		return responseDescription;
	}



	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}



	public String getComments() {
		return comments;
	}



	public void setComments(String comments) {
		this.comments = comments;
	}



	public String getResponseCode() {
		return responseCode;
	}



	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}



	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	public String getLastModifyIp() {
		return lastModifyIp;
	}



	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	public String getLastModifyUser() {
		return lastModifyUser;
	}



	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();		
		return detailsMap;
	}

	public String getResponseCodeDpf() {
		return responseCodeDpf;
	}

	public void setResponseCodeDpf(String responseCodeDpf) {
		this.responseCodeDpf = responseCodeDpf;
	}

}