package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum GeographicLocationType {
	
	COUNTRY(Integer.valueOf(1),"PAIS"),
	DEPARTMENT(Integer.valueOf(2),"DEPARTAMENTO"),
	PROVINCE(Integer.valueOf(3),"CIUDAD"),
	DISTRICT(Integer.valueOf(4),"BARRIO");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private GeographicLocationType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static GeographicLocationType get(Integer code) {
		return lookup.get(code);
	}

	public static final List<GeographicLocationType> list = new ArrayList<GeographicLocationType>();
	public static final Map<Integer, GeographicLocationType> lookup = new HashMap<Integer, GeographicLocationType>();
	static {
		for (GeographicLocationType s : EnumSet.allOf(GeographicLocationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
