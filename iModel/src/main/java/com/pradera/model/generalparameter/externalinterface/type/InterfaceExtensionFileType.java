package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterfaceExtensionFileType {
	
	/** The cvs. */
	CVS(Integer.valueOf(4),"CVS",".xls"),
	
	/** The xml. */
	XML(Integer.valueOf(5),"XML",".xml"),
	
	/** The txt. */
	TXT(Integer.valueOf(6),"TXT",".txt");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	private String prefix;
	
	/** The Constant list. */
	public final static List<ExtensionFileStateType> list=new ArrayList<ExtensionFileStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, ExtensionFileStateType> lookup=new HashMap<Integer, ExtensionFileStateType>();

	static {
		for(ExtensionFileStateType c : ExtensionFileStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceExtensionFileType(Integer code, String value, String prefix){
		this.code=code;
		this.value=value;
		this.prefix = prefix;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the prefix.
	 *
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * Sets the prefix.
	 *
	 * @param prefix the new prefix
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}