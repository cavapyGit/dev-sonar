package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum  QuotationStateType{

	ACTIVE(Integer.valueOf(1755),"ACTIVO"),
	INACTIVE(Integer.valueOf(1756),"INACTIVO");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private QuotationStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<QuotationStateType> list = new ArrayList<QuotationStateType>();
	public static final Map<Integer, QuotationStateType> lookup = new HashMap<Integer, QuotationStateType>();
	static {
		for (QuotationStateType i : EnumSet.allOf(QuotationStateType.class)) {
			list.add(i);
			lookup.put(i.getCode(), i);
		}
	}

}
