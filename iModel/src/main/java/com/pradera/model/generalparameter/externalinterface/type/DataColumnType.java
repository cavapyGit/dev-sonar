package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ExternalInterfaceExecutionType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
public enum DataColumnType {

	/** The timestamp. */
	TIMESTAMP(Integer.valueOf(1579),"TIMESTAMP"),
	
	/** The boot. */
	NUMBER(Integer.valueOf(1580),"NUMBER"),
	
	/** The char. */
	CHAR(Integer.valueOf(1581),"CHAR"),
	
	/** The date. */
	DATE(Integer.valueOf(1582),"DATE"),
	
	/** The VARCHA r2. */
	VARCHAR2(Integer.valueOf(1583),"VARCHAR2"),
	
	/** The blob. */
	BLOB(Integer.valueOf(1584),"BLOB");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<DataColumnType> list=new ArrayList<DataColumnType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, DataColumnType> lookup=new HashMap<Integer, DataColumnType>();

	static {
		for(DataColumnType c : DataColumnType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private DataColumnType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the data column type
	 */
	public static DataColumnType get(Integer codigo) {
		return lookup.get(codigo);
	}
}