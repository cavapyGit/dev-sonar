package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterfaceTemplateDetailType {
	
	/** The header. */
	HEADER(Integer.valueOf(1558),"CABECERA"),
	
	/** The detail. */
	DETAIL(Integer.valueOf(1559),"DETALLE");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new interface template detail type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceTemplateDetailType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/** The Constant list. */
	public final static List<InterfaceTemplateDetailType> list=new ArrayList<InterfaceTemplateDetailType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceTemplateDetailType> lookup=new HashMap<Integer, InterfaceTemplateDetailType>();

	static {
		for(InterfaceTemplateDetailType d : InterfaceTemplateDetailType.values()){
			lookup.put(d.getCode(), d);
			list.add( d );
		}
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
