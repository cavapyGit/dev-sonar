package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum  QuotationDeleteMotiveType{

	MOTIVE1(Integer.valueOf(1757),"MOTIVO 1"),
	MOTIVE2(Integer.valueOf(1758),"MOTIVO 2"),
	OTHER_MOTIVE(Integer.valueOf(1759),"OTRO MOTIVO");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private QuotationDeleteMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<QuotationDeleteMotiveType> list = new ArrayList<QuotationDeleteMotiveType>();
	public static final Map<Integer, QuotationDeleteMotiveType> lookup = new HashMap<Integer, QuotationDeleteMotiveType>();
	static {
		for (QuotationDeleteMotiveType i : EnumSet.allOf(QuotationDeleteMotiveType.class)) {
			list.add(i);
			lookup.put(i.getCode(), i);
		}
	}

}
