package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE_COLUMN database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_COLUMN")
public class ExternalInterfaceColumn implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interface col pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_COLUMN_IDEXTINTERFACECOLPK_GENERATOR", sequenceName="SQ_ID_EXT_INTERFACE_COL_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_COLUMN_IDEXTINTERFACECOLPK_GENERATOR")
	@Column(name="ID_EXT_INTERFACE_COL_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfaceColPk;

	/** The column name. */
	@Column(name="COLUMN_NAME", nullable=false, length=50)
	private String columnName;

	/** The data type origin. */
	@Column(name="DATA_TYPE_ORIGIN", nullable=false)
	private Integer dataTypeOrigin;
		
	/** The data type target. */
	@Column(name="DATA_TYPE_TARGET", nullable=false, length=20)
	private Integer dataTypeTarget;

	/** The decimal quantity. */
	@Column(name="DECIMAL_QUANTITY", precision=10)
	private Integer decimalQuantity;

	/** The decimal separator. */
	@Column(name="DECIMAL_SEPARATOR", length=20)
	private String decimalSeparator;

	/** The default value. */
	@Column(name="DEFAULT_VALUE", nullable=false, length=20)
	private String defaultValue;

	/** The default value missing. */
	@Column(name="DEFAULT_VALUE_MISSING", nullable=true, length=1)
	private String defaultValueMissing;

	/** The format value. */
	@Column(name="FORMAT_VALUE", nullable=true)
	private Integer formatValue;

	/** The interface column state. */
	@Column(name="INTERFACE_COLUMN_STATE", precision=10)
	private Integer interfaceColumnState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

	/** The length. */
	@Column(name="\"LENGTH\"", nullable=false, precision=10)
	private Integer length;

	/** The order priority. */
	@Column(name="ORDER_PRIORITY", nullable=false, precision=10)
	private Integer orderPriority;

	/** The ind visible. */
	@Column(name="IND_VISIBLE", nullable=false, length=1)
	private Integer indVisible;
	
	/** The ind visible. */
	@Column(name="IND_PARAMETER_TABLE", nullable=false, length=1)
	private Integer indParameterTable;
	
    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	//bi-directional many-to-one association to ExternalInterfaceTable
	/** The external interface table. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_TAB_FK", nullable=false)
	private ExternalInterfaceTable externalInterfaceTable;
	
	/** The column name alias. */
	@Column(name="COLUMN_NAME_ALIAS", nullable=false, length=50)
	private String columnNameAlias;

	//bi-directional many-to-one association to ExternalInterfaceFileName
	/** The external interface file names. */
	@OneToMany(mappedBy="externalInterfaceColumn", cascade=CascadeType.ALL)
	private List<ExternalInterfaceFileName> externalInterfaceFileNames;

	//bi-directional many-to-one association to ExtInterfaceFilterPredicate
	/** The ext interface filter predicates. */
	@OneToMany(mappedBy="externalInterfaceColumn", cascade=CascadeType.ALL)
	private List<ExtInterfaceFilterPredicate> extInterfaceFilterPredicates;

	/** The is selected. */
	@Transient
	private Boolean isSelected;
	
	/** The type origin value. */
	@Transient
	private String typeOriginValue;
	
	/** The type target value. */
	@Transient
	private String typeTargetValue;
	
	/** The is parameter table. */
	@Transient
	private Boolean isParameterTable;
		
    /**
     * The Constructor.
     */
    public ExternalInterfaceColumn() {
    }

	/**
	 * Gets the id ext interface col pk.
	 *
	 * @return the id ext interface col pk
	 */
	public Long getIdExtInterfaceColPk() {
		return idExtInterfaceColPk;
	}

	/**
	 * Sets the id ext interface col pk.
	 *
	 * @param idExtInterfaceColPk the id ext interface col pk
	 */
	public void setIdExtInterfaceColPk(Long idExtInterfaceColPk) {
		this.idExtInterfaceColPk = idExtInterfaceColPk;
	}

	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * Sets the column name.
	 *
	 * @param columnName the column name
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * Gets the data type origin.
	 *
	 * @return the data type origin
	 */
	public Integer getDataTypeOrigin() {
		return dataTypeOrigin;
	}

	/**
	 * Sets the data type origin.
	 *
	 * @param dataTypeOrigin the data type origin
	 */
	public void setDataTypeOrigin(Integer dataTypeOrigin) {
		this.dataTypeOrigin = dataTypeOrigin;
	}

	/**
	 * Gets the data type target.
	 *
	 * @return the data type target
	 */
	public Integer getDataTypeTarget() {
		return this.dataTypeTarget;
	}

	/**
	 * Sets the data type target.
	 *
	 * @param dataTypeTarget the new data type target
	 */
	public void setDataTypeTarget(Integer dataTypeTarget) {
		this.dataTypeTarget = dataTypeTarget;
	}

	/**
	 * Gets the decimal quantity.
	 *
	 * @return the decimal quantity
	 */
	public Integer getDecimalQuantity() {
		return decimalQuantity;
	}

	/**
	 * Sets the decimal quantity.
	 *
	 * @param decimalQuantity the decimal quantity
	 */
	public void setDecimalQuantity(Integer decimalQuantity) {
		this.decimalQuantity = decimalQuantity;
	}

	/**
	 * Gets the decimal separator.
	 *
	 * @return the decimal separator
	 */
	public String getDecimalSeparator() {
		return decimalSeparator;
	}

	/**
	 * Sets the decimal separator.
	 *
	 * @param decimalSeparator the decimal separator
	 */
	public void setDecimalSeparator(String decimalSeparator) {
		this.decimalSeparator = decimalSeparator;
	}

	/**
	 * Gets the default value.
	 *
	 * @return the default value
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Sets the default value.
	 *
	 * @param defaultValue the default value
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * Gets the default value missing.
	 *
	 * @return the default value missing
	 */
	public String getDefaultValueMissing() {
		return defaultValueMissing;
	}

	/**
	 * Sets the default value missing.
	 *
	 * @param defaultValueMissing the default value missing
	 */
	public void setDefaultValueMissing(String defaultValueMissing) {
		this.defaultValueMissing = defaultValueMissing;
	}

	/**
	 * Gets the format value.
	 *
	 * @return the format value
	 */
	public Integer getFormatValue() {
		return formatValue;
	}

	/**
	 * Sets the format value.
	 *
	 * @param formatValue the new format value
	 */
	public void setFormatValue(Integer formatValue) {
		this.formatValue = formatValue;
	}

	/**
	 * Gets the interface column state.
	 *
	 * @return the interface column state
	 */
	public Integer getInterfaceColumnState() {
		return interfaceColumnState;
	}

	/**
	 * Sets the interface column state.
	 *
	 * @param interfaceColumnState the interface column state
	 */
	public void setInterfaceColumnState(Integer interfaceColumnState) {
		this.interfaceColumnState = interfaceColumnState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the order priority.
	 *
	 * @return the order priority
	 */
	public Integer getOrderPriority() {
		return orderPriority;
	}

	/**
	 * Sets the order priority.
	 *
	 * @param orderPriority the order priority
	 */
	public void setOrderPriority(Integer orderPriority) {
		this.orderPriority = orderPriority;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the external interface table.
	 *
	 * @return the external interface table
	 */
	public ExternalInterfaceTable getExternalInterfaceTable() {
		return externalInterfaceTable;
	}

	/**
	 * Sets the external interface table.
	 *
	 * @param externalInterfaceTable the external interface table
	 */
	public void setExternalInterfaceTable(
			ExternalInterfaceTable externalInterfaceTable) {
		this.externalInterfaceTable = externalInterfaceTable;
	}

	/**
	 * Gets the column name alias.
	 *
	 * @return the column name alias
	 */
	public String getColumnNameAlias() {
		return columnNameAlias;
	}

	/**
	 * Sets the column name alias.
	 *
	 * @param columnNameAlias the column name alias
	 */
	public void setColumnNameAlias(String columnNameAlias) {
		this.columnNameAlias = columnNameAlias;
	}

	/**
	 * Gets the external interface file names.
	 *
	 * @return the external interface file names
	 */
	public List<ExternalInterfaceFileName> getExternalInterfaceFileNames() {
		return externalInterfaceFileNames;
	}

	/**
	 * Sets the external interface file names.
	 *
	 * @param externalInterfaceFileNames the external interface file names
	 */
	public void setExternalInterfaceFileNames(
			List<ExternalInterfaceFileName> externalInterfaceFileNames) {
		this.externalInterfaceFileNames = externalInterfaceFileNames;
	}

	/**
	 * Gets the ext interface filter predicates.
	 *
	 * @return the ext interface filter predicates
	 */
	public List<ExtInterfaceFilterPredicate> getExtInterfaceFilterPredicates() {
		return extInterfaceFilterPredicates;
	}

	/**
	 * Sets the ext interface filter predicates.
	 *
	 * @param extInterfaceFilterPredicates the ext interface filter predicates
	 */
	public void setExtInterfaceFilterPredicates(List<ExtInterfaceFilterPredicate> extInterfaceFilterPredicates) {
		this.extInterfaceFilterPredicates = extInterfaceFilterPredicates;
	}

	/**
	 * Gets the length.
	 *
	 * @return the length
	 */
	public Integer getLength() {
		return length;
	}

	/**
	 * Sets the length.
	 *
	 * @param length the length
	 */
	public void setLength(Integer length) {
		this.length = length;
	}

	/**
	 * Gets the type origin value.
	 *
	 * @return the type origin value
	 */
	public String getTypeOriginValue() {
		return typeOriginValue;
	}

	/**
	 * Sets the type origin value.
	 *
	 * @param typeOriginValue the new type origin value
	 */
	public void setTypeOriginValue(String typeOriginValue) {
		this.typeOriginValue = typeOriginValue;
	}

	/**
	 * Gets the ind visible.
	 *
	 * @return the ind visible
	 */
	public Integer getIndVisible() {
		return indVisible;
	}

	/**
	 * Sets the ind visible.
	 *
	 * @param indVisible the new ind visible
	 */
	public void setIndVisible(Integer indVisible) {
		this.indVisible = indVisible;
	}

	/**
	 * Gets the checks if is selected.
	 *
	 * @return the checks if is selected
	 */
	public Boolean getIsSelected() {
		return isSelected;
	}

	/**
	 * Sets the checks if is selected.
	 *
	 * @param isSelected the new checks if is selected
	 */
	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}		

	/**
	 * Gets the type target value.
	 *
	 * @return the type target value
	 */
	public String getTypeTargetValue() {
		return typeTargetValue;
	}

	/**
	 * Sets the type target value.
	 *
	 * @param typeTargetValue the new type target value
	 */
	public void setTypeTargetValue(String typeTargetValue) {
		this.typeTargetValue = typeTargetValue;
	}

	/**
	 * Gets the ind parameter table.
	 *
	 * @return the ind parameter table
	 */
	public Integer getIndParameterTable() {
		return indParameterTable;
	}

	/**
	 * Sets the ind parameter table.
	 *
	 * @param indParameterTable the new ind parameter table
	 */
	public void setIndParameterTable(Integer indParameterTable) {
		this.indParameterTable = indParameterTable;
	}

	/**
	 * Gets the checks if is parameter table.
	 *
	 * @return the checks if is parameter table
	 */
	public Boolean getIsParameterTable() {
		return isParameterTable;
	}

	/**
	 * Sets the checks if is parameter table.
	 *
	 * @param isParameterTable the new checks if is parameter table
	 */
	public void setIsParameterTable(Boolean isParameterTable) {
		if(isParameterTable.equals(Boolean.TRUE)){
			indParameterTable = 1;
		}else{
			indParameterTable = 0;
		}
		this.isParameterTable = isParameterTable;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();		
		
		detailsMap.put("externalInterfaceFileNames", externalInterfaceFileNames);
		detailsMap.put("extInterfaceFilterPredicates", extInterfaceFilterPredicates);
		return detailsMap;
	}
	
	
}