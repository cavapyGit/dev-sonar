package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuer;

@Entity
@Table(name="CORRELATIVE")
public class Correlative implements Serializable, Auditable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2297560499201421653L;

	@Id
	@SequenceGenerator(name="CORRELATIVE_IDCERTIFICATEPK_GENERATOR", sequenceName="SQ_ID_CORRELATIVE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CORRELATIVE_IDCERTIFICATEPK_GENERATOR")
	@Column(name="ID_CORRELATIVE_PK")
	private Long idCorrelativePk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;
	
	@Column(name="CORRELATIVE_CD")
	private String correlativeCd;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CORRELATIVE_DATE")
	private Date correlativeDate;
	
	@Column(name="CORRELATIVE_NUMBER")
	private Integer correlativeNumber;
	
	@Column(name="CORRELATIVE_TEXT")
	private String correlativeText;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	
	
	public Correlative() {
		super();
	}



	public Long getIdCorrelativePk() {
		return idCorrelativePk;
	}



	public void setIdCorrelativePk(Long idCorrelativePk) {
		this.idCorrelativePk = idCorrelativePk;
	}



	public Participant getParticipant() {
		return participant;
	}



	public void setParticipant(Participant participant) {
		this.participant = participant;
	}



	public Issuer getIssuer() {
		return issuer;
	}



	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}



	public String getCorrelativeCd() {
		return correlativeCd;
	}



	public void setCorrelativeCd(String correlativeCd) {
		this.correlativeCd = correlativeCd;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public Date getCorrelativeDate() {
		return correlativeDate;
	}



	public void setCorrelativeDate(Date correlativeDate) {
		this.correlativeDate = correlativeDate;
	}



	public Integer getCorrelativeNumber() {
		return correlativeNumber;
	}



	public void setCorrelativeNumber(Integer correlativeNumber) {
		this.correlativeNumber = correlativeNumber;
	}

	

	public String getCorrelativeText() {
		return correlativeText;
	}



	public void setCorrelativeText(String correlativeText) {
		this.correlativeText = correlativeText;
	}



	public String getLastModifyUser() {
		return lastModifyUser;
	}



	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	public String getLastModifyIp() {
		return lastModifyIp;
	}



	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	
	

}
