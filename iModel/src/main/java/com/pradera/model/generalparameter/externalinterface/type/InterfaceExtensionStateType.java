package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterfaceExtensionStateType {

	REGISTERED(Integer.valueOf(1585),"REGISTRADO"),
	DELETED(Integer.valueOf(1586),"ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceExtensionStateType> list=new ArrayList<InterfaceExtensionStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceExtensionStateType> lookup=new HashMap<Integer, InterfaceExtensionStateType>();

	static {
		for(InterfaceExtensionStateType c : InterfaceExtensionStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	private InterfaceExtensionStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
