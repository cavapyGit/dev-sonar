package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BillingCorrelativeType {

	/** Correlativo del servicio */
	SERVICE_CODE_COR(Integer.valueOf(1),"SERVICE_CODE_COR"),
	/** Correlativo de Factura */
	BILL_CODE_COR(Integer.valueOf(2),"BILL_CODE_COR"),
	/** Correlativo NCF */
	BILL_CODE_NCF_COR(Integer.valueOf(3),"BILL_CODE_NCF_COR");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<BillingCorrelativeType> list=new ArrayList<BillingCorrelativeType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, BillingCorrelativeType> lookup=new HashMap<Integer, BillingCorrelativeType>();

	static {
		for(BillingCorrelativeType c : BillingCorrelativeType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	private BillingCorrelativeType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
