package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.generalparameter.type.FinancialIndicatorStateType;
import com.pradera.model.generalparameter.type.PeriodType;

// TODO: Auto-generated Javadoc
/**
 * The Class FinancialIndicator.
 *
 * @author PraderaTechnologies
 */
@Entity
@Table(name = "FINANCIAL_INDICATOR")
public class FinancialIndicator implements Serializable,Auditable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id financial indicator pk. */
	@Id
	@SequenceGenerator(name = "FINANCIAL_PK_GENERATOR", sequenceName = "SQ_FINANCIAL_PK_GENERATOR", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FINANCIAL_PK_GENERATOR")
	@Column(name = "ID_FINANCIAL_INDICATOR_PK")
	private Integer idFinancialIndicatorPk;

	/** The indicator code. */
	@Column(name = "INDICATOR_CODE")
	private Integer indicatorCode;
	
	/** The indicator type. */
	@Column(name = "INDICATOR_TYPE")
	private Integer indicatorType;

	/** The initial date. */
	@Column(name = "INITIAL_DATE")
	private Date initialDate;

	/** The end date. */
	@Column(name = "END_DATE")
	private Date endDate;

	/** The id source info. */
	@Column(name = "ID_SOURCE_INFORMATION")
	private Integer idSourceInfo;	
	
	/** The periodicity type. */
	@Column(name = "PERIODICITY_TYPE")
	private Integer periodicityType;
	
	/** The rate value. */
	@Column(name = "RATE_VALUE")
	private BigDecimal rateValue;
	
	/** The observations. */
	@Column(name = "COMMENTS")
	private String observations;
	
	/** The finantial state. */
	@Column(name = "FINANTIAL_STATE")
	private Integer finantialState;
	
	/** The registry date. */
	@Column(name = "REGISTRY_DATE")
	private Date registryDate;

	/** The update date. */
	@Column(name = "UPDATE_DATE")
	private Date updateDate;
	
	/** The last modify date. */
	@NotNull
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@NotNull
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@NotNull
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The last modify app. */
	@NotNull
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The indicator description. */
	@Transient
	private String indicatorDescription;
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {

        if (loggerUser != null) {
            lastModifyApp  = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp   = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	
	/**
	 * Gets the indicator description.
	 *
	 * @return the indicatorDescription
	 */
	public String getIndicatorDescription() {
		return indicatorDescription;
	}

	/**
	 * Sets the indicator description.
	 *
	 * @param indicatorDescription the indicatorDescription to set
	 */
	public void setIndicatorDescription(String indicatorDescription) {
		if(indicatorDescription != null){
		indicatorDescription = indicatorDescription.toUpperCase();
		}
		this.indicatorDescription = indicatorDescription;
	}

	/**
	 * Gets the id financial indicator pk.
	 *
	 * @return the idFinancialIndicatorPk
	 */
	public Integer getIdFinancialIndicatorPk() {
		return idFinancialIndicatorPk;
	}

	/**
	 * Sets the id financial indicator pk.
	 *
	 * @param idFinancialIndicatorPk the idFinancialIndicatorPk to set
	 */
	public void setIdFinancialIndicatorPk(Integer idFinancialIndicatorPk) {
		this.idFinancialIndicatorPk = idFinancialIndicatorPk;
	}

	/**
	 * Gets the indicator type.
	 *
	 * @return the indicatorType
	 */
	public Integer getIndicatorType() {
		return indicatorType;
	}

	/**
	 * Sets the indicator type.
	 *
	 * @param indicatorType the indicatorType to set
	 */
	public void setIndicatorType(Integer indicatorType) {
		this.indicatorType = indicatorType;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
			this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		   this.endDate = endDate;
	}

	/**
	 * Gets the id source info.
	 *
	 * @return the idSourceInfo
	 */
	public Integer getIdSourceInfo() {
		return idSourceInfo;
	}

	/**
	 * Sets the id source info.
	 *
	 * @param idSourceInfo the idSourceInfo to set
	 */
	public void setIdSourceInfo(Integer idSourceInfo) {
		this.idSourceInfo = idSourceInfo;
	}

	/**
	 * Instantiates a new financial indicator.
	 */
	public FinancialIndicator() {

	}

	/**
	 * Gets the periodicity type.
	 *
	 * @return the periodicityType
	 */
	public Integer getPeriodicityType() {
		return periodicityType;
	}

	/**
	 * Sets the periodicity type.
	 *
	 * @param periodicityType the periodicityType to set
	 */
	public void setPeriodicityType(Integer periodicityType) {
		this.periodicityType = periodicityType;
	}

	/**
	 * Gets the rate value.
	 *
	 * @return the rateValue
	 */
	public BigDecimal getRateValue() {
		return rateValue;
	}

	/**
	 * Sets the rate value.
	 *
	 * @param rateValue the rateValue to set
	 */
	public void setRateValue(BigDecimal rateValue) {
		this.rateValue = rateValue;
	}

	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}

	/**
	 * Sets the observations.
	 *
	 * @param observations the observations to set
	 */
	public void setObservations(String observations) {
		if(observations != null){
			observations = observations.toUpperCase();
		}
		this.observations = observations;
	}

	/**
	 * Gets the finantial state.
	 *
	 * @return the finantialState
	 */
	public Integer getFinantialState() {
		return finantialState;
	}

	/**
	 * Sets the finantial state.
	 *
	 * @param finantialState the finantialState to set
	 */
	public void setFinantialState(Integer finantialState) {
		this.finantialState = finantialState;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Gets the update date.
	 *
	 * @return the update date
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the update date.
	 *
	 * @param updateDate the new update date
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * To get Periodicity Type Description.
	 *
	 * @return description
	 */
	public String getPeriodicityTypeDescription(){
		String descripcion = null;
		if(periodicityType != null){
			if(PeriodType.get(periodicityType)!=null)
				descripcion = PeriodType.get(periodicityType).getValue();
		}
		 return descripcion;
	}		

	/**
	 * Gets the indicator code.
	 *
	 * @return the indicator code
	 */
	public Integer getIndicatorCode() {
		return indicatorCode;
	}

	/**
	 * Sets the indicator code.
	 *
	 * @param indicatorCode the new indicator code
	 */
	public void setIndicatorCode(Integer indicatorCode) {
		this.indicatorCode = indicatorCode;
	}

	/**
	 * Gets the state type description.
	 *
	 * @return the state type description
	 */
	public String getStateTypeDescription(){
		String descripcion = null;
		if(finantialState != null){
			if(FinancialIndicatorStateType.get(finantialState)!=null)
				descripcion = FinancialIndicatorStateType.get(finantialState).getValue();
		}
		 return descripcion;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
