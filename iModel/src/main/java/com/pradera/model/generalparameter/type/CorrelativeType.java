package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum CorrelativeType {

	ISSUER_CODE_COR(Integer.valueOf(1),"ISSUER_CODE_COR"),
	CAT_CODE_COR(Integer.valueOf(2),"CAT_CODE_COR"),
	SEND_LIP_COR(Integer.valueOf(3),"SEND_LIP_COR");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<CorrelativeType> list=new ArrayList<CorrelativeType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, CorrelativeType> lookup=new HashMap<Integer, CorrelativeType>();

	static {
		for(CorrelativeType c : CorrelativeType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	private CorrelativeType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
