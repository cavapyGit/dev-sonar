package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterfaceTemplateStateType {

	/** The registered. */
	REGISTERED(Integer.valueOf(1570),"REGISTRADO"),
		
	/** The confirmed. */
	CONFIRMED (Integer.valueOf(1571), "CONFIRMADO"),	
	
	/** The blocked. */
	BLOCKED(Integer.valueOf(1572),"BLOQUEADO"),
	
	/** The annulled. */
	ANNULLED (Integer.valueOf(1573), "ANULADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceTemplateStateType> list=new ArrayList<InterfaceTemplateStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceTemplateStateType> lookup=new HashMap<Integer, InterfaceTemplateStateType>();

	static {
		for(InterfaceTemplateStateType c : InterfaceTemplateStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	private InterfaceTemplateStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
