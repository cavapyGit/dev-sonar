package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum SecurityGroupType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/05/2013
 */
public enum SecurityGroupType {
	
	/** The List 1. */
	LIST1(Integer.valueOf(1498),"Nómina 1",1),
	
	/** The List 2. */
	LIST2(Integer.valueOf(1499),"Nómina 2",2),
	
	/** The List 3. */
	LIST3(Integer.valueOf(1500),"Nómina 3",3),
	
	/** The List 4. */
	LIST4(Integer.valueOf(1501),"Nómina 4",4),
	
	
	/** The List 5. */
	LIST5(Integer.valueOf(1502),"Nómina 5",5);
	
	/** The Constant list. */
	public final static List<SecurityGroupType> list=new ArrayList<SecurityGroupType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, SecurityGroupType> lookup=new HashMap<Integer, SecurityGroupType>();
	
	static {
		for(SecurityGroupType sg : SecurityGroupType.values()){
			lookup.put(sg.getCode(), sg);
			list.add( sg );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/** The numeric. */
	private Integer numeric;
	
	/**
	 * Instantiates a new Security Group type.
	 *
	 * @param code the code
	 * @param value the value	 
	 */
	private SecurityGroupType(Integer code, String value){
		this.code=code;
		this.value=value;		
	}
	
	/**
	 * Instantiates a new security group type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param numeric the numeric
	 */
	private SecurityGroupType(Integer code, String value, Integer numeric) {
		this.code = code;
		this.value = value;
		this.numeric = numeric;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
	
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the numeric.
	 *
	 * @return the numeric
	 */
	public Integer getNumeric() {
		return numeric;
	}

	/**
	 * Sets the numeric.
	 *
	 * @param numeric the new numeric
	 */
	public void setNumeric(Integer numeric) {
		this.numeric = numeric;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the Security Group type
	 */
	public static SecurityGroupType get(Integer code) {
		return lookup.get(code);
	}
	
}
