package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the EXTERNAL_INTERFACE_TEMPLATE database table.
 * 
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_TEMPLATE")
public class ExternalInterfaceTemplate implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_TEMPLATE_IDEXTINFERFACETEMPLATEPK_GENERATOR", sequenceName="SQ_ID_INFERFACE_TEMPLATE_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_TEMPLATE_IDEXTINFERFACETEMPLATEPK_GENERATOR")
	@Column(name="ID_EXT_INFERFACE_TEMPLATE_PK", unique=true, nullable=false, precision=10)
	private Long idExtInferfaceTemplatePk;

	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE", nullable=false)
	private Date registerDate;

	@Column(name="REGISTER_USER", nullable=false, length=20)
	private String registerUser;

	@Column(name="TEMPLATE_DESCRIPTION", length=1000)
	private String templateDescription;

	@Column(name="TEMPLATE_NAME", nullable=false, length=100)
	private String templateName;

	@Column(name="TEMPLATE_STATE", nullable=false)
	private Integer templateState;

	//bi-directional many-to-one association to ExternalInterface
	@OneToMany(mappedBy="externalInterfaceTemplate")
	private List<ExternalInterface> externalInterfaces;

	//bi-directional many-to-one association to ExtInterfaceTemplateDetail
	@OneToMany(mappedBy="externalInterfaceTemplate",cascade={CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.MERGE,CascadeType.DETACH,CascadeType.REMOVE}, orphanRemoval=true)
	private List<ExtInterfaceTemplateDetail> extInterfaceTemplateDetails;

    public ExternalInterfaceTemplate() {
    }

	public Long getIdExtInferfaceTemplatePk() {
		return this.idExtInferfaceTemplatePk;
	}

	public void setIdExtInferfaceTemplatePk(Long idExtInferfaceTemplatePk) {
		this.idExtInferfaceTemplatePk = idExtInferfaceTemplatePk;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return this.registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public String getTemplateDescription() {
		return this.templateDescription;
	}

	public void setTemplateDescription(String templateDescription) {
		this.templateDescription = templateDescription;
	}

	public String getTemplateName() {
		return this.templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public Integer getTemplateState() {
		return this.templateState;
	}

	public void setTemplateState(Integer templateState) {
		this.templateState = templateState;
	}

	public List<ExternalInterface> getExternalInterfaces() {
		return this.externalInterfaces;
	}

	public void setExternalInterfaces(List<ExternalInterface> externalInterfaces) {
		this.externalInterfaces = externalInterfaces;
	}
	
	public List<ExtInterfaceTemplateDetail> getExtInterfaceTemplateDetails() {
		return this.extInterfaceTemplateDetails;
	}

	public void setExtInterfaceTemplateDetails(List<ExtInterfaceTemplateDetail> extInterfaceTemplateDetails) {
		this.extInterfaceTemplateDetails = extInterfaceTemplateDetails;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
		detailsMap.put("extInterfaceTemplateDetails", extInterfaceTemplateDetails);
		return detailsMap;
	}
}