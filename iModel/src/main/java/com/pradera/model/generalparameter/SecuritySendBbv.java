package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the SECURITY_SEND_BBV database table.
 * 
 */
@Entity
@Table(name="SECURITY_SEND_BBV")
@NamedQuery(name="SecuritySendBbv.findAll", query="SELECT s FROM SecuritySendBbv s")
public class SecuritySendBbv implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SECURITY_SEND_BBV_IDSECURIRYSENDPK_GENERATOR", sequenceName="SEQ_ID_SECURIRY_SEND_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_SEND_BBV_IDSECURIRYSENDPK_GENERATOR")
	
	@Column(name="ID_SECURIRY_SEND_PK", unique=true, nullable=false, precision=10)
	private Long idSecurirySendPk;

	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

	@Column(name="NUMBER_PROCESS", nullable=false, precision=10)
	private Integer numberProcess;
	
	@Column(name="NUMBER_COUPON", nullable=false, precision=10)
	private Integer numberCuopon;
	
	@Temporal(TemporalType.DATE)
	@Column(name="SECURITY_EXPIRATION", nullable=false)
	private Date securityExpiration;

	@Temporal(TemporalType.DATE)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	@Column(nullable=false, length=23)
	private String security;

	@Temporal(TemporalType.DATE)
	@Column(name="SECURITY_EMITION", nullable=false)
	private Date securityEmition;

	@Column(name="SEND_STATE", nullable=false, length=23)
	private String sendState;
	
	@Column(name="SEND_DESCRIPTION",length=200)
	private String sendDescription;
	

	public SecuritySendBbv() {
	}
	
	public Integer getNumberCuopon() {
		return numberCuopon;
	}

	public void setNumberCuopon(Integer numberCuopon) {
		this.numberCuopon = numberCuopon;
	}

	public Date getSecurityExpiration() {
		return securityExpiration;
	}

	public void setSecurityExpiration(Date securityExpiration) {
		this.securityExpiration = securityExpiration;
	}

	public Long getIdSecurirySendPk() {
		return idSecurirySendPk;
	}

	public void setIdSecurirySendPk(Long idSecurirySendPk) {
		this.idSecurirySendPk = idSecurirySendPk;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getNumberProcess() {
		return numberProcess;
	}

	public void setNumberProcess(Integer numberProcess) {
		this.numberProcess = numberProcess;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getSecurity() {
		return this.security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public Date getSecurityEmition() {
		return this.securityEmition;
	}

	public void setSecurityEmition(Date securityEmition) {
		this.securityEmition = securityEmition;
	}

	public String getSendState() {
		return this.sendState;
	}

	public void setSendState(String sendState) {
		this.sendState = sendState;
	}
	
	
	public String getSendDescription() {
		return sendDescription;
	}

	public void setSendDescription(String sendDescription) {
		this.sendDescription = sendDescription;
	}

	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}