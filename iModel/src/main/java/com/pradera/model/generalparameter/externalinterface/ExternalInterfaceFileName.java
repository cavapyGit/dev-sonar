package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE_FILE_NAME database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25-nov-2013
 */
@Entity
@Table(name="EXTERNAL_INTERFACE_FILE_NAME")
public class ExternalInterfaceFileName implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interface file pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_FILE_NAME_IDEXTINTERFACEFILEPK_GENERATOR", sequenceName="SQ_ID_EXT_INTERFACE_FILE_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_FILE_NAME_IDEXTINTERFACEFILEPK_GENERATOR")
	@Column(name="ID_EXT_INTERFACE_FILE_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfaceFilePk;

	/** The interface filename state. */
	@Column(name="INTERFACE_FILENAME_STATE", nullable=false, precision=10)
	private Long interfaceFilenameState;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

	/** The parameter order. */
	@Column(name="PARAMETER_ORDER", nullable=false, precision=10)
	private Long parameterOrder;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	//bi-directional many-to-one association to ExternalInterface
	/** The external interface. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_FK", nullable=false)
	private ExternalInterface externalInterface;

	//bi-directional many-to-one association to ExternalInterfaceColumn
	/** The external interface column. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INTERFACE_COL_FK")
	private ExternalInterfaceColumn externalInterfaceColumn;

    /**
     * Instantiates a new external interface file name.
     */
    public ExternalInterfaceFileName() {
    }

	/**
	 * Gets the id ext interface file pk.
	 *
	 * @return the id ext interface file pk
	 */
	public Long getIdExtInterfaceFilePk() {
		return this.idExtInterfaceFilePk;
	}

	/**
	 * Sets the id ext interface file pk.
	 *
	 * @param idExtInterfaceFilePk the new id ext interface file pk
	 */
	public void setIdExtInterfaceFilePk(Long idExtInterfaceFilePk) {
		this.idExtInterfaceFilePk = idExtInterfaceFilePk;
	}

	/**
	 * Gets the interface filename state.
	 *
	 * @return the interface filename state
	 */
	public Long getInterfaceFilenameState() {
		return this.interfaceFilenameState;
	}

	/**
	 * Sets the interface filename state.
	 *
	 * @param interfaceFilenameState the new interface filename state
	 */
	public void setInterfaceFilenameState(Long interfaceFilenameState) {
		this.interfaceFilenameState = interfaceFilenameState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the parameter order.
	 *
	 * @return the parameter order
	 */
	public Long getParameterOrder() {
		return this.parameterOrder;
	}

	/**
	 * Sets the parameter order.
	 *
	 * @param parameterOrder the new parameter order
	 */
	public void setParameterOrder(Long parameterOrder) {
		this.parameterOrder = parameterOrder;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the external interface.
	 *
	 * @return the external interface
	 */
	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}

	/**
	 * Sets the external interface.
	 *
	 * @param externalInterface the new external interface
	 */
	public void setExternalInterface(ExternalInterface externalInterface) {
		this.externalInterface = externalInterface;
	}
	
	/**
	 * Gets the external interface column.
	 *
	 * @return the external interface column
	 */
	public ExternalInterfaceColumn getExternalInterfaceColumn() {
		return this.externalInterfaceColumn;
	}

	/**
	 * Sets the external interface column.
	 *
	 * @param externalInterfaceColumn the new external interface column
	 */
	public void setExternalInterfaceColumn(ExternalInterfaceColumn externalInterfaceColumn) {
		this.externalInterfaceColumn = externalInterfaceColumn;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}	
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
	
}