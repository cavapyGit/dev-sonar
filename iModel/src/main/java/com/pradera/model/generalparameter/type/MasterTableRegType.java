package com.pradera.model.generalparameter.type;

/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public enum MasterTableRegType {
	DEFINITION(new Integer(1),"DEFINICION"),
	ELEMENT(new Integer(2),"ELEMENTO");	
	private Integer code;
	private String value;
	public static final List<MasterTableRegType> list = new ArrayList<MasterTableRegType>();
	public static final Map<Integer, MasterTableRegType> lookup = new HashMap<Integer, MasterTableRegType>();
	public static List<MasterTableRegType> listSomeElements(MasterTableRegType... transferSecuritiesTypeParams){
		List<MasterTableRegType> retorno = new ArrayList<MasterTableRegType>();
		for(MasterTableRegType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	static {
		for (MasterTableRegType s : EnumSet.allOf(MasterTableRegType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private MasterTableRegType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
}
