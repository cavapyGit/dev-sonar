package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum CurrencyType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/05/2013
 */
public enum CurrencyType {
	
	/** The bob. */	
	/**PARAMETER TABLE INDICADOR 5 ES PARA LA ASFI*/
	PYG(Integer.valueOf(127),"GUARANIES","1","PYG",10),
	/** The bob. */
	UFV(Integer.valueOf(1734),"UNIDAD DE FOMENTO DE VIVIENDA","2","UFV",14),
	/**PARAMETER TABLE INDICADOR 5*/
	/** The usd. */
	USD(Integer.valueOf(430),"DOLARES AMERICANOS","3","USD",12),
	
	/** The eur. */
	EU(Integer.valueOf(1304),"EUROS","4","EU",15),
	
	/** The dmv. */
	DMV(Integer.valueOf(1853),"DOLARES CON MANTENIMIENTO DE VALORES","5","DMV",11);	
	
	/** The Constant list. */
	public final static List<CurrencyType> list=new ArrayList<CurrencyType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, CurrencyType> lookup=new HashMap<Integer, CurrencyType>();
	
	static {
		for(CurrencyType c : CurrencyType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The code cd. */
	private String codeCd;
	
	/** The code iso. */
	private String codeIso;
	
	private Integer codeAsfi;
	
	/**
	 * Instantiates a new currency type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 * @param codeIso the code iso
	 */
	private CurrencyType(Integer code, String value, String codeCd, String codeIso,Integer codeAsfi){
		this.code=code;
		this.value=value;
		this.codeCd=codeCd;
		this.codeIso = codeIso;
		this.codeAsfi = codeAsfi;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the currency type
	 */
	public static CurrencyType get(Integer code) {
		return lookup.get(code);
	}
	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
	
	/**
	 * Gets the code cd.
	 *
	 * @return the code cd
	 */
	public String getCodeCd(){
		return this.codeCd;
	}

	/**
	 * Gets the code iso.
	 *
	 * @return the code iso
	 */
	public String getCodeIso() {
		return codeIso;
	}

	/**
	 * Sets the code iso.
	 *
	 * @param codeIso the new code iso
	 */
	public void setCodeIso(String codeIso) {
		this.codeIso = codeIso;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Sets the code cd.
	 *
	 * @param codeCd the new code cd
	 */
	public void setCodeCd(String codeCd) {
		this.codeCd = codeCd;
	}
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<CurrencyType> listSomeElements(CurrencyType... transferSecuritiesTypeParams){
		List<CurrencyType> retorno = new ArrayList<CurrencyType>();
		for(CurrencyType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}

	public Integer getCodeAsfi() {
		return codeAsfi;
	}

	public void setCodeAsfi(Integer codeAsfi) {
		this.codeAsfi = codeAsfi;
	}
}
