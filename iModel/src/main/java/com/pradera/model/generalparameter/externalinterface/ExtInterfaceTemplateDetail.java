package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXT_INTERFACE_TEMPLATE_DETAIL database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@Entity
@Table(name="EXT_INTERFACE_TEMPLATE_DETAIL")
public class ExtInterfaceTemplateDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id ext interface temp det pk. */
	@Id
	@SequenceGenerator(name="EXT_INTERFACE_TEMPLATE_DETAIL_IDEXTINTERFACETEMPDETPK_GENERATOR", sequenceName="SQ_ID_EXT_INTERF_TEMP_DET_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXT_INTERFACE_TEMPLATE_DETAIL_IDEXTINTERFACETEMPDETPK_GENERATOR")
	@Column(name="ID_EXT_INTERFACE_TEMP_DET_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfaceTempDetPk;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

	/** The order priority. */
	@Column(name="ORDER_PRIORITY", precision=10)
	private Integer orderPriority;

    /** The register date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE", nullable=false)
	private Date registerDate;

	/** The register user. */
	@Column(name="REGISTER_USER", nullable=false, length=20)
	private String registerUser;

	/** The template detail state. */
	@Column(name="TEMPLATE_DETAIL_STATE", nullable=false, precision=10)
	private Integer templateDetailState;

	/** The template detail type. */
	@Column(name="TEMPLATE_DETAIL_TYPE", precision=10)
	private Integer templateDetailType;

	//bi-directional many-to-one association to ExternalInterfaceTemplate
	/** The external interface template. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INFERFACE_TEMPLATE_FK", nullable=false)
	private ExternalInterfaceTemplate externalInterfaceTemplate;
	
	/** The ext interface queries. */
	@OneToMany(mappedBy="interfaceTemplateDetail")
	private List<ExternalInterfaceQuery> extInterfaceQueries;
	
	/** The description. */
	@Transient
	private String description;
	
	/** The contain query. */
	@Transient
	private Boolean containQuery;
	
	/** The contain filter. */
	@Transient
	private Boolean containFilter;

    /**
     * The Constructor.
     */
    public ExtInterfaceTemplateDetail() {
    }

	/**
	 * Gets the id ext interface temp det pk.
	 *
	 * @return the id ext interface temp det pk
	 */
	public Long getIdExtInterfaceTempDetPk() {
		return this.idExtInterfaceTempDetPk;
	}

	/**
	 * Sets the id ext interface temp det pk.
	 *
	 * @param idExtInterfaceTempDetPk the id ext interface temp det pk
	 */
	public void setIdExtInterfaceTempDetPk(Long idExtInterfaceTempDetPk) {
		this.idExtInterfaceTempDetPk = idExtInterfaceTempDetPk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the order priority.
	 *
	 * @return the order priority
	 */
	public Integer getOrderPriority() {
		return this.orderPriority;
	}

	/**
	 * Sets the order priority.
	 *
	 * @param orderPriority the order priority
	 */
	public void setOrderPriority(Integer orderPriority) {
		this.orderPriority = orderPriority;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return this.registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the register user.
	 *
	 * @return the register user
	 */
	public String getRegisterUser() {
		return this.registerUser;
	}

	/**
	 * Sets the register user.
	 *
	 * @param registerUser the register user
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	/**
	 * Gets the template detail state.
	 *
	 * @return the template detail state
	 */
	public Integer getTemplateDetailState() {
		return this.templateDetailState;
	}

	/**
	 * Sets the template detail state.
	 *
	 * @param templateDetailState the template detail state
	 */
	public void setTemplateDetailState(Integer templateDetailState) {
		this.templateDetailState = templateDetailState;
	}

	/**
	 * Gets the template detail type.
	 *
	 * @return the template detail type
	 */
	public Integer getTemplateDetailType() {
		return this.templateDetailType;
	}

	/**
	 * Sets the template detail type.
	 *
	 * @param templateDetailType the template detail type
	 */
	public void setTemplateDetailType(Integer templateDetailType) {
		this.templateDetailType = templateDetailType;
	}

	/**
	 * Gets the external interface template.
	 *
	 * @return the external interface template
	 */
	public ExternalInterfaceTemplate getExternalInterfaceTemplate() {
		return this.externalInterfaceTemplate;
	}

	/**
	 * Sets the external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 */
	public void setExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) {
		this.externalInterfaceTemplate = externalInterfaceTemplate;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the ext interface queries.
	 *
	 * @return the ext interface queries
	 */
	public List<ExternalInterfaceQuery> getExtInterfaceQueries() {
		return extInterfaceQueries;
	}

	/**
	 * Sets the ext interface queries.
	 *
	 * @param extInterfaceQueries the ext interface queries
	 */
	public void setExtInterfaceQueries(
			List<ExternalInterfaceQuery> extInterfaceQueries) {
		this.extInterfaceQueries = extInterfaceQueries;
	}

	/**
	 * Gets the contain query.
	 *
	 * @return the contain query
	 */
	public Boolean getContainQuery() {
		return containQuery;
	}

	/**
	 * Sets the contain query.
	 *
	 * @param containQuery the new contain query
	 */
	public void setContainQuery(Boolean containQuery) {
		this.containQuery = containQuery;
	}

	/**
	 * Gets the contain filter.
	 *
	 * @return the contain filter
	 */
	public Boolean getContainFilter() {
		return containFilter;
	}

	/**
	 * Sets the contain filter.
	 *
	 * @param containFilter the new contain filter
	 */
	public void setContainFilter(Boolean containFilter) {
		this.containFilter = containFilter;
	}
	
}