package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum MasterTableType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/06/2013
 */
public enum MasterTableType {
	
	/** The securities state. */
	SECURITIES_STATE(Integer.valueOf(57)),
	
	/** The securities class. */
	SECURITIES_CLASS(Integer.valueOf(52)),
	
	/** The securities class. */
	FORMULA_TYPE(Integer.valueOf(525)),

	/** The securities group. */
	SECURITIES_GROUP(Integer.valueOf(397)),
	
	/** The securities type. */
	SECURITIES_TYPE(Integer.valueOf(51)),
	
	/** The instrument type. */
	INSTRUMENT_TYPE(Integer.valueOf(50)),
	
	/** The issuance type. */
	ISSUANCE_TYPE(Integer.valueOf( 58 )),
	
	/** The issuance state. */
	ISSUANCE_STATE(Integer.valueOf( 54 )),
	
	/** The issuance state. */
	ISSUANCE_STATE_AUX(Integer.valueOf( 1188 )),
	
	/** The currency. */
	CURRENCY(Integer.valueOf(53)),
	
	/** The agemajor doc repre nat. */
	AGEMAJOR_DOC_REPRE_NAT(Integer.valueOf(185)),  
	
	/** The ageless father doc repre nat. */
	AGELESS_INABILITY_FATHER_DOC_REPRE_NAT(Integer.valueOf(186)),
	
	/** The ageless tutor doc repre nat. */
	AGELESS_INABILITY_TUTOR_DOC_REPRE_NAT(Integer.valueOf(187)),
	
	/** The unable doc repre nat. */
	UNABLE_DOC_REPRE_NAT(Integer.valueOf(188)),
	
	/** The juridic doc repre nat. */
	NATURAL_DOC_REPRE_NAT(Integer.valueOf(189)),
	
	/** The statal entity doc repre nat. */
	AGELESS_INABILITY_LEGAL_DOC_REPRE_NAT(Integer.valueOf(190)),
	
	/** The participant doc repre nat. */
	PARTICIPANT_DOC_REPRE_NAT(Integer.valueOf(191)),
	
	/** The issuer doc repre nat. */
	ISSUER_DOC_REPRE_NAT(Integer.valueOf(192)),
	
	/** The bank doc repre nat. */
	BANK_DOC_REPRE_NAT(Integer.valueOf(193)),
	
	/** The agemajor inability doc repre jur. */
	AGEMAJOR_INABILITY_DOC_REPRE_JUR(Integer.valueOf(194)),
	
	/** The ageless doc repre jur. */
	AGELESS_DOC_REPRE_JUR(Integer.valueOf(195)),
	
	/** The ageless tutor doc repre jur. */
	AGELESS_TUTOR_DOC_REPRE_JUR(Integer.valueOf(196)),
	
	/** The unable doc repre jur. */
	UNABLE_DOC_REPRE_JUR(Integer.valueOf(197)),
	
	/** The juridic doc repre jur. */
	JURIDIC_DOC_REPRE_JUR(Integer.valueOf(198)),
	
	/** The statal entity doc repre jur. */
	STATAL_ENTITY_DOC_REPRE_JUR(Integer.valueOf(199)),
	
	/** The credit rating scales. */
	CREDIT_RATING_SCALES_ISSUER(Integer.valueOf(601)),
	
	CREDIT_RATING_SCALES_LONG_TERM(Integer.valueOf(295)),
	
	CREDIT_RATING_SCALES_SHORT_TERM(Integer.valueOf(568)),
	
	CREDIT_RATING_SCALES_PREFERED_STOCK(Integer.valueOf(569)),
	
	CREDIT_RATING_SCALES_EQUITIES(Integer.valueOf(570)),
	
	/** The person jur doc repre jur. */
	PERSON_JUR_DOC_REPRE_JUR(Integer.valueOf(200)),
	
	/** The t clase juridico titular. */
	T_CLASE_JURIDICO_TITULAR(Integer.valueOf(213)),
	
	/** The sex. */
	SEX(Integer.valueOf(143)),
	
	/** The person type. */
	PERSON_TYPE(Integer.valueOf(25)),
	
	/** The type document identity person. */
	TYPE_DOCUMENT_IDENTITY_PERSON(Integer.valueOf(15)),
	
	/** The class reprentative. */
	CLASS_REPRENTATIVE(Integer.valueOf(14)),
	
	/** The asset laundry motive pep. */
	ASSET_LAUNDRY_MOTIVE_PEP(Integer.valueOf(47)),
	
	/** The asset laundry source info. */
	ASSET_LAUNDRY_SOURCE_INFO(Integer.valueOf(214)),	
	
	/** The pep charge. */
	PEP_CHARGE(Integer.valueOf(48)),
	
	/** The petitioner type. */
	PETITIONER_TYPE(Integer.valueOf(145)),
	
	/** The CAT type. */
	CAT_TYPE(Integer.valueOf(1179)),
	
	/** The block type. */
	BLOCK_TYPE(Integer.valueOf(120)),
	
	/** The accreditation certificate state. */
	ACCREDITATION_CERTIFICATE_STATE(Integer.valueOf(217)),
	
	/** The unblock accreditation certificate state. */
	UNBLOCK_ACCREDITATION_CERTIFICATE_STATE(Integer.valueOf(582)),
	
	/** The participant document type. */
	PARTICIPANT_DOCUMENT_TYPE(Integer.valueOf(5)),
	
	/** The participant type. */
	PARTICIPANT_TYPE(Integer.valueOf(6)),
	
	/** The participant class. */
	PARTICIPANT_CLASS(Integer.valueOf(7)),
	
	/** The participant class indirect. */
	PARTICIPANT_CLASS_INDIRECT(Integer.valueOf(183)),
	
	/** The participant state. */
	PARTICIPANT_STATE(Integer.valueOf(2)), 
	
	/** The economic sector. */
	ECONOMIC_SECTOR(Integer.valueOf(8)),
	
	/** The economic activity. */
	ECONOMIC_ACTIVITY(Integer.valueOf(9)),
	
	/** The accounts type. */
	ACCOUNTS_TYPE(Integer.valueOf(35)),
	
	/** The asset laundry file type. */
	ASSET_LAUNDRY_FILE_TYPE(Integer.valueOf(218)),
	
	/** The holder account status. */
	HOLDER_ACCOUNT_STATUS(Integer.valueOf(223)),
	
	/** The holder request reject motive. */
	HOLDER_REQUEST_REJECT_MOTIVE(Integer.valueOf(224)),
	
	/** The holder request annular motive. */
	HOLDER_REQUEST_ANNULAR_MOTIVE(Integer.valueOf(225)),
	
	/** The holder account request reject motive. */
	HOLDER_ACCOUNT_REQUEST_REJECT_MOTIVE(Integer.valueOf(224)),
	
	/** The holder account request annular motive. */
	HOLDER_ACCOUNT_REQUEST_ANNULAR_MOTIVE(Integer.valueOf(225)),
	
	/** The state creation holder request. */
	STATE_CREATION_HOLDER_REQUEST(Integer.valueOf(226)),
	
	/** The state block unblock holder request. */
	STATE_BLOCK_UNBLOCK_HOLDER_REQUEST(Integer.valueOf(230)), 
	
	/** The holder block motive. */
	HOLDER_BLOCK_MOTIVE(Integer.valueOf(231)),
	
	/** The holder unblock motive. */
	HOLDER_UNBLOCK_MOTIVE(Integer.valueOf(232)),
    
    /** The holder request type. */
    HOLDER_REQUEST_TYPE(Integer.valueOf(234)),
    
    /** The holder block unblock actions type. */
    HOLDER_BLOCK_UNBLOCK_ACTIONS_TYPE(Integer.valueOf(235)),
	
	/** The state history state holder type. */
	STATE_HISTORY_STATE_HOLDER_TYPE(Integer.valueOf(236)),
	
	/** The participant modification request state. */
	PARTICIPANT_MODIFICATION_REQUEST_STATE(Integer.valueOf(233)),
	
	/** The security source. */
	SECURITY_SOURCE(Integer.valueOf(237)),
	
	/** The interest type. */
	INTEREST_TYPE(Integer.valueOf(74)),
	
	/** The calendar type. */
	CALENDAR_TYPE(Integer.valueOf(71)),
	
	/** The calendar days. */
	CALENDAR_DAYS(Integer.valueOf(238)),
	
	/** The payment modality. */
	PAYMENT_MODALITY(Integer.valueOf(239)),
	
	/** The placement segment reject motive. */
	PLACEMENT_SEGMENT_REJECT_MOTIVE(Integer.valueOf(308)),
	
	/** The rate type. */
	RATE_TYPE(Integer.valueOf(240)),
	
	/** The interest periodicity. */
	INTEREST_PERIODICITY(Integer.valueOf(73)),
	
	/** The motive block holderaccount. */
	MOTIVE_BLOCK_HOLDERACCOUNT(Integer.valueOf(245)),
	
	/** The holderaccount documents block. */
	HOLDERACCOUNT_DOCUMENTS_BLOCK(Integer.valueOf(297)),
	
	/** The operation type. */
	OPERATION_TYPE(Integer.valueOf( 241 )),
	
	/** The participant block ubblock annul type. */
	PARTICIPANT_BLOCK_UBBLOCK_ANNUL_TYPE(Integer.valueOf(242)),
	
	/** The participant block motive. */
	PARTICIPANT_BLOCK_MOTIVE(Integer.valueOf(128)),
	
	/** The participant unblock motive. */
	PARTICIPANT_UNBLOCK_MOTIVE(Integer.valueOf(243)),
	
	/** The participant annul motive. */
	PARTICIPANT_ANNUL_MOTIVE(Integer.valueOf(244)),
	
	/** The security yield type. */
	SECURITY_YIELD_TYPE(Integer.valueOf( 68 )),
	
	/** The cash nominal. */
	CASH_NOMINAL(Integer.valueOf(246)),
	
	/** The amortization type. */
	AMORTIZATION_TYPE(Integer.valueOf(77)),
	
	/** The amortization on. */
	AMORTIZATION_ON(Integer.valueOf(247)),
	
	/** The investor type. */
	INVESTOR_TYPE(Integer.valueOf(136)),
	
	/** The financial index. */
	FINANCIAL_INDEX(Integer.valueOf(76)),
	
	/** The asset laundry person type. */
	ASSET_LAUNDRY_PERSON_TYPE(Integer.valueOf(46)),
	
	/** The amortization factor. */
	AMORTIZATION_FACTOR(Integer.valueOf( 666 )),
	
	/** The participant block ubblock annul state. */
	PARTICIPANT_BLOCK_UBBLOCK_ANNUL_STATE(Integer.valueOf(1)),
	
	/** The participant block ubblock annul reject motive. */
	PARTICIPANT_BLOCK_UBBLOCK_ANNUL_REJECT_MOTIVE(Integer.valueOf(250)),
	
	/** The participant modification reject motive. */
	PARTICIPANT_MODIFICATION_REJECT_MOTIVE(Integer.valueOf(470)),
	
	/** The participant role. */
	PARTICIPANT_ROLE(Integer.valueOf(251)),
	
	/** The accreditation payment way. */
	ACCREDITATION_PAYMENT_WAY(Integer.valueOf(264)),
	
	/** The accreditation destination deliver. */
	ACCREDITATION_DESTINATION_DELIVER(Integer.valueOf(267)),
	
	/** The security inversionist state. */
	SECURITY_INVERSIONIST_STATE(Integer.valueOf(265)),
	
	/** The block entity state. */
	BLOCK_ENTITY_STATE(Integer.valueOf(298)),
	
	/** The security state. */
	SECURITY_STATE(Integer.valueOf(57)),
	
	/** The issuer state. */
	ISSUER_STATE(Integer.valueOf(248)),
	
	/** The issuer file national type. */
	ISSUER_FILE_NATIONAL_TYPE(Integer.valueOf(23)),
	
	/** The issuer file foreign type. */
	ISSUER_FILE_FOREIGN_TYPE(Integer.valueOf(352)),
	
	/** The issuer file differentiated type. */
	ISSUER_FILE_DIFFERENTIATED_TYPE(Integer.valueOf(353)),
	
	/** The increase capital motive. */
	INCREASE_CAPITAL_MOTIVE(Integer.valueOf(152)),
	
	/** The delivery place. */
	DELIVERY_PLACE(Integer.valueOf(151)),
	
	/** The issuer block request status type. */
	ISSUER_BLOCK_REQUEST_STATUS_TYPE(Integer.valueOf(19)),
	
	/** The issuer block request motive type. */
	ISSUER_BLOCK_REQUEST_MOTIVE_TYPE(Integer.valueOf(20)),
	//ISSUER_UNBLOCK_REQUEST_STATUS_TYPE(Integer.valueOf(21)), -- block and unblock use the same STAtus type
	/** The issuer unblock request motive type. */
	ISSUER_UNBLOCK_REQUEST_MOTIVE_TYPE(Integer.valueOf(22)),
	
	/** The issuer request type. */
	ISSUER_REQUEST_TYPE(Integer.valueOf(11)),
	
	/** The issuer request reject motive type. */
	ISSUER_REQUEST_REJECT_MOTIVE_TYPE(Integer.valueOf(107)),
	
	/** The useful number days. */
	TAX_FACTOR(Integer.valueOf(359)),
	
	/** The society type. */
	SOCIETY_TYPE(Integer.valueOf(130)),
	
	/** The accreditation signature. */
	ACCREDITATION_SIGNATURE(Integer.valueOf(278)),
	
	/** The accreditation reject motive. */
	ACCREDITATION_REJECT_MOTIVE(Integer.valueOf(277)),
	
	/** The state pna. */
	STATE_PNA(Integer.valueOf(282)),
	
	/** The participant modification type. */
	PARTICIPANT_MODIFICATION_TYPE(Integer.valueOf(281)),
	
	/** The participant file national type. */
	PARTICIPANT_FILE_TYPE(Integer.valueOf(3)),		
	
	/** The security block motive. */
	SECURITY_BLOCK_MOTIVE(Integer.valueOf(134)),
	
	/** The negotiation parameters master table pk. */
	NEGOTIATION_PARAMETERS_MASTER_TABLE_PK(Integer.valueOf(285)),
	
	/** The index financial parameter table pk. */
	INDEX_FINANCIAL_PARAMETER_TABLE_PK(Integer.valueOf(711)),
	
	/** The rate financial parameter table pk. */
	RATE_FINANCIAL_PARAMETER_TABLE_PK(Integer.valueOf(712)),
	
	/** The rate period type parameter table pk. */
	RATE_PERIOD_TYPE_PARAMETER_TABLE_PK(Integer.valueOf(73)),
	
	/** The participant unification motives. */
	PARTICIPANT_UNIFICATION_MOTIVES(Integer.valueOf(119)),
	
	/** The financial period. */
	FINANCIAL_PERIOD(Integer.valueOf(293)),
	
	/** The dematerialization motive. */
	DEMATERIALIZATION_MOTIVE(Integer.valueOf(141)),
	
	/** The dematerialization state. */
	DEMATERIALIZATION_STATE(Integer.valueOf(140)),
	
	/** The placement segment state. */
	PLACEMENT_SEGMENT_STATE(Integer.valueOf(249)),
	
	/** The agemajor boliv or foreign resident doc holder nat. */
	AGEMAJOR_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(201)),
	
	/** The agemajor boliv no resident doc holder nat. */
	AGEMAJOR_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(202)),
	
	/** The agemajor foreign no resident doc holder nat. */
	AGEMAJOR_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(203)),
	
	/** The ageless boliv or foreign resident doc holder nat. */
	AGELESS_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(204)),
	
	/** The ageless boliv no resident doc holder nat. */
	AGELESS_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(205)),
	
	/** The ageless foreign no resident doc holder nat. */
	AGELESS_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(206)),
	
	/** The unable boliv or foreign resident doc holder nat. */
	UNABLE_BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(207)),
	
	/** The unable boliv no resident doc holder nat. */
	UNABLE_BOLIV_NO_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(208)),
	
	/** The unable foreign no resident doc holder nat. */
	UNABLE_FOREIGN_NO_RESIDENT_DOC_HOLDER_NAT(Integer.valueOf(209)),
	
	/** The boliv or foreign resident doc holder jur. */
	BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_JUR(Integer.valueOf(210)),
	
	/** The foreign no resident doc holder jur. */
	FOREIGN_NO_RESIDENT_DOC_HOLDER_JUR(Integer.valueOf(212)),
	
	/** The statal entity doc holder jur. */
	STATAL_ENTITY_DOC_HOLDER_JUR(Integer.valueOf(311)),
	
	/** The estados operacion mcn. */
	ESTADOS_OPERACION_MCN(Integer.valueOf(257)),
	
	/** The settlement date request. */
	SETTLEMENT_REQUEST_STATE(Integer.valueOf(348)),
	
	/** The settlement date request type. */
	SETTLEMENT_DATE_REQUEST_TYPE(Integer.valueOf(349)),
	
	/** The settlement unfulfillment type. */
	UNFULFILLMENT_TYPE(Integer.valueOf(350)),
	
	/** The settlement unfulfillment reason. */
	UNFULFILLMENT_REASON(Integer.valueOf(351)),
	
	/** The settlement date request type. */
	SETTLEMENT_REQUEST_TYPE(Integer.valueOf(528)),
	
	SETTLEMENT_TYPE_EXCHANGE_TYPE(Integer.valueOf(534)),
	
	SETTLEMENT_ANTICIPATION_TYPE(Integer.valueOf(536)),
	
	/** The trust doc holder jur. */
	TRUST_DOC_HOLDER_JUR(Integer.valueOf(312)),	
	
	/** The trust no resident doc holder jur. */
	TRUST_NO_RESIDENT_DOC_HOLDER_JUR(Integer.valueOf(313)),
	
	/** The participant union state. */
	PARTICIPANT_UNION_STATE(Integer.valueOf(318)),
	
	/** The security unblock motive. */
	SECURITY_UNBLOCK_MOTIVE(Integer.valueOf(135)),
	
	/** The holder file state type. */
	HOLDER_FILE_STATE_TYPE(Integer.valueOf(314)),
	
	/** The registry type. */
	REGISTRY_TYPE(Integer.valueOf(315)),
	
	/** The holder file type. */
	HOLDER_FILE_TYPE(Integer.valueOf(316)),
	
	/** The motivos cancelacion operacion mcn. */
	MOTIVOS_CANCELACION_OPERACION_MCN(Integer.valueOf(288)),
	
	/** The holder req file his type. */
	HOLDER_REQ_FILE_HIS_TYPE(Integer.valueOf(317)),
	
	/** The holder account files. */
	HOLDER_ACCOUNT_FILES(Integer.valueOf(346)),
	
	/** The afectation reject motive. */
	AFECTATION_REJECT_MOTIVE(Integer.valueOf(483)),
	
	/** The information source. */
	INFORMATION_SOURCE(Integer.valueOf(133)),
	
	/** The information source. */
	DAILY_EXCHANGE_RATE_STATE(Integer.valueOf(559)),
	
	/** The financial indicator status. */
	FINANCIAL_INDICATOR_STATUS(Integer.valueOf(343)),
	
	/** The financial indicator active status. */
	FINANCIAL_INDICATOR_ACTIVE_STATUS(Integer.valueOf(1206)),
	
	/** The financial indicator defeated status. */
	FINANCIAL_INDICATOR_DEFEATED_STATUS(Integer.valueOf(1207)),
	
	/** The agreement type. */
	AGREEMENT_TYPE(Integer.valueOf(150)),
	
	/** The request type. */
	REQUEST_TYPE(Integer.valueOf(272)),
	/** The security location. */
	SECURITY_LOCATION(Integer.valueOf(256)),
	/** The security states. */
	SECURITY_STATES(Integer.valueOf(253)),
	
	/** The type affectation. */
	TYPE_AFFECTATION(new Integer(323)),
	
	/** The level affectation. */
	LEVEL_AFFECTATION(new Integer(324)),
	
	/** The state affectation request. */
	DEPOSIT_STATE_REQUEST(new Integer(421)),
	
	/** The state affectation request. */
	DEPOSIT_STATE(new Integer(253)),
	
	/** The state affectation request. */
	STATE_AFFECTATION_REQUEST(new Integer(325)),
	
	/** The state affectation request. */
	REASON_REJECT_ANULLED_REQUEST(new Integer(286)),
	
	/** The state affectation. */
	STATE_AFFECTATION(new Integer(328)),
	
	/** The affectation form. */
	AFFECTATION_FORM(new Integer(124)),
	
	/** The state reversals request. */
	STATE_REVERSALS_REQUEST(new Integer(94)),
	
	/** The motive reversals. */
	MOTIVE_REVERSALS(new Integer(32)),
	
	/** The motive reject reversals. */
	MOTIVE_REJECT_REVERSALS(new Integer(484)),
	
	/** The motive reject dematerialization. */
	MOTIVE_REJECT_DEMATERIALIZATION(new Integer(344)),
	
	/** The estado operacion internacional. */
	ESTADO_OPERACION_INTERNACIONAL(new Integer(329)),
	
	/** The corporative event. */
	CORPORATIVE_EVENT(new Integer(149)),
	
	/** The business process type. */
	BUSINESS_PROCESS_TYPE(new Integer(372)),
	
	/** The business process state. */
	BUSINESS_PROCESS_STATE(new Integer(373)),
	
	/** The process logger state. */
	PROCESS_LOGGER_STATE(new Integer(374)),
	
	/** The process logger scheduled state. */
	PROCESS_LOGGER_SCHEDULED_STATE(new Integer(375)),
	
	//ChangeOwnershipOperations
	
	/** The master table change ownership reject motive type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_REJECT_MOTIVE_TYPE(new Integer(371)),
	
	/** The master table change ownership motive type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_MOTIVE_TYPE(new Integer(289)),
	
	/** The master table change ownership balance type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_BALANCE_TYPE(new Integer(290)),
	
	/** The master table change ownership status type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_STATUS_TYPE(new Integer(291)),
	
	/** The master table change ownership proc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_PROC_TYPE(new Integer(342)),
	
	/** The master table change ownership entire state doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_ENTIRE_STATE_DOC_TYPE(new Integer(334)),
	
	/** The master table change ownership marriage partnership equity doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_MARRIAGE_PARTNERSHIP_EQUITY_DOC_TYPE(new Integer(335)),
	
	/** The master table change ownership coownership division partition doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_COOWNERSHIP_DIVISION_PARTITION_DOC_TYPE(new Integer(336)),
	
	/** The master table change ownership donation doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_DONATION_DOC_TYPE(new Integer(337)),
	
	/** The master table change ownership merger split equity doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_MERGER_SPLIT_EQUITY_DOC_TYPE(new Integer(338)),
	
	/** The master table change ownership nonrecourse debt doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_NONRECOURSE_DEBT_DOC_TYPE(new Integer(339)),
	
	/** The master table change ownership swap doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_SWAP_DOC_TYPE(new Integer(340)),
	
	/** The master table change ownership writ doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_WRIT_DOC_TYPE(new Integer(341)),
	
	/** The master table change ownership writ doc type. */
	MASTER_TABLE_CHANGE_OWNERSHIP_CHANGE_DOC_TYPE(new Integer(477)),
	
	/** The master table issuer type */
	MASTER_TABLE_OFFER_TYPE(new Integer(1195)),
	
	//END ChangeOwnershipOperations 
	
	/** The master table coupon grant type. */
	MASTER_TABLE_COUPON_GRANT_TYPE(new Integer(461)),
	
	/** The master table coupon grant state type. */
	MASTER_TABLE_COUPON_GRANT_STATE_TYPE(new Integer(462)),
	
	/** The master table coupon transfer state type. */
	MASTER_TABLE_COUPON_TRANSFER_STATE_TYPE(new Integer(465)),
	
	/** The loanable securities operation state. */
	LOANABLE_SECURITIES_OPERATION_STATE(new Integer(466)),
	
	/** The loanable securities operation type request. */
	LOANABLE_SECURITIES_OPERATION_TYPE_REQUEST(new Integer(472)),
	
	/** The loanable securities operation annul motive. */
	LOANABLE_SECURITIES_OPERATION_ANNUL_MOTIVE(Integer.valueOf(478)),
	
	/** The loanable securities operation reject motive. */
	LOANABLE_SECURITIES_OPERATION_REJECT_MOTIVE(Integer.valueOf(479)),
	
	/** The  loanable securities operation reject motive retirement. */
	LOANABLE_SECURITIES_OPERATION_REJECT_MOTIVE_RETIREMENT(Integer.valueOf(480)),
	
	/** The  loanable unblock motive. */
	LOANABLE_UNBLOCK_MOTIVE(Integer.valueOf(481)),	
	
	/** The  currency settlement state. */
	CURRENCY_SETTLEMENT_STATE(new Integer(488)),
	
	/** The  currency settlement annul motive. */
	CURRENCY_SETTLEMENT_ANNUL_MOTIVE(new Integer(489)),
	
	/** The  currency settlement reject motive. */
	CURRENCY_SETTLEMENT_REJECT_MOTIVE(new Integer(490)),
	//OTC OPERATIONS
	/** The search date type. */
	SEARCH_DATE_TYPE(new Integer(326)),
	
	/** The otc operation state. */
	OTC_OPERATION_STATE(new Integer(327)),
	
	SIRTEX_OPERATION_STATE(new Integer(495)),
	
	/** The negotiations factors. */
	NEGOTIATIONS_FACTORS(new Integer(400)),
	
	/** The otc request origing. */
	OTC_REQUEST_ORIGING(new Integer(156)),
	
	/** The settlement type. */
	SETTLEMENT_TYPE(new Integer(12)),
	
	/** The settlement scheme type. */
	SETTLEMENT_SCHEME_TYPE(new Integer(158)),
	
	/** The assignment process state. */
	ASSIGNMENT_PROCESS_STATE(new Integer(261)),
	
	REASSIGNMENT_REQUEST_STATE(new Integer(557)),
	
	/** The participant assignment process state. */
	PARTICIPANT_ASSIGNMENT_PROCESS_STATE(new Integer(262)),
	//END - OTC OPERATIONS
	//FUNDS
	/** The  bank block motive. */
	BANK_BLOCK_MOTIVE(new Integer(498)),
	
	/** The  bank unblock motive. */
	BANK_UNBLOCK_MOTIVE(new Integer(499)),
	
	/** The  bank accounts block motive. */
	BANK_ACCOUNTS_BLOCK_MOTIVE(new Integer(502)),
	
	/** The  bank accounts unblock motive. */
	BANK_ACCOUNTS_UNBLOCK_MOTIVE(new Integer(503)),
	
	/** The master table fund account type. */
	MASTER_TABLE_FUND_ACCOUNT_TYPE(Integer.valueOf(268)),
	
	/** The master table fund operation group deposit. */
	MASTER_TABLE_FUND_OPERATION_GROUP_DEPOSIT(Integer.valueOf(269)),
	
	/** The master table fund operation group retirement. */
	MASTER_TABLE_FUND_OPERATION_GROUP_RETIREMENT(Integer.valueOf(284)),
	
	/** The master table fund operation state. */
	MASTER_TABLE_FUND_OPERATION_STATE(Integer.valueOf(271)),
	
	/** The master table fund operation process type. */
	MASTER_TABLE_FUND_OPERATION_PROCESS_TYPE(Integer.valueOf(482)),
	
	/** The master table bank account class. */
	MASTER_TABLE_BANK_ACCOUNT_CLASS(Integer.valueOf(280)),
	
	MASTER_TABLE_SETTLEMENT_PROCESS_TYPE(Integer.valueOf(355)),
	
	/** The master table remove settlement. */
	MASTER_TABLE_REMOVE_SETTLEMENT(Integer.valueOf(407)),
	
	/** The master table bank account type. */
	MASTER_TABLE_BANK_ACCOUNT_TYPE(Integer.valueOf(216)),
	
	/** The master table bank type. */
	MASTER_TABLE_BANK_TYPE(Integer.valueOf(273)),
	
	/** The master table bank type. */
	MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_TYPE(Integer.valueOf(500)),
	
	/** The master table cash account use type. */
	MASTER_TABLE_CASH_ACCOUNT_USE_TYPE(Integer.valueOf(292)),
	
	/** The master table cash account state. */
	MASTER_TABLE_CASH_ACCOUNT_STATE(Integer.valueOf(276)),

	/** The master table swift message state. */
	MASTER_TABLE_SWIFT_MESSAGE_STATE(Integer.valueOf(385)),

	/*The parameter table for messages in processed state*/
	/** The parameter table message state processed. */
	PARAMETER_TABLE_MESSAGE_STATE_PROCESSED(Integer.valueOf(1443)),

	/*The parameter table for messages in not processed state*/
	/** The parameter table message state not processed. */
	PARAMETER_TABLE_MESSAGE_STATE_NOT_PROCESSED(Integer.valueOf(1444)),

	//HOLIDAY
	/** The master table holiday type. */
	MASTER_TABLE_HOLIDAY_TYPE(Integer.valueOf(365)),
	
	/** The parameter table bank account class physical. */
	PARAMETER_TABLE_BANK_ACCOUNT_CLASS_PHYSICAL(Integer.valueOf(680)),
	
	/** The parameter table bank account class virtual. */
	PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL(Integer.valueOf(681)),

	/** The parameter table bank account class owner. */
	PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER(Long.valueOf(694)),
	
	/** The parameter table bank account class thirds. */
	PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS(Long.valueOf(695)),

	/** The parameter table bank origin national. */
	PARAMETER_TABLE_BANK_ORIGIN_NATIONAL(Long.valueOf(699)),
	
	/** The parameter table bank origin foreigner. */
	PARAMETER_TABLE_BANK_ORIGIN_FOREIGNER(Long.valueOf(700)),
	
	/** The parameter table operations international state. */
	PARAMETER_TABLE_OPERATIONS_INTERNATIONAL_STATE(Integer.valueOf(329)),
	
	/** The parameter table use type send. */
	PARAMETER_TABLE_USE_TYPE_SEND(Long.valueOf(743)),
	
	/** The parameter table use type reception. */
	PARAMETER_TABLE_USE_TYPE_RECEPTION(Long.valueOf(744)),
	
	/** The parameter table use type both. */
	PARAMETER_TABLE_USE_TYPE_BOTH(Long.valueOf(745)),

	/** The parameter table issuer state activate. */
	PARAMETER_TABLE_ISSUER_STATE_ACTIVATE(Long.valueOf(578)),
	
	/** The parameter table participant state register. */
	PARAMETER_TABLE_PARTICIPANT_STATE_REGISTER(Long.valueOf(6)),
	
	/** The parameter table bank state registered. */
	PARAMETER_TABLE_BANK_STATE_REGISTERED(Integer.valueOf(678)),
	
	/** The parameter table participant class stock exchange. */
	PARAMETER_TABLE_PARTICIPANT_CLASS_STOCK_EXCHANGE(Long.valueOf(28)),

	/** The parameter table currency type pesos. */
	PARAMETER_TABLE_CURRENCY_TYPE_PESOS(Integer.valueOf(127)),
	
	/** The parameter table currency type dolares. */
	PARAMETER_TABLE_CURRENCY_TYPE_DOLARES(Integer.valueOf(430)),

	/** The parameter table operation state registered. */
	PARAMETER_TABLE_OPERATION_STATE_REGISTERED(Long.valueOf(612)),
	
	/** The parameter table operation state cancel. */
	PARAMETER_TABLE_OPERATION_STATE_CANCEL(Long.valueOf(613)),
	
	/** The parameter table operation state assigned. */
	PARAMETER_TABLE_OPERATION_STATE_ASSIGNED(Long.valueOf(614)),
	
	/** The parameter table operation state cash settle. */
	PARAMETER_TABLE_OPERATION_STATE_CASH_SETTLE(Long.valueOf(615)),
	
	/** The parameter table operation state term settle. */
	PARAMETER_TABLE_OPERATION_STATE_TERM_SETTLE(Long.valueOf(616)),

	/** The parameter table fund operation state registered. */
	PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED(Integer.valueOf(669)),
	
	/** The parameter table fund operation state confirmed. */
	PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED(Integer.valueOf(670)),
	
	/** The parameter table fund operation state rejected. */
	PARAMETER_TABLE_FUND_OPERATION_STATE_REJECTED(Integer.valueOf(671)),
	
	/** The parameter table fund operation state observed. */
	PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED(Integer.valueOf(672)),
	
	/** The parameter table fund operation state transferred to centralizer. */
	PARAMETER_TABLE_FUND_OPERATION_STATE_TRANSFERRED_TO_CENTRALIZER(Integer.valueOf(2850)),

	/** The PARAMETE r_ tabl e_ tr n_399_400. */
	PARAMETER_TABLE_TRN_399_400(Long.valueOf(1155)),
	
	/** The PARAMETE r_ tabl e_ tr n_900_901. */
	PARAMETER_TABLE_TRN_900_901(Long.valueOf(1156)),
	
	/** The parameter table wrong trn. */
	PARAMETER_TABLE_WRONG_TRN(Long.valueOf(1157)),
	
	/** The parameter table wrong issuer nemonic. */
	PARAMETER_TABLE_WRONG_ISSUER_NEMONIC(Long.valueOf(1158)),
	
	/** The parameter table wrong settlement schema. */
	PARAMETER_TABLE_WRONG_SETTLEMENT_SCHEMA(Long.valueOf(1159)),
	
	/** The parameter table net settlement. */
	PARAMETER_TABLE_NET_SETTLEMENT(Integer.valueOf(354)),
	
	/** The parameter table net settlement. */
	PARAMETER_TABLE_SCHED_SETTLEMENT(Integer.valueOf(509)),
	
	/** The parameter table not exist operation. */
	PARAMETER_TABLE_NOT_EXIST_OPERATION(Long.valueOf(1160)),
	
	/** The parameter table not exist mechanism. */
	PARAMETER_TABLE_NOT_EXIST_MECHANISM(Long.valueOf(1161)),
	
	/** The parameter table wrong modality group. */
	PARAMETER_TABLE_WRONG_MODALITY_GROUP(Long.valueOf(1162)),
	
	/** The parameter table wrong settlement date. */
	PARAMETER_TABLE_WRONG_SETTLEMENT_DATE(Long.valueOf(1163)),
	
	/** The parameter table wrong payment date. */
	PARAMETER_TABLE_WRONG_PAYMENT_DATE(Long.valueOf(1164)),
	
	/** The parameter table wrong operation state. */
	PARAMETER_TABLE_WRONG_OPERATION_STATE(Long.valueOf(1165)),
	
	/** The parameter table wrong currency. */
	PARAMETER_TABLE_WRONG_CURRENCY(Long.valueOf(1166)),
	
	/** The parameter table wrong participant settler. */
	PARAMETER_TABLE_WRONG_PARTICIPANT_SETTLER(Long.valueOf(1167)),

	/** The parameter table not exist cash account. */
	PARAMETER_TABLE_NOT_EXIST_CASH_ACCOUNT(Long.valueOf(1503)),
	
	/** The parameter table special payment. */
	PARAMETER_TABLE_SPECIAL_PAYMENT(Long.valueOf(1725)),

	/** The parameter table incharge not confirmed. */
	PARAMETER_TABLE_INCHARGE_NOT_CONFIRMED(Long.valueOf(1495)),
	
	/** The parameter table participant already paid. */
	PARAMETER_TABLE_PARTICIPANT_ALREADY_PAID(Long.valueOf(1496)),
	
	/** The parameter table operation paid. */
	PARAMETER_TABLE_OPERATION_PAID(Long.valueOf(1497)),
	
	/** The parameter table wrong cash account. */
	PARAMETER_TABLE_WRONG_CASH_ACCOUNT(Long.valueOf(1168)),
	
	/** The parameter table not exist repayment operation. */
	PARAMETER_TABLE_NOT_EXIST_REPAYMENT_OPERATION(Long.valueOf(1169)),

	/** The parameter table reference payment doesnt manage a correlative. */
	PARAMETER_TABLE_PAYMENT_REFENCE_INCOMPLETE(Long.valueOf(1670)),

	/** The parameter table ambiguous group. */
	PARAMETER_TABLE_AMBIGUOUS_GROUP(Long.valueOf(1728)),
	
	/** The parameter table operation group guarantees. */
	PARAMETER_TABLE_OPERATION_GROUP_GUARANTEES(Integer.valueOf(703)),
	
	/** The parameter table operation group rates. */
	PARAMETER_TABLE_OPERATION_GROUP_RATES(Integer.valueOf(704)),
	
	/** The parameter table operation group int operations. */
	PARAMETER_TABLE_OPERATION_GROUP_INT_OPERATIONS(Integer.valueOf(705)),
	
	/** The parameter table operation group return. */
	PARAMETER_TABLE_OPERATION_GROUP_RETURN(Integer.valueOf(706)),
	
	/** The parameter table operation group settlement. */
	PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT(Integer.valueOf(708)),
	
	/** The parameter table operation group benefits. */
	PARAMETER_TABLE_OPERATION_GROUP_BENEFITS(Integer.valueOf(709)),
	
	/** The parameter table operation group tax. */
	PARAMETER_TABLE_OPERATION_GROUP_TAX(Integer.valueOf(710)),

	/** The parameter table operation group tax. */
	PARAMETER_TABLE_OPERATION_GROUP_CENTRALIZER_FUNDS(Integer.valueOf(1224)),

	/** The parameter table operation IVA tax. */
	PARAMETER_TABLE_IVA_TAX(Integer.valueOf(2308)),
	
	/** The parameter table participant account operations. */
	PARAMETER_TABLE_PARTICIPANT_ACCOUNT_OPERATIONS(Integer.valueOf(1225)),
	
	/** The parameter table participant account accounting. */
	PARAMETER_TABLE_PARTICIPANT_ACCOUNT_ACCOUNTING(Integer.valueOf(1226)),
	
	/** The parameter requester block unbloc holderaccount. */
	PARAMETER_REQUESTER_BLOCK_UNBLOC_HOLDERACCOUNT(Integer.valueOf(345)),
	
	/** The parameter table cash account state activate string. */
	PARAMETER_TABLE_CASH_ACCOUNT_STATE_ACTIVATE_STRING("ACTIVA"),
	
	/** The parameter table cash account state register string. */
	PARAMETER_TABLE_CASH_ACCOUNT_STATE_REGISTER_STRING("REGISTRADA"),
	
	/** The parameter table cash account state pesos string. */
	PARAMETER_TABLE_CASH_ACCOUNT_STATE_PESOS_STRING("PESOS DOMINICANOS"),
	
	/** The parameter table cash account state dolar string. */
	PARAMETER_TABLE_CASH_ACCOUNT_STATE_DOLAR_STRING("DOLARES"),
	
	/** The parameter table bank state activad. */
	PARAMETER_TABLE_BANK_STATE_BLOCK(Integer.valueOf(679)),
	
	/** The parameter table bank rnc doc type. */
	PARAMETER_TABLE_BANK_RNC_DOC_TYPE(Long.valueOf(24)),
	
	/** The parameter table bank dio doc type. */
	PARAMETER_TABLE_BANK_DIO_DOC_TYPE(Long.valueOf(25)),
	
	/** The retirement motive state. */
	RETIREMENT_MOTIVE_STATE(Integer.valueOf(83)),

	/** The parameter table anulate other motive. */
	PARAMETER_TABLE_ANULATE_OTHER_MOTIVE(Long.valueOf(1660)),
	
	/** The parameter table reject other motive. */
	PARAMETER_TABLE_REJECT_OTHER_MOTIVE(Long.valueOf(1663)),
	
	/** The parameter table cancel other motive. */
	PARAMETER_TABLE_CANCEL_OTHER_MOTIVE(Long.valueOf(1666)),
	
	/** The master table bank origin. */
	MASTER_TABLE_BANK_ORIGIN(Integer.valueOf(283)),
	
	/** The master table state type. */
	MASTER_TABLE_STATE_TYPE(Integer.valueOf(274)),
	
	/** The master table state bank accounts type. */
	MASTER_TABLE_INSTITUTION_BANK_ACCOUNTS_STATE_TYPE(Integer.valueOf(501)),
	
	/** The master table reject reason. */
	MASTER_TABLE_ANULATE_MOTIVE_INT_OPERATION(Integer.valueOf(446)),
	
	/** The master table reject motive int operation. */
	MASTER_TABLE_REJECT_MOTIVE_INT_OPERATION(Integer.valueOf(447)),
	
	/** The master table cancel motive int operation. */
	MASTER_TABLE_CANCEL_MOTIVE_INT_OPERATION(Integer.valueOf(448)),
	
	//END FUNDS
	/** The retirement motive parameter. */
	RETIREMENT_MOTIVE_PARAMETER(Integer.valueOf(125)),
	
	RETIREMENT_REDEMPTION_TYPE(Integer.valueOf(560)),

	//Stock Calculation
	
	PARAMETER_PROCESS_STOCK_PRELIMINARY_AUTOMATIC(Integer.valueOf(2310)),
	
	PARAMETER_PROCESS_DEFINITIVE_AUTOMATIC(Integer.valueOf(2311)),
	
	/** The stock calculation type. */
	STOCK_CALCULATION_TYPE(Integer.valueOf(126)),
	
	/** The stock calculation state. */
	STOCK_CALCULATION_STATE(Integer.valueOf(361)),
	
	/** The stock calculation class. */
	STOCK_CALCULATION_CLASS(Integer.valueOf(362)),
	
	/** The corporative process state. */
	CORPORATIVE_PROCESS_STATE(Integer.valueOf(358)),
	
	/** The master table reject motive participant union. */
	MASTER_TABLE_REJECT_MOTIVE_PARTICIPANT_UNION(Integer.valueOf(366)),

	/** The master table reject motive transfer securities available. */
	MASTER_TABLE_REJECT_MOTIVE_TRANSFER_SECURITIES_AVAILABLE(Integer.valueOf(118)),
	
	/** The funds search type operation. */
	FUNDS_SEARCH_TYPE_OPERATION(new Integer(388)),
	//REPORTS
	
	/** The report type. */
	REPORT_TYPE(Integer.valueOf(376)),
	
	/** The report filter type. */
	REPORT_FILTER_TYPE(Integer.valueOf(377)),
	
	/** The report logger state. */
	REPORT_LOGGER_STATE(Integer.valueOf(378)),
	
	/** The report format. */
	REPORT_FORMAT(Integer.valueOf(379)),
	
	/** The unification file type. */
	UNIFICATION_FILE_TYPE(Integer.valueOf(381)),
	
	LOADING_FILE_OPERATIONS_PROCESS_TYPE(Integer.valueOf(382)),
	
	/** The account cash funds type. */
	ACCOUNT_CASH_FUNDS_TYPE(Integer.valueOf(268)),

	/** The funds retirement search operation. */
	FUNDS_RETIREMENT_SEARCH_OPERATION(Integer.valueOf(388)),
	
	/** The funds retirement type. */
	FUNDS_RETIREMENT_TYPE(Integer.valueOf(389)),

	
	/** The rectification operation state. */
	RECTIFICATION_OPERATION_STATE(Integer.valueOf(387)),
	
	/** The rectification operation type. */
	RECTIFICATION_OPERATION_TYPE(Integer.valueOf(390)),
	
	/** The rectification motive. */
	RECTIFICATION_MOTIVE(Integer.valueOf(395)),
	
	/** The rectification reject motive. */
	RECTIFICATION_REJECT_MOTIVE(Integer.valueOf(396)),
	
	/* The investor origin. */
	/** The investor origin. */
	INVESTOR_ORIGIN(Integer.valueOf(394)),
	
	/** The Group Name of Securities. */
	SECURITIES_GROUP_TABLE_REFERENCE(Integer.valueOf(181)),
	
	/** The holder state type. */
	HOLDER_STATE_TYPE(Integer.valueOf(228)),

	/** The transfer type. */
	TRANSFER_TYPE(Integer.valueOf(117)),

	/** The transfer type. */
	TRANSFER_ACTIONS(Integer.valueOf(294)),
	
	/** The security transfer state. */
	SECURITY_TRANSFER_STATE(Integer.valueOf(87)),
	
	/** The affectation type. */
	AFFECTATION_TYPE(Integer.valueOf(323)),
	
	/** The ind stock reference type. */
	IND_STOCK_REFERENCE_TYPE(Integer.valueOf(321)),
	
	/** The ind funds reference type. */
	IND_FUNDS_REFERENCE_TYPE(Integer.valueOf(322)),
	
	/** The ind stock reference type. */
	IND_MARGIN_REFERENCE_TYPE(Integer.valueOf(399)),
	
	/** The incharge settlement state. */
	INCHARGE_SETTLEMENT_STATE(new Integer(263)),
	
	/** The incharge agreement state. */
	INCHARGE_AGREEMENT_STATE(new Integer(573)),
	
	/** The request state. */
	REQUEST_STATE(Integer.valueOf(368)),
	
	/** The allocation benefit process type. */
	ALLOCATION_BENEFIT_PROCESS_TYPE(Integer.valueOf(405)),
	
	/** The amortization modality. */
	INTEREST_PAYMENT_MODALITY(Integer.valueOf(404)),
	
	/** The max days holder request. */
	MAX_DAYS_HOLDER_REQUEST(Integer.valueOf(406)),
	
	// BUSINESS PROCESS
	/** The master table notification type. */
	MASTER_TABLE_NOTIFICATION_TYPE(Integer.valueOf(167)),
	
	/** The master table notification destination type. */
	MASTER_TABLE_NOTIFICATION_DESTINATION_TYPE(Integer.valueOf(166)),
	
	/** The master table margin factor. */
	MASTER_TABLE_MARGIN_FACTOR(Integer.valueOf(416)),
	
	PROGRAM_INTEREST_STATE(Integer.valueOf(369)),
	
	//EXTERNAL INTERFACE

	/** The detail template type. */
	DETAIL_TEMPLATE_TYPE(Integer.valueOf(410)),

	/** The external interface execution type. */
	EXTERNAL_INTERFACE_EXECUTION_TYPE(Integer.valueOf(409)),
	
	/** The external interface state type. */
	EXTERNAL_INTERFACE_STATE_TYPE(Integer.valueOf(412)),
	
	/** The interface template state type. */
	INTERFACE_TEMPLATE_STATE_TYPE(Integer.valueOf(414)),
	
	/** The interface template detail state type. */
	INTERFACE_TEMPLATE_DETAIL_STATE_TYPE(Integer.valueOf(413)),
	
	/** The interface predicate type. */
	INTERFACE_PREDICATE_TYPE(Integer.valueOf(423)),
	
	/** The interface operator logic type. */
	INTERFACE_OPERATOR_LOGIC_TYPE(Integer.valueOf(430)),
	
	/** The sql comparator type. */
	SQL_COMPARATOR_TYPE(Integer.valueOf(431)),
	
	//BILLING SERVICE
	
	/** The master table entity collection. */
	MASTER_TABLE_ENTITY_COLLECTION(Integer.valueOf(171)),

	/** The master table calculation period. */
	MASTER_TABLE_CALCULATION_PERIOD(Integer.valueOf(174)),
	
	/** The master table collection period. */
	MASTER_TABLE_COLLECTION_PERIOD(Integer.valueOf(175)),
	
	/** The master table type service. */
	MASTER_TABLE_TYPE_SERVICE(Integer.valueOf(383)),
	
	/** The master table record state. */
	MASTER_TABLE_RECORD_STATE(Integer.valueOf(402)),
	
	/** The month type. */
	MONTH_TYPE(Integer.valueOf(417)),
	
	/** The month type. */
	BILLING_PROCESSED_STATUS(Integer.valueOf(444)),
	
	/** The external interface column type. */
	EXTERNAL_INTERFACE_COLUMN_TYPE(Integer.valueOf(418)),
	
	/** The max days dematerialization. */
	MAX_DAYS_DEMATERIALIZATION(Integer.valueOf(432)),
	
	/** The max days available securities transfer. */
	MAX_DAYS_AVAILABLE_SECURITIES_TRANSFER(Integer.valueOf(433)),
	
	/** The max days blocked securities transfer. */
	MAX_DAYS_BLOCKED_SECURITIES_TRANSFER(Integer.valueOf(434)),
	
	/** The max days availables ownership exchange. */
	MAX_DAYS_AVAILABLES_OWNERSHIP_EXCHANGE(Integer.valueOf(435)),
	
	/** The max days blocked ownership exchange. */
	MAX_DAYS_BLOCKED_OWNERSHIP_EXCHANGE(Integer.valueOf(436)),
	
	/** The max days availables accreditation certification. */
	MAX_DAYS_AVAILABLES_ACCREDITATION_CERTIFICATION(Integer.valueOf(437)),
	
	/** The max days blocked accreditation certification. */
	MAX_DAYS_BLOCKED_ACCREDITATION_CERTIFICATION(Integer.valueOf(438)),
	
	/** The max days block request. */
	MAX_DAYS_BLOCK_REQUEST(Integer.valueOf(439)),

	/** The accreditation unblock motive. */
	ACCREDITATION_UNBLOCK_MOTIVE(Integer.valueOf(445)),

	/** The tipo solicitud sercurity quote. */
	TIPO_SOLICITUD_SERCURITY_QUOTE(Integer.valueOf(450)),
	
	/** The security quote states. */
	SECURITY_QUOTE_STATES(Integer.valueOf(440)),
	
	/** The external interface send hour. */
	EXTERNAL_INTERFACE_SEND_HOUR(Integer.valueOf(454)),
	
	/** The external interface format date. */
	EXTERNAL_INTERFACE_FORMAT_DATE(Integer.valueOf(456)),
	
	/** The external interface send. */
	EXTERNAL_INTERFACE_SEND(Integer.valueOf(442)),
	
	/** The special payment state. */
	SPECIAL_PAYMENT_STATE(Integer.valueOf(460)),
	
	/** The split coupon state. */
	SPLIT_COUPON_STATE(Integer.valueOf(424)),
	
	SPLIT_CONFIGURATION_TYPE(Integer.valueOf(588)),
	
	/** The securitie term. */
	SECURITIE_TERM(Integer.valueOf(401)),
	
	/** The holder account status. */
	HOLDER_ACCOUNT_REQUEST_STATUS(Integer.valueOf(221)),
	
	/** The account group. */
	ACCOUNT_GROUP_TYPE(Integer.valueOf(219)),
	
	/** The quotation state. */
	QUOTATION_STATE(Integer.valueOf(270)),
	
	/** The quotation delete motive. */
	QUOTATION_DELETE_MOTIVE(Integer.valueOf(468)),
	
	/** The holder account status. */
	HOLDER_ACCOUNT_TYPE_ACTION(Integer.valueOf(222)),
	
	/** The split coupon request motive reject. */
	SPLIT_COUPON_REQUEST_MOTIVE_REJECT(Integer.valueOf(471)),
	
	/**  Pep classification. */
	PEP_CLASSIFICATION(Integer.valueOf(469)),
	
	/** The types balance credit. */
	TYPES_BALANCE_CREDIT(Integer.valueOf(144)),
	
	/**  level sanction to participan. */
	LEVEL_SANCTION_PARTICIPAN(Integer.valueOf(485)),
	
	/**  level reason to sanction. */
	STATE_SANCTION_PARTICIPAN(Integer.valueOf(486)),
	
	/**  level reason to sanction. */
	REASON_SANCTION_PARTICIPAN(Integer.valueOf(487)),
	
	/** The corporative event class. */
	CORPORATIVE_EVENT_CLASS(Integer.valueOf(491)),
	
	/**  level reason to sanction. */
	PERIOD_SANCTION_PARTICIPAN(Integer.valueOf(494)),
	
	/** The massive operation class. */
	MASSIVE_OPERATION_CLASS(Integer.valueOf(496)),
	
	/** The massive operation type. */
	MASSIVE_OPERATION_TYPE(Integer.valueOf(497)),
	
    /** The document emission source. */
    DOCUMENT_SOURCE(Integer.valueOf(504)),
	
	/** The reference of note to external. */
	REFERENCE_NOTE_TO_EXTERNAL(Integer.valueOf(526)),
	
	/** The recipient of note to external. */
	RECIPIENT_NOTE_TO_EXTERNAL(Integer.valueOf(527)),
	
	/** The simulator simulation types. */
	SIMULATOR_SIMULATION_FORMS(Integer.valueOf(524)),
	
	/** The simulator simulation requester user types. */
	SIMULATOR_SIMULATION_REQUESTER_USER_TYPES(Integer.valueOf(523)),
	
	/** The simulator simulation process states. */
	SIMULATOR_SIMULATION_PROCESS_STATES(Integer.valueOf(522)),

	/**VALUATOR PROCESS EXECUTION*/
	
	/** The valuator process execution date types. */
	VALUATOR_PROCESS_EXECUTION_DATE_TYPES(Integer.valueOf(542)),
	
	/** The valuator process execution types. */
	VALUATOR_PROCESS_EXECUTION_TYPES(Integer.valueOf(519)),
	
	/** The valuator process execution states. */
	VALUATOR_PROCESS_EXECUTION_STATES(Integer.valueOf(518)),
	
	/** The valuator process execution date types. */
	VALUATOR_PROCESS_EXECUTION_VALUATOR_TYPES(Integer.valueOf(541)),
	
	/** The valuator process execution states. */
	VALUATOR_EXECUTION_FAILED_MOTIVE(Integer.valueOf(521)),
	
	/** The valuator process execution states. */
	VALUATOR_PROCESS_EXECUTION_DETAIL_OBJECT(Integer.valueOf(520)),
	
	/**VALUATOR PROCESS EXECUTION*/
	
	/** The custody operation state. */
	CUSTODY_OPERATION_STATE(Integer.valueOf(505)),
	
	ACCOUNT_OPERATION_STATE(Integer.valueOf(260)),
	
	REST_MOD_PAYMENT_SCHEDULE_REJECTION_MOTIVE(Integer.valueOf(511)),
	
	ACCREDITACION_CERTIFICATION_MOTIVES(Integer.valueOf(529)),
	
	/**MARKETFACT*/
	MARKETFACT_FILE_TYPE(Integer.valueOf(516)),
	MARKETFACT_FILE_STATE(Integer.valueOf(530)),
	MARKETFACT_PROCESS_TYPE(Integer.valueOf(531)),
	MARKETFACT_FILE_SOURCE(Integer.valueOf(532)),
	MARKETFACT_FILE_LOADING(Integer.valueOf(585)),
	
	VALUATOR_TYPE(Integer.valueOf(514)),
	VALUATOR_INFORMATION_TYPE(Integer.valueOf(515)),
	VALUATOR_SITUATION(Integer.valueOf(513)),
	
	/** Consignation **/
	
	CONSIGNATION_PROCESS_STATE(Integer.valueOf(463)),
	
	/** Encargo negociacion **/
	ASSIGNMENT_REQUEST_MOTIVE_REJECT(Integer.valueOf(539)),
	
	VALORIZATION_TYPE(Integer.valueOf(554)),
	
	ENFORCE_OPERATION_MOTIVE_REJECT(Integer.valueOf(535)),
	
	TRADE_REQUEST_SETTLEMENT_REQUEST(Integer.valueOf(562)),
	
	TRADE_REQUEST_SIRTEX_TERM(Integer.valueOf(564)),
	
	TRADE_REQUEST_SIRTEX_RENEW_STATE(Integer.valueOf(565)),
	
	BLOCK_ENFORCE_STATE(Integer.valueOf(476)),
	
	INTERFACE_RECEPTION_TYPE(Integer.valueOf(561)),
	
	EXTERNAL_INTERFACE_STATE(Integer.valueOf(563)),
	
	STATE_OPERATION_SETTLEMENT_PROCESS(Integer.valueOf(356)),
	
	CIVIL_STATUS(Integer.valueOf(571)),
	
	CONVERTIBLE_STOCK_TYPE(Integer.valueOf(566)),
	
	COUPON_SITUATION(Integer.valueOf(574)),
	
	PROGRAM_SCHEDULE_STATE(Integer.valueOf(369)),
	
	TOTAL_INCOME(Integer.valueOf(572)),
	
	CHAIN_OPERATION_STATE(Integer.valueOf(510)),
	
	RENEWAL_OPERATION_STATE(Integer.valueOf(578)),
	
	RENEWAL_OPERATION_MOTIVE(Integer.valueOf(577)),
	
	DIGITAL_SIGNATURE_TYPE(Integer.valueOf(583)),
	
	/** The retirement reject motive. */
	RETIREMENT_REJECT_MOTIVE(Integer.valueOf(584)),

	 //modifiy entity
	BRANCH_OFFICE(new Integer(1237)),//585	
	PAYROLL_TYPE(new Integer(1238)),	//586	ok
	PAYROLL_HEADER_STATE(new Integer(1239)),//587	ok
	PAYROLL_DETAIL_STATE(new Integer(1240)),//588	ok
	SECURITIES_MANAGER_RESP(new Integer(1241)),//589
	BLOCK_RESTRICTION_TYPE(new Integer(1242)),//590
	VAULTS_FOR_BRANCH(new Integer(1243)),//591
	CERTIFICATE_OPERATIONS(new Integer(1244)),//592
	CERTFICIATE_OPERATION_STATE(new Integer(1245)),//593
	/*
	BRANCH_OFFICE(new Integer(585)),	
	PAYROLL_TYPE(new Integer(586)),	
	PAYROLL_HEADER_STATE(new Integer(587)),	
	PAYROLL_DETAIL_STATE(new Integer(588)),
	SECURITIES_MANAGER_RESP(new Integer(589)),
	BLOCK_RESTRICTION_TYPE(new Integer(590)),
	VAULTS_FOR_BRANCH(new Integer(591)),
	CERTIFICATE_OPERATIONS(new Integer(592)),
	CERTFICIATE_OPERATION_STATE(new Integer(593)),*/
	//modifiy entity
	
	/** The parameter web services lip and send . */
	COSTUMER_SERVICES_LIP_EDV(Integer.valueOf(223)),
	
	REQUEST_REASON_REASSIGNMENT(Integer.valueOf(589)),
	
	PARAMETERS_FOLDERS_FTP(Integer.valueOf(553)),
	
	PHYSICAL_CERT_FILES(Integer.valueOf(1196)),
	
	MOTIVE_CHANNEL_OPENING(Integer.valueOf(1198)),
	
	STATUS_CHANNEL_OPENING(Integer.valueOf(1199)),
	
	MOTIVE_CHANNEL_CLOUSSURE(Integer.valueOf(1201)),
	
	STATES_HOLDER_ANNULATION(Integer.valueOf(1202)),
	
	MOTIVES_HOLDER_ANNULATION(Integer.valueOf(1203)),
	
	FILE_TYPE_HOLDER_ANNULATION(Integer.valueOf(1204)),
	
	CANCEL_REQUEST_HOLDER_ANNULATION(Integer.valueOf(1205)),
	
	STATUS_SECURITY_REMOVED(Integer.valueOf(1207)),
	
	OPEN_GENERAL_ASSIGNMENT(Integer.valueOf(1208)),
	
	ROLE_SIMULATION_POSITION(Integer.valueOf(601)),
	
	TOKKEN_CODE_SAP(Integer.valueOf(595)),
	
	END_POINT_SAP(Integer.valueOf(596)),
	
	EMAIL_FOR_EXTERNAL(Integer.valueOf(1216)),
	
	AUTHORIZED_SIGNATURE_STATE(Integer.valueOf(1214)),

	RESTRICCIONES_OPERACIONES_OTC(Integer.valueOf(1221)),
	
	INVESTOR_TYPE_ECONOMIC_ACTIVITY(Integer.valueOf(1219)),
	
	MOTIVES_INMOVILIZATION_REJECT(Integer.valueOf(1248)),
	
	MOTIVES_FILES_HOLDER_MODIFICATION(Integer.valueOf(1249)),

	DAYS_OF_REVERT_DEP_RET_PHYSICAL(Integer.valueOf(1254)),
	
	STATE_INDPDD(Integer.valueOf(1255)),
	
	CORPORATIVE_CONSIGNATION_PAYMENT_TYPE(Integer.valueOf(1256)),//PDD / PDI
	
	PEP_GRADE(Integer.valueOf(1259)),
	
	ENCODER_AGENT(Integer.valueOf(1260)),
	
	PAYROLL_FILE_TYPE(Integer.valueOf(1261))
	;
	

	/*
	 * 597<- STATE_PHYSICAL_OPERATION
	 * 16 <- MOTIVE_PHYSICAL_OPERATION
	 * */
	
	/** The Constant list. */
	public final static List<MasterTableType> list=new ArrayList<MasterTableType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, MasterTableType> lookup=new HashMap<Integer, MasterTableType>();
	
	static {
		for(MasterTableType c : MasterTableType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The lng code. */
	private Long lngCode;
	
	/** The str code. */
	private String strCode;
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param code the code
	 */
	private MasterTableType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param lngCode the lng code
	 */
	private MasterTableType(Long lngCode){
		this.lngCode = lngCode;
	}
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param code the code
	 */
	private MasterTableType(String code){
		this.strCode=code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the master table type
	 */
	public static MasterTableType get(Integer codigo) {
        return lookup.get(codigo);
    }

	/**
	 * Gets the.
	 *
	 * @param lngCode the lng code
	 * @return the master table type
	 */
	public static MasterTableType get(Long lngCode){
		return lookup.get(new Integer(lngCode.intValue()));
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}

	/**
	 * Gets the lng code.
	 *
	 * @return the lng code
	 */
	public Long getLngCode(){
		return this.lngCode;
	}
	
	/**
	 * Gets the str code.
	 *
	 * @return the strCode
	 */
	public String getStrCode() {
		return strCode;
	}
	
	/**
	 * Sets the str code.
	 *
	 * @param strCode the strCode to set
	 */
	public void setStrCode(String strCode) {
		this.strCode = strCode;
	}
	
}