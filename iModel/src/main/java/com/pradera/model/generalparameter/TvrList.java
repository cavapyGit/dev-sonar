package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.generalparameter.type.SecurityGroupType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;

/**
 * @author PraderaTechnologies
 * 
 */

@Entity
@Table(name = "TVR_LIST")
public class TvrList implements Serializable , Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "TVR_PK_LIST", sequenceName = "SQ_TVR_PK_LIST", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TVR_PK_LIST")
	@Column(name = "ID_GROUP_PK")
	private Integer id_group_pk;

	@Column(name = "GROUP_PERCENT")
	private BigDecimal groupPercent;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifiedDate;

	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifiedApp;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifiedIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifiedUser;

	@Column(name = "SECURITY_CLASS")
	private Integer securityClass;

	@Column(name = "ID_NEGOTIATION_MECHANISM_FK")
	private Long idNegotiationMechFk;

	@Column(name = "ID_NEGOTIATION_MODALITY_FK")
	private Long idNegotiationModFk;

	@Column(name = "ID_LIST_TVR")
	private Integer idListTvr;
	
	@Transient
	private String classTypeDescription;
	
	@Transient
	private Integer securityListValue;
	
	@Transient
	private String securityGroupDescription;

	public TvrList() {

	}

	/**
	 * @return the id_group_pk
	 */
	public Integer getId_group_pk() {
		return id_group_pk;
	}

	/**
	 * @param id_group_pk
	 *            the id_group_pk to set
	 */
	public void setId_group_pk(Integer id_group_pk) {
		this.id_group_pk = id_group_pk;
	}

	/**
	 * @return the groupPercent
	 */
	public BigDecimal getGroupPercent() {
		return groupPercent;
	}

	/**
	 * @param groupPercent
	 *            the groupPercent to set
	 */
	public void setGroupPercent(BigDecimal groupPercent) {
		this.groupPercent = groupPercent;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate
	 *            the lastModifiedDate to set
	 */
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the lastModifiedApp
	 */
	public Integer getLastModifiedApp() {
		return lastModifiedApp;
	}

	/**
	 * @param lastModifiedApp
	 *            the lastModifiedApp to set
	 */
	public void setLastModifiedApp(Integer lastModifiedApp) {
		this.lastModifiedApp = lastModifiedApp;
	}

	/**
	 * @return the lastModifiedIp
	 */
	public String getLastModifiedIp() {
		return lastModifiedIp;
	}

	/**
	 * @param lastModifiedIp
	 *            the lastModifiedIp to set
	 */
	public void setLastModifiedIp(String lastModifiedIp) {
		this.lastModifiedIp = lastModifiedIp;
	}

	/**
	 * @return the lastModifiedUser
	 */
	public String getLastModifiedUser() {
		return lastModifiedUser;
	}

	/**
	 * @param lastModifiedUser
	 *            the lastModifiedUser to set
	 */
	public void setLastModifiedUser(String lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	/**
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * @param securityClass
	 *            the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * @return the idNegotiationMechFk
	 */
	public Long getIdNegotiationMechFk() {
		return idNegotiationMechFk;
	}

	/**
	 * @param idNegotiationMechFk
	 *            the idNegotiationMechFk to set
	 */
	public void setIdNegotiationMechFk(Long idNegotiationMechFk) {
		this.idNegotiationMechFk = idNegotiationMechFk;
	}

	/**
	 * @return the idNegotiationModFk
	 */
	public Long getIdNegotiationModFk() {
		return idNegotiationModFk;
	}

	/**
	 * @param idNegotiationModFk
	 *            the idNegotiationModFk to set
	 */
	public void setIdNegotiationModFk(Long idNegotiationModFk) {
		this.idNegotiationModFk = idNegotiationModFk;
	}

	/**
	 * @return the idListTvr
	 */
	public Integer getIdListTvr() {
		return idListTvr;
	}

	/**
	 * @param idListTvr
	 *            the idListTvr to set
	 */
	public void setIdListTvr(Integer idListTvr) {
		this.idListTvr = idListTvr;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifiedApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifiedDate = loggerUser.getAuditTime();
            lastModifiedIp = loggerUser.getIpAddress();
            lastModifiedUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>(); 
        return detailsMap;
	}
	
	/**
	 * Get the Security class description
	 * 
	 * @return the String type
	 */
	public String getClassTypeDescription() {
		if(Validations.validateIsNotNullAndPositive(securityClass)){
			classTypeDescription = SecurityClassType.get(securityClass).getValue();
			return classTypeDescription;
		}
		return "";
	}

	/**
	 * @param classTypeDescription the classTypeDescription to set
	 */
	public void setClassTypeDescription(String classTypeDescription) {
		this.classTypeDescription = classTypeDescription;
	}

	/**
	 * @return the securityListValue
	 */
	public Integer getSecurityListValue() {
		return securityListValue;
	}

	/**
	 * @param securityListValue the securityListValue to set
	 */
	public void setSecurityListValue(Integer securityListValue) {
		this.securityListValue = securityListValue;
	}

	/**
	 * Get the Security Group description
	 * 
	 * @return the securityGroupDescription
	 */
	public String getSecurityGroupDescription() {
		if(Validations.validateIsNotNullAndPositive(idListTvr)){
			this.securityGroupDescription = SecurityGroupType.get(idListTvr).getValue();
		}
		return securityGroupDescription;
	}

	
	

	
}
