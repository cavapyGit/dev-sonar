package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="REQUEST_DAILY_EXCHANGE_RATES")
public class RequestDailyExchangeRates implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="REQ_DAILY_EXCHANGERATES_DAILY_EXCHANGE_PK_GENERATOR", sequenceName="SQ_ID_REQ_DAILY_EXCHANGE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REQ_DAILY_EXCHANGERATES_DAILY_EXCHANGE_PK_GENERATOR")
	
	@Column(name="ID_REQ_DAILY_EXCHANGE_PK")
	private Integer idRequestDailyExchangepk;
	
	@Temporal( TemporalType.DATE)
	@Column(name="DATE_RATE")
	private Date dateRate;
	
	@Column(name="ID_CURRENCY")
	private Integer idCurrency;
	
	@Transient
	private String currencyDescription;
	
	@Column(name="ID_SOURCE_INFORMATION")
	private Integer idSourceinformation;

	@Column(name="SELL_PRICE")
	private BigDecimal sellPrice;

	@Column(name="BUY_PRICE")
	private BigDecimal buyPrice;

	@Column(name="AVERAGE_PRICE")
	private BigDecimal averagePrice;
	
	@Column(name="IND_REFERENCE_PRICE")
	private Integer indReferencePrice;
	
	@Column(name="REFERENCE_PRICE")
	private BigDecimal referencePrice;
	
	@Column(name="EXCHANGE_STATE", precision=10)
	private Integer exchangeState;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="CONFIRM_USER")
	private String confirmUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;
	
	
	/**
	 * @return the idRequestDailyExchangepk
	 */
	public Integer getIdRequestDailyExchangepk() {
		return idRequestDailyExchangepk;
	}
	/**
	 * @param idRequestDailyExchangepk the idRequestDailyExchangepk to set
	 */
	public void setIdRequestDailyExchangepk(Integer idRequestDailyExchangepk) {
		this.idRequestDailyExchangepk = idRequestDailyExchangepk;
	}
	/**
	 * @return the dateRate
	 */
	public Date getDateRate() {
		return dateRate;
	}
	/**
	 * @param dateRate the dateRate to set
	 */
	public void setDateRate(Date dateRate) {
		this.dateRate = dateRate;
	}
	/**
	 * @return the idCurrency
	 */
	public Integer getIdCurrency() {
		return idCurrency;
	}
	/**
	 * @param idCurrency the idCurrency to set
	 */
	public void setIdCurrency(Integer idCurrency) {
		this.idCurrency = idCurrency;
	}
	/**
	 * @return the currencyDescription
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}
	/**
	 * @param currencyDescription the currencyDescription to set
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	/**
	 * @return the idSourceinformation
	 */
	public Integer getIdSourceinformation() {
		return idSourceinformation;
	}
	/**
	 * @param idSourceinformation the idSourceinformation to set
	 */
	public void setIdSourceinformation(Integer idSourceinformation) {
		this.idSourceinformation = idSourceinformation;
	}
	/**
	 * @return the sellPrice
	 */
	public BigDecimal getSellPrice() {
		return sellPrice;
	}
	/**
	 * @param sellPrice the sellPrice to set
	 */
	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}
	/**
	 * @return the buyPrice
	 */
	public BigDecimal getBuyPrice() {
		return buyPrice;
	}
	/**
	 * @param buyPrice the buyPrice to set
	 */
	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}
	/**
	 * @return the averagePrice
	 */
	public BigDecimal getAveragePrice() {
		return averagePrice;
	}
	/**
	 * @param averagePrice the averagePrice to set
	 */
	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}
	/**
	 * @return the indReferencePrice
	 */
	public Integer getIndReferencePrice() {
		return indReferencePrice;
	}
	/**
	 * @param indReferencePrice the indReferencePrice to set
	 */
	public void setIndReferencePrice(Integer indReferencePrice) {
		this.indReferencePrice = indReferencePrice;
	}
	/**
	 * @return the referencePrice
	 */
	public BigDecimal getReferencePrice() {
		return referencePrice;
	}
	/**
	 * @param referencePrice the referencePrice to set
	 */
	public void setReferencePrice(BigDecimal referencePrice) {
		this.referencePrice = referencePrice;
	}
	/**
	 * @return the exchangeState
	 */
	public Integer getExchangeState() {
		return exchangeState;
	}
	/**
	 * @param exchangeState the exchangeState to set
	 */
	public void setExchangeState(Integer exchangeState) {
		this.exchangeState = exchangeState;
	}
	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	/**
	 * @return the registryUser
	 */
	public String getRegistryUser() {
		return registryUser;
	}
	/**
	 * @param registryUser the registryUser to set
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	/**
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	/**
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	/**
	 * @return the confirmUser
	 */
	public String getConfirmUser() {
		return confirmUser;
	}
	/**
	 * @param confirmUser the confirmUser to set
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}
	/**
	 * @return the confirmDate
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}
	/**
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
