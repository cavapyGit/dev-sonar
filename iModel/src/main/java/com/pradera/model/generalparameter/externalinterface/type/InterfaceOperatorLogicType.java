package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum InterfaceOperatorLogicType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 13-nov-2013
 */
public enum InterfaceOperatorLogicType {
		
	/** The automatic. */
	AND(Integer.valueOf(1614),"AND"),
	
	/** The boot. */
	OR(Integer.valueOf(1615),"OR");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<InterfaceOperatorLogicType> list=new ArrayList<InterfaceOperatorLogicType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, InterfaceOperatorLogicType> lookup=new HashMap<Integer, InterfaceOperatorLogicType>();

	static {
		for(InterfaceOperatorLogicType c : InterfaceOperatorLogicType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InterfaceOperatorLogicType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the interface operator logic type
	 */
	public static InterfaceOperatorLogicType get(Integer code) {
		return lookup.get(code);
	}
}