package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.constants.Constants;
import com.pradera.model.generalparameter.type.ParameterTableStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParameterTable.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@Entity
@Cacheable(true)
@Table(name="PARAMETER_TABLE")
@NamedQueries({
	@NamedQuery(name = ParameterTable.FIND_NAME_BY_PK, query = " select param from ParameterTable param left join fetch param.relatedParameter  where param.parameterTablePk = :pk"),
	@NamedQuery(name = ParameterTable.FIND_BCB_CODE_CURRENCY, query = " select p.shortInteger from ParameterTable p where p.parameterTablePk = :pk")
})
public class ParameterTable implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	public static final String FIND_NAME_BY_PK = "findNameByPk";
	
	public static final String FIND_BCB_CODE_CURRENCY = "findBcbCodeCurrency";
	
	/** The parameter table pk. */
	@Id
	// No existe tal secuancia en la BBDD, ademas los insert se hacen desde SQL, por eso se comenta
	//@SequenceGenerator(name="PARAMETER_TABLE_PARAMETERTABLEPK_GENERATOR", sequenceName="SQ_PARAMETER_TABLE_PK",initialValue=1,allocationSize=1)
	//@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARAMETER_TABLE_PARAMETERTABLEPK_GENERATOR")
	@Column(name="PARAMETER_TABLE_PK")
	private Integer parameterTablePk;

	/** The date1. */
	@Temporal(TemporalType.TIMESTAMP)
	private Date date1;

	/** The date2. */
	@Temporal(TemporalType.DATE)
	private Date date2;

	/** The decimal amount1. */
	@Column(name="DECIMAL_AMOUNT1")
	private Double decimalAmount1;

	/** The decimal amount2. */
	@Column(name="DECIMAL_AMOUNT2")
	private Double decimalAmount2;

	/** The description. */
	private String description;

	/** The element subclasification1. */
	@Column(name="ELEMENT_SUBCLASIFICATION1")
	private Integer elementSubclasification1;

	/** The element subclasification2. */
	@Column(name="ELEMENT_SUBCLASIFICATION2")
	private Integer elementSubclasification2;

	/** The indicator1. */
	private String indicator1;

	/** The indicator2. */
	private String indicator2;

	/** The indicator3. */
	private String indicator3;

	/** The indicator4. */
	private Integer indicator4;

	/** The indicator5. */
	private Integer indicator5;

	/** The indicator6. */
	private Integer indicator6;
	
	
	//bi-directional many-to-one association to ParameterTable
    /** The related parameter. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RELATED_PARAMETER_TABLE_FK",referencedColumnName="PARAMETER_TABLE_PK")
	private ParameterTable relatedParameter;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The long integer. */
	@Column(name="LONG_INTEGER")
	private Long longInteger;

	/** The observation. */
	@Column(name="COMMENTS")
	private String observation;

	/** The parameter name. */
	@Column(name="PARAMETER_NAME")
	private String parameterName;

	/** The parameter table cd. */
	@Column(name="PARAMETER_TABLE_CD")
	private String parameterTableCd;

	/** The short integer. */
	@Column(name="SHORT_INTEGER")
	private Integer shortInteger;

	/** The state. */
	@Column(name="PARAMETER_STATE")
	private Integer parameterState;

	/** The text1. */
	@Column(name="TEXT1")
	private String text1;

	/** The text2. */
	@Column(name="TEXT2")
	private String text2;

	/** The text3. */
	@Column(name="TEXT3")
	private String text3;

	//bi-directional many-to-one association to MasterTable
	/** The master table. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MASTER_TABLE_FK")
	private MasterTable masterTable;

	/**
	 * Instantiates a new parameter table.
	 */
	public ParameterTable() {
	}
	
	public ParameterTable(Integer masterTablePk, Integer parameterTablePk,
			String description, String parameterName, Integer parameterState,
			String indicator2, String indicator1, String text1, String text2,
			String text3, String observation, Integer elementSubclasification1,
			Integer indicator4, Integer indicator5, Integer indicator6,
			Integer shortInteger) {
		super();
		this.masterTable = new MasterTable(masterTablePk);
		this.parameterTablePk = parameterTablePk;
		this.description = description;
		this.parameterName = parameterName;
		this.parameterState = parameterState;
		this.indicator2 = indicator2;
		this.indicator1 = indicator1;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.observation = observation;
		this.elementSubclasification1 = elementSubclasification1;
		this.indicator4 = indicator4;
		this.indicator5 = indicator5;
		this.indicator6 = indicator6;
		this.shortInteger = shortInteger;
	}
	
	public ParameterTable(Integer masterTablePk, Integer parameterTablePk,
			String description, String parameterName, Integer parameterState,
			String indicator2, String indicator1, String text1, String text2,
			String text3, String observation, Integer elementSubclasification1,
			Integer indicator4, Integer indicator5, Integer indicator6,
			Integer shortInteger,Long longInteger) {
		super();
		this.masterTable = new MasterTable(masterTablePk);
		this.parameterTablePk = parameterTablePk;
		this.description = description;
		this.parameterName = parameterName;
		this.parameterState = parameterState;
		this.indicator2 = indicator2;
		this.indicator1 = indicator1;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.observation = observation;
		this.elementSubclasification1 = elementSubclasification1;
		this.indicator4 = indicator4;
		this.indicator5 = indicator5;
		this.indicator6 = indicator6;
		this.shortInteger = shortInteger;
		this.longInteger  =	longInteger;
	}

	public ParameterTable(Integer parameterTablePk) {
		super();
		this.parameterTablePk = parameterTablePk;
	}

	public ParameterTable(Integer parameterTablePk, String parameterName) {
		super();
		this.parameterTablePk = parameterTablePk;
		this.parameterName = parameterName;
	}


	/**
	 * Gets the parameter table pk.
	 *
	 * @return the parameter table pk
	 */
	public Integer getParameterTablePk() {
		return parameterTablePk;
	}

	/**
	 * Sets the parameter table pk.
	 *
	 * @param parameterTablePk the new parameter table pk
	 */
	public void setParameterTablePk(Integer parameterTablePk) {
		this.parameterTablePk = parameterTablePk;
	}

	/**
	 * Gets the date1.
	 *
	 * @return the date1
	 */
	public Date getDate1() {
		return date1;
	}

	/**
	 * Sets the date1.
	 *
	 * @param date1 the new date1
	 */
	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	/**
	 * Gets the date2.
	 *
	 * @return the date2
	 */
	public Date getDate2() {
		return date2;
	}

	/**
	 * Sets the date2.
	 *
	 * @param date2 the new date2
	 */
	public void setDate2(Date date2) {
		this.date2 = date2;
	}

	/**
	 * Gets the decimal amount1.
	 *
	 * @return the decimal amount1
	 */
	public Double getDecimalAmount1() {
		return decimalAmount1;
	}

	/**
	 * Sets the decimal amount1.
	 *
	 * @param decimalAmount1 the new decimal amount1
	 */
	public void setDecimalAmount1(Double decimalAmount1) {
		this.decimalAmount1 = decimalAmount1;
	}

	/**
	 * Gets the decimal amount2.
	 *
	 * @return the decimal amount2
	 */
	public Double getDecimalAmount2() {
		return decimalAmount2;
	}

	/**
	 * Sets the decimal amount2.
	 *
	 * @param decimalAmount2 the new decimal amount2
	 */
	public void setDecimalAmount2(Double decimalAmount2) {
		this.decimalAmount2 = decimalAmount2;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Gets the description upper format.
	 * this method is useful to convert Upper String to format like "Xxxx Yyyyy Zzzzz" 
	 * @return the description upper format
	 */
	public String getDescriptionUpperFormat() {
		char[] caracteres = description.toLowerCase().toCharArray();
		caracteres[0] = Character.toUpperCase(caracteres[0]);
		for (int i = 0; i < description.length()- 2; i++){
			if (caracteres[i] == ' ' || caracteres[i] == '.' || caracteres[i] == ','){
		    	caracteres[i + 1] = Character.toUpperCase(caracteres[i + 1]);
		    }
		}
		return new String(caracteres);
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the element subclasification1.
	 *
	 * @return the element subclasification1
	 */
	public Integer getElementSubclasification1() {
		return elementSubclasification1;
	}

	/**
	 * Sets the element subclasification1.
	 *
	 * @param elementSubclasification1 the new element subclasification1
	 */
	public void setElementSubclasification1(Integer elementSubclasification1) {
		this.elementSubclasification1 = elementSubclasification1;
	}

	/**
	 * Gets the element subclasification2.
	 *
	 * @return the element subclasification2
	 */
	public Integer getElementSubclasification2() {
		return elementSubclasification2;
	}

	/**
	 * Sets the element subclasification2.
	 *
	 * @param elementSubclasification2 the new element subclasification2
	 */
	public void setElementSubclasification2(Integer elementSubclasification2) {
		this.elementSubclasification2 = elementSubclasification2;
	}

	/**
	 * Gets the indicator1.
	 *
	 * @return the indicator1
	 */
	public String getIndicator1() {
		return indicator1;
	}

	/**
	 * Sets the indicator1.
	 *
	 * @param indicator1 the new indicator1
	 */
	public void setIndicator1(String indicator1) {
		this.indicator1 = indicator1;
	}

	/**
	 * Gets the indicator2.
	 *
	 * @return the indicator2
	 */
	public String getIndicator2() {
		return indicator2;
	}

	/**
	 * Sets the indicator2.
	 *
	 * @param indicator2 the new indicator2
	 */
	public void setIndicator2(String indicator2) {
		this.indicator2 = indicator2;
	}

	/**
	 * Gets the indicator3.
	 *
	 * @return the indicator3
	 */
	public String getIndicator3() {
		return indicator3;
	}

	/**
	 * Sets the indicator3.
	 *
	 * @param indicator3 the new indicator3
	 */
	public void setIndicator3(String indicator3) {
		this.indicator3 = indicator3;
	}

	/**
	 * Gets the indicator4.
	 *
	 * @return the indicator4
	 */
	public Integer getIndicator4() {
		return indicator4;
	}

	/**
	 * Sets the indicator4.
	 *
	 * @param indicator4 the new indicator4
	 */
	public void setIndicator4(Integer indicator4) {
		this.indicator4 = indicator4;
	}

	/**
	 * Gets the indicator5.
	 *
	 * @return the indicator5
	 */
	public Integer getIndicator5() {
		return indicator5;
	}

	/**
	 * Sets the indicator5.
	 *
	 * @param indicator5 the new indicator5
	 */
	public void setIndicator5(Integer indicator5) {
		this.indicator5 = indicator5;
	}

	/**
	 * Gets the indicator6.
	 *
	 * @return the indicator6
	 */
	public Integer getIndicator6() {
		return indicator6;
	}

	/**
	 * Sets the indicator6.
	 *
	 * @param indicator6 the new indicator6
	 */
	public void setIndicator6(Integer indicator6) {
		this.indicator6 = indicator6;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the long integer.
	 *
	 * @return the long integer
	 */
	public Long getLongInteger() {
		return longInteger;
	}

	/**
	 * Sets the long integer.
	 *
	 * @param longInteger the new long integer
	 */
	public void setLongInteger(Long longInteger) {
		this.longInteger = longInteger;
	}

	/**
	 * Gets the observation.
	 *
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}

	/**
	 * Sets the observation.
	 *
	 * @param observation the new observation
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

	/**
	 * Gets the parameter name.
	 *
	 * @return the parameter name
	 */
	public String getParameterName() {
		return parameterName;
	}

	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the new parameter name
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	/**
	 * Gets the parameter table cd.
	 *
	 * @return the parameter table cd
	 */
	public String getParameterTableCd() {
		return parameterTableCd;
	}

	/**
	 * Sets the parameter table cd.
	 *
	 * @param parameterTableCd the new parameter table cd
	 */
	public void setParameterTableCd(String parameterTableCd) {
		this.parameterTableCd = parameterTableCd;
	}

	/**
	 * Gets the short integer.
	 *
	 * @return the short integer
	 */
	public Integer getShortInteger() {
		return shortInteger;
	}

	/**
	 * Sets the short integer.
	 *
	 * @param shortInteger the new short integer
	 */
	public void setShortInteger(Integer shortInteger) {
		this.shortInteger = shortInteger;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getParameterState() {
		return parameterState;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setParameterState(Integer state) {
		this.parameterState = state;
	}

	/**
	 * Gets the text1.
	 *
	 * @return the text1
	 */
	public String getText1() {
		return text1;
	}

	/**
	 * Sets the text1.
	 *
	 * @param text1 the new text1
	 */
	public void setText1(String text1) {
		this.text1 = text1;
	}

	/**
	 * Gets the text2.
	 *
	 * @return the text2
	 */
	public String getText2() {
		return text2;
	}

	/**
	 * Sets the text2.
	 *
	 * @param text2 the new text2
	 */
	public void setText2(String text2) {
		this.text2 = text2;
	}

	/**
	 * Gets the text3.
	 *
	 * @return the text3
	 */
	public String getText3() {
		return text3;
	}

	/**
	 * Sets the text3.
	 *
	 * @param text3 the new text3
	 */
	public void setText3(String text3) {
		this.text3 = text3;
	}

	/**
	 * Gets the master table.
	 *
	 * @return the master table
	 */
	public MasterTable getMasterTable() {
		return masterTable;
	}

	/**
	 * Sets the master table.
	 *
	 * @param masterTable the new master table
	 */
	public void setMasterTable(MasterTable masterTable) {
		this.masterTable = masterTable;
	}

	/**
	 * Gets the related parameter.
	 *
	 * @return the related parameter
	 */
	public ParameterTable getRelatedParameter() {
		return relatedParameter;
	}

	/**
	 * Sets the related parameter.
	 *
	 * @param relatedParameter the new related parameter
	 */
	public void setRelatedParameter(ParameterTable relatedParameter) {
		this.relatedParameter = relatedParameter;
	}
	
	public String getStatusDescripction(){
		String descripcion = null;
		  if (parameterState != null) {
			  if(ParameterTableStateType .get(parameterState) != null)
		      descripcion = ParameterTableStateType.get(parameterState).getValue();
		  }
		  return descripcion;
	}
	
	public String getDescriptionParameterNameFormat(){
		String formatResult = null;
		if(description!=null && parameterName!=null){
			formatResult = description.concat(Constants.DASH).concat(parameterName);
		}
		
		return formatResult;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((parameterTablePk == null) ? 0 : parameterTablePk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParameterTable other = (ParameterTable) obj;
		if (parameterTablePk == null) {
			if (other.parameterTablePk != null)
				return false;
		} else if (!parameterTablePk.equals(other.parameterTablePk))
			return false;
		return true;
	}

}