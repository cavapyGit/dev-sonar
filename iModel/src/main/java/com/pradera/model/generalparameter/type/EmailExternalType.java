package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public enum EmailExternalType {

	THOMPSON(Integer.valueOf(2560),"EMAIL PARA THOMPONN"),
	
	;

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private EmailExternalType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<EmailExternalType> list = new ArrayList<EmailExternalType>();
	public static final Map<Integer, EmailExternalType> lookup = new HashMap<Integer, EmailExternalType>();
	static {
		for (EmailExternalType s : EnumSet.allOf(EmailExternalType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	public static EmailExternalType get(Integer code) {
		return lookup.get(code);
	}
}
