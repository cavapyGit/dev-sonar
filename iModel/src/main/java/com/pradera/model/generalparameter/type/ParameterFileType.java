package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum MasterTableType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/06/2013
 */
public enum ParameterFileType {
	
	/** MASTER_TABLE 1196 ARCHIVOS PARA DEPOSITO DE TITULOS FISICOS */
	TITULO_FISICO(Integer.valueOf(2457)),
	NUMERO_IDENTIFICACION_TRIBUTARIA(Integer.valueOf(2458)),
	MATRICULA_COMERCIO(Integer.valueOf(2459)),
	CEDULA_IDENTIDAD(Integer.valueOf(2460)),
	CERTIFICACION_ASFI(Integer.valueOf(2461)),
	CRONOGRAMA_CUPONES(Integer.valueOf(2462)),
	OTROS(Integer.valueOf(2463)),
	
	;
	
	/** The Constant list. */
	public final static List<ParameterFileType> list=new ArrayList<ParameterFileType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, ParameterFileType> lookup=new HashMap<Integer, ParameterFileType>();
	
	static {
		for(ParameterFileType c : ParameterFileType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The str code. */
	private String strCode;
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param code the code
	 */
	private ParameterFileType(Integer code) {
		this.code = code;
	}
	
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param code the code
	 */
	private ParameterFileType(String code){
		this.strCode=code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the master table type
	 */
	public static ParameterFileType get(Integer codigo) {
        return lookup.get(codigo);
    }

	/**
	 * Gets the.
	 *
	 * @param lngCode the lng code
	 * @return the master table type
	 */
	public static ParameterFileType get(Long lngCode){
		return lookup.get(new Integer(lngCode.intValue()));
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}

	/**
	 * Gets the str code.
	 *
	 * @return the strCode
	 */
	public String getStrCode() {
		return strCode;
	}
	
	/**
	 * Sets the str code.
	 *
	 * @param strCode the strCode to set
	 */
	public void setStrCode(String strCode) {
		this.strCode = strCode;
	}
	
}