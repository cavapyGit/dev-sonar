package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;




/**
 * Copyright PraderaTechnologies 2013.
 * The persistent class for the HOLIDAY database table.
 * The Class Holiday.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/02/2013
 */
@NamedQueries({
	@NamedQuery(name = Holiday.HOLIDAY_SEARCH_BY_STATE, query = " Select H From Holiday H Where H.state=:state ")
})
@Entity
@Table(name="BUSINESS_HOLIDAY")
public class Holiday implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String HOLIDAY_SEARCH_BY_STATE="Holiday.searchByState";

	/** Constructor */
	public Holiday() {
    }
	
	/** The holiday table pk.*/
	@Id
	@SequenceGenerator(name="HOLIDAY_PK_GENERATOR", sequenceName="SQ_ID_HOLIDAY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLIDAY_PK_GENERATOR")
	@Column(name="ID_HOLIDAY_PK")
	private Integer idHolidayPk;

	/** The Holiday Date.*/
    @Temporal(TemporalType.DATE)
	@Column(name="DATE_HOLIDAY")
	private Date holidayDate;
	
	/** The Registry User. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The Registry Date. */
	@Temporal(TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The Last Modify User. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The Last Modify Date.*/
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	/** The Last Modify IP. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The Last Modify APP. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The Description. */
	@Column(name="DESCRIPTION")
	private String description;
	
	/** The State. */
	@Column(name="STATE")
	private Integer state;
	
	/** The Geographic Location Foreign Key. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_GEOGRAPHIC_LOCATION_FK")
	private GeographicLocation geographicLocation;
	
	
	/** The Holiday Type. 
	 *  HolidayType is coming from behavior of HolidayMgmtBean.java  
	 * */
	@NotNull
	@Column(name="HOLIDAY_TYPE")
	private Integer holidayType;

	
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	
	/**
	 * @return the idHolidayPk
	 */
	public Integer getIdHolidayPk() {
		return idHolidayPk;
	}


	/**
	 * @param idHolidayPk the idHolidayPk to set
	 */
	public void setIdHolidayPk(Integer idHolidayPk) {
		this.idHolidayPk = idHolidayPk;
	}


	/**
	 * @return the holidayDate
	 */
	public Date getHolidayDate() {
		return holidayDate;
	}


	/**
	 * @param holidayDate the holidayDate to set
	 */
	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}


	/**
	 * @return the registryUser
	 */
	public String getRegistryUser() {
		return registryUser;
	}


	/**
	 * @param registryUser the registryUser to set
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}


	/**
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}


	/**
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}


	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}


	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}


	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}


	/**
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * @return the geographicLocation
	 */
	public GeographicLocation getGeographicLocation() {
		return geographicLocation;
	}


	/**
	 * @param geographicLocation the geographicLocation to set
	 */
	public void setGeographicLocation(GeographicLocation geographicLocation) {
		this.geographicLocation = geographicLocation;
	}


	/**
	 * @return the holidayType
	 */
	public Integer getHolidayType() {
		return holidayType;
	}


	/**
	 * @param holidayType the holidayType to set
	 */
	public void setHolidayType(Integer holidayType) {
		this.holidayType = holidayType;
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
