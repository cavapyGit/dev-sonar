package com.pradera.model.generalparameter;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;


/**
 * The persistent class for the GEOGRAPHIC_LOCATION database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name = GeographicLocation.FIND_BY_CODE_GEO_LOC, query = "Select g From GeographicLocation g WHERE g.codeGeographicLocation=:codeGeographicLocPrm and g.typeGeographicLocation = :typeGeographicLocPrm ")
})
@Entity
@Table(name="GEOGRAPHIC_LOCATION")
public class GeographicLocation implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;

	public static final String  FIND_BY_CODE_GEO_LOC= "GeographicLocation.findByCodeGeoLoc";
	
	@Id
	@SequenceGenerator(name="GEOGRAPHIC_LOCATION_IDGEOGRAPHICLOCATIONPK_GENERATOR", sequenceName="SQ_ID_GEOGRAPHIC_LOCATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GEOGRAPHIC_LOCATION_IDGEOGRAPHICLOCATIONPK_GENERATOR")
	@Column(name="ID_GEOGRAPHIC_LOCATION_PK")
	private Integer idGeographicLocationPk;

	@Column(name="CODE_GEOGRAPHIC_LOCATION")
	private String codeGeographicLocation;

	@Column(name="CODE_SPP")
	private String codeSpp;

	@Column(name="CODE_UBIGEO")
	private String codeUbigueo;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	private String name;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	private Integer state;

	@Column(name="TYPE_GEOGRAPHIC_LOCATION")
	private Integer typeGeographicLocation;
	
	@Column(name="IND_LOCAL_COUNTRY")
	private Integer localCountry;

	//bi-directional many-to-one association to GeographicLocation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LOCATION_REFERENCE_FK")
	private GeographicLocation geographicLocation;

	//bi-directional many-to-one association to GeographicLocation
	@OneToMany(fetch=FetchType.LAZY, mappedBy="geographicLocation")
	private List<GeographicLocation> geographicLocations;

	@Transient
	private Boolean isSelected;

    public GeographicLocation() {
    }
    
	public GeographicLocation(Integer idGeographicLocationPk) {
		super();
		this.idGeographicLocationPk = idGeographicLocationPk;
	}



	public Integer getIdGeographicLocationPk() {
		return this.idGeographicLocationPk;
	}

	public void setIdGeographicLocationPk(Integer idGeographicLocationPk) {
		this.idGeographicLocationPk = idGeographicLocationPk;
	}

	public String getCodeGeographicLocation() {
		return this.codeGeographicLocation;
	}

	public void setCodeGeographicLocation(String codeGeographicLocation) {
		if(codeGeographicLocation!=null){
			codeGeographicLocation = codeGeographicLocation.toUpperCase();
		}
		this.codeGeographicLocation = codeGeographicLocation;
	}

	public String getCodeSpp() {
		return this.codeSpp;
	}

	public void setCodeSpp(String codeSpp) {
		if(codeSpp!=null){
			codeSpp = codeSpp.toUpperCase();
		}
		this.codeSpp = codeSpp;
	}

	public String getCodeUbigueo() {
		return this.codeUbigueo;
	}

	public void setCodeUbigueo(String codeUbigueo) {
		if(codeUbigueo!=null){
			codeUbigueo = codeUbigueo.toUpperCase();
		}
		this.codeUbigueo = codeUbigueo;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		if(name!=null){
			name=name.toUpperCase();
		}
		this.name = name;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getTypeGeographicLocation() {
		return this.typeGeographicLocation;
	}

	public void setTypeGeographicLocation(Integer typeGeographicLocation) {
		this.typeGeographicLocation = typeGeographicLocation;
	}

	public GeographicLocation getGeographicLocation() {
		return this.geographicLocation;
	}

	public void setGeographicLocation(GeographicLocation geographicLocation) {
		this.geographicLocation = geographicLocation;
	}
	
	public List<GeographicLocation> getGeographicLocations() {
		return this.geographicLocations;
	}

	public void setGeographicLocations(List<GeographicLocation> geographicLocations) {
		this.geographicLocations = geographicLocations;
	}
	
	/**
     * Gets the geographicLocation state description.
     *
     * @return the geographicLocation state description
     */
    public String getGeographiclocationStateDescription() {
		String description = null;
		if (state != null) {
			description = GeographicLocationStateType.lookup.get(state).getValue();
		}
		return description;
	}
    
    /**
     * Gets the geographicLocation type description.
     * 
     * @return the geographicLocation type description 
     */
    public String getGeographiclocationTypeDescription() {
    	String description = null;
		if (typeGeographicLocation != null) {
			description = GeographicLocationType.lookup.get(typeGeographicLocation).getValue();
		}
		return description;
    }

	/**
	 * @return the localCountry
	 */
	public Integer getLocalCountry() {
		return localCountry;
	}

	/**
	 * @param localCountry the localCountry to set
	 */
	public void setLocalCountry(Integer localCountry) {
		this.localCountry = localCountry;
	}
	
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("geographicLocations", geographicLocations);
        return detailsMap;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
}