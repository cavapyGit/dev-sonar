
package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the EXTENSION_FILE_TYPE database table.
 * 
 */
@Entity
@Table(name="EXTENSION_FILE_TYPE")
public class ExtensionFileType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EXTENSION_FILE_TYPE_IDEXTENSIONFILETYPEPK_GENERATOR", sequenceName="SQ_ID_EXTENSION_FILE_TYPE_PK" ,initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTENSION_FILE_TYPE_IDEXTENSIONFILETYPEPK_GENERATOR")
	@Column(name="ID_EXTENSION_FILE_TYPE_PK", unique=true, nullable=false, precision=10)
	private Long idExtensionFileTypePk;

	@Column(name="EXTENSION_FILE", nullable=false, length=20)
	private String extensionFile;

	@Column(name="EXTENSION_FILE_STATE", nullable=false, precision=10)
	private Integer extensionFileState;

	@Column(name="LAST_MODIFY_APP", nullable=false, precision=10)
	private Long lastModifyApp;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP", nullable=false, length=20)
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER", nullable=false, length=20)
	private String lastModifyUser;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	//bi-directional many-to-one association to ExternalInterfaceExtension
	@OneToMany(mappedBy="extensionFileType")
	private List<ExternalInterfaceExtension> externalInterfaceExtensions;
	
	/** The is selected. */
	@Transient
	private Boolean isSelected;

	public ExtensionFileType() {
		
    }
    
    public ExtensionFileType(Long idExtensionFileTypePk) {
    	this.idExtensionFileTypePk = idExtensionFileTypePk;
    }

	public Long getIdExtensionFileTypePk() {
		return this.idExtensionFileTypePk;
	}

	public void setIdExtensionFileTypePk(Long idExtensionFileTypePk) {
		this.idExtensionFileTypePk = idExtensionFileTypePk;
	}

	public String getExtensionFile() {
		return this.extensionFile;
	}

	public void setExtensionFile(String extensionFile) {
		this.extensionFile = extensionFile;
	}

	public Integer getExtensionFileState() {
		return this.extensionFileState;
	}

	public void setExtensionFileState(Integer extensionFileState) {
		this.extensionFileState = extensionFileState;
	}

	public Long getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public List<ExternalInterfaceExtension> getExternalInterfaceExtensions() {
		return this.externalInterfaceExtensions;
	}

	public void setExternalInterfaceExtensions(List<ExternalInterfaceExtension> externalInterfaceExtensions) {
		this.externalInterfaceExtensions = externalInterfaceExtensions;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}	
}