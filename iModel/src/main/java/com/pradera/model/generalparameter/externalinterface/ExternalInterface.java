package com.pradera.model.generalparameter.externalinterface;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.report.Report;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the EXTERNAL_INTERFACE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-nov-2013
 */
@Entity
@NamedQueries({
	@NamedQuery(
			name=ExternalInterface.INTERFACE_GET_BY_NAME, 
			query=" select ei from ExternalInterface ei where ei.interfaceName = :interfaceName "),
	@NamedQuery(
		name = ExternalInterface.INTERFACE_GET, 
		query = "Select new com.pradera.model.generalparameter.externalinterface.ExternalInterface(int.idExtInterfacePk,int.sendPeriocity) " +
				"From ExternalInterface int " +
				"Where int.externalInterfaceState = :intState AND int.executionType IN (:executionValues)")	
})
@Table(name="EXTERNAL_INTERFACE")
public class ExternalInterface implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String INTERFACE_GET_BY_NAME ="interface.getbyname";
	
	public static final String INTERFACE_GET ="interface.list";

	/** The id ext interface pk. */
	@Id
	@SequenceGenerator(name="EXTERNAL_INTERFACE_IDEXTINTERFACEPK_GENERATOR", sequenceName="SQ_ID_EXTERNAL_INTERFACE_PK" ,initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNAL_INTERFACE_IDEXTINTERFACEPK_GENERATOR")
	@Column(name="ID_EXT_INTERFACE_PK", unique=true, nullable=false, precision=10)
	private Long idExtInterfacePk;

	/** The description. */
	@Column(length=100)
	private String description;

	/** The execution type. */
	@Column(name="EXECUTION_TYPE", nullable=false, precision=10)
	private Integer executionType;

	/** The external interface state. */
	@Column(name="EXTERNAL_INTERFACE_STATE", nullable=false, precision=10)
	private Integer externalInterfaceState;

	/** The file name. */
	@Column(name="FILE_NAME", nullable=false, length=100)
	private String fileName;
	
	@Column(name="SCHEMA_DIRECTORY", nullable=false, length=200)
	private String schemaDirectory;
	
	@Column(name="SCHEMA_RESPONSE_DIRECTORY")
	private String schemaResponseDirectory;
	
	@Column(name="SCHEMA_ROOT_TAG")
	private String schemaRootTag;

	/** The id business process fk. */
	@Column(name="ID_BUSINESS_PROCESS_FK", nullable=false, precision=10)
	private Long idBusinessProcessFk;

	/** The ind access webservice. */
	@Column(name="IND_ACCESS_WEBSERVICE")
	private Integer indAccessWebservice;
	
	/** The ind empty send. */
	@Column(name="IND_EMPTY_SEND",nullable=false, length=1)
	private Integer indEmptySend;

	@Column(name="IND_INTERFACE_TYPE")
	private Integer indInterfaceType;
	
	/** The interface name. */
	@Column(name="INTERFACE_NAME", nullable=false, length=200)
	private String interfaceName;
	
	@Column(name="RELATIVE_SERVICE_URL")
	private String relativeServiceUrl;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP", nullable=false)
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE", nullable=false)
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP", nullable=false)
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER", nullable=false)
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;

	/** The send periocity. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="SEND_PERIOCITY", nullable=false)
	private Date sendPeriocity;

	/** The separator attribute. */
	@Column(name="SEPARATOR_ATTRIBUTE", nullable=true, length=20)
	private String separatorAttribute;

	/** The separator attribute. */
//	@Column(name="QUERY", nullable=true, length=3000)
//	private String query;
	
//	@Basic(fetch=FetchType.LAZY)
	@Lob()
	@Column(name="QUERY_SQL")
	private String querySql;
	
	//bi-directional many-to-one association to ExternalInterfaceTemplate
	/** The external interface template. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXT_INFERFACE_TEMPLATE_FK", nullable=false)
	private ExternalInterfaceTemplate externalInterfaceTemplate;

	//bi-directional many-to-one association to report
	/** The report. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REPORT_FK", nullable=false)
	private Report report;
	
	//bi-directional many-to-one association to ExternalInterfaceExtension
	/** The external interface extensions. */
	@OneToMany(mappedBy="externalInterface", cascade=CascadeType.ALL)
	private List<ExternalInterfaceExtension> externalInterfaceExtensions;

	//bi-directional many-to-one association to ExternalInterfaceFileName
	/** The external interface file names. */
	@OneToMany(mappedBy="externalInterface", cascade=CascadeType.ALL)
	private List<ExternalInterfaceFileName> externalInterfaceFileNames;

	//bi-directional many-to-one association to ExternalInterfaceSend
	/** The external interface sends. */
	@OneToMany(mappedBy="externalInterface", cascade=CascadeType.ALL)
	private List<ExternalInterfaceSend> externalInterfaceSends;

	//bi-directional many-to-one association to ExternalInterfaceSend
	/** The external interface querys. */
	@OneToMany(mappedBy="externalInterface", cascade=CascadeType.ALL)
	private List<ExternalInterfaceQuery> externalInterfaceQuerys;
	
	//bi-directional many-to-one association to ExternalInterfaceUserType
	/** The external interface user types. */
	@OneToMany(mappedBy="externalInterface", cascade=CascadeType.ALL)
	private List<ExternalInterfaceUserType> externalInterfaceUserTypes;
	
	/** The interface user types selected. */
	@Transient
	private List<String> interfaceUserTypesSelected;
	
	/** The interface extensions selected. */
	@Transient
	private List<String> interfaceExtensionsSelected;
	
	/** The interface content datas. */
	@Transient
	private List<InterfaceContentData> interfaceContentDatas;

    /**
     * The Constructor.
     */
    public ExternalInterface() {
    }
    
    public ExternalInterface(Long idExtInterfacePk) {
    	this.idExtInterfacePk = idExtInterfacePk;
    }

	/**
	 * Gets the id ext interface pk.
	 *
	 * @return the id ext interface pk
	 */
	public Long getIdExtInterfacePk() {
		return this.idExtInterfacePk;
	}

	/**
	 * Sets the id ext interface pk.
	 *
	 * @param idExtInterfacePk the id ext interface pk
	 */
	public void setIdExtInterfacePk(Long idExtInterfacePk) {
		this.idExtInterfacePk = idExtInterfacePk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the execution type.
	 *
	 * @return the execution type
	 */
	public Integer getExecutionType() {
		return this.executionType;
	}

	/**
	 * Sets the execution type.
	 *
	 * @param executionType the execution type
	 */
	public void setExecutionType(Integer executionType) {
		this.executionType = executionType;
	}

	/**
	 * Gets the external interface state.
	 *
	 * @return the external interface state
	 */
	public Integer getExternalInterfaceState() {
		return this.externalInterfaceState;
	}

	/**
	 * Sets the external interface state.
	 *
	 * @param externalInterfaceState the external interface state
	 */
	public void setExternalInterfaceState(Integer externalInterfaceState) {
		this.externalInterfaceState = externalInterfaceState;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return this.fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the id business process fk.
	 *
	 * @return the id business process fk
	 */
	public Long getIdBusinessProcessFk() {
		return this.idBusinessProcessFk;
	}

	/**
	 * Sets the id business process fk.
	 *
	 * @param idBusinessProcessFk the id business process fk
	 */
	public void setIdBusinessProcessFk(Long idBusinessProcessFk) {
		this.idBusinessProcessFk = idBusinessProcessFk;
	}

	/**
	 * Gets the interface name.
	 *
	 * @return the interface name
	 */
	public String getInterfaceName() {
		return this.interfaceName;
	}

	/**
	 * Sets the interface name.
	 *
	 * @param interfaceName the interface name
	 */
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the send periocity.
	 *
	 * @return the send periocity
	 */
	public Date getSendPeriocity() {
		return sendPeriocity;
	}

	/**
	 * Sets the send periocity.
	 *
	 * @param sendPeriocity the new send periocity
	 */
	public void setSendPeriocity(Date sendPeriocity) {
		this.sendPeriocity = sendPeriocity;
	}

	/**
	 * Gets the separator attribute.
	 *
	 * @return the separator attribute
	 */
	public String getSeparatorAttribute() {
		return this.separatorAttribute;
	}

	/**
	 * Sets the separator attribute.
	 *
	 * @param separatorAttribute the separator attribute
	 */
	public void setSeparatorAttribute(String separatorAttribute) {
		this.separatorAttribute = separatorAttribute;
	}

	/**
	 * Gets the external interface template.
	 *
	 * @return the external interface template
	 */
	public ExternalInterfaceTemplate getExternalInterfaceTemplate() {
		return this.externalInterfaceTemplate;
	}

	/**
	 * Sets the external interface template.
	 *
	 * @param externalInterfaceTemplate the external interface template
	 */
	public void setExternalInterfaceTemplate(ExternalInterfaceTemplate externalInterfaceTemplate) {
		this.externalInterfaceTemplate = externalInterfaceTemplate;
	}
	
	/**
	 * Gets the external interface extensions.
	 *
	 * @return the external interface extensions
	 */
	public List<ExternalInterfaceExtension> getExternalInterfaceExtensions() {
		return this.externalInterfaceExtensions;
	}

	/**
	 * Sets the external interface extensions.
	 *
	 * @param externalInterfaceExtensions the external interface extensions
	 */
	public void setExternalInterfaceExtensions(List<ExternalInterfaceExtension> externalInterfaceExtensions) {
		this.externalInterfaceExtensions = externalInterfaceExtensions;
	}
	
	/**
	 * Gets the external interface file names.
	 *
	 * @return the external interface file names
	 */
	public List<ExternalInterfaceFileName> getExternalInterfaceFileNames() {
		return this.externalInterfaceFileNames;
	}

	/**
	 * Sets the external interface file names.
	 *
	 * @param externalInterfaceFileNames the external interface file names
	 */
	public void setExternalInterfaceFileNames(List<ExternalInterfaceFileName> externalInterfaceFileNames) {
		this.externalInterfaceFileNames = externalInterfaceFileNames;
	}
	
	/**
	 * Gets the external interface sends.
	 *
	 * @return the external interface sends
	 */
	public List<ExternalInterfaceSend> getExternalInterfaceSends() {
		return this.externalInterfaceSends;
	}

	/**
	 * Sets the external interface sends.
	 *
	 * @param externalInterfaceSends the external interface sends
	 */
	public void setExternalInterfaceSends(List<ExternalInterfaceSend> externalInterfaceSends) {
		this.externalInterfaceSends = externalInterfaceSends;
	}
	
	/**
	 * Gets the external interface user types.
	 *
	 * @return the external interface user types
	 */
	public List<ExternalInterfaceUserType> getExternalInterfaceUserTypes() {
		return this.externalInterfaceUserTypes;
	}

	/**
	 * Sets the external interface user types.
	 *
	 * @param externalInterfaceUserTypes the external interface user types
	 */
	public void setExternalInterfaceUserTypes(List<ExternalInterfaceUserType> externalInterfaceUserTypes) {
		this.externalInterfaceUserTypes = externalInterfaceUserTypes;
	}

	/**
	 * Gets the ind access webservice.
	 *
	 * @return the ind access webservice
	 */
	public Integer getIndAccessWebservice() {
		return indAccessWebservice;
	}

	/**
	 * Sets the ind access webservice.
	 *
	 * @param indAccessWebservice the ind access webservice
	 */
	public void setIndAccessWebservice(Integer indAccessWebservice) {
		this.indAccessWebservice = indAccessWebservice;
	}

	/**
	 * Gets the external interface querys.
	 *
	 * @return the external interface querys
	 */
	public List<ExternalInterfaceQuery> getExternalInterfaceQuerys() {
		return externalInterfaceQuerys;
	}

	/**
	 * Sets the external interface querys.
	 *
	 * @param externalInterfaceQuerys the external interface querys
	 */
	public void setExternalInterfaceQuerys(
			List<ExternalInterfaceQuery> externalInterfaceQuerys) {
		this.externalInterfaceQuerys = externalInterfaceQuerys;
	}

	/**
	 * Gets the report.
	 *
	 * @return the report
	 */
	public Report getReport() {
		return report;
	}

	/**
	 * Sets the report.
	 *
	 * @param report the report
	 */
	public void setReport(Report report) {
		this.report = report;
	}

	/**
	 * Gets the ind empty send.
	 *
	 * @return the ind empty send
	 */
	public Integer getIndEmptySend() {
		return indEmptySend;
	}

	/**
	 * Sets the ind empty send.
	 *
	 * @param indEmptySend the new ind empty send
	 */
	public void setIndEmptySend(Integer indEmptySend) {
		this.indEmptySend = indEmptySend;
	}

	/**
	 * Gets the interface user types selected.
	 *
	 * @return the interface user types selected
	 */
	public List<String> getInterfaceUserTypesSelected() {
		return interfaceUserTypesSelected;
	}

	/**
	 * Sets the interface user types selected.
	 *
	 * @param interfaceUserTypesSelected the new interface user types selected
	 */
	public void setInterfaceUserTypesSelected(
			List<String> interfaceUserTypesSelected) {
		this.interfaceUserTypesSelected = interfaceUserTypesSelected;
	}

	/**
	 * Gets the interface extensions selected.
	 *
	 * @return the interface extensions selected
	 */
	public List<String> getInterfaceExtensionsSelected() {
		return interfaceExtensionsSelected;
	}

	
	/**
	 * Gets the interface content datas.
	 *
	 * @return the interface content datas
	 */
	public List<InterfaceContentData> getInterfaceContentDatas() {
		return interfaceContentDatas;
	}

	/**
	 * Sets the interface content datas.
	 *
	 * @param interfaceContentDatas the new interface content datas
	 */
	public void setInterfaceContentDatas(
			List<InterfaceContentData> interfaceContentDatas) {
		this.interfaceContentDatas = interfaceContentDatas;
	}

	/**
	 * Sets the interface extensions selected.
	 *
	 * @param interfaceExtensionsSelected the new interface extensions selected
	 */
	public void setInterfaceExtensionsSelected(
			List<String> interfaceExtensionsSelected) {
		this.interfaceExtensionsSelected = interfaceExtensionsSelected;
	}
	
	public String getSchemaDirectory() {
		return schemaDirectory;
	}

	public void setSchemaDirectory(String schemaDirectory) {
		this.schemaDirectory = schemaDirectory;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}		
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String, List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();		
		
		detailsMap.put("externalInterfaceExtensions", externalInterfaceExtensions);
		detailsMap.put("externalInterfaceFileNames", externalInterfaceFileNames);
		detailsMap.put("externalInterfaceUserTypes", externalInterfaceUserTypes);
		detailsMap.put("externalInterfaceSends", externalInterfaceSends);
		detailsMap.put("externalInterfaceQuerys", externalInterfaceQuerys);
		return detailsMap;
	}

	/**
	 * Instantiates a new external interface.
	 *
	 * @param idExtInterfacePk the id ext interface pk
	 * @param sendPeriocity the send periocity
	 */
	public ExternalInterface(Long idExtInterfacePk, Date sendPeriocity) {
		super();
		this.idExtInterfacePk = idExtInterfacePk;
		this.sendPeriocity = sendPeriocity;
	}

	public String getSchemaResponseDirectory() {
		return schemaResponseDirectory;
	}

	public void setSchemaResponseDirectory(String schemaResponseDirectory) {
		this.schemaResponseDirectory = schemaResponseDirectory;
	}

	public String getSchemaRootTag() {
		return schemaRootTag;
	}

	public void setSchemaRootTag(String schemaRootTag) {
		this.schemaRootTag = schemaRootTag;
	}

	public Integer getIndInterfaceType() {
		return indInterfaceType;
	}

	public void setIndInterfaceType(Integer indInterfaceType) {
		this.indInterfaceType = indInterfaceType;
	}

	public String getRelativeServiceUrl() {
		return relativeServiceUrl;
	}

	public void setRelativeServiceUrl(String relativeServiceUrl) {
		this.relativeServiceUrl = relativeServiceUrl;
	}

	/**
	 * @return the query
	 */
//	public String getQuery() {
//		return query;
//	}
//
//	/**
//	 * @param query the query to set
//	 */
//	public void setQuery(String query) {
//		this.query = query;
//	}

	public String getQuerySql() {
		return querySql;
	}

	public void setQuerySql(String querySql) {
		this.querySql = querySql;
	}
	
	
}