package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum Daily Exchange Rate State.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/05/2013
 */
public enum DailyExchangeRatesType {
	

	/** The register state. */
	REGISTER(Integer.valueOf(2169),"REGISTRADO"),
	
	/** The confirm state. */
	CONFIRM(Integer.valueOf(2170),"CONFIRMADO");	
	
	/** The Constant list. */
	public final static List<DailyExchangeRatesType> list=new ArrayList<DailyExchangeRatesType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, DailyExchangeRatesType> lookup=new HashMap<Integer, DailyExchangeRatesType>();
	
	static {
		for(DailyExchangeRatesType c : DailyExchangeRatesType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new Daily Exchange Rate State type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private DailyExchangeRatesType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the Daily Exchange Rate State  type
	 */
	public static DailyExchangeRatesType get(Integer code) {
		return lookup.get(code);
	}
	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
	
	
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}


	
	/**
	 * List some elements.
	 *
	 * @param DailyExchangeRatesType the Daily Exchange Rate State  type params
	 * @return the list
	 */
	public static List<DailyExchangeRatesType> listSomeElements(DailyExchangeRatesType... dailyExchangeRatesTypeParams){
		List<DailyExchangeRatesType> retorno = new ArrayList<DailyExchangeRatesType>();
		for(DailyExchangeRatesType dailyExchangeRatesType: dailyExchangeRatesTypeParams){
			retorno.add(dailyExchangeRatesType);
		}
		return retorno;
	}
}
