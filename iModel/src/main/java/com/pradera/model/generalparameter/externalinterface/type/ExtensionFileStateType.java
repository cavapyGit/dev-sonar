package com.pradera.model.generalparameter.externalinterface.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ExternalInterfaceExecutionType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/10/2013
 */
public enum ExtensionFileStateType {

	/** The automatic. */
	REGISTERED(Integer.valueOf(1560),"REGISTRADO"),
	
	/** The boot. */
	DELETED(Integer.valueOf(1551),"ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public final static List<ExtensionFileStateType> list=new ArrayList<ExtensionFileStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, ExtensionFileStateType> lookup=new HashMap<Integer, ExtensionFileStateType>();

	static {
		for(ExtensionFileStateType c : ExtensionFileStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/**
	 * Instantiates a new external interface execution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ExtensionFileStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}