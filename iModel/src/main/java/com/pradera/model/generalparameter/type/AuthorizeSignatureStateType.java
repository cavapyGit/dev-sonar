package com.pradera.model.generalparameter.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author equinajo.
 * @version 1.0 , 22/08/2020
 */
public enum AuthorizeSignatureStateType {
	
	REGISTERED(Integer.valueOf(2553)),
	
	CONFIRMED(Integer.valueOf(2554)),
	
	REJECTED(Integer.valueOf(2555));
	
	/** The Constant list. */
	public final static List<AuthorizeSignatureStateType> list=new ArrayList<AuthorizeSignatureStateType>();
	
	/** The Constant lookup. */
	public final static Map<Integer, AuthorizeSignatureStateType> lookup=new HashMap<Integer, AuthorizeSignatureStateType>();
	
	static {
		for(AuthorizeSignatureStateType c : AuthorizeSignatureStateType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The lng code. */
	private Long lngCode;
	
	/** The str code. */
	private String strCode;
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param code the code
	 */
	private AuthorizeSignatureStateType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param lngCode the lng code
	 */
	private AuthorizeSignatureStateType(Long lngCode){
		this.lngCode = lngCode;
	}
	
	/**
	 * Instantiates a new master table type.
	 *
	 * @param code the code
	 */
	private AuthorizeSignatureStateType(String code){
		this.strCode=code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the master table type
	 */
	public static AuthorizeSignatureStateType get(Integer codigo) {
        return lookup.get(codigo);
    }

	/**
	 * Gets the.
	 *
	 * @param lngCode the lng code
	 * @return the master table type
	 */
	public static AuthorizeSignatureStateType get(Long lngCode){
		return lookup.get(new Integer(lngCode.intValue()));
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}

	/**
	 * Gets the lng code.
	 *
	 * @return the lng code
	 */
	public Long getLngCode(){
		return this.lngCode;
	}
	
	/**
	 * Gets the str code.
	 *
	 * @return the strCode
	 */
	public String getStrCode() {
		return strCode;
	}
	
	/**
	 * Sets the str code.
	 *
	 * @param strCode the strCode to set
	 */
	public void setStrCode(String strCode) {
		this.strCode = strCode;
	}
	
}