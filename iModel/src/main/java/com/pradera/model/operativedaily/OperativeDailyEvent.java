package com.pradera.model.operativedaily;

import java.util.Date;

public abstract class OperativeDailyEvent implements OperativeFeaturesInterface{
	
	private Date beginTime;
	private Date finishTime;
	private Integer hasError;
	private Object errorDetail;
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	public Integer getHasError() {
		return hasError;
	}
	public void setHasError(Integer hasError) {
		this.hasError = hasError;
	}
	public Object getErrorDetail() {
		return errorDetail;
	}
	public void setErrorDetail(Object errorDetail) {
		this.errorDetail = errorDetail;
	}
}