package com.pradera.model.operativedaily;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.process.BusinessProcess;


/**
 * The persistent class for the OPERATIVE_DAILY_SETUP database table.
 * 
 */
@Entity
@Table(name="OPERATIVE_DAILY_SETUP")
@NamedQuery(name="OperativeDailySetup.findAll", query="SELECT o FROM OperativeDailySetup o")
public class OperativeDailySetup implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="OPERATIVE_DAILY_SETUP_IDOPERATIVEDAILYSETUPPK_GENERATOR", sequenceName="SQ_ID_OPERAT_DAILY_SETUP_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OPERATIVE_DAILY_SETUP_IDOPERATIVEDAILYSETUPPK_GENERATOR")
	@Column(name="ID_OPERATIVE_DAILY_SETUP_PK")
	private Long idOperativeDailySetupPk;

	@Column(name="EVENT_NAME")
	private String eventName;

	@Column(name="EXECUTION_ORDER")
	private Integer executionOrder;

	@ManyToOne
	@JoinColumn(name="ID_BUSINESS_PROCESS_FK")
	private BusinessProcess businessProcess;

	@Column(name="IND_ACTIVE")
	private Integer indActive;

	@Column(name="IND_HAVE_DETAIL")
	private Integer indHaveDetail;

	@Column(name="IND_RETRY_MANUAL")
	private Integer indRetryManual;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Lob
	@Column(name="NRO_ROW_QUERY")
	private String nroRowQuery;
	
	@Lob
	@Column(name="VALIDATION_QUERY")
	private String validationQuery;

	//bi-directional many-to-one association to OperativeDailyDetail
	@OneToMany(mappedBy="operativeDailySetup")
	private List<OperativeDailyDetail> operativeDailyDetails;

	//bi-directional many-to-one association to OperativeDailySetup
	@ManyToOne
	@JoinColumn(name="ID_OPERATIVE_DAILY_SETUP_FK")
	private OperativeDailySetup operativeDailySetup;

	//bi-directional many-to-one association to OperativeDailySetup
	@OneToMany(mappedBy="operativeDailySetup")
	private List<OperativeDailySetup> operativeDailySetups;

	public OperativeDailySetup() {
	}
	
	public Long getIdOperativeDailySetupPk() {
		return idOperativeDailySetupPk;
	}

	public void setIdOperativeDailySetupPk(Long idOperativeDailySetupPk) {
		this.idOperativeDailySetupPk = idOperativeDailySetupPk;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Integer getExecutionOrder() {
		return executionOrder;
	}

	public void setExecutionOrder(Integer executionOrder) {
		this.executionOrder = executionOrder;
	}

	public Integer getIndActive() {
		return indActive;
	}

	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}

	public Integer getIndHaveDetail() {
		return indHaveDetail;
	}

	public void setIndHaveDetail(Integer indHaveDetail) {
		this.indHaveDetail = indHaveDetail;
	}

	public Integer getIndRetryManual() {
		return indRetryManual;
	}

	public void setIndRetryManual(Integer indRetryManual) {
		this.indRetryManual = indRetryManual;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public List<OperativeDailyDetail> getOperativeDailyDetails() {
		return operativeDailyDetails;
	}

	public void setOperativeDailyDetails(
			List<OperativeDailyDetail> operativeDailyDetails) {
		this.operativeDailyDetails = operativeDailyDetails;
	}

	public OperativeDailySetup getOperativeDailySetup() {
		return operativeDailySetup;
	}

	public void setOperativeDailySetup(OperativeDailySetup operativeDailySetup) {
		this.operativeDailySetup = operativeDailySetup;
	}

	public List<OperativeDailySetup> getOperativeDailySetups() {
		return operativeDailySetups;
	}

	public void setOperativeDailySetups(
			List<OperativeDailySetup> operativeDailySetups) {
		this.operativeDailySetups = operativeDailySetups;
	}

	public OperativeDailyDetail addOperativeDailyDetail(OperativeDailyDetail operativeDailyDetail) {
		getOperativeDailyDetails().add(operativeDailyDetail);
		operativeDailyDetail.setOperativeDailySetup(this);
		return operativeDailyDetail;
	}
	public OperativeDailyDetail removeOperativeDailyDetail(OperativeDailyDetail operativeDailyDetail) {
		getOperativeDailyDetails().remove(operativeDailyDetail);
		operativeDailyDetail.setOperativeDailySetup(null);

		return operativeDailyDetail;
	}
	public OperativeDailySetup addOperativeDailySetup(OperativeDailySetup operativeDailySetup) {
		getOperativeDailySetups().add(operativeDailySetup);
		operativeDailySetup.setOperativeDailySetup(this);

		return operativeDailySetup;
	}

	public OperativeDailySetup removeOperativeDailySetup(OperativeDailySetup operativeDailySetup) {
		getOperativeDailySetups().remove(operativeDailySetup);
		operativeDailySetup.setOperativeDailySetup(null);

		return operativeDailySetup;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {

		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public BusinessProcess getBusinessProcess() {
		return businessProcess;
	}

	public void setBusinessProcess(BusinessProcess businessProcess) {
		this.businessProcess = businessProcess;
	}

	public String getNroRowQuery() {
		return nroRowQuery;
	}

	public void setNroRowQuery(String nroRowQuery) {
		this.nroRowQuery = nroRowQuery;
	}

	public String getValidationQuery() {
		return validationQuery;
	}

	public void setValidationQuery(String validationQuery) {
		this.validationQuery = validationQuery;
	}
	
}