package com.pradera.model.operativedaily;

/**
 * The Interface OperativeFeaturesInterface.
 */
public interface OperativeFeaturesInterface {

	/**
	 * Checks if is process failed.
	 *
	 * @return true, if is process failed
	 */
	public boolean isProcessFailed();
	
	/**
	 * Checks if is process finish.
	 *
	 * @return true, if is process finish
	 */
	public boolean isProcessFinish();
	
	/**
	 * Restart process.
	 *
	 * @return true, if successful
	 */
	public boolean restartProcess();
	
	/**
	 * Gets the process status.
	 *
	 * @return the process status
	 */
	public Integer getProcessStatus();
}
