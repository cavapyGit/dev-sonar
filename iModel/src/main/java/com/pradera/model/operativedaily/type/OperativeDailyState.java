package com.pradera.model.operativedaily.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum CurrencyType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/05/2013
 */
public enum OperativeDailyState {
	
	/** The bob. */
	REGISTER(Integer.valueOf(2342),"REGISTRADO"),
	
	/** The usd. */
	WAITING(Integer.valueOf(2343),"EN ESPERA"),
	
	/** The usd. */
	PROCESSING(Integer.valueOf(2344),"PROCESANDO"),
	
	/** The eur. */
	FINALIZED(Integer.valueOf(2345),"FINALIZADO");

	/** The Constant list. */
	public final static List<OperativeDailyState> list=new ArrayList<OperativeDailyState>();
	
	/** The Constant lookup. */
	public final static Map<Integer, OperativeDailyState> lookup=new HashMap<Integer, OperativeDailyState>();
	
	static {
		for(OperativeDailyState c : OperativeDailyState.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new currency type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 * @param codeIso the code iso
	 */
	private OperativeDailyState(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the currency type
	 */
	public static OperativeDailyState get(Integer code) {
		return lookup.get(code);
	}
	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
	

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<OperativeDailyState> listSomeElements(OperativeDailyState... transferSecuritiesTypeParams){
		List<OperativeDailyState> retorno = new ArrayList<OperativeDailyState>();
		for(OperativeDailyState TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
}
