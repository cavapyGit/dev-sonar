package com.pradera.model.operativedaily;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the OPERATIVE_DAILY_LOGGER database table.
 * 
 */
@Entity
@Table(name="OPERATIVE_DAILY_LOGGER")
@NamedQuery(name="OperativeDailyLogger.findAll", query="SELECT o FROM OperativeDailyLogger o")
public class OperativeDailyLogger implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="OPERATIVE_DAILY_LOGGER_IDOPERATIONDAILYLOGGERPK_GENERATOR", sequenceName="SQ_ID_OPERAT_DAILY_LOGGER_PK",initialValue=1,allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OPERATIVE_DAILY_LOGGER_IDOPERATIONDAILYLOGGERPK_GENERATOR")
	@Column(name="ID_OPERATION_DAILY_LOGGER_PK")
	private Long idOperationDailyLoggerPk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Lob
	@Column(name="ERROR_DETAIL")
	private String errorDetail;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FINISH_TIME")
	private Date finishTime;

	@Column(name="IND_ERROR")
	private Integer indError;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to OperativeDailyDetail
	@ManyToOne
	@JoinColumn(name="ID_OPERATION_DAILY_DETAIL_FK")
	private OperativeDailyDetail operativeDailyDetail;

	public OperativeDailyLogger() {
	}

	public long getIdOperationDailyLoggerPk() {
		return this.idOperationDailyLoggerPk;
	}

	public void setIdOperationDailyLoggerPk(long idOperationDailyLoggerPk) {
		this.idOperationDailyLoggerPk = idOperationDailyLoggerPk;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public String getErrorDetail() {
		return this.errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	public Date getFinishTime() {
		return this.finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Integer getIndError() {
		return this.indError;
	}

	public void setIndError(Integer indError) {
		this.indError = indError;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public OperativeDailyDetail getOperativeDailyDetail() {
		return this.operativeDailyDetail;
	}

	public void setOperativeDailyDetail(OperativeDailyDetail operativeDailyDetail) {
		this.operativeDailyDetail = operativeDailyDetail;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {

		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}