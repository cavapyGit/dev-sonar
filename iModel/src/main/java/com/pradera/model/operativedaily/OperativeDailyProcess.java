package com.pradera.model.operativedaily;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the OPERATIVE_DAILY_PROCESS database table.
 * 
 */
@Entity
@Table(name="OPERATIVE_DAILY_PROCESS")
@NamedQuery(name="OperativeDailyProcess.findAll", query="SELECT o FROM OperativeDailyProcess o")
public class OperativeDailyProcess implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="OPERATIVE_DAILY_PROCESS_IDOPERATIVEDAILYPROCESSPK_GENERATOR", sequenceName="SQ_ID_OPERAT_DAILY_PROCESS_PK",initialValue=1,allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OPERATIVE_DAILY_PROCESS_IDOPERATIVEDAILYPROCESSPK_GENERATOR")
	@Column(name="ID_OPERATIVE_DAILY_PROCESS_PK")
	private Long idOperativeDailyProcessPk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FINISH_TIME")
	private Date finishTime;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.DATE)
	@Column(name="PROCESS_DATE")
	private Date processDate;

	@Column(name="PROCESS_STATE")
	private Integer processState;

	@Temporal(TemporalType.DATE)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

	@Column(name="REGISTER_USER")
	private String registerUser;

	//bi-directional many-to-one association to OperativeDailyDetail
	@OneToMany(mappedBy="operativeDailyProcess")
	private List<OperativeDailyDetail> operativeDailyDetails;

	public OperativeDailyProcess() {
	}

	public long getIdOperativeDailyProcessPk() {
		return this.idOperativeDailyProcessPk;
	}

	public void setIdOperativeDailyProcessPk(long idOperativeDailyProcessPk) {
		this.idOperativeDailyProcessPk = idOperativeDailyProcessPk;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getFinishTime() {
		return this.finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public Integer getProcessState() {
		return this.processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return this.registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public List<OperativeDailyDetail> getOperativeDailyDetails() {
		return this.operativeDailyDetails;
	}

	public void setOperativeDailyDetails(List<OperativeDailyDetail> operativeDailyDetails) {
		this.operativeDailyDetails = operativeDailyDetails;
	}

	public OperativeDailyDetail addOperativeDailyDetail(OperativeDailyDetail operativeDailyDetail) {
		getOperativeDailyDetails().add(operativeDailyDetail);
		operativeDailyDetail.setOperativeDailyProcess(this);

		return operativeDailyDetail;
	}

	public OperativeDailyDetail removeOperativeDailyDetail(OperativeDailyDetail operativeDailyDetail) {
		getOperativeDailyDetails().remove(operativeDailyDetail);
		operativeDailyDetail.setOperativeDailyProcess(null);

		return operativeDailyDetail;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {

		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}