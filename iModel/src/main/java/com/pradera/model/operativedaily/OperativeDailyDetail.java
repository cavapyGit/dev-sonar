package com.pradera.model.operativedaily;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the OPERATIVE_DAILY_DETAIL database table.
 * 
 */
@Entity
@Table(name="OPERATIVE_DAILY_DETAIL")
@NamedQuery(name="OperativeDailyDetail.findAll", query="SELECT o FROM OperativeDailyDetail o")
public class OperativeDailyDetail implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="OPERATIVE_DAILY_DETAIL_IDOPERATIONDAILYDETAILPK_GENERATOR", sequenceName="SQ_ID_OPERAT_DAILY_DETAIL_PK",initialValue=1,allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OPERATIVE_DAILY_DETAIL_IDOPERATIONDAILYDETAILPK_GENERATOR")
	@Column(name="ID_OPERATION_DAILY_DETAIL_PK")
	private Long idOperationDailyDetailPk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Column(name="EXECUTION_ORDER")
	private Integer executionOrder;

	@Column(name="EXECUTIONS_COUNT")
	private Integer executionsCount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FINISH_TIME")
	private Date finishTime;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="PROCESS_STATE")
	private Integer processState;

	@Column(name="ROW_QUANTITY")
	private Double rowQuantity;
	
	@Transient
	private Integer indLastDetail = ComponentConstant.ZERO;

	//bi-directional many-to-one association to OperativeDailyProcess
	@ManyToOne
	@JoinColumn(name="ID_OPERATIVE_DAILY_PROCESS_FK")
	private OperativeDailyProcess operativeDailyProcess;

	//bi-directional many-to-one association to OperativeDailySetup
	@ManyToOne
	@JoinColumn(name="ID_OPERATIVE_DAILY_SETUP_FK")
	private OperativeDailySetup operativeDailySetup;

	//bi-directional many-to-one association to OperativeDailyLogger
	@OneToMany(mappedBy="operativeDailyDetail")
	private List<OperativeDailyLogger> operativeDailyLoggers;

	public OperativeDailyDetail() {
	}
	
	public Long getIdOperationDailyDetailPk() {
		return idOperationDailyDetailPk;
	}

	public void setIdOperationDailyDetailPk(Long idOperationDailyDetailPk) {
		this.idOperationDailyDetailPk = idOperationDailyDetailPk;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Integer getExecutionOrder() {
		return executionOrder;
	}

	public void setExecutionOrder(Integer executionOrder) {
		this.executionOrder = executionOrder;
	}

	public Integer getExecutionsCount() {
		return executionsCount;
	}

	public void setExecutionsCount(Integer executionsCount) {
		this.executionsCount = executionsCount;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Double getRowQuantity() {
		return rowQuantity;
	}

	public void setRowQuantity(Double rowQuantity) {
		this.rowQuantity = rowQuantity;
	}

	public OperativeDailyProcess getOperativeDailyProcess() {
		return operativeDailyProcess;
	}

	public void setOperativeDailyProcess(OperativeDailyProcess operativeDailyProcess) {
		this.operativeDailyProcess = operativeDailyProcess;
	}

	public OperativeDailySetup getOperativeDailySetup() {
		return operativeDailySetup;
	}

	public void setOperativeDailySetup(OperativeDailySetup operativeDailySetup) {
		this.operativeDailySetup = operativeDailySetup;
	}

	public List<OperativeDailyLogger> getOperativeDailyLoggers() {
		return operativeDailyLoggers;
	}

	public void setOperativeDailyLoggers(
			List<OperativeDailyLogger> operativeDailyLoggers) {
		this.operativeDailyLoggers = operativeDailyLoggers;
	}

	public OperativeDailyLogger addOperativeDailyLogger(OperativeDailyLogger operativeDailyLogger) {
		getOperativeDailyLoggers().add(operativeDailyLogger);
		operativeDailyLogger.setOperativeDailyDetail(this);

		return operativeDailyLogger;
	}
	public OperativeDailyLogger removeOperativeDailyLogger(OperativeDailyLogger operativeDailyLogger) {
		getOperativeDailyLoggers().remove(operativeDailyLogger);
		operativeDailyLogger.setOperativeDailyDetail(null);

		return operativeDailyLogger;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {

		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getIndLastDetail() {
		return indLastDetail;
	}

	public void setIndLastDetail(Integer indLastDetail) {
		this.indLastDetail = indLastDetail;
	}
}