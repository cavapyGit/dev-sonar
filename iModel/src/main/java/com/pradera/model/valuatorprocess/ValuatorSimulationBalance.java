package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.HolderAccountBalance;


/**
 * The persistent class for the VALUATOR_SIMULATION_BALANCE database table.
 * 
 */
@Entity
@Table(name="VALUATOR_SIMULATION_BALANCE")
@NamedQuery(name="ValuatorSimulationBalance.findAll", query="SELECT v FROM ValuatorSimulationBalance v")
public class ValuatorSimulationBalance implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_SIMULATION_BALANCE_IDSIMULATIONBALANCEPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_SIM_BALANCE_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_SIMULATION_BALANCE_IDSIMULATIONBALANCEPK_GENERATOR")
	@Column(name="ID_SIMULATION_BALANCE_PK")
	private Long idSimulationBalancePk;

	@Column(name="AVAILABLE_BALANCE")
	private BigDecimal availableBalance;

	@Column(name="IND_NOMINAL_RATE")
	private Integer indNominalRate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="PROCESS_STATE")
	private Integer processState;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="REGISTRY_DATE")
//	private Date registryDate;
//
//	@Column(name="REGISTRY_USER")
//	private String registryUser;

	@Column(name="SIMULATION_AMOUNT")
	private BigDecimal simulationAmount;

//	@Column(name="SIMULATION_AMOUNT_OHER")
//	private BigDecimal simulationAmountOher;
	
	@Column(name="SIMULATION_AVAILABLE_AMOUNT")
	private BigDecimal simulationAvailableAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="SIMULATION_DATE")
	private Date simulationDate;

	@Column(name="SIMULATION_PRICE")
	private BigDecimal simulationPrice;

	@Column(name="SIMULATION_RATE")
	private BigDecimal simulationRate;

	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;

	@Column(name="VALUATOR_CODE")
	private String valuatorCode;

	@Column(name="VALUATOR_CODE_SIMULATED")
	private String valuatorCodeSimulated;

	//bi-directional many-to-one association to HolderAccountBalance
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_HOLDER_ACCOUNT_FK", referencedColumnName="ID_HOLDER_ACCOUNT_PK"),
		@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK"),
		@JoinColumn(name="ID_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK")
		})
	private HolderAccountBalance holderAccountBalance;

	//bi-directional many-to-one association to ValuatorProcessSimulator
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_SIMULATOR_FK")
	private ValuatorProcessSimulator valuatorProcessSimulator;

	public ValuatorSimulationBalance() {
	}

	public Long getIdSimulationBalancePk() {
		return this.idSimulationBalancePk;
	}

	public void setIdSimulationBalancePk(Long idSimulationBalancePk) {
		this.idSimulationBalancePk = idSimulationBalancePk;
	}

	public BigDecimal getAvailableBalance() {
		return this.availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
//
//	public Date getRegistryDate() {
//		return this.registryDate;
//	}
//
//	public void setRegistryDate(Date registryDate) {
//		this.registryDate = registryDate;
//	}
//
//	public String getRegistryUser() {
//		return this.registryUser;
//	}
//
//	public void setRegistryUser(String registryUser) {
//		this.registryUser = registryUser;
//	}

	public BigDecimal getSimulationAmount() {
		return this.simulationAmount;
	}

	public void setSimulationAmount(BigDecimal simulationAmount) {
		this.simulationAmount = simulationAmount;
	}

	public BigDecimal getSimulationAvailableAmount() {
		return simulationAvailableAmount;
	}

	public void setSimulationAvailableAmount(BigDecimal simulationAvailableAmount) {
		this.simulationAvailableAmount = simulationAvailableAmount;
	}

	public Date getSimulationDate() {
		return this.simulationDate;
	}

	public void setSimulationDate(Date simulationDate) {
		this.simulationDate = simulationDate;
	}

	public BigDecimal getSimulationPrice() {
		return this.simulationPrice;
	}

	public void setSimulationPrice(BigDecimal simulationPrice) {
		this.simulationPrice = simulationPrice;
	}

	public BigDecimal getSimulationRate() {
		return this.simulationRate;
	}

	public void setSimulationRate(BigDecimal simulationRate) {
		this.simulationRate = simulationRate;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public String getValuatorCode() {
		return this.valuatorCode;
	}

	public void setValuatorCode(String valuatorCode) {
		this.valuatorCode = valuatorCode;
	}

	public String getValuatorCodeSimulated() {
		return this.valuatorCodeSimulated;
	}

	public void setValuatorCodeSimulated(String valuatorCodeSimulated) {
		this.valuatorCodeSimulated = valuatorCodeSimulated;
	}

	public HolderAccountBalance getHolderAccountBalance() {
		return this.holderAccountBalance;
	}

	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	public ValuatorProcessSimulator getValuatorProcessSimulator() {
		return this.valuatorProcessSimulator;
	}

	public void setValuatorProcessSimulator(ValuatorProcessSimulator valuatorProcessSimulator) {
		this.valuatorProcessSimulator = valuatorProcessSimulator;
	}

	public Integer getIndNominalRate() {
		return indNominalRate;
	}

	public void setIndNominalRate(Integer indNominalRate) {
		this.indNominalRate = indNominalRate;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}