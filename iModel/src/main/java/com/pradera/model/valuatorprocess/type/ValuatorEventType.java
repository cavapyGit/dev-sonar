package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorEventType {

	EVENT_VALIDATIONS		(Long.valueOf(1000), "VERIFICACION DE VALIDACIONES"),
	EVENT_1_1_TRANSIT_BALANCES(Long.valueOf(1010), "VERIFICACION DE VALORES EN SALDOS COMPROMETIDOS"),
	EVENT_1_2_MARKETFACT_FILE(Long.valueOf(1020), "VERIFICACION DE ARCHIVOS DE HECHOS DE MERCADO"),
	EVENT_1_3_INCONSISTENTS(Long.valueOf(1030), "VERIFICACION DE PROCESOS INCONSISTENTES"),
	EVENT_1_4_SETTLEMENT_CLOSE(Long.valueOf(1040), "VERIFICACION DE CIERRE DE LIQUIDACIONES"),
	EVENT_1_5_DAY_END(Long.valueOf(1050), "VERIFICACION DE CIERRE DE DIA"),

	EVENT_CALCULATION_PRICE(Long.valueOf(1060), "CALCULO DE PRECIO"),
	EVENT_2_1_PRICE_FIXED_INCOME(Long.valueOf(1070), "CALCULO DE PRECIO DE VALORES RENTA FIJA"),
	EVENT_2_2_PRICE_VARIABLE_INCOME(Long.valueOf(1080), "CALCULO DE PRECIO DE VALORES RENTA VARIABLE"),
	
	EVENT_UPDATING_PORTFOLY(Long.valueOf(1090), "ACTUALIZACION DE PRECIO EN CARTERAS INVERSIONISTA"),
	
	EVENT_UPDATING_OPERATION(Long.valueOf(1100), "ACTUALIZACION DE PRECIO EN OPERACIONES VIVAS"),
	EVENT_4_1_UPDATE_BLOCK_OPERATION(Long.valueOf(1110), "ACTUALIZACION DE PRECIO EN OPERACIONES DE BLOQUEO"),
	EVENT_4_2_UPDATE_REPORTING_OPERATION(Long.valueOf(1120), "ACTUALIZACION DE PRECIO EN OPERACIONES DE REPORTO"),
	EVENT_4_3_UPDATE_PHYSICAL_CERTIFICATE(Long.valueOf(1130), "ACTUALIZACION DE PRECION EN FISICOS");
	
	/** The code. */
	private Long code;
	/** The description. */
	private String description;

	private ValuatorEventType(Long code, String description) {
		this.code = code;
		this.description = description;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static final List<ValuatorEventType> list = new ArrayList<ValuatorEventType>();
	public static final Map<Long, ValuatorEventType> lookup = new HashMap<Long, ValuatorEventType>();
	static {
		for (ValuatorEventType s : EnumSet.allOf(ValuatorEventType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorEventType get(Long codigo) {
		return lookup.get(codigo);
	}
	
	public static ValuatorEventType isValidationEvent(Long eventCode) {
		switch(ValuatorEventType.get(eventCode)){
			case EVENT_VALIDATIONS: 
			case EVENT_1_1_TRANSIT_BALANCES: 
			case EVENT_1_2_MARKETFACT_FILE:
			case EVENT_1_3_INCONSISTENTS:	 
			case EVENT_1_4_SETTLEMENT_CLOSE:
			case EVENT_1_5_DAY_END: 					return ValuatorEventType.EVENT_VALIDATIONS;
			
			case EVENT_CALCULATION_PRICE:
			case EVENT_2_1_PRICE_FIXED_INCOME:
			case EVENT_2_2_PRICE_VARIABLE_INCOME: 		return ValuatorEventType.EVENT_CALCULATION_PRICE;
			
			case EVENT_UPDATING_PORTFOLY: 		return ValuatorEventType.EVENT_UPDATING_PORTFOLY;
			
			case EVENT_UPDATING_OPERATION:
			case EVENT_4_1_UPDATE_BLOCK_OPERATION:
			case EVENT_4_2_UPDATE_REPORTING_OPERATION:
			case EVENT_4_3_UPDATE_PHYSICAL_CERTIFICATE:	return EVENT_UPDATING_OPERATION;
		}
		return null;
	}
}