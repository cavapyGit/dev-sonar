package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorExecutionStateType {

	REGISTERED  (Integer.valueOf(1986), "REGISTRADO"),
	VALUATING (Integer.valueOf(1987), "VALORANDO"),
	FINISHED_WITH_ERROR (Integer.valueOf(2317), "FINALIZADO CON ERRORES"),
	FINISHED (Integer.valueOf(1988), "FINALIZADO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorExecutionStateType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorExecutionStateType> list = new ArrayList<ValuatorExecutionStateType>();
	public static final Map<Integer, ValuatorExecutionStateType> lookup = new HashMap<Integer, ValuatorExecutionStateType>();
	static {
		for (ValuatorExecutionStateType s : EnumSet.allOf(ValuatorExecutionStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorExecutionStateType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}