package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_SIMULATOR_CODE database table.
 * 
 */
@Entity
@Table(name="VALUATOR_SIMULATOR_CODE")
@NamedQuery(name="ValuatorSimulatorCode.findAll", query="SELECT v FROM ValuatorSimulatorCode v")
public class ValuatorSimulatorCode implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_SIMULATOR_CODE_IDSIMULATORCODEPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_SIMUL_CODE_PK", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_SIMULATOR_CODE_IDSIMULATORCODEPK_GENERATOR")
	@Column(name="ID_SIMULATOR_CODE_PK")
	private Long idSimulatorCodePk;

	@Column(name="AVERAGE_RATE")
	private BigDecimal averageRate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="REGISTRY_DATE")
//	private Date registryDate;
//
//	@Column(name="REGISTRY_USER")
//	private String registryUser;

	//bi-directional many-to-one association to ValuatorCode
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_CODE_FK")
	private ValuatorCode valuatorCode;

	//bi-directional many-to-one association to ValuatorProcessSimulator
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_SIMULATOR_FK")
	private ValuatorProcessSimulator valuatorProcessSimulator;
	
	/** The valuator simulator securities. */
	@OneToMany(mappedBy = "valuatorSimulatorCode")
	private List<ValuatorSimulatorSecurity> valuatorSimulatorSecurities;

	public ValuatorSimulatorCode() {
	}

	public Long getIdSimulatorCodePk() {
		return this.idSimulatorCodePk;
	}

	public void setIdSimulatorCodePk(Long idSimulatorCodePk) {
		this.idSimulatorCodePk = idSimulatorCodePk;
	}

	public BigDecimal getAverageRate() {
		return this.averageRate;
	}

	public void setAverageRate(BigDecimal averageRate) {
		this.averageRate = averageRate;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

//	public Date getRegistryDate() {
//		return this.registryDate;
//	}
//
//	public void setRegistryDate(Date registryDate) {
//		this.registryDate = registryDate;
//	}
//
//	public String getRegistryUser() {
//		return this.registryUser;
//	}
//
//	public void setRegistryUser(String registryUser) {
//		this.registryUser = registryUser;
//	}

	public ValuatorCode getValuatorCode() {
		return this.valuatorCode;
	}

	public void setValuatorCode(ValuatorCode valuatorCode) {
		this.valuatorCode = valuatorCode;
	}

	public ValuatorProcessSimulator getValuatorProcessSimulator() {
		return this.valuatorProcessSimulator;
	}

	public void setValuatorProcessSimulator(ValuatorProcessSimulator valuatorProcessSimulator) {
		this.valuatorProcessSimulator = valuatorProcessSimulator;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public List<ValuatorSimulatorSecurity> getValuatorSimulatorSecurities() {
		return valuatorSimulatorSecurities;
	}

	public void setValuatorSimulatorSecurities(
			List<ValuatorSimulatorSecurity> valuatorSimulatorSecurities) {
		this.valuatorSimulatorSecurities = valuatorSimulatorSecurities;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}