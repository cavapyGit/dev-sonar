package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_TERM_RANGE database table.
 * 
 */
@Entity
@Cacheable(true)
@Table(name="VALUATOR_TERM_RANGE")
@NamedQuery(name="ValuatorTermRange.findAll", query="SELECT v FROM ValuatorTermRange v")
public class ValuatorTermRange implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_TERM_RANGE_IDTERMRANGEPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_RANGE_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_TERM_RANGE_IDTERMRANGEPK_GENERATOR")
	@Column(name="ID_TERM_RANGE_PK")
	private Long idTermRangePk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MAX_RANGE")
	private BigDecimal maxRange;

	@Column(name="MIN_RANGE")
	private BigDecimal minRange;

	@Column(name="RANGE_CODE")
	private String rangeCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	public ValuatorTermRange() {
	}

	public Long getIdTermRangePk() {
		return this.idTermRangePk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getMaxRange() {
		return this.maxRange;
	}

	public void setMaxRange(BigDecimal maxRange) {
		this.maxRange = maxRange;
	}

	public BigDecimal getMinRange() {
		return this.minRange;
	}

	public void setMinRange(BigDecimal minRange) {
		this.minRange = minRange;
	}

	public String getRangeCode() {
		return this.rangeCode;
	}

	public void setRangeCode(String rangeCode) {
		this.rangeCode = rangeCode;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public void setIdTermRangePk(Long idTermRangePk) {
		this.idTermRangePk = idTermRangePk;
	}

	public ValuatorTermRange(Long idTermRangePk, String rangeCode, BigDecimal maxRange) {
		super();
		this.idTermRangePk = idTermRangePk;
		this.rangeCode = rangeCode;
		this.maxRange = maxRange;
	}

	public ValuatorTermRange(Long idTermRangePk, String rangeCode, BigDecimal minRange, BigDecimal maxRange) {
		super();
		this.idTermRangePk = idTermRangePk;
		this.rangeCode = rangeCode;
		this.minRange = minRange;
		this.maxRange = maxRange;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}