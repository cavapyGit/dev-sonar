package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorSecurityCodSituation {

	CURRENT  (Integer.valueOf(1969), "ACTUAL"),
	HISTORY (Integer.valueOf(1970), "HISTORY");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorSecurityCodSituation(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorSecurityCodSituation> list = new ArrayList<ValuatorSecurityCodSituation>();
	public static final Map<Integer, ValuatorSecurityCodSituation> lookup = new HashMap<Integer, ValuatorSecurityCodSituation>();
	static {
		for (ValuatorSecurityCodSituation s : EnumSet.allOf(ValuatorSecurityCodSituation.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorSecurityCodSituation get(Integer codigo) {
		return lookup.get(codigo);
	}
	
}
