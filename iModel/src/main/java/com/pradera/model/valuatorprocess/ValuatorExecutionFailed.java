package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.model.component.HolderAccountBalance;


/**
 * The persistent class for the VALUATOR_EXECUTION_FAILED database table.
 * 
 */
@Entity
@Table(name="VALUATOR_EXECUTION_FAILED")
@NamedQuery(name="ValuatorExecutionFailed.findAll", query="SELECT v FROM ValuatorExecutionFailed v")
public class ValuatorExecutionFailed implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_EXECUTION_FAILED_IDEXECUTIONFAILEDPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_EXEC_FAIL_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_EXECUTION_FAILED_IDEXECUTIONFAILEDPK_GENERATOR")
	@Column(name="ID_EXECUTION_FAILED_PK")
	private Long idExecutionFailedPk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MOTIVE")
	private Integer motive;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional many-to-one association to HolderAccountBalance
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_HOLDER_ACCOUNT_FK", referencedColumnName="ID_HOLDER_ACCOUNT_PK"),
		@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK"),
		@JoinColumn(name="ID_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK")
		})
	private HolderAccountBalance holderAccountBalance;

	//bi-directional many-to-one association to ValuatorExecutionDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXECUTION_DETAIL_FK")
	private ValuatorExecutionDetail valuatorExecutionDetail;

	public ValuatorExecutionFailed() {
	}

	public Long getIdExecutionFailedPk() {
		return this.idExecutionFailedPk;
	}

	public void setIdExecutionFailedPk(Long idExecutionFailedPk) {
		this.idExecutionFailedPk = idExecutionFailedPk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public HolderAccountBalance getHolderAccountBalance() {
		return this.holderAccountBalance;
	}

	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	public ValuatorExecutionDetail getValuatorExecutionDetail() {
		return this.valuatorExecutionDetail;
	}

	public void setValuatorExecutionDetail(ValuatorExecutionDetail valuatorExecutionDetail) {
		this.valuatorExecutionDetail = valuatorExecutionDetail;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}
}