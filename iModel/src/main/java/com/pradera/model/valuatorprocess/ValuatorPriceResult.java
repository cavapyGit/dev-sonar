package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismOperation;


/**
 * The persistent class for the VALUATOR_PROCESS_SETUP database table.
 * 
 */
@Entity
@Table(name="VALUATOR_PRICE_RESULT")
@NamedQuery(name="ValuatorPriceResult", query="SELECT v FROM ValuatorPriceResult v")
public class ValuatorPriceResult implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_SQ_ID_VALUATOR_PRICE_RESULT_PK", sequenceName="SQ_ID_VALUATOR_PRICE_RESULT_PK",initialValue=1,allocationSize = 10000)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_SQ_ID_VALUATOR_PRICE_RESULT_PK")
	@Column(name="ID_PRICE_RESULT_PK")
	private Long idPriceResultPk;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="VALUATOR_RATE")
	private BigDecimal valuatorRate;

    /** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="PROCESS_DATE")
	private Date processDate;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Column(name="ECONOMIC_TERM")
	private BigDecimal economicTerm;
	
	//bi-directional many-to-one association to Security
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",insertable=false,updatable=false)
	private Security security;
	
	@Column(name="ID_SECURITY_CODE_FK")
	private String securityCode;
	
	@Column(name="ID_VALUATOR_CODE_FK")
	private Long valuatorCodeFk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCESS_EXECUTION_FK")
	private ValuatorProcessExecution valuatorProcessExecution;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="IND_HOMOLOGATED")
	private Integer indHomologated;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK",insertable=false,updatable=false)
	private MechanismOperation mechanismOperation;
	
	@Column(name="ID_MECHANISM_OPERATION_FK")
	private Long mechanismOperationPk;

	public Long getIdPriceResultPk() {
		return idPriceResultPk;
	}

	public void setIdPriceResultPk(Long idPriceResultPk) {
		this.idPriceResultPk = idPriceResultPk;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public ValuatorProcessExecution getValuatorProcessExecution() {
		return valuatorProcessExecution;
	}

	public void setValuatorProcessExecution(
			ValuatorProcessExecution valuatorProcessExecution) {
		this.valuatorProcessExecution = valuatorProcessExecution;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public Integer getIndHomologated() {
		return indHomologated;
	}

	public void setIndHomologated(Integer indHomologated) {
		this.indHomologated = indHomologated;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public BigDecimal getValuatorRate() {
		return valuatorRate;
	}

	public void setValuatorRate(BigDecimal valuatorRate) {
		this.valuatorRate = valuatorRate;
	}

	public BigDecimal getEconomicTerm() {
		return economicTerm;
	}

	public void setEconomicTerm(BigDecimal economicTerm) {
		this.economicTerm = economicTerm;
	}

	public Long getValuatorCodeFk() {
		return valuatorCodeFk;
	}

	public void setValuatorCodeFk(Long valuatorCodeFk) {
		this.valuatorCodeFk = valuatorCodeFk;
	}

	public Long getMechanismOperationPk() {
		return mechanismOperationPk;
	}

	public void setMechanismOperationPk(Long mechanismOperationPk) {
		this.mechanismOperationPk = mechanismOperationPk;
	}
}