package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_PROCESS_CLASS database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name = ValuatorProcessClass.SEARCH_VALUATOR_PROCESS_CLASS, query = "Select vpc from ValuatorProcessClass vpc where vpc.idValuatorClassPk=:idValuatorClassPrm")
})
@Entity
@Cacheable(true)
@Table(name="VALUATOR_PROCESS_CLASS")
@NamedQuery(name="ValuatorProcessClass.findAll", query="SELECT v FROM ValuatorProcessClass v")
public class ValuatorProcessClass implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	public static final String  SEARCH_VALUATOR_PROCESS_CLASS= "ValuatorProcessClass.searchByPk";
	
	@Id
	@Column(name="ID_VALUATOR_CLASS_PK")
	private String idValuatorClassPk;

	@Column(name="CLASS_DESCRIPTION")
	private String classDescription;

	private Integer currency;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MIN_RELEVANT_AMOUNT")
	private BigDecimal minRelevantAmount;
	
	@Column(name="FORMULE_TYPE")
	private Integer formuleType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="TOP_RELEVANT_AMOUNT")
	private BigDecimal topRelevantAmount;
	
	@Column(name="INSTRUMENT_TYPE")
	private Integer instrumentType;
	
	@Column(name="IND_CFI")
	private Integer indCfi;
	
	@Column(name="IND_ZERO_COUPON")
	private Integer indZeroCoupon;
	
	@Column(name="IND_FIXED_COUPON")
	private Integer indFixedCoupon;
	
	@Column(name="IND_VARIABLE_COUPON")
	private Integer indVariableCoupon;
	
	@Column(name="ID_SECURITY_CLASS_TRG")
	private Integer idSecurityClassTrg;
	
	@Column(name="ID_CONVERTIBLE_TYPE")
	private Integer idConvertibleType;

	//bi-directional many-to-one association to SecurityClassValuator
	@OneToMany(mappedBy="valuatorProcessClass")
	private List<SecurityClassValuator> securityClassValuators;

	public ValuatorProcessClass() {
	}
	
	public ValuatorProcessClass(String idValuatorClassPk,
			String classDescription, Integer currency,
			BigDecimal minRelevantAmount, BigDecimal topRelevantAmount,Integer formuleType,
			Integer instrumentType) {
		super();
		this.idValuatorClassPk = idValuatorClassPk;
		this.classDescription = classDescription;
		this.currency = currency;
		this.minRelevantAmount = minRelevantAmount;
		this.topRelevantAmount = topRelevantAmount;
		this.formuleType=formuleType;
		this.instrumentType=instrumentType;
	}
	
	public String getIdValuatorClassPk() {
		return this.idValuatorClassPk;
	}

	public void setIdValuatorClassPk(String idValuatorClassPk) {
		this.idValuatorClassPk = idValuatorClassPk;
	}

	public String getClassDescription() {
		return this.classDescription;
	}

	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getMinRelevantAmount() {
		return this.minRelevantAmount;
	}

	public void setMinRelevantAmount(BigDecimal minRelevantAmount) {
		this.minRelevantAmount = minRelevantAmount;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public BigDecimal getTopRelevantAmount() {
		return this.topRelevantAmount;
	}

	public void setTopRelevantAmount(BigDecimal topRelevantAmount) {
		this.topRelevantAmount = topRelevantAmount;
	}

	public List<SecurityClassValuator> getSecurityClassValuators() {
		return this.securityClassValuators;
	}

	public void setSecurityClassValuators(List<SecurityClassValuator> securityClassValuators) {
		this.securityClassValuators = securityClassValuators;
	}

	public SecurityClassValuator addSecurityClassValuator(SecurityClassValuator securityClassValuator) {
		getSecurityClassValuators().add(securityClassValuator);
		securityClassValuator.setValuatorProcessClass(this);
		return securityClassValuator;
	}

	public SecurityClassValuator removeSecurityClassValuator(SecurityClassValuator securityClassValuator) {
		getSecurityClassValuators().remove(securityClassValuator);
		securityClassValuator.setValuatorProcessClass(null);
		return securityClassValuator;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public ValuatorProcessClass(String idValuatorClassPk) {
		super();
		this.idValuatorClassPk = idValuatorClassPk;
	}

	public Integer getFormuleType() {
		return formuleType;
	}

	public void setFormuleType(Integer formuleType) {
		this.formuleType = formuleType;
	}
	
	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getIndCfi() {
		return indCfi;
	}

	public void setIndCfi(Integer indCfi) {
		this.indCfi = indCfi;
	}

	public Integer getIndZeroCoupon() {
		return indZeroCoupon;
	}

	public void setIndZeroCoupon(Integer indZeroCoupon) {
		this.indZeroCoupon = indZeroCoupon;
	}

	public Integer getIndFixedCoupon() {
		return indFixedCoupon;
	}

	public void setIndFixedCoupon(Integer indFixedCoupon) {
		this.indFixedCoupon = indFixedCoupon;
	}

	public Integer getIndVariableCoupon() {
		return indVariableCoupon;
	}

	public void setIndVariableCoupon(Integer indVariableCoupon) {
		this.indVariableCoupon = indVariableCoupon;
	}

	public Integer getIdSecurityClassTrg() {
		return idSecurityClassTrg;
	}

	public void setIdSecurityClassTrg(Integer idSecurityClassTrg) {
		this.idSecurityClassTrg = idSecurityClassTrg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((classDescription == null) ? 0 : classDescription.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result
				+ ((formuleType == null) ? 0 : formuleType.hashCode());
		result = prime
				* result
				+ ((idSecurityClassTrg == null) ? 0 : idSecurityClassTrg
						.hashCode());
		result = prime
				* result
				+ ((idValuatorClassPk == null) ? 0 : idValuatorClassPk
						.hashCode());
		result = prime * result + ((indCfi == null) ? 0 : indCfi.hashCode());
		result = prime * result
				+ ((indFixedCoupon == null) ? 0 : indFixedCoupon.hashCode());
		result = prime
				* result
				+ ((indVariableCoupon == null) ? 0 : indVariableCoupon
						.hashCode());
		result = prime * result
				+ ((indZeroCoupon == null) ? 0 : indZeroCoupon.hashCode());
		result = prime * result
				+ ((instrumentType == null) ? 0 : instrumentType.hashCode());
		result = prime * result
				+ ((lastModifyApp == null) ? 0 : lastModifyApp.hashCode());
		result = prime * result
				+ ((lastModifyDate == null) ? 0 : lastModifyDate.hashCode());
		result = prime * result
				+ ((lastModifyIp == null) ? 0 : lastModifyIp.hashCode());
		result = prime * result
				+ ((lastModifyUser == null) ? 0 : lastModifyUser.hashCode());
		result = prime
				* result
				+ ((minRelevantAmount == null) ? 0 : minRelevantAmount
						.hashCode());
		result = prime * result
				+ ((registryDate == null) ? 0 : registryDate.hashCode());
		result = prime * result
				+ ((registryUser == null) ? 0 : registryUser.hashCode());
		result = prime
				* result
				+ ((securityClassValuators == null) ? 0
						: securityClassValuators.hashCode());
		result = prime
				* result
				+ ((topRelevantAmount == null) ? 0 : topRelevantAmount
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValuatorProcessClass other = (ValuatorProcessClass) obj;
		if (!idValuatorClassPk.equals(other.idValuatorClassPk))
			return false;
		return true;
	}

	public Integer getIdConvertibleType() {
		return idConvertibleType;
	}

	public void setIdConvertibleType(Integer idConvertibleType) {
		this.idConvertibleType = idConvertibleType;
	}
}