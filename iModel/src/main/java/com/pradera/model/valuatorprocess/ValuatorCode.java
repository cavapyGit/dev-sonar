package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_CODE database table.
 * 
 */
@Entity
@Table(name="VALUATOR_CODE")
@NamedQuery(name="ValuatorCode.findAll", query="SELECT v FROM ValuatorCode v")
public class ValuatorCode implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_CODE_IDVALUATORCODEPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_CODE_PK",initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_CODE_IDVALUATORCODEPK_GENERATOR")
	@Column(name="ID_VALUATOR_CODE_PK")
	private Long idValuatorCodePk;

	@Column(name="ID_VALUATOR_CODE")
	private String idValuatorCode;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to ValuatorSecurityCode
	@OneToMany(mappedBy="valuatorCode")
	private List<ValuatorSecurityCode> valuatorSecurityCodes;

	public ValuatorCode() {
	}

	public Long getIdValuatorCodePk() {
		return this.idValuatorCodePk;
	}

	public void setIdValuatorCodePk(Long idValuatorCodePk) {
		this.idValuatorCodePk = idValuatorCodePk;
	}

	public String getIdValuatorCode() {
		return this.idValuatorCode;
	}

	public void setIdValuatorCode(String idValuatorCode) {
		this.idValuatorCode = idValuatorCode;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public List<ValuatorSecurityCode> getValuatorSecurityCodes() {
		return this.valuatorSecurityCodes;
	}

	public void setValuatorSecurityCodes(List<ValuatorSecurityCode> valuatorSecurityCodes) {
		this.valuatorSecurityCodes = valuatorSecurityCodes;
	}

	public ValuatorSecurityCode addValuatorSecurityCode(ValuatorSecurityCode valuatorSecurityCode) {
		getValuatorSecurityCodes().add(valuatorSecurityCode);
		valuatorSecurityCode.setValuatorCode(this);

		return valuatorSecurityCode;
	}

	public ValuatorSecurityCode removeValuatorSecurityCode(ValuatorSecurityCode valuatorSecurityCode) {
		getValuatorSecurityCodes().remove(valuatorSecurityCode);
		valuatorSecurityCode.setValuatorCode(null);

		return valuatorSecurityCode;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public ValuatorCode(Long idValuatorCodePk, String idValuatorCode) {
		super();
		this.idValuatorCodePk = idValuatorCodePk;
		this.idValuatorCode = idValuatorCode;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public ValuatorCode(Long idValuatorCodePk) {
		super();
		this.idValuatorCodePk = idValuatorCodePk;
	}
}