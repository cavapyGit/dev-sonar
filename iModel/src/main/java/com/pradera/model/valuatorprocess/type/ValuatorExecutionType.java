package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorExecutionType {

	AUTOMATIC  (Integer.valueOf(1989), "AUTOMATICO"),
	MANUAL (Integer.valueOf(1990), "MANUAL"),
	
	FOR_INS(Integer.valueOf(1), "TIPO DE EJECUCION PARA INSERTS"),
	FOR_UPD(Integer.valueOf(0), "TIPO DE EJECUCION PARA MODIFICACION"),;
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorExecutionType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorExecutionType> list = new ArrayList<ValuatorExecutionType>();
	public static final Map<Integer, ValuatorExecutionType> lookup = new HashMap<Integer, ValuatorExecutionType>();
	static {
		for (ValuatorExecutionType s : EnumSet.allOf(ValuatorExecutionType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorExecutionType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}