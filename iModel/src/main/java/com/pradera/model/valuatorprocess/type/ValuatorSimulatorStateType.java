package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorSimulatorStateType {
	
	SIMULATING  (Integer.valueOf(1997), "SIMULANDO"),
	FINISHED (Integer.valueOf(1998), "FINALIZADO"),
	REGISTERED (Integer.valueOf(1996), "REGISTRADO"),
	ERROR (Integer.valueOf(2071), "ERROR");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorSimulatorStateType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorSimulatorStateType> list = new ArrayList<ValuatorSimulatorStateType>();
	public static final Map<Integer, ValuatorSimulatorStateType> lookup = new HashMap<Integer, ValuatorSimulatorStateType>();
	static {
		for (ValuatorSimulatorStateType s : EnumSet.allOf(ValuatorSimulatorStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorSimulatorStateType get(Integer codigo) {
		return lookup.get(codigo);
	}	
	
}
