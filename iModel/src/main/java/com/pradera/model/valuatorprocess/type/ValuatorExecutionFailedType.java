package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum ValuatorExecutionDetailType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/09/2014
 */
public enum ValuatorExecutionFailedType {
	INCONSISTENT_BALANCES  (Integer.valueOf(1994), "SALDOS INCONSISTENTES"),
	AUDIT_NOT_AVAILABLE (Integer.valueOf(1995), "AUDITORIA NO DISPONIBLE");
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	/**
	 * Instantiates a new valuator execution detail type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private ValuatorExecutionFailedType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param descripcion the new description
	 */
	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	/** The Constant list. */
	public static final List<ValuatorExecutionFailedType> list = new ArrayList<ValuatorExecutionFailedType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ValuatorExecutionFailedType> lookup = new HashMap<Integer, ValuatorExecutionFailedType>();
	static {
		for (ValuatorExecutionFailedType s : EnumSet.allOf(ValuatorExecutionFailedType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the valuator execution detail type
	 */
	public static ValuatorExecutionFailedType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}