package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorInformationType {

	/**the EARLY_MATURITY*/
	BY_VALUATOR_CODE  (Integer.valueOf(1974), "POR CODIGO DE VALORADOR"),
	/**the DIRECT_MATURITY*/
	BY_SECURITY_CODE (Integer.valueOf(1975), "POR CODIGO DE VALOR");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorInformationType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorInformationType> list = new ArrayList<ValuatorInformationType>();
	public static final Map<Integer, ValuatorInformationType> lookup = new HashMap<Integer, ValuatorInformationType>();
	static {
		for (ValuatorInformationType s : EnumSet.allOf(ValuatorInformationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorInformationType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}