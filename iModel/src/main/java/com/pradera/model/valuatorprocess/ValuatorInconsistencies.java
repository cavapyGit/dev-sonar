package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismOperation;


/**
 * The persistent class for the VALUATOR_PROCESS_EXECUTION database table.
 * 
 */
@Entity
@Table(name="VALUATOR_INCONSISTENCIES")
public class ValuatorInconsistencies implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SQ_ID_VALUATOR_INCONSIST_PK_GENERATOR", sequenceName="SQ_ID_VALUATOR_INCONSIST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_VALUATOR_INCONSIST_PK_GENERATOR")
	@Column(name="ID_VALUATOR_INCONSISTENCIES_PK")
	private Long idValuatorInconsistenciesPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_EXECUTION_FK")
	private ValuatorProcessExecution valuatorProcessExecution;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MECHANISM_OPERATION_FK")
    private MechanismOperation mechanismOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACCREDITATION_OPERATION_FK")
	private AccreditationOperation accreditationOperation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",insertable=false,updatable=false)
	private Security security;
	
	@Column(name="ID_SECURITY_CODE_FK")
	private String securityCode;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PARTICIPANT_CODE_FK")
	private Participant participant;

    /** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
	
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

    /** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE_OPERATION")
	private Date marketDateOperation;	
	
	@Column(name="MARKET_RATE_OPERATION")
	private BigDecimal marketRateOperation;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE_OPERATION")
	private BigDecimal marketPriceOperation;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="ROLE")
	private Integer role;

	public Long getIdValuatorInconsistenciesPk() {
		return idValuatorInconsistenciesPk;
	}

	public void setIdValuatorInconsistenciesPk(Long idValuatorInconsistenciesPk) {
		this.idValuatorInconsistenciesPk = idValuatorInconsistenciesPk;
	}

	public ValuatorProcessExecution getValuatorProcessExecution() {
		return valuatorProcessExecution;
	}

	public void setValuatorProcessExecution(
			ValuatorProcessExecution valuatorProcessExecution) {
		this.valuatorProcessExecution = valuatorProcessExecution;
	}

	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}

	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	public AccreditationOperation getAccreditationOperation() {
		return accreditationOperation;
	}

	public void setAccreditationOperation(
			AccreditationOperation accreditationOperation) {
		this.accreditationOperation = accreditationOperation;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Date getMarketDateOperation() {
		return marketDateOperation;
	}

	public void setMarketDateOperation(Date marketDateOperation) {
		this.marketDateOperation = marketDateOperation;
	}

	public BigDecimal getMarketRateOperation() {
		return marketRateOperation;
	}

	public void setMarketRateOperation(BigDecimal marketRateOperation) {
		this.marketRateOperation = marketRateOperation;
	}

	public BigDecimal getMarketPriceOperation() {
		return marketPriceOperation;
	}

	public void setMarketPriceOperation(BigDecimal marketPriceOperation) {
		this.marketPriceOperation = marketPriceOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}