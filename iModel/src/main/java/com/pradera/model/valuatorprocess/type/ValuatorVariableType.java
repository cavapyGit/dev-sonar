package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorVariableType {

	PM  (Integer.valueOf(1980), "PRECIO PROMEDIO PONDERADO"),
	PD (Integer.valueOf(1981), "PAGO DE DIVIDENDOS"),
	RE  (Integer.valueOf(1982), "REGALIAS"),
	VP  (Integer.valueOf(1983), "VALOR PATRIMONIAL PROPORCIONAL"),
	VX  (Integer.valueOf(1984), "PAGO DE DIVIDENDOS EN REGALIA"),
	VC  (Integer.valueOf(1985), "VALOR CUOTA");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorVariableType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorVariableType> list = new ArrayList<ValuatorVariableType>();
	public static final Map<Integer, ValuatorVariableType> lookup = new HashMap<Integer, ValuatorVariableType>();
	static {
		for (ValuatorVariableType s : EnumSet.allOf(ValuatorVariableType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorVariableType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}