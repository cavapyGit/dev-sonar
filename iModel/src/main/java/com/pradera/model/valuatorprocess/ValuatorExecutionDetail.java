package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.settlement.SettlementAccountMarketfact;


/**
 * The persistent class for the VALUATOR_EXECUTION_DETAIL database table.
 * 
 */
@Entity
@Table(name="VALUATOR_EXECUTION_DETAIL")
@NamedQuery(name="ValuatorExecutionDetail.findAll", query="SELECT v FROM ValuatorExecutionDetail v")
public class ValuatorExecutionDetail implements Serializable,Auditable,Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_EXECUTION_DETAIL_IDEXECUTIONDETAILPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_EXEC_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_EXECUTION_DETAIL_IDEXECUTIONDETAILPK_GENERATOR")
	@Column(name="ID_EXECUTION_DETAIL_PK")
	private Long idExecutionDetailPk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Column(name="DETAIL_TYPE")
	private Integer detailType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FINISH_TIME")
	private Date finishTime;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="ORDER_EXECUTION")
	private Integer orderExecution;

	@Column(name="PROCESS_STATE")
	private Integer processState;

	@Column(name="QUANTITY_FINISHED")
	private Long quantityFinished;

	@Column(name="QUANTITY_INCONSISTENT")
	private Long quantityInconsistent;

	@Column(name="QUANTITY_PROCESS")
	private Long quantityProcess;

	@Temporal(TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional many-to-one association to ValuatorProcessExecution
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCESS_EXECUTION_FK")
	private ValuatorProcessExecution valuatorProcessExecution;
	
	@Column(name="SECURITY_CLASS")
	private Integer securityClass;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_EVENT_FK")
	private ValuatorEventBehavior valuatorEventBehavior;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXECUTION_DETAIL_FK")
	private ValuatorExecutionDetail executionDetail;

	//bi-directional many-to-one association to ValuatorExecutionFailed
	@OneToMany(mappedBy="valuatorExecutionDetail")
	private List<ValuatorExecutionFailed> valuatorExecutionFaileds;
	
	@OneToMany(mappedBy="executionDetail", fetch=FetchType.LAZY)
	private List<ValuatorExecutionDetail> valuatorExecutionDetails;

	public ValuatorExecutionDetail() {
	}

	public Long getIdExecutionDetailPk() {
		return this.idExecutionDetailPk;
	}

	public void setIdExecutionDetailPk(Long idExecutionDetailPk) {
		this.idExecutionDetailPk = idExecutionDetailPk;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getFinishTime() {
		return this.finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public ValuatorProcessExecution getValuatorProcessExecution() {
		return this.valuatorProcessExecution;
	}

	public void setValuatorProcessExecution(ValuatorProcessExecution valuatorProcessExecution) {
		this.valuatorProcessExecution = valuatorProcessExecution;
	}

	public List<ValuatorExecutionFailed> getValuatorExecutionFaileds() {
		return this.valuatorExecutionFaileds;
	}

	public void setValuatorExecutionFaileds(List<ValuatorExecutionFailed> valuatorExecutionFaileds) {
		this.valuatorExecutionFaileds = valuatorExecutionFaileds;
	}

	public ValuatorExecutionFailed addValuatorExecutionFailed(ValuatorExecutionFailed valuatorExecutionFailed) {
		getValuatorExecutionFaileds().add(valuatorExecutionFailed);
		valuatorExecutionFailed.setValuatorExecutionDetail(this);

		return valuatorExecutionFailed;
	}

	public ValuatorExecutionFailed removeValuatorExecutionFailed(ValuatorExecutionFailed valuatorExecutionFailed) {
		getValuatorExecutionFaileds().remove(valuatorExecutionFailed);
		valuatorExecutionFailed.setValuatorExecutionDetail(null);

		return valuatorExecutionFailed;
	}

	public Integer getDetailType() {
		return detailType;
	}

	public void setDetailType(Integer detailType) {
		this.detailType = detailType;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getOrderExecution() {
		return orderExecution;
	}

	public void setOrderExecution(Integer orderExecution) {
		this.orderExecution = orderExecution;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Long getQuantityFinished() {
		return quantityFinished;
	}

	public void setQuantityFinished(Long quantityFinished) {
		this.quantityFinished = quantityFinished;
	}

	public Long getQuantityInconsistent() {
		return quantityInconsistent;
	}

	public void setQuantityInconsistent(Long quantityInconsistent) {
		this.quantityInconsistent = quantityInconsistent;
	}

	public Long getQuantityProcess() {
		return quantityProcess;
	}

	public void setQuantityProcess(Long quantityProcess) {
		this.quantityProcess = quantityProcess;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public ValuatorEventBehavior getValuatorEventBehavior() {
		return valuatorEventBehavior;
	}

	public void setValuatorEventBehavior(ValuatorEventBehavior valuatorEventBehavior) {
		this.valuatorEventBehavior = valuatorEventBehavior;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public ValuatorExecutionDetail getExecutionDetail() {
		return executionDetail;
	}

	public void setExecutionDetail(ValuatorExecutionDetail executionDetail) {
		this.executionDetail = executionDetail;
	}

	public List<ValuatorExecutionDetail> getValuatorExecutionDetails() {
		return valuatorExecutionDetails;
	}

	public void setValuatorExecutionDetails(
			List<ValuatorExecutionDetail> valuatorExecutionDetails) {
		this.valuatorExecutionDetails = valuatorExecutionDetails;
	}
	public ValuatorExecutionDetail clone() throws CloneNotSupportedException {
        return (ValuatorExecutionDetail) super.clone();
    }
}