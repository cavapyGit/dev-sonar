package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorType {

	BY_PARTICIPANT		(Integer.valueOf(2081), "POR PARTICIPANTE"),
	BY_INVESTOR			(Integer.valueOf(2082), "POR INVERSIONISTA"),
	BY_SECURITY			(Integer.valueOf(2083), "POR CLAVE VALOR"),
	ENTIRE_PORTFOLIO	(Integer.valueOf(2084), "TODA LA CARTERA");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static final List<ValuatorType> list = new ArrayList<ValuatorType>();
	public static final Map<Integer, ValuatorType> lookup = new HashMap<Integer, ValuatorType>();
	static {
		for (ValuatorType s : EnumSet.allOf(ValuatorType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}