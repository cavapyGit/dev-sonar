package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_PROCESS_EXECUTION database table.
 * 
 */
@Entity
@Table(name="VALUATOR_PROCESS_EXECUTION")
@NamedQuery(name="ValuatorProcessExecution.findAll", query="SELECT v FROM ValuatorProcessExecution v")
public class ValuatorProcessExecution implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long idProcessExecutionPk;
	
	/** The trade operation. */
	@MapsId
	@OneToOne(cascade=CascadeType.PERSIST,fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCESS_EXECUTION_PK")
	private ValuatorProcessOperation valuatorProcessOperation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Column(name="EXECUTION_TYPE")
	private Integer executionType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FINISH_TIME")
	private Date finishTime;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.DATE)
	@Column(name="PROCESS_DATE")
	private Date processDate;

	@Column(name="PROCESS_STATE")
	private Integer processState;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="VALUATOR_TYPE")
	private Integer valuatorType;
	
	@Column(name="IND_ALL_PORTFOLLY")
	private Integer indAllPortFolly;

	@Lob()
	@Column(name="ERROR_MESSAGE")
	private String errorMessage;

	//bi-directional many-to-one association to ValuatorExecutionBalance
	@OneToMany(mappedBy="valuatorProcessExecution")
	private List<ValuatorExecutionParameters> valuatorExecutionParameters;

	//bi-directional many-to-one association to ValuatorExecutionDetail
	@OneToMany(mappedBy="valuatorProcessExecution")
	private List<ValuatorExecutionDetail> valuatorExecutionDetails;

	public ValuatorProcessExecution() {
	}

	public Long getIdProcessExecutionPk() {
		return this.idProcessExecutionPk;
	}

	public void setIdProcessExecutionPk(Long idProcessExecutionPk) {
		this.idProcessExecutionPk = idProcessExecutionPk;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getFinishTime() {
		return this.finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public List<ValuatorExecutionParameters> getValuatorExecutionParameters() {
		return this.valuatorExecutionParameters;
	}

	public void setValuatorExecutionParameters(List<ValuatorExecutionParameters> valuatorExecutionBalances) {
		this.valuatorExecutionParameters = valuatorExecutionBalances;
	}

	public ValuatorExecutionParameters addValuatorExecutionBalance(ValuatorExecutionParameters valuatorExecutionBalance) {
		getValuatorExecutionParameters().add(valuatorExecutionBalance);
		valuatorExecutionBalance.setValuatorProcessExecution(this);

		return valuatorExecutionBalance;
	}

	public ValuatorExecutionParameters removeValuatorExecutionBalance(ValuatorExecutionParameters valuatorExecutionBalance) {
		getValuatorExecutionParameters().remove(valuatorExecutionBalance);
		valuatorExecutionBalance.setValuatorProcessExecution(null);

		return valuatorExecutionBalance;
	}

	public List<ValuatorExecutionDetail> getValuatorExecutionDetails() {
		return this.valuatorExecutionDetails;
	}

	public void setValuatorExecutionDetails(List<ValuatorExecutionDetail> valuatorExecutionDetails) {
		this.valuatorExecutionDetails = valuatorExecutionDetails;
	}

	public ValuatorExecutionDetail addValuatorExecutionDetail(ValuatorExecutionDetail valuatorExecutionDetail) {
		getValuatorExecutionDetails().add(valuatorExecutionDetail);
		valuatorExecutionDetail.setValuatorProcessExecution(this);

		return valuatorExecutionDetail;
	}

	public ValuatorExecutionDetail removeValuatorExecutionDetail(ValuatorExecutionDetail valuatorExecutionDetail) {
		getValuatorExecutionDetails().remove(valuatorExecutionDetail);
		valuatorExecutionDetail.setValuatorProcessExecution(null);

		return valuatorExecutionDetail;
	}

	public ValuatorProcessOperation getValuatorProcessOperation() {
		return this.valuatorProcessOperation;
	}

	public void setValuatorProcessOperation(ValuatorProcessOperation valuatorProcessOperation) {
		this.valuatorProcessOperation = valuatorProcessOperation;
	}

	public Integer getExecutionType() {
		return executionType;
	}

	public void setExecutionType(Integer executionType) {
		this.executionType = executionType;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getValuatorType() {
		return valuatorType;
	}

	public void setValuatorType(Integer valuatorType) {
		this.valuatorType = valuatorType;
	}

	public ValuatorProcessExecution(Long idProcessExecutionPk) {
		super();
		this.idProcessExecutionPk = idProcessExecutionPk;
	}

	public Integer getIndAllPortFolly() {
		return indAllPortFolly;
	}

	public void setIndAllPortFolly(Integer indAllPortFolly) {
		this.indAllPortFolly = indAllPortFolly;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}