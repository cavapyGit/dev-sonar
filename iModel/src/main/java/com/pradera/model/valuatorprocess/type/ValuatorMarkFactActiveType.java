package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorMarkFactActiveType {

	/**FILE TYPE**/
	AC  ("AC"),
	NM  ("NM"),
	NA  ("NA");
	
	/** The code. */
	private String code;

	private ValuatorMarkFactActiveType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static final List<ValuatorMarkFactActiveType> list = new ArrayList<ValuatorMarkFactActiveType>();
	public static final Map<String, ValuatorMarkFactActiveType> lookup = new HashMap<String, ValuatorMarkFactActiveType>();
	static {
		for (ValuatorMarkFactActiveType s : EnumSet.allOf(ValuatorMarkFactActiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorMarkFactActiveType get(String codigo) {
		return lookup.get(codigo);
	}	
}