package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorSecMarkFactType {

	/**the EARLY_MATURITY */
	BY_MARKET  (Integer.valueOf(1972), "POR MERCADO"),
	/**the DIRECT_MATURITY*/
	BY_SECURITY (Integer.valueOf(1973), "POR VALOR");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorSecMarkFactType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorSecMarkFactType> list = new ArrayList<ValuatorSecMarkFactType>();
	public static final Map<Integer, ValuatorSecMarkFactType> lookup = new HashMap<Integer, ValuatorSecMarkFactType>();
	static {
		for (ValuatorSecMarkFactType s : EnumSet.allOf(ValuatorSecMarkFactType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorSecMarkFactType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}