package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorSecurityCodState {

	/**the EARLY_MATURITY - Reporto Anticipado - Selar*/
	REGISTERED  (Integer.valueOf(1967), "REGISTRADO"),
	/**the DIRECT_MATURITY - Reporto Directo Selve*/
	DELETED (Integer.valueOf(1968), "ELIMINADO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorSecurityCodState(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorSecurityCodState> list = new ArrayList<ValuatorSecurityCodState>();
	public static final Map<Integer, ValuatorSecurityCodState> lookup = new HashMap<Integer, ValuatorSecurityCodState>();
	static {
		for (ValuatorSecurityCodState s : EnumSet.allOf(ValuatorSecurityCodState.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorSecurityCodState get(Integer codigo) {
		return lookup.get(codigo);
	}
	
}
