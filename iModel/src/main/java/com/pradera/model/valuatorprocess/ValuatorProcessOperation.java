package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_PROCESS_OPERATION database table.
 * 
 */
@Entity
@Table(name="VALUATOR_PROCESS_OPERATION")
@NamedQuery(name="ValuatorProcessOperation.findAll", query="SELECT v FROM ValuatorProcessOperation v")
public class ValuatorProcessOperation implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_PROCESS_OPERATION_IDVALUATOROPERATIONPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_EXECUTION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_PROCESS_OPERATION_IDVALUATOROPERATIONPK_GENERATOR")
	@Column(name="ID_VALUATOR_OPERATION_PK")
	private Long idValuatorOperationPk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OPERATION_NUMBER")
	private Long operationNumber;

	@Column(name="OPERATION_TYPE")
	private Long operationType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional one-to-one association to ValuatorProcessExecution
	@OneToOne(mappedBy="valuatorProcessOperation", fetch=FetchType.LAZY)
	private ValuatorProcessExecution valuatorProcessExecution;

	public ValuatorProcessOperation() {
	}

	public Long getIdValuatorOperationPk() {
		return this.idValuatorOperationPk;
	}

	public void setIdValuatorOperationPk(Long idValuatorOperationPk) {
		this.idValuatorOperationPk = idValuatorOperationPk;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public ValuatorProcessExecution getValuatorProcessExecution() {
		return this.valuatorProcessExecution;
	}

	public void setValuatorProcessExecution(ValuatorProcessExecution valuatorProcessExecution) {
		this.valuatorProcessExecution = valuatorProcessExecution;
	}

	public Long getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	public Long getOperationType() {
		return operationType;
	}

	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}