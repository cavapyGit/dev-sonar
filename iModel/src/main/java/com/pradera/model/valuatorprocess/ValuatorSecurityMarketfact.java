package com.pradera.model.valuatorprocess;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the VALUATOR_SECURITY_MARKETFACT database table.
 * 
 */
@Entity
@Table(name="VALUATOR_SECURITY_MARKETFACT")
@NamedQuery(name="ValuatorSecurityMarketfact.findAll", query="SELECT v FROM ValuatorSecurityMarketfact v")
public class ValuatorSecurityMarketfact implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_SECURITY_MARKETFACT_IDVALUATORMARKETFACTPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_MARKFACT_PK",initialValue = 1, allocationSize = 1000)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_SECURITY_MARKETFACT_IDVALUATORMARKETFACTPK_GENERATOR")
	@Column(name="ID_VALUATOR_MARKETFACT_PK")
	private Long idValuatorMarketfactPk;

	@Column(name="AVERAGE_RATE")
	private BigDecimal averageRate;

	@Column(name="CIRCULATION_BALANCE")
	private BigDecimal circulationBalance;

	@Column(name="INFORMATION_TYPE")
	private Integer informationType;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MARKET_AMOUNT")
	private BigDecimal marketAmount;

	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="VALUATOR_TYPE")
	private Integer valuatorType;

	//bi-directional many-to-one association to ValuatorSecurityCode
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_SECURITY_FK")
	private ValuatorSecurityCode valuatorSecurityCode;

	public ValuatorSecurityMarketfact() {
	}

	public Long getIdValuatorMarketfactPk() {
		return this.idValuatorMarketfactPk;
	}

	public void setIdValuatorMarketfactPk(Long idValuatorMarketfactPk) {
		this.idValuatorMarketfactPk = idValuatorMarketfactPk;
	}

	public BigDecimal getAverageRate() {
		return this.averageRate;
	}

	public void setAverageRate(BigDecimal averageRate) {
		this.averageRate = averageRate;
	}

	public BigDecimal getCirculationBalance() {
		return this.circulationBalance;
	}

	public void setCirculationBalance(BigDecimal circulationBalance) {
		this.circulationBalance = circulationBalance;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getMarketAmount() {
		return this.marketAmount;
	}

	public void setMarketAmount(BigDecimal marketAmount) {
		this.marketAmount = marketAmount;
	}

	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public ValuatorSecurityCode getValuatorSecurityCode() {
		return this.valuatorSecurityCode;
	}

	public void setValuatorSecurityCode(ValuatorSecurityCode valuatorSecurityCode) {
		this.valuatorSecurityCode = valuatorSecurityCode;
	}

	public Integer getInformationType() {
		return informationType;
	}

	public void setInformationType(Integer informationType) {
		this.informationType = informationType;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getValuatorType() {
		return valuatorType;
	}

	public void setValuatorType(Integer valuatorType) {
		this.valuatorType = valuatorType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}
}