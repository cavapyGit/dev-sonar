package com.pradera.model.valuatorprocess;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the VALUATOR_MARKETFACT_MECHANISM database table.
 * 
 */
@Entity
@Table(name="VALUATOR_MARKETFACT_MECHANISM")
@NamedQuery(name="ValuatorMarketfactMechanism.findAll", query="SELECT v FROM ValuatorMarketfactMechanism v")
public class ValuatorMarketfactMechanism implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_MARKETFACT_MECHANISM_IDVALUATORMECHANISMPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_MECHANISM_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_MARKETFACT_MECHANISM_IDVALUATORMECHANISMPK_GENERATOR")
	@Column(name="ID_VALUATOR_MECHANISM_PK")
	private Long idValuatorMechanismPk;

	@Column(name="AVERAGE_PRICE")
	private BigDecimal averagePrice;

	@Column(name="AVERAGE_RATE")
	private BigDecimal averageRate;

	@Temporal(TemporalType.DATE)
	@Column(name="INFORMATION_DATE")
	private Date informationDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MARKETFACT_DATE")
	private Date marketfactDate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MARKETFACT_ACTIVE")
	private String marketfactActive;

	@Column(name="MARKETFACT_TYPE")
	private Integer marketfactType;

	@Column(name="MECHANISM_CODE")
	private String mechanismCode;

	@Column(name="MINIMUN_AMOUNT")
	private BigDecimal minimunAmount;

	@Column(name="NEGOTIATION_AMOUNT")
	private BigDecimal negotiationAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="SECURITY_CLASS")
	private String securityClass;

	@Column(name="SECURITY_CODE")
	private String securityCode;

	@Column(name="VALUATOR_CODE")
	private String valuatorCode;

	@Column(name="VALUATOR_TYPE")
	private String valuatorType;

	//bi-directional many-to-one association to ValuatorMarketfactConsolidat
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_CONSOLIDAT_FK")
	private ValuatorMarketfactConsolidat valuatorMarketfactConsolidat;

	//bi-directional many-to-one association to ValuatorMarketfactFile
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_FILE_FK")
	private ValuatorMarketfactFile valuatorMarketfactFile;

	public ValuatorMarketfactMechanism() {
	}

	public Long getIdValuatorMechanismPk() {
		return this.idValuatorMechanismPk;
	}

	public void setIdValuatorMechanismPk(Long idValuatorMechanismPk) {
		this.idValuatorMechanismPk = idValuatorMechanismPk;
	}

	public BigDecimal getAveragePrice() {
		return this.averagePrice;
	}

	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}

	public BigDecimal getAverageRate() {
		return this.averageRate;
	}

	public void setAverageRate(BigDecimal averageRate) {
		this.averageRate = averageRate;
	}

	public Date getInformationDate() {
		return this.informationDate;
	}

	public void setInformationDate(Date informationDate) {
		this.informationDate = informationDate;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getMarketfactActive() {
		return this.marketfactActive;
	}

	public void setMarketfactActive(String marketfactActive) {
		this.marketfactActive = marketfactActive;
	}

	public String getMechanismCode() {
		return this.mechanismCode;
	}

	public void setMechanismCode(String mechanismCode) {
		this.mechanismCode = mechanismCode;
	}

	public BigDecimal getMinimunAmount() {
		return this.minimunAmount;
	}

	public void setMinimunAmount(BigDecimal minimunAmount) {
		this.minimunAmount = minimunAmount;
	}

	public BigDecimal getNegotiationAmount() {
		return this.negotiationAmount;
	}

	public void setNegotiationAmount(BigDecimal negotiationAmount) {
		this.negotiationAmount = negotiationAmount;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public String getSecurityClass() {
		return this.securityClass;
	}

	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	public String getSecurityCode() {
		return this.securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getValuatorCode() {
		return this.valuatorCode;
	}

	public void setValuatorCode(String valuatorCode) {
		this.valuatorCode = valuatorCode;
	}

	public String getValuatorType() {
		return this.valuatorType;
	}

	public void setValuatorType(String valuatorType) {
		this.valuatorType = valuatorType;
	}

	public ValuatorMarketfactConsolidat getValuatorMarketfactConsolidat() {
		return this.valuatorMarketfactConsolidat;
	}

	public void setValuatorMarketfactConsolidat(ValuatorMarketfactConsolidat valuatorMarketfactConsolidat) {
		this.valuatorMarketfactConsolidat = valuatorMarketfactConsolidat;
	}

	public ValuatorMarketfactFile getValuatorMarketfactFile() {
		return this.valuatorMarketfactFile;
	}

	public void setValuatorMarketfactFile(ValuatorMarketfactFile valuatorMarketfactFile) {
		this.valuatorMarketfactFile = valuatorMarketfactFile;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getMarketfactType() {
		return marketfactType;
	}

	public void setMarketfactType(Integer marketfactType) {
		this.marketfactType = marketfactType;
	}
	
	
	public Date getMarketfactDate() {
		return marketfactDate;
	}

	public void setMarketfactDate(Date marketfactDate) {
		this.marketfactDate = marketfactDate;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}