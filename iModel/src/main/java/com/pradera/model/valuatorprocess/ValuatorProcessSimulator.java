package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the VALUATOR_PROCESS_SIMULATOR database table.
 * 
 */
@Entity
@Table(name="VALUATOR_PROCESS_SIMULATOR")
@NamedQuery(name="ValuatorProcessSimulator.findAll", query="SELECT v FROM ValuatorProcessSimulator v")
public class ValuatorProcessSimulator implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long idValuatorSimulatorPk;
	
	/** The trade operation. */
	@MapsId
	@OneToOne(cascade=CascadeType.PERSIST,fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_SIMULATOR_PK")
	private ValuatorProcessOperation valuatorProcessOperation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FINISH_TIME")
	private Date finishTime;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK",referencedColumnName="ID_HOLDER_ACCOUNT_PK")
	private HolderAccount holderAccount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",referencedColumnName="ID_SECURITY_CODE_PK")
	private Security security;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="PROCESS_STATE")
	private Integer processState;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="REQUESTER_TYPE")
	private Integer requesterType;

	@Temporal(TemporalType.DATE)
	@Column(name="SIMULATION_DATE")
	private Date simulationDate;

	@Column(name="SIMULATION_NAME")
	private String simulationName;

	@Column(name="SIMULATION_TYPE")
	private Integer simulationType;
	
	@Column(name="SIMULATION_FORM")
	private Integer simulationForm;

	//bi-directional many-to-one association to ValuatorSimulationBalance
	@OneToMany(mappedBy="valuatorProcessSimulator")
	private List<ValuatorSimulationBalance> valuatorSimulationBalances;

	//bi-directional many-to-one association to ValuatorSimulatorCode
	@OneToMany(mappedBy="valuatorProcessSimulator")
	private List<ValuatorSimulatorCode> valuatorSimulatorCodes;

	public ValuatorProcessSimulator() {
	}
	
	public Long getIdValuatorSimulatorPk() {
		return this.idValuatorSimulatorPk;
	}

	public void setIdValuatorSimulatorPk(Long idValuatorSimulatorPk) {
		this.idValuatorSimulatorPk = idValuatorSimulatorPk;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getFinishTime() {
		return this.finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Date getSimulationDate() {
		return this.simulationDate;
	}

	public void setSimulationDate(Date simulationDate) {
		this.simulationDate = simulationDate;
	}

	public String getSimulationName() {
		return this.simulationName;
	}

	public void setSimulationName(String simulationName) {
		this.simulationName = simulationName;
	}

	public ValuatorProcessOperation getValuatorProcessOperation() {
		return this.valuatorProcessOperation;
	}

	public void setValuatorProcessOperation(ValuatorProcessOperation valuatorProcessOperation) {
		this.valuatorProcessOperation = valuatorProcessOperation;
	}

	public List<ValuatorSimulationBalance> getValuatorSimulationBalances() {
		return this.valuatorSimulationBalances;
	}

	public void setValuatorSimulationBalances(List<ValuatorSimulationBalance> valuatorSimulationBalances) {
		this.valuatorSimulationBalances = valuatorSimulationBalances;
	}

	public ValuatorSimulationBalance addValuatorSimulationBalance(ValuatorSimulationBalance valuatorSimulationBalance) {
		getValuatorSimulationBalances().add(valuatorSimulationBalance);
		valuatorSimulationBalance.setValuatorProcessSimulator(this);

		return valuatorSimulationBalance;
	}

	public ValuatorSimulationBalance removeValuatorSimulationBalance(ValuatorSimulationBalance valuatorSimulationBalance) {
		getValuatorSimulationBalances().remove(valuatorSimulationBalance);
		valuatorSimulationBalance.setValuatorProcessSimulator(null);

		return valuatorSimulationBalance;
	}

	public List<ValuatorSimulatorCode> getValuatorSimulatorCodes() {
		return this.valuatorSimulatorCodes;
	}

	public void setValuatorSimulatorCodes(List<ValuatorSimulatorCode> valuatorSimulatorCodes) {
		this.valuatorSimulatorCodes = valuatorSimulatorCodes;
	}

	public ValuatorSimulatorCode addValuatorSimulatorCode(ValuatorSimulatorCode valuatorSimulatorCode) {
		getValuatorSimulatorCodes().add(valuatorSimulatorCode);
		valuatorSimulatorCode.setValuatorProcessSimulator(this);

		return valuatorSimulatorCode;
	}

	public ValuatorSimulatorCode removeValuatorSimulatorCode(ValuatorSimulatorCode valuatorSimulatorCode) {
		getValuatorSimulatorCodes().remove(valuatorSimulatorCode);
		valuatorSimulatorCode.setValuatorProcessSimulator(null);

		return valuatorSimulatorCode;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Integer getRequesterType() {
		return requesterType;
	}

	public void setRequesterType(Integer requesterType) {
		this.requesterType = requesterType;
	}

	public Integer getSimulationType() {
		return simulationType;
	}

	public void setSimulationType(Integer simulationType) {
		this.simulationType = simulationType;
	}
	
	public Integer getSimulationForm() {
		return simulationForm;
	}

	public void setSimulationForm(Integer simulationForm) {
		this.simulationForm = simulationForm;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}