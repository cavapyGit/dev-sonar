package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum ValuatorExecutionDetailType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/09/2014
 */
public enum ValuatorDetailType {
	/** The security. */
	SECURITY  (Integer.valueOf(1991), "VALORACION DE VALORES"),
	/** The balance. */
	BALANCE (Integer.valueOf(1992), "VALORACION DE SALDOS"),
	/** The operations. */
	OPERATIONS (Integer.valueOf(1993), "VALORACION DE OPERACIONES"),
	
	VALIDATIONS  (Integer.valueOf(0), "VALIDACIONES"),;
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	/**
	 * Instantiates a new valuator execution detail type.
	 *
	 * @param code the code
	 * @param descripcion the descripcion
	 */
	private ValuatorDetailType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param descripcion the new description
	 */
	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	/** The Constant list. */
	public static final List<ValuatorDetailType> list = new ArrayList<ValuatorDetailType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ValuatorDetailType> lookup = new HashMap<Integer, ValuatorDetailType>();
	static {
		for (ValuatorDetailType s : EnumSet.allOf(ValuatorDetailType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the valuator execution detail type
	 */
	public static ValuatorDetailType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}