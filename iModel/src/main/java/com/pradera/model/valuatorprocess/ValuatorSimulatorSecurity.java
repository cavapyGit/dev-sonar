package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the VALUATOR_SIMULATOR_SECURITY database table.
 * 
 */
@Entity
@Table(name="VALUATOR_SIMULATOR_SECURITY")
@NamedQuery(name="ValuatorSimulatorSecurity.findAll", query="SELECT v FROM ValuatorSimulatorSecurity v")
public class ValuatorSimulatorSecurity implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	// @SequenceGenerator(name="SQ_ID_VALUATOR_SIMUL_SEC_PK", sequenceName="SQ_ID_VALUATOR_SEC_CLASS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_CLASS_VALUATOR_IDSECURITYVALUATORPK_GENERATOR")
	@Column(name="ID_SIMULATOR_SECURITY_PK")
	private Long idSimulatorSecurityPk;
	
	@Column(name="AVERATE_RATE")
	private BigDecimal averateRate;

	@Column(name="IND_NOMINAL_VALUE")
	private Integer idNominalValue;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="REGISTRY_DATE")
//	private Date registryDate;
//
//	@Column(name="REGISTRY_USER")
//	private String registryUser;

	//bi-directional many-to-one association to ValuatorSimulatorCode
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SIMULATOR_CODE_FK",referencedColumnName="ID_SIMULATOR_CODE_PK")
	private ValuatorSimulatorCode valuatorSimulatorCode;
	
	//bi-directional many-to-one association to Security
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",referencedColumnName="ID_SECURITY_CODE_PK")
	private Security security;
	
	public ValuatorSimulatorSecurity() {}
	
	public Long getIdSimulatorSecurityPk() {
		return idSimulatorSecurityPk;
	}

	public void setIdSimulatorSecurityPk(Long idSimulatorSecurityPk) {
		this.idSimulatorSecurityPk = idSimulatorSecurityPk;
	}

	public BigDecimal getAverateRate() {
		return averateRate;
	}

	public void setAverateRate(BigDecimal averateRate) {
		this.averateRate = averateRate;
	}

	public Integer getIdNominalValue() {
		return idNominalValue;
	}

	public void setIdNominalValue(Integer idNominalValue) {
		this.idNominalValue = idNominalValue;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

//	public Date getRegistryDate() {
//		return registryDate;
//	}
//
//	public void setRegistryDate(Date registryDate) {
//		this.registryDate = registryDate;
//	}
//
//	public String getRegistryUser() {
//		return registryUser;
//	}
//
//	public void setRegistryUser(String registryUser) {
//		this.registryUser = registryUser;
//	}

	public ValuatorSimulatorCode getValuatorSimulatorCode() {
		return valuatorSimulatorCode;
	}

	public void setValuatorSimulatorCode(ValuatorSimulatorCode valuatorSimulatorCode) {
		this.valuatorSimulatorCode = valuatorSimulatorCode;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}