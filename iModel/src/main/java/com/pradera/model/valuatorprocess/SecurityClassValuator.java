package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the SECURITY_CLASS_VALUATOR database table.
 * 
 */
@Entity
@Table(name="SECURITY_CLASS_VALUATOR")
@NamedQuery(name="SecurityClassValuator.findAll", query="SELECT s FROM SecurityClassValuator s")
public class SecurityClassValuator implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SECURITY_CLASS_VALUATOR_IDSECURITYVALUATORPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_SEC_CLASS_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_CLASS_VALUATOR_IDSECURITYVALUATORPK_GENERATOR")
	@Column(name="ID_SECURITY_VALUATOR_PK")
	private Long idSecurityValuatorPk;

	@Column(name="CLASS_STATE")
	private Integer classState;

	@Column(name="ID_SECURITY_CLASS_FK")
	private Integer idSecurityClassFk;
	
	@Column(name="FORMULE_TYPE")
	private Integer formuleType;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	//bi-directional many-to-one association to ValuatorProcessClass
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_CLASS_FK")
	private ValuatorProcessClass valuatorProcessClass;

	public SecurityClassValuator() {
	}
	
	public SecurityClassValuator(Long idSecurityValuatorPk,
			Integer idSecurityClassFk, Integer formuleType, Date registryDate,
			ValuatorProcessClass valuatorProcessClass) {
		super();
		this.idSecurityValuatorPk = idSecurityValuatorPk;
		this.idSecurityClassFk = idSecurityClassFk;
		this.formuleType = formuleType;
		this.registryDate = registryDate;
		this.valuatorProcessClass = valuatorProcessClass;
	}
	
	public Long getIdSecurityValuatorPk() {
		return this.idSecurityValuatorPk;
	}

	public void setIdSecurityValuatorPk(Long idSecurityValuatorPk) {
		this.idSecurityValuatorPk = idSecurityValuatorPk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public ValuatorProcessClass getValuatorProcessClass() {
		return this.valuatorProcessClass;
	}

	public void setValuatorProcessClass(ValuatorProcessClass valuatorProcessClass) {
		this.valuatorProcessClass = valuatorProcessClass;
	}

	public Integer getClassState() {
		return classState;
	}

	public void setClassState(Integer classState) {
		this.classState = classState;
	}

	public Integer getIdSecurityClassFk() {
		return idSecurityClassFk;
	}

	public void setIdSecurityClassFk(Integer idSecurityClassFk) {
		this.idSecurityClassFk = idSecurityClassFk;
	}

	public Integer getFormuleType() {
		return formuleType;
	}

	public void setFormuleType(Integer formuleType) {
		this.formuleType = formuleType;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}