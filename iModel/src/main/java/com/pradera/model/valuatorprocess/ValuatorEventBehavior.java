package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_PROCESS_EXECUTION database table.
 * 
 */
@Entity
@Table(name="VALUATOR_EVENT_BEHAVIOR")
public class ValuatorEventBehavior implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="ID_VALUATOR_EVENT_PK")
	private Long idValuatorEventPk;

	@Column(name="EXECUTION_SEQUENCE")
	private BigDecimal executionSequence;

	@Column(name="EVENT_NAME")
	private String eventName;
	
	@Column(name="ID_VALUATOR_DETAIL_TYPE")
	private Integer valuatorDetailType;

	@Column(name="IND_ACTIVE")
	private Integer indActive;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;


	//bi-directional many-to-one association to ValuatorExecutionBalance
	@OneToMany(mappedBy="valuatorEventBehavior")
	private List<ValuatorExecutionDetail> valuatorExecutionDetails;


	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}


	public Long getIdValuatorEventPk() {
		return idValuatorEventPk;
	}


	public void setIdValuatorEventPk(Long idValuatorEventPk) {
		this.idValuatorEventPk = idValuatorEventPk;
	}


	public BigDecimal getExecutionSequence() {
		return executionSequence;
	}


	public void setExecutionSequence(BigDecimal executionSequence) {
		this.executionSequence = executionSequence;
	}


	public String getEventName() {
		return eventName;
	}


	public void setEventName(String eventName) {
		this.eventName = eventName;
	}


	public Integer getIndActive() {
		return indActive;
	}


	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}


	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	public String getLastModifyIp() {
		return lastModifyIp;
	}


	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	public String getLastModifyUser() {
		return lastModifyUser;
	}


	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	public List<ValuatorExecutionDetail> getValuatorExecutionDetails() {
		return valuatorExecutionDetails;
	}


	public void setValuatorExecutionDetails(
			List<ValuatorExecutionDetail> valuatorExecutionDetails) {
		this.valuatorExecutionDetails = valuatorExecutionDetails;
	}

	public Integer getValuatorDetailType() {
		return valuatorDetailType;
	}

	public void setValuatorDetailType(Integer valuatorDetailType) {
		this.valuatorDetailType = valuatorDetailType;
	}
}