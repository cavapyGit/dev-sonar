package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ValutorProcessExecutionDateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/04/2013
 */
public enum ValuatorProcessExecutionDateType {

	/** The date type. */
	DATE_TYPE(new Integer(2085), "TIPO DE FECHA");
			
	/** The code. */
	private Integer code;
	
	/** The description. */
	private String description;

	/**
	 * Instantiates a new valutor process execution date type.
	 *
	 * @param code the code
	 * @param description the description
	 */
	private ValuatorProcessExecutionDateType(Integer code, String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/** The Constant list. */
	public static final List<ValuatorProcessExecutionDateType> list = new ArrayList<ValuatorProcessExecutionDateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ValuatorProcessExecutionDateType> lookup = new HashMap<Integer, ValuatorProcessExecutionDateType>();
	static {
		for (ValuatorProcessExecutionDateType s : EnumSet.allOf(ValuatorProcessExecutionDateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorProcessExecutionDateType get(Integer code) {
		return lookup.get(code);
	}
}