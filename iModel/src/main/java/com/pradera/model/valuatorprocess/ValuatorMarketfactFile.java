package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the VALUATOR_MARKETFACT_FILE database table.
 * 
 */
@Entity
@Table(name="VALUATOR_MARKETFACT_FILE")
@NamedQuery(name="ValuatorMarketfactFile.findAll", query="SELECT v FROM ValuatorMarketfactFile v")
public class ValuatorMarketfactFile implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_MARKETFACT_FILE_IDVALUATORFILEPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_MARKETFACT_FILE_IDVALUATORFILEPK_GENERATOR")
	@Column(name="ID_VALUATOR_FILE_PK")
	private Long idValuatorFilePk;

	@Column(name="FILE_NAME")
	private String fileName;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="FILE_STATE")
	private Integer fileState;
	
	@Column(name="PROCESS_TYPE")
	private Integer processType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Lob()
	@Basic(fetch=FetchType.LAZY)
	@Column(name="MARKETFACT_FILE")
	private byte[] marketfactFile;

	@Column(name="MARKETFACT_TYPE")
	private Integer marketfactType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Temporal(TemporalType.DATE)
	@Column(name="PROCESS_DATE")
	private Date processDate;
	
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Column(name="ROW_QUANTITY")
	private Long rowQuantity;
	
	@Column(name="FILE_SOURCE")
	private Integer fileSource;

	//bi-directional many-to-one association to ValuatorMarketfactHistory
	@OneToMany(mappedBy="valuatorMarketfactFile",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<ValuatorMarketfactHistory> valuatorMarketfactHistories;

	//bi-directional many-to-one association to ValuatorMarketfactMechanism
	@OneToMany(mappedBy="valuatorMarketfactFile",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<ValuatorMarketfactMechanism> valuatorMarketfactMechanisms;

	public ValuatorMarketfactFile() {
	}

	public Long getIdValuatorFilePk() {
		return this.idValuatorFilePk;
	}

	public void setIdValuatorFilePk(Long idValuatorFilePk) {
		this.idValuatorFilePk = idValuatorFilePk;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public byte[] getMarketfactFile() {
		return this.marketfactFile;
	}

	public void setMarketfactFile(byte[] marketfactFile) {
		this.marketfactFile = marketfactFile;
	}

	public Integer getMarketfactType() {
		return this.marketfactType;
	}

	public void setMarketfactType(Integer marketfactType) {
		this.marketfactType = marketfactType;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public List<ValuatorMarketfactHistory> getValuatorMarketfactHistories() {
		return this.valuatorMarketfactHistories;
	}

	public void setValuatorMarketfactHistories(List<ValuatorMarketfactHistory> valuatorMarketfactHistories) {
		this.valuatorMarketfactHistories = valuatorMarketfactHistories;
	}

	public ValuatorMarketfactHistory addValuatorMarketfactHistory(ValuatorMarketfactHistory valuatorMarketfactHistory) {
		getValuatorMarketfactHistories().add(valuatorMarketfactHistory);
		valuatorMarketfactHistory.setValuatorMarketfactFile(this);

		return valuatorMarketfactHistory;
	}

	public ValuatorMarketfactHistory removeValuatorMarketfactHistory(ValuatorMarketfactHistory valuatorMarketfactHistory) {
		getValuatorMarketfactHistories().remove(valuatorMarketfactHistory);
		valuatorMarketfactHistory.setValuatorMarketfactFile(null);

		return valuatorMarketfactHistory;
	}

	public List<ValuatorMarketfactMechanism> getValuatorMarketfactMechanisms() {
		return this.valuatorMarketfactMechanisms;
	}

	public void setValuatorMarketfactMechanisms(List<ValuatorMarketfactMechanism> valuatorMarketfactMechanisms) {
		this.valuatorMarketfactMechanisms = valuatorMarketfactMechanisms;
	}

	public ValuatorMarketfactMechanism addValuatorMarketfactMechanism(ValuatorMarketfactMechanism valuatorMarketfactMechanism) {
		getValuatorMarketfactMechanisms().add(valuatorMarketfactMechanism);
		valuatorMarketfactMechanism.setValuatorMarketfactFile(this);

		return valuatorMarketfactMechanism;
	}

	public ValuatorMarketfactMechanism removeValuatorMarketfactMechanism(ValuatorMarketfactMechanism valuatorMarketfactMechanism) {
		getValuatorMarketfactMechanisms().remove(valuatorMarketfactMechanism);
		valuatorMarketfactMechanism.setValuatorMarketfactFile(null);

		return valuatorMarketfactMechanism;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getFileState() {
		return fileState;
	}

	public void setFileState(Integer fileState) {
		this.fileState = fileState;
	}

	public Integer getProcessType() {
		return processType;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	
	public Long getRowQuantity() {
		return rowQuantity;
	}

	public void setRowQuantity(Long rowQuantity) {
		this.rowQuantity = rowQuantity;
	}
	
	public Integer getFileSource() {
		return fileSource;
	}

	public void setFileSource(Integer fileSource) {
		this.fileSource = fileSource;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((marketfactType == null) ? 0 : marketfactType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValuatorMarketfactFile other = (ValuatorMarketfactFile) obj;
		if (marketfactType == null) {
			if (other.marketfactType != null)
				return false;
		} else if (!marketfactType.equals(other.marketfactType))
			return false;
		return true;
	}
}