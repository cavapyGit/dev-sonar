package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the VALUATOR_PROCESS_SETUP database table.
 * 
 */
@Entity
@Table(name="VALUATOR_PROCESS_SETUP")
@NamedQuery(name="ValuatorProcessSetup.findAll", query="SELECT v FROM ValuatorProcessSetup v")
public class ValuatorProcessSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_PROCESS_SETUP_IDVALUATORSETUPPK_GENERATOR", sequenceName="SQ_ID_ID_VALUATOR_SETUP_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_PROCESS_SETUP_IDVALUATORSETUPPK_GENERATOR")
	@Column(name="ID_VALUATOR_SETUP_PK")
	private Long idValuatorSetupPk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="EXECUTION_HOUR")
	private Date executionHour;

	@Column(name="IND_AUTOMATIC")
	private Integer indAutomatic;

	@Column(name="IND_MANUAL")
	private Integer indManual;

	@Column(name="IND_VALUATOR_CUSTODY")
	private Integer indValuatorCustody;

	@Column(name="IND_VALUATOR_NEGOTIATIONS")
	private Integer indValuatorNegotiations;

	@Column(name="IND_VALUATOR_PORTFOLY")
	private Integer indValuatorPortfoly;

	@Column(name="IND_VALUATOR_SECURITY")
	private Integer indValuatorSecurity;

	@Column(name="IND_VALUATOS_GUARANTEES")
	private Integer indValuatosGuarantees;
	
	@Column(name="COEFFICIENT_IO")
	private BigDecimal coefficientIO;
	
	@Column(name="LENGTH_CODE_FIXED")
	private Integer lengthCodeFixed;
	
	@Column(name="LENGTH_CODE_VARIABLE")
	private Integer lengthCodeVariable;
	
	@Column(name="PREPAID_CODE")
	private String prepaidCode;
	
	@Column(name="SUBORDINATE_CODE")
	private String subOrdinatedCode;
	
	@Column(name="IND_HISTORY")
	private Integer  indHistory;
	
	@Transient
	private Map<Integer,String> parameters;
	
	@Transient
	private List<SecurityClassValuator> securityClassValuators;
	
	@Transient
	private Map<String,ValuatorProcessClass> valuatorProcessClass;
	
	@Transient
	private List<ValuatorTermRange> valuatorTermRange;
	
	@Transient
	private ConcurrentHashMap<Date,Map<String,Map<Integer,Map<Object,Map<Date,List<BigDecimal>>>>>> securityResultPriceMap;
//	private ConcurrentHashMap<Date,Map<String,Map<Integer,Map<BigDecimal,BigDecimal>>>> securityResultPriceMap;
	
	@Transient
	private ConcurrentHashMap<String,List<ValuatorMarketfactConsolidat>> marketFactConsolidat;
	
	@Transient
	private Object arrayWithPrices;
	
	@Transient
	private ConcurrentHashMap<String,Map<Object,BigDecimal>> theoricMarketPriceMap;
	
	@Transient
	private ConcurrentHashMap<Long,String> valuatorCodeMap;
	
	@Transient
	private ArrayBlockingQueue<Object> securitiesRatePriceTmp;
	
	@Transient private Map<String,Object> securitiesRatePriceMap;

	public ValuatorProcessSetup() {
	}

	public Long getIdValuatorSetupPk() {
		return this.idValuatorSetupPk;
	}

	public void setIdValuatorSetupPk(Long idValuatorSetupPk) {
		this.idValuatorSetupPk = idValuatorSetupPk;
	}

	public Date getExecutionHour() {
		return this.executionHour;
	}

	public void setExecutionHour(Date executionHour) {
		this.executionHour = executionHour;
	}

	public Integer getIndAutomatic() {
		return this.indAutomatic;
	}

	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	}

	public Integer getIndManual() {
		return this.indManual;
	}

	public void setIndManual(Integer indManual) {
		this.indManual = indManual;
	}

	public Integer getIndValuatorCustody() {
		return this.indValuatorCustody;
	}

	public void setIndValuatorCustody(Integer indValuatorCustody) {
		this.indValuatorCustody = indValuatorCustody;
	}

	public Integer getIndValuatorNegotiations() {
		return this.indValuatorNegotiations;
	}

	public void setIndValuatorNegotiations(Integer indValuatorNegotiations) {
		this.indValuatorNegotiations = indValuatorNegotiations;
	}

	public Integer getIndValuatorPortfoly() {
		return this.indValuatorPortfoly;
	}

	public void setIndValuatorPortfoly(Integer indValuatorPortfoly) {
		this.indValuatorPortfoly = indValuatorPortfoly;
	}

	public Integer getIndValuatorSecurity() {
		return this.indValuatorSecurity;
	}

	public void setIndValuatorSecurity(Integer indValuatorSecurity) {
		this.indValuatorSecurity = indValuatorSecurity;
	}

	public Integer getIndValuatosGuarantees() {
		return this.indValuatosGuarantees;
	}

	public void setIndValuatosGuarantees(Integer indValuatosGuarantees) {
		this.indValuatosGuarantees = indValuatosGuarantees;
	}

	public BigDecimal getCoefficientIO() {
		return coefficientIO;
	}

	public void setCoefficientIO(BigDecimal coefficientIO) {
		this.coefficientIO = coefficientIO;
	}

	public Integer getLengthCodeFixed() {
		return lengthCodeFixed;
	}

	public void setLengthCodeFixed(Integer lengthCodeFixed) {
		this.lengthCodeFixed = lengthCodeFixed;
	}

	public Integer getLengthCodeVariable() {
		return lengthCodeVariable;
	}

	public void setLengthCodeVariable(Integer lengthCodeVariable) {
		this.lengthCodeVariable = lengthCodeVariable;
	}

	public Map<Integer, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<Integer, String> parameters) {
		this.parameters = parameters;
	}

	public String getPrepaidCode() {
		return prepaidCode;
	}

	public void setPrepaidCode(String prepaidCode) {
		this.prepaidCode = prepaidCode;
	}

	public String getSubOrdinatedCode() {
		return subOrdinatedCode;
	}

	public void setSubOrdinatedCode(String subOrdinatedCode) {
		this.subOrdinatedCode = subOrdinatedCode;
	}

	public List<SecurityClassValuator> getSecurityClassValuators() {
		return securityClassValuators;
	}

	public void setSecurityClassValuators(List<SecurityClassValuator> securityClassValuators) {
		this.securityClassValuators = securityClassValuators;
	}

	public Map<String,ValuatorProcessClass> getValuatorProcessClass() {
		return valuatorProcessClass;
	}

	public void setValuatorProcessClass(Map<String,ValuatorProcessClass> valuatorProcessClass) {
		this.valuatorProcessClass = valuatorProcessClass;
	}

	public List<ValuatorTermRange> getValuatorTermRange() {
		return valuatorTermRange;
	}

	public void setValuatorTermRange(List<ValuatorTermRange> valuatorTermRange) {
		this.valuatorTermRange = valuatorTermRange;
	}

	public ArrayBlockingQueue<Object> getSecuritiesRatePriceTmp() {
		return securitiesRatePriceTmp;
	}

	public void setSecuritiesRatePriceTmp(
			ArrayBlockingQueue<Object> securitiesRatePriceTmp) {
		this.securitiesRatePriceTmp = securitiesRatePriceTmp;
	}

	public ConcurrentHashMap<String, List<ValuatorMarketfactConsolidat>> getMarketFactConsolidat() {
		return marketFactConsolidat;
	}

	public void setMarketFactConsolidat(
			ConcurrentHashMap<String, List<ValuatorMarketfactConsolidat>> marketFactConsolidat) {
		this.marketFactConsolidat = marketFactConsolidat;
	}

	public ConcurrentHashMap<Date, Map<String, Map<Integer, Map<Object, Map<Date,List<BigDecimal>>>>>> getSecurityResultPriceMap() {
		return securityResultPriceMap;
	}

	public void setSecurityResultPriceMap(ConcurrentHashMap<Date, Map<String, Map<Integer, Map<Object, Map<Date,List<BigDecimal>>>>>> securityResultPriceMap) {
		this.securityResultPriceMap = securityResultPriceMap;
	}

	public Integer getIndHistory() {
		return indHistory;
	}

	public void setIndHistory(Integer indHistory) {
		this.indHistory = indHistory;
	}

	public ConcurrentHashMap<String, Map<Object, BigDecimal>> getTheoricMarketPriceMap() {
		return theoricMarketPriceMap;
	}

	public void setTheoricMarketPriceMap(
			ConcurrentHashMap<String, Map<Object, BigDecimal>> theoricMarketPriceMap) {
		this.theoricMarketPriceMap = theoricMarketPriceMap;
	}

	public ConcurrentHashMap<Long, String> getValuatorCodeMap() {
		return valuatorCodeMap;
	}

	public void setValuatorCodeMap(ConcurrentHashMap<Long, String> valuatorCodeMap) {
		this.valuatorCodeMap = valuatorCodeMap;
	}

	public Object getArrayWithPrices() {
		return arrayWithPrices;
	}

	public void setArrayWithPrices(Object arrayWithPrices) {
		this.arrayWithPrices = arrayWithPrices;
	}

	public Map<String, Object> getSecuritiesRatePriceMap() {
		return securitiesRatePriceMap;
	}

	public void setSecuritiesRatePriceMap(Map<String, Object> securitiesRatePriceMap) {
		this.securitiesRatePriceMap = securitiesRatePriceMap;
	}
}