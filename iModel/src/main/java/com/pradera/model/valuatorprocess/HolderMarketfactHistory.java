package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.model.component.HolderAccountBalance;

/**
 * The persistent class for the HOLDER_MARKETFACT_HISTORY database table.
 * 
 */
@Entity
@Table(name="HOLDER_MARKETFACT_HISTORY")
@NamedQuery(name="HolderMarketfactHistory.findAll", query="SELECT h FROM HolderMarketfactHistory h")
public class HolderMarketfactHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOLDER_MARKETFACT_HISTORY_IDMARKETFACTHISTORYPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_MARKFAC_HYS_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_MARKETFACT_HISTORY_IDMARKETFACTHISTORYPK_GENERATOR")
	@Column(name="ID_MARKETFACT_HISTORY_PK")
	private Long idMarketfactHistoryPk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;

	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Column(name="THEORIC_PRICE")
	private BigDecimal theoricPrice;

	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;

	@Temporal(TemporalType.DATE)
	@Column(name="PROCESS_DATE")
	private Date processDate;
//
//	@Column(name="REGISTRY_USER")
//	private String registryUser;

	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;
	
	@Column(name="AVAILABLE_BALANCE")
	private BigDecimal availableBalance;

	@Column(name="VALUATOR_AMOUNT")
	private BigDecimal valuatorAmount;

	@Column(name="VALUATOR_AMOUNT_OHER") 
	private BigDecimal valuatorAmountOher;

	//bi-directional many-to-one association to HolderAccountBalance
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_HOLDER_ACCOUNT_FK", referencedColumnName="ID_HOLDER_ACCOUNT_PK"),
		@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK"),
		@JoinColumn(name="ID_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK")
		})
	private HolderAccountBalance holderAccountBalance;

	public HolderMarketfactHistory() {
	}

	public Long getIdMarketfactHistoryPk() {
		return this.idMarketfactHistoryPk;
	}

	public void setIdMarketfactHistoryPk(Long idMarketfactHistoryPk) {
		this.idMarketfactHistoryPk = idMarketfactHistoryPk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMarketDate() {
		return this.marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getMarketRate() {
		return this.marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

//	public Date getRegistryDate() {
//		return this.registryDate;
//	}
//
//	public void setRegistryDate(Date registryDate) {
//		this.registryDate = registryDate;
//	}
//
//	public String getRegistryUser() {
//		return this.registryUser;
//	}
//
//	public void setRegistryUser(String registryUser) {
//		this.registryUser = registryUser;
//	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getValuatorAmount() {
		return this.valuatorAmount;
	}

	public void setValuatorAmount(BigDecimal valuatorAmount) {
		this.valuatorAmount = valuatorAmount;
	}

	public BigDecimal getValuatorAmountOher() {
		return this.valuatorAmountOher;
	}

	public void setValuatorAmountOher(BigDecimal valuatorAmountOher) {
		this.valuatorAmountOher = valuatorAmountOher;
	}

	public HolderAccountBalance getHolderAccountBalance() {
		return this.holderAccountBalance;
	}

	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public BigDecimal getTheoricPrice() {
		return theoricPrice;
	}

	public void setTheoricPrice(BigDecimal theoricPrice) {
		this.theoricPrice = theoricPrice;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
}