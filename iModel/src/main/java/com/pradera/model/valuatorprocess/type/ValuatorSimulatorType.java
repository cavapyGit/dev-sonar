package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorSimulatorType {
	
	SIMULATION_PARTIAL  (Integer.valueOf(2035), "SIMULACION PARCIAL"),
	SIMULATION_TOTAL (Integer.valueOf(2034), "SIMULACION TOTAL");
	
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorSimulatorType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorSimulatorType> list = new ArrayList<ValuatorSimulatorType>();
	public static final Map<Integer, ValuatorSimulatorType> lookup = new HashMap<Integer, ValuatorSimulatorType>();
	static {
		for (ValuatorSimulatorType s : EnumSet.allOf(ValuatorSimulatorType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorSimulatorType get(Integer codigo) {
		return lookup.get(codigo);
	}	
	
}
