package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorMarkFactFileType {

	/**FILE TYPE**/
	FIXED_FILE  (Integer.valueOf(1977), "ARCHIVO RTA FIJA"),
	VARIABLE_FILE (Integer.valueOf(1978), "ARCHIVO RTA VARIABLE"),
	HISTORY_FILE  (Integer.valueOf(1979), "ARCHIVO HISTORICO"),
	
	/**FILE STATE**/
	RECEPCIONADO(Integer.valueOf(2025),"RECEPCIONADO"),
	PROCESS(Integer.valueOf(2026),"PROCESANDO"),
	PENDING(Integer.valueOf(2027),"PENDIENTE"),
	REPLACE(Integer.valueOf(2028),"REEMPLAZADO"),
	
	/**PROCESS TYPE**/
	MANUAL(Integer.valueOf(2029),"MANUAL"),
	AUTOMATIC(Integer.valueOf(2030),"AUTOMATICO"),
	
	/**LOADING FILE TYPE**/
	WORKING_DAY(Integer.valueOf(2329),"LABORABLE"),
	HOLIDAY_DAY(Integer.valueOf(2330),"FERIADO"),
	;
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorMarkFactFileType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorMarkFactFileType> list = new ArrayList<ValuatorMarkFactFileType>();
	public static final Map<Integer, ValuatorMarkFactFileType> lookup = new HashMap<Integer, ValuatorMarkFactFileType>();
	static {
		for (ValuatorMarkFactFileType s : EnumSet.allOf(ValuatorMarkFactFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorMarkFactFileType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}