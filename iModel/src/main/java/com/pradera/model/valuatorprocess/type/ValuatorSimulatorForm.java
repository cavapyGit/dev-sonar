package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorSimulatorForm {
	
	BY_VALUATION_CODE  (Integer.valueOf(2002), "POR COD VALORACION"),
	BALANCE (Integer.valueOf(2003), "POR SALDO");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorSimulatorForm(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorSimulatorForm> list = new ArrayList<ValuatorSimulatorForm>();
	public static final Map<Integer, ValuatorSimulatorForm> lookup = new HashMap<Integer, ValuatorSimulatorForm>();
	static {
		for (ValuatorSimulatorForm s : EnumSet.allOf(ValuatorSimulatorForm.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorSimulatorForm get(Integer codigo) {
		return lookup.get(codigo);
	}	
	
}
