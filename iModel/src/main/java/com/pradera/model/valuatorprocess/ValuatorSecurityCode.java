package com.pradera.model.valuatorprocess;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the VALUATOR_SECURITY_CODE database table.
 * 
 */
@Entity
@Table(name="VALUATOR_SECURITY_CODE")
@NamedQuery(name="ValuatorSecurityCode.findAll", query="SELECT v FROM ValuatorSecurityCode v")
public class ValuatorSecurityCode implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALUATOR_SECURITY_CODE_IDVALUATORSECURITYPK_GENERATOR", sequenceName="SQ_ID_VALUATOR_SECURITY_PK",initialValue = 1, allocationSize = 1000)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALUATOR_SECURITY_CODE_IDVALUATORSECURITYPK_GENERATOR")
	@Column(name="ID_VALUATOR_SECURITY_PK")
	private Long idValuatorSecurityPk;

	@Temporal(TemporalType.DATE)
	@Column(name="FINAL_DATE")
	private Date finalDate;

	@Temporal(TemporalType.DATE)
	@Column(name="INITIAL_DATE")
	private Date initialDate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="VALUATOR_SITUATION")
	private Integer valuatorSituation;

	@Column(name="VALUATOR_STATE")
	private Integer valuatorState;

	//bi-directional many-to-one association to Security
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",insertable=false,updatable=false)
	private Security security;
	
	@Column(name="ID_SECURITY_CODE_FK")
	private String securityCode;

	//bi-directional many-to-one association to ValuatorCode
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_CODE_FK")
	private ValuatorCode valuatorCode;
	
	//bi-directional many-to-one association to ValuatorTermRange
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TERM_RANGE_FK")
	private ValuatorTermRange valuatorTermRange;

	//bi-directional many-to-one association to ValuatorSecurityMarketfact
	@OneToMany(mappedBy="valuatorSecurityCode")
	private List<ValuatorSecurityMarketfact> valuatorSecurityMarketfacts;

	public ValuatorSecurityCode() {
	}

	public Long getIdValuatorSecurityPk() {
		return this.idValuatorSecurityPk;
	}

	public void setIdValuatorSecurityPk(Long idValuatorSecurityPk) {
		this.idValuatorSecurityPk = idValuatorSecurityPk;
	}

	public Date getFinalDate() {
		return this.finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Date getInitialDate() {
		return this.initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Security getSecurity() {
		return this.security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public ValuatorCode getValuatorCode() {
		return this.valuatorCode;
	}

	public void setValuatorCode(ValuatorCode valuatorCode) {
		this.valuatorCode = valuatorCode;
	}

	public List<ValuatorSecurityMarketfact> getValuatorSecurityMarketfacts() {
		return this.valuatorSecurityMarketfacts;
	}

	public void setValuatorSecurityMarketfacts(List<ValuatorSecurityMarketfact> valuatorSecurityMarketfacts) {
		this.valuatorSecurityMarketfacts = valuatorSecurityMarketfacts;
	}

	public ValuatorSecurityMarketfact addValuatorSecurityMarketfact(ValuatorSecurityMarketfact valuatorSecurityMarketfact) {
		getValuatorSecurityMarketfacts().add(valuatorSecurityMarketfact);
		valuatorSecurityMarketfact.setValuatorSecurityCode(this);

		return valuatorSecurityMarketfact;
	}

	public ValuatorSecurityMarketfact removeValuatorSecurityMarketfact(ValuatorSecurityMarketfact valuatorSecurityMarketfact) {
		getValuatorSecurityMarketfacts().remove(valuatorSecurityMarketfact);
		valuatorSecurityMarketfact.setValuatorSecurityCode(null);

		return valuatorSecurityMarketfact;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getValuatorSituation() {
		return valuatorSituation;
	}

	public void setValuatorSituation(Integer valuatorSituation) {
		this.valuatorSituation = valuatorSituation;
	}

	public Integer getValuatorState() {
		return valuatorState;
	}

	public void setValuatorState(Integer valuatorState) {
		this.valuatorState = valuatorState;
	}

	public ValuatorTermRange getValuatorTermRange() {
		return valuatorTermRange;
	}

	public void setValuatorTermRange(ValuatorTermRange valuatorTermRange) {
		this.valuatorTermRange = valuatorTermRange;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public ValuatorSecurityCode(Long idValuatorSecurityPk) {
		super();
		this.idValuatorSecurityPk = idValuatorSecurityPk;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
}