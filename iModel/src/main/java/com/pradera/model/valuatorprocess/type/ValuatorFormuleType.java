package com.pradera.model.valuatorprocess.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorFormuleType {

	WITH_COUPONS  (Integer.valueOf(2004), "FORM. VALOR AL RENDIMIENTO CON CUPONES"),
	WITHOUT_COUPONS  (Integer.valueOf(2005), "FORM. VALOR AL DESCUENTO SIN CUPONES"),
	OTHER  (Integer.valueOf(2006), "OTHER");
	
	/** The code. */
	private Integer code;
	/** The description. */
	private String description;

	private ValuatorFormuleType(Integer code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}

	public static final List<ValuatorFormuleType> list = new ArrayList<ValuatorFormuleType>();
	public static final Map<Integer, ValuatorFormuleType> lookup = new HashMap<Integer, ValuatorFormuleType>();
	static {
		for (ValuatorFormuleType s : EnumSet.allOf(ValuatorFormuleType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ValuatorFormuleType get(Integer codigo) {
		return lookup.get(codigo);
	}	
}