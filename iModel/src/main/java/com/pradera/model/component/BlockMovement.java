package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the BLOCK_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="BLOCK_MOVEMENT")
public class BlockMovement implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BLOCK_MOVEMENT_GENERATOR", sequenceName="SQ_ID_BLOCK_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BLOCK_MOVEMENT_GENERATOR")
	@Column(name="ID_BLOCK_MOVEMENT_PK")
	private Long idBlockMovementPk;

	//bi-directional many-to-one association to blockOperationDetail
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;

    //bi-directional many-to-one association to corporativeProcessResult
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_PROCESS_RESULT")
	private CorporativeProcessResult corporativeProcessResult;

    //bi-directional many-to-one association to holderAccount
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

    //bi-directional many-to-one association to isinCode
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

    //bi-directional many-to-one association to participant
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;

    //bi-directional many-to-one association to unblockOperationDetail
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_UNBLOCK_OPERATION_FK")
	private UnblockOperation unblockOperation; 

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	@Column(name="MOVEMENT_QUANTITY")
	private BigDecimal movementQuantity;

	//bi-directional many-to-one association to HolderAccountMovement
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="HOLDER_ACCOUNT_MOVEMENT_FK")
	private HolderAccountMovement holderAccountMovement;

	//bi-directional many-to-one association to MovementType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

    
    public BlockMovement() {
    }

	public Long getIdBlockMovementPk() {
		return this.idBlockMovementPk;
	}

	public void setIdBlockMovementPk(Long idBlockMovementPk) {
		this.idBlockMovementPk = idBlockMovementPk;
	}

	

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	public CorporativeProcessResult getCorporativeProcessResult() {
		return corporativeProcessResult;
	}

	public void setCorporativeProcessResult(
			CorporativeProcessResult corporativeProcessResult) {
		this.corporativeProcessResult = corporativeProcessResult;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	

	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	
	public UnblockOperation getUnblockOperation() {
		return unblockOperation;
	}

	public void setUnblockOperation(UnblockOperation unblockOperation) {
		this.unblockOperation = unblockOperation;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMovementDate() {
		return this.movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public BigDecimal getMovementQuantity() {
		return this.movementQuantity;
	}

	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	public HolderAccountMovement getHolderAccountMovement() {
		return this.holderAccountMovement;
	}

	public void setHolderAccountMovement(HolderAccountMovement holderAccountMovement) {
		this.holderAccountMovement = holderAccountMovement;
	}
	
	public MovementType getMovementType() {
		return this.movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}