package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the HOLDER_ACCOUNT_BALANCE database table.
 * 
 */
@Entity
@Table(name="HOLDER_ACCOUNT_BALANCE_EXP")
public class HolderAccountBalanceExp implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOLDEREXP_BALANCE_GENERATOR", sequenceName="SQ_ID_HOLDEREXP_BALANCE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDEREXP_BALANCE_GENERATOR")
	@Column(name="ID_HOLDER_EXP_PK")
	private Long idHolderExpPk;
	
	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance = BigDecimal.ZERO;
	
	@Column(name="AVAILABLE_BALANCE") 
	private BigDecimal availableBalance = BigDecimal.ZERO;
	
	@Column(name="TRANSIT_BALANCE")
	private BigDecimal transitBalance = BigDecimal.ZERO;
	
	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance = BigDecimal.ZERO;
	
	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance = BigDecimal.ZERO;
	
	@Column(name="OTHER_BLOCK_BALANCE")
	private BigDecimal otherBlockBalance = BigDecimal.ZERO;
	
	@Column(name="RESERVE_BALANCE")
	private BigDecimal reserveBalance = BigDecimal.ZERO;
	
	@Column(name="OPPOSITION_BALANCE")
	private BigDecimal oppositionBalance = BigDecimal.ZERO;
	
	@Column(name="ACCREDITATION_BALANCE")
	private BigDecimal accreditationBalance = BigDecimal.ZERO;

	@Column(name="PURCHASE_BALANCE")
	private BigDecimal purchaseBalance = BigDecimal.ZERO;

	@Column(name="SALE_BALANCE")
	private BigDecimal saleBalance = BigDecimal.ZERO;
	
	@Column(name="REPORTING_BALANCE")
	private BigDecimal reportingBalance = BigDecimal.ZERO;
	
	@Column(name="REPORTED_BALANCE")
	private BigDecimal reportedBalance = BigDecimal.ZERO;
	
	@Column(name="MARGIN_BALANCE")
	private BigDecimal marginBalance = BigDecimal.ZERO;
	
	@Column(name="BORROWER_BALANCE")
	private BigDecimal borrowerBalance = BigDecimal.ZERO;

	@Column(name="LENDER_BALANCE")
	private BigDecimal lenderBalance = BigDecimal.ZERO;

	@Column(name="LOANABLE_BALANCE")
	private BigDecimal loanableBalance = BigDecimal.ZERO;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="ID_HOLDER_ACCOUNT_PK")
	private Long holderAccount;
	
	@Column(name="ID_PARTICIPANT_PK")
	private Long participant;
	
	@Column(name="ID_SECURITY_CODE_PK")
	private String security;

    //For the datatable
    @Transient
    private BigDecimal transferAmmount;
    @Transient
    private BigDecimal copiedAvailableBalance;
    @Transient
    private Boolean selected = false;
    @Transient
    private Boolean disabled = false;
    @Transient
    private BigDecimal pendingSellBalance;
    @Transient
    private Integer registryNumber;
    @Transient
    private String participantDesc;
    @Transient
    private String holderDesc;
    @Transient
    private String accountNumberDesc;
    @Transient
    private Long marketCount;

	public HolderAccountBalanceExp() {
//    	this.id = new HolderAccountBalancePK();
    }
	
	public Long getIdHolderExpPk() {
		return idHolderExpPk;
	}

	public void setIdHolderExpPk(Long idHolderExpPk) {
		this.idHolderExpPk = idHolderExpPk;
	}

	public BigDecimal getAccreditationBalance() {
		return accreditationBalance;
	}

	public void setAccreditationBalance(BigDecimal accreditationBalance) {
		this.accreditationBalance = accreditationBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getBorrowerBalance() {
		return this.borrowerBalance;
	}

	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getLenderBalance() {
		return this.lenderBalance;
	}

	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	public BigDecimal getMarginBalance() {
		return this.marginBalance;
	}

	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	public BigDecimal getOtherBlockBalance() {
		return this.otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getPawnBalance() {
		return this.pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getReportedBalance() {
		return this.reportedBalance;
	}

	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	public BigDecimal getReportingBalance() {
		return this.reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getReserveBalance() {
		return this.reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getTransitBalance() {
		return this.transitBalance;
	}

	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	public BigDecimal getTransferAmmount() {
		return transferAmmount;
	}

	public void setTransferAmmount(BigDecimal transferAmmount) {
		this.transferAmmount = transferAmmount;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}

	/**
	 * @return the copiedAvailableBalance
	 */
	public BigDecimal getCopiedAvailableBalance() {
		return copiedAvailableBalance;
	}

	public void setCopiedAvailableBalance(BigDecimal copiedAvailableBalance) {
		this.copiedAvailableBalance = copiedAvailableBalance;
	}

	public BigDecimal getPendingSellBalance() {
		return pendingSellBalance;
	}

	public void setPendingSellBalance(BigDecimal pendingSellBalance) {
		this.pendingSellBalance = pendingSellBalance;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getRegistryNumber() {
		return registryNumber;
	}

	public void setRegistryNumber(Integer registryNumber) {
		this.registryNumber = registryNumber;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}



	public String getParticipantDesc() {
		return participantDesc;
	}

	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}

	public String getHolderDesc() {
		return holderDesc;
	}

	public void setHolderDesc(String holderDesc) {
		this.holderDesc = holderDesc;
	}

	public String getAccountNumberDesc() {
		return accountNumberDesc;
	}

	public void setAccountNumberDesc(String accountNumberDesc) {
		this.accountNumberDesc = accountNumberDesc;
	}

	/**
	 * @return the marketCount
	 */
	public Long getMarketCount() {
		return marketCount;
	}

	/**
	 * @param marketCount the marketCount to set
	 */
	public void setMarketCount(Long marketCount) {
		this.marketCount = marketCount;
	}

	public Long getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Long getParticipant() {
		return participant;
	}

	public void setParticipant(Long participant) {
		this.participant = participant;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}
	
	
	
	
}