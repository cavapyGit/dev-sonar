package com.pradera.model.component;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


@Entity
@Table(name="CONTROL_BOVEDA")
@IdClass(VaultControlComponentPk.class)
public class VaultControlComponent implements Serializable, Auditable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="BOVEDA")
	private String boveda;
	
	@Id
	@Column(name="ARMARIO")
	private String armario;
	
	@Id
	@Column(name="NIVEL")
	private Integer nivel;
	
	@Id
	@Column(name="CAJA")
	private Integer caja;
	
	@Id
	@Column(name="FOLIO")
	private String folio;
	
	@Id
	@Column(name="INDICE_CAJA")
	private Integer indiceCaja;
	
	@Column(name="ID_SECURITY_CODE_FK")
	private String idSecurityCodeFk;

	@Column(name="CERTIFICADO")
	private Byte[] certificado;
	
	@Column(name="VALOR")
	private String valor;
	
	@Column(name="FECHA_INGRESO")
	private Date fechaIngreso;
	
	@Column(name="FECHA_RETIRO")
	private Date fechaRetiro;
	
	@Column(name="ESTADO")
	private Integer estado;
	
	@Column(name="IND_COUPON")
	private Integer indCoupon;
	
	@Column(name="COUPON_NUMBER")
	private Integer couponNumber;
	
	@Column(name="ID_PENDING_SECURITY_CODE_PK")
	private String idPendingSecurityCodePk;
	
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="ID_PHYSICAL_CERTIFICATE_FK")
	private Long idPhysicalCertificateFk;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "ARMARIO", referencedColumnName = "ARMARIO", updatable = false, insertable = false),
		@JoinColumn(name = "NIVEL", referencedColumnName = "NIVEL", updatable = false, insertable = false),
		@JoinColumn(name = "CAJA", referencedColumnName = "CAJA", updatable = false, insertable = false)
	})
	private VaultControlRules vaultControlRules;
	
	public String getBoveda() {
		return boveda;
	}

	public void setBoveda(String boveda) {
		this.boveda = boveda;
	}

	public String getArmario() {
		return armario;
	}

	public void setArmario(String armario) {
		this.armario = armario;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public Integer getCaja() {
		return caja;
	}

	public void setCaja(Integer caja) {
		this.caja = caja;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public Integer getIndiceCaja() {
		return indiceCaja;
	}

	public void setIndiceCaja(Integer indiceCaja) {
		this.indiceCaja = indiceCaja;
	}

	public Byte[] getCertificado() {
		return certificado;
	}

	public void setCertificado(Byte[] certificado) {
		this.certificado = certificado;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaRetiro() {
		return fechaRetiro;
	}

	public void setFechaRetiro(Date fechaRetiro) {
		this.fechaRetiro = fechaRetiro;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public VaultControlRules getVaultControlRules() {
		return vaultControlRules;
	}

	public void setVaultControlRules(VaultControlRules vaultControlRules) {
		this.vaultControlRules = vaultControlRules;
	}

	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}

	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}
	
	public Integer getIndCoupon() {
		return indCoupon;
	}

	public void setIndCoupon(Integer indCoupon) {
		this.indCoupon = indCoupon;
	}

	public Integer getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getIdPendingSecurityCodePk() {
		return idPendingSecurityCodePk;
	}

	public void setIdPendingSecurityCodePk(String idPendingSecurityCodePk) {
		this.idPendingSecurityCodePk = idPendingSecurityCodePk;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public Long getIdPhysicalCertificateFk() {
		return idPhysicalCertificateFk;
	}

	public void setIdPhysicalCertificateFk(Long idPhysicalCertificateFk) {
		this.idPhysicalCertificateFk = idPhysicalCertificateFk;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {

		return null;
	}
	
}
