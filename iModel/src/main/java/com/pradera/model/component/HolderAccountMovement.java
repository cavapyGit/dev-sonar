package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.guarantees.GuaranteeMovement;
import com.pradera.model.guarantees.GuaranteeOperation;
import com.pradera.model.negotiation.TradeOperation;
import com.pradera.model.settlement.SettlementOperation;


/**
 * The persistent class for the HOLDER_ACCOUNT_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="HOLDER_ACCOUNT_MOVEMENT")
public class HolderAccountMovement implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOLDER_ACCOUNT_MOVEMENT_GENERATOR", sequenceName="SQ_ID_ACCOUNT_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLDER_ACCOUNT_MOVEMENT_GENERATOR")
	@Column(name="ID_HOLDER_ACCOUNT_MOVEMENT_PK")
	private Long idHolderAccountMovementPk;

	@Column(name="CURRENCY")
	private Integer currency;

	//bi-directional many-to-one association to corporativeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;

    //bi-directional many-to-one association to custodyOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CUSTODY_OPERATION_FK")
	private CustodyOperation custodyOperation;
    
    //bi-directional many-to-one association to refCustodyOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REF_CUSTODY_OPERATION_FK")
	private CustodyOperation refCustodyOperation;

    //bi-directional many-to-one association to guaranteeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_GUARANTEE_OPERATION_FK")
	private GuaranteeOperation guaranteeOperation;

	@Column(name="ID_MECHANISM")
	private Long idMechanism;

	@Column(name="ID_MODALITY")
	private Long idModality;

	@Column(name="ID_SOURCE_PARTICIPANT")
	private Long idSourceParticipant;

	@Column(name="ID_TARGET_PARTICIPANT")
	private Long idTargetParticipant;

	//bi-directional many-to-one association to tradeOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_OPERATION_FK")
	private TradeOperation tradeOperation;
    
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	@Column(name="MOVEMENT_QUANTITY")
	private BigDecimal movementQuantity;

	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	@Column(name="OPERATION_NUMBER")
	private Long operationNumber;

	@Column(name="OPERATION_PRICE")
	private BigDecimal operationPrice;

	//bi-directional many-to-one association to BlockMovement
	@OneToMany(mappedBy="holderAccountMovement")
	private Set<BlockMovement> blockMovements;

	//bi-directional many-to-one association to GuaranteeMovement
	@OneToMany(mappedBy="holderAccountMovement")
	private Set<GuaranteeMovement> guaranteeMovements;

	//bi-directional many-to-one association to HolderAccountBalance
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_HOLDER_ACCOUNT_FK", referencedColumnName="ID_HOLDER_ACCOUNT_PK"),
		@JoinColumn(name="ID_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK"),
		@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK")
		})
	private HolderAccountBalance holderAccountBalance;

	//bi-directional many-to-one association to HolderAccountMovement
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_REFERENCE_MOVEMENT_FK")
	private HolderAccountMovement holderAccountMovement;
	
	//bi-directional many-to-one association to MovementType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

    @Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
    
    public HolderAccountMovement() {
    }

	public Long getIdHolderAccountMovementPk() {
		return this.idHolderAccountMovementPk;
	}

	public void setIdHolderAccountMovementPk(Long idHolderAccountMovementPk) {
		this.idHolderAccountMovementPk = idHolderAccountMovementPk;
	}

	public Integer getCurrency() {
		return this.currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	
	public Long getIdMechanism() {
		return this.idMechanism;
	}

	public void setIdMechanism(Long idMechanism) {
		this.idMechanism = idMechanism;
	}

	public Long getIdModality() {
		return this.idModality;
	}

	public void setIdModality(Long idModality) {
		this.idModality = idModality;
	}

	public Long getIdSourceParticipant() {
		return this.idSourceParticipant;
	}

	public void setIdSourceParticipant(Long idSourceParticipant) {
		this.idSourceParticipant = idSourceParticipant;
	}

	public Long getIdTargetParticipant() {
		return this.idTargetParticipant;
	}

	public void setIdTargetParticipant(Long idTargetParticipant) {
		this.idTargetParticipant = idTargetParticipant;
	}

	

	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	public GuaranteeOperation getGuaranteeOperation() {
		return guaranteeOperation;
	}

	public void setGuaranteeOperation(GuaranteeOperation guaranteeOperation) {
		this.guaranteeOperation = guaranteeOperation;
	}

	public TradeOperation getTradeOperation() {
		return tradeOperation;
	}

	public void setTradeOperation(TradeOperation tradeOperation) {
		this.tradeOperation = tradeOperation;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMovementDate() {
		return this.movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public BigDecimal getMovementQuantity() {
		return this.movementQuantity;
	}

	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	public BigDecimal getNominalValue() {
		return this.nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public Date getOperationDate() {
		return this.operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public Long getOperationNumber() {
		return this.operationNumber;
	}

	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	public BigDecimal getOperationPrice() {
		return this.operationPrice;
	}

	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}

	public Set<BlockMovement> getBlockMovements() {
		return this.blockMovements;
	}

	public void setBlockMovements(Set<BlockMovement> blockMovements) {
		this.blockMovements = blockMovements;
	}
	
	public Set<GuaranteeMovement> getGuaranteeMovements() {
		return this.guaranteeMovements;
	}

	public void setGuaranteeMovements(Set<GuaranteeMovement> guaranteeMovements) {
		this.guaranteeMovements = guaranteeMovements;
	}
	
	public HolderAccountBalance getHolderAccountBalance() {
		return this.holderAccountBalance;
	}

	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}
	
	public HolderAccountMovement getHolderAccountMovement() {
		return this.holderAccountMovement;
	}

	public void setHolderAccountMovement(HolderAccountMovement holderAccountMovement) {
		this.holderAccountMovement = holderAccountMovement;
	}
	
	
	public MovementType getMovementType() {
		return this.movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	public CustodyOperation getRefCustodyOperation() {
		return refCustodyOperation;
	}

	public void setRefCustodyOperation(CustodyOperation refCustodyOperation) {
		this.refCustodyOperation = refCustodyOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}