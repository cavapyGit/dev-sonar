package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the SECURITIES_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="SECURITIES_MOVEMENT")
public class SecuritiesMovement implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SECURITIES_MOVEMENT_GENERATOR", sequenceName="SQ_ID_SECURITIES_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITIES_MOVEMENT_GENERATOR")
	@Column(name="ID_SECURITIES_MOVEMENT_PK")
	private Long idSecuritiesMovementPk;

	//bi-directional many-to-one association to securities
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MOVEMENT_AMOUNT")
	private BigDecimal movementAmount;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	@Column(name="MOVEMENT_QUANTITY")
	private BigDecimal movementQuantity;
	
	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;
	
	//bi-directional many-to-one association to MovementType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

	//bi-directional many-to-one association to SecuritiesOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITIES_OPERATION_FK")
	private SecuritiesOperation securitiesOperation;
    
    public SecuritiesMovement() {
    }

	public Long getIdSecuritiesMovementPk() {
		return this.idSecuritiesMovementPk;
	}

	public void setIdSecuritiesMovementPk(Long idSecuritiesMovementPk) {
		this.idSecuritiesMovementPk = idSecuritiesMovementPk;
	}

	

	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getMovementAmount() {
		return this.movementAmount;
	}

	public void setMovementAmount(BigDecimal movementAmount) {
		this.movementAmount = movementAmount;
	}

	public Date getMovementDate() {
		return this.movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public BigDecimal getMovementQuantity() {
		return this.movementQuantity;
	}

	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	public MovementType getMovementType() {
		return this.movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}
	
	public SecuritiesOperation getSecuritiesOperation() {
		return this.securitiesOperation;
	}

	public void setSecuritiesOperation(SecuritiesOperation securitiesOperation) {
		this.securitiesOperation = securitiesOperation;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}		

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}