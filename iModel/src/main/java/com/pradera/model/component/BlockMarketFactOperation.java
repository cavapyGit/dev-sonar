package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;


/**
 * The persistent class for the BLOCK_OPERATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="BLOCK_MARKETFACT_OPERATION")
public class BlockMarketFactOperation implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "SQ_BLOCK_MARKET_OPE_PK", sequenceName = "SQ_ID_BLOCK_MARKET_OPERAT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_BLOCK_MARKET_OPE_PK")
	@Column(name="ID_BLOCK_MARKETFACT_PK")
	private Long idBlockMarketFactOperatPk;
	
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_MARKETFACT_BALANCE_FK")
//	private HolderMarketFactBalance holderMarketFactBalance;
//	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BLOCK_OPERATION_FK")
	private BlockOperation blockOperation;
	
	@Column(name="BLOCK_MARKET_STATE")
	private Integer blockMarketState;
	
	@Column(name="ACTUAL_BLOCK_BALANCE")
	private BigDecimal actualBlockBalance;
	
	@Column(name="ORIGINAL_BLOCK_BALANCE")
	private BigDecimal originalBlockBalance;
	
	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registryUser;

	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;

	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="IND_ACTIVE")
	private Integer indActive = ComponentConstant.ONE;
	
	public Long getIdBlockMarketFactOperatPk() {
		return idBlockMarketFactOperatPk;
	}

	public void setIdBlockMarketFactOperatPk(Long idBlockMarketOperatPk) {
		this.idBlockMarketFactOperatPk = idBlockMarketOperatPk;
	}

//	public HolderMarketFactBalance getHolderMarketFactBalance() {
//		return holderMarketFactBalance;
//	}
//
//	public void setHolderMarketFactBalance(HolderMarketFactBalance holderMarketFactBalance) {
//		this.holderMarketFactBalance = holderMarketFactBalance;
//	}

	public Integer getBlockMarketState() {
		return blockMarketState;
	}

	public void setBlockMarketState(Integer blockMarketState) {
		this.blockMarketState = blockMarketState;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BigDecimal getActualBlockBalance() {
		return actualBlockBalance;
	}

	public void setActualBlockBalance(BigDecimal actualBlockBalance) {
		this.actualBlockBalance = actualBlockBalance;
	}

	public BigDecimal getOriginalBlockBalance() {
		return originalBlockBalance;
	}

	public void setOriginalBlockBalance(BigDecimal originalBlockBalance) {
		this.originalBlockBalance = originalBlockBalance;
	}

	public Integer getIndActive() {
		return indActive;
	}

	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}
}