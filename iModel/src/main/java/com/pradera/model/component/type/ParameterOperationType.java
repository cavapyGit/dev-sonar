package com.pradera.model.component.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ParameterOperationType {

	/* OPERATION TYPES TO SECURITIES */
	SEC_EARLY_REDEMPTION (new Long(10015), "REDENCION DE VALORES DE EMISION RF"),
	SEC_EARLY_REDEMPTION_REVERSION (new Long(10035), "REVERSION DE REDENCION DE VALORES RF"),
	SEC_PRIMARY_PLACEMENT (new Long(20001), "COLOCACION PRIMARIA"),
	/* Corporative Security Operation */
	SEC_CAPITAL_AMORTIZATION (new Long(20003), "PROCESO DE AMORTIZACION DE CAPITAL DE VALOR"),
	SEC_STOCK_DIVIDENDS (new Long(20004), "PROCESO DE ENTREGA DE DIVIDENDOS EN ACCIONES"),
	SEC_PREFERED_SUBSCRIPTION (new Long(20005), "PROCESO DE SUSCRIPCION PREFERENTES DE VALORES"),
	SEC_END_SUBSCRIPTION (new Long(20006), "PROCESO DE TERMINO DE SUSCRIPCION PREFERENTE DE VALORES"),
	SEC_CNV_NO_CAP_VAR (new Long(20007), "PROCESO DE CAMBIO DE VALOR NOMINAL SIN VARIACION DE CAPITAL"),
	SEC_SECURITIES_FUSION (new Long(20008), "PROCESO DE FUSION DE VALORES"),
	SEC_SECURITIES_UNIFICATION (new Long(20009), "PROCESO DE UNIFICACION DE VALORES"),
	SEC_SECURITIES_EXCISION (new Long(20010), "PROCESO DE ESCISION DE VALORES"),
	SEC_SECURITIES_EXCLUSION (new Long(20011), "PROCESO DE EXCLUSION DE VALORES"),
	SEC_CAPITAL_REDUCTION (new Long(20012), "PROCESO DE REDUCCION DE CAPITAL DE VALORES"),
	SEC_RESCUE_CAPITAL (new Long(20016), "RESCATE DE CAPITAL"),
	SEC_CONVERTIBILY_BONE_TO_ACTION (new Long(20023), "CONVERTIBILIDAD DE BONOS EN ACCIONES"),
	
	SEC_SECURITIES_DEMATERIALIZATION_MIXED (new Long(20013), "DESMATERIALIZACION DE VALORES EMISION MIXTA"),
	SEC_SECURITIES_DEMATERIALIZATION (new Long(20056), "DESMATERIALIZACION DE VALORES EMISION DESMATERIALIZADA "),
	SEC_SECURITIES_DONATION (new Long(20014), "DONACION DE VALORES"),
	SEC_SECURITIES_SUSCRIPTION (new Long(20015), "SUSCRIPCION DE VALORES"),
	SEC_SECURITIES_PRIMARY_PLACEMENT_DEMATERIALIZED (new Long(20034), "DESMATERIALIZACION POR COMPRA DE ACCIONES BANCARIAS EMISION DESM."),
	SEC_SECURITIES_AUCTION (new Long(20035), "DESMATERIALIZACION DE VALORES EN SUBASTA"),
	SEC_SECURITIES_DIRECT_PURCHASE_BCB(new Long(20036),"DESMATERIALIZACION DE VALORES EN VENTA DIRECTA BIC"),
	SEC_SECURITIES_DIRECT_PURCHASE_TGN(new Long(20037),"DESMATERIALIZACION DE VALORES EN VENTA DIRECTA TGN"),
	SEC_SECURITIES_MONEY_PURCHASE(new Long(20038),"DESMATERIALIZACION DE VALORES EN COMPRA MESA DE DINERO"),
	SEC_SECURITIES_DIRECT_PURCHASE_PRIVATE_MIXED(new Long(20039),"DESMATERIALIZACION DE VALORES EN COMPRA PRIVADA EMISION MIXTA"),
	SEC_SECURITIES_PRIMARY_PLACEMENT_MIXED(new Long(20042),"DESMATERIALIZACION POR COMPRA DE ACCIONES BANCARIAS EMISION MIXTA"),
	SEC_SECURITIES_DIRECT_PURCHASE_PRIVATE_DEMATERIALIZED(new Long(20043),"DESMATERIALIZACION DE VALORES EN COMPRA PRIVADA EMISION DESM."),
	SEC_SECURITIES_DEMATERIALIZATION_PHYSICAL (new Long(20060), "DESMATERIZALIZACION DE VALORES FISICOS"),
	
	

	SEC_SECURITIES_SERIALIZED_REVERSION (new Long(20040), "REVERSION DE VALORES SERIALIZADOS"),
	SEC_SECURITIES_REDEMPTION_FIXED_INCOME(new Long(20017), "REDENCION DE VALORES RF"),
	SEC_SECURITIES_REDEMPTION_EQUITIES(new Long(20018), "REDENCION DE VALORES RV"),
	SEC_SECURITIES_REVERSION (new Long(20040), "REVERSION DE VALORES"),
	SEC_SECURITIES_ANTICIPATED_REDEMPTION (new Long(20041), "REDENCION ANTICIPADA DE VALORES"),
	SEC_RECTIFICATION_SECURITIES_DEMATERIALIZATION_MIXED (new Long(20024), "RECTIFICACION DE DESMATERIALIZACION DE VALORES EMISION MIXTA"),
	SEC_RECTIFICATION_SECURITIES_DEMATERIALIZATION (new Long(20057), "RECTIFICACION DE DESMATERIALIZACION DE VALORES EMISION DESM."),
	SEC_RECTIFICATION_SECURITIES_DONATION (new Long(20025), "RECTIFICACION DE DONACION DE VALORES"),
	SEC_RECTIFICATION_SECURITIES_SUSCRIPTION (new Long(20026), "RECTIFICACION DE SUSCRIPCION DE VALORES"),
	SEC_RECTIFICATION_SECURITIES_PRIMARY_PLACEMENT_DEMATERIALIZED (new Long(20044), "RECTIFICACION DE DESMATERIALIZACION POR COMPRA DE ACCIONES BANCARIAS EMISION DESM"),
	SEC_RECTIFICATION_SECURITIES_PRIMARY_PLACEMENT_MIXED (new Long(20045), "RECTIFICATION DE DESMATERIALIZACION POR COMPRA DE ACCIONES BANCARIAS EMISION MIXTA"),
	SEC_RECTIFICATION_SECURITIES_DIRECT_PURCHASE_PRIVATE_MIXED (new Long(20046), "RECTIFICACION DE DESMATERIALIZACION POR ADQUISICION DIRECTA PRIVADA EMISION MIXTA "),
	SEC_RECTIFICATION_SECURITIES_DIRECT_PURCHASE_PRIVATE_DEMATERIALIZED (new Long(20047), "RECTIFICACION DE DESMATERIALIZACION POR ADQUISICION DIRECTA PRIVADA EMISION DESM"),
	SEC_RECTIFICATION_SECURITIES_AUCTION (new Long(20048), "RECTIFICACION DE DESMATERIALIZACION POR COMPRA EN SUBASTA"),
	SEC_RECTIFICATION_SECURITIES_DIRECT_PURCHASE_BCB (new Long(20049), "RECTIFICACION DE DESMATERIALIZACION POR COMPRA DIRECTA BIC"),
	SEC_RECTIFICATION_SECURITIES_DIRECT_PURCHASE_TGN (new Long(20050), "RECTIFICACION DE DESMATERIALIZACION POR COMPRA TESORO DIRECTO TGN"),
	SEC_RECTIFICATION_SECURITIES_MONEY_PURCHASE (new Long(20051), "RECTIFICACION DE DESMATERIALIZACION POR COMPRA EN MESA DE DINERO"),
	
	SEC_RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME(new Long(20027), "RECTIFICACION DE REDENCION DE VALORES RF"),
	SEC_RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME_SRC(new Long(20055), "RECTIFICACION DE REDENCION DE VALORES RF (REDENCION ANTICIPADA)"),
	SEC_RECTIFICATION_SECURITIES_REDEMPTION_VARIABLE_INCOME (new Long(20028), "RECTIFICACION DE REDENCION DE VALORES RV"),
	SEC_RECTIFICATION_SECURITIES_REVERSION (new Long(20052), "RECTIFICACION DE REVERSION DE VALORES"),
	
	SEC_SENDING_INTERNATIONAL_SECURITIES (new Long(20029), "ENVIO DE VALORES INTERNACIONALES"),
	SEC_RECEPTION_INTERNATIONAL_SECURITIES (new Long(20030), "RECEPCION DE VALORES INTERNACIONALES"),
	SEC_SPLIT_SECURITIES_DETACHMENT (new Long(20031), "DESPRENDIMIENTO DE SPLIT VALOR"),
	
	
	/* OPERATION TYPES TO CUSTODY */
	SECURITIES_TRANSFER_AVAILABLE_SOURCE_TARGET (new Long(30001), "TRASPASO DE VALORES DISPONIBLES ORIGEN - DESTINO"),
	SECURITIES_TRANSFER_AVAILABLE_TARGET_SOURCE (new Long(30002), "TRASPASO DE VALORES DISPONIBLES DESTINO - ORIGEN"),

	ISSUANCE_DEMATERIALIZATION_GUARDA_MANAGED_SERIALIZED (new Long(20066), "INGRESO POR GUARDA ADMINISTRADA DE VALORES"),
	ISSUANCE_DEMATERIALIZATION_GUARDA_ECLUSIVE_SERIALIZED (new Long(20067), "INGRESO POR GUARDA EXCLUSIVA DE VALORES"),
	
	ISSUANCE_DEMATERIALIZATION_GUARDA_MANAGED (new Long(20062), "INGRESO POR GUARDA DE EMISION"),
	ISSUANCE_DEMATERIALIZATION_GUARDA_ECLUSIVE (new Long(20064), "INGRESO POR GUARDA EXCLUSIVA DE EMISION"),
	SECURITY_RETIREMENT_GUARDA_MANAGED_CONFIRM (new Long(20063), "SALIDA POR GUARDA ADMINISTRADA DE VALORES"),
	SECURITY_RETIREMENT_GUARDA_EXCLUSIVE_CONFIRM (new Long(20065), "SALIDA POR GUARDA EXCLUSIVA DE VALORES"),
	SECURITIES_DEMATERIALIZATION_GUARDA_MANAGED (new Long(30286), "INGRESO POR GUARDA DE VALORES ADMINISTRADA"),
	SECURITIES_DEMATERIALIZATION_GUARDA_EXCLUSIVE (new Long(30287), "INGRESO POR GUARDA EXCLUSIVA DE VALORES"),
	SECURITIES_RETIREMENT_GUARDA_MANAGED (new Long(30285), "SALIDA POR GUARDA DE VALORES ADMINISTRADA"),
	SECURITIES_RETIREMENT_GUARDA_EXCLUSIVE (new Long(30288), "SALIDA POR GUARDA DE VALORES ECVLUSIVA"),
	
	
	SECURITIES_DEMATERIALIZATION_ANNOTATION (new Long(30289), "DESMATERIALIZACION"),
	
	SECURITIES_DEMATERIALIZATION (new Long(30003), "DESMATERIALIZACION DE VALORES"),
	SECURITIES_DONATION (new Long(30004), "DONACION DE VALORES"),
	SECURITIES_SUSCRIPTION (new Long(30005), "SUSCRIPCION DE VALORES"),
	SECURITIES_AUCTION_PURCHASE(new Long(30263),"COMPRA SUBASTA"),
	SECURITIES_DIRECT_PURCHASE_BCB(new Long(30264),"VENTA DIRECTA BIC"),
	SECURITIES_DIRECT_PURCHASE_TGN(new Long(30265),"VENTA DIRECTA TGN"),
	SECURITIES_MONEY_PURCHASE(new Long(30266),"COMPRA MESA DE DINERO"),
	SECURITIES_DIRECT_PURCHASE_PRIVATE(new Long(30267),"COMPRA PRIVADA"),
	SECURITIES_BANK_SHARES_PURCHASE(new Long(30274),"COLOCACION DE ACCIONES BANCARIAS"),
	SECURITIES_BANK_DEMATERIALIZATION_PHYSICAL(new Long(30284),"DESMATERIZALIZACION DE VALORES FISICOS"),
	
	SECURITIES_ACCREDITATION (new Long(30006), "ACREDITACION DE VALORES"),
	ACCREDITATION_CERTIFICATE_UNBLOCK (new Long(30007), "DESBLOQUEO DE CERTIFICADO DE ACREDITACION"),
	CHANGE_OWNERSHIP_ENTIRE_STATE (new Long(30008), "CAMBIO DE TITULARIDAD DE MASA HEREDITARIA"),
	CHANGE_OWNERSHIP_MARRIAGE_PARTNERSHIP_EQUITY (new Long(30009), "CAMBIO DE TITULARIDAD DEL PATRIMONIO DE SOCIEDAD CONYUGAL"),
	CHANGE_OWNERSHIP_COOWNERSHIP_DIVISION_PARTITION (new Long(30010), "CAMBIO DE TITULARIDAD POR DIVISION Y PARTICION DE LA COPROPIEDAD"),
	CHANGE_OWNERSHIP_DONATION (new Long(30011), "CAMBIO DE TITULARIDAD POR DONACION"),
	CHANGE_OWNERSHIP_MERGER_SPLIT (new Long(30012), "CAMBIO DE TITULARIDAD POR FUSION O ESCISION"),
	CHANGE_OWNERSHIP_NONRECOURSE_DEBT (new Long(30013), "CAMBIO DE TITULARIDAD POR DONACION EN PAGO"),
	CHANGE_OWNERSHIP_SWAP (new Long(30014), "CAMBIO DE TITULARIDAD POR PERMUTA"),
	CHANGE_OWNERSHIP_WRIT (new Long(30015), "CAMBIO DE TITULARIDAD POR MANDATO JUDICIAL"),
	CHANGE_OWNERSHIP_DENOMINATION(new Long(30016), "CAMBIO DE TITULARIDAD POR CAMBIO DE RAZON SOCIAL"),
	CHANGE_OWNERSHIP_OTHERS (new Long(30017), "CAMBIO DE TITULARIDAD POR OTROS"),
	SECURITIES_PAWN_BLOCK (new Long(30018), "BLOQUEO DE VALORES EN PRENDA"),
	SECURITIES_BAN_BLOCK (new Long(30019), "BLOQUEO DE VALORES EN EMBARGO"),
	SECURITIES_OTHERS_BLOCK (new Long(30020), "BLOQUEO DE VALORES EN BLOQUEO OTROS"),
	SECURITIES_RESERVE_BLOCK (new Long(30021), "BLOQUEO DE VALORES POR ENCAJE"),
	SECURITIES_OPPOSITION_BLOCK (new Long(30022), "BLOQUEO DE VALORES POR OPOSICION"),
	SECURITIES_PAWN_REBLOCK (new Long(30023), "REBLOQUEO DE VALORES EN PRENDA"),
	SECURITIES_BAN_REBLOCK (new Long(30024), "REBLOQUEO DE VALORES EN EMBARGO"),
	SECURITIES_OTHERS_REBLOCK (new Long(30025), "REBLOQUEO DE VALORES EN BLOQUEO OTROS"),
	SECURITIES_RESERVE_REBLOCK (new Long(30026), "REBLOQUEO DE VALORES POR ENCAJE"),
	SECURITIES_OPPOSITION_REBLOCK (new Long(30027), "REBLOQUEO DE VALORES POR OPOSICION"),
	SECURITIES_PAWN_UNBLOCK (new Long(30028), "DESBLOQUEO DE VALORES EN PRENDA"),
	SECURITIES_BAN_UNBLOCK (new Long(30029), "DESBLOQUEO DE VALORES EN EMBARGO"),
	SECURITIES_OTHERS_UNBLOCK (new Long(30030), "DESBLOQUEO DE VALORES EN BLOQUEO OTROS"),
	SECURITIES_RESERVE_UNBLOCK (new Long(30031), "DESBLOQUEO DE VALORES POR ENCAJE"),
	SECURITIES_OPPOSITION_UNBLOCK (new Long(30032), "DESBLOQUEO DE VALORES POR OPOSICION"),
	SECURITIES_PAWN_UNBLOCK_REBLOCK (new Long(30033), "DESBLOQUEO DE REBLOQUEO DE VALORES EN PRENDA"),
	SECURITIES_BAN_UNBLOCK_REBLOCK (new Long(30034), "DESBLOQUEO DE REBLOQUEO DE VALORES EN EMBARGO"),
	SECURITIES_OTHERS_UNBLOCK_REBLOCK (new Long(30035), "DESBLOQUEO DE REBLOQUEO DE VALORES EN BLOQUEO OTROS"),
	SECURITIES_RESERVE_UNBLOCK_REBLOCK (new Long(30036), "DESBLOQUEO DE REBLOQUEO DE VALORES POR ENCAJE"),
	SECURITIES_OPPOSITION_UNBLOCK_REBLOCK (new Long(30037), "DESBLOQUEO DE REBLOQUEO DE VALORES POR OPOSICION"),
	
	SECURITIES_REDEMPTION_FIXED_INCOME (new Long(30038), "REDENCION DE VALORES RF"),
	SECURITIES_REDEMPTION_EQUITIES (new Long(30039), "REDENCION DE VALORES RV"),
	SECURITIES_REVERSION (new Long(30040), "REVERSION DE VALORES"),
	SECURITIES_EARLY_PAYMENT (new Long(30276), "REDENCION ANTICIPADA DE VALORES"),
	
	PARTICIPANT_UNION(new Long(30041), "UNIFICACION DE PARTICIPANTES"),
	
	PHYSICAL_CERTIFICATE_DEPOSIT (new Long(30051), "DEPOSITO DE TITULOS FISICOS"),
	PHYSICAL_CERTIFICATE_RETIREMENT (new Long(30052), "RETIRO DE TITULOS FISICOS"),
	
	RECTIFICATION_SECURITIES_TRANSFER_AVAILABLE (new Long(30042), "RECTIFICACION DE TRASPASO DE SALDO DISPONIBLE"),
	RECTIFICATION_SECURITIES_TRANSFER_BLOCK (new Long(30043), "RECTIFICACION DE TRASPASO DE SALDO BLOQUEADO"),
	
	RECTIFICATION_CHANGE_OWNERSHIP_AVAILABLE (new Long(30044), "RECTIFICACION DE CAMBIO DE TITULARIDAD DE SALDO DISPONIBLE"),
	RECTIFICATION_CHANGE_OWNERSHIP_BLOCK (new Long(30045), "RECTIFICACION DE CAMBIO DE TITULARIDAD DE SALDO BLOQUEADO"),
	
	RECTIFICATION_SECURITIES_DEMATERIALIZATION (new Long(30046), "RECTIFICACION DE DESMATERIALIZACION DE VALORES"),
	RECTIFICATION_SECURITIES_DONATION (new Long(30047), "RECTIFICACION DE DONACION DE VALORES"),
	RECTIFICATION_SECURITIES_SUSPCRITION (new Long(30048), "RECTIFICACION DE SUSCRIPCION DE VALORES"),
	RECTIFICATION_SECURITIES_AUCTION (new Long(30268), "RECTIFICATION DE DESMATERIALIZACION DE VALORES EN SUBASTA"),
	RECTIFICATION_SECURITIES_DIRECT_SALE_BCB (new Long(30269), "RECTIFICATION DE DESMATERIALIZACION DE VALORES EN VENTA DIRECTA BIC"),
	RECTIFICATION_SECURITIES_DIRECT_SALE_TGN (new Long(30270), "RECTIFICATION DE DESMATERIALIZACION DE VALORES EN VENTA DIRECTA TGN"),
	RECTIFICATION_SECURITIES_MONEY_PURCHASE (new Long(30271), "RECTIFICATION DE DESMATERIALIZACION DE VALORES EN COMPRA MESA DE DINERO"),
	RECTIFICATION_SECURITIES_PRIVATE_SALE (new Long(30272), "RECTIFICATION DE DESMATERIALIZACION DE VALORES EN COMPRA PRIVADA"),
	RECTIFICATION_SECURITIES_BANK_SHARES_PURCHASE(new Long(30275),"RECTIFICATION DE DESMATERIALIZACION DE ACCIONES BANCARIAS"),
	
	RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME (new Long(30278), "RECTIFICACION DE REDENCION DE VALORES RF"),
	RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME_SRC (new Long(30281), "RECTIFICACION DE REDENCION DE VALORES RF (REDENCION ANTICIPADA)"),
	RECTIFICATION_SECURITIES_REDEMPTION_VARIABLE_INCOME (new Long(30049), "RECTIFICACION DE REDENCION DE VALORES RV"),
	RECTIFICATION_SECURITIES_REVERSION (new Long(30050), "RECTIFICACION DE REVERSION DE VALORES"),

	/* OPERATION TYPES TO NEGOTIAONS */
	RECEPTION_INTERNATIONAL_SECURITIES (new Long(30053), "RECEPCION DE VALORES INTERNACIONALES"),
	UPDATE_SENDING_INTERNATIONAL_SECURITIES (new Long(30234), "CANCELACION DE ENVIO DE VALORES INTERNACIONALES"),
	SENDING_INTERNATIONAL_SECURITIES (new Long(30054), "ENVIO DE VALORES INTERNACIONALES"),
	UPDATE_RECEPTION_INTERNATIONAL_SECURITIES (new Long(30235), "CANCELACION DE RECEPCION DE VALORES INTERNACIONALES"),
	CASH_FIXED_INCOME (new Long(30055), "NEGOCIACION DE CONTADO RENTA FIJA"),
	CASH_EQUITIES (new Long(30056), "NEGOCIACION DE CONTADO RENTA VARIABLE"),
	REPORTO_FIXED_INCOME (new Long(30057), "NEGOCIACION DE REPORTO RENTA FIJA"),
	REPORTO_EQUITIES (new Long(30058), "NEGOCIACION DE REPORTO RENTA VARIABLE"),
	PRIMARY_PLACEMENT_FIXED_INCOME (new Long(30059), "NEGOCIACION DE COLOCACION PRIMARIA RENTA FIJA"),
	PRIMARY_PLACEMENT_EQUITIES (new Long(30060), "NEGOCIACION DE COLOCACION PRIMARIA RENTA VARIABLE"),
	SECUNDARY_REPO_FIXED_INCOME (new Long(30061), "NEGOCIACION DE REPO SECUNDARIO RENTA FIJA"),
	SECUNDARY_REPO_EQUITIES (new Long(30062), "NEGOCIACION DE REPO SECUNDARIO RENTA VARIABLE"),
	VIEW_REPORTO_FIXED_INCOME (new Long(30063), "NEGOCIACION DE REPORTO A LA VISTA RENTA FIJA"),
	VIEW_REPORTO_EQUITIES (new Long(30064), "NEGOCIACION DE REPORTO A LA VISTA RENTA VARIABLE"),
	COUPON_SPLIT_FIXED_INCOME (new Long(30065), "NEGOCIACION SPLIT DE CUPONES RENTA FIJA"),
	SWAP_FIXED_INCOME (new Long(30066), "NEGOCIACION DE PERMUTA RENTA FIJA"),
	SWAP_EQUITIES (new Long(30067), "NEGOCIACION DE PERMUTA RENTA VARIABLE"),
	SECURITIES_LOAN_FIXED_INCOME (new Long(30068), "NEGOCIACION DE PRESTAMO DE VALORES RENTA FIJA"),
	SECURITIES_LOAN_EQUITIES (new Long(30069), "NEGOCIACION DE PRESTAMO DE VALORES RENTA VARIABLE"),
	FOWARD_SALE_FIXED_INCOME (new Long(30070), "NEGOCIACION FOWARD RENTA FIJA"),
	FOWARD_SALE_EQUITIES (new Long(30071), "NEGOCIACION FOWARD RENTA VARIABLE"),
	REPURCHASE_FIXED_INCOME (new Long(30072), "NEGOCIACION DE RECOMPRA RENTA FIJA"),
	REPURCHASE_EQUITIES (new Long(30073), "NEGOCIACION DE RECOMPRA RENTA VARIABLE"),
	SALE_FIXED_INCOME (new Long(30250), "NEGOCIACION SUBASTA RF"),
	SALE_EQUITIES (new Long(30251), "NEGOCIACION SUBASTA RV"),
	REVERT_REPORT_FIXED_INCOME (new Long(30252), "NEGOCIACION DE REPORTO REVERSO RENTA FIJA"),
	
	CHAIN_CASH_SETTLEMENT (new Long(30262), "LIQUIDACION DE CADENA DE MODALIDADES CONTADO"),
	CHAIN_REPO_SETTLEMENT (new Long(30273), "LIQUIDACION DE CADENA DE MODALIDADES REPORTO"),
	
	FORCED_PURCHASE_SETTLEMENT (new Long(30277), "LIQUIDACION POR COMPRA FORZOSA"),
	
	SECURITIES_DETACHMENT (new Long(30232), "DESPRENDIMIENTO DE CUPON"),
	SECURITIES_DETACHMENT_NOT_BLOCKED (new Long(30233), "DESPRENDIMIENTO DE CUPON SIN SALDO DISPONIBLE"),
	
	/* sirtex modification */
	SIRTEX_CASH_FIXED_INCOME_MODIFY (new Long(30246), "MODIFICACION SIRTEX DE CONTADO RENTA FIJA"),
	SIRTEX_CASH_EQUITIES_MODIFY (new Long(30247), "MODIFICACION SIRTEX DE CONTADO RENTA VARIABLE"),
	SIRTEX_REPORTO_FIXED_INCOME_MODIFY (new Long(30248), "MODIFICACION SIRTEX DE REPORTO RENTA FIJA"),
	SIRTEX_REPORTO_EQUITIES_MODIFY (new Long(30249), "MODIFICACION SIRTEX DE REPORTO RENTA VARIABLE"),
	
	/* guarantees */
	INITIAL_GUARANTEES_MARGIN_BLOCK (new Long(30074), "BLOQUEO POR PRESENTACION DE GARANTIA INICIAL"),
	REPLACEMENT_GUARANTEES_MARGIN_BLOCK (new Long(30075), "BLOQUEO POR ENTREGA DE MARGEN DE GARANTIA ADICIONAL"),
	ADITIONAL_GUARANTEES_MARGIN_BLOCK (new Long(30076), "BLOQUEO POR REPOSICION DE MARGEN DE GARANTIA"),
	GUARANTEES_MARGIN_UNBLOCK (new Long(30077), "LIBERACION DE MARGEN DE GARANTIA"),
	GUARANTEES_MARGIN_FINISHING (new Long(30078), "CULMINACION DE GARANTIAS EN MARGEN"),
	GUARANTEES_DIVIDENDS_PRINCIPAL_FINISHING (new Long(30079), "CULMINACION DE GARANTIAS EN DIVIDENDOS DE PRINCIPAL"),
	GUARANTEES_DIVIDENDS_MARGIN_FINISHING (new Long(30080), "CULMINACION DE GARANTIAS EN DIVIDENDOS DE MARGEN"),
	
	/* Corporative Process Operation types */
	AMORTIZATION_AVAILABLE (new Long(30101), "AMORTIZACION DE SALDO DISPONIBLE"),
	AMORTIZATION_PAWN (new Long(30102), "AMORTIZACION DE SALDO EN PRENDA"),
	AMORTIZATION_BAN (new Long(30103), "AMORTIZACION DE SALDO EN EMBARGO"),
	AMORTIZATION_OTHER (new Long(30104), "AMORTIZACION DE SALDO EN BLOQUEO OTROS"),
	AMORTIZATION_RESERVE (new Long(30105), "AMORTIZACION DE SALDO EN ENCAJE"),
	AMORTIZATION_OPPOSTION (new Long(30106), "AMORTIZACION DE SALDO EN OPOSICION"),
	
	RESCUE_AVAILABLE (new Long(30107), "RESCATE DE CAPITAL"),
	
	STOCK_DIVIDENDS_AVAILABLE (new Long(30108), "DIVIDENDOS EN ACCIONES"),
	STOCK_DIVIDENDS_PAWN (new Long(30109), "DIVIDENDOS EN ACCIONES EN PRENDA"),
	STOCK_DIVIDENDS_BAN (new Long(30110), "DIVIDENDOS EN ACCIONES EN EMBARGO"),
	STOCK_DIVIDENDS_OTHER (new Long(30111), "DIVIDENDOS EN ACCIONES EN BLOQUEO OTROS"),
	STOCK_DIVIDENDS_RESERVE (new Long(30112), "DIVIDENDOS EN ACCIONES EN ENCAJE"),
	STOCK_DIVIDENDS_OPPOSTION (new Long(30113), "DIVIDENDOS EN ACCIONES EN OPOSICION"),
	STOCK_DIVIDENDS_REPORTED (new Long(30114), "DIVIDENDOS EN ACCIONES EN REPORTADO"),
	STOCK_DIVIDENDS_REPORTING (new Long(30115), "DIVIDENDOS EN ACCIONES EN GARANTIA PRINCIPAL"),
	STOCK_DIVIDENDS_GUARANTEE (new Long(30116), "DIVIDENDOS EN ACCIONES EN MARGEN DE GARANTIA"),
	
	SUSCRIPTION_AVAILABLE (new Long(30117), "SUSCRIPCION DE VALORES PREFERENTES"),
	SUSCRIPTION_PAWN (new Long(30118), "SUSCRIPCION DE VALORES PREFERENTES EN PRENDA"),
	SUSCRIPTION_BAN (new Long(30119), "SUSCRIPCION DE VALORES PREFERENTES EN EMBARGO"),
	SUSCRIPTION_OTHER (new Long(30120), "SUSCRIPCION DE VALORES PREFERENTES EN BLOQUEO OTROS"),
	SUSCRIPTION_RESERVE (new Long(30121), "SUSCRIPCION DE VALORES PREFERENTES EN ENCAJE"),
	SUSCRIPTION_OPPOSTION (new Long(30122), "SUSCRIPCION DE VALORES PREFERENTES EN OPOSICION"),
	
	END_SUSCRIPTION_AVAILABLE (new Long(30123), "TERMINO DE SUSCRIPCION DE VALORES PREFERENTES"),
	END_SUSCRIPTION_PAWN (new Long(30124), "TERMINO DE SUSCRIPCION DE VALORES PREFERENTES EN PRENDA"),
	END_SUSCRIPTION_BAN (new Long(30125), "TERMINO DE SUSCRIPCION DE VALORES PREFERENTES EN EMBARGO"),
	END_SUSCRIPTION_OTHER (new Long(30126), "TERMINO DE SUSCRIPCION DE VALORES PREFERENTES EN BLOQUEO OTROS"),
	END_SUSCRIPTION_RESERVE (new Long(30127), "TERMINO DE SUSCRIPCION DE VALORES PREFERENTES EN ENCAJE"),
	END_SUSCRIPTION_OPPOSTION (new Long(30128), "TERMINO DE SUSCRIPCION DE VALORES PREFERENTES EN OPOSICION"),
	
	CNV_NO_VAR_AVAILABLE (new Long(30129), "CVN SIN VARIACION DE CAPITAL"),
	CNV_NO_VAR_PAWN (new Long(30130), "CVN SIN VARIACION DE CAPITAL EN PRENDA"),
	CNV_NO_VAR_BAN (new Long(30131), "CVN SIN VARIACION DE CAPITAL EN EMBARGO"),
	CNV_NO_VAR_OTHER (new Long(30132), "CVN SIN VARIACION DE CAPITAL EN BLOQUEO OTROS"),
	CNV_NO_VAR_RESERVE (new Long(30133), "CVN SIN VARIACION DE CAPITAL EN ENCAJE"),
	CNV_NO_VAR_OPPOSTION (new Long(30134), "CVN SIN VARIACION DE CAPITAL EN OPOSICION"),
	CNV_NO_VAR_REBLOCK_PAWN (new Long(30135), "CVN SIN VARIACION DE CAPITAL EN REBLOQUEO PRENDA"),
	CNV_NO_VAR_REBLOCK_BAN (new Long(30136), "CVN SIN VARIACION DE CAPITAL EN REBLOQUEO EMBARGO"),
	CNV_NO_VAR_REBLOCK_OTHER (new Long(30137), "CVN SIN VARIACION DE CAPITAL EN REBLOQUEO BLOQUEO OTROS"),
	CNV_NO_VAR_REBLOCK_RESERVE (new Long(30138), "CVN SIN VARIACION DE CAPITAL EN REBLOQUEO ENCAJE"),
	CNV_NO_VAR_REBLOCK_OPPOSTION (new Long(30139), "CVN SIN VARIACION DE CAPITAL EN REBLOQUEO OPOSICION"),
	CNV_NO_VAR_REPORTED (new Long(30140), "CVN SIN VARIACION DE CAPITAL EN REPORTADO"),
	CNV_NO_VAR_REPORTING (new Long(30141), "CVN SIN VARIACION DE CAPITAL EN GARANTIA PRINCIPAL"),
	CNV_NO_VAR_GUARANTEE (new Long(30142), "CVN SIN VARIACION DE CAPITAL EN MARGEN DE GARANTIA"),
	CNV_NO_VAR_PRINCIPAL_DIVIDENDS (new Long(30143), "CVN SIN VARIACION DE CAPITAL EN DIVIDENDOS DE PRINCIPAL"),
	CNV_NO_VAR_MARGIN_DIVIDENDS (new Long(30144), "CVN SIN VARIACION DE CAPITAL EN DIVIDENDOS DE MARGEN"),

	CNV_VAR_AVAILABLE (new Long(30145), "CVN CON VARIACION DE CAPITAL"),
	
	FUSION_AVAILABLE (new Long(30146), "FUSION DE VALORES"),
	FUSION_PAWN (new Long(30147), "FUSION DE VALORES EN PRENDA"),
	FUSION_BAN (new Long(30148), "FUSION DE VALORES EN EMBARGO"),
	FUSION_OTHER (new Long(30149), "FUSION DE VALORES EN BLOQUEO OTROS"),
	FUSION_RESERVE (new Long(30150), "FUSION DE VALORES EN ENCAJE"),
	FUSION_OPPOSTION (new Long(30151), "FUSION DE VALORES EN OPOSICION"),
	FUSION_REBLOCK_PAWN (new Long(30152), "FUSION DE VALORES EN REBLOQUEO PRENDA"),
	FUSION_REBLOCK_BAN (new Long(30153), "FUSION DE VALORES EN REBLOQUEO EMBARGO"),
	FUSION_REBLOCK_OTHER (new Long(30154), "FUSION DE VALORES EN REBLOQUEO BLOQUEO OTROS"),
	FUSION_REBLOCK_RESERVE (new Long(30155), "FUSION DE VALORES EN REBLOQUEO ENCAJE"),
	FUSION_REBLOCK_OPPOSTION (new Long(30156), "FUSION DE VALORES EN REBLOQUEO OPOSICION"),
	FUSION_REPORTED (new Long(30157), "FUSION DE VALORES EN REPORTADO"),
	FUSION_REPORTING (new Long(30158), "FUSION DE VALORES EN GARANTIA PRINCIPAL"),
	FUSION_GUARANTEE (new Long(30159), "FUSION DE VALORES EN MARGEN DE GARANTIA"),
	FUSION_PRINCIPAL_DIVIDENDS (new Long(30160), "FUSION DE VALORES EN DIVIDENDOS DE PRINCIPAL"),
	FUSION_MARGIN_DIVIDENDS (new Long(30161), "FUSION DE VALORES EN DIVIDENDOS DE MARGEN"),
	
	UNIFICATION_AVAILABLE (new Long(30162), "UNIFICACION DE VALORES"),
	UNIFICATION_PAWN (new Long(30163), "UNIFICACION DE VALORES EN PRENDA"),
	UNIFICATION_BAN (new Long(30164), "UNIFICACION DE VALORES EN EMBARGO"),
	UNIFICATION_OTHER (new Long(30165), "UNIFICACION DE VALORES EN BLOQUEO OTROS"),
	UNIFICATION_RESERVE (new Long(30166), "UNIFICACION DE VALORES EN ENCAJE"),
	UNIFICATION_OPPOSTION (new Long(30167), "UNIFICACION DE VALORES EN OPOSICION"),
	UNIFICATION_REBLOCK_PAWN (new Long(30168), "UNIFICACION DE VALORES EN REBLOQUEO PRENDA"),
	UNIFICATION_REBLOCK_BAN (new Long(30169), "UNIFICACION DE VALORES EN REBLOQUEO EMBARGO"),
	UNIFICATION_REBLOCK_OTHER (new Long(30170), "UNIFICACION DE VALORES EN REBLOQUEO BLOQUEO OTROS"),
	UNIFICATION_REBLOCK_RESERVE (new Long(30171), "UNIFICACION DE VALORES EN REBLOQUEO ENCAJE"),
	UNIFICATION_REBLOCK_OPPOSTION (new Long(30172), "UNIFICACION DE VALORES EN REBLOQUEO OPOSICION"),
	UNIFICATION_REPORTED (new Long(30173), "UNIFICACION DE VALORES EN REPORTADO"),
	UNIFICATION_REPORTING (new Long(30174), "UNIFICACION DE VALORES EN GARANTIA PRINCIPAL"),
	UNIFICATION_GUARANTEE (new Long(30175), "UNIFICACION DE VALORES EN MARGEN DE GARANTIA"),
	UNIFICATION_PRINCIPAL_DIVIDENDS (new Long(30176), "UNIFICACION DE VALORES EN DIVIDENDOS DE PRINCIPAL"),
	UNIFICATION_MARGIN_DIVIDENDS (new Long(30177), "UNIFICACION DE VALORES EN DIVIDENDOS DE MARGEN"),
	
	REDUCTION_AVAILABLE (new Long(30178), "REDUCCION DE CAPITAL"),
	REDUCTION_PAWN (new Long(30179), "REDUCCION DE CAPITAL EN PRENDA"),
	REDUCTION_BAN (new Long(30180), "REDUCCION DE CAPITAL EN EMBARGO"),
	REDUCTION_OTHER (new Long(30181), "REDUCCION DE CAPITAL EN BLOQUEO OTROS"),
	REDUCTION_RESERVE (new Long(30182), "REDUCCION DE CAPITAL EN ENCAJE"),
	REDUCTION_OPPOSTION (new Long(30183), "REDUCCION DE CAPITAL EN OPOSICION"),
	REDUCTION_REBLOCK_PAWN (new Long(30184), "REDUCCION DE CAPITAL EN REBLOQUEO PRENDA"),
	REDUCTION_REBLOCK_BAN (new Long(30185), "REDUCCION DE CAPITAL EN REBLOQUEO EMBARGO"),
	REDUCTION_REBLOCK_OTHER (new Long(30186), "REDUCCION DE CAPITAL EN REBLOQUEO BLOQUEO OTROS"),
	REDUCTION_REBLOCK_RESERVE (new Long(30187), "REDUCCION DE CAPITAL EN REBLOQUEO ENCAJE"),
	REDUCTION_REBLOCK_OPPOSTION (new Long(30188), "REDUCCION DE CAPITAL EN REBLOQUEO OPOSICION"),
	REDUCTION_REPORTED (new Long(30189), "REDUCCION DE CAPITAL EN REPORTADO"),
	REDUCTION_REPORTING (new Long(30190), "REDUCCION DE CAPITAL EN GARANTIA PRINCIPAL"),
	REDUCTION_GUARANTEE (new Long(30191), "REDUCCION DE CAPITAL EN MARGEN DE GARANTIA"),
	REDUCTION_PRINCIPAL_DIVIDENDS (new Long(30192), "REDUCCION DE CAPITAL EN DIVIDENDOS DE PRINCIPAL"),
	REDUCTION_MARGIN_DIVIDENDS (new Long(30193), "REDUCCION DE CAPITAL EN DIVIDENDOS DE MARGEN"),
	
	EXCISION_AVAILABLE (new Long(30194), "ESCISION DE VALORES"),
	EXCISION_PAWN (new Long(30195), "ESCISION DE VALORES EN PRENDA"),
	EXCISION_BAN (new Long(30196), "ESCISION DE VALORES EN EMBARGO"),
	EXCISION_OTHER (new Long(30197), "ESCISION DE VALORES EN BLOQUEO OTROS"),
	EXCISION_RESERVE (new Long(30198), "ESCISION DE VALORES EN ENCAJE"),
	EXCISION_OPPOSTION (new Long(30199), "ESCISION DE VALORES EN OPOSICION"),
	EXCISION_REBLOCK_PAWN (new Long(30200), "ESCISION DE VALORES EN REBLOQUEO PRENDA"),
	EXCISION_REBLOCK_BAN (new Long(30201), "ESCISION DE VALORES EN REBLOQUEO EMBARGO"),
	EXCISION_REBLOCK_OTHER (new Long(30202), "ESCISION DE VALORES EN REBLOQUEO BLOQUEO OTROS"),
	EXCISION_REBLOCK_RESERVE (new Long(30203), "ESCISION DE VALORES EN REBLOQUEO ENCAJE"),
	EXCISION_REBLOCK_OPPOSTION (new Long(30204), "ESCISION DE VALORES EN REBLOQUEO OPOSICION"),
	EXCISION_REPORTED (new Long(30205), "ESCISION DE VALORES EN REPORTADO"),
	EXCISION_REPORTING (new Long(30206), "ESCISION DE VALORES EN GARANTIA PRINCIPAL"),
	EXCISION_GUARANTEE (new Long(30207), "ESCISION DE VALORES EN MARGEN DE GARANTIA"),
	EXCISION_PRINCIPAL_DIVIDENDS (new Long(30208), "ESCISION DE VALORES EN DIVIDENDOS DE PRINCIPAL"),
	EXCISION_MARGIN_DIVIDENDS (new Long(30209), "ESCISION DE VALORES EN DIVIDENDOS DE MARGEN"),
	
	EXCLUSION_AVAILABLE (new Long(30210), "EXCLUSION DE VALORES"),
	EXCLUSION_PAWN (new Long(30211), "EXCLUSION DE VALORES EN PRENDA"),
	EXCLUSION_BAN (new Long(30212), "EXCLUSION DE VALORES EN EMBARGO"),
	EXCLUSION_OTHER (new Long(30213), "EXCLUSION DE VALORES EN BLOQUEO OTROS"),
	EXCLUSION_RESERVE (new Long(30214), "EXCLUSION DE VALORES EN ENCAJE"),
	EXCLUSION_OPPOSTION (new Long(30215), "EXCLUSION DE VALORES EN OPOSICION"),
	EXCLUSION_REBLOCK_PAWN (new Long(30216), "EXCLUSION DE VALORES EN REBLOQUEO PRENDA"),
	EXCLUSION_REBLOCK_BAN (new Long(30217), "EXCLUSION DE VALORES EN REBLOQUEO EMBARGO"),
	EXCLUSION_REBLOCK_OTHER (new Long(30218), "EXCLUSION DE VALORES EN REBLOQUEO OTROS"),
	EXCLUSION_REBLOCK_RESERVE (new Long(30219), "EXCLUSION DE VALORES EN REBLOQUEO ENCAJE"),
	EXCLUSION_REBLOCK_OPPOSTION (new Long(30220), "EXCLUSION DE VALORES EN REBLOQUEO OPOSICION"),
	
	CONVERTIBLE_B2A_AVAILABLE (new Long(30221), "CONVERTIBILIDAD DE BONOS EN ACCIONES"),
	CONVERTIBLE_B2A_PAWN (new Long(30222), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN PRENDA"),
	CONVERTIBLE_B2A_BAN (new Long(30223), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN EMBARGO"),
	CONVERTIBLE_B2A_OTHER (new Long(30224), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN BLOQUEO OTROS"),
	CONVERTIBLE_B2A_RESERVE (new Long(30225), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN ENCAJE"),
	CONVERTIBLE_B2A_OPPOSTION (new Long(30226), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN OPOSICION"),
	CONVERTIBLE_B2A_REBLOCK_PAWN (new Long(30227), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN REBLOQUEO PRENDA"),
	CONVERTIBLE_B2A_REBLOCK_BAN (new Long(30228), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN REBLOQUEO EMBARGO"),
	CONVERTIBLE_B2A_REBLOCK_OTHER (new Long(30229), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN REBLOQUEO OTROS"),
	CONVERTIBLE_B2A_REBLOCK_RESERVE (new Long(30230), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN REBLOQUEO ENCAJE"),
	CONVERTIBLE_B2A_REBLOCK_OPPOSTION (new Long(30231), "CONVERTIBILIDAD DE BONOS EN ACCIONES EN REBLOQUEO OPOSICION"),
	/* End of Corporate Process */
	
	/* Start Grant operations */
	COUPON_GRANT_AVAILABLE_BALANCE (new Long(30236), "CESION DE CUPON DE SALDO DISPONIBLE"),
	COUPON_GRANT_GRANTED_BALANCE (new Long(30237), "CESION DE CUPON DE SALDO CEDIDO"),
	FINISHING_GRANTED_BALANCE (new Long(30238), "CULMINACION DE SALDO CEDIDO"),
	FINISHING_GRANTED_COUPON (new Long(30239), "CULMINACION DE CUPON CEDIDO"),
	FINISHING_GRANT_BALANCE (new Long(30240), "CULMINACION DE SALDO EN CESION"),
	FINISHING_GRANT_COUPON (new Long(30241), "CULMINACION DE CUPON EN CESION"),
	/* End Grant operations */
	BLOCK_ENFORCE_EXECUTION (new Long(30244), "EJECUCION DE BLOQUEOS"),
	
	LOANABLE_SECURITIES_REGISTER (new Long(30242), "INGRESO DE VALORES PRESTABLES"),
	LOANABLE_SECURITIES_RETIREMENT (new Long(30243), "RETIRO DE VALORES PRESTABLES"),
	
	/* operation dpf's */
	REVERSION_SECURITIES_REDEMPTION_FIXED_INCOME (new Long(30279), "REVERSION DE REDENCION ANTICIPADA DE VALORES RF"),
	SECURITIES_REVERSION_SECURITIES_REDEMPTION_FIXED_INCOME (new Long(20053), "REVERSION DE REDENCION ANTICIPADA DE VALORES RF"),
	
	OPERATION_SECURITIES_RENEWAL_DPF (new Long(30280), "REVERSION DE REDENCION ANTICIPADA DE VALORES RF"),
	SECURITIES_OPERATION_SECURITIES_RENEWAL_DPF (new Long(20054), "REVERSION DE REDENCION ANTICIPADA DE VALORES RF"),
	
	/**Retirement Securities*/
	WITHDRAWAL_OF_SECURITIES_EXPIRED (new Long(30282), "RETIRO DE VALORES VENCIDOS"),
	
	// Issue 1379
	TRANSFER_FOR_EXCHANGE_SECURITIES(new Long(30284), "TRANSFERENCIA POR INTERCAMBIO DE VALORES"),
	;
	
	private Long code;
	private String description;
	
	
	private ParameterOperationType(Long code, String description) {
		this.code = code;
		this.description = description;
	}


	public Long getCode() {
		return code;
	}


	public void setCode(Long code) {
		this.code = code;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public static final List<ParameterOperationType> list = new ArrayList<ParameterOperationType>();
	public static final Map<Long, ParameterOperationType> lookup = new HashMap<Long, ParameterOperationType>();
	static {
		for (ParameterOperationType s : EnumSet.allOf(ParameterOperationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the ParameterOperationType
	 */
	public static ParameterOperationType get(Long code) {
		return lookup.get(code);
	}
}
