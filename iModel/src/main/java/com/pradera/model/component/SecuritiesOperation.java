package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.TradeOperation;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SECURITIES_OPERATION database table.
 * 
 */
@Entity
@Table(name="SECURITIES_OPERATION")
public class SecuritiesOperation implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id securities operation pk. */
	@Id
	@SequenceGenerator(name="SECURITIES_OPERATION_GENERATOR", sequenceName="SQ_ID_SECURITIES_OPERATION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITIES_OPERATION_GENERATOR")
	@Column(name="ID_SECURITIES_OPERATION_PK")
	private Long idSecuritiesOperationPk;

	/** The cash amount. */
	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	//bi-directional many-to-one association to securities
    /** The corporative operation. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CORPORATIVE_OPERATION_FK")
	private CorporativeOperation corporativeOperation;

    //bi-directional many-to-one association to custodyOperation
    /** The custody operation. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CUSTODY_OPERATION_FK")
	private CustodyOperation custodyOperation;
    
    //bi-directional many-to-one association to custodyOperation
    /** The trade operation. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TRADE_OPERATION_FK")
	private TradeOperation tradeOperation;

    //bi-directional many-to-one association to securities
    /** The securities. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The operation date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	/** The operation type. */
	@Column(name="OPERATION_TYPE")
	private Long operationType;

	/** The stock quantity. */
	@Column(name="STOCK_QUANTITY")
	private BigDecimal stockQuantity;
	
	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;

    /** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;

	//bi-directional many-to-one association to IssuanceOperation
	/** The issuance operations. */
	@OneToMany(mappedBy="securitiesOperation")
	private List<IssuanceOperation> issuanceOperations;	

	//bi-directional many-to-one association to SecuritiesMovement
	/** The securities movements. */
	@OneToMany(mappedBy="securitiesOperation")
	private List<SecuritiesMovement> securitiesMovements;

	
	
    public SecuritiesOperation(Long idSecuritiesOperationPk) {
		super();
		this.idSecuritiesOperationPk = idSecuritiesOperationPk;
	}

	/**
     * Instantiates a new securities operation.
     */
    public SecuritiesOperation() {
    }

	/**
	 * Gets the id securities operation pk.
	 *
	 * @return the id securities operation pk
	 */
	public Long getIdSecuritiesOperationPk() {
		return this.idSecuritiesOperationPk;
	}

	/**
	 * Sets the id securities operation pk.
	 *
	 * @param idSecuritiesOperationPk the new id securities operation pk
	 */
	public void setIdSecuritiesOperationPk(Long idSecuritiesOperationPk) {
		this.idSecuritiesOperationPk = idSecuritiesOperationPk;
	}

	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return this.cashAmount;
	}

	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	

	/**
	 * Gets the corporative operation.
	 *
	 * @return the corporative operation
	 */
	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	/**
	 * Sets the corporative operation.
	 *
	 * @param corporativeOperation the new corporative operation
	 */
	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	/**
	 * Gets the custody operation.
	 *
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * Sets the custody operation.
	 *
	 * @param custodyOperation the new custody operation
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the new securities
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return this.operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Long getOperationType() {
		return this.operationType;
	}

	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return this.stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}


	/**
	 * Gets the issuance operations.
	 *
	 * @return the issuance operations
	 */
	public List<IssuanceOperation> getIssuanceOperations() {
		return issuanceOperations;
	}

	/**
	 * Sets the issuance operations.
	 *
	 * @param issuanceOperations the new issuance operations
	 */
	public void setIssuanceOperations(List<IssuanceOperation> issuanceOperations) {
		this.issuanceOperations = issuanceOperations;
	}
	
	/**
	 * Gets the securities movements.
	 *
	 * @return the securities movements
	 */
	public List<SecuritiesMovement> getSecuritiesMovements() {
		return securitiesMovements;
	}

	/**
	 * Sets the securities movements.
	 *
	 * @param securitiesMovements the new securities movements
	 */
	public void setSecuritiesMovements(List<SecuritiesMovement> securitiesMovements) {
		this.securitiesMovements = securitiesMovements;
	}
	
	/**
	 * Gets the trade operation.
	 *
	 * @return the trade operation
	 */
	public TradeOperation getTradeOperation() {
		return tradeOperation;
	}

	/**
	 * Sets the trade operation.
	 *
	 * @param tradeOperation the new trade operation
	 */
	public void setTradeOperation(TradeOperation tradeOperation) {
		this.tradeOperation = tradeOperation;
	}	

	
	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            if(custodyOperation != null)
            	custodyOperation.setAudit(loggerUser);
            if (tradeOperation != null)
            	tradeOperation.setAudit(loggerUser);
            if (corporativeOperation != null)
            	corporativeOperation.setAudit(loggerUser);
        }
		
	}

	
	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	 public Map<String, List<? extends Auditable>> getListForAudit() {
	 	HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("securitiesMovements",  securitiesMovements);
        detailsMap.put("issuanceOperations", issuanceOperations);
        return detailsMap;
	 }
}