package com.pradera.model.component;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.pradera.model.accounts.Participant;

@Entity
@Table(name="IDEPOSITARY_SETUP")
public class IdepositarySetup implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_SETUP_PK")
	private Long idSetupPk;
	
	@Column(name="ID_DEFAULT_COUNTRY")
	private Integer idDefaultCountry;
	
	@Column(name="ID_DEFAULT_LANGUAGE")
	private Integer idDefaultLanguaje;
	
	@Column(name="IND_MARKET_FACT")
	private Integer indMarketFact;
	
	@Column(name="IND_BANK_ACCOUNT_HOLDER")
	private Integer indBankAccount;
	
	@Column(name="ID_CENTRAL_BANK")
	private Long idCentralBank;
	
	@Column(name="IND_FUNDS_SWIFT")
	private Integer indFundsSwift;
	
	@Column(name="IND_FUNDS_THIRD_PART")
	private Integer indFundsThridPart;
	
	@Column(name="ID_PARTICIPANT_DEPOSITARY")
	private Long idParticipantDepositary;

	@Column(name="ID_IN_CHARGE_PARTICIPANT")
	private Long IdInChargeParticipant;
	
	@Column(name="PAYMENT_TYPE")
	private Integer paymentType;
	
	@Transient
	private Map<String,Participant> participantByIssuerCode;

	public IdepositarySetup() {
		super();
	}

	public Long getIdSetupPk() {
		return idSetupPk;
	}

	public void setIdSetupPk(Long idSetupPk) {
		this.idSetupPk = idSetupPk;
	}

	public Integer getIdDefaultCountry() {
		return idDefaultCountry;
	}

	public void setIdDefaultCountry(Integer idDefaultCountry) {
		this.idDefaultCountry = idDefaultCountry;
	}

	public Integer getIdDefaultLanguaje() {
		return idDefaultLanguaje;
	}

	public void setIdDefaultLanguaje(Integer idDefaultLanguaje) {
		this.idDefaultLanguaje = idDefaultLanguaje;
	}

	public Integer getIndMarketFact() {
		return indMarketFact;
	}

	public void setIndMarketFact(Integer indMarketFact) {
		this.indMarketFact = indMarketFact;
	}

	public Integer getIndBankAccount() {
		return indBankAccount;
	}

	public void setIndBankAccount(Integer indBankAccount) {
		this.indBankAccount = indBankAccount;
	}

	public Long getIdCentralBank() {
		return idCentralBank;
	}

	public void setIdCentralBank(Long idCentralBank) {
		this.idCentralBank = idCentralBank;
	}

	public Integer getIndFundsSwift() {
		return indFundsSwift;
	}

	public void setIndFundsSwift(Integer indFundsSwift) {
		this.indFundsSwift = indFundsSwift;
	}

	public Integer getIndFundsThridPart() {
		return indFundsThridPart;
	}

	public void setIndFundsThridPart(Integer indFundsThridPart) {
		this.indFundsThridPart = indFundsThridPart;
	}

	public Long getIdParticipantDepositary() {
		return idParticipantDepositary;
	}

	public void setIdParticipantDepositary(Long idParticipantDepositary) {
		this.idParticipantDepositary = idParticipantDepositary;
	}

	/**
	 * @return the participantByIssuerCode
	 */
	public Map<String, Participant> getParticipantByIssuerCode() {
		return participantByIssuerCode;
	}

	/**
	 * @param participantByIssuerCode the participantByIssuerCode to set
	 */
	public void setParticipantByIssuerCode(
			Map<String, Participant> participantByIssuerCode) {
		this.participantByIssuerCode = participantByIssuerCode;
	}

	public Long getIdInChargeParticipant() {
		return IdInChargeParticipant;
	}

	public void setIdInChargeParticipant(Long idInChargeParticipant) {
		IdInChargeParticipant = idInChargeParticipant;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}
	
}