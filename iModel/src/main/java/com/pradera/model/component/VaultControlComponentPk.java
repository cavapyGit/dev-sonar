package com.pradera.model.component;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class VaultControlComponentPk implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Column(name="BOVEDA")
	private String boveda;
	
	@Column(name="ARMARIO")
	private String armario;
	
	@Column(name="NIVEL")
	private Integer nivel;
	
	@Column(name="CAJA")
	private Integer caja;
	
	@Column(name = "FOLIO")
	private String folio;
	
	@Column(name="INDICE_CAJA")
	private Integer indiceCaja;

	public String getBoveda() {
		return boveda;
	}

	public void setBoveda(String boveda) {
		this.boveda = boveda;
	}

	public String getArmario() {
		return armario;
	}

	public void setArmario(String armario) {
		this.armario = armario;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public Integer getCaja() {
		return caja;
	}

	public void setCaja(Integer caja) {
		this.caja = caja;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public Integer getIndiceCaja() {
		return indiceCaja;
	}

	public void setIndiceCaja(Integer indiceCaja) {
		this.indiceCaja = indiceCaja;
	}

	@Override
	public int hashCode() {
		return Objects.hash(armario, boveda, caja, folio, indiceCaja, nivel);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VaultControlComponentPk other = (VaultControlComponentPk) obj;
		return Objects.equals(armario, other.armario) && Objects.equals(boveda, other.boveda)
				&& Objects.equals(caja, other.caja) && Objects.equals(folio, other.folio)
				&& Objects.equals(indiceCaja, other.indiceCaja) && Objects.equals(nivel, other.nivel);
	}
	
}
