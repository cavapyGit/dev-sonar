package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.valuatorprocess.ValuatorCode;

/**
 * The persistent class for the HOLDER_ACCOUNT_BALANCE database table.
 * 
 */
@Entity
@Table(name="HOLDER_MARKETFACT_BALANCE")
public class HolderMarketFactBalance implements Serializable,Auditable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MARKETFACT_BALANCE_GENERATOR", sequenceName="SQ_ID_MARKETFACT_BALANCE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MARKETFACT_BALANCE_GENERATOR")
	@Column(name="ID_MARKETFACT_BALANCE_PK")
	private Long IdMarketfactBalancePk;

	//bi-directional many-to-one association to HolderAccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK",insertable=false,updatable=false)
	private HolderAccount holderAccount;
	
	//bi-directional many-to-one association to Participant
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",insertable=false,updatable=false)
	private Participant participant;
	
	//bi-directional many-to-one association to Security
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",insertable=false,updatable=false)
	private Security security;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_HOLDER_ACCOUNT_FK", referencedColumnName="ID_HOLDER_ACCOUNT_PK",updatable=false),
		@JoinColumn(name="ID_PARTICIPANT_FK", referencedColumnName="ID_PARTICIPANT_PK",updatable=false),
		@JoinColumn(name="ID_SECURITY_CODE_FK", referencedColumnName="ID_SECURITY_CODE_PK",updatable=false)
	})
	private HolderAccountBalance holderAccountBalance;
	
//	@OneToMany(fetch=FetchType.LAZY,mappedBy="holderMarketFactBalance")
//	private List<BlockMarketFactOperation> blockMarketFactOperationList;
//	
//	@OneToMany(fetch=FetchType.LAZY,mappedBy="holderMarketFactBalance")
//	private List<UnblockMarketFactOperation> unblockMarketFactOperationList;
	
	/** The market rate. */
	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;

    /** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;
    
	/** The market rate. */
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Column(name="ACCREDITATION_BALANCE")
	private BigDecimal accreditationBalance;

	@Column(name="AVAILABLE_BALANCE") 
	private BigDecimal availableBalance;

	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance;

	@Column(name="BORROWER_BALANCE")
	private BigDecimal borrowerBalance;

	@Column(name="PURCHASE_BALANCE")
	private BigDecimal purchaseBalance;

	@Column(name="LENDER_BALANCE")
	private BigDecimal lenderBalance;

	@Column(name="LOANABLE_BALANCE")
	private BigDecimal loanableBalance;
	
	@Column(name="MARGIN_BALANCE")
	private BigDecimal marginBalance;
	
	@Column(name="OPPOSITION_BALANCE")
	private BigDecimal oppositionBalance;

	@Column(name="OTHER_BLOCK_BALANCE")
	private BigDecimal otherBlockBalance;

	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance;

	@Column(name="REPORTED_BALANCE")
	private BigDecimal reportedBalance;

	@Column(name="REPORTING_BALANCE")
	private BigDecimal reportingBalance;

	@Column(name="RESERVE_BALANCE")
	private BigDecimal reserveBalance;

	@Column(name="SALE_BALANCE")
	private BigDecimal saleBalance;

	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance;

	@Column(name="TRANSIT_BALANCE")
	private BigDecimal transitBalance;
	
	@Column(name="VALUATOR_AMOUNT")
	private BigDecimal valuatorAmount;
	
	@Column(name="VALUATOR_AMOUNT_OTHER")
	private BigDecimal valuatorAmountOther;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="IND_ACTIVE")
	private Integer indActive;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_CODE_FK",insertable=false,updatable=false)
	private ValuatorCode valuatorCode;
	
    @Transient
    private BigDecimal transferAmmount;
    
    public HolderMarketFactBalance() {
		super();
		this.totalBalance= BigDecimal.ZERO;
		this.availableBalance= BigDecimal.ZERO;
		this.transitBalance= BigDecimal.ZERO;
		this.pawnBalance= BigDecimal.ZERO;
		this.banBalance= BigDecimal.ZERO;
		this.otherBlockBalance= BigDecimal.ZERO;
		this.reserveBalance= BigDecimal.ZERO;
		this.oppositionBalance= BigDecimal.ZERO;
		this.accreditationBalance= BigDecimal.ZERO;
		this.purchaseBalance= BigDecimal.ZERO;
		this.saleBalance= BigDecimal.ZERO;
		this.marginBalance= BigDecimal.ZERO;
		this.reportedBalance= BigDecimal.ZERO;
		this.reportingBalance= BigDecimal.ZERO;
		this.loanableBalance= BigDecimal.ZERO;
		this.lenderBalance= BigDecimal.ZERO;
		this.borrowerBalance= BigDecimal.ZERO;
	}

	public Long getIdMarketfactBalancePk() {
		return IdMarketfactBalancePk;
	}

	public void setIdMarketfactBalancePk(Long idMarketfactBalancePk) {
		IdMarketfactBalancePk = idMarketfactBalancePk;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public HolderAccountBalance getHolderAccountBalance() {
		return holderAccountBalance;
	}

	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

//	public List<BlockMarketFactOperation> getBlockMarketFactOperationList() {
//		return blockMarketFactOperationList;
//	}
//
//	public void setBlockMarketFactOperationList(
//			List<BlockMarketFactOperation> blockMarketFactOperationList) {
//		this.blockMarketFactOperationList = blockMarketFactOperationList;
//	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getAccreditationBalance() {
		return accreditationBalance;
	}

	public void setAccreditationBalance(BigDecimal accreditationBalance) {
		this.accreditationBalance = accreditationBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getBorrowerBalance() {
		return borrowerBalance;
	}

	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	public BigDecimal getLenderBalance() {
		return lenderBalance;
	}

	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}

	public BigDecimal getMarginBalance() {
		return marginBalance;
	}

	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getReportedBalance() {
		return reportedBalance;
	}

	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getTransitBalance() {
		return transitBalance;
	}

	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getIndActive() {
		return indActive;
	}

	public void setIndActive(Integer indActive) {
		this.indActive = indActive;
	}

	public BigDecimal getTransferAmmount() {
		return transferAmmount;
	}

	public void setTransferAmmount(BigDecimal transferAmmount) {
		this.transferAmmount = transferAmmount;
	}

	public BigDecimal getValuatorAmount() {
		return valuatorAmount;
	}

	public void setValuatorAmount(BigDecimal valuatorAmount) {
		this.valuatorAmount = valuatorAmount;
	}

	public BigDecimal getValuatorAmountOther() {
		return valuatorAmountOther;
	}

	public void setValuatorAmountOther(BigDecimal valuatorAmountOther) {
		this.valuatorAmountOther = valuatorAmountOther;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public HolderMarketFactBalance clone() throws CloneNotSupportedException {
        return (HolderMarketFactBalance) super.clone();
    }

	public ValuatorCode getValuatorCode() {
		return valuatorCode;
	}

	public void setValuatorCode(ValuatorCode valuatorCode) {
		this.valuatorCode = valuatorCode;
	}
}