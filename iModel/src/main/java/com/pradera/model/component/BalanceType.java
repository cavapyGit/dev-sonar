package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the BALANCE_TYPE database table.
 * 
 */
@Entity
@Table(name="BALANCE_TYPE")
@Cacheable(true)
public class BalanceType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_BALANCE_TYPE_PK")
	private Long idBalanceTypePk;

	@Column(name="BALANCE_CLASS")
	private Long balanceClass;

	@Column(name="BALANCE_GROUP")
	private Long balanceGroup;

	@Column(name="BALANCE_NAME")
	private String balanceName;

	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="LAST_MODIFY_APP")
	private BigDecimal lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="\"STATE\"")
	private Long state;

	
    public BalanceType() {
    }

	public Long getIdBalanceTypePk() {
		return this.idBalanceTypePk;
	}

	public void setIdBalanceTypePk(Long idBalanceTypePk) {
		this.idBalanceTypePk = idBalanceTypePk;
	}
	
	public String getBalanceName() {
		return this.balanceName;
	}

	public void setBalanceName(String balanceName) {
		this.balanceName = balanceName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(BigDecimal lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Long getBalanceClass() {
		return balanceClass;
	}

	public void setBalanceClass(Long balanceClass) {
		this.balanceClass = balanceClass;
	}

	public Long getBalanceGroup() {
		return balanceGroup;
	}

	public void setBalanceGroup(Long balanceGroup) {
		this.balanceGroup = balanceGroup;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

		
}