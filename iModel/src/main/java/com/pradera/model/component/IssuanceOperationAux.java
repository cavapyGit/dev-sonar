package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Issuance;
/**
 * The persistent class for the ISSUANCE_OPERATION database table.
 * 
 */
@Entity
@Table(name="ISSUANCE_OPERATION_AUX")
public class IssuanceOperationAux implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ISSUANCE_OPERATION_AUX_GENERATOR", sequenceName="SQ_ID_ISS_OPERATION_AUX_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUANCE_OPERATION_AUX_GENERATOR")
	@Column(name="ID_ISSUANCE_OPERATION_PK")
	private Long idIssuanceOperationPk;

	@Column(name="CASH_AMOUNT")
	private BigDecimal cashAmount;

	//bi-directional many-to-one association to issuance
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUANCE_CODE_FK")
	private Issuance issuance;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	@Column(name="OPERATION_TYPE")
	private Long operationType;

	//bi-directional many-to-one association to IssuanceMovement
	@OneToMany(mappedBy="issuanceOperation")
	private List<IssuanceMovement> issuanceMovements;

	//bi-directional many-to-one association to SecuritiesOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITIES_OPERATION_FK")
	private SecuritiesOperation securitiesOperation;
    
    public IssuanceOperationAux() {}

	public Long getIdIssuanceOperationPk() {
		return this.idIssuanceOperationPk;
	}

	public void setIdIssuanceOperationPk(Long idIssuanceOperationPk) {
		this.idIssuanceOperationPk = idIssuanceOperationPk;
	}

	public BigDecimal getCashAmount() {
		return this.cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	

	public Issuance getIssuance() {
		return issuance;
	}

	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getOperationDate() {
		return this.operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public Long getOperationType() {
		return this.operationType;
	}

	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}

	public List<IssuanceMovement> getIssuanceMovements() {
		return this.issuanceMovements;
	}

	public void setIssuanceMovements(List<IssuanceMovement> issuanceMovements) {
		this.issuanceMovements = issuanceMovements;
	}
	
	public SecuritiesOperation getSecuritiesOperation() {
		return this.securitiesOperation;
	}

	public void setSecuritiesOperation(SecuritiesOperation securitiesOperation) {
		this.securitiesOperation = securitiesOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("issuanceMovements", issuanceMovements);        
        return detailsMap;
	}
}