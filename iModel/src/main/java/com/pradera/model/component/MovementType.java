package com.pradera.model.component;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the MOVEMENT_TYPE database table.
 * 
 */
@Entity
@Cacheable(true)
@Table(name="MOVEMENT_TYPE")
public class MovementType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MOVEMENT_TYPE_PK")
	private Long idMovementTypePk;

	@Column(name="ACCOUNTING_BALANCE")
	private Long accountingBalance;

	@Column(name="ACCOUNTING_SCHEMA")
	private Long accountingSchema;

	@Column(name="IND_ACCOUNTING")
	private Integer indAccounting;

	@Column(name="IND_BILLING")
	private Integer indBilling;

	@Column(name="IND_STOCK_PROCESS")
	private Integer indStockProcess;

	@Column(name="IND_BLOCK_CALCULATION")
	private Integer indBlockCalculation;
	
	@Column(name="IND_VISIBILITY")
	private Integer indVisibility;
	
	@Column(name="IND_BLOCK_MOVEMENT")
	private Integer indBlockMovement;
	
	@Column(name="IND_GUARANTEE_MOVEMENT")
	private Integer indGuaranteeMovement;
	
	@Column(name="IND_SETTLEMENT_MOVEMENT")
	private Integer indSettlementMovement;
	
	@Column(name="IND_GRANT_MOVEMENT")
	private Integer indGrantMovement;
	
	@Column(name="IND_MOVEMENT_CLASS")
	private Integer indMovementClass;
	
	@Column(name="IND_MARKETFACT_MOVEMENT")
	private Integer indMarketfactMovement;
	
	@Column(name="LAST_MODIFY_APP")
	private Long lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MODULE_TYPE")
	private Long moduleType;

	@Column(name="MOVEMENT_NAME")
	private String movementName;

	@Column(name="REFERENCE_BALANCE_TYPE")
	private Long referenceBalanceType;
	
	@Transient	
	private String descriptionModuleType;

    public MovementType() {
    }

	public Long getIdMovementTypePk() {
		return this.idMovementTypePk;
	}

	public void setIdMovementTypePk(Long idMovementTypePk) {
		this.idMovementTypePk = idMovementTypePk;
	}

	public Integer getIndMarketfactMovement() {
		return indMarketfactMovement;
	}

	public void setIndMarketfactMovement(Integer indMarketfactMovement) {
		this.indMarketfactMovement = indMarketfactMovement;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	
	public Long getAccountingBalance() {
		return accountingBalance;
	}

	public void setAccountingBalance(Long accountingBalance) {
		this.accountingBalance = accountingBalance;
	}

	public Long getAccountingSchema() {
		return accountingSchema;
	}

	public void setAccountingSchema(Long accountingSchema) {
		this.accountingSchema = accountingSchema;
	}

	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Long getModuleType() {
		return moduleType;
	}

	public void setModuleType(Long moduleType) {
		this.moduleType = moduleType;
	}

	public String getMovementName() {
		return this.movementName;
	}

	public void setMovementName(String movementName) {
		this.movementName = movementName;
	}

	public Long getReferenceBalanceType() {
		return this.referenceBalanceType;
	}

	public void setReferenceBalanceType(Long referenceBalanceType) {
		this.referenceBalanceType = referenceBalanceType;
	}

	public Integer getIndAccounting() {
		return indAccounting;
	}

	public void setIndAccounting(Integer indAccounting) {
		this.indAccounting = indAccounting;
	}

	public Integer getIndBilling() {
		return indBilling;
	}

	public void setIndBilling(Integer indBilling) {
		this.indBilling = indBilling;
	}

	public Integer getIndStockProcess() {
		return indStockProcess;
	}

	public void setIndStockProcess(Integer indStockProcess) {
		this.indStockProcess = indStockProcess;
	}

	public Integer getIndVisibility() {
		return indVisibility;
	}

	public void setIndVisibility(Integer indVisibility) {
		this.indVisibility = indVisibility;
	}

	public Integer getIndBlockMovement() {
		return indBlockMovement;
	}

	public void setIndBlockMovement(Integer indBlockMovement) {
		this.indBlockMovement = indBlockMovement;
	}

	public Integer getIndGuaranteeMovement() {
		return indGuaranteeMovement;
	}

	public void setIndGuaranteeMovement(Integer indGuaranteeMovement) {
		this.indGuaranteeMovement = indGuaranteeMovement;
	}

	public Integer getIndMovementClass() {
		return indMovementClass;
	}

	public void setIndMovementClass(Integer indMovementClass) {
		this.indMovementClass = indMovementClass;
	}

	
	public Integer getIndSettlementMovement() {
		return indSettlementMovement;
	}

	public void setIndSettlementMovement(Integer indSettlementMovement) {
		this.indSettlementMovement = indSettlementMovement;
	}

	public Integer getIndBlockCalculation() {
		return indBlockCalculation;
	}

	public void setIndBlockCalculation(Integer indBlockCalculation) {
		this.indBlockCalculation = indBlockCalculation;
	}

	public Integer getIndGrantMovement() {
		return indGrantMovement;
	}

	public void setIndGrantMovement(Integer indGrantMovement) {
		this.indGrantMovement = indGrantMovement;
	}

	public String getDescriptionModuleType() {
		return descriptionModuleType;
	}

	public void setDescriptionModuleType(String descriptionModuleType) {
		this.descriptionModuleType = descriptionModuleType;
	}
	
	

}