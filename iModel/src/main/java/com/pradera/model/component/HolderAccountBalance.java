package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

/**
 * The persistent class for the HOLDER_ACCOUNT_BALANCE database table.
 * 
 */
@Entity
@NamedQueries(@NamedQuery(name="byHolderAccountBalancePk", query="SELECT hab FROM HolderAccountBalance hab WHERE hab.id.idParticipantPk = :idPartPkParamm AND hab.id.idHolderAccountPk = :idAccountPkParam AND hab.id.idSecurityCodePk = :idIsinPkParam"))
@Table(name="HOLDER_ACCOUNT_BALANCE")
public class HolderAccountBalance implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	public static final String GET_OBJECT = "byHolderAccountBalancePk";

	@EmbeddedId
	private HolderAccountBalancePK id;
	
	@Column(name="TOTAL_BALANCE")
	private BigDecimal totalBalance = BigDecimal.ZERO;
	
	@Column(name="AVAILABLE_BALANCE") 
	private BigDecimal availableBalance = BigDecimal.ZERO;
	
	@Column(name="TRANSIT_BALANCE")
	private BigDecimal transitBalance = BigDecimal.ZERO;
	
	@Column(name="PAWN_BALANCE")
	private BigDecimal pawnBalance = BigDecimal.ZERO;
	
	@Column(name="BAN_BALANCE")
	private BigDecimal banBalance = BigDecimal.ZERO;
	
	@Column(name="OTHER_BLOCK_BALANCE")
	private BigDecimal otherBlockBalance = BigDecimal.ZERO;
	
	@Column(name="RESERVE_BALANCE")
	private BigDecimal reserveBalance = BigDecimal.ZERO;
	
	@Column(name="OPPOSITION_BALANCE")
	private BigDecimal oppositionBalance = BigDecimal.ZERO;
	
	@Column(name="ACCREDITATION_BALANCE")
	private BigDecimal accreditationBalance = BigDecimal.ZERO;

	@Column(name="PURCHASE_BALANCE")
	private BigDecimal purchaseBalance = BigDecimal.ZERO;

	@Column(name="SALE_BALANCE")
	private BigDecimal saleBalance = BigDecimal.ZERO;
	
	@Column(name="REPORTING_BALANCE")
	private BigDecimal reportingBalance = BigDecimal.ZERO;
	
	@Column(name="REPORTED_BALANCE")
	private BigDecimal reportedBalance = BigDecimal.ZERO;
	
	@Column(name="MARGIN_BALANCE")
	private BigDecimal marginBalance = BigDecimal.ZERO;
	
	@Column(name="BORROWER_BALANCE")
	private BigDecimal borrowerBalance = BigDecimal.ZERO;

	@Column(name="LENDER_BALANCE")
	private BigDecimal lenderBalance = BigDecimal.ZERO;

	@Column(name="LOANABLE_BALANCE")
	private BigDecimal loanableBalance = BigDecimal.ZERO;
	
//	@Column(name="GRANTED_BALANCE")
//	private BigDecimal grantedBalance = BigDecimal.ZERO;
//
//	@Column(name="GRANT_BALANCE")
//	private BigDecimal grantBalance = BigDecimal.ZERO;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to HolderAccount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_PK",insertable=false,updatable=false)
	private HolderAccount holderAccount;
	
	//bi-directional many-to-one association to Participant
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_PK",insertable=false,updatable=false)
	private Participant participant;
	
	//bi-directional many-to-one association to Security
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_PK",insertable=false,updatable=false)
	private Security security;
	
	@OneToMany(mappedBy = "holderAccountBalance", fetch=FetchType.LAZY)
	private List<HolderAccountMovement> holderAccountMovements;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="holderAccountBalance", fetch=FetchType.LAZY)
	 private List<HolderMarketFactBalance> holderMarketfactBalance;

    //For the datatable
    @Transient
    private BigDecimal transferAmmount;
    @Transient
    private BigDecimal copiedAvailableBalance;
    @Transient
    private Boolean selected = false;
    @Transient
    private Boolean disabled = false;
    @Transient
    private BigDecimal pendingSellBalance;
    @Transient
    private Integer registryNumber;
    @Transient
    private String styleClassButton;
    @Transient
    private Long marketCount;
    @Version
    private Long version;
    
    public HolderAccountBalance(
			BigDecimal totalBalance, BigDecimal availableBalance,
			BigDecimal pawnBalance , BigDecimal banBalance,
			BigDecimal otherBlockBalance, BigDecimal sellBalance,
			BigDecimal buyBalance, BigDecimal reportedBalance,
			BigDecimal reportingBalance, BigDecimal marginBalance,
			BigDecimal transitBalance, BigDecimal accreditationBalance,
			BigDecimal reserveBalance, BigDecimal oppositionBalance,
			BigDecimal grantedBalance,BigDecimal grantBalance) {
    	    	
		super();
		this.totalBalance = totalBalance;
		this.availableBalance = availableBalance;
		this.pawnBalance = pawnBalance;
		this.banBalance = banBalance;
		this.otherBlockBalance = otherBlockBalance;
		this.saleBalance = sellBalance;
		this.purchaseBalance = buyBalance;
		this.reportedBalance = reportedBalance;
		this.reportingBalance = reportingBalance;
		this.marginBalance = marginBalance;
		this.transitBalance = transitBalance;
		this.accreditationBalance = accreditationBalance;
		this.reserveBalance = reserveBalance;
		this.oppositionBalance = oppositionBalance;
//		this.grantedBalance = grantedBalance;
//		this.grantBalance = grantBalance;
	}
    
//    /**
//	 * @return the grantedBalance
//	 */
//	public BigDecimal getGrantedBalance() {
//		return grantedBalance;
//	}
//
//	/**
//	 * @param grantedBalance the grantedBalance to set
//	 */
//	public void setGrantedBalance(BigDecimal grantedBalance) {
//		this.grantedBalance = grantedBalance;
//	}
//
//	/**
//	 * @return the grantBalance
//	 */
//	public BigDecimal getGrantBalance() {
//		return grantBalance;
//	}
//
//	/**
//	 * @param grantBalance the grantBalance to set
//	 */
//	public void setGrantBalance(BigDecimal grantBalance) {
//		this.grantBalance = grantBalance;
//	}

	public HolderAccountBalance() {
    	this.id = new HolderAccountBalancePK();
    }

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public HolderAccountBalancePK getId() {
		return this.id;
	}

	public void setId(HolderAccountBalancePK id) {
		this.id = id;
	}
	

	public BigDecimal getAccreditationBalance() {
		return accreditationBalance;
	}

	public void setAccreditationBalance(BigDecimal accreditationBalance) {
		this.accreditationBalance = accreditationBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getBorrowerBalance() {
		return this.borrowerBalance;
	}

	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getLenderBalance() {
		return this.lenderBalance;
	}

	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}
/*
	public BigDecimal getLenderableBalance() {
		return this.lenderableBalance;
	}

	public void setLenderableBalance(BigDecimal lenderableBalance) {
		this.lenderableBalance = lenderableBalance;
	}
*/
	public BigDecimal getMarginBalance() {
		return this.marginBalance;
	}

	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	public BigDecimal getOtherBlockBalance() {
		return this.otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getPawnBalance() {
		return this.pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getReportedBalance() {
		return this.reportedBalance;
	}

	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	public BigDecimal getReportingBalance() {
		return this.reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getReserveBalance() {
		return this.reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getTransitBalance() {
		return this.transitBalance;
	}

	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	public HolderAccount getHolderAccount() {
		return this.holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public BigDecimal getTransferAmmount() {
		return transferAmmount;
	}

	public void setTransferAmmount(BigDecimal transferAmmount) {
		this.transferAmmount = transferAmmount;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}

	/**
	 * @return the copiedAvailableBalance
	 */
	public BigDecimal getCopiedAvailableBalance() {
		return copiedAvailableBalance;
	}

	/**
	 * @param copiedAvailableBalance the copiedAvailableBalance to set
	 */
	public void setCopiedAvailableBalance(BigDecimal copiedAvailableBalance) {
		this.copiedAvailableBalance = copiedAvailableBalance;
	}

	public BigDecimal getPendingSellBalance() {
		return pendingSellBalance;
	}

	public void setPendingSellBalance(BigDecimal pendingSellBalance) {
		this.pendingSellBalance = pendingSellBalance;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param accreditationBalance
	 * @param availableBalance
	 * @param banBalance
	 * @param borrowerBalance
	 * @param buyBalance
	 * @param lenderBalance
	 * @param loanableBalance
	 * @param marginBalance
	 * @param oppositionBalance
	 * @param otherBlockBalance
	 * @param pawnBalance
	 * @param reportedBalance
	 * @param reportingBalance
	 * @param reserveBalance
	 * @param sellBalance
	 * @param totalBalance
	 * @param transitBalance
	 * @param holderAccount
	 * @param participant
	 * @param security
	 */
	public HolderAccountBalance(BigDecimal accreditationBalance,
			BigDecimal availableBalance, BigDecimal banBalance,
			BigDecimal borrowerBalance, BigDecimal buyBalance,
			BigDecimal lenderBalance, BigDecimal loanableBalance,
			BigDecimal marginBalance, BigDecimal oppositionBalance,
			BigDecimal otherBlockBalance, BigDecimal pawnBalance,
			BigDecimal reportedBalance, BigDecimal reportingBalance,
			BigDecimal reserveBalance, BigDecimal sellBalance,
			BigDecimal totalBalance, BigDecimal transitBalance) {
		this.accreditationBalance = accreditationBalance;
		this.availableBalance = availableBalance;
		this.banBalance = banBalance;
		this.borrowerBalance = borrowerBalance;
		this.purchaseBalance = buyBalance;
		this.lenderBalance = lenderBalance;
		this.loanableBalance = loanableBalance;
		this.marginBalance = marginBalance;
		this.oppositionBalance = oppositionBalance;
		this.otherBlockBalance = otherBlockBalance;
		this.pawnBalance = pawnBalance;
		this.reportedBalance = reportedBalance;
		this.reportingBalance = reportingBalance;
		this.reserveBalance = reserveBalance;
		this.saleBalance = sellBalance;
		this.totalBalance = totalBalance;
		this.transitBalance = transitBalance;
	}

	public List<HolderAccountMovement> getHolderAccountMovements() {
		return holderAccountMovements;
	}

	public void setHolderAccountMovements(
			List<HolderAccountMovement> holderAccountMovements) {
		this.holderAccountMovements = holderAccountMovements;
	}

	public List<HolderMarketFactBalance> getHolderMarketfactBalance() {
		return holderMarketfactBalance;
	}

	public void setHolderMarketfactBalance(
			List<HolderMarketFactBalance> holderMarketfactBalance) {
		this.holderMarketfactBalance = holderMarketfactBalance;
	}

	public Integer getRegistryNumber() {
		return registryNumber;
	}

	public void setRegistryNumber(Integer registryNumber) {
		this.registryNumber = registryNumber;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public String getStyleClassButton() {
		return styleClassButton;
	}

	public void setStyleClassButton(String styleClassButton) {
		this.styleClassButton = styleClassButton;
	}

	/**
	 * @return the marketCount
	 */
	public Long getMarketCount() {
		return marketCount;
	}

	/**
	 * @param marketCount the marketCount to set
	 */
	public void setMarketCount(Long marketCount) {
		this.marketCount = marketCount;
	}
	
}