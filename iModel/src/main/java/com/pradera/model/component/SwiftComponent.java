package com.pradera.model.component;

import java.io.Serializable;
import javax.persistence.*;

import com.pradera.model.funds.FundsOperationType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the SWIFT_COMPONENT database table.
 * 
 */
@Entity
@Table(name="SWIFT_COMPONENT")
public class SwiftComponent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_SWIFT_COMPONENT_PK")
	private Long idSwiftComponentPk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RETURN_SWIFT_COMPONENT_FK")
	private SwiftComponent swiftComponent;

//	@Column(name="NAME")
//	private String name;
//
//	@Column(name="DESCRIPTION")
//	private String description;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_TYPE_FK")
	private FundsOperationType fundsOperationType;

	@Column(name="CURRENCY")
	private Integer currency;

	@Column(name="TRN_FIELD")
	private String trnField;

	@Column(name="SWIFT_MESSAGE_TYPE")
	private Integer swiftMessageType;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;


    public SwiftComponent() {
    }

	public Long getIdSwiftComponentPk() {
		return this.idSwiftComponentPk;
	}

	public void setIdSwiftComponentPk(Long idSwiftComponentPk) {
		this.idSwiftComponentPk = idSwiftComponentPk;
	}
	
//	public String getDescription() {
//		return this.description;
//	}
//
//	public void setDescription(String description) {
//		this.description = description;
//	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

//	public String getName() {
//		return this.name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}

	public String getTrnField() {
		return this.trnField;
	}

	public void setTrnField(String trnField) {
		this.trnField = trnField;
	}

	public FundsOperationType getFundsOperationType() {
		return this.fundsOperationType;
	}

	public void setFundsOperationType(FundsOperationType fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}
	
	public SwiftComponent getSwiftComponent() {
		return this.swiftComponent;
	}

	public void setSwiftComponent(SwiftComponent swiftComponent) {
		this.swiftComponent = swiftComponent;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Integer getSwiftMessageType() {
		return swiftMessageType;
	}

	public void setSwiftMessageType(Integer swiftMessageType) {
		this.swiftMessageType = swiftMessageType;
	}	
}