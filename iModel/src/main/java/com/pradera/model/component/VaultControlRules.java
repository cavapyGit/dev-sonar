package com.pradera.model.component;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="REGLAS_BOVEDA")
@IdClass(VaultControlRulesPk.class)
public class VaultControlRules implements Serializable ,Auditable {
	
	private static final long serialVersionUID = 1L;
	
	//@Id
	@Column(name="ID")
    //@SequenceGenerator(name="VAULT_CONTROL_RULES_GENERATOR", sequenceName="SQ_VAULT_CONTROL_RULES_PK",initialValue=1,allocationSize=1)
  	//@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VAULT_CONTROL_RULES_GENERATOR")
	private Integer id;
	
	@Column(name="FECHA_VENCIMIENTO")
	private Date expirationDate;
	
	@Id
	@Column(name="ARMARIO")
	private Integer cabinet;
	
	@Id
	@Column(name="NIVEL")
	private Integer level;
	
	@Id
	@Column(name="CAJA")
	private Integer box;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="CERTIFICATES_PER_BOX")
	private Integer certificatesPerBox;
	
	@Column(name="ISSUERS_PER_BOX")
	private Integer issuersPerBox;
	
	@Column(name="IND_BOX_FOR_ISSUERS")
	private Integer indBoxForIssuers;
	
	@Column(name="SECURITIES_CLASS")
	private Integer securitiesClass;
	
	@Transient
	private Boolean isSelected;
	
	@Transient
	private String issuersPerBoxDescription;
	
	@Transient
	private Integer expirationYear;
	
	@Transient
	private Integer usedCapacity;
	
	@Transient
	private String securityDescription;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getCabinet() {
		return cabinet;
	}

	public void setCabinet(Integer cabinet) {
		this.cabinet = cabinet;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getBox() {
		return box;
	}

	public void setBox(Integer box) {
		this.box = box;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}	

	public Integer getCertificatesPerBox() {
		return certificatesPerBox;
	}

	public void setCertificatesPerBox(Integer certificatesPerBox) {
		this.certificatesPerBox = certificatesPerBox;
	}

	public Integer getIssuersPerBox() {
		return issuersPerBox;
	}

	public void setIssuersPerBox(Integer issuersPerBox) {
		this.issuersPerBox = issuersPerBox;
	}

	public Integer getIndBoxForIssuers() {
		return indBoxForIssuers;
	}

	public void setIndBoxForIssuers(Integer indBoxForIssuers) {
		this.indBoxForIssuers = indBoxForIssuers;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getIssuersPerBoxDescription() {
		return issuersPerBoxDescription;
	}

	public void setIssuersPerBoxDescription(String issuersPerBoxDescription) {
		this.issuersPerBoxDescription = issuersPerBoxDescription;
	}

	public Integer getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(Integer expirationYear) {
		this.expirationYear = expirationYear;
	}

	public Integer getSecuritiesClass() {
		return securitiesClass;
	}

	public void setSecuritiesClass(Integer securitiesClass) {
		this.securitiesClass = securitiesClass;
	}

	public Integer getUsedCapacity() {
		return usedCapacity;
	}

	public void setUsedCapacity(Integer usedCapacity) {
		this.usedCapacity = usedCapacity;
	}

	public String getSecurityDescription() {
		return securityDescription;
	}

	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
