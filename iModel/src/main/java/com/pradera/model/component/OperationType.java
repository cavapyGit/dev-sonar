package com.pradera.model.component;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the OPERATION_TYPE database table.
 * 
 */
@Entity
//@Cacheable(true)
@Table(name="OPERATION_TYPE")
public class OperationType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_OPERATION_TYPE_PK")
	private Long idOperationTypePk;

	@Column(name="IND_REBLOCK_OPERATION")
	private Integer indReblockOperation;

	@Column(name="RECTIFICATION_TYPE")
	private Integer rectificationType;

	@Column(name="IND_SETTLEMENT")
	private Integer indSettlement;

	@Column(name="LAST_MODIFY_APP")
	private Long lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MODULE_TYPE")
	private Long moduleType;

	@Column(name="OPERATION_NAME")
	private String operationName;

	//bi-directional many-to-one association to OperationType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OPERATION_TYPE_CASCADE")
	private OperationType operationTypeCascade;

	@Column(name="PROCESS_TYPE")
	private Long processType;

	@Column(name="IND_ACTIVATE_REBLOCK")
	private Integer indActivateReblock;
	
	
    public OperationType() {
    }

	public Long getIdOperationTypePk() {
		return this.idOperationTypePk;
	}

	public void setIdOperationTypePk(Long idOperationTypePk) {
		this.idOperationTypePk = idOperationTypePk;
	}

	
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	
	public String getOperationName() {
		return this.operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Long getModuleType() {
		return moduleType;
	}

	public void setModuleType(Long moduleType) {
		this.moduleType = moduleType;
	}

	public Long getProcessType() {
		return processType;
	}

	public void setProcessType(Long processType) {
		this.processType = processType;
	}

	public OperationType getOperationTypeCascade() {
		return operationTypeCascade;
	}

	public void setOperationTypeCascade(OperationType operationTypeCascade) {
		this.operationTypeCascade = operationTypeCascade;
	}

	public Integer getIndReblockOperation() {
		return indReblockOperation;
	}

	public void setIndReblockOperation(Integer indReblockOperation) {
		this.indReblockOperation = indReblockOperation;
	}

	public Integer getRectificationType() {
		return rectificationType;
	}

	public void setRectificationType(Integer indRectification) {
		this.rectificationType = indRectification;
	}

	public Integer getIndSettlement() {
		return indSettlement;
	}

	public void setIndSettlement(Integer indSettlement) {
		this.indSettlement = indSettlement;
	}

	public Integer getIndActivateReblock() {
		return indActivateReblock;
	}

	public void setIndActivateReblock(Integer indActivateReblock) {
		this.indActivateReblock = indActivateReblock;
	}
	/**
	 * @param idOperationTypePk
	 * @param operationName
	 */
	public OperationType(Long idOperationTypePk, String operationName) {
		this.idOperationTypePk = idOperationTypePk;
		this.operationName = operationName;
	}

}