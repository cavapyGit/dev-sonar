package com.pradera.model.component;

import java.io.Serializable;
import javax.persistence.*;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountBalancePK.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Embeddable
public class HolderAccountBalancePK implements Serializable {
	//default serial version id, required for serializable classes.
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant pk. */
	@Column(name="ID_PARTICIPANT_PK")
	private Long idParticipantPk;

	/** The id holder account pk. */
	@Column(name="ID_HOLDER_ACCOUNT_PK")
	private Long idHolderAccountPk;

	/** The id security code pk. */
	@Column(name="ID_SECURITY_CODE_PK")
	private String idSecurityCodePk;

    /**
     * Instantiates a new holder account balance pk.
     */
    public HolderAccountBalancePK() {
    }
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return this.idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return this.idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return this.idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idIsinCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idIsinCodePk) {
		this.idSecurityCodePk = idIsinCodePk;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof HolderAccountBalancePK)) {
			return false;
		}
		HolderAccountBalancePK castOther = (HolderAccountBalancePK)other;
		return 
			(this.idParticipantPk.equals(castOther.idParticipantPk))
			&& (this.idHolderAccountPk.equals(castOther.idHolderAccountPk))
			&& this.idSecurityCodePk.equals(castOther.idSecurityCodePk);

    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idParticipantPk ^ (this.idParticipantPk >>> 32)));
		hash = hash * prime + ((int) (this.idHolderAccountPk ^ (this.idHolderAccountPk >>> 32)));
		hash = hash * prime + this.idSecurityCodePk.hashCode();
		
		return hash;
    }
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.idParticipantPk+"-"+this.idHolderAccountPk+"-"+this.idSecurityCodePk;
	}
}