package com.pradera.model.component;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class VaultControlRulesPk implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="ARMARIO")
	private Integer cabinet;
	
	@Column(name="NIVEL")
	private Integer level;
	
	@Column(name="CAJA")
	private Integer box;

	public Integer getCabinet() {
		return cabinet;
	}

	public void setCabinet(Integer cabinet) {
		this.cabinet = cabinet;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getBox() {
		return box;
	}

	public void setBox(Integer box) {
		this.box = box;
	}

	@Override
	public int hashCode() {
		return Objects.hash(box, cabinet, level);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VaultControlRulesPk other = (VaultControlRulesPk) obj;
		return Objects.equals(box, other.box) && Objects.equals(cabinet, other.cabinet)
				&& Objects.equals(level, other.level);
	}

}
