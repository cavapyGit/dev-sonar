package com.pradera.model.component;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MOVEMENT_BEHAVIOR database table.
 * 
 */
@Entity
@Cacheable(true)
@Table(name="MOVEMENT_BEHAVIOR")
public class MovementBehavior implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MOVEMENT_BEHAVIOR_PK")
	private Long idMovementBehaviorPk;

	@Column(name="ID_BEHAVIOR")
	private Long idBehavior;

	@Column(name="LAST_MODIFY_APP")
	private BigDecimal lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to BalanceType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BALANCE_TYPE_FK")
	private BalanceType balanceType;

	//bi-directional many-to-one association to MovementType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

    public MovementBehavior() {
    }

	public Long getIdMovementBehaviorPk() {
		return this.idMovementBehaviorPk;
	}

	public void setIdMovementBehaviorPk(Long idMovementBehaviorPk) {
		this.idMovementBehaviorPk = idMovementBehaviorPk;
	}

	public Long getIdBehavior() {
		return this.idBehavior;
	}

	public void setIdBehavior(Long idBehavior) {
		this.idBehavior = idBehavior;
	}

	public BigDecimal getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(BigDecimal lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BalanceType getBalanceType() {
		return this.balanceType;
	}

	public void setBalanceType(BalanceType balanceType) {
		this.balanceType = balanceType;
	}
	
	public MovementType getMovementType() {
		return this.movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}
	
}