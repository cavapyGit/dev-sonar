package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;

@Entity
@Table(name="HOLDER_MARKETFACT_MOVEMENT")
public class HolderMarketFactMovement implements Serializable,Auditable{

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MARKETFACT_MOVEMENT_GENERATOR", sequenceName="SQ_ID_MARKETFACT_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MARKETFACT_MOVEMENT_GENERATOR")
	@Column(name="ID_MARKETFACT_MOVEMENT_PK")
	private Long idMarketFactMovementPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_MOVEMENT_FK")
	private HolderAccountMovement holderAccountMovement;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;
	
	@Temporal( TemporalType.DATE)
	@Column(name="MARKET_DATE")
	private Date marketDate;

	@Column(name="MARKET_RATE")
	private BigDecimal marketRate;
	
	@Column(name="MARKET_PRICE")
	private BigDecimal marketPrice;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	@Column(name="MOVEMENT_QUANTITY")
	private BigDecimal movementQuantity;

	@Column(name="VALUATOR_AMOUNT")
	private BigDecimal valuatorAmount;
	
	
	public HolderMarketFactMovement() {
		super();
	}

	public Long getIdMarketFactMovementPk() {
		return idMarketFactMovementPk;
	}

	public void setIdMarketFactMovementPk(Long idMarketFactMovementPk) {
		this.idMarketFactMovementPk = idMarketFactMovementPk;
	}

	public HolderAccountMovement getHolderAccountMovement() {
		return holderAccountMovement;
	}

	public void setHolderAccountMovement(HolderAccountMovement holderAccountMovement) {
		this.holderAccountMovement = holderAccountMovement;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public MovementType getMovementType() {
		return movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMovementDate() {
		return movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public BigDecimal getMovementQuantity() {
		return movementQuantity;
	}

	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	public BigDecimal getValuatorAmount() {
		return valuatorAmount;
	}

	public void setValuatorAmount(BigDecimal valuatorAmount) {
		this.valuatorAmount = valuatorAmount;
	}
}