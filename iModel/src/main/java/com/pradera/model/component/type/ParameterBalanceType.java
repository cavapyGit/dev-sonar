package com.pradera.model.component.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ParameterBalanceType {

	ISSUE_AMOUNT (new Long(1001), "MONTO EMITIDO "),
	SEGMENT_AMOUNT (new Long(1002), "MONTO PENDIENTE PARA TRAMO"),
	PLACED_AMOUNT (new Long(1003), "MONTO COLOCADO"),
	AMORTIZED_AMOUNT (new Long(1004), "MONTO AMORTIZADO"),
	CIRCULATION_AMOUNT (new Long(1005), "MONTO EN CIRCULACION"),
	
	SHARE_BALANCE (new Long(2001), "VALORES CONTABLES"),
	PLACED_BALANCE (new Long(2002), "VALORES CONTABLES"),
	CIRCULATION_BALANCE (new Long(2003), "VALORES CONTABLES"),
	PHYSICAL_BALANCE (new Long(2004), "VALORES CONTABLES"),
	DEMATERIALIZED_BALANCE (new Long(2005), "VALORES CONTABLES"),
	
	TOTAL_BALANCE (new Long(3001), "SALDO CONTABLE"),
	AVAILABLE_BALANCE (new Long(3002), "SALDO DISPONIBLE"),
	TRANSIT_BALANCE (new Long(3003), "SALDO EN TRANSITO"),
	PAWN_BALANCE (new Long(3004), "SALDO EN PRENDA"),
	BAN_BALANCE (new Long(3005), "SALDO EN EMBARGO"),
	OTHERS_BALANCE (new Long(3006), "SALDO BLOUEO OTROS"),
	RESERVE_BALANCE (new Long(3007), "SALDO EN ENCAJE"),
	OPPOSITION_BALANCE (new Long(3008), "SALDO EN OPOSICION"),
	ACCREDITATION_BALANCE (new Long(3009), "SALDO EN ACREDITACION"),
	REPORTING_BALANCE (new Long(3010), "SALDO REPORTANTE"),
	REPORTED_BALANCE (new Long(3011), "SALDO REPORTADO"),
	MARGIN_BALANCE (new Long(3012), "SALDO EN MARGEN"),
	PURCHASE_BALANCE (new Long(3013), "SALDO REFERENCIAL COMPRA"),
	SALE_BALANCE (new Long(3014), "SALDO COMPROMETIDO VENTA"),
	LENDER_BALANCE (new Long(3015), "SALDO PRESTAMIESTA"),
	LOANABLE_BALANCE (new Long(3016), "SALDO PRESTABLE"),
	BORROWER_BALANCE (new Long(3017), "SALDO PRESTATARIO"),
	GRANTED_BALANCE (new Long(3018), "SALDO CEDIDO"),
	GRANT_BALANCE (new Long(3019), "SALDO EN CESION"),
	
	TOTAL_AMOUNT(new Long(4001), "MONTO TOTAL"),
	AVAILABLE_AMOUNT(new Long(4002), "MONTO DISPONIBLE"),
	DEPOSIT_AMOUNT(new Long(4003), "MONTO DEPOSITO"),
	RETIREMENT_AMOUNT(new Long(4004), "MONTO RETIRO"),
	MARGIN_AMOUNT(new Long(4005), "MONTO MARGEN"),
	
	TOTAL_GUARANTEE(new Long(5001), "GARANTIA TOTAL"),
	PRINCIPAL_GUARANTEE(new Long(5002), "GARANTIA PRINCIPAL"),
	MARGIN_GUARANTEE(new Long(5003), "GARANTIA MARGEN"),
	PRINCIPAL_DIVIDENDS_GUARANTEE(new Long(5004), "GARANTIA DIVIDENDOS PRINCIPAL"),
	MARGIN_DIVIDENDS_GUARANTEE(new Long(5005), "GARANTIA DIVIDENDOS MARGEN"),
	INTEREST_MARGIN_GUARANTEE(new Long(5006), "GARANTIA INTERESES MARGEN");
			
	/** The code. */
	private Long code;
	
	private String descripcion;

	private ParameterBalanceType(Long code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static final List<ParameterBalanceType> list = new ArrayList<ParameterBalanceType>();
	public static final Map<Long, ParameterBalanceType> lookup = new HashMap<Long, ParameterBalanceType>();
	static {
		for (ParameterBalanceType s : EnumSet.allOf(ParameterBalanceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
