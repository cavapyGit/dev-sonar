package com.pradera.model.component;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.model.corporatives.type.OperationMovementSourceTarget;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.process.BusinessProcess;


/**
 * The persistent class for the OPERATION_MOVEMENT database table.
 * 
 */
@Entity
@Cacheable(true)
@Table(name="OPERATION_MOVEMENT")
public class OperationMovement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_OPERATION_MOVEMENT_PK")
	private Long idOperationMovementPk;

	@Column(name="EXECUTION_ORDER")
	private Long executionOrder;

	@Column(name="ID_STATE")
	private Long idState;

	@Column(name="IND_SOURCE_TARGET")
	private Long indSourceTarget;
	
	@Column(name="ENTITY_TYPE_AFFECTED")
	private Integer entityTypeAffected;

	@Column(name="LAST_MODIFY_APP")
	private Long lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to BusinessProcess
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BUSINESS_PROCESS_FK")
	private BusinessProcess businessProcess;

	//bi-directional many-to-one association to FundsOperationType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUNDS_OPERATION_TYPE_FK")
	private FundsOperationType fundsOperationType;

	//bi-directional many-to-one association to MovementType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

	//bi-directional many-to-one association to OperationType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OPERATION_TYPE_FK")
	private OperationType operationType;

    
    
    public OperationMovement() {
    }

	public Long getIdOperationMovementPk() {
		return this.idOperationMovementPk;
	}

	public void setIdOperationMovementPk(Long idOperationMovementPk) {
		this.idOperationMovementPk = idOperationMovementPk;
	}


	public Long getExecutionOrder() {
		return executionOrder;
	}

	public void setExecutionOrder(Long executionOrder) {
		this.executionOrder = executionOrder;
	}

	public Long getIdState() {
		return idState;
	}

	public void setIdState(Long idState) {
		this.idState = idState;
	}

	public Long getIndSourceTarget() {
		return indSourceTarget;
	}

	public void setIndSourceTarget(Long indSourceTarget) {
		this.indSourceTarget = indSourceTarget;
	}

	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BusinessProcess getBusinessProcess() {
		return this.businessProcess;
	}

	public void setBusinessProcess(BusinessProcess businessProcess) {
		this.businessProcess = businessProcess;
	}
	
	public FundsOperationType getFundsOperationType() {
		return this.fundsOperationType;
	}

	public void setFundsOperationType(FundsOperationType fundsOperationType) {
		this.fundsOperationType = fundsOperationType;
	}
	
	public MovementType getMovementType() {
		return this.movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}
	
	public OperationType getOperationType() {
		return this.operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}
	
	public Integer getEntityTypeAffected() {
		return entityTypeAffected;
	}

	public void setEntityTypeAffected(Integer entityTypeAffected) {
		this.entityTypeAffected = entityTypeAffected;
	}
	
	public boolean isSourceTargetBoth() {
		return (this.entityTypeAffected == null)?false:OperationMovementSourceTarget.BOTH.getCode().compareTo( this.entityTypeAffected )==0;
	}
	
	public boolean isSourceTargetManager() {
		return (this.entityTypeAffected == null)?false:OperationMovementSourceTarget.MANAGER.getCode().compareTo( this.entityTypeAffected )==0;
	}
	
	public boolean isSourceTargetManaged() {
		return (this.entityTypeAffected == null)?false:OperationMovementSourceTarget.MANAGED.getCode().compareTo( this.entityTypeAffected )==0;
	}
	
}