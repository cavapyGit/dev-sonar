package com.pradera.model.component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Issuance;


/**
 * The persistent class for the ISSUANCE_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="ISSUANCE_MOVEMENT")
public class IssuanceMovement implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ISSUANCE_MOVEMENT_GENERATOR", sequenceName="SQ_ID_ISSUANCE_MOVEMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUANCE_MOVEMENT_GENERATOR")
	@Column(name="ID_ISSUANCE_MOVEMENT_PK")
	private Long idIssuanceMovementPk;

	//bi-directional many-to-one association to issuance
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUANCE_CODE_FK")
	private Issuance issuance;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="MOVEMENT_AMOUNT")
	private BigDecimal movementAmount;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="MOVEMENT_DATE")
	private Date movementDate;

	//bi-directional many-to-one association to IssuanceOperation
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUANCE_OPERATION_FK")
	private IssuanceOperation issuanceOperation;

	//bi-directional many-to-one association to MovementType
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;

    
    
    public IssuanceMovement() {
    }

	public Long getIdIssuanceMovementPk() {
		return this.idIssuanceMovementPk;
	}

	public void setIdIssuanceMovementPk(Long idIssuanceMovementPk) {
		this.idIssuanceMovementPk = idIssuanceMovementPk;
	}

	

	public Issuance getIssuance() {
		return issuance;
	}

	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getMovementAmount() {
		return this.movementAmount;
	}

	public void setMovementAmount(BigDecimal movementAmount) {
		this.movementAmount = movementAmount;
	}

	public Date getMovementDate() {
		return this.movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public IssuanceOperation getIssuanceOperation() {
		return this.issuanceOperation;
	}

	public void setIssuanceOperation(IssuanceOperation issuanceOperation) {
		this.issuanceOperation = issuanceOperation;
	}
	
	public MovementType getMovementType() {
		return this.movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}