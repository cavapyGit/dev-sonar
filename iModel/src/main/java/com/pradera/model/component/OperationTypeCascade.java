package com.pradera.model.component;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="OPERATION_TYPE_CASCADE")
public class OperationTypeCascade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id	
	@Column(name="ID_OPERATION_CASCADE_PK")
	private Long idOperationTypeCascadePk;
	
	//bi-directional many-to-one association to OperationType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OPERATION_TYPE_FK")
	private OperationType operationType;
	
	//bi-directional many-to-one association to OperationType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OPERATION_TYPE_CASCADE")
	private OperationType operationTypeCascade;
	
	@Column(name="LAST_MODIFY_APP")
	private Long lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
}
