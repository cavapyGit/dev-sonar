package com.pradera.model.process;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.pradera.model.notification.NotificationConfiguration;
import com.pradera.model.notification.ProcessNotification;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BusinessProcess.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/03/2013
 */
@Entity
@Cacheable(true)
@Table(name = "BUSINESS_PROCESS")
@NamedQueries({
		@NamedQuery(name=BusinessProcess.PROCESS_FIND_BY_LEVEL, query=" select new BusinessProcess(b.idBusinessProcessPk,b.processName) from  BusinessProcess b where b.processLevel = :level and b.processState= :status")})
public class BusinessProcess implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant REPORTE_FIND_BY_FK. */
	public static final String PROCESS_FIND_BY_LEVEL = "BusinessProcess.findByLevel";

	/** The id business process pk. */
	@Id
	@SequenceGenerator(name = "BUSINESS_PROCESS_IDBUSINESSPROCESSPK_GENERATOR", sequenceName = "SQ_ID_BUSINESS_PROCESS_PK",allocationSize=1,initialValue=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BUSINESS_PROCESS_IDBUSINESSPROCESSPK_GENERATOR")
	@Column(name = "ID_BUSINESS_PROCESS_PK")
	private Long idBusinessProcessPk;
	
	@Version
	private Long version;

	/** The bean name. */
	@Column(name = "BEAN_NAME")
	private String beanName;

	/** The description. */
	private String description;

	/** The id option privilege. */
	@Column(name = "ID_OPTION_PRIVILEGE")
	private Integer idOptionPrivilege;

	/** The ind notices. */
	@Column(name = "IND_NOTICES")
	private Integer indNotices;

	/** The ind sequence batch. */
	@Column(name = "IND_SEQUENCE_BATCH")
	private Integer indSequenceBatch;
	
	@Column(name = "IND_MARKET_FACT")
	private Integer indMarketFact;

	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The mnemonic. */
	private String mnemonic;

	/** The process level. */
	@Column(name = "PROCESS_LEVEL")
	private Integer processLevel;

	/** The process name. */
	@Column(name = "PROCESS_NAME")
	private String processName;

	/** The process type. */
	@Column(name = "PROCESS_TYPE")
	private Integer processType;

	/** The process state. */
	@Column(name = "PROCESS_STATE")
	private Integer processState;
	
	
	/** The idSystem	 */
	@Column(name = "ID_SYSTEM")
	private Integer idSystem;
        
    @Column(name = "MODULE_NAME")
    private String moduleName;
    
    @Column(name = "IND_ACTIVATE_REBLOCK")
    private Integer indActivateReblock;
    

	// bi-directional many-to-one association to BusinessProcess
	/** The business process. */
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_REFERENCE_PROCESS_FK")
	private BusinessProcess businessProcess;

	// bi-directional many-to-one association to BusinessProcess
	/** The business processes. */
	@OneToMany(mappedBy = "businessProcess")
	private List<BusinessProcess> businessProcesses;

	// bi-directional many-to-one association to ProcessNotification
	/** The process notifications. */
	@OneToMany(mappedBy = "businessProcess")
	private List<ProcessNotification> processNotifications;
	
	@OneToMany(mappedBy = "businessProcess")
	private List<NotificationConfiguration> processConfigurations;

	// bi-directional many-to-one association to ProcessSchedule
	/** The process schedules. */
	@OneToMany(mappedBy = "businessProcess")
	private List<ProcessSchedule> processSchedules;

	// bi-directional many-to-one association to ProcessLogger
	/** The process loggers. */
	@OneToMany(mappedBy = "businessProcess", cascade = { CascadeType.ALL })
	private List<ProcessLogger> processLoggers;

	/**
	 * Instantiates a new business process.
	 */
	public BusinessProcess() {
	}

	public BusinessProcess(Long idBusinessProcessPk, String processName) {
		super();
		this.idBusinessProcessPk = idBusinessProcessPk;
		this.processName = processName;
	}

	/**
	 * Gets the id business process pk.
	 *
	 * @return the id business process pk
	 */
	public Long getIdBusinessProcessPk() {
		return this.idBusinessProcessPk;
	}

	/**
	 * Sets the id business process pk.
	 *
	 * @param idBusinessProcessPk the new id business process pk
	 */
	public void setIdBusinessProcessPk(Long idBusinessProcessPk) {
		this.idBusinessProcessPk = idBusinessProcessPk;
	}

	/**
	 * Gets the bean name.
	 *
	 * @return the bean name
	 */
	public String getBeanName() {
		return this.beanName;
	}

	/**
	 * Sets the bean name.
	 *
	 * @param beanName the new bean name
	 */
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the id option privilege.
	 *
	 * @return the id option privilege
	 */
	public Integer getIdOptionPrivilege() {
		return this.idOptionPrivilege;
	}

	/**
	 * Sets the id option privilege.
	 *
	 * @param idOptionPrivilege the new id option privilege
	 */
	public void setIdOptionPrivilege(Integer idOptionPrivilege) {
		this.idOptionPrivilege = idOptionPrivilege;
	}

	/**
	 * Gets the ind notices.
	 *
	 * @return the ind notices
	 */
	public Integer getIndNotices() {
		return this.indNotices;
	}

	/**
	 * Sets the ind notices.
	 *
	 * @param indNotices the new ind notices
	 */
	public void setIndNotices(Integer indNotices) {
		this.indNotices = indNotices;
	}

	/**
	 * Gets the ind sequence batch.
	 *
	 * @return the ind sequence batch
	 */
	public Integer getIndSequenceBatch() {
		return this.indSequenceBatch;
	}

	/**
	 * Sets the ind sequence batch.
	 *
	 * @param indSequenceBatch the new ind sequence batch
	 */
	public void setIndSequenceBatch(Integer indSequenceBatch) {
		this.indSequenceBatch = indSequenceBatch;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return this.mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the process level.
	 *
	 * @return the process level
	 */
	public Integer getProcessLevel() {
		return this.processLevel;
	}

	/**
	 * Sets the process level.
	 *
	 * @param processLevel the new process level
	 */
	public void setProcessLevel(Integer processLevel) {
		this.processLevel = processLevel;
	}

	/**
	 * Gets the process name.
	 *
	 * @return the process name
	 */
	public String getProcessName() {
		return this.processName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param processName the new process name
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Integer getProcessType() {
		return this.processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	
	/**
	 * Gets the business process.
	 *
	 * @return the business process
	 */
	public BusinessProcess getBusinessProcess() {
		return this.businessProcess;
	}

	/**
	 * Sets the business process.
	 *
	 * @param businessProcess the new business process
	 */
	public void setBusinessProcess(BusinessProcess businessProcess) {
		this.businessProcess = businessProcess;
	}

	/**
	 * Gets the business processes.
	 *
	 * @return the business processes
	 */
	public List<BusinessProcess> getBusinessProcesses() {
		return this.businessProcesses;
	}

	/**
	 * Sets the business processes.
	 *
	 * @param businessProcesses the new business processes
	 */
	public void setBusinessProcesses(List<BusinessProcess> businessProcesses) {
		this.businessProcesses = businessProcesses;
	}

	/**
	 * Gets the process loggers.
	 *
	 * @return the process loggers
	 */
	public List<ProcessLogger> getProcessLoggers() {
		return this.processLoggers;
	}

	/**
	 * Sets the process loggers.
	 *
	 * @param processLoggers the new process loggers
	 */
	public void setProcessLoggers(List<ProcessLogger> processLoggers) {
		this.processLoggers = processLoggers;
	}

	/**
	 * Gets the process notifications.
	 *
	 * @return the process notifications
	 */
	public List<ProcessNotification> getProcessNotifications() {
		return processNotifications;
	}

	/**
	 * Sets the process notifications.
	 *
	 * @param processNotifications the new process notifications
	 */
	public void setProcessNotifications(
			List<ProcessNotification> processNotifications) {
		this.processNotifications = processNotifications;
	}

	/**
	 * Gets the process schedules.
	 *
	 * @return the process schedules
	 */
	public List<ProcessSchedule> getProcessSchedules() {
		return processSchedules;
	}

	/**
	 * Sets the process schedules.
	 *
	 * @param processSchedules the new process schedules
	 */
	public void setProcessSchedules(List<ProcessSchedule> processSchedules) {
		this.processSchedules = processSchedules;
	}

	/**
	 * Gets the process state.
	 *
	 * @return the process state
	 */
	public Integer getProcessState() {
		return processState;
	}

	/**
	 * Sets the process state.
	 *
	 * @param processState the new process state
	 */
	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	/**
	 * @return the idSystem
	 */
	public Integer getIdSystem() {
		return idSystem;
	}

	/**
	 * @param idSystem the idSystem to set
	 */
	public void setIdSystem(Integer idSystem) {
		this.idSystem = idSystem;
	}

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Integer getIndActivateReblock() {
		return indActivateReblock;
	}

	public void setIndActivateReblock(Integer indActivateReblock) {
		this.indActivateReblock = indActivateReblock;
	}

	public Integer getIndMarketFact() {
		return indMarketFact;
	}

	public void setIndMarketFact(Integer indMarketFact) {
		this.indMarketFact = indMarketFact;
	}

	public List<NotificationConfiguration> getProcessConfigurations() {
		return processConfigurations;
	}

	public void setProcessConfigurations(
			List<NotificationConfiguration> processConfigurations) {
		this.processConfigurations = processConfigurations;
	}

	
}