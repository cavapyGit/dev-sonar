package com.pradera.model.process.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum ScheduleExecuteType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-jun-2015
 */
public enum ScheduleExecuteType {
	
	/** The finish. */
	FINISH(Integer.valueOf(0)),
	
	/** The running. */
	RUNNING(Integer.valueOf(1)),
	
	/** The running error. */
	RUNNING_ERROR(Integer.valueOf(2));
	
	/** The code. */
	private Integer code;
	
	 /** The Constant lookup. */
		private static final Map<Integer, ScheduleExecuteType> lookup = new HashMap<Integer, ScheduleExecuteType>();
	    static {
	    	for (ScheduleExecuteType d : EnumSet.allOf(ScheduleExecuteType.class)) {
	            lookup.put(d.getCode(), d);
	    	}
	}
	    
    /**
     * Gets the.
     *
     * @param codigo the codigo
     * @return the schedule execute type
     */
    public static ScheduleExecuteType get(Integer codigo) {
        return lookup.get(codigo);
    }
	
	/**
	 * Instantiates a new schedule execute type.
	 *
	 * @param code the code
	 */
	private ScheduleExecuteType(Integer code){
		this.setCode(code);
	}
	

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	private void setCode(Integer code) {
		this.code = code;
	}
}
