/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pradera.model.process;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.common.type.BooleanType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SchedulerInfo.
 *
 * 
 */
@Embeddable
@Access(AccessType.FIELD)
public class SchedulerInfo implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The start date. */
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    /** The end date. */
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    
    /** The schedule second. */
    @Column(name = "SCHEDULE_SECOND")
    private String scheduleSecond;
    
    /** The schedule minute. */
    @Column(name = "SCHEDULE_MINUTE")
    private String scheduleMinute;
    
    /** The schedule hour. */
    @Column(name = "SCHEDULE_HOUR")
    private String scheduleHour;
    
    /** The schedule day of month. */
    @Column(name = "SCHEDULE_DAYOFMONTH")
    private String scheduleDayOfMonth;
    
    /** The schedule month. */
    @Column(name = "SCHEDULE_MONTH")
    private String scheduleMonth;
    
    /** The schedule day of week. */
    @Column(name = "SCHEDULE_DAYOFWEEK")
    private String scheduleDayOfWeek;
    
    /** The schedule year. */
    @Column(name = "SCHEDULE_YEAR")
    private String scheduleYear;
    
    /** The ind persistence. */
    @Column(name = "IND_PERSISTENT")
    private Integer indPersistence;
    
    @Transient
    private Date time;
    
    @Transient
    private Long idProcessSchedulePk;
    
    /**
     * Gets the expression.
     *
     * @return the expression
     */
    public String getExpression() {
        StringBuilder expression = new StringBuilder(50);
        expression.append("sec=").append(scheduleSecond).append(";min=").append(scheduleMinute).append(";hour=").append(scheduleHour);
        expression.append(";dayOfMonth=").append(scheduleDayOfMonth).append(";month=").append(scheduleMonth);
        expression.append(";year=").append(scheduleYear).append(";dayOfWeek=").append(scheduleDayOfWeek);
        expression.append(";persistence=").append(BooleanType.YES.getCode().equals(indPersistence)?"S":"N");
        return expression.toString();
    }
    
    /**
     * Instance with default values.
     */
    public SchedulerInfo(){
        startDate = null;
        endDate = null;
        scheduleSecond = "0";
        scheduleMinute = "0";
        scheduleHour = "0";
        scheduleDayOfMonth = "*";
        scheduleMonth = "*";
        scheduleDayOfWeek = "*";
        scheduleYear = "*";
        indPersistence = BooleanType.NO.getCode();
    }

    /**
     * Gets the start date.
     *
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets the end date.
     *
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Sets the end date.
     *
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Gets the schedule second.
     *
     * @return the scheduleSecond
     */
    public String getScheduleSecond() {
        return scheduleSecond;
    }

    /**
     * Sets the schedule second.
     *
     * @param scheduleSecond the scheduleSecond to set
     */
    public void setScheduleSecond(String scheduleSecond) {
        this.scheduleSecond = scheduleSecond;
    }

    /**
     * Gets the schedule minute.
     *
     * @return the scheduleMinute
     */
    public String getScheduleMinute() {
        return scheduleMinute;
    }

    /**
     * Sets the schedule minute.
     *
     * @param scheduleMinute the scheduleMinute to set
     */
    public void setScheduleMinute(String scheduleMinute) {
        this.scheduleMinute = scheduleMinute;
    }

    /**
     * Gets the schedule hour.
     *
     * @return the scheduleHour
     */
    public String getScheduleHour() {
        return scheduleHour;
    }

    /**
     * Sets the schedule hour.
     *
     * @param scheduleHour the scheduleHour to set
     */
    public void setScheduleHour(String scheduleHour) {
        this.scheduleHour = scheduleHour;
    }

    /**
     * Gets the schedule day of month.
     *
     * @return the scheduleDayOfMonth
     */
    public String getScheduleDayOfMonth() {
        return scheduleDayOfMonth;
    }

    /**
     * Sets the schedule day of month.
     *
     * @param scheduleDayOfMonth the scheduleDayOfMonth to set
     */
    public void setScheduleDayOfMonth(String scheduleDayOfMonth) {
        this.scheduleDayOfMonth = scheduleDayOfMonth;
    }

    /**
     * Gets the schedule month.
     *
     * @return the scheduleMonth
     */
    public String getScheduleMonth() {
        return scheduleMonth;
    }

    /**
     * Sets the schedule month.
     *
     * @param scheduleMonth the scheduleMonth to set
     */
    public void setScheduleMonth(String scheduleMonth) {
        this.scheduleMonth = scheduleMonth;
    }

    /**
     * Gets the schedule day of week.
     *
     * @return the scheduleDayOfWeek
     */
    public String getScheduleDayOfWeek() {
        return scheduleDayOfWeek;
    }

    /**
     * Sets the schedule day of week.
     *
     * @param scheduleDayOfWeek the scheduleDayOfWeek to set
     */
    public void setScheduleDayOfWeek(String scheduleDayOfWeek) {
        this.scheduleDayOfWeek = scheduleDayOfWeek;
    }

    /**
     * Gets the schedule year.
     *
     * @return the scheduleYear
     */
    public String getScheduleYear() {
        return scheduleYear;
    }

    /**
     * Sets the schedule year.
     *
     * @param scheduleYear the scheduleYear to set
     */
    public void setScheduleYear(String scheduleYear) {
        this.scheduleYear = scheduleYear;
    }

    /**
     * Gets the ind persistence.
     *
     * @return the indPersistence
     */
    public Integer getIndPersistence() {
        return indPersistence;
    }

    /**
     * Sets the ind persistence.
     *
     * @param indPersistence the indPersistence to set
     */
    public void setIndPersistence(Integer indPersistence) {
        this.indPersistence = indPersistence;
    }
    
    /**
     * Sets the ind boolean persistence.
     *
     * @param indPersistence the new ind boolean persistence
     */
    public void setIndBooleanPersistence(boolean indPersistence){
    	this.indPersistence = BooleanType.get(indPersistence).getCode();
    }
    
    /**
     * Gets the ind boolean persistence.
     *
     * @return the ind boolean persistence
     */
    public boolean getIndBooleanPersistence(){
		if(indPersistence!=null && indPersistence.equals(BooleanType.YES.getCode())){
			return BooleanType.YES.getBooleanValue();
		}
    	return false;
    }
    
    /**
     * Gets the persistence description.
     *
     * @return the persistence description
     */
    public String getPersistenceDescription(){
    	String result = null;
    	if(indPersistence!=null && indPersistence.equals(BooleanType.YES.getCode())){
    		result = BooleanType.YES.getValue();
    	} else {
    		result = BooleanType.NO.getValue();
    	}
    	return result;
    }

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Long getIdProcessSchedulePk() {
		return idProcessSchedulePk;
	}

	public void setIdProcessSchedulePk(Long idProcessSchedulePk) {
		this.idProcessSchedulePk = idProcessSchedulePk;
	}
    
}
