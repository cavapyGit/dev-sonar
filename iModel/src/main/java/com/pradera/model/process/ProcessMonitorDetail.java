package com.pradera.model.process;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hcoarite
 */


@Entity
@Table(name = "PROCESS_MONITOR_DETAIL")
@NamedQueries({
    @NamedQuery(name = "ProcessMonitorDetail.findAll", query = "SELECT p FROM ProcessMonitorDetail p")})
public class ProcessMonitorDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_PROCESS_MONITOR_DETAIL_PK")
    private Long idProcessMonitorDetailPk;
    @Column(name = "ID_BUSINESS_PROCESS_FK")
    private Long idBusinessProcessFk;
    @Column(name = "PROCESS_NAME")
    private String processName;
    @Column(name = "STATE")
    private Short state;
    @Column(name = "QUERY_SQL")
    private String querySql;
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Column(name = "LAST_MODIFY_APP")
    private Long lastModifyApp;

    public ProcessMonitorDetail() {
    }

    public ProcessMonitorDetail(Long idProcessMonitorDetailPk) {
        this.idProcessMonitorDetailPk = idProcessMonitorDetailPk;
    }

    public Long getIdProcessMonitorDetailPk() {
        return idProcessMonitorDetailPk;
    }

    public void setIdProcessMonitorDetailPk(Long idProcessMonitorDetailPk) {
        this.idProcessMonitorDetailPk = idProcessMonitorDetailPk;
    }

    public Long getIdBusinessProcessFk() {
        return idBusinessProcessFk;
    }

    public void setIdBusinessProcessFk(Long idBusinessProcessFk) {
        this.idBusinessProcessFk = idBusinessProcessFk;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public String getQuerySql() {
        return querySql;
    }

    public void setQuerySql(String querySql) {
        this.querySql = querySql;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProcessMonitorDetailPk != null ? idProcessMonitorDetailPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcessMonitorDetail)) {
            return false;
        }
        ProcessMonitorDetail other = (ProcessMonitorDetail) object;
        if ((this.idProcessMonitorDetailPk == null && other.idProcessMonitorDetailPk != null) || (this.idProcessMonitorDetailPk != null && !this.idProcessMonitorDetailPk.equals(other.idProcessMonitorDetailPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
    	
        return "javaapplication1.ProcessMonitorDetail[ idProcessMonitorDetailPk=" + idProcessMonitorDetailPk + " ]";
    }
    
    
}
