package com.pradera.model.process.type;

import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum SendNotificationType.
 * ON if Business process should send notification
 * OFF if Business process dont should send notification
 * @author PraderaTechnologies.
 * @version 1.0 , 18/05/2013
 */
public enum SendNotificationType {
	
	/** The off. */
	OFF(Integer.valueOf(0)),
	
	/** The on. */
	ON(Integer.valueOf(1));
	
	/** The code. */
	private Integer code;

	/**
	 * Instantiates a new send notification type.
	 *
	 * @param code the code
	 */
	private SendNotificationType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}
	
	/** The Constant lookup. */
	private static final Map<Integer, SendNotificationType> lookup = new HashMap<Integer, SendNotificationType>();
    static {
        for (SendNotificationType d : SendNotificationType.values())
            lookup.put(d.getCode(), d);
    }
    
    /**
     * Gets the.
     *
     * @param codigo the codigo
     * @return the batch type
     */
    public static SendNotificationType get(Integer codigo) {
        return lookup.get(codigo);
    }
	
}
