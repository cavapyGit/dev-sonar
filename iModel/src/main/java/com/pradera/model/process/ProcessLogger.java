package com.pradera.model.process;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ProcessLogger.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/03/2013
 */
@NamedQueries({
	@NamedQuery(name=ProcessLogger.PROCESSLOGGER_DETAILS,
			query=" select distinct p from ProcessLogger p left join fetch p.processLoggerDetails pd where p.idProcessLoggerPk = :processId ")
})
@Entity
@Table(name="PROCESS_LOGGER")
public class ProcessLogger implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String PROCESSLOGGER_DETAILS = "ProcessLogger.getProcessAndDetails";

	/** The id process logger pk. */
	@Id
	@SequenceGenerator(name="PROCESS_LOGGER_IDPROCESSLOGGERPK_GENERATOR", sequenceName="SQ_ID_PROCESS_LOGGER_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROCESS_LOGGER_IDPROCESSLOGGERPK_GENERATOR")
	@Column(name="ID_PROCESS_LOGGER_PK")
	private Long idProcessLoggerPk;

    /** The finishing date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="FINISHING_DATE")
	private Date finishingDate;

	/** The id user account. */
	@Column(name="ID_USER_ACCOUNT")
	private String idUserAccount;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The process date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="PROCESS_DATE")
	private Date processDate;

	/** The processing time. */
	@Column(name="PROCESSING_TIME")
	private Time processingTime;

	/** The logger state. */
	@Column(name="LOGGER_STATE")
	private Integer loggerState;
	
	/** The error detail. */
	@Column(name="ERROR_DETAIL")
	private String errorDetail;

	//bi-directional many-to-one association to BusinessProcess
	/** The business process. */
	@ManyToOne
	@JoinColumn(name="ID_BUSINESS_PROCESS_FK",referencedColumnName="ID_BUSINESS_PROCESS_PK")
	private BusinessProcess businessProcess;

	//bi-directional many-to-one association to ProcessLoggerDetail
	/** The process logger details. */
	@OneToMany(mappedBy="processLogger",cascade=CascadeType.ALL)
	private List<ProcessLoggerDetail> processLoggerDetails;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("processLoggerDetails", processLoggerDetails);
        return detailsMap;
	}

    /**
     * Instantiates a new process logger.
     */
    public ProcessLogger() {
    }

	/**
	 * Gets the id process logger pk.
	 *
	 * @return the id process logger pk
	 */
	public Long getIdProcessLoggerPk() {
		return this.idProcessLoggerPk;
	}

	/**
	 * Sets the id process logger pk.
	 *
	 * @param idProcessLoggerPk the new id process logger pk
	 */
	public void setIdProcessLoggerPk(Long idProcessLoggerPk) {
		this.idProcessLoggerPk = idProcessLoggerPk;
	}

	/**
	 * Gets the finishing date.
	 *
	 * @return the finishing date
	 */
	public Date getFinishingDate() {
		return this.finishingDate;
	}

	/**
	 * Sets the finishing date.
	 *
	 * @param finishingDate the new finishing date
	 */
	public void setFinishingDate(Date finishingDate) {
		this.finishingDate = finishingDate;
	}

	/**
	 * Gets the id user account.
	 *
	 * @return the id user account
	 */
	public String getIdUserAccount() {
		return this.idUserAccount;
	}

	/**
	 * Sets the id user account.
	 *
	 * @param idUserAccount the new id user account
	 */
	public void setIdUserAccount(String idUserAccount) {
		this.idUserAccount = idUserAccount;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the process date.
	 *
	 * @return the process date
	 */
	public Date getProcessDate() {
		return this.processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate the new process date
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * Gets the processing time.
	 *
	 * @return the processing time
	 */
	public Object getProcessingTime() {
		return this.processingTime;
	}

	/**
	 * Sets the processing time.
	 *
	 * @param processingTime the new processing time
	 */
	public void setProcessingTime(Time processingTime) {
		this.processingTime = processingTime;
	}



	/**
	 * Gets the business process.
	 *
	 * @return the business process
	 */
	public BusinessProcess getBusinessProcess() {
		return this.businessProcess;
	}

	/**
	 * Sets the business process.
	 *
	 * @param businessProcess the new business process
	 */
	public void setBusinessProcess(BusinessProcess businessProcess) {
		this.businessProcess = businessProcess;
	}
	
	/**
	 * Gets the process logger details.
	 *
	 * @return the process logger details
	 */
	public List<ProcessLoggerDetail> getProcessLoggerDetails() {
		return this.processLoggerDetails;
	}

	/**
	 * Sets the process logger details.
	 *
	 * @param processLoggerDetails the new process logger details
	 */
	public void setProcessLoggerDetails(List<ProcessLoggerDetail> processLoggerDetails) {
		this.processLoggerDetails = processLoggerDetails;
	}

	/**
	 * Gets the logger state.
	 *
	 * @return the logger state
	 */
	public Integer getLoggerState() {
		return loggerState;
	}

	/**
	 * Sets the logger state.
	 *
	 * @param loggerState the new logger state
	 */
	public void setLoggerState(Integer loggerState) {
		this.loggerState = loggerState;
	}

	/**
	 * Gets the error detail.
	 *
	 * @return the error detail
	 */
	public String getErrorDetail() {
		return errorDetail;
	}

	/**
	 * Sets the error detail.
	 *
	 * @param errorDetail the new error detail
	 */
	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}


	
}