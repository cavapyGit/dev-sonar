package com.pradera.model.process;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ProcessLoggerDetail.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/03/2013
 */
@Entity
@Table(name="PROCESS_LOGGER_DETAIL")
public class ProcessLoggerDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id process logger detail pk. */
	@Id
	@SequenceGenerator(name="PROCESS_LOGGER_DETAIL_IDPROCESSLOGGERDETAILPK_GENERATOR", sequenceName="SQ_ID_PROCESS_LOGGER_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROCESS_LOGGER_DETAIL_IDPROCESSLOGGERDETAILPK_GENERATOR")
	@Column(name="ID_PROCESS_LOGGER_DETAIL_PK")
	private Long idProcessLoggerDetailPk;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The parameter name. */
	@Column(name="PARAMETER_NAME")
	private String parameterName;

	/** The parameter value. */
	@Column(name="PARAMETER_VALUE")
	private String parameterValue;

	//bi-directional many-to-one association to ProcessLogger
    /** The process logger. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCESS_LOGGER_FK",referencedColumnName="ID_PROCESS_LOGGER_PK")
	private ProcessLogger processLogger;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {

		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

    /**
     * Instantiates a new process logger detail.
     */
    public ProcessLoggerDetail() {
    }

	/**
	 * Gets the id process logger detail pk.
	 *
	 * @return the id process logger detail pk
	 */
	public Long getIdProcessLoggerDetailPk() {
		return this.idProcessLoggerDetailPk;
	}

	/**
	 * Sets the id process logger detail pk.
	 *
	 * @param idProcessLoggerDetailPk the new id process logger detail pk
	 */
	public void setIdProcessLoggerDetailPk(Long idProcessLoggerDetailPk) {
		this.idProcessLoggerDetailPk = idProcessLoggerDetailPk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the parameter name.
	 *
	 * @return the parameter name
	 */
	public String getParameterName() {
		return this.parameterName;
	}

	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the new parameter name
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	/**
	 * Gets the parameter value.
	 *
	 * @return the parameter value
	 */
	public String getParameterValue() {
		return this.parameterValue;
	}

	/**
	 * Sets the parameter value.
	 *
	 * @param parameterValue the new parameter value
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	/**
	 * Gets the process logger.
	 *
	 * @return the process logger
	 */
	public ProcessLogger getProcessLogger() {
		return this.processLogger;
	}

	/**
	 * Sets the process logger.
	 *
	 * @param processLogger the new process logger
	 */
	public void setProcessLogger(ProcessLogger processLogger) {
		this.processLogger = processLogger;
	}

	
	
}