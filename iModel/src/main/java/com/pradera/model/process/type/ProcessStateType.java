package com.pradera.model.process.type;

import java.util.HashMap;
import java.util.Map;



/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ProcessStateType.
 * has REGISTERED and ANNULED
 * @author PraderaTechnologies.
 * @version 1.0 , 29/11/2012
 */
public enum ProcessStateType {

	/** The online. */
	REGISTERED(Integer.valueOf(1362),"REGISTRADO"),
	/** The batch. */
	ANNULED(Integer.valueOf(1363),"ANULADO");
	
	/** The code. */
	private Integer code;
	
	private String value;
	
	 /**
 	 * Instantiates a new process state type.
 	 *
 	 * @param code the code
 	 */
 	private ProcessStateType(Integer code,String value) {
		 this.code = code;
		 this.value = value;
	}
	 
 	/** The Constant lookup. */
	private static final Map<Integer, ProcessStateType> lookup = new HashMap<Integer, ProcessStateType>();
	
    static {
        for (ProcessStateType d : ProcessStateType.values())
            lookup.put(d.getCode(), d);
    }
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}
	
	public String getValue() {
		return this.value;
	}
    
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the alert type
	 */
	public static ProcessStateType get(Integer codigo) {
        return lookup.get(codigo);
    }
	 
}
