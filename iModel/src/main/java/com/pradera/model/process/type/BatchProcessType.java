package com.pradera.model.process.type;

import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum BatchProcessType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/12/2012
 */
public enum BatchProcessType {
	
	/** The sequential. */
	SEQUENTIAL(Integer.valueOf(1)),
	
	/** The paralell. */
	PARALELL(Integer.valueOf(2));
	
	/** The code. */
	private Integer code;

	/**
	 * Instantiates a new batch type.
	 *
	 * @param code the code
	 */
	private BatchProcessType(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}
	
	/** The Constant lookup. */
	private static final Map<Integer, BatchProcessType> lookup = new HashMap<Integer, BatchProcessType>();
    static {
        for (BatchProcessType d : BatchProcessType.values()) {
            lookup.put(d.getCode(), d);
        }
    }
    
    /**
     * Gets the.
     *
     * @param codigo the codigo
     * @return the batch type
     */
    public static BatchProcessType get(Integer codigo) {
        return lookup.get(codigo);
    }
}
