package com.pradera.model.process.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum ProcessType.
 * has ONLINE and BATCH
 * @author PraderaTechnologies.
 * @version 1.0 , 29/11/2012
 */
public enum ProcessType {

	/** The online. */
	ONLINE(Integer.valueOf(1360),"ONLINE"),
	/** The batch. */
	BATCH(Integer.valueOf(1361),"BATCH");
	
	/** The code. */
	private Integer code;
	
	/** The Value. */
	private String value;
	
	 /**
 	 * Instantiates a new process type.
 	 *
 	 * @param code the code
 	 */
 	private ProcessType(Integer code,String value) {
		 this.code = code;
		 this.value = value;
	}
 	
 	/** The Constant List. */
 		public static final List<ProcessType> list = new ArrayList<ProcessType>();
	 /** The Constant lookup. */
		private static final Map<Integer, ProcessType> lookup = new HashMap<Integer, ProcessType>();
	    static {
	    	for (ProcessType d : EnumSet.allOf(ProcessType.class)) {
	        
	        	list.add(d);
	            lookup.put(d.getCode(), d);
	    	}
	    }
		
		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return this.code;
		}
	    
		/**
		 * Gets the.
		 *
		 * @param codigo the codigo
		 * @return the alert type
		 */
		public static ProcessType get(Integer codigo) {
	        return lookup.get(codigo);
	    }

		/**
		 * Get the value
		 * 
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * Set the value
		 * 
		 * @param value the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}
	 
		
}
