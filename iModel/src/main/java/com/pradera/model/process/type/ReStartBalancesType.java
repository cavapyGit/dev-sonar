package com.pradera.model.process.type;


public enum ReStartBalancesType {
	/**Estado registrado para re inicio de saldos contables*/
	REGISTERED(Integer.valueOf(2509),"REGISTRADO"),
	/**Ejecutado, es cuando se confirmo y ya se re inicio los saldos contables*/
	EXECUTED(Integer.valueOf(2511),"EJECUTADO"),
	/**Ejecutado, es cuando se confirmo y ya se re inicio los saldos contables*/
	CONFIRMED(Integer.valueOf(2512),"CONFIRMADO"),
	/**Rechazado*/
	REJECTED(Integer.valueOf(2510),"RECHAZADO");
	
	/** The code. */
	private Integer code;
	
	/** The Value. */
	private String value;
	
	 /**
 	 * Instantiates a new process type.
 	 *
 	 * @param code the code
 	 */
 	private ReStartBalancesType(Integer code,String value) {
		 this.code = code;
		 this.value = value;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
