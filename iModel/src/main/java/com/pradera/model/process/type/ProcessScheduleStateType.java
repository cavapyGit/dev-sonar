package com.pradera.model.process.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author PraderaTechnologies
 * 
 */
public enum ProcessScheduleStateType {
	
	ACTIVE(Integer.valueOf(1369),"ACTIVADO"),
	DEACTIVE(Integer.valueOf(1370),"DESACTIVADO");

	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private ProcessScheduleStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<ProcessScheduleStateType> list = new ArrayList<ProcessScheduleStateType>();
	
	public static final Map<Integer, ProcessScheduleStateType> lookup = new HashMap<Integer, ProcessScheduleStateType>();
	static {
		for (ProcessScheduleStateType s : EnumSet.allOf(ProcessScheduleStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the process schedule state type
	 */
	public static ProcessScheduleStateType get(Integer codigo) {
        return lookup.get(codigo);
    }

}
