package com.pradera.model.process;

import java.io.Serializable;



/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class Process.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/11/2012
 */
public class Process implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6497781176492442094L;
	
	/** The id process. */
	private Integer idProcess;
	
	/** The name. */
	private String name;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The type process. */
	private Integer processType;
	
	/** The type alert. */
	private Integer alertType;
	
	
	/** The batch type. */
	private Integer batchType;
	
	/** The bean name. */
	private String beanName;

	/**
	 * Instantiates a new process.
	 */
	public Process() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id process.
	 *
	 * @return the id process
	 */
	public Integer getIdProcess() {
		return idProcess;
	}

	/**
	 * Sets the id process.
	 *
	 * @param idProcess the new id process
	 */
	public void setIdProcess(Integer idProcess) {
		this.idProcess = idProcess;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * Gets the alert type.
	 *
	 * @return the alert type
	 */
	public Integer getAlertType() {
		return alertType;
	}

	/**
	 * Sets the alert type.
	 *
	 * @param alertType the new alert type
	 */
	public void setAlertType(Integer alertType) {
		this.alertType = alertType;
	}

	/**
	 * Gets the batch type.
	 *
	 * @return the batch type
	 */
	public Integer getBatchType() {
		return batchType;
	}

	/**
	 * Sets the batch type.
	 *
	 * @param batchType the new batch type
	 */
	public void setBatchType(Integer batchType) {
		this.batchType = batchType;
	}

	/**
	 * Gets the bean name.
	 *
	 * @return the bean name
	 */
	public String getBeanName() {
		return beanName;
	}

	/**
	 * Sets the bean name.
	 *
	 * @param beanName the new bean name
	 */
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	

	

}
