package com.pradera.model.process;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.process.type.ProcessScheduleStateType;

/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 *
 * The Class ProcessSchedule.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/03/2013
 */
@NamedQueries({
	@NamedQuery(name = ProcessSchedule.PROCESSSCHEDULE, query = "Select distinct s From ProcessSchedule s join fetch s.businessProcess bp where s.scheduleState = :activeState")
})
@Entity
@Table(name = "PROCESS_SCHEDULE")
public class ProcessSchedule implements Serializable, Auditable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    
    /** The Constant PARTICIPANT. */
	public static final String PROCESSSCHEDULE = "ProcessSchedule.searchAllScheduler";
    /**
     * The id process schedule pk.
     */
    @Id
    @SequenceGenerator(name = "PROCESS_SCHEDULE_IDPROCESSSCHEDULEPK_GENERATOR", sequenceName = "SQ_ID_PROCESS_SCHEDULE_PK", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PROCESS_SCHEDULE_IDPROCESSSCHEDULEPK_GENERATOR")
    @Column(name = "ID_PROCESS_SCHEDULE_PK")
    private Long idProcessSchedulePk;
    
    @Version
    private Long version;
    
    /**
     * The schedule state.
     */
    @Column(name = "SCHEDULE_STATE")
    private Integer scheduleState;
    
    @Column(name = "SCHEDULE_DESCRIPTION")    
    private String scheduleDescription;

    
    
    @Column(name = "REGISTRY_USER")
    @NotNull
    private String registryUser;
    
    @Column(name = "REGISTRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date registryDate;
    
    @Transient
    private Date nextTimeOut;
    
    /**
     * The last modify app.
     */
    @Column(name = "LAST_MODIFY_APP")
    @NotNull
    private Integer lastModifyApp;
    /**
     * The last modify date.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_MODIFY_DATE")
    @NotNull
    private Date lastModifyDate;
    /**
     * The last modify ip.
     */
    @Column(name = "LAST_MODIFY_IP")
    @NotNull
    private String lastModifyIp;
    /**
     * The last modify user.
     */
    @Column(name = "LAST_MODIFY_USER")
    @NotNull
    private String lastModifyUser;
    //bi-directional many-to-one association to BusinessProcess
    /**
     * The business process.
     */
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "ID_BUSINESS_PROCESS_FK")
    private BusinessProcess businessProcess;
    //bi-directional many-to-one association to ProcessSchedule
    /**
     * The process schedule.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_REFERENCE_SCHEDULE_FK")
    private ProcessSchedule processSchedule;
    //bi-directional many-to-one association to ProcessSchedule

    
    @Embedded
    private SchedulerInfo scheduleInfo;
    
    @Transient
    private boolean blExist;
    
    @Column(name="SCHEDULE_EXECUTION")
    private Integer scheduleExecution;

    /**
     * Instantiates a new process schedule.
     */
    public ProcessSchedule() {
        scheduleInfo = new SchedulerInfo();
    }
    
    /**
     * Gets the schedule state description.
     *
     * @return the schedule state description
     */
    public String getScheduleStateDescription() {
    	return ProcessScheduleStateType.get(scheduleState).getValue();
	}
    
    /**
     * Gets the id process schedule pk.
     *
     * @return the id process schedule pk
     */
    public Long getIdProcessSchedulePk() {
        return this.idProcessSchedulePk;
    }

    /**
     * Gets the last modify date.
     *
     * @return the last modify date
     */
    public Date getLastModifyDate() {
        return this.lastModifyDate;
    }

    /**
     * Sets the last modify date.
     *
     * @param lastModifyDate the new last modify date
     */
    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    /**
     * Gets the last modify ip.
     *
     * @return the last modify ip
     */
    public String getLastModifyIp() {
        return this.lastModifyIp;
    }

    /**
     * Sets the last modify ip.
     *
     * @param lastModifyIp the new last modify ip
     */
    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    /**
     * Gets the last modify user.
     *
     * @return the last modify user
     */
    public String getLastModifyUser() {
        return this.lastModifyUser;
    }

    /**
     * Sets the last modify user.
     *
     * @param lastModifyUser the new last modify user
     */
    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    /**
     * Gets the last modify app.
     *
     * @return the last modify app
     */
    public Integer getLastModifyApp() {
        return lastModifyApp;
    }

    /**
     * Sets the last modify app.
     *
     * @param lastModifyApp the new last modify app
     */
    public void setLastModifyApp(Integer lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    /**
     * Gets the business process.
     *
     * @return the business process
     */
    public BusinessProcess getBusinessProcess() {
        return this.businessProcess;
    }

    /**
     * Sets the business process.
     *
     * @param businessProcess the new business process
     */
    public void setBusinessProcess(BusinessProcess businessProcess) {
        this.businessProcess = businessProcess;
    }

    /**
     * Gets the process schedule.
     *
     * @return the process schedule
     */
    public ProcessSchedule getProcessSchedule() {
        return this.processSchedule;
    }

    /**
     * Sets the process schedule.
     *
     * @param processSchedule the new process schedule
     */
    public void setProcessSchedule(ProcessSchedule processSchedule) {
        this.processSchedule = processSchedule;
    }

    /**
     * Gets the schedule state.
     *
     * @return the schedule state
     */
    public Integer getScheduleState() {
        return scheduleState;
    }

    /**
     * Sets the schedule state.
     *
     * @param scheduleState the new schedule state
     */
    public void setScheduleState(Integer scheduleState) {
        this.scheduleState = scheduleState;
    }

    /**
     * Sets the id process schedule pk.
     *
     * @param idProcessSchedulePk the new id process schedule pk
     */
    public void setIdProcessSchedulePk(Long idProcessSchedulePk) {
        this.idProcessSchedulePk = idProcessSchedulePk;
    }

    /**
     * @return the scheduleDescription
     */
    public String getScheduleDescription() {
        return scheduleDescription;
    }

    /**
     * @param scheduleDescription the scheduleDescription to set
     */
    public void setScheduleDescription(String scheduleDescription) {
        this.scheduleDescription = scheduleDescription;
    }

    /**
     * @return the registryUser
     */
    public String getRegistryUser() {
        return registryUser;
    }

    /**
     * @param registryUser the registryUser to set
     */
    public void setRegistryUser(String registryUser) {
        this.registryUser = registryUser;
    }

    /**
     * @return the registryDate
     */
    public Date getRegistryDate() {
        return registryDate;
    }

    /**
     * @param registryDate the registryDate to set
     */
    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    /**
     * @return the scheduleInfo
     */
    public SchedulerInfo getScheduleInfo() {
        return scheduleInfo;
    }

    /**
     * @param scheduleInfo the scheduleInfo to set
     */
    public void setScheduleInfo(SchedulerInfo scheduleInfo) {
        this.scheduleInfo = scheduleInfo;
    }

    /**
     * @return the nextTimeOut
     */
    public Date getNextTimeOut() {
        return nextTimeOut;
    }

    /**
     * @param nextTimeOut the nextTimeOut to set
     */
    public void setNextTimeOut(Date nextTimeOut) {
        this.nextTimeOut = nextTimeOut;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.idProcessSchedulePk != null ? this.idProcessSchedulePk.hashCode() : 0);
        hash = 53 * hash + (this.scheduleState != null ? this.scheduleState.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProcessSchedule other = (ProcessSchedule) obj;
        if (this.idProcessSchedulePk != other.idProcessSchedulePk && (this.idProcessSchedulePk == null || !this.idProcessSchedulePk.equals(other.idProcessSchedulePk))) {
            return false;
        }
        if (this.scheduleState != other.scheduleState && (this.scheduleState == null || !this.scheduleState.equals(other.scheduleState))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProcessSchedule{" + "idProcessSchedulePk=" + idProcessSchedulePk + ", scheduleState=" + scheduleState + ", scheduleDescription=" + scheduleDescription + '}';
    }

    @Override
    public void setAudit(LoggerUser loggerUser) {
        if(loggerUser != null){
            lastModifyApp=loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate=loggerUser.getAuditTime();
            lastModifyIp=loggerUser.getIpAddress();
            lastModifyUser=loggerUser.getUserName();
        }
    }

    @Override
    public Map<String, List<? extends Auditable>> getListForAudit() {
       return null;
    }

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public boolean isBlExist() {
		return blExist;
	}

	public void setBlExist(boolean blExist) {
		this.blExist = blExist;
	}

	public Integer getScheduleExecution() {
		return scheduleExecution;
	}

	public void setScheduleExecution(Integer scheduleExecution) {
		this.scheduleExecution = scheduleExecution;
	}
    
    
}