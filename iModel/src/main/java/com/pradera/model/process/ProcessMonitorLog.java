package com.pradera.model.process;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.pradera.model.component.type.ParameterOperationType;

/**
 *
 * @author hcoarite
 */


@Entity
@Table(name = "PROCESS_MONITOR_LOG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcessMonitorLog.findAll", query = "SELECT p FROM ProcessMonitorLog p")})
public class ProcessMonitorLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@SequenceGenerator(name="PROCESS_MONITOR_LOG_ID_GENERATOR", sequenceName="SQ_ID_PROCESS_MONITOR_LOG_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROCESS_MONITOR_LOG_ID_GENERATOR")
    @Column(name = "ID_PROCESS_MONITOR_LOG_PK")
    private Long idProcessMonitorLogPk;
    @Column(name = "ID_PROCESS_MONITOR_DETAIL_FK")
    private Long idProcessMonitorDetailFk;
    @Column(name = "PROCESS_STATUS")
    private Long processStatus;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Column(name = "LAST_MODIFY_APP")
    private Long lastModifyApp;
    
    @Transient
    private String progressDesc;
    
    public ProcessMonitorLog() {
    }

    public ProcessMonitorLog(Long idProcessMonitorLogPk) {
        this.idProcessMonitorLogPk = idProcessMonitorLogPk;
    }

    public Long getIdProcessMonitorLogPk() {
        return idProcessMonitorLogPk;
    }

    public void setIdProcessMonitorLogPk(Long idProcessMonitorLogPk) {
        this.idProcessMonitorLogPk = idProcessMonitorLogPk;
    }

    public Long getIdProcessMonitorDetailFk() {
        return idProcessMonitorDetailFk;
    }

    public void setIdProcessMonitorDetailFk(Long idProcessMonitorDetailFk) {
        this.idProcessMonitorDetailFk = idProcessMonitorDetailFk;
    }

    public Long getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(Long processStatus) {
        this.processStatus = processStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProcessMonitorLogPk != null ? idProcessMonitorLogPk.hashCode() : 0);
        return hash;
    }

    public String getProgressDesc() {
		return progressDesc;
	}
	public void setProgressDesc(String progressDesc) {
		this.progressDesc = progressDesc;
	}

	@Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcessMonitorLog)) {
            return false;
        }
        ProcessMonitorLog other = (ProcessMonitorLog) object;
        if ((this.idProcessMonitorLogPk == null && other.idProcessMonitorLogPk != null) || (this.idProcessMonitorLogPk != null && !this.idProcessMonitorLogPk.equals(other.idProcessMonitorLogPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
    	
        return "javaapplication1.ProcessMonitorLog[ idProcessMonitorLogPk=" + idProcessMonitorLogPk + " ]";
    }
    
    
    
}
