package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;


/**
 * The persistent class for the ISSUER_HISTORY_STATE database table.
 * 
 */
@Entity
@Table(name="ISSUER_HISTORY_STATE")
public class IssuerHistoryState implements Serializable,Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ISSUER_HISTORY_STATE_IDISSUERHISSTATEPK_GENERATOR", sequenceName="SQ_ID_ISSUER_HISTORY_STATE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUER_HISTORY_STATE_IDISSUERHISSTATEPK_GENERATOR")
	@Column(name="ID_ISS_HISTORY_STATE_PK")
	private Long idIssHistoryStatePk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="NEW_STATE")
	private Integer newState;

	@Column(name="OLD_STATE")
	private Integer oldState;
	@Column(name="MOTIVE")
	private Integer motive;
	
	@Column(name="UPDATE_STATE_DATE")
	private Date updateStateDate;
	
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional many-to-one association to Issuer
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;
    
    //bi-directional many-to-one association to IssuerRequest
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_REQUEST_FK")
	private IssuerRequest issuerRequest;

    public IssuerHistoryState() {
    }

    public String getOldStateDescription(){
    	if(Validations.validateIsNotNullAndPositive(oldState)){
    		return IssuerStateType.get(oldState).getValue();
    	}
    	return null;
    }
    
    public String getNewStateDescription(){
    	if(Validations.validateIsNotNullAndPositive(newState)){
    		return IssuerStateType.get(newState).getValue();
    	}
    	return null;
    }
    
	public Long getIdIssHistoryStatePk() {
		return idIssHistoryStatePk;
	}

	public void setIdIssHistoryStatePk(Long idIssHistoryStatePk) {
		this.idIssHistoryStatePk = idIssHistoryStatePk;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getNewState() {
		return newState;
	}

	public void setNewState(Integer newState) {
		this.newState = newState;
	}

	public Integer getOldState() {
		return oldState;
	}

	public void setOldState(Integer oldState) {
		this.oldState = oldState;
	}

	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public Date getUpdateStateDate() {
		return updateStateDate;
	}

	public void setUpdateStateDate(Date updateStateDate) {
		this.updateStateDate = updateStateDate;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public IssuerRequest getIssuerRequest() {
		return issuerRequest;
	}

	public void setIssuerRequest(IssuerRequest issuerRequest) {
		this.issuerRequest = issuerRequest;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
    
    
	
}