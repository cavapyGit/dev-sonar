package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ValuatorConvertivilityType {
	

	MANDATORY(Integer.valueOf(2199),"OBLIGATORIO"), 
	OPTIONAL(Integer.valueOf(2200),"OPTIONAL");
	
	public final static List<ValuatorConvertivilityType> list=new ArrayList<ValuatorConvertivilityType>();
	public static final Map<Integer, ValuatorConvertivilityType> lookup = new HashMap<Integer, ValuatorConvertivilityType>();

	private Integer code;
	private String value;
	
	static{
		for(ValuatorConvertivilityType d : ValuatorConvertivilityType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	private ValuatorConvertivilityType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static ValuatorConvertivilityType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
}
