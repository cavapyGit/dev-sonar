package com.pradera.model.issuancesecuritie.type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Enum que manejo los estados de la tabla IssuanceAux
 * 
 * 
 * @author eibanez
 *
 */
public enum IssuanceAuxStateType {
	REGISTERED(Integer.valueOf(2400),"REGISTRADO"),
	REVIEWED(Integer.valueOf(2401),"REVISADO"),
	APPROBED(Integer.valueOf(2402),"APROBADO"),
	CONFIRMED(Integer.valueOf(2403),"CONFIRMADO"),
	REJECTED(Integer.valueOf(2404),"RECHAZADO"),
	EXPIRED(Integer.valueOf(445),"VENCIDO"),
	SUSPENDED(Integer.valueOf(1205),"SUSPENDIDO");
	
		/** The Constant list. */
	private static final List<IssuanceAuxStateType> list=new ArrayList<IssuanceAuxStateType>();
	
	/** The Constant map. */
	private static final Map<Integer, IssuanceAuxStateType> lookup=new HashMap<Integer, IssuanceAuxStateType>();
	
	static{
		for(IssuanceAuxStateType issu : IssuanceAuxStateType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new issuance state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuanceAuxStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static IssuanceAuxStateType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
}






