package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

public enum CalendarType {
	

    COMMERCIAL(Integer.valueOf(145),"COMERCIAL","360"), 
	CALENDAR(Integer.valueOf(541),"CALENDARIO","365");
	
	public final static List<CalendarType> list=new ArrayList<CalendarType>();
	public static final Map<Integer, CalendarType> lookup = new HashMap<Integer, CalendarType>();

	private Integer code;
	private String value;
	private String text1;
	
	static{
		for(CalendarType d : CalendarType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	/**
	 * Instantiates a new instrument type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 */
	private CalendarType(Integer code, String value,String text1){
		this.code=code;
		this.value=value;
		this.text1=text1;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the instrument type
	 */
	public static CalendarType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	

	
}
