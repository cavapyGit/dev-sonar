package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum RequestParticipantFileType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/04/2013
 */
public enum RequestIssuerFileType {
	
	/** The register. */
	REGISTER(Integer.valueOf(1106),"REGISTRO"),
	
	/** The block. */
	BLOCK(Integer.valueOf(1107),"BLOQUEO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new request participant file type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private RequestIssuerFileType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<RequestIssuerFileType> list = new ArrayList<RequestIssuerFileType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RequestIssuerFileType> lookup = new HashMap<Integer, RequestIssuerFileType>();
	static {
		for (RequestIssuerFileType s : EnumSet.allOf(RequestIssuerFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
