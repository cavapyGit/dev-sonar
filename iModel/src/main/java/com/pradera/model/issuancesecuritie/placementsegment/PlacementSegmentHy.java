package com.pradera.model.issuancesecuritie.placementsegment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PLACEMENT_SEGMENT_HYS database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/04/2013
 */
@Entity
@Table(name="PLACEMENT_SEGMENT_HYS")
public class PlacementSegmentHy implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id placement segment hys pk. */
	@Id
	@SequenceGenerator(name="SQ_PLACEMENT_SEGMENT_HYS_PK", sequenceName="SQ_ID_PLACEMENT_SEGMENT_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_PLACEMENT_SEGMENT_HYS_PK")
	@Column(name="ID_PLACEMENT_SEGMENT_HYS_PK")
	private Long idPlacementSegmentHysPk;

    /** The action date. */
    @Temporal( TemporalType.DATE)
	@Column(name="ACTION_DATE")
	private Date actionDate;

	/** The action motive. */
	@Column(name="ACTION_MOTIVE")
	private BigDecimal actionMotive;

	/** The amount to place. */
	@Column(name="AMOUNT_TO_PLACE")
	private BigDecimal amountToPlace;

    /** The begining date. */
    @Temporal( TemporalType.DATE)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

    /** The closing date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CLOSING_DATE")
	private Date closingDate;

	/** The id issuance code fk. */
	@Column(name="ID_ISSUANCE_CODE_FK")
	private String idIssuanceCodeFk;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The other action motive. */
	@Column(name="OTHER_ACTION_MOTIVE")
	private String otherActionMotive;

	/** The placed amount. */
	@Column(name="PLACED_AMOUNT")
	private BigDecimal placedAmount;

	/** The placement term. */
	@Column(name="PLACEMENT_TERM")
	private BigDecimal placementTerm;

    /** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The registry user. */
	@Column(name="CONFIRM_USER")
	private String confirmUser;
	
    /** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;
    
    /** The open date. */
    @Temporal( TemporalType.DATE)
	@Column(name="OPEN_DATE")
	private Date openDate;

	/** The request type. */
	@Column(name="REQUEST_TYPE")
	private BigDecimal requestType;

	/** The state placement segment. */
	@Column(name="STATE_PLACEMENT_SEGMENT")
	private BigDecimal statePlacementSegment;

	/** The tranche number. */
	@Column(name="TRANCHE_NUMBER")
	private BigDecimal trancheNumber;
	
	/** The fixed amount. */
	@Column(name="FIXED_AMOUNT")
	private BigDecimal fixedAmount;
	
	/** The request fixed amount. */
	@Column(name="REQUEST_FIXED_AMOUNT")
	private BigDecimal requestFixedAmount;
	
	/** The floating amount. */
	@Column(name="FLOATING_AMOUNT")
	private BigDecimal floatingAmount;
	
	/** The request floating amount. */
	@Column(name="REQUEST_FLOATING_AMOUNT")
	private BigDecimal requestFloatingAmount;
	
	/** The placement segment type. */
	@Column(name="PLACEMENT_SEGMENT_TYPE")
	private Integer placementSegmentType;
	
	@Lob()
	@Column(name="REOPEN_FILE")
	private byte[] reopenFile;
	
	@Column(name="REOPEN_FILE_NAME")
	private String reopenFileName;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PLACEMENT_SEGMENT_FK")
	private PlacementSegment placementSegment;
	
	//bi-directional many-to-one association to PlacementSegmentDetailHy
	/** The placement segment detail hys. */
	@OneToMany(mappedBy="placementSegmentHy")
	private List<PlacementSegmentDetailHy> placementSegmentDetailHys;

	//bi-directional many-to-one association to PlacementSegParticStrucHy
	/** The placement seg partic struc hys. */
	@OneToMany(mappedBy="placementSegmentHy")
	private List<PlacementSegParticStrucHy> placementSegParticStrucHys;

    /**
     * Instantiates a new placement segment hy.
     */
    public PlacementSegmentHy() {
    }

	/**
	 * Gets the id placement segment hys pk.
	 *
	 * @return the id placement segment hys pk
	 */
	public Long getIdPlacementSegmentHysPk() {
		return this.idPlacementSegmentHysPk;
	}

	/**
	 * Sets the id placement segment hys pk.
	 *
	 * @param idPlacementSegmentHysPk the new id placement segment hys pk
	 */
	public void setIdPlacementSegmentHysPk(Long idPlacementSegmentHysPk) {
		this.idPlacementSegmentHysPk = idPlacementSegmentHysPk;
	}

	/**
	 * Gets the action date.
	 *
	 * @return the action date
	 */
	public Date getActionDate() {
		return this.actionDate;
	}

	/**
	 * Sets the action date.
	 *
	 * @param actionDate the new action date
	 */
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 * Gets the action motive.
	 *
	 * @return the action motive
	 */
	public BigDecimal getActionMotive() {
		return this.actionMotive;
	}

	/**
	 * Sets the action motive.
	 *
	 * @param actionMotive the new action motive
	 */
	public void setActionMotive(BigDecimal actionMotive) {
		this.actionMotive = actionMotive;
	}

	/**
	 * Gets the amount to place.
	 *
	 * @return the amount to place
	 */
	public BigDecimal getAmountToPlace() {
		return this.amountToPlace;
	}

	/**
	 * Sets the amount to place.
	 *
	 * @param amountToPlace the new amount to place
	 */
	public void setAmountToPlace(BigDecimal amountToPlace) {
		this.amountToPlace = amountToPlace;
	}

	/**
	 * Gets the begining date.
	 *
	 * @return the begining date
	 */
	public Date getBeginingDate() {
		return this.beginingDate;
	}

	/**
	 * Sets the begining date.
	 *
	 * @param beginingDate the new begining date
	 */
	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	/**
	 * Gets the closing date.
	 *
	 * @return the closing date
	 */
	public Date getClosingDate() {
		return this.closingDate;
	}

	/**
	 * Sets the closing date.
	 *
	 * @param closingDate the new closing date
	 */
	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	/**
	 * Gets the id issuance code fk.
	 *
	 * @return the id issuance code fk
	 */
	public String getIdIssuanceCodeFk() {
		return this.idIssuanceCodeFk;
	}

	/**
	 * Sets the id issuance code fk.
	 *
	 * @param idIssuanceCodeFk the new id issuance code fk
	 */
	public void setIdIssuanceCodeFk(String idIssuanceCodeFk) {
		this.idIssuanceCodeFk = idIssuanceCodeFk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the other action motive.
	 *
	 * @return the other action motive
	 */
	public String getOtherActionMotive() {
		return this.otherActionMotive;
	}

	/**
	 * Sets the other action motive.
	 *
	 * @param otherActionMotive the new other action motive
	 */
	public void setOtherActionMotive(String otherActionMotive) {
		this.otherActionMotive = otherActionMotive;
	}

	/**
	 * Gets the placed amount.
	 *
	 * @return the placed amount
	 */
	public BigDecimal getPlacedAmount() {
		return this.placedAmount;
	}

	/**
	 * Sets the placed amount.
	 *
	 * @param placedAmount the new placed amount
	 */
	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	/**
	 * Gets the placement term.
	 *
	 * @return the placement term
	 */
	public BigDecimal getPlacementTerm() {
		return this.placementTerm;
	}

	/**
	 * Sets the placement term.
	 *
	 * @param placementTerm the new placement term
	 */
	public void setPlacementTerm(BigDecimal placementTerm) {
		this.placementTerm = placementTerm;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public BigDecimal getRequestType() {
		return this.requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(BigDecimal requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the state placement segment.
	 *
	 * @return the state placement segment
	 */
	public BigDecimal getStatePlacementSegment() {
		return this.statePlacementSegment;
	}

	/**
	 * Sets the state placement segment.
	 *
	 * @param statePlacementSegment the new state placement segment
	 */
	public void setStatePlacementSegment(BigDecimal statePlacementSegment) {
		this.statePlacementSegment = statePlacementSegment;
	}

	/**
	 * Gets the tranche number.
	 *
	 * @return the tranche number
	 */
	public BigDecimal getTrancheNumber() {
		return this.trancheNumber;
	}

	/**
	 * Sets the tranche number.
	 *
	 * @param trancheNumber the new tranche number
	 */
	public void setTrancheNumber(BigDecimal trancheNumber) {
		this.trancheNumber = trancheNumber;
	}

	/**
	 * Gets the confirm user.
	 *
	 * @return the confirm user
	 */
	public String getConfirmUser() {
		return confirmUser;
	}

	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the new confirm user
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * Gets the open date.
	 *
	 * @return the open date
	 */
	public Date getOpenDate() {
		return openDate;
	}

	/**
	 * Sets the open date.
	 *
	 * @param openDate the new open date
	 */
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the placement segment detail hys.
	 *
	 * @return the placement segment detail hys
	 */
	public List<PlacementSegmentDetailHy> getPlacementSegmentDetailHys() {
		return this.placementSegmentDetailHys;
	}

	/**
	 * Sets the placement segment detail hys.
	 *
	 * @param placementSegmentDetailHys the new placement segment detail hys
	 */
	public void setPlacementSegmentDetailHys(List<PlacementSegmentDetailHy> placementSegmentDetailHys) {
		this.placementSegmentDetailHys = placementSegmentDetailHys;
	}
	
	/**
	 * Gets the placement seg partic struc hys.
	 *
	 * @return the placement seg partic struc hys
	 */
	public List<PlacementSegParticStrucHy> getPlacementSegParticStrucHys() {
		return this.placementSegParticStrucHys;
	}

	/**
	 * Sets the placement seg partic struc hys.
	 *
	 * @param placementSegParticStrucHys the new placement seg partic struc hys
	 */
	public void setPlacementSegParticStrucHys(List<PlacementSegParticStrucHy> placementSegParticStrucHys) {
		this.placementSegParticStrucHys = placementSegParticStrucHys;
	}

	/**
	 * Gets the fixed amount.
	 *
	 * @return the fixed amount
	 */
	public BigDecimal getFixedAmount() {
		return fixedAmount;
	}

	/**
	 * Sets the fixed amount.
	 *
	 * @param fixedAmount the new fixed amount
	 */
	public void setFixedAmount(BigDecimal fixedAmount) {
		this.fixedAmount = fixedAmount;
	}

	/**
	 * Gets the request fixed amount.
	 *
	 * @return the request fixed amount
	 */
	public BigDecimal getRequestFixedAmount() {
		return requestFixedAmount;
	}

	/**
	 * Sets the request fixed amount.
	 *
	 * @param requestFixedAmount the new request fixed amount
	 */
	public void setRequestFixedAmount(BigDecimal requestFixedAmount) {
		this.requestFixedAmount = requestFixedAmount;
	}

	/**
	 * Gets the floating amount.
	 *
	 * @return the floating amount
	 */
	public BigDecimal getFloatingAmount() {
		return floatingAmount;
	}

	/**
	 * Sets the floating amount.
	 *
	 * @param floatingAmount the new floating amount
	 */
	public void setFloatingAmount(BigDecimal floatingAmount) {
		this.floatingAmount = floatingAmount;
	}

	/**
	 * Gets the request floating amount.
	 *
	 * @return the request floating amount
	 */
	public BigDecimal getRequestFloatingAmount() {
		return requestFloatingAmount;
	}

	/**
	 * Sets the request floating amount.
	 *
	 * @param requestFloatingAmount the new request floating amount
	 */
	public void setRequestFloatingAmount(BigDecimal requestFloatingAmount) {
		this.requestFloatingAmount = requestFloatingAmount;
	}

	/**
	 * Gets the placement segment type.
	 *
	 * @return the placement segment type
	 */
	public Integer getPlacementSegmentType() {
		return placementSegmentType;
	}


	public PlacementSegment getPlacementSegment() {
		return placementSegment;
	}

	public void setPlacementSegment(PlacementSegment placementSegment) {
		this.placementSegment = placementSegment;
	}

	/**
	 * Sets the placement segment type.
	 *
	 * @param placementSegmentType the new placement segment type
	 */
	public void setPlacementSegmentType(Integer placementSegmentType) {
		this.placementSegmentType = placementSegmentType;
	}

	public byte[] getReopenFile() {
		return reopenFile;
	}

	public void setReopenFile(byte[] reopenFile) {
		this.reopenFile = reopenFile;
	}

	public String getReopenFileName() {
		return reopenFileName;
	}

	public void setReopenFileName(String reopenFileName) {
		this.reopenFileName = reopenFileName;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("placementSegmentDetailHys", placementSegmentDetailHys);
        detailsMap.put("placementSegParticStrucHys", placementSegParticStrucHys);
		return detailsMap;
	}
}