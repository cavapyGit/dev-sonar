package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum SecuritieTermType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03/04/2014
 */
public enum SecurityTermType { 

	/** The long term ins. */
	LONG_TERM_INS(Integer.valueOf(402),"INSTRUMENTOS DE LARGO PLAZO",null),
	
	/** The short term ins. */
	SHORT_TERM_INS(Integer.valueOf(403),"INSTRUMENTOS DE CORTO PLAZO", Integer.valueOf(360)),
	
	/** The medium term ins. */
	MEDIUM_TERM_INS(Integer.valueOf(405),"INSTRUMENTOS DE MEDIANO PLAZO", Integer.valueOf(3600));
	

	/** The Constant lookup. */
	public static final Map<Integer,SecurityTermType> lookup=new HashMap<Integer, SecurityTermType>();
	
	/** The Constant list. */
	public static final List<SecurityTermType> list=new ArrayList<SecurityTermType>();

	static{
		for(SecurityTermType s:SecurityTermType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The max term days. */
	private Integer maxTermDays;
	
	/**
	 * Instantiates a new securitie term type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param maxTermDays the max term days
	 */
	private SecurityTermType(Integer code, String value,Integer maxTermDays){
		this.code=code;
		this.value=value;
		this.maxTermDays=maxTermDays;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie term type
	 */
	public static SecurityTermType get(Integer code) {
		return lookup.get(code);
	}
	
	public static SecurityTermType getByDays(Integer days) {
		Integer code=null;
		if(days<=SecurityTermType.SHORT_TERM_INS.getMaxTermDays()){
			code= SecurityTermType.SHORT_TERM_INS.getCode();
		}else if(days<=SecurityTermType.MEDIUM_TERM_INS.getMaxTermDays()){
			code= SecurityTermType.MEDIUM_TERM_INS.getCode();
		}else {
			code= SecurityTermType.LONG_TERM_INS.getCode();
		}
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the max term days.
	 *
	 * @return the max term days
	 */
	public Integer getMaxTermDays() {
		return maxTermDays;
	}

	/**
	 * Sets the max term days.
	 *
	 * @param maxTermDays the new max term days
	 */
	public void setMaxTermDays(Integer maxTermDays) {
		this.maxTermDays = maxTermDays;
	}	
	
	
	
}
