package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
//Tipo de emision --> 333

/**
 * The Enum SecurityStateType.
 */
public enum SecurityStateType {

	/** The registered. */
	REGISTERED(Integer.valueOf(131),"REGISTRADO"),
	
	/** The redeemed. */
	REDEEMED(Integer.valueOf(641),"REDIMIDO"),
	
	/** The unified. */
	UNIFIED(Integer.valueOf(642),"UNIFICADO"),
	
	/** The fused. */
	FUSED(Integer.valueOf(643),"FUSIONADO"),
	
	/** The escisionado. */
	ESCISIONADO(Integer.valueOf(644),"ESCISIONADO"),
	
	/** The excluded. */
	EXCLUDED(Integer.valueOf(833 ),"EXCLUIDO"),
	/** The blocked. */
	BLOCKED(Integer.valueOf(834),"BLOQUEADO"),
	
	EXPIRED(Integer.valueOf(2033 ),"VENCIDO"),
	
	SUSPENDED(Integer.valueOf(1372),"SUSPENDIDO"),
	
	GUARDA_EXCLUSIVE(Integer.valueOf(2842),"GUARDA EXCLUSIVA"),
	
	GUARDA_MIXTA(Integer.valueOf(2879),"GUARDA MIXTA"),
	
	RETIREMENT(Integer.valueOf(2848),"RETIRADO"),
	
	GUARDA_EXCLUSIVE_EXPIRATION(Integer.valueOf(2916),"GUARDA EXCLUSIVA VENCIDA"),
	
	;
	
	
	/** The Constant list. */
	public static final List<SecurityStateType> list=new ArrayList<SecurityStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SecurityStateType> lookup=new HashMap<Integer, SecurityStateType>();
	
	static {
		for(SecurityStateType e : SecurityStateType.values()){
			lookup.put(e.getCode(), e);
			list.add( e );
		}
	}
	
	/** The code. */
	private Integer  code;
	
	/** The value. */
	private String value;
	

	/**
	 * Instantiates a new security state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SecurityStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the security state type
	 */
	public static SecurityStateType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
