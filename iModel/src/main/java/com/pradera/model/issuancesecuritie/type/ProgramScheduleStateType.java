package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Master Id 369
public enum ProgramScheduleStateType {
	
	PENDING(Integer.valueOf(1350),"VIGENTE"), 
	EXPIRED(Integer.valueOf(1351),"VENCIDO"),
	REDEEMED(Integer.valueOf(2172),"REDIMIDO"),
	CANCELED(Integer.valueOf(1732),"CANCELADO");
	/*
	BLOCKED(Integer.valueOf(1352),"BLOQUEADO"),
	ISSUER_PAID(Integer.valueOf(1689),"PAGADO EMISOR"),
	*/
	
	public final static List<ProgramScheduleStateType> list=new ArrayList<ProgramScheduleStateType>();
	public static final Map<Integer, ProgramScheduleStateType> lookup = new HashMap<Integer, ProgramScheduleStateType>();
	
	private Integer code;
	private String value;
	
	static{
		for(ProgramScheduleStateType d : ProgramScheduleStateType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	

	private ProgramScheduleStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static ProgramScheduleStateType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}
	

	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

}
