package com.pradera.model.issuancesecuritie;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.SecurityMotiveBlockType;
import com.pradera.model.issuancesecuritie.type.SecurityMotiveUnblockType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the SECURITY_HISTORY_STATE database table.
 * 
 */
@Entity
@Table(name="SECURITY_HISTORY_STATE")
public class SecurityHistoryState implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SECURITY_HISTORY_STATE_IDSECHISTORYSTATEPK_GENERATOR", sequenceName="SQ_ID_SEC_HISTORY_STATE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_HISTORY_STATE_IDSECHISTORYSTATEPK_GENERATOR")
	@Column(name="ID_SEC_HISTORY_STATE_PK")
	private long idSecHistoryStatePk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	private Integer motive;
	
	@Column(name="COMMENTS")
	private String comments;

	@Column(name="NEW_STATE")
	private Integer newState;

	@Column(name="OLD_STATE")
	private Integer oldState;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_STATE_DATE")
	private Date updateStateDate;

	//bi-directional many-to-one association to Security
    @ManyToOne
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

    public SecurityHistoryState() {
    }

	public long getIdSecHistoryStatePk() {
		return this.idSecHistoryStatePk;
	}

	public void setIdSecHistoryStatePk(long idSecHistoryStatePk) {
		this.idSecHistoryStatePk = idSecHistoryStatePk;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getMotive() {
		return this.motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public Integer getNewState() {
		return this.newState;
	}

	public void setNewState(Integer newState) {
		this.newState = newState;
	}

	public Integer getOldState() {
		return this.oldState;
	}

	public void setOldState(Integer oldState) {
		this.oldState = oldState;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Date getUpdateStateDate() {
		return this.updateStateDate;
	}

	public void setUpdateStateDate(Date updateStateDate) {
		this.updateStateDate = updateStateDate;
	}

	public Security getSecurity() {
		return this.security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public boolean isOtherMotive(){
		if(isOtherMotiveBlock() || isOtherMotiveUnblock()){
			return true;
		}
		return false;
	}
	
	public boolean isOtherMotiveBlock(){
		if(SecurityMotiveBlockType.OTHER.getCode().equals( getMotive() )){
			return true;
		}
		return false;
	}
	
	public boolean isOtherMotiveUnblock(){
		if(SecurityMotiveUnblockType.OTHER.getCode().equals( getMotive() )){
			return true;
		}
		return false;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}
	
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
	
}