package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum SecuritieType.
 */
public enum SecuritySourceType {


	NATIONAL(Integer.valueOf(532),"NACIONAL"),	
	FOREIGN(Integer.valueOf(533),"EXTRANJERO");
	
	/** The Constant lookup. */
	public static final Map<Integer,SecuritySourceType> lookup=new HashMap<Integer, SecuritySourceType>();
	
	/** The Constant list. */
	public static final List<SecuritySourceType> list=new ArrayList<SecuritySourceType>();

	static{
		for(SecuritySourceType s:SecuritySourceType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SecuritySourceType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static SecuritySourceType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
	
}
