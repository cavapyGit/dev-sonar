package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the SECURITY_HISTORY_BALANCE database table.
 * 
 */
@Entity
@Table(name="SECURITY_HISTORY_BALANCE")
public class SecurityHistoryBalance implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SECURITY_HISTORY_BALANCE_IDSECHISTORYBALANCEPK_GENERATOR", sequenceName="SQ_ID_SEC_HISTORY_BALANCE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_HISTORY_BALANCE_IDSECHISTORYBALANCEPK_GENERATOR")
	@Column(name="ID_SEC_HISTORY_BALANCE_PK")
	private Long idSecHistoryBalancePk;

	@Column(name="AMORTIZATION_AMOUNT")
	private BigDecimal amortizationAmount;

	@Column(name="DESMATERIALIZED_BALANCE")
	private BigDecimal desmaterializedBalance;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="NOMINAL_VALUE")
	private BigDecimal nominalValue;

	@Column(name="PHYSICAL_BALANCE")
	private BigDecimal physicalBalance;

	@Column(name="PLACED_AMOUNT")
	private BigDecimal placedAmount;

	@Column(name="PLACED_BALANCE")
	private BigDecimal placedBalance;

	@Column(name="SHARE_BALANCE")
	private BigDecimal shareBalance;

	@Column(name="SHARE_CAPITAL")
	private BigDecimal shareCapital;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_BALANCE_DATE")
	private Date updateBalanceDate;

	//bi-directional many-to-one association to Security
    @ManyToOne
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

    public SecurityHistoryBalance() {
    }

	public Long getIdSecHistoryBalancePk() {
		return this.idSecHistoryBalancePk;
	}

	public void setIdSecHistoryBalancePk(Long idSecHistoryBalancePk) {
		this.idSecHistoryBalancePk = idSecHistoryBalancePk;
	}

	public BigDecimal getAmortizationAmount() {
		return this.amortizationAmount;
	}

	public void setAmortizationAmount(BigDecimal amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	public BigDecimal getDesmaterializedBalance() {
		return this.desmaterializedBalance;
	}

	public void setDesmaterializedBalance(BigDecimal desmaterializedBalance) {
		this.desmaterializedBalance = desmaterializedBalance;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getNominalValue() {
		return this.nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public BigDecimal getPhysicalBalance() {
		return this.physicalBalance;
	}

	public void setPhysicalBalance(BigDecimal physicalBalance) {
		this.physicalBalance = physicalBalance;
	}

	public BigDecimal getPlacedAmount() {
		return this.placedAmount;
	}

	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	public BigDecimal getPlacedBalance() {
		return this.placedBalance;
	}

	public void setPlacedBalance(BigDecimal placedBalance) {
		this.placedBalance = placedBalance;
	}

	public BigDecimal getShareBalance() {
		return this.shareBalance;
	}

	public void setShareBalance(BigDecimal shareBalance) {
		this.shareBalance = shareBalance;
	}

	public BigDecimal getShareCapital() {
		return this.shareCapital;
	}

	public void setShareCapital(BigDecimal shareCapital) {
		this.shareCapital = shareCapital;
	}

	public Date getUpdateBalanceDate() {
		return this.updateBalanceDate;
	}

	public void setUpdateBalanceDate(Date updateBalanceDate) {
		this.updateBalanceDate = updateBalanceDate;
	}

	public Security getSecurity() {
		return this.security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}