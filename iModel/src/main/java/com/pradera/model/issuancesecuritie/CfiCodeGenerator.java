package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

// TODO: Auto-generated Javadoc
/**
 * The Class CfiCodeGenerator.
 */
@Entity
@Table(name = "CFI_CODE_GENERATOR")
public class CfiCodeGenerator implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id cfi code pk. */
	@Id
	@Column(name="ID_CFI_CODE_PK")
	private Integer idCfiCodePk;
	
	/** The security class. */
	@Column(name="SECURITY_CLASS")
	private Integer securityClass;
	
	/** The state security. */
	//@Transient
	//private Integer stateSecurity;
	
	/** The security term. */
	@Column(name="SECURITY_TERM")
	private Integer securityTerm;
	
	/** The interest type. */
	@Column(name="INTEREST_TYPE")
	private Integer interestType;

	/** The instrument type. */
	@Column(name="INSTRUMENT_TYPE")
	private Integer instrumentType;
	
	/** The economic sector. */
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;
	
	/** The ind early redemption. */
	@Column(name="IND_EARLY_REDEMPTION")
	private Integer indEarlyRedemption;
	
	/** The capital payment modality. */
	@Column(name="CAPITAL_PAYMENT_MODALITY")
	private Integer capitalPaymentModality;
	
	/** The security class ref. */
	@Column(name="SECURITY_CLASS_REF")
	private String securityClassRef;
	
	/** The id group fk. */
	@Column(name="ID_GROUP_FK")
	private String idGroupFk;
	
	/** The id category fk. */
	@Column(name="ID_CATEGORY_FK")
	private String idCategoryFk;
	
	/** The id attribute_1_fk. */
	@Column(name="ID_ATTRIBUTE_1_FK")
	private String idAttribute_1_fk;
	
	/** The id attribute_2_fk. */
	@Column(name="ID_ATTRIBUTE_2_FK")
	private String idAttribute_2_fk;
	
	/** The id attribute_3_fk. */
	@Column(name="ID_ATTRIBUTE_3_FK")
	private String idAttribute_3_fk;
	
	/** The id attribute_4_fk. */
	@Column(name="ID_ATTRIBUTE_4_FK")
	private String idAttribute_4_fk;
	
	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The last modify date. */
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/**
	 * Instantiates a new cfi code generator.
	 */
	public CfiCodeGenerator() {
		super();
	}

	/**
	 * Gets the id cfi code pk.
	 *
	 * @return the id cfi code pk
	 */
	public Integer getIdCfiCodePk() {
		return idCfiCodePk;
	}

	/**
	 * Sets the id cfi code pk.
	 *
	 * @param idCfiCodePk the new id cfi code pk
	 */
	public void setIdCfiCodePk(Integer idCfiCodePk) {
		this.idCfiCodePk = idCfiCodePk;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the security term.
	 *
	 * @return the security term
	 */
	public Integer getSecurityTerm() {
		return securityTerm;
	}

	/**
	 * Sets the security term.
	 *
	 * @param securityTerm the new security term
	 */
	public void setSecurityTerm(Integer securityTerm) {
		this.securityTerm = securityTerm;
	}

	/**
	 * Gets the interest type.
	 *
	 * @return the interest type
	 */
	public Integer getInterestType() {
		return interestType;
	}

	/**
	 * Sets the interest type.
	 *
	 * @param interestType the new interest type
	 */
	public void setInterestType(Integer interestType) {
		this.interestType = interestType;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the ind early redemption.
	 *
	 * @return the ind early redemption
	 */
	public Integer getIndEarlyRedemption() {
		return indEarlyRedemption;
	}

	/**
	 * Sets the ind early redemption.
	 *
	 * @param indEarlyRedemption the new ind early redemption
	 */
	public void setIndEarlyRedemption(Integer indEarlyRedemption) {
		this.indEarlyRedemption = indEarlyRedemption;
	}

	public Integer getCapitalPaymentModality() {
		return capitalPaymentModality;
	}

	public void setCapitalPaymentModality(Integer capitalPaymentModality) {
		this.capitalPaymentModality = capitalPaymentModality;
	}

	/**
	 * Gets the security class ref.
	 *
	 * @return the security class ref
	 */
	public String getSecurityClassRef() {
		return securityClassRef;
	}

	/**
	 * Sets the security class ref.
	 *
	 * @param securityClassRef the new security class ref
	 */
	public void setSecurityClassRef(String securityClassRef) {
		this.securityClassRef = securityClassRef;
	}

	/**
	 * Gets the id group fk.
	 *
	 * @return the id group fk
	 */
	public String getIdGroupFk() {
		return idGroupFk;
	}

	/**
	 * Sets the id group fk.
	 *
	 * @param idGroupFk the new id group fk
	 */
	public void setIdGroupFk(String idGroupFk) {
		this.idGroupFk = idGroupFk;
	}

	/**
	 * Gets the id category fk.
	 *
	 * @return the id category fk
	 */
	public String getIdCategoryFk() {
		return idCategoryFk;
	}

	/**
	 * Sets the id category fk.
	 *
	 * @param idCategoryFk the new id category fk
	 */
	public void setIdCategoryFk(String idCategoryFk) {
		this.idCategoryFk = idCategoryFk;
	}

	/**
	 * Gets the id attribute_1_fk.
	 *
	 * @return the id attribute_1_fk
	 */
	public String getIdAttribute_1_fk() {
		return idAttribute_1_fk;
	}

	/**
	 * Sets the id attribute_1_fk.
	 *
	 * @param idAttribute_1_fk the new id attribute_1_fk
	 */
	public void setIdAttribute_1_fk(String idAttribute_1_fk) {
		this.idAttribute_1_fk = idAttribute_1_fk;
	}

	/**
	 * Gets the id attribute_2_fk.
	 *
	 * @return the id attribute_2_fk
	 */
	public String getIdAttribute_2_fk() {
		return idAttribute_2_fk;
	}

	/**
	 * Sets the id attribute_2_fk.
	 *
	 * @param idAttribute_2_fk the new id attribute_2_fk
	 */
	public void setIdAttribute_2_fk(String idAttribute_2_fk) {
		this.idAttribute_2_fk = idAttribute_2_fk;
	}

	/**
	 * Gets the id attribute_3_fk.
	 *
	 * @return the id attribute_3_fk
	 */
	public String getIdAttribute_3_fk() {
		return idAttribute_3_fk;
	}

	/**
	 * Sets the id attribute_3_fk.
	 *
	 * @param idAttribute_3_fk the new id attribute_3_fk
	 */
	public void setIdAttribute_3_fk(String idAttribute_3_fk) {
		this.idAttribute_3_fk = idAttribute_3_fk;
	}

	/**
	 * Gets the id attribute_4_fk.
	 *
	 * @return the id attribute_4_fk
	 */
	public String getIdAttribute_4_fk() {
		return idAttribute_4_fk;
	}

	/**
	 * Sets the id attribute_4_fk.
	 *
	 * @param idAttribute_4_fk the new id attribute_4_fk
	 */
	public void setIdAttribute_4_fk(String idAttribute_4_fk) {
		this.idAttribute_4_fk = idAttribute_4_fk;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}	
	
	@Override
	public String toString(){
		return "\nClass : "+this.securityClass+
				"\nSecurityTerm :"+this.getSecurityTerm()+
				"\nInterestType : "+this.interestType+
				"\nInstrumentType :"+this.instrumentType;
	}
	
}
