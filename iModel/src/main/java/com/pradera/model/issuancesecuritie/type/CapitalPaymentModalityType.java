package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum CapitalPaymentModalityType.
 */
public enum CapitalPaymentModalityType {
	
	/** The proporcional. */
	PARTIAL(Integer.valueOf(544),"PARCIAL","1"), 
	
	/** The no proporcional. */
	AT_MATURITY(Integer.valueOf(545),"AL VENCIMIENTO","2");
	

	/** The Constant list. */
	public final static List<CapitalPaymentModalityType> list=new ArrayList<CapitalPaymentModalityType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, CapitalPaymentModalityType> lookup = new HashMap<Integer, CapitalPaymentModalityType>();
	

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The code cd. */
	private String codeCd;
	
	static{
		for(CapitalPaymentModalityType d : CapitalPaymentModalityType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	

	/**
	 * Instantiates a new amortization factor type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 */
	private CapitalPaymentModalityType(Integer code, String value,String codeCd){
		this.code=code;
		this.value=value;
		this.codeCd=codeCd;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the amortization factor type
	 */
	public static CapitalPaymentModalityType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}


	/**
	 * Gets the code cd.
	 *
	 * @return the code cd
	 */
	public String getCodeCd() {
		return codeCd;
	}

	
}
