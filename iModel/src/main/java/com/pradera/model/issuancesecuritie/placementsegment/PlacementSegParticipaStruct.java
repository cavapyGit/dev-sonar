package com.pradera.model.issuancesecuritie.placementsegment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PLACEMENT_SEG_PARTICIPA_STRUCT database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/05/2013
 */
@Entity
@Table(name="PLACEMENT_SEG_PARTICIPA_STRUCT")
public class PlacementSegParticipaStruct implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id plac seg participa struc pk. */
	@Id
	@SequenceGenerator(name="SQ_PLACEMENT_SEG_PART_ESTRUCT_PK", sequenceName="SQ_ID_PLAC_SEG_PAR_STRUCT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_PLACEMENT_SEG_PART_ESTRUCT_PK")
	@Column(name="ID_PLAC_SEG_PARTICIPA_STRUC_PK")
	private Long idPlacSegParticipaStrucPk;

	/** The description. */
	@Transient
	private String description;

//	@Column(name="ID_PARTICIPANT_FK")
	/** The participant. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK",referencedColumnName="ID_PARTICIPANT_PK")
	private Participant participant;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state plac seg participant. */
	@Column(name="STATE_PLAC_SEG_PARTICIPANT")
	private Integer statePlacSegParticipant;
	
	/** The issuer account. */
	@Transient
	private HolderAccount issuerAccount;

	//bi-directional many-to-one association to PlacementSegment
	/** The placement segment. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PLACEMENT_SEGMENT_FK")
	private PlacementSegment placementSegment;
	
	/** The holder account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

    /**
     * Instantiates a new placement seg participa struct.
     */
    public PlacementSegParticipaStruct() {
    }

	/**
	 * Gets the id plac seg participa struc pk.
	 *
	 * @return the id plac seg participa struc pk
	 */
	public Long getIdPlacSegParticipaStrucPk() {
		return this.idPlacSegParticipaStrucPk;
	}

	/**
	 * Sets the id plac seg participa struc pk.
	 *
	 * @param idPlacSegParticipaStrucPk the new id plac seg participa struc pk
	 */
	public void setIdPlacSegParticipaStrucPk(Long idPlacSegParticipaStrucPk) {
		this.idPlacSegParticipaStrucPk = idPlacSegParticipaStrucPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the placement segment.
	 *
	 * @return the placement segment
	 */
	public PlacementSegment getPlacementSegment() {
		return this.placementSegment;
	}

	/**
	 * Sets the placement segment.
	 *
	 * @param placementSegment the new placement segment
	 */
	public void setPlacementSegment(PlacementSegment placementSegment) {
		this.placementSegment = placementSegment;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param panticipant the new participant
	 */
	public void setParticipant(Participant panticipant) {
		this.participant = panticipant;
	}

	/**
	 * Gets the state plac seg participant.
	 *
	 * @return the state plac seg participant
	 */
	public Integer getStatePlacSegParticipant() {
		return statePlacSegParticipant;
	}

	/**
	 * Sets the state plac seg participant.
	 *
	 * @param statePlacSegParticipant the new state plac seg participant
	 */
	public void setStatePlacSegParticipant(Integer statePlacSegParticipant) {
		this.statePlacSegParticipant = statePlacSegParticipant;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the issuer account.
	 *
	 * @return the issuer account
	 */
	public HolderAccount getIssuerAccount() {
		return issuerAccount;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Sets the issuer account.
	 *
	 * @param issuerAccount the new issuer account
	 */
	public void setIssuerAccount(HolderAccount issuerAccount) {
		this.issuerAccount = issuerAccount;
	}
}