package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SecurityRemovedStateType {

	REGISTRADO(Integer.valueOf(2516),"ESTADO REGISTRADO DE UNA SOLICITUD DE ELIMINACION DE VALOR"),
	APROBADO(Integer.valueOf(2517),"ESTADO APROBADO DE UNA SOLICITUD DE ELIMINACION DE VALOR"),
	ANULADO(Integer.valueOf(2518),"ESTADO ANULADO DE UNA SOLICITUD DE ELIMINACION DE VALOR"),
	CONFIRMADO(Integer.valueOf(2519),"ESTADO CONFIRMADO DE UNA SOLICITUD DE ELIMINACION DE VALOR"),
	RECHAZADO(Integer.valueOf(2520),"ESTADO RECHAZADO DE UNA SOLICITUD DE ELIMINACION DE VALOR"),
	
	;
	
	/** The Constant list. */
	public static final List<SecurityRemovedStateType> list=new ArrayList<SecurityRemovedStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SecurityRemovedStateType> lookup=new HashMap<Integer, SecurityRemovedStateType>();
	
	static {
		for(SecurityRemovedStateType e : SecurityRemovedStateType.values()){
			lookup.put(e.getCode(), e);
			list.add( e );
		}
	}
	
	/** The code. */
	private Integer  code;
	
	/** The value. */
	private String value;
	
	private SecurityRemovedStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static SecurityRemovedStateType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getValue() {
		return value;
	}
}
