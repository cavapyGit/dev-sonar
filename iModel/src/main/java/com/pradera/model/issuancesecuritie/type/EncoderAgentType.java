package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum EncoderAgentType {
	
	CAVAPY(Integer.valueOf(2941),"CAVAPY"),
	BVA(Integer.valueOf(2942),"BOLSA VALORES DE ASUNCIÓN"),
	INTERNATIONAL(Integer.valueOf(2943),"INTERNACIONAL"),
	;
	
	public final static List<EncoderAgentType> list=new ArrayList<EncoderAgentType>();
	public static final Map<Integer, EncoderAgentType> lookup = new HashMap<Integer, EncoderAgentType>();
	
	private Integer code;
	private String value;
	
	static{
		for(EncoderAgentType d : EncoderAgentType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	private EncoderAgentType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static EncoderAgentType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
