package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Master Id 368
public enum RequestStateType {
	
	CONFIRMED(Integer.valueOf(1347),"CONFIRMADO"), 
	REJECTED(Integer.valueOf(1348),"RECHAZADO"),
	REGISTERED(Integer.valueOf(1349),"REGISTRADO"),
	ANNULED(Integer.valueOf(1510),"ANULADO"),
	APPROVED(Integer.valueOf(1511),"APROBADO");
	
	public final static List<RequestStateType> list=new ArrayList<RequestStateType>();
	public static final Map<Integer, RequestStateType> lookup = new HashMap<Integer, RequestStateType>();
	
	private Integer code;
	private String value;
	
	static{
		for(RequestStateType d : RequestStateType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	

	private RequestStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static RequestStateType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}
	

	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

}
