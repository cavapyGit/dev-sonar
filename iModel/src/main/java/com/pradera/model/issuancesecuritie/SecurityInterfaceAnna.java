package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;




/**
 * The persistent class for the SECURITY_INTERFACE_ANNA database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/06/2015
 */
@Entity
@Table(name = "SECURITY_INTERFACE_ANNA")
public class SecurityInterfaceAnna implements Serializable, Auditable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id Security Interface Anna Pk. */
	@Id
	@Column(name = "ID_SEC_INTERFACE_ANNA_PK")	
	@SequenceGenerator(name="SECURITY_INTERFACE_ANNA_GENERATION", sequenceName="SQ_ID_SEC_INTERFACE_ANNA_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_INTERFACE_ANNA_GENERATION")
	@NotNull
	private Long idSecurityInterfaceAnnaPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_SECURITY_CODE_FK")
	private Security security;

	
	@Column(name = "ID_CORRELATIVE_ANNA_CODE")
	private String correlativeAnnaCode;

	@Column(name = "NAME_FILE_INTERFACE")
	private String nameFileInterface;
	
	/** The Generation date . */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "GENERATION_DATE")
	private Date generationDate;

	/** The Generation User */
	@Column(name="GENERATION_USER")
	private String generationUser;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	
	
	/**
	 * @return the idSecurityInterfaceAnnaPk
	 */
	public Long getIdSecurityInterfaceAnnaPk() {
		return idSecurityInterfaceAnnaPk;
	}

	/**
	 * @param idSecurityInterfaceAnnaPk the idSecurityInterfaceAnnaPk to set
	 */
	public void setIdSecurityInterfaceAnnaPk(Long idSecurityInterfaceAnnaPk) {
		this.idSecurityInterfaceAnnaPk = idSecurityInterfaceAnnaPk;
	}



	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the correlativeAnnaCode
	 */
	public String getCorrelativeAnnaCode() {
		return correlativeAnnaCode;
	}

	/**
	 * @param correlativeAnnaCode the correlativeAnnaCode to set
	 */
	public void setCorrelativeAnnaCode(String correlativeAnnaCode) {
		this.correlativeAnnaCode = correlativeAnnaCode;
	}

	/**
	 * @return the nameFileInterface
	 */
	public String getNameFileInterface() {
		return nameFileInterface;
	}

	/**
	 * @param nameFileInterface the nameFileInterface to set
	 */
	public void setNameFileInterface(String nameFileInterface) {
		this.nameFileInterface = nameFileInterface;
	}

	/**
	 * @return the generationDate
	 */
	public Date getGenerationDate() {
		return generationDate;
	}

	/**
	 * @param generationDate the generationDate to set
	 */
	public void setGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
	}

	/**
	 * @return the generationUser
	 */
	public String getGenerationUser() {
		return generationUser;
	}

	/**
	 * @param generationUser the generationUser to set
	 */
	public void setGenerationUser(String generationUser) {
		this.generationUser = generationUser;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
            generationUser = loggerUser.getUserName();
            generationDate = loggerUser.getAuditTime();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
	
}