package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.issuancesecuritie.type.IssuanceCertificateStateType;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
/**
 * The persistent class for the ISSUANCE_CERTIFICATE_AUX database table.
 * 
 */
@Entity
@Table(name="ISSUANCE_CERTIFICATE_AUX")
public class IssuanceCertificateAux implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id					     
	@SequenceGenerator(name="CERTIFICATE_ISSUANCE_AUX_IDCERTIFICATEPK_GENERATOR", sequenceName="SQ_ID_CERTIFICATE_AUX_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CERTIFICATE_ISSUANCE_AUX_IDCERTIFICATEPK_GENERATOR")
	@Column(name="ID_CERTIFICATE_PK")
	private Integer idCertificatePk;

    @Lob()
	@Column(name="CERTIFICATE_FILE")
    @NotNull
	private byte[] certificateFile;

	@Column(name="CERTIFICATE_NUMBER")
	@NotNull
	private String certificateNumber;

	@Column(name="CERTIFICATE_TYPE")
	private Integer certificateType;

	//bi-directional many-to-one association to Holder
    @ManyToOne
	@JoinColumn(name="ID_HOLDER_FK")
    @NotNull
	private Holder holder;

	//bi-directional many-to-one association to Issuance
    @ManyToOne
	@JoinColumn(name="ID_ISSUANCE_CODE_FK")
    @NotNull
	private IssuanceAux issuance;

	//bi-directional many-to-one association to CertificateIssuance
    @ManyToOne
	@JoinColumn(name="ID_SOURCE_CERTIFICATE_FK")
	private IssuanceCertificate issuanceCertificate;
    
	//bi-directional many-to-one association to CertificateIssuance
	@OneToMany(mappedBy="issuanceCertificate")
	private List<IssuanceCertificate> issuanceCertificates;

	@Column(name="ISSUANCE_AMOUNT")
	@NotNull
	private BigDecimal issuanceAmount;;

	@Column(name="ISSUANCE_BALANCE")
	private Double issuanceBalance;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="ISSUANCE_DATE")
	private Date issuanceDate;

	@Column(name="LAST_MODIFY_APP")
	@NotNull
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
    @NotNull
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	@NotNull
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	@NotNull
	private String lastModifyUser;

	@Column(name="NOMINAL_VALUE")
	private Double nominalValue;

	@Temporal( TemporalType.TIMESTAMP)
    @Column(name="REGISTRY_DATE")
    @NotNull
    private Date registryDate;

    @Column(name="REGISTRY_USER")
    @NotNull
    private String registryUser;

	@Column(name="SERIAL_NUMBER")
	@NotNull
	private String serialNumber;

	@Column(name="STATE_CERTIFICATE")
	@NotNull
	private Integer stateCertificate;
	
	@Column(name="FILENAME")
	@NotNull
	private String fileName;
	
	@Transient
	private String fieldOnDtb;

    public IssuanceCertificateAux() {}

	public Integer getIdCertificatePk() {
		return idCertificatePk;
	}

	public void setIdCertificatePk(Integer idCertificatePk) {
		this.idCertificatePk = idCertificatePk;
	}

	public byte[] getCertificateFile() {
		return certificateFile;
	}

	public void setCertificateFile(byte[] certificateFile) {
		this.certificateFile = certificateFile;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public Integer getCertificateType() {
		return certificateType;
	}

	public void setCertificateType(Integer certificateType) {
		this.certificateType = certificateType;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public IssuanceAux getIssuance() {
		return issuance;
	}

	public void setIssuance(IssuanceAux issuance) {
		this.issuance = issuance;
	}

	public IssuanceCertificate getIssuanceCertificate() {
		return issuanceCertificate;
	}

	public void setIssuanceCertificate(IssuanceCertificate issuanceCertificate) {
		this.issuanceCertificate = issuanceCertificate;
	}

	public List<IssuanceCertificate> getIssuanceCertificates() {
		return issuanceCertificates;
	}

	public void setIssuanceCertificates(
			List<IssuanceCertificate> issuanceCertificates) {
		this.issuanceCertificates = issuanceCertificates;
	}

	public BigDecimal getIssuanceAmount() {
		return issuanceAmount;
	}

	public void setIssuanceAmount(BigDecimal issuanceAmount) {
		this.issuanceAmount = issuanceAmount;
	}

	public Double getIssuanceBalance() {
		return issuanceBalance;
	}

	public void setIssuanceBalance(Double issuanceBalance) {
		this.issuanceBalance = issuanceBalance;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Double getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(Double nominalValue) {
		this.nominalValue = nominalValue;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getStateCertificate() {
		return stateCertificate;
	}

	public void setStateCertificate(Integer stateCertificate) {
		this.stateCertificate = stateCertificate;
	}

    public String getDescriptionState(){
    	if(this.stateCertificate!=null){
    		return IssuanceCertificateStateType.get(stateCertificate).getValue();
    	}
    	return "";
    }

    public String getFileName() {
		return fileName;
	}

	public void setFileName(String filename) {
		this.fileName = filename;
	}

	@Override
	public String toString() {
		return "IssuanceCertificate [idCertificatePk=" + idCertificatePk
				+ ", certificateFile=" + Arrays.toString(certificateFile)
				+ ", certificateNumber=" + certificateNumber
				+ ", certificateType=" + certificateType + ", holder=" + holder
				+ ", issuance=" + issuance + ", issuanceCertificate="
				+ issuanceCertificate + ", issuanceCertificates="
				+ issuanceCertificates + ", issuanceAmount=" + issuanceAmount
				+ ", issuanceBalance=" + issuanceBalance + ", issuanceDate="
				+ issuanceDate + ", lastModifyApp=" + lastModifyApp
				+ ", lastModifyDate=" + lastModifyDate + ", lastModifyIp="
				+ lastModifyIp + ", lastModifyUser=" + lastModifyUser
				+ ", nominalValue=" + nominalValue + ", registryDate="
				+ registryDate + ", registryUser=" + registryUser
				+ ", serialNumber=" + serialNumber + ", stateCertificate="
				+ stateCertificate + "]";
	}

	public String getFieldOnDtb() {
		fieldOnDtb= "IssuanceCertificate [idCertificatePk=" + idCertificatePk
				+ ", certificateNumber=" + certificateNumber
				+ ", issuanceAmount=" + issuanceAmount + ", serialNumber="
				+ serialNumber + ", stateCertificate=" + stateCertificate + "]";
		return fieldOnDtb;
	}

	public void setFieldOnDtb(String fieldOnDtb) {
		this.fieldOnDtb = fieldOnDtb;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}