package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Enum SecurityInversionistStateType.
 */
public enum SecurityInversionistStateType {

	
	/** The registered. */
	REGISTERED(Integer.valueOf(637),"REGISTRADO"),
	
	/** The blocked. */
	BLOCKED(Integer.valueOf(638),"BLOQUEADO");
	

	/** The Constant lookup. */
	public static final Map<Integer,SecurityInversionistStateType> lookup=new HashMap<Integer, SecurityInversionistStateType>();
	
	/** The Constant list. */
	public static final List<SecurityInversionistStateType> list=new ArrayList<SecurityInversionistStateType>();

	static{
		for(SecurityInversionistStateType s:SecurityInversionistStateType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	

	/**
	 * Instantiates a new security inversionist state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SecurityInversionistStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the security inversionist state type
	 */
	public static SecurityInversionistStateType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
	
}
