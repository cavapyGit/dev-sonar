package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum IssuanceStateType.
 */
public enum IssuerStateType {
	
	/** The authorized. */
	REGISTERED(Integer.valueOf(578),"REGISTRADO"),
	
	/** The expired. */
	BLOCKED(Integer.valueOf(579),"BLOQUEADO"),
	
	/** The expired. */
	PENDING(Integer.valueOf(2650),"PENDIENTE");
	
//	/** The canceled. */
//	CANCELED(Integer.valueOf(579),"ANULADO");
	
	/** The Constant list. */
	private static final List<IssuerStateType> list=new ArrayList<IssuerStateType>();
	
	/** The Constant map. */
	public static final Map<Integer, IssuerStateType> lookup=new HashMap<Integer, IssuerStateType>();
	
	static{
		for(IssuerStateType issu : IssuerStateType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new issuance state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuerStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static IssuerStateType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
}