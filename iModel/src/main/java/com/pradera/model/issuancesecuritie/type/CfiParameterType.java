package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum CfiParameterType {
	
	ATTRIBUTE1(Integer.valueOf(464),"ATRIBUTO 1","1"), 
	ATTRIBUTE2(Integer.valueOf(465),"ATRIBUTO 2","2"), 
	ATTRIBUTE3(Integer.valueOf(466),"ATRIBUTO 3","3"), 
	ATTRIBUTE4(Integer.valueOf(467),"ATRIBUTO 4","4");
	
	public final static List<CfiParameterType> list=new ArrayList<CfiParameterType>();
	
	public static final Map<Integer, CfiParameterType> lookup = new HashMap<Integer, CfiParameterType>();
	

	private Integer code;
	private String value;
	private String codeCd;
	
	static{
		for(CfiParameterType d : CfiParameterType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	

	private CfiParameterType(Integer code, String value,String codeCd){
		this.code=code;
		this.value=value;
		this.codeCd=codeCd;
	}
	
	public static CfiParameterType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	public String getCodeCd() {
		return codeCd;
	}

	
}
