package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.generalparameter.type.CurrencyType;

// TODO: Auto-generated Javadoc
/**
 * The Enum IssuanceStateType.
 */
public enum IssuanceStateType {
	
	/** The authorized. */
	AUTHORIZED(Integer.valueOf(128),"REGISTRADO"),
	
	/** The expired. */
	EXPIRED(Integer.valueOf(445),"VENCIDO"),
	
	/** The suspendido. */
	SUSPENDED(Integer.valueOf(1205),"SUSPENDIDO");
	
	
	/** The Constant list. */
	private static final List<IssuanceStateType> list=new ArrayList<IssuanceStateType>();
	
	/** The Constant map. */
	private static final Map<Integer, IssuanceStateType> lookup=new HashMap<Integer, IssuanceStateType>();
	
	static{
		for(IssuanceStateType issu : IssuanceStateType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new issuance state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuanceStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static IssuanceStateType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
	
}
