package com.pradera.model.issuancesecuritie.placementsegment.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum SecuritieType.
 */
public enum PlacementSegmentMotiveRejectType {

	REQUEST_FROM_ISSUER(Integer.valueOf(851),"SOLICITUD DE EMISOR"),
	NOT_EXIST_INFORMATIOn(Integer.valueOf(852),"FALTA INFORMACION"),
	REQUEST_FROM_CEVALDOM(Integer.valueOf(853),"SOLICITUD DE CEVALDOM"),
	OTHER(Integer.valueOf(1000),"OTROS"),;
	
	/** The Constant lookup. */
	public static final Map<Integer,PlacementSegmentMotiveRejectType> lookup=new HashMap<Integer, PlacementSegmentMotiveRejectType>();
	
	/** The Constant list. */
	public static final List<PlacementSegmentMotiveRejectType> list=new ArrayList<PlacementSegmentMotiveRejectType>();

	static{
		for(PlacementSegmentMotiveRejectType s:PlacementSegmentMotiveRejectType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PlacementSegmentMotiveRejectType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static PlacementSegmentMotiveRejectType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
	
}
