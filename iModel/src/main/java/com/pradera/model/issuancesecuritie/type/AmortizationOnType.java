package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Master Id 247
public enum AmortizationOnType {
	
	NOMINAL_VALUE(Integer.valueOf(570),"VALOR NOMINAL"), 
	CAPITAL(Integer.valueOf(571),"CAPITAL");
	
	public final static List<AmortizationOnType> list=new ArrayList<AmortizationOnType>();
	public static final Map<Integer, AmortizationOnType> lookup = new HashMap<Integer, AmortizationOnType>();
	
	private Integer code;
	private String value;
	
	static{
		for(AmortizationOnType d : AmortizationOnType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	

	private AmortizationOnType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static AmortizationOnType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}
	

	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

}
