package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

public enum CalendarMonthType  {
	  

    _30(Integer.valueOf(1577),"30",30), 
	ACTUAL(Integer.valueOf(1578),"ACTUAL",null);
    
	public final static List<CalendarMonthType> list=new ArrayList<CalendarMonthType>();
	public static final Map<Integer, CalendarMonthType> lookup = new HashMap<Integer, CalendarMonthType>();

	private Integer code;
	private String stringValue;
	private Integer integerValue;
	
	static{
		for(CalendarMonthType d : CalendarMonthType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	

	private CalendarMonthType(Integer code,String stringValue, Integer integerValue){
		this.code=code;
		this.stringValue=stringValue;
		this.integerValue=integerValue;
	}
	

	public static CalendarMonthType get(Integer code) {
		return lookup.get(code);
	}
	

	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}


	public String getStringValue() {
		return stringValue;
	}


	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}


	public Integer getIntegerValue() {
		return integerValue;
	}


	public void setIntegerValue(Integer integerValue) {
		this.integerValue = integerValue;
	}

	
	
}
