package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.IssuanceCertificateStateType;

@Entity
@Table(name="ISSUANCE_FILE_AUX")
public class IssuanceFileAux implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;
	
					     
//	@SequenceGenerator(name="ISSUANCE_FILE_AUX_PK_GENERATOR", sequenceName="SQ_ID_ISS_FILE_AUX_PK",initialValue=1,allocationSize=1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUANCE_FILE_AUX_PK_GENERATOR")
	
	
	@Id	
	@SequenceGenerator(name="ISSUANCE_FILE_PK_GENERATORR", sequenceName="SQ_ID_ISSUANCE_FILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUANCE_FILE_PK_GENERATORR")
	@Column(name="ID_ISSUANCE_FILE_PK")
	private Integer idIssuanceFilePk;

	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="DOCUMENT_DATE")
	private Date documentDate;
	
	@Lob()
	@Column(name="DOCUMENT_FILE")
	private byte[] documentFile;
	
	@Column(name="DOCUMENT_STATE")
	private Integer documentState;
	
	@Column(name="FILE_NAME")
	private String fileName;
	
	@ManyToOne
	@JoinColumn(name="ID_ISSUANCE_CODE_FK")
	private IssuanceAux issuance;
	 
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private Date maxDocumentDate;
	
	public IssuanceFileAux() {
		super();
		this.maxDocumentDate= new Date();
	}

	public Integer getIdIssuanceFilePk() {
		return idIssuanceFilePk;
	}

	public void setIdIssuanceFilePk(Integer idIssuanceFilePk) {
		this.idIssuanceFilePk = idIssuanceFilePk;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Date getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public byte[] getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	public Integer getDocumentState() {
		return documentState;
	}

	public void setDocumentState(Integer documentState) {
		this.documentState = documentState;
	}

	public String getDescriptionState(){
    	if(this.documentState!=null){
    		return IssuanceCertificateStateType.get(documentState).getValue();
    	}
    	return "";
    }
	
	public IssuanceAux getIssuance() {
		return issuance;
	}

	public void setIssuance(IssuanceAux issuance) {
		this.issuance = issuance;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getMaxDocumentDate() {
		return maxDocumentDate;
	}

	public void setMaxDocumentDate(Date maxDocumentDate) {
		this.maxDocumentDate = maxDocumentDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}	
}
