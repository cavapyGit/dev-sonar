package com.pradera.model.issuancesecuritie.type;

public enum PaymentScheduleReqHiRejectType {

	OTHER(Integer.valueOf(1966));
	
	private Integer code;
	
	private PaymentScheduleReqHiRejectType(Integer code){
		this.code=code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}	
}
