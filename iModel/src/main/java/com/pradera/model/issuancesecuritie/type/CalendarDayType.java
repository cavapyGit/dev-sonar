package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

public enum CalendarDayType  {
	  

    _360(Integer.valueOf(542),"360",360),  
	_365(Integer.valueOf(543),"365",365),	
	ACTUAL(Integer.valueOf(1589),"ACTUAL",366);
    
	public final static List<CalendarDayType> list=new ArrayList<CalendarDayType>();
	public static final Map<Integer, CalendarDayType> lookup = new HashMap<Integer, CalendarDayType>();

	private Integer code;
	private String stringValue;
	private Integer integerValue;
	
	static{
		for(CalendarDayType d : CalendarDayType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	

	private CalendarDayType(Integer code, String stringValue, Integer integerValue){
		this.code=code;
		this.stringValue=stringValue;
		this.integerValue=integerValue;
	}
	

	public static CalendarDayType get(Integer code) {
		return lookup.get(code);
	}
	

	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}


	public String getStringValue() {
		return stringValue;
	}


	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}


	public Integer getIntegerValue() {
		return integerValue;
	}

	public void setIntegerValue(Integer integerValue) {
		this.integerValue = integerValue;
	}
	
}
