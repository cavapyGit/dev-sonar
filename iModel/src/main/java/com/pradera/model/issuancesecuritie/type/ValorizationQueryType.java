package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AmortizationFactorType.
 */
public enum ValorizationQueryType {
	
	/** The proporcional. */
	VAL_NOMINAL(Integer.valueOf(2141),"VALOR NOMINAL"), 
	
	/** The no proporcional. */
	VAL_BOL(Integer.valueOf(2142),"VALOR BOLSA"),
	
	MARKET_FACT (Integer.valueOf(2143),"HECHOS DE MERCADO");
	

	/** The Constant list. */
	public final static List<ValorizationQueryType> list=new ArrayList<ValorizationQueryType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ValorizationQueryType> lookup = new HashMap<Integer, ValorizationQueryType>();
	

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	static{
		for(ValorizationQueryType d : ValorizationQueryType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	

	/**
	 * Instantiates a new amortization factor type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 */
	private ValorizationQueryType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the amortization factor type
	 */
	public static ValorizationQueryType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}