package com.pradera.model.issuancesecuritie.placementsegment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PLACEMENT_SEGMENT database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/03/2013
 */
@Entity
@NamedQueries({
	@NamedQuery(name = PlacementSegment.PLAC_STATE, query = "SELECT new com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment(p.idPlacementSegmentPk, p.placementSegmentState) FROM PlacementSegment p WHERE p.idPlacementSegmentPk = :idPlacementPkParam"),
	@NamedQuery(name = PlacementSegment.PLAC_ISSUANCE_DETAILS, query = "SELECT count(p),sum(p.amountToPlace) FROM PlacementSegment p WHERE p.issuance.idIssuanceCodePk=:idIssuancePrm and p.placementSegmentState=:statePrm"),
	@NamedQuery(name = PlacementSegment.PLAC_SECURITIES_BY_PARTICIPANT, 
					   query = "SELECT count(ps.idPlacementSegmentPk) from PlacementSegment ps join ps.placementSegmentDetails psd join ps.placementSegParticipaStructs psp where ps.issuance.idIssuanceCodePk=:idIssuancePkPrm and psd.security.idSecurityCodePk=:idSecurityCodePrm and psp.participant.idParticipantPk=:idParticipantPrm and ps.placementSegmentState in (:statePrm)")
})
@Table(name="PLACEMENT_SEGMENT")
public class PlacementSegment implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant PLAC_STATE. */
	public static final String PLAC_STATE = "placementSegment.searchState";
	public static final String PLAC_ISSUANCE_DETAILS = "placementSegment.placIssuanceDetails";
	public static final String PLAC_SECURITIES_BY_PARTICIPANT = "placementSegment.placSecuritiesByPart";

	/** The id placement segment pk. */
	@Id
	@SequenceGenerator(name="SQ_PLACEMENT_SEGMENT_PK", sequenceName="SQ_ID_PLACEMENT_SEGMENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_PLACEMENT_SEGMENT_PK")
	@Column(name="ID_PLACEMENT_SEGMENT_PK")
	private Long idPlacementSegmentPk;

    /** The action date. */
    @Temporal(TemporalType.DATE)
	@Column(name="ACTION_DATE")
	private Date actionDate;

	/** The action motive. */
	@Column(name="ACTION_MOTIVE")
	private Integer actionMotive;

	/** The amount to place. */
	@Column(name="AMOUNT_TO_PLACE")
	private BigDecimal amountToPlace;
	
	/** The amount to place temp. */
	@Transient
	private BigDecimal amountToPlaceTemp;

	/** The amount Confirm Temp. */
	@Transient
	private BigDecimal amountConfirm;
	
    /** The begining date. */
    @Temporal( TemporalType.DATE)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

    /** The closing date. */
    @Temporal( TemporalType.DATE)
	@Column(name="CLOSING_DATE")
	private Date closingDate;

	/** The issuance. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUANCE_CODE_FK",referencedColumnName="ID_ISSUANCE_CODE_PK")
	private Issuance issuance;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The other action motive. */
	@Column(name="OTHER_ACTION_MOTIVE")
	private String otherActionMotive;

	/** The placed amount. */
	@Column(name="PLACED_AMOUNT")
	private BigDecimal placedAmount;

	/** The placement term. */
	@Column(name="PLACEMENT_TERM")
	private BigDecimal placementTerm;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="CONFIRM_USER")
	private String confirmUser;
	
    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;
    
    /** The open date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="OPEN_DATE")
	private Date openDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state placement segment. */
	@Column(name="STATE_PLACEMENT_SEGMENT")
	private Integer placementSegmentState;

	/** The tranche number. */
	@Column(name="TRANCHE_NUMBER")
	private Integer trancheNumber;
	
	/** The fixed amount. */
	@Column(name="FIXED_AMOUNT")
	private BigDecimal fixedAmount;
	
	/** The request fixed amount. */
	@Column(name="REQUEST_FIXED_AMOUNT")
	private BigDecimal requestFixedAmount;
	
	/** The floating amount. */
	@Column(name="FLOATING_AMOUNT")
	private BigDecimal floatingAmount;
	
	/** The request floating amount. */
	@Column(name="REQUEST_FLOATING_AMOUNT")
	private BigDecimal requestFloatingAmount;
	
	/** The placement segment type. */
	@Column(name="PLACEMENT_SEGMENT_TYPE")
	private Integer placementSegmentType;
	
	/** The fixed amount. */
	@Column(name="BALANCE_TO_CONFIRM")
	private BigDecimal balanceToConfirm;
	
	/** The description participant struc. */
//	@Column(name="DESCRIPTION_PARTICIPANT_STRUC")
	@Transient
	private String descriptionParticipantStruc;
	
	/** The description holder account. */
//	@Column(name="DESCRIPTION_HOLDER_ACCOUNT")
	@Transient
	private String descriptionHolderAccount;
	
	@Transient
	private byte[] reopenFile;
	
	@Transient
	private String reopenFileName;
	
	@Transient
	private List<PlacementSegmentHy> listPlacementSegmentHys;

	//bi-directional many-to-one association to PlacementSegmentDetail
	/** The placement segment details. */
	@OneToMany(mappedBy="placementSegment",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<PlacementSegmentDetail> placementSegmentDetails;

	//bi-directional many-to-one association to PlacementSegParticipaStruct
	/** The placement seg participa structs. */
	@OneToMany(mappedBy="placementSegment",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<PlacementSegParticipaStruct> placementSegParticipaStructs;
	
	@Column(name="PENDING_OPERATION")
	private BigDecimal pendingOperation;
	
	@Column(name="PENDING_ANNOTATION")
	private BigDecimal pendingAnnotation;
	
	@Column(name="REOPEN_REQUEST")
	private Integer reopenRequest;
	
	@Transient
	private String stateDescription;
	
	@Transient
	private Integer placementePeriod;

	@Transient
	private boolean shareBanking;
	
	
    /**
     * Instantiates a new placement segment.
     */
    public PlacementSegment() {
    }
    

	/**
	 * Instantiates a new placement segment.
	 *
	 * @param idPlacementSegmentPk the id placement segment pk
	 * @param amountToPlace the amount to place
	 * @param placedAmount the placed amount
	 * @param placementTerm the placement term
	 * @param placementSegmentState the placement segment state
	 * @param trancheNumber the tranche number
	 * @param fixedAmount the fixed amount
	 * @param requestFixedAmount the request fixed amount
	 * @param floatingAmount the floating amount
	 * @param requestFloatingAmount the request floating amount
	 */
	public PlacementSegment(Long idPlacementSegmentPk,
			BigDecimal amountToPlace, BigDecimal placedAmount,
			BigDecimal placementTerm, Integer placementSegmentState,
			Integer trancheNumber, BigDecimal fixedAmount, BigDecimal requestFixedAmount,
			BigDecimal floatingAmount, BigDecimal requestFloatingAmount, Integer placementSegmentType) {
		super();
		this.idPlacementSegmentPk = idPlacementSegmentPk;
		this.amountToPlace = amountToPlace;
		this.placedAmount = placedAmount;
		this.placementTerm = placementTerm;
		this.placementSegmentState = placementSegmentState;
		this.trancheNumber = trancheNumber;
		this.fixedAmount = fixedAmount;
		this.requestFixedAmount = requestFixedAmount;
		this.floatingAmount = floatingAmount;
		this.requestFloatingAmount = requestFloatingAmount;
		this.placementSegmentType = placementSegmentType;
	}
	
	/**
	 * Instantiates a new placement segment.
	 *
	 * @param idPlacementSegmentPk the id placement segment pk
	 * @param amountToPlace the amount to place
	 * @param placedAmount the placed amount
	 * @param placementTerm the placement term
	 * @param placementSegmentState the placement segment state
	 * @param trancheNumber the tranche number
	 */
	public PlacementSegment(Long idPlacementSegmentPk,
			BigDecimal amountToPlace, BigDecimal placedAmount,
			BigDecimal placementTerm, Integer placementSegmentState,
			Integer trancheNumber) {
		super();
		this.idPlacementSegmentPk = idPlacementSegmentPk;
		this.amountToPlace = amountToPlace;
		this.placedAmount = placedAmount;
		this.placementTerm = placementTerm;
		this.placementSegmentState = placementSegmentState;
		this.trancheNumber = trancheNumber;
	}
	
    
	public PlacementSegment(Long idPlacementSegmentPk,
			BigDecimal amountToPlace, BigDecimal placedAmount,
			BigDecimal placementTerm, Integer placementSegmentState,
			Integer trancheNumber, BigDecimal fixedAmount, BigDecimal requestFixedAmount,
			BigDecimal floatingAmount, BigDecimal requestFloatingAmount, Integer placementSegmentType,
			BigDecimal placedAmountDet, BigDecimal requestAmountDet ,Integer statePlacementSegmentDet,
			Integer placementSegmentDetType, String idIsinCodePk) {
		super();
		this.idPlacementSegmentPk = idPlacementSegmentPk;
		this.amountToPlace = amountToPlace;
		this.placedAmount = placedAmount;
		this.placementTerm = placementTerm;
		this.placementSegmentState = placementSegmentState;
		this.trancheNumber = trancheNumber;
		this.fixedAmount = fixedAmount;
		this.requestFixedAmount = requestFixedAmount;
		this.floatingAmount = floatingAmount;
		this.requestFloatingAmount = requestFloatingAmount;
		this.placementSegmentType = placementSegmentType;
		PlacementSegmentDetail placementSegmentDetail = new PlacementSegmentDetail();
		placementSegmentDetail.setPlacedAmount(placedAmountDet);
		placementSegmentDetail.setRequestAmount(requestAmountDet);
		placementSegmentDetail.setStatePlacementSegmentDet(statePlacementSegmentDet);
		placementSegmentDetail.setPlacementSegmentDetType(placementSegmentDetType);
		placementSegmentDetail.setSecurity(new Security(idIsinCodePk));
		this.placementSegmentDetails = new ArrayList<PlacementSegmentDetail>(0);
		this.placementSegmentDetails.add(placementSegmentDetail);
	}

	/**
	 * Gets the id placement segment pk.
	 *
	 * @return the id placement segment pk
	 */
	public Long getIdPlacementSegmentPk() {
		return this.idPlacementSegmentPk;
	}

	/**
	 * Instantiates a new placement segment.
	 *
	 * @param idPlacementSegmentPk the id placement segment pk
	 * @param placementSegmentState the placement segment state
	 */
	public PlacementSegment(Long idPlacementSegmentPk,
			Integer placementSegmentState) {
		super();
		this.idPlacementSegmentPk = idPlacementSegmentPk;
		this.placementSegmentState = placementSegmentState;
	}


	/**
	 * Sets the id placement segment pk.
	 *
	 * @param idPlacementSegmentPk the new id placement segment pk
	 */
	public void setIdPlacementSegmentPk(Long idPlacementSegmentPk) {
		this.idPlacementSegmentPk = idPlacementSegmentPk;
	}

	/**
	 * Gets the action date.
	 *
	 * @return the action date
	 */
	public Date getActionDate() {
		return this.actionDate;
	}

	/**
	 * Sets the action date.
	 *
	 * @param actionDate the new action date
	 */
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 * Gets the action motive.
	 *
	 * @return the action motive
	 */
	public Integer getActionMotive() {
		return this.actionMotive;
	}

	/**
	 * Sets the action motive.
	 *
	 * @param actionMotive the new action motive
	 */
	public void setActionMotive(Integer actionMotive) {
		this.actionMotive = actionMotive;
	}

	/**
	 * Gets the amount to place.
	 *
	 * @return the amount to place
	 */
	public BigDecimal getAmountToPlace() {
		return this.amountToPlace;
	}

	/**
	 * Sets the amount to place.
	 *
	 * @param amountToPlace the new amount to place
	 */
	public void setAmountToPlace(BigDecimal amountToPlace) {
		this.amountToPlace = amountToPlace;
	}

	/**
	 * Gets the begining date.
	 *
	 * @return the begining date
	 */
	public Date getBeginingDate() {
		return this.beginingDate;
	}

	/**
	 * Sets the begining date.
	 *
	 * @param beginingDate the new begining date
	 */
	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	/**
	 * Gets the closing date.
	 *
	 * @return the closing date
	 */
	public Date getClosingDate() {
		return this.closingDate;
	}

	/**
	 * Sets the closing date.
	 *
	 * @param closingDate the new closing date
	 */
	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the other action motive.
	 *
	 * @return the other action motive
	 */
	public String getOtherActionMotive() {
		return this.otherActionMotive;
	}

	/**
	 * Sets the other action motive.
	 *
	 * @param otherActionMotive the new other action motive
	 */
	public void setOtherActionMotive(String otherActionMotive) {
		this.otherActionMotive = otherActionMotive;
	}

	/**
	 * Gets the placed amount.
	 *
	 * @return the placed amount
	 */
	public BigDecimal getPlacedAmount() {
		return this.placedAmount;
	}

	/**
	 * Sets the placed amount.
	 *
	 * @param placedAmount the new placed amount
	 */
	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	/**
	 * Gets the placement term.
	 *
	 * @return the placement term
	 */
	public BigDecimal getPlacementTerm() {
		return this.placementTerm;
	}

	/**
	 * Sets the placement term.
	 *
	 * @param placementTerm the new placement term
	 */
	public void setPlacementTerm(BigDecimal placementTerm) {
		this.placementTerm = placementTerm;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state placement segment.
	 *
	 * @return the state placement segment
	 */
	public Integer getPlacementSegmentState() {
		return this.placementSegmentState;
	}
	
	/**
	 * Gets the placement segment state description.
	 *
	 * @return the placement segment state description
	 */
	public String getPlacementSegmentStateDescription() {
		return PlacementSegmentStateType.lookup.get(this.placementSegmentState).getValue();
	}

	/**
	 * Sets the state placement segment.
	 *
	 * @param statePlacementSegment the new state placement segment
	 */
	public void setPlacementSegmentState(Integer statePlacementSegment) {
		this.placementSegmentState = statePlacementSegment;
	}

	/**
	 * Gets the tranche number.
	 *
	 * @return the tranche number
	 */
	public Integer getTrancheNumber() {
		return this.trancheNumber;
	}

	/**
	 * Sets the tranche number.
	 *
	 * @param trancheNumber the new tranche number
	 */
	public void setTrancheNumber(Integer trancheNumber) {
		this.trancheNumber = trancheNumber;
	}

	/**
	 * Gets the placement segment details.
	 *
	 * @return the placement segment details
	 */
	public List<PlacementSegmentDetail> getPlacementSegmentDetails() {
		return this.placementSegmentDetails;
	}

	/**
	 * Sets the placement segment details.
	 *
	 * @param placementSegmentDetails the new placement segment details
	 */
	public void setPlacementSegmentDetails(List<PlacementSegmentDetail> placementSegmentDetails) {
		this.placementSegmentDetails = placementSegmentDetails;
	}
	
	/**
	 * Gets the placement seg participa structs.
	 *
	 * @return the placement seg participa structs
	 */
	public List<PlacementSegParticipaStruct> getPlacementSegParticipaStructs() {
		return this.placementSegParticipaStructs;
	}

	/**
	 * Sets the placement seg participa structs.
	 *
	 * @param placementSegParticipaStructs the new placement seg participa structs
	 */
	public void setPlacementSegParticipaStructs(List<PlacementSegParticipaStruct> placementSegParticipaStructs) {
		this.placementSegParticipaStructs = placementSegParticipaStructs;
	}

	/**
	 * Gets the issuance.
	 *
	 * @return the issuance
	 */
	public Issuance getIssuance() {
		return issuance;
	}

	/**
	 * Sets the issuance.
	 *
	 * @param issuance the new issuance
	 */
	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}
	
	/**
	 * Gets the description participant struc.
	 *
	 * @return the description participant struc
	 */
	public String getDescriptionParticipantStruc() {
		return descriptionParticipantStruc;
	}

	/**
	 * Sets the description participant struc.
	 *
	 * @param descriptionParticipantStruc the new description participant struc
	 */
	public void setDescriptionParticipantStruc(String descriptionParticipantStruc) {
		this.descriptionParticipantStruc = descriptionParticipantStruc;
	}

	/**
	 * Gets the description holder account.
	 *
	 * @return the description holder account
	 */
	public String getDescriptionHolderAccount() {
		return descriptionHolderAccount;
	}

	/**
	 * Sets the description holder account.
	 *
	 * @param descriptionHolderAccount the new description holder account
	 */
	public void setDescriptionHolderAccount(String descriptionHolderAccount) {
		this.descriptionHolderAccount = descriptionHolderAccount;
	}

	/**
	 * Gets the confirm user.
	 *
	 * @return the confirm user
	 */
	public String getConfirmUser() {
		return confirmUser;
	}

	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the new confirm user
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the open date.
	 *
	 * @return the open date
	 */
	public Date getOpenDate() {
		return openDate;
	}

	/**
	 * Sets the open date.
	 *
	 * @param openDate the new open date
	 */
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	
	public Integer getPlacementePeriod() {
		return placementePeriod;
	}


	public void setPlacementePeriod(Integer placementePeriod) {
		this.placementePeriod = placementePeriod;
	}


	/**
	 * Gets the amount available for fixed handler.
	 *
	 * @return the amount available for fixed handler
	 */
	public BigDecimal getAmountAvailableForFixedHandler(){
		if(this.getAmountToPlace() == null || this.getPlacedAmountFixedFromDetail() == null){
			return null;
		}
		return this.amountToPlace.subtract(this.getPlacedAmountFixedFromDetail());
	}
	
	/**
	 * Gets the placed amount from detail.
	 *
	 * @return the placed amount from detail
	 */
	public BigDecimal getPlacedAmountFixedFromDetail(){
		if(placementSegmentDetails==null){
			return null;
		}
		BigDecimal sumSecurities = BigDecimal.ZERO;
		Boolean haveSecurityLikeFloating = Boolean.FALSE;
		for(PlacementSegmentDetail placSegDet: this.getPlacementSegmentDetails()){
			if(placSegDet.isSelected() && placSegDet.getPlacementSegmentDetType()!=null && placSegDet.getPlacementSegmentDetType().equals(SecurityPlacementType.FIXED.getCode())){
				if(placSegDet.getPlacedAmount()==null){
					continue;
				}
				if(sumSecurities==null){
					sumSecurities = placSegDet.getPlacedAmount().abs();
					continue;
				}
				sumSecurities = sumSecurities.add(placSegDet.getPlacedAmount());
			}else if(placSegDet.getPlacementSegmentDetType()!=null && placSegDet.getPlacementSegmentDetType().equals(SecurityPlacementType.FLOATING.getCode())){
				haveSecurityLikeFloating = Boolean.TRUE;
			}
		}
		if(sumSecurities==null && haveSecurityLikeFloating){
			return BigDecimal.ZERO;
		}
		return sumSecurities;
	}
	
	/**
	 * Gets the placed amount floating from detail.
	 *
	 * @return the placed amount floating from detail
	 */
	public BigDecimal getPlacedAmountFloatingFromDetail(){
		if(Validations.validateIsNull(this.getPlacementSegmentDetails())){
			return null;
		}
		if(Validations.validateIsNull(this.getAmountToPlace())){
			return null;
		}
		if(Validations.validateIsNull(this.getPlacedAmountFixedFromDetail())){
			return this.getPlacedAmount();
		}
		for(PlacementSegmentDetail placementSegmentDetail: this.getPlacementSegmentDetails()){
			if(placementSegmentDetail.isSelected() && placementSegmentDetail.getPlacementSegmentDetType()!=null && placementSegmentDetail.getPlacementSegmentDetType().equals(SecurityPlacementType.FLOATING.getCode())){
				return this.getAmountToPlace().subtract(this.getPlacedAmountFixedFromDetail());
			}
		}
		return BigDecimal.ZERO;
	}
	
	/**
	 * Gets the part list from part struct.
	 *
	 * @return the part list from part struct
	 */
	public List<Participant> getPartListFromPartStruct(){
		if(placementSegParticipaStructs==null){
			return null;
		}
		List<Participant> participantListTemp = new ArrayList<Participant>();
		for(PlacementSegParticipaStruct placementSegParticipaStruct: placementSegParticipaStructs){
			if(placementSegParticipaStruct.getParticipant()==null){
				continue;
			}
			participantListTemp.add(placementSegParticipaStruct.getParticipant());
		}
		return participantListTemp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub

		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("placementSegmentDetails", placementSegmentDetails);
        detailsMap.put("placementSegParticipaStructs", placementSegParticipaStructs);
		return detailsMap;
	}

	public boolean isRegisteredState(){
		if(PlacementSegmentStateType.REGISTERED.getCode().equals( placementSegmentState )){
			return true;
		}
		return false;
	}
	
	public boolean isConfirmedState(){
		if(PlacementSegmentStateType.CONFIRMED.getCode().equals( placementSegmentState )){
			return true;
		}
		return false;
	}
	
	public boolean isClosedState(){
		if(PlacementSegmentStateType.CLOSED.getCode().equals( placementSegmentState )){
			return true;
		}
		return false;
	}
	
	public boolean isOpenedState(){
		if(PlacementSegmentStateType.OPENED.getCode().equals( placementSegmentState )){
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the amount to place temp.
	 *
	 * @return the amount to place temp
	 */
	public BigDecimal getAmountToPlaceTemp() {
		return amountToPlaceTemp;
	}

	/**
	 * Sets the amount to place temp.
	 *
	 * @param amountToPlaceTemp the new amount to place temp
	 */
	public void setAmountToPlaceTemp(BigDecimal amountToPlaceTemp) {
		this.amountToPlaceTemp = amountToPlaceTemp;
	}

	/**
	 * Gets the fixed amount.
	 *
	 * @return the fixed amount
	 */
	public BigDecimal getFixedAmount() {
		return fixedAmount;
	}

	/**
	 * Sets the fixed amount.
	 *
	 * @param fixedAmount the new fixed amount
	 */
	public void setFixedAmount(BigDecimal fixedAmount) {
		this.fixedAmount = fixedAmount;
	}

	/**
	 * Gets the request fixed amount.
	 *
	 * @return the request fixed amount
	 */
	public BigDecimal getRequestFixedAmount() {
		return requestFixedAmount;
	}

	/**
	 * Sets the request fixed amount.
	 *
	 * @param requestFixedAmount the new request fixed amount
	 */
	public void setRequestFixedAmount(BigDecimal requestFixedAmount) {
		this.requestFixedAmount = requestFixedAmount;
	}

	/**
	 * Gets the floating amount.
	 *
	 * @return the floating amount
	 */
	public BigDecimal getFloatingAmount() {
		return floatingAmount;
	}

	/**
	 * Sets the floating amount.
	 *
	 * @param floatingAmount the new floating amount
	 */
	public void setFloatingAmount(BigDecimal floatingAmount) {
		this.floatingAmount = floatingAmount;
	}

	/**
	 * Gets the request floating amount.
	 *
	 * @return the request floating amount
	 */
	public BigDecimal getRequestFloatingAmount() {
		return requestFloatingAmount;
	}

	/**
	 * Sets the request floating amount.
	 *
	 * @param requestFloatingAmount the new request floating amount
	 */
	public void setRequestFloatingAmount(BigDecimal requestFloatingAmount) {
		this.requestFloatingAmount = requestFloatingAmount;
	}

	/**
	 * Gets the placement segment type.
	 *
	 * @return the placement segment type
	 */
	public Integer getPlacementSegmentType() {
		return placementSegmentType;
	}

	/**
	 * Sets the placement segment type.
	 *
	 * @param placementSegmentType the new placement segment type
	 */
	public void setPlacementSegmentType(Integer placementSegmentType) {
		this.placementSegmentType = placementSegmentType;
	}


	public BigDecimal getBalanceToConfirm() {
		return balanceToConfirm;
	}


	public void setBalanceToConfirm(BigDecimal balanceToConfirm) {
		this.balanceToConfirm = balanceToConfirm;
	}


	public String getStateDescription() {
		return stateDescription;
	}


	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}


	/**
	 * @return the amountConfirm
	 */
	public BigDecimal getAmountConfirm() {
		return amountConfirm;
	}


	/**
	 * @param amountConfirm the amountConfirm to set
	 */
	public void setAmountConfirm(BigDecimal amountConfirm) {
		this.amountConfirm = amountConfirm;
	}


	/**
	 * Gets the pending operation.
	 *
	 * @return the pending operation
	 */
	public BigDecimal getPendingOperation() {
		return pendingOperation;
	}


	public void setPendingOperation(BigDecimal pendingOperation) {
		this.pendingOperation = pendingOperation;
	}


	public BigDecimal getPendingAnnotation() {
		return pendingAnnotation;
	}


	/**
	 * Sets the pending annotation.
	 *
	 * @param pendingAnnotation the new pending annotation
	 */
	public void setPendingAnnotation(BigDecimal pendingAnnotation) {
		this.pendingAnnotation = pendingAnnotation;
	}


	public boolean isShareBanking() {
		return shareBanking;
	}


	public void setShareBanking(boolean shareBanking) {
		this.shareBanking = shareBanking;
	}

	public byte[] getReopenFile() {
		return reopenFile;
	}

	public void setReopenFile(byte[] reopenFile) {
		this.reopenFile = reopenFile;
	}

	public String getReopenFileName() {
		return reopenFileName;
	}

	public void setReopenFileName(String reopenFileName) {
		this.reopenFileName = reopenFileName;
	}

	public List<PlacementSegmentHy> getListPlacementSegmentHys() {
		return listPlacementSegmentHys;
	}

	public void setListPlacementSegmentHys(List<PlacementSegmentHy> listPlacementSegmentHys) {
		this.listPlacementSegmentHys = listPlacementSegmentHys;
	}

	public Integer getReopenRequest() {
		return reopenRequest;
	}

	public void setReopenRequest(Integer reopenRequest) {
		this.reopenRequest = reopenRequest;
	}
	
}