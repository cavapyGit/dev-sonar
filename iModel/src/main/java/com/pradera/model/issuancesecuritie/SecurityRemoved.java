package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "SECURITY_REMOVED")
@NamedQueries({
    @NamedQuery(name = "SecurityRemoved.findAll", query = "SELECT s FROM SecurityRemoved s"),
    @NamedQuery(name = "SecurityRemoved.findByIdSecurityRemovedPk", query = "SELECT s FROM SecurityRemoved s WHERE s.idSecurityRemovedPk = :idSecurityRemovedPk"),
    @NamedQuery(name = "SecurityRemoved.findByIdSecurityCodePk", query = "SELECT s FROM SecurityRemoved s WHERE s.idSecurityCodePk = :idSecurityCodePk"),
    @NamedQuery(name = "SecurityRemoved.findByIssuanceDate", query = "SELECT s FROM SecurityRemoved s WHERE s.issuanceDate = :issuanceDate"),
    @NamedQuery(name = "SecurityRemoved.findByExpirationDate", query = "SELECT s FROM SecurityRemoved s WHERE s.expirationDate = :expirationDate"),
    @NamedQuery(name = "SecurityRemoved.findByCurrency", query = "SELECT s FROM SecurityRemoved s WHERE s.currency = :currency"),
    @NamedQuery(name = "SecurityRemoved.findByNominalValue", query = "SELECT s FROM SecurityRemoved s WHERE s.nominalValue = :nominalValue"),
    @NamedQuery(name = "SecurityRemoved.findByInterestRateNominal", query = "SELECT s FROM SecurityRemoved s WHERE s.interestRateNominal = :interestRateNominal"),
    @NamedQuery(name = "SecurityRemoved.findByCouponNumber", query = "SELECT s FROM SecurityRemoved s WHERE s.couponNumber = :couponNumber"),
    @NamedQuery(name = "SecurityRemoved.findByMotiveRemoved", query = "SELECT s FROM SecurityRemoved s WHERE s.motiveRemoved = :motiveRemoved"),
    @NamedQuery(name = "SecurityRemoved.findByRegistryDate", query = "SELECT s FROM SecurityRemoved s WHERE s.registryDate = :registryDate"),
    @NamedQuery(name = "SecurityRemoved.findByStateSecurityRemoved", query = "SELECT s FROM SecurityRemoved s WHERE s.stateSecurityRemoved = :stateSecurityRemoved"),
    @NamedQuery(name = "SecurityRemoved.findByLastModifyUser", query = "SELECT s FROM SecurityRemoved s WHERE s.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "SecurityRemoved.findByLastModifyDate", query = "SELECT s FROM SecurityRemoved s WHERE s.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "SecurityRemoved.findByLastModifyIp", query = "SELECT s FROM SecurityRemoved s WHERE s.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "SecurityRemoved.findByLastModifyApp", query = "SELECT s FROM SecurityRemoved s WHERE s.lastModifyApp = :lastModifyApp")})
public class SecurityRemoved implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_SECURITY_REMOVED", sequenceName="SQ_SECURITY_REMOVED",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_SECURITY_REMOVED")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SECURITY_REMOVED_PK", nullable = false)
    private Long idSecurityRemovedPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SECURITY_CODE_PK", nullable = false, length = 23)
    private String idSecurityCodePk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISSUANCE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date issuanceDate;
    
    @Column(name = "EXPIRATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CURRENCY", nullable = false)
    private Integer currency;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOMINAL_VALUE", nullable = false, precision = 22, scale = 8)
    private BigDecimal nominalValue;
    
    @Column(name = "INTEREST_RATE_NOMINAL", precision = 12, scale = 8)
    private BigDecimal interestRateNominal;
    
    @Column(name = "COUPON_NUMBER")
    private Integer couponNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "MOTIVE_REMOVED", nullable = false, length = 1000)
    private String motiveRemoved;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE_SECURITY_REMOVED", nullable = false)
    private Integer stateSecurityRemoved;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSecurityRemovedFk", fetch = FetchType.LAZY)
    private List<SecurityRemovedHistory> securityRemovedHistoryList;

    public SecurityRemoved() {
    }

    public SecurityRemoved(Long idSecurityRemovedPk) {
        this.idSecurityRemovedPk = idSecurityRemovedPk;
    }

    public SecurityRemoved(Long idSecurityRemovedPk, String idSecurityCodePk, Date issuanceDate, Date expirationDate, Integer currency, BigDecimal nominalValue, BigDecimal interestRateNominal, Integer couponNumber, String motiveRemoved, Date registryDate, Integer stateSecurityRemoved, String lastModifyUser, Date lastModifyDate, String lastModifyIp, Long lastModifyApp) {
        this.idSecurityRemovedPk = idSecurityRemovedPk;
        this.idSecurityCodePk = idSecurityCodePk;
        this.issuanceDate = issuanceDate;
        this.expirationDate = expirationDate;
        this.currency = currency;
        this.nominalValue = nominalValue;
        this.interestRateNominal = interestRateNominal;
        this.couponNumber = couponNumber;
        this.motiveRemoved = motiveRemoved;
        this.registryDate = registryDate;
        this.stateSecurityRemoved = stateSecurityRemoved;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdSecurityRemovedPk() {
        return idSecurityRemovedPk;
    }

    public void setIdSecurityRemovedPk(Long idSecurityRemovedPk) {
        this.idSecurityRemovedPk = idSecurityRemovedPk;
    }

    public String getIdSecurityCodePk() {
        return idSecurityCodePk;
    }

    public void setIdSecurityCodePk(String idSecurityCodePk) {
        this.idSecurityCodePk = idSecurityCodePk;
    }

    public Date getIssuanceDate() {
        return issuanceDate;
    }

    public void setIssuanceDate(Date issuanceDate) {
        this.issuanceDate = issuanceDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public BigDecimal getNominalValue() {
        return nominalValue;
    }

    public void setNominalValue(BigDecimal nominalValue) {
        this.nominalValue = nominalValue;
    }

    public BigDecimal getInterestRateNominal() {
        return interestRateNominal;
    }

    public void setInterestRateNominal(BigDecimal interestRateNominal) {
        this.interestRateNominal = interestRateNominal;
    }

    public Integer getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(Integer couponNumber) {
        this.couponNumber = couponNumber;
    }

    public String getMotiveRemoved() {
        return motiveRemoved;
    }

    public void setMotiveRemoved(String motiveRemoved) {
        this.motiveRemoved = motiveRemoved;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public Integer getStateSecurityRemoved() {
        return stateSecurityRemoved;
    }

    public void setStateSecurityRemoved(Integer stateSecurityRemoved) {
        this.stateSecurityRemoved = stateSecurityRemoved;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public List<SecurityRemovedHistory> getSecurityRemovedHistoryList() {
        return securityRemovedHistoryList;
    }

    public void setSecurityRemovedHistoryList(List<SecurityRemovedHistory> securityRemovedHistoryList) {
        this.securityRemovedHistoryList = securityRemovedHistoryList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSecurityRemovedPk != null ? idSecurityRemovedPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecurityRemoved)) {
            return false;
        }
        SecurityRemoved other = (SecurityRemoved) object;
        if ((this.idSecurityRemovedPk == null && other.idSecurityRemovedPk != null) || (this.idSecurityRemovedPk != null && !this.idSecurityRemovedPk.equals(other.idSecurityRemovedPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.SecurityRemoved[ idSecurityRemovedPk=" + idSecurityRemovedPk + " ]";
    }
    
}

