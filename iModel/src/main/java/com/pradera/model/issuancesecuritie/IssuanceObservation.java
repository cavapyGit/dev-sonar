package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the ISSUANCE_OBSERVATION database table.
 * 
 */
@Entity
@Table(name="ISSUANCE_OBSERVATION")
public class IssuanceObservation implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="ISSUANCE_OBS_PK_GENERATOR", sequenceName="SQ_ID_ISS_OBS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUANCE_OBS_PK_GENERATOR")
	@Column(name = "ID_ISSUANCE_OBS_PK")
	private Integer idIssuanceObsPk;
	
	@ManyToOne
	@JoinColumn(name="ID_ISSUANCE_CODE_FK")
	private IssuanceAux issuance;
	
	@Column(name="OBSERVATION_TEXT")
	private String obsText;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OBSERVATION_DATE")
	private Date obsDate;
	
	@Column(name="OBSERVATION_STATE")
	private Integer obsState;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	public Integer getIdIssuanceObsPk() {
		return idIssuanceObsPk;
	}

	public void setIdIssuanceObsPk(Integer idIssuanceObsPk) {
		this.idIssuanceObsPk = idIssuanceObsPk;
	}

	public IssuanceAux getIssuance() {
		return issuance;
	}

	public void setIssuance(IssuanceAux issuance) {
		this.issuance = issuance;
	}

	public String getObsText() {
		return obsText;
	}

	public void setObsText(String obsText) {
		this.obsText = obsText;
	}

	public Date getObsDate() {
		return obsDate;
	}

	public void setObsDate(Date obsDate) {
		this.obsDate = obsDate;
	}

	public Integer getObsState() {
		return obsState;
	}

	public void setObsState(Integer obsState) {
		this.obsState = obsState;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = 2701;
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
