package com.pradera.model.issuancesecuritie;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.AmortizationType;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleReqHiRejectType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the AMO_PAYMENT_SCHEDULE_REQ_HIS database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/04/2014
 */
@Entity
@Table(name="AMO_PAYMENT_SCHEDULE_REQ_HIS")
public class AmoPaymentScheduleReqHi implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id amo schedule req his pk. */
	@Id
	@SequenceGenerator(name="AMO_PAYMENT_SCHEDULE_REQ_HIS_IDAMOSCHEDULEREQHISPK_GENERATOR", sequenceName="SQ_ID_AMO_SCHEDULE_REQ_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AMO_PAYMENT_SCHEDULE_REQ_HIS_IDAMOSCHEDULEREQHISPK_GENERATOR")
	@Column(name="ID_AMO_SCHEDULE_REQ_HIS_PK")
	private Long idAmoScheduleReqHisPk;
	
	/** The amortization payment schedule. */
	@ManyToOne
	@JoinColumn(name="ID_AMO_PAYMENT_SCHEDULE_FK")
	private AmortizationPaymentSchedule amortizationPaymentSchedule;

	/** The amortization factor. */
	@Column(name="AMORTIZATION_FACTOR")
	private BigDecimal amortizationFactor;

	/** The amortization type. */
	@Column(name="AMORTIZATION_TYPE")
	private Integer amortizationType;

	/** The calendar days. */
	@Column(name="CALENDAR_DAYS")
	private Integer calendarDays;

	/** The corporative days. */
	@Column(name="CORPORATIVE_DAYS")
	private Integer corporativeDays;

    /** The security. */
    @ManyToOne
	@JoinColumn(name="ID_SECURITY_CODE_FK")
    @NotNull
	private Security security;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The periodicity. */
	private Integer periodicity;
	
	/** The periodicity days. */
	@Column(name="PERIODICITY_DAYS")
	private Integer periodicityDays;	

	/** The registry days. */
	@Column(name="REGISTRY_DAYS")
	private Integer registryDays;

	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;

	/** The schedule state. */
	@Column(name="SCHEDULE_STATE")
	private Integer scheduleState;

	/** The tax factor. */
	@Column(name="TAX_FACTOR")
	private BigDecimal taxFactor;
	
	/** The reject motive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/** The reject other motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;

	//bi-directional many-to-one association to ProgramAmoCouponReqHi
	/** The program amo coupon req his. */
	@OneToMany(mappedBy="amoPaymentScheduleReqHi",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<ProgramAmoCouponReqHi> programAmoCouponReqHis;
	
	/** The request state. */
	@Column(name="REQUEST_STATE")
	@NotNull
	private Integer requestState;
	
	/** The payment modality. */
	@Column(name="PAYMENT_MODALITY")
	private Integer paymentModality;
	
    /** The payment first expiration date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="PAYMENT_FIRST_EXPIRATION_DATE")
	private Date paymentFirstExpirationDate;
	
    /** The number payments. */
    @Column(name="NUMBER_PAYMENTS")
	private Integer numberPayments;
	
    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
    
	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registryUser;

    /**
     * Instantiates a new amo payment schedule req hi.
     */
    public AmoPaymentScheduleReqHi() {
    	programAmoCouponReqHis=new ArrayList<ProgramAmoCouponReqHi>();
    }

	/**
	 * Gets the id amo schedule req his pk.
	 *
	 * @return the id amo schedule req his pk
	 */
	public Long getIdAmoScheduleReqHisPk() {
		return this.idAmoScheduleReqHisPk;
	}

	/**
	 * Sets the id amo schedule req his pk.
	 *
	 * @param idAmoScheduleReqHisPk the new id amo schedule req his pk
	 */
	public void setIdAmoScheduleReqHisPk(Long idAmoScheduleReqHisPk) {
		this.idAmoScheduleReqHisPk = idAmoScheduleReqHisPk;
	}

	/**
	 * Gets the amortization factor.
	 *
	 * @return the amortization factor
	 */
	public BigDecimal getAmortizationFactor() {
		return this.amortizationFactor;
	}

	/**
	 * Sets the amortization factor.
	 *
	 * @param amortizationFactor the new amortization factor
	 */
	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}

	/**
	 * Gets the amortization type.
	 *
	 * @return the amortization type
	 */
	public Integer getAmortizationType() {
		return this.amortizationType;
	}

	/**
	 * Sets the amortization type.
	 *
	 * @param amortizationType the new amortization type
	 */
	public void setAmortizationType(Integer amortizationType) {
		this.amortizationType = amortizationType;
	}

	/**
	 * Gets the calendar days.
	 *
	 * @return the calendar days
	 */
	public Integer getCalendarDays() {
		return this.calendarDays;
	}

	/**
	 * Sets the calendar days.
	 *
	 * @param calendarDays the new calendar days
	 */
	public void setCalendarDays(Integer calendarDays) {
		this.calendarDays = calendarDays;
	}

	/**
	 * Gets the corporative days.
	 *
	 * @return the corporative days
	 */
	public Integer getCorporativeDays() {
		return this.corporativeDays;
	}

	/**
	 * Sets the corporative days.
	 *
	 * @param corporativeDays the new corporative days
	 */
	public void setCorporativeDays(Integer corporativeDays) {
		this.corporativeDays = corporativeDays;
	}


	/**
	 * Gets the amortization payment schedule.
	 *
	 * @return the amortization payment schedule
	 */
	public AmortizationPaymentSchedule getAmortizationPaymentSchedule() {
		return amortizationPaymentSchedule;
	}

	/**
	 * Sets the amortization payment schedule.
	 *
	 * @param amortizationPaymentSchedule the new amortization payment schedule
	 */
	public void setAmortizationPaymentSchedule(
			AmortizationPaymentSchedule amortizationPaymentSchedule) {
		this.amortizationPaymentSchedule = amortizationPaymentSchedule;
	}

	

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the periodicity.
	 *
	 * @return the periodicity
	 */
	public Integer getPeriodicity() {
		return this.periodicity;
	}

	/**
	 * Sets the periodicity.
	 *
	 * @param periodicity the new periodicity
	 */
	public void setPeriodicity(Integer periodicity) {
		this.periodicity = periodicity;
	}

	/**
	 * Gets the registry days.
	 *
	 * @return the registry days
	 */
	public Integer getRegistryDays() {
		return this.registryDays;
	}

	/**
	 * Sets the registry days.
	 *
	 * @param registryDays the new registry days
	 */
	public void setRegistryDays(Integer registryDays) {
		this.registryDays = registryDays;
	}

	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return this.registryType;
	}

	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	/**
	 * Gets the schedule state.
	 *
	 * @return the schedule state
	 */
	public Integer getScheduleState() {
		return this.scheduleState;
	}

	/**
	 * Sets the schedule state.
	 *
	 * @param scheduleState the new schedule state
	 */
	public void setScheduleState(Integer scheduleState) {
		this.scheduleState = scheduleState;
	}

	/**
	 * Gets the tax factor.
	 *
	 * @return the tax factor
	 */
	public BigDecimal getTaxFactor() {
		return this.taxFactor;
	}

	/**
	 * Sets the tax factor.
	 *
	 * @param taxFactor the new tax factor
	 */
	public void setTaxFactor(BigDecimal taxFactor) {
		this.taxFactor = taxFactor;
	}

	/**
	 * Gets the program amo coupon req his.
	 *
	 * @return the program amo coupon req his
	 */
	public List<ProgramAmoCouponReqHi> getProgramAmoCouponReqHis() {
		return this.programAmoCouponReqHis;
	}

	/**
	 * Sets the program amo coupon req his.
	 *
	 * @param programAmoCouponReqHis the new program amo coupon req his
	 */
	public void setProgramAmoCouponReqHis(List<ProgramAmoCouponReqHi> programAmoCouponReqHis) {
		this.programAmoCouponReqHis = programAmoCouponReqHis;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AmoPaymentScheduleReqHi [idAmoScheduleReqHisPk="
				+ idAmoScheduleReqHisPk + ", amortizationPaymentSchedule="
				+ amortizationPaymentSchedule + ", amortizationFactor="
				+ amortizationFactor + ", amortizationType=" + amortizationType
				+ ", calendarDays=" + calendarDays + ", corporativeDays="
				+ corporativeDays + ", idSecurityCodeFk=" + security.toString()
				+ ", lastModifyApp=" + lastModifyApp + ", lastModifyDate="
				+ lastModifyDate + ", lastModifyIp=" + lastModifyIp
				+ ", lastModifyUser=" + lastModifyUser + ", periodicity="
				+ periodicity + ", registryDays=" + registryDays
				+ ", registryType=" + registryType + ", scheduleState="
				+ scheduleState + ", taxFactor=" + taxFactor
				+ ", programAmoCouponReqHis=" + programAmoCouponReqHis + "]";
	}

	
	

	

	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("programAmoCouponReqHis", programAmoCouponReqHis);
        return detailsMap;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	/**
	 * Gets the periodicity days.
	 *
	 * @return the periodicity days
	 */
	public Integer getPeriodicityDays() {
		return periodicityDays;
	}

	/**
	 * Sets the periodicity days.
	 *
	 * @param periodicityDays the new periodicity days
	 */
	public void setPeriodicityDays(Integer periodicityDays) {
		this.periodicityDays = periodicityDays;
	}

	
	
	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}
	
	

	/**
	 * Gets the payment modality.
	 *
	 * @return the payment modality
	 */
	public Integer getPaymentModality() {
		return paymentModality;
	}

	/**
	 * Sets the payment modality.
	 *
	 * @param paymentModality the new payment modality
	 */
	public void setPaymentModality(Integer paymentModality) {
		this.paymentModality = paymentModality;
	}

	/**
	 * Gets the payment first expiration date.
	 *
	 * @return the payment first expiration date
	 */
	public Date getPaymentFirstExpirationDate() {
		return paymentFirstExpirationDate;
	}

	/**
	 * Sets the payment first expiration date.
	 *
	 * @param paymentFirstExpirationDate the new payment first expiration date
	 */
	public void setPaymentFirstExpirationDate(Date paymentFirstExpirationDate) {
		this.paymentFirstExpirationDate = paymentFirstExpirationDate;
	}

	/**
	 * Gets the number payments.
	 *
	 * @return the number payments
	 */
	public Integer getNumberPayments() {
		return numberPayments;
	}

	/**
	 * Sets the number payments.
	 *
	 * @param numberPayments the new number payments
	 */
	public void setNumberPayments(Integer numberPayments) {
		this.numberPayments = numberPayments;
	}

	/**
	 * Sets the data from amo payment schedule.
	 *
	 * @param amoPaymentSchedule the amo payment schedule
	 * @param loggerUser the logger user
	 * @param registryType the registry type
	 */
	public void setDataFromAmoPaymentSchedule(AmortizationPaymentSchedule amoPaymentSchedule, LoggerUser loggerUser, Integer registryType){
		this.setAudit(loggerUser);
		
		this.setAmortizationPaymentSchedule(amoPaymentSchedule);
		this.setPaymentModality( amoPaymentSchedule.getPaymentModality() );
		this.setAmortizationType( amoPaymentSchedule.getAmortizationType() );
		this.setNumberPayments( amoPaymentSchedule.getNumberPayments() );
		this.setPaymentFirstExpirationDate( amoPaymentSchedule.getPaymentFirstExpirationDate() );
		this.setAmortizationFactor( amoPaymentSchedule.getAmortizationFactor() );
		this.setTaxFactor( amoPaymentSchedule.getTaxFactor() );
		this.setCalendarDays( amoPaymentSchedule.getCalendarType() );
		this.setPeriodicity( amoPaymentSchedule.getPeriodicity() );
		this.setPeriodicityDays( amoPaymentSchedule.getPeriodicityDays() );
		this.setRegistryDays( amoPaymentSchedule.getRegistryDays() );
		this.setCorporativeDays( amoPaymentSchedule.getCorporativeDays() );
		this.setScheduleState( amoPaymentSchedule.getScheduleState() );
		this.setRegistryType( registryType );
		this.setRequestState(Integer.valueOf(0));
		
		this.setRegistryDate(loggerUser.getAuditTime());
		this.setRegistryUser(loggerUser.getUserName());
	}
	
	/**
	 * Checks if is by days periodicity.
	 *
	 * @return true, if is by days periodicity
	 */
	public boolean isByDaysPeriodicity(){
		if(InterestPeriodicityType.BY_DAYS.getCode().equals( getPeriodicity() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is reject other motive selected.
	 *
	 * @return true, if is reject other motive selected
	 */
	public boolean isRejectOtherMotiveSelected(){
		if(PaymentScheduleReqHiRejectType.OTHER.getCode().equals( getRejectMotive() )){
			return true;
		}
		return false;
	}
	
	/*Amortization type start*/
	
	public boolean isProportionalAmortizationType(){
		if(AmortizationType.PROPORTIONAL.getCode().equals(getAmortizationType()))
			return true;		
		return false;
	}
	
	public boolean isNotProportionalAmortizationType(){
		if(AmortizationType.NO_PROPORTIONAL.getCode().equals(getAmortizationType()))
			return true;
		return false;
	}
	
	/*Amortization type end*/
	
	
	/*payment modality start*/
	
	public boolean isPartialPaymentModality(){
		if(CapitalPaymentModalityType.PARTIAL.getCode().equals( getPaymentModality() )){
			return true;
		}
		return false;
	}
	
	public boolean isAtMaturityPaymentModality(){
		if(CapitalPaymentModalityType.AT_MATURITY.getCode().equals( getPaymentModality() )){
			return true;
		}
		return false;
	}
	
	/*payment modality end*/
	
}