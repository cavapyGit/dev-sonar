package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "SECURITY_REMOVED_HISTORY")
@NamedQueries({
    @NamedQuery(name = "SecurityRemovedHistory.findAll", query = "SELECT s FROM SecurityRemovedHistory s"),
    @NamedQuery(name = "SecurityRemovedHistory.findByIdSecurityRemovedHisPk", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.idSecurityRemovedHisPk = :idSecurityRemovedHisPk"),
    @NamedQuery(name = "SecurityRemovedHistory.findByIdSecurityCodePk", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.idSecurityCodePk = :idSecurityCodePk"),
    @NamedQuery(name = "SecurityRemovedHistory.findByIssuanceDate", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.issuanceDate = :issuanceDate"),
    @NamedQuery(name = "SecurityRemovedHistory.findByExpirationDate", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.expirationDate = :expirationDate"),
    @NamedQuery(name = "SecurityRemovedHistory.findByCurrency", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.currency = :currency"),
    @NamedQuery(name = "SecurityRemovedHistory.findByNominalValue", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.nominalValue = :nominalValue"),
    @NamedQuery(name = "SecurityRemovedHistory.findByInterestRateNominal", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.interestRateNominal = :interestRateNominal"),
    @NamedQuery(name = "SecurityRemovedHistory.findByCouponNumber", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.couponNumber = :couponNumber"),
    @NamedQuery(name = "SecurityRemovedHistory.findByMotiveRemoved", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.motiveRemoved = :motiveRemoved"),
    @NamedQuery(name = "SecurityRemovedHistory.findByRegistryDate", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.registryDate = :registryDate"),
    @NamedQuery(name = "SecurityRemovedHistory.findByStateSecurityRemoved", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.stateSecurityRemoved = :stateSecurityRemoved"),
    @NamedQuery(name = "SecurityRemovedHistory.findByLastModifyUser", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "SecurityRemovedHistory.findByLastModifyDate", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "SecurityRemovedHistory.findByLastModifyIp", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "SecurityRemovedHistory.findByLastModifyApp", query = "SELECT s FROM SecurityRemovedHistory s WHERE s.lastModifyApp = :lastModifyApp")})
public class SecurityRemovedHistory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="SQ_SECURITY_REMOVED_HIS", sequenceName="SQ_SECURITY_REMOVED_HIS",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_SECURITY_REMOVED_HIS")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SECURITY_REMOVED_HIS_PK", nullable = false)
    private Long idSecurityRemovedHisPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SECURITY_CODE_PK", nullable = false, length = 23)
    private String idSecurityCodePk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISSUANCE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date issuanceDate;
    
    @Column(name = "EXPIRATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CURRENCY", nullable = false)
    private Integer currency;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOMINAL_VALUE", nullable = false, precision = 22, scale = 8)
    private BigDecimal nominalValue;
    
    @Column(name = "INTEREST_RATE_NOMINAL", precision = 12, scale = 8)
    private BigDecimal interestRateNominal;
    
    @Column(name = "COUPON_NUMBER")
    private Integer couponNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "MOTIVE_REMOVED", nullable = false, length = 1000)
    private String motiveRemoved;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE_SECURITY_REMOVED", nullable = false)
    private Integer stateSecurityRemoved;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER", nullable = false, length = 20)
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP", nullable = false, length = 20)
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP", nullable = false)
    private Long lastModifyApp;
    @JoinColumn(name = "ID_SECURITY_REMOVED_FK", referencedColumnName = "ID_SECURITY_REMOVED_PK", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SecurityRemoved idSecurityRemovedFk;

    public SecurityRemovedHistory() {
    }

    public SecurityRemovedHistory(Long idSecurityRemovedHisPk) {
        this.idSecurityRemovedHisPk = idSecurityRemovedHisPk;
    }

    public SecurityRemovedHistory(Long idSecurityRemovedHisPk, String idSecurityCodePk, Date issuanceDate, Date expirationDate, Integer currency, BigDecimal nominalValue, BigDecimal interestRateNominal, Integer couponNumber, String motiveRemoved, Date registryDate, Integer stateSecurityRemoved, String lastModifyUser, Date lastModifyDate, String lastModifyIp, Long lastModifyApp) {
        this.idSecurityRemovedHisPk = idSecurityRemovedHisPk;
        this.idSecurityCodePk = idSecurityCodePk;
        this.issuanceDate = issuanceDate;
        this.expirationDate = expirationDate;
        this.currency = currency;
        this.nominalValue = nominalValue;
        this.interestRateNominal = interestRateNominal;
        this.couponNumber = couponNumber;
        this.motiveRemoved = motiveRemoved;
        this.registryDate = registryDate;
        this.stateSecurityRemoved = stateSecurityRemoved;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdSecurityRemovedHisPk() {
        return idSecurityRemovedHisPk;
    }

    public void setIdSecurityRemovedHisPk(Long idSecurityRemovedHisPk) {
        this.idSecurityRemovedHisPk = idSecurityRemovedHisPk;
    }

    public String getIdSecurityCodePk() {
        return idSecurityCodePk;
    }

    public void setIdSecurityCodePk(String idSecurityCodePk) {
        this.idSecurityCodePk = idSecurityCodePk;
    }

    public Date getIssuanceDate() {
        return issuanceDate;
    }

    public void setIssuanceDate(Date issuanceDate) {
        this.issuanceDate = issuanceDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public BigDecimal getNominalValue() {
        return nominalValue;
    }

    public void setNominalValue(BigDecimal nominalValue) {
        this.nominalValue = nominalValue;
    }

    public BigDecimal getInterestRateNominal() {
        return interestRateNominal;
    }

    public void setInterestRateNominal(BigDecimal interestRateNominal) {
        this.interestRateNominal = interestRateNominal;
    }

    public Integer getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(Integer couponNumber) {
        this.couponNumber = couponNumber;
    }

    public String getMotiveRemoved() {
        return motiveRemoved;
    }

    public void setMotiveRemoved(String motiveRemoved) {
        this.motiveRemoved = motiveRemoved;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public Integer getStateSecurityRemoved() {
        return stateSecurityRemoved;
    }

    public void setStateSecurityRemoved(Integer stateSecurityRemoved) {
        this.stateSecurityRemoved = stateSecurityRemoved;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public SecurityRemoved getIdSecurityRemovedFk() {
        return idSecurityRemovedFk;
    }

    public void setIdSecurityRemovedFk(SecurityRemoved idSecurityRemovedFk) {
        this.idSecurityRemovedFk = idSecurityRemovedFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSecurityRemovedHisPk != null ? idSecurityRemovedHisPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecurityRemovedHistory)) {
            return false;
        }
        SecurityRemovedHistory other = (SecurityRemovedHistory) object;
        if ((this.idSecurityRemovedHisPk == null && other.idSecurityRemovedHisPk != null) || (this.idSecurityRemovedHisPk != null && !this.idSecurityRemovedHisPk.equals(other.idSecurityRemovedHisPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.SecurityRemovedHistory[ idSecurityRemovedHisPk=" + idSecurityRemovedHisPk + " ]";
    }
    
}
