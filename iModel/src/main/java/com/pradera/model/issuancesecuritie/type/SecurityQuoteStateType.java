package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.accounts.holderaccounts.type.HolderAccountBankType;

// TODO: Auto-generated Javadoc
//Tipo de emision --> 333
/**
 * The Enum IssuanceType.
 */
public enum SecurityQuoteStateType {

	/** The dematerialized. */
	REGISTERED(Integer.valueOf(1645),"REGISTRADO"),
	
	/** The physical. */
	ANNULATED(Integer.valueOf(1646),"ANULADO"),
	
	/** The mixed. */
	CONFIRMED(Integer.valueOf(1647),"CONFIRMADO");
	
	/** The Constant list. */
	public static final List<SecurityQuoteStateType> list=new ArrayList<SecurityQuoteStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SecurityQuoteStateType> lookup=new HashMap<Integer, SecurityQuoteStateType>();
	
	static {
		for(SecurityQuoteStateType e : SecurityQuoteStateType.values()){
			lookup.put(e.getCode(), e);
			list.add( e );
		}
	}
	
	/** The code. */
	private Integer  code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new issuance type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SecurityQuoteStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	public static SecurityQuoteStateType get(Integer codigo) {
		return lookup.get(codigo);
	}
	
}
