package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CFI_CATEGORY database table.
 * 
 */
@Entity
@Table(name="CFI_CATEGORY")
public class CfiCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CFI_CATEGORY_IDCATEGORYPK_GENERATOR", sequenceName="SQ_ID_CATEGORY_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFI_CATEGORY_IDCATEGORYPK_GENERATOR")
	@Column(name="ID_CATEGORY_PK")
	@NotNull
	private String idCategoryPk;

	@Column(name="DESCRIPTION_ENG")
	private String descriptionEng;

	@Column(name="DESCRIPTION_ESP")
	private String descriptionEsp;

	@Column(name="LAST_MODIFY_APP")
	private BigDecimal lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

    public CfiCategory() {
    }

	public String getIdCategoryPk() {
		return this.idCategoryPk;
	}

	public void setIdCategoryPk(String idCategoryPk) {
		this.idCategoryPk = idCategoryPk;
	}

	public String getDescriptionEng() {
		return this.descriptionEng;
	}

	public void setDescriptionEng(String descriptionEng) {
		this.descriptionEng = descriptionEng;
	}

	public String getDescriptionEsp() {
		return this.descriptionEsp;
	}

	public void setDescriptionEsp(String descriptionEsp) {
		this.descriptionEsp = descriptionEsp;
	}

	public BigDecimal getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(BigDecimal lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

}