package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantFileType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public enum IssuerFileDifferentiatedType {

	/** The certificationvalidity. */
	DOCUMENT_ORGANIZATIONAL_STRUCTURE_ENTITY(Integer.valueOf(1252),"DOCUMENTO DE ESTRUCTURA ORGANICA DE LA ENTIDAD"),

	/** The bylawsfile. */
	CERTIFICATION_BOARD_OF_ENTITY(Integer.valueOf(1253),"CERTIFICACION DEL MAXIMO ORGANO DE LA ENTIDAD"),

	/** The representativecertificationfile. */
	REPRESENTATIVECERTIFICATIONFILE(Integer.valueOf(1254),"CERTIFICACION DEL REPRESENTANTE"),

	/** The representativepassportfile. */
	IDENTITY_CARD_REPRESENTATIVE(Integer.valueOf(1255),"CEDULA DEL REPRESENTANTE"),

	/** The certificationauthorityfile. */
	CERTIFICATION_ISSUED_BY_SIV(Integer.valueOf(1256),"CERTIFICACION EMITIDA POR LA SIV");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant file type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuerFileDifferentiatedType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<IssuerFileDifferentiatedType> list = new ArrayList<IssuerFileDifferentiatedType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, IssuerFileDifferentiatedType> lookup = new HashMap<Integer, IssuerFileDifferentiatedType>();
	static {
		for (IssuerFileDifferentiatedType s : EnumSet.allOf(IssuerFileDifferentiatedType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant file foreign type
	 */
	public static IssuerFileDifferentiatedType get(Integer code) {
		return lookup.get(code);
	}

}
