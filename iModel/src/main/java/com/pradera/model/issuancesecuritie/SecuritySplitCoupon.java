package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name = "SECURITY_SPLIT_COUPON")
public class SecuritySplitCoupon implements Serializable, Auditable, Cloneable{

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SECURITY_SPLIT_COUPON_PK_GENERATOR", sequenceName="SQ_ID_SECURITY_SPLIT_COUPON ",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_SPLIT_COUPON_PK_GENERATOR")
	@Column(name = "ID_SECURITY_SPLIT_COUPON_PK")
	private Long idSecuritySplitCouponPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_ISIN_CODE_FK")
	private Security securities;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_PROGRAM_INTEREST_FK")
	private ProgramInterestCoupon programInterestCoupon;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_PROGRAM_AMORTIZATION_FK")
	private ProgramAmortizationCoupon programAmortizationCoupon;
	
	@Column(name = "COUPON_STATE")
	private Integer couponState;
	
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	
	
	public SecuritySplitCoupon() {
		super();
	}

	
	public Long getIdSecuritySplitCouponPk() {
		return idSecuritySplitCouponPk;
	}



	public void setIdSecuritySplitCouponPk(Long idSecuritySplitCouponPk) {
		this.idSecuritySplitCouponPk = idSecuritySplitCouponPk;
	}



	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public ProgramInterestCoupon getProgramInterestCoupon() {
		return programInterestCoupon;
	}

	public void setProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon) {
		this.programInterestCoupon = programInterestCoupon;
	}

	public Integer getCouponState() {
		return couponState;
	}

	public void setCouponState(Integer couponState) {
		this.couponState = couponState;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	
	@Override
    public void setAudit(LoggerUser loggerUser) {
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();

        return detailsMap;
	}
	
	public SecuritySplitCoupon clone() throws CloneNotSupportedException {
        return (SecuritySplitCoupon) super.clone();
    }

	public ProgramAmortizationCoupon getProgramAmortizationCoupon() {
		return programAmortizationCoupon;
	}

	public void setProgramAmortizationCoupon(
			ProgramAmortizationCoupon programAmortizationCoupon) {
		this.programAmortizationCoupon = programAmortizationCoupon;
	}
	
	
}
