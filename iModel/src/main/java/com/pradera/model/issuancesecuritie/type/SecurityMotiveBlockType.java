package com.pradera.model.issuancesecuritie.type;

public enum SecurityMotiveBlockType {

	OTHER(Integer.valueOf(1955)),
	GUARDA_EXCLUSIVA(Integer.valueOf(2876)),
	GUARDA_MIXTA(Integer.valueOf(2877));
	
	private Integer code;
	
	
	/**
	 * Instantiates a new security motive unblock type.
	 *
	 * @param code the code
	 */
	private SecurityMotiveBlockType(Integer code){
		this.code=code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
}
