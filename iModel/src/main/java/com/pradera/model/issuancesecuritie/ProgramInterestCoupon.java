package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.coupongrant.CouponGrantOperation;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;


/**
 * The persistent class for the PROGRAM_INTEREST_COUPON database table.
 * 
 */
@Entity
@Table(name="PROGRAM_INTEREST_COUPON")
public class ProgramInterestCoupon implements Serializable, Auditable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="PROGRAM_INTEREST_COUPON_IDPROGRAMINTERESTPK_GENERATOR", sequenceName="SQ_ID_PROG_INT_COUPON_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROGRAM_INTEREST_COUPON_IDPROGRAMINTERESTPK_GENERATOR")
	@Column(name="ID_PROGRAM_INTEREST_PK")
	private Long idProgramInterestPk;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CORPORATIVE_DATE")
	private Date corporativeDate;

	@Column(name="COUPON_NUMBER")
	private Integer couponNumber;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CUTOFF_DATE")
	private Date cutoffDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPERITATION_DATE")
	private Date experitationDate;

	@Column(name="FINANTIAL_INDEX_RATE")
	private BigDecimal finantialIndexRate;

	@Column(name="IND_ROUNDING")
	private Integer indRounding;

	@Column(name="INTEREST_FACTOR")
	private BigDecimal interestFactor;

	@Column(name="INTEREST_RATE")
	private BigDecimal interestRate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

	@Column(name="PAYMENT_DAYS")
	private Integer paymentDays;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="STATE_PROGRAM_INTEREST")
	private Integer stateProgramInterest;
	
	@Column(name="SITUATION_PROGRAM_INTEREST")
	private Integer situationProgramInterest;

	@Column(name="PAYED_AMOUNT")
    private BigDecimal payedAmount;

	@Column(name="COUPON_AMOUNT")
	private BigDecimal couponAmount;
	
	/** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="EXCHANGE_DATE_RATE")
	private Date exchangeDateRate;
    
	/** The market rate. */
	@Column(name="CURRENCY_PAYMENT")
	private Integer currencyPayment;
	
	//bi-directional many-to-one association to InterestPaymentCronogram
    @ManyToOne
	@JoinColumn(name="ID_INT_PAYMENT_SCHEDULE_FK")
    @NotNull
	private InterestPaymentSchedule interestPaymentSchedule;
    
//    @OneToMany(mappedBy = "programInterestCoupon", fetch=FetchType.LAZY)
//    private List<CorporativeOperation> corporativeOperations;
	@OneToOne(mappedBy="programInterestCoupon",cascade=CascadeType.DETACH,fetch=FetchType.LAZY)
    private CorporativeOperation corporativeOperation; 
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SPLIT_SECURITY_CODE_FK")
	private Security splitSecurities;
	
	@OneToMany(mappedBy = "programInterestCoupon")
	private List<CouponGrantOperation> couponGrantOperations;
	
	private Integer currency;
	
    /** The is selected. */
    @Transient
    private Boolean isSelected;
    
    @Transient
    private String dataRowDtbInterestPaymentSchedule;
    
    @Transient
    private boolean lastCoupon;

    public ProgramInterestCoupon() {
    }

	public Long getIdProgramInterestPk() {
		return this.idProgramInterestPk;
	}

	public void setIdProgramInterestPk(Long idProgramInterestPk) {
		this.idProgramInterestPk = idProgramInterestPk;
	}

	public Date getBeginingDate() {
		return this.beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public Date getCorporativeDate() {
		return this.corporativeDate;
	}

	public void setCorporativeDate(Date corporativeDate) {
		this.corporativeDate = corporativeDate;
	}

	public Integer getCouponNumber() {
		return this.couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Integer getCurrency() {
		return this.currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Date getCutoffDate() {
		return this.cutoffDate;
	}

	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}

	public Date getExperitationDate() {
		return this.experitationDate;
	}

	public void setExperitationDate(Date experitationDate) {
		this.experitationDate = experitationDate;
	}

	public BigDecimal getFinantialIndexRate() {
		return this.finantialIndexRate;
	}

	public void setFinantialIndexRate(BigDecimal finantialIndexRate) {
		this.finantialIndexRate = finantialIndexRate;
	}

	public Integer getIndRounding() {
		return this.indRounding;
	}

	public void setIndRounding(Integer indRounding) {
		this.indRounding = indRounding;
	}

	public BigDecimal getInterestFactor() {
		return this.interestFactor;
	}

	public void setInterestFactor(BigDecimal interestFactor) {
		this.interestFactor = interestFactor;
	}

	public BigDecimal getInterestRate() {
		return this.interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	

	public Integer getPaymentDays() {
		return paymentDays;
	}

	public void setPaymentDays(Integer paymentDays) {
		this.paymentDays = paymentDays;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Integer getStateProgramInterest() {
		return this.stateProgramInterest;
	}

	public void setStateProgramInterest(Integer stateProgramInterest) {
		this.stateProgramInterest = stateProgramInterest;
	}

	public InterestPaymentSchedule getInterestPaymentCronogram() {
		return this.interestPaymentSchedule;
	}

	public void setInterestPaymentCronogram(InterestPaymentSchedule interestPaymentSchedule) {
		this.interestPaymentSchedule = interestPaymentSchedule;
	}
	
	public Security getSplitSecurities() {
		return splitSecurities;
	}

	public void setSplitSecurities(Security splitSecurities) {
		this.splitSecurities = splitSecurities;
	}

//	public List<CorporativeOperation> getCorporativeOperations() {
//		return corporativeOperations;
//	}
//
//	public void setCorporativeOperations(
//			List<CorporativeOperation> corporativeOperations) {
//		this.corporativeOperations = corporativeOperations;
//	}

	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	public BigDecimal getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(BigDecimal couponAmount) {
		this.couponAmount = couponAmount;
	}

	public String getStateDescription(){
		if(stateProgramInterest!=null){
			return ProgramScheduleStateType.get(stateProgramInterest).getValue();
		}
		return "";
	}
	
	/**
	 * @return the couponGrantOperations
	 */
	public List<CouponGrantOperation> getCouponGrantOperations() {
		return couponGrantOperations;
	}

	/**
	 * @param couponGrantOperations the couponGrantOperations to set
	 */
	public void setCouponGrantOperations(
			List<CouponGrantOperation> couponGrantOperations) {
		this.couponGrantOperations = couponGrantOperations;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param idProgramInterestPk
	 * @param couponNumber
	 * @param interestRate
	 * @param paymentDate
	 */
	public ProgramInterestCoupon(Long idProgramInterestPk,
			Integer couponNumber, BigDecimal interestRate, Date paymentDate, Integer stateProgramInterest, Date experitationDate) {
		this.idProgramInterestPk = idProgramInterestPk;
		this.couponNumber = couponNumber;
		this.interestRate = interestRate;
		this.paymentDate = paymentDate;
		this.stateProgramInterest = stateProgramInterest;
		this.experitationDate = experitationDate;
	}
	
	public ProgramInterestCoupon(Long idProgramInterestPk,
			Integer couponNumber, BigDecimal interestRate, Date paymentDate, 
			Integer stateProgramInterest, Date experitationDate,Date registryDate , Date beginingDate) {
		this.idProgramInterestPk = idProgramInterestPk;
		this.couponNumber = couponNumber;
		this.interestRate = interestRate;
		this.paymentDate = paymentDate;
		this.stateProgramInterest = stateProgramInterest;
		this.experitationDate = experitationDate;
		this.registryDate = registryDate;
		this.beginingDate = beginingDate;
	}

	public ProgramInterestCoupon(Long idProgramInterestPk) {
		this.idProgramInterestPk = idProgramInterestPk;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	public BigDecimal getPayedAmount() {
		return payedAmount;
	}

	public void setPayedAmount(BigDecimal payedAmount) {
		this.payedAmount = payedAmount;
	}

	
	public String getDataRowDtbInterestPaymentSchedule() {
		dataRowDtbInterestPaymentSchedule= "ProgramInterestCoupon [idProgramInterestPk="
				+ idProgramInterestPk + ", beginingDate=" + beginingDate
				+ ", couponNumber=" + couponNumber + ", experitationDate="
				+ experitationDate + ", interestRate=" + interestRate
				+ ", paymentDate=" + paymentDate + ", registryDate="
				+ registryDate + ", stateProgramInterest="
				+ stateProgramInterest + ", payedAmount=" + payedAmount + "]";
		return dataRowDtbInterestPaymentSchedule;
	}
	
	public void setDataRowDtbInterestPaymentSchedule(String dataRowDtbInterestPaymentSchedule){
		this.dataRowDtbInterestPaymentSchedule=dataRowDtbInterestPaymentSchedule;
	}
	
	/* State indicators start*/
	
	public boolean isPendingState(){
		if(ProgramScheduleStateType.PENDING.getCode().equals( this.getStateProgramInterest() ))
			return true;
		return false;
	}
	
	public boolean isPaidState(){
		if(ProgramScheduleStateType.EXPIRED.getCode().equals( this.getStateProgramInterest() )){
			return true;
		}
		return false;
	}
	
	public boolean isFirstCoupon(){
		if(Integer.valueOf(1).compareTo(getCouponNumber())==0){
			return true;
		}
		return false;
	}
	
	
	
	/* State indicators end*/
	
	public boolean isLastCoupon() {
		return lastCoupon;
	}

	public void setLastCoupon(boolean lastCoupon) {
		this.lastCoupon = lastCoupon;
	}

	public void setDataFromProgramIntCouponReqHi(ProgramIntCouponReqHi programIntCouponReqHi, LoggerUser loggerUser){
		this.setAudit(loggerUser);
		
		this.setCouponNumber( programIntCouponReqHi.getCouponNumber() );
		this.setBeginingDate( programIntCouponReqHi.getBeginingDate() );
		this.setExperitationDate( programIntCouponReqHi.getExpirationDate() );
		this.setRegistryDate( programIntCouponReqHi.getRegistryDate() );
		
		this.setCutoffDate( programIntCouponReqHi.getCutoffDate() );
		this.setCorporativeDate( programIntCouponReqHi.getCorporativeDate() );
		this.setPaymentDate(programIntCouponReqHi.getPaymentDate());
		this.setPaymentDays(programIntCouponReqHi.getPaymentDays());
		this.setCurrency(programIntCouponReqHi.getCurrency() );
		this.setFinantialIndexRate( programIntCouponReqHi.getFinantialIndexRate() );
		this.setInterestFactor(programIntCouponReqHi.getInterestFactor());
		this.setInterestRate(programIntCouponReqHi.getInterestRate());
		this.setIndRounding( programIntCouponReqHi.getIndRounding() );
		this.setStateProgramInterest(programIntCouponReqHi.getStateProgramInterest());
		this.setPayedAmount( programIntCouponReqHi.getPayedAmount() );
		this.setCouponAmount( programIntCouponReqHi.getCouponAmount() );
		
		this.setExchangeDateRate(programIntCouponReqHi.getExchangeDateRate());
		this.setCurrencyPayment(programIntCouponReqHi.getCurrencyPayment());
	}

	public Integer getSituationProgramInterest() {
		return situationProgramInterest;
	}

	public void setSituationProgramInterest(Integer situationProgramInterest) {
		this.situationProgramInterest = situationProgramInterest;
	}

	public Date getExchangeDateRate() {
		return exchangeDateRate;
	}

	public void setExchangeDateRate(Date exchangeDateRate) {
		this.exchangeDateRate = exchangeDateRate;
	}

	public Integer getCurrencyPayment() {
		return currencyPayment;
	}

	public void setCurrencyPayment(Integer currencyPayment) {
		this.currencyPayment = currencyPayment;
	}
	
	
	
}