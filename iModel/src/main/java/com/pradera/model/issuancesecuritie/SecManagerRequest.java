package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.type.IssuerBlockRequestMotiveType;
import com.pradera.model.accounts.type.IssuerRequestStateType;
import com.pradera.model.accounts.type.IssuerRequestType;
import com.pradera.model.accounts.type.IssuerUnblockRequestMotiveType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ISSUER_REQUEST database table.
 * 
 */
@Entity
@Table(name="SEC_MANAGER__REQUEST")
public class SecManagerRequest implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id issuer request pk. */
	@Id
	@SequenceGenerator(name="SEC_MAN_REQUEST_IDSECMANREQUESTPK_GENERATOR", sequenceName="SQ_ID_SEC_MAN_REQUEST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEC_MAN_REQUEST_IDSECMANREQUESTPK_GENERATOR")
	@Column(name="ID_SEC_MANAGER_REQUEST_PK")
	private Long idSecManagerRequestPk;

    /** The action date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="ACTION_DATE")
	private Date actionDate;

	/** The action motive. */
	@Column(name="ACTION_MOTIVE")
	private Integer actionMotive;

	/** The action other motive. */
	@Column(name="ACTION_OTHER_MOTIVE")
	private String actionOtherMotive;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The request motive. */
	@Column(name="REQUEST_MOTIVE")
	private Integer requestMotive;

	/** The request number. */
	@Column(name="REQUEST_NUMBER")
	private Long requestNumber;

	/** The request other motive. */
	@Column(name="REQUEST_OTHER_MOTIVE")
	private String requestOtherMotive;

	/** The request type. */
	@Column(name="REQUEST_TYPE")
	private Integer requestType;

	/** The state. */
	@Column(name="STATE")
	private Integer state;
	
	//bi-directional many-to-one association to Issuer
    /** The issuer. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITIES_MANAGER_FK")
	private SecuritiesManager securitiesManager;
    
    //bi-directional one-to-one association to IssuerModification
  	/** The issuer files. */
    @OneToMany(mappedBy="secManagerRequest",cascade=CascadeType.ALL)
  	private List<ManagerFile> managerFiles;

	//bi-directional one-to-one association to IssuerModification
	/** The issuer history states. */
	@OneToMany(mappedBy="issuerRequest",cascade=CascadeType.ALL)
	private List<IssuerHistoryState> issuerHistoryStates;
	
	/** The issuer file histories. */
	@OneToMany(mappedBy="secManagerRequest",cascade=CascadeType.ALL)
  	private List<ManagerFileHistory> managerFileHistories;
	
	/** The issuer histories. */
	@OneToMany(mappedBy="secManagerRequest")
  	private List<SecManagerHistory> secManagerHistories;
	
	/** The observations. */
	@Column(name="COMMENTS")
	private String observations;
	
	@Transient
	private String businessName;

	@Transient
	private String mnemonic;

	
	/**
     * Instantiates a new issuer request.
     */
    public SecManagerRequest() {
    	securitiesManager=new SecuritiesManager();
    }
    
    /**
     * Gets the request type description.
     *
     * @return the request type description
     */
    public String getRequestTypeDescription(){
    	if(Validations.validateIsNotNullAndPositive(requestType)){
    		return IssuerRequestType.get(requestType).getValue();
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the request motive description.
     *
     * @return the request motive description
     */
    public String getRequestMotiveDescription(){
    	if(Validations.validateIsNotNullAndPositive(requestMotive) && 
    			Validations.validateIsNotNullAndPositive(requestType)){
    		if(IssuerRequestType.BLOCK.getCode().equals(requestType)){
    			return IssuerBlockRequestMotiveType.get(requestMotive).getValue();
    		} else if(IssuerRequestType.UNBLOCK.getCode().equals(requestType)){
    			return IssuerUnblockRequestMotiveType.get(requestMotive).getValue();
    		}/*  else if(IssuerBlockRequestTypeType.ANNULMENT.getCode().equals(requestType)){
    			return ParticipantAnnulMotiveType.get(requestMotive).getValue();
    		}*/ else {
    			return null;
    		}
    		
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the issuer request state description.
     *
     * @return the issuer request state description
     */
    public String getIssuerRequestStateDescription(){
    	if(Validations.validateIsNotNullAndPositive(state)){
    		return IssuerRequestStateType.get(state).getValue();
    	} else {
    		return null;
    	}
    }


	/**
	 * Gets the action date.
	 *
	 * @return the action date
	 */
	public Date getActionDate() {
		return actionDate;
	}

	/**
	 * Sets the action date.
	 *
	 * @param actionDate the new action date
	 */
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 * Gets the action motive.
	 *
	 * @return the action motive
	 */
	public Integer getActionMotive() {
		return actionMotive;
	}

	/**
	 * Sets the action motive.
	 *
	 * @param actionMotive the new action motive
	 */
	public void setActionMotive(Integer actionMotive) {
		this.actionMotive = actionMotive;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the request motive.
	 *
	 * @return the request motive
	 */
	public Integer getRequestMotive() {
		return requestMotive;
	}

	/**
	 * Sets the request motive.
	 *
	 * @param requestMotive the new request motive
	 */
	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the request other motive.
	 *
	 * @return the request other motive
	 */
	public String getRequestOtherMotive() {
		return requestOtherMotive;
	}

	/**
	 * Sets the request other motive.
	 *
	 * @param requestOtherMotive the new request other motive
	 */
	public void setRequestOtherMotive(String requestOtherMotive) {
		this.requestOtherMotive = requestOtherMotive;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	public SecuritiesManager getSecuritiesManager() {
		return securitiesManager;
	}

	public void setSecuritiesManager(SecuritiesManager securitiesManager) {
		this.securitiesManager = securitiesManager;
	}

	public Long getIdSecManagerRequestPk() {
		return idSecManagerRequestPk;
	}

	public void setIdSecManagerRequestPk(Long idSecManagerRequestPk) {
		this.idSecManagerRequestPk = idSecManagerRequestPk;
	}

	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}

	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}

	/**
	 * Gets the issuer history states.
	 *
	 * @return the issuer history states
	 */
	public List<IssuerHistoryState> getIssuerHistoryStates() {
		return issuerHistoryStates;
	}

	/**
	 * Sets the issuer history states.
	 *
	 * @param issuerHistoryStates the new issuer history states
	 */
	public void setIssuerHistoryStates(List<IssuerHistoryState> issuerHistoryStates) {
		this.issuerHistoryStates = issuerHistoryStates;
	}

	/**
	 * Gets the action other motive.
	 *
	 * @return the action other motive
	 */
	public String getActionOtherMotive() {
		return actionOtherMotive;
	}

	/**
	 * Sets the action other motive.
	 *
	 * @param actionOtherMotive the new action other motive
	 */
	public void setActionOtherMotive(String actionOtherMotive) {
		this.actionOtherMotive = actionOtherMotive;
	}		

	
	
	

	public List<ManagerFile> getManagerFiles() {
		return managerFiles;
	}

	public void setManagerFiles(List<ManagerFile> managerFiles) {
		this.managerFiles = managerFiles;
	}

	public List<ManagerFileHistory> getManagerFileHistories() {
		return managerFileHistories;
	}

	public void setManagerFileHistories(
			List<ManagerFileHistory> managerFileHistories) {
		this.managerFileHistories = managerFileHistories;
	}

	
	

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public List<SecManagerHistory> getSecManagerHistories() {
		return secManagerHistories;
	}

	public void setSecManagerHistory(List<SecManagerHistory> secManagerHistories) {
		this.secManagerHistories = secManagerHistories;
	}

	public void setSecManagerHistories(List<SecManagerHistory> secManagerHistories) {
		this.secManagerHistories = secManagerHistories;
	}

	/* (non-Javadoc)
	 * @see com.arkin.commons.audit.Auditable#setAudit(com.arkin.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.arkin.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap =
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("managerFiles", managerFiles);
        detailsMap.put("issuerHistoryStates", issuerHistoryStates);
        detailsMap.put("managerFileHistories", managerFileHistories);
        return detailsMap;
	}
    	
}