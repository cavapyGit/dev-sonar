/*
 * 
 */
package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.CalendarType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleStateType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the INTEREST_PAYMENT_CRONOGRAM database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/04/2014
 */
@Entity
@NamedQueries({
	@NamedQuery(name = InterestPaymentSchedule.FIND_BY_SECURITY_AND_STATE, 
			query = "SELECT i FROM InterestPaymentSchedule i WHERE i.security.idSecurityCodePk = :idSecurityPrm and i.scheduleState=:statePrm")
})
@Table(name="INTEREST_PAYMENT_SCHEDULE")
public class InterestPaymentSchedule implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_SECURITY_AND_STATE="InterestPaymentSchedule.searchBySecurityAndState";
							
	/** The id int payment schedule pk. */
	@Id
	@SequenceGenerator(name="INTEREST_PAYMENT_SCHEDULE_IDINTPAYMENTSCHEDULEPK_GENERATOR", sequenceName="SQ_ID_INT_PAYMENT_SCHEDULE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INTEREST_PAYMENT_SCHEDULE_IDINTPAYMENTSCHEDULEPK_GENERATOR")
	@Column(name="ID_INT_PAYMENT_SCHEDULE_PK")
	private Long idIntPaymentSchedulePk;


	/** The schedule state. */
	@Column(name="SCHEDULE_STATE")
	private Integer scheduleState;

    /** The expiration date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

	/** The financial index. */
	@Column(name="FINANCIAL_INDEX")
	private Integer financialIndex;

	/** The security. */
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	/** The interest factor. */
	@Column(name="INTEREST_FACTOR")
	private BigDecimal interestFactor;

	/** The interest periodicity. */
	@Column(name="INTEREST_PERIODICITY")
	private Integer interestPeriodicity;

	/** The interest type. */
	@Column(name="INTEREST_TYPE")
	private Integer interestType;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The periodicity. */
	@Column(name="PERIODICITY")
	private Integer periodicity;
	
	/** The periodicity days. */
	@Column(name="PERIODICITY_DAYS")
	private Integer periodicityDays;
	
	/** The yield. */
	@Column(name="YIELD")
	private Integer yield;
	
	/** The calendar type. */
	@Column(name="CALENDAR_TYPE")
	private Integer calendarType;

	/** The registry days. */
	@Column(name="REGISTRY_DAYS")
	private Integer registryDays;

	/** The tax factor. */
	@Column(name="TAX_FACTOR")
	private BigDecimal taxFactor;
	
	/** The cash nominal. */
	@Column(name="CASH_NOMINAL")
	private Integer cashNominal;
	
	/** The spread. */
	private BigDecimal spread;
	
	/** The calendar month. */
	@Column(name="CALENDAR_MONTH")
	private Integer calendarMonth;
	
	/** The calendar days. */
	@Column(name="CALENDAR_DAYS")
	private Integer calendarDays;
	
	/** The rate type. */
	@Column(name="RATE_TYPE")
	private Integer rateType;
	
	/** The rate value. */
	@Column(name="RATE_VALUE")
	private BigDecimal rateValue;
	
	/** The operation type. */
	@Column(name="OPERATION_TYPE")
	private Integer operationType;
	
	/** The minimum rate. */
	@Column(name="MINIMUM_RATE")
	private BigDecimal minimumRate;
	
	/** The maximum rate. */
	@Column(name="MAXIMUM_RATE")
	private BigDecimal maximumRate;
	

	/** The int payment schedule req his. */
	@OneToMany(mappedBy="interestPaymentSchedule",fetch=FetchType.LAZY,cascade=CascadeType.ALL ) //,orphanRemoval=true
	private List<IntPaymentScheduleReqHi> intPaymentScheduleReqHis;
	
	//bi-directional many-to-one association to CronogramRestruncturing
	/** The cronogram restructurings. */
	@OneToMany(mappedBy="interestPaymentSchedule",fetch=FetchType.LAZY)
	private List<CronogramRestructuring> cronogramRestructurings;

	//bi-directional many-to-one association to ProgramInterestCoupon
	/** The program interest coupons. */
	@OneToMany(mappedBy="interestPaymentSchedule",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<ProgramInterestCoupon> programInterestCoupons;
	
	/** The is all coupons pending payment. */
	@Transient
	private boolean isAllCouponsPendingPayment;
	@Transient
	private String cashNominalDesc;
	@Transient
	private String periodicityDesc;
	@Transient
	private String calendarTypeDesc;
	@Transient
	private String yieldDesc;
	@Transient
	private String interestPeriodicityDesc;
	@Transient
	private boolean selected;

    /**
     * Instantiates a new interest payment schedule.
     */
    public InterestPaymentSchedule() {
    	programInterestCoupons=new ArrayList<ProgramInterestCoupon>();
    }

	/**
	 * Gets the id int payment schedule pk.
	 *
	 * @return the id int payment schedule pk
	 */
	public Long getIdIntPaymentSchedulePk() {
		return idIntPaymentSchedulePk;
	}

	/**
	 * Sets the id int payment schedule pk.
	 *
	 * @param idIntPaymentSchedulePk the new id int payment schedule pk
	 */
	public void setIdIntPaymentSchedulePk(Long idIntPaymentSchedulePk) {
		this.idIntPaymentSchedulePk = idIntPaymentSchedulePk;
	}
	

	/**
	 * Gets the schedule state.
	 *
	 * @return the schedule state
	 */
	public Integer getScheduleState() {
		return scheduleState;
	}



	/**
	 * Sets the schedule state.
	 *
	 * @param scheduleState the new schedule state
	 */
	public void setScheduleState(Integer scheduleState) {
		this.scheduleState = scheduleState;
	}



	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return this.expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the financial index.
	 *
	 * @return the financial index
	 */
	public Integer getFinancialIndex() {
		return this.financialIndex;
	}

	/**
	 * Sets the financial index.
	 *
	 * @param financialIndex the new financial index
	 */
	public void setFinancialIndex(Integer financialIndex) {
		this.financialIndex = financialIndex;
	}

	

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the cronogram restructurings.
	 *
	 * @return the cronogram restructurings
	 */
	public List<CronogramRestructuring> getCronogramRestructurings() {
		return cronogramRestructurings;
	}

	/**
	 * Sets the cronogram restructurings.
	 *
	 * @param cronogramRestructurings the new cronogram restructurings
	 */
	public void setCronogramRestructurings(
			List<CronogramRestructuring> cronogramRestructurings) {
		this.cronogramRestructurings = cronogramRestructurings;
	}

	/**
	 * Gets the interest factor.
	 *
	 * @return the interest factor
	 */
	public BigDecimal getInterestFactor() {
		return this.interestFactor;
	}

	/**
	 * Sets the interest factor.
	 *
	 * @param interestFactor the new interest factor
	 */
	public void setInterestFactor(BigDecimal interestFactor) {
		this.interestFactor = interestFactor;
	}

	/**
	 * Gets the interest periodicity.
	 *
	 * @return the interest periodicity
	 */
	public Integer getInterestPeriodicity() {
		return this.interestPeriodicity;
	}

	/**
	 * Sets the interest periodicity.
	 *
	 * @param interestPeriodicity the new interest periodicity
	 */
	public void setInterestPeriodicity(Integer interestPeriodicity) {
		this.interestPeriodicity = interestPeriodicity;
	}

	/**
	 * Gets the interest type.
	 *
	 * @return the interest type
	 */
	public Integer getInterestType() {
		return this.interestType;
	}

	/**
	 * Sets the interest type.
	 *
	 * @param interestType the new interest type
	 */
	public void setInterestType(Integer interestType) {
		this.interestType = interestType;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the periodicity.
	 *
	 * @return the periodicity
	 */
	public Integer getPeriodicity() {
		return this.periodicity;
	}

	/**
	 * Sets the periodicity.
	 *
	 * @param periodicity the new periodicity
	 */
	public void setPeriodicity(Integer periodicity) {
		this.periodicity = periodicity;
	}

	/**
	 * Gets the registry days.
	 *
	 * @return the registry days
	 */
	public Integer getRegistryDays() {
		return this.registryDays;
	}

	/**
	 * Sets the registry days.
	 *
	 * @param registryDays the new registry days
	 */
	public void setRegistryDays(Integer registryDays) {
		this.registryDays = registryDays;
	}

	/**
	 * Gets the tax factor.
	 *
	 * @return the tax factor
	 */
	public BigDecimal getTaxFactor() {
		return this.taxFactor;
	}

	/**
	 * Sets the tax factor.
	 *
	 * @param taxFactor the new tax factor
	 */
	public void setTaxFactor(BigDecimal taxFactor) {
		this.taxFactor = taxFactor;
	}

	/**
	 * Gets the cronogram restruncturings.
	 *
	 * @return the cronogram restruncturings
	 */
	public List<CronogramRestructuring> getCronogramRestruncturings() {
		return this.cronogramRestructurings;
	}

	/**
	 * Sets the cronogram restruncturings.
	 *
	 * @param cronogramRestructurings the new cronogram restruncturings
	 */
	public void setCronogramRestruncturings(List<CronogramRestructuring> cronogramRestructurings) {
		this.cronogramRestructurings = cronogramRestructurings;
	}
	
	/**
	 * Gets the program interest coupons.
	 *
	 * @return the program interest coupons
	 */
	public List<ProgramInterestCoupon> getProgramInterestCoupons() {
		return this.programInterestCoupons;
	}

	/**
	 * Sets the program interest coupons.
	 *
	 * @param programInterestCoupons the new program interest coupons
	 */
	public void setProgramInterestCoupons(List<ProgramInterestCoupon> programInterestCoupons) {
		this.programInterestCoupons = programInterestCoupons;
	}

	
	
	/**
	 * Gets the int payment schedule req his.
	 *
	 * @return the int payment schedule req his
	 */
	public List<IntPaymentScheduleReqHi> getIntPaymentScheduleReqHis() {
		return intPaymentScheduleReqHis;
	}



	/**
	 * Sets the int payment schedule req his.
	 *
	 * @param intPaymentScheduleReqHis the new int payment schedule req his
	 */
	public void setIntPaymentScheduleReqHis(
			List<IntPaymentScheduleReqHi> intPaymentScheduleReqHis) {
		this.intPaymentScheduleReqHis = intPaymentScheduleReqHis;
	}



	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();

        	if(programInterestCoupons!=null) {
        		for(ProgramInterestCoupon current: programInterestCoupons) {
        			current.setAudit(loggerUser);
        		}
        	}
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("programInterestCoupons", programInterestCoupons);
        return detailsMap;
	}



	/**
	 * Gets the calendar type.
	 *
	 * @return the calendar type
	 */
	public Integer getCalendarType() {
		return calendarType;
	}



	/**
	 * Sets the calendar type.
	 *
	 * @param calendarType the new calendar type
	 */
	public void setCalendarType(Integer calendarType) {
		this.calendarType = calendarType;
	}



	/**
	 * Gets the yield.
	 *
	 * @return the yield
	 */
	public Integer getYield() {
		return yield;
	}



	/**
	 * Sets the yield.
	 *
	 * @param yield the new yield
	 */
	public void setYield(Integer yield) {
		this.yield = yield;
	}




	/**
	 * Gets the spread.
	 *
	 * @return the spread
	 */
	public BigDecimal getSpread() {
		return spread;
	}



	/**
	 * Sets the spread.
	 *
	 * @param spread the new spread
	 */
	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}



	/**
	 * Gets the cash nominal.
	 *
	 * @return the cash nominal
	 */
	public Integer getCashNominal() {
		return cashNominal;
	}



	/**
	 * Sets the cash nominal.
	 *
	 * @param cashNominal the new cash nominal
	 */
	public void setCashNominal(Integer cashNominal) {
		this.cashNominal = cashNominal;    
	}
	
	
	/**
	 * Gets the calendar days description.
	 *
	 * @return the calendar days description
	 */
	public String getCalendarDaysDescription(){
		if(calendarType!=null){
			return CalendarType.lookup.get(calendarType).getText1();
		}
		return "";
	}
	
	/**
	 * Gets the interest type description.
	 *
	 * @return the interest type description
	 */
	public String getInterestTypeDescription(){
//		if(Validations.validateIsNotNullAndNotEmpty(interestType)){
//			return InterestType.get(interestType).getValue();
//		}
		return "";
	}

	/**
	 * Checks if is all coupons pending payment.
	 *
	 * @return true, if is all coupons pending payment
	 */
	public boolean isAllCouponsPendingPayment() {
		return isAllCouponsPendingPayment;
	}



	/**
	 * Sets the all coupons pending payment.
	 *
	 * @param isAllCouponsPendingPayment the new all coupons pending payment
	 */
	public void setAllCouponsPendingPayment(boolean isAllCouponsPendingPayment) {
		this.isAllCouponsPendingPayment = isAllCouponsPendingPayment;
	}



	/**
	 * Gets the calendar month.
	 *
	 * @return the calendar month
	 */
	public Integer getCalendarMonth() {
		return calendarMonth;
	}



	/**
	 * Sets the calendar month.
	 *
	 * @param calendarMonth the new calendar month
	 */
	public void setCalendarMonth(Integer calendarMonth) {
		this.calendarMonth = calendarMonth;
	}



	/**
	 * Gets the calendar days.
	 *
	 * @return the calendar days
	 */
	public Integer getCalendarDays() {
		return calendarDays;
	}



	/**
	 * Sets the calendar days.
	 *
	 * @param calendarDays the new calendar days
	 */
	public void setCalendarDays(Integer calendarDays) {
		this.calendarDays = calendarDays;
	}



	/**
	 * Gets the rate type.
	 *
	 * @return the rate type
	 */
	public Integer getRateType() {
		return rateType;
	}



	/**
	 * Sets the rate type.
	 *
	 * @param rateType the new rate type
	 */
	public void setRateType(Integer rateType) {
		this.rateType = rateType;
	}



	/**
	 * Gets the rate value.
	 *
	 * @return the rate value
	 */
	public BigDecimal getRateValue() {
		return rateValue;
	}



	/**
	 * Sets the rate value.
	 *
	 * @param rateValue the new rate value
	 */
	public void setRateValue(BigDecimal rateValue) {
		this.rateValue = rateValue;
	}



	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Integer getOperationType() {
		return operationType;
	}



	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Integer operationType) {
		this.operationType = operationType;
	}



	/**
	 * Gets the minimum rate.
	 *
	 * @return the minimum rate
	 */
	public BigDecimal getMinimumRate() {
		return minimumRate;
	}



	/**
	 * Sets the minimum rate.
	 *
	 * @param minimumRate the new minimum rate
	 */
	public void setMinimumRate(BigDecimal minimumRate) {
		this.minimumRate = minimumRate;
	}



	/**
	 * Gets the maximum rate.
	 *
	 * @return the maximum rate
	 */
	public BigDecimal getMaximumRate() {
		return maximumRate;
	}



	/**
	 * Sets the maximum rate.
	 *
	 * @param maximumRate the new maximum rate
	 */
	public void setMaximumRate(BigDecimal maximumRate) {
		this.maximumRate = maximumRate;
	}
	
	
	/**
	 * Gets the periodicity days.
	 *
	 * @return the periodicity days
	 */
	public Integer getPeriodicityDays() {
		return periodicityDays;
	}



	/**
	 * Sets the periodicity days.
	 *
	 * @param periodicityDays the new periodicity days
	 */
	public void setPeriodicityDays(Integer periodicityDays) {
		this.periodicityDays = periodicityDays;
	}



	/**
	 * Sets the data from int payment schedule req hi.
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param loggerUser the logger user
	 */
	public void setDataFromIntPaymentScheduleReqHi(IntPaymentScheduleReqHi intPaymentScheduleReqHi, LoggerUser loggerUser){
		this.setAudit(loggerUser);
		       
		this.setPeriodicity(intPaymentScheduleReqHi.getPeriodicity());
		this.setPeriodicityDays( intPaymentScheduleReqHi.getPeriodicityDays() );
		this.setExpirationDate( intPaymentScheduleReqHi.getExpirationDate() );
		
        this.setCalendarMonth( intPaymentScheduleReqHi.getCalendarMonth() );
		this.setCalendarType( intPaymentScheduleReqHi.getCalendarType() );
		this.setCalendarDays( intPaymentScheduleReqHi.getCalendarDays() );
		
		this.setRateType( intPaymentScheduleReqHi.getRateType() );
		this.setRateValue( intPaymentScheduleReqHi.getRateValue() );
		this.setOperationType( intPaymentScheduleReqHi.getOperationType() );

		this.setInterestType(intPaymentScheduleReqHi.getInterestType());
		this.setFinancialIndex( intPaymentScheduleReqHi.getFinancialIndex() );
		this.setSpread( intPaymentScheduleReqHi.getSpread()  );  
		this.setMinimumRate( intPaymentScheduleReqHi.getMinimumRate() );
		this.setMaximumRate( intPaymentScheduleReqHi.getMaximumRate() );
		
		this.setInterestPeriodicity(intPaymentScheduleReqHi.getInterestPeriodicity());
		this.setInterestFactor(intPaymentScheduleReqHi.getInterestFactor());
		this.setTaxFactor( intPaymentScheduleReqHi.getTaxFactor() );
		this.setRegistryDays( intPaymentScheduleReqHi.getRegistryDays() );
		this.setYield(intPaymentScheduleReqHi.getYield() );
		this.setCashNominal( intPaymentScheduleReqHi.getCashNominal() );
		
		this.setScheduleState(intPaymentScheduleReqHi.getScheduleState());

	}
	
	public void setDataFromSecurity(Security security){
		this.setSecurity(security);
		this.setPeriodicity(security.getPeriodicity());
		this.setPeriodicityDays( security.getPeriodicityDays() );
		this.setExpirationDate(security.getExpirationDate());
		this.setCalendarType( security.getCalendarType() );
		this.setInterestPeriodicity( security.getInterestRatePeriodicity() );
		this.setInterestType( security.getInterestType() );
		this.setInterestFactor( security.getInterestRate() );
		//security.getInterestPaymentSchedule().setFinancialIndex(financialIndex) #Falta el indice financiero
		//security.getInterestPaymentSchedule().setTaxFactor(taxFactor)# Falta definir
		this.setRegistryDays(security.getStockRegistryDays());
		this.setYield( security.getYield() );
		this.setCashNominal(security.getCashNominal());
		this.setSpread( security.getSpread() );
		this.setScheduleState( PaymentScheduleStateType.REGTERED.getCode() );
		
		this.setCalendarMonth( security.getCalendarMonth() );
		this.setCalendarDays( security.getCalendarDays() );
		this.setRateType( security.getRateType() );
		this.setRateValue( security.getRateValue() );
		this.setOperationType( security.getOperationType() );
		this.setMinimumRate( security.getMinimumRate() );
		this.setMaximumRate( security.getMaximumRate() );
	}

	public String getCashNominalDesc() {
		return cashNominalDesc;
	}

	public void setCashNominalDesc(String cashNominalDesc) {
		this.cashNominalDesc = cashNominalDesc;
	}

	public String getPeriodicityDesc() {
		return periodicityDesc;
	}

	public void setPeriodicityDesc(String periodicityDesc) {
		this.periodicityDesc = periodicityDesc;
	}

	public String getCalendarTypeDesc() {
		return calendarTypeDesc;
	}

	public void setCalendarTypeDesc(String calendarTypeDesc) {
		this.calendarTypeDesc = calendarTypeDesc;
	}

	public String getYieldDesc() {
		return yieldDesc;
	}

	public void setYieldDesc(String yieldDesc) {
		this.yieldDesc = yieldDesc;
	}

	public String getInterestPeriodicityDesc() {
		return interestPeriodicityDesc;
	}

	public void setInterestPeriodicityDesc(String interestPeriodicityDesc) {
		this.interestPeriodicityDesc = interestPeriodicityDesc;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
		
}