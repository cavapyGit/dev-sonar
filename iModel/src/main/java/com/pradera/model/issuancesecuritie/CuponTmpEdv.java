package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "CUPON_TMP_EDV")
@NamedQueries({
    @NamedQuery(name = "CuponTmpEdv.findAll", query = "SELECT c FROM CuponTmpEdv c")})
public class CuponTmpEdv implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
	@SequenceGenerator(name="CUPON_TMP_EDV_PK_GENERATOR", sequenceName="SQ_ID_CUP_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CUPON_TMP_EDV_PK_GENERATOR")
	@Column(name="ID_CUP_PK")
    private Long idCupPk;
    @Column(name = "CUP_SERIE")
    private String cupSerie;
    @Column(name = "CUP_FECHA_VENCI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cupFechaVenci;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CUP_AMORTIZAK")
    private BigDecimal cupAmortizak;
    @Column(name = "CUP_DSC")
    private String cupDsc;
    @Column(name = "CUP_DSM")
    private String cupDsm;
    @Column(name = "CUP_NRO_CUPON")
    private Integer cupNroCupon;
    @Column(name = "CUP_INST")
    private String cupInst;
    @Column(name = "CUP_TASA")
    private BigDecimal cupTasa;

    public CuponTmpEdv() {
    }

    public CuponTmpEdv(Long idCupPk) {
        this.idCupPk = idCupPk;
    }

    public Long getIdCupPk() {
        return idCupPk;
    }

    public void setIdCupPk(Long idCupPk) {
        this.idCupPk = idCupPk;
    }

    public String getCupSerie() {
        return cupSerie;
    }

    public void setCupSerie(String cupSerie) {
        this.cupSerie = cupSerie;
    }

    public Date getCupFechaVenci() {
        return cupFechaVenci;
    }

    public void setCupFechaVenci(Date cupFechaVenci) {
        this.cupFechaVenci = cupFechaVenci;
    }

    public BigDecimal getCupAmortizak() {
        return cupAmortizak;
    }

    public void setCupAmortizak(BigDecimal cupAmortizak) {
        this.cupAmortizak = cupAmortizak;
    }

    public String getCupDsc() {
        return cupDsc;
    }

    public void setCupDsc(String cupDsc) {
        this.cupDsc = cupDsc;
    }

    public String getCupDsm() {
        return cupDsm;
    }

    public void setCupDsm(String cupDsm) {
        this.cupDsm = cupDsm;
    }

    public Integer getCupNroCupon() {
        return cupNroCupon;
    }

    public void setCupNroCupon(Integer cupNroCupon) {
        this.cupNroCupon = cupNroCupon;
    }

    public String getCupInst() {
        return cupInst;
    }

    public void setCupInst(String cupInst) {
        this.cupInst = cupInst;
    }

    public BigDecimal getCupTasa() {
        return cupTasa;
    }

    public void setCupTasa(BigDecimal cupTasa) {
        this.cupTasa = cupTasa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCupPk != null ? idCupPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuponTmpEdv)) {
            return false;
        }
        CuponTmpEdv other = (CuponTmpEdv) object;
        if ((this.idCupPk == null && other.idCupPk != null) || (this.idCupPk != null && !this.idCupPk.equals(other.idCupPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
    	System.out.println("*********************************************");
    	System.out.println("ID_CUP_PK:"+idCupPk);
        System.out.println("CUP_SERIE:"+cupSerie);
        System.out.println("CUP_FECHA_VENCI:"+cupFechaVenci);
        System.out.println("CUP_AMORTIZAK:"+cupAmortizak);
        System.out.println("CUP_DSC:"+cupDsc);
        System.out.println("CUP_DSM:"+ cupDsm);
        System.out.println("CUP_NRO_CUPON:"+ cupNroCupon);
        System.out.println("CUP_INST:"+ cupInst);
        System.out.println("CUP_TASA:"+cupTasa);
        System.out.println("*********************************************");
        return "javaapplication1.CuponTmpEdv[ idCupPk=" + idCupPk + " ]";
    }
}
