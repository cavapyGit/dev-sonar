package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityForeignDepositoryPK.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Embeddable
public class SecurityForeignDepositoryPK implements Serializable {
	//default serial version id, required for serializable classes.
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id international depository pk. */
	@Column(name="ID_INTERNATIONAL_DEPOSITORY_PK")
	private Long idInternationalDepositoryPk;

	/** The id security code pk. */
	@Column(name="ID_SECURITY_CODE_PK")
	private String idSecurityCodePk;

    /**
     * Instantiates a new security foreign depository pk.
     */
    public SecurityForeignDepositoryPK() {
    }
	
	/**
	 * Gets the id international depository pk.
	 *
	 * @return the id international depository pk
	 */
	public Long getIdInternationalDepositoryPk() {
		return this.idInternationalDepositoryPk;
	}
	
	/**
	 * Sets the id international depository pk.
	 *
	 * @param idInternationalDepositoryPk the new id international depository pk
	 */
	public void setIdInternationalDepositoryPk(Long idInternationalDepositoryPk) {
		this.idInternationalDepositoryPk = idInternationalDepositoryPk;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return this.idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SecurityForeignDepositoryPK)) {
			return false;
		}
		SecurityForeignDepositoryPK castOther = (SecurityForeignDepositoryPK)other;
		return 
			(this.idInternationalDepositoryPk.equals(castOther.idInternationalDepositoryPk))
			&& this.idSecurityCodePk.equals(castOther.idSecurityCodePk);

    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idInternationalDepositoryPk ^ (this.idInternationalDepositoryPk >>> 32)));
		hash = hash * prime + this.idSecurityCodePk.hashCode();
		
		return hash;
    }
}