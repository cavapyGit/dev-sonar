package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.negotiation.MechanismModality;


/**
 * The persistent class for the SECURITY_NEGOTIATION_MECHANISM database table.
 * 
 */
@NamedQueries({ 
	@NamedQuery(name = SecurityNegotiationMechanism.SEC_NEGOTIATION_MECHANISM_BY_PARAMS, 
			query = "Select nm from SecurityNegotiationMechanism nm inner join fetch nm.mechanismModality mm where nm.security.idSecurityCodePk=:idSecurityCodePkPrm and nm.securityNegotationState=:statePrm"),
	@NamedQuery(name = SecurityNegotiationMechanism.SEC_NEGOTIATION_STATE, 
			query = "SELECT nvl(count(nm.idSecurityNegMechPk),0)  " +
					"FROM SecurityNegotiationMechanism nm " +
					"WHERE nm.security.idSecurityCodePk=:idSecurityCodePkParam AND " +
					"	   nm.mechanismModality.id.idNegotiationMechanismPk=:idNegotiationMechanismParam AND" +
					" 	   nm.mechanismModality.id.idNegotiationModalityPk=:idNegotiationModalityParam AND " +
					"	   nm.securityNegotationState = :idStateParam ")
})
@Entity																										
@Table(name="SECURITY_NEGOTIATION_MECHANISM")
public class SecurityNegotiationMechanism implements Serializable, Auditable, Cloneable { 
	private static final long serialVersionUID = 1L;

	public static final String SEC_NEGOTIATION_MECHANISM_BY_PARAMS = "SecurityNegotiationMechanism.secNegotiationMechanismByParams";
	
	public static final String SEC_NEGOTIATION_STATE = "SecurityNegotiationMechanism.state";
	
	@Id
	@SequenceGenerator(name="SECURITY_NEGOTIATION_MECHANISM_IDSECURITYNEGMECHPK_GENERATOR", sequenceName="SQ_ID_SECURITY_NEG_MECH_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_NEGOTIATION_MECHANISM_IDSECURITYNEGMECHPK_GENERATOR")
	@Column(name="ID_SECURITY_NEG_MECH_PK")
	private Long idSecurityNegMechPk;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="SECURITY_NEGOTATION_STATE")
	private Integer securityNegotationState;

	//bi-directional many-to-one association to MechanismModality
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="ID_NEGOTIATION_MECHANISM_FK", referencedColumnName="ID_NEGOTIATION_MECHANISM_PK"),
		@JoinColumn(name="ID_NEGOTIATION_MODALITY_FK", referencedColumnName="ID_NEGOTIATION_MODALITY_PK")
		})
	private MechanismModality mechanismModality;

	//bi-directional many-to-one association to Security
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

    public SecurityNegotiationMechanism() {
    }

	public Long getIdSecurityNegMechPk() {
		return this.idSecurityNegMechPk;
	}

	public void setIdSecurityNegMechPk(Long idSecurityNegMechPk) {
		this.idSecurityNegMechPk = idSecurityNegMechPk;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getSecurityNegotationState() {
		return this.securityNegotationState;
	}

	public void setSecurityNegotationState(Integer securityNegotationState) {
		this.securityNegotationState = securityNegotationState;
	}

	public MechanismModality getMechanismModality() {
		return this.mechanismModality;
	}

	public void setMechanismModality(MechanismModality mechanismModality) {
		this.mechanismModality = mechanismModality;
	}
	
	public Security getSecurity() {
		return this.security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * @param idSecurityNegMechPk
	 * @param securityNegotationState
	 */
	public SecurityNegotiationMechanism(Long idSecurityNegMechPk,Integer securityNegotationState) {
		this.idSecurityNegMechPk = idSecurityNegMechPk;
		this.securityNegotationState = securityNegotationState;
	}
	
	public SecurityNegotiationMechanism clone() throws CloneNotSupportedException {
        return (SecurityNegotiationMechanism) super.clone();
    }
}