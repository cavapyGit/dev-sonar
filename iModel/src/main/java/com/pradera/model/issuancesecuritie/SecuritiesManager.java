package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.IssuerDocumentType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SocietyType;

@Entity
@Table(name = "SECURITIES_MANAGER")
public class SecuritiesManager implements Serializable, Auditable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_SECURITIES_MANAGER_PK")	
	private String idSecuritiesManagerPk;

	@Column(name = "BUSINESS_NAME")
	private String businessName;
	
    @Column(name="DESCRIPTION")
	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name = "CLOSING_SERVICE_DATE")
	private Date closingServiceDate;

	@Column(name = "CONTACT_EMAIL")
	private String contactEmail;
	
	@Column(name = "EMAIL")
	private String email;

	@Column(name = "CONTACT_NAME")
	private String contactName;

	@Column(name = "CONTACT_PHONE")
	private String contactPhone;

	@Temporal(TemporalType.DATE)
	@Column(name = "CONTRACT_DATE")
	private Date contractDate;

	@Column(name = "CONTRACT_NUMBER")
	private String contractNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Column(name = "DOCUMENT_NUMBER")
	private String documentNumber;

	@Column(name = "DOCUMENT_TYPE")
	private Integer documentType;

	@Column(name = "ECONOMIC_ACTIVITY")
	private Integer economicActivity;

	@Column(name = "ECONOMIC_SECTOR")
	private Integer economicSector;
	
	@Column(name = "FAX_NUMBER")
	private String faxNumber;

	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name = "LEGAL_ADDRESS")
	private String legalAddress;

	@Column(name = "MNEMONIC")
	private String mnemonic;

	@Column(name = "COMMENTS")
	private String observation;

	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;
	
	@Column(name = "REGISTRY_USER")
	private String registryUser;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "RESIDENCE_COUNTRY")
	private GeographicLocation geographicLocation;

	@Column(name = "SOCIAL_CAPITAL")
	private BigDecimal socialCapital;

	@Column(name = "SOCIETY_TYPE")
	private Integer societyType;

	@Column(name = "STATE_MANAGER")
	private Integer stateManager;

	@Temporal(TemporalType.DATE)
	@Column(name = "SUPER_RESOLUTION_DATE")
	private Date superResolutionDate;

	@Column(name = "SUPERINTENDENT_RESOLUTION")
	private String superintendentResolution;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
    @Column(name="WEBSITE")
	private String website;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "RETIREMENT_DATE")
	private Date retirementDate;
	
	@Column(name = "IND_CONTRACT_DEPOSITORY")
	private Integer indContractDepository;
	
	@Column(name = "DEPARTMENT")
	private Integer department;

	@Column(name = "PROVINCE")
	private Integer province;
	
	@Column(name = "DISTRICT")
	private Integer district;
	
	@Transient
	private boolean indHolderWillBeCreated;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	@OneToMany(mappedBy = "securitiesManager",fetch = FetchType.LAZY)
	private List<Security> securities;
	
	/** The issuer files. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="securitiesManager")
	private List<ManagerFile> managerFiles;
	
	@Transient
	private String departmentDescription;
	
	@Transient
    private Long relatedCui;
    
	@Transient
    private String fundAdministrator;
	
	@Transient
    private String fundType;
	
	@Transient
    private String transferNumber;
	
	@Transient
	private Date enrolledStockExchDate;
	
	@Transient
	private Integer currency;
	
	@Transient
	private String documentTypeDescription;
	
	@Transient
	private String documentSourceDescription;
	
	@Transient
	private Integer documentSource;
	
	@Transient
	private String provinceDescription;
	
	@Transient
	private String districtDescription;
	
	@Transient
	private Integer indStockExchEnrolled;
	
	@Transient
	private Integer creditRatingScales;
	
	public SecuritiesManager(){
		
	}	
	
	
	
	public SecuritiesManager(String idSecuritiesManagerPk, String businessName,
			String documentNumber, Integer documentType,
			Integer economicActivity, Integer economicSector, String mnemonic,
			Integer societyType, Integer stateManager) {
		super();
		this.idSecuritiesManagerPk = idSecuritiesManagerPk;
		this.businessName = businessName;
		this.documentNumber = documentNumber;
		this.documentType = documentType;
		this.economicActivity = economicActivity;
		this.economicSector = economicSector;
		this.mnemonic = mnemonic;
		this.societyType = societyType;
		this.stateManager = stateManager;
	}



	public String getIdSecuritiesManagerPk() {
		return idSecuritiesManagerPk;
	}

	public void setIdSecuritiesManagerPk(String idSecuritiesManagerPk) {
		this.idSecuritiesManagerPk = idSecuritiesManagerPk;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getClosingServiceDate() {
		return closingServiceDate;
	}

	public void setClosingServiceDate(Date closingServiceDate) {
		this.closingServiceDate = closingServiceDate;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public Integer getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	public Integer getEconomicSector() {
		return economicSector;
	}

	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getLegalAddress() {
		return legalAddress;
	}

	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRegistryUser() {
		return registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public GeographicLocation getGeographicLocation() {
		return geographicLocation;
	}

	public void setGeographicLocation(GeographicLocation geographicLocation) {
		this.geographicLocation = geographicLocation;
	}

	public BigDecimal getSocialCapital() {
		return socialCapital;
	}

	public void setSocialCapital(BigDecimal socialCapital) {
		this.socialCapital = socialCapital;
	}

	public Integer getSocietyType() {
		return societyType;
	}

	public void setSocietyType(Integer societyType) {
		this.societyType = societyType;
	}

	public Integer getStateManager() {
		return stateManager;
	}

	public void setStateManager(Integer stateManager) {
		this.stateManager = stateManager;
	}

	public Date getSuperResolutionDate() {
		return superResolutionDate;
	}

	public void setSuperResolutionDate(Date superResolutionDate) {
		this.superResolutionDate = superResolutionDate;
	}

	public String getSuperintendentResolution() {
		return superintendentResolution;
	}

	public void setSuperintendentResolution(String superintendentResolution) {
		this.superintendentResolution = superintendentResolution;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Date getRetirementDate() {
		return retirementDate;
	}

	public void setRetirementDate(Date retirementDate) {
		this.retirementDate = retirementDate;
	}

	public Integer getIndContractDepository() {
		return indContractDepository;
	}

	public void setIndContractDepository(Integer indContractDepository) {
		this.indContractDepository = indContractDepository;
	}

	public Integer getDepartment() {
		return department;
	}

	public void setDepartment(Integer department) {
		this.department = department;
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getDistrict() {
		return district;
	}

	public void setDistrict(Integer district) {
		this.district = district;
	}
	
	
	
	public List<Security> getSecurities() {
		return securities;
	}



	public void setSecurities(List<Security> securities) {
		this.securities = securities;
	}

	

	


	public List<ManagerFile> getManagerFiles() {
		return managerFiles;
	}



	public void setManagerFiles(List<ManagerFile> managerFiles) {
		this.managerFiles = managerFiles;
	}



	public String getFullDocument(){
    	if(Validations.validateIsNotNullAndPositive(documentType)
    			&& Validations.validateIsNotNull(documentNumber)){
    		StringBuilder sb = new StringBuilder();
    		sb.append(getDocumentTypeDescription());
    		sb.append("-");
    		sb.append(documentNumber);
    		return  sb.toString();
    	} else {
    		return null;
    	}
    }
	
    public String getDocumentTypeDescription(){
    	if(Validations.validateIsNotNullAndPositive(documentType)){
    		if(Validations.validateIsNotNull(IssuerDocumentType.get(documentType))){
				return IssuerDocumentType.get(documentType).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
	/**
     * Gets the economic activity description.
     *
     * @return the economic activity description
     */
    public String getEconomicActivityDescription(){
    	if(Validations.validateIsNotNullAndPositive(economicActivity)){    		
    		if(Validations.validateIsNotNull(EconomicActivityType.get(economicActivity))){
				return EconomicActivityType.get(economicActivity).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the economic sector description.
     *
     * @return the economic sector description
     */
    public String getEconomicSectorDescription(){
    	if(Validations.validateIsNotNullAndPositive(economicSector)){
    		if(Validations.validateIsNotNull(EconomicSectorType.get(economicSector))){
				return EconomicSectorType.get(economicSector).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
    public String getSocietyTypeDescription(){
    	if(Validations.validateIsNotNullAndPositive(societyType)){
    		if(Validations.validateIsNotNull(SocietyType.get(societyType))){
				return SocietyType.get(societyType).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
	public String getStateIssuerDescription(){
		if(Validations.validateIsNotNullAndPositive(stateManager)){
			if(Validations.validateIsNotNull(IssuerStateType.get(stateManager))){
				return IssuerStateType.get(stateManager).getValue();
			} else {
				return null;
			}
			
		} else {
			return null;
		}
	}
	
	

	public Holder getHolder() {
		return holder;
	}



	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	

	public boolean isIndHolderWillBeCreated() {
		return indHolderWillBeCreated;
	}



	public void setIndHolderWillBeCreated(boolean indHolderWillBeCreated) {
		this.indHolderWillBeCreated = indHolderWillBeCreated;
	}



	public String getDepartmentDescription() {
		return departmentDescription;
	}



	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}



	public Long getRelatedCui() {
		return relatedCui;
	}



	public void setRelatedCui(Long relatedCui) {
		this.relatedCui = relatedCui;
	}



	public String getFundAdministrator() {
		return fundAdministrator;
	}



	public void setFundAdministrator(String fundAdministrator) {
		this.fundAdministrator = fundAdministrator;
	}

	

	public String getFundType() {
		return fundType;
	}



	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	

	public String getTransferNumber() {
		return transferNumber;
	}



	public void setTransferNumber(String transferNumber) {
		this.transferNumber = transferNumber;
	}


	
	

	public Date getEnrolledStockExchDate() {
		return enrolledStockExchDate;
	}



	public void setEnrolledStockExchDate(Date enrolledStockExchDate) {
		this.enrolledStockExchDate = enrolledStockExchDate;
	}

	

	public Integer getCurrency() {
		return currency;
	}



	public void setCurrency(Integer currency) {
		this.currency = currency;
	}



	public void setDocumentTypeDescription(String documentTypeDescription) {
		this.documentTypeDescription = documentTypeDescription;
	}

	

	public String getDocumentSourceDescription() {
		return documentSourceDescription;
	}



	public void setDocumentSourceDescription(String documentSourceDescription) {
		this.documentSourceDescription = documentSourceDescription;
	}

	


	public Integer getIndStockExchEnrolled() {
		return indStockExchEnrolled;
	}



	public void setIndStockExchEnrolled(Integer indStockExchEnrolled) {
		this.indStockExchEnrolled = indStockExchEnrolled;
	}



	public Integer getDocumentSource() {
		return documentSource;
	}



	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}

	public String getDistrictDescription() {
		return districtDescription;
	}

	/**
	 * Sets the district description.
	 *
	 * @param districtDescription the new district description
	 */
	public void setDistrictDescription(String districtDescription) {
		this.districtDescription = districtDescription;
	}	

	/**
	 * Gets the province description.
	 *
	 * @return the province description
	 */
	public String getProvinceDescription() {
		return provinceDescription;
	}

	/**
	 * Sets the province description.
	 *
	 * @param provinceDescription the new province description
	 */
	public void setProvinceDescription(String provinceDescription) {
		this.provinceDescription = provinceDescription;
	}
	
	

	public Integer getCreditRatingScales() {
		return creditRatingScales;
	}



	public void setCreditRatingScales(Integer creditRatingScales) {
		this.creditRatingScales = creditRatingScales;
	}



	public String getIndContractDepositoryDescription(){
		if(Validations.validateIsNotNull(indContractDepository)){
			return BooleanType.get(indContractDepository).getValue();
		}
		return null;
	}
	
	public String getIndStockExchEnrolledDescription(){
		if(Validations.validateIsNotNull(indStockExchEnrolled)){
			return BooleanType.get(indStockExchEnrolled).getValue();
		}
		return null;
	}
	
	public String getCurrencyDescription(){
		if(Validations.validateIsNotNullAndPositive(currency)){
			if(Validations.validateIsNotNull(CurrencyType.get(currency))){
				return CurrencyType.get(currency).getValue();
			} else {
				return null;
			}
			
		} else {
			return null;
		}
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null){
			 lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
	         lastModifyDate = loggerUser.getAuditTime();
	         lastModifyIp = loggerUser.getIpAddress();
	         lastModifyUser = loggerUser.getUserName();
		}		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}
}
