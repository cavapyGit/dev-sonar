package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//574
public enum ProgramScheduleSituationType {

	PENDING(Integer.valueOf(2258),"PENDIENTE"),
	ISSUER_PAID(Integer.valueOf(2259),"PAGADO EN EMISOR"),
	EDV_PAID(Integer.valueOf(2260),"PAGADO EDV");
	
	private Integer code;
	private String value;
	
	public final static List<ProgramScheduleSituationType> list=new ArrayList<ProgramScheduleSituationType>();
	public static final Map<Integer, ProgramScheduleSituationType> lookup = new HashMap<Integer, ProgramScheduleSituationType>();
	
	static{
		for(ProgramScheduleSituationType d : ProgramScheduleSituationType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	

	private ProgramScheduleSituationType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static ProgramScheduleSituationType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}
	

	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
}
