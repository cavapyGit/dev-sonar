/**@author mmacalupu
 * this enum is to handle diferent kinds of
 * Securities' Locked.  
 * 
 */

package com.pradera.model.issuancesecuritie.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

import com.pradera.model.negotiation.type.MechanismOperationStateType;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum IssuancePlacementType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/03/2013
 */
public enum SecurityPlacementType {
	
	/** The fij. */
	FIXED(new Integer(691),"FIJA"),
	
	/** The floating. */
	FLOATING(new Integer(692),"FLOTANTE"),
	
	/** The mixt. */
	MIXED(new Integer(693),"MIXTA");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<SecurityPlacementType> list = new ArrayList<SecurityPlacementType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SecurityPlacementType> lookup = new HashMap<Integer, SecurityPlacementType>();
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<SecurityPlacementType> listSomeElements(SecurityPlacementType... transferSecuritiesTypeParams){
		List<SecurityPlacementType> retorno = new ArrayList<SecurityPlacementType>();
		for(SecurityPlacementType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	
	public static List<SecurityPlacementType> listSecurityPlacementType(){
		List<SecurityPlacementType> lstPlacementType = new ArrayList<SecurityPlacementType>();
		for(SecurityPlacementType s : SecurityPlacementType.values()){
			if(!s.getCode().equals( SecurityPlacementType.MIXED.getCode() )){
				lstPlacementType.add(s);
			}
		}
		return lstPlacementType;
	}
	
	public static List<SecurityPlacementType> listIssuancePlacementType(){
		List<SecurityPlacementType> lstPlacementType = new ArrayList<SecurityPlacementType>();
		for(SecurityPlacementType s : SecurityPlacementType.values()){
			lstPlacementType.add(s);
		}
		return lstPlacementType;
	}
	
	static {
		for (SecurityPlacementType s : EnumSet.allOf(SecurityPlacementType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new issuance placement type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private SecurityPlacementType(int ordinal, String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the security placement type
	 */
	public static SecurityPlacementType get(Integer code) {
		return lookup.get(code);
	}
}