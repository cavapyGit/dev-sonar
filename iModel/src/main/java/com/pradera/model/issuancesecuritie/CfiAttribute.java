package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CFI_ATTRIBUTE database table.
 * 
 */
@Entity
@Table(name="CFI_ATTRIBUTE")
public class CfiAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CfiAttributePK id;

	@Column(name="ATTRIBUTE_LEVEL")
	private BigDecimal attributeLevel;

	@Column(name="DESCRIPTION_ENG")
	private String descriptionEng;

	@Column(name="DESCRIPTION_ESP")
	private String descriptionEsp;

	@Column(name="LAST_MODIFY_APP")
	private BigDecimal lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

    public CfiAttribute() {
    }
    
    public CfiAttribute(CfiAttributePK cfiAttributePK) {
    	this.id=cfiAttributePK;
    }

	public CfiAttributePK getId() {
		return this.id;
	}

	public void setId(CfiAttributePK id) {
		this.id = id;
	}
	
	public BigDecimal getAttributeLevel() {
		return this.attributeLevel;
	}

	public void setAttributeLevel(BigDecimal attributeLevel) {
		this.attributeLevel = attributeLevel;
	}

	public String getDescriptionEng() {
		return this.descriptionEng;
	}

	public void setDescriptionEng(String descriptionEng) {
		this.descriptionEng = descriptionEng;
	}

	public String getDescriptionEsp() {
		return this.descriptionEsp;
	}

	public void setDescriptionEsp(String descriptionEsp) {
		this.descriptionEsp = descriptionEsp;
	}

	public BigDecimal getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(BigDecimal lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

}