package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * The primary key class for the CFI_GROUP database table.
 * 
 */
@Embeddable
public class CfiGroupPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_GROUP_PK")
	@NotNull
	private String idGroupPk;

	@Column(name="ID_CATEGORY_PK")
	private String idCategoryPk;

    public CfiGroupPK() {
    }
	public String getIdGroupPk() {
		return this.idGroupPk;
	}
	public void setIdGroupPk(String idGroupPk) {
		this.idGroupPk = idGroupPk;
	}
	public String getIdCategoryPk() {
		return this.idCategoryPk;
	}
	public void setIdCategoryPk(String idCategoryPk) {
		this.idCategoryPk = idCategoryPk;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CfiGroupPK)) {
			return false;
		}
		CfiGroupPK castOther = (CfiGroupPK)other;
		return 
			this.idGroupPk.equals(castOther.idGroupPk)
			&& this.idCategoryPk.equals(castOther.idCategoryPk);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idGroupPk.hashCode();
		hash = hash * prime + this.idCategoryPk.hashCode();
		
		return hash;
    }
}