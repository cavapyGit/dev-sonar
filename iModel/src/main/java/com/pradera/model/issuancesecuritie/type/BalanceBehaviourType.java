package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BalanceBehaviourType {
	

	SUM(Integer.valueOf(1),"SUMA"), 
	SUBTRACTION(Integer.valueOf(2),"RESTA");
	
	public final static List<BalanceBehaviourType> list=new ArrayList<BalanceBehaviourType>();
	public static final Map<Integer, BalanceBehaviourType> lookup = new HashMap<Integer, BalanceBehaviourType>();

	private Integer code;
	private String value;
	
	static{
		for(BalanceBehaviourType d : BalanceBehaviourType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	private BalanceBehaviourType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static BalanceBehaviourType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
}
