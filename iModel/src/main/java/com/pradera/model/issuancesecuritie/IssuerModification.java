package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the ISSUER_MODIFICATION database table.
 * 
 */
@Entity
@Table(name="ISSUER_MODIFICATION")
public class IssuerModification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	// @SequenceGenerator(name="ISSUER_MODIFICATION_IDISSUERREQUESTPK_GENERATOR", sequenceName="SQ_ID_ISSUER_REQUEST_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUER_REQUEST_IDISSUERREQUESTPK_GENERATOR")
	@Column(name="ID_ISSUER_REQUEST_PK")
	private long idIssuerRequestPk;

	@Column(name="ID_MODIFY_FIELD_FK")
	private BigDecimal idModifyFieldFk;

	@Column(name="ID_REGISTRY_TABLE")
	private BigDecimal idRegistryTable;

	@Column(name="LAST_MODIFY_APP")
	private BigDecimal lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Lob()
	@Column(name="NEW_FILE")
	private byte[] newFile;

	@Column(name="NEW_VALUE")
	private String newValue;

    @Lob()
	@Column(name="OLD_FILE")
	private byte[] oldFile;

	@Column(name="OLD_VALUE")
	private String oldValue;

    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional one-to-one association to IssuerRequest
	@OneToOne
	@JoinColumn(name="ID_ISSUER_REQUEST_PK")
	private IssuerRequest issuerRequest;

    public IssuerModification() {
    }

	public long getIdIssuerRequestPk() {
		return this.idIssuerRequestPk;
	}

	public void setIdIssuerRequestPk(long idIssuerRequestPk) {
		this.idIssuerRequestPk = idIssuerRequestPk;
	}

	public BigDecimal getIdModifyFieldFk() {
		return this.idModifyFieldFk;
	}

	public void setIdModifyFieldFk(BigDecimal idModifyFieldFk) {
		this.idModifyFieldFk = idModifyFieldFk;
	}

	public BigDecimal getIdRegistryTable() {
		return this.idRegistryTable;
	}

	public void setIdRegistryTable(BigDecimal idRegistryTable) {
		this.idRegistryTable = idRegistryTable;
	}

	public BigDecimal getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(BigDecimal lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public byte[] getNewFile() {
		return this.newFile;
	}

	public void setNewFile(byte[] newFile) {
		this.newFile = newFile;
	}

	public String getNewValue() {
		return this.newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public byte[] getOldFile() {
		return this.oldFile;
	}

	public void setOldFile(byte[] oldFile) {
		this.oldFile = oldFile;
	}

	public String getOldValue() {
		return this.oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public IssuerRequest getIssuerRequest() {
		return this.issuerRequest;
	}

	public void setIssuerRequest(IssuerRequest issuerRequest) {
		this.issuerRequest = issuerRequest;
	}
	
}