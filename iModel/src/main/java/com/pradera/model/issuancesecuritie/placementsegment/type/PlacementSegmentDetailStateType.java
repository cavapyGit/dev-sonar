package com.pradera.model.issuancesecuritie.placementsegment.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum SecuritieType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/04/2013
 */
public enum PlacementSegmentDetailStateType {


	/** The registered. */
	REGISTERED(Integer.valueOf(854),"REGISTRADO"),	
	
	/** The deleted. */
	DELETED(Integer.valueOf(855),"ELIMINADO");
	
	/** The Constant lookup. */
	public static final Map<Integer,PlacementSegmentDetailStateType> lookup=new HashMap<Integer, PlacementSegmentDetailStateType>();
	
	/** The Constant list. */
	public static final List<PlacementSegmentDetailStateType> list=new ArrayList<PlacementSegmentDetailStateType>();

	static{
		for(PlacementSegmentDetailStateType s:PlacementSegmentDetailStateType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PlacementSegmentDetailStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static PlacementSegmentDetailStateType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
}