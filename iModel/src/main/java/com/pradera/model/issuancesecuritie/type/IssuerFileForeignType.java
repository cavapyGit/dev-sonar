package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantFileType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public enum IssuerFileForeignType {

	/** The certificationvalidity. */
	CERTIFICATIONVALIDITY(Integer.valueOf(1245),"CERTIFICADO DE VIGENCIA"),

	/** The bylawsfile. */
	BYLAWSFILE(Integer.valueOf(1246),"ESTATUTOS SOCIALES VIGENTES"),

	/** The shareholderslistfile. */
	SHAREHOLDERSLISTFILE(Integer.valueOf(1247),"LISTADO DE ACCIONISTAS DE LA SOCIEDAD"),

	/** The certificationadmcouncil. */
	CERTIFICATIONADMCOUNCIL(Integer.valueOf(1248),"CERTIFICACIÓN DEL CONSEJO DE ADMINISTRACIÓN"),	

	/** The representativecertificationfile. */
	REPRESENTATIVECERTIFICATIONFILE(Integer.valueOf(1249),"CERTIFICACIÓN DEL REPRESENTANTE"),

	/** The representativepassportfile. */
	REPRESENTATIVEPASSPORTFILE(Integer.valueOf(1250),"CÉDULA O PASAPORTE DEL REPRESENTANTE"),

	/** The certificationauthorityfile. */
	CERTIFICATION_ISSUED_BY_SIV(Integer.valueOf(1251),"CERTIFICACIÓN EMITIDA POR LA SIV");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant file type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuerFileForeignType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<IssuerFileForeignType> list = new ArrayList<IssuerFileForeignType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, IssuerFileForeignType> lookup = new HashMap<Integer, IssuerFileForeignType>();
	static {
		for (IssuerFileForeignType s : EnumSet.allOf(IssuerFileForeignType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant file foreign type
	 */
	public static IssuerFileForeignType get(Integer code) {
		return lookup.get(code);
	}

}
