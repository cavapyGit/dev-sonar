package com.pradera.model.issuancesecuritie.placementsegment.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Enum SecuritieType.
 */
public enum PlacementSegmentStateType {

	REGISTERED(Integer.valueOf(580),"REGISTRADO"),
	CONFIRMED(Integer.valueOf(581),"CONFIRMADO"),
	OPENED(Integer.valueOf(582),"APERTURADO"),
	SUSPENDED(Integer.valueOf(583),"SUSPENDIDO"),
	CLOSED(Integer.valueOf(584),"CERRADO"),
	REJECT(Integer.valueOf(868),"RECHAZADO");
	
	/** The Constant lookup. */
	public static final Map<Integer,PlacementSegmentStateType> lookup=new HashMap<Integer, PlacementSegmentStateType>();
	
	/** The Constant list. */
	public static final List<PlacementSegmentStateType> list=new ArrayList<PlacementSegmentStateType>();

	static{
		for(PlacementSegmentStateType s:PlacementSegmentStateType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PlacementSegmentStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static PlacementSegmentStateType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<PlacementSegmentStateType> listSomeElements(PlacementSegmentStateType... transferSecuritiesTypeParams){
		List<PlacementSegmentStateType> retorno = new ArrayList<PlacementSegmentStateType>();
		for(PlacementSegmentStateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
}