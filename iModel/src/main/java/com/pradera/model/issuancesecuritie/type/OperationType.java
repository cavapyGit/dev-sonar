package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum OperationType {
	

	SUM(Integer.valueOf(549),"SUMA"), 
	SUBTRACTION(Integer.valueOf(550),"RESTA");
	
	public final static List<OperationType> list=new ArrayList<OperationType>();
	public static final Map<Integer, OperationType> lookup = new HashMap<Integer, OperationType>();

	private Integer code;
	private String value;
	
	static{
		for(OperationType d : OperationType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	private OperationType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static OperationType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
}
