package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
/**
 * The persistent class for the ISSUANCE_HISTORY_BALANCE database table.
 * 
 */
@Entity
@Table(name="ISSUANCE_HISTORY_BALANCE_AUX")
public class IssuanceHistoryBalanceAux implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ISSUANCE_HISTORY_BALANCE_AUX_IDISSUANCEHISTORYBALANCEPK_GENERATOR", sequenceName="SQ_ID_ISS_HIS_BALANCE_AUX_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUANCE_HISTORY_BALANCE_AUX_IDISSUANCEHISTORYBALANCEPK_GENERATOR")
	@Column(name="ID_ISS_HISTORY_BALANCE_PK")
	private Long idIssuanceHistoryBalancePk;

	//bi-directional many-to-one association to Issuance
    @ManyToOne
	@JoinColumn(name="ID_ISSUANCE_CODE_FK")
	private Issuance issuance;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;
    
    @Column(name="ISSUANCE_AMOUNT")
    private BigDecimal issuanceAmount;
    
    @Transient
    @Column(name="TRANCHE_AMOUNT")
    private BigDecimal trancheAmount;
    
    @Column(name="PLACED_AMOUNT")
    private BigDecimal placedAmount;
    
	@Column(name="AMORTIZATION_AMOUNT")
	private BigDecimal amortizationAmount;
	
	@Column(name="CIRCULATION_AMOUNT")
	private BigDecimal circulationAmount;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
    public IssuanceHistoryBalanceAux() {}

	public Long getIdIssuanceHistoryBalancePk() {
		return idIssuanceHistoryBalancePk;
	}

	public void setIdIssuanceHistoryBalancePk(Long idIssuanceHistoryBalancePk) {
		this.idIssuanceHistoryBalancePk = idIssuanceHistoryBalancePk;
	}

	public Issuance getIssuance() {
		return issuance;
	}

	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public BigDecimal getIssuanceAmount() {
		return issuanceAmount;
	}

	public void setIssuanceAmount(BigDecimal issuanceAmount) {
		this.issuanceAmount = issuanceAmount;
	}

	public BigDecimal getTrancheAmount() {
		return trancheAmount;
	}

	public void setTrancheAmount(BigDecimal trancheAmount) {
		this.trancheAmount = trancheAmount;
	}

	public BigDecimal getPlacedAmount() {
		return placedAmount;
	}

	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	public BigDecimal getAmortizationAmount() {
		return amortizationAmount;
	}

	public void setAmortizationAmount(BigDecimal amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	public BigDecimal getCirculationAmount() {
		return circulationAmount;
	}

	public void setCirculationAmount(BigDecimal circulationAmount) {
		this.circulationAmount = circulationAmount;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
}