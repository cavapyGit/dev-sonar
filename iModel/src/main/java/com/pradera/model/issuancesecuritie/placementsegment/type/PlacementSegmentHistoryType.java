package com.pradera.model.issuancesecuritie.placementsegment.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum SecuritieType.
 */
public enum PlacementSegmentHistoryType {

	MODIFIED(Integer.valueOf(656),"MODIFICADO"),	
	REGISTER(Integer.valueOf(657),"REGISTRADO");
	
	/** The Constant lookup. */
	public static final Map<Integer,PlacementSegmentHistoryType> lookup=new HashMap<Integer, PlacementSegmentHistoryType>();
	
	/** The Constant list. */
	public static final List<PlacementSegmentHistoryType> list=new ArrayList<PlacementSegmentHistoryType>();

	static{
		for(PlacementSegmentHistoryType s:PlacementSegmentHistoryType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PlacementSegmentHistoryType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static PlacementSegmentHistoryType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}	
	
}
