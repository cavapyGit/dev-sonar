package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceAuxStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.issuancesecuritie.type.SecurityType;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ISSUANCE_AUX database table.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 22/03/2013
 */
@Entity
@Table(name="ISSUANCE_AUX")
@NamedQueries({
	@NamedQuery(name = IssuanceAux.ISSUANCE_STATE, query = "SELECT new com.pradera.model.issuancesecuritie.IssuanceAux(i.idIssuanceCodePk, i.stateIssuance) FROM IssuanceAux i WHERE i.idIssuanceCodePk = :idIssuanceCodePkParam")
})
public class IssuanceAux implements Serializable, Auditable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant ISSUANCE_STATE. */
	public static final String ISSUANCE_STATE = "issuance.searchStateAux";//**

	/** The id issuance code pk. */
	@Id
	@Column(name = "ID_ISSUANCE_CODE_PK")
	private String idIssuanceCodePk;

	/** The amortization amount. */
	@Column(name = "AMORTIZATION_AMOUNT")
	private BigDecimal amortizationAmount;

	/** The circulation amount. */
	@Column(name = "CIRCULATION_AMOUNT")
	private BigDecimal circulationAmount;

	/** The currency. */
	private Integer currency;

	/** The description. */
	private String description;

	/** The expiration date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "EXPIRATION_DATE")
	private Date expirationDate;

	/** The instrument type. */
	@Column(name = "INSTRUMENT_TYPE")
	private Integer instrumentType;

	/** The issuance amount. */
	@Column(name = "ISSUANCE_AMOUNT")
	private BigDecimal issuanceAmount;

	// bi-directional many-to-one association to GeographicLocation
	/** The geographic location. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ISSUANCE_COUNTRY")
	private GeographicLocation geographicLocation;

	/** The issuance date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ISSUANCE_DATE")
	private Date issuanceDate;

	/** The issuance type. */
	@Column(name = "ISSUANCE_TYPE")
	private Integer issuanceType;

	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The number placement tranches. */
	@Column(name = "NUMBER_PLACEMENT_TRANCHES")
	private Integer numberPlacementTranches;

	/** The number total tranches. */
	@Column(name = "NUMBER_TOTAL_TRANCHES")
	private Integer numberTotalTranches;

	/** The placed amount. */
	@Column(name = "PLACED_AMOUNT")
	private BigDecimal placedAmount;

	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registryUser;

	/** The resolution date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RESOLUTION_DATE")
	private Date resolutionDate;

	/** The resolution number. */
	@Column(name = "RESOLUTION_NUMBER")
	private String resolutionNumber;

	/** The security class. */
	@Column(name = "SECURITY_CLASS")
	private Integer securityClass;

	/** The security type. */
	@Column(name = "SECURITY_TYPE")
	private Integer securityType;

	/** The state issuance. */
	@Column(name = "STATE_ISSUANCE")
	private Integer stateIssuance;

	/** The segment amount. */
	@Column(name = "AMOUNT_CONFIRMED_SEGMENTS")
	private BigDecimal amountConfirmedSegments;
	
	/** The amount opened segments. */
	@Column(name = "AMOUNT_OPENED_SEGMENTS")
	private BigDecimal amountOpenedSegments;

	/** The current tranche. */
	@Column(name = "CURRENT_TRANCHE")
	private Integer currentTranche;
	
	/** The placement type. */
	@Column(name = "PLACEMENT_TYPE")
	private Integer placementType;
	
	/** The ind have certificate. */
	@Column(name="IND_HAVE_CERTIFICATE")
	private Integer indHaveCertificate;
	
	/** The ind primary placement. */
	@Column(name="IND_PRIMARY_PLACEMENT")
	private Integer indPrimaryPlacement;
	
	/** The ind regulator report. */
	@Column(name="IND_REGULATOR_REPORT")
	private Integer indRegulatorReport;
	
	/** The credit rating scales. */
	@Column(name = "CREDIT_RATING_SCALES")
	private Integer creditRatingScales;
	
	/** The version. */
	@Version
	private Long version;
	
//	@Column(name="OFFER_TYPE")
//	private Integer offerType;

	// bi-directional many-to-one association to Issuer
	/** The issuer. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_ISSUER_FK")
	private Issuer issuer;
	
	/** The negotiation factor. */
	@Column(name = "NEGOTIATION_FACTOR")
	private Double negotiationFactor;
	
	/** estado cheker. */
	@Column(name = "STATE_CHK")
	private Integer stateChk; 
	
	//** sincronizado para produccion
	/** The resolution number. */
	@Column(name = "ENDORSEMENT")
	private String endorsement;
	
	/** The testimony date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TESTIMONY_DATE")
	private Date testimonyDate;

	/** The testimony number. */
	@Column(name = "TESTIMONY_NUMBER")
	private String testimonyNumber;

	// bi-directional many-to-one association to Security
	/** The securities. */
	@OneToMany(mappedBy = "issuance")
	private List<Security> securities;
	
	// bi-directional many-to-one association to IssuanceCertificate
	/** The issuance certificates. */
	@OneToMany(mappedBy = "issuance", cascade = CascadeType.ALL)
	private List<IssuanceCertificateAux> issuanceCertificates;
	
	/** The issuance files. */
	@OneToMany(mappedBy = "issuance", cascade = CascadeType.ALL)
	private List<IssuanceFileAux> issuanceFiles;

	// bi-directional many-to-one association to issuanceHistoryBalance
	/** The issuance history balances. */
	@OneToMany(mappedBy = "issuance", cascade = CascadeType.ALL)
	private List<IssuanceHistoryBalance> issuanceHistoryBalances;
	
	@Column(name = "IND_EXCHANGE_SECURITIES")
	private Integer indExchangeSecurities;
	
	/** The amount registered segment. */
	@Transient
	private BigDecimal amountRegisteredSegment;
	
	/** The registered tranches. */
	@Transient
	private Integer registeredTranches;
	
	/** The amount confirmed segment. */
	@Transient
	private BigDecimal amountConfirmedSegment;
	
	/** The amount opened segment. */
	@Transient
	private BigDecimal amountOpenedSegment;
	
	/** The opened tranches. */
	@Transient
	private Integer openedTranches;
	
	/** The confirmed tranches. */
	@Transient
	private Integer confirmedTranches;
	
	/** The closed tranches. */
	@Transient
	private Integer closedTranches;
	
	/** The placed securities balance. */
	@Transient
	private Integer placedSecuritiesBalance;
	
	/** The securities in negotiations. */
	@Transient
	public boolean securitiesInNegotiations;
	
	/** The securities registed. */
	@Transient
	public boolean securitiesRegisted;

	/** The valid tranches. */
	@Transient
	public Integer validTranches;
	
	/** dias de expiracion de colocacion */
	@Column(name = "PLACEMENT_EXPIRATION_DAYS")
	private Integer placementExpirationDays; 
	
	/** fecha de expiracion de colocacion. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PLACEMENT_EXPIRATION_DATE")
	private Date placementExpirationDate;
	
	/**
	 * Instantiates a new issuance.
	 */
	public IssuanceAux() {
		indRegulatorReport = ComponentConstant.ZERO;
		geographicLocation=new GeographicLocation();
	}

	/**
	 * Gets the id issuance code pk.
	 * 
	 * @return the id issuance code pk
	 */
	public String getIdIssuanceCodePk() {
		return idIssuanceCodePk;
	}

	/**
	 * Sets the id issuance code pk.
	 * 
	 * @param idIssuanceCodePk
	 *            the new id issuance code pk
	 */
	public void setIdIssuanceCodePk(String idIssuanceCodePk) {
		this.idIssuanceCodePk = idIssuanceCodePk;
	}

	/**
	 * Gets the currency.
	 * 
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 * 
	 * @param currency
	 *            the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the expiration date.
	 * 
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 * 
	 * @param expirationDate
	 *            the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the instrument type.
	 * 
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 * 
	 * @param instrumentType
	 *            the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the geographic location.
	 * 
	 * @return the geographic location
	 */
	public GeographicLocation getGeographicLocation() {
		return geographicLocation;
	}

	/**
	 * Sets the geographic location.
	 * 
	 * @param geographicLocation
	 *            the new geographic location
	 */
	public void setGeographicLocation(GeographicLocation geographicLocation) {
		this.geographicLocation = geographicLocation;
	}

	/**
	 * Gets the issuance date.
	 * 
	 * @return the issuance date
	 */
	public Date getIssuanceDate() {
		return issuanceDate;
	}

	/**
	 * Sets the issuance date.
	 * 
	 * @param issuanceDate
	 *            the new issuance date
	 */
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	/**
	 * Gets the issuance type.
	 * 
	 * @return the issuance type
	 */
	public Integer getIssuanceType() {
		return issuanceType;
	}

	/**
	 * Instantiates a new issuance.
	 *
	 * @param idIssuanceCodePk the id issuance code pk
	 */
	public IssuanceAux(String idIssuanceCodePk) {
		super();
		this.idIssuanceCodePk = idIssuanceCodePk;
	}

	/**
	 * Sets the issuance type.
	 * 
	 * @param issuanceType
	 *            the new issuance type
	 */
	public void setIssuanceType(Integer issuanceType) {
		this.issuanceType = issuanceType;
	}

	/**
	 * Gets the last modify app.
	 * 
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 * 
	 * @param lastModifyApp
	 *            the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 * 
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 * 
	 * @param lastModifyDate
	 *            the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 * 
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 * 
	 * @param lastModifyIp
	 *            the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 * 
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 * 
	 * @param lastModifyUser
	 *            the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the number placement tranches.
	 * 
	 * @return the number placement tranches
	 */
	public Integer getNumberPlacementTranches() {
		return numberPlacementTranches;
	}

	/**
	 * Sets the number placement tranches.
	 * 
	 * @param numberPlacementTranches
	 *            the new number placement tranches
	 */
	public void setNumberPlacementTranches(Integer numberPlacementTranches) {
		this.numberPlacementTranches = numberPlacementTranches;
	}

	/**
	 * Gets the number total tranches.
	 * 
	 * @return the number total tranches
	 */
	public Integer getNumberTotalTranches() {
		return numberTotalTranches;
	}

	/**
	 * Sets the number total tranches.
	 * 
	 * @param numberTotalTranches
	 *            the new number total tranches
	 */
	public void setNumberTotalTranches(Integer numberTotalTranches) {
		this.numberTotalTranches = numberTotalTranches;
	}

	/**
	 * Gets the registry date.
	 * 
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 * 
	 * @param registryDate
	 *            the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 * 
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 * 
	 * @param registryUser
	 *            the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the resolution date.
	 * 
	 * @return the resolution date
	 */
	public Date getResolutionDate() {
		return resolutionDate;
	}

	/**
	 * Sets the resolution date.
	 * 
	 * @param resolutionDate
	 *            the new resolution date
	 */
	public void setResolutionDate(Date resolutionDate) {
		this.resolutionDate = resolutionDate;
	}

	/**
	 * Gets the resolution number.
	 * 
	 * @return the resolution number
	 */
	public String getResolutionNumber() {
		return resolutionNumber;
	}

	/**
	 * Sets the resolution number.
	 * 
	 * @param resolutionNumber
	 *            the new resolution number
	 */
	public void setResolutionNumber(String resolutionNumber) {
		this.resolutionNumber = resolutionNumber;
	}

	/**
	 * Gets the security class.
	 * 
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 * 
	 * @param securityClass
	 *            the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the security type.
	 * 
	 * @return the security type
	 */
	public Integer getSecurityType() {
		return securityType;
	}

	/**
	 * Sets the security type.
	 * 
	 * @param securityType
	 *            the new security type
	 */
	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}

	/**
	 * Gets the state issuance.
	 * 
	 * @return the state issuance
	 */
	public Integer getStateIssuance() {
		return stateIssuance;
	}

	/**
	 * Sets the state issuance.
	 * 
	 * @param stateIssuance
	 *            the new state issuance
	 */
	public void setStateIssuance(Integer stateIssuance) {
		this.stateIssuance = stateIssuance;
	}

	/**
	 * Gets the issuer.
	 * 
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 * 
	 * @param issuer
	 *            the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the securities.
	 * 
	 * @return the securities
	 */
	public List<Security> getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 * 
	 * @param securities
	 *            the new securities
	 */
	public void setSecurities(List<Security> securities) {
		this.securities = securities;
	}

	/**
	 * Gets the issuance certificates.
	 * 
	 * @return the issuance certificates
	 */
	public List<IssuanceCertificateAux> getIssuanceCertificates() {
		return issuanceCertificates;
	}

	/**
	 * Sets the issuance certificates.
	 * 
	 * @param issuanceCertificates
	 *            the new issuance certificates
	 */
	public void setIssuanceCertificates(
			List<IssuanceCertificateAux> issuanceCertificates) {
		this.issuanceCertificates = issuanceCertificates;
	}

	/**
	 * Gets the issuance history balances.
	 * 
	 * @return the issuance history balances
	 */
	public List<IssuanceHistoryBalance> getIssuanceHistoryBalances() {
		return issuanceHistoryBalances;
	}

	/**
	 * Sets the issuance history balances.
	 * 
	 * @param issuanceHistoryBalances
	 *            the new issuance history balances
	 */
	public void setIssuanceHistoryBalances(
			List<IssuanceHistoryBalance> issuanceHistoryBalances) {
		this.issuanceHistoryBalances = issuanceHistoryBalances;
	}

	/*
	 * Addition Info field -- Start
	 */
	/**
	 * Gets the instrument type description.
	 * 
	 * @return the instrument type description
	 */
	public String getInstrumentTypeDescription() {
		try {
			if (getInstrumentType() != null) {
				return InstrumentType.get(getInstrumentType()).getValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Gets the security type description.
	 * 
	 * @return the security type description
	 */
	public String getSecurityTypeDescription() {
		try {
			if(getSecurityType() != null && SecurityType.get(getSecurityType())!=null) {
				return SecurityType.get(getSecurityType()).getValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	/**
	 * Gets the security class description.
	 * 
	 * @return the security class description
	 */
	public String getSecurityClassDescription() {
		if(getSecurityClass()!=null && SecurityClassType.get(getSecurityClass())!=null){
			return SecurityClassType.get(getSecurityClass()).getValue();
		}		
		return "";
	}

	/**
	 * Gets the state description.
	 * 
	 * @return the state description
	 */
	public String getStateDescription() {
		try {
			if (getStateChk() != null) {
				return IssuanceAuxStateType.get(getStateChk()).getValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Gets the amortization amount.
	 * 
	 * @return the amortization amount
	 */
	public BigDecimal getAmortizationAmount() {
		return amortizationAmount;
	}

	/**
	 * Sets the amortization amount.
	 * 
	 * @param amortizationAmount
	 *            the new amortization amount
	 */
	public void setAmortizationAmount(BigDecimal amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	/**
	 * Gets the circulation amount.
	 * 
	 * @return the circulation amount
	 */
	public BigDecimal getCirculationAmount() {
		return circulationAmount;
	}

	/**
	 * Sets the circulation amount.
	 * 
	 * @param circulationAmount
	 *            the new circulation amount
	 */
	public void setCirculationAmount(BigDecimal circulationAmount) {
		this.circulationAmount = circulationAmount;
	}

	/**
	 * Gets the issuance amount.
	 * 
	 * @return the issuance amount
	 */
	public BigDecimal getIssuanceAmount() {
		return issuanceAmount;
	}

	/**
	 * Sets the issuance amount.
	 * 
	 * @param issuanceAmount
	 *            the new issuance amount
	 */
	public void setIssuanceAmount(BigDecimal issuanceAmount) {
		this.issuanceAmount = issuanceAmount;
	}

	/**
	 * Gets the placed amount.
	 * 
	 * @return the placed amount
	 */
	public BigDecimal getPlacedAmount() {
		return placedAmount;
	}

	/**
	 * Sets the placed amount.
	 * 
	 * @param placedAmount
	 *            the new placed amount
	 */
	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	/**
	 * Gets the current tranche.
	 * 
	 * @return the current tranche
	 */
	public Integer getCurrentTranche() {
		return currentTranche;
	}

	/**
	 * Sets the current tranche.
	 * 
	 * @param currentTranche
	 *            the new current tranche
	 */
	public void setCurrentTranche(Integer currentTranche) {
		this.currentTranche = currentTranche;
	}

	/**
	 * Gets the placement type.
	 *
	 * @return the placement type
	 */
	public Integer getPlacementType() {
		return placementType;
	}

	/**
	 * Sets the placement type.
	 *
	 * @param placementType the new placement type
	 */
	public void setPlacementType(Integer placementType) {
		this.placementType = placementType;
	}
	
	/**
	 * Gets the placement type description.
	 *
	 * @return the placement type description
	 */
	public String getPlacementTypeDescription() {
		if(placementType==null){
			return null;
		}
		return SecurityPlacementType.lookup.get(this.getPlacementType()).getValue();
	}

	/**
	 * Gets the credit rating scales.
	 *
	 * @return the credit rating scales
	 */
	public Integer getCreditRatingScales() {
		return creditRatingScales;
	}

	/**
	 * Sets the credit rating scales.
	 *
	 * @param creditRatingScales the new credit rating scales
	 */
	public void setCreditRatingScales(Integer creditRatingScales) {
		this.creditRatingScales = creditRatingScales;
	}

	/**
	 * Gets the amount confirmed segments.
	 *
	 * @return the amount confirmed segments
	 */
	public BigDecimal getAmountConfirmedSegments() {
		return amountConfirmedSegments;
	}

	/**
	 * Sets the amount confirmed segments.
	 *
	 * @param amountConfirmedSegments the new amount confirmed segments
	 */
	public void setAmountConfirmedSegments(BigDecimal amountConfirmedSegments) {
		this.amountConfirmedSegments = amountConfirmedSegments;
	}

	/**
	 * Gets the amount opened segments.
	 *
	 * @return the amount opened segments
	 */
	public BigDecimal getAmountOpenedSegments() {
		return amountOpenedSegments;
	}

	/**
	 * Gets the negotiation factor.
	 *
	 * @return the negotiation factor
	 */
	public Double getNegotiationFactor() {
		return negotiationFactor;
	}

	/**
	 * Sets the negotiation factor.
	 *
	 * @param negotiationFactor the new negotiation factor
	 */
	public void setNegotiationFactor(Double negotiationFactor) {
		this.negotiationFactor = negotiationFactor;
	}

	/**
	 * Sets the amount opened segments.
	 *
	 * @param amountOpenedSegments the new amount opened segments
	 */
	public void setAmountOpenedSegments(BigDecimal amountOpenedSegments) {
		this.amountOpenedSegments = amountOpenedSegments;
	}
	
	/**
	 * Gets the amount available.
	 *
	 * @return the amount available
	 */
	public BigDecimal getAmountAvailable(){
		if(idIssuanceCodePk==null || idIssuanceCodePk.length()==0){
			return null;
		}
		if(amountConfirmedSegments==null || amountOpenedSegments==null || issuanceAmount==null || placedAmount==null){
			return BigDecimal.ZERO;
		}
		return issuanceAmount.subtract(amountConfirmedSegments);
	}
	
	/**
	 * Gets the ind have certificate.
	 *
	 * @return the ind have certificate
	 */
	public Integer getIndHaveCertificate() {
		return indHaveCertificate;
	}

	/**
	 * Sets the ind have certificate.
	 *
	 * @param indHaveCertificate the new ind have certificate
	 */
	public void setIndHaveCertificate(Integer indHaveCertificate) {
		this.indHaveCertificate = indHaveCertificate;
	}

	/**
	 * Gets the ind primary placement.
	 *
	 * @return the ind primary placement
	 */
	public Integer getIndPrimaryPlacement() {
		return indPrimaryPlacement;
	}

	/**
	 * Sets the ind primary placement.
	 *
	 * @param indPrimaryPlacement the new ind primary placement
	 */
	public void setIndPrimaryPlacement(Integer indPrimaryPlacement) {
		this.indPrimaryPlacement = indPrimaryPlacement;
	}
	
	
	/* Security Type Indicator Start */
	
	/**
	 * Gets the amount registered segment.
	 *
	 * @return the amount registered segment
	 */
	public BigDecimal getAmountRegisteredSegment() {
		return amountRegisteredSegment;
	}

	/**
	 * Sets the amount registered segment.
	 *
	 * @param amountRegisteredSegment the new amount registered segment
	 */
	public void setAmountRegisteredSegment(BigDecimal amountRegisteredSegment) {
		this.amountRegisteredSegment = amountRegisteredSegment;
	}

	/**
	 * Gets the registered tranches.
	 *
	 * @return the registered tranches
	 */
	public Integer getRegisteredTranches() {
		return registeredTranches;
	}

	/**
	 * Sets the registered tranches.
	 *
	 * @param registeredTranches the new registered tranches
	 */
	public void setRegisteredTranches(Integer registeredTranches) {
		this.registeredTranches = registeredTranches;
	}

	/**
	 * Gets the confirmed tranches.
	 *
	 * @return the confirmed tranches
	 */
	public Integer getConfirmedTranches() {
		return confirmedTranches;
	}

	/**
	 * Sets the confirmed tranches.
	 *
	 * @param confirmedTranches the new confirmed tranches
	 */
	public void setConfirmedTranches(Integer confirmedTranches) {
		this.confirmedTranches = confirmedTranches;
	}
	

	

	/**
	 * Gets the amount confirmed segment.
	 *
	 * @return the amount confirmed segment
	 */
	public BigDecimal getAmountConfirmedSegment() {
		return amountConfirmedSegment;
	}

	/**
	 * Sets the amount confirmed segment.
	 *
	 * @param amountConfirmedSegment the new amount confirmed segment
	 */
	public void setAmountConfirmedSegment(BigDecimal amountConfirmedSegment) {
		this.amountConfirmedSegment = amountConfirmedSegment;
	}

	/**
	 * Gets the closed tranches.
	 *
	 * @return the closed tranches
	 */
	public Integer getClosedTranches() {
		return closedTranches;
	}

	/**
	 * Sets the closed tranches.
	 *
	 * @param closedTranches the new closed tranches
	 */
	public void setClosedTranches(Integer closedTranches) {
		this.closedTranches = closedTranches;
	}
	
	

	/**
	 * Gets the amount opened segment.
	 *
	 * @return the amount opened segment
	 */
	public BigDecimal getAmountOpenedSegment() {
		return amountOpenedSegment;
	}

	/**
	 * Sets the amount opened segment.
	 *
	 * @param amountOpenedSegment the new amount opened segment
	 */
	public void setAmountOpenedSegment(BigDecimal amountOpenedSegment) {
		this.amountOpenedSegment = amountOpenedSegment;
	}

	/**
	 * Gets the opened tranches.
	 *
	 * @return the opened tranches
	 */
	public Integer getOpenedTranches() {
		return openedTranches;
	}

	/**
	 * Sets the opened tranches.
	 *
	 * @param openedTranches the new opened tranches
	 */
	public void setOpenedTranches(Integer openedTranches) {
		this.openedTranches = openedTranches;
	}

	/**
	 * Gets the ind regulator report.
	 *
	 * @return the ind regulator report
	 */
	public Integer getIndRegulatorReport() {
		return indRegulatorReport;
	}

	/**
	 * Sets the ind regulator report.
	 *
	 * @param indRegulatorReport the new ind regulator report
	 */
	public void setIndRegulatorReport(Integer indRegulatorReport) {
		this.indRegulatorReport = indRegulatorReport;
	}
	
	

//	public Integer getOfferType() {
//		return offerType;
//	}
//
//	public void setOfferType(Integer offerType) {
//		this.offerType = offerType;
//	}

	/**
 * Checks if is issue type participation installments.
 *
 * @return true, if is issue type participation installments
 */
public boolean isIssueTypeParticipationInstallments(){
		if(SecurityClassType.CFC.getCode().equals( getSecurityClass()  )){
			return true;
		}
		return false;			
	}
		
	/* Security Type Indicator End */
	
	/* Security Class */
	
	/**
	 * Checks if is dpf security class.
	 *
	 * @return true, if is dpf security class
	 */
	public boolean isDpfSecurityClass(){
		if(SecurityClassType.DPF.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is dpa security class.
	 *
	 * @return true, if is dpa security class
	 */
	public boolean isDpaSecurityClass(){
		if(SecurityClassType.DPA.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is dpf dpa security class.
	 *
	 * @return true, if is dpf dpa security class
	 */
	public boolean isDpfDpaSecurityClass(){
		if(isDpfSecurityClass() || isDpaSecurityClass()){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is acc rf security class.
	 *
	 * @return true, if is acc rf security class
	 */
	public boolean isAccRfSecurityClass(){
		if(SecurityClassType.ACC_RF.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}	
	
	/* Security Class */
	
	/*	IssuanceAux State Start	*/
	
	/**
	 * Checks states. Issuance maker checker 
	 *
	 */
	public boolean isRegisteredState(){
		if(IssuanceAuxStateType.REGISTERED.getCode().equals( getStateIssuance() ))
			return true;
		return false;
	}
	public boolean isReviewedState(){
		if(IssuanceAuxStateType.REVIEWED.getCode().equals( getStateIssuance() ))
			return true;
		return false;
	}
	public boolean isApprobedState(){
		if(IssuanceAuxStateType.APPROBED.getCode().equals( getStateIssuance() ))
			return true;
		return false;
	}
	public boolean isConfirmedState(){
		if(IssuanceAuxStateType.CONFIRMED.getCode().equals( getStateIssuance() ))
			return true;
		return false;
	}
	public boolean isRejectedState(){
		if(IssuanceAuxStateType.REJECTED.getCode().equals( getStateIssuance() ))
			return true;
		return false;
	}
		
	/*  Issuance State End	*/
	
	/**
	 * Checks if is physical issue type.
	 * 
	 * @return true, if is mixed issue type
	 */
	public boolean isPhysicalIssuanceForm() {
		if (getIssuanceType() != null
				&& getIssuanceType().equals(
						IssuanceType.PHYSICAL.getCode())) {
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is mixed issue type.
	 * 
	 * @return true, if is mixed issue type
	 */
	public boolean isMixedIssuanceForm() {
		if (getIssuanceType() != null
				&& getIssuanceType().equals(
						IssuanceType.MIXED.getCode())) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if is desmaterialized issue type.
	 * 
	 * @return true, if is desmaterialized issue type
	 */
	public boolean isDesmaterializedIssuanceForm(){
		if(getIssuanceType()!=null
				&& getIssuanceType().equals(  IssuanceType.DEMATERIALIZED.getCode() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is primary placement negotiation.
	 *
	 * @return true, if is primary placement negotiation
	 */
	public boolean isPrimaryPlacementNegotiation(){
		if(getIndPrimaryPlacement()!=null && 
				getIndPrimaryPlacement().equals( BooleanType.YES.getCode() )){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Checks if is fixed income instrument type.
	 *
	 * @return true, if is fixed income instrument type
	 */
	public boolean isFixedIncomeInstrumentType() {
		if (getInstrumentType() != null
				&& getInstrumentType().equals(
						InstrumentType.FIXED_INCOME.getCode())) {
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is physical issue type.
	 *
	 * @return true, if is physical issue type
	 */
	public boolean isPhysicalIssueType() {
		if (getIssuanceType() != null
				&& getIssuanceType().equals(
						IssuanceType.PHYSICAL.getCode())) {
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is fixed instrument type acc.
	 *
	 * @return true, if is fixed instrument type acc
	 */
	public boolean isFixedInstrumentTypeAcc(){
		if(InstrumentType.FIXED_INCOME.getCode().equals( getInstrumentType() ) && 
				!(SecurityClassType.ACC_RF.getCode().equals( getSecurityClass() ))){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is variable instrument type acc.
	 *
	 * @return true, if is variable instrument type acc
	 */
	public boolean isVariableInstrumentTypeAcc(){
		if(InstrumentType.VARIABLE_INCOME.getCode().equals( getInstrumentType() ) || 
				(SecurityClassType.ACC_RF.getCode().equals(getSecurityClass()))){
			return true;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();
		if(loggerUser != null) {
			if(loggerUser.getIdPrivilegeOfSystem() != null) {
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
			}
            if(loggerUser.getAuditTime() != null) {
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            }
            if(loggerUser.getIpAddress() != null) {
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            }
            if(loggerUser.getUserName() != null) {
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            }            
		} else {
			lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = objLoggerUser.getAuditTime();
			lastModifyIp = objLoggerUser.getIpAddress();
			lastModifyUser = objLoggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("issuanceCertificates", issuanceCertificates);
        detailsMap.put("issuanceFiles",issuanceFiles);
        return detailsMap;

	}
	
	/**
	 * Gets the placed securities balance.
	 *
	 * @return the placed securities balance
	 */
	public Integer getPlacedSecuritiesBalance() {
		return placedSecuritiesBalance;
	}

	/**
	 * Sets the placed securities balance.
	 *
	 * @param placedSecuritiesBalance the new placed securities balance
	 */
	public void setPlacedSecuritiesBalance(Integer placedSecuritiesBalance) {
		this.placedSecuritiesBalance = placedSecuritiesBalance;
	}

	
	
	/**
	 * Checks if is securities in negotiations.
	 *
	 * @return true, if is securities in negotiations
	 */
	public boolean isSecuritiesInNegotiations() {
		return securitiesInNegotiations;
	}

	/**
	 * Sets the securities in negotiations.
	 *
	 * @param securitiesInNegotiations the new securities in negotiations
	 */
	public void setSecuritiesInNegotiations(boolean securitiesInNegotiations) {
		this.securitiesInNegotiations = securitiesInNegotiations;
	}

	/**
	 * Instantiates a new issuance.
	 *
	 * @param idIssuanceCodePk the id issuance code pk
	 * @param stateIssuance the state issuance
	 */
	public IssuanceAux(String idIssuanceCodePk, Integer stateIssuance) {
		this.idIssuanceCodePk = idIssuanceCodePk;
		this.stateIssuance = stateIssuance;
	}

	/**
	 * Gets the issuance files.
	 *
	 * @return the issuance files
	 */
	public List<IssuanceFileAux> getIssuanceFiles() {
		return issuanceFiles;
	}

	/**
	 * Sets the issuance files.
	 *
	 * @param issuanceFiles the new issuance files
	 */
	public void setIssuanceFiles(List<IssuanceFileAux> issuanceFiles) {
		this.issuanceFiles = issuanceFiles;
	}

	/**
	 * Checks if is securities registed.
	 *
	 * @return true, if is securities registed
	 */
	public boolean isSecuritiesRegisted() {
		return securitiesRegisted;
	}

	/**
	 * Sets the securities registed.
	 *
	 * @param securitiesRegisted the new securities registed
	 */
	public void setSecuritiesRegisted(boolean securitiesRegisted) {
		this.securitiesRegisted = securitiesRegisted;
	}

	/**
	 * Gets the valid tranches.
	 *
	 * @return the valid tranches
	 */
	public Integer getValidTranches() {
		return validTranches;
	}

	/**
	 * Sets the valid tranches.
	 *
	 * @param validTranches the new valid tranches
	 */
	public void setValidTranches(Integer validTranches) {
		this.validTranches = validTranches;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}	
    
	public Integer getStateChk() {
		return stateChk;
	}

	public void setStateChk(Integer stateChk) {
		this.stateChk = stateChk;
	}
	
	//** sincronizado para produccion
	public String getEndorsement() {
		return endorsement;
	}

	public void setEndorsement(String endorsement) {
		this.endorsement = endorsement;
	}
	
	/** issue 767 */
	public Date getTestimonyDate() {
		return testimonyDate;
	}

	public void setTestimonyDate(Date testimonyDate) {
		this.testimonyDate = testimonyDate;
	}

	public String getTestimonyNumber() {
		return testimonyNumber;
	}

	public void setTestimonyNumber(String testimonyNumber) {
		this.testimonyNumber = testimonyNumber;
	}

	public Integer getIndExchangeSecurities() {
		return indExchangeSecurities;
	}

	public void setIndExchangeSecurities(Integer indExchangeSecurities) {
		this.indExchangeSecurities = indExchangeSecurities;
	}

	@Override
	public String toString() {
		return "IssuanceAux [idIssuanceCodePk=" + idIssuanceCodePk + ", amortizationAmount=" + amortizationAmount
				+ ", circulationAmount=" + circulationAmount + ", currency=" + currency + ", description=" + description
				+ ", expirationDate=" + expirationDate + ", instrumentType=" + instrumentType + ", issuanceAmount="
				+ issuanceAmount + ", geographicLocation=" + geographicLocation + ", issuanceDate=" + issuanceDate
				+ ", issuanceType=" + issuanceType + ", lastModifyApp=" + lastModifyApp + ", lastModifyDate="
				+ lastModifyDate + ", lastModifyIp=" + lastModifyIp + ", lastModifyUser=" + lastModifyUser
				+ ", numberPlacementTranches=" + numberPlacementTranches + ", numberTotalTranches="
				+ numberTotalTranches + ", placedAmount=" + placedAmount + ", registryDate=" + registryDate
				+ ", registryUser=" + registryUser + ", resolutionDate=" + resolutionDate + ", resolutionNumber="
				+ resolutionNumber + ", securityClass=" + securityClass + ", securityType=" + securityType
				+ ", stateIssuance=" + stateIssuance + ", amountConfirmedSegments=" + amountConfirmedSegments
				+ ", amountOpenedSegments=" + amountOpenedSegments + ", currentTranche=" + currentTranche
				+ ", placementType=" + placementType + ", indHaveCertificate=" + indHaveCertificate
				+ ", indPrimaryPlacement=" + indPrimaryPlacement + ", indRegulatorReport=" + indRegulatorReport
				+ ", creditRatingScales=" + creditRatingScales + ", version=" + version + ", issuer=" + issuer
				+ ", negotiationFactor=" + negotiationFactor + ", stateChk=" + stateChk + ", endorsement=" + endorsement
				+ ", testimonyDate=" + testimonyDate + ", testimonyNumber=" + testimonyNumber + ", securities="
				+ securities + ", issuanceCertificates=" + issuanceCertificates + ", issuanceFiles=" + issuanceFiles
				+ ", issuanceHistoryBalances=" + issuanceHistoryBalances + ", indExchangeSecurities="
				+ indExchangeSecurities + ", amountRegisteredSegment=" + amountRegisteredSegment
				+ ", registeredTranches=" + registeredTranches + ", amountConfirmedSegment=" + amountConfirmedSegment
				+ ", amountOpenedSegment=" + amountOpenedSegment + ", openedTranches=" + openedTranches
				+ ", confirmedTranches=" + confirmedTranches + ", closedTranches=" + closedTranches
				+ ", placedSecuritiesBalance=" + placedSecuritiesBalance + ", securitiesInNegotiations="
				+ securitiesInNegotiations + ", securitiesRegisted=" + securitiesRegisted + ", validTranches="
				+ validTranches + "]";
	}

	public Integer getPlacementExpirationDays() {
		return placementExpirationDays;
	}

	public void setPlacementExpirationDays(Integer placementExpirationDays) {
		this.placementExpirationDays = placementExpirationDays;
	}

	public Date getPlacementExpirationDate() {
		return placementExpirationDate;
	}

	public void setPlacementExpirationDate(Date placementExpirationDate) {
		this.placementExpirationDate = placementExpirationDate;
	}
	
	

}