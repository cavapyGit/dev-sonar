package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Master Id 367
public enum PaymentScheduleStateType {
	
	REGTERED(Integer.valueOf(1345),"REGISTRADO"), 
	BLOQUED(Integer.valueOf(1346),"BLOQUEADO");
	
	public final static List<PaymentScheduleStateType> list=new ArrayList<PaymentScheduleStateType>();
	public static final Map<Integer, PaymentScheduleStateType> lookup = new HashMap<Integer, PaymentScheduleStateType>();
	
	private Integer code;
	private String value;
	
	static{
		for(PaymentScheduleStateType d : PaymentScheduleStateType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	

	private PaymentScheduleStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static PaymentScheduleStateType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}
	

	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

}
