package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.type.IssuerBlockRequestMotiveType;
import com.pradera.model.accounts.type.IssuerRequestStateType;
import com.pradera.model.accounts.type.IssuerRequestType;
import com.pradera.model.accounts.type.IssuerUnblockRequestMotiveType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ISSUER_REQUEST database table.
 * 
 */
@Entity
@Table(name="ISSUER_REQUEST")
public class IssuerRequest implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id issuer request pk. */
	@Id
	@SequenceGenerator(name="ISSUER_REQUEST_IDISSUERREQUESTPK_GENERATOR", sequenceName="SQ_ID_ISSUER_REQUEST_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUER_REQUEST_IDISSUERREQUESTPK_GENERATOR")
	@Column(name="ID_ISSUER_REQUEST_PK")
	private Long idIssuerRequestPk;

    /** The action date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="ACTION_DATE")
	private Date actionDate;

	/** The action motive. */
	@Column(name="ACTION_MOTIVE")
	private Integer actionMotive;

	/** The action other motive. */
	@Column(name="ACTION_OTHER_MOTIVE")
	private String actionOtherMotive;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The request motive. */
	@Column(name="REQUEST_MOTIVE")
	private Integer requestMotive;

	/** The request number. */
	@Column(name="REQUEST_NUMBER")
	private Long requestNumber;

	/** The request other motive. */
	@Column(name="REQUEST_OTHER_MOTIVE")
	private String requestOtherMotive;

	/** The request type. */
	@Column(name="REQUEST_TYPE")
	private Integer requestType;

	/** The state. */
	@Column(name="STATE")
	private Integer state;
	
	//bi-directional many-to-one association to Issuer
    /** The issuer. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;
    
    //bi-directional one-to-one association to IssuerModification
  	/** The issuer files. */
    @OneToMany(mappedBy="issuerRequest",cascade=CascadeType.ALL)
  	private List<IssuerFile> issuerFiles;

	//bi-directional one-to-one association to IssuerModification
	/** The issuer history states. */
	@OneToMany(mappedBy="issuerRequest",cascade=CascadeType.ALL)
	private List<IssuerHistoryState> issuerHistoryStates;
	
	/** The issuer file histories. */
	@OneToMany(mappedBy="issuerRequest",cascade=CascadeType.ALL)
  	private List<IssuerFileHistory> issuerFileHistories;
	
	/** The issuer histories. */
	@OneToMany(mappedBy="issuerRequest")
  	private List<IssuerHistory> issuerHistories;
	
	/** The observations. */
	@Column(name="COMMENTS")
	private String observations;

	/**
     * Instantiates a new issuer request.
     */
    public IssuerRequest() {
    }
    
    /**
     * Gets the request type description.
     *
     * @return the request type description
     */
    public String getRequestTypeDescription(){
    	if(Validations.validateIsNotNullAndPositive(requestType)){
    		return IssuerRequestType.get(requestType).getValue();
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the request motive description.
     *
     * @return the request motive description
     */
    public String getRequestMotiveDescription(){
    	if(Validations.validateIsNotNullAndPositive(requestMotive) && 
    			Validations.validateIsNotNullAndPositive(requestType)){
    		if(IssuerRequestType.BLOCK.getCode().equals(requestType)){
    			return IssuerBlockRequestMotiveType.get(requestMotive).getValue();
    		} else if(IssuerRequestType.UNBLOCK.getCode().equals(requestType)){
    			return IssuerUnblockRequestMotiveType.get(requestMotive).getValue();
    		}/*  else if(IssuerBlockRequestTypeType.ANNULMENT.getCode().equals(requestType)){
    			return ParticipantAnnulMotiveType.get(requestMotive).getValue();
    		}*/ else {
    			return null;
    		}
    		
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the issuer request state description.
     *
     * @return the issuer request state description
     */
    public String getIssuerRequestStateDescription(){
    	if(Validations.validateIsNotNullAndPositive(state)){
    		return IssuerRequestStateType.get(state).getValue();
    	} else {
    		return null;
    	}
    }

	/**
	 * Gets the id issuer request pk.
	 *
	 * @return the id issuer request pk
	 */
	public Long getIdIssuerRequestPk() {
		return idIssuerRequestPk;
	}

	/**
	 * Sets the id issuer request pk.
	 *
	 * @param idIssuerRequestPk the new id issuer request pk
	 */
	public void setIdIssuerRequestPk(Long idIssuerRequestPk) {
		this.idIssuerRequestPk = idIssuerRequestPk;
	}

	/**
	 * Gets the action date.
	 *
	 * @return the action date
	 */
	public Date getActionDate() {
		return actionDate;
	}

	/**
	 * Sets the action date.
	 *
	 * @param actionDate the new action date
	 */
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 * Gets the action motive.
	 *
	 * @return the action motive
	 */
	public Integer getActionMotive() {
		return actionMotive;
	}

	/**
	 * Sets the action motive.
	 *
	 * @param actionMotive the new action motive
	 */
	public void setActionMotive(Integer actionMotive) {
		this.actionMotive = actionMotive;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the request motive.
	 *
	 * @return the request motive
	 */
	public Integer getRequestMotive() {
		return requestMotive;
	}

	/**
	 * Sets the request motive.
	 *
	 * @param requestMotive the new request motive
	 */
	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the request other motive.
	 *
	 * @return the request other motive
	 */
	public String getRequestOtherMotive() {
		return requestOtherMotive;
	}

	/**
	 * Sets the request other motive.
	 *
	 * @param requestOtherMotive the new request other motive
	 */
	public void setRequestOtherMotive(String requestOtherMotive) {
		this.requestOtherMotive = requestOtherMotive;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the issuer files.
	 *
	 * @return the issuer files
	 */
	public List<IssuerFile> getIssuerFiles() {
		return issuerFiles;
	}

	/**
	 * Sets the issuer files.
	 *
	 * @param issuerFiles the new issuer files
	 */
	public void setIssuerFiles(List<IssuerFile> issuerFiles) {
		this.issuerFiles = issuerFiles;
	}

	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}

	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}

	/**
	 * Gets the issuer history states.
	 *
	 * @return the issuer history states
	 */
	public List<IssuerHistoryState> getIssuerHistoryStates() {
		return issuerHistoryStates;
	}

	/**
	 * Sets the issuer history states.
	 *
	 * @param issuerHistoryStates the new issuer history states
	 */
	public void setIssuerHistoryStates(List<IssuerHistoryState> issuerHistoryStates) {
		this.issuerHistoryStates = issuerHistoryStates;
	}

	/**
	 * Gets the action other motive.
	 *
	 * @return the action other motive
	 */
	public String getActionOtherMotive() {
		return actionOtherMotive;
	}

	/**
	 * Sets the action other motive.
	 *
	 * @param actionOtherMotive the new action other motive
	 */
	public void setActionOtherMotive(String actionOtherMotive) {
		this.actionOtherMotive = actionOtherMotive;
	}		

	/**
	 * Gets the issuer file histories.
	 *
	 * @return the issuer file histories
	 */
	public List<IssuerFileHistory> getIssuerFileHistories() {
		return issuerFileHistories;
	}

	/**
	 * Sets the issuer file histories.
	 *
	 * @param issuerFileHistories the new issuer file histories
	 */
	public void setIssuerFileHistories(List<IssuerFileHistory> issuerFileHistories) {
		this.issuerFileHistories = issuerFileHistories;
	}
		

	/**
	 * Gets the issuer histories.
	 *
	 * @return the issuer histories
	 */
	public List<IssuerHistory> getIssuerHistories() {
		return issuerHistories;
	}

	/**
	 * Sets the issuer histories.
	 *
	 * @param issuerHistories the new issuer histories
	 */
	public void setIssuerHistories(List<IssuerHistory> issuerHistories) {
		this.issuerHistories = issuerHistories;
	}
	

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap =
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("issuerFiles", issuerFiles);
        detailsMap.put("issuerHistoryStates", issuerHistoryStates);
        detailsMap.put("issuerFileHistories", issuerFileHistories);
        return detailsMap;
	}
    	
}