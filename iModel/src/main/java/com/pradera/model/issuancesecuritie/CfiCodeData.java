package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CFI_CODE_DATA database table.
 * 
 */
@Entity
@Table(name="CFI_CODE_DATA")
public class CfiCodeData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CFI_CODE_DATA_IDISINCODEPK_GENERATOR", sequenceName="SQ_ID_SECURITY_CODE_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFI_CODE_DATA_IDISINCODEPK_GENERATOR")
	@Column(name="ID_SECURITY_CODE_PK")
	private String idSecurityCodePk;

	@Column(name="BUY_SELL")
	private BigDecimal buySell;

	@Column(name="COMMERCIAL_TYPE")
	private BigDecimal commercialType;

	@Column(name="GUARANTY_TYPE")
	private BigDecimal guarantyType;

	@Column(name="INCOME_TYPE")
	private BigDecimal incomeType;

	@Column(name="ISSUE_ORDER")
	private BigDecimal issueOrder;

	@Column(name="ISSUEANCE_FORM")
	private BigDecimal issueanceForm;

	@Column(name="PAY_STATE")
	private BigDecimal payState;

	@Column(name="POLICY_DISTRIBUTION")
	private BigDecimal policyDistribution;

	@Column(name="POLICY_INVESTMENT")
	private BigDecimal policyInvestment;

	@Column(name="REBATE_TYPE")
	private BigDecimal rebateType;

	@Column(name="REDEMPTION_TYPE")
	private BigDecimal redemptionType;

	@Column(name="RIGHT_TO_VOTE")
	private BigDecimal rightToVote;

	@Column(name="SUBJACENTS_ASSETS")
	private BigDecimal subjacentsAssets;

	@Column(name="TRANSFER_PROPERTY")
	private BigDecimal transferProperty;

	//bi-directional one-to-one association to Security
//	@OneToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_SECURITY_CODE_PK")
//	private Security security;

    public CfiCodeData() {
    }

	public String getIdSecurityCodePk() {
		return this.idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public BigDecimal getBuySell() {
		return this.buySell;
	}

	public void setBuySell(BigDecimal buySell) {
		this.buySell = buySell;
	}

	public BigDecimal getCommercialType() {
		return this.commercialType;
	}

	public void setCommercialType(BigDecimal commercialType) {
		this.commercialType = commercialType;
	}

	public BigDecimal getGuarantyType() {
		return this.guarantyType;
	}

	public void setGuarantyType(BigDecimal guarantyType) {
		this.guarantyType = guarantyType;
	}

	public BigDecimal getIncomeType() {
		return this.incomeType;
	}

	public void setIncomeType(BigDecimal incomeType) {
		this.incomeType = incomeType;
	}

	public BigDecimal getIssueOrder() {
		return this.issueOrder;
	}

	public void setIssueOrder(BigDecimal issueOrder) {
		this.issueOrder = issueOrder;
	}

	public BigDecimal getIssueanceForm() {
		return this.issueanceForm;
	}

	public void setIssueanceForm(BigDecimal issueanceForm) {
		this.issueanceForm = issueanceForm;
	}

	public BigDecimal getPayState() {
		return this.payState;
	}

	public void setPayState(BigDecimal payState) {
		this.payState = payState;
	}

	public BigDecimal getPolicyDistribution() {
		return this.policyDistribution;
	}

	public void setPolicyDistribution(BigDecimal policyDistribution) {
		this.policyDistribution = policyDistribution;
	}

	public BigDecimal getPolicyInvestment() {
		return this.policyInvestment;
	}

	public void setPolicyInvestment(BigDecimal policyInvestment) {
		this.policyInvestment = policyInvestment;
	}

	public BigDecimal getRebateType() {
		return this.rebateType;
	}

	public void setRebateType(BigDecimal rebateType) {
		this.rebateType = rebateType;
	}

	public BigDecimal getRedemptionType() {
		return this.redemptionType;
	}

	public void setRedemptionType(BigDecimal redemptionType) {
		this.redemptionType = redemptionType;
	}

	public BigDecimal getRightToVote() {
		return this.rightToVote;
	}

	public void setRightToVote(BigDecimal rightToVote) {
		this.rightToVote = rightToVote;
	}

	public BigDecimal getSubjacentsAssets() {
		return this.subjacentsAssets;
	}

	public void setSubjacentsAssets(BigDecimal subjacentsAssets) {
		this.subjacentsAssets = subjacentsAssets;
	}

	public BigDecimal getTransferProperty() {
		return this.transferProperty;
	}

	public void setTransferProperty(BigDecimal transferProperty) {
		this.transferProperty = transferProperty;
	}
}