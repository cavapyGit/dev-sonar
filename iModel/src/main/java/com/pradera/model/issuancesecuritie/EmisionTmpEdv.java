package com.pradera.model.issuancesecuritie;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "EMISION_TMP_EDV")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmisionTmpEdv.findAll", query = "SELECT e FROM EmisionTmpEdv e"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnSerie", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnSerie = :emnSerie"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnCodinst", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnCodinst = :emnCodinst"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnCodem", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnCodem = :emnCodem"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnCodemision", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnCodemision = :emnCodemision"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnMonedaGen", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnMonedaGen = :emnMonedaGen"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnMoneda", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnMoneda = :emnMoneda"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnMetodoValuacion", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnMetodoValuacion = :emnMetodoValuacion"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnFechaEmision", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnFechaEmision = :emnFechaEmision"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnPlazo", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnPlazo = :emnPlazo"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnFechaVenci", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnFechaVenci = :emnFechaVenci"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnTrem", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnTrem = :emnTrem"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnTasaTxt", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnTasaTxt = :emnTasaTxt"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnTdesc", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnTdesc = :emnTdesc"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnPlazoAmortiza", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnPlazoAmortiza = :emnPlazoAmortiza"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnNroCupones", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnNroCupones = :emnNroCupones"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnValorEmision", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnValorEmision = :emnValorEmision"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnMontoAjuste", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnMontoAjuste = :emnMontoAjuste"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnVn", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnVn = :emnVn"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnAgenteRegistrador", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnAgenteRegistrador = :emnAgenteRegistrador"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnTipoaccion", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnTipoaccion = :emnTipoaccion"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnStatus", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnStatus = :emnStatus"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnFechaBv", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnFechaBv = :emnFechaBv"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnDsc", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnDsc = :emnDsc"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnDsm", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnDsm = :emnDsm"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnDiasredimidos", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnDiasredimidos = :emnDiasredimidos"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnTiporegistroMesa", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnTiporegistroMesa = :emnTiporegistroMesa"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnTipogenericovalor", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnTipogenericovalor = :emnTipogenericovalor"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnLcupNegociable", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnLcupNegociable = :emnLcupNegociable"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnLsubproducto", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnLsubproducto = :emnLsubproducto"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnEdv", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnEdv = :emnEdv"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnLfciNeg", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnLfciNeg = :emnLfciNeg"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnPrepago", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnPrepago = :emnPrepago"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnLsubor", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnLsubor = :emnLsubor"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnLtasafija", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnLtasafija = :emnLtasafija"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnTipobono", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnTipobono = :emnTipobono"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnSerieacc", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnSerieacc = :emnSerieacc"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnLtasaDif", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnLtasaDif = :emnLtasaDif"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnTir", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnTir = :emnTir"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnLhabilitado", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnLhabilitado = :emnLhabilitado"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnFeajusteBcb", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnFeajusteBcb = :emnFeajusteBcb"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnEstado", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnEstado = :emnEstado"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnFechaEnvio", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnFechaEnvio = :emnFechaEnvio"),
    @NamedQuery(name = "EmisionTmpEdv.findByEmnNegociable", query = "SELECT e FROM EmisionTmpEdv e WHERE e.emnNegociable = :emnNegociable"),
    @NamedQuery(name = "EmisionTmpEdv.findByLastModifyUser", query = "SELECT e FROM EmisionTmpEdv e WHERE e.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "EmisionTmpEdv.findByLastModifyDate", query = "SELECT e FROM EmisionTmpEdv e WHERE e.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "EmisionTmpEdv.findByLastModifyIp", query = "SELECT e FROM EmisionTmpEdv e WHERE e.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "EmisionTmpEdv.findByLastModifyApp", query = "SELECT e FROM EmisionTmpEdv e WHERE e.lastModifyApp = :lastModifyApp")})
public class EmisionTmpEdv implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EMN_SERIE")
    private String emnSerie;
    @Column(name = "EMN_CODINST")
    private String emnCodinst;
    @Column(name = "EMN_CODEM")
    private String emnCodem;
    @Column(name = "EMN_CODEMISION")
    private String emnCodemision;
    @Column(name = "EMN_MONEDA_GEN")
    private String emnMonedaGen;
    @Column(name = "EMN_MONEDA")
    private String emnMoneda;
    @Column(name = "EMN_METODO_VALUACION")
    private String emnMetodoValuacion;
    @Column(name = "EMN_FECHA_EMISION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date emnFechaEmision;
    @Column(name = "EMN_PLAZO")
    private String emnPlazo;
    @Column(name = "EMN_FECHA_VENCI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date emnFechaVenci;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "EMN_TREM")
    private BigDecimal emnTrem;
    @Column(name = "EMN_TASA_TXT")
    private String emnTasaTxt;
    @Column(name = "EMN_TDESC")
    private BigDecimal emnTdesc;
    @Column(name = "EMN_PLAZO_AMORTIZA")
    private String emnPlazoAmortiza;
    @Column(name = "EMN_NRO_CUPONES")
    private Integer emnNroCupones;
    @Column(name = "EMN_VALOR_EMISION")
    private BigDecimal emnValorEmision;
    @Column(name = "EMN_MONTO_AJUSTE")
    private BigDecimal emnMontoAjuste;
    @Column(name = "EMN_VN")
    private BigDecimal emnVn;
    @Column(name = "EMN_AGENTE_REGISTRADOR")
    private String emnAgenteRegistrador;
    @Column(name = "EMN_TIPOACCION")
    private String emnTipoaccion;
    @Column(name = "EMN_STATUS")
    private String emnStatus;
    @Column(name = "EMN_FECHA_BV")
    @Temporal(TemporalType.TIMESTAMP)
    private Date emnFechaBv;
    @Column(name = "EMN_DSC")
    private String emnDsc;
    @Column(name = "EMN_DSM")
    private String emnDsm;
    @Column(name = "EMN_DIASREDIMIDOS")
    private Short emnDiasredimidos;
    @Column(name = "EMN_TIPOREGISTRO_MESA")
    private String emnTiporegistroMesa;
    @Column(name = "EMN_TIPOGENERICOVALOR")
    private Short emnTipogenericovalor;
    @Column(name = "EMN_LCUP_NEGOCIABLE")
    private String emnLcupNegociable;
    @Column(name = "EMN_LSUBPRODUCTO")
    private String emnLsubproducto;
    @Column(name = "EMN_EDV")
    private String emnEdv;
    @Column(name = "EMN_LFCI_NEG")
    private String emnLfciNeg;
    @Column(name = "EMN_PREPAGO")
    private String emnPrepago;
    @Column(name = "EMN_LSUBOR")
    private String emnLsubor;
    @Column(name = "EMN_LTASAFIJA")
    private String emnLtasafija;
    @Column(name = "EMN_TIPOBONO")
    private String emnTipobono;
    @Column(name = "EMN_SERIEACC")
    private String emnSerieacc;
    @Column(name = "EMN_LTASA_DIF")
    private String emnLtasaDif;
    @Column(name = "EMN_TIR")
    private BigDecimal emnTir;
    @Column(name = "EMN_LHABILITADO")
    private String emnLhabilitado;
    @Column(name = "EMN_FEAJUSTE_BCB")
    @Temporal(TemporalType.TIMESTAMP)
    private Date emnFeajusteBcb;
    @Column(name = "EMN_ESTADO")
    private String emnEstado;
    @Column(name = "EMN_FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date emnFechaEnvio;
    @Column(name = "EMN_NEGOCIABLE")
    private String emnNegociable;
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Column(name = "LAST_MODIFY_APP")
    private Long lastModifyApp;

    public EmisionTmpEdv() {
    }

    public EmisionTmpEdv(String emnSerie) {
        this.emnSerie = emnSerie;
    }

    public String getEmnSerie() {
        return emnSerie;
    }

    public void setEmnSerie(String emnSerie) {
        this.emnSerie = emnSerie;
    }

    public String getEmnCodinst() {
        return emnCodinst;
    }

    public void setEmnCodinst(String emnCodinst) {
        this.emnCodinst = emnCodinst;
    }

    public String getEmnCodem() {
        return emnCodem;
    }

    public void setEmnCodem(String emnCodem) {
        this.emnCodem = emnCodem;
    }

    public String getEmnCodemision() {
        return emnCodemision;
    }

    public void setEmnCodemision(String emnCodemision) {
        this.emnCodemision = emnCodemision;
    }

    public String getEmnMonedaGen() {
        return emnMonedaGen;
    }

    public void setEmnMonedaGen(String emnMonedaGen) {
        this.emnMonedaGen = emnMonedaGen;
    }

    public String getEmnMoneda() {
        return emnMoneda;
    }

    public void setEmnMoneda(String emnMoneda) {
        this.emnMoneda = emnMoneda;
    }

    public String getEmnMetodoValuacion() {
        return emnMetodoValuacion;
    }

    public void setEmnMetodoValuacion(String emnMetodoValuacion) {
        this.emnMetodoValuacion = emnMetodoValuacion;
    }

    public Date getEmnFechaEmision() {
        return emnFechaEmision;
    }

    public void setEmnFechaEmision(Date emnFechaEmision) {
        this.emnFechaEmision = emnFechaEmision;
    }

    public String getEmnPlazo() {
        return emnPlazo;
    }

    public void setEmnPlazo(String emnPlazo) {
        this.emnPlazo = emnPlazo;
    }

    public Date getEmnFechaVenci() {
        return emnFechaVenci;
    }

    public void setEmnFechaVenci(Date emnFechaVenci) {
        this.emnFechaVenci = emnFechaVenci;
    }

    public BigDecimal getEmnTrem() {
        return emnTrem;
    }

    public void setEmnTrem(BigDecimal emnTrem) {
        this.emnTrem = emnTrem;
    }

    public String getEmnTasaTxt() {
        return emnTasaTxt;
    }

    public void setEmnTasaTxt(String emnTasaTxt) {
        this.emnTasaTxt = emnTasaTxt;
    }

    public BigDecimal getEmnTdesc() {
        return emnTdesc;
    }

    public void setEmnTdesc(BigDecimal emnTdesc) {
        this.emnTdesc = emnTdesc;
    }

    public String getEmnPlazoAmortiza() {
        return emnPlazoAmortiza;
    }

    public void setEmnPlazoAmortiza(String emnPlazoAmortiza) {
        this.emnPlazoAmortiza = emnPlazoAmortiza;
    }

    public Integer getEmnNroCupones() {
        return emnNroCupones;
    }

    public void setEmnNroCupones(Integer emnNroCupones) {
        this.emnNroCupones = emnNroCupones;
    }

    public BigDecimal getEmnValorEmision() {
        return emnValorEmision;
    }

    public void setEmnValorEmision(BigDecimal emnValorEmision) {
        this.emnValorEmision = emnValorEmision;
    }

    public BigDecimal getEmnMontoAjuste() {
        return emnMontoAjuste;
    }

    public void setEmnMontoAjuste(BigDecimal emnMontoAjuste) {
        this.emnMontoAjuste = emnMontoAjuste;
    }

    public BigDecimal getEmnVn() {
        return emnVn;
    }

    public void setEmnVn(BigDecimal emnVn) {
        this.emnVn = emnVn;
    }

    public String getEmnAgenteRegistrador() {
        return emnAgenteRegistrador;
    }

    public void setEmnAgenteRegistrador(String emnAgenteRegistrador) {
        this.emnAgenteRegistrador = emnAgenteRegistrador;
    }

    public String getEmnTipoaccion() {
        return emnTipoaccion;
    }

    public void setEmnTipoaccion(String emnTipoaccion) {
        this.emnTipoaccion = emnTipoaccion;
    }

    public String getEmnStatus() {
        return emnStatus;
    }

    public void setEmnStatus(String emnStatus) {
        this.emnStatus = emnStatus;
    }

    public Date getEmnFechaBv() {
        return emnFechaBv;
    }

    public void setEmnFechaBv(Date emnFechaBv) {
        this.emnFechaBv = emnFechaBv;
    }

    public String getEmnDsc() {
        return emnDsc;
    }

    public void setEmnDsc(String emnDsc) {
        this.emnDsc = emnDsc;
    }

    public String getEmnDsm() {
        return emnDsm;
    }

    public void setEmnDsm(String emnDsm) {
        this.emnDsm = emnDsm;
    }

    public Short getEmnDiasredimidos() {
        return emnDiasredimidos;
    }

    public void setEmnDiasredimidos(Short emnDiasredimidos) {
        this.emnDiasredimidos = emnDiasredimidos;
    }

    public String getEmnTiporegistroMesa() {
        return emnTiporegistroMesa;
    }

    public void setEmnTiporegistroMesa(String emnTiporegistroMesa) {
        this.emnTiporegistroMesa = emnTiporegistroMesa;
    }

    public Short getEmnTipogenericovalor() {
        return emnTipogenericovalor;
    }

    public void setEmnTipogenericovalor(Short emnTipogenericovalor) {
        this.emnTipogenericovalor = emnTipogenericovalor;
    }

    public String getEmnLcupNegociable() {
        return emnLcupNegociable;
    }

    public void setEmnLcupNegociable(String emnLcupNegociable) {
        this.emnLcupNegociable = emnLcupNegociable;
    }

    public String getEmnLsubproducto() {
        return emnLsubproducto;
    }

    public void setEmnLsubproducto(String emnLsubproducto) {
        this.emnLsubproducto = emnLsubproducto;
    }

    public String getEmnEdv() {
        return emnEdv;
    }

    public void setEmnEdv(String emnEdv) {
        this.emnEdv = emnEdv;
    }

    public String getEmnLfciNeg() {
        return emnLfciNeg;
    }

    public void setEmnLfciNeg(String emnLfciNeg) {
        this.emnLfciNeg = emnLfciNeg;
    }

    public String getEmnPrepago() {
        return emnPrepago;
    }

    public void setEmnPrepago(String emnPrepago) {
        this.emnPrepago = emnPrepago;
    }

    public String getEmnLsubor() {
        return emnLsubor;
    }

    public void setEmnLsubor(String emnLsubor) {
        this.emnLsubor = emnLsubor;
    }

    public String getEmnLtasafija() {
        return emnLtasafija;
    }

    public void setEmnLtasafija(String emnLtasafija) {
        this.emnLtasafija = emnLtasafija;
    }

    public String getEmnTipobono() {
        return emnTipobono;
    }

    public void setEmnTipobono(String emnTipobono) {
        this.emnTipobono = emnTipobono;
    }

    public String getEmnSerieacc() {
        return emnSerieacc;
    }

    public void setEmnSerieacc(String emnSerieacc) {
        this.emnSerieacc = emnSerieacc;
    }

    public String getEmnLtasaDif() {
        return emnLtasaDif;
    }

    public void setEmnLtasaDif(String emnLtasaDif) {
        this.emnLtasaDif = emnLtasaDif;
    }

    public BigDecimal getEmnTir() {
        return emnTir;
    }

    public void setEmnTir(BigDecimal emnTir) {
        this.emnTir = emnTir;
    }

    public String getEmnLhabilitado() {
        return emnLhabilitado;
    }

    public void setEmnLhabilitado(String emnLhabilitado) {
        this.emnLhabilitado = emnLhabilitado;
    }

    public Date getEmnFeajusteBcb() {
        return emnFeajusteBcb;
    }

    public void setEmnFeajusteBcb(Date emnFeajusteBcb) {
        this.emnFeajusteBcb = emnFeajusteBcb;
    }

    public String getEmnEstado() {
        return emnEstado;
    }

    public void setEmnEstado(String emnEstado) {
        this.emnEstado = emnEstado;
    }

    public Date getEmnFechaEnvio() {
        return emnFechaEnvio;
    }

    public void setEmnFechaEnvio(Date emnFechaEnvio) {
        this.emnFechaEnvio = emnFechaEnvio;
    }

    public String getEmnNegociable() {
        return emnNegociable;
    }

    public void setEmnNegociable(String emnNegociable) {
        this.emnNegociable = emnNegociable;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emnSerie != null ? emnSerie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmisionTmpEdv)) {
            return false;
        }
        EmisionTmpEdv other = (EmisionTmpEdv) object;
        if ((this.emnSerie == null && other.emnSerie != null) || (this.emnSerie != null && !this.emnSerie.equals(other.emnSerie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
    	System.out.println("-------------------------------------------------");
        System.out.println( "EMN_SERIE:"+emnSerie);
        System.out.println( "EMN_CODINST:"+ emnCodinst);
        System.out.println( "EMN_CODEM:"+ emnCodem);
        System.out.println( "EMN_CODEMISION:"+ emnCodemision);
        System.out.println( "EMN_MONEDA_GEN:"+ emnMonedaGen);
        System.out.println( "EMN_MONEDA:"+ emnMoneda);
        System.out.println( "EMN_METODO_VALUACION:"+ emnMetodoValuacion);
        System.out.println( "EMN_FECHA_EMISION:"+ emnFechaEmision);
        System.out.println( "EMN_PLAZO:"+ emnPlazo);
        System.out.println( "EMN_FECHA_VENCI:"+emnFechaVenci);
        System.out.println( "EMN_TREM:"+emnTrem);
        System.out.println( "EMN_TASA_TXT:"+ emnTasaTxt);
        System.out.println( "EMN_TDESC:"+emnTdesc);
        System.out.println( "EMN_PLAZO_AMORTIZA:"+ emnPlazoAmortiza);
        System.out.println( "EMN_NRO_CUPONES:"+emnNroCupones);
        System.out.println( "EMN_VALOR_EMISION:"+emnValorEmision);
        System.out.println( "EMN_MONTO_AJUSTE:"+emnMontoAjuste);
        System.out.println( "EMN_VN:"+emnVn);
        System.out.println( "EMN_AGENTE_REGISTRADOR:"+ emnAgenteRegistrador);
        System.out.println( "EMN_TIPOACCION:"+ emnTipoaccion);
        System.out.println( "EMN_STATUS:"+ emnStatus);
        System.out.println( "EMN_FECHA_BV:"+ emnFechaBv);
        System.out.println( "EMN_DSC:"+ emnDsc);
        System.out.println( "EMN_DSM:"+ emnDsm);
        System.out.println( "EMN_DIASREDIMIDOS:"+ emnDiasredimidos);
        System.out.println( "EMN_TIPOREGISTRO_MESA:"+ emnTiporegistroMesa);
        System.out.println( "EMN_TIPOGENERICOVALOR:"+emnTipogenericovalor);
        System.out.println( "EMN_LCUP_NEGOCIABLE:"+ emnLcupNegociable);
        System.out.println( "EMN_LSUBPRODUCTO:"+ emnLsubproducto);
        System.out.println( "EMN_EDV:"+ emnEdv);
        System.out.println( "EMN_LFCI_NEG:"+ emnLfciNeg);
        System.out.println( "EMN_PREPAGO:"+ emnPrepago);
        System.out.println( "EMN_LSUBOR:"+ emnLsubor);
        System.out.println( "EMN_LTASAFIJA:"+ emnLtasafija);
        System.out.println( "EMN_TIPOBONO:"+ emnTipobono);
        System.out.println( "EMN_SERIEACC:"+ emnSerieacc);
        System.out.println( "EMN_LTASA_DIF:"+ emnLtasaDif);
        System.out.println( "EMN_TIR:"+emnTir);
        System.out.println( "EMN_LHABILITADO:"+ emnLhabilitado);
        System.out.println( "EMN_FEAJUSTE_BCB:"+ emnFeajusteBcb);
        System.out.println( "EMN_ESTADO:"+ emnEstado);
        System.out.println( "EMN_FECHA_ENVIO:"+ emnFechaEnvio);
        System.out.println( "EMN_NEGOCIABLE:"+ emnNegociable);
        System.out.println( "LAST_MODIFY_USER:"+ lastModifyUser);
        System.out.println( "LAST_MODIFY_DATE:"+ lastModifyDate);
        System.out.println( "LAST_MODIFY_IP:"+ lastModifyIp);
        System.out.println( "LAST_MODIFY_APP:"+ lastModifyApp);
        return "----------------------------------------------------";
    }
    
}
