package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

public enum InterestType {
	

	FIXED(Integer.valueOf(148),"FIJO","1",Integer.valueOf(1)), 
	VARIABLE(Integer.valueOf(534),"VARIABLE","2",Integer.valueOf(2)),
	MIXED(Integer.valueOf(535),"MIXTO","3",Integer.valueOf(3)),
	CERO(Integer.valueOf(427),"CERO","4",null);
	
	public final static List<InterestType> list=new ArrayList<InterestType>();
	public static final Map<Integer, InterestType> lookup = new HashMap<Integer, InterestType>();

	private Integer code;
	private String value;
	private String codeCd;
	private Integer interfaceCode;
	
	static{
		for(InterestType d : InterestType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	/**
	 * Instantiates a new instrument type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 */
	private InterestType(Integer code, String value,String codeCd, Integer interfaceCode){
		this.code=code;
		this.value=value;
		this.codeCd=codeCd;
		this.interfaceCode=interfaceCode;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the instrument type
	 */
	public static InterestType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the code cd.
	 *
	 * @return the code cd
	 */
	public String getCodeCd() {
		return codeCd;
	}

	public Integer getInterfaceCode() {
		return interfaceCode;
	}

	public void setInterfaceCode(Integer interfaceCode) {
		this.interfaceCode = interfaceCode;
	}

	
}
