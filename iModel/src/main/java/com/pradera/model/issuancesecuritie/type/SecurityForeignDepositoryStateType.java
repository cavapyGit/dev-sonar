package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SecurityForeignDepositoryStateType {

	REGISTERED(Integer.valueOf(637),"REGISTRADO"),
	BLOCKED(Integer.valueOf(638),"BLOQUEADO");
	
	public static final Map<Integer,SecurityForeignDepositoryStateType> lookup=new HashMap<Integer, SecurityForeignDepositoryStateType>();
	public static final List<SecurityForeignDepositoryStateType> list=new ArrayList<SecurityForeignDepositoryStateType>();

	static{
		for(SecurityForeignDepositoryStateType s:SecurityForeignDepositoryStateType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	private Integer code;
	private String value;
	

	private SecurityForeignDepositoryStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	

	public static SecurityForeignDepositoryStateType get(Integer code) {
		return lookup.get(code);
	}
	

	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	

	public String getValue() {
		return value;
	}
	
	
	public void setValue(String value) {
		this.value = value;
	}	
	
}
