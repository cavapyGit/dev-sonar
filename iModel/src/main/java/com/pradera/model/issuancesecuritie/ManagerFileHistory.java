package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ISSUER_FILE_HISTORY database table.
 * 
 */
@Entity
@Table(name="MANAGER_FILE_HISTORY")
public class ManagerFileHistory implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id issuer file his pk. */
	@Id
	@SequenceGenerator(name="MANAGER_FILE_HISTORY_IDMANAGERFILEHISPK_GENERATOR", sequenceName="SQ_ID_MANAGER_FILE_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MANAGER_FILE_HISTORY_IDMANAGERFILEHISPK_GENERATOR")
	@Column(name="ID_MANAGER_FILE_HIS_PK")
	private Long idManagerFileHisPk;

	/** The description. */
	private String description;

	/** The document file. */
	@Lob
	@Column(name="DOCUMENT_FILE")
	private byte[] documentFile;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The filename. */
	private String filename;
	
	/** The issuer. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITIES_MANAGER_FK")
	private SecuritiesManager securitiesManager;

	/** The issuer request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SEC_MANAGER_REQUEST_FK", referencedColumnName="ID_SEC_MANAGER_REQUEST_PK")
	private SecManagerRequest secManagerRequest;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The request file type. */
	@Column(name="REQUEST_FILE_TYPE")
	private Integer requestFileType;

	/** The state file. */
	@Column(name="STATE_FILE")
	private Integer stateFile;
	
	/** The document type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;
	
//	/** The streamed content file. */
//	@Transient
//	private StreamedContent streamedContentFile;

	/**
	 * The Constructor.
	 */
	public ManagerFileHistory() {
	}
	

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * Sets the description.
	 *
	 * @param description the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * Gets the document file.
	 *
	 * @return the document file
	 */
	public byte[] getDocumentFile() {
		return documentFile;
	}



	/**
	 * Sets the document file.
	 *
	 * @param documentFile the document file
	 */
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}



	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}



	/**
	 * Sets the document type.
	 *
	 * @param documentType the document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}



	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}



	/**
	 * Sets the filename.
	 *
	 * @param filename the filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	

	public SecuritiesManager getSecuritiesManager() {
		return securitiesManager;
	}


	public void setSecuritiesManager(SecuritiesManager securitiesManager) {
		this.securitiesManager = securitiesManager;
	}


	public Long getIdManagerFileHisPk() {
		return idManagerFileHisPk;
	}


	public void setIdManagerFileHisPk(Long idManagerFileHisPk) {
		this.idManagerFileHisPk = idManagerFileHisPk;
	}


	public SecManagerRequest getSecManagerRequest() {
		return secManagerRequest;
	}


	public void setSecManagerRequest(SecManagerRequest secManagerRequest) {
		this.secManagerRequest = secManagerRequest;
	}


	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}



	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}



	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}



	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}



	/**
	 * Gets the request file type.
	 *
	 * @return the request file type
	 */
	public Integer getRequestFileType() {
		return requestFileType;
	}



	/**
	 * Sets the request file type.
	 *
	 * @param requestFileType the request file type
	 */
	public void setRequestFileType(Integer requestFileType) {
		this.requestFileType = requestFileType;
	}



	/**
	 * Gets the state file.
	 *
	 * @return the state file
	 */
	public Integer getStateFile() {
		return stateFile;
	}



	/**
	 * Sets the state file.
	 *
	 * @param stateFile the state file
	 */
	public void setStateFile(Integer stateFile) {
		this.stateFile = stateFile;
	}			
	
	

	public Integer getRegistryType() {
		return registryType;
	}

	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	/* (non-Javadoc)
	 * @see com.arkin.commons.audit.Auditable#setAudit(com.arkin.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	
	/* (non-Javadoc)
	 * @see com.arkin.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        return null;
	}

}