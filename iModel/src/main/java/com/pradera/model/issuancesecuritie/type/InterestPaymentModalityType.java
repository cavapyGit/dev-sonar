package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum AmortizationPaymentModalityType.
 */
//INTEREST_PAYMENT_MODALITY(Integer.valueOf(404)
public enum InterestPaymentModalityType {
	
	/** The proporcional. */
	BY_COUPON(Integer.valueOf(1526),"POR CUPON","1"), 
	
	/** The no proporcional. */
	AT_MATURITY(Integer.valueOf(1527),"AL VENCIMIENTO","2");
	

	/** The Constant list. */
	public final static List<InterestPaymentModalityType> list=new ArrayList<InterestPaymentModalityType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, InterestPaymentModalityType> lookup = new HashMap<Integer, InterestPaymentModalityType>();
	

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The code cd. */
	private String codeCd;
	
	static{
		for(InterestPaymentModalityType d : InterestPaymentModalityType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	

	/**
	 * Instantiates a new amortization factor type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 */
	private InterestPaymentModalityType(Integer code, String value,String codeCd){
		this.code=code;
		this.value=value;
		this.codeCd=codeCd;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the amortization factor type
	 */
	public static InterestPaymentModalityType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}


	/**
	 * Gets the code cd.
	 *
	 * @return the code cd
	 */
	public String getCodeCd() {
		return codeCd;
	}

	
}
