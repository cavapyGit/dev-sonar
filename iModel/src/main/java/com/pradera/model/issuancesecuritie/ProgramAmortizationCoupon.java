package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;


/**
 * The persistent class for the PROGRAM_AMORTIZATION_COUPON database table.
 * 
 */
@Entity
@Table(name="PROGRAM_AMORTIZATION_COUPON")
public class ProgramAmortizationCoupon implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="PROGRAM_AMORTIZATION_COUPON_IDPROGRAMAMORTIZATIONPK_GENERATOR", sequenceName="SQ_ID_PROG_AMO_COUPON_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROGRAM_AMORTIZATION_COUPON_IDPROGRAMAMORTIZATIONPK_GENERATOR")
	@Column(name="ID_PROGRAM_AMORTIZATION_PK")
	private Long idProgramAmortizationPk;

	@Column(name="AMORTIZATION_FACTOR")
	private BigDecimal amortizationFactor;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CORPORATIVE_DATE")
	private Date corporativeDate;

	@Column(name="COUPON_NUMBER")
	private Integer couponNumber;

	private Integer currency;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CUTOFF_DATE")
	private Date cutoffDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPRITATION_DATE")
	private Date expirationDate;

	@Column(name="IND_ROUNDING")
	private Integer indRounding;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="STATE_PROGRAM_AMORTIZATON")
	private Integer stateProgramAmortization;
	
	@Column(name="SITUATION_PROGRAM_AMORTIZATION")
	private Integer situationProgramAmortization;

	//bi-directional many-to-one association to AmortizationPaymentSchedule
    @ManyToOne
	@JoinColumn(name="ID_AMO_PAYMENT_SCHEDULE_FK")
    @NotNull
	private AmortizationPaymentSchedule amortizationPaymentSchedule;

    @Column(name="AMORTIZATION_AMOUNT")
    private BigDecimal amortizationAmount;

    @Column(name="PAYED_AMOUNT")
    private BigDecimal payedAmount;

	@Column(name="COUPON_AMOUNT")
	private BigDecimal couponAmount;
    
	/** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="EXCHANGE_DATE_RATE")
	private Date exchangeDateRate;
    
	/** The market rate. */
	@Column(name="CURRENCY_PAYMENT")
	private Integer currencyPayment;
	
    @Transient
    private BigDecimal nominalValue;
    
    @Transient
    private Boolean ultimateAmortization;

//    @OneToMany(mappedBy = "programAmortizationCoupon", fetch=FetchType.LAZY)
//    private List<CorporativeOperation> corporativeOperations;
	@OneToOne(mappedBy="programAmortizationCoupon",cascade=CascadeType.DETACH,fetch=FetchType.LAZY)
    private CorporativeOperation corporativeOperation; 
	
	@Transient
	private String dataRowDtbAmortizationPaymentSchedule;
	
	@Transient
	private boolean selected;
	
	@Transient
	private boolean lastPayment;
	
	@Transient
	private int paymentAmortizationDays;
    
    public ProgramAmortizationCoupon() {
    }
    
    public int getPaymentAmortizationDays() {
		return paymentAmortizationDays;
	}

    public void setPaymentAmortizationDays(int paymentAmortizationDays) {
		this.paymentAmortizationDays = paymentAmortizationDays;
	}

	public Long getIdProgramAmortizationPk() {
		return this.idProgramAmortizationPk;
	}

	public void setIdProgramAmortizationPk(Long idProgramAmortizationPk) {
		this.idProgramAmortizationPk = idProgramAmortizationPk;
	}

	public BigDecimal getAmortizationFactor() {
		return this.amortizationFactor;
	}

	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}

	public Date getBeginingDate() {
		return this.beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public Date getCorporativeDate() {
		return this.corporativeDate;
	}

	public void setCorporativeDate(Date corporativeDate) {
		this.corporativeDate = corporativeDate;
	}

	public Integer getCouponNumber() {
		return this.couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Integer getCurrency() {
		return this.currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Date getCutoffDate() {
		return this.cutoffDate;
	}

	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getIndRounding() {
		return this.indRounding;
	}

	public void setIndRounding(Integer indRounding) {
		this.indRounding = indRounding;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getStateProgramAmortization() {
		return this.stateProgramAmortization;
	}

	public void setStateProgramAmortization(Integer stateProgramAmortization) {
		this.stateProgramAmortization = stateProgramAmortization;
	}

	

	public AmortizationPaymentSchedule getAmortizationPaymentSchedule() {
		return amortizationPaymentSchedule;
	}

	public void setAmortizationPaymentSchedule(
			AmortizationPaymentSchedule amortizationPaymentSchedule) {
		this.amortizationPaymentSchedule = amortizationPaymentSchedule;
	}

	public BigDecimal getAmortizationAmount() {
		return amortizationAmount;
	}

	public void setAmortizationAmount(BigDecimal amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	public BigDecimal getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(BigDecimal couponAmount) {
		this.couponAmount = couponAmount;
	}

	public String getStateDescription(){
		if(stateProgramAmortization!=null){
			return ProgramScheduleStateType.get(stateProgramAmortization).getValue();
		}
		return "";
	}
	
	/* State indicators start*/
	
	public boolean isPendingState(){
		if(ProgramScheduleStateType.PENDING.getCode().equals( this.getStateProgramAmortization() ))
			return true;
		return false;
	}
	
	public boolean isPaidState(){
		if(ProgramScheduleStateType.EXPIRED.getCode().equals( this.getStateProgramAmortization() )){
			return true;
		}
		return false;
	}
	
	/* State indicators end*/
	
//	public List<CorporativeOperation> getCorporativeOperations() {
//		return corporativeOperations;
//	}
//
//	public void setCorporativeOperations(
//			List<CorporativeOperation> corporativeOperations) {
//		this.corporativeOperations = corporativeOperations;
//	}

	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	public void setCorporativeOperation(CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal getPayedAmount() {
		return payedAmount;
	}

	public void setPayedAmount(BigDecimal payedAmount) {
		this.payedAmount = payedAmount;
	}
	
	

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getDataRowDtbAmortizationPaymentSchedule() {
		dataRowDtbAmortizationPaymentSchedule= dataRowDtbAmortizationPaymentSchedule= "ProgramAmortizationCoupon [idProgramAmortizationPk="
				+ idProgramAmortizationPk + ", beginingDate=" + beginingDate
				+ ", couponNumber=" + couponNumber + ", expirationDate="
				+ expirationDate + ", paymentDate=" + paymentDate
				+ ", stateProgramAmortization=" + stateProgramAmortization
				+ ", registryDate=" + registryDate
				+ ", amortizationFactor=" + amortizationFactor
				+ ", payedAmount=" + payedAmount + "]";
		return dataRowDtbAmortizationPaymentSchedule;
	}
	
	public void setDataRowDtbAmortizationPaymentSchedule(String dataRowDtbAmortizationPaymentSchedule){
		this.dataRowDtbAmortizationPaymentSchedule=dataRowDtbAmortizationPaymentSchedule;
	}
	
	public boolean isFirstCoupon(){
		if(Integer.valueOf(1).compareTo(getCouponNumber())==0){
			return true;
		}
		return false;
	}
	
	
	
	public boolean isLastPayment() {
		return lastPayment;
	}

	public void setLastPayment(boolean lastPayment) {
		this.lastPayment = lastPayment;
	}

	public void setDataFromProgramAmoCouponReqHi(ProgramAmoCouponReqHi programAmoCouponReqHi, LoggerUser loggerUser){
		this.setAudit(loggerUser);
		
		this.setCouponNumber( programAmoCouponReqHi.getCouponNumber() );
		this.setCurrency( programAmoCouponReqHi.getCurrency() );
		this.setAmortizationFactor(programAmoCouponReqHi.getAmortizationFactor());
		this.setPaymentDate(programAmoCouponReqHi.getPaymentDate());
		this.setRegistryDate( programAmoCouponReqHi.getRegistryDate() );
		this.setCutoffDate( programAmoCouponReqHi.getCutoffDate() );
		
		this.setCorporativeDate( programAmoCouponReqHi.getCorporativeDate() );
		this.setBeginingDate( programAmoCouponReqHi.getBeginingDate() );
		this.setExpirationDate( programAmoCouponReqHi.getExpirationDate()  );
		this.setIndRounding( programAmoCouponReqHi.getIndRounding() );
		this.setRegisterDate( programAmoCouponReqHi.getRegisterDate() );
		this.setRegistryUser( programAmoCouponReqHi.getRegistryUser() );
		this.setStateProgramAmortization( programAmoCouponReqHi.getStateProgramAmortizaton() );
		this.setSituationProgramAmortization(ProgramScheduleSituationType.PENDING.getCode());
		this.setAmortizationAmount( programAmoCouponReqHi.getAmortizationAmount() );
		this.setPayedAmount( programAmoCouponReqHi.getPayedAmount() );
		this.setCouponAmount(programAmoCouponReqHi.getCouponAmount());
		
		this.setExchangeDateRate(programAmoCouponReqHi.getExchangeDateRate());
		this.setCurrencyPayment(programAmoCouponReqHi.getCurrencyPayment());
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public Integer getSituationProgramAmortization() {
		return situationProgramAmortization;
	}

	public void setSituationProgramAmortization(Integer situationProgramAmortization) {
		this.situationProgramAmortization = situationProgramAmortization;
	}

	public Boolean getUltimateAmortization() {
		return ultimateAmortization;
	}

	public void setUltimateAmortization(Boolean ultimateAmortization) {
		this.ultimateAmortization = ultimateAmortization;
	}

	public Date getExchangeDateRate() {
		return exchangeDateRate;
	}

	public void setExchangeDateRate(Date exchangeDateRate) {
		this.exchangeDateRate = exchangeDateRate;
	}

	public Integer getCurrencyPayment() {
		return currencyPayment;
	}

	public void setCurrencyPayment(Integer currencyPayment) {
		this.currencyPayment = currencyPayment;
	}

	
	
}