package com.pradera.model.issuancesecuritie;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the PROGRAM_AMO_COUPON_REQ_HIS database table.
 * 
 */
@Entity
@Table(name="PROGRAM_AMO_COUPON_REQ_HIS")
public class ProgramAmoCouponReqHi implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PROGRAM_AMO_COUPON_REQ_HIS_IDPROGAMOCOUPONREQHISPK_GENERATOR", sequenceName="SQ_ID_AMO_SCHEDULE_REQ_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROGRAM_AMO_COUPON_REQ_HIS_IDPROGAMOCOUPONREQHISPK_GENERATOR")
	@Column(name="ID_PROG_AMO_COUPON_REQ_HIS_PK")
	private long idProgAmoCouponReqHisPk;

	@Column(name="AMORTIZATION_AMOUNT")
	private BigDecimal amortizationAmount;

	@Column(name="AMORTIZATION_FACTOR")
	private BigDecimal amortizationFactor;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CORPORATIVE_DATE")
	private Date corporativeDate;

	@Column(name="COUPON_NUMBER")
	private Integer couponNumber;

	private Integer currency;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CUTOFF_DATE")
	private Date cutoffDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPRITATION_DATE")
	private Date expirationDate;

	@Column(name="IND_ROUNDING")
	private Integer indRounding;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTER_DATE")
	private Date registerDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="STATE_PROGRAM_AMORTIZATON")
	private Integer stateProgramAmortizaton;
	
	@Column(name="PAYED_AMOUNT")
    private BigDecimal payedAmount;

	@Column(name="COUPON_AMOUNT")
	private BigDecimal couponAmount;

	/** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="EXCHANGE_DATE_RATE")
	private Date exchangeDateRate;
    
	/** The market rate. */
	@Column(name="CURRENCY_PAYMENT")
	private Integer currencyPayment;
	
	//bi-directional many-to-one association to AmoPaymentScheduleReqHi
    @ManyToOne
	@JoinColumn(name="ID_AMO_SCHEDULE_REQ_HIS_FK")
	private AmoPaymentScheduleReqHi amoPaymentScheduleReqHi;
    
	@Transient
	private boolean selected;
	@Transient
	private boolean disabled;
	@Transient
	private Date paymentDateMin;
	@Transient
	private Date paymentDateMax;
	@Transient
	private Date minBeginDate;
	@Transient
	private Date maxBeginDate;
	@Transient
	private Date minExpirationDate;
	@Transient
	private Date maxExpirationDate;
	
    public ProgramAmoCouponReqHi() {
    }

	public long getIdProgAmoCouponReqHisPk() {
		return this.idProgAmoCouponReqHisPk;
	}

	public void setIdProgAmoCouponReqHisPk(long idProgAmoCouponReqHisPk) {
		this.idProgAmoCouponReqHisPk = idProgAmoCouponReqHisPk;
	}

	public BigDecimal getAmortizationAmount() {
		return this.amortizationAmount;
	}

	public void setAmortizationAmount(BigDecimal amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	public BigDecimal getAmortizationFactor() {
		return this.amortizationFactor;
	}

	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}

	public Date getBeginingDate() {
		return this.beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public Date getCorporativeDate() {
		return this.corporativeDate;
	}

	public void setCorporativeDate(Date corporativeDate) {
		this.corporativeDate = corporativeDate;
	}

	public Integer getCouponNumber() {
		return this.couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Integer getCurrency() {
		return this.currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Date getCutoffDate() {
		return this.cutoffDate;
	}

	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}

	

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getIndRounding() {
		return this.indRounding;
	}

	public void setIndRounding(Integer indRounding) {
		this.indRounding = indRounding;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Integer getStateProgramAmortizaton() {
		return this.stateProgramAmortizaton;
	}

	public void setStateProgramAmortizaton(Integer stateProgramAmortizaton) {
		this.stateProgramAmortizaton = stateProgramAmortizaton;
	}

	public AmoPaymentScheduleReqHi getAmoPaymentScheduleReqHi() {
		return amoPaymentScheduleReqHi;
	}

	public void setAmoPaymentScheduleReqHi(
			AmoPaymentScheduleReqHi amoPaymentScheduleReqHi) {
		this.amoPaymentScheduleReqHi = amoPaymentScheduleReqHi;
	}

	public BigDecimal getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(BigDecimal couponAmount) {
		this.couponAmount = couponAmount;
	}

	public String getStateDescription(){
		if(stateProgramAmortizaton!=null){
			return ProgramScheduleStateType.get(stateProgramAmortizaton).getValue();
		}
		return "";
	}
	 
	public boolean isPendingState(){
		if(stateProgramAmortizaton!=null){
			if(stateProgramAmortizaton.equals( ProgramScheduleStateType.PENDING.getCode() )){
				return Boolean.TRUE;
			}
		}
		return false;
	}
	
	public boolean isPaidState(){
		if(stateProgramAmortizaton!=null){
			if(stateProgramAmortizaton.equals( ProgramScheduleStateType.EXPIRED.getCode() )){
				return Boolean.TRUE;
			}
		}
		return false;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Date getPaymentDateMin() {
		return paymentDateMin;
	}

	public void setPaymentDateMin(Date paymentDateMin) {
		this.paymentDateMin = paymentDateMin;
	}

	public Date getPaymentDateMax() {
		return paymentDateMax;
	}

	public void setPaymentDateMax(Date paymentDateMax) {
		this.paymentDateMax = paymentDateMax;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public Date getMinBeginDate() {
		return minBeginDate;
	}

	public void setMinBeginDate(Date minBeginDate) {
		this.minBeginDate = minBeginDate;
	}

	public Date getMaxBeginDate() {
		return maxBeginDate;
	}

	public void setMaxBeginDate(Date maxBeginDate) {
		this.maxBeginDate = maxBeginDate;
	}

	public Date getMinExpirationDate() {
		return minExpirationDate;
	}

	public void setMinExpirationDate(Date minExpirationDate) {
		this.minExpirationDate = minExpirationDate;
	}

	public Date getMaxExpirationDate() {
		return maxExpirationDate;
	}

	public void setMaxExpirationDate(Date maxExpirationDate) {
		this.maxExpirationDate = maxExpirationDate;
	}

	public BigDecimal getPayedAmount() {
		return payedAmount;
	}

	public void setPayedAmount(BigDecimal payedAmount) {
		this.payedAmount = payedAmount;
	}

	public Date getExchangeDateRate() {
		return exchangeDateRate;
	}

	public void setExchangeDateRate(Date exchangeDateRate) {
		this.exchangeDateRate = exchangeDateRate;
	}

	public Integer getCurrencyPayment() {
		return currencyPayment;
	}

	public void setCurrencyPayment(Integer currencyPayment) {
		this.currencyPayment = currencyPayment;
	}

	public void setDataFromProgramAmortizationCoupon(ProgramAmortizationCoupon programAmortizationCoupon, LoggerUser loggerUser){
		this.setAudit(loggerUser);
		
		this.setCouponNumber( programAmortizationCoupon.getCouponNumber() );
		this.setCurrency( programAmortizationCoupon.getCurrency() );
		this.setAmortizationFactor( programAmortizationCoupon.getAmortizationFactor() );
		
		this.setPaymentDate( programAmortizationCoupon.getPaymentDate() );
		this.setRegistryDate( programAmortizationCoupon.getRegistryDate() );
		this.setCutoffDate( programAmortizationCoupon.getCutoffDate() );
		this.setCorporativeDate( programAmortizationCoupon.getCorporativeDate() );
		this.setBeginingDate( programAmortizationCoupon.getBeginingDate() );
		this.setExpirationDate( programAmortizationCoupon.getExpirationDate() );
		this.setIndRounding( programAmortizationCoupon.getIndRounding() );
		
		this.setRegisterDate( loggerUser.getAuditTime() );
		this.setRegistryUser( loggerUser.getUserName() );
		this.setAmortizationAmount( programAmortizationCoupon.getAmortizationAmount() );
		this.setStateProgramAmortizaton(programAmortizationCoupon.getStateProgramAmortization());  
		this.setPayedAmount( programAmortizationCoupon.getPayedAmount() );
		this.setCouponAmount(programAmortizationCoupon.getCouponAmount());
		
		this.setExchangeDateRate(programAmortizationCoupon.getExchangeDateRate());
		this.setCurrencyPayment(programAmortizationCoupon.getCurrencyPayment());
	}
	
}