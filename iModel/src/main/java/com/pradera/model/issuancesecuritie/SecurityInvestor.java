package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SECURITY_INVERSIONIST database table.
 * 
 */
@Entity
@Table(name="SECURITY_INVESTOR")
@NamedQueries({
//	@NamedQuery(name=SecurityInvestor.SEARCH_DESCRIPTION_BY_SECURITY_AND_STATE,query="Select si.securityInversionistPk, si.inversionistType, pt.description from SecurityInversionist si, ParameterTable pt where si.inversionistType=pt.parameterTablePk and si.security.idIsinCodePk=:idIsinCodePrm and si.securityInversionistState=:secInvStatePrm order by pt.description"),
//	@NamedQuery(name=SecurityInvestor.SEARCH_BY_SECURITY_AND_STATE,query="Select si from SecurityInversionist si where si.security.idIsinCodePk=:idIsinCodePrm and si.securityInversionistState=:secInvStatePrm")
	@NamedQuery(name=SecurityInvestor.SEARCH_BY_SECURITY,query="Select si from SecurityInvestor si where si.security.idSecurityCodePk=:idSecurityCodePrm")
})
public class SecurityInvestor implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
//	public static final String SEARCH_DESCRIPTION_BY_SECURITY_AND_STATE="SecurityInversionist.SearchDescriptionBySecurityAndState"; 
//	public static final String SEARCH_BY_SECURITY_AND_STATE="SecurityInversionist.SearchBySecurityAndState"; 
	public static final String SEARCH_BY_SECURITY="SecurityInversionist.SearchBySecurity";
	
	/** The security inversionist pk. */
	@Id
	@SequenceGenerator(name="SECURITY_INVESTOR_IDSECURITYINVESTORPK_GENERATOR", sequenceName="SQ_ID_SECURITY_INVESTOR_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_INVESTOR_IDSECURITYINVESTORPK_GENERATOR")
	@Column(name="ID_SECURITY_INVESTOR_PK")
	private Long idSecurityInvestorPk;
	
	//bi-directional many-to-one association to Security
    /** The security. */
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	@Column(name="IND_NATURAL")
	private Integer indNatural;
	
	@Column(name="IND_JURIDICAL")
	private Integer indJuridical;
	
	@Column(name="IND_FCI")
	private Integer indCfi;
	
	@Column(name="IND_LOCAL")
	private Integer indLocal;
	
	
	@Column(name="IND_FOREIGN")
	private Integer indForeign;
	
	/** The security investor state. */
	@Column(name="SECURITY_INVESTOR_STATE")
	private Integer securityInvestorState;
	
	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
    
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Transient
	private boolean boolIndNatural;
	
	@Transient
	private boolean boolIndJuridical;
	
	@Transient
	private boolean boolIndLocal;
	
	@Transient
	private boolean boolIndForeign;
	
	
    /**
     * Instantiates a new security inversionist.
     */
    public SecurityInvestor() {
    	indCfi = ComponentConstant.ZERO;
    }

    

	public Long getIdSecurityInvestorPk() {
		return idSecurityInvestorPk;
	}



	public void setIdSecurityInvestorPk(Long idSecurityInvestorPk) {
		this.idSecurityInvestorPk = idSecurityInvestorPk;
	}



	public Security getSecurity() {
		return security;
	}



	public void setSecurity(Security security) {
		this.security = security;
	}



	public Integer getIndNatural() {
		return indNatural;
	}



	public void setIndNatural(Integer indNatural) {
		this.indNatural = indNatural;
	}



	public Integer getIndJuridical() {
		return indJuridical;
	}



	public void setIndJuridical(Integer indJuridical) {
		this.indJuridical = indJuridical;
	}



	public Integer getIndLocal() {
		return indLocal;
	}



	public void setIndLocal(Integer indLocal) {
		this.indLocal = indLocal;
	}



	public Integer getIndForeign() {
		return indForeign;
	}



	public void setIndForeign(Integer indForeign) {
		this.indForeign = indForeign;
	}



	public Integer getSecurityInvestorState() {
		return securityInvestorState;
	}



	public void setSecurityInvestorState(Integer securityInvestorState) {
		this.securityInvestorState = securityInvestorState;
	}



	public String getLastModifyUser() {
		return lastModifyUser;
	}



	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	public String getLastModifyIp() {
		return lastModifyIp;
	}



	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	

	public boolean isBoolIndNatural() {
		if(BooleanType.YES.getCode().equals( indNatural )){
			boolIndNatural=true;
		}
		return boolIndNatural;
	}



	public void setBoolIndNatural(boolean boolIndNatural) {
		if(boolIndNatural){
			indNatural=BooleanType.YES.getCode();
		}else{
			indNatural=BooleanType.NO.getCode();
		}
		this.boolIndNatural = boolIndNatural;
	}



	public boolean isBoolIndJuridical() {
		if(BooleanType.YES.getCode().equals( indJuridical )){
			boolIndJuridical=true;
		}
		return boolIndJuridical;
	}



	public void setBoolIndJuridical(boolean boolIndJuridical) {
		if(boolIndJuridical){
			indJuridical=BooleanType.YES.getCode();
		}else{
			indJuridical=BooleanType.NO.getCode();
		}
		this.boolIndJuridical = boolIndJuridical;
	}



	public boolean isBoolIndLocal() {
		if(BooleanType.YES.getCode().equals( indLocal )){
			boolIndLocal=true;
		}
		return boolIndLocal;
	}



	public void setBoolIndLocal(boolean boolIndLocal) {
		if(boolIndLocal){
			indLocal=BooleanType.YES.getCode();
		}else{
			indLocal=BooleanType.NO.getCode();
		}
		this.boolIndLocal = boolIndLocal;
	}



	public boolean isBoolIndForeign() {
		if(BooleanType.YES.getCode().equals( indForeign )){
			boolIndForeign=true;
		}
		return boolIndForeign;
	}



	public void setBoolIndForeign(boolean boolIndForeign) {
		if(boolIndForeign){
			indForeign=BooleanType.YES.getCode();
		}else{
			indForeign=BooleanType.NO.getCode();
		}
		this.boolIndForeign = boolIndForeign;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getIndCfi() {
		return indCfi;
	}

	public void setIndCfi(Integer indCfi) {
		this.indCfi = indCfi;
	}
}