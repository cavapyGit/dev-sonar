package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InterestClassType {
	
	EFFECTIVE(Integer.valueOf(567),"EFECTIVO"),
	NOMINAL(Integer.valueOf(568),"NOMINAL");
	
	public final static List<InterestClassType> list=new ArrayList<InterestClassType>();
	public static final Map<Integer, InterestClassType> lookup = new HashMap<Integer, InterestClassType>();
	
	private Integer code;
	private String value;
	
	static{
		for(InterestClassType d : InterestClassType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	private InterestClassType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static InterestClassType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
