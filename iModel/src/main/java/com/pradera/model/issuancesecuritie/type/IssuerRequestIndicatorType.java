package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum IssuerRequestType.
 * This type was create to filter the Issuer request type (block/unblock or modification).
 * The code is comparated with the column indicator_1 from parameter_table
 */
public enum IssuerRequestIndicatorType {
	
	/** The block unblock request type. */
	BLOCK_UNBLOCK_REQUEST_TYPE("1"),
	
	/** The modification request type. */
	MODIFICATION_REQUEST_TYPE("2");


	/** The Constant list. */
	public final static List<IssuerRequestIndicatorType> list=new ArrayList<IssuerRequestIndicatorType>();
	
	/** The Constant lookup. */
	public final static Map<String, IssuerRequestIndicatorType> lookup=new HashMap<String, IssuerRequestIndicatorType>();
	
	static {
		for(IssuerRequestIndicatorType c : IssuerRequestIndicatorType.values()){
			lookup.put(c.getCode(), c);
			list.add( c );
		}
	}
	
	/** The code. */
	private String code;
	
	/**
	 * Instantiates a new issuer request type.
	 *
	 * @param code the code
	 */
	private IssuerRequestIndicatorType(String code){
		this.code=code;
	}
	
	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}



	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}



	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the master table type
	 */
	public static IssuerRequestIndicatorType get(String code) {
        return lookup.get(code);
    }

	
}