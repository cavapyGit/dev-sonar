package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the SECURITY_CODE_FORMAT database table.
 * 
 */
@Entity
@Table(name="SECURITY_CODE_FORMAT")
public class SecurityCodeFormat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_SECURITY_CODE_FORMAT_PK")
	@NotNull
	private BigDecimal idSecurityCodeFormatPk;

	@Column(name="SECURITY_CLASS")
	private Integer securityClass;

	@Column(name="INSTRUMENT_TYPE")
	private Integer instrumentType;

	@Column(name="POSITION_MNEMONIC")
	private String positionMnemonic;

	@Column(name="POSITION_CURRENCY")
	private String positionCurrency;

	@Column(name="POSITION_ISSUE_CHECK_DIGIT")
	private Integer positionIssueCheckDigit;

	@Column(name="POSITION_NUMERICAL_DATA")
	private String positionNumericalData;

	@Column(name="POSITION_SECURITY_TERM")
	private String positionSecurityTerm;

	@Column(name="POSITION_ISSUANCE_YEAR")
	private String positionIssuanceYear;
	
	@Column(name="POSITION_WEEK")
	private String positionWeek;

	@Column(name="IND_DASH")
	private Integer indDash;

	@Column(name="IND_ALPHANUMERIC")
	private Integer indAlphanumeric;

	@Column(name="MIN_LENGTH")
	private Integer minLegth;

	@Column(name="MAX_LENGTH")
	private Integer maxLength;

	public BigDecimal getIdSecurityCodeFormatPk() {
		return idSecurityCodeFormatPk;
	}

	public void setIdSecurityCodeFormatPk(BigDecimal idSecurityCodeFormatPk) {
		this.idSecurityCodeFormatPk = idSecurityCodeFormatPk;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public String getPositionMnemonic() {
		return positionMnemonic;
	}

	public void setPositionMnemonic(String positionMnemonic) {
		this.positionMnemonic = positionMnemonic;
	}

	public String getPositionCurrency() {
		return positionCurrency;
	}

	public void setPositionCurrency(String positionCurrency) {
		this.positionCurrency = positionCurrency;
	}

	public Integer getPositionIssueCheckDigit() {
		return positionIssueCheckDigit;
	}

	public void setPositionIssueCheckDigit(Integer positionIssueCheckDigit) {
		this.positionIssueCheckDigit = positionIssueCheckDigit;
	}

	public String getPositionNumericalData() {
		return positionNumericalData;
	}

	public void setPositionNumericalData(String positionNumericalData) {
		this.positionNumericalData = positionNumericalData;
	}

	public String getPositionSecurityTerm() {
		return positionSecurityTerm;
	}

	public void setPositionSecurityTerm(String positionSecurityTerm) {
		this.positionSecurityTerm = positionSecurityTerm;
	}

	public String getPositionIssuanceYear() {
		return positionIssuanceYear;
	}

	public void setPositionIssuanceYear(String positionIssuanceYear) {
		this.positionIssuanceYear = positionIssuanceYear;
	}

	public String getPositionWeek() {
		return positionWeek;
	}

	public void setPositionWeek(String positionWeek) {
		this.positionWeek = positionWeek;
	}

	public Integer getIndDash() {
		return indDash;
	}

	public void setIndDash(Integer indDash) {
		this.indDash = indDash;
	}

	public Integer getIndAlphanumeric() {
		return indAlphanumeric;
	}

	public void setIndAlphanumeric(Integer indAlphanumeric) {
		this.indAlphanumeric = indAlphanumeric;
	}

	public Integer getMinLegth() {
		return minLegth;
	}

	public void setMinLegth(Integer minLegth) {
		this.minLegth = minLegth;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}



}