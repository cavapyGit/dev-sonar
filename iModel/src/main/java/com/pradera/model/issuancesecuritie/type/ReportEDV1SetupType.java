package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum ReportEDV1SetupType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 09-sep-2015
 */
public enum ReportEDV1SetupType {
	
	
	/** The date top. */
	DATE_TOP(Integer.valueOf(2331),"DATE_TOP");
	
	
	/** The Constant list. */
	public final static List<ReportEDV1SetupType> list=new ArrayList<ReportEDV1SetupType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ReportEDV1SetupType> lookup = new HashMap<Integer, ReportEDV1SetupType>();
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	static{
		for(ReportEDV1SetupType d : ReportEDV1SetupType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	/**
	 * Instantiates a new report ed v1 setup type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ReportEDV1SetupType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the report ed v1 setup type
	 */
	public static ReportEDV1SetupType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
