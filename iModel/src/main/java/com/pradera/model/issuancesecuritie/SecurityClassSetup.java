package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@NamedQueries({
	@NamedQuery(name = SecurityClassSetup.FIND_BY_SECTOR_AND_OFFER_TYPE, 
			query = "SELECT s FROM SecurityClassSetup s where s.instrumentType = :instrumentType and " +
					" s.economicSector = :economicSectorPrm and s.offerType=:offerType and s.securityClass=:securityClassPrm")
})
@Table(name="SECURITY_CLASS_SETUP")
public class SecurityClassSetup implements Serializable, Auditable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_SECTOR_AND_OFFER_TYPE="SecurityClassSetup.findBySectorAndOffeType";
	
	@Id
	@Column(name="ID_SECURITY_CLASS_SETUP_PK")
	private Long idSecurityClassSetupPk;
	
	@Column(name="OFFER_TYPE")
	private Integer offerType;
	
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;
	
	@Column(name="SECURITY_CLASS")
	private Integer securityClass;
	
	@Column(name="INSTRUMENT_TYPE")
	private Integer instrumentType;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="LAST_MODIFY_DATE ")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	public Long getIdSecurityClassSetupPk() {
		return idSecurityClassSetupPk;
	}

	public void setIdSecurityClassSetupPk(Long idSecurityClassSetupPk) {
		this.idSecurityClassSetupPk = idSecurityClassSetupPk;
	}

	public Integer getOfferType() {
		return offerType;
	}

	public void setOfferType(Integer offerType) {
		this.offerType = offerType;
	}

	

	public Integer getEconomicSector() {
		return economicSector;
	}

	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
