package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum InstrumentType.
 */
public enum InstrumentType {
	 
	/** The fixed income. */
	FIXED_INCOME(Integer.valueOf(124),"RENTA FIJA","1"), 
	
	/** The variable income. */
	VARIABLE_INCOME(Integer.valueOf(399),"RENTA VARIABLE","2"),
	
	/** The mixed income. */
	MIXED_INCOME(Integer.valueOf(1750),"RENTA MIXTA","3");
	
	
	/** The Constant list. */
	public final static List<InstrumentType> list=new ArrayList<InstrumentType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, InstrumentType> lookup = new HashMap<Integer, InstrumentType>();
	
	/** The code. */	
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The code cd. */
	private String codeCd;
	
	static{
		for(InstrumentType d : InstrumentType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	
	
	/**
	 * Instantiates a new instrument type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 */
	private InstrumentType(Integer code, String value,String codeCd){
		this.code=code;
		this.value=value;
		this.codeCd=codeCd;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the instrument type
	 */
	public static InstrumentType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the code cd.
	 *
	 * @return the code cd
	 */
	public String getCodeCd() {
		return codeCd;
	}

	
}
