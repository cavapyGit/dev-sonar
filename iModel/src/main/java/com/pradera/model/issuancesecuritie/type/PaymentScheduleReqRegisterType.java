package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Master Id 370
public enum PaymentScheduleReqRegisterType {
	
	REQUEST_MODIFICATION(Integer.valueOf(1353),"PETICION MODIFICACION"), 
	REQUEST_RESTRUCTURING(Integer.valueOf(1354),"PETICION RESTRUCTURACION"),
	HISTORY(Integer.valueOf(1355),"HISTORIA");
	
	public final static List<PaymentScheduleReqRegisterType> list=new ArrayList<PaymentScheduleReqRegisterType>();
	public static final Map<Integer, PaymentScheduleReqRegisterType> lookup = new HashMap<Integer, PaymentScheduleReqRegisterType>();
	
	private Integer code;
	private String value;
	
	static{
		for(PaymentScheduleReqRegisterType d : PaymentScheduleReqRegisterType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
		}
	}	

	private PaymentScheduleReqRegisterType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	public static PaymentScheduleReqRegisterType get(Integer code) {
		return lookup.get(code);
	}
	
	public Integer getCode() {
		return code;
	}
	

	public void setCode(Integer code) {
		this.code = code;
	}
	

	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

}
