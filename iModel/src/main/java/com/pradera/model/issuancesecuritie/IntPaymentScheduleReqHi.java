package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.CalendarType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleReqHiRejectType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the INT_PAYMENT_SCHEDULE_REQ_HIS database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/04/2014
 */
@Entity
@Table(name="INT_PAYMENT_SCHEDULE_REQ_HIS")
public class IntPaymentScheduleReqHi implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id int schedule req his pk. */
	@Id
	@SequenceGenerator(name="INT_PAYMENT_SCHEDULE_REQ_HIS_IDINTSCHEDULEREQHISPK_GENERATOR", sequenceName="SQ_ID_INT_SCHEDULE_REQ_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INT_PAYMENT_SCHEDULE_REQ_HIS_IDINTSCHEDULEREQHISPK_GENERATOR")
	@Column(name="ID_INT_SCHEDULE_REQ_HIS_PK")
	private Long idIntScheduleReqHisPk;
	
	/** The interest payment schedule. */
	@ManyToOne
	@JoinColumn(name="ID_INT_PAYMENT_SCHEDULE_FK")
	private InterestPaymentSchedule interestPaymentSchedule;

	/** The calendar type. */
	@Column(name="CALENDAR_TYPE")
	private Integer calendarType;

	/** The cash nominal. */
	@Column(name="CASH_NOMINAL")
	private Integer cashNominal;

    /** The expiration date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

	/** The financial index. */
	@Column(name="FINANCIAL_INDEX")
	private Integer financialIndex;

    /** The security. */
    @ManyToOne
	@JoinColumn(name="ID_SECURITY_CODE_FK")
    @NotNull
	private Security security;

	/** The interest factor. */
	@Column(name="INTEREST_FACTOR")
	private BigDecimal interestFactor;

	/** The interest periodicity. */
	@Column(name="INTEREST_PERIODICITY")
	private Integer interestPeriodicity;

	/** The interest type. */
	@Column(name="INTEREST_TYPE")
	private Integer interestType;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The periodicity. */
	private Integer periodicity;
	
	/** The periodicity days. */
	@Column(name="PERIODICITY_DAYS")
	private Integer periodicityDays;	

	/** The registry days. */
	@Column(name="REGISTRY_DAYS")
	private Integer registryDays;

	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;

	/** The schedule state. */
	@Column(name="SCHEDULE_STATE")
	private Integer scheduleState;

	/** The spread. */
	private BigDecimal spread;

	/** The tax factor. */
	@Column(name="TAX_FACTOR")
	private BigDecimal taxFactor;

	/** The yield. */
	private Integer yield;
	
	/** The calendar month. */
	@Column(name="CALENDAR_MONTH")
	private Integer calendarMonth;
	
	/** The calendar days. */
	@Column(name="CALENDAR_DAYS")
	private Integer calendarDays;
	
	/** The rate type. */
	@Column(name="RATE_TYPE")
	private Integer rateType;
	
	/** The rate value. */
	@Column(name="RATE_VALUE")
	private BigDecimal rateValue;
	
	/** The operation type. */
	@Column(name="OPERATION_TYPE")
	private Integer operationType;
	
	/** The minimum rate. */
	@Column(name="MINIMUM_RATE")
	private BigDecimal minimumRate;
	
	/** The maximum rate. */
	@Column(name="MAXIMUM_RATE")
	private BigDecimal maximumRate;
	
	/** The reject motive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/** The reject other motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;

	//bi-directional many-to-one association to ProgramIntCouponReqHi
	/** The program int coupon req his. */
	@OneToMany(mappedBy="intPaymentScheduleReqHi",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<ProgramIntCouponReqHi> programIntCouponReqHis;
	
	/** The request state. */
	@Column(name="REQUEST_STATE")
	@NotNull
	private Integer requestState;
	
    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
    
	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registryUser;
	
	/** The is all coupons pending payment. */
	@Transient
	private boolean isAllCouponsPendingPayment;
	
    /**
     * Instantiates a new int payment schedule req hi.
     */
    public IntPaymentScheduleReqHi() {
    	programIntCouponReqHis=new ArrayList<ProgramIntCouponReqHi>();
    }

	/**
	 * Gets the id int schedule req his pk.
	 *
	 * @return the id int schedule req his pk
	 */
	public Long getIdIntScheduleReqHisPk() {
		return this.idIntScheduleReqHisPk;
	}

	/**
	 * Sets the id int schedule req his pk.
	 *
	 * @param idIntScheduleReqHisPk the new id int schedule req his pk
	 */
	public void setIdIntScheduleReqHisPk(Long idIntScheduleReqHisPk) {
		this.idIntScheduleReqHisPk = idIntScheduleReqHisPk;
	}

	/**
	 * Gets the calendar type.
	 *
	 * @return the calendar type
	 */
	public Integer getCalendarType() {
		return this.calendarType;
	}

	/**
	 * Sets the calendar type.
	 *
	 * @param calendarType the new calendar type
	 */
	public void setCalendarType(Integer calendarType) {
		this.calendarType = calendarType;
	}

	/**
	 * Gets the cash nominal.
	 *
	 * @return the cash nominal
	 */
	public Integer getCashNominal() {
		return this.cashNominal;
	}

	/**
	 * Sets the cash nominal.
	 *
	 * @param cashNominal the new cash nominal
	 */
	public void setCashNominal(Integer cashNominal) {
		this.cashNominal = cashNominal;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return this.expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the financial index.
	 *
	 * @return the financial index
	 */
	public Integer getFinancialIndex() {
		return this.financialIndex;
	}

	/**
	 * Sets the financial index.
	 *
	 * @param financialIndex the new financial index
	 */
	public void setFinancialIndex(Integer financialIndex) {
		this.financialIndex = financialIndex;
	}

	

	/**
	 * Gets the interest payment schedule.
	 *
	 * @return the interest payment schedule
	 */
	public InterestPaymentSchedule getInterestPaymentSchedule() {
		return interestPaymentSchedule;
	}

	/**
	 * Sets the interest payment schedule.
	 *
	 * @param interestPaymentSchedule the new interest payment schedule
	 */
	public void setInterestPaymentSchedule(
			InterestPaymentSchedule interestPaymentSchedule) {
		this.interestPaymentSchedule = interestPaymentSchedule;
	}

	

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the interest factor.
	 *
	 * @return the interest factor
	 */
	public BigDecimal getInterestFactor() {
		return this.interestFactor;
	}

	/**
	 * Sets the interest factor.
	 *
	 * @param interestFactor the new interest factor
	 */
	public void setInterestFactor(BigDecimal interestFactor) {
		this.interestFactor = interestFactor;
	}

	/**
	 * Gets the interest periodicity.
	 *
	 * @return the interest periodicity
	 */
	public Integer getInterestPeriodicity() {
		return this.interestPeriodicity;
	}

	/**
	 * Sets the interest periodicity.
	 *
	 * @param interestPeriodicity the new interest periodicity
	 */
	public void setInterestPeriodicity(Integer interestPeriodicity) {
		this.interestPeriodicity = interestPeriodicity;
	}

	/**
	 * Gets the interest type.
	 *
	 * @return the interest type
	 */
	public Integer getInterestType() {
		return this.interestType;
	}

	/**
	 * Sets the interest type.
	 *
	 * @param interestType the new interest type
	 */
	public void setInterestType(Integer interestType) {
		this.interestType = interestType;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the periodicity.
	 *
	 * @return the periodicity
	 */
	public Integer getPeriodicity() {
		return this.periodicity;
	}

	/**
	 * Sets the periodicity.
	 *
	 * @param periodicity the new periodicity
	 */
	public void setPeriodicity(Integer periodicity) {
		this.periodicity = periodicity;
	}

	/**
	 * Gets the registry days.
	 *
	 * @return the registry days
	 */
	public Integer getRegistryDays() {
		return this.registryDays;
	}

	/**
	 * Sets the registry days.
	 *
	 * @param registryDays the new registry days
	 */
	public void setRegistryDays(Integer registryDays) {
		this.registryDays = registryDays;
	}

	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return this.registryType;
	}

	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	/**
	 * Gets the schedule state.
	 *
	 * @return the schedule state
	 */
	public Integer getScheduleState() {
		return this.scheduleState;
	}

	/**
	 * Sets the schedule state.
	 *
	 * @param scheduleState the new schedule state
	 */
	public void setScheduleState(Integer scheduleState) {
		this.scheduleState = scheduleState;
	}

	/**
	 * Gets the spread.
	 *
	 * @return the spread
	 */
	public BigDecimal getSpread() {
		return this.spread;
	}

	/**
	 * Sets the spread.
	 *
	 * @param spread the new spread
	 */
	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}

	/**
	 * Gets the tax factor.
	 *
	 * @return the tax factor
	 */
	public BigDecimal getTaxFactor() {
		return this.taxFactor;
	}

	/**
	 * Sets the tax factor.
	 *
	 * @param taxFactor the new tax factor
	 */
	public void setTaxFactor(BigDecimal taxFactor) {
		this.taxFactor = taxFactor;
	}

	/**
	 * Gets the yield.
	 *
	 * @return the yield
	 */
	public Integer getYield() {
		return this.yield;
	}

	/**
	 * Sets the yield.
	 *
	 * @param yield the new yield
	 */
	public void setYield(Integer yield) {
		this.yield = yield;
	}

	/**
	 * Gets the program int coupon req his.
	 *
	 * @return the program int coupon req his
	 */
	public List<ProgramIntCouponReqHi> getProgramIntCouponReqHis() {
		return this.programIntCouponReqHis;
	}

	/**
	 * Sets the program int coupon req his.
	 *
	 * @param programIntCouponReqHis the new program int coupon req his
	 */
	public void setProgramIntCouponReqHis(List<ProgramIntCouponReqHi> programIntCouponReqHis) {
		this.programIntCouponReqHis = programIntCouponReqHis;
	}

	/**
	 * Gets the calendar days description.
	 *
	 * @return the calendar days description
	 */
	public String getCalendarDaysDescription(){
		if(calendarType!=null){
			return CalendarType.lookup.get(calendarType).getText1();
		}
		return "";
	}

	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IntPaymentScheduleReqHi [calendarType=" + calendarType
				+ ", interestFactor=" + interestFactor + ", interestType="
				+ interestType + ", periodicity=" + periodicity + ", spread="
				+ spread + ", calendarMonth=" + calendarMonth
				+ ", calendarDays=" + calendarDays + ", rateType=" + rateType
				+ ", rateValue=" + rateValue + ", operationType="
				+ operationType + ", minimumRate=" + minimumRate
				+ ", maximumRate=" + maximumRate + "]";
	}

	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("programIntCouponReqHis", programIntCouponReqHis);
        return detailsMap;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	

	/**
	 * Gets the calendar month.
	 *
	 * @return the calendar month
	 */
	public Integer getCalendarMonth() {
		return calendarMonth;
	}

	/**
	 * Sets the calendar month.
	 *
	 * @param calendarMonth the new calendar month
	 */
	public void setCalendarMonth(Integer calendarMonth) {
		this.calendarMonth = calendarMonth;
	}

	/**
	 * Gets the calendar days.
	 *
	 * @return the calendar days
	 */
	public Integer getCalendarDays() {
		return calendarDays;
	}

	/**
	 * Sets the calendar days.
	 *
	 * @param calendarDays the new calendar days
	 */
	public void setCalendarDays(Integer calendarDays) {
		this.calendarDays = calendarDays;
	}

	/**
	 * Gets the rate type.
	 *
	 * @return the rate type
	 */
	public Integer getRateType() {
		return rateType;
	}

	/**
	 * Sets the rate type.
	 *
	 * @param rateType the new rate type
	 */
	public void setRateType(Integer rateType) {
		this.rateType = rateType;
	}

	/**
	 * Gets the rate value.
	 *
	 * @return the rate value
	 */
	public BigDecimal getRateValue() {
		return rateValue;
	}

	/**
	 * Sets the rate value.
	 *
	 * @param rateValue the new rate value
	 */
	public void setRateValue(BigDecimal rateValue) {
		this.rateValue = rateValue;
	}

	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Integer getOperationType() {
		return operationType;
	}

	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Integer operationType) {
		this.operationType = operationType;
	}

	/**
	 * Gets the minimum rate.
	 *
	 * @return the minimum rate
	 */
	public BigDecimal getMinimumRate() {
		return minimumRate;
	}

	/**
	 * Sets the minimum rate.
	 *
	 * @param minimumRate the new minimum rate
	 */
	public void setMinimumRate(BigDecimal minimumRate) {
		this.minimumRate = minimumRate;
	}

	/**
	 * Gets the maximum rate.
	 *
	 * @return the maximum rate
	 */
	public BigDecimal getMaximumRate() {
		return maximumRate;
	}

	/**
	 * Sets the maximum rate.
	 *
	 * @param maximumRate the new maximum rate
	 */
	public void setMaximumRate(BigDecimal maximumRate) {
		this.maximumRate = maximumRate;
	}
	
	/**
	 * Gets the periodicity days.
	 *
	 * @return the periodicity days
	 */
	public Integer getPeriodicityDays() {
		return periodicityDays;
	}

	/**
	 * Sets the periodicity days.
	 *
	 * @param periodicityDays the new periodicity days
	 */
	public void setPeriodicityDays(Integer periodicityDays) {
		this.periodicityDays = periodicityDays;
	}

	/**
	 * Checks if is all coupons pending payment.
	 *
	 * @return true, if is all coupons pending payment
	 */
	public boolean isAllCouponsPendingPayment() {
		return isAllCouponsPendingPayment;
	}

	/**
	 * Sets the all coupons pending payment.
	 *
	 * @param isAllCouponsPendingPayment the new all coupons pending payment
	 */
	public void setAllCouponsPendingPayment(boolean isAllCouponsPendingPayment) {
		this.isAllCouponsPendingPayment = isAllCouponsPendingPayment;
	}
	
	/* Indicators --Start */
	
	/* Interest type --start*/
	
	/**
	 * Checks if is fixed interest type.
	 *
	 * @return true, if is fixed interest type
	 */
	public boolean isFixedInterestType(){
		if(InterestType.FIXED.getCode().equals(getInterestType())){
			return true;
		}
		return false;
	}

	/**
	 * Checks if is mixed interest type.
	 *
	 * @return true, if is mixed interest type
	 */
	public boolean isMixedInterestType(){
		if(InterestType.MIXED.getCode().equals(getInterestType() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is variable interest type.
	 *
	 * @return true, if is variable interest type
	 */
	public boolean isVariableInterestType(){
		if(InterestType.VARIABLE.getCode().equals( getInterestType() )){
			return true;
		}
		return false;
	}
	
	/* Interest type --end*/
	
	/* Calendar type --start */
	
	/**
	 * Checks if is commercial calendar type.
	 *
	 * @return true, if is commercial calendar type
	 */
	public boolean isCommercialCalendarType(){
		if(CalendarType.COMMERCIAL.getCode().equals( getCalendarType() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is calendar calendar type.
	 *
	 * @return true, if is calendar calendar type
	 */
	public boolean isCalendarCalendarType(){
		if(CalendarType.CALENDAR.getCode().equals( getCalendarType() )){
			return true;
		}
		return false;
	}
	
	/* Calendar type --end*/
	
	/* Calendar Month --start */
	
	/**
	 * Checks if is _30 calendar month.
	 *
	 * @return true, if is _30 calendar month
	 */
	public boolean is_30CalendarMonth(){
		if(CalendarMonthType._30.getCode().equals( getCalendarMonth() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is actual calendar month.
	 *
	 * @return true, if is actual calendar month
	 */
	public boolean isActualCalendarMonth(){
		if(CalendarMonthType.ACTUAL.getCode().equals( getCalendarMonth() )){
			return true;
		}
		return false;
	}
	
	/* Calendar Month --end */
	
	/**
	 * Checks if is by days periodicity.
	 *
	 * @return true, if is by days periodicity
	 */
	public boolean isByDaysPeriodicity(){
		if(InterestPeriodicityType.BY_DAYS.getCode().equals( getPeriodicity() )){
			return true;
		}
		return false;
	}
	
	public boolean isRejectOtherMotiveSelected(){
		if(PaymentScheduleReqHiRejectType.OTHER.getCode().equals( getRejectMotive() )){
			return true;
		}
		return false;
	}
	
	

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/**
	 * Sets the data from int payment schedule.
	 *
	 * @param intPaymentSchedule the int payment schedule
	 * @param loggerUser the logger user
	 * @param registryType the registry type
	 */
	public void setDataFromIntPaymentSchedule(InterestPaymentSchedule intPaymentSchedule, LoggerUser loggerUser, Integer registryType){
		this.setAudit(loggerUser);
		
		this.setInterestPaymentSchedule(intPaymentSchedule);
		this.setPeriodicity(intPaymentSchedule.getPeriodicity());
		this.setPeriodicityDays( intPaymentSchedule.getPeriodicityDays() );
		this.setExpirationDate( intPaymentSchedule.getExpirationDate() );
		
		this.setCalendarMonth( intPaymentSchedule.getCalendarMonth() );
		this.setCalendarType( intPaymentSchedule.getCalendarType() );
		this.setCalendarDays( intPaymentSchedule.getCalendarDays() );
		
		this.setRateType( intPaymentSchedule.getRateType() );
		this.setRateValue( intPaymentSchedule.getRateValue() );
		this.setOperationType( intPaymentSchedule.getOperationType() );
		
		this.setInterestType( intPaymentSchedule.getInterestType() );
		this.setFinancialIndex( intPaymentSchedule.getFinancialIndex() );
		this.setSpread( intPaymentSchedule.getSpread() );
		this.setMinimumRate( intPaymentSchedule.getMinimumRate() );
		this.setMaximumRate( intPaymentSchedule.getMaximumRate() );
		
		this.setInterestPeriodicity( intPaymentSchedule.getInterestPeriodicity() );
		this.setInterestFactor( intPaymentSchedule.getInterestFactor() );
		this.setTaxFactor( intPaymentSchedule.getTaxFactor() );
		this.setRegistryDays( intPaymentSchedule.getRegistryDays() );
		this.setYield( intPaymentSchedule.getYield() );
		this.setCashNominal( intPaymentSchedule.getCashNominal() );

		this.setScheduleState( intPaymentSchedule.getScheduleState() );
		this.setRegistryType( registryType );
		this.setRequestState( Integer.valueOf(0) );
		
		this.setRegistryUser( loggerUser.getUserName() );
		this.setRegistryDate( loggerUser.getAuditTime() );
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Object clone(){
		Object clone=null;
		try {
			clone=super.clone();
		} catch (CloneNotSupportedException   e) {
			return null;
		}
		return clone;
	}
	
}