package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.AmortizationType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleStateType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the AMORTIZATION_PAYMENT_CRONOGRAM database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/04/2014
 */
@Entity
@NamedQueries({
	@NamedQuery(name = AmortizationPaymentSchedule.FIND_BY_SECURITY_AND_STATE, 
			query = "SELECT a FROM AmortizationPaymentSchedule a WHERE a.security.idSecurityCodePk = :idSecurityPrm and a.scheduleState = :statePrm")
})
@Table(name="AMORTIZATION_PAYMENT_SCHEDULE")
public class AmortizationPaymentSchedule implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant FIND_BY_SECURITY_AND_STATE. */
	public static final String FIND_BY_SECURITY_AND_STATE="AmortizationPaymentSchedule.searchBySecurityAndState";
	
	/** The id amo payment schedule pk. */
	@Id
	@SequenceGenerator(name="AMORTIZATION_PAYMENT_SCHEDULE_IDAMOPAYMENTSCHEDULEPK_GENERATOR", sequenceName="SQ_ID_AMO_PAYMENT_SCHEDULE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AMORTIZATION_PAYMENT_SCHEDULE_IDAMOPAYMENTSCHEDULEPK_GENERATOR")
	@Column(name="ID_AMO_PAYMENT_SCHEDULE_PK")
	private Long idAmoPaymentSchedulePk;

	/** The amortization factor. */
	@Column(name="AMORTIZATION_FACTOR")
	private BigDecimal amortizationFactor;

	/** The amortization type. */
	@Column(name="AMORTIZATION_TYPE")
	private Integer amortizationType;

	/** The calendar type. */
	@Column(name="CALENDAR_TYPE")
	private Integer calendarType;

	/** The corporative days. */
	@Column(name="CORPORATIVE_DAYS")
	private Integer corporativeDays;

	/** The schedule state. */
	@Column(name="SCHEDULE_STATE")
	private Integer scheduleState;

	/** The security. */
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The periodicity. */
	@Column(name="PERIODICITY")
	private Integer periodicity;

	/** The periodicity days. */
	@Column(name="PERIODICITY_DAYS")
	private Integer periodicityDays;	
	
	/** The registry days. */
	@Column(name="REGISTRY_DAYS")
	private Integer registryDays;

	/** The tax factor. */
	@Column(name="TAX_FACTOR")
	private BigDecimal taxFactor;

	/** The payment modality. */
	@Column(name="PAYMENT_MODALITY")
	private Integer paymentModality;
	
    /** The payment first expiration date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="PAYMENT_FIRST_EXPIRATION_DATE")
	private Date paymentFirstExpirationDate;
	
    /** The number payments. */
    @Column(name="NUMBER_PAYMENTS")
	private Integer numberPayments;
	
	/** The amo payment schedule req hi. */
	@OneToMany(mappedBy="amortizationPaymentSchedule",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<AmoPaymentScheduleReqHi> amoPaymentScheduleReqHi;
	
	//bi-directional many-to-one association to ProgramAmortizationCoupon
	/** The program amortization coupons. */
	@OneToMany(mappedBy="amortizationPaymentSchedule",cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	private List<ProgramAmortizationCoupon> programAmortizationCoupons;
	
	/** The is all coupons pending payment. */
	@Transient
	private boolean isAllCouponsPendingPayment;
	
	@Transient
	private BigDecimal totalAmortizationFactor;
	
	@Transient
	private BigDecimal totalAmortizationCoupon;
	
    /**
     * Instantiates a new amortization payment schedule.
     */
    public AmortizationPaymentSchedule() {
    	programAmortizationCoupons=new ArrayList<ProgramAmortizationCoupon>();
    }
    
    public void calculateTotalFactorAndAmount(){
    	calculateTotalAmoartizationFactor();
    	calculateTotalAmortizationCupon();
    }

    public void calculateTotalAmoartizationFactor(){
    	totalAmortizationFactor=BigDecimal.ZERO;
    	if(getProgramAmortizationCoupons()!=null && !getProgramAmortizationCoupons().isEmpty()){
        	for(ProgramAmortizationCoupon pro : getProgramAmortizationCoupons()){
        		totalAmortizationFactor=totalAmortizationFactor.add(pro.getAmortizationFactor());
        	}
    	}
    }
    
    public void calculateTotalAmortizationCupon(){
    	totalAmortizationCoupon=BigDecimal.ZERO;
    	if(getProgramAmortizationCoupons()!=null && !getProgramAmortizationCoupons().isEmpty()){
        	for(ProgramAmortizationCoupon pro : getProgramAmortizationCoupons()){
        		totalAmortizationCoupon=totalAmortizationCoupon.add( pro.getCouponAmount() );
        	}
    	}
    }
    

	/**
	 * Gets the id amo payment schedule pk.
	 *
	 * @return the id amo payment schedule pk
	 */
	public Long getIdAmoPaymentSchedulePk() {
		return idAmoPaymentSchedulePk;
	}



	/**
	 * Sets the id amo payment schedule pk.
	 *
	 * @param idAmoPaymentSchedulePk the new id amo payment schedule pk
	 */
	public void setIdAmoPaymentSchedulePk(Long idAmoPaymentSchedulePk) {
		this.idAmoPaymentSchedulePk = idAmoPaymentSchedulePk;
	}



	/**
	 * Gets the amortization factor.
	 *
	 * @return the amortization factor
	 */
	public BigDecimal getAmortizationFactor() {
		return this.amortizationFactor;
	}

	/**
	 * Sets the amortization factor.
	 *
	 * @param amortizationFactor the new amortization factor
	 */
	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}

	/**
	 * Gets the amortization type.
	 *
	 * @return the amortization type
	 */
	public Integer getAmortizationType() {
		return this.amortizationType;
	}

	/**
	 * Sets the amortization type.
	 *
	 * @param amortizationType the new amortization type
	 */
	public void setAmortizationType(Integer amortizationType) {
		this.amortizationType = amortizationType;
	}



	/**
	 * Gets the calendar type.
	 *
	 * @return the calendar type
	 */
	public Integer getCalendarType() {
		return calendarType;
	}


	/**
	 * Sets the calendar type.
	 *
	 * @param calendarType the new calendar type
	 */
	public void setCalendarType(Integer calendarType) {
		this.calendarType = calendarType;
	}


	/**
	 * Gets the corporative days.
	 *
	 * @return the corporative days
	 */
	public Integer getCorporativeDays() {
		return this.corporativeDays;
	}

	/**
	 * Sets the corporative days.
	 *
	 * @param corporativeDays the new corporative days
	 */
	public void setCorporativeDays(Integer corporativeDays) {
		this.corporativeDays = corporativeDays;
	}

	

	/**
	 * Gets the schedule state.
	 *
	 * @return the schedule state
	 */
	public Integer getScheduleState() {
		return scheduleState;
	}


	/**
	 * Sets the schedule state.
	 *
	 * @param scheduleState the new schedule state
	 */
	public void setScheduleState(Integer scheduleState) {
		this.scheduleState = scheduleState;
	}


	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	

	/**
	 * Gets the periodicity.
	 *
	 * @return the periodicity
	 */
	public Integer getPeriodicity() {
		return periodicity;
	}


	/**
	 * Sets the periodicity.
	 *
	 * @param periodicity the new periodicity
	 */
	public void setPeriodicity(Integer periodicity) {
		this.periodicity = periodicity;
	}


	/**
	 * Gets the registry days.
	 *
	 * @return the registry days
	 */
	public Integer getRegistryDays() {
		return this.registryDays;
	}

	/**
	 * Sets the registry days.
	 *
	 * @param registryDays the new registry days
	 */
	public void setRegistryDays(Integer registryDays) {
		this.registryDays = registryDays;
	}

	/**
	 * Gets the tax factor.
	 *
	 * @return the tax factor
	 */
	public BigDecimal getTaxFactor() {
		return this.taxFactor;
	}

	/**
	 * Sets the tax factor.
	 *
	 * @param taxFactor the new tax factor
	 */
	public void setTaxFactor(BigDecimal taxFactor) {
		this.taxFactor = taxFactor;
	}

	/**
	 * Gets the periodicity days.
	 *
	 * @return the periodicity days
	 */
	public Integer getPeriodicityDays() {
		return periodicityDays;
	}


	/**
	 * Sets the periodicity days.
	 *
	 * @param periodicityDays the new periodicity days
	 */
	public void setPeriodicityDays(Integer periodicityDays) {
		this.periodicityDays = periodicityDays;
	}


	/**
	 * Gets the program amortization coupons.
	 *
	 * @return the program amortization coupons
	 */
	public List<ProgramAmortizationCoupon> getProgramAmortizationCoupons() {
		return programAmortizationCoupons;
	}

	/**
	 * Sets the program amortization coupons.
	 *
	 * @param programAmortizationCoupons the new program amortization coupons
	 */
	public void setProgramAmortizationCoupons(
			List<ProgramAmortizationCoupon> programAmortizationCoupons) {
		this.programAmortizationCoupons = programAmortizationCoupons;
	}

	
	
	/**
	 * Gets the amo payment schedule req hi.
	 *
	 * @return the amo payment schedule req hi
	 */
	public List<AmoPaymentScheduleReqHi> getAmoPaymentScheduleReqHi() {
		return amoPaymentScheduleReqHi;
	}


	/**
	 * Sets the amo payment schedule req hi.
	 *
	 * @param amoPaymentScheduleReqHi the new amo payment schedule req hi
	 */
	public void setAmoPaymentScheduleReqHi(
			List<AmoPaymentScheduleReqHi> amoPaymentScheduleReqHi) {
		this.amoPaymentScheduleReqHi = amoPaymentScheduleReqHi;
	}

	/*Amortization type start*/
	
	/**
	 * Checks if is proportional amortization type.
	 *
	 * @return true, if is proportional amortization type
	 */
	public boolean isProportionalAmortizationType(){
		if(AmortizationType.PROPORTIONAL.getCode().equals(getAmortizationType()))
			return true;		
		return false;
	}
	
	/**
	 * Checks if is not proportional amortization type.
	 *
	 * @return true, if is not proportional amortization type
	 */
	public boolean isNotProportionalAmortizationType(){
		if(AmortizationType.NO_PROPORTIONAL.getCode().equals(getAmortizationType()))
			return true;
		return false;
	}
	
	/*Amortization type end*/

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        	if(programAmortizationCoupons!=null) {
        		for(ProgramAmortizationCoupon current: programAmortizationCoupons) {
        			current.setAudit(loggerUser);	
        		}
        	}
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("programAmortizationCoupons", programAmortizationCoupons);
        return detailsMap;
	}


	/**
	 * Checks if is all coupons pending payment.
	 *
	 * @return true, if is all coupons pending payment
	 */
	public boolean isAllCouponsPendingPayment() {
		return isAllCouponsPendingPayment;
	}


	/**
	 * Sets the all coupons pending payment.
	 *
	 * @param isAllCouponsPendingPayment the new all coupons pending payment
	 */
	public void setAllCouponsPendingPayment(boolean isAllCouponsPendingPayment) {
		this.isAllCouponsPendingPayment = isAllCouponsPendingPayment;
	}
	
	

	/**
	 * Gets the payment modality.
	 *
	 * @return the payment modality
	 */
	public Integer getPaymentModality() {
		return paymentModality;
	}


	/**
	 * Sets the payment modality.
	 *
	 * @param paymentModality the new payment modality
	 */
	public void setPaymentModality(Integer paymentModality) {
		this.paymentModality = paymentModality;
	}


	/**
	 * Gets the payment first expiration date.
	 *
	 * @return the payment first expiration date
	 */
	public Date getPaymentFirstExpirationDate() {
		return paymentFirstExpirationDate;
	}


	/**
	 * Sets the payment first expiration date.
	 *
	 * @param paymentFirstExpirationDate the new payment first expiration date
	 */
	public void setPaymentFirstExpirationDate(Date paymentFirstExpirationDate) {
		this.paymentFirstExpirationDate = paymentFirstExpirationDate;
	}


	/**
	 * Gets the number payments.
	 *
	 * @return the number payments
	 */
	public Integer getNumberPayments() {
		return numberPayments;
	}


	/**
	 * Sets the number payments.
	 *
	 * @param numberPayments the new number payments
	 */
	public void setNumberPayments(Integer numberPayments) {
		this.numberPayments = numberPayments;
	}


	/**
	 * Sets the data from amo payment schedule req hi.
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param loggerUser the logger user
	 */
	public void setDataFromAmoPaymentScheduleReqHi(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, LoggerUser loggerUser){
		this.setAudit(loggerUser);
		       
		this.setPaymentModality( amoPaymentScheduleReqHi.getPaymentModality() );
		this.setAmortizationType( amoPaymentScheduleReqHi.getAmortizationType() );
		this.setAmortizationFactor( amoPaymentScheduleReqHi.getAmortizationFactor() );
		this.setTaxFactor( amoPaymentScheduleReqHi.getTaxFactor() );
		this.setScheduleState( amoPaymentScheduleReqHi.getScheduleState() );
        this.setPeriodicity(amoPaymentScheduleReqHi.getPeriodicity());
        this.setPeriodicityDays( amoPaymentScheduleReqHi.getPeriodicityDays() );
        this.setPaymentFirstExpirationDate( amoPaymentScheduleReqHi.getPaymentFirstExpirationDate() );
        this.setNumberPayments( amoPaymentScheduleReqHi.getNumberPayments() );
        this.setRegistryDays(amoPaymentScheduleReqHi.getRegistryDays());
        this.setCorporativeDays(amoPaymentScheduleReqHi.getCorporativeDays());
        this.setCalendarType( amoPaymentScheduleReqHi.getCalendarDays() );
        
//        this.setLastModifyUser(loggerUser.getUserName());
//        this.setLastModifyDate(loggerUser.getAuditTime());
//        this.setLastModifyIp( loggerUser.getIpAddress() );
//        this.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem()); 

	}

	public void setDataSecurity(Security security){
		this.setSecurity(security);
		this.setAmortizationType( security.getAmortizationType() );
		this.setAmortizationFactor( security.getAmortizationFactor() );
		//security.getAmortizationPaymentSchedule().setTaxFactor( ); falta implementar
		this.setCalendarType( security.getCalendarType()); 
		this.setPeriodicity( security.getAmortizationPeriodicity() );
		this.setPeriodicityDays( security.getAmortizationPeriodicityDays() );
		this.setRegistryDays( security.getStockRegistryDays() );
		this.setCorporativeDays( Integer.valueOf(0) );
		this.setScheduleState( PaymentScheduleStateType.REGTERED.getCode() );
	
		this.setPaymentModality( security.getCapitalPaymentModality() );
		this.setPaymentFirstExpirationDate( security.getPaymentFirstExpirationDate() );
	}
	


	public BigDecimal getTotalAmortizationFactor() {
		return totalAmortizationFactor;
	}

	public void setTotalAmortizationFactor(BigDecimal totalAmortizationFactor) {
		this.totalAmortizationFactor = totalAmortizationFactor;
	}

	public BigDecimal getTotalAmortizationCoupon() {
		return totalAmortizationCoupon;
	}


	public void setTotalAmortizationCoupon(BigDecimal totalAmortizationAmount) {
		this.totalAmortizationCoupon = totalAmortizationAmount;
	}
	
	
	
}