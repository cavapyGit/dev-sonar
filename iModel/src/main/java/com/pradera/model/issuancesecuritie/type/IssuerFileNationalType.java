package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantFileType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public enum IssuerFileNationalType {
	
	/** The commercialregisterfile. */
	COMMERCIALREGISTERFILE(Integer.valueOf(94),"CERTIFICADO DE REGISTRO MERCANTIL"),

	/** The bylawsfile. */
	BYLAWSFILE(Integer.valueOf(1238),"ESTATUTOS SOCIALES VIGENTES"),

	/** The shareholderslistfile. */
	SHAREHOLDERSLISTFILE(Integer.valueOf(1239),"LISTADO DE ACCIONISTAS DE LA SOCIEDAD"),

	/** The generalassemblyfile. */
	GENERALASSEMBLYFILE(Integer.valueOf(1240),"ACTA DE LA ASAMBLEA GENERAL DE ACCIONISTAS"),

	/** The registrationrncfile. */
	REGISTRATIONRNCFILE(Integer.valueOf(1241),"CONSTANCIA DE INSCRIPCIÓN EN EL RNC"),

	/** The representativecertificationfile. */
	REPRESENTATIVECERTIFICATIONFILE(Integer.valueOf(1242),"CERTIFICACIÓN DEL REPRESENTANTE"),

	/** The representativepassportfile. */
	REPRESENTATIVEPASSPORTFILE(Integer.valueOf(1243),"CÉDULA O PASAPORTE DEL REPRESENTANTE"),
	
	/** The certificationauthorityfile. */
	CERTIFICATION_ISSUED_BY_SIV(Integer.valueOf(1244),"CERTIFICACIÓN EMITIDA POR LA SIV");		
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant file type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuerFileNationalType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<IssuerFileNationalType> list = new ArrayList<IssuerFileNationalType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, IssuerFileNationalType> lookup = new HashMap<Integer, IssuerFileNationalType>();
	static {
		for (IssuerFileNationalType s : EnumSet.allOf(IssuerFileNationalType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant file national type
	 */
	public static IssuerFileNationalType get(Integer code) {
		return lookup.get(code);
	}

}
