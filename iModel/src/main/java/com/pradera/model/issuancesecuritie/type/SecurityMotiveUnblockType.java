/*
 * 
 */
package com.pradera.model.issuancesecuritie.type;

public enum SecurityMotiveUnblockType {
	
	OTHER(Integer.valueOf(1956));
	
	private Integer code;
	
	
	/**
	 * Instantiates a new security motive unblock type.
	 *
	 * @param code the code
	 */
	private SecurityMotiveUnblockType(Integer code){
		this.code=code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}
