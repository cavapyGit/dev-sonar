package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc


/**
 * The Enum SecurityAuthorizedType.
 */
public enum SecurityAuthorizedType {

	AUTHORIZED(Integer.valueOf(1),"AUTORIZADO"),
	
	NOT_AUTHORIZED(Integer.valueOf(0),"NO AUTORIZADO");
	
	
	/** The Constant list. */
	public static final List<SecurityAuthorizedType> list=new ArrayList<SecurityAuthorizedType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SecurityAuthorizedType> lookup=new HashMap<Integer, SecurityAuthorizedType>();
	
	static {
		for(SecurityAuthorizedType e : SecurityAuthorizedType.values()){
			lookup.put(e.getCode(), e);
			list.add( e );
		}
	}
	
	/** The code. */
	private Integer  code;
	
	/** The value. */
	private String value;
	

	/**
	 * Instantiates a new security state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SecurityAuthorizedType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the security state type
	 */
	public static SecurityAuthorizedType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
