package com.pradera.model.issuancesecuritie.placementsegment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PLACEMENT_SEGMENT_DETAIL database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
@Entity
@Table(name="PLACEMENT_SEGMENT_DETAIL")
public class PlacementSegmentDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id placement segment det pk. */
	@Id
	@SequenceGenerator(name="SQ_PLACEMENT_SEGMENT_DETAIL_PK", sequenceName="SQ_ID_PLACEMENT_SEGMENT_DET_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_PLACEMENT_SEGMENT_DETAIL_PK")
	@Column(name="ID_PLACEMENT_SEGMENT_DET_PK")
	private Long idPlacementSegmentDetPk;

	/** The description. */
	private String description;

	/** The security. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK",referencedColumnName="ID_SECURITY_CODE_PK")
	private Security security;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The placed amount. */
	@Column(name="PLACED_AMOUNT")
	private BigDecimal placedAmount;
	
	/** The request amount. */
	@Column(name="REQUEST_AMOUNT")
	private BigDecimal requestAmount;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state placement segment det. */
	@Column(name="STATE_PLACEMENT_SEGMENT_DET")
	private Integer statePlacementSegmentDet;
	
	@Column(name="PLACEMENT_SEGMENT_DET_TYPE")
	private Integer placementSegmentDetType;

	/** The fixed amount. */
	@Column(name="BALANCE_TO_CONFIRM")
	private BigDecimal balanceToConfirm;
	
	//bi-directional many-to-one association to PlacementSegment
	/** The placement segment. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PLACEMENT_SEGMENT_FK")
	private PlacementSegment placementSegment;

	/** The fixed amount. */
	@Column(name="REQUEST_BALANCE")
	private BigDecimal requestBalance;
	
	/** The pending operation. */
	@Column(name="PENDING_OPERATION")
	private BigDecimal pendingOperation;
	
	/** The pending annotation. */
	@Column(name="PENDING_ANNOTATION")
	private BigDecimal pendingAnnotation;

	
	/** The selected. */
	@Transient
	private boolean selected;
	
	@Transient
	private BigDecimal securityBalance;

	@Transient
	private BigDecimal pendingPlacement;
	
	
	
	
    /**
     * Instantiates a new placement segment detail.
     */
    public PlacementSegmentDetail() {
    }

	/**
	 * Gets the id placement segment det pk.
	 *
	 * @return the id placement segment det pk
	 */
	public Long getIdPlacementSegmentDetPk() {
		return this.idPlacementSegmentDetPk;
	}

	/**
	 * Sets the id placement segment det pk.
	 *
	 * @param idPlacementSegmentDetPk the new id placement segment det pk
	 */
	public void setIdPlacementSegmentDetPk(Long idPlacementSegmentDetPk) {
		this.idPlacementSegmentDetPk = idPlacementSegmentDetPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the placed amount.
	 *
	 * @return the placed amount
	 */
	public BigDecimal getPlacedAmount() {
		return this.placedAmount;
	}

	/**
	 * Sets the placed amount.
	 *
	 * @param placedAmount the new placed amount
	 */
	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the placement segment.
	 *
	 * @return the placement segment
	 */
	public PlacementSegment getPlacementSegment() {
		return this.placementSegment;
	}

	/**
	 * Sets the placement segment.
	 *
	 * @param placementSegment the new placement segment
	 */
	public void setPlacementSegment(PlacementSegment placementSegment) {
		this.placementSegment = placementSegment;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Sets the state placement segment det.
	 *
	 * @param statePlacementSegmentDet the new state placement segment det
	 */
	public void setStatePlacementSegmentDet(Integer statePlacementSegmentDet) {
		this.statePlacementSegmentDet = statePlacementSegmentDet;
	}

	/**
	 * Gets the state placement segment det.
	 *
	 * @return the state placement segment det
	 */
	public Integer getStatePlacementSegmentDet() {
		return statePlacementSegmentDet;
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * Gets the placement segment det type.
	 *
	 * @return the placement segment det type
	 */
	public Integer getPlacementSegmentDetType() {
		return placementSegmentDetType;
	}

	/**
	 * Sets the placement segment det type.
	 *
	 * @param placementSegmentDetType the new placement segment det type
	 */
	public void setPlacementSegmentDetType(Integer placementSegmentDetType) {
		this.placementSegmentDetType = placementSegmentDetType;
	}

	/**
	 * Gets the request amount.
	 *
	 * @return the request amount
	 */
	public BigDecimal getRequestAmount() {
		return requestAmount;
	}

	/**
	 * Sets the request amount.
	 *
	 * @param requestAmount the new request amount
	 */
	public void setRequestAmount(BigDecimal requestAmount) {
		this.requestAmount = requestAmount;
	}

	public BigDecimal getBalanceToConfirm() {
		return balanceToConfirm;
	}

	public void setBalanceToConfirm(BigDecimal balanceToConfirm) {
		this.balanceToConfirm = balanceToConfirm;
	}

	
	
	public BigDecimal getSecurityBalance() {
		return securityBalance;
	}

	public void setSecurityBalance(BigDecimal securityBalance) {
		this.securityBalance = securityBalance;
	}

	
	/**
	 * @return the requestBalance
	 */
	public BigDecimal getRequestBalance() {
		return requestBalance;
	}

	/**
	 * @param requestBalance the requestBalance to set
	 */
	public void setRequestBalance(BigDecimal requestBalance) {
		this.requestBalance = requestBalance;
	}

	


	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal getPendingOperation() {
		return pendingOperation;
	}

	public void setPendingOperation(BigDecimal pendingOperation) {
		this.pendingOperation = pendingOperation;
	}

	public BigDecimal getPendingAnnotation() {
		return pendingAnnotation;
	}

	public void setPendingAnnotation(BigDecimal pendingAnnotation) {
		this.pendingAnnotation = pendingAnnotation;
	}

	public BigDecimal getPendingPlacement() {
		return pendingPlacement;
	}

	public void setPendingPlacement(BigDecimal pendingPlacement) {
		this.pendingPlacement = pendingPlacement;
	}

	
}