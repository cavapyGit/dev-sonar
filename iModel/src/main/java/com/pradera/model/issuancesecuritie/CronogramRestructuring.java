package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CRONOGRAM_RESTRUNCTURING database table.
 * 
 */
@Entity
@Table(name="CRONOGRAM_RESTRUCTURING")
public class CronogramRestructuring implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CRONOGRAM_RESTRUNCTURING_IDCRONOGRAMRESTRUCTURINGPK_GENERATOR", sequenceName="SQ_ID_CRONOGRAM_RESTRUCTURING_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CRONOGRAM_RESTRUNCTURING_IDCRONOGRAMRESTRUCTURINGPK_GENERATOR")
	@Column(name="ID_CRONOGRAM_RESTRUCTURING_PK")
	private Long idCronogramRestructuringPk;

	@Column(name="AMORTIZATION_FACTOR")
	private BigDecimal amortizationFactor;

	@Column(name="AMORTIZATION_TYPE")
	private BigDecimal amortizationType;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

	@Column(name="COUPON_AMORTIZATION_FACTOR")
	private BigDecimal couponAmortizationFactor;

	@Column(name="CRONOGRAM_TYPE_AFFECTED")
	private BigDecimal cronogramTypeAffected;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

	@Column(name="FINANCIAL_INDEX")
	private BigDecimal financialIndex;

	@Column(name="INTEREST_FACTOR")
	private BigDecimal interestFactor;

	@Column(name="INTEREST_PERIODICITY")
	private BigDecimal interestPeriodicity;

	@Column(name="INTEREST_RATE")
	private BigDecimal interestRate;

	@Column(name="INTEREST_TYPE")
	private BigDecimal interestType;

	@Column(name="LAST_MODIFY_APP")
	private BigDecimal lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="PERIODICITY_CRONOGRAM")
	private BigDecimal periodicityCronogram;

	@Column(name="REGISTRY_USER")
	private String registryUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="RESTRUCTURATION_DATE")
	private Date restructurationDate;

	@Column(name="RESTRUCTURING_TYPE")
	private BigDecimal restructuringType;

	//bi-directional many-to-one association to InterestPaymentCronogram
    @ManyToOne
	@JoinColumn(name="ID_INT_PAYMENT_SCHEDULE_FK")
	private InterestPaymentSchedule interestPaymentSchedule;

    public CronogramRestructuring() {
    }

	public Long getIdCronogramRestructuringPk() {
		return this.idCronogramRestructuringPk;
	}

	public void setIdCronogramRestructuringPk(Long idCronogramRestructuringPk) {
		this.idCronogramRestructuringPk = idCronogramRestructuringPk;
	}

	public BigDecimal getAmortizationFactor() {
		return this.amortizationFactor;
	}

	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}

	public BigDecimal getAmortizationType() {
		return this.amortizationType;
	}

	public void setAmortizationType(BigDecimal amortizationType) {
		this.amortizationType = amortizationType;
	}

	public Date getBeginingDate() {
		return this.beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public BigDecimal getCouponAmortizationFactor() {
		return this.couponAmortizationFactor;
	}

	public void setCouponAmortizationFactor(BigDecimal couponAmortizationFactor) {
		this.couponAmortizationFactor = couponAmortizationFactor;
	}

	public BigDecimal getCronogramTypeAffected() {
		return this.cronogramTypeAffected;
	}

	public void setCronogramTypeAffected(BigDecimal cronogramTypeAffected) {
		this.cronogramTypeAffected = cronogramTypeAffected;
	}

	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public BigDecimal getFinancialIndex() {
		return this.financialIndex;
	}

	public void setFinancialIndex(BigDecimal financialIndex) {
		this.financialIndex = financialIndex;
	}

	public BigDecimal getInterestFactor() {
		return this.interestFactor;
	}

	public void setInterestFactor(BigDecimal interestFactor) {
		this.interestFactor = interestFactor;
	}

	public BigDecimal getInterestPeriodicity() {
		return this.interestPeriodicity;
	}

	public void setInterestPeriodicity(BigDecimal interestPeriodicity) {
		this.interestPeriodicity = interestPeriodicity;
	}

	public BigDecimal getInterestRate() {
		return this.interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public BigDecimal getInterestType() {
		return this.interestType;
	}

	public void setInterestType(BigDecimal interestType) {
		this.interestType = interestType;
	}

	public BigDecimal getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(BigDecimal lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getPeriodicityCronogram() {
		return this.periodicityCronogram;
	}

	public void setPeriodicityCronogram(BigDecimal periodicityCronogram) {
		this.periodicityCronogram = periodicityCronogram;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public Date getRestructurationDate() {
		return this.restructurationDate;
	}

	public void setRestructurationDate(Date restructurationDate) {
		this.restructurationDate = restructurationDate;
	}

	public BigDecimal getRestructuringType() {
		return this.restructuringType;
	}

	public void setRestructuringType(BigDecimal restructuringType) {
		this.restructuringType = restructuringType;
	}

	public InterestPaymentSchedule getInterestPaymentCronogram() {
		return this.interestPaymentSchedule;
	}

	public void setInterestPaymentCronogram(InterestPaymentSchedule interestPaymentSchedule) {
		this.interestPaymentSchedule = interestPaymentSchedule;
	}
	
}