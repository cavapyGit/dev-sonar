package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the SECURITY_FOREIGN_DEPOSITORY database table.
 * 
 */
@NamedQueries({
	@NamedQuery(name=SecurityForeignDepository.SEARCH_BY_SECURITY_AND_STATE,query="Select s from SecurityForeignDepository s where s.id.idSecurityCodePk=:idSecurityCodePrm and s.securityForeignState=:statePrm")  
})
@Entity
@Table(name="SECURITY_FOREIGN_DEPOSITORY")
public class SecurityForeignDepository implements Serializable, Auditable, Cloneable {
	private static final long serialVersionUID = 1L;
	
	public static final String SEARCH_BY_SECURITY_AND_STATE="SecurityForeignDepository.SearchBySecurityAndState"; 

	@EmbeddedId
	private SecurityForeignDepositoryPK id;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

//    @Temporal( TemporalType.TIMESTAMP)
//	@Column(name="REGISTRY_DATE")
//	private Date registryDate;

	@Column(name="SECURITY_FOREIGN_STATE")
	private Integer securityForeignState;

	//bi-directional many-to-one association to Security
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_PK",insertable=false,updatable=false)
	private Security security;

    public SecurityForeignDepository() {
    }

	public SecurityForeignDepositoryPK getId() {
		return this.id;
	}

	public void setId(SecurityForeignDepositoryPK id) {
		this.id = id;
	}
	
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

//	public Date getRegistryDate() {
//		return this.registryDate;
//	}
//
//	public void setRegistryDate(Date registryDate) {
//		this.registryDate = registryDate;
//	}

	public Integer getSecurityForeignState() {
		return this.securityForeignState;
	}

	public void setSecurityForeignState(Integer securityForeignState) {
		this.securityForeignState = securityForeignState;
	}

	public Security getSecurity() {
		return this.security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public SecurityForeignDepository clone() throws CloneNotSupportedException {
        return (SecurityForeignDepository) super.clone();
    }
	
}