package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Enum IssuanceStateType.
 */

public enum SocietyType {
	

	/** The anonym society. */
	ANONYM_SOCIETY(Integer.valueOf(209),"SOCIEDAD ANONIMA"),
	
	/** The anonym society public subscrip. */
	ANONYM_SOCIETY_PUBLIC_SUBSCRIP(Integer.valueOf(1257),"SOCIEDAD EN RESPONSABILIDAD LIMITADA"),

	/** The limited liability company. */
	LIMITED_LIABILITY_COMPANY(Integer.valueOf(1258),"SOCIEDAD COLECTIVA"),
	
	/** The society collective name. */
	SOCIETY_COLLECTIVE_NAME(Integer.valueOf(1259),"SOCIEDAD EN COMANDITA SIMPLE"),
	
	/** The society simple comandita. */
	SOCIETY_SIMPLE_COMANDITA(Integer.valueOf(1260),"SOCIEDAD EN COMANDITA POR ACCIONES"),
	
	/** The comandita society for shares. */
	COMANDITA_SOCIETY_FOR_SHARES(Integer.valueOf(1261),"ASOCIACION ACCIDENTAL O DE CUENTAS EN PARITICIPACION"),
	
	/** The individual limited liability comp. */
	INDIVIDUAL_LIMITED_LIABILITY_COMP(Integer.valueOf(1262),"EMPRESAS INDIVIDUALES DE RESPONSABILIDAD LIMITADA"),
	
	/** The anonym society simplified. */
	ANONYM_SOCIETY_SIMPLIFIED(Integer.valueOf(1263),"SOCIEDADES ANONIMAS SIMPLIFICADAS"),
	
	UNIPERSONAL_SOCIETY(Integer.valueOf(2469),"SOCIEDAD UNIPERSONAL");

	
	
	/** The Constant list. */
	private static final List<SocietyType> list=new ArrayList<SocietyType>();
	
	/** The Constant map. */
	public static final Map<Integer, SocietyType> lookup=new HashMap<Integer, SocietyType>();
	
	static{
		for(SocietyType issu : SocietyType.values()){
			lookup.put(issu.getCode(), issu);
			list.add(issu);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new issuance state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SocietyType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the society type
	 */
	public static SocietyType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode(){
		return this.code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
}