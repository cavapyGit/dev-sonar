package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.accounts.type.OfferType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.IssuerDocumentType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SocietyType;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ISSUER_HISTORY database table.
 * 
 */
@Entity
@Table(name="ISSUER_HISTORY")
public class IssuerHistory implements Serializable, Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id issuer history pk. */
	@Id
	@SequenceGenerator(name="ISSUER_HISTORY_IDISSUERHISTORYPK_GENERATOR", sequenceName="SQ_ID_ISSUER_HISTORY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISSUER_HISTORY_IDISSUERHISTORYPK_GENERATOR")
	@Column(name="ID_ISSUER_HISTORY_PK")
	private Long idIssuerHistoryPk;

	/** The business name. */
	@Column(name="BUSINESS_NAME")
	private String businessName;

	/** The closing service date. */
	@Temporal(TemporalType.DATE)
	@Column(name="CLOSING_SERVICE_DATE")
	private Date closingServiceDate;

	/** The comments. */
	@Column(name="COMMENTS")
	private String observation;

	/** The contact email. */
	@Column(name="CONTACT_EMAIL")
	private String contactEmail;

	/** The contact name. */
	@Column(name="CONTACT_NAME")
	private String contactName;

	/** The contact phone. */
	@Column(name="CONTACT_PHONE")
	private String contactPhone;

	/** The contract date. */
	@Temporal(TemporalType.DATE)
	@Column(name="CONTRACT_DATE")
	private Date contractDate;

	/** The contract number. */
	@Column(name="CONTRACT_NUMBER")
	private String contractNumber;

	/** The creation date. */
	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	/** The currency. */
	@Column(name="CURRENCY")
	private Integer currency;

	/** The department. */
	@Column(name="DEPARTMENT")
	private Integer department;

	/** The district. */
	@Column(name="DISTRICT")
	private Integer district;

	/** The document number. */
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	/** The document type. */
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The economic activity. */
	@Column(name="ECONOMIC_ACTIVITY")
	private Integer economicActivity;

	@Column(name="INVESTOR_TYPE")
    private Integer investorType;
	
	/** The economic sector. */
	@Column(name="ECONOMIC_SECTOR")
	private Integer economicSector;

	/** The email. */
	@Column(name="EMAIL")
	private String email;

	/** The enrolled stock exch date. */
	@Temporal(TemporalType.DATE)
	@Column(name="ENROLLED_STOCK_EXCH_DATE")
	private Date enrolledStockExchDate;

	/** The fax number. */
	@Column(name="FAX_NUMBER")
	private String faxNumber;

	/** The holder. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;

	/** The issuer request. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_REQUEST_FK")
	private IssuerRequest issuerRequest;

	/** The ind stock exch enrolled. */
	@Column(name="IND_STOCK_EXCH_ENROLLED")
	private Integer indStockExchEnrolled;
	
	@Column(name = "CREDIT_RATING_SCALES")
	private Integer creditRatingScales;
	
	/**Columnas para negociacio de SIRTEX*/
	@Column(name = "SEC_CLASS_SIRTEX_NEGOTIATE")
	private Integer securityClassSirtexNegotiate;
	
	@Transient
	private String securityClassSirtexNegotiateDesc;
	
	@Column(name = "CURRENCY_SIRTEX_NEGOTIATE")
	private Integer currencySirtexNegotiate;
	
	@Transient
	private String currencySirtexNegotiateDesc;
	
	@Transient
	private String indSirtexNegotiateDesc;
	
	@Column(name = "PERSON_TYPE_SIRTEX_NEGOTIATE")
	private Integer personTypeSirtexNegotiate;
	
	@Transient
	private String personTypeSirtexNegotiateDesc;
	
	@Column(name = "WITH_COUPONS_SIRTEX_NEGOTIATE")
	private Integer withCouponsSirtexNegotiate;
	
	@Transient
	private String withCouponsSirtexNegotiateDesc;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "ISSUANCE_DATE_SIRTEX_NEGOTIATE")
	private Date issuanceDateSirtexNegotiate;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The legal address. */
	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;

	/** The mnemonic. */
	private String mnemonic;

	/** The phone number. */
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;

	/** The province. */
	private Integer province;

	/** The registry type. */
	@Column(name="REGISTRY_TYPE")
	private Integer registryType;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The geographic location. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "RESIDENCE_COUNTRY")
	private GeographicLocation geographicLocation;

	/** The social capital. */
	@Column(name="SOCIAL_CAPITAL")
	private BigDecimal socialCapital;

	/** The society type. */
	@Column(name="SOCIETY_TYPE")
	private Integer societyType;

	/** The state. */
	@Column(name="STATE_ISSUER")
	private Integer stateIssuer;

	/** The super resolution date. */
	@Temporal(TemporalType.DATE)
	@Column(name="SUPER_RESOLUTION_DATE")
	private Date superResolutionDate;

	/** The superintendent resolution. */
	@Column(name="SUPERINTENDENT_RESOLUTION")
	private String superintendentResolution;
	
	/** The website. */	
	private String website;
	
	/** The retirement date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "RETIREMENT_DATE")
	private Date retirementDate;
	
	/** The ind contract depository. */
	@Column(name = "IND_CONTRACT_DEPOSITORY")
	private Integer indContractDepository;

	@Transient
	private String departmentDescription;
	
	/** The province description. */
	@Transient
	private String provinceDescription;
	
	/** The district description. */
	@Transient
	private String districtDescription;
	
	/** The ind_differentiated. */
	@Column(name="IND_DIFFERENTIATED")
	private Integer indDifferentiated;
	
	@Column(name = "FUND_ADMINISTRATOR")
    private String fundAdministrator;
    
	@Column(name = "FUND_TYPE")
    private String fundType;
    
    @Column(name = "TRANSFER_NUMBER")
    private String transferNumber;
    
    @Column(name = "RELATED_CUI")
    private Long relatedCui;
    
    @Column(name="DOCUMENT_SOURCE")
	private Integer documentSource;

	/** The business name. */
	@Column(name = "RMV_REGISTER")
	private String rmvRegister;
	
	/** The expiration date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "CONSTITUTION_DATE")
	private Date constitutionDate;
    
	@Column(name="IND_SIRTEX_NEGOTIATE")
	private Integer indSirtexNegotiate;
	
	/** The offer type */
	@Column(name="OFFER_TYPE")
	private Integer offerType;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "SIRTEX_NEGOTIATE_DATE")
	private Date sirtexNegotiateDate;
	
	@Transient
	private String documentSourceDescription;
	@Transient
	private String investorTypeDescription;
	/**
	 * The Constructor.
	 */
	public IssuerHistory() {
	}
	
	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription(){
		if(Validations.validateIsNotNullAndPositive(currency)){
			if(Validations.validateIsNotNull(CurrencyType.get(currency))){
				return CurrencyType.get(currency).getValue();
			} else {
				return null;
			}
			
		} else {
			return null;
		}
	}
	
	/**
	 * Gets the ind stock exch enrolled description.
	 *
	 * @return the ind stock exch enrolled description
	 */
	public String getIndStockExchEnrolledDescription(){
		if(Validations.validateIsNotNull(indStockExchEnrolled)){
			return BooleanType.get(indStockExchEnrolled).getValue();
		}
		return null;
	}
	
	/**
	 * Gets the ind contract depository description.
	 *
	 * @return the ind contract depository description
	 */
	public String getIndContractDepositoryDescription(){
		if(Validations.validateIsNotNull(indContractDepository)){
			return BooleanType.get(indContractDepository).getValue();
		}
		return null;
	}
	
	/**
	 * Gets the state issuer description.
	 *
	 * @return the state issuer description
	 */
	public String getStateIssuerDescription(){
		if(Validations.validateIsNotNullAndPositive(stateIssuer)){
			if(Validations.validateIsNotNull(IssuerStateType.get(stateIssuer))){
				return IssuerStateType.get(stateIssuer).getValue();
			} else {
				return null;
			}
			
		} else {
			return null;
		}
	}
	
	/**
     * Gets the economic activity description.
     *
     * @return the economic activity description
     */
    public String getEconomicActivityDescription(){
    	if(Validations.validateIsNotNullAndPositive(economicActivity)){    		
    		if(Validations.validateIsNotNull(EconomicActivityType.get(economicActivity))){
				return EconomicActivityType.get(economicActivity).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the economic sector description.
     *
     * @return the economic sector description
     */
    public String getEconomicSectorDescription(){
    	if(Validations.validateIsNotNullAndPositive(economicSector)){
    		if(Validations.validateIsNotNull(EconomicSectorType.get(economicSector))){
				return EconomicSectorType.get(economicSector).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the economic sector description.
     *
     * @return the economic sector description
     */
    public String getOfferTypeDescription(){
    	if(Validations.validateIsNotNullAndPositive(offerType)){
    		if(Validations.validateIsNotNull(OfferType.get(offerType))){
				return OfferType.get(offerType).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the document type description.
     *
     * @return the document type description
     */
    public String getDocumentTypeDescription(){
    	if(Validations.validateIsNotNullAndPositive(documentType)){
    		if(Validations.validateIsNotNull(IssuerDocumentType.get(documentType))){
				return IssuerDocumentType.get(documentType).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
    
    /**
     * Gets the society type description.
     *
     * @return the society type description
     */
    public String getSocietyTypeDescription(){
    	if(Validations.validateIsNotNullAndPositive(societyType)){
    		if(Validations.validateIsNotNull(SocietyType.get(societyType))){
				return SocietyType.get(societyType).getValue();
			} else {
				return null;
			}
    	} else {
    		return null;
    	}
    }
	
	
	/**
	 * Gets the id issuer history pk.
	 *
	 * @return the id issuer history pk
	 */
	public Long getIdIssuerHistoryPk() {
		return idIssuerHistoryPk;
	}



	/**
	 * Sets the id issuer history pk.
	 *
	 * @param idIssuerHistoryPk the id issuer history pk
	 */
	public void setIdIssuerHistoryPk(Long idIssuerHistoryPk) {
		this.idIssuerHistoryPk = idIssuerHistoryPk;
	}



	/**
	 * Gets the business name.
	 *
	 * @return the business name
	 */
	public String getBusinessName() {
		return businessName;
	}



	/**
	 * Sets the business name.
	 *
	 * @param businessName the business name
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}



	/**
	 * Gets the closing service date.
	 *
	 * @return the closing service date
	 */
	public Date getClosingServiceDate() {
		return closingServiceDate;
	}



	/**
	 * Sets the closing service date.
	 *
	 * @param closingServiceDate the closing service date
	 */
	public void setClosingServiceDate(Date closingServiceDate) {
		this.closingServiceDate = closingServiceDate;
	}

	/**
	 * Gets the observation.
	 *
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}

	/**
	 * Sets the observation.
	 *
	 * @param observation the new observation
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

	/**
	 * Gets the contact email.
	 *
	 * @return the contact email
	 */
	public String getContactEmail() {
		return contactEmail;
	}



	/**
	 * Sets the contact email.
	 *
	 * @param contactEmail the contact email
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}



	/**
	 * Gets the contact name.
	 *
	 * @return the contact name
	 */
	public String getContactName() {
		return contactName;
	}



	/**
	 * Sets the contact name.
	 *
	 * @param contactName the contact name
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}



	/**
	 * Gets the contact phone.
	 *
	 * @return the contact phone
	 */
	public String getContactPhone() {
		return contactPhone;
	}



	/**
	 * Sets the contact phone.
	 *
	 * @param contactPhone the contact phone
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}



	/**
	 * Gets the contract date.
	 *
	 * @return the contract date
	 */
	public Date getContractDate() {
		return contractDate;
	}



	/**
	 * Sets the contract date.
	 *
	 * @param contractDate the contract date
	 */
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}



	/**
	 * Gets the contract number.
	 *
	 * @return the contract number
	 */
	public String getContractNumber() {
		return contractNumber;
	}



	/**
	 * Sets the contract number.
	 *
	 * @param contractNumber the contract number
	 */
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}



	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}



	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}



	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}



	/**
	 * Sets the currency.
	 *
	 * @param currency the currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}



	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public Integer getDepartment() {
		return department;
	}



	/**
	 * Sets the department.
	 *
	 * @param department the department
	 */
	public void setDepartment(Integer department) {
		this.department = department;
	}



	/**
	 * Gets the district.
	 *
	 * @return the district
	 */
	public Integer getDistrict() {
		return district;
	}



	/**
	 * Sets the district.
	 *
	 * @param district the district
	 */
	public void setDistrict(Integer district) {
		this.district = district;
	}



	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}



	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}



	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}



	/**
	 * Sets the document type.
	 *
	 * @param documentType the document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}



	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public Integer getEconomicActivity() {
		return economicActivity;
	}



	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}



	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return economicSector;
	}



	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}



	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}



	/**
	 * Sets the email.
	 *
	 * @param email the email
	 */
	public void setEmail(String email) {
		this.email = email;
	}



	/**
	 * Gets the enrolled stock exch date.
	 *
	 * @return the enrolled stock exch date
	 */
	public Date getEnrolledStockExchDate() {
		return enrolledStockExchDate;
	}



	/**
	 * Sets the enrolled stock exch date.
	 *
	 * @param enrolledStockExchDate the enrolled stock exch date
	 */
	public void setEnrolledStockExchDate(Date enrolledStockExchDate) {
		this.enrolledStockExchDate = enrolledStockExchDate;
	}



	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return faxNumber;
	}



	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}



	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}



	/**
	 * Sets the holder.
	 *
	 * @param holder the holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}



	/**
	 * Gets the issuer request.
	 *
	 * @return the issuer request
	 */
	public IssuerRequest getIssuerRequest() {
		return issuerRequest;
	}



	/**
	 * Sets the issuer request.
	 *
	 * @param issuerRequest the issuer request
	 */
	public void setIssuerRequest(IssuerRequest issuerRequest) {
		this.issuerRequest = issuerRequest;
	}



	/**
	 * Gets the ind stock exch enrolled.
	 *
	 * @return the ind stock exch enrolled
	 */
	public Integer getIndStockExchEnrolled() {
		return indStockExchEnrolled;
	}



	/**
	 * Sets the ind stock exch enrolled.
	 *
	 * @param indStockExchEnrolled the ind stock exch enrolled
	 */
	public void setIndStockExchEnrolled(Integer indStockExchEnrolled) {
		this.indStockExchEnrolled = indStockExchEnrolled;
	}



	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/**
	 * Gets the legal address.
	 *
	 * @return the legal address
	 */
	public String getLegalAddress() {
		return legalAddress;
	}



	/**
	 * Sets the legal address.
	 *
	 * @param legalAddress the legal address
	 */
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}



	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}



	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}



	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}



	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber the phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}



	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public Integer getProvince() {
		return province;
	}



	/**
	 * Sets the province.
	 *
	 * @param province the province
	 */
	public void setProvince(Integer province) {
		this.province = province;
	}



	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return registryType;
	}



	/**
	 * Sets the registry type.
	 *
	 * @param registryType the registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}



	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}



	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}



	/**
	 * Gets the geographic location.
	 *
	 * @return the geographic location
	 */
	public GeographicLocation getGeographicLocation() {
		return geographicLocation;
	}



	/**
	 * Sets the geographic location.
	 *
	 * @param geographicLocation the geographic location
	 */
	public void setGeographicLocation(GeographicLocation geographicLocation) {
		this.geographicLocation = geographicLocation;
	}



	/**
	 * Gets the social capital.
	 *
	 * @return the social capital
	 */
	public BigDecimal getSocialCapital() {
		return socialCapital;
	}



	/**
	 * Sets the social capital.
	 *
	 * @param socialCapital the social capital
	 */
	public void setSocialCapital(BigDecimal socialCapital) {
		this.socialCapital = socialCapital;
	}



	/**
	 * Gets the society type.
	 *
	 * @return the society type
	 */
	public Integer getSocietyType() {
		return societyType;
	}



	/**
	 * Sets the society type.
	 *
	 * @param societyType the society type
	 */
	public void setSocietyType(Integer societyType) {
		this.societyType = societyType;
	}



	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getStateIssuer() {
		return stateIssuer;
	}



	/**
	 * Sets the state.
	 *
	 * @param stateIssuer the new state issuer
	 */
	public void setStateIssuer(Integer stateIssuer) {
		this.stateIssuer = stateIssuer;
	}



	/**
	 * Gets the super resolution date.
	 *
	 * @return the super resolution date
	 */
	public Date getSuperResolutionDate() {
		return superResolutionDate;
	}



	/**
	 * Sets the super resolution date.
	 *
	 * @param superResolutionDate the super resolution date
	 */
	public void setSuperResolutionDate(Date superResolutionDate) {
		this.superResolutionDate = superResolutionDate;
	}



	/**
	 * Gets the superintendent resolution.
	 *
	 * @return the superintendent resolution
	 */
	public String getSuperintendentResolution() {
		return superintendentResolution;
	}

	/**
	 * Sets the superintendent resolution.
	 *
	 * @param superintendentResolution the superintendent resolution
	 */
	public void setSuperintendentResolution(String superintendentResolution) {
		this.superintendentResolution = superintendentResolution;
	}

	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * Sets the website.
	 *
	 * @param website the new website
	 */
	public void setWebsite(String website) {
		this.website = website;
	}		

	/**
	 * Gets the retirement date.
	 *
	 * @return the retirement date
	 */
	public Date getRetirementDate() {
		return retirementDate;
	}

	/**
	 * Sets the retirement date.
	 *
	 * @param retirementDate the new retirement date
	 */
	public void setRetirementDate(Date retirementDate) {
		this.retirementDate = retirementDate;
	}		

	/**
	 * Gets the ind contract depository.
	 *
	 * @return the ind contract depository
	 */
	public Integer getIndContractDepository() {
		return indContractDepository;
	}

	/**
	 * Sets the ind contract depository.
	 *
	 * @param indContractDepository the new ind contract depository
	 */
	public void setIndContractDepository(Integer indContractDepository) {
		this.indContractDepository = indContractDepository;
	}
	/**
	 * Gets the department description.
	 *
	 * @return the department description
	 */
	public String getDepartmentDescription() {
		return departmentDescription;
	}

	/**
	 * Sets the department description.
	 *
	 * @param departmentDescription the new department description
	 */
	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}
	/**
	 * Gets the province description.
	 *
	 * @return the province description
	 */
	public String getProvinceDescription() {
		return provinceDescription;
	}

	/**
	 * Sets the province description.
	 *
	 * @param provinceDescription the new province description
	 */
	public void setProvinceDescription(String provinceDescription) {
		this.provinceDescription = provinceDescription;
	}

	/**
	 * Gets the district description.
	 *
	 * @return the district description
	 */
	public String getDistrictDescription() {
		return districtDescription;
	}

	/**
	 * Sets the district description.
	 *
	 * @param districtDescription the new district description
	 */
	public void setDistrictDescription(String districtDescription) {
		this.districtDescription = districtDescription;
	}		

	
	public String getFundAdministrator() {
		return fundAdministrator;
	}

	public void setFundAdministrator(String fundAdministrator) {
		this.fundAdministrator = fundAdministrator;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public String getTransferNumber() {
		return transferNumber;
	}

	public void setTransferNumber(String transferNumber) {
		this.transferNumber = transferNumber;
	}

	public Long getRelatedCui() {
		return relatedCui;
	}

	public void setRelatedCui(Long relatedCui) {
		this.relatedCui = relatedCui;
	}
	
	public Integer getDocumentSource() {
		return documentSource;
	}

	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}

	/**
	 * Gets the ind differentiated.
	 *
	 * @return the ind differentiated
	 */
	public Integer getIndDifferentiated() {
		return indDifferentiated;
	}

	/**
	 * Sets the ind differentiated.
	 *
	 * @param indDifferentiated the new ind differentiated
	 */
	public void setIndDifferentiated(Integer indDifferentiated) {
		this.indDifferentiated = indDifferentiated;
	}
	
	

	public String getDocumentSourceDescription() {
		return documentSourceDescription;
	}

	public void setDocumentSourceDescription(String documentSourceDescription) {
		this.documentSourceDescription = documentSourceDescription;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        return null;
	}

	public Integer getCreditRatingScales() {
		return creditRatingScales;
	}

	public void setCreditRatingScales(Integer creditRatingScales) {
		this.creditRatingScales = creditRatingScales;
	}

	public String getRmvRegister() {
		return rmvRegister;
	}

	public void setRmvRegister(String rmvRegister) {
		this.rmvRegister = rmvRegister;
	}

	public Date getConstitutionDate() {
		return constitutionDate;
	}

	public void setConstitutionDate(Date constitutionDate) {
		this.constitutionDate = constitutionDate;
	}


    public Integer getIndSirtexNegotiate() {
		return indSirtexNegotiate;
	}
    

	public void setIndSirtexNegotiate(Integer indSirtexNegotiate) {
		this.indSirtexNegotiate = indSirtexNegotiate;
	}

	public Date getSirtexNegotiateDate() {
		return sirtexNegotiateDate;
	}

	public void setSirtexNegotiateDate(Date sirtexNegotiateDate) {
		this.sirtexNegotiateDate = sirtexNegotiateDate;
	}

	public Integer getOfferType() {
		return offerType;
	}

	public void setOfferType(Integer offerType) {
		this.offerType = offerType;
	}

	public Integer getSecurityClassSirtexNegotiate() {
		return securityClassSirtexNegotiate;
	}

	public void setSecurityClassSirtexNegotiate(Integer securityClassSirtexNegotiate) {
		this.securityClassSirtexNegotiate = securityClassSirtexNegotiate;
	}

	public Integer getCurrencySirtexNegotiate() {
		return currencySirtexNegotiate;
	}

	public void setCurrencySirtexNegotiate(Integer currencySirtexNegotiate) {
		this.currencySirtexNegotiate = currencySirtexNegotiate;
	}

	public Integer getPersonTypeSirtexNegotiate() {
		return personTypeSirtexNegotiate;
	}

	public void setPersonTypeSirtexNegotiate(Integer personTypeSirtexNegotiate) {
		this.personTypeSirtexNegotiate = personTypeSirtexNegotiate;
	}

	public Integer getWithCouponsSirtexNegotiate() {
		return withCouponsSirtexNegotiate;
	}

	public void setWithCouponsSirtexNegotiate(Integer withCouponsSirtexNegotiate) {
		this.withCouponsSirtexNegotiate = withCouponsSirtexNegotiate;
	}

	public Date getIssuanceDateSirtexNegotiate() {
		return issuanceDateSirtexNegotiate;
	}

	public void setIssuanceDateSirtexNegotiate(Date issuanceDateSirtexNegotiate) {
		this.issuanceDateSirtexNegotiate = issuanceDateSirtexNegotiate;
	}

	public String getSecurityClassSirtexNegotiateDesc() {
		return securityClassSirtexNegotiateDesc;
	}

	public void setSecurityClassSirtexNegotiateDesc(
			String securityClassSirtexNegotiateDesc) {
		this.securityClassSirtexNegotiateDesc = securityClassSirtexNegotiateDesc;
	}

	public String getCurrencySirtexNegotiateDesc() {
		return currencySirtexNegotiateDesc;
	}

	public void setCurrencySirtexNegotiateDesc(String currencySirtexNegotiateDesc) {
		this.currencySirtexNegotiateDesc = currencySirtexNegotiateDesc;
	}

	public String getPersonTypeSirtexNegotiateDesc() {
		return personTypeSirtexNegotiateDesc;
	}

	public void setPersonTypeSirtexNegotiateDesc(
			String personTypeSirtexNegotiateDesc) {
		this.personTypeSirtexNegotiateDesc = personTypeSirtexNegotiateDesc;
	}

	public String getWithCouponsSirtexNegotiateDesc() {
		return withCouponsSirtexNegotiateDesc;
	}

	public void setWithCouponsSirtexNegotiateDesc(
			String withCouponsSirtexNegotiateDesc) {
		this.withCouponsSirtexNegotiateDesc = withCouponsSirtexNegotiateDesc;
	}

	public String getIndSirtexNegotiateDesc() {
		return indSirtexNegotiateDesc;
	}

	public void setIndSirtexNegotiateDesc(String indSirtexNegotiateDesc) {
		this.indSirtexNegotiateDesc = indSirtexNegotiateDesc;
	}

	public Integer getInvestorType() {
		return investorType;
	}

	public void setInvestorType(Integer investorType) {
		this.investorType = investorType;
	}

	public String getInvestorTypeDescription() {
		return investorTypeDescription;
	}

	public void setInvestorTypeDescription(String investorTypeDescription) {
		this.investorTypeDescription = investorTypeDescription;
	}
	
	
}