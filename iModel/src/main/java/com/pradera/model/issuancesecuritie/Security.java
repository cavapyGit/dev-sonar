package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.LazyInitializationException;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.contextholder.LoggerUserConstants;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.corporatives.CustodyCommissionDetail;
import com.pradera.model.corporatives.stockcalculation.validation.StockBySecurity;
import com.pradera.model.corporatives.type.CorporativeSecuritySourceType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.guarantees.SecurityGvcList;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.type.AmortizationType;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.CalendarType;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.InterestClassType;
import com.pradera.model.issuancesecuritie.type.InterestPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecuritySourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityTermType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorSecurityCode;




// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SECURITY database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
@Entity
@NamedQueries({
	@NamedQuery(name = Security.SECURITY_STATE,query = "SELECT new com.pradera.model.issuancesecuritie.Security(s.idSecurityCodePk, s.stateSecurity) FROM Security s WHERE s.idSecurityCodePk = :idSecurityCodePkParam"),
	@NamedQuery(name = Security.SECURITIES_BY_ISSUANCE,	query = "SELECT count(s.idSecurityCodePk) FROM Security s WHERE s.issuance.idIssuanceCodePk = :idIssuanceCodePrm"),
	@NamedQuery(name = Security.SECURITIE_SINGLE_DATA,	query = "select new com.pradera.model.issuancesecuritie.Security(s.idSecurityBcbCode, s.paymentAgent, s.instrumentType, s.securityClass, s.stateSecurity ) from Security s where s.idSecurityCodePk = :idSecurityCodePkParam")
})
@Table(name = "SECURITY")
public class Security implements Serializable, Auditable, Cloneable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant SECURITY_STATE. */
	public static final String SECURITY_STATE="security.searchState";
	
	public static final String SECURITIES_BY_ISSUANCE="security.securitiesByIssuance";
	
	public static final String SECURITIE_SINGLE_DATA="security.securitie.single.data";

	/** The id isin code pk. */
	@Id
	@Column(name = "ID_SECURITY_CODE_PK")
	@NotNull(groups={StockBySecurity.class})
	private String idSecurityCodePk;
	
	/** The id security code only. */
	@Column(name="ID_SECURITY_CODE_ONLY")
	private String idSecurityCodeOnly;
	
	@Column(name="ID_SECURITY_BCB_CODE")
	private String idSecurityBcbCode;
	
	/** The id isin code. */
	@Column(name = "ID_ISIN_CODE")
	private String idIsinCode;

	/** The alternative code. */
	@Column(name = "ALTERNATIVE_CODE")
	private String alternativeCode;

	/** The amortization amount. */
	@Column(name = "AMORTIZATION_AMOUNT")
	private BigDecimal amortizationAmount;

	/** The amortization factor. */
	@Column(name = "AMORTIZATION_FACTOR")
	private BigDecimal amortizationFactor;

	/** The amortization type. */
	@Column(name = "AMORTIZATION_TYPE")
	private Integer amortizationType;

	/** The amortization periodicity. */
	@Column(name="AMORTIZATION_PERIODICITY")
	private Integer amortizationPeriodicity;

//	/** The calendar days. */
	/** The calendar days. */
	@Column(name = "CALENDAR_DAYS")
	private Integer calendarDays;

	/** The calendar type. */
	@Column(name="CALENDAR_TYPE")
	private Integer calendarType;
	
	/** The month type. */
	@Column(name="CALENDAR_MONTH")
	private Integer calendarMonth;

	/** The cfi code. */
	@Column(name = "CFI_CODE")
	private String cfiCode;
	
	/** The Short cfi code. */
	@Column(name = "FISN_CODE")
	private String fsinCode;
	
	/** The bol cfi code. */
	@Transient
	private boolean bolCfiCode;

	/** The circulation amount. */
	@Column(name = "CIRCULATION_AMOUNT")
	private BigDecimal circulationAmount;

	/** The circulation balance. */
	@Column(name = "CIRCULATION_BALANCE")
	private BigDecimal circulationBalance;

	/** The corporative process days. */
	@Column(name = "CORPORATIVE_PROCESS_DAYS")
	@NotNull
	private Integer corporativeProcessDays;

	/** The currency. */
	@Column(name="CURRENCY")
	private Integer currency;

	/** The amortization on. */
	@Column(name="AMORTIZATION_ON")
	private Integer amortizationOn;

	/** The currency name. */
	@Transient
	private String currencyName;

	/** The current nominal value. */
	@Column(name = "CURRENT_NOMINAL_VALUE")
	private BigDecimal currentNominalValue;

	/** The deposit registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DEPOSIT_REGISTRY_DATE")
	private Date depositRegistryDate;

	/** The description. */
	@Column(name = "DESCRIPTION")
	private String description;

	/** The desmaterialized balance. */
	@Column(name = "DESMATERIALIZED_BALANCE")
	private BigDecimal desmaterializedBalance;

	/** The expiration date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRATION_DATE")
	private Date expirationDate;

	/** The financial index. */
	@Column(name = "FINANCIAL_INDEX")
	private Integer financialIndex;

	/** The id group fk. */
	@Column(name = "ID_GROUP_FK")
	private Integer idGroupFk;

	/** The ind amortization type. 
	@Column(name = "IND_AMORTIZATION_TYPE")
	private Integer indAmortizationType;
	*/

	/** The ind convertible stock. */
	@Column(name = "IND_CONVERTIBLE_STOCK")
	@NotNull
	private Integer indConvertibleStock;

	/** The ind early redemption. */
	@Column(name = "IND_EARLY_REDEMPTION")
	@NotNull
	private Integer indEarlyRedemption;

	/** The ind is coupon. */
	@Column(name = "IND_IS_COUPON")
	@NotNull
	private Integer indIsCoupon;
	
	/** The ind is detached. */
	@Column(name = "IND_IS_DETACHED")
	@NotNull
	private Integer indIsDetached;
	
	/** The ind is detached. */
	@Column(name = "IND_AUTHORIZED")
	private Integer indAuthorized;
	
	/** The registry date. */
	@Column(name="AUTHORIZED_DATE")
	private Date authorizedDate;
	
	/** The registry user. */
	@Column(name = "AUTHORIZED_USER")
	private String authorizedUser;
	
	/** The authorized name. */
	@Transient
	private String authorizedName;
	
	/** The ind extended term. */
	@Column(name="IND_EXTENDED_TERM")
	@NotNull
	private Integer indExtendedTerm;
	
	/** The ind is fractionable. */
	@Column(name = "IND_IS_FRACTIONABLE")
	@NotNull
	private Integer indIsFractionable;

	/** The ind issuance management. */
	@Column(name = "IND_ISSUANCE_MANAGEMENT")
	@NotNull
	private Integer indIssuanceManagement;

//	/** The ind materializable. */
//	@Column(name = "IND_MATERIALIZABLE")
//	@NotNull
//	private Integer indMaterializable;

	/** The ind payment benefit. */
	@Column(name = "IND_PAYMENT_BENEFIT")
	@NotNull
	private Integer indPaymentBenefit;

	/** The ind payment reporting. */
	@Column(name = "IND_PAYMENT_REPORTING")
	private Integer indPaymentReporting;
	
	/** The ind receivable custody. */
	@Column(name = "IND_RECEIVABLE_CUSTODY")
	@NotNull
	private Integer indReceivableCustody;

	/** The ind securitization. */
	@Column(name = "IND_SECURITIZATION")
	@NotNull
	private Integer indSecuritization;

	/** The ind split coupon. */
	@Column(name = "IND_SPLIT_COUPON")
	@NotNull
	private Integer indSplitCoupon;

	/** The ind trader secondary. */
	@Column(name = "IND_TRADER_SECONDARY")
	@NotNull
	private Integer indTraderSecondary;

	/** The ind capitalizable interest. */
	@Column(name="IND_CAPITALIZABLE_INTEREST")
	@NotNull
	private Integer indCapitalizableInterest;

	/** The security months term. */
	@Column(name="SECURITY_MONTHS_TERM")
	private Integer securityMonthsTerm;
	
	/** The security days term. */
	@Column(name="SECURITY_DAYS_TERM")
	private Integer securityDaysTerm; 

	/** The capital payment modality. */
	@Column(name="CAPITAL_PAYMENT_MODALITY")
	private Integer capitalPaymentModality;

	/** The initial nominal value. */
	@NotNull
	@Column(name = "INITIAL_NOMINAL_VALUE")
	private BigDecimal initialNominalValue;

	/** The instrument type. */
	@Column(name = "INSTRUMENT_TYPE")
	private Integer instrumentType;

	/** The interest factor. */
	@Column(name = "INTEREST_FACTOR")
	private BigDecimal interestFactor;

	/** The interest payment modality. */
	@Column(name="INTEREST_PAYMENT_MODALITY")
	private Integer interestPaymentModality;

	/** The interest rate. */
	@Column(name = "INTEREST_RATE")
	private BigDecimal interestRate;

	@Column(name = "DISCOUNT_RATE")
	private BigDecimal discountRate;
	
	@Column(name = "YIELD_RATE")
	private BigDecimal yieldRate;

	/** The interest rate periodicity. */
	@Column(name = "INTEREST_RATE_PERIODICITY")
	private Integer interestRatePeriodicity;

	/** The interest type. */
	@Column(name = "INTEREST_TYPE")
	private Integer interestType;

	/** The issuance country. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ISSUANCE_COUNTRY")
	private GeographicLocation issuanceCountry;
	
	/** The issuance date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ISSUANCE_DATE")
	private Date issuanceDate;

	/** The issuance form. */
	@Column(name = "ISSUANCE_FORM")
	private Integer issuanceForm;

	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The number coupons. */
	@Column(name="NUMBER_COUPONS")
	private Integer numberCoupons;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The maximum invesment. */
	@Column(name = "MAXIMUM_INVESMENT")
	private BigDecimal maximumInvesment;

	/** The minimum invesment. */
	@Column(name = "MINIMUM_INVESMENT")
	private BigDecimal minimumInvesment;

	/** The minimum invesment. */
	@Column(name = "QUANTITY_BCB")
	private BigDecimal quantityBcb;
	
	/** The minimum invesment. */
	@Column(name = "ISSUANCE_QUANTITY_SEC_BCB")
	private BigDecimal issuanceQuantitySecBcb;

	/** The minium rate. */
	@Column(name="MINIMUM_RATE")
	private BigDecimal minimumRate;

	/** The maximum rate. */
	@Column(name="MAXIMUM_RATE")
	private BigDecimal maximumRate;

	/** The mnemonic. */
	private String mnemonic;
	
	/** The periodicity days. */
	@Column(name="PERIODICITY_DAYS")
	private Integer periodicityDays;
	
	/** The amortization periodicity days. */
	@Column(name="AMORTIZATION_PERIODICITY_DAYS")
	private Integer amortizationPeriodicityDays;

	/** The periodicity. */

	private Integer periodicity;

	/** The physical balance. */
	@Column(name = "PHYSICAL_BALANCE")
	private BigDecimal physicalBalance;

	/** The placed amount. */
	@Column(name = "PLACED_AMOUNT")
	private BigDecimal placedAmount;

	/** The placed balance. */
	@Column(name = "PLACED_BALANCE")
	private BigDecimal placedBalance;

	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registryUser;

	/** The retirement date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RETIREMENT_DATE")
	private Date retirementDate;

	/** The security class. */
	@Column(name = "SECURITY_CLASS")
	private Integer securityClass;

	/** The security serial. */
	@Column(name = "SECURITY_SERIAL")
	private String securitySerial;

	/** The security source. */
	@Column(name = "SECURITY_SOURCE")
	@NotNull
	private Integer securitySource;

	/** The security type. */
	@Column(name = "SECURITY_TYPE")
	private Integer securityType;

	/** The serial program. */
	@Column(name = "SERIAL_PROGRAM")
	private String serialProgram;

	/** The share balance. */
	@Column(name = "SHARE_BALANCE")

	private BigDecimal shareBalance;

	/** The share capital. */
	@Column(name = "SHARE_CAPITAL")

	private BigDecimal shareCapital;

	/** The state security. */
	@Column(name = "STATE_SECURITY")
	private Integer stateSecurity;

	/** The stock registry days. */
	@Column(name = "STOCK_REGISTRY_DAYS")
	private Integer stockRegistryDays;

	/** The registry date. */
	@Column(name="REGISTRY_DATE")
	@NotNull
	private Date registryDate;

	/** The yield. */
	private Integer yield;

	/** The spread. */
	private BigDecimal spread;

	/** The indexed. */
	private Integer indexed;

	/** The coupon first payment coupons. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="COUPON_FIRST_EXPIRATION_DATE")
	private Date couponFirstExpirationDate;

	/** The payment first expiration date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PAYMENT_FIRST_EXPIRATION_DATE")
	private Date paymentFirstExpirationDate;

	/** The cash nominal. */
	@Column(name="CASH_NOMINAL")
	private Integer cashNominal;

	/** The rate type. */
	@Column(name="RATE_TYPE")
	private Integer rateType;
	
	/** The rate value. */
	@Column(name="RATE_VALUE")
	private BigDecimal rateValue;

	/** The operation type. */
	@Column(name="OPERATION_TYPE")
	private Integer operationType;
	
	/** The security term. */
	@Column(name="SECURITY_TERM")
	private Integer securityTerm;

	/** The inscription date. */
	@Column(name="INSCRIPTION_DATE")
	private Date inscriptionDate;
	
	/** The ind has split securities. */
	@Column(name="IND_HAS_SPLIT_SECURITIES")
	@NotNull
	private Integer indHasSplitSecurities; //if the parent securities has split child securities
	
	/** The ind tax exempt. */
	@Column(name="IND_TAX_EXEMPT")
	@NotNull
	private Integer indTaxExempt;
	
	@Column(name="IND_PREPAID")
	private Integer indPrepaid;
	
	/** The ind holder detail. */
	@Column(name="IND_HOLDER_DETAIL")
	@NotNull
	private Integer indHolderDetail;
	
	/** The ind subordinated. */
	@Column(name="IND_SUBORDINATED")
	@NotNull
	private Integer indSubordinated;
	
	/** The ind subordinated. */
	@Column(name="IND_ISSUANCE_PROTEST")
	private Integer indIssuanceProtest;
	
	@Column(name="ID_SECURITY_CLASS_TRG")
	private Integer idSecurityClassTrg;

	@Column(name="CONVERTIBLE_STOCK_TYPE")
	private Integer convertibleStockType;
	
	/** The payment agent. */
	@Column(name="PAYMENT_AGENT")
	private String paymentAgent;
	
	/** The payment requirement. */
	@Column(name="PAYMENT_REQUIREMENT")
	private String paymentRequirement;
	
	/** The physical deposit balance. */
	@Column(name="PHYSICAL_DEPOSIT_BALANCE")
	private BigDecimal physicalDepositBalance;
	
	/** The id source security code. */
	@Column(name = "ID_SOURCE_SECURITY_CODE_PK")
	private String idSourceSecurityCodePk;
	
	/** The passive interest rate. */
	@Column(name = "PASSIVE_INTEREST_RATE")
	private BigDecimal passiveInterestRate;
	
	/** The id source security code. */
	@Column(name = "OBSERVATIONS")
	private String observations;
	
	/** The not traded. */
	@Column(name="NOT_TRADED")
	private Integer notTraded;
	
	/** The id fraction security code. */
	@Column(name = "ID_FRACTION_SECURITY_CODE_PK")
	private String idFractionSecurityCodePk;
	
	// bi-directional many-to-one association to Issuance
	/** The issuance. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_ISSUANCE_CODE_FK")
	private Issuance issuance;

	// bi-directional many-to-one association to Issuer
	/** The issuer. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_SECURITIES_MANAGER_FK")
	private SecuritiesManager securitiesManager;
	
	/** The valuator process class. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALUATOR_CLASS_FK")
	private ValuatorProcessClass valuatorProcessClass;
	
	@Column(name="ID_GEOGRAPHIC_LOCATION_FK")
	private Integer geographicLocation;
	
	/** The id isin code. */
	@Column(name = "PLACE_PAYMENT")
	private String placePayment;
	
	/** Issue 1325. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRATION_FONDO_DATE")
	private Date expirationFondoDate;

	// bi-directional many-to-one association to Security
	/** The security. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_REF_SECURITY_CODE_FK")
	private Security security;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_PREPAID_SECURITY_CODE_FK")
	private Security securityOrigin;
	
	/** The id subproduct ref fk. */
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_SUBPRODUCT_REF_FK")
	private Security  idSubproductRefFk;

	@Column(name="IND_ELECTRONIC_CUPON")
	private Integer indElectronicCupon = 1;//por defecto que sus cupones sean electronicos
	
	/** The ind subordinated. */
	@Column(name="represents_number_votes")
	private Integer representsNumberVotes;

	@Column(name="REFERENCE_PART_CODE")
	private String referencePartCode;

	@Column(name="IND_IS_MANAGED")
	private Integer indIsManaged;
	
	// bi-directional many-to-one association to Security
	/** The securities. */
	@OneToMany(mappedBy = "security",fetch=FetchType.LAZY)
	private List<Security> securities;

	// bi-directional many-to-one association to SecurityForeignDepository
	/** The security foreign depositories. */
	@OneToMany(mappedBy = "security",cascade=CascadeType.ALL)
	private List<SecurityForeignDepository> securityForeignDepositories;

	// bi-directional many-to-one association to SecurityHistoryBalance
	/** The security history balances. */
	@OneToMany(mappedBy = "security")
	private List<SecurityHistoryBalance> securityHistoryBalances;

	// bi-directional many-to-one association to SecurityHistoryState
	/** The security history states. */
	@OneToMany(mappedBy = "security")
	private List<SecurityHistoryState> securityHistoryStates;
	
	//bi-directional many-to-one association to SecurityNegotiationMechanism
	/** The security negotiation mechanisms. */
	@OneToMany(mappedBy="security",cascade=CascadeType.ALL)
	private List<SecurityNegotiationMechanism> securityNegotiationMechanisms;

	/** The holder account balances. */
	@OneToMany(mappedBy="security",cascade=CascadeType.ALL)
	private List<HolderAccountBalance> holderAccountBalances;

	/** The placement segment detail list. */
	@OneToMany(mappedBy="security")
	private List<PlacementSegmentDetail> placementSegmentDetailList;

	/** The custody commission detail list. */
	@OneToMany(mappedBy="security")
	private List<CustodyCommissionDetail> custodyCommissionDetailList;

	/** The interest payment schedule. */
	@LazyToOne(LazyToOneOption.NO_PROXY)
	@OneToOne(mappedBy="security",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private InterestPaymentSchedule interestPaymentSchedule;
	
	/** The amortization payment schedule. */
	@LazyToOne(LazyToOneOption.NO_PROXY)
	@OneToOne(mappedBy="security",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private AmortizationPaymentSchedule amortizationPaymentSchedule;
	
	//@NotNull
	/** The security investor. */
	@LazyToOne(LazyToOneOption.NO_PROXY)
	@OneToOne(mappedBy="security",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private SecurityInvestor securityInvestor;
	
	/** The security gvc lists. */
	@OneToMany(mappedBy="security")
	private List<SecurityGvcList> securityGvcLists;
	
	/** The list valuator security code. */
	@OneToMany(mappedBy="security")
	private List<ValuatorSecurityCode> listValuatorSecurityCode;

	@LazyToOne(LazyToOneOption.NO_PROXY)
	@OneToMany(mappedBy="security",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<SecuritySerialRange> securitySerialRange;
	
	/** The id source security code. */
	@Column(name = "SUB_CLASS")
	private String subClass;
	
	/** The id source security code. */
	@Column(name = "number_share_initial")
	private Integer numberShareInitial;
	
	/** The id source security code. */
	@Column(name = "number_share_final")
	private Integer numberShareFinal;

	@Column(name = "ENCODER_ANGENT")
	private Integer encoderAgent;	
	
	/** The selected. */
	@Transient
	private boolean selected;

	/** The instrument type description. */
	@Transient
	private String instrumentTypeDescription;

	/** The class type description. */
	@Transient
	private String classTypeDescription;
	
	/** The security type description. */
	@Transient
	private String securityTypeDescription;
	
	/** The currency type description. */
	@Transient
	private String currencyTypeDescription;

	/** The interest type description. */
	@Transient
	private String interestTypeDescription;
	
	/** The corporative source type. */
	@Transient
	private Integer corporativeSourceType;
	
	/** The corporative source type description. */
	@Transient
	private String corporativeSourceTypeDescription; 
	
	/** The in negotiations. */
	@Transient
	private boolean inNegotiations;

	/** The security class desc. */
	@Transient
	private String securityClassDesc;
	
	/** The periodicity desc. */
	@Transient
	private String periodicityDesc;
	
	/** The has chronogram. */
	@Transient
	private String hasChronogram;
	
	/** The interest payment modality desc. */
	@Transient
	private String interestPaymentModalityDesc;
	
	/** The in bank stock exchange. */
	@Transient
	private boolean inBankStockExchange;
	
	/** The available physical balance. */
	@Transient
	private BigDecimal availablePhysicalBalance;
	
	/** The economic activity. */
	@Transient
	private Integer economicActivity;
	
	/** The exchange rela target security. */
	@Transient
	private BigDecimal exchangeRelaTargetSecurity;

	/** The exchange rela source security. */
	@Transient
	private BigDecimal exchangeRelaSourceSecurity;
	
	@Transient
	private BigDecimal deliveryFactorForCorporateOperation;
	
	@Transient
	private BigDecimal newShareCapital;
	
	@Transient
	private BigDecimal newShareBalance;
	
	@Transient
	private BigDecimal newCirculationBalance;
	
	@Transient
	private BigDecimal variationAmount;
	
	@Transient
	private BigDecimal physicalAvailable;
	
	@Transient
	private Date marketDate;
    
	@Transient
	private BigDecimal marketPrice;
	
	@Transient
	private Long pendientDays;
	
	@Transient
	private String negociable;

	@Transient
	private Integer motiveAnnotation;
	
	@Transient
	private String serialRange;

	@Transient
	private Boolean modifyIssuance = Boolean.TRUE;

	@Transient
	private Boolean stateExpirate = Boolean.FALSE;
	/**
	 * Instantiates a new security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param idIsinCode the id isin code
	 * @param cfiCode the cfi code
	 * @param alternativeCode the alternative code
	 * @param mnemonic the mnemonic
	 * @param description the description
	 * @param instrumentType the instrument type
	 * @param securityClass the security class
	 * @param securityType the security type
	 * @param securitySerial the security serial
	 * @param issuanceDate the issuance date
	 * @param expirationDate the expiration date
	 * @param currency the currency
	 * @param currentNominalValue the current nominal value
	 * @param interestType the interest type
	 * @param interestRate the interest rate
	 * @param idIssuerCode the id issuer code
	 * @param bussinessName the bussiness name
	 * @param shareBalance the share balance
	 * @param shareCapital the share capital
	 * @param circulationBalance the circulation balance
	 * @param stateSecurity the state security
	 * @param indConvertibleStock the ind convertible stock
	 * @param indTaxExempt the ind tax exempt
	 * @param issuanceId the issuance id
	 * @param issuerMnemonic the issuer mnemonic
	 */
	public Security(String idSecurityCodePk,String idIsinCode, String cfiCode, String alternativeCode,
			String mnemonic, String description, Integer instrumentType, 
			Integer securityClass, Integer securityType, String securitySerial,
			Date issuanceDate, Date expirationDate, Integer currency, BigDecimal currentNominalValue,  
			Integer interestType, BigDecimal interestRate, String idIssuerCode, String bussinessName, BigDecimal  shareBalance,
			BigDecimal shareCapital, BigDecimal circulationBalance, Integer stateSecurity, Integer indConvertibleStock,Integer indTaxExempt, String issuanceId,
			String issuerMnemonic
			) {
		this.idSecurityCodePk = idSecurityCodePk;
		this.idIsinCode = idIsinCode;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.currentNominalValue = currentNominalValue;
		this.description = description;
		this.expirationDate = expirationDate;
		this.instrumentType = instrumentType;
		this.interestRate = interestRate;
		this.interestType = interestType;
		this.issuanceDate = issuanceDate;
		this.mnemonic = mnemonic;
		this.securityClass = securityClass;
		this.securityType = securityType;
		this.securitySerial = securitySerial;
		this.currency = currency;
		this.shareCapital=shareCapital;
		this.shareBalance=shareBalance;
		this.circulationBalance=circulationBalance;
		this.stateSecurity=stateSecurity;
		this.issuer = new Issuer(idIssuerCode, bussinessName,issuerMnemonic);
		this.indConvertibleStock = indConvertibleStock;
		this.indTaxExempt=indTaxExempt;
		this.issuance = new Issuance(issuanceId);
	}
	
	/**
	 * Datos simple de un valor
	 * */
	public Security(String idSecurityBcbCode, String paymentAgent, Integer instrumentType, Integer securityClass, Integer stateSecurity) {
		super();
		this.idSecurityBcbCode = idSecurityBcbCode;
		this.paymentAgent = paymentAgent;
		this.instrumentType = instrumentType;
		this.securityClass = securityClass;
		this.stateSecurity = stateSecurity;
	}


	/**
	 * Instantiates a new security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param idIsinCode the id isin code
	 * @param alternativeCode the alternative code
	 * @param cfiCode the cfi code
	 * @param description the description
	 * @param mnemonic the mnemonic
	 * @param securityType the security type
	 * @param securityClass the security class
	 * @param stateSecurity the state security
	 * @param initialNominalValue the initial nominal value
	 * @param currentNominalValue the current nominal value
	 * @param interestRate the interest rate
	 * @param issuanceDate the issuance date
	 * @param securityDaysTerm the security days term
	 */
	public Security(String idSecurityCodePk, String idIsinCode, String alternativeCode,
			String cfiCode, String description, String mnemonic,
			Integer securityType, Integer securityClass, Integer stateSecurity,
			BigDecimal initialNominalValue, BigDecimal currentNominalValue,
			BigDecimal interestRate, Date issuanceDate, Integer securityDaysTerm) {
		super();
		this.idSecurityCodePk=idSecurityCodePk;
		this.idIsinCode = idIsinCode;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.description = description;
		this.mnemonic = mnemonic;
		this.securityType = securityType;
		this.securityClass=securityClass;
		this.stateSecurity = stateSecurity;
		this.initialNominalValue=initialNominalValue;
		this.currentNominalValue=currentNominalValue;
		this.interestRate=interestRate;
		this.issuanceDate=issuanceDate;
		this.securityDaysTerm=securityDaysTerm;
	}

	/**
	 * Instantiates a new security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param idIsinCode the id isin code
	 * @param alternativeCode the alternative code
	 * @param cfiCode the cfi code
	 * @param description the description
	 * @param mnemonic the mnemonic
	 * @param securityType the security type
	 * @param securityClass the security class
	 * @param stateSecurity the state security
	 * @param initialNominalValue the initial nominal value
	 * @param currentNominalValue the current nominal value
	 * @param interestRate the interest rate
	 * @param issuanceDate the issuance date
	 * @param securityDaysTerm the security days term
	 * @param currency the currency
	 */
	public Security(String idSecurityCodePk, String idIsinCode, String alternativeCode,
			String cfiCode, String description, String mnemonic,
			Integer securityType, Integer securityClass, Integer stateSecurity,
			BigDecimal initialNominalValue, BigDecimal currentNominalValue,
			BigDecimal interestRate, Date issuanceDate, Integer securityDaysTerm, Integer currency,
			BigDecimal desmaterializedBalance, Date expirationDate) {
		super();
		this.idSecurityCodePk=idSecurityCodePk;
		this.idIsinCode = idIsinCode;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.description = description;
		this.mnemonic = mnemonic;
		this.securityType = securityType;
		this.securityClass=securityClass;
		this.stateSecurity = stateSecurity;
		this.initialNominalValue=initialNominalValue;
		this.currentNominalValue=currentNominalValue;
		this.interestRate=interestRate;
		this.issuanceDate=issuanceDate;
		this.securityDaysTerm=securityDaysTerm;
		this.currency=currency;
		this.desmaterializedBalance=desmaterializedBalance;
		this.expirationDate=expirationDate;
	}
	
	
	/**
	 * Instantiates a new security.
	 * 
	 * @param idSecurityCodePk
	 * @param idIsinCode
	 * @param alternativeCode
	 * @param cfiCode
	 * @param description
	 * @param mnemonic
	 * @param securityType
	 * @param securityClass
	 * @param stateSecurity
	 * @param initialNominalValue
	 * @param currentNominalValue
	 * @param interestRate
	 * @param issuanceDate
	 * @param securityDaysTerm
	 * @param currency
	 * @param desmaterializedBalance
	 * @param expirationDate
	 * @param numberCoupons
	 */
	public Security(String idSecurityCodePk, String idIsinCode, String alternativeCode,
			String cfiCode, String description, String mnemonic,
			Integer securityType, Integer securityClass, Integer stateSecurity,
			BigDecimal initialNominalValue, BigDecimal currentNominalValue,
			BigDecimal interestRate, Date issuanceDate, Integer securityDaysTerm, Integer currency,
			BigDecimal desmaterializedBalance, Date expirationDate, Integer numberCoupons) {
		
		this.idSecurityCodePk=idSecurityCodePk;
		this.idIsinCode = idIsinCode;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.description = description;
		this.mnemonic = mnemonic;
		this.securityType = securityType;
		this.securityClass=securityClass;
		this.stateSecurity = stateSecurity;
		this.initialNominalValue=initialNominalValue;
		this.currentNominalValue=currentNominalValue;
		this.interestRate=interestRate;
		this.issuanceDate=issuanceDate;
		this.securityDaysTerm=securityDaysTerm;
		this.currency=currency;
		this.desmaterializedBalance=desmaterializedBalance;
		this.expirationDate=expirationDate;
		this.numberCoupons=numberCoupons;
	}
	
	public Security(String idSecurityCodePk, String idIsinCode, String alternativeCode,
			String cfiCode, String description, String mnemonic,
			Integer securityType, Integer securityClass, Integer stateSecurity,
			BigDecimal initialNominalValue, BigDecimal currentNominalValue,
			BigDecimal interestRate, Date issuanceDate, Integer securityDaysTerm, Integer currency,
			BigDecimal desmaterializedBalance, Date expirationDate, Integer numberCoupons,String registryUser, Integer indAuthorized) {
		
		this.idSecurityCodePk=idSecurityCodePk;
		this.idIsinCode = idIsinCode;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.description = description;
		this.mnemonic = mnemonic;
		this.securityType = securityType;
		this.securityClass=securityClass;
		this.stateSecurity = stateSecurity;
		this.initialNominalValue=initialNominalValue;
		this.currentNominalValue=currentNominalValue;
		this.interestRate=interestRate;
		this.issuanceDate=issuanceDate;
		this.securityDaysTerm=securityDaysTerm;
		this.currency=currency;
		this.desmaterializedBalance=desmaterializedBalance;
		this.expirationDate=expirationDate;
		this.numberCoupons=numberCoupons;
		this.registryUser=registryUser;
		this.indAuthorized=indAuthorized;
	}
	
	public Security(String idSecurityCodePk, String idIsinCode, String alternativeCode,
			String cfiCode, String description, String mnemonic,
			Integer securityType, Integer securityClass, Integer stateSecurity,
			BigDecimal initialNominalValue, BigDecimal currentNominalValue,
			BigDecimal interestRate, Date issuanceDate, Integer securityDaysTerm, Integer currency,
			BigDecimal desmaterializedBalance, Date expirationDate, Integer numberCoupons,String registryUser) {
		
		this.idSecurityCodePk=idSecurityCodePk;
		this.idIsinCode = idIsinCode;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.description = description;
		this.mnemonic = mnemonic;
		this.securityType = securityType;
		this.securityClass=securityClass;
		this.stateSecurity = stateSecurity;
		this.initialNominalValue=initialNominalValue;
		this.currentNominalValue=currentNominalValue;
		this.interestRate=interestRate;
		this.issuanceDate=issuanceDate;
		this.securityDaysTerm=securityDaysTerm;
		this.currency=currency;
		this.desmaterializedBalance=desmaterializedBalance;
		this.expirationDate=expirationDate;
		this.numberCoupons=numberCoupons;
		this.registryUser=registryUser;
	}
	
	public Security(String idSecurityCodePk, String idIsinCode, String alternativeCode,
			String cfiCode, String description, String mnemonic,
			Integer securityType, Integer securityClass, Integer stateSecurity,
			BigDecimal initialNominalValue, BigDecimal currentNominalValue,
			BigDecimal interestRate, Date issuanceDate, Integer securityDaysTerm, Integer currency,
			BigDecimal desmaterializedBalance, Date expirationDate, Integer numberCoupons, Integer notTraded,String registryUser) {
		
		this.idSecurityCodePk=idSecurityCodePk;
		this.idIsinCode = idIsinCode;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.description = description;
		this.mnemonic = mnemonic;
		this.securityType = securityType;
		this.securityClass=securityClass;
		this.stateSecurity = stateSecurity;
		this.initialNominalValue=initialNominalValue;
		this.currentNominalValue=currentNominalValue;
		this.interestRate=interestRate;
		this.issuanceDate=issuanceDate;
		this.securityDaysTerm=securityDaysTerm;
		this.currency=currency;
		this.desmaterializedBalance=desmaterializedBalance;
		this.expirationDate=expirationDate;
		this.numberCoupons=numberCoupons;
		this.registryUser=registryUser;
		this.notTraded = notTraded;
	}
	
	/**
	 * Instantiates a new security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param idIsinCode the id isin code
	 * @param alternativeCode the alternative code
	 * @param cfiCode the cfi code
	 * @param description the description
	 * @param mnemonic the mnemonic
	 * @param securityType the security type
	 * @param securityClass the security class
	 * @param stateSecurity the state security
	 * @param initialNominalValue the initial nominal value
	 * @param currentNominalValue the current nominal value
	 * @param interestRate the interest rate
	 * @param issuanceDate the issuance date
	 * @param securityDaysTerm the security days term
	 * @param currency the currency
	 */
	public Security(String idSecurityCodePk, String idIsinCode, String alternativeCode,
			String cfiCode, String description, String mnemonic,
			Integer securityType, Integer securityClass, Integer stateSecurity,
			BigDecimal initialNominalValue, BigDecimal currentNominalValue,
			BigDecimal interestRate, Date issuanceDate, Integer securityDaysTerm, Integer currency) {
		super();
		this.idSecurityCodePk=idSecurityCodePk;
		this.idIsinCode = idIsinCode;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.description = description;
		this.mnemonic = mnemonic;
		this.securityType = securityType;
		this.securityClass=securityClass;
		this.stateSecurity = stateSecurity;
		this.initialNominalValue=initialNominalValue;
		this.currentNominalValue=currentNominalValue;
		this.interestRate=interestRate;
		this.issuanceDate=issuanceDate;
		this.securityDaysTerm=securityDaysTerm;
		this.currency=currency;
	}
	
	/**
	 * Instantiates a new security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param alternativeCode the alternative code
	 * @param cfiCode the cfi code
	 * @param description the description
	 * @param mnemonic the mnemonic
	 * @param securityType the security type
	 * @param stateSecurity the state security
	 * @param currency the currency
	 * @param initialNominalValue the initial nominal value
	 * @param currentNominalValue the current nominal value
	 */
	public Security(String idSecurityCodePk, String alternativeCode,
			String cfiCode, String description, String mnemonic,
			Integer securityType, Integer stateSecurity, Integer currency,
			BigDecimal initialNominalValue, BigDecimal currentNominalValue) {
		super();
		this.idSecurityCodePk = idSecurityCodePk;
		this.alternativeCode = alternativeCode;
		this.cfiCode = cfiCode;
		this.description = description;
		this.mnemonic = mnemonic;
		this.securityType = securityType;
		this.stateSecurity = stateSecurity;
		this.currency = currency;
		this.initialNominalValue=initialNominalValue;
		this.currentNominalValue=currentNominalValue;
	}
	
	/**
	 * Instantiates a new security.
	 */
	public Security() {
		interestPaymentSchedule=new InterestPaymentSchedule();
		amortizationPaymentSchedule=new AmortizationPaymentSchedule();
		issuance=new Issuance();
		securityInvestor=new SecurityInvestor();
		valuatorProcessClass=new ValuatorProcessClass();
		indPrepaid = ComponentConstant.ZERO;
		indExtendedTerm = ComponentConstant.ZERO;
	}

	/**
	 * Gets the security type description.
	 *
	 * @return the security type description
	 */
	public String getSecurityTypeDescription(){
		return securityTypeDescription;
	}

	
	/**
	 * Gets the interest type description.
	 *
	 * @return the interest type description
	 */
	public String getInterestTypeDescription() {
		return interestTypeDescription;
	}

	/**
	 * Sets the interest type description.
	 *
	 * @param interestTypeDescription the new interest type description
	 */
	public void setInterestTypeDescription(String interestTypeDescription) {
		this.interestTypeDescription = interestTypeDescription;
	}

	/**
	 * Gets the currency type description.
	 *
	 * @return the currency type description
	 */
	public String getCurrencyTypeDescription() {
		return currencyTypeDescription;
	}
	
	/**
	 * Gets the currency iso type description.
	 *
	 * @return the currency iso type description
	 */
	public String getCurrencyIsoTypeDescription(){
		return CurrencyType.get(this.currency).getCodeIso();
	}

	/**
	 * Sets the currency type description.
	 *
	 * @param currencyTypeDescription the new currency type description
	 */
	public void setCurrencyTypeDescription(String currencyTypeDescription) {
		this.currencyTypeDescription = currencyTypeDescription;
	}

	/**
	 * Gets the instrument type description.
	 *
	 * @return the instrument type description
	 */
	public String getInstrumentTypeDescription() {
		return instrumentTypeDescription;
	}

	/**
	 * Sets the instrument type description.
	 *
	 * @param instrumentTypeDescription the new instrument type description
	 */
	public void setInstrumentTypeDescription(String instrumentTypeDescription) {
		this.instrumentTypeDescription = instrumentTypeDescription;
	}

	/**
	 * Gets the id isin code.
	 *
	 * @return the id isin code
	 */
	public String getIdIsinCode() {
		return idIsinCode;
	}

	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCode the new id isin code pk
	 */
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}

	/**
	 * Gets the alternative code.
	 *
	 * @return the alternative code
	 */
	public String getAlternativeCode() {
		return alternativeCode;
	}

	/**
	 * Sets the alternative code.
	 *
	 * @param alternativeCode the new alternative code
	 */
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}

	/**
	 * Gets the amortization amount.
	 *
	 * @return the amortization amount
	 */
	public BigDecimal getAmortizationAmount() {
		return amortizationAmount;
	}

	/**
	 * Sets the amortization amount.
	 *
	 * @param amortizationAmount the new amortization amount
	 */
	public void setAmortizationAmount(BigDecimal amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	/**
	 * Gets the amortization factor.
	 *
	 * @return the amortization factor
	 */
	public BigDecimal getAmortizationFactor() {
		return amortizationFactor;
	}

	/**
	 * Sets the amortization factor.
	 *
	 * @param amortizationFactor the new amortization factor
	 */
	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}

	/**
	 * Gets the amortization type.
	 *
	 * @return the amortization type
	 */
	public Integer getAmortizationType() {
		return amortizationType;
	}

	/**
	 * Sets the amortization type.
	 *
	 * @param amortizationType the new amortization type
	 */
	public void setAmortizationType(Integer amortizationType) {
		this.amortizationType = amortizationType;
	}

	/**
	 * Gets the amortization periodicity.
	 *
	 * @return the amortization periodicity
	 */
	public Integer getAmortizationPeriodicity() {
		return amortizationPeriodicity;
	}

	/**
	 * Sets the amortization periodicity.
	 *
	 * @param amortizationPeriodicity the new amortization periodicity
	 */
	public void setAmortizationPeriodicity(Integer amortizationPeriodicity) {
		this.amortizationPeriodicity = amortizationPeriodicity;
	}

	/**
	 * Gets the calendar days.
	 *
	 * @return the calendar days
	 */
	public Integer getCalendarDays() {
		return calendarDays;
	}

	/**
	 * Sets the calendar days.
	 *
	 * @param calendarDays the new calendar days
	 */
	public void setCalendarDays(Integer calendarDays) {
		this.calendarDays = calendarDays;
	}

	/**
	 * Gets the calendar type.
	 *
	 * @return the calendar type
	 */
	public Integer getCalendarType() {
		return calendarType;
	}

	/**
	 * Sets the calendar type.
	 *
	 * @param calendarType the new calendar type
	 */
	public void setCalendarType(Integer calendarType) {
		this.calendarType = calendarType;
	}

	/**
	 * Gets the cfi code.
	 *
	 * @return the cfi code
	 */
	public String getCfiCode() {
		return cfiCode;
	}

	/**
	 * Sets the cfi code.
	 *
	 * @param cfiCode the new cfi code
	 */
	public void setCfiCode(String cfiCode) {
		this.cfiCode = cfiCode;
	}

	/**
	 * Gets the circulation amount.
	 *
	 * @return the circulation amount
	 */
	public BigDecimal getCirculationAmount() {
		return circulationAmount;
	}

	/**
	 * Sets the circulation amount.
	 *
	 * @param circulationAmount the new circulation amount
	 */
	public void setCirculationAmount(BigDecimal circulationAmount) {
		this.circulationAmount = circulationAmount;
	}

	/**
	 * Gets the circulation balance.
	 *
	 * @return the circulation balance
	 */
	public BigDecimal getCirculationBalance() {
		return circulationBalance;
	}

	/**
	 * Sets the circulation balance.
	 *
	 * @param circulationBalance the new circulation balance
	 */
	public void setCirculationBalance(BigDecimal circulationBalance) {
		this.circulationBalance = circulationBalance;
	}

	/**
	 * Gets the corporative process days.
	 *
	 * @return the corporative process days
	 */
	public Integer getCorporativeProcessDays() {
		return corporativeProcessDays;
	}

	/**
	 * Sets the corporative process days.
	 *
	 * @param corporativeProcessDays the new corporative process days
	 */
	public void setCorporativeProcessDays(Integer corporativeProcessDays) {
		this.corporativeProcessDays = corporativeProcessDays;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the amortization on.
	 *
	 * @return the amortization on
	 */
	public Integer getAmortizationOn() {
		return amortizationOn;
	}

	/**
	 * Sets the amortization on.
	 *
	 * @param amortizationOn the new amortization on
	 */
	public void setAmortizationOn(Integer amortizationOn) {
		this.amortizationOn = amortizationOn;
	}

	/**
	 * Gets the currency name.
	 *
	 * @return the currency name
	 */
	public String getCurrencyName() {
		return currencyName;
	}

	/**
	 * Sets the currency name.
	 *
	 * @param currencyName the new currency name
	 */
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	/**
	 * Gets the current nominal value.
	 *
	 * @return the current nominal value
	 */
	public BigDecimal getCurrentNominalValue() {
		return currentNominalValue;
	}

	/**
	 * Sets the current nominal value.
	 *
	 * @param currentNominalValue the new current nominal value
	 */
	public void setCurrentNominalValue(BigDecimal currentNominalValue) {
		this.currentNominalValue = currentNominalValue;
	}

	/**
	 * Gets the deposit registry date.
	 *
	 * @return the deposit registry date
	 */
	public Date getDepositRegistryDate() {
		return depositRegistryDate;
	}

	/**
	 * Sets the deposit registry date.
	 *
	 * @param depositRegistryDate the new deposit registry date
	 */
	public void setDepositRegistryDate(Date depositRegistryDate) {
		this.depositRegistryDate = depositRegistryDate;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the desmaterialized balance.
	 *
	 * @return the desmaterialized balance
	 */
	public BigDecimal getDesmaterializedBalance() {
		return desmaterializedBalance;
	}

	/**
	 * Sets the desmaterialized balance.
	 *
	 * @param desmaterializedBalance the new desmaterialized balance
	 */
	public void setDesmaterializedBalance(BigDecimal desmaterializedBalance) {
		this.desmaterializedBalance = desmaterializedBalance;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	*/
	public Integer getEconomicActivity() {
		return economicActivity;
	}
 
	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the new economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the financial index.
	 *
	 * @return the financial index
	 */
	public Integer getFinancialIndex() {
		return financialIndex;
	}

	/**
	 * Sets the financial index.
	 *
	 * @param financialIndex the new financial index
	 */
	public void setFinancialIndex(Integer financialIndex) {
		this.financialIndex = financialIndex;
	}

	/**
	 * Gets the id group fk.
	 *
	 * @return the id group fk
	 */
	public Integer getIdGroupFk() {
		return idGroupFk;
	}

	/**
	 * Sets the id group fk.
	 *
	 * @param idGroupFk the new id group fk
	 */
	public void setIdGroupFk(Integer idGroupFk) {
		this.idGroupFk = idGroupFk;
	}


	/**
	 * Gets the ind convertible stock.
	 *
	 * @return the ind convertible stock
	 */
	public Integer getIndConvertibleStock() {
		return indConvertibleStock;
	}

	/**
	 * Sets the ind convertible stock.
	 *
	 * @param indConvertibleStock the new ind convertible stock
	 */
	public void setIndConvertibleStock(Integer indConvertibleStock) {
		this.indConvertibleStock = indConvertibleStock;
	}

	/**
	 * Gets the ind early redemption.
	 *
	 * @return the ind early redemption
	 */
	public Integer getIndEarlyRedemption() {
		return indEarlyRedemption;
	}

	/**
	 * Sets the ind early redemption.
	 *
	 * @param indEarlyRedemption the new ind early redemption
	 */
	public void setIndEarlyRedemption(Integer indEarlyRedemption) {
		this.indEarlyRedemption = indEarlyRedemption;
		if(isEarlyRedemption()){
			this.setIndSplitCoupon(BooleanType.NO.getCode());
		}
	}

	/**
	 * Gets the ind is coupon.
	 *
	 * @return the ind is coupon
	 */
	public Integer getIndIsCoupon() {
		return indIsCoupon;
	}

	/**
	 * Sets the ind is coupon.
	 *
	 * @param indIsCoupon the new ind is coupon
	 */
	public void setIndIsCoupon(Integer indIsCoupon) {
		this.indIsCoupon = indIsCoupon;
	}

	/**
	 * Gets the ind is fractionable.
	 *
	 * @return the ind is fractionable
	 */
	public Integer getIndIsFractionable() {
		return indIsFractionable;
	}

	/**
	 * Sets the ind is fractionable.
	 *
	 * @param indIsFractionable the new ind is fractionable
	 */
	public void setIndIsFractionable(Integer indIsFractionable) {
		this.indIsFractionable = indIsFractionable;
	}

	/**
	 * Gets the ind issuance management.
	 *
	 * @return the ind issuance management
	 */
	public Integer getIndIssuanceManagement() {
		return indIssuanceManagement;
	}

	/**
	 * Sets the ind issuance management.
	 *
	 * @param indIssuanceManagement the new ind issuance management
	 */
	public void setIndIssuanceManagement(Integer indIssuanceManagement) {
		this.indIssuanceManagement = indIssuanceManagement;
	}

	/**
	 * Gets the ind payment benefit.
	 *
	 * @return the ind payment benefit
	 */
	public Integer getIndPaymentBenefit() {
		return indPaymentBenefit;
	}

	/**
	 * Sets the ind payment benefit.
	 *
	 * @param indPaymentBenefit the new ind payment benefit
	 */
	public void setIndPaymentBenefit(Integer indPaymentBenefit) {
		this.indPaymentBenefit = indPaymentBenefit;
	}

	/**
	 * Gets the ind receivable custody.
	 *
	 * @return the ind receivable custody
	 */
	public Integer getIndReceivableCustody() {
		return indReceivableCustody;
	}

	/**
	 * Sets the ind receivable custody.
	 *
	 * @param indReceivableCustody the new ind receivable custody
	 */
	public void setIndReceivableCustody(Integer indReceivableCustody) {
		this.indReceivableCustody = indReceivableCustody;
	}

	/**
	 * Gets the ind securitization.
	 *
	 * @return the ind securitization
	 */
	public Integer getIndSecuritization() {
		return indSecuritization;
	}

	/**
	 * Sets the ind securitization.
	 *
	 * @param indSecuritization the new ind securitization
	 */
	public void setIndSecuritization(Integer indSecuritization) {
		this.indSecuritization = indSecuritization;
	}

	/**
	 * Gets the ind split coupon.
	 *
	 * @return the ind split coupon
	 */
	public Integer getIndSplitCoupon() {
		return indSplitCoupon;
	}

	/**
	 * Sets the ind split coupon.
	 *
	 * @param indSplitCoupon the new ind split coupon
	 */
	public void setIndSplitCoupon(Integer indSplitCoupon) {
		this.indSplitCoupon = indSplitCoupon;
		if(isAllowSplitCoupons()){
				this.setIndEarlyRedemption(BooleanType.NO.getCode());
		}
	}

	/**
	 * Gets the ind trader secondary.
	 *
	 * @return the ind trader secondary
	 */
	public Integer getIndTraderSecondary() {
		return indTraderSecondary;
	}

	/**
	 * Sets the ind trader secondary.
	 *
	 * @param indTraderSecondary the new ind trader secondary
	 */
	public void setIndTraderSecondary(Integer indTraderSecondary) {
		this.indTraderSecondary = indTraderSecondary;
	}

	/**
	 * Gets the ind capitalizable interest.
	 *
	 * @return the ind capitalizable interest
	 */
	public Integer getIndCapitalizableInterest() {
		return indCapitalizableInterest;
	}

	/**
	 * Sets the ind capitalizable interest.
	 *
	 * @param indCapitalizableInterest the new ind capitalizable interest
	 */
	public void setIndCapitalizableInterest(Integer indCapitalizableInterest) {
		this.indCapitalizableInterest = indCapitalizableInterest;
	}

	/**
	 * Gets the security months term.
	 *
	 * @return the security months term
	 */
	public Integer getSecurityMonthsTerm() {
		return securityMonthsTerm;
	}

	/**
	 * Sets the security months term.
	 *
	 * @param securityMonthsTerm the new security months term
	 */
	public void setSecurityMonthsTerm(Integer securityMonthsTerm) {
		this.securityMonthsTerm = securityMonthsTerm;
	}

	/**
	 * Gets the capital payment modality.
	 *
	 * @return the capital payment modality
	 */
	public Integer getCapitalPaymentModality() {
		return capitalPaymentModality;
	}

	/**
	 * Sets the capital payment modality.
	 *
	 * @param capitalPaymentModality the new capital payment modality
	 */
	public void setCapitalPaymentModality(Integer capitalPaymentModality) {
		this.capitalPaymentModality = capitalPaymentModality;
	}

	/**
	 * Gets the initial nominal value.
	 *
	 * @return the initial nominal value
	 */
	public BigDecimal getInitialNominalValue() {
		return initialNominalValue;
	}

	/**
	 * Sets the initial nominal value.
	 *
	 * @param initialNominalValue the new initial nominal value
	 */
	public void setInitialNominalValue(BigDecimal initialNominalValue) {
		this.initialNominalValue = initialNominalValue;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the interest factor.
	 *
	 * @return the interest factor
	 */
	public BigDecimal getInterestFactor() {
		return interestFactor;
	}

	/**
	 * Sets the interest factor.
	 *
	 * @param interestFactor the new interest factor
	 */
	public void setInterestFactor(BigDecimal interestFactor) {
		this.interestFactor = interestFactor;
	}

	/**
	 * Gets the interest payment modality.
	 *
	 * @return the interest payment modality
	 */
	public Integer getInterestPaymentModality() {
		return interestPaymentModality;
	}

	/**
	 * Sets the interest payment modality.
	 *
	 * @param interestPaymentModality the new interest payment modality
	 */
	public void setInterestPaymentModality(Integer interestPaymentModality) {
		this.interestPaymentModality = interestPaymentModality;
	}

	/**
	 * Gets the interest rate.
	 *
	 * @return the interest rate
	 */
	public BigDecimal getInterestRate() {
		return interestRate;
	}

	/**
	 * Sets the interest rate.
	 *
	 * @param interestRate the new interest rate
	 */
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	/**
	 * Gets the interest rate periodicity.
	 *
	 * @return the interest rate periodicity
	 */
	public Integer getInterestRatePeriodicity() {
		return interestRatePeriodicity;
	}

	/**
	 * Sets the interest rate periodicity.
	 *
	 * @param interestRatePeriodicity the new interest rate periodicity
	 */
	public void setInterestRatePeriodicity(Integer interestRatePeriodicity) {
		this.interestRatePeriodicity = interestRatePeriodicity;
	}

	/**
	 * Gets the interest type.
	 *
	 * @return the interest type
	 */
	public Integer getInterestType() {
		return interestType;
	}

	/**
	 * Sets the interest type.
	 *
	 * @param interestType the new interest type
	 */
	public void setInterestType(Integer interestType) {
		this.interestType = interestType;
	}

	/**
	 * Gets the issuance country.
	 *
	 * @return the issuance country
	 */
	public GeographicLocation getIssuanceCountry() {
		return issuanceCountry;
	}

	/**
	 * Sets the issuance country.
	 *
	 * @param issuanceCountry the new issuance country
	 */
	public void setIssuanceCountry(GeographicLocation issuanceCountry) {
		this.issuanceCountry = issuanceCountry;
	}

	/**
	 * Gets the issuance date.
	 *
	 * @return the issuance date
	 */
	public Date getIssuanceDate() {
		return issuanceDate;
	}

	/**
	 * Sets the issuance date.
	 *
	 * @param issuanceDate the new issuance date
	 */
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	/**
	 * Gets the issuance form.
	 *
	 * @return the issuance form
	 */
	public Integer getIssuanceForm() {
		return issuanceForm;
	}

	/**
	 * Sets the issuance form.
	 *
	 * @param issuanceForm the new issuance form
	 */
	public void setIssuanceForm(Integer issuanceForm) {
		this.issuanceForm = issuanceForm;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the number coupons.
	 *
	 * @return the number coupons
	 */
	public Integer getNumberCoupons() {
		return numberCoupons;
	}

	/**
	 * Sets the number coupons.
	 *
	 * @param numberCoupons the new number coupons
	 */
	public void setNumberCoupons(Integer numberCoupons) {
		this.numberCoupons = numberCoupons;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the maximum invesment.
	 *
	 * @return the maximum invesment
	 */
	public BigDecimal getMaximumInvesment() {
		return maximumInvesment;
	}

	/**
	 * Sets the maximum invesment.
	 *
	 * @param maximumInvesment the new maximum invesment
	 */
	public void setMaximumInvesment(BigDecimal maximumInvesment) {
		this.maximumInvesment = maximumInvesment;
	}

	/**
	 * Gets the minimum invesment.
	 *
	 * @return the minimum invesment
	 */
	public BigDecimal getMinimumInvesment() {
		return minimumInvesment;
	}

	/**
	 * Sets the minimum invesment.
	 *
	 * @param minimumInvesment the new minimum invesment
	 */
	public void setMinimumInvesment(BigDecimal minimumInvesment) {
		this.minimumInvesment = minimumInvesment;
	}
	
	/**
	 * Sets quantity BCB issue 578
	 * @return
	 */
	public BigDecimal getQuantityBcb() {
		return quantityBcb;
	}

	/**
	 * Gets quantity BCB issue 578
	 * @param quantityBcb
	 */
	public void setQuantityBcb(BigDecimal quantityBcb) {
		this.quantityBcb = quantityBcb;
	}
	
	/**
	 * Gets issuanceQuantitySecBcb issue 578
	 * @param quantityBcb
	 */
	
	public BigDecimal getIssuanceQuantitySecBcb() {
		return issuanceQuantitySecBcb;
	}
	
	/**
	 * Sets issuanceQuantitySecBcb issue 578
	 * @return
	 */
	
	public void setIssuanceQuantitySecBcb(BigDecimal issuanceQuantitySecBcb) {
		this.issuanceQuantitySecBcb = issuanceQuantitySecBcb;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the periodicity.
	 *
	 * @return the periodicity
	 */
	public Integer getPeriodicity() {
		return periodicity;
	}

	/**
	 * Sets the periodicity.
	 *
	 * @param periodicity the new periodicity
	 */
	public void setPeriodicity(Integer periodicity) {
		this.periodicity = periodicity;
	}

	/**
	 * Gets the physical balance.
	 *
	 * @return the physical balance
	 */
	public BigDecimal getPhysicalBalance() {
		return physicalBalance;
	}

	/**
	 * Sets the physical balance.
	 *
	 * @param physicalBalance the new physical balance
	 */
	public void setPhysicalBalance(BigDecimal physicalBalance) {
		this.physicalBalance = physicalBalance;
	}

	/**
	 * Gets the placed amount.
	 *
	 * @return the placed amount
	 */
	public BigDecimal getPlacedAmount() {
		return placedAmount;
	}

	/**
	 * Sets the placed amount.
	 *
	 * @param placedAmount the new placed amount
	 */
	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	/**
	 * Gets the placed balance.
	 *
	 * @return the placed balance
	 */
	public BigDecimal getPlacedBalance() {
		return placedBalance;
	}

	/**
	 * Sets the placed balance.
	 *
	 * @param placedBalance the new placed balance
	 */
	public void setPlacedBalance(BigDecimal placedBalance) {
		this.placedBalance = placedBalance;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the retirement date.
	 *
	 * @return the retirement date
	 */
	public Date getRetirementDate() {
		return retirementDate;
	}

	/**
	 * Sets the retirement date.
	 *
	 * @param retirementDate the new retirement date
	 */
	public void setRetirementDate(Date retirementDate) {
		this.retirementDate = retirementDate;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	
	/**
	 * Gets the security serial.
	 *
	 * @return the security serial
	 */
	public String getSecuritySerial() {
		return securitySerial;
	}

	/**
	 * Sets the security serial.
	 *
	 * @param securitySerial the new security serial
	 */
	public void setSecuritySerial(String securitySerial) {
		this.securitySerial = securitySerial;
	}

	/**
	 * Gets the security source.
	 *
	 * @return the security source
	 */
	public Integer getSecuritySource() {
		return securitySource;
	}

	/**
	 * Sets the security source.
	 *
	 * @param securitySource the new security source
	 */
	public void setSecuritySource(Integer securitySource) {
		this.securitySource = securitySource;
	}

	/**
	 * Gets the security type.
	 *
	 * @return the security type
	 */
	public Integer getSecurityType() {
		return securityType;
	}

	/**
	 * Sets the security type.
	 *
	 * @param securityType the new security type
	 */
	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}

	/**
	 * Gets the serial program.
	 *
	 * @return the serial program
	 */
	public String getSerialProgram() {
		return serialProgram;
	}

	/**
	 * Sets the serial program.
	 *
	 * @param serialProgram the new serial program
	 */
	public void setSerialProgram(String serialProgram) {
		this.serialProgram = serialProgram;
	}

	/**
	 * Gets the share balance.
	 *
	 * @return the share balance
	 */
	public BigDecimal getShareBalance() {
		return shareBalance;
	}

	/**
	 * Sets the share balance.
	 *
	 * @param shareBalance the new share balance
	 */
	public void setShareBalance(BigDecimal shareBalance) {
		this.shareBalance = shareBalance;
	}

	/**
	 * Gets the share capital.
	 *
	 * @return the share capital
	 */
	public BigDecimal getShareCapital() {
		return shareCapital;
	}

	/**
	 * Sets the share capital.
	 *
	 * @param shareCapital the new share capital
	 */
	public void setShareCapital(BigDecimal shareCapital) {
		this.shareCapital = shareCapital;
	}

	/**
	 * Gets the state security.
	 *
	 * @return the state security
	 */
	public Integer getStateSecurity() {
		return stateSecurity;
	}
	
	/**
	 * Gets the state security description.
	 *
	 * @return the state security description
	 */
	public String getStateSecurityDescription() {
		return SecurityStateType.lookup.get(stateSecurity).getValue();
	}

	/**
	 * Sets the state security.
	 *
	 * @param stateSecurity the new state security
	 */
	public void setStateSecurity(Integer stateSecurity) {
		this.stateSecurity = stateSecurity;
	}

	/**
	 * Gets the stock registry days.
	 *
	 * @return the stock registry days
	 */
	public Integer getStockRegistryDays() {
		return stockRegistryDays;
	}

	/**
	 * Sets the stock registry days.
	 *
	 * @param stockRegistryDays the new stock registry days
	 */
	public void setStockRegistryDays(Integer stockRegistryDays) {
		this.stockRegistryDays = stockRegistryDays;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the yield.
	 *
	 * @return the yield
	 */
	public Integer getYield() {
		return yield;
	}

	/**
	 * Sets the yield.
	 *
	 * @param yield the new yield
	 */
	public void setYield(Integer yield) {
		this.yield = yield;
	}

	/**
	 * Gets the spread.
	 *
	 * @return the spread
	 */
	public BigDecimal getSpread() {
		return spread;
	}

	/**
	 * Sets the spread.
	 *
	 * @param spread the new spread
	 */
	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}

	/**
	 * Gets the indexed.
	 *
	 * @return the indexed
	 */
	public Integer getIndexed() {
		return indexed;
	}

	/**
	 * Sets the indexed.
	 *
	 * @param indexed the new indexed
	 */
	public void setIndexed(Integer indexed) {
		this.indexed = indexed;
	}

	/**
	 * Gets the rate type.
	 *
	 * @return the rate type
	 */
	public Integer getRateType() {
		return rateType;
	}

	/**
	 * Sets the rate type.
	 *
	 * @param rateType the new rate type
	 */
	public void setRateType(Integer rateType) {
		this.rateType = rateType;
	}

	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Integer getOperationType() {
		return operationType;
	}

	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Integer operationType) {
		this.operationType = operationType;
	}

	/**
	 * Gets the issuance.
	 *
	 * @return the issuance
	 */
	public Issuance getIssuance() {
		return issuance;
	}

	/**
	 * Sets the issuance.
	 *
	 * @param issuance the new issuance
	 */
	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public List<Security> getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the new securities
	 */
	public void setSecurities(List<Security> securities) {
		this.securities = securities;
	}

	/**
	 * Gets the security foreign depositories.
	 *
	 * @return the security foreign depositories
	 */
	public List<SecurityForeignDepository> getSecurityForeignDepositories() {
		return securityForeignDepositories;
	}

	/**
	 * Sets the security foreign depositories.
	 *
	 * @param securityForeignDepositories the new security foreign depositories
	 */
	public void setSecurityForeignDepositories(
			List<SecurityForeignDepository> securityForeignDepositories) {
		this.securityForeignDepositories = securityForeignDepositories;
	}

	/**
	 * Gets the security history balances.
	 *
	 * @return the security history balances
	 */
	public List<SecurityHistoryBalance> getSecurityHistoryBalances() {
		return securityHistoryBalances;
	}

	/**
	 * Sets the security history balances.
	 *
	 * @param securityHistoryBalances the new security history balances
	 */
	public void setSecurityHistoryBalances(
			List<SecurityHistoryBalance> securityHistoryBalances) {
		this.securityHistoryBalances = securityHistoryBalances;
	}

	/**
	 * Gets the coupon first expiration date.
	 *
	 * @return the coupon first expiration date
	 */
	public Date getCouponFirstExpirationDate() {
		return couponFirstExpirationDate;
	}

	/**
	 * Sets the coupon first expiration date.
	 *
	 * @param couponFirstExpirationDate the new coupon first expiration date
	 */
	public void setCouponFirstExpirationDate(Date couponFirstExpirationDate) {
		this.couponFirstExpirationDate = couponFirstExpirationDate;
	}

	/**
	 * Gets the security history states.
	 *
	 * @return the security history states
	 */
	public List<SecurityHistoryState> getSecurityHistoryStates() {
		return securityHistoryStates;
	}

	/**
	 * Gets the maximum rate.
	 *
	 * @return the maximum rate
	 */
	public BigDecimal getMaximumRate() {
		return maximumRate;
	}

	/**
	 * Sets the maximum rate.
	 *
	 * @param maximumRate the new maximum rate
	 */
	public void setMaximumRate(BigDecimal maximumRate) {
		this.maximumRate = maximumRate;
	}

	/**
	 * Gets the minimum rate.
	 *
	 * @return the minimum rate
	 */
	public BigDecimal getMinimumRate() {
		return minimumRate;
	}

	/**
	 * Sets the minimum rate.
	 *
	 * @param minimumRate the new minimum rate
	 */
	public void setMinimumRate(BigDecimal minimumRate) {
		this.minimumRate = minimumRate;
	}

	/**
	 * Sets the security history states.
	 *
	 * @param securityHistoryStates the new security history states
	 */
	public void setSecurityHistoryStates(
			List<SecurityHistoryState> securityHistoryStates) {
		this.securityHistoryStates = securityHistoryStates;
	}

	/**
	 * Gets the security negotiation mechanisms.
	 *
	 * @return the security negotiation mechanisms
	 */
	public List<SecurityNegotiationMechanism> getSecurityNegotiationMechanisms() {
		return securityNegotiationMechanisms;
	}

	/**
	 * Sets the security negotiation mechanisms.
	 *
	 * @param securityNegotiationMechanisms the new security negotiation mechanisms
	 */
	public void setSecurityNegotiationMechanisms(
			List<SecurityNegotiationMechanism> securityNegotiationMechanisms) {
		this.securityNegotiationMechanisms = securityNegotiationMechanisms;
	}

	/**
	 * Gets the holder account balances.
	 *
	 * @return the holder account balances
	 */
	public List<HolderAccountBalance> getHolderAccountBalances() {
		return holderAccountBalances;
	}

	/**
	 * Sets the holder account balances.
	 *
	 * @param holderAccountBalances the new holder account balances
	 */
	public void setHolderAccountBalances(
			List<HolderAccountBalance> holderAccountBalances) {
		this.holderAccountBalances = holderAccountBalances;
	}

	/**
	 * Gets the security source description.
	 *
	 * @return the security source description
	 */
	public String getSecuritySourceDescription(){
		if(securitySource!=null && SecuritySourceType.get( securitySource )!=null){
			return SecuritySourceType.get( securitySource ).getValue();
		}
		return "";
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * Gets the placement segment detail list.
	 *
	 * @return the placement segment detail list
	 */
	public List<PlacementSegmentDetail> getPlacementSegmentDetailList() {
		return placementSegmentDetailList;
	}

	/**
	 * Sets the placement segment detail list.
	 *
	 * @param placementSegmentDetailList the new placement segment detail list
	 */
	public void setPlacementSegmentDetailList(
			List<PlacementSegmentDetail> placementSegmentDetailList) {
		this.placementSegmentDetailList = placementSegmentDetailList;
	}

	/**
	 * Gets the inscription date.
	 *
	 * @return the inscription date
	 */
	public Date getInscriptionDate() {
		return inscriptionDate;
	}

	/**
	 * Sets the inscription date.
	 *
	 * @param inscriptionDate the new inscription date
	 */
	public void setInscriptionDate(Date inscriptionDate) {
		this.inscriptionDate = inscriptionDate;
	}
	
	
	/**
	 * Gets the cash nominal.
	 *
	 * @return the cash nominal
	 */
	public Integer getCashNominal() {
		return cashNominal;
	}

	/**
	 * Sets the cash nominal.
	 *
	 * @param cashNominal the new cash nominal
	 */
	public void setCashNominal(Integer cashNominal) {
		this.cashNominal = cashNominal;
	}

	/**
	 * Gets the interest payment schedule.
	 *
	 * @return the interest payment schedule
	 */
	public InterestPaymentSchedule getInterestPaymentSchedule() {
		return interestPaymentSchedule;
	}

	/**
	 * Sets the interest payment schedule.
	 *
	 * @param interestPaymentSchedule the new interest payment schedule
	 */
	public void setInterestPaymentSchedule(
			InterestPaymentSchedule interestPaymentSchedule) {
		this.interestPaymentSchedule = interestPaymentSchedule;
	}

	/**
	 * Gets the amortization payment schedule.
	 *
	 * @return the amortization payment schedule
	 */
	public AmortizationPaymentSchedule getAmortizationPaymentSchedule() {
		return amortizationPaymentSchedule;
	}

	/**
	 * Sets the amortization payment schedule.
	 *
	 * @param amortizationPaymentSchedule the new amortization payment schedule
	 */
	public void setAmortizationPaymentSchedule(
			AmortizationPaymentSchedule amortizationPaymentSchedule) {
		this.amortizationPaymentSchedule = amortizationPaymentSchedule;
	}
	
	
	
	/**
	 * Gets the security investor.
	 *
	 * @return the security investor
	 */
	public SecurityInvestor getSecurityInvestor() {
		return securityInvestor;
	}

	/**
	 * Sets the security investor.
	 *
	 * @param securityInvestor the new security investor
	 */
	public void setSecurityInvestor(SecurityInvestor securityInvestor) {
		this.securityInvestor = securityInvestor;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idSecurityCodePk == null) ? 0 : idSecurityCodePk.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Security other = (Security) obj;
		if (idSecurityCodePk == null) {
			if (other.idSecurityCodePk != null)
				return false;
		} else if (!idSecurityCodePk.equals(other.idSecurityCodePk))
			return false;
		return true;
	}

	/**
	 * Clean fixed instrument type fields.
	 */
	public void cleanFixedInstrumentTypeFields(){
		this.setInterestType(null);
		this.setYield(null);
		this.setCashNominal(null);
		this.setInterestRate(null);
		this.setPeriodicity(null);
		this.setInterestRatePeriodicity(null);
		this.setCalendarType(null);
		this.setCalendarDays(null);
		this.setInterestPaymentModality(null);
		
		this.setRateType(null);
		this.setOperationType(null);
		this.setSpread(null);
		this.setIndexed(null);
		this.setMinimumRate(null);
		this.setMaximumRate(null);
		
//		this.setIssuanceDate(null);
		this.setExpirationDate(null);
		this.setCouponFirstExpirationDate(null);
		this.setSecurityMonthsTerm(null);
		this.setNumberCoupons(null);
		
		this.setCapitalPaymentModality(null);
		this.setAmortizationType(null);
		this.setAmortizationFactor(null);
		this.setAmortizationPeriodicity(null);
		this.setAmortizationOn(null);				
		this.setFinancialIndex(null);
		if(this.isPartQuoteSecurityType()){
			this.setStockRegistryDays(null);
		}
		
		this.setIndSplitCoupon( BooleanType.NO.getCode() );
		this.setIndHasSplitSecurities( BooleanType.NO.getCode() );
		this.setIndIsDetached( BooleanType.NO.getCode() );
		this.setIndIsCoupon( BooleanType.NO.getCode() );
		this.setIndExtendedTerm(BooleanType.NO.getCode());
		this.setIndCapitalizableInterest(BooleanType.NO.getCode());
		this.setIndConvertibleStock(BooleanType.NO.getCode());

	}
	
	/**
	 * Default values.
	 *
	 * @throws Exception the exception
	 */
	public void defaultValues() throws Exception{
		
		this.setInscriptionDate(new Date());
		this.setMaximumRate(null);
		this.setMinimumRate(null);
		this.setStockRegistryDays(Integer.valueOf(1));
		this.setShareCapital(BigDecimal.ZERO);
		this.setShareBalance(BigDecimal.ZERO);
		this.setCirculationAmount(BigDecimal.ZERO);
		this.setCirculationBalance(BigDecimal.ZERO);
		this.setDesmaterializedBalance(BigDecimal.ZERO);
		this.setPhysicalBalance(BigDecimal.ZERO);
		this.setPhysicalDepositBalance(BigDecimal.ZERO);
		this.getSecurityInvestor().setIndForeign(BooleanType.YES.getCode());
		this.getSecurityInvestor().setIndLocal( BooleanType.YES.getCode() );
		this.getSecurityInvestor().setIndJuridical( BooleanType.YES.getCode() );
		this.getSecurityInvestor().setIndNatural( BooleanType.YES.getCode() );
		this.getSecurityInvestor().setIndCfi(BooleanType.NO.getCode());
		
		this.setIndTaxExempt(BooleanType.NO.getCode() );
		this.setIndIsCoupon( BooleanType.NO.getCode() );
		this.setIndIsFractionable( BooleanType.YES.getCode() );
		this.setIndIsDetached(BooleanType.NO.getCode() );
	//	this.setIndSubordinated(BooleanType.NO.getCode());
		
		this.setIndSecuritization(BooleanType.NO.getCode());
		
		this.setIndPaymentBenefit(BooleanType.YES.getCode());
		this.setIndPaymentReporting(BooleanType.NO.getCode());
		this.setIndReceivableCustody(BooleanType.NO.getCode());
		this.setIndIssuanceManagement(BooleanType.NO.getCode());
		this.setIndConvertibleStock( BooleanType.NO.getCode() );
		this.setIndEarlyRedemption(BooleanType.YES.getCode());
		
		if(this.isFixedInstrumentType()){
			this.setIndSplitCoupon( BooleanType.NO.getCode() );
		}
		if(this.isAccRvSecurityClass() || this.isAccRfSecurityClass()){
			this.setIndSubordinated(BooleanType.NO.getCode());
		}
		
		if(this.isDpfSecurityClass() || this.isDpaSecurityClass()){
			this.setIndSplitCoupon( BooleanType.NO.getCode() );
		}
		if(this.isBbsSecurityClass() || this.isBtsSecurityClass()){
			this.setIndSplitCoupon( BooleanType.YES.getCode() );
		}
		if(this.isFixedInstrumentType()){
			
			this.setIndExtendedTerm(BooleanType.NO.getCode());
			this.setIndCapitalizableInterest(BooleanType.NO.getCode());
			if(SecurityClassType.BBC.getCode().equals( getSecurityClass() )){
				this.setIndConvertibleStock( BooleanType.YES.getCode() );
			}
			if(this.isMixedIssuanceForm()){
		//		this.setIndPaymentBenefit(BooleanType.NO.getCode());
				
				this.setShareCapital(null);
				this.setCirculationAmount(null);
				
				this.setMinimumInvesment(BigDecimal.ZERO);
				this.setMaximumInvesment(BigDecimal.ZERO);
				
			}else if(this.isPhysicalIssuanceForm()){
				
		//		this.setIndPaymentBenefit(BooleanType.NO.getCode());
				this.setIndSplitCoupon( BooleanType.NO.getCode() );
				
			}
			
		}else if(this.isVariableInstrumentType()){
			cleanFixedInstrumentTypeFields();
			
		}
		if(this.getIssuance()!=null){
			if(!this.getIssuance().isPrimaryPlacementNegotiation()){
				this.setIndTraderSecondary(BooleanType.YES.getCode());
			}	
		}	
	}
	
	/* Security Class */
	
	public boolean isEnableDiscountRate(){
		if(isEnableYieldRate()){
			return true;
		}
		return false;
	}
	
	public boolean isEnableYieldRate(){
		if(SecurityClassType.LTS.getCode().equals(getSecurityClass()) ||
		   SecurityClassType.LBS.getCode().equals(getSecurityClass()) ||
		   SecurityClassType.CDS.getCode().equals(getSecurityClass()) ||
		   SecurityClassType.LRS.getCode().equals(getSecurityClass())
		) {
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is dpf security class.
	 *
	 * @return true, if is dpf security class
	 */
	public boolean isDpfSecurityClass(){
		if(SecurityClassType.DPF.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is dpa security class.
	 *
	 * @return true, if is dpa security class
	 */
	public boolean isDpaSecurityClass(){
		if(SecurityClassType.DPA.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	public boolean isDpfDpaSecurityClass(){
		if(isDpfSecurityClass() || isDpaSecurityClass()){
			return true;
		}
		return false;
	}
	
	public boolean isPgsSecurityClass(){
		if(SecurityClassType.PGS.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	public boolean isBbbSecurityClass(){
		if(SecurityClassType.BSP.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}

	public boolean isBmsSecurityClass(){
		if(SecurityClassType.BMS.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}

	public boolean isPgbSecurityClass(){
		if(SecurityClassType.PGB.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}

	public boolean isBbcSecurityClass(){
		if(SecurityClassType.BBC.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}

	public boolean isVtdSecurityClass(){
		if(SecurityClassType.VTD.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}


	/**
	 * Checks if is bbs security class.
	 *
	 * @return true, if is bbs security class
	 */
	public boolean isBbsSecurityClass(){
		if(SecurityClassType.BBS.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is bts security class.
	 *
	 * @return true, if is bts security class
	 */
	public boolean isBtsSecurityClass(){
		if(SecurityClassType.BTS.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	public boolean isAccRfSecurityClass(){
		if(SecurityClassType.ACC_RF.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	public boolean isAccRvSecurityClass(){
		if(SecurityClassType.ACC.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	public boolean isBbxSecurityClass(){
		if(SecurityClassType.BBX.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	public boolean isBtxSecurityClass(){
		if(SecurityClassType.BTX.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	public boolean isBlpSecurityClass(){
		if(SecurityClassType.BLP.getCode().equals( getSecurityClass() )){
			return true;
		}
		return false;
	}
	
	
	/* Security Class */
	
	/*Split Coupons Indicators Start*/
	
	/**
	 * Checks if is checks for split securities.
	 *
	 * @return true, if is checks for split securities
	 */
	public boolean isHasSplitSecurities(){
		if(BooleanType.YES.getCode().equals(getIndHasSplitSecurities())){
			return true;
		}
		return false;
	}

	public boolean isAllowSplitCoupons(){
		if(BooleanType.YES.getCode().equals( getIndSplitCoupon() )){
			return true;
		}
		return false;
	} 
	
	/**
	 * Checks if is coupon.
	 *
	 * @return true, if is coupon
	 */
	public boolean isCoupon(){
		if(BooleanType.YES.getCode().equals( getIndIsCoupon() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is detached.
	 *
	 * @return true, if is detached
	 */
	public boolean isDetached(){
		if(BooleanType.YES.getCode().equals( getIndIsDetached() )){
			return true;
		}
		return false;
	}
	
	public boolean isPrepaid(){
		if(BooleanType.YES.getCode().equals( getIndPrepaid() )){
			return true;
		}
		return false;
	}
	
	public boolean isSecurityType_Public() {
		if(SecurityType.PUBLIC.getCode().equals(getIssuance().getSecurityType())) {
			return true;
		}
		return false;
	}
	
	/*Split Coupons Indicators End*/
	
	/* Subordinated  Start */
	
	/**
	 * Checks if is subordinated.
	 *
	 * @return true, if is subordinated
	 */
	public boolean isSubordinated(){
		if(BooleanType.YES.getCode().equals( getIndSubordinated() )){
			return true;
		}
		return false;
	}	
	/* Subordinated End */
	
	/* Early Redemption Start */
	/**
	 * Checks if is early redemption.
	 *
	 * @return true, if is early redemption
	 */
	public boolean isEarlyRedemption(){
		if(BooleanType.YES.getCode().equals( getIndEarlyRedemption() )){
			return true;
		}
		return false;
	}
	/* Early Redemption End */
	
	/* Issuance form start */
	
	/**
	 * Checks if is desmaterialized issuance form.
	 *
	 * @return true, if is desmaterialized issuance form
	 */
	public boolean isDesmaterializedIssuanceForm(){
		if(getIssuanceForm()!=null
				&& getIssuanceForm().equals(  IssuanceType.DEMATERIALIZED.getCode() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is physical issuance form.
	 *
	 * @return true, if is physical issuance form
	 */
	public boolean isPhysicalIssuanceForm(){
		if(getIssuanceForm()!=null
				&& getIssuanceForm().equals(  IssuanceType.PHYSICAL.getCode())){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is mixed issuance form.
	 *
	 * @return true, if is mixed issuance form
	 */
	public boolean isMixedIssuanceForm() {
		if(IssuanceType.MIXED.getCode().equals( getIssuanceForm() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is mixed issue and fixed inst.
	 *
	 * @return true, if is mixed issue and fixed inst
	 */
	public boolean isMixedIssueAndFixedInst(){
		if(isMixedIssuanceForm() && isFixedInstrumentType()){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is have payment schedule no desm.
	 *
	 * @return true, if is have payment schedule no desm
	 */
	public boolean isHavePaymentScheduleNoDesm(){
		if((isMixedIssuanceForm() || isPhysicalIssuanceForm()) && isFixedInstrumentType()){
			return true;
		}
		return false;
	}
		
	/* Issuance form end */	
	
	/*Interest Periodicity Start*/
	
	/**
	 * Checks if is by days interest periodicity.
	 *
	 * @return true, if is by days interest periodicity
	 */
	public boolean isByDaysInterestPeriodicity(){
		if(InterestPeriodicityType.BY_DAYS.getCode().equals( getPeriodicity() )){
			return true;
		}
		return false;
	}
	
	public boolean isIndistinctPeriodicity(){
		if(InterestPeriodicityType.INDISTINCT.getCode().equals( getPeriodicity() )){
			return true;
		}
		return false;
	}	
	
	/**
	 * Checks if is indistinct amortization periodicity.
	 *
	 * @return true, if is indistinct amortization periodicity
	 */
	public boolean isIndistinctAmortizationPeriodicity(){
		if(InterestPeriodicityType.INDISTINCT.getCode().equals( getAmortizationPeriodicity() )){
			return true;
		}
		return false;
	}
	
	/*Interest Periodicity End*/

	/*Amortization Periodicity Start*/
	
	/**
	 * Checks if is by days amortization periodicity.
	 *
	 * @return true, if is by days amortization periodicity
	 */
	public boolean isByDaysAmortizationPeriodicity(){
		if(InterestPeriodicityType.BY_DAYS.getCode().equals( amortizationPeriodicity )){
			return true;
		}
		return false;
	}
	
	/*Amortization Periodicity End*/
	
	/* Instrument Type start */
	
	/**
	 * Checks if is fixed instrument type.
	 *
	 * @return true, if is fixed instrument type
	 */
	public boolean isFixedInstrumentType(){
		if(InstrumentType.FIXED_INCOME.getCode().equals( getInstrumentType() )){
			return true;
		}
		return false;
	}
	
	public boolean isFixedInstrumentTypeAcc(){
		if(InstrumentType.FIXED_INCOME.getCode().equals( getInstrumentType() ) && 
				!(SecurityClassType.ACC_RF.getCode().equals( getSecurityClass() ))){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is variable instrument type.
	 *
	 * @return true, if is variable instrument type
	 */
	public boolean isVariableInstrumentType(){
		if(InstrumentType.VARIABLE_INCOME.getCode().equals( getInstrumentType() )){
			return true;
		}
		return false;
	}
	
	public boolean isVariableInstrumentTypeAcc(){
		if(InstrumentType.VARIABLE_INCOME.getCode().equals( getInstrumentType() ) || 
				(SecurityClassType.ACC_RF.getCode().equals(getSecurityClass()))){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is mixed instrument type.
	 *
	 * @return true, if is mixed instrument type
	 */
	public boolean isMixedInstrumentType(){
		if(InstrumentType.MIXED_INCOME.getCode().equals( getInstrumentType() )){
			return true;
		}
		return false;
	}
	
	/* Instrument Type end */
		
	/*payment modality start*/
	
	/**
	 * Checks if is partial amort payment modality.
	 *
	 * @return true, if is partial amort payment modality
	 */
	public boolean isPartialAmortPaymentModality(){
		if(CapitalPaymentModalityType.PARTIAL.getCode().equals( getCapitalPaymentModality() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is at maturity amort payment modality.
	 *
	 * @return true, if is at maturity amort payment modality
	 */
	public boolean isAtMaturityAmortPaymentModality(){
		if(CapitalPaymentModalityType.AT_MATURITY.getCode().equals( getCapitalPaymentModality() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is by coupon int payment modality.
	 *
	 * @return true, if is by coupon int payment modality
	 */
	public boolean isByCouponIntPaymentModality(){
		if(InterestPaymentModalityType.BY_COUPON.getCode().equals(  getInterestPaymentModality()  )  ){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is at maturity int payment modality.
	 *
	 * @return true, if is at maturity int payment modality
	 */
	public boolean isAtMaturityIntPaymentModality(){
		if(InterestPaymentModalityType.AT_MATURITY.getCode().equals(  getInterestPaymentModality() )){
			return true;
		}
		return false;
	}
	/*payment modality end*/
	
	/*Amortization type start*/
	
	/**
	 * Checks if is proportional amortization type.
	 *
	 * @return true, if is proportional amortization type
	 */
	public boolean isProportionalAmortizationType(){
		if(AmortizationType.PROPORTIONAL.getCode().equals(getAmortizationType()))
			return true;		
		return false;
	}
	
	/**
	 * Checks if is not proportional amortization type.
	 *
	 * @return true, if is not proportional amortization type
	 */
	public boolean isNotProportionalAmortizationType(){
		if(AmortizationType.NO_PROPORTIONAL.getCode().equals(getAmortizationType()))
			return true;
		return false;
	}
	
	/*Amortization type end*/

	/* Interest type start*/
	
		/**
	 * Checks if is fixed interest type.
	 *
	 * @return true, if is fixed interest type
	 */
	public boolean isFixedInterestType(){
			if(InterestType.FIXED.getCode().equals(getInterestType())){
				return true;
			}
			return false;
		}
	
		/**
		 * Checks if is mixed interest type.
		 *
		 * @return true, if is mixed interest type
		 */
		public boolean isMixedInterestType(){
			if(InterestType.MIXED.getCode().equals(getInterestType() )){
				return true;
			}
			return false;
		}
		
		/**
		 * Checks if is variable interest type.
		 *
		 * @return true, if is variable interest type
		 */
		public boolean isVariableInterestType(){
			if(InterestType.VARIABLE.getCode().equals( getInterestType() )){
				return true;
			}
			return false;
		}
		
		/**
		 * Checks if is cero interest type.
		 *
		 * @return true, if is cero interest type
		 */
		public boolean isCeroInterestType(){
			if(InterestType.CERO.getCode().equals( getInterestType() )){
				return true;
			}
			return false;
		}
		
	public boolean isCeroInterestBySecuriyClass(){

		if(getSecurityClass()!=null){
			if(SecurityClassType.CDS.getCode().equals( getSecurityClass() ) || 
					SecurityClassType.LTS.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.CUP.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.LBS.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.CDB.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.CDD.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.LCB.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.LTC.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.NCF.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.CDI.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.CNC.getCode().equals( getSecurityClass() ) ||
					SecurityClassType.NCM.getCode().equals( getSecurityClass() )){
				return true;
			}
		}
		return false;
	}
		
	/* Interest type end*/
	
	/* Security Type */
	
	/**
	 * Checks if is part quote security type.
	 *
	 * @return true, if is part quote security type
	 */
	public boolean isPartQuoteSecurityType(){
		if(SecurityType.PAR_QUO.getCode().equals( getSecurityType() )){
			return true;
		}
		return false;
	}
	
	/* Security Type */
	
	/* Security Term Start */
	
	/**
	 * Checks if is short security term.
	 *
	 * @return true, if is short security term
	 */
	public boolean isShortSecurityTerm(){
		if(SecurityTermType.SHORT_TERM_INS.getCode().equals( getSecurityTerm() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is medium security term.
	 *
	 * @return true, if is medium security term
	 */
	public boolean isMediumSecurityTerm(){
		if(SecurityTermType.MEDIUM_TERM_INS.getCode().equals( getSecurityTerm() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is long security term.
	 *
	 * @return true, if is long security term
	 */
	public boolean isLongSecurityTerm(){
		if(SecurityTermType.LONG_TERM_INS.getCode().equals( getSecurityTerm() )){
			return true;
		}
		return false;
	}
	
	/* Security Term End */
	
	/* Interest Class Start */
	/**
	 * Checks if is effective interest class.
	 *
	 * @return true, if is effective interest class
	 */
	public boolean isEffectiveInterestClass(){
		if(InterestClassType.EFFECTIVE.getCode().equals( getCashNominal() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is nominal interest class.
	 *
	 * @return true, if is nominal interest class
	 */
	public boolean isNominalInterestClass(){
		if(InterestClassType.NOMINAL.getCode().equals( getCashNominal() )){
			return true;
		}
		return false;
	}
	
	/* Interest Class End */
	
	/* Security Source Start */
	
	/**
	 * Checks if is foreign security source.
	 *
	 * @return true, if is foreign security source
	 */
	public boolean isForeignSecuritySource(){
		if(SecuritySourceType.FOREIGN.getCode().equals( getSecuritySource() )){
			return true;
		}
		return false;
	}
	
	/* Security Source End */
	
	/* Calendar Type Start */
	
	/**
	 * Checks if is commercial calendar type.
	 *
	 * @return true, if is commercial calendar type
	 */
	public boolean isCommercialCalendarType(){
		if(CalendarType.COMMERCIAL.getCode().equals( getCalendarType() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is calendar calendar type.
	 *
	 * @return true, if is calendar calendar type
	 */
	public boolean isCalendarCalendarType(){
		if(CalendarType.CALENDAR.getCode().equals( getCalendarType() )){
			return true;
		}
		return false;
	}
	
	/* Calendar Type End */
	
	/* Calendar Month Start */
	
	/**
	 * Checks if is _30 calendar month.
	 *
	 * @return true, if is _30 calendar month
	 */
	public boolean is_30CalendarMonth(){
		if(CalendarMonthType._30.getCode().equals( getCalendarMonth() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is actual calendar month.
	 *
	 * @return true, if is actual calendar month
	 */
	public boolean isActualCalendarMonth(){
		if(CalendarMonthType.ACTUAL.getCode().equals( getCalendarMonth() )){
			return true;
		}
		return false;
	}
	
	/* Calendar Month End */
	
	/* Calendar Day */
	
	/**
	 * Checks if is _360 calendar day.
	 *
	 * @return true, if is _360 calendar day
	 */
	public boolean is_360CalendarDay(){
		if(CalendarDayType._360.getCode().equals( getCalendarDays() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is _365 calendar day.
	 *
	 * @return true, if is _365 calendar day
	 */
	public boolean is_365CalendarDay(){
		if(CalendarDayType._365.getCode().equals( getCalendarDays() )){
			return true;
		}
		return false;
	}
	
	/* Calendar Day */
	
	/* State start */
	
	/**
	 * Checks if is registered state.
	 *
	 * @return true, if is registered state
	 */
	public boolean isRegisteredState(){
		if(SecurityStateType.REGISTERED.getCode().equals( this.getStateSecurity() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Check if is guarda exclusive state
	 * 
	 * @return
	 */
	public boolean isGuardaExclusive() {
		if(SecurityStateType.GUARDA_EXCLUSIVE.getCode().equals(this.getStateSecurity())) {
			return true;
		}
		return false;
	}
	
	/* State end*/
	
	/**
	 * Checks if is national security source.
	 *
	 * @return true, if is national security source
	 */
	public boolean isNationalSecuritySource(){
		if(SecuritySourceType.NATIONAL.getCode().equals( this.getSecuritySource() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is actual calendar day.
	 *
	 * @return true, if is actual calendar day
	 */
	public boolean isActualCalendarDay(){
		if(CalendarDayType.ACTUAL.getCode().equals( getCalendarDays() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive(){
		if(isOnGoing() || isInNegotiations()){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is on going.
	 *
	 * @return true, if is on going
	 */
	public boolean isOnGoing(){
		if(this.issuanceDate!=null){
			Date securityBeginDate=truncateDateTime(this.issuanceDate);
			Date currentDate=truncateDateTime(new Date());		
			if(securityBeginDate.equals(currentDate) || securityBeginDate.before( currentDate )){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if is extended term.
	 *
	 * @return true, if is extended term
	 */
	public boolean isExtendedTerm(){
		if(BooleanType.YES.getCode().equals(getIndExtendedTerm())){
			return true;
		}
		return false;
	}
	
	public boolean isConvertibleStock(){
		if(BooleanType.YES.getCode().equals( getIndConvertibleStock() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the id security code pk split.
	 *
	 * @return the id security code pk split
	 */
	public String getIdSecurityCodePkSplit(){
		if(Validations.validateIsNotNullAndNotEmpty(this.getIdSecurityCodePk()) 
		    && (this.getIdSecurityCodePk().length()>4 && this.getIdSecurityCodePk().charAt(4)=='4')){
			return this.getIdSecurityCodePk().substring(4);
		}
		return getIdSecurityCodePk();
	}
		
	/**
	 * Truncate date time.
	 *
	 * @param truncateDate the truncate date
	 * @return the date
	 */
	public static Date truncateDateTime(Date truncateDate) {
		Calendar cal=Calendar.getInstance();
		cal.setTime(truncateDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		LoggerUser objLoggerUser = LoggerUserConstants.getLoggerUser();	
		if(loggerUser != null) {			
			if(loggerUser.getIdPrivilegeOfSystem() != null){
				lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			} else {
				lastModifyApp = LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM;
				loggerUser.setIdPrivilegeOfSystem(LoggerUserConstants.ID_PRIVILEGE_OF_SYSTEM);
			}
            if(loggerUser.getAuditTime() != null){
            	lastModifyDate = loggerUser.getAuditTime();
            } else {
            	lastModifyDate = objLoggerUser.getAuditTime();
            	loggerUser.setAuditTime(objLoggerUser.getAuditTime());
            }
            if(loggerUser.getIpAddress() != null){
            	lastModifyIp = loggerUser.getIpAddress();
            } else {
            	lastModifyIp = objLoggerUser.getIpAddress();
            	loggerUser.setIpAddress(objLoggerUser.getIpAddress());
            }
            if(loggerUser.getUserName() != null) {
            	lastModifyUser = loggerUser.getUserName();
            } else {
            	lastModifyUser = objLoggerUser.getUserName();
            	loggerUser.setUserName(objLoggerUser.getUserName());
            }                        
            if(amortizationPaymentSchedule != null){
            	amortizationPaymentSchedule.setAudit(loggerUser);
            }
            if(interestPaymentSchedule != null){
            	interestPaymentSchedule.setAudit(loggerUser);
            }
            if(securityInvestor != null){
            	securityInvestor.setAudit(loggerUser);
            }
		} else {					
			lastModifyApp = objLoggerUser.getIdPrivilegeOfSystem();						
			lastModifyDate = objLoggerUser.getAuditTime();						
			lastModifyIp = objLoggerUser.getIpAddress();						
			lastModifyUser = objLoggerUser.getUserName();					
			if(amortizationPaymentSchedule != null){
            	amortizationPaymentSchedule.setAudit(objLoggerUser);
            }
            if(interestPaymentSchedule != null){
            	interestPaymentSchedule.setAudit(objLoggerUser);
            }
            if(securityInvestor != null){
            	securityInvestor.setAudit(objLoggerUser);
            }			
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
        HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("securityForeignDepositories", securityForeignDepositories);
        detailsMap.put("securityNegotiationMechanisms", securityNegotiationMechanisms);
        return detailsMap;
	}

	/**
	 * Instantiates a new security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 */
	public Security(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Instantiates a new security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param stateSecurity the state security
	 */
	public Security(String idSecurityCodePk, Integer stateSecurity) {
		this.idSecurityCodePk = idSecurityCodePk;
		this.stateSecurity = stateSecurity;
	}
	
	/**
	 * Gets the corporative source type.
	 *
	 * @return the corporative source type
	 */
	public Integer getCorporativeSourceType() {
		return corporativeSourceType;
	}

	/**
	 * Sets the corporative source type.
	 *
	 * @param corporativeSourceType the new corporative source type
	 */
	public void setCorporativeSourceType(Integer corporativeSourceType) {
		this.corporativeSourceType = corporativeSourceType;
	}
	
	/**
	 * Gets the corporative source type description.
	 *
	 * @return the corporative source type description
	 */
	public String getCorporativeSourceTypeDescription() {
		if(corporativeSourceType!=null){
			corporativeSourceTypeDescription = CorporativeSecuritySourceType.get(corporativeSourceType).getValue();
		}
		else{
			corporativeSourceTypeDescription = "";
		}
		return corporativeSourceTypeDescription;
	}

	/**
	 * Sets the corporative source type description.
	 *
	 * @param corporativeSourceTypeDescription the new corporative source type description
	 */
	public void setCorporativeSourceTypeDescription(
			String corporativeSourceTypeDescription) {
		this.corporativeSourceTypeDescription = corporativeSourceTypeDescription;
	}

	/**
	 * Gets the ind has split securities.
	 *
	 * @return the ind has split securities
	 */
	public Integer getIndHasSplitSecurities() {
		return indHasSplitSecurities;
	}

	/**
	 * Sets the ind has split securities.
	 *
	 * @param indHasSplitSecurities the new ind has split securities
	 */
	public void setIndHasSplitSecurities(Integer indHasSplitSecurities) {
		this.indHasSplitSecurities = indHasSplitSecurities;
	}

	/**
	 * Gets the ind holder detail.
	 *
	 * @return the ind holder detail
	 */
	public Integer getIndHolderDetail() {
		return indHolderDetail;
	}

	/**
	 * Sets the ind holder detail.
	 *
	 * @param indHolderDetail the new ind holder detail
	 */
	public void setIndHolderDetail(Integer indHolderDetail) {
		this.indHolderDetail = indHolderDetail;
	}

	/**
	 * Gets the ind tax exempt.
	 *
	 * @return the ind tax exempt
	 */
	public Integer getIndTaxExempt() {
		return indTaxExempt;
	}

	/**
	 * Sets the ind tax exempt.
	 *
	 * @param indTaxExempt the new ind tax exempt
	 */
	public void setIndTaxExempt(Integer indTaxExempt) {
		this.indTaxExempt = indTaxExempt;
	}

	/**
	 * Gets the custody commission detail list.
	 *
	 * @return the custody commission detail list
	 */
	public List<CustodyCommissionDetail> getCustodyCommissionDetailList() {
		return custodyCommissionDetailList;
	}
	
	/**
	 * Sets the custody commission detail list.
	 *
	 * @param custodyCommissionDetailList the new custody commission detail list
	 */
	public void setCustodyCommissionDetailList(
			List<CustodyCommissionDetail> custodyCommissionDetailList) {
		this.custodyCommissionDetailList = custodyCommissionDetailList;
	}

	/**
	 * Gets the calendar month.
	 *
	 * @return the calendar month
	 */
	public Integer getCalendarMonth() {
		return calendarMonth;
	}

	/**
	 * Sets the calendar month.
	 *
	 * @param calendarMonth the new calendar month
	 */
	public void setCalendarMonth(Integer calendarMonth) {
		this.calendarMonth = calendarMonth;
	}

	/**
	 * Gets the security gvc lists.
	 *
	 * @return the securityGvcLists
	 */
	public List<SecurityGvcList> getSecurityGvcLists() {
		return securityGvcLists;
	}

	/**
	 * Sets the security gvc lists.
	 *
	 * @param securityGvcLists the securityGvcLists to set
	 */
	public void setSecurityGvcLists(List<SecurityGvcList> securityGvcLists) {
		this.securityGvcLists = securityGvcLists;
	}

	/**
	 * Gets the ind is detached.
	 *
	 * @return the ind is detached
	 */
	public Integer getIndIsDetached() {
		return indIsDetached;
	}

	/**
	 * Sets the ind is detached.
	 *
	 * @param indIsDetached the new ind is detached
	 */
	public void setIndIsDetached(Integer indIsDetached) {
		this.indIsDetached = indIsDetached;
	}

	/**
	 * Gets the rate value.
	 *
	 * @return the rate value
	 */
	public BigDecimal getRateValue() {
		return rateValue;
	}

	/**
	 * Sets the rate value.
	 *
	 * @param rateValue the new rate value
	 */
	public void setRateValue(BigDecimal rateValue) {
		this.rateValue = rateValue;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	

	/**
	 * Gets the periodicity days.
	 *
	 * @return the periodicity days
	 */
	public Integer getPeriodicityDays() {
		return periodicityDays;
	}

	/**
	 * Sets the periodicity days.
	 *
	 * @param periodicityDays the new periodicity days
	 */
	public void setPeriodicityDays(Integer periodicityDays) {
		this.periodicityDays = periodicityDays;
	}

	/**
	 * Gets the ind subordinated.
	 *
	 * @return the ind subordinated
	 */
	public Integer getIndSubordinated() {
		return indSubordinated;
	}

	/**
	 * Sets the ind subordinated.
	 *
	 * @param indSubordinated the new ind subordinated
	 */
	public void setIndSubordinated(Integer indSubordinated) {
		this.indSubordinated = indSubordinated;
	}
	
	

	/**
	 * Gets the security days term.
	 *
	 * @return the security days term
	 */
	public Integer getSecurityDaysTerm() {
		return securityDaysTerm;
	}

	/**
	 * Sets the security days term.
	 *
	 * @param securityDaysTerm the new security days term
	 */
	public void setSecurityDaysTerm(Integer securityDaysTerm) {
		this.securityDaysTerm = securityDaysTerm;
	}
		
	/**
	 * Gets the amortization periodicity days.
	 *
	 * @return the amortization periodicity days
	 */
	public Integer getAmortizationPeriodicityDays() {
		return amortizationPeriodicityDays;
	}

	/**
	 * Sets the amortization periodicity days.
	 *
	 * @param amortizationPeriodicityDays the new amortization periodicity days
	 */
	public void setAmortizationPeriodicityDays(Integer amortizationPeriodicityDays) {
		this.amortizationPeriodicityDays = amortizationPeriodicityDays;
	}
	
	

	/**
	 * Checks if is in negotiations.
	 *
	 * @return true, if is in negotiations
	 */
	public boolean isInNegotiations() {
		return inNegotiations;
	}

	/**
	 * Sets the in negotiations.
	 *
	 * @param inNegotiations the new in negotiations
	 */
	public void setInNegotiations(boolean inNegotiations) {
		this.inNegotiations = inNegotiations;
	}
	
	

	/**
	 * Gets the security term.
	 *
	 * @return the security term
	 */
	public Integer getSecurityTerm() {
		return securityTerm;
	}

	/**
	 * Sets the security term.
	 *
	 * @param securityTerm the new security term
	 */
	public void setSecurityTerm(Integer securityTerm) {
		this.securityTerm = securityTerm;
	}

	
	
	/**
	 * Gets the payment agent.
	 *
	 * @return the payment agent
	 */
	public String getPaymentAgent() {
		return paymentAgent;
	}

	/**
	 * Sets the payment agent.
	 *
	 * @param paymentAgent the new payment agent
	 */
	public void setPaymentAgent(String paymentAgent) {
		this.paymentAgent = paymentAgent;
	}

	public String getFsinCode() {
		return fsinCode;
	}

	public void setFsinCode(String fsinCode) {
		this.fsinCode = fsinCode;
	}
	
	/**
	 * Gets the payment requirement.
	 *
	 * @return the payment requirement
	 */
	public String getPaymentRequirement() {
		return paymentRequirement;
	}

	/**
	 * Sets the payment requirement.
	 *
	 * @param paymentRequirement the new payment requirement
	 */
	public void setPaymentRequirement(String paymentRequirement) {
		this.paymentRequirement = paymentRequirement;
	}

	/**
	 * Gets the payment first expiration date.
	 *
	 * @return the payment first expiration date
	 */
	public Date getPaymentFirstExpirationDate() {
		return paymentFirstExpirationDate;
	}

	/**
	 * Sets the payment first expiration date.
	 *
	 * @param paymentFirstExpirationDate the new payment first expiration date
	 */
	public void setPaymentFirstExpirationDate(Date paymentFirstExpirationDate) {
		this.paymentFirstExpirationDate = paymentFirstExpirationDate;
	}

	/**
	 * Sets the security type description.
	 *
	 * @param securityTypeDescription the new security type description
	 */
	public void setSecurityTypeDescription(String securityTypeDescription) {
		this.securityTypeDescription = securityTypeDescription;
	}
	
	

	/**
	 * Gets the class type description.
	 *
	 * @return the class type description
	 */
	public String getClassTypeDescription() {
		return classTypeDescription;
	}

	/**
	 * Sets the class type description.
	 *
	 * @param classTypeDescription the new class type description
	 */
	public void setClassTypeDescription(String classTypeDescription) {
		this.classTypeDescription = classTypeDescription;
	}
	
	

	/**
	 * Gets the ind extended term.
	 *
	 * @return the ind extended term
	 */
	public Integer getIndExtendedTerm() {
		return indExtendedTerm;
	}

	/**
	 * Sets the ind extended term.
	 *
	 * @param indExtendedTerm the new ind extended term
	 */
	public void setIndExtendedTerm(Integer indExtendedTerm) {
		this.indExtendedTerm = indExtendedTerm;
	}
	
	public Integer getIndAuthorized() {
		return indAuthorized;
	}

	public void setIndAuthorized(Integer indAuthorized) {
		this.indAuthorized = indAuthorized;
	}
	
	public Date getAuthorizedDate() {
		return authorizedDate;
	}

	public void setAuthorizedDate(Date authorizedDate) {
		this.authorizedDate = authorizedDate;
	}

	public String getAuthorizedUser() {
		return authorizedUser;
	}

	public void setAuthorizedUser(String authorizedUser) {
		this.authorizedUser = authorizedUser;
	}

	public String getAuthorizedName() {
		return authorizedName;
	}

	public void setAuthorizedName(String authorizedName) {
		this.authorizedName = authorizedName;
	}

	/**
	 * Gets the id security code only.
	 *
	 * @return the id security code only
	 */
	public String getIdSecurityCodeOnly() {
		return idSecurityCodeOnly;
	}

	/**
	 * Sets the id security code only.
	 *
	 * @param idSecurityCodeOnly the new id security code only
	 */
	public void setIdSecurityCodeOnly(String idSecurityCodeOnly) {
		this.idSecurityCodeOnly = idSecurityCodeOnly;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		try {
		return "Security [idSecurityCodePk=" + idSecurityCodePk
				+ ", idIsinCode=" + idIsinCode + ", alternativeCode="
				+ alternativeCode + ", amortizationAmount="
				+ amortizationAmount + ", amortizationFactor="
				+ amortizationFactor + ", amortizationType=" + amortizationType
				+ ", amortizationPeriodicity=" + amortizationPeriodicity
				+ ", calendarDays=" + calendarDays + ", calendarType="
				+ calendarType + ", calendarMonth=" + calendarMonth
				+ ", cfiCode=" + cfiCode + ", circulationAmount="
				+ circulationAmount + ", circulationBalance="
				+ circulationBalance + ", corporativeProcessDays="
				+ corporativeProcessDays + ", currency=" + currency
				+ ", amortizationOn=" + amortizationOn + ", currencyName="
				+ currencyName + ", currentNominalValue=" + currentNominalValue
				+ ", depositRegistryDate=" + depositRegistryDate
				+ ", description=" + description + ", desmaterializedBalance="
				+ desmaterializedBalance +  ", expirationDate=" + expirationDate
				+ ", financialIndex=" + financialIndex + ", idGroupFk="
				+ idGroupFk 
				+ ", indConvertibleStock=" + indConvertibleStock
				+ ", indEarlyRedemption=" + indEarlyRedemption
				+ ", indIsCoupon=" + indIsCoupon + ", indIsDetached="
				+ indIsDetached + ", indIsFractionable=" + indIsFractionable
				+ ", indIssuanceManagement=" + indIssuanceManagement
				+ ", indPaymentBenefit=" + indPaymentBenefit
				+ ", indReceivableCustody=" + indReceivableCustody
				+ ", indSecuritization=" + indSecuritization
				+ ", indSplitCoupon=" + indSplitCoupon
				+ ", indTraderSecondary=" + indTraderSecondary
				+ ", indCapitalizableInterest=" + indCapitalizableInterest
				+ ", securityMonthsTerm=" + securityMonthsTerm
				+ ", securityDaysTerm=" + securityDaysTerm
				+ ", capitalPaymentModality=" + capitalPaymentModality
				+ ", initialNominalValue=" + initialNominalValue
				+ ", instrumentType=" + instrumentType + ", interestFactor="
				+ interestFactor + ", interestPaymentModality="
				+ interestPaymentModality + ", interestRate=" + interestRate
				+ ", interestRatePeriodicity=" + interestRatePeriodicity
				+ ", interestType=" + interestType + ", issuanceCountry="
				+ issuanceCountry + ", issuanceDate=" + issuanceDate
				+ ", issuanceForm=" + issuanceForm + ", lastModifyApp="
				+ lastModifyApp + ", lastModifyDate=" + lastModifyDate
				+ ", numberCoupons=" + numberCoupons + ", lastModifyIp="
				+ lastModifyIp + ", lastModifyUser=" + lastModifyUser
				+ ", maximumInvesment=" + maximumInvesment
				+ ", minimumInvesment=" + minimumInvesment + ", minimumRate="
				+ minimumRate + ", maximumRate=" + maximumRate + ", mnemonic="
				+ mnemonic + ", periodicityDays=" + periodicityDays
				+ ", amortizationPeriodicityDays="
				+ amortizationPeriodicityDays + ", periodicity=" + periodicity
				+ ", physicalBalance=" + physicalBalance + ", placedAmount="
				+ placedAmount + ", placedBalance=" + placedBalance
				+ ", registryUser=" + registryUser + ", retirementDate="
				+ retirementDate + ", securityClass=" + securityClass
				+ ", securitySerial=" + securitySerial + ", securitySource="
				+ securitySource + ", securityType=" + securityType
				+ ", serialProgram=" + serialProgram + ", shareBalance="
				+ shareBalance + ", shareCapital=" + shareCapital
				+ ", stateSecurity=" + stateSecurity + ", stockRegistryDays="
				+ stockRegistryDays + ", registryDate=" + registryDate
				+ ", yield=" + yield + ", spread=" + spread + ", indexed="
				+ indexed + ", couponFirstExpirationDate="
				+ couponFirstExpirationDate + ", cashNominal=" + cashNominal
				+ ", rateType=" + rateType + ", rateValue=" + rateValue
				+ ", operationType=" + operationType + ", securityTerm="
				+ securityTerm + ", issuance=" + issuance + ", issuer="
				+ issuer + ", security=" + security + ", securities="
				+ securities + ", securityForeignDepositories="
				+ securityForeignDepositories + ", securityHistoryBalances="
				+ securityHistoryBalances + ", securityHistoryStates="
				+ securityHistoryStates + ", securityNegotiationMechanisms="
				+ securityNegotiationMechanisms + ", holderAccountBalances="
				+ holderAccountBalances + ", placementSegmentDetailList="
				+ placementSegmentDetailList + ", custodyCommissionDetailList="
				+ custodyCommissionDetailList + ", interestPaymentSchedule="
				+ interestPaymentSchedule + ", amortizationPaymentSchedule="
				+ amortizationPaymentSchedule + ", securityInvestor="
				+ securityInvestor + ", securityGvcLists=" + securityGvcLists
				+ ", selected=" + selected + ", instrumentTypeDescription="
				+ currencyTypeDescription + ", interestTypeDescription="
				+ interestTypeDescription + ", inscriptionDate="
				+ inscriptionDate + ", corporativeSourceType="
				+ corporativeSourceType + ", corporativeSourceTypeDescription="
				+ corporativeSourceTypeDescription + ", inNegotiations="
				+ inNegotiations + ", indHasSplitSecurities="
				+ indHasSplitSecurities + ", indTaxExempt=" + indTaxExempt
				+ ", indHolderDetail=" + indHolderDetail + ", indSubordinated="
				+ indSubordinated + ", paymentAgent=" + paymentAgent
				+ ", paymentRequirement=" + paymentRequirement + "]";
		} catch (LazyInitializationException e) {
			return "";
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Security clone() throws CloneNotSupportedException{
		return (Security)super.clone();
	}

	/**
	 * Gets the security class desc.
	 *
	 * @return the security class desc
	 */
	public String getSecurityClassDesc() {
		return securityClassDesc;
	}

	/**
	 * Sets the security class desc.
	 *
	 * @param securityClassDesc the new security class desc
	 */
	public void setSecurityClassDesc(String securityClassDesc) {
		this.securityClassDesc = securityClassDesc;
	}

	/**
	 * Gets the periodicity desc.
	 *
	 * @return the periodicity desc
	 */
	public String getPeriodicityDesc() {
		return periodicityDesc;
	}

	/**
	 * Sets the periodicity desc.
	 *
	 * @param periodicityDesc the new periodicity desc
	 */
	public void setPeriodicityDesc(String periodicityDesc) {
		this.periodicityDesc = periodicityDesc;
	}
	
	/**
	 * Gets the checks for chronogram.
	 *
	 * @return the checks for chronogram
	 */
	public String getHasChronogram() {
		if(interestPaymentSchedule!=null){
			hasChronogram = BooleanType.YES.getValue();
		}else{
			hasChronogram = BooleanType.NO.getValue();
		}

		return hasChronogram;
	}

	/**
	 * Sets the checks for chronogram.
	 *
	 * @param hasChronogram the new checks for chronogram
	 */
	public void setHasChronogram(String hasChronogram) {
		this.hasChronogram = hasChronogram;
	}

	/**
	 * Gets the interest payment modality desc.
	 *
	 * @return the interest payment modality desc
	 */
	public String getInterestPaymentModalityDesc() {
		return interestPaymentModalityDesc;
	}

	/**
	 * Sets the interest payment modality desc.
	 *
	 * @param interestPaymentModalityDesc the new interest payment modality desc
	 */
	public void setInterestPaymentModalityDesc(String interestPaymentModalityDesc) {
		this.interestPaymentModalityDesc = interestPaymentModalityDesc;
	}

	/**
	 * Gets the id subproduct ref fk.
	 *
	 * @return the id subproduct ref fk
	 */
	public Security getIdSubproductRefFk() {
		return idSubproductRefFk;
	}

	/**
	 * Sets the id subproduct ref fk.
	 *
	 * @param idSubproductRefFk the new id subproduct ref fk
	 */
	public void setIdSubproductRefFk(Security idSubproductRefFk) {
		this.idSubproductRefFk = idSubproductRefFk;
	}

	/**
	 * Instantiates a new security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param valuatorProcessClass the valuator process class
	 */
	public Security(String idSecurityCodePk, String valuatorProcessClass) {
		super();
		this.idSecurityCodePk = idSecurityCodePk;
		this.valuatorProcessClass = new ValuatorProcessClass(valuatorProcessClass);
	}

	/**
	 * Gets the valuator process class.
	 *
	 * @return the valuator process class
	 */
	public ValuatorProcessClass getValuatorProcessClass() {
		return valuatorProcessClass;
	}

	/**
	 * Sets the valuator process class.
	 *
	 * @param valuatorProcessClass the new valuator process class
	 */
	public void setValuatorProcessClass(ValuatorProcessClass valuatorProcessClass) {
		this.valuatorProcessClass = valuatorProcessClass;
	}
	
	/**
	 * Checks if is bank stock exchange.
	 *
	 * @return true, if is bank stock exchange
	 */
	public boolean isBankStockExchange() {
		if( (SecurityClassType.ACC.getCode().equals( getSecurityClass() ) ||
				SecurityClassType.ACE.getCode().equals( getSecurityClass() )) &&
				EconomicActivityType.BANCOS.getCode().equals(issuer.getEconomicActivity())){
			inBankStockExchange=true;
		}
		else
			inBankStockExchange=false;
		return inBankStockExchange;		
	}
	
	/**
	 * Checks if is shares security type.
	 *
	 * @return true, if is shares security type
	 */
	public boolean isSharesSecurityType() {
		if( SecurityType.SHARES.getCode().equals( getSecurityType() )){
			return true;
		}
		return false;		
	}
	
	/**
	 * Gets the available physical balance.
	 *
	 * @return the available physical balance
	 */
	public BigDecimal getAvailablePhysicalBalance() {		
		if(Validations.validateIsNullOrEmpty(physicalDepositBalance)){
			physicalDepositBalance = BigDecimal.ZERO;
		}
		if(Validations.validateIsNotNull(circulationBalance) 
				&& Validations.validateIsNotNull(physicalDepositBalance)){
			availablePhysicalBalance=new BigDecimal(circulationBalance.intValue()-physicalDepositBalance.intValue());
		}else{
			availablePhysicalBalance= new BigDecimal(0);
		}
		return availablePhysicalBalance;
	}

	/**
	 * Sets the available physical balance.
	 *
	 * @param availablePhysicalBalance the new available physical balance
	 */
	public void setAvailablePhysicalBalance(BigDecimal availablePhysicalBalance) {
		this.availablePhysicalBalance = availablePhysicalBalance;
	}

	/**
	 * Gets the list valuator security code.
	 *
	 * @return the list valuator security code
	 */
	public List<ValuatorSecurityCode> getListValuatorSecurityCode() {
		return listValuatorSecurityCode;
	}

	/**
	 * Sets the list valuator security code.
	 *
	 * @param listValuatorSecurityCode the new list valuator security code
	 */
	public void setListValuatorSecurityCode(
			List<ValuatorSecurityCode> listValuatorSecurityCode) {
		this.listValuatorSecurityCode = listValuatorSecurityCode;
	}

	/**
	 * Gets the physical deposit balance.
	 *
	 * @return the physical deposit balance
	 */
	public BigDecimal getPhysicalDepositBalance() {
		return physicalDepositBalance;
	}

	/**
	 * Sets the physical deposit balance.
	 *
	 * @param physicalDepositBalance the new physical deposit balance
	 */
	public void setPhysicalDepositBalance(BigDecimal physicalDepositBalance) {
		this.physicalDepositBalance = physicalDepositBalance;
	}
	

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public BigDecimal getYieldRate() {
		return yieldRate;
	}

	public void setYieldRate(BigDecimal yieldRate) {
		this.yieldRate = yieldRate;
	}

	
	
	public Integer getIndPrepaid() {
		return indPrepaid;
	}

	public void setIndPrepaid(Integer indPrepaid) {
		this.indPrepaid = indPrepaid;
	}

	/**
	 * Gets the bol cfi code.
	 *
	 * @return the bol cfi code
	 */
	public boolean getBolCfiCode() {
		return bolCfiCode;
	}

	/**
	 * Sets the bol cfi code.
	 *
	 * @param bolCfiCode the new bol cfi code
	 */
	public void setBolCfiCode(boolean bolCfiCode) {
		this.bolCfiCode = bolCfiCode;
	}

	public Security getSecurityOrigin() {
		return securityOrigin;
	}

	public void setSecurityOrigin(Security securityOrigin) {
		this.securityOrigin = securityOrigin;
	}

	public Integer getIdSecurityClassTrg() {
		return idSecurityClassTrg;
	}

	public void setIdSecurityClassTrg(Integer idSecurityClassTrg) {
		this.idSecurityClassTrg = idSecurityClassTrg;
	}

	public Integer getConvertibleStockType() {
		return convertibleStockType;
	}

	public void setConvertibleStockType(Integer convertibleStockType) {
		this.convertibleStockType = convertibleStockType;
	}

	public boolean isInBankStockExchange() {
		return inBankStockExchange;
	}

	public void setInBankStockExchange(boolean inBankStockExchange) {
		this.inBankStockExchange = inBankStockExchange;
	}

	public String getIdSecurityBcbCode() {
		return idSecurityBcbCode;
	}

	public void setIdSecurityBcbCode(String idSecurityBcbCode) {
		this.idSecurityBcbCode = idSecurityBcbCode;
	}

	public BigDecimal getExchangeRelaTargetSecurity() {
		return exchangeRelaTargetSecurity;
	}

	public void setExchangeRelaTargetSecurity(BigDecimal exchangeRelaTargetSecurity) {
		this.exchangeRelaTargetSecurity = exchangeRelaTargetSecurity;
	}

	public BigDecimal getExchangeRelaSourceSecurity() {
		return exchangeRelaSourceSecurity;
	}

	public void setExchangeRelaSourceSecurity(BigDecimal exchangeRelaSourceSecurity) {
		this.exchangeRelaSourceSecurity = exchangeRelaSourceSecurity;
	}

	public BigDecimal getDeliveryFactorForCorporateOperation() {
		return deliveryFactorForCorporateOperation;
	}
 
	public void setDeliveryFactorForCorporateOperation(
			BigDecimal deliveryFactorForCorporateOperation) {
		this.deliveryFactorForCorporateOperation = deliveryFactorForCorporateOperation;
	}

	public BigDecimal getNewShareCapital() {
		return newShareCapital;
	}

	public void setNewShareCapital(BigDecimal newShareCapital) {
		this.newShareCapital = newShareCapital;
	}

	public BigDecimal getNewShareBalance() {
		return newShareBalance;
	}

	public void setNewShareBalance(BigDecimal newShareBalance) {
		this.newShareBalance = newShareBalance;
	}

	public BigDecimal getNewCirculationBalance() {
		return newCirculationBalance;
	}

	public void setNewCirculationBalance(BigDecimal newCirculationBalance) {
		this.newCirculationBalance = newCirculationBalance;
	}

	public BigDecimal getVariationAmount() {
		return variationAmount;
	}

	public void setVariationAmount(BigDecimal variationAmount) {
		this.variationAmount = variationAmount;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * @return the idSourceSecurityCodePk
	 */
	public String getIdSourceSecurityCodePk() {
		return idSourceSecurityCodePk;
	}

	/**
	 * @param idSourceSecurityCodePk the idSourceSecurityCodePk to set
	 */
	public void setIdSourceSecurityCodePk(String idSourceSecurityCodePk) {
		this.idSourceSecurityCodePk = idSourceSecurityCodePk;
	}

	/**
	 * @return the passiveInterestRate
	 */
	public BigDecimal getPassiveInterestRate() {
		return passiveInterestRate;
	}

	/**
	 * @param passiveInterestRate the passiveInterestRate to set
	 */
	public void setPassiveInterestRate(BigDecimal passiveInterestRate) {
		this.passiveInterestRate = passiveInterestRate;
	}

	/**
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}

	/**
	 * @param observations the observations to set
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}

	public Long getPendientDays() {
		return pendientDays;
	}

	public void setPendientDays(Long pendientDays) {
		this.pendientDays = pendientDays;
	}

	/**
	 * @return the physicalAvailable
	 */
	public BigDecimal getPhysicalAvailable() {
		return physicalAvailable;
	}

	/**
	 * @param physicalAvailable the physicalAvailable to set
	 */
	public void setPhysicalAvailable(BigDecimal physicalAvailable) {
		this.physicalAvailable = physicalAvailable;
	}

	/**
	 * @return the notTraded
	 */
	public Integer getNotTraded() {
		return notTraded;
	}

	/**
	 * @param notTraded the notTraded to set
	 */
	public void setNotTraded(Integer notTraded) {
		this.notTraded = notTraded;
	}

	/**
	 * Gets the ind payment reporting.
	 *
	 * @return the ind payment reporting
	 */
	public Integer getIndPaymentReporting() {
		return indPaymentReporting;
	}

	/**
	 * Sets the ind payment reporting.
	 *
	 * @param indPaymentReporting the new ind payment reporting
	 */
	public void setIndPaymentReporting(Integer indPaymentReporting) {
		this.indPaymentReporting = indPaymentReporting;
	}

	public String getNegociable() {
		negociable = "";
		// solo se mostrara si es o no negociable a los valores DPF o DPA
		if(notTraded != null && 
				(idSecurityCodePk.indexOf("DPF-") == 0 || idSecurityCodePk.indexOf("DPA-") == 0 ) ){
			if(notTraded == 1){
				negociable = "NO";
			} else {
				negociable = "SI";	
			}			
		}
		return negociable;
	}

	public void setNegociable(String negociable) {
		this.negociable = negociable;
	}

	public String getIdFractionSecurityCodePk() {
		return idFractionSecurityCodePk;
	}

	public void setIdFractionSecurityCodePk(String idFractionSecurityCodePk) {
		this.idFractionSecurityCodePk = idFractionSecurityCodePk;
	}

	public Integer getGeographicLocation() {
		return geographicLocation;
	}

	public void setGeographicLocation(Integer geographicLocation) {
		this.geographicLocation = geographicLocation;
	}

	public String getPlacePayment() {
		return placePayment;
	}

	public void setPlacePayment(String placePayment) {
		this.placePayment = placePayment;
	}

	public Integer getIndIssuanceProtest() {
		return indIssuanceProtest;
	}

	public void setIndIssuanceProtest(Integer indIssuanceProtest) {
		this.indIssuanceProtest = indIssuanceProtest;
	}

	/**
	 * @return the expirationFondoDate
	 */
	public Date getExpirationFondoDate() {
		return expirationFondoDate;
	}

	/**
	 * @param expirationFondoDate the expirationFondoDate to set
	 */
	public void setExpirationFondoDate(Date expirationFondoDate) {
		this.expirationFondoDate = expirationFondoDate;
	}

	public SecuritiesManager getSecuritiesManager() {
		return securitiesManager;
	}

	public void setSecuritiesManager(SecuritiesManager securitiesManager) {
		this.securitiesManager = securitiesManager;
	}

	public Integer getIndElectronicCupon() {
		return indElectronicCupon;
	}

	public void setIndElectronicCupon(Integer indElectronicCupon) {
		this.indElectronicCupon = indElectronicCupon;
	}

	public Integer getRepresentsNumberVotes() {
		return representsNumberVotes;
	}

	public void setRepresentsNumberVotes(Integer representsNumberVotes) {
		this.representsNumberVotes = representsNumberVotes;
	}

	public Integer getMotiveAnnotation() {
		return motiveAnnotation;
	}

	public void setMotiveAnnotation(Integer motiveAnnotation) {
		this.motiveAnnotation = motiveAnnotation;
	}

	public String getSubClass() {
		return subClass;
	}

	public void setSubClass(String subClass) {
		this.subClass = subClass;
	}

	public Integer getNumberShareInitial() {
		return numberShareInitial;
	}

	public void setNumberShareInitial(Integer numberShareInitial) {
		this.numberShareInitial = numberShareInitial;
	}

	public Integer getNumberShareFinal() {
		return numberShareFinal;
	}

	public void setNumberShareFinal(Integer numberShareFinal) {
		this.numberShareFinal = numberShareFinal;
	}

	public String getReferencePartCode() {
		return referencePartCode;
	}

	public void setReferencePartCode(String referencePartCode) {
		this.referencePartCode = referencePartCode;
	}

	public String getSerialRange() {
		return serialRange;
	}

	public void setSerialRange(String serialRange) {
		this.serialRange = serialRange;
	}

	public List<SecuritySerialRange> getSecuritySerialRange() {
		return securitySerialRange;
	}

	public void setSecuritySerialRange(List<SecuritySerialRange> securitySerialRange) {
		this.securitySerialRange = securitySerialRange;
	}

	public Integer getIndIsManaged() {
		return indIsManaged;
	}

	public void setIndIsManaged(Integer indIsManaged) {
		this.indIsManaged = indIsManaged;
	}

	public Boolean getModifyIssuance() {
		return modifyIssuance;
	}

	public void setModifyIssuance(Boolean modifyIssuance) {
		this.modifyIssuance = modifyIssuance;
	}

	public Boolean getStateExpirate() {
		return stateExpirate;
	}

	public void setStateExpirate(Boolean stateExpirate) {
		this.stateExpirate = stateExpirate;
	}

	public Integer getEncoderAgent() {
		return encoderAgent;
	}

	public void setEncoderAgent(Integer encoderAgent) {
		this.encoderAgent = encoderAgent;
	}

	
}