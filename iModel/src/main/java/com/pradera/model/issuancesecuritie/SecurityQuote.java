package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SECURITY database table.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
@Entity
@NamedQueries({ @NamedQuery(name = SecurityQuote.SECURITY_QUOTE, query = "SELECT new com.pradera.model.issuancesecuritie.SecurityQuote(s.idSecurityQuotePk, s.quoteState) FROM SecurityQuote s WHERE s.idSecurityQuotePk = :idSecQuoteParam") })
@Table(name = "SECURITY_QUOTE")
public class SecurityQuote implements Serializable, Auditable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant SECURITY_STATE. */
	public static final String SECURITY_QUOTE = "securityQuote.searchState";

	/** The id isin code pk. */
	@Id
	@SequenceGenerator(name="SQ_SECURITY_QUOTE_PK", sequenceName="SQ_ID_SECURITY_QUOTE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_SECURITY_QUOTE_PK")
	@Column(name = "ID_SECURITY_QUOTE_PK")
	private Long idSecurityQuotePk;
	
	/** The security. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SECURITY_CODE_FK")
	private Security security;
	
	/** The quote value price. */
	@Column(name = "QUOTE_VALUE_PRICE")
	private BigDecimal quoteValuePrice;
	
	/** The quote value before. */
	@Column(name = "QUOTE_VALUE_BEFORE")
	private BigDecimal quoteValueBefore;
	
	/** The quote state. */
	@Column(name = "QUOTE_STATE")
	private Integer quoteState;
	
	/** The quote date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "QUOTE_DATE")
	private Date quoteDate;
	
	/** The share balance. */
	@Column(name = "SHARE_BALANCE")
	private BigDecimal shareBalance;

	/** The share capital. */
	@Column(name = "SHARE_CAPITAL")
	private BigDecimal shareCapital;
	
	@Transient
	@Temporal(TemporalType.DATE)
	@Column(name = "QUOTE_DATE_BEFORE")
	private Date quoteDateBefore;
	
	/** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The registry user. */
	@Column(name = "REGISTRY_USER")
	private String registryUser;
	
	/** The registry user. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REGISTRY_DATE")
	private Date registryDate;
	
	/** The issuer. */
	@Transient
	private Issuer issuer;
	
	/** The last modify app. */
	@Column(name = "REQUEST_TYPE")
	private Integer requestType;
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
			lastModifyDate = loggerUser.getAuditTime();
			lastModifyIp = loggerUser.getIpAddress();
			lastModifyUser = loggerUser.getUserName();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the id security quote pk.
	 *
	 * @return the id security quote pk
	 */
	public Long getIdSecurityQuotePk() {
		return idSecurityQuotePk;
	}

	/**
	 * Sets the id security quote pk.
	 *
	 * @param idSecurityQuotePk the new id security quote pk
	 */
	public void setIdSecurityQuotePk(Long idSecurityQuotePk) {
		this.idSecurityQuotePk = idSecurityQuotePk;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the quote value price.
	 *
	 * @return the quote value price
	 */
	public BigDecimal getQuoteValuePrice() {
		return quoteValuePrice;
	}

	/**
	 * Sets the quote value price.
	 *
	 * @param quoteValuePrice the new quote value price
	 */
	public void setQuoteValuePrice(BigDecimal quoteValuePrice) {
		this.quoteValuePrice = quoteValuePrice;
	}

	/**
	 * Gets the quote value before.
	 *
	 * @return the quote value before
	 */
	public BigDecimal getQuoteValueBefore() {
		return quoteValueBefore;
	}

	/**
	 * Sets the quote value before.
	 *
	 * @param quoteValueBefore the new quote value before
	 */
	public void setQuoteValueBefore(BigDecimal quoteValueBefore) {
		this.quoteValueBefore = quoteValueBefore;
	}

	/**
	 * Gets the quote state.
	 *
	 * @return the quote state
	 */
	public Integer getQuoteState() {
		return quoteState;
	}

	/**
	 * Sets the quote state.
	 *
	 * @param quoteState the new quote state
	 */
	public void setQuoteState(Integer quoteState) {
		this.quoteState = quoteState;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Instantiates a new security quote.
	 *
	 * @param idSecurityQuotePk the id security quote pk
	 * @param quoteState the quote state
	 */
	public SecurityQuote(Long idSecurityQuotePk, Integer quoteState) {
		super();
		this.idSecurityQuotePk = idSecurityQuotePk;
		this.quoteState = quoteState;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Instantiates a new security quote.
	 */
	public SecurityQuote() {
		super();
	}

	/**
	 * Gets the quote date.
	 *
	 * @return the quote date
	 */
	public Date getQuoteDate() {
		return quoteDate;
	}

	/**
	 * Sets the quote date.
	 *
	 * @param quoteDate the new quote date
	 */
	public void setQuoteDate(Date quoteDate) {
		this.quoteDate = quoteDate;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	/**
	 * Gets the quote date before.
	 *
	 * @return the quote date before
	 */
	public Date getQuoteDateBefore() {
		return quoteDateBefore;
	}
	/**
	 * Sets the quote date before.
	 *
	 * @param quoteDateBefore the new quote date before
	 */
	public void setQuoteDateBefore(Date quoteDateBefore) {
		this.quoteDateBefore = quoteDateBefore;
	}

	public Integer getRequestType() {
		return requestType;
	}

	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	public BigDecimal getShareBalance() {
		return shareBalance;
	}

	public void setShareBalance(BigDecimal shareBalance) {
		this.shareBalance = shareBalance;
	}

	public BigDecimal getShareCapital() {
		return shareCapital;
	}

	public void setShareCapital(BigDecimal shareCapital) {
		this.shareCapital = shareCapital;
	}
	
}