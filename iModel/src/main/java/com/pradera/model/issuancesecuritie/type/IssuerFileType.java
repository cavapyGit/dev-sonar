package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum ParticipantDocumentType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public enum IssuerFileType {
	
	/** The rnc. */
	ISSUER_DEFAULT_DOCUMENT(Integer.valueOf(94));

	
	/** The code. */
	private Integer code;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
			
	/**
	 * Instantiates a new participant document type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuerFileType(Integer code) {
		this.code = code;
	}

	/** The Constant list. */
	public static final List<IssuerFileType> list = new ArrayList<IssuerFileType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, IssuerFileType> lookup = new HashMap<Integer, IssuerFileType>();
	static {
		for (IssuerFileType s : EnumSet.allOf(IssuerFileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant document type
	 */
	public static IssuerFileType get(Integer code) {
		return lookup.get(code);
	}

}
