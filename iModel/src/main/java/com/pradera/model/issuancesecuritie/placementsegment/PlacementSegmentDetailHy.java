package com.pradera.model.issuancesecuritie.placementsegment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the PLACEMENT_SEGMENT_DETAIL_HYS database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/05/2013
 */
@Entity
@Table(name="PLACEMENT_SEGMENT_DETAIL_HYS")
public class PlacementSegmentDetailHy implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id placement segment det hy pk. */
	@Id
	@SequenceGenerator(name="SQ_PLACEMENT_SEGMENT_DETAIL_HYS_PK", sequenceName="SQ_ID_PLAC_SEGMENT_DET_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_PLACEMENT_SEGMENT_DETAIL_HYS_PK")
	@Column(name="ID_PLACEMENT_SEGMENT_DET_HY_PK")
	private Long idPlacementSegmentDetHyPk;

	/** The description. */
	private String description;

	/** The id isin code fk. */
	@Column(name="ID_SECURITY_CODE_FK")
	private String idSecurityCodeFk;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    /** The last modify date. */
    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The placed amount. */
	@Column(name="PLACED_AMOUNT")
	private BigDecimal placedAmount;

    /** The registry date. */
    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
    
    /** The request amount. */
    @Column(name="REQUEST_AMOUNT")
	private BigDecimal requestAmount;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state placement segment det. */
	@Column(name="STATE_PLACEMENT_SEGMENT_DET")
	private BigDecimal statePlacementSegmentDet;

	/** The placement segment type. */
	@Column(name="PLACEMENT_SEGMENT_DET_TYPE_HY")
	private Integer placementSegmentTypeHy;
	
	//bi-directional many-to-one association to PlacementSegmentHy
	/** The placement segment hy. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PLACEMENT_SEGMENT_HYS_FK")
	private PlacementSegmentHy placementSegmentHy;

    /**
     * Instantiates a new placement segment detail hy.
     */
    public PlacementSegmentDetailHy() {
    }

	/**
	 * Gets the id placement segment det hy pk.
	 *
	 * @return the id placement segment det hy pk
	 */
	public Long getIdPlacementSegmentDetHyPk() {
		return this.idPlacementSegmentDetHyPk;
	}

	/**
	 * Sets the id placement segment det hy pk.
	 *
	 * @param idPlacementSegmentDetHyPk the new id placement segment det hy pk
	 */
	public void setIdPlacementSegmentDetHyPk(Long idPlacementSegmentDetHyPk) {
		this.idPlacementSegmentDetHyPk = idPlacementSegmentDetHyPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the id isin code fk.
	 *
	 * @return the id isin code fk
	 */
	public String getIdSecurityCodeFk() {
		return this.idSecurityCodeFk;
	}

	/**
	 * Sets the id isin code fk.
	 *
	 * @param idIsinCodeFk the new id isin code fk
	 */
	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the placed amount.
	 *
	 * @return the placed amount
	 */
	public BigDecimal getPlacedAmount() {
		return this.placedAmount;
	}

	/**
	 * Sets the placed amount.
	 *
	 * @param placedAmount the new placed amount
	 */
	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state placement segment det.
	 *
	 * @return the state placement segment det
	 */
	public BigDecimal getStatePlacementSegmentDet() {
		return this.statePlacementSegmentDet;
	}

	/**
	 * Sets the state placement segment det.
	 *
	 * @param statePlacementSegmentDet the new state placement segment det
	 */
	public void setStatePlacementSegmentDet(BigDecimal statePlacementSegmentDet) {
		this.statePlacementSegmentDet = statePlacementSegmentDet;
	}

	/**
	 * Gets the placement segment hy.
	 *
	 * @return the placement segment hy
	 */
	public PlacementSegmentHy getPlacementSegmentHy() {
		return this.placementSegmentHy;
	}

	/**
	 * Sets the placement segment hy.
	 *
	 * @param placementSegmentHy the new placement segment hy
	 */
	public void setPlacementSegmentHy(PlacementSegmentHy placementSegmentHy) {
		this.placementSegmentHy = placementSegmentHy;
	}

	/**
	 * Gets the request amount.
	 *
	 * @return the request amount
	 */
	public BigDecimal getRequestAmount() {
		return requestAmount;
	}

	/**
	 * Sets the request amount.
	 *
	 * @param requestAmount the new request amount
	 */
	public void setRequestAmount(BigDecimal requestAmount) {
		this.requestAmount = requestAmount;
	}

	public Integer getPlacementSegmentTypeHy() {
		return placementSegmentTypeHy;
	}

	public void setPlacementSegmentTypeHy(Integer placementSegmentTypeHy) {
		this.placementSegmentTypeHy = placementSegmentTypeHy;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
			lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
	        lastModifyDate = loggerUser.getAuditTime();
	        lastModifyIp = loggerUser.getIpAddress();
	        lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}