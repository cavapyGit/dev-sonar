package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

/**
 * The persistent class for the NOMINAL_VALUE_HISTORY database table.
 * 
 */
@Entity
@Table(name="NOMINAL_VALUE_HISTORY")
public class NominalValueHistory implements Serializable, Auditable{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	@Id
	@SequenceGenerator(name="NOMINAL_VALUE_HISTORY_GENERATOR", sequenceName="SQ_ID_NOMINAL_VALUE_HISTORY", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NOMINAL_VALUE_HISTORY_GENERATOR")
	@Column(name="ID_NOMINAL_VALUE_HISTORY_PK")
	private Long idNominalValueHistoryPk;
	
	/** The security. */
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;
	
	/** The current nominal value. */
	@Column(name = "NOMINAL_VALUE")
	private BigDecimal nominalValue;
	
	/** The cutoff date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "CUT_DATE")
    private Date cutDate;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;	

	public NominalValueHistory() {
		super();
	}
	
	/**
	 * @return the idNominalValueHistoryPk
	 */
	public Long getIdNominalValueHistoryPk() {
		return idNominalValueHistoryPk;
	}

	/**
	 * @param idNominalValueHistoryPk the idNominalValueHistoryPk to set
	 */
	public void setIdNominalValueHistoryPk(Long idNominalValueHistoryPk) {
		this.idNominalValueHistoryPk = idNominalValueHistoryPk;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * @return the cutDate
	 */
	public Date getCutDate() {
		return cutDate;
	}

	/**
	 * @param cutDate the cutDate to set
	 */
	public void setCutDate(Date cutDate) {
		this.cutDate = cutDate;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
