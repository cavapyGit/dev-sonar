package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum SecuritieType. Master 51
 */
public enum SecurityType { 

	/** The shares. */
	SHARES(Integer.valueOf(125),"ACCIONES", null),	
	
	PRIVATE(Integer.valueOf(1763),"PRIVADO", null),
	
	PUBLIC(Integer.valueOf(1762),"PUBLICO", null),
	/** The par quo. */
	PAR_QUO(Integer.valueOf(404),"CUOTAS DE PARTICIPACION", null);
	
	/** The Constant lookup. */
	public static final Map<Integer,SecurityType> lookup=new HashMap<Integer, SecurityType>();
	
	/** The Constant list. */
	public static final List<SecurityType> list=new ArrayList<SecurityType>();

	static{
		for(SecurityType s:SecurityType.values()){
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	private Integer maxTermDays;
	
	/**
	 * Instantiates a new securitie type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SecurityType(Integer code, String value,Integer maxTermDays){
		this.code=code;
		this.value=value;
		this.maxTermDays=maxTermDays;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie type
	 */
	public static SecurityType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public Integer getMaxTermDays() {
		return maxTermDays;
	}

	public void setMaxTermDays(Integer maxTermDays) {
		this.maxTermDays = maxTermDays;
	}	
	
	
	
}
