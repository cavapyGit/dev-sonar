package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum SecuritieClassType.
 */
public enum SecurityClassType {
	
	/** The ac.  */
	ACE(new Integer(126),"ACCIÓN ORDINARIA EXTERIOR","01","AOE"),
	
	/** The ai. */
	ACC(new Integer(406),"ACCION ORDINARIA SIMPLE","02","OSN"),
	
	/** The acp. */
	ACP(new Integer(407),"ACCIONES PREFERENTES","03","ACP"),
	
	/** The acp rf. */
	ACC_RF(new Integer(1962),"ACCION PREFERIDA","03","PRF"),
	
	ACC_OVM(new Integer(2855),"ACCION ORDINARIA VOTO MULTIPLE","03","OVM"),
	
	ACC_APE(new Integer(2856),"ACCION PREFERIDA EXTERIOR","03","APE"),
	
	ADP(new Integer(2897), "ACCION DERECHO PREFERENTE", "03", "ADP"),
	/** The bh. */
	BLP(new Integer(408),"BONOS LARGO PLAZO","04","BLP"),
	
	/** The bs. */
	BTS(new Integer(409),"BONOS DEL TESORO GENERAL DE LA NACION","05","BTS"),
	
	/** The bso. */
	BMS(new Integer(410),"BONOS MUNICIPALES","06","BMU"),
	
	/** The bsp. */
	BSP(new Integer(411),"BONOS FINANCIEROS","07","BFI"),
	
	/** The bsp. */
	BEX(new Integer(1947),"BONOS DEL EXTERIOR","07","BEX"),
	
	/** The bt. */
	VTD(new Integer(412),"VALORES DE TITULARIZACION DE CONTENIDO CREDITICIO","08","VTD"),
	
	/** The bc. */
	BBC(new Integer(413),"BONOS BANCARIOS CONVERTIBLES EN ACCIONES","09","BBC"), 
	
	/** The be. */
	BTX(new Integer(414),"BONOS DE TESORO","10","BTP"),
	
	/** The bca. */
	BCA(new Integer(415),"BONOS BIC EXTRABURSATILES","11","BBX"),  
	
	/** The baf. */
	CDS(new Integer(416),"CERTIFICADOS DE DEPOSITO BIC","12","CDS"),
	
	/** The lh. */
	BCP(new Integer(417),"BONOS CORTO PLAZO","13","BCP"),
	
	
	/** The tchn. */
	LTS(new Integer(418),"LETRAS DEL TESORO","14","LTS"),
	
	/** The pc. */
	PGB(new Integer(419),"PAGARES BURSATILES","15","PGB"),
	
	/** The cdb. */
	DPF(new Integer(420),"CERTIFICADO DE DEPOSITO DE AHORRO","16","CDA"),
	
	/** The dpa. */
	DPA(new Integer(1976),"DPF","43","DPA"),
	
	/** The letras. */
	CUP(new Integer(421),"CUPONES","17","CUP"),
	
	/** The pagares. */
	CSP(new Integer(425),"CERTIFICADOS DE SUSCRIPCION PREFERENTE","21","CSP"),
	
	/** The fc. */
	CFC(new Integer(426),"FONDO DE INVERSION LOCAL","22","FIL"),
	
	/** The bbs. */
	BBS(new Integer(1936),"BONOS DEL BANCO CENTRAL DE BOLIVIA","29","BBS"),

	/** The bbx. */
	BBX(new Integer(415),"BONOS BIC EXTRABURSATILES","11","BBX"),
	
	BBB(new Integer(411),"BONOS BANCARIOS BURSATILES","11","BBB"),
	
	BPB(new Integer(2366),"BONOS PARTICIPATIVOS","11","BPB"),
	
	/** The bcb. */
	BCB(new Integer(1946),"BONOS DEL BANCO CENTRAL DE BOLIVIA","34","BIC"),

	/** The cdb. */
	CDB(new Integer(1948),"CEDES BANCARIOS","36","CDB"),

	/** The cdd. */
	CDD(new Integer(1949),"CERTIFICADOS DE DEVOLUCION DE DEPOSITOS","37","CDD"),

	/** The lbs. */
	LBS(new Integer(1937),"LETRAS DE REGULACION MONETARIA","30","LRM"),

	/** The lcb. */
	LCB(new Integer(1950),"LETRAS DE CAMBIO BURSATILES","38","LCB"),

	/** The ltc. */
	LTC(new Integer(1951),"LETRAS DE CAMBIO","39","LTC"),

	/** The ncf. */
	NCF(new Integer(1952),"NOTAS DE CREDITO FISCAL","40","NCF"),

	/** The pgr. */
	PGR(new Integer(1953),"PAGARES NEGOCIADOS EN RUEDO","41","PGR"),

	/** The pgs. */
	PGS(new Integer(1954),"PAGARES","42","PAG"),

	/** The cdi. */
	CDI(new Integer(2088),"CEDEIM - CERTIFICADOS DE DEVOLUCION IMPOSITIVA","44","CDI"),

	/** The cnc. */
	CNC(new Integer(2089),"CENOCREN - CERTIFICADOS DE NOTAS DE CREDITO NEGOCIABLES","45","CNC"),

	/** The ncm. */
	NCM(new Integer(2090),"NOTAS DE CREDITO MUNICIPALES","46","NCM"),
	
	CFF(new Integer(2242),"CONTRATO DE FUTURO","46","CTF"),
	
	FCT(new Integer(2244),"FACTURAS","46","FCT"),
	
	BB(new Integer(2245),"BONOS DEL BANCO CENTRAL DE BOLIVIA EXTRABURSATILES","46","BB"),
	
	AOP(new Integer(2246),"ACCIONES DE OFERTA PRIVADA","46","AOP"),
	
	CPP(new Integer(2247),"CUOTAS DE PARTICIPACION EN SOCIEDADES DE RESPONSABILIDAD LIMITADA","46","CPP"),
	
	BOP(new Integer(1854),"BONOS DE OFERTA PRIVADA","46","CPP"),
	
	/** The anr. */
	ANR(new Integer(1945),"ACCIONES NO REGISTRADAS EN BOLSA","33","ANR"),
	
	PGE(new Integer(1855),"PAGARES DE OFERTA PRIVADA","28","PGE"),
	
	/** New class LRS, BRS, ind redemption true    */
	
	LRS(new Integer(2353),"LETRAS BONOS DEL BANCO CENTRAL DE BOLIVIA CON OPCION DE RESCATE ANTICIPADO","30","LBS"),
	
	BRS(new Integer(2352),"BONOS DEL BANCO CENTRAL DE BOLIVIA CON OPCION DE RESCATE ANTICIPADO","30","LBS"),
	
	BSU(new Integer(2814),"BONO SUBORDINADO","16","BSU"),
	BOC(new Integer(2860),"BONOS CORPORATIVOS","16","BOC"),
	BVE(new Integer(2861),"BONO VERDE","16","BSU"),
	BMT(new Integer(2862),"BONO MULTILATERAL","16","BSU"),
	CHQ(new Integer(2863),"CHEQUES","16","CHQ"),
	CUPS(new Integer(2864),"CUPONES","16","CUP"),
	OPA(new Integer(2865),"OPCIONES AMERICANAS","16","OPA"),
	OPE(new Integer(2866),"OPCIONES EUROPEAS","16","OPE");

	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The code cd. */
	private String codeCd;
	
	/** The text 1. */
	private String text1;

	/** The Constant lookup. */
	public static final Map<Integer, SecurityClassType> lookup = new HashMap<Integer, SecurityClassType>();
	
	/** The Constant list. */
	public static final List<SecurityClassType> list = new ArrayList<SecurityClassType>();
	
	static {
		for (SecurityClassType s : EnumSet.allOf(SecurityClassType.class)) {
			lookup.put(s.getCode(), s);
			list.add(s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the securitie class type
	 */
	public static SecurityClassType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Instantiates a new securitie class type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 * @param text1 the text1
	 */
	private SecurityClassType(Integer code, String value, String codeCd, String text1) {
		this.code = code;
		this.value = value;
		this.codeCd = codeCd;
		this.text1=text1;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Gets the code cd.
	 *
	 * @return the code cd
	 */
	public String getCodeCd() {
		return codeCd;
	}
	
	/**
	 * Gets the text1.
	 *
	 * @return the text1
	 */
	public String getText1(){
		return text1;
	}
	

}
