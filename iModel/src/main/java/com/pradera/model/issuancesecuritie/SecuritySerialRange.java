package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="SECURITY_SERIAL_RANGE")
public class SecuritySerialRange implements Serializable, Auditable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SECURITY_SERIAL_RANGE_PK_GENERATOR", sequenceName="SQ_ID_SECURITY_SERIAL_RANGE_PK ",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECURITY_SERIAL_RANGE_PK_GENERATOR")
	@Column(name="ID_SECURITY_SERIAL_RANGE_PK")
	private Long idSecuritySerialRangePk;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security security;

	@Column(name="SERIAL_RANGE")
	private String serialRange;

	@Column(name="NUMBER_SHARE_INITIAL")
	private Integer numberShareInitial;

	@Column(name="NUMBER_SHARE_FINAL")
	private Integer numberShareFinal;
	
	@Column(name="STATE_RANGE")
	private Integer stateRange;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="LAST_MODIFY_DATE ")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	
	public Long getIdSecuritySerialRangePk() {
		return idSecuritySerialRangePk;
	}

	public void setIdSecuritySerialRangePk(Long idSecuritySerialRangePk) {
		this.idSecuritySerialRangePk = idSecuritySerialRangePk;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public String getSerialRange() {
		return serialRange;
	}

	public void setSerialRange(String serialRange) {
		this.serialRange = serialRange;
	}

	public Integer getNumberShareInitial() {
		return numberShareInitial;
	}

	public void setNumberShareInitial(Integer numberShareInitial) {
		this.numberShareInitial = numberShareInitial;
	}

	public Integer getNumberShareFinal() {
		return numberShareFinal;
	}

	public void setNumberShareFinal(Integer numberShareFinal) {
		this.numberShareFinal = numberShareFinal;
	}

	public Integer getStateRange() {
		return stateRange;
	}

	public void setStateRange(Integer stateRange) {
		this.stateRange = stateRange;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
