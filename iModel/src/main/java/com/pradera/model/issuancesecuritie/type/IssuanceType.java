package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
//Tipo de emision --> 333
/**
 * The Enum IssuanceType.
 */
public enum IssuanceType {

	/** The dematerialized. */
	DEMATERIALIZED(Integer.valueOf(132),"DESMATERIALIZADO"),
	
	/** The physical. */
	PHYSICAL(Integer.valueOf(400),"FISICO"),
	
	/** The mixed. */
	MIXED(Integer.valueOf(401),"MIXTA");
	
	/** The Constant list. */
	public static final List<IssuanceType> list=new ArrayList<IssuanceType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, IssuanceType> lookup=new HashMap<Integer, IssuanceType>();
	
	static {
		for(IssuanceType e : IssuanceType.values()){
			lookup.put(e.getCode(), e);
			list.add( e );
		}
	}
	
	/** The code. */
	private Integer  code;
	
	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new issuance type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuanceType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	

	

}
