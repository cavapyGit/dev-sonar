package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;


/**
 * The persistent class for the PROGRAM_INT_COUPON_REQ_HIS database table.
 * 
 */
@Entity
@Table(name="PROGRAM_INT_COUPON_REQ_HIS")
public class ProgramIntCouponReqHi implements Serializable, Auditable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PROGRAM_INT_COUPON_REQ_HIS_IDPROGINTCOUPONREQHISPK_GENERATOR", sequenceName="SQ_ID_PROG_INT_COU_REQ_HIS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROGRAM_INT_COUPON_REQ_HIS_IDPROGINTCOUPONREQHISPK_GENERATOR")
	@Column(name="ID_PROG_INT_COUPON_REQ_HIS_PK")
	private Long idProgIntCouponReqHisPk;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CORPORATIVE_DATE")
	private Date corporativeDate;

	@Column(name="COUPON_NUMBER")
	private Integer couponNumber;

	private Integer currency;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CUTOFF_DATE")
	private Date cutoffDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPERITATION_DATE")
	private Date expirationDate;

	@Column(name="FINANTIAL_INDEX_RATE")
	private BigDecimal finantialIndexRate;

	@Column(name="IND_ROUNDING")
	private Integer indRounding;

	@Column(name="INTEREST_FACTOR")
	private BigDecimal interestFactor;

	@Column(name="INTEREST_RATE")
	private BigDecimal interestRate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

	@Column(name="PAYMENT_DAYS")
	private Integer paymentDays;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="STATE_PROGRAM_INTEREST")
	private Integer stateProgramInterest;
	
	@Column(name="PAYED_AMOUNT")
    private BigDecimal payedAmount;

	@Column(name="COUPON_AMOUNT")
	private BigDecimal couponAmount;

	/** The last modify date. */
    @Temporal(TemporalType.DATE)
	@Column(name="EXCHANGE_DATE_RATE")
	private Date exchangeDateRate;
    
	/** The market rate. */
	@Column(name="CURRENCY_PAYMENT")
	private Integer currencyPayment;
	
	//bi-directional many-to-one association to IntPaymentScheduleReqHi
    @ManyToOne
	@JoinColumn(name="ID_INT_SCHEDULE_REQ_HIS_FK")
	private IntPaymentScheduleReqHi intPaymentScheduleReqHi;
    
	@Transient
	private boolean selected;
	@Transient
	private boolean disabled;
	@Transient
	private Date paymentDateMin;
	@Transient
	private Date paymentDateMax;
	@Transient
	private Date minBeginDate;
	@Transient
	private Date maxBeginDate;
	@Transient
	private Date minExpirationDate;
	@Transient
	private Date maxExpirationDate;
	
    public ProgramIntCouponReqHi() {
    }

	public Long getIdProgIntCouponReqHisPk() {
		return this.idProgIntCouponReqHisPk;
	}

	public void setIdProgIntCouponReqHisPk(Long idProgIntCouponReqHisPk) {
		this.idProgIntCouponReqHisPk = idProgIntCouponReqHisPk;
	}

	public Date getBeginingDate() {
		return this.beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public Date getCorporativeDate() {
		return this.corporativeDate;
	}

	public void setCorporativeDate(Date corporativeDate) {
		this.corporativeDate = corporativeDate;
	}

	public Integer getCouponNumber() {
		return this.couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Integer getCurrency() {
		return this.currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Date getCutoffDate() {
		return this.cutoffDate;
	}

	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}

	

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public BigDecimal getFinantialIndexRate() {
		return this.finantialIndexRate;
	}

	public void setFinantialIndexRate(BigDecimal finantialIndexRate) {
		this.finantialIndexRate = finantialIndexRate;
	}

	public Integer getIndRounding() {
		return this.indRounding;
	}

	public void setIndRounding(Integer indRounding) {
		this.indRounding = indRounding;
	}

	public BigDecimal getInterestFactor() {
		return this.interestFactor;
	}

	public void setInterestFactor(BigDecimal interestFactor) {
		this.interestFactor = interestFactor;
	}

	public BigDecimal getInterestRate() {
		return this.interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Integer getPaymentDays() {
		return this.paymentDays;
	}

	public void setPaymentDays(Integer paymentDays) {
		this.paymentDays = paymentDays;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Integer getStateProgramInterest() {
		return this.stateProgramInterest;
	}

	public void setStateProgramInterest(Integer stateProgramInterest) {
		this.stateProgramInterest = stateProgramInterest;
	}

	public IntPaymentScheduleReqHi getIntPaymentScheduleReqHi() {
		return this.intPaymentScheduleReqHi;
	}

	public void setIntPaymentScheduleReqHi(IntPaymentScheduleReqHi intPaymentScheduleReqHi) {
		this.intPaymentScheduleReqHi = intPaymentScheduleReqHi;
	}
	
	public BigDecimal getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(BigDecimal couponAmount) {
		this.couponAmount = couponAmount;
	}

	public String getStateDescription(){
		if(stateProgramInterest!=null){
			return ProgramScheduleStateType.get(stateProgramInterest).getValue();
		}
		return "";
	}
	
	public boolean isPendingState(){
		if(stateProgramInterest!=null){
			if(stateProgramInterest.equals( ProgramScheduleStateType.PENDING.getCode() )){
				return Boolean.TRUE;
			}
		}
		return false;
	}
	
	public boolean isPaidState(){
		if(stateProgramInterest!=null){
			if(stateProgramInterest.equals( ProgramScheduleStateType.EXPIRED.getCode() )){
				return Boolean.TRUE;
			}
		}
		return false;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if(loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
		}
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Date getPaymentDateMin() {
		return paymentDateMin;
	}

	public void setPaymentDateMin(Date paymentDateMin) {
		this.paymentDateMin = paymentDateMin;
	}

	public Date getPaymentDateMax() {
		return paymentDateMax;
	}

	public void setPaymentDateMax(Date paymentDateMax) {
		this.paymentDateMax = paymentDateMax;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public Date getMinBeginDate() {
		return minBeginDate;
	}

	public void setMinBeginDate(Date minBeginDate) {
		this.minBeginDate = minBeginDate;
	}

	public Date getMaxBeginDate() {
		return maxBeginDate;
	}

	public void setMaxBeginDate(Date maxBeginDate) {
		this.maxBeginDate = maxBeginDate;
	}

	public Date getMinExpirationDate() {
		return minExpirationDate;
	}

	public void setMinExpirationDate(Date minExpirationDate) {
		this.minExpirationDate = minExpirationDate;
	}

	public Date getMaxExpirationDate() {
		return maxExpirationDate;
	}

	public void setMaxExpirationDate(Date maxExpirationDate) {
		this.maxExpirationDate = maxExpirationDate;
	}

	public BigDecimal getPayedAmount() {
		return payedAmount;
	}

	public void setPayedAmount(BigDecimal payedAmount) {
		this.payedAmount = payedAmount;
	}
	
	public Date getExchangeDateRate() {
		return exchangeDateRate;
	}

	public void setExchangeDateRate(Date exchangeDateRate) {
		this.exchangeDateRate = exchangeDateRate;
	}

	public Integer getCurrencyPayment() {
		return currencyPayment;
	}

	public void setCurrencyPayment(Integer currencyPayment) {
		this.currencyPayment = currencyPayment;
	}

	public void setDataFromProgramInterestCoupon(ProgramInterestCoupon programInterestCoupon, LoggerUser loggerUser){
		this.setAudit(loggerUser);
		
		this.setCouponNumber( programInterestCoupon.getCouponNumber() );
		this.setBeginingDate(programInterestCoupon.getBeginingDate());
		this.setExpirationDate(programInterestCoupon.getExperitationDate());
		this.setRegistryDate( programInterestCoupon.getRegistryDate() );
		
		this.setCutoffDate( programInterestCoupon.getCutoffDate() );
		this.setCorporativeDate( programInterestCoupon.getCorporativeDate() );
		this.setPaymentDate( programInterestCoupon.getPaymentDate() );
		this.setPaymentDays( programInterestCoupon.getPaymentDays() );
		this.setCurrency( programInterestCoupon.getCurrency() );
		this.setFinantialIndexRate( programInterestCoupon.getFinantialIndexRate() );
		this.setInterestFactor( programInterestCoupon.getInterestFactor() );
		this.setInterestRate( programInterestCoupon.getInterestRate() );
		this.setIndRounding( programInterestCoupon.getIndRounding() );
		this.setStateProgramInterest( programInterestCoupon.getStateProgramInterest() );
		this.setPayedAmount( programInterestCoupon.getPayedAmount() );
		this.setCouponAmount( programInterestCoupon.getCouponAmount() );
		
		this.setExchangeDateRate(programInterestCoupon.getExchangeDateRate());
		this.setCurrencyPayment(programInterestCoupon.getCurrencyPayment());
	}
	
    @Override
    public ProgramIntCouponReqHi clone() throws CloneNotSupportedException {
    	ProgramIntCouponReqHi cloned = (ProgramIntCouponReqHi) super.clone();
        // Realizar cualquier ajuste necesario en el objeto clonado aquí
        return cloned;
    }
	
}