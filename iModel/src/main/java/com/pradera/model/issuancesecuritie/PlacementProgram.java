package com.pradera.model.issuancesecuritie;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PLACEMENT_PROGRAM database table.
 * 
 */
@Entity
@Table(name="PLACEMENT_PROGRAM")
public class PlacementProgram implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID_PLACEMENT_PROGRAM_PK")
	private BigDecimal idPlacementProgramPk;
	
    @Temporal( TemporalType.DATE)
	@Column(name="ACTION_DATE")
	private Date actionDate;

	@Column(name="ACTION_MOTIVE")
	private BigDecimal actionMotive;

	@Column(name="AMOUNT_TO_PLACE")
	private BigDecimal amountToPlace;

    @Temporal( TemporalType.DATE)
	@Column(name="BEGINING_DATE")
	private Date beginingDate;

    @Temporal( TemporalType.DATE)
	@Column(name="CLOSING_DATE")
	private Date closingDate;

	@Column(name="HOLDER_CLASS")
	private BigDecimal holderClass;

	@Column(name="ID_HOLDER_ACCOUNT_FK")
	private BigDecimal idHolderAccountFk;

	@Column(name="LAST_MODIFY_APP")
	private BigDecimal lastModifyApp;

    @Temporal( TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="OTHER_ACTION_MOTIVE")
	private String otherActionMotive;

	@Column(name="PLACED_AMOUNT")
	private BigDecimal placedAmount;

	@Column(name="PLACEMENT_STATE")
	private BigDecimal placementState;

	@Column(name="PLACEMENT_TERM")
	private BigDecimal placementTerm;

    @Temporal( TemporalType.DATE)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	@Column(name="REGISTRY_USER")
	private String registryUser;

	@Column(name="TRANCHE_NUMBER")
	private BigDecimal trancheNumber;

	//bi-directional many-to-one association to Issuer
    @ManyToOne
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

    public PlacementProgram() {
    }

	public Date getActionDate() {
		return this.actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public BigDecimal getActionMotive() {
		return this.actionMotive;
	}

	public void setActionMotive(BigDecimal actionMotive) {
		this.actionMotive = actionMotive;
	}

	public BigDecimal getAmountToPlace() {
		return this.amountToPlace;
	}

	public void setAmountToPlace(BigDecimal amountToPlace) {
		this.amountToPlace = amountToPlace;
	}

	public Date getBeginingDate() {
		return this.beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public Date getClosingDate() {
		return this.closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public BigDecimal getHolderClass() {
		return this.holderClass;
	}

	public void setHolderClass(BigDecimal holderClass) {
		this.holderClass = holderClass;
	}

	public BigDecimal getIdHolderAccountFk() {
		return this.idHolderAccountFk;
	}

	public void setIdHolderAccountFk(BigDecimal idHolderAccountFk) {
		this.idHolderAccountFk = idHolderAccountFk;
	}

	public BigDecimal getIdPlacementProgramPk() {
		return this.idPlacementProgramPk;
	}

	public void setIdPlacementProgramPk(BigDecimal idPlacementProgramPk) {
		this.idPlacementProgramPk = idPlacementProgramPk;
	}

	public BigDecimal getLastModifyApp() {
		return this.lastModifyApp;
	}

	public void setLastModifyApp(BigDecimal lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getOtherActionMotive() {
		return this.otherActionMotive;
	}

	public void setOtherActionMotive(String otherActionMotive) {
		this.otherActionMotive = otherActionMotive;
	}

	public BigDecimal getPlacedAmount() {
		return this.placedAmount;
	}

	public void setPlacedAmount(BigDecimal placedAmount) {
		this.placedAmount = placedAmount;
	}

	public BigDecimal getPlacementState() {
		return this.placementState;
	}

	public void setPlacementState(BigDecimal placementState) {
		this.placementState = placementState;
	}

	public BigDecimal getPlacementTerm() {
		return this.placementTerm;
	}

	public void setPlacementTerm(BigDecimal placementTerm) {
		this.placementTerm = placementTerm;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	public BigDecimal getTrancheNumber() {
		return this.trancheNumber;
	}

	public void setTrancheNumber(BigDecimal trancheNumber) {
		this.trancheNumber = trancheNumber;
	}

	public Issuer getIssuer() {
		return this.issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
}