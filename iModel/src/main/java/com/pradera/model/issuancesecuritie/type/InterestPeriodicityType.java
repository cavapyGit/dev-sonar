
package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.generalparameter.type.PeriodType;

// TODO: Auto-generated Javadoc

public enum InterestPeriodicityType {
	
	
	BY_DAYS(Integer.valueOf(1747),"POR DIAS",Integer.valueOf(0),null),
	MONTHLY(Integer.valueOf(147),"MENSUAL",Integer.valueOf(1),Integer.valueOf(12)), 
	BIMONTHLY(Integer.valueOf(536),"BIMESTRAL",Integer.valueOf(2),Integer.valueOf(6)), 
	QUARTERLY(Integer.valueOf(537),"TRIMESTRAL",Integer.valueOf(3),Integer.valueOf(4)),
	FOURMONTHLY(Integer.valueOf(538),"CUATRIMESTRAL",Integer.valueOf(4),Integer.valueOf(3)),
	BIANNUAL(Integer.valueOf(539),"SEMESTRAL",Integer.valueOf(6),Integer.valueOf(2)),
	ANNUAL(Integer.valueOf(540),"ANUAL",Integer.valueOf(12),Integer.valueOf(1)),
	INDISTINCT(Integer.valueOf(1935),"INDISTINTO",Integer.valueOf(0),null);
	
	public final static List<InterestPeriodicityType> list=new ArrayList<InterestPeriodicityType>();
	public static final Map<Integer, InterestPeriodicityType> lookup = new HashMap<Integer, InterestPeriodicityType>();
	public static final Map<String, InterestPeriodicityType> lookupValue = new HashMap<String, InterestPeriodicityType>();
	
	private Integer code;
	private String value;
	private Integer indicator1;
	private Integer indicator2;
	
	static{
		for(InterestPeriodicityType d : InterestPeriodicityType.values()){
			list.add(d);
			lookup.put(d.getCode(),d);
			lookupValue.put(d.getValue(), d);
		}
	}	
	
	/**
	 * Instantiates a new instrument type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param codeCd the code cd
	 */
	private InterestPeriodicityType(Integer code, String value,Integer indicator1,Integer indicator2){
		this.code=code;
		this.value=value;
		this.indicator1=indicator1;
		this.indicator2=indicator2;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the instrument type
	 */
	public static InterestPeriodicityType get(Integer code) {
		return lookup.get(code);
	}
	
	public static InterestPeriodicityType getWitgDescription(String description) {
		return lookupValue.get(description);
	}
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}


	public Integer getIndicator1() {
		return indicator1;
	}

	public void setIndicator1(Integer indicator1) {
		this.indicator1 = indicator1;
	}

	public Integer getIndicator2() {
		return indicator2;
	}
	
}
