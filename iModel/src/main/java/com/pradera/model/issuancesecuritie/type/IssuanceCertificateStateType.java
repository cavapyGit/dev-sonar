package com.pradera.model.issuancesecuritie.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum IssuanceCertificateStateType.
 */
public enum IssuanceCertificateStateType {

	/** The registered. */
	REGISTERED(Integer.valueOf(129),"REGISTRADO"),
	
	/** The deleted. */
	DELETED(Integer.valueOf(446),"ELIMINADO"),
	
	/** The to registered. */
	TO_REGISTERED(Integer.valueOf(447),"POR REGISTRAR"),
	
	/** The to deleted. */
	TO_DELETED(Integer.valueOf(448),"POR ELIMINAR");
	
	/** The Constant list. */
	public final static List<IssuanceCertificateStateType> list=new ArrayList<IssuanceCertificateStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, IssuanceCertificateStateType> lookup = new HashMap<Integer, IssuanceCertificateStateType>();
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	static{
		for(IssuanceCertificateStateType c : IssuanceCertificateStateType.values()){
			list.add(c);
			lookup.put(c.getCode(),c);
		}
	}
	
	/**
	 * Instantiates a new issuance certificate state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private IssuanceCertificateStateType(Integer code, String value){
		this.code=code;
		this.value=value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the issuance certificate state type
	 */
	public static IssuanceCertificateStateType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
