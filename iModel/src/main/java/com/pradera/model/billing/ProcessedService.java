package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the PROCESSED_SERVICE database table.
 * 
 */
@Entity
@Table(name="PROCESSED_SERVICE")
public class ProcessedService implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PROCESSED_SERVICE_GENERATOR", sequenceName="SQ_ID_PROCESSED_SERVICE_PK", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROCESSED_SERVICE_GENERATOR")
	@Column(name="ID_PROCESSED_SERVICE_PK", unique=true, nullable=false, precision=10)
	private Long idProcessedServicePk;

	@Temporal(TemporalType.DATE)
	@Column(name="CALCULATION_DATE")
	private Date calculationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE", nullable=false)
	private Date operationDate;
	
	@Column(name="MONTH_PROCESS")
	private Integer monthProcess;

	@Column(name="PROCESSED_STATE")
	private Integer processedState;

	@Column(name="YEAR_PROCESS")
	private Integer yearProcess;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="INITIAL_DATE", nullable=false)
	private Date initialDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FINAL_DATE", nullable=false)
	private Date finalDate;

	//bi-directional many-to-one association to BillingService
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BILLING_SERVICE_FK", nullable=false)
	private BillingService billingService;

	//bi-directional many-to-one association to CollectionRecord
	@OneToMany(mappedBy="processedService",cascade=CascadeType.ALL)
	private List<CollectionRecord> collectionRecords;	

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient	
	private String descriptionProcessedState;

	@Transient	
	private Long countCollection;
	
	
	public ProcessedService() {
		
	}
	
	
	public ProcessedService(Long idProcessedServicePk , Date calculationDate , Date operationDate,
			Integer monthProcess , Integer yearProcess,Integer processedState, BillingService billingService,
			Date initialDate, Date finalDate,
			Integer lastModifyApp,Date lastModifyDate,String lastModifyIp,String lastModifyUser,
			Long countCollection) {
		
		this.idProcessedServicePk=idProcessedServicePk;
		this.calculationDate=calculationDate;
		this.operationDate=operationDate;
		this.monthProcess=monthProcess;
		this.yearProcess=yearProcess;
		this.processedState=processedState;		
		this.countCollection=countCollection;
		this.lastModifyApp=lastModifyApp;
		this.lastModifyDate=lastModifyDate;
		this.lastModifyIp=lastModifyIp;
		this.lastModifyUser=lastModifyUser;
		this.billingService=billingService;
		
		this.initialDate=initialDate;
		this.finalDate=finalDate;
		
	}
	
	public ProcessedService(Long idProcessedServicePk) {
		
		
		
	}
	
	
	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}


	public Long getIdProcessedServicePk() {
		return idProcessedServicePk;
	}

	public void setIdProcessedServicePk(Long idProcessedServicePk) {
		this.idProcessedServicePk = idProcessedServicePk;
	}



	public Date getCalculationDate() {
		return this.calculationDate;
	}

	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}


	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	

	public Integer getMonthProcess() {
		return monthProcess;
	}

	public void setMonthProcess(Integer monthProcess) {
		this.monthProcess = monthProcess;
	}

	public Integer getProcessedState() {
		return processedState;
	}

	public void setProcessedState(Integer processedState) {
		this.processedState = processedState;
	}

	public Integer getYearProcess() {
		return yearProcess;
	}

	public void setYearProcess(Integer yearProcess) {
		this.yearProcess = yearProcess;
	}

	public BillingService getBillingService() {
		return this.billingService;
	}

	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}

	public List<CollectionRecord> getCollectionRecords() {
		return this.collectionRecords;
	}

	public void setCollectionRecords(List<CollectionRecord> collectionRecords) {
		this.collectionRecords = collectionRecords;
	}
	
	

	/**
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}


	/**
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}


	/**
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}


	/**
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}


	public CollectionRecord addCollectionRecord(CollectionRecord collectionRecord) {
		getCollectionRecords().add(collectionRecord);
		collectionRecord.setProcessedService(this);

		return collectionRecord;
	}

	public CollectionRecord removeCollectionRecord(CollectionRecord collectionRecord) {
		getCollectionRecords().remove(collectionRecord);
		collectionRecord.setProcessedService(null);

		return collectionRecord;
	}



	public String getDescriptionProcessedState() {
		return descriptionProcessedState;
	}

	public void setDescriptionProcessedState(String descriptionProcessedState) {
		this.descriptionProcessedState = descriptionProcessedState;
	}

	
	
	
	public Long getCountCollection() {
		return countCollection;
	}

	public void setCountCollection(Long countCollection) {
		this.countCollection = countCollection;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }

	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("collectionRecords", collectionRecords);
        return detailsMap;
	}

}