package com.pradera.model.billing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the SERVICE_RATE_SCALE database table.
 * 
 */
@Entity
@Table(name="SERVICE_RATE_SCALE")
public class ServiceRateScale implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SERVICE_RATE_SCALE_GENERATOR", sequenceName="SQ_ID_SERVICE_RATE_SCALE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SERVICE_RATE_SCALE_GENERATOR")
	@Column(name="ID_SERVICE_RATE_SCALE_PK")
	private Long idServiceRateScalePk;
	
	@Column(name="SCALE_ORDER")
	private Integer scaleOrder;
	
	@Column(name="MAXIMUM_RANGE")
	private BigDecimal maximumRange;
	
	@Column(name="MINIMUM_RANGE")
	private BigDecimal minimumRange;
	
	@Column(name="SCALE_TYPE")
	private Integer scaleType;	
	
	@Column(name="IND_ACCUMULATIVE_SCALE")
	private Integer indAccumulativeScale;	
	
	@Column(name="IND_STAGING")
	private Integer indStaging;
	
	@Column(name="IND_OTHER_CURRENCY")
	private Integer indOtherCurrency;
	
	@Column(name="OTHER_CURRENCY")
	private Integer otherCurrency;

	@Column(name="MAXIMUM_COLLECTION_AMOUNT")
	private BigDecimal maximumCollectionAmount;
	
	@Column(name="MINIMUM_COLLECTION_AMOUNT")
	private BigDecimal minimumCollectionAmount;

	@Column(name="SCALE_PERCENT")
	private BigDecimal scalePercent;
		
	@Column(name="SCALE_AMOUNT")
	private BigDecimal scaleAmount;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	//bi-directional many-to-one association to ServiceRate
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SERVICE_RATE_FK")
	private ServiceRate serviceRate;

	public ServiceRateScale() {
	}

	

	public Long getIdServiceRateScalePk() {
		return idServiceRateScalePk;
	}



	public void setIdServiceRateScalePk(Long idServiceRateScalePk) {
		this.idServiceRateScalePk = idServiceRateScalePk;
	}

	

	public Integer getScaleOrder() {
		return scaleOrder;
	}



	public void setScaleOrder(Integer scaleOrder) {
		this.scaleOrder = scaleOrder;
	}



	public BigDecimal getScaleAmount() {
		return scaleAmount;
	}



	public void setScaleAmount(BigDecimal scaleAmount) {
		this.scaleAmount = scaleAmount;
	}



	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getMaximumCollectionAmount() {
		return this.maximumCollectionAmount;
	}

	public void setMaximumCollectionAmount(BigDecimal maximumCollectionAmount) {
		this.maximumCollectionAmount = maximumCollectionAmount;
	}

	public BigDecimal getMaximumRange() {
		return this.maximumRange;
	}

	public void setMaximumRange(BigDecimal maximumRange) {
		this.maximumRange = maximumRange;
	}

	public BigDecimal getMinimumCollectionAmount() {
		return this.minimumCollectionAmount;
	}

	public void setMinimumCollectionAmount(BigDecimal minimumCollectionAmount) {
		this.minimumCollectionAmount = minimumCollectionAmount;
	}

	public BigDecimal getMinimumRange() {
		return this.minimumRange;
	}

	public void setMinimumRange(BigDecimal minimumRange) {
		this.minimumRange = minimumRange;
	}

	public BigDecimal getScalePercent() {
		return this.scalePercent;
	}

	public void setScalePercent(BigDecimal scalePercent) {
		this.scalePercent = scalePercent;
	}

	public ServiceRate getServiceRate() {
		return this.serviceRate;
	}

	public void setServiceRate(ServiceRate serviceRate) {
		this.serviceRate = serviceRate;
	}


	public Integer getScaleType() {
		return scaleType;
	}


	public void setScaleType(Integer scaleType) {
		this.scaleType = scaleType;
	}

	/**
	 * Indicator Staggered Rates
	 * 0: Not Accumulative
	 * 1: Accumulative 
	 * @return the indAccumulativeScale
	 */
	public Integer getIndAccumulativeScale() {
		return indAccumulativeScale;
	}


	/**
	 * Indicator Staggered Rates
	 * 0: Not Accumulative
	 * 1: Accumulative 
	 * @param indAccumulativeScale the indAccumulativeScale to set
	 */
	public void setIndAccumulativeScale(Integer scaleAccumulative) {
		this.indAccumulativeScale = scaleAccumulative;
	}



	/**
	 * Indicator Scale Rate.
	 *  0: Fixed
	 *  1: Percentage 
	 * @return the indStaging
	 */
	public Integer getIndStaging() {
		return indStaging;
	}



	/**
	 * Indicator Scale Rate.
	 *  0: Fixed
	 *  1: Percentage 
	 * @param indStaging the indStaging to set
	 */
	public void setIndStaging(Integer indStaging) {
		this.indStaging = indStaging;
	}



	/**
	 * @return the indOtherCurrency
	 */
	public Integer getIndOtherCurrency() {
		return indOtherCurrency;
	}



	/**
	 * @param indOtherCurrency the indOtherCurrency to set
	 */
	public void setIndOtherCurrency(Integer indOtherCurrency) {
		this.indOtherCurrency = indOtherCurrency;
	}



	/**
	 * @return the otherCurrency
	 */
	public Integer getOtherCurrency() {
		return otherCurrency;
	}



	/**
	 * @param otherCurrency the otherCurrency to set
	 */
	public void setOtherCurrency(Integer otherCurrency) {
		this.otherCurrency = otherCurrency;
	}



	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		// 
				if (loggerUser != null) {
		            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
		            lastModifyDate = loggerUser.getAuditTime();
		            lastModifyIp = loggerUser.getIpAddress();
		            lastModifyUser = loggerUser.getUserName();
		        }
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}