package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ServiceStateType {
    SERVICE_STATUS_PK(Integer.valueOf(172)),
	SERVICE_STATUS_REGISTER_PK (Integer.valueOf(252)),
	SERVICE_STATUS_ACTIVE_PK(Integer.valueOf(1546)),
	SERVICE_STATUS_CANCELLED_PK(Integer.valueOf(1409)),
	SERVICE_STATUS_EXPIRED_PK (Integer.valueOf(1411)),
	SERVICE_STATUS_LOCK_PK (Integer.valueOf(1410)),
	SERVICE_STATUS_UNLOCK_PK (Integer.valueOf(1));
    
    /** The code. */
	private Integer code;
	
	ServiceStateType(Integer code){
		this.code = code;
	}
	
	/** The Constant list. */
	public static final List<ServiceStateType> list = new ArrayList<ServiceStateType>();

	/** The Constant lookup. */
	public static final Map<Integer, ServiceStateType> lookup = new HashMap<Integer, ServiceStateType>();

	static {
		for (ServiceStateType s : EnumSet.allOf(ServiceStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
}
