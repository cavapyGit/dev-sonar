package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum RateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum RateType {
	
	/** The fixed. */
	  
	FIJO(Integer.valueOf(1467),"FIJO"),
	
	/** The percentage. */
	PORCENTUAL(Integer.valueOf(1468),"PORCENTUAL"),
	
	/** The staggered fixed. */
	ESCALONADO_FIJO(Integer.valueOf(1469),"ESCALONADO_FIJO"),
	
	/** The staggered percentage. */
	ESCALONADO_PORCENTUAL(Integer.valueOf(1525),"ESCALONADO_PORCENTUAL"),
	
	/** The staggered mix. */
	ESCALONADO_MIXTO(Integer.valueOf(1892),"ESCALONADO_MIXTO"),
	
	/** The movement fixed**/
	TIPO_MOVIMIENTO_FIJO(Integer.valueOf(1471),"TIPO_MOVIMIENTO_FIJO"),
	
	/** The movement percentage **/
	TIPO_MOVIMIENTO_PORCENTUAL(Integer.valueOf(1524),"TIPO_MOVIMIENTO_PORCENTUAL");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private RateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<RateType> list = new ArrayList<RateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RateType> lookup = new HashMap<Integer, RateType>();
	static {
		for (RateType s : EnumSet.allOf(RateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant state type
	 */
	public static RateType get(Integer code) {
		return lookup.get(code);
	}

}
