package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	// TODO: Auto-generated Javadoc
	/**
	 * <ul>
	 * <li>
	 * Project Pradera.
	 * Copyright 2014.</li>
	 * </ul>
	 * 
	 * The Enum BillingPeriodType.
	 *
	 * @author PraderaTechnologies.
	 * @version 1.0 , 14/12/2014
	 */
	public enum BillingPeriodType {
		
	
		DAILY(Integer.valueOf(255),"DIARIO"), 

		WEEKLY(Integer.valueOf(2077),"SEMANAL"), 
		
		MONTHLY(Integer.valueOf(1428),"MENSUAL"), 
		
		QUARTERLY(Integer.valueOf(2078),"TRIMESTRAL"), 
		
		YEARLY(Integer.valueOf(1429),"ANUAL"), 
		
		EVENT(Integer.valueOf(1430),"POR EVENTO");
		
		/** The code. */
		private Integer code;
		
		/** The value. */
		private String value;
		
		/** The Constant list. */
		public static final List<BillingPeriodType> list = new ArrayList<BillingPeriodType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, BillingPeriodType> lookup = new HashMap<Integer, BillingPeriodType>();

		static {
			for (BillingPeriodType s : EnumSet.allOf(BillingPeriodType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		/**
		 * Instantiates a new document type.
		 *
		 * @param code the code
		 * @param value the value
		 * @param abrev the abrev
		 */
		private BillingPeriodType(Integer code, String value) {
			this.code = code;
			this.value = value;
		}
		
		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}
		
		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		
		/**
		 * Gets the description.
		 *
		 * @param locale the locale
		 * @return the description
		 */
		public String getDescription(Locale locale) {
			return null;
		}
		
		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the document type
		 */
		public static BillingPeriodType get(Integer code) {
			return lookup.get(code);
		}
}