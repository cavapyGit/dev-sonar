package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	// TODO: Auto-generated Javadoc
	/**
	 * <ul>
	 * <li>
	 * Project Pradera.
	 * Copyright 2014.</li>
	 * </ul>
	 * 
	 * The Enum EntityCollectionType.
	 *
	 * @author PraderaTechnologies.
	 * @version 1.0 , 14/07/2014
	 */
	public enum EntityCollectionType {
		
		MANAGER(Integer.valueOf(251),"ADMIN"), 

		STOCK_EXCHANGE(Integer.valueOf(1405),"BOLSA DE VALORES"), 
		
		ISSUERS(Integer.valueOf(1406),"EMISORES"), 
		
		PARTICIPANTS(Integer.valueOf(1407),"PARTICIPANTES"), 
		
		HOLDERS(Integer.valueOf(1408),"HOLDER"), 
		
		OTHERS(Integer.valueOf(1530),"OTROS"), 		
		
		CENTRAL_BANK(Integer.valueOf(1435),"BANCO CENTRAL"), 
		
		HACIENDA(Integer.valueOf(1470),"MINISTERIO DE HACIENDA"), 
		
		AFP(Integer.valueOf(1726),"AFP"),
		
		REGULATOR_ENTITY(Integer.valueOf(1730),"ENTIDAD DE REGULACION");
		
		/** The code. */
		private Integer code;
		
		/** The value. */
		private String value;
		
		/** The Constant list. */
		public static final List<EntityCollectionType> list = new ArrayList<EntityCollectionType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, EntityCollectionType> lookup = new HashMap<Integer, EntityCollectionType>();

		static {
			for (EntityCollectionType s : EnumSet.allOf(EntityCollectionType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		/**
		 * Instantiates a new document type.
		 *
		 * @param code the code
		 * @param value the value
		 * @param abrev the abrev
		 */
		private EntityCollectionType(Integer code, String value) {
			this.code = code;
			this.value = value;
		}
		
		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}
		
		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		
		/**
		 * Gets the description.
		 *
		 * @param locale the locale
		 * @return the description
		 */
		public String getDescription(Locale locale) {
			return null;
		}
		
		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the document type
		 */
		public static EntityCollectionType get(Integer code) {
			return lookup.get(code);
		}
}