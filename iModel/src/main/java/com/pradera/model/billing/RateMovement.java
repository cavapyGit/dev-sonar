package com.pradera.model.billing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.MovementType;


/**
 * The persistent class for the RATE_MOVEMENT database table.
 * 
 */
@Entity
@Table(name="RATE_MOVEMENT")
public class RateMovement implements Serializable, Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RATE_MOVEMENT_GENERATOR", sequenceName="SQ_ID_RATE_MOVEMENT_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RATE_MOVEMENT_GENERATOR")
	@Column(name="ID_RATE_MOVEMENT_PK")
	private Integer idRateMovementPk;


	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;


	@Column(name="RATE_AMOUNT")
	private BigDecimal rateAmount;

	@Column(name="RATE_PERCENT")
	private BigDecimal ratePercent;

	//bi-directional many-to-one association to ServiceRate
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SERVICE_RATE_FK")
	private ServiceRate serviceRate;
	
	
	//bi-directional many-to-one association to ServiceRate
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOVEMENT_TYPE_FK")
	private MovementType movementType;
	
	public RateMovement() {
	}

	public long getIdRateMovementPk() {
		return this.idRateMovementPk;
	}

	public MovementType getMovementType() {
		return movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public void setIdRateMovementPk(Integer idRateMovementPk) {
		this.idRateMovementPk = idRateMovementPk;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getRateAmount() {
		return this.rateAmount;
	}

	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}

	public BigDecimal getRatePercent() {
		return this.ratePercent;
	}

	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}

	public ServiceRate getServiceRate() {
		return this.serviceRate;
	}

	public void setServiceRate(ServiceRate serviceRate) {
		this.serviceRate = serviceRate;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}