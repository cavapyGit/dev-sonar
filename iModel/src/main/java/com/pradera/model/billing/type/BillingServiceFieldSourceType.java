package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project EDV.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Enum AccountingFieldSourceType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2014
*/
public enum  BillingServiceFieldSourceType {
	
	ACCOUNT_NUMBER(Integer.valueOf(2045),"ACCOUNT_NUMBER"),//
	
	CANTIDAD_MOVIMIENTOS(Integer.valueOf(2046),"CANTIDAD_MOVIMIENTOS"),//////////////////////
	
	CURRENCY(Integer.valueOf(2047),"CURRENCY"),//
	
	CURRENT_NOMINAL_VALUE(Integer.valueOf(2048),"CURRENT_NOMINAL_VALUE"),/////////////////
	
	DESMATERIALIZED_BALANCE(Integer.valueOf(2049),"DESMATERIALIZED_BALANCE"),//
	
	HOLDER(Integer.valueOf(2050),"ID_HOLDER_FK"),//
	
	HOLDER_ACCOUNT(Integer.valueOf(2051),"ID_HOLDER_ACCOUNT_FK"),//
	
	ISSUANCE_CODE(Integer.valueOf(2052),"ID_ISSUANCE_CODE_FK"),//
	
	ISSUER(Integer.valueOf(2053),"ID_ISSUER_FK"),//
	
	MOVEMENT_DATE(Integer.valueOf(2054),"MOVEMENT_DATE"),//
	
	MOVEMENT_QUANTITY(Integer.valueOf(2055),"MOVEMENT_QUANTITY"),//
	
	MOVEMENT_TYPE(Integer.valueOf(2056),"ID_MOVEMENT_TYPE_FK"),//
	
	NOMINAL_VALUE(Integer.valueOf(2057),"NOMINAL_VALUE"),//
	
	NOT_PLACED(Integer.valueOf(2058),"NOT_PLACED"),//
	
	OPERATION_DATE(Integer.valueOf(2059),"OPERATION_DATE"),//
	
	OPERATION_NUMBER(Integer.valueOf(2060),"OPERATION_NUMBER"),//
	
	OPERATION_PRICE(Integer.valueOf(2061),"OPERATION_PRICE"),
	
	PARTICIPANT(Integer.valueOf(2062),"ID_PARTICIPANT_FK"),//
	
	PLACED_AMOUNT(Integer.valueOf(2063),"PLACED_AMOUNT"),
	
	PRICE(Integer.valueOf(2064),"PRICE"),
	
	REGISTRY_DATE(Integer.valueOf(2065),"REGISTRY_DATE"),
	
	SECURITY(Integer.valueOf(2066),"ID_SECURITY_CODE_FK"),//
	
	SECURITY_CLASS(Integer.valueOf(2067),"SECURITY_CLASS"),//

	SOURCE_PARTICIPANT(Integer.valueOf(2068),"ID_SOURCE_PARTICIPANT_FK"),
	
	TARGET_PARTICIPANT(Integer.valueOf(2069),"ID_TARGET_PARTICIPANT_FK"),

	TOTAL_BALANCE(Integer.valueOf(2070),"TOTAL_BALANCE"),//
	
	SEQUENTIAL(Integer.valueOf(2289),"SEQUENTIAL"),//
	
	BALLOT_SEQUENTIAL(Integer.valueOf(2290),"BALLOT_SEQUENTIAL"),//
	
	NEGOTIATION_MODALITY(Integer.valueOf(2291),"ID_NEGOTIATION_MODALITY_FK"),//
	
	ROLE(Integer.valueOf(2292),"ROLE"),//
	
	BALLOT(Integer.valueOf(2293),"BALLOT_NUMBER"),//
	
	;
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingFieldSourceType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BillingServiceFieldSourceType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<BillingServiceFieldSourceType> list = new ArrayList<BillingServiceFieldSourceType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BillingServiceFieldSourceType> lookup      = new HashMap<Integer, BillingServiceFieldSourceType>();
	public static final Map<String, BillingServiceFieldSourceType>  lookupValue = new HashMap<String, BillingServiceFieldSourceType>();
	
	static {
		for (BillingServiceFieldSourceType s : EnumSet.allOf(BillingServiceFieldSourceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
			lookupValue.put(s.getValue(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingFieldSourceType state type
	 */
	public static BillingServiceFieldSourceType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static BillingServiceFieldSourceType get(String value) {
		return lookupValue.get(value);
	}
	
}
