package com.pradera.model.billing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="COLLECTION_RECORD_DETAIL")
public class CollectionRecordDetail implements Serializable ,Auditable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="COLLECTIONRECORD_DETAIL_PK_GENERATOR", sequenceName="SQ_COLLECTION_RECORD_DETAIL_PK", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COLLECTIONRECORD_DETAIL_PK_GENERATOR")
	@Column(name="ID_COLLECTION_RECORD_DETAIL_PK", unique=true, nullable=false, precision=10)
	private Long idCollectionRecordDetailPk;
	
	@Column(name="DESCRIPTION_RATE", nullable=false)
	private String descriptionRate;
	
	@Column(name="RATE_CODE", precision=10)
	private Integer rateCode;
	
	@Column(name="MOVEMENT_COUNT", precision=10)
	private Integer movementCount;
	
	@Column(name="ID_MOVEMENT_TYPE_FK")
	private Long idMovementTypePk;
	
	@Column(name = "ID_SECURITY_CODE_FK")
	private String idSecurityCodeFk;
	
	@Column(name="RATE_AMOUNT", precision=30, scale=8)
	private BigDecimal rateAmount;
	
	@Column(name="RATE_PERCENT", precision=30, scale=8)
	private BigDecimal ratePercent;

	@Column(name="SEQUENCE_NUMBER", precision=10)
	private Integer sequenceNumber;
	
	@Column(name="CURRENCY_RATE", precision=10)
	private Integer currencyRate;
		
	@Column(name="MOVEMENT_QUANTITY", precision=10 )
	private Long movementQuantity;
	
	@Column(name="OPERATION_PRICE", precision=30, scale=8)
	private BigDecimal operationPrice;
	
	@Transient	
	private Date calculationDate;
	
	//bi-directional many-to-one association to CollectionRecord
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COLLECTION_RECORD_FK")
	private CollectionRecord collectionRecord;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SERVICE_RATE_FK")
	private ServiceRate serviceRate;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="MOVEMENT_DATE")
	private Date movementDate ;
	 
	@Column(name="OPERATION_DATE")
	private Date operationDate ;
	 
	@Column(name="OPERATION_NUMBER")
	private Long operationNumber ;
	 
	@Column(name="ID_SOURCE_PARTICIPANT_FK")
	private Long idSourceParticipant ;
	 
	@Column(name="ID_TARGET_PARTICIPANT_FK")
	private Long idTargetParticipant ;
	 
	@Column(name="ID_HOLDER_ACCOUNT_FK")
	private Long holderAccount ;
	
	@Column(name="ID_HOLDER_FK")
	private Long idHolder ;
	
	@Column(name="RATE_AMOUNT_BILLED", precision=30, scale=8)
	private BigDecimal rateAmountBilled;
	
	@Column(name="RATE_PERCENT_BILLED", precision=30, scale=8)
	private BigDecimal ratePercentBilled;
	
	@Column(name="EXCHANGE_RATE_CURRENT", precision=30, scale=8)
	private BigDecimal exchangeRateCurrent;
	
	/** The security class. */
	@Column(name = "SECURITY_CLASS")
	private Integer securityClass;

	@Column(name = "ID_ISSUANCE_CODE_FK")
	private String idIssuanceCodeFk;
	
	/** The security class. */
	@Column(name = "CURRENCY_ORIGIN")
	private Integer currencyOrigin;
	
	/***The issuer */
	@Column(name="ID_ISSUER_FK")
	private String idIssuer;
	
	/** The security class. */
	@Column(name = "MODALITY")
	private Integer modality;
	
	@Column(name="NOMINAL_VALUE", precision=30, scale=8)
	private BigDecimal nominalValue;
	
	@Column(name="CALCULATION_BALANCE", precision=30, scale=8)
	private BigDecimal calculationBalance;
	
	@Column(name="BALLOT_SEQ")
	private String ballotSequential;
	
	@Column(name="ROLE")
	private Integer role;
	
	@Column(name="NEGOTIATED_AMOUNT_ORIGIN", precision=30, scale=8)
	private BigDecimal negotiatedAmountOrigin;
	
	@Column(name="NEGOTIATED_AMOUNT", precision=30, scale=8)
	private BigDecimal negotiatedAmount;
	
	@Column(name="MNEMONIC_HOLDER")
	private String mnemonicHolder;
	
	@Column(name="ECONOMIC_ACTIVITY_PART")
	private Integer economicActivityParticipant;
	
	@Column(name="PORTFOLIO")
	private Integer portfolio;
	
	public CollectionRecordDetail(){
		
	}
	

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		return null;
	}

	/**
	 * @return the idCollectionRecordDetailPk
	 */
	public Long getIdCollectionRecordDetailPk() {
		return idCollectionRecordDetailPk;
	}

	/**
	 * @param idCollectionRecordDetailPk the idCollectionRecordDetailPk to set
	 */
	public void setIdCollectionRecordDetailPk(Long idCollectionRecordDetailPk) {
		this.idCollectionRecordDetailPk = idCollectionRecordDetailPk;
	}

	/**
	 * @return the descriptionRate
	 */
	public String getDescriptionRate() {
		return descriptionRate;
	}

	/**
	 * @param descriptionRate the descriptionRate to set
	 */
	public void setDescriptionRate(String descriptionRate) {
		this.descriptionRate = descriptionRate;
	}

	/**
	 * @return the rateCode
	 */
	public Integer getRateCode() {
		return rateCode;
	}

	/**
	 * @param rateCode the rateCode to set
	 */
	public void setRateCode(Integer rateCode) {
		this.rateCode = rateCode;
	}

	/**
	 * @return the movementCount
	 */
	public Integer getMovementCount() {
		return movementCount;
	}

	/**
	 * @param movementCount the movementCount to set
	 */
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}

	/**
	 * @return the idMovementTypePk
	 */
	public Long getIdMovementTypePk() {
		return idMovementTypePk;
	}

	/**
	 * @param idMovementTypePk the idMovementTypePk to set
	 */
	public void setIdMovementTypePk(Long idMovementTypePk) {
		this.idMovementTypePk = idMovementTypePk;
	}

	/**
	 * @return the idSecurityCodeFk
	 */
	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}

	/**
	 * @param idSecurityCodeFk the idSecurityCodeFk to set
	 */
	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}

	/**
	 * @return the rateAmount
	 */
	public BigDecimal getRateAmount() {
		return rateAmount;
	}

	/**
	 * @param rateAmount the rateAmount to set
	 */
	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}

	/**
	 * @return the ratePercent
	 */
	public BigDecimal getRatePercent() {
		return ratePercent;
	}

	/**
	 * @param ratePercent the ratePercent to set
	 */
	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}

	/**
	 * @return the sequenceNumber
	 */
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * @param sequenceNumber the sequenceNumber to set
	 */
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	/**
	 * @return the currencyRate
	 */
	public Integer getCurrencyRate() {
		return currencyRate;
	}

	/**
	 * @param currencyRate the currencyRate to set
	 */
	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}

	/**
	 * @return the movementQuantity
	 */
	public Long getMovementQuantity() {
		return movementQuantity;
	}

	/**
	 * @param movementQuantity the movementQuantity to set
	 */
	public void setMovementQuantity(Long movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	/**
	 * @return the operationPrice
	 */
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}

	/**
	 * @param operationPrice the operationPrice to set
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}

	/**
	 * @return the calculationDate
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}

	/**
	 * @param calculationDate the calculationDate to set
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}

	/**
	 * @return the collectionRecord
	 */
	public CollectionRecord getCollectionRecord() {
		return collectionRecord;
	}

	/**
	 * @param collectionRecord the collectionRecord to set
	 */
	public void setCollectionRecord(CollectionRecord collectionRecord) {
		this.collectionRecord = collectionRecord;
	}

	/**
	 * @return the serviceRate
	 */
	public ServiceRate getServiceRate() {
		return serviceRate;
	}

	/**
	 * @param serviceRate the serviceRate to set
	 */
	public void setServiceRate(ServiceRate serviceRate) {
		this.serviceRate = serviceRate;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the movementDate
	 */
	public Date getMovementDate() {
		return movementDate;
	}

	/**
	 * @param movementDate the movementDate to set
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	/**
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * @return the operationNumber
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * @return the idSourceParticipant
	 */
	public Long getIdSourceParticipant() {
		return idSourceParticipant;
	}

	/**
	 * @param idSourceParticipant the idSourceParticipant to set
	 */
	public void setIdSourceParticipant(Long idSourceParticipant) {
		this.idSourceParticipant = idSourceParticipant;
	}

	/**
	 * @return the idTargetParticipant
	 */
	public Long getIdTargetParticipant() {
		return idTargetParticipant;
	}

	/**
	 * @param idTargetParticipant the idTargetParticipant to set
	 */
	public void setIdTargetParticipant(Long idTargetParticipant) {
		this.idTargetParticipant = idTargetParticipant;
	}

	/**
	 * @return the holderAccount
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}

	/**
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * @return the idHolder
	 */
	public Long getIdHolder() {
		return idHolder;
	}

	/**
	 * @param idHolder the idHolder to set
	 */
	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}


	/**
	 * @return the rateAmountBilled
	 */
	public BigDecimal getRateAmountBilled() {
		return rateAmountBilled;
	}


	/**
	 * @param rateAmountBilled the rateAmountBilled to set
	 */
	public void setRateAmountBilled(BigDecimal rateAmountBilled) {
		this.rateAmountBilled = rateAmountBilled;
	}


	/**
	 * @return the ratePercentBilled
	 */
	public BigDecimal getRatePercentBilled() {
		return ratePercentBilled;
	}


	/**
	 * @param ratePercentBilled the ratePercentBilled to set
	 */
	public void setRatePercentBilled(BigDecimal ratePercentBilled) {
		this.ratePercentBilled = ratePercentBilled;
	}


	/**
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}


	/**
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}


	/**
	 * @return the currencyOrigin
	 */
	public Integer getCurrencyOrigin() {
		return currencyOrigin;
	}


	/**
	 * @param currencyOrigin the currencyOrigin to set
	 */
	public void setCurrencyOrigin(Integer currencyOrigin) {
		this.currencyOrigin = currencyOrigin;
	}


	/**
	 * @return the modality
	 */
	public Integer getModality() {
		return modality;
	}


	/**
	 * @param modality the modality to set
	 */
	public void setModality(Integer modality) {
		this.modality = modality;
	}


	/**
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}


	/**
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}


	/**
	 * @return the calculationBalance
	 */
	public BigDecimal getCalculationBalance() {
		return calculationBalance;
	}


	/**
	 * @param calculationBalance the calculationBalance to set
	 */
	public void setCalculationBalance(BigDecimal calculationBalance) {
		this.calculationBalance = calculationBalance;
	}


	/**
	 * 
	 * Liq: Ballot Sequential
	 * Custody: Number Operation
	 * @return the ballotSequential
	 */
	public String getBallotSequential() {
		return ballotSequential;
	}


	/**
	 * Liq: Ballot Sequential
	 * Custody: Number Operation
	 * @param ballotSequential the ballotSequential to set
	 */
	public void setBallotSequential(String ballotSequential) {
		this.ballotSequential = ballotSequential;
	}


	/**
	 * @return the negotiatedAmountOrigin
	 */
	public BigDecimal getNegotiatedAmountOrigin() {
		return negotiatedAmountOrigin;
	}


	/**
	 * @param negotiatedAmountOrigin the negotiatedAmountOrigin to set
	 */
	public void setNegotiatedAmountOrigin(BigDecimal negotiatedAmountOrigin) {
		this.negotiatedAmountOrigin = negotiatedAmountOrigin;
	}


	/**
	 * @return the negotiatedAmount
	 */
	public BigDecimal getNegotiatedAmount() {
		return negotiatedAmount;
	}


	/**
	 * @param negotiatedAmount the negotiatedAmount to set
	 */
	public void setNegotiatedAmount(BigDecimal negotiatedAmount) {
		this.negotiatedAmount = negotiatedAmount;
	}


	/**
	 * Liq: Role 
	 * Custody: Block Type
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}


	/**
	 * @param role the role to set
	 */
	public void setRole(Integer role) {
		this.role = role;
	}


	/**
	 * @return the exchangeRateCurrent
	 */
	public BigDecimal getExchangeRateCurrent() {
		return exchangeRateCurrent;
	}


	/**
	 * @param exchangeRateCurrent the exchangeRateCurrent to set
	 */
	public void setExchangeRateCurrent(BigDecimal exchangeRateCurrent) {
		this.exchangeRateCurrent = exchangeRateCurrent;
	}


	/**
	 * @return the idIssuanceCodeFk
	 */
	public String getIdIssuanceCodeFk() {
		return idIssuanceCodeFk;
	}


	/**
	 * @param idIssuanceCodeFk the idIssuanceCodeFk to set
	 */
	public void setIdIssuanceCodeFk(String idIssuanceCodeFk) {
		this.idIssuanceCodeFk = idIssuanceCodeFk;
	}


	/**
	 * @return the idIssuer
	 */
	public String getIdIssuer() {
		return idIssuer;
	}


	/**
	 * @param idIssuer the idIssuer to set
	 */
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}


	/**
	 * @return the mnemonicHolder
	 */
	public String getMnemonicHolder() {
		return mnemonicHolder;
	}


	/**
	 * @param mnemonicHolder the mnemonicHolder to set
	 */
	public void setMnemonicHolder(String mnemonicHolder) {
		this.mnemonicHolder = mnemonicHolder;
	}


	/**
	 * @return the economicActivityParticipant
	 */
	public Integer getEconomicActivityParticipant() {
		return economicActivityParticipant;
	}


	/**
	 * @param economicActivityParticipant the economicActivityParticipant to set
	 */
	public void setEconomicActivityParticipant(Integer economicActivityParticipant) {
		this.economicActivityParticipant = economicActivityParticipant;
	}


	/**
	 * @return the portfolio
	 */
	public Integer getPortfolio() {
		return portfolio;
	}


	/**
	 * @param portfolio the portfolio to set
	 */
	public void setPortfolio(Integer portfolio) {
		this.portfolio = portfolio;
	}





}
