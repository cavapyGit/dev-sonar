package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2013.</li>
 * </ul>
 * 
 * The Enum ServiceClientType.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 14/07/2013
 */
public enum ServiceClientType {

	EMISORES(Integer.valueOf(1520), "EMISORES"),

	PARTICIPANTES(Integer.valueOf(1521), "PARTICIPANTES"),

	TITULARES(Integer.valueOf(1522), "TITULARES"),

	OTROS(Integer.valueOf(1523), "OTROS");

	/** The code. */
	private Integer code;

	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<ServiceClientType> list = new ArrayList<ServiceClientType>();

	/** The Constant lookup. */
	public static final Map<Integer, ServiceClientType> lookup = new HashMap<Integer, ServiceClientType>();

	static {
		for (ServiceClientType s : EnumSet.allOf(ServiceClientType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new document type.
	 * 
	 * @param code
	 *            the code
	 * @param value
	 *            the value
	 * @param abrev
	 *            the abrev
	 */
	private ServiceClientType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Gets the description.
	 * 
	 * @param locale
	 *            the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return null;
	}

	/**
	 * Gets the.
	 * 
	 * @param code
	 *            the code
	 * @return the document type
	 */
	public static ServiceClientType get(Integer code) {
		return lookup.get(code);
	}
}