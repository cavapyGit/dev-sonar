package com.pradera.model.billing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the COLLECTION_RECORD database table.
 * 
 */
@Entity
@Table(name="COLLECTION_RECORD")
public class CollectionRecord implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="COLLECTIONRECORDPK_GENERATOR", sequenceName="SQ_ID_COLLECTION_RECORD_PK", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COLLECTIONRECORDPK_GENERATOR")
	@Column(name="ID_COLLECTION_RECORD_PK", unique=true, nullable=false, precision=10)
	private Long idCollectionRecordPk;

	@Column(name="ENTITY_COLLECTION", precision=10)
	private Integer entityCollectionId;

	@Column(name="COLLECTION_AMOUNT", precision=30, scale=8)
	private BigDecimal collectionAmount;
	
	@Column(name="GROSS_AMOUNT", nullable=false, precision=30, scale=8)
	private BigDecimal grossAmount;
	
	@Column(name="TAX_APPLIED", precision=30, scale=8)
	private BigDecimal taxApplied;

	@Column(name="IND_BILLED", nullable=false, precision=1)
	private Integer indBilled;
	
	@Column(name="CURRENCY_TYPE", precision=10)
	private Integer currencyType;

	@Column(name="OTHER_ENTITY_DESCRIPTION", length=100)
	private String otherEntityDescription;

	@Column(name="OTHER_ENTITY_DOCUMENT", length=21)
	private String otherEntityDocument;

	/** The residence country. */
	@Column(name="OTHER_ENTITY_COUNTRY")	
	private Integer otherEntityCountry;
	
	/** The department. */
	@Column(name="OTHER_ENTITY_DEPARTMENT")
	private Integer otherEntityDepartment;
	
	/** The province. */
	@Column(name="OTHER_ENTITY_PROVINCE")	
	private Integer otherEntityProvince;
	
	/** The district. */
	@Column(name="OTHER_ENTITY_DISTRICT")
	private Integer otherEntityDistrict;

	/** The address. */
	@Column(name="OTHER_ENTITY_ADDRESS")	
	private String otherEntityAddress;
	
	@Column(name="RECORD_STATE", precision=10)
	private Integer recordState;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CALCULATION_DATE")
	private Date calculationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Transient	
	private Integer entityCollection;
	
	//bi-directional many-to-one association to ProcessedService
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCESSED_SERVICE_FK",referencedColumnName="ID_PROCESSED_SERVICE_PK")
	private ProcessedService processedService;
	
	@OneToMany(mappedBy="collectionRecord")
	private List<CollectionRecordDetail> collectionRecordDetail;
	
	@Column(name="KEY_ENTITY_COLLECTION",length=200)
	private String keyEntityCollection;
	
	@Column(name="DOCUMENT_TYPE", precision=10)	
	private Integer documentType;
	
	@Column(name="COLLECTION_AMOUNT_BILLED", precision=30, scale=8)
	private BigDecimal collectionAmountBilled;
	
	@Column(name="GROSS_AMOUNT_BILLED", precision=30, scale=8)
	private BigDecimal grossAmountBilled;
	
	@Column(name="TAX_APPLIED_BILLED", precision=30, scale=8)
	private BigDecimal taxAppliedBilled;

	@Column(name="EXCHANGE_RATE_CURRENT", precision=30, scale=8)
	private BigDecimal exchangeRateCurrent;
	
	@Column(name="CURRENCY_BILLING", precision=10)
	private Integer currencyBilling;
	
	@Column(name="COUNT_REQUEST", precision=10)
	private Integer countRequest;
	
	@Column(name="REQUESTS_CONCATENATED", length=1000)
	private String requestConcatenated;
	
	@Column(name="IND_MINIMUM_AMOUNT")
	private Integer indMinimumAmount;
	
	@Column(name="MINIMUM_AMOUNT")
	private BigDecimal minimumAmount;
	
	@Column(name="CURRENCY_MINIMUM_AMOUNT")
	private Integer currencyMinimumAmount;
	
	@Column(name="MNEMONIC_CONCATENATED", length=1000)
	private String mnemonicConcatenated;
	
	public CollectionRecord() {
	}

	
	

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("collectionRecordDetail", collectionRecordDetail);
        return detailsMap;
	}




	/**
	 * @return the idCollectionRecordPk
	 */
	public Long getIdCollectionRecordPk() {
		return idCollectionRecordPk;
	}




	/**
	 * @param idCollectionRecordPk the idCollectionRecordPk to set
	 */
	public void setIdCollectionRecordPk(Long idCollectionRecordPk) {
		this.idCollectionRecordPk = idCollectionRecordPk;
	}




	/**
	 * @return the entityCollectionId
	 */
	public Integer getEntityCollectionId() {
		return entityCollectionId;
	}




	/**
	 * @param entityCollectionId the entityCollectionId to set
	 */
	public void setEntityCollectionId(Integer entityCollectionId) {
		this.entityCollectionId = entityCollectionId;
	}




	/**
	 * @return the collectionAmount
	 */
	public BigDecimal getCollectionAmount() {
		return collectionAmount;
	}




	/**
	 * @param collectionAmount the collectionAmount to set
	 */
	public void setCollectionAmount(BigDecimal collectionAmount) {
		this.collectionAmount = collectionAmount;
	}




	/**
	 * @return the grossAmount
	 */
	public BigDecimal getGrossAmount() {
		return grossAmount;
	}




	/**
	 * @param grossAmount the grossAmount to set
	 */
	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}




	/**
	 * @return the taxApplied
	 */
	public BigDecimal getTaxApplied() {
		return taxApplied;
	}




	/**
	 * @param taxApplied the taxApplied to set
	 */
	public void setTaxApplied(BigDecimal taxApplied) {
		this.taxApplied = taxApplied;
	}




	/**
	 * @return the indBilled
	 */
	public Integer getIndBilled() {
		return indBilled;
	}




	/**
	 * @param indBilled the indBilled to set
	 */
	public void setIndBilled(Integer indBilled) {
		this.indBilled = indBilled;
	}




	/**
	 * @return the currencyType
	 */
	public Integer getCurrencyType() {
		return currencyType;
	}




	/**
	 * @param currencyType the currencyType to set
	 */
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}




	/**
	 * @return the otherEntityDescription
	 */
	public String getOtherEntityDescription() {
		return otherEntityDescription;
	}




	/**
	 * @param otherEntityDescription the otherEntityDescription to set
	 */
	public void setOtherEntityDescription(String otherEntityDescription) {
		this.otherEntityDescription = otherEntityDescription;
	}




	/**
	 * @return the otherEntityDocument
	 */
	public String getOtherEntityDocument() {
		return otherEntityDocument;
	}




	/**
	 * @param otherEntityDocument the otherEntityDocument to set
	 */
	public void setOtherEntityDocument(String otherEntityDocument) {
		this.otherEntityDocument = otherEntityDocument;
	}




	/**
	 * @return the otherEntityCountry
	 */
	public Integer getOtherEntityCountry() {
		return otherEntityCountry;
	}




	/**
	 * @param otherEntityCountry the otherEntityCountry to set
	 */
	public void setOtherEntityCountry(Integer otherEntityCountry) {
		this.otherEntityCountry = otherEntityCountry;
	}




	/**
	 * @return the otherEntityDepartment
	 */
	public Integer getOtherEntityDepartment() {
		return otherEntityDepartment;
	}




	/**
	 * @param otherEntityDepartment the otherEntityDepartment to set
	 */
	public void setOtherEntityDepartment(Integer otherEntityDepartment) {
		this.otherEntityDepartment = otherEntityDepartment;
	}




	/**
	 * @return the otherEntityProvince
	 */
	public Integer getOtherEntityProvince() {
		return otherEntityProvince;
	}




	/**
	 * @param otherEntityProvince the otherEntityProvince to set
	 */
	public void setOtherEntityProvince(Integer otherEntityProvince) {
		this.otherEntityProvince = otherEntityProvince;
	}




	/**
	 * @return the otherEntityDistrict
	 */
	public Integer getOtherEntityDistrict() {
		return otherEntityDistrict;
	}




	/**
	 * @param otherEntityDistrict the otherEntityDistrict to set
	 */
	public void setOtherEntityDistrict(Integer otherEntityDistrict) {
		this.otherEntityDistrict = otherEntityDistrict;
	}




	/**
	 * @return the otherEntityAddress
	 */
	public String getOtherEntityAddress() {
		return otherEntityAddress;
	}




	/**
	 * @param otherEntityAddress the otherEntityAddress to set
	 */
	public void setOtherEntityAddress(String otherEntityAddress) {
		this.otherEntityAddress = otherEntityAddress;
	}




	/**
	 * @return the recordState
	 */
	public Integer getRecordState() {
		return recordState;
	}




	/**
	 * @param recordState the recordState to set
	 */
	public void setRecordState(Integer recordState) {
		this.recordState = recordState;
	}




	/**
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}




	/**
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}




	/**
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}




	/**
	 * @param securities the securities to set
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}




	/**
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}




	/**
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}




	/**
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}




	/**
	 * @param participant the participant to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}




	/**
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}




	/**
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}




	/**
	 * @return the calculationDate
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}




	/**
	 * @param calculationDate the calculationDate to set
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}




	/**
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}




	/**
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}




	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}




	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}




	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}




	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}




	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}




	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}




	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}




	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}




	/**
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}




	/**
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}




	/**
	 * @return the processedService
	 */
	public ProcessedService getProcessedService() {
		return processedService;
	}




	/**
	 * @param processedService the processedService to set
	 */
	public void setProcessedService(ProcessedService processedService) {
		this.processedService = processedService;
	}




	/**
	 * @return the collectionRecordDetail
	 */
	public List<CollectionRecordDetail> getCollectionRecordDetail() {
		return collectionRecordDetail;
	}




	/**
	 * @param collectionRecordDetail the collectionRecordDetail to set
	 */
	public void setCollectionRecordDetail(
			List<CollectionRecordDetail> collectionRecordDetail) {
		this.collectionRecordDetail = collectionRecordDetail;
	}




	/**
	 * @return the keyEntityCollection
	 */
	public String getKeyEntityCollection() {
		return keyEntityCollection;
	}




	/**
	 * @param keyEntityCollection the keyEntityCollection to set
	 */
	public void setKeyEntityCollection(String keyEntityCollection) {
		this.keyEntityCollection = keyEntityCollection;
	}




	/**
	 * @return the documentType
	 */
	public Integer getDocumentType() {
		return documentType;
	}




	/**
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}




	/**
	 * @return the collectionAmountBilled
	 */
	public BigDecimal getCollectionAmountBilled() {
		return collectionAmountBilled;
	}




	/**
	 * @param collectionAmountBilled the collectionAmountBilled to set
	 */
	public void setCollectionAmountBilled(BigDecimal collectionAmountBilled) {
		this.collectionAmountBilled = collectionAmountBilled;
	}




	/**
	 * @return the grossAmountBilled
	 */
	public BigDecimal getGrossAmountBilled() {
		return grossAmountBilled;
	}




	/**
	 * @param grossAmountBilled the grossAmountBilled to set
	 */
	public void setGrossAmountBilled(BigDecimal grossAmountBilled) {
		this.grossAmountBilled = grossAmountBilled;
	}




	/**
	 * @return the taxAppliedBilled
	 */
	public BigDecimal getTaxAppliedBilled() {
		return taxAppliedBilled;
	}




	/**
	 * @param taxAppliedBilled the taxAppliedBilled to set
	 */
	public void setTaxAppliedBilled(BigDecimal taxAppliedBilled) {
		this.taxAppliedBilled = taxAppliedBilled;
	}




	/**
	 * The exchange rate at the date of billing
	 *  ExchangeRate To Billed
	 * @return the exchangeRateCurrent
	 */
	public BigDecimal getExchangeRateCurrent() {
		return exchangeRateCurrent;
	}




	/**
	 * The exchange rate at the date of billing
	 * ExchangeRate To Billed
	 * @param exchangeRateCurrent the exchangeRateCurrent to set
	 */
	public void setExchangeRateCurrent(BigDecimal exchangeRateCurrent) {
		this.exchangeRateCurrent = exchangeRateCurrent;
	}




	/**
	 * @return the currencyBilling
	 */
	public Integer getCurrencyBilling() {
		return currencyBilling;
	}




	/**
	 * @param currencyBilling the currencyBilling to set
	 */
	public void setCurrencyBilling(Integer currencyBilling) {
		this.currencyBilling = currencyBilling;
	}




	/**
	 * @return the countRequest
	 */
	public Integer getCountRequest() {
		return countRequest;
	}




	/**
	 * @param countRequest the countRequest to set
	 */
	public void setCountRequest(Integer countRequest) {
		this.countRequest = countRequest;
	}




	/**
	 * @return the indMinimumAmount
	 */
	public Integer getIndMinimumAmount() {
		return indMinimumAmount;
	}




	/**
	 * @param indMinimumAmount the indMinimumAmount to set
	 */
	public void setIndMinimumAmount(Integer indMinimumAmount) {
		this.indMinimumAmount = indMinimumAmount;
	}




	/**
	 * @return the minimumAmount
	 */
	public BigDecimal getMinimumAmount() {
		return minimumAmount;
	}




	/**
	 * @param minimumAmount the minimumAmount to set
	 */
	public void setMinimumAmount(BigDecimal minimumAmount) {
		this.minimumAmount = minimumAmount;
	}




	/**
	 * @return the currencyMinimumAmount
	 */
	public Integer getCurrencyMinimumAmount() {
		return currencyMinimumAmount;
	}




	/**
	 * @param currencyMinimumAmount the currencyMinimumAmount to set
	 */
	public void setCurrencyMinimumAmount(Integer currencyMinimumAmount) {
		this.currencyMinimumAmount = currencyMinimumAmount;
	}




	/**
	 * @return the requestConcatenated
	 */
	public String getRequestConcatenated() {
		return requestConcatenated;
	}




	/**
	 * @param requestConcatenated the requestConcatenated to set
	 */
	public void setRequestConcatenated(String requestConcatenated) {
		this.requestConcatenated = requestConcatenated;
	}




	/**
	 * @return the mnemonicConcatenated
	 */
	public String getMnemonicConcatenated() {
		return mnemonicConcatenated;
	}




	/**
	 * @param mnemonicConcatenated the mnemonicConcatenated to set
	 */
	public void setMnemonicConcatenated(String mnemonicConcatenated) {
		this.mnemonicConcatenated = mnemonicConcatenated;
	}

	
}