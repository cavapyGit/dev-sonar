package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	// TODO: Auto-generated Javadoc
	/**
	 * <ul>
	 * <li>
	 * Project Pradera.
	 * Copyright 2014.</li>
	 * </ul>
	 * 
	 * The Enum IndexExceptionType.
	 *
	 * @author PraderaTechnologies.
	 * @version 1.0 , 27/07/2015
	 */
	public enum IndexExceptionType {
		
		/*
		 * 0: Participant
		 * 1: Holder
		 * 2: Security
		 * 3: Security Class
		 * 4: Holder Account
		 * 5: Issuer
		 * 6: Issuance
		 */
		PARTICIPANT(Integer.valueOf(0),"PARTICIPANT"), 

		HOLDER(Integer.valueOf(1),"HOLDER"), 
		
		SECURITY(Integer.valueOf(2),"SECURITY"), 
		
		SECURITY_CLASS(Integer.valueOf(3),"SECURITY_CLASS"), 
		
		HOLDER_ACCOUNT(Integer.valueOf(4),"HOLDER_ACCOUNT"), 
		
		ISSUER(Integer.valueOf(5),"ISSUER"), 		
		
		ISSUANCE(Integer.valueOf(6),"ISSUANCE"),
		
		LENGTH_LIST(Integer.valueOf(7),"LENGTH_LIST");
		
		/** The code. */
		private Integer code;
		
		/** The value. */
		private String value;
		
		/** The Constant list. */
		public static final List<IndexExceptionType> list = new ArrayList<IndexExceptionType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, IndexExceptionType> lookup = new HashMap<Integer, IndexExceptionType>();

		static {
			for (IndexExceptionType s : EnumSet.allOf(IndexExceptionType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		/**
		 * Instantiates a new document type.
		 *
		 * @param code the code
		 * @param value the value
		 * @param abrev the abrev
		 */
		private IndexExceptionType(Integer code, String value) {
			this.code = code;
			this.value = value;
		}
		
		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}
		
		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		
		/**
		 * Gets the description.
		 *
		 * @param locale the locale
		 * @return the description
		 */
		public String getDescription(Locale locale) {
			return null;
		}
		
		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the document type
		 */
		public static IndexExceptionType get(Integer code) {
			return lookup.get(code);
		}
}