package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


	// TODO: Auto-generated Javadoc
/**
	 * <ul>
	 * <li>
	 * Project Pradera.
	 * Copyright 2013.</li>
	 * </ul>
	 * 
	 * The Enum BillingStateType.
	 *
	 * @author PraderaTechnologies.
	 * @version 1.0 , 14/07/2013
	 */
	public enum BillingStateType {


		PRELIMINAR(Integer.valueOf(1643),"PRELIMINAR"), 
		
		DEFINITIVO(Integer.valueOf(1644),"DEFINITIVO"), 
		
		ERROR(Integer.valueOf(1654),"ERROR"); 
		
		
		/** The code. */
		private Integer code;
		
		/** The value. */
		private String value;
		
		/** The Constant list. */
		public static final List<BillingStateType> list = new ArrayList<BillingStateType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, BillingStateType> lookup = new HashMap<Integer, BillingStateType>();

		static {
			for (BillingStateType s : EnumSet.allOf(BillingStateType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		/**
		 * Instantiates a new document type.
		 *
		 * @param code the code
		 * @param value the value
		 * @param abrev the abrev
		 */
		private BillingStateType(Integer code, String value) {
			this.code = code;
			this.value = value;
		}
		
		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}
		
		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		
		/**
		 * Gets the description.
		 *
		 * @param locale the locale
		 * @return the description
		 */
		public String getDescription(Locale locale) {
			return null;
		}
		
		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the document type
		 */
		public static BillingStateType get(Integer code) {
			return lookup.get(code);
		}
}