package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum RateStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public enum RateStateType {
	
	/** The registered. */
	  
	ACTIVED(Integer.valueOf(1464),"ACTIVO"),
	
	/** The blocked. */
	EXPIRED(Integer.valueOf(1465),"CADUCADO"),
	
	/** The annulled. */
	CANCELED(Integer.valueOf(1466),"CANCELADO"),
	
	/** The unified. */
	REGISTERED(Integer.valueOf(1552),"REGISTRADO"),
	
	LOCKED(Integer.valueOf(1553),"BLOQUEADO"),
	
	UNLOCKED(Integer.valueOf(1),"DESBLOQUEAR"),
	
	/**Is not State, only Temporal */
	PENDING(Integer.valueOf(0), "PENDIENTE");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new participant state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private RateStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<RateStateType> list = new ArrayList<RateStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RateStateType> lookup = new HashMap<Integer, RateStateType>();
	static {
		for (RateStateType s : EnumSet.allOf(RateStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the participant state type
	 */
	public static RateStateType get(Integer code) {
		return lookup.get(code);
	}

}
