package com.pradera.model.billing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the BILLING_CALCULATION_DETAIL database table.
 * 
 */
@Entity
@Table(name="BILLING_CALCULATION_DETAIL")
public class BillingCalculationDetail implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BILLING_CALCULATION_DETAILPK_GENERATOR", sequenceName="SQ_ID_BILLING_CALC_DETAIL_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILLING_CALCULATION_DETAILPK_GENERATOR")
	@Column(name="ID_BILLING_CALC_DETAIL_PK")
	private Long idBillingCalcDetailPk;

	@ManyToOne
	@JoinColumn(name="ID_BILLING_SERVICE_FK")
	private BillingService billingService;

	//bi-directional many-to-one association to BillingCalculation
	@ManyToOne
	@JoinColumn(name="ID_BILLING_CALCULATION_FK")
	private BillingCalculation billingCalculation;
		
	@Column(name="GROSS_AMOUNT", precision=30, scale=2)
	private BigDecimal grossAmount;
	
	@Column(name="COLLECTION_AMOUNT", precision=30, scale=2)
	private BigDecimal collectionAmount;
		
	@Column(name="TAX_APPLIED", precision=30, scale=2)
	private BigDecimal taxApplied;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	public Long getIdBillingCalcDetailPk() {
		return idBillingCalcDetailPk;
	}

	public void setIdBillingCalcDetailPk(Long idBillingCalcDetailPk) {
		this.idBillingCalcDetailPk = idBillingCalcDetailPk;
	}

	public BillingService getBillingService() {
		return billingService;
	}

	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}

	public BillingCalculation getBillingCalculation() {
		return billingCalculation;
	}

	public void setBillingCalculation(BillingCalculation billingCalculation) {
		this.billingCalculation = billingCalculation;
	}

	public BigDecimal getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public BigDecimal getCollectionAmount() {
		return collectionAmount;
	}

	public void setCollectionAmount(BigDecimal collectionAmount) {
		this.collectionAmount = collectionAmount;
	}

	public BigDecimal getTaxApplied() {
		return taxApplied;
	}

	public void setTaxApplied(BigDecimal taxApplied) {
		this.taxApplied = taxApplied;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}