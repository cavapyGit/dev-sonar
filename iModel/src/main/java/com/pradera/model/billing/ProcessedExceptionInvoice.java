package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the EXCEPTION_INVOICE_GENERATIONS database table.
 * 
 */
@Entity
@Table(name="PROCESSED_EXCEPTION_INVOICE")
public class ProcessedExceptionInvoice implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PROCESSED_EXCEPTION_INVOICE_GEN", sequenceName="SQ_ID_PROCESSED_EXCEPTION_PK", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROCESSED_EXCEPTION_INVOICE_GEN")
	@Column(name="ID_PROCESSED_EXCEPTION_PK", unique=true, nullable=false, precision=10)
	private Long idProcessedExceptionPk;
	
	@Column(name="IND_INTEGRATED_BILL")
	private Integer indIntegratedBill;
	
	@Column(name="EXCEPTION_STATE", precision=10)
	private Integer exceptionState;
	
	@OneToMany(mappedBy="processedExceptionInvoice",fetch=FetchType.LAZY)
	private List<ExceptionInvoiceGenerations> exceptionInvoiceGenerations;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;


	
	

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("exceptionInvoiceGenerations", exceptionInvoiceGenerations);
        return detailsMap;
	}



	/**
	 * @return the idProcessedExceptionPk
	 */
	public Long getIdProcessedExceptionPk() {
		return idProcessedExceptionPk;
	}



	/**
	 * @param idProcessedExceptionPk the idProcessedExceptionPk to set
	 */
	public void setIdProcessedExceptionPk(Long idProcessedExceptionPk) {
		this.idProcessedExceptionPk = idProcessedExceptionPk;
	}



	/**
	 * @return the indIntegratedBill
	 */
	public Integer getIndIntegratedBill() {
		return indIntegratedBill;
	}



	/**
	 * @param indIntegratedBill the indIntegratedBill to set
	 */
	public void setIndIntegratedBill(Integer indIntegratedBill) {
		this.indIntegratedBill = indIntegratedBill;
	}



	/**
	 * @return the exceptionState
	 */
	public Integer getExceptionState() {
		return exceptionState;
	}



	/**
	 * @param exceptionState the exceptionState to set
	 */
	public void setExceptionState(Integer exceptionState) {
		this.exceptionState = exceptionState;
	}



	/**
	 * @return the exceptionInvoiceGenerations
	 */
	public List<ExceptionInvoiceGenerations> getExceptionInvoiceGenerations() {
		return exceptionInvoiceGenerations;
	}



	/**
	 * @param exceptionInvoiceGenerations the exceptionInvoiceGenerations to set
	 */
	public void setExceptionInvoiceGenerations(
			List<ExceptionInvoiceGenerations> exceptionInvoiceGenerations) {
		this.exceptionInvoiceGenerations = exceptionInvoiceGenerations;
	}



	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	
}