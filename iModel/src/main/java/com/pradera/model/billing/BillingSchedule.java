package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the EXCEPTION_INVOICE_GENERATIONS database table.
 * 
 */
@Entity
@Table(name="BILLING_SCHEDULE")
public class BillingSchedule implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BILLING_SCHEDULE_GEN", sequenceName="SQ_ID_BILLING_SCHEDULE_PK", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILLING_SCHEDULE_GEN")
	@Column(name="ID_BILLING_SCHEDULE_PK", unique=true, nullable=false, precision=10)
	private Long idBillingSchedulePk;

	@Column(name="COLLECTION_PERIOD", precision=10)
	private Integer collectionPeriod;
	
	@Column(name="DAY_OF_WEEK")
	private String dayOfWeek;
	
	@Column(name="DAY_OF_MONTH")
	private String dayOfMonth;
	
	@Column(name="MONTH")
	private String month;
	
	@Column(name="YEAR")
	private String year;
	
	@Column(name = "IND_BCB_EXCEPTION")
	private Integer indBcbException;
	
	@Column(name="IND_END_MONTH", precision=10)
	private Integer indEndOfMonth;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;


	//bi-directional many-to-one association to CollectionRecord
	@OneToMany(mappedBy="billingSchedule", fetch = FetchType.LAZY)
	private List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations;

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("listExceptionInvoiceGenerations", listExceptionInvoiceGenerations);
        return detailsMap;
	}



	/**
	 * @return the idBillingSchedulePk
	 */
	public Long getIdBillingSchedulePk() {
		return idBillingSchedulePk;
	}



	/**
	 * @param idBillingSchedulePk the idBillingSchedulePk to set
	 */
	public void setIdBillingSchedulePk(Long idBillingSchedulePk) {
		this.idBillingSchedulePk = idBillingSchedulePk;
	}



	/**
	 * @return the collectionPeriod
	 */
	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}



	/**
	 * @param collectionPeriod the collectionPeriod to set
	 */
	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}



	/**
	 * @return the dayOfWeek
	 */
	public String getDayOfWeek() {
		return dayOfWeek;
	}



	/**
	 * @param dayOfWeek the dayOfWeek to set
	 */
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}



	/**
	 * @return the dayOfMonth
	 */
	public String getDayOfMonth() {
		return dayOfMonth;
	}



	/**
	 * @param dayOfMonth the dayOfMonth to set
	 */
	public void setDayOfMonth(String dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}



	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}



	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}



	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}



	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}



	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/**
	 * @return the listExceptionInvoiceGenerations
	 */
	public List<ExceptionInvoiceGenerations> getListExceptionInvoiceGenerations() {
		return listExceptionInvoiceGenerations;
	}



	/**
	 * @param listExceptionInvoiceGenerations the listExceptionInvoiceGenerations to set
	 */
	public void setListExceptionInvoiceGenerations(
			List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations) {
		this.listExceptionInvoiceGenerations = listExceptionInvoiceGenerations;
	}



	/**
	 * @return the indEndOfMonth
	 */
	public Integer getIndEndOfMonth() {
		return indEndOfMonth;
	}



	/**
	 * @param indEndOfMonth the indEndOfMonth to set
	 */
	public void setIndEndOfMonth(Integer indEndOfMonth) {
		this.indEndOfMonth = indEndOfMonth;
	}



	public Integer getIndBcbException() {
		return indBcbException;
	}



	public void setIndBcbException(Integer indBcbException) {
		this.indBcbException = indBcbException;
	}


	
	
	
}