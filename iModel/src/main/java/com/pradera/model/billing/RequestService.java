package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;

@Entity
@Table(name="REQUEST_SERVICE")
public class RequestService implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="SERVICE_REQ_GENERATOR", sequenceName="SQ_ID_REQ_SERVICE_PK" ,initialValue=1,allocationSize=1)
	@GeneratedValue(generator="SERVICE_REQ_GENERATOR",strategy=GenerationType.SEQUENCE)
	@Column(name="ID_REQ_SERVICE_PK", unique=true, nullable=false, precision=10)
	private Long idRequestServicePk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCESSED_SERVICE_FK",referencedColumnName="ID_PROCESSED_SERVICE_PK")
	private ProcessedService processedService;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BILLING_SERVICE_FK",referencedColumnName="ID_BILLING_SERVICE_PK")
	private BillingService billingService;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK",referencedColumnName="ID_HOLDER_PK")
	private Holder holder;
		
	@Column(name="OBSERVATION_TEXT",length=1500)
	private String obsText;
	
	@Column(name="CLIENT_NAME",length=150)
	private String clientName;

	@Column(name="CLIENT_NIT",length=21)
	private String clientNit;
	
	@Column(name="SERVICE_NAME",length=300)
	private String servName;
	
	@Column(name="SERVICE_CODE",length=5)
	private String servCode;
	
	@Column(name="PRICE_TRANSPORT")
	private Integer priceTransport;
	
	@Temporal(TemporalType.DATE)
	@Column(name="OBSERVATION_DATE")
	private Date obsDate;
	
	@Column(name="OBSERVATION_STATE")
	private Integer obsState;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Transient
	private String descriptionStateRequest;
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public BillingService getBillingService() {
		return billingService;
	}

	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}

	public Long getIdRequestServicePk() {
		return idRequestServicePk;
	}

	public void setIdRequestServicePk(Long idRequestServicePk) {
		this.idRequestServicePk = idRequestServicePk;
	}

	public ProcessedService getProcessedService() {
		return processedService;
	}

	public void setProcessedService(ProcessedService processedService) {
		this.processedService = processedService;
	}

	public String getObsText() {
		return obsText;
	}

	public void setObsText(String obsText) {
		this.obsText = obsText;
	}

	public Date getObsDate() {
		return obsDate;
	}

	public void setObsDate(Date obsDate) {
		this.obsDate = obsDate;
	}

	public Integer getObsState() {
		return obsState;
	}

	public void setObsState(Integer obsState) {
		this.obsState = obsState;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}	
	
	public String getServName() {
		return servName;
	}

	public void setServName(String servName) {
		this.servName = servName;
	}
	
	public String getServCode() {
		return servCode;
	}

	public void setServCode(String servCode) {
		this.servCode = servCode;
	}
	
	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientNit() {
		return clientNit;
	}

	public void setClientNit(String clientNit) {
		this.clientNit = clientNit;
	}

	public Integer getPriceTransport() {
		return priceTransport;
	}

	public void setPriceTransport(Integer priceTransport) {
		this.priceTransport = priceTransport;
	}

	public String getDescriptionStateRequest() {
		if (obsState != null) {
			if (obsState.equals(1)) {
				descriptionStateRequest = "REGISTRADO";
			} else if (obsState.equals(2)) {
				descriptionStateRequest = "CONFIRMADO";
			} else if (obsState.equals(3)) {
				descriptionStateRequest = "RECHAZADO";
			} else {
				descriptionStateRequest = "";
			}
		}
		return descriptionStateRequest;
	}

	public void setDescriptionStateRequest(String descriptionStateRequest) {
		this.descriptionStateRequest = descriptionStateRequest;
	}	
		

}
