package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the RATE_INCLUSION database table.
 * 
 */
@Entity
@Table(name="RATE_INCLUSION")
public class RateInclusion implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
		
	@Id
	@SequenceGenerator(name="RATEINCLUSIONPK_GENERATOR", sequenceName="SQ_ID_RATE_INCLUSION_PK" , initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RATEINCLUSIONPK_GENERATOR")
	@Column(name="ID_RATE_INCLUSION_PK", unique=true, nullable=false, precision=10)
	private Long idRateInclusionPk;
	
	@Column(name="EXCEPTION_RULE")
	private Integer exceptionRule; 
	
	@Column(name="INCLUSION_CODE")
	private Integer inclusionCode;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SERVICE_RATE_FK")
	private ServiceRate serviceRate;
	
	@Column(name="SECURITY_CLASS")
	private Integer securityClass;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="INCLUSION_DATE")
	private Date inclusionDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="COMMENTS")
	private String comments;

	
	@Column(name="INCLUSION_STATE")
	private Integer inclusionState;
	
	@Column(name="IND_SELECT_ALL")
	private Integer indSelectAll;

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUANCE_CODE_FK")
	private Issuance issuance;
	
	
	

	public RateInclusion() {
	}
	
	

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}



	/**
	 * @return the idRateInclusionPk
	 */
	public Long getIdRateInclusionPk() {
		return idRateInclusionPk;
	}



	/**
	 * @param idRateInclusionPk the idRateInclusionPk to set
	 */
	public void setIdRateInclusionPk(Long idRateInclusionPk) {
		this.idRateInclusionPk = idRateInclusionPk;
	}



	/**
	 * @return the exceptionRule
	 */
	public Integer getExceptionRule() {
		return exceptionRule;
	}



	/**
	 * @param exceptionRule the exceptionRule to set
	 */
	public void setExceptionRule(Integer exceptionRule) {
		this.exceptionRule = exceptionRule;
	}



	/**
	 * @return the inclusionCode
	 */
	public Integer getInclusionCode() {
		return inclusionCode;
	}



	/**
	 * @param inclusionCode the inclusionCode to set
	 */
	public void setInclusionCode(Integer inclusionCode) {
		this.inclusionCode = inclusionCode;
	}



	/**
	 * @return the serviceRate
	 */
	public ServiceRate getServiceRate() {
		return serviceRate;
	}



	/**
	 * @param serviceRate the serviceRate to set
	 */
	public void setServiceRate(ServiceRate serviceRate) {
		this.serviceRate = serviceRate;
	}



	/**
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}



	/**
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}



	/**
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}



	/**
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}



	/**
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}



	/**
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}



	/**
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}



	/**
	 * @param securities the securities to set
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}



	/**
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}



	/**
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}



	/**
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}



	/**
	 * @param participant the participant to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}



	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * @return the inclusionDate
	 */
	public Date getInclusionDate() {
		return inclusionDate;
	}



	/**
	 * @param inclusionDate the inclusionDate to set
	 */
	public void setInclusionDate(Date inclusionDate) {
		this.inclusionDate = inclusionDate;
	}



	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}



	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}



	/**
	 * @return the inclusionState
	 */
	public Integer getInclusionState() {
		return inclusionState;
	}



	/**
	 * @param inclusionState the inclusionState to set
	 */
	public void setInclusionState(Integer inclusionState) {
		this.inclusionState = inclusionState;
	}



	/**
	 * @return the indSelectAll
	 */
	public Integer getIndSelectAll() {
		return indSelectAll;
	}



	/**
	 * @param indSelectAll the indSelectAll to set
	 */
	public void setIndSelectAll(Integer indSelectAll) {
		this.indSelectAll = indSelectAll;
	}



	/**
	 * @return the issuance
	 */
	public Issuance getIssuance() {
		return issuance;
	}



	/**
	 * @param issuance the issuance to set
	 */
	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}

	

}