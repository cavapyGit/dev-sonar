package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Enum DocumentType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/11/2013
 */
public enum BillingServiceFilter {
	ENTITY_COLLECTION_PK(Integer.valueOf(171),"ENTIDAD DE COBRO"),
	SERVICE_STATUS_PK(Integer.valueOf(172),"ESTADO DE SERVICIO"),
	COLLECTION_BASE_PK(Integer.valueOf(173),"BASE DE COBRO"),
	CALCULATION_PERIOD_PK(Integer.valueOf(174),"PERIODO DE CALCULO"),
	COLLECTION_PERIOD_PK(Integer.valueOf(175),"PERIODO DE COBRO"),
	SERVICE_TYPE_PK(Integer.valueOf(383),"TIPO DE SERVICIO"),
	SERVICE_CLIENT_PK(Integer.valueOf(403),"CLIENTE DE SERVICIO"),
	EXCEPTION_RATES_STATUS_PK(Integer.valueOf(455),"ESTADO DE LA EXCEPCION DE TARIFAS"),
	EXCEPTION_INVOICE_STATUS_PK(Integer.valueOf(540),"ESTADO DE EXCEPCION DE FACTURA"),
	XML_STATUS(Integer.valueOf(552),"ESTADO DE LOS ARCHIVOS XML"),
	SETUP_BILLING(Integer.valueOf(553),"CONFIGURACION DE MODULO TARIFAS Y SERVICIOS"),
	CODIGO_CONTABLE_INGRESO(Integer.valueOf(576),"CODIGO CONTABLE DE INGRESO"),
	SOURCE_STATUS(Integer.valueOf(600),"ESTADO DE LA FUENTE DE BASE COBRO");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<BillingServiceFilter> list = new ArrayList<BillingServiceFilter>();
	/** The Constant lookup. */
	public static final Map<Integer, BillingServiceFilter> lookup = new HashMap<Integer, BillingServiceFilter>();
	static {
		for (BillingServiceFilter s : EnumSet.allOf(BillingServiceFilter.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/**
	 * Instantiates a new document type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param abrev the abrev
	 */
	private BillingServiceFilter(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description 
	 */
	public String getDescription(Locale locale) {
		return null;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the document type
	 */
	public static BillingServiceFilter get(Integer code) {
		return lookup.get(code);
	}

}
