package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the BILLING_CORRELATIVE database table.
 * 
 */
@Entity
@Table(name="BILLING_CORRELATIVE")
public class BillingCorrelative implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BILLING_CORRELATIVEPK_GENERATOR", sequenceName="SQ_ID_BILLING_CORRELATIVE_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILLING_CORRELATIVEPK_GENERATOR")
	@Column(name="ID_BILLING_CORRELATIVE_PK")
	private Long idBillingCorrelativePk;

	@Column(name="DESCRIPTION",length=100)
	private String description;

	@Column(name="CORRELATIVE_CD",length=20)
	private String correlativeCD;
	
	@Column(name="CORRELATIVE_NUMBER",precision=10)
	private Long correlativeNumber;
	
	@Column(name="BILLING_SERIE",length=20)
	private String billingSerie;

	@Column(name="BILLING_TCF",length=10)
	private String billingTCF;
	
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	

	public Long getIdBillingCorrelativePk() {
		return idBillingCorrelativePk;
	}

	public void setIdBillingCorrelativePk(Long idBillingCorrelativePk) {
		this.idBillingCorrelativePk = idBillingCorrelativePk;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCorrelativeCD() {
		return correlativeCD;
	}

	public void setCorrelativeCD(String correlativeCD) {
		this.correlativeCD = correlativeCD;
	}

	

	public Long getCorrelativeNumber() {
		return correlativeNumber;
	}

	public void setCorrelativeNumber(Long correlativeNumber) {
		this.correlativeNumber = correlativeNumber;
	}

	public String getBillingSerie() {
		return billingSerie;
	}

	public void setBillingSerie(String billingSerie) {
		this.billingSerie = billingSerie;
	}

	public String getBillingTCF() {
		return billingTCF;
	}

	public void setBillingTCF(String billingTCF) {
		this.billingTCF = billingTCF;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}
}
