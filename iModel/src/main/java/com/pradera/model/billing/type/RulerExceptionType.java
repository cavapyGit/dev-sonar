package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum BaseCollectionType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/11/2013
 */
public enum RulerExceptionType {

	SECURITY_EXCEPTION_RATE(Integer.valueOf(1548),"VALOR"),
	SECURITY_CLASS_EXCEPTION_RATE(Integer.valueOf(1547),"CLASE VALOR"),
	PARTICIPANT_EXCEPTION_RATE(Integer.valueOf(1549),"PARTICIPANTE"),
	ISSUER_EXCEPTION_RATE(Integer.valueOf(1550),"EMISOR"),
	HOLDER_EXCEPTION_RATE(Integer.valueOf(1551),"TITULAR");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new BaseCollection state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private RulerExceptionType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<RulerExceptionType> list = new ArrayList<RulerExceptionType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RulerExceptionType> lookup = new HashMap<Integer, RulerExceptionType>();
	static {
		for (RulerExceptionType s : EnumSet.allOf(RulerExceptionType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the BaseCollection  state type
	 */
	public static RulerExceptionType get(Integer code) {
		return lookup.get(code);
	}

}
