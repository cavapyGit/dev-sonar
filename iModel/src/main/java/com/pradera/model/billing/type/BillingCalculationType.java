package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum BillingCalculationType {


	/** End The Constant Processed Service */
	PRELIMINAR(Integer.valueOf(1643),"PRELIMINAR"), 
	
	DEFINITIVO(Integer.valueOf(1644),"DEFINITIVO"), 
	
	ERROR(Integer.valueOf(1654),"ERROR");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<BillingCalculationType> list = new ArrayList<BillingCalculationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BillingCalculationType> lookup = new HashMap<Integer, BillingCalculationType>();

	static {
		for (BillingCalculationType s : EnumSet.allOf(BillingCalculationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new document type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param abrev the abrev
	 */
	private BillingCalculationType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return null;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the document type
	 */
	public static BillingCalculationType get(Integer code) {
		return lookup.get(code);
	}
}
