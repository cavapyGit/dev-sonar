package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the COLLECTION_RECORD database table.
 * 
 */
@Entity
@Table(name="BILLING_ECONOMIC_ACTIVITY")
public class BillingEconomicActivity implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BILLING_ECONOMIC_ACTIVITY_GENERATOR", sequenceName="SQ_ID_BILLING_ECONOMIC_ACT_PK", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILLING_ECONOMIC_ACTIVITY_GENERATOR")
	@Column(name="ID_BILLING_ECONOMIC_ACT_PK", unique=true, nullable=false, precision=10)
	private Long idBillingEconomicActivityPk;

	@Column(name="ECONOMIC_ACTIVITY", precision=10)
	private Integer economicActivity;

	//bi-directional many-to-one association to ProcessedService
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BILLING_SERVICE_FK",referencedColumnName="ID_BILLING_SERVICE_PK")
	private BillingService billingService;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	
	public BillingEconomicActivity() {
	}

	
	

	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        
        return detailsMap;
	}




	/**
	 * @return the idBillingEconomicActivityPk
	 */
	public Long getIdBillingEconomicActivityPk() {
		return idBillingEconomicActivityPk;
	}




	/**
	 * @param idBillingEconomicActivityPk the idBillingEconomicActivityPk to set
	 */
	public void setIdBillingEconomicActivityPk(Long idBillingEconomicActivityPk) {
		this.idBillingEconomicActivityPk = idBillingEconomicActivityPk;
	}




	/**
	 * @return the economicActivity
	 */
	public Integer getEconomicActivity() {
		return economicActivity;
	}




	/**
	 * @param economicActivity the economicActivity to set
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}




	/**
	 * @return the billingService
	 */
	public BillingService getBillingService() {
		return billingService;
	}




	/**
	 * @param billingService the billingService to set
	 */
	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}




	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}




	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}




	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}




	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}




	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}




	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}




	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}




	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	
}