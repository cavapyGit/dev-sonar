package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuer;


/**
 * The persistent class for the BILLING_ENTITY_EXCEPTION database table.
 * 
 */
@Entity
@Table(name="BILLING_ENTITY_EXCEPTION")
public class BillingEntityException implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BILLING_ENTITY_EXCEP_GEN", sequenceName="SQ_ID_BILLING_ENTITY_EXCEP_PK", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILLING_ENTITY_EXCEP_GEN")
	@Column(name="ID_BILLING_ENTITY_EXCEPTION_PK", unique=true, nullable=false, precision=10)
	private Long idBillingEntityExceptionPk;

	@Column(name="ENTITY_COLLECTION", precision=10)
	private Integer entityCollectionId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to CollectionRecord
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_EXCEPTION_INVOICE_FK")
	private ExceptionInvoiceGenerations exceptionInvoiceGenerations;
	
	@Column(name="OTHER_ENTITY_DOCUMENT", length=25)
	private String otherEntityDocument;
	
	@Column(name="OTHER_ENTITY_DESCRIPTION", length=100)
	private String otherEntityDescription;	
	
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		
        return null;
	}


	/**
	 * @return the idBillingEntityExceptionPk
	 */
	public Long getIdBillingEntityExceptionPk() {
		return idBillingEntityExceptionPk;
	}


	/**
	 * @param idBillingEntityExceptionPk the idBillingEntityExceptionPk to set
	 */
	public void setIdBillingEntityExceptionPk(Long idBillingEntityExceptionPk) {
		this.idBillingEntityExceptionPk = idBillingEntityExceptionPk;
	}


	/**
	 * @return the entityCollectionId
	 */
	public Integer getEntityCollectionId() {
		return entityCollectionId;
	}


	/**
	 * @param entityCollectionId the entityCollectionId to set
	 */
	public void setEntityCollectionId(Integer entityCollectionId) {
		this.entityCollectionId = entityCollectionId;
	}


	/**
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}


	/**
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}


	/**
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}


	/**
	 * @param participant the participant to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}


	/**
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}


	/**
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}


	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}


	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}


	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	/**
	 * @return the exceptionInvoiceGenerations
	 */
	public ExceptionInvoiceGenerations getExceptionInvoiceGenerations() {
		return exceptionInvoiceGenerations;
	}


	/**
	 * @param exceptionInvoiceGenerations the exceptionInvoiceGenerations to set
	 */
	public void setExceptionInvoiceGenerations(
			ExceptionInvoiceGenerations exceptionInvoiceGenerations) {
		this.exceptionInvoiceGenerations = exceptionInvoiceGenerations;
	}


	/**
	 * @return the otherEntityDescription
	 */
	public String getOtherEntityDescription() {
		return otherEntityDescription;
	}


	/**
	 * @param otherEntityDescription the otherEntityDescription to set
	 */
	public void setOtherEntityDescription(String otherEntityDescription) {
		this.otherEntityDescription = otherEntityDescription;
	}


	/**
	 * @return the otherEntityDocument
	 */
	public String getOtherEntityDocument() {
		return otherEntityDocument;
	}


	/**
	 * @param otherEntityDocument the otherEntityDocument to set
	 */
	public void setOtherEntityDocument(String otherEntityDocument) {
		this.otherEntityDocument = otherEntityDocument;
	}


	
}