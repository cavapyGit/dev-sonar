package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2014.</li>
 * </ul>
 * 
 * The Enum InvoiceExceptionStatus.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/11/2014
 */
public enum InvoiceExceptionStatus {
	

	REGISTER(Integer.valueOf(2075), "REGISTRADO"),
	DELETED(Integer.valueOf(2076), "ELIMINADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	/** The Constant list. */
	public static final List<InvoiceExceptionStatus> list = new ArrayList<InvoiceExceptionStatus>();
	/** The Constant lookup. */
	public static final Map<Integer, InvoiceExceptionStatus> lookup = new HashMap<Integer, InvoiceExceptionStatus>();
	
	
	static {
		for (InvoiceExceptionStatus s : EnumSet.allOf(InvoiceExceptionStatus.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/**
	 * Instantiates a new document type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InvoiceExceptionStatus(Integer code, String value) {
		this.code = code;
		this.value = value;	
	
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}	
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */

	public String getDescription(Locale locale) {
		return null;
	}
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the document type
	 */
	public static InvoiceExceptionStatus get(Integer code) {
		return lookup.get(code);
	}
}
