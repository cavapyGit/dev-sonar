package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum BillingServiceSourceStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/11/2013
 */
public enum BillingServiceSourceStateType {
	
	REGISTERED(Integer.valueOf(2291),"REGISTRADO"),
	
	DELETED(Integer.valueOf(2292),"ELIMINADO"),
	
	MODIFIED(Integer.valueOf(2293),"MODIFICADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	/** The Constant list. */
	public static final List<RateExceptionStatus> list = new ArrayList<RateExceptionStatus>();
	/** The Constant lookup. */
	public static final Map<Integer, RateExceptionStatus> lookup = new HashMap<Integer, RateExceptionStatus>();
	
	
	static {
		for (RateExceptionStatus s : EnumSet.allOf(RateExceptionStatus.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	/**
	 * Instantiates a new document type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BillingServiceSourceStateType(Integer code, String value) {
		this.code = code;
		this.value = value;	
	
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}	
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */

	public String getDescription(Locale locale) {
		return null;
	}
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the document type
	 */
	public static RateExceptionStatus get(Integer code) {
		return lookup.get(code);
	}

}
