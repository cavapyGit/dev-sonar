package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


/**
 * The persistent class for the RATE_EXCLUSION database table.
 * 
 */
@Entity
@Table(name="RATE_EXCLUSION")
public class RateExclusion implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="RATEEXCLUSIONPK_GENERATOR", sequenceName="SQ_ID_RATE_EXCLUSION_PK" , initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RATEEXCLUSIONPK_GENERATOR")
	@Column(name="ID_RATE_EXCLUSION_PK", unique=true, nullable=false, precision=10)
	private Long idRateExclusionPk;
	
	@Column(name="EXCEPTION_RULE")
	private Integer exceptionRule; 
		
	@Column(name="EXCLUSION_CODE")
	private Integer exclusionCode;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_ACCOUNT_FK")
	private HolderAccount holderAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SECURITY_CODE_FK")
	private Security securities;
	
	@Column(name="SECURITY_CLASS")
	private Integer securityClass;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SERVICE_RATE_FK")
	private ServiceRate serviceRate;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="EXCLUSION_DATE")
	private Date exclusionDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
 	
	@Column(name="EXCLUSION_STATE")
	private Integer exclusionState;
	
	@Column(name="IND_SELECT_ALL")
	private Integer indSelectAll;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUANCE_CODE_FK")
	private Issuance issuance;
	
	
	public Integer getExclusionState() {
		return exclusionState;
	}


	public void setExclusionState(Integer exclusionState) {
		this.exclusionState = exclusionState;
	}


	public Date getExclusionDate() {
		return exclusionDate;
	}


	public void setExclusionDate(Date exclusionDate) {
		this.exclusionDate = exclusionDate;
	}


	public RateExclusion() {
	}
		
	

	public Integer getExceptionRule() {
		return exceptionRule;
	}



	public void setExceptionRule(Integer exceptionRule) {
		this.exceptionRule = exceptionRule;
	}



	public void setIdRateExclusionPk(Long idRateExclusionPk) {
		this.idRateExclusionPk = idRateExclusionPk;
	}



	public long getIdRateExclusionPk() {
		return this.idRateExclusionPk;
	}

	

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public ServiceRate getServiceRate() {
		return serviceRate;
	}

	public void setServiceRate(ServiceRate serviceRate) {
		this.serviceRate = serviceRate;
	}

	

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Security getSecurities() {
		return securities;
	}

	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	
	
	public Integer getExclusionCode() {
		return exclusionCode;
	}


	public void setExclusionCode(Integer exclusionCode) {
		this.exclusionCode = exclusionCode;
	}


	public Integer getIndSelectAll() {
		return indSelectAll;
	}


	public void setIndSelectAll(Integer indSelectAll) {
		this.indSelectAll = indSelectAll;
	}


	public Issuance getIssuance() {
		return issuance;
	}


	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}


	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	
}