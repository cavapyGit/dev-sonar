package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum ProcessedServiceType {


	PENDIENTE(Integer.valueOf(-1),"PENDIENTE"), 
	
	REGISTRADO(Integer.valueOf(1514),"REGISTRADO"), 
	
	NO_PROCESADO(Integer.valueOf(1515),"NO PROCESADO"), 
	
	EJECUTANDO(Integer.valueOf(1516),"EJECUTANDO"),
	
	PROCESADO(Integer.valueOf(1517),"PROCESADO"),
	
	ERROR(Integer.valueOf(1518),"ERROR"),
	
	FACTURADO(Integer.valueOf(1519),"FACTURADO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<ProcessedServiceType> list = new ArrayList<ProcessedServiceType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ProcessedServiceType> lookup = new HashMap<Integer, ProcessedServiceType>();

	static {
		for (ProcessedServiceType s : EnumSet.allOf(ProcessedServiceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new document type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param abrev the abrev
	 */
	private ProcessedServiceType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return null;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the document type
	 */
	public static ProcessedServiceType get(Integer code) {
		return lookup.get(code);
	}
}
