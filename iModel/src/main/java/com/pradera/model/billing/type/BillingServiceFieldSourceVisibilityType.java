package com.pradera.model.billing.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project EDV.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Enum AccountingFieldSourceVisibilityType.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/11/2014
*/
public enum  BillingServiceFieldSourceVisibilityType {
	
	TRUE(Integer.valueOf(1),"TRUE"),
	
	FALSE(Integer.valueOf(0),"FALSE");
	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;	
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new AccountingFieldSourceVisibilityType state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BillingServiceFieldSourceVisibilityType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<BillingServiceFieldSourceVisibilityType> list = new ArrayList<BillingServiceFieldSourceVisibilityType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BillingServiceFieldSourceVisibilityType> lookup      = new HashMap<Integer, BillingServiceFieldSourceVisibilityType>();
	public static final Map<String, BillingServiceFieldSourceVisibilityType>  lookupValue = new HashMap<String, BillingServiceFieldSourceVisibilityType>();
	
	static {
		for (BillingServiceFieldSourceVisibilityType s : EnumSet.allOf(BillingServiceFieldSourceVisibilityType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
			lookupValue.put(s.getValue(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the AccountingFieldSourceVisibilityType state type
	 */
	public static BillingServiceFieldSourceVisibilityType get(Integer code) {
		return lookup.get(code);
	}
	
	public static BillingServiceFieldSourceVisibilityType get(String value) {
		return lookupValue.get(value);
	}
	
}
