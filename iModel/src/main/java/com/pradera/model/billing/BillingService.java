package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the BILLING_SERVICE database table.
 * 
 */
@Entity
@Table(name="BILLING_SERVICE")
public class BillingService implements Serializable,Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BILLINGSERVICEPK_GENERATOR", sequenceName="SQ_ID_BILLING_SERVICE_PK" , initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILLINGSERVICEPK_GENERATOR")
	@Column(name="ID_BILLING_SERVICE_PK", unique=true, nullable=false, precision=10)
	private Long idBillingServicePk;

	@Column(name="BASE_COLLECTION")
	private Integer baseCollection;

	@Column(name="BUSINESS_LINE")
	private String businessLine;
	
	@Column(name="FINANTIAL_CODE")
	private String finantialCode;
	
	@Column(name="CALCULATION_PERIOD")
	private Integer calculationPeriod;

	@Column(name="CLIENT_SERVICE_TYPE")
	private Integer clientServiceType;

	@Column(name="COLLECTION_PERIOD", precision=10)
	private Integer collectionPeriod;

	@Column(name="COLLECTION_TYPE", precision=10)
	private Integer collectionType;

	@Column(name="COMMENTS",length=3000)
	private String observation;

	@Column(name="DOCUMENT_SERVICE_TYPE", precision=10)
	private Integer documentServiceType;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_EFFECTIVE_DATE")
	private Date endEffectiveDate;

	@Column(name="ENTITY_COLLECTION", precision=10)
	private Integer entityCollection;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="INITIAL_EFFECTIVE_DATE")
	private Date initialEffectiveDate;

	@Column(name="SERVICE_CODE", length=5)
	private String serviceCode;

	@Column(name="SERVICE_NAME", nullable=false, length=50)
	private String serviceName;

	@Column(name="STATE_SERVICE", precision=10)
	private Integer serviceState;

	@Column(name="SERVICE_TYPE", precision=10)
	private Integer serviceType;

	@Column(name="TAX_APPLIED", precision=12, scale=8)
	private Integer taxApplied;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UNLOCK_DATE")
	private Date unlockDate;

	@Column(name="UNLOCK_USER", length=20)
	private String unlockUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LOCK_DATE")
	private Date lockDate;

	@Column(name="LOCK_USER", length=20)
	private String lockUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE", nullable=false)
	private Date registryDate;

	@Column(name="REGISTRY_USER", nullable=false, length=20)
	private String registryUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CANCEL_DATE")
	private Date cancelDate;

	@Column(name="CANCEL_USER", length=20)
	private String cancelUser;
	
	@Column(name="IND_INTEGRATED_BILL")
	private Integer inIntegratedBill;
	
	@Column(name="PORTFOLIO")
	private Integer portfolio;

	@Column(name="IND_SOURCE_BASE_COLLECTION")
	private Integer indSourceBaseCollection;
	
	@Column(name="REFERENCE_RATE", nullable=false)
	private String referenceRate;

	//bi-directional many-to-one association to BillingServiceSource
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BILLING_SERVICE_SOURCE_FK",referencedColumnName="ID_BILLING_SERVICE_SOURCE_PK")
	private BillingServiceSource billingServiceSource;

	//bi-directional many-to-one association to ProcessedService
	@OneToMany(mappedBy="billingService",fetch=FetchType.LAZY)
	private List<ProcessedService> processedServices;
	
	//bi-directional many-to-one association to ServiceRate
	@OneToMany(mappedBy="billingService",fetch=FetchType.LAZY)
	private List<ServiceRate> serviceRates;
	
	//bi-directional many-to-one association to ServiceRate
	@OneToMany(mappedBy="billingService",fetch=FetchType.LAZY)
	private List<BillingEconomicActivity> billingEconomicActivitys;
	
	//bi-directional many-to-one association to ExceptionInvoiceGenerations
	@OneToMany(mappedBy="billingService",fetch=FetchType.LAZY)
	private List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Column(name="CURRENCY_BILLING")
	private Integer currencyBilling;
	
	@Column(name="CURRENCY_CALCULATION")
	private Integer currencyCalculation;
	
	@Column(name="SOURCE_INFORMATION")
	private Integer sourceInformation;
	
	@Column(name="ACCOUNTING_CODE", length=50)
	private String accountingAccount;
	
	@Transient	
	private String descriptionCollectionEntity;
	@Transient
	private String descriptionBaseCollection;
	@Transient
	private String descriptionStateBillingService;
	@Transient 
	private String descriptionServiceType;
	@Transient 
	private String descriptionCollectionPeriod;
	@Transient 
	private String descriptionClientServiceType;
	@Transient 
	private String descriptionCalculationPeriod;

	
	public BillingService() {
	}

	public Long getIdBillingServicePk() {
		return this.idBillingServicePk;
	}
	
	

	/**
	 * @param idBillingServicePk
	 * @param serviceCode
	 * @param serviceName
	 */
	public BillingService(Long idBillingServicePk, String serviceCode,
			String serviceName) {
		this.idBillingServicePk = idBillingServicePk;
		this.serviceCode = serviceCode;
		this.serviceName = serviceName;
	}

	/**
	 * @param serviceCode
	 */
	public BillingService(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * @param idBillingServicePk
	 */
	public BillingService(Long idBillingServicePk) {
		this.idBillingServicePk = idBillingServicePk;
	}

	//BillingServiceMgmtServiceBean getListBillingServiceByParameters
	
	/**
	 * Full Constructor
	 * 
	 * @param idBillingServicePk
	 * @param baseCollection
	 * @param businessLine
	 * @param finantialCode
	 * @param calculationPeriod
	 * @param clientServiceType
	 * @param collectionPeriod
	 * @param collectionType
	 * @param observation
	 * @param endEffectiveDate
	 * @param entityCollection
	 * @param initialEffectiveDate
	 * @param serviceCode
	 * @param serviceName
	 * @param serviceState
	 * @param serviceType
	 * @param taxApplied
	 * @param registryDate
	 * @param inIntegratedBill
	 * @param indSourceBaseCollection
	 * @param billingServiceSource
	 * @param currencyBilling
	 * @param descriptionCollectionEntity
	 * @param descriptionBaseCollection
	 * @param descriptionStateBillingService
	 * @param descriptionServiceType
	 * @param descriptionCollectionPeriod
	 * @param descriptionClientServiceType
	 * @param descriptionCalculationPeriod
	 * @param descriptionAccountingAccount
	 * @param sourceInformation
	 * @param currencyCalculation
	 * @param accountingAccount
	 * @param referenceRate
	 */
	public BillingService(Long idBillingServicePk, Integer baseCollection,
			String businessLine, String finantialCode, Integer calculationPeriod,
			Integer clientServiceType, Integer collectionPeriod,
			Integer collectionType, String observation, Date endEffectiveDate,
			Integer entityCollection, Date initialEffectiveDate,
			String serviceCode, String serviceName, Integer serviceState,
			Integer serviceType, Integer taxApplied, Date registryDate,
			Integer inIntegratedBill, Integer indSourceBaseCollection,
			BillingServiceSource billingServiceSource, Integer currencyBilling,
			String descriptionCollectionEntity, String descriptionBaseCollection,
			String descriptionStateBillingService, String descriptionServiceType,
			String descriptionCollectionPeriod,
			String descriptionClientServiceType, String descriptionCalculationPeriod,
			Integer sourceInformation, Integer currencyCalculation,
			String accountingAccount,String referenceRate, Integer portfolio) {
		this.idBillingServicePk = idBillingServicePk;
		this.baseCollection = baseCollection;
		this.businessLine = businessLine;
		this.finantialCode = finantialCode;
		this.calculationPeriod = calculationPeriod;
		this.clientServiceType = clientServiceType;
		this.collectionPeriod = collectionPeriod;
		this.collectionType = collectionType;
		this.observation = observation;
		this.endEffectiveDate = endEffectiveDate;
		this.entityCollection = entityCollection;
		this.initialEffectiveDate = initialEffectiveDate;
		this.serviceCode = serviceCode;
		this.serviceName = serviceName;
		this.serviceState = serviceState;
		this.serviceType = serviceType;
		this.taxApplied = taxApplied;
		this.registryDate = registryDate;
		this.inIntegratedBill = inIntegratedBill;
		this.indSourceBaseCollection = indSourceBaseCollection;
		this.billingServiceSource = billingServiceSource;
		this.currencyBilling = currencyBilling;
		this.descriptionCollectionEntity = descriptionCollectionEntity;
		this.descriptionBaseCollection = descriptionBaseCollection;
		this.descriptionStateBillingService = descriptionStateBillingService;
		this.descriptionServiceType = descriptionServiceType;
		this.descriptionCollectionPeriod = descriptionCollectionPeriod;
		this.descriptionClientServiceType = descriptionClientServiceType;
		this.descriptionCalculationPeriod = descriptionCalculationPeriod;
		this.sourceInformation=sourceInformation;
		this.currencyCalculation=currencyCalculation;
		this.accountingAccount=accountingAccount;
		this.referenceRate=referenceRate;
		this.portfolio=portfolio;


	}

	
 

	


	/**
	 * @return the baseCollection
	 */
	public Integer getBaseCollection() {
		return baseCollection;
	}

	/**
	 * @param baseCollection the baseCollection to set
	 */
	public void setBaseCollection(Integer baseCollection) {
		this.baseCollection = baseCollection;
	}

	/**
	 * @return the businessLine
	 */
	public String getBusinessLine() {
		return businessLine;
	}

	/**
	 * @param businessLine the businessLine to set
	 */
	public void setBusinessLine(String businessLine) {
		this.businessLine = businessLine;
	}

	/**
	 * @return the finantialCode
	 */
	public String getFinantialCode() {
		return finantialCode;
	}

	/**
	 * @param finantialCode the finantialCode to set
	 */
	public void setFinantialCode(String finantialCode) {
		this.finantialCode = finantialCode;
	}

	/**
	 * @return the calculationPeriod
	 */
	public Integer getCalculationPeriod() {
		return calculationPeriod;
	}

	/**
	 * @param calculationPeriod the calculationPeriod to set
	 */
	public void setCalculationPeriod(Integer calculationPeriod) {
		this.calculationPeriod = calculationPeriod;
	}

	/**
	 * @return the clientServiceType
	 */
	public Integer getClientServiceType() {
		return clientServiceType;
	}

	/**
	 * @param clientServiceType the clientServiceType to set
	 */
	public void setClientServiceType(Integer clientServiceType) {
		this.clientServiceType = clientServiceType;
	}

	/**
	 * @return the collectionPeriod
	 */
	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}

	/**
	 * @param collectionPeriod the collectionPeriod to set
	 */
	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}

	/**
	 * @return the collectionType
	 */
	public Integer getCollectionType() {
		return collectionType;
	}

	/**
	 * @param collectionType the collectionType to set
	 */
	public void setCollectionType(Integer collectionType) {
		this.collectionType = collectionType;
	}

	/**
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}

	/**
	 * @param observation the observation to set
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

	/**
	 * @return the documentServiceType
	 */
	public Integer getDocumentServiceType() {
		return documentServiceType;
	}

	/**
	 * @param documentServiceType the documentServiceType to set
	 */
	public void setDocumentServiceType(Integer documentServiceType) {
		this.documentServiceType = documentServiceType;
	}

	/**
	 * @return the endEffectiveDate
	 */
	public Date getEndEffectiveDate() {
		return endEffectiveDate;
	}

	/**
	 * @param endEffectiveDate the endEffectiveDate to set
	 */
	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}

	/**
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}

	/**
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}

	/**
	 * @return the initialEffectiveDate
	 */
	public Date getInitialEffectiveDate() {
		return initialEffectiveDate;
	}

	/**
	 * @param initialEffectiveDate the initialEffectiveDate to set
	 */
	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the serviceState
	 */
	public Integer getServiceState() {
		return serviceState;
	}

	/**
	 * @param serviceState the serviceState to set
	 */
	public void setServiceState(Integer serviceState) {
		this.serviceState = serviceState;
	}

	/**
	 * @return the serviceType
	 */
	public Integer getServiceType() {
		return serviceType;
	}

	/**
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * @return the taxApplied
	 */
	public Integer getTaxApplied() {
		return taxApplied;
	}

	/**
	 * @param taxApplied the taxApplied to set
	 */
	public void setTaxApplied(Integer taxApplied) {
		this.taxApplied = taxApplied;
	}

	/**
	 * @return the unlockDate
	 */
	public Date getUnlockDate() {
		return unlockDate;
	}

	/**
	 * @param unlockDate the unlockDate to set
	 */
	public void setUnlockDate(Date unlockDate) {
		this.unlockDate = unlockDate;
	}

	/**
	 * @return the unlockUser
	 */
	public String getUnlockUser() {
		return unlockUser;
	}

	/**
	 * @param unlockUser the unlockUser to set
	 */
	public void setUnlockUser(String unlockUser) {
		this.unlockUser = unlockUser;
	}

	/**
	 * @return the lockDate
	 */
	public Date getLockDate() {
		return lockDate;
	}

	/**
	 * @param lockDate the lockDate to set
	 */
	public void setLockDate(Date lockDate) {
		this.lockDate = lockDate;
	}

	/**
	 * @return the lockUser
	 */
	public String getLockUser() {
		return lockUser;
	}

	/**
	 * @param lockUser the lockUser to set
	 */
	public void setLockUser(String lockUser) {
		this.lockUser = lockUser;
	}

	/**
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * @return the registryUser
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * @param registryUser the registryUser to set
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * @return the cancelDate
	 */
	public Date getCancelDate() {
		return cancelDate;
	}

	/**
	 * @param cancelDate the cancelDate to set
	 */
	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	/**
	 * @return the cancelUser
	 */
	public String getCancelUser() {
		return cancelUser;
	}

	/**
	 * @param cancelUser the cancelUser to set
	 */
	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}

	/**
	 * @return the inIntegratedBill
	 */
	public Integer getInIntegratedBill() {
		return inIntegratedBill;
	}

	/**
	 * @param inIntegratedBill the inIntegratedBill to set
	 */
	public void setInIntegratedBill(Integer inIntegratedBill) {
		this.inIntegratedBill = inIntegratedBill;
	}

	/**
	 * @return the processedServices
	 */
	public List<ProcessedService> getProcessedServices() {
		return processedServices;
	}

	/**
	 * @param processedServices the processedServices to set
	 */
	public void setProcessedServices(List<ProcessedService> processedServices) {
		this.processedServices = processedServices;
	}

	/**
	 * @return the serviceRates
	 */
	public List<ServiceRate> getServiceRates() {
		return serviceRates;
	}

	/**
	 * @param serviceRates the serviceRates to set
	 */
	public void setServiceRates(List<ServiceRate> serviceRates) {
		this.serviceRates = serviceRates;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the currencyBilling
	 */
	public Integer getCurrencyBilling() {
		return currencyBilling;
	}

	/**
	 * @param currencyBilling the currencyBilling to set
	 */
	public void setCurrencyBilling(Integer currencyBilling) {
		this.currencyBilling = currencyBilling;
	}

	/**
	 * @return the descriptionCollectionEntity
	 */
	public String getDescriptionCollectionEntity() {
		return descriptionCollectionEntity;
	}

	/**
	 * @param descriptionCollectionEntity the descriptionCollectionEntity to set
	 */
	public void setDescriptionCollectionEntity(String descriptionCollectionEntity) {
		this.descriptionCollectionEntity = descriptionCollectionEntity;
	}

	/**
	 * @return the descriptionBaseCollection
	 */
	public String getDescriptionBaseCollection() {
		return descriptionBaseCollection;
	}

	/**
	 * @param descriptionBaseCollection the descriptionBaseCollection to set
	 */
	public void setDescriptionBaseCollection(String descriptionBaseCollection) {
		this.descriptionBaseCollection = descriptionBaseCollection;
	}

	/**
	 * @return the descriptionStateBillingService
	 */
	public String getDescriptionStateBillingService() {
		return descriptionStateBillingService;
	}

	/**
	 * @param descriptionStateBillingService the descriptionStateBillingService to set
	 */
	public void setDescriptionStateBillingService(
			String descriptionStateBillingService) {
		this.descriptionStateBillingService = descriptionStateBillingService;
	}

	/**
	 * @return the descriptionServiceType
	 */
	public String getDescriptionServiceType() {
		return descriptionServiceType;
	}

	/**
	 * @param descriptionServiceType the descriptionServiceType to set
	 */
	public void setDescriptionServiceType(String descriptionServiceType) {
		this.descriptionServiceType = descriptionServiceType;
	}

	/**
	 * @return the descriptionCollectionPeriod
	 */
	public String getDescriptionCollectionPeriod() {
		return descriptionCollectionPeriod;
	}

	/**
	 * @param descriptionCollectionPeriod the descriptionCollectionPeriod to set
	 */
	public void setDescriptionCollectionPeriod(String descriptionCollectionPeriod) {
		this.descriptionCollectionPeriod = descriptionCollectionPeriod;
	}

	/**
	 * @return the descriptionClientServiceType
	 */
	public String getDescriptionClientServiceType() {
		return descriptionClientServiceType;
	}

	/**
	 * @param descriptionClientServiceType the descriptionClientServiceType to set
	 */
	public void setDescriptionClientServiceType(String descriptionClientServiceType) {
		this.descriptionClientServiceType = descriptionClientServiceType;
	}

	/**
	 * @param idBillingServicePk the idBillingServicePk to set
	 */
	public void setIdBillingServicePk(Long idBillingServicePk) {
		this.idBillingServicePk = idBillingServicePk;
	}

	/**
	 * @return the currencyCalculation
	 */
	public Integer getCurrencyCalculation() {
		return currencyCalculation;
	}

	/**
	 * @param currencyCalculation the currencyCalculation to set
	 */
	public void setCurrencyCalculation(Integer currencyCalculation) {
		this.currencyCalculation = currencyCalculation;
	}



	
	/**
	 * @return the descriptionCalculationPeriod
	 */
	public String getDescriptionCalculationPeriod() {
		return descriptionCalculationPeriod;
	}

	/**
	 * @param descriptionCalculationPeriod the descriptionCalculationPeriod to set
	 */
	public void setDescriptionCalculationPeriod(String descriptionCalculationPeriod) {
		this.descriptionCalculationPeriod = descriptionCalculationPeriod;
	}

	/**
	 * @return the indSourceBaseCollection
	 */
	public Integer getIndSourceBaseCollection() {
		return indSourceBaseCollection;
	}

	/**
	 * @param indSourceBaseCollection the indSourceBaseCollection to set
	 */
	public void setIndSourceBaseCollection(Integer indSourceBaseCollection) {
		this.indSourceBaseCollection = indSourceBaseCollection;
	}
	
	

	/**
	 * @return the billingServiceSource
	 */
	public BillingServiceSource getBillingServiceSource() {
		return billingServiceSource;
	}

	/**
	 * @param billingServiceSource the billingServiceSource to set
	 */
	public void setBillingServiceSource(BillingServiceSource billingServiceSource) {
		this.billingServiceSource = billingServiceSource;
	}
	

	/**
	 * @return the listExceptionInvoiceGenerations
	 */
	public List<ExceptionInvoiceGenerations> getListExceptionInvoiceGenerations() {
		return listExceptionInvoiceGenerations;
	}

	/**
	 * @param listExceptionInvoiceGenerations the listExceptionInvoiceGenerations to set
	 */
	public void setListExceptionInvoiceGenerations(
			List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations) {
		this.listExceptionInvoiceGenerations = listExceptionInvoiceGenerations;
	}

	

	/**
	 * @return the sourceInformation
	 */
	public Integer getSourceInformation() {
		return sourceInformation;
	}

	/**
	 * @param sourceInformation the sourceInformation to set
	 */
	public void setSourceInformation(Integer sourceInformation) {
		this.sourceInformation = sourceInformation;
	}

	/**
	 * @return the accountingAccount
	 */
	public String getAccountingAccount() {
		return accountingAccount;
	}

	/**
	 * @param accountingAccount the accountingAccount to set
	 */
	public void setAccountingAccount(String accountingAccount) {
		this.accountingAccount = accountingAccount;
	}

	/**
	 * @return the referenceRate
	 */
	public String getReferenceRate() {
		return referenceRate;
	}

	/**
	 * @param referenceRate the referenceRate to set
	 */
	public void setReferenceRate(String referenceRate) {
		this.referenceRate = referenceRate;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
				if (loggerUser != null) {
		            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
		            lastModifyDate = loggerUser.getAuditTime();
		            lastModifyIp = loggerUser.getIpAddress();
		            lastModifyUser = loggerUser.getUserName();
		        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the billingEconomicActivitys
	 */
	public List<BillingEconomicActivity> getBillingEconomicActivitys() {
		return billingEconomicActivitys;
	}

	/**
	 * @param billingEconomicActivitys the billingEconomicActivitys to set
	 */
	public void setBillingEconomicActivitys(List<BillingEconomicActivity> billingEconomicActivitys) {
		this.billingEconomicActivitys = billingEconomicActivitys;
	}

	/**
	 * @return the portfolio
	 */
	public Integer getPortfolio() {
		return portfolio;
	}

	/**
	 * @param portfolio the portfolio to set
	 */
	public void setPortfolio(Integer portfolio) {
		this.portfolio = portfolio;
	}

}