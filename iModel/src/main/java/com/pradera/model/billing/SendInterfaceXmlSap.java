package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "SEND_INTERFACE_XML_SAP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SendInterfaceXmlSap.findAll", query = "SELECT s FROM SendInterfaceXmlSap s")})
public class SendInterfaceXmlSap implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="INTERFACE_XML_SAP_GENERATION", sequenceName="SQ_ID_SEND_INTERFACE_XML_SAP_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INTERFACE_XML_SAP_GENERATION")
    @Column(name = "ID_SEND_INTERFACE_XML_SAP_PK")
    private Long idSendInterfaceXmlSapPk;
    @Column(name="ID_ACCOUNTING_PROCESS_FK")
	private Long idAccountingProcessFk;
	@Column(name="ID_BILLING_CALC_PROCESS_FK")
	private Long idBillingCalcProcessFk;
    @Column(name = "STATE")
    private Integer state;
    @Column(name = "TOTAL_RECORDS")
    private Integer totalRecords;
    @Column(name = "ACCEPTED_RECORDS")
    private Integer acceptedRecords;
    @Column(name = "REJECT_RECORDS")
    private Integer rejectRecords;
    @Basic(optional = false)
    @Column(name = "RESPONSE_VALUE")
    private String responseValue;
    
    @Column(name = "ID_TICKET")
    private Long idTicket;
    
    @Column(name = "NUMBER_RESEND")
    private Integer numberReSend;
    
    @Basic(optional = false)
    @Column(name = "INVOICE_LIST_STRING")
    private String invoiceListString;
    
    @Basic(optional = false)
    @Column(name = "REGISTRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registryDate;
    @Basic(optional = false)
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Basic(optional = false)
    @Column(name = "LAST_MODIFY_APP")
    private Integer lastModifyApp;
    @Basic(optional = false)
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    
//  @Basic(optional = false)
//  @Column(name = "ID_INTERFACE_TRANSACTION_FK")
//  private Long idInterfacesTransactionFk;
  
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INTERFACE_TRANSACTION_FK", nullable=false)
	private InterfaceTransaction idInterfacesTransactionFk;
    
    

    public SendInterfaceXmlSap() {
    }

	public Long getIdSendInterfaceXmlSapPk() {
		return idSendInterfaceXmlSapPk;
	}

	public void setIdSendInterfaceXmlSapPk(Long idSendInterfaceXmlSapPk) {
		this.idSendInterfaceXmlSapPk = idSendInterfaceXmlSapPk;
	}

	public InterfaceTransaction getIdInterfacesTransactionFk() {
		return idInterfacesTransactionFk;
	}

	public void setIdInterfacesTransactionFk(InterfaceTransaction idInterfacesTransactionFk) {
		this.idInterfacesTransactionFk = idInterfacesTransactionFk;
	}

	public Integer getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Integer getAcceptedRecords() {
		return acceptedRecords;
	}

	public void setAcceptedRecords(Integer acceptedRecords) {
		this.acceptedRecords = acceptedRecords;
	}

	public Integer getRejectRecords() {
		return rejectRecords;
	}

	public void setRejectRecords(Integer rejectRecords) {
		this.rejectRecords = rejectRecords;
	}


	public String getResponseValue() {
		return responseValue;
	}

	public void setResponseValue(String responseValue) {
		this.responseValue = responseValue;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Long getIdAccountingProcessFk() {
		return idAccountingProcessFk;
	}

	public void setIdAccountingProcessFk(Long idAccountingProcessFk) {
		this.idAccountingProcessFk = idAccountingProcessFk;
	}

	public Long getIdBillingCalcProcessFk() {
		return idBillingCalcProcessFk;
	}

	public void setIdBillingCalcProcessFk(Long idBillingCalcProcessFk) {
		this.idBillingCalcProcessFk = idBillingCalcProcessFk;
	}

	public Long getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(Long idTicket) {
		this.idTicket = idTicket;
	}

	public Integer getNumberReSend() {
		return numberReSend;
	}

	public void setNumberReSend(Integer numberReSend) {
		this.numberReSend = numberReSend;
	}

	public String getInvoiceListString() {
		return invoiceListString;
	}

	public void setInvoiceListString(String invoiceListString) {
		this.invoiceListString = invoiceListString;
	}
}
