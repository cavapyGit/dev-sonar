package com.pradera.model.billing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the SERVICE_RATE database table.
 * 
 */
@Entity
@Table(name="SERVICE_RATE")
public class ServiceRate implements Serializable,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SERVICE_RATE_GENERATOR", sequenceName="SQ_ID_SERVICE_RATE_PK" ,initialValue=1,allocationSize=1)
	@GeneratedValue(generator="SERVICE_RATE_GENERATOR",strategy=GenerationType.SEQUENCE)
	@Column(name="ID_SERVICE_RATE_PK", unique=true, nullable=false)
	private Long idServiceRatePk;
	
	@Column(name="COMMENTS",length=200)
	private String comments;

	@Column(name="CURRENCY_RATE")
	private Integer currencyRate;

	@Temporal(TemporalType.DATE)
	@Column(name="END_EFFECTIVE_DATE")
	private Date endEffectiveDate;

	@Column(name="IND_TAXED")
	private Integer indTaxed;

	@Temporal(TemporalType.DATE)
	@Column(name="INITIAL_EFFECTIVE_DATE")
	private Date initialEffectiveDate;

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Column(name="RATE_AMOUNT")
	private BigDecimal rateAmount;

	@Column(name="RATE_CODE")
	private Integer rateCode;

	@Column(name="RATE_NAME")
	private String rateName;

	@Column(name="RATE_PERCENT")
	private BigDecimal ratePercent;

	@Column(name="RATE_STATE")
	private Integer rateState;

	@Column(name="RATE_TYPE")
	private Integer rateType;

	@Column(name="IND_SCALE_MOVEMENT")
	private Integer indScaleMovement;
	
	
	@Column(name="IND_MINIMUM_AMOUNT")
	private Integer indMinimumAmount;
	
	@Column(name="MINIMUM_AMOUNT")
	private BigDecimal minimumAmount;
	
	@Column(name="CURRENCY_MINIMUM_AMOUNT")
	private Integer currencyMinimumAmount;
	
	@Column(name="IND_MINIMUM_PROCESS")
	private Integer indMinimumProcess;
	

	//bi-directional many-to-one association to RateMovement
	@OneToMany(mappedBy="serviceRate", fetch=FetchType.LAZY)
	private List<RateMovement> rateMovements;

	//bi-directional many-to-one association to RateExclusion
	@OneToMany(mappedBy="serviceRate", fetch=FetchType.LAZY)
	private List<RateExclusion> rateExclusions;

	//bi-directional many-to-one association to RateInclusion
	@OneToMany(mappedBy="serviceRate", fetch=FetchType.LAZY)
	private List<RateInclusion> rateInclusions;

	//bi-directional many-to-one association to ServiceRateScale
	@OneToMany(mappedBy="serviceRate" , fetch=FetchType.LAZY)
	private List<ServiceRateScale> serviceRateScales;
	
	//bi-directional many-to-one association to BillingService
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BILLING_SERVICE_FK", nullable=false)
	private BillingService billingService;
	
	@Transient
	private String descriptionStatus;
	
	@Transient
	private String descriptionType;
	
	@Transient
	private Boolean isSelected;
	
	public ServiceRate() {
	}
	
	
	
	public String getDescriptionStatus() {
		return descriptionStatus;
	}



	public void setDescriptionStatus(String descriptionStatus) {
		this.descriptionStatus = descriptionStatus;
	}



	public String getDescriptionType() {
		return descriptionType;
	}



	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}



	public BillingService getBillingService() {
		return billingService;
	}



	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}



	public Long getIdServiceRatePk() {
		return this.idServiceRatePk;
	}

	



	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	
	public Integer getCurrencyRate() {
		return currencyRate;
	}



	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}



	public Date getEndEffectiveDate() {
		return this.endEffectiveDate;
	}

	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}

	

	public Date getInitialEffectiveDate() {
		return this.initialEffectiveDate;
	}

	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}

	
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public BigDecimal getRateAmount() {
		return this.rateAmount;
	}

	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}

	

	public String getRateName() {
		return this.rateName;
	}

	public void setRateName(String rateName) {
		this.rateName = rateName;
	}

	public BigDecimal getRatePercent() {
		return this.ratePercent;
	}

	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}

	
	

	public Integer getIndTaxed() {
		return indTaxed;
	}

	public void setIndTaxed(Integer indTaxed) {
		this.indTaxed = indTaxed;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getRateCode() {
		return rateCode;
	}

	public void setRateCode(Integer rateCode) {
		this.rateCode = rateCode;
	}

	public Integer getRateState() {
		return rateState;
	}

	public void setRateState(Integer rateState) {
		this.rateState = rateState;
	}

	public Integer getRateType() {
		return rateType;
	}

	public void setRateType(Integer rateType) {
		this.rateType = rateType;
	}

	public void setIdServiceRatePk(Long idServiceRatePk) {
		this.idServiceRatePk = idServiceRatePk;
	}

	public List<RateMovement> getRateMovements() {
		return this.rateMovements;
	}

	public void setRateMovements(List<RateMovement> rateMovements) {
		this.rateMovements = rateMovements;
	}

	public RateMovement addRateMovement(RateMovement rateMovement) {
		getRateMovements().add(rateMovement);
		rateMovement.setServiceRate(this);

		return rateMovement;
	}

	public RateMovement removeRateMovement(RateMovement rateMovement) {
		getRateMovements().remove(rateMovement);
		rateMovement.setServiceRate(null);

		return rateMovement;
	}

	

	/**
	 * @return the serviceRateScales
	 */
	public List<ServiceRateScale> getServiceRateScales() {
		return serviceRateScales;
	}



	/**
	 * @param serviceRateScales the serviceRateScales to set
	 */
	public void setServiceRateScales(List<ServiceRateScale> serviceRateScales) {
		this.serviceRateScales = serviceRateScales;
	}



	public ServiceRateScale addServiceRateScale(ServiceRateScale serviceRateScale) {
		getServiceRateScales().add(serviceRateScale);
		serviceRateScale.setServiceRate(this);

		return serviceRateScale;
	}

	public ServiceRateScale removeServiceRateScale(ServiceRateScale serviceRateScale) {
		getServiceRateScales().remove(serviceRateScale);
		serviceRateScale.setServiceRate(null);

		return serviceRateScale;
	}
	
	

	

	/**
	 * @return the rateExclusions
	 */
	public List<RateExclusion> getRateExclusions() {
		return rateExclusions;
	}



	/**
	 * @param rateExclusions the rateExclusions to set
	 */
	public void setRateExclusions(List<RateExclusion> rateExclusions) {
		this.rateExclusions = rateExclusions;
	}



	/**
	 * @return the rateInclusions
	 */
	public List<RateInclusion> getRateInclusions() {
		return rateInclusions;
	}



	/**
	 * @param rateInclusions the rateInclusions to set
	 */
	public void setRateInclusions(List<RateInclusion> rateInclusions) {
		this.rateInclusions = rateInclusions;
	}



	/**
	 * @return the indScaleMovement
	 */
	public Integer getIndScaleMovement() {
		return indScaleMovement;
	}



	/**
	 * @param indScaleMovement the indScaleMovement to set
	 */
	public void setIndScaleMovement(Integer indScaleMovement) {
		this.indScaleMovement = indScaleMovement;
	}



	/**
	 * @return the isSelected
	 */
	public Boolean getIsSelected() {
		return isSelected;
	}



	/**
	 * @param isSelected the isSelected to set
	 */
	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}


	/**
	 * *Indicador si la tarifa Fija tiene un monto minimo
	 * 0: Sin monto Minimo
	 * 1: Con monto Minimo
	 * @return the indMinimumAmount
	 */
	public Integer getIndMinimumAmount() {
		return indMinimumAmount;
	}



	/**
	 * *Indicador si la tarifa Fija tiene un monto minimo
  		0: Sin monto Minimo
  		1: Con monto Minimo
	 * @param indMinimumAmount the indMinimumAmount to set
	 */
	public void setIndMinimumAmount(Integer indMinimumAmount) {
		this.indMinimumAmount = indMinimumAmount;
	}



	/**
	 * @return the minimumAmount
	 */
	public BigDecimal getMinimumAmount() {
		return minimumAmount;
	}



	/**
	 * @param minimumAmount the minimumAmount to set
	 */
	public void setMinimumAmount(BigDecimal minimumAmount) {
		this.minimumAmount = minimumAmount;
	}



	/**
	 * @return the currencyMinimumAmount
	 */
	public Integer getCurrencyMinimumAmount() {
		return currencyMinimumAmount;
	}



	/**
	 * @param currencyMinimumAmount the currencyMinimumAmount to set
	 */
	public void setCurrencyMinimumAmount(Integer currencyMinimumAmount) {
		this.currencyMinimumAmount = currencyMinimumAmount;
	}



	/**
	 * @return the indMinimumProcess
	 */
	public Integer getIndMinimumProcess() {
		return indMinimumProcess;
	}



	/**
	 * @param indMinimumProcess the indMinimumProcess to set
	 */
	public void setIndMinimumProcess(Integer indMinimumProcess) {
		this.indMinimumProcess = indMinimumProcess;
	}



	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		// 
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}


	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("rateMovements", rateMovements);
        detailsMap.put("rateExclusions", rateExclusions);
        detailsMap.put("rateInclusions", rateInclusions);
        detailsMap.put("serviceRateScales", serviceRateScales);
		return detailsMap;
	}
	
	public RateMovement getRateMovement(Long typeMovementPk ){
		RateMovement rateMovement=null;
		for (RateMovement rateMovementSeq : rateMovements) {
			if (rateMovementSeq.getMovementType().getIdMovementTypePk().equals(typeMovementPk)){
				rateMovement=rateMovementSeq;
			}
		}
		return rateMovement;
	}



}