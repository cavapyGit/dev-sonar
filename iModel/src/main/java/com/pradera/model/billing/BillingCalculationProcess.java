package com.pradera.model.billing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the BILLING_CALCULATION_PROCESS database table.
 * 
 */
@Entity
@Table(name="BILLING_CALCULATION_PROCESS")
@NamedQueries({
	@NamedQuery(name = BillingCalculationProcess.BILLING_CALCULATION_PROCESS_CONTENT_BY_PK, query = " select bcp.fileXml from BillingCalculationProcess bcp where bcp.idBillingCalcProcessPk =:idBillingCalcProcessPk ")
})
public class BillingCalculationProcess implements Serializable ,Auditable{
	
	private static final long serialVersionUID = 1L;
	public static final String BILLING_CALCULATION_PROCESS_CONTENT_BY_PK = "BillingCalculationProcess.getContentByPk";
	
	@Id
	@SequenceGenerator(name="BILLING_CALCULATION_PROCESS_GENERATOR", sequenceName="SQ_ID_BILLING_CALC_PROCESS_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILLING_CALCULATION_PROCESS_GENERATOR")
	@Column(name="ID_BILLING_CALC_PROCESS_PK")
	private Long idBillingCalcProcessPk;

	@Column(name="BILLING_PERIOD")
	private Integer billingPeriod;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPERATION_DATE")
	private Date operationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BILLING_DATE")
	private Date billingDate;
	

	@Column(name="ENTITY_COLLECTION", precision=10)
	private Integer entityCollection;
	
	@Column(name="PROCESSED_BILLING_NUMBER")
	private Long processedBillingNumber;
	
	@Column(name="BILLING_YEAR")
	private Integer billingYear;
	
	@Column(name="BILLING_MONTH")
	private Integer billingMonth;
	
	@Column(name="PROCESSED_STATE")
	private Integer processedState;
	
	//bi-directional many-to-one association to BillingCalculation
	@OneToMany(mappedBy="billingCalculationProcess",cascade=CascadeType.ALL)
	private List<BillingCalculation> billingCalculations;	
	

	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	@Lob
    @NotNull
	@Column(name="FILE_XML")
    @Basic(fetch=FetchType.LAZY)
	private byte[] fileXml;
	
	@Column(name="NAME_FILE")
	private String nameFile;
	
	@Transient 
	private String descriptionBillingPeriod;
	@Transient	
	private String descriptionEntityCollection;
	@Transient	
	private String descriptionProcessedState;
	
	/** The billing service code. */
	@Transient	
	private String billingServiceCode;
	
	public BillingCalculationProcess() {
		this.billingCalculations = new ArrayList<BillingCalculation>();
	}

	


	/**
	 * @param idBillingCalcProcessPk
	 * @param billingPeriod
	 * @param operationDate
	 * @param billingDate
	 * @param entityCollection
	 * @param processedState
	 * @param nameFile
	 */
	public BillingCalculationProcess(Long idBillingCalcProcessPk,
			Integer billingPeriod, Date operationDate, Date billingDate,
			Integer entityCollection, Integer processedState, String nameFile) {
		this.idBillingCalcProcessPk = idBillingCalcProcessPk;
		this.billingPeriod = billingPeriod;
		this.operationDate = operationDate;
		this.billingDate = billingDate;
		this.entityCollection = entityCollection;
		this.processedState = processedState;
		this.nameFile = nameFile;
	}
	
	




	/**
	 * @param idBillingCalcProcessPk
	 * @param operationDate
	 * @param billingDate
	 * @param processedState
	 * @param nameFile
	 * @param descriptionCalculationPeriod
	 * @param descriptionEntityCollection
	 * @param descriptionProcessedState
	 */
	public BillingCalculationProcess(Long idBillingCalcProcessPk,
			Date operationDate, Date billingDate, Integer processedState,
			String nameFile, String descriptionBillingPeriod,
			String descriptionEntityCollection, String descriptionProcessedState) {
		this.idBillingCalcProcessPk = idBillingCalcProcessPk;
		this.operationDate = operationDate;
		this.billingDate = billingDate;
		this.processedState = processedState;
		this.nameFile = nameFile;
		this.descriptionBillingPeriod = descriptionBillingPeriod;
		this.descriptionEntityCollection = descriptionEntityCollection;
		this.descriptionProcessedState = descriptionProcessedState;
	}




	/**
	 * @return the idBillingCalcProcessPk
	 */
	public Long getIdBillingCalcProcessPk() {
		return idBillingCalcProcessPk;
	}




	/**
	 * @param idBillingCalcProcessPk the idBillingCalcProcessPk to set
	 */
	public void setIdBillingCalcProcessPk(Long idBillingCalcProcessPk) {
		this.idBillingCalcProcessPk = idBillingCalcProcessPk;
	}




	/**
	 * @return the billingPeriod
	 */
	public Integer getBillingPeriod() {
		return billingPeriod;
	}




	/**
	 * @param billingPeriod the billingPeriod to set
	 */
	public void setBillingPeriod(Integer billingPeriod) {
		this.billingPeriod = billingPeriod;
	}




	/**
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}




	/**
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}




	/**
	 * @return the billingDate
	 */
	public Date getBillingDate() {
		return billingDate;
	}




	/**
	 * @param billingDate the billingDate to set
	 */
	public void setBillingDate(Date billingDate) {
		this.billingDate = billingDate;
	}




	/**
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}




	/**
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}




	/**
	 * @return the processedBillingNumber
	 */
	public Long getProcessedBillingNumber() {
		return processedBillingNumber;
	}




	/**
	 * @param processedBillingNumber the processedBillingNumber to set
	 */
	public void setProcessedBillingNumber(Long processedBillingNumber) {
		this.processedBillingNumber = processedBillingNumber;
	}




	/**
	 * @return the billingYear
	 */
	public Integer getBillingYear() {
		return billingYear;
	}




	/**
	 * @param billingYear the billingYear to set
	 */
	public void setBillingYear(Integer billingYear) {
		this.billingYear = billingYear;
	}




	/**
	 * @return the billingMonth
	 */
	public Integer getBillingMonth() {
		return billingMonth;
	}




	/**
	 * @param billingMonth the billingMonth to set
	 */
	public void setBillingMonth(Integer billingMonth) {
		this.billingMonth = billingMonth;
	}




	/**
	 * @return the processedState
	 */
	public Integer getProcessedState() {
		return processedState;
	}




	/**
	 * @param processedState the processedState to set
	 */
	public void setProcessedState(Integer processedState) {
		this.processedState = processedState;
	}




	/**
	 * @return the billingCalculations
	 */
	public List<BillingCalculation> getBillingCalculations() {
		return billingCalculations;
	}




	/**
	 * @param billingCalculations the billingCalculations to set
	 */
	public void setBillingCalculations(List<BillingCalculation> billingCalculations) {
		this.billingCalculations = billingCalculations;
	}




	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}




	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}




	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}




	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}




	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}




	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}




	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}




	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}




	/**
	 * @return the fileXml
	 */
	public byte[] getFileXml() {
		return fileXml;
	}




	/**
	 * @param fileXml the fileXml to set
	 */
	public void setFileXml(byte[] fileXml) {
		this.fileXml = fileXml;
	}




	/**
	 * @return the nameFile
	 */
	public String getNameFile() {
		return nameFile;
	}




	/**
	 * @param nameFile the nameFile to set
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}




	/**
	 * @return the descriptionBillingPeriod
	 */
	public String getDescriptionBillingPeriod() {
		return descriptionBillingPeriod;
	}




	/**
	 * @param descriptionBillingPeriod the descriptionBillingPeriod to set
	 */
	public void setDescriptionBillingPeriod(String descriptionBillingPeriod) {
		this.descriptionBillingPeriod = descriptionBillingPeriod;
	}




	/**
	 * @return the descriptionEntityCollection
	 */
	public String getDescriptionEntityCollection() {
		return descriptionEntityCollection;
	}




	/**
	 * @param descriptionEntityCollection the descriptionEntityCollection to set
	 */
	public void setDescriptionEntityCollection(String descriptionEntityCollection) {
		this.descriptionEntityCollection = descriptionEntityCollection;
	}




	/**
	 * @return the descriptionProcessedState
	 */
	public String getDescriptionProcessedState() {
		return descriptionProcessedState;
	}




	/**
	 * @param descriptionProcessedState the descriptionProcessedState to set
	 */
	public void setDescriptionProcessedState(String descriptionProcessedState) {
		this.descriptionProcessedState = descriptionProcessedState;
	}




	/**
	 * @return the billingServiceCode
	 */
	public String getBillingServiceCode() {
		return billingServiceCode;
	}




	/**
	 * @param billingServiceCode the billingServiceCode to set
	 */
	public void setBillingServiceCode(String billingServiceCode) {
		this.billingServiceCode = billingServiceCode;
	}




	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("billingCalculations", billingCalculations);
        return detailsMap;
	}

}