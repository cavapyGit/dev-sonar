package com.pradera.model.billing;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


/**
 * The persistent class for the EXCEPTION_INVOICE_GENERATIONS database table.
 * 
 */
@Entity
@Table(name="EXCEPTION_INVOICE_GENERATIONS")
public class ExceptionInvoiceGenerations implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EXCEPTION_INVOICE_GENERATIONS_GEN", sequenceName="SQ_EXC_INVOICE_GENERATIONS_PK", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXCEPTION_INVOICE_GENERATIONS_GEN")
	@Column(name="ID_EXCEPTION_INVOICE_PK", unique=true, nullable=false, precision=10)
	private Long idExceptionInvoicePk;

	@Column(name="EXCEPTION_DATE")
	private Date exceptionDate;
	
	@Column(name="EXCEPTION_STATE", precision=10)
	private Integer exceptionState;
	
	@Column(name="TYPE_EXCEPTION", precision=10)
	private Integer typeException;
	
	@Column(name="IND_SELECT_ALL")
	private Integer indSelectAll;
	
	//bi-directional many-to-one association to BillingService
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BILLING_SERVICE_FK", nullable=false)
	private BillingService billingService;
	
	//bi-directional many-to-one association to BillingService
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BILLING_SCHEDULE_FK", nullable=false)
	private BillingSchedule billingSchedule;
	
	//bi-directional many-to-one association to ProcessedExceptionInvoice
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCESSED_EXCEPTION_FK", nullable=false)
	private ProcessedExceptionInvoice processedExceptionInvoice;

	@OneToMany(mappedBy="exceptionInvoiceGenerations",fetch=FetchType.LAZY)
	private List<BillingEntityException> billingEntityException;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;


	
	
	

	/**
	 * 
	 */
	public ExceptionInvoiceGenerations() {
	}



	/**
	 * @param idExceptionInvoicePk
	 * @param exceptionDate
	 * @param exceptionState
	 * @param typeException
	 * @param indSelectAll
	 * @param billingService
	 * @param billingSchedule
	 * @param processedExceptionInvoice
	 * @param billingEntityException
	 */
	public ExceptionInvoiceGenerations(Long idExceptionInvoicePk,
			Date exceptionDate, Integer exceptionState, Integer typeException,
			Integer indSelectAll, BillingService billingService,
			BillingSchedule billingSchedule,
			ProcessedExceptionInvoice processedExceptionInvoice,
			List<BillingEntityException> billingEntityException) {
		this.idExceptionInvoicePk = idExceptionInvoicePk;
		this.exceptionDate = exceptionDate;
		this.exceptionState = exceptionState;
		this.typeException = typeException;
		this.indSelectAll = indSelectAll;
		this.billingService = billingService;
		this.billingSchedule = billingSchedule;
		this.processedExceptionInvoice = processedExceptionInvoice;
		this.billingEntityException = billingEntityException;
	}



	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
		
	}



	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
        detailsMap.put("billingEntityException", billingEntityException);
        return detailsMap;
	}



	/**
	 * @return the idExceptionInvoicePk
	 */
	public Long getIdExceptionInvoicePk() {
		return idExceptionInvoicePk;
	}



	/**
	 * @param idExceptionInvoicePk the idExceptionInvoicePk to set
	 */
	public void setIdExceptionInvoicePk(Long idExceptionInvoicePk) {
		this.idExceptionInvoicePk = idExceptionInvoicePk;
	}



	/**
	 * @return the exceptionDate
	 */
	public Date getExceptionDate() {
		return exceptionDate;
	}



	/**
	 * @param exceptionDate the exceptionDate to set
	 */
	public void setExceptionDate(Date exceptionDate) {
		this.exceptionDate = exceptionDate;
	}



	/**
	 * @return the exceptionState
	 */
	public Integer getExceptionState() {
		return exceptionState;
	}



	/**
	 * @param exceptionState the exceptionState to set
	 */
	public void setExceptionState(Integer exceptionState) {
		this.exceptionState = exceptionState;
	}



	/**
	 * @return the typeException
	 */
	public Integer getTypeException() {
		return typeException;
	}



	/**
	 * @param typeException the typeException to set
	 */
	public void setTypeException(Integer typeException) {
		this.typeException = typeException;
	}



	/**
	 * 0: All
	 * 1: One
	 * @return the indSelectAll
	 */
	public Integer getIndSelectAll() {
		return indSelectAll;
	}



	/**
	 * @param indSelectAll the indSelectAll to set
	 */
	public void setIndSelectAll(Integer indSelectAll) {
		this.indSelectAll = indSelectAll;
	}



	/**
	 * @return the billingService
	 */
	public BillingService getBillingService() {
		return billingService;
	}



	/**
	 * @param billingService the billingService to set
	 */
	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}



	/**
	 * @return the billingSchedule
	 */
	public BillingSchedule getBillingSchedule() {
		return billingSchedule;
	}



	/**
	 * @param billingSchedule the billingSchedule to set
	 */
	public void setBillingSchedule(BillingSchedule billingSchedule) {
		this.billingSchedule = billingSchedule;
	}



	/**
	 * @return the billingEntityException
	 */
	public List<BillingEntityException> getBillingEntityException() {
		return billingEntityException;
	}



	/**
	 * @param billingEntityException the billingEntityException to set
	 */
	public void setBillingEntityException(
			List<BillingEntityException> billingEntityException) {
		this.billingEntityException = billingEntityException;
	}



	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}



	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}



	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}



	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}



	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}



	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}



	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}



	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}



	/**
	 * @return the processedExceptionInvoice
	 */
	public ProcessedExceptionInvoice getProcessedExceptionInvoice() {
		return processedExceptionInvoice;
	}



	/**
	 * @param processedExceptionInvoice the processedExceptionInvoice to set
	 */
	public void setProcessedExceptionInvoice(
			ProcessedExceptionInvoice processedExceptionInvoice) {
		this.processedExceptionInvoice = processedExceptionInvoice;
	}


	
}