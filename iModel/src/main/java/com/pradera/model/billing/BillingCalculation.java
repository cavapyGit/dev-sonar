package com.pradera.model.billing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuer;


/**
 * The persistent class for the BILLING_CALCULATION database table.
 * 
 */
@Entity
@Table(name="BILLING_CALCULATION")
public class BillingCalculation implements Serializable ,Auditable{
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@SequenceGenerator(name="BILLING_CALCULATION_GENERATION", sequenceName="SQ_ID_BILLING_CALCULATION_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILLING_CALCULATION_GENERATION")
	@Column(name="ID_BILLING_CALCULATION_PK")
	private Long idBillingCalculationPk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BILLING_CALC_PROCESS_FK",referencedColumnName="ID_BILLING_CALC_PROCESS_PK")
	private BillingCalculationProcess billingCalculationProcess;

	//bi-directional many-to-one association to BillingCalculationDetail
	@OneToMany(mappedBy="billingCalculation",cascade=CascadeType.ALL)
	private List<BillingCalculationDetail> billingCalculationDetails;
	
	@Column(name="BILLING_NUMBER", precision=10)
	private Long billingNumber;
	
//	@Column(name="BILLING_NCF",length=19)
//	private String billingNFC;
	
	@Column(name="ENTITY_CODE",length=21)
	private String entityCode;
	
	@Column(name="ENTITY_DESCRIPTION",length=100)
	private String entityDescription;
	
	@Column(name="INDENTIFY_TYPE", precision=10)
	private Integer indentifyType;
	
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name="BILLING_DATE")
	private Date billingDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BILLING_DATE_START")
	private Date billingDateStart;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BILLING_COURT_DATE")
	private Date billingCourtDate;

	@Column(name="ENTITY_COLLECTION",precision=10)
	private Integer entityCollection;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOLDER_FK")
	private Holder holder;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ISSUER_FK")
	private Issuer issuer;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
	private Participant participant;
	
	@Column(name="GROSS_AMOUNT",  precision=30, scale=8)
	private BigDecimal grossAmount;
	
	@Column(name="NET_AMOUNT",  precision=30, scale=8)
	private BigDecimal netAmount;
	
	@Column(name="NET_TAX",  precision=30, scale=8)
	private BigDecimal netTax;
		
	@Column(name="KEY_ENTITY_COLLECTION",length=200)
	private String keyEntityCollection;
	
	@Column(name="CURRENCY_TYPE",precision=10)
	private Integer currencyType;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
				
	@Transient
	private Integer indIntegratedBill;

	/** The residence country. */
	@Column(name="OTHER_ENTITY_COUNTRY")	
	private Integer otherEntityCountry;
	
	/** The department. */
	@Column(name="OTHER_ENTITY_DEPARTMENT")
	private Integer otherEntityDepartment;
	
	/** The province. */
	@Column(name="OTHER_ENTITY_PROVINCE")	
	private Integer otherEntityProvince;
	
	/** The district. */
	@Column(name="OTHER_ENTITY_DISTRICT")
	private Integer otherEntityDistrict;

	/** The address. */
	@Column(name="OTHER_ENTITY_ADDRESS")	
	private String otherEntityAddress;
	
	@Column(name="COUNT_REQUEST",precision=10)
	private Integer countRequest;
	
	@Column(name="REQUESTS_CONCATENATED", length=1000)
	private String requestConcatenated;
	
	@Column(name="MNEMONIC_CONCATENATED", length=1000)
	private String mnemonicConcatenated;
	
	public BillingCalculation() {
		this.billingCalculationDetails	= new ArrayList<BillingCalculationDetail>();
	}

	public Long getIdBillingCalculationPk() {
		return idBillingCalculationPk;
	}

	public void setIdBillingCalculationPk(Long idBillingCalculationPk) {
		this.idBillingCalculationPk = idBillingCalculationPk;
	}

	public Long getBillingNumber() {
		return billingNumber;
	}

	public void setBillingNumber(Long billingNumber) {
		this.billingNumber = billingNumber;
	}

//	public String getBillingNFC() {
//		return billingNFC;
//	}
//
//
//	public void setBillingNFC(String billingNFC) {
//		this.billingNFC = billingNFC;
//	}


	public String getEntityCode() {
		return entityCode;
	}


	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}


	public String getEntityDescription() {
		return entityDescription;
	}


	public void setEntityDescription(String entityDescription) {
		this.entityDescription = entityDescription;
	}

	/**
	 * El tipo de Documento de Otros
	 * @return
	 */
	public Integer getIndentifyType() {
		return indentifyType;
	}

	/**
	 * Tipo de Documento Otros
	 * @param indentifyType
	 */
	public void setIndentifyType(Integer indentifyType) {
		this.indentifyType = indentifyType;
	}


	public Date getBillingDate() {
		return billingDate;
	}


	public void setBillingDate(Date billingDate) {
		this.billingDate = billingDate;
	}


	public Date getBillingDateStart() {
		return billingDateStart;
	}


	public void setBillingDateStart(Date billingDateStart) {
		this.billingDateStart = billingDateStart;
	}


	public Date getBillingCourtDate() {
		return billingCourtDate;
	}


	public void setBillingCourtDate(Date billingCourtDate) {
		this.billingCourtDate = billingCourtDate;
	}


	public Integer getEntityCollection() {
		return entityCollection;
	}


	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}


	public Holder getHolder() {
		return holder;
	}


	public void setHolder(Holder holder) {
		this.holder = holder;
	}


	public Issuer getIssuer() {
		return issuer;
	}


	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}


	public Participant getParticipant() {
		return participant;
	}


	public void setParticipant(Participant participant) {
		this.participant = participant;
	}


	public BigDecimal getGrossAmount() {
		return grossAmount;
	}


	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}


	public BigDecimal getNetAmount() {
		return netAmount;
	}


	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}


	public BigDecimal getNetTax() {
		return netTax;
	}


	public void setNetTax(BigDecimal netTax) {
		this.netTax = netTax;
	}


	public Integer getLastModifyApp() {
		return lastModifyApp;
	}


	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}


	public Date getLastModifyDate() {
		return lastModifyDate;
	}


	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}


	public String getLastModifyIp() {
		return lastModifyIp;
	}


	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	public String getLastModifyUser() {
		return lastModifyUser;
	}


	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}


	public List<BillingCalculationDetail> getBillingCalculationDetails() {
		return billingCalculationDetails;
	}


	public void setBillingCalculationDetails(
			List<BillingCalculationDetail> billingCalculationDetails) {
		this.billingCalculationDetails = billingCalculationDetails;
	}


	public BillingCalculationProcess getBillingCalculationProcess() {
		return billingCalculationProcess;
	}

	public void setBillingCalculationProcess(
			BillingCalculationProcess billingCalculationProcess) {
		this.billingCalculationProcess = billingCalculationProcess;
	}
	
	
	public Integer getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}

	public String getKeyEntityCollection() {
		return keyEntityCollection;
	}

	public void setKeyEntityCollection(String keyEntityCollection) {
		this.keyEntityCollection = keyEntityCollection;
	}
				

	public Integer getOtherEntityCountry() {
		return otherEntityCountry;
	}

	public void setOtherEntityCountry(Integer otherEntityCountry) {
		this.otherEntityCountry = otherEntityCountry;
	}

	public Integer getOtherEntityDepartment() {
		return otherEntityDepartment;
	}

	public void setOtherEntityDepartment(Integer otherEntityDepartment) {
		this.otherEntityDepartment = otherEntityDepartment;
	}

	public Integer getOtherEntityProvince() {
		return otherEntityProvince;
	}

	public void setOtherEntityProvince(Integer otherEntityProvince) {
		this.otherEntityProvince = otherEntityProvince;
	}

	public Integer getOtherEntityDistrict() {
		return otherEntityDistrict;
	}

	public void setOtherEntityDistrict(Integer otherEntityDistrict) {
		this.otherEntityDistrict = otherEntityDistrict;
	}

	public String getOtherEntityAddress() {
		return otherEntityAddress;
	}

	public void setOtherEntityAddress(String otherEntityAddress) {
		this.otherEntityAddress = otherEntityAddress;
	}

	public Integer getIndIntegratedBill() {
		return indIntegratedBill;
	}

	public void setIndIntegratedBill(Integer indIntegratedBill) {
		this.indIntegratedBill = indIntegratedBill;
	}
	/**
	 * @return the countRequest
	 */
	public Integer getCountRequest() {
		return countRequest;
	}

	/**
	 * @param countRequest the countRequest to set
	 */
	public void setCountRequest(Integer countRequest) {
		this.countRequest = countRequest;
	}
	/**
	 * @return the requestConcatenated
	 */
	public String getRequestConcatenated() {
		return requestConcatenated;
	}

	/**
	 * @return the mnemonicConcatenated
	 */
	public String getMnemonicConcatenated() {
		return mnemonicConcatenated;
	}

	/**
	 * @param mnemonicConcatenated the mnemonicConcatenated to set
	 */
	public void setMnemonicConcatenated(String mnemonicConcatenated) {
		this.mnemonicConcatenated = mnemonicConcatenated;
	}

	/**
	 * @param requestConcatenated the requestConcatenated to set
	 */
	public void setRequestConcatenated(String requestConcatenated) {
		this.requestConcatenated = requestConcatenated;
	}
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}



	@Override
	 public Map<String, List<? extends Auditable>> getListForAudit() {
	  HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();
	        detailsMap.put("BillingCalculationDetail", billingCalculationDetails);
	        return detailsMap;
	 }

		
}