package com.pradera.model.security.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SystemType {
	
	SECURITY(Integer.valueOf(1),"SEGURIDAD"),
	
	CSDCORE(Integer.valueOf(2),"CSDCORE");
	
	private Integer code;
	
	private String value;

	/** The Constant list. */
	public static final List<SystemType> list = new ArrayList<SystemType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SystemType> lookup = new HashMap<Integer, SystemType>();

	static {
		for (SystemType s : EnumSet.allOf(SystemType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new state type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private SystemType(Integer code, String value) {
		this.code = code;
		this.value = value;	
	}
	

	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the state type
	 */
	public static SystemType get(Integer code) {
		return lookup.get(code);
	}
	
}
