package com.pradera.model.swift;


public enum SwiftMessageType {

	MESSAGE_103(Long.valueOf(103),"103"),
	MESSAGE_202(Long.valueOf(202),"202"),
	MESSAGE_900(Long.valueOf(900),"9XX");

	private Long code;
	private String description;

	private SwiftMessageType(Long code, String description){
		this.code = code;
		this.description = description;
	}

	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}