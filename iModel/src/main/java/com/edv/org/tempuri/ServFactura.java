
package com.edv.org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.edv.org.datacontract.schemas._2004._07.iedvservicios.ArrayOfFacturaC;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="facturaC" type="{http://schemas.datacontract.org/2004/07/IEDVServicios}ArrayOfFacturaC" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "facturaC"
})
@XmlRootElement(name = "ServFactura")
public class ServFactura {

    @XmlElementRef(name = "facturaC", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFacturaC> facturaC;

    /**
     * Obtiene el valor de la propiedad facturaC.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFacturaC }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFacturaC> getFacturaC() {
        return facturaC;
    }

    /**
     * Define el valor de la propiedad facturaC.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFacturaC }{@code >}
     *     
     */
    public void setFacturaC(JAXBElement<ArrayOfFacturaC> value) {
        this.facturaC = value;
    }

}
