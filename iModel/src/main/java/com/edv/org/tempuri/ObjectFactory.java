
package com.edv.org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.edv.org.datacontract.schemas._2004._07.iedvservicios.ArrayOfAsientoC;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.ArrayOfFacturaC;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.EIRESAS;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServAsientoResponseServAsientoResult_QNAME = new QName("http://tempuri.org/", "ServAsientoResult");
    private final static QName _ServAsientoAsientoC_QNAME = new QName("http://tempuri.org/", "asientoC");
    private final static QName _ServFacturaResponseServFacturaResult_QNAME = new QName("http://tempuri.org/", "ServFacturaResult");
    private final static QName _ServFacturaFacturaC_QNAME = new QName("http://tempuri.org/", "facturaC");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServFactura }
     * 
     */
    public ServFactura createServFactura() {
        return new ServFactura();
    }

    /**
     * Create an instance of {@link ServAsientoResponse }
     * 
     */
    public ServAsientoResponse createServAsientoResponse() {
        return new ServAsientoResponse();
    }

    /**
     * Create an instance of {@link ServFacturaResponse }
     * 
     */
    public ServFacturaResponse createServFacturaResponse() {
        return new ServFacturaResponse();
    }

    /**
     * Create an instance of {@link ServAsiento }
     * 
     */
    public ServAsiento createServAsiento() {
        return new ServAsiento();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EIRESAS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ServAsientoResult", scope = ServAsientoResponse.class)
    public JAXBElement<EIRESAS> createServAsientoResponseServAsientoResult(EIRESAS value) {
        return new JAXBElement<EIRESAS>(_ServAsientoResponseServAsientoResult_QNAME, EIRESAS.class, ServAsientoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAsientoC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "asientoC", scope = ServAsiento.class)
    public JAXBElement<ArrayOfAsientoC> createServAsientoAsientoC(ArrayOfAsientoC value) {
        return new JAXBElement<ArrayOfAsientoC>(_ServAsientoAsientoC_QNAME, ArrayOfAsientoC.class, ServAsiento.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EIRESAS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ServFacturaResult", scope = ServFacturaResponse.class)
    public JAXBElement<EIRESAS> createServFacturaResponseServFacturaResult(EIRESAS value) {
        return new JAXBElement<EIRESAS>(_ServFacturaResponseServFacturaResult_QNAME, EIRESAS.class, ServFacturaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFacturaC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "facturaC", scope = ServFactura.class)
    public JAXBElement<ArrayOfFacturaC> createServFacturaFacturaC(ArrayOfFacturaC value) {
        return new JAXBElement<ArrayOfFacturaC>(_ServFacturaFacturaC_QNAME, ArrayOfFacturaC.class, ServFactura.class, value);
    }

}
