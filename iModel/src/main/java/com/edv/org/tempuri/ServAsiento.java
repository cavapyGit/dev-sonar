
package com.edv.org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.edv.org.datacontract.schemas._2004._07.iedvservicios.ArrayOfAsientoC;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>S
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="asientoC" type="{http://schemas.datacontract.org/2004/07/IEDVServicios}ArrayOfAsientoC" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "asientoC"
})
@XmlRootElement(name = "ServAsiento")
public class ServAsiento {

    @XmlElementRef(name = "asientoC", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAsientoC> asientoC;

    /**
     * Obtiene el valor de la propiedad asientoC.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAsientoC }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAsientoC> getAsientoC() {
        return asientoC;
    }

    /**
     * Define el valor de la propiedad asientoC.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAsientoC }{@code >}
     *     
     */
    public void setAsientoC(JAXBElement<ArrayOfAsientoC> value) {
        this.asientoC = value;
    }

}
