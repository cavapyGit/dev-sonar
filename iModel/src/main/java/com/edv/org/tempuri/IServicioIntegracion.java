package com.edv.org.tempuri;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.edv.org.datacontract.schemas._2004._07.iedvservicios.ArrayOfAsientoC;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.ArrayOfFacturaC;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.EIRESAS;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IServicioIntegracion", targetNamespace = "http://tempuri.org/")
public interface IServicioIntegracion {


    /**
     * 
     * @param asientoC
     * @return
     *     returns org.datacontract.schemas._2004._07.iedvservicios.EIRESAS
     */
    @WebMethod(operationName = "ServAsiento", action = "http://tempuri.org/IServicioIntegracion/ServAsiento")
    @WebResult(name = "ServAsientoResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ServAsiento", targetNamespace = "http://tempuri.org/", className = "org.tempuri.ServAsiento")
    @ResponseWrapper(localName = "ServAsientoResponse", targetNamespace = "http://tempuri.org/", className = "org.tempuri.ServAsientoResponse")
    public EIRESAS servAsiento(
        @WebParam(name = "asientoC", targetNamespace = "http://tempuri.org/")
        ArrayOfAsientoC asientoC);

    /**
     * 
     * @param facturaC
     * @return
     *     returns org.datacontract.schemas._2004._07.iedvservicios.EIRESAS
     */
    @WebMethod(operationName = "ServFactura", action = "http://tempuri.org/IServicioIntegracion/ServFactura")
    @WebResult(name = "ServFacturaResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ServFactura", targetNamespace = "http://tempuri.org/", className = "org.tempuri.ServFactura")
    @ResponseWrapper(localName = "ServFacturaResponse", targetNamespace = "http://tempuri.org/", className = "org.tempuri.ServFacturaResponse")
    public EIRESAS servFactura(
        @WebParam(name = "facturaC", targetNamespace = "http://tempuri.org/")
        ArrayOfFacturaC facturaC);

}
