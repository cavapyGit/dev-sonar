
package com.edv.org.datacontract.schemas._2004._07.iedvservicios;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EIRESAS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EIRESAS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cnterr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="detalle" type="{http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades}ArrayOfEIRESAD" minOccurs="0"/>
 *         &lt;element name="idticket" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="msgrespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EIRESAS", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", propOrder = {
    "cnterr",
    "detalle",
    "idticket",
    "msgrespuesta"
})
public class EIRESAS {

    protected Integer cnterr;
    @XmlElementRef(name = "detalle", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfEIRESAD> detalle;
    protected Integer idticket;
    @XmlElementRef(name = "msgrespuesta", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", type = JAXBElement.class, required = false)
    protected JAXBElement<String> msgrespuesta;

    /**
     * Obtiene el valor de la propiedad cnterr.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCnterr() {
        return cnterr;
    }

    /**
     * Define el valor de la propiedad cnterr.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCnterr(Integer value) {
        this.cnterr = value;
    }

    /**
     * Obtiene el valor de la propiedad detalle.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEIRESAD }{@code >}
     *     
     */
    public JAXBElement<ArrayOfEIRESAD> getDetalle() {
        return detalle;
    }

    /**
     * Define el valor de la propiedad detalle.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEIRESAD }{@code >}
     *     
     */
    public void setDetalle(JAXBElement<ArrayOfEIRESAD> value) {
        this.detalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idticket.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdticket() {
        return idticket;
    }

    /**
     * Define el valor de la propiedad idticket.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdticket(Integer value) {
        this.idticket = value;
    }

    /**
     * Obtiene el valor de la propiedad msgrespuesta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMsgrespuesta() {
        return msgrespuesta;
    }

    /**
     * Define el valor de la propiedad msgrespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMsgrespuesta(JAXBElement<String> value) {
        this.msgrespuesta = value;
    }

}
