
package com.edv.org.datacontract.schemas._2004._07.iedvservicios;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FacturaC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FacturaC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CLIENTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CLIENTE_FACTURAR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CUENTA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DESCRIPCION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Detalle" type="{http://schemas.datacontract.org/2004/07/IEDVServicios}ArrayOfFacturaD" minOccurs="0"/>
 *         &lt;element name="FECOBRO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IMPUESTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MONEDA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NUMCXC" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NUMFACT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="REF_AUX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TIPOSERVICIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TOKKEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FacturaC", propOrder = {
    "cliente",
    "clientefacturar",
    "cuenta",
    "descripcion",
    "detalle",
    "fecobro",
    "impuesto",
    "moneda",
    "numcxc",
    "numfact",
    "refaux",
    "tiposervicio",
    "tokken"
})
public class FacturaC {

    @XmlElementRef(name = "CLIENTE", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cliente;
    @XmlElementRef(name = "CLIENTE_FACTURAR", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> clientefacturar;
    @XmlElementRef(name = "CUENTA", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cuenta;
    @XmlElementRef(name = "DESCRIPCION", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> descripcion;
    @XmlElementRef(name = "Detalle", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFacturaD> detalle;
    @XmlElementRef(name = "FECOBRO", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fecobro;
    @XmlElementRef(name = "IMPUESTO", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> impuesto;
    @XmlElementRef(name = "MONEDA", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> moneda;
    @XmlElement(name = "NUMCXC")
    protected Integer numcxc;
    @XmlElement(name = "NUMFACT")
    protected Integer numfact;
    @XmlElementRef(name = "REF_AUX", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> refaux;
    @XmlElementRef(name = "TIPOSERVICIO", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tiposervicio;
    @XmlElementRef(name = "TOKKEN", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tokken;

    /**
     * Obtiene el valor de la propiedad cliente.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCLIENTE() {
        return cliente;
    }

    /**
     * Define el valor de la propiedad cliente.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCLIENTE(JAXBElement<String> value) {
        this.cliente = value;
    }

    /**
     * Obtiene el valor de la propiedad clientefacturar.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCLIENTEFACTURAR() {
        return clientefacturar;
    }

    /**
     * Define el valor de la propiedad clientefacturar.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCLIENTEFACTURAR(JAXBElement<String> value) {
        this.clientefacturar = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCUENTA() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCUENTA(JAXBElement<String> value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcion.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDESCRIPCION() {
        return descripcion;
    }

    /**
     * Define el valor de la propiedad descripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDESCRIPCION(JAXBElement<String> value) {
        this.descripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad detalle.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFacturaD }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFacturaD> getDetalle() {
        return detalle;
    }

    /**
     * Define el valor de la propiedad detalle.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFacturaD }{@code >}
     *     
     */
    public void setDetalle(JAXBElement<ArrayOfFacturaD> value) {
        this.detalle = value;
    }

    /**
     * Obtiene el valor de la propiedad fecobro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFECOBRO() {
        return fecobro;
    }

    /**
     * Define el valor de la propiedad fecobro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFECOBRO(JAXBElement<String> value) {
        this.fecobro = value;
    }

    /**
     * Obtiene el valor de la propiedad impuesto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIMPUESTO() {
        return impuesto;
    }

    /**
     * Define el valor de la propiedad impuesto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIMPUESTO(JAXBElement<String> value) {
        this.impuesto = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMONEDA() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMONEDA(JAXBElement<String> value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad numcxc.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNUMCXC() {
        return numcxc;
    }

    /**
     * Define el valor de la propiedad numcxc.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNUMCXC(Integer value) {
        this.numcxc = value;
    }

    /**
     * Obtiene el valor de la propiedad numfact.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNUMFACT() {
        return numfact;
    }

    /**
     * Define el valor de la propiedad numfact.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNUMFACT(Integer value) {
        this.numfact = value;
    }

    /**
     * Obtiene el valor de la propiedad refaux.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getREFAUX() {
        return refaux;
    }

    /**
     * Define el valor de la propiedad refaux.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setREFAUX(JAXBElement<String> value) {
        this.refaux = value;
    }

    /**
     * Obtiene el valor de la propiedad tiposervicio.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTIPOSERVICIO() {
        return tiposervicio;
    }

    /**
     * Define el valor de la propiedad tiposervicio.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTIPOSERVICIO(JAXBElement<String> value) {
        this.tiposervicio = value;
    }

    /**
     * Obtiene el valor de la propiedad tokken.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTOKKEN() {
        return tokken;
    }

    /**
     * Define el valor de la propiedad tokken.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTOKKEN(JAXBElement<String> value) {
        this.tokken = value;
    }

}
