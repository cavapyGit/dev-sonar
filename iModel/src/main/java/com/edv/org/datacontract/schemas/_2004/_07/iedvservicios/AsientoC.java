
package com.edv.org.datacontract.schemas._2004._07.iedvservicios;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AsientoC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AsientoC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Detalle" type="{http://schemas.datacontract.org/2004/07/IEDVServicios}ArrayOfAsientoD" minOccurs="0"/>
 *         &lt;element name="FECHA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GLOSA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NUMCBTE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TIPO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TIPOCAMBIOCAB" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="TOKKEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AsientoC", propOrder = {
    "detalle",
    "fecha",
    "glosa",
    "numcbte",
    "tipo",
    "tipocambiocab",
    "tokken"
})
public class AsientoC {

    @XmlElementRef(name = "Detalle", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAsientoD> detalle;
    @XmlElementRef(name = "FECHA", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fecha;
    @XmlElementRef(name = "GLOSA", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> glosa;
    @XmlElement(name = "NUMCBTE")
    protected Integer numcbte;
    @XmlElementRef(name = "TIPO", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipo;
    @XmlElement(name = "TIPOCAMBIOCAB")
    protected Double tipocambiocab;
    @XmlElementRef(name = "TOKKEN", namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tokken;

    /**
     * Obtiene el valor de la propiedad detalle.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAsientoD }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAsientoD> getDetalle() {
        return detalle;
    }

    /**
     * Define el valor de la propiedad detalle.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAsientoD }{@code >}
     *     
     */
    public void setDetalle(JAXBElement<ArrayOfAsientoD> value) {
        this.detalle = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFECHA() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFECHA(JAXBElement<String> value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad glosa.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGLOSA() {
        return glosa;
    }

    /**
     * Define el valor de la propiedad glosa.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGLOSA(JAXBElement<String> value) {
        this.glosa = value;
    }

    /**
     * Obtiene el valor de la propiedad numcbte.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNUMCBTE() {
        return numcbte;
    }

    /**
     * Define el valor de la propiedad numcbte.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNUMCBTE(Integer value) {
        this.numcbte = value;
    }

    /**
     * Obtiene el valor de la propiedad tipo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTIPO() {
        return tipo;
    }

    /**
     * Define el valor de la propiedad tipo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTIPO(JAXBElement<String> value) {
        this.tipo = value;
    }

    /**
     * Obtiene el valor de la propiedad tipocambiocab.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTIPOCAMBIOCAB() {
        return tipocambiocab;
    }

    /**
     * Define el valor de la propiedad tipocambiocab.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTIPOCAMBIOCAB(Double value) {
        this.tipocambiocab = value;
    }

    /**
     * Obtiene el valor de la propiedad tokken.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTOKKEN() {
        return tokken;
    }

    /**
     * Define el valor de la propiedad tokken.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTOKKEN(JAXBElement<String> value) {
        this.tokken = value;
    }

}
