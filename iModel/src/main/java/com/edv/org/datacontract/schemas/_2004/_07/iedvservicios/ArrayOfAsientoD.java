
package com.edv.org.datacontract.schemas._2004._07.iedvservicios;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfAsientoD complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAsientoD">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AsientoD" type="{http://schemas.datacontract.org/2004/07/IEDVServicios}AsientoD" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAsientoD", propOrder = {
    "asientoD"
})
public class ArrayOfAsientoD {

    @XmlElement(name = "AsientoD", nillable = true)
    protected List<AsientoD> asientoD;

    /**
     * Gets the value of the asientoD property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the asientoD property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAsientoD().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AsientoD }
     * 
     * 
     */
    public List<AsientoD> getAsientoD() {
        if (asientoD == null) {
            asientoD = new ArrayList<AsientoD>();
        }
        return this.asientoD;
    }

}
