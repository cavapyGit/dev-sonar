
package com.edv.org.datacontract.schemas._2004._07.iedvservicios;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.iedvservicios package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FacturaD_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "FacturaD");
    private final static QName _EIRESAD_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", "EIRESAD");
    private final static QName _FacturaC_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "FacturaC");
    private final static QName _AsientoD_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "AsientoD");
    private final static QName _AsientoC_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "AsientoC");
    private final static QName _ArrayOfEIRESAD_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", "ArrayOfEIRESAD");
    private final static QName _ArrayOfFacturaD_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "ArrayOfFacturaD");
    private final static QName _ArrayOfFacturaC_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "ArrayOfFacturaC");
    private final static QName _EIRESAS_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", "EIRESAS");
    private final static QName _ArrayOfAsientoC_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "ArrayOfAsientoC");
    private final static QName _ArrayOfAsientoD_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "ArrayOfAsientoD");
    private final static QName _FacturaCMONEDA_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "MONEDA");
    private final static QName _FacturaCDetalle_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "Detalle");
    private final static QName _FacturaCDESCRIPCION_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "DESCRIPCION");
    private final static QName _FacturaCCUENTA_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "CUENTA");
    private final static QName _FacturaCREFAUX_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "REF_AUX");
    private final static QName _FacturaCTOKKEN_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "TOKKEN");
    private final static QName _FacturaCTIPOSERVICIO_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "TIPOSERVICIO");
    private final static QName _FacturaCCLIENTE_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "CLIENTE");
    private final static QName _FacturaCFECOBRO_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "FECOBRO");
    private final static QName _FacturaCIMPUESTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "IMPUESTO");
    private final static QName _FacturaCCLIENTEFACTURAR_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "CLIENTE_FACTURAR");
    private final static QName _FacturaDITEM_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "ITEM");
    private final static QName _AsientoDGLOSAPARTICULAR_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "GLOSAPARTICULAR");
    private final static QName _AsientoDOPERACION_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "OPERACION");
    private final static QName _EIRESADEstado_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", "estado");
    private final static QName _EIRESASDetalle_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", "detalle");
    private final static QName _EIRESASMsgrespuesta_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", "msgrespuesta");
    private final static QName _AsientoCGLOSA_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "GLOSA");
    private final static QName _AsientoCTIPO_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "TIPO");
    private final static QName _AsientoCFECHA_QNAME = new QName("http://schemas.datacontract.org/2004/07/IEDVServicios", "FECHA");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.iedvservicios
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfFacturaC }
     * 
     */
    public ArrayOfFacturaC createArrayOfFacturaC() {
        return new ArrayOfFacturaC();
    }

    /**
     * Create an instance of {@link EIRESAS }
     * 
     */
    public EIRESAS createEIRESAS() {
        return new EIRESAS();
    }

    /**
     * Create an instance of {@link ArrayOfAsientoC }
     * 
     */
    public ArrayOfAsientoC createArrayOfAsientoC() {
        return new ArrayOfAsientoC();
    }

    /**
     * Create an instance of {@link ArrayOfAsientoD }
     * 
     */
    public ArrayOfAsientoD createArrayOfAsientoD() {
        return new ArrayOfAsientoD();
    }

    /**
     * Create an instance of {@link FacturaC }
     * 
     */
    public FacturaC createFacturaC() {
        return new FacturaC();
    }

    /**
     * Create an instance of {@link FacturaD }
     * 
     */
    public FacturaD createFacturaD() {
        return new FacturaD();
    }

    /**
     * Create an instance of {@link AsientoD }
     * 
     */
    public AsientoD createAsientoD() {
        return new AsientoD();
    }

    /**
     * Create an instance of {@link AsientoC }
     * 
     */
    public AsientoC createAsientoC() {
        return new AsientoC();
    }

    /**
     * Create an instance of {@link ArrayOfFacturaD }
     * 
     */
    public ArrayOfFacturaD createArrayOfFacturaD() {
        return new ArrayOfFacturaD();
    }

    /**
     * Create an instance of {@link EIRESAD }
     * 
     */
    public EIRESAD createEIRESAD() {
        return new EIRESAD();
    }

    /**
     * Create an instance of {@link ArrayOfEIRESAD }
     * 
     */
    public ArrayOfEIRESAD createArrayOfEIRESAD() {
        return new ArrayOfEIRESAD();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacturaD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "FacturaD")
    public JAXBElement<FacturaD> createFacturaD(FacturaD value) {
        return new JAXBElement<FacturaD>(_FacturaD_QNAME, FacturaD.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EIRESAD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", name = "EIRESAD")
    public JAXBElement<EIRESAD> createEIRESAD(EIRESAD value) {
        return new JAXBElement<EIRESAD>(_EIRESAD_QNAME, EIRESAD.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacturaC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "FacturaC")
    public JAXBElement<FacturaC> createFacturaC(FacturaC value) {
        return new JAXBElement<FacturaC>(_FacturaC_QNAME, FacturaC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsientoD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "AsientoD")
    public JAXBElement<AsientoD> createAsientoD(AsientoD value) {
        return new JAXBElement<AsientoD>(_AsientoD_QNAME, AsientoD.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsientoC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "AsientoC")
    public JAXBElement<AsientoC> createAsientoC(AsientoC value) {
        return new JAXBElement<AsientoC>(_AsientoC_QNAME, AsientoC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEIRESAD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", name = "ArrayOfEIRESAD")
    public JAXBElement<ArrayOfEIRESAD> createArrayOfEIRESAD(ArrayOfEIRESAD value) {
        return new JAXBElement<ArrayOfEIRESAD>(_ArrayOfEIRESAD_QNAME, ArrayOfEIRESAD.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFacturaD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "ArrayOfFacturaD")
    public JAXBElement<ArrayOfFacturaD> createArrayOfFacturaD(ArrayOfFacturaD value) {
        return new JAXBElement<ArrayOfFacturaD>(_ArrayOfFacturaD_QNAME, ArrayOfFacturaD.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFacturaC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "ArrayOfFacturaC")
    public JAXBElement<ArrayOfFacturaC> createArrayOfFacturaC(ArrayOfFacturaC value) {
        return new JAXBElement<ArrayOfFacturaC>(_ArrayOfFacturaC_QNAME, ArrayOfFacturaC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EIRESAS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", name = "EIRESAS")
    public JAXBElement<EIRESAS> createEIRESAS(EIRESAS value) {
        return new JAXBElement<EIRESAS>(_EIRESAS_QNAME, EIRESAS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAsientoC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "ArrayOfAsientoC")
    public JAXBElement<ArrayOfAsientoC> createArrayOfAsientoC(ArrayOfAsientoC value) {
        return new JAXBElement<ArrayOfAsientoC>(_ArrayOfAsientoC_QNAME, ArrayOfAsientoC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAsientoD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "ArrayOfAsientoD")
    public JAXBElement<ArrayOfAsientoD> createArrayOfAsientoD(ArrayOfAsientoD value) {
        return new JAXBElement<ArrayOfAsientoD>(_ArrayOfAsientoD_QNAME, ArrayOfAsientoD.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "MONEDA", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCMONEDA(String value) {
        return new JAXBElement<String>(_FacturaCMONEDA_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFacturaD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "Detalle", scope = FacturaC.class)
    public JAXBElement<ArrayOfFacturaD> createFacturaCDetalle(ArrayOfFacturaD value) {
        return new JAXBElement<ArrayOfFacturaD>(_FacturaCDetalle_QNAME, ArrayOfFacturaD.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "DESCRIPCION", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCDESCRIPCION(String value) {
        return new JAXBElement<String>(_FacturaCDESCRIPCION_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "CUENTA", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCCUENTA(String value) {
        return new JAXBElement<String>(_FacturaCCUENTA_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "REF_AUX", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCREFAUX(String value) {
        return new JAXBElement<String>(_FacturaCREFAUX_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "TOKKEN", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCTOKKEN(String value) {
        return new JAXBElement<String>(_FacturaCTOKKEN_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "TIPOSERVICIO", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCTIPOSERVICIO(String value) {
        return new JAXBElement<String>(_FacturaCTIPOSERVICIO_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "CLIENTE", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCCLIENTE(String value) {
        return new JAXBElement<String>(_FacturaCCLIENTE_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "FECOBRO", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCFECOBRO(String value) {
        return new JAXBElement<String>(_FacturaCFECOBRO_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "IMPUESTO", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCIMPUESTO(String value) {
        return new JAXBElement<String>(_FacturaCIMPUESTO_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "CLIENTE_FACTURAR", scope = FacturaC.class)
    public JAXBElement<String> createFacturaCCLIENTEFACTURAR(String value) {
        return new JAXBElement<String>(_FacturaCCLIENTEFACTURAR_QNAME, String.class, FacturaC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "ITEM", scope = FacturaD.class)
    public JAXBElement<String> createFacturaDITEM(String value) {
        return new JAXBElement<String>(_FacturaDITEM_QNAME, String.class, FacturaD.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "CUENTA", scope = AsientoD.class)
    public JAXBElement<String> createAsientoDCUENTA(String value) {
        return new JAXBElement<String>(_FacturaCCUENTA_QNAME, String.class, AsientoD.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "GLOSAPARTICULAR", scope = AsientoD.class)
    public JAXBElement<String> createAsientoDGLOSAPARTICULAR(String value) {
        return new JAXBElement<String>(_AsientoDGLOSAPARTICULAR_QNAME, String.class, AsientoD.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "OPERACION", scope = AsientoD.class)
    public JAXBElement<String> createAsientoDOPERACION(String value) {
        return new JAXBElement<String>(_AsientoDOPERACION_QNAME, String.class, AsientoD.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", name = "estado", scope = EIRESAD.class)
    public JAXBElement<String> createEIRESADEstado(String value) {
        return new JAXBElement<String>(_EIRESADEstado_QNAME, String.class, EIRESAD.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEIRESAD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", name = "detalle", scope = EIRESAS.class)
    public JAXBElement<ArrayOfEIRESAD> createEIRESASDetalle(ArrayOfEIRESAD value) {
        return new JAXBElement<ArrayOfEIRESAD>(_EIRESASDetalle_QNAME, ArrayOfEIRESAD.class, EIRESAS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios.Entidades", name = "msgrespuesta", scope = EIRESAS.class)
    public JAXBElement<String> createEIRESASMsgrespuesta(String value) {
        return new JAXBElement<String>(_EIRESASMsgrespuesta_QNAME, String.class, EIRESAS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "GLOSA", scope = AsientoC.class)
    public JAXBElement<String> createAsientoCGLOSA(String value) {
        return new JAXBElement<String>(_AsientoCGLOSA_QNAME, String.class, AsientoC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAsientoD }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "Detalle", scope = AsientoC.class)
    public JAXBElement<ArrayOfAsientoD> createAsientoCDetalle(ArrayOfAsientoD value) {
        return new JAXBElement<ArrayOfAsientoD>(_FacturaCDetalle_QNAME, ArrayOfAsientoD.class, AsientoC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "TOKKEN", scope = AsientoC.class)
    public JAXBElement<String> createAsientoCTOKKEN(String value) {
        return new JAXBElement<String>(_FacturaCTOKKEN_QNAME, String.class, AsientoC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "TIPO", scope = AsientoC.class)
    public JAXBElement<String> createAsientoCTIPO(String value) {
        return new JAXBElement<String>(_AsientoCTIPO_QNAME, String.class, AsientoC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IEDVServicios", name = "FECHA", scope = AsientoC.class)
    public JAXBElement<String> createAsientoCFECHA(String value) {
        return new JAXBElement<String>(_AsientoCFECHA_QNAME, String.class, AsientoC.class, value);
    }

}
