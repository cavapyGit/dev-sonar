package com.edv.collection.economic.rigth;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "DETAIL_COLLECTION_GLOSS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailCollectionGloss.findAll", query = "SELECT d FROM DetailCollectionGloss d"),
    @NamedQuery(name = "DetailCollectionGloss.findByIdDetailCollectionGlossPk", query = "SELECT d FROM DetailCollectionGloss d WHERE d.idDetailCollectionGlossPk = :idDetailCollectionGlossPk"),
    @NamedQuery(name = "DetailCollectionGloss.findByIdParticipantPk", query = "SELECT d FROM DetailCollectionGloss d WHERE d.idParticipantPk = :idParticipantPk"),
    @NamedQuery(name = "DetailCollectionGloss.findByGlosaNotClassified", query = "SELECT d FROM DetailCollectionGloss d WHERE d.glosaNotClassified = :glosaNotClassified"),
    @NamedQuery(name = "DetailCollectionGloss.findByAmountReceivable", query = "SELECT d FROM DetailCollectionGloss d WHERE d.amountReceivable = :amountReceivable"),
    @NamedQuery(name = "DetailCollectionGloss.findByCurrency", query = "SELECT d FROM DetailCollectionGloss d WHERE d.currency = :currency"),
    @NamedQuery(name = "DetailCollectionGloss.findByRegistreDate", query = "SELECT d FROM DetailCollectionGloss d WHERE d.registreDate = :registreDate"),
    @NamedQuery(name = "DetailCollectionGloss.findByDepositDate", query = "SELECT d FROM DetailCollectionGloss d WHERE d.depositDate = :depositDate"),
    @NamedQuery(name = "DetailCollectionGloss.findByRetirementDate", query = "SELECT d FROM DetailCollectionGloss d WHERE d.retirementDate = :retirementDate"),
    @NamedQuery(name = "DetailCollectionGloss.findByLastModifyUser", query = "SELECT d FROM DetailCollectionGloss d WHERE d.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "DetailCollectionGloss.findByLastModifyDate", query = "SELECT d FROM DetailCollectionGloss d WHERE d.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "DetailCollectionGloss.findByLastModifyIp", query = "SELECT d FROM DetailCollectionGloss d WHERE d.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "DetailCollectionGloss.findByLastModifyApp", query = "SELECT d FROM DetailCollectionGloss d WHERE d.lastModifyApp = :lastModifyApp")})
public class DetailCollectionGloss implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id  
    @SequenceGenerator(name="DETAIL_COLLECTION_GENERATOR", sequenceName="SQ_ID_DETAIL_COLLECTION_GLOSS",initialValue=1,allocationSize=1)
  	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DETAIL_COLLECTION_GENERATOR")
    @Column(name = "ID_DETAIL_COLLECTION_GLOSS_PK")
    private Long idDetailCollectionGlossPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PARTICIPANT_PK")
    private Long idParticipantPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PAYMENT_AGENT_FK")
    private Long idPaymentAgentFk;
    @Transient
    private String paymentAgentDesc; 
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_HOLDER_FK")
    private Long idHolderFk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "GLOSA_NOT_CLASSIFIED")
    private String glosaNotClassified;
    /*Monto depositado*/
    @Basic(optional = false)
    @Column(name = "AMOUNT_DEPOSITED")
    private BigDecimal amountDeposited;
    /*Monto depositado*/
    @Basic(optional = false)
    @Column(name = "AMOUNT_TRANSFERRED")
    private BigDecimal amountTransferred;
    /**Monto a cobrar*/
    @Basic(optional = false)
    @NotNull
    @Column(name = "AMOUNT_RECEIVABLE")
    private BigDecimal amountReceivable;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CURRENCY")
    private Integer currency;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SECURITY_CLASS")
    private Integer securityClass;
    @Column(name = "TRANSFER_STATUS")
    private Integer transferStatus;
    @Transient
    private String descTransferStatus; 
    @Transient
    private String currencyDesc;
    @Column(name = "DEPOSIT_STATUS")
    private Integer depositStatus;
    @Column(name = "SOURCE_DEPOSIT")
    private Integer sourceDeposit;
    @Transient
    private String descDepositStatus;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registreDate;
    @Basic(optional = false)
    @Column(name = "DEPOSIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date depositDate;
    @Basic(optional = false)
    @Column(name = "RETIREMENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date retirementDate;
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;
	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;
	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Size(min = 1, max = 1500)
    @Column(name = "LST_ID_CATS")
    private String idCatListaText;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PAYMENT_DATE")
	private Date paymentDate;
	
	@Column(name = "ID_ISSUER_FK")
    private String idIssuerFk;
	
	/*Banco por el cual nos deposito*/
	@Column(name = "ID_BANK_FK")
	private Long idBankPk;
	/**Nuemero de cuenta en la cual fue ejecutada el deposito*/
	
	@Column(name = "ACCOUNT_NUMBER_DEPOSIT")
	private String accountNumbderDeposit;
    
	@Lob()
   	@Column(name="FILE_AUX", updatable=true)
   	private byte[] fileAux;
	
	@Column(name="FILE_AUX_NAME")
	private String fileAuxName;
	
	
	public DetailCollectionGloss() {
		// TODO Auto-generated constructor stub
	}
	public DetailCollectionGloss(Long idDetailCollectionGlossPk) {
		this.idDetailCollectionGlossPk = idDetailCollectionGlossPk;
	}
	public Long getIdDetailCollectionGlossPk() {
		return idDetailCollectionGlossPk;
	}
	public void setIdDetailCollectionGlossPk(Long idDetailCollectionGlossPk) {
		this.idDetailCollectionGlossPk = idDetailCollectionGlossPk;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Long getIdPaymentAgentFk() {
		return idPaymentAgentFk;
	}
	public void setIdPaymentAgentFk(Long idPaymentAgentFk) {
		this.idPaymentAgentFk = idPaymentAgentFk;
	}
	public String getPaymentAgentDesc() {
		return paymentAgentDesc;
	}
	public void setPaymentAgentDesc(String paymentAgentDesc) {
		this.paymentAgentDesc = paymentAgentDesc;
	}
	public Long getIdHolderFk() {
		return idHolderFk;
	}
	public void setIdHolderFk(Long idHolderFk) {
		this.idHolderFk = idHolderFk;
	}
	public String getGlosaNotClassified() {
		return glosaNotClassified;
	}
	public void setGlosaNotClassified(String glosaNotClassified) {
		this.glosaNotClassified = glosaNotClassified;
	}
	public BigDecimal getAmountDeposited() {
		return amountDeposited;
	}
	public void setAmountDeposited(BigDecimal amountDeposited) {
		this.amountDeposited = amountDeposited;
	}
	public BigDecimal getAmountTransferred() {
		return amountTransferred;
	}
	public void setAmountTransferred(BigDecimal amountTransferred) {
		this.amountTransferred = amountTransferred;
	}
	public BigDecimal getAmountReceivable() {
		return amountReceivable;
	}
	public void setAmountReceivable(BigDecimal amountReceivable) {
		this.amountReceivable = amountReceivable;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public Integer getTransferStatus() {
		return transferStatus;
	}
	public void setTransferStatus(Integer transferStatus) {
		this.transferStatus = transferStatus;
	}
	public String getDescTransferStatus() {
		return descTransferStatus;
	}
	public void setDescTransferStatus(String descTransferStatus) {
		this.descTransferStatus = descTransferStatus;
	}
	public String getCurrencyDesc() {
		return currencyDesc;
	}
	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}
	public Integer getDepositStatus() {
		return depositStatus;
	}
	public void setDepositStatus(Integer depositStatus) {
		this.depositStatus = depositStatus;
	}
	public Integer getSourceDeposit() {
		return sourceDeposit;
	}
	public void setSourceDeposit(Integer sourceDeposit) {
		this.sourceDeposit = sourceDeposit;
	}
	public String getDescDepositStatus() {
		return descDepositStatus;
	}
	public void setDescDepositStatus(String descDepositStatus) {
		this.descDepositStatus = descDepositStatus;
	}
	public Date getRegistreDate() {
		return registreDate;
	}
	public void setRegistreDate(Date registreDate) {
		this.registreDate = registreDate;
	}
	public Date getDepositDate() {
		return depositDate;
	}
	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}
	public Date getRetirementDate() {
		return retirementDate;
	}
	public void setRetirementDate(Date retirementDate) {
		this.retirementDate = retirementDate;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public String getIdCatListaText() {
		return idCatListaText;
	}
	public void setIdCatListaText(String idCatListaText) {
		this.idCatListaText = idCatListaText;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getIdIssuerFk() {
		return idIssuerFk;
	}
	public void setIdIssuerFk(String idIssuerFk) {
		this.idIssuerFk = idIssuerFk;
	}
	public Long getIdBankPk() {
		return idBankPk;
	}
	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}
	public String getAccountNumbderDeposit() {
		return accountNumbderDeposit;
	}
	public void setAccountNumbderDeposit(String accountNumbderDeposit) {
		this.accountNumbderDeposit = accountNumbderDeposit;
	}
	public byte[] getFileAux() {
		return fileAux;
	}
	public void setFileAux(byte[] fileAux) {
		this.fileAux = fileAux;
	}
	public String getFileAuxName() {
		return fileAuxName;
	}
	public void setFileAuxName(String fileAuxName) {
		this.fileAuxName = fileAuxName;
	}
	
}
