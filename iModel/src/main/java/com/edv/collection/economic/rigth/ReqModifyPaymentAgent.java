package com.edv.collection.economic.rigth;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "REQ_MODIFY_PAYMENT_AGENT")
public class ReqModifyPaymentAgent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
   	@SequenceGenerator(name="MODIFY_PAYMENT_AGENT_GENERATOR", sequenceName="SQ_ID_REQ_MOD_PAY_AGENT_PK",initialValue=1,allocationSize=1)
   	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MODIFY_PAYMENT_AGENT_GENERATOR")
    @NotNull
    @Column(name = "ID_REQ_MOD_PAY_AGENT_PK")
    private Long idReqModPayAgentPk;
    @Column(name = "ID_PAYMENT_AGENT_FK")
    private long idPaymentAgentFk;
    @Column(name = "TYPE_PAYMENT_AGENT")
    private Integer typePaymentAgent;
    @Size(max = 10)
    @Column(name = "MNEMONIC")
    private String mnemonic;
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 30)
    @Column(name = "NAME")
    private String name;
    @Size(max = 30)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 30)
    @Column(name = "LAST_NAME")
    private String lastName;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 20)
    @Column(name = "CELL_PHONE")
    private String cellPhone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 20)
    @Column(name = "PHONE")
    private String phone;
    @Size(max = 8)
    @Column(name = "INTERN")
    private String intern;
    @Size(max = 20)
    @Column(name = "JOB")
    private String job;
    @Size(max = 60)
    @Column(name = "SECURITY_CLASS_LIST")
    private String securityClassList;
    @Column(name = "REGISTRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registreDate;
    @Column(name = "STATE")
    private Integer state;
    @Column(name = "SITUATION")
    private Integer situation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP")
    private long lastModifyApp;
    
    @Column(name = "IND_LIP")
    private Integer indLip;
    
    @Column(name = "IND_SEND_URL_PAYMENT")
    private Integer indSendUrlPayment;
    
    @Column(name = "ENCRYPT")
    private Integer encryp;
    
    /**Certificado*/
	@Lob()
   	@Column(name="CERTIFICATE", updatable=true)
   	private byte[] certificate;
	
	@Column(name="CERTIFICATE_NAME")
	private String certificateName;


    public ReqModifyPaymentAgent() {
    	name=firstName=lastName=cellPhone=email=intern=job=phone="";
    }

    public ReqModifyPaymentAgent(Long idReqModPayAgentPk) {
        this.idReqModPayAgentPk = idReqModPayAgentPk;
    }

    public ReqModifyPaymentAgent(Long idReqModPayAgentPk, long idPaymentAgentFk, String name, String firstName, String email, String securityClassList, String lastModifyUser, Date lastModifyDate, String lastModifyIp, long lastModifyApp) {
        this.idReqModPayAgentPk = idReqModPayAgentPk;
        this.idPaymentAgentFk = idPaymentAgentFk;
        this.name = name;
        this.firstName = firstName;
        this.email = email;
        this.securityClassList = securityClassList;
        this.lastModifyUser = lastModifyUser;
        this.lastModifyDate = lastModifyDate;
        this.lastModifyIp = lastModifyIp;
        this.lastModifyApp = lastModifyApp;
    }

    public Long getIdReqModPayAgentPk() {
        return idReqModPayAgentPk;
    }

    public void setIdReqModPayAgentPk(Long idReqModPayAgentPk) {
        this.idReqModPayAgentPk = idReqModPayAgentPk;
    }

    public long getIdPaymentAgentFk() {
		return idPaymentAgentFk;
	}

	public Integer getIndSendUrlPayment() {
		return indSendUrlPayment;
	}

	public void setIndSendUrlPayment(Integer indSendUrlPayment) {
		this.indSendUrlPayment = indSendUrlPayment;
	}

	public Integer getIndLip() {
		return indLip;
	}

	public void setIndLip(Integer indLip) {
		this.indLip = indLip;
	}

	public void setIdPaymentAgentFk(long idPaymentAgentFk) {
		this.idPaymentAgentFk = idPaymentAgentFk;
	}

	public Integer getTypePaymentAgent() {
		return typePaymentAgent;
	}

	public void setTypePaymentAgent(Integer typePaymentAgent) {
		this.typePaymentAgent = typePaymentAgent;
	}


	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIntern() {
        return intern;
    }

    public void setIntern(String intern) {
        this.intern = intern;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getSecurityClassList() {
        return securityClassList;
    }

    public void setSecurityClassList(String securityClassList) {
        this.securityClassList = securityClassList;
    }

    public Date getRegistreDate() {
        return registreDate;
    }
    public void setRegistreDate(Date registreDate) {
        this.registreDate = registreDate;
    }

    public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getSituation() {
		return situation;
	}

	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReqModPayAgentPk != null ? idReqModPayAgentPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReqModifyPaymentAgent)) {
            return false;
        }
        ReqModifyPaymentAgent other = (ReqModifyPaymentAgent) object;
        if ((this.idReqModPayAgentPk == null && other.idReqModPayAgentPk != null) || (this.idReqModPayAgentPk != null && !this.idReqModPayAgentPk.equals(other.idReqModPayAgentPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.mavenproject1.ReqModifyPaymentAgent[ idReqModPayAgentPk=" + idReqModPayAgentPk + " ]";
    }

	public byte[] getCertificate() {
		return certificate;
	}

	public void setCertificate(byte[] certificate) {
		this.certificate = certificate;
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public Integer getEncryp() {
		return encryp;
	}

	public void setEncryp(Integer encryp) {
		this.encryp = encryp;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
