package com.edv.collection.economic.rigth;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "BENEFICIARY_BANK_ACCOUNT")
public class BeneficiaryBankAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="BANK_ACCOUNT_GENERATOR", sequenceName="SQ_ID_BEN_BANK_ACCOUNT_PK",initialValue=1,allocationSize=1)
  	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BANK_ACCOUNT_GENERATOR")
    @NotNull
    @Column(name = "ID_BEN_BANK_ACCOUNT_PK")
    private Long idBenBankAccountPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PARTICIPANT_FK")
    private long idParticipantFk;
    
    @Transient
    private String participantDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_HOLDER_FK")
    private Long idHolderFk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACCOUNT_NUMBER")
    private Integer accountNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_BANK_FK")
    private Long idBankFk;
    @Transient
    private String fullName;
    @Transient
    private String bankDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACCOUNT_TYPE")
    private Integer accountType;
    @Transient
    private String accountTypeDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CURRENCY")
    private Integer currency;
    @Size(max = 20)
    @Column(name = "DEP_ACCOUNT_NUMBER")
    private String depAccountNumber;//numero de cuenta para deposito
    @Basic(optional = false)
    @NotNull
    @Column(name = "REGISTRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registreDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE")
    private Integer state;
    @Transient
    private String stateDesc;
    @Lob()
    @Basic(fetch=FetchType.EAGER)
   	@Column(name="DOCUMENT_FILE")
   	private byte[] documentFile;
	@Column(name="FILE_NAME")
	private String fileName;
	/**Razon y archivo de bloqueo*/
	@Lob()
    @Basic(fetch=FetchType.EAGER)
   	@Column(name="DOCUMENT_LOCK_FILE")
   	private byte[] documentLockFile;
	@Column(name="LOCK_FILE_NAME")
	private String lockFileName;
	
	@Column(name="BLOCKING_REASON")
	private String blockingReason;
	
    /** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;
	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;
	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;

    public BeneficiaryBankAccount() {
    }

	public Long getIdBenBankAccountPk() {
		return idBenBankAccountPk;
	}

	public void setIdBenBankAccountPk(Long idBenBankAccountPk) {
		this.idBenBankAccountPk = idBenBankAccountPk;
	}

	public long getIdParticipantFk() {
		return idParticipantFk;
	}

	public void setIdParticipantFk(long idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}

	public String getParticipantDesc() {
		return participantDesc;
	}

	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}

	public Long getIdHolderFk() {
		return idHolderFk;
	}

	public void setIdHolderFk(Long idHolderFk) {
		this.idHolderFk = idHolderFk;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Long getIdBankFk() {
		return idBankFk;
	}

	public void setIdBankFk(Long idBankFk) {
		this.idBankFk = idBankFk;
	}

	public String getBankDesc() {
		return bankDesc;
	}

	public void setBankDesc(String bankDesc) {
		this.bankDesc = bankDesc;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public String getAccountTypeDesc() {
		return accountTypeDesc;
	}

	public void setAccountTypeDesc(String accountTypeDesc) {
		this.accountTypeDesc = accountTypeDesc;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getDepAccountNumber() {
		return depAccountNumber;
	}

	public void setDepAccountNumber(String depAccountNumber) {
		this.depAccountNumber = depAccountNumber;
	}

	public Date getRegistreDate() {
		return registreDate;
	}

	public void setRegistreDate(Date registreDate) {
		this.registreDate = registreDate;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public byte[] getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public byte[] getDocumentLockFile() {
		return documentLockFile;
	}

	public void setDocumentLockFile(byte[] documentLockFile) {
		this.documentLockFile = documentLockFile;
	}

	public String getLockFileName() {
		return lockFileName;
	}

	public void setLockFileName(String lockFileName) {
		this.lockFileName = lockFileName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBlockingReason() {
		return blockingReason;
	}

	public void setBlockingReason(String blockingReason) {
		this.blockingReason = blockingReason;
	}
}
