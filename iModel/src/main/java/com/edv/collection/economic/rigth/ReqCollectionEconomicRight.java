package com.edv.collection.economic.rigth;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.model.accounts.Participant;

@Entity
@Table(name = "REQ_COLLECTION_ECONOMIC_RIGHT")
public class ReqCollectionEconomicRight implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id  
    @SequenceGenerator(name="REQUEST_COLLECTION_RIGHT_GENERATOR", sequenceName="SQ_REQ_COLLECTION_ECONOMIC_PK",initialValue=1,allocationSize=1)
  	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REQUEST_COLLECTION_RIGHT_GENERATOR")
    @Column(name = "ID_REQ_COLLECTION_ECONOMIC_PK")
    private Long idReqCollectionEconomicPk;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PARTICIPANT_FK")
    private Participant participant;
    @Transient
    private String participantDesc;
    @Column(name = "INSTRUMENT_TYPE")
    private Integer instrumentType;
    @Transient
    private String instrumentTypeDesc;
    @Column(name = "ID_ISSUER_FK")
    private String idIssuerFk;
    @Column(name = "SECURITY_CLASS")
    private Integer securityClass;
    @Column(name = "ID_SECURITY_CODE_FK")
    private String idSecurityCodeFk;
    @Column(name = "CURRENCY")
    private Integer currency;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNT_SECURITIES") // suma totall de valores de todos los cobros que corresponden a la solicitud
    private Integer amountSecurities;
    @Column(name = "PROCESS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processDate;
    
    @Column(name = "INITIAL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date initialDate;
    
    @Column(name = "FINAL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finalDate;
    
    @Column(name = "STATE")
    private Integer state;
    @Transient
    private String stateDesc;
    @Transient
    private String submitDesc;
    @Column(name = "SUBMIT")
    private Integer submit;
    /** The last modify app. */
  	@Column(name = "LAST_MODIFY_APP")
  	private Integer lastModifyApp;
  	@Temporal(TemporalType.TIMESTAMP)
  	@Column(name = "LAST_MODIFY_DATE")
  	private Date lastModifyDate;
  	/** The last modify ip. */
  	@Column(name = "LAST_MODIFY_IP")
  	private String lastModifyIp;
  	/** The last modify user. */
  	@Column(name = "LAST_MODIFY_USER")
  	private String lastModifyUser;
  	
  	public ReqCollectionEconomicRight() {
	}
  	
  	public ReqCollectionEconomicRight(Long idReqCollectionEconomicPk,Participant participant,String idIssuerFk){
  		this.idReqCollectionEconomicPk = idReqCollectionEconomicPk;
  		this.participant = participant;
  		this.idIssuerFk = idIssuerFk;
  	}
    public ReqCollectionEconomicRight(Long idReqCollectionEconomicPk) {
    	this.idReqCollectionEconomicPk = idReqCollectionEconomicPk;
	}

	public Long getIdReqCollectionEconomicPk() {
		return idReqCollectionEconomicPk;
	}

	public void setIdReqCollectionEconomicPk(Long idReqCollectionEconomicPk) {
		this.idReqCollectionEconomicPk = idReqCollectionEconomicPk;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public String getParticipantDesc() {
		return participantDesc;
	}
	public void setParticipantDesc(String participantDesc) {
		this.participantDesc = participantDesc;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public String getInstrumentTypeDesc() {
		return instrumentTypeDesc;
	}
	public void setInstrumentTypeDesc(String instrumentTypeDesc) {
		this.instrumentTypeDesc = instrumentTypeDesc;
	}
	public Integer getAmountSecurities() {
		return amountSecurities;
	}
	public void setAmountSecurities(Integer amountSecurities) {
		this.amountSecurities = amountSecurities;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getStateDesc() {
		return stateDesc;
	}
	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}
	public String getSubmitDesc() {
		return submitDesc;
	}
	public void setSubmitDesc(String submitDesc) {
		this.submitDesc = submitDesc;
	}
	public Integer getSubmit() {
		return submit;
	}
	public void setSubmit(Integer submit) {
		this.submit = submit;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getIdIssuerFk() {
		return idIssuerFk;
	}

	public void setIdIssuerFk(String idIssuerFk) {
		this.idIssuerFk = idIssuerFk;
	}


	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}

	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
}