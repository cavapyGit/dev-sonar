package com.edv.collection.economic.rigth;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "COLLECTION_ECONOMIC_RIGHT")
public class CollectionEconomicRigth implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id  
    @SequenceGenerator(name="COLLECTION_RIGHT_GENERATOR", sequenceName="SQ_COLLECTION_ECONOMIC_PK",initialValue=1,allocationSize=1)
  	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COLLECTION_RIGHT_GENERATOR")
    @Column(name = "ID_COLLECTION_ECONOMIC_PK")
    private Long idCollectionEconomicPk;
    @Basic(optional = false)
    @Column(name = "ID_REQ_COLLECTION_ECONOMIC_FK")
    private Long idReqCollectionEconomicFk;
    /*Para RF es el id de PROGRAM INTEREST COUPON*/
    @Basic(optional = false)
    @Column(name = "ID_GENERAL_PURPOSE_PK")
    private Long idGeneralPurposePk;
    @Basic(optional = true)
    @Column(name = "ID_HOLDER_ACCOUNT_PK")
    private Long idHolderAccountPk;
    @Column(name = "DUE_DATE")
    @Temporal(TemporalType.DATE)
    private Date dueDate;
    @Column(name = "PAYMENT_DATE")
    @Temporal(TemporalType.DATE)
    private Date paymentDate;
    @Basic(optional = false)
    @Size(min = 1, max = 20)
    @Column(name = "ID_SECURITY_CODE_FK")
    private String idSecurityCodeFk;
    @Size(max = 20)
    @Column(name = "ALTERNATE_CODE")
    private String alternateCode;
    @Basic(optional = true)
    @Column(name = "ID_HOLDER_PK")
    private Long idHolderPk;
    @Column(name = "CURRENCY")
    private Integer currency;
    @Column(name = "STATE")
    private Integer state;
    @Column(name = "CHECK_CHARGE")
    private Integer checkCharge;
    @Column(name = "ACCOUNT_NUMBER")
    private Integer accountNumber;
    @Column(name="COUPON_NUMBER")
	private Integer couponNumber;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNT_SECURITIES")
    private BigDecimal amountSecurities;
    @Column(name = "INTEREST")
    private BigDecimal interest;
    @Column(name = "AMORTIZATION")
    private BigDecimal amortization;
    @Column(name = "AMOUNT_RECEIVABLE")
    private BigDecimal amountReceivable;
    @Column(name = "CHECK_OTHER")
    private Integer checkOther;
    @Column(name = "ANOTHER_CURENCY")
    private Integer anotherCurency;
    @Column(name = "ANOTHER_AMOUNT_RECEIVABLE")
    private BigDecimal anotherAmountReceivable;
    @Column(name = "ID_BANK_FK")
    private Long idBankFk;
    @Size(max = 20)
    @Column(name = "DEP_ACCOUNT_NUMBER")
    private String depAccountNumber;
    
    @Size(max = 50)
    @Column(name = "GLOSA_NOT_CLASSIFIED")
    private String glosaNotClassified;
    @Column(name = "REGISTRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registreDate;
//    @Column(name = "INSTRUMENT_TYPE")
//    private Integer instrumentType;
    @Column(name = "NOTIFICATION_READING")
    private Integer notificationReading;
    /** The last modify app. */
	@Column(name = "LAST_MODIFY_APP")
	private Integer lastModifyApp;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFY_DATE")
	private Date lastModifyDate;
	/** The last modify ip. */
	@Column(name = "LAST_MODIFY_IP")
	private String lastModifyIp;
	/** The last modify user. */
	@Column(name = "LAST_MODIFY_USER")
	private String lastModifyUser;
	
	public CollectionEconomicRigth(
			Long idCollectionEconomicPk,
			Date paymentDate,
			Integer currency,
			BigDecimal amountReceivable,
			Long idBankFk,
			String depAccountNumber) {
		this.idCollectionEconomicPk =  idCollectionEconomicPk;
		this.paymentDate = paymentDate;
		this.currency = currency;
		this.amountReceivable = amountReceivable;
		this.idBankFk = idBankFk;
		this.depAccountNumber = depAccountNumber;
	}
	public CollectionEconomicRigth(
			Long idCollectionEconomicPk,
			Date paymentDate,
			Date dueDate,
			Integer checkOther,
			Integer currency,
			Integer anotherCurency,
			BigDecimal amountReceivable,
			BigDecimal anotherAmountReceivable,
			Long idBankFk,
			String depAccountNumber) {
		this.idCollectionEconomicPk =  idCollectionEconomicPk;
		this.paymentDate = paymentDate;
		this.dueDate = dueDate;
		this.checkOther = checkOther;
		this.currency = currency;
		this.anotherCurency = anotherCurency;
		this.amountReceivable = amountReceivable;
		this.anotherAmountReceivable = anotherAmountReceivable;
		this.idBankFk = idBankFk;
		this.depAccountNumber = depAccountNumber;
	}
	
	public CollectionEconomicRigth() {
	}
	
	public CollectionEconomicRigth(Long idCollectionEconomicPk,Long idCorporativeOperationFk,Integer currency) {
		this.idCollectionEconomicPk = idCollectionEconomicPk;
		this.currency = currency;
	}
	public Long getIdCollectionEconomicPk() {
		return idCollectionEconomicPk;
	}
	public void setIdCollectionEconomicPk(Long idCollectionEconomicPk) {
		this.idCollectionEconomicPk = idCollectionEconomicPk;
	}
	public Long getIdReqCollectionEconomicFk() {
		return idReqCollectionEconomicFk;
	}
	public void setIdReqCollectionEconomicFk(Long idReqCollectionEconomicFk) {
		this.idReqCollectionEconomicFk = idReqCollectionEconomicFk;
	}
	public Long getIdGeneralPurposePk() {
		return idGeneralPurposePk;
	}
	public void setIdGeneralPurposePk(Long idGeneralPurposePk) {
		this.idGeneralPurposePk = idGeneralPurposePk;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}
	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}
	public String getAlternateCode() {
		return alternateCode;
	}
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getCheckCharge() {
		return checkCharge;
	}
	public void setCheckCharge(Integer checkCharge) {
		this.checkCharge = checkCharge;
	}
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Integer getCouponNumber() {
		return couponNumber;
	}
	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}
	public BigDecimal getAmountSecurities() {
		return amountSecurities;
	}
	public void setAmountSecurities(BigDecimal amountSecurities) {
		this.amountSecurities = amountSecurities;
	}
	public BigDecimal getInterest() {
		return interest;
	}
	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}
	public BigDecimal getAmortization() {
		return amortization;
	}
	public void setAmortization(BigDecimal amortization) {
		this.amortization = amortization;
	}
	public BigDecimal getAmountReceivable() {
		return amountReceivable;
	}
	public void setAmountReceivable(BigDecimal amountReceivable) {
		this.amountReceivable = amountReceivable;
	}
	public Integer getCheckOther() {
		return checkOther;
	}
	public void setCheckOther(Integer checkOther) {
		this.checkOther = checkOther;
	}
	public Integer getAnotherCurency() {
		return anotherCurency;
	}
	public void setAnotherCurency(Integer anotherCurency) {
		this.anotherCurency = anotherCurency;
	}
	public BigDecimal getAnotherAmountReceivable() {
		return anotherAmountReceivable;
	}
	public void setAnotherAmountReceivable(BigDecimal anotherAmountReceivable) {
		this.anotherAmountReceivable = anotherAmountReceivable;
	}
	public Long getIdBankFk() {
		return idBankFk;
	}
	public void setIdBankFk(Long idBankFk) {
		this.idBankFk = idBankFk;
	}
	public String getDepAccountNumber() {
		return depAccountNumber;
	}
	public void setDepAccountNumber(String depAccountNumber) {
		this.depAccountNumber = depAccountNumber;
	}
	public String getGlosaNotClassified() {
		return glosaNotClassified;
	}
	public void setGlosaNotClassified(String glosaNotClassified) {
		this.glosaNotClassified = glosaNotClassified;
	}
	public Date getRegistreDate() {
		return registreDate;
	}
	public void setRegistreDate(Date registreDate) {
		this.registreDate = registreDate;
	}
	public Integer getNotificationReading() {
		return notificationReading;
	}
	public void setNotificationReading(Integer notificationReading) {
		this.notificationReading = notificationReading;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
}