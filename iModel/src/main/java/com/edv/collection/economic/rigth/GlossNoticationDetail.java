package com.edv.collection.economic.rigth;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rlarico
 */
@Entity
@Table(name = "GLOSS_NOTICATION_DETAIL")
@NamedQueries({
    @NamedQuery(name = "GlossNoticationDetail.findAll", query = "SELECT g FROM GlossNoticationDetail g"),
    @NamedQuery(name = "GlossNoticationDetail.findByIdPkGlossNotification", query = "SELECT g FROM GlossNoticationDetail g WHERE g.idPkGlossNotification = :idPkGlossNotification"),
    @NamedQuery(name = "GlossNoticationDetail.findByRegistreDate", query = "SELECT g FROM GlossNoticationDetail g WHERE g.registreDate = :registreDate"),
    @NamedQuery(name = "GlossNoticationDetail.findByLastModifyUser", query = "SELECT g FROM GlossNoticationDetail g WHERE g.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "GlossNoticationDetail.findByLastModifyDate", query = "SELECT g FROM GlossNoticationDetail g WHERE g.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "GlossNoticationDetail.findByLastModifyIp", query = "SELECT g FROM GlossNoticationDetail g WHERE g.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "GlossNoticationDetail.findByLastModifyApp", query = "SELECT g FROM GlossNoticationDetail g WHERE g.lastModifyApp = :lastModifyApp")})
public class GlossNoticationDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name="SQ_ID_GLOSS_NOTIFICATION", sequenceName="SQ_ID_GLOSS_NOTIFICATION",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_ID_GLOSS_NOTIFICATION")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PK_GLOSS_NOTIFICATION", nullable = false, precision = 20, scale = 0)
    private BigDecimal idPkGlossNotification;
    @Column(name = "REGISTRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registreDate;
    @Size(max = 20)
    @Column(name = "LAST_MODIFY_USER", length = 20)
    private String lastModifyUser;
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Size(max = 20)
    @Column(name = "LAST_MODIFY_IP", length = 20)
    private String lastModifyIp;
    @Column(name = "LAST_MODIFY_APP")
    private Long lastModifyApp;
    @JoinColumn(name = "ID_FK_PAYMENT_AGENT", referencedColumnName = "ID_PAYMENT_AGENT_PK")
    @ManyToOne(fetch = FetchType.LAZY)
    private PaymentAgent idFkPaymentAgent;
    @JoinColumn(name = "ID_FK_GLOSS_CODE", referencedColumnName = "ID_DETAIL_COLLECTION_GLOSS_PK")
    @ManyToOne(fetch = FetchType.LAZY)
    private DetailCollectionGloss idFkGlossCode;
    
    @Column(name = "NOTIFICATION_SENT")
    private Integer notificationSent;
    
    @Column(name = "HTML_MESSAGE")
    private String htmlMessage;
    
    @Column(name = "HTML_MESSAGE_PAYMENT")
    private String htmlMessagePayment;
    
    @Lob()
    @Basic(fetch=FetchType.LAZY)
	@Column(name="GLOSS_PDF")
	private byte[] glossPdf;
    
    @Transient
    private boolean selected;

    public GlossNoticationDetail() {
    	//por defecto 0 tiene reenvio
    	notificationSent=Integer.valueOf(0);
    }

    public GlossNoticationDetail(BigDecimal idPkGlossNotification) {
        this.idPkGlossNotification = idPkGlossNotification;
    }

    public BigDecimal getIdPkGlossNotification() {
        return idPkGlossNotification;
    }

    public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public byte[] getGlossPdf() {
		return glossPdf;
	}

	public void setGlossPdf(byte[] glossPdf) {
		this.glossPdf = glossPdf;
	}

	public String getHtmlMessagePayment() {
		return htmlMessagePayment;
	}

	public void setHtmlMessagePayment(String htmlMessagePayment) {
		this.htmlMessagePayment = htmlMessagePayment;
	}

	public String getHtmlMessage() {
		return htmlMessage;
	}

	public void setHtmlMessage(String htmlMessage) {
		this.htmlMessage = htmlMessage;
	}

	public Integer getNotificationSent() {
		return notificationSent;
	}

	public void setNotificationSent(Integer notificationSent) {
		this.notificationSent = notificationSent;
	}

	public void setIdPkGlossNotification(BigDecimal idPkGlossNotification) {
        this.idPkGlossNotification = idPkGlossNotification;
    }

    public Date getRegistreDate() {
        return registreDate;
    }

    public void setRegistreDate(Date registreDate) {
        this.registreDate = registreDate;
    }

    public String getLastModifyUser() {
        return lastModifyUser;
    }

    public void setLastModifyUser(String lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public String getLastModifyIp() {
        return lastModifyIp;
    }

    public void setLastModifyIp(String lastModifyIp) {
        this.lastModifyIp = lastModifyIp;
    }

    public Long getLastModifyApp() {
        return lastModifyApp;
    }

    public void setLastModifyApp(Long lastModifyApp) {
        this.lastModifyApp = lastModifyApp;
    }

    public PaymentAgent getIdFkPaymentAgent() {
        return idFkPaymentAgent;
    }

    public void setIdFkPaymentAgent(PaymentAgent idFkPaymentAgent) {
        this.idFkPaymentAgent = idFkPaymentAgent;
    }

    public DetailCollectionGloss getIdFkGlossCode() {
        return idFkGlossCode;
    }

    public void setIdFkGlossCode(DetailCollectionGloss idFkGlossCode) {
        this.idFkGlossCode = idFkGlossCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPkGlossNotification != null ? idPkGlossNotification.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GlossNoticationDetail)) {
            return false;
        }
        GlossNoticationDetail other = (GlossNoticationDetail) object;
        if ((this.idPkGlossNotification == null && other.idPkGlossNotification != null) || (this.idPkGlossNotification != null && !this.idPkGlossNotification.equals(other.idPkGlossNotification))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.GlossNoticationDetail[ idPkGlossNotification=" + idPkGlossNotification + " ]";
    }
    
}


