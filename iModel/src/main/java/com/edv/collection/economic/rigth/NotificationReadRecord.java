package com.edv.collection.economic.rigth;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "NOTIFICATION_READ_RECORD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificationReadRecord.findAll", query = "SELECT n FROM NotificationReadRecord n"),
    @NamedQuery(name = "NotificationReadRecord.findByIdNotificationReadRecordPk", query = "SELECT n FROM NotificationReadRecord n WHERE n.idNotificationReadRecordPk = :idNotificationReadRecordPk"),
    @NamedQuery(name = "NotificationReadRecord.findByIdPaymetAgentFk", query = "SELECT n FROM NotificationReadRecord n WHERE n.idPaymetAgentFk = :idPaymetAgentFk"),
    @NamedQuery(name = "NotificationReadRecord.findByIdSecurityCodeFk", query = "SELECT n FROM NotificationReadRecord n WHERE n.idSecurityCodeFk = :idSecurityCodeFk"),
    @NamedQuery(name = "NotificationReadRecord.findByNoticeDate", query = "SELECT n FROM NotificationReadRecord n WHERE n.noticeDate = :noticeDate"),
    @NamedQuery(name = "NotificationReadRecord.findByReadingDate", query = "SELECT n FROM NotificationReadRecord n WHERE n.readingDate = :readingDate"),
    @NamedQuery(name = "NotificationReadRecord.findByState", query = "SELECT n FROM NotificationReadRecord n WHERE n.state = :state"),
    @NamedQuery(name = "NotificationReadRecord.findByLastModifyUser", query = "SELECT n FROM NotificationReadRecord n WHERE n.lastModifyUser = :lastModifyUser"),
    @NamedQuery(name = "NotificationReadRecord.findByLastModifyDate", query = "SELECT n FROM NotificationReadRecord n WHERE n.lastModifyDate = :lastModifyDate"),
    @NamedQuery(name = "NotificationReadRecord.findByLastModifyIp", query = "SELECT n FROM NotificationReadRecord n WHERE n.lastModifyIp = :lastModifyIp"),
    @NamedQuery(name = "NotificationReadRecord.findByLastModifyApp", query = "SELECT n FROM NotificationReadRecord n WHERE n.lastModifyApp = :lastModifyApp")})
public class NotificationReadRecord implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2228579579984382652L;
    @Id
    @SequenceGenerator(name="NOTIFICATION_READ_RECORD_GENERATOR",sequenceName="SQ_ID_NOTI_READ_RECORD_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NOTIFICATION_READ_RECORD_GENERATOR")
    @NotNull
    @Column(name = "ID_NOTIFICATION_READ_RECORD_PK")
    private Long idNotificationReadRecordPk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PAYMET_AGENT_FK")
    private Long idPaymetAgentFk;
    @Basic(optional = false)
    @Column(name = "ID_GENERAL_PURPOSE_PK")
    private Long idGeneralPurposePk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ID_SECURITY_CODE_FK")
    private String idSecurityCodeFk;
    @Column(name = "NOTICE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date noticeDate;
    @Column(name = "READING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date readingDate;
    @Column(name = "STATE")
    private Integer state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP")
    private Long lastModifyApp;
    public NotificationReadRecord() {
		// TODO Auto-generated constructor stub
	}
	public Long getIdNotificationReadRecordPk() {
		return idNotificationReadRecordPk;
	}
	public Long getIdPaymetAgentFk() {
		return idPaymetAgentFk;
	}
	public void setIdPaymetAgentFk(Long idPaymetAgentFk) {
		this.idPaymetAgentFk = idPaymetAgentFk;
	}
	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}
	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}
	public Date getNoticeDate() {
		return noticeDate;
	}
	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}
	public Date getReadingDate() {
		return readingDate;
	}
	public void setReadingDate(Date readingDate) {
		this.readingDate = readingDate;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public Long getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public void setIdNotificationReadRecordPk(Long idNotificationReadRecordPk) {
		this.idNotificationReadRecordPk = idNotificationReadRecordPk;
	}
	public Long getIdGeneralPurposePk() {
		return idGeneralPurposePk;
	}
	public void setIdGeneralPurposePk(Long idGeneralPurposePk) {
		this.idGeneralPurposePk = idGeneralPurposePk;
	}
}
