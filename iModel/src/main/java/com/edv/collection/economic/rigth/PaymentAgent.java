package com.edv.collection.economic.rigth;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "PAYMENT_AGENT")
    public class PaymentAgent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@SequenceGenerator(name="PAYMENT_AGENT_GENERATOR", sequenceName="SQ_ID_PAYMENT_AGENT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PAYMENT_AGENT_GENERATOR")
    @NotNull
    @Column(name = "ID_PAYMENT_AGENT_PK")
    private Long idPaymentAgentPk;
    
    @Column(name = "ID_PAYMENT_AGENT_FK")
    private Long idPaymentAgentFk;
    @Column(name = "STATE")
    private Integer state;
    /**Parameter table, tipo de actividad economica*/
    @Column(name = "TYPE_PAYMENT_AGENT")
    private Integer typePaymentAgent;
    @Basic(optional = false)
    @Size(max = 40)
    @Column(name = "NAME")
    private String name;
    
    @Basic(optional = false)
    @Size(max = 10)
    @Column(name = "MNEMONIC")
    private String mnemonic;
    
    @Basic(optional = false)
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    
    @Column(name = "SITUATION")
    private Integer situation;
    
    @Transient
    private String paymentAgentFullName;
    
    @Transient
    private String stateDesc;
    
    @Transient
    private String situationDesc;
    
    /**Certificado*/
	@Lob()
   	@Column(name="CERTIFICATE", updatable=true)
   	private byte[] certificate;
	
	@Column(name="CERTIFICATE_NAME")
	private String certificateName;
	   
    @Basic(optional = false)
    @Size(max = 40)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 40)
    @Column(name = "LAST_NAME")
    private String lastName;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 20)
    @Column(name = "CELL_PHONE")
    private String cellPhone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 20)
    @Column(name = "PHONE")
    private String phone;
    @Size(max = 8)
    @Column(name = "INTERN")
    private String intern;
    @Size(max = 20)
    @Column(name = "JOB")
    private String job;
    @Basic(optional = false)
    @Size(max = 600)
    @Column(name = "SECURITY_CLASS_LIST")
    private String securityClassList;
    
    @Column(name = "REGISTRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registreDate;
       
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_MODIFY_APP")
    private long lastModifyApp;
    
    @Column(name = "IND_LIP")
    private Integer indLip;
    
    @Column(name = "IND_SEND_URL_PAYMENT")
    private Integer indSendUrlPayment;
    
    @OneToMany(mappedBy = "idFkPaymentAgent", fetch = FetchType.LAZY)
    private List<GlossNoticationDetail> glossNoticationDetailList;
    
    @Column(name = "ENCRYPT")
    private Integer encryp;
    
    @Column(name = "SIGN")
    private Integer sign;
    
    
    //@ManyToOne(fetch=FetchType.LAZY)
   	//@JoinColumn(name="ID_DIGITAL_SIGNATURE_FK")
    //private DigitalSignature digitalSignature;
    /**
     * objeto para obetenr el banco en caso de que no sea lip
     */
    @Transient
    private String bankDescription;
    @Transient
    private Long idBank;
    
    /**
     * objeto para obtener el numero de cuenta corriente del banco en caso de q NO sea LIP
     */
    @Transient
    private String currentAccountNumber;
    
    @Transient
    private String htmlMessage;
    
    @Transient
	private byte[] glossPdf;

    public PaymentAgent() {
    	name=firstName=lastName=cellPhone=email=intern=job=phone="";
    }

    public PaymentAgent(Long idPaymentAgentPk) {
        this.idPaymentAgentPk = idPaymentAgentPk;
    }

    public Long getIdPaymentAgentPk() {
        return idPaymentAgentPk;
    }

    public String getBankDescription() {
		return bankDescription;
	}

	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}

	public String getCurrentAccountNumber() {
		return currentAccountNumber;
	}

	public Integer getIndSendUrlPayment() {
		return indSendUrlPayment;
	}

	public void setIndSendUrlPayment(Integer indSendUrlPayment) {
		this.indSendUrlPayment = indSendUrlPayment;
	}

	public void setCurrentAccountNumber(String currentAccountNumber) {
		this.currentAccountNumber = currentAccountNumber;
	}

	public Long getIdBank() {
		return idBank;
	}

	public void setIdBank(Long idBank) {
		this.idBank = idBank;
	}

	public String getHtmlMessage() {
		return htmlMessage;
	}

	public void setHtmlMessage(String htmlMessage) {
		this.htmlMessage = htmlMessage;
	}

	public byte[] getGlossPdf() {
		return glossPdf;
	}

	public void setGlossPdf(byte[] glossPdf) {
		this.glossPdf = glossPdf;
	}

	public Integer getIndLip() {
		return indLip;
	}

	public void setIndLip(Integer indLip) {
		this.indLip = indLip;
	}

	public void setIdPaymentAgentPk(Long idPaymentAgentPk) {
        this.idPaymentAgentPk = idPaymentAgentPk;
    }

    public List<GlossNoticationDetail> getGlossNoticationDetailList() {
		return glossNoticationDetailList;
	}

	public void setGlossNoticationDetailList(List<GlossNoticationDetail> glossNoticationDetailList) {
		this.glossNoticationDetailList = glossNoticationDetailList;
	}

	public Integer getTypePaymentAgent() {
        return typePaymentAgent;
    }

    public void setTypePaymentAgent(Integer typePaymentAgent) {
        this.typePaymentAgent = typePaymentAgent;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIntern() {
        return intern;
    }

    public void setIntern(String intern) {
        this.intern = intern;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getSecurityClassList() {
        return securityClassList;
    }

    public void setSecurityClassList(String securityClassList) {
        this.securityClassList = securityClassList;
    }

  
    public long getLastModifyApp() {
        return lastModifyApp;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPaymentAgentPk != null ? idPaymentAgentPk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentAgent)) {
            return false;
        }
        PaymentAgent other = (PaymentAgent) object;
        if ((this.idPaymentAgentPk == null && other.idPaymentAgentPk != null) || (this.idPaymentAgentPk != null && !this.idPaymentAgentPk.equals(other.idPaymentAgentPk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.mavenproject1.PaymentAgent[ idPaymentAgentPk=" + idPaymentAgentPk + " ]";
        
    }

    public Integer getSituation() {
        return situation;
    }

    public void setSituation(Integer situation) {
        this.situation = situation;
    }

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getPaymentAgentFullName() {
//		StringBuilder fullName=new StringBuilder();
//		fullName.append(name==null?"":name);
//		fullName.append(" ");
//		fullName.append(firstName==null?"":firstName);
//		fullName.append(" ");
//		fullName.append(lastName==null?"":lastName);
//		paymentAgentFullName=fullName.toString();
//		return paymentAgentFullName.trim();
		return paymentAgentFullName;
	}

	public void setPaymentAgentFullName(String paymentAgentFullName) {
		this.paymentAgentFullName = paymentAgentFullName;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public String getSituationDesc() {
		return situationDesc;
	}

	public void setSituationDesc(String situationDesc) {
		this.situationDesc = situationDesc;
	}

	public Date getRegistreDate() {
		return registreDate;
	}

	public void setRegistreDate(Date registreDate) {
		this.registreDate = registreDate;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public void setLastModifyApp(long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Long getIdPaymentAgentFk() {
		return idPaymentAgentFk;
	}

	public void setIdPaymentAgentFk(Long idPaymentAgentFk) {
		this.idPaymentAgentFk = idPaymentAgentFk;
	}

	public Integer getEncryp() {
		return encryp;
	}

	public void setEncryp(Integer encryp) {
		this.encryp = encryp;
	}

	public Integer getSign() {
		return sign;
	}

	public void setSign(Integer sign) {
		this.sign = sign;
	}


	public byte[] getCertificate() {
		return certificate;
	}

	public void setCertificate(byte[] certificate) {
		this.certificate = certificate;
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
}
