package com.edv.collection.economic.rigth;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "FUNDS_COLLECTION")
public class FundsCollection implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
	@SequenceGenerator(name="FUNDS_COLLECTION_GENERATOR", sequenceName="SQ_ID_FUNDS_COLLECTION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FUNDS_COLLECTION_GENERATOR")
    @Column(name = "ID_FUNDS_COLLECTION_PK")
    private Long idFundsCollectionPk;
    @Column(name = "ORIGIN_PARTICIPANT_CODE")
    private String originParticipantCode;
    @Column(name = "ORIGIN_PARTICIPANT_DESC")
    private String originParticipantDesc;
    @Column(name = "ORIGIN_DEPARTMENT")
    private String originDepartment;
    @Column(name = "TARGET_PARTICIPANT_CODE")
    private String targetParticipantCode;
    @Column(name = "TARGET_PARTICIPANT_DESC")
    private String targetParticipantDesc;
    @Column(name = "TARGET_DEPARTMENT")
    private String targetDepartment;
    @Column(name = "ORIGIN_BANK_ACCOUNT_CODE")
    private String originBankAccountCode;
    @Column(name = "ORIGIN_CURRENCY")
    private Short originCurrency;
    @Transient
    private String descOriginCurrency;
    @Column(name = "COD_TYPE_OPERATION")
    private String codTypeOperation;
    @Column(name = "DESC_TYPE_OPERATION")
    private String descTypeOperation;
    @Column(name = "NUMBER_REQUEST")
    private Long numberRequest;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "RETIREMENT_AMOUNT")
    private BigDecimal retirementAmount;
    @Column(name = "ACTUAL_BALANCE")
    private BigDecimal actualBalance;
    @Column(name = "PREVIOUS_BALANCE")
    private BigDecimal previousBalance;
    @Column(name = "DATE_OPERATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOperation;
    @Column(name = "HOUR_OPERATION")
    private String hourOperation;
    @Column(name = "TEXT_DESCRIPTION")
    private String textDescription;
    @Column(name = "GLOSS")
    private String gloss;
    @Column(name = "OPERATION_STATE")
    private Integer operationState;
    @Transient
    private String descOperationState;
    @Column(name = "COMMENTS")
    private String comments;
    @Column(name = "IND_DEPOSIT_LIP")
    private Short indDepositLip;
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Column(name = "LAST_MODIFY_APP")
    private Long lastModifyApp;
    public FundsCollection() {
		// TODO Auto-generated constructor stub
	}
    
    public FundsCollection(Long idFundCollectionPk) {
		this.idFundsCollectionPk = idFundCollectionPk;
	}

	public Long getIdFundsCollectionPk() {
		return idFundsCollectionPk;
	}

	public void setIdFundsCollectionPk(Long idFundsCollectionPk) {
		this.idFundsCollectionPk = idFundsCollectionPk;
	}

	public String getOriginParticipantCode() {
		return originParticipantCode;
	}

	public void setOriginParticipantCode(String originParticipantCode) {
		this.originParticipantCode = originParticipantCode;
	}

	public String getOriginParticipantDesc() {
		return originParticipantDesc;
	}

	public void setOriginParticipantDesc(String originParticipantDesc) {
		this.originParticipantDesc = originParticipantDesc;
	}

	public String getOriginDepartment() {
		return originDepartment;
	}

	public void setOriginDepartment(String originDepartment) {
		this.originDepartment = originDepartment;
	}

	public String getTargetParticipantCode() {
		return targetParticipantCode;
	}

	public void setTargetParticipantCode(String targetParticipantCode) {
		this.targetParticipantCode = targetParticipantCode;
	}

	public String getTargetParticipantDesc() {
		return targetParticipantDesc;
	}

	public void setTargetParticipantDesc(String targetParticipantDesc) {
		this.targetParticipantDesc = targetParticipantDesc;
	}

	public String getTargetDepartment() {
		return targetDepartment;
	}

	public void setTargetDepartment(String targetDepartment) {
		this.targetDepartment = targetDepartment;
	}

	public String getOriginBankAccountCode() {
		return originBankAccountCode;
	}

	public void setOriginBankAccountCode(String originBankAccountCode) {
		this.originBankAccountCode = originBankAccountCode;
	}

	public Short getOriginCurrency() {
		return originCurrency;
	}

	public void setOriginCurrency(Short originCurrency) {
		this.originCurrency = originCurrency;
	}

	public String getDescOriginCurrency() {
		return descOriginCurrency;
	}

	public void setDescOriginCurrency(String descOriginCurrency) {
		this.descOriginCurrency = descOriginCurrency;
	}

	public String getCodTypeOperation() {
		return codTypeOperation;
	}

	public void setCodTypeOperation(String codTypeOperation) {
		this.codTypeOperation = codTypeOperation;
	}

	public String getDescTypeOperation() {
		return descTypeOperation;
	}

	public void setDescTypeOperation(String descTypeOperation) {
		this.descTypeOperation = descTypeOperation;
	}

	public Long getNumberRequest() {
		return numberRequest;
	}

	public void setNumberRequest(Long numberRequest) {
		this.numberRequest = numberRequest;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getRetirementAmount() {
		return retirementAmount;
	}

	public void setRetirementAmount(BigDecimal retirementAmount) {
		this.retirementAmount = retirementAmount;
	}

	public BigDecimal getActualBalance() {
		return actualBalance;
	}

	public void setActualBalance(BigDecimal actualBalance) {
		this.actualBalance = actualBalance;
	}

	public BigDecimal getPreviousBalance() {
		return previousBalance;
	}

	public void setPreviousBalance(BigDecimal previousBalance) {
		this.previousBalance = previousBalance;
	}

	public Date getDateOperation() {
		return dateOperation;
	}

	public void setDateOperation(Date dateOperation) {
		this.dateOperation = dateOperation;
	}

	public String getHourOperation() {
		return hourOperation;
	}

	public void setHourOperation(String hourOperation) {
		this.hourOperation = hourOperation;
	}

	public String getTextDescription() {
		return textDescription;
	}

	public void setTextDescription(String textDescription) {
		this.textDescription = textDescription;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public String getDescOperationState() {
		return descOperationState;
	}

	public void setDescOperationState(String descOperationState) {
		this.descOperationState = descOperationState;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Short getIndDepositLip() {
		return indDepositLip;
	}

	public void setIndDepositLip(Short indDepositLip) {
		this.indDepositLip = indDepositLip;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Long getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Long lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
    
}