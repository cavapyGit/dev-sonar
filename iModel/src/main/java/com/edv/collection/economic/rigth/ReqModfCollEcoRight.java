package com.edv.collection.economic.rigth;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "REQ_MODF_COLL_ECO_RIGHT")
public class ReqModfCollEcoRight implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="REQUEST_MODF_COLLECTION_RIGHT_GENERATOR", sequenceName="SQ_REQ_MODF_COLL_ECO_PK",initialValue=1,allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REQUEST_MODF_COLLECTION_RIGHT_GENERATOR")
    @Column(name = "ID_REQ_MOF_COLL_ECO_PK")
    private Long idReqMofCollEcoPk;
    //@ManyToOne(fetch=FetchType.LAZY)
    @Column(name = "ID_REQ_COLLE_ECONOMIC_FK")
    private  Long idReqColleEconomicRightFk;
    @Column(name = "REGISTRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registreDate;
    @Column(name = "STATE")
    private Integer state;
    @Transient
    private String stateDesc;
    /** The last modify app. */
    @Column(name = "LAST_MODIFY_APP")
    private Integer lastModifyApp;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_MODIFY_DATE")
    private Date lastModifyDate;
    /** The last modify ip. */
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    /** The last modify user. */
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
	public Long getIdReqMofCollEcoPk() {
		return idReqMofCollEcoPk;
	}
	public void setIdReqMofCollEcoPk(Long idReqMofCollEcoPk) {
		this.idReqMofCollEcoPk = idReqMofCollEcoPk;
	}
	public Long getIdReqColleEconomicRightFk() {
		return idReqColleEconomicRightFk;
	}
	public void setIdReqColleEconomicRightFk(Long idReqColleEconomicRightFk) {
		this.idReqColleEconomicRightFk = idReqColleEconomicRightFk;
	}
	public Date getRegistreDate() {
		return registreDate;
	}
	public void setRegistreDate(Date registreDate) {
		this.registreDate = registreDate;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getStateDesc() {
		return stateDesc;
	}
	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
  }
