package com.edv.collection.economic.rigth;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hcoarite
 */
@Entity
@Table(name = "EQUIVALENT_DEPOSIT_GLOSSES")
@XmlRootElement
public class EquivalentDepositGlosses implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id  
    @SequenceGenerator(name="EQUIVALENT_DEPOSIT_GLOSSES_GENERATOR", sequenceName="SQ_ID_EQ_DEPOSIT_GLOSSES_PK",initialValue=1,allocationSize=1)
  	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EQUIVALENT_DEPOSIT_GLOSSES_GENERATOR")
    @Column(name = "ID_EQ_DEPOSIT_GLOSSES_PK")
    private Long idEqDepositGlossesPk;
    @Column(name = "TOTAL_AMOUNT")
    private BigDecimal totalAmount;
    @Size(max = 100)
    @Column(name = "LIST_FUNDS_COLLECTION")
    private String listFundsCollection;
    @Size(max = 40)
    @Column(name = "REGISTER_USER")
    private String registerUser;
    @Column(name = "REGISTRE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registreDate;
    @Size(max = 38)
    @Column(name = "CONFIRM_USER")
    private String confirmUser;
    @Column(name = "CONFIRM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date confirmDate;
    @Column(name = "OPERATION_STATE")
    private Integer operationState;
    @Size(max = 10)
    @Column(name = "COMMENTS")
    private String comments;
    @Size(max = 10)
    @Column(name = "LAST_MODIFY_USER")
    private String lastModifyUser;
    @Column(name = "LAST_MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate;
    @Size(max = 20)
    @Column(name = "LAST_MODIFY_IP")
    private String lastModifyIp;
    @Column(name = "LAST_MODIFY_APP")
    private Integer lastModifyApp;
    @Column(name = "ID_PARTICIPANT_FK")
    private Long idParticipantFk;
    @Column(name = "CURRENCY")
    private Integer currency;
    @Column(name = "PAYMENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date paymentDate;
    @Size(max = 10)
    @Column(name = "GLOSS")
    private String gloss;
    //Lista de fondos mostrados en los resultados
    @Column(name = "LIST_FUNDS_COLLECTION_SHOW")
    private String listFundsCollectionShow;
  //Lista de glosas mostrados en los resultados
    @Column(name = "LIST_DETAIL_COLLECTION_SHOW")
    private String listDetailCollectionShow;
    @JoinColumn(name = "ID_DETAIL_COLLECTION_GLOSS_FK", referencedColumnName = "ID_DETAIL_COLLECTION_GLOSS_PK")
    @ManyToOne
    private DetailCollectionGloss idDetailCollectionGlossFk;
    public EquivalentDepositGlosses() {
		// TODO Auto-generated constructor stub
	}
    
    public EquivalentDepositGlosses(Long idEqDepositGlossesPk) {
		this.idEqDepositGlossesPk = idEqDepositGlossesPk;
	}

	public Long getIdEqDepositGlossesPk() {
		return idEqDepositGlossesPk;
	}

	public void setIdEqDepositGlossesPk(Long idEqDepositGlossesPk) {
		this.idEqDepositGlossesPk = idEqDepositGlossesPk;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getListFundsCollection() {
		return listFundsCollection;
	}

	public void setListFundsCollection(String listFundsCollection) {
		this.listFundsCollection = listFundsCollection;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getRegistreDate() {
		return registreDate;
	}

	public void setRegistreDate(Date registreDate) {
		this.registreDate = registreDate;
	}

	public String getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public Integer getOperationState() {
		return operationState;
	}

	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public Long getIdParticipantFk() {
		return idParticipantFk;
	}

	public void setIdParticipantFk(Long idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public String getListFundsCollectionShow() {
		return listFundsCollectionShow;
	}

	public void setListFundsCollectionShow(String listFundsCollectionShow) {
		this.listFundsCollectionShow = listFundsCollectionShow;
	}

	public String getListDetailCollectionShow() {
		return listDetailCollectionShow;
	}

	public void setListDetailCollectionShow(String listDetailCollectionShow) {
		this.listDetailCollectionShow = listDetailCollectionShow;
	}

	public DetailCollectionGloss getIdDetailCollectionGlossFk() {
		return idDetailCollectionGlossFk;
	}

	public void setIdDetailCollectionGlossFk(DetailCollectionGloss idDetailCollectionGlossFk) {
		this.idDetailCollectionGlossFk = idDetailCollectionGlossFk;
	}
    
}
