package com.pradera.corporate.stockcalculation;


import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
/*import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.descriptor.api.Descriptors;
import java.io.File;
import javax.ejb.EJB;
import javax.inject.Inject;
//import org.jboss.shrinkwrap.descriptor.api.beans10.BeansDescriptor;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;*/
import org.junit.Test;
import org.junit.runner.RunWith;
/*
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.TestClassRoot;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.corporateevents.stockcalculation.facade.ManageStockCalculationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.issuancesecuritie.Security;
*/
//@RunWith(Arquillian.class)
public class StockCalculationTest {
/*
	@Inject
	PraderaLogger log;
	
	@EJB
	ManageStockCalculationServiceFacade manageStockCalculationService;
	
	@EJB
	CorporateProcessServiceFacade corporateProcessService;*/
	
	//@Deployment
	public static WebArchive createTestArchive() {/*
  		File[] libs = Maven.resolver()
                  .loadPomFromFile("pom.xml")
                  .importRuntimeDependencies()
                  .asFile();
  		 BeansDescriptor beans = Descriptors.create(BeansDescriptor.class)
  				 .getOrCreateInterceptors().clazz("com.pradera.commons.interceptores.PerformanceInterceptor").up()
  				 .getOrCreateInterceptors().clazz("com.pradera.core.framework.extension.transaction.jta.TransactionalInterceptor").up();
  				 
  		
  		WebArchive war =
  		
  		ShrinkWrap.create(WebArchive.class, "iCorporativeEvents.war")
  				.addAsLibraries(libs)
  				.addPackages(true, TestClassRoot.class.getPackage())
  				.addAsManifestResource("MANIFEST.MF")
  				.addAsWebInfResource(new File("src/main/webapp/WEB-INF/ejb-jar.xml"))
  				.addAsWebInfResource(new StringAsset(beans.exportAsString()), "beans.xml");
  				//reports templates  				
//  				.addAsResource(new File("src/main/resources/reports/custody/accreditationOperationReport.prpt"),"reports/custody/accreditationOperationReport.prpt");
  		System.out.println(war.toString((Formatters.VERBOSE)));
  		return war;*/
		return null;
  	}
	
	//@Test
	public void testRollback(){
		/*try{
			LoggerUser loggerUser = new LoggerUser();
			loggerUser.setIpAddress("192.168.1.56");
			loggerUser.setUserName("RCASO");
			loggerUser.setIdUserSessionPk(1l);
			loggerUser.setSessionId("sessssxkxkdd");
			loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
			//Praderafilter in each request get current UserAcction
			UserAcctions userAcctions = new UserAcctions();
			userAcctions.setIdPrivilegeAdd(1);
			userAcctions.setIdPrivilegeRegister(1);
			loggerUser.setUserAction(userAcctions);
			loggerUser.setIdPrivilegeOfSystem(1);
			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		log.info("Probando rollback");
		corporateProcessService.testRollbackYTransaction();
		log.info("no deberia entrar aca ");
		} catch(Exception ex){
			log.error(ex.getMessage());
			log.info("deberia rollback el cambio del indicador a 0");
		}
		log.info("terminando metodo");*/
	}
	
//	@Test
	public void testRegisterStockCalculation(){
		/*LoggerUser loggerUser = new LoggerUser();
		loggerUser.setIpAddress("192.168.1.56");
		loggerUser.setUserName("ADMIN");
		loggerUser.setIdUserSessionPk(1l);
		loggerUser.setSessionId("sessssxkxkdd");
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		//Praderafilter in each request get current UserAcction
		UserAcctions userAcctions = new UserAcctions();
		userAcctions.setIdPrivilegeAdd(1);
		userAcctions.setIdPrivilegeRegister(1);
		loggerUser.setUserAction(userAcctions);
		loggerUser.setIdPrivilegeOfSystem(1);
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		StockCalculationProcess stockCalculationProcess = new StockCalculationProcess();
		stockCalculationProcess.setCutoffDate(CommonsUtilities.convertStringtoDate("23/05/2014"));
		stockCalculationProcess.setRegistryDate(CommonsUtilities.convertStringtoDate("23/05/2014"));
		stockCalculationProcess.setStockType(StockType.HOLDER.getCode());
		stockCalculationProcess.setIndAutomactic(BooleanType.NO.getCode());
		stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
		Security security = new Security();
		security.setIdSecurityCodePk("DO01022132G2");;
		stockCalculationProcess.setSecurity(security);
		try{
		manageStockCalculationService.registerStockCalculation(stockCalculationProcess);
		log.info("Despues de ejecutar el Stock");
		} catch(ServiceException sex){
			log.error(sex.getMessage());
		}*/
	}
}
