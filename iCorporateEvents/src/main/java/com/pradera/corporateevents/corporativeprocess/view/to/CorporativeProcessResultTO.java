package com.pradera.corporateevents.corporativeprocess.view.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporativeProcessResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class CorporativeProcessResultTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 636571749616919106L;
	
	/** The id coporative process. */
	private Long idCoporativeProcess;
	
	/** The id participant. */
	private Long idParticipant; 
	
	/** The id holder account. */
	private Long idHolderAccount;
	
	/** The id securities. */
	private String idSecurities;
	
	/** The stock quantity. */
	private BigDecimal stockQuantity; //Stock quantity to move in the account balance
	
	/** The id isin code. */
	private String idIsinCode;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the id holder account.
	 *
	 * @return the id holder account
	 */
	public Long getIdHolderAccount() {
		return idHolderAccount;
	}

	/**
	 * Sets the id holder account.
	 *
	 * @param idHolderAccount the new id holder account
	 */
	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}

	/**
	 * Gets the id securities.
	 *
	 * @return the id securities
	 */
	public String getIdSecurities() {
		return idSecurities;
	}

	/**
	 * Sets the id securities.
	 *
	 * @param idSecurities the new id securities
	 */
	public void setIdSecurities(String idSecurities) {
		this.idSecurities = idSecurities;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Gets the id isin code.
	 *
	 * @return the id isin code
	 */
	public String getIdIsinCode() {
		return idIsinCode;
	}

	/**
	 * Sets the id isin code.
	 *
	 * @param idIsinCode the new id isin code
	 */
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}


	/**
	 * Gets the id coporative process.
	 *
	 * @return the id coporative process
	 */
	public Long getIdCoporativeProcess() {
		return idCoporativeProcess;
	}

	/**
	 * Sets the id coporative process.
	 *
	 * @param idCoporativeProcess the new id coporative process
	 */
	public void setIdCoporativeProcess(Long idCoporativeProcess) {
		this.idCoporativeProcess = idCoporativeProcess;
	}

}
