package com.pradera.corporateevents.corporativeprocess.view;

import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationGroup;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryGroupConversation;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateServiceFacade;
import com.pradera.corporateevents.corporativeprocess.importanceevents.view.ImportanceEventBean;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.service.CorporateProcessServiceBean;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporateMonitoringDetailSearchTO;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporateMonitoringTO;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporativeProcessDetailsTO;
import com.pradera.corporateevents.stockcalculation.facade.ManageStockCalculationServiceFacade;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeSituationType;
import com.pradera.model.corporatives.type.DeliveryPlacementType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class CorporateMonitoringBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCorporateEvents
 * @Creation_Date :
 */

@LoggerCreateBean
@DepositaryWebBean
public class CorporateMonitoringBean extends GenericBaseBean {

	/** added default serial version uid. */
	private static final long serialVersionUID = 1L;
	
	@EJB
	CorporateProcessServiceBean corporateProcessService;
	
	/** The corporate process service facade. */
	@EJB
	CorporateProcessServiceFacade corporateProcessServiceFacade;
	
	/** The Corporate MonitoringProcess Service facade. */
	@EJB
	private CorporateServiceFacade corporateServiceFacade;
	
	/** Holds the current user info. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The manage stock calculation facade. */
	@EJB
	ManageStockCalculationServiceFacade manageStockCalculationFacade;
	
	/** The batch service. */
	@EJB
	BatchServiceBean batchService;
	
	/** Holds the process detail link page. */
	@Inject
	@ConversationGroup(DepositaryGroupConversation.class)
	private ImportanceEventBean importanceEventBeanDetail;
		
	/** The General Parameter Facade. */
	@Inject
	private GeneralParametersFacade generalParametersFacade;
	
	/** Holder the Securities help button. */
	@Inject
	private SecuritiesHelperBean securitiesHelper;
	
	/** Holder the Issuer help button. */
	@Inject
	private IssuerHelperBean issuerHelper;
	
	/** The id dialog confirm sucess. */
	private static String ID_DIALOG_CONFIRM_SUCESS ="idDialDepositary";
	
	/** The CorporateMonitoringFilter *. */
	private CorporateMonitoringTO corporateMonitoringFilter;	
	
	/** Hold the selected row details. */
	private CorporativeOperation selectedCorporativeOperation;
	
	/** the selected multiple records. */
	private CorporativeOperation[] checkCorporativeOperation;

	/** The Search DataModel. */
	private GenericDataModel<CorporativeOperation> corporateDataModel;

	/** The process list. */
	private List<Map<String, String>> processList;
	
	/* Holds Security details*/ 
	/** The sec. */
	private Security sec ;
	
	/** Holds Preliminary Adjust DataModel. */
	private GenericDataModel<CorporateMonitoringDetailSearchTO> corpProcessResultDataModel;
	
	/** Holds the total balance from Search Result. */
	private BigDecimal totalBalance;
	
	/** Holds the Available balance from Search Result. */
	private BigDecimal availableBalance;
	
	/** Holds the total custody amount from Search Result. */
	private BigDecimal custodyAmount;
	
	/** Holds the total tax amount from Search Result. */
	private BigDecimal taxAmount;
	
	/** Holds the total stock balance from Search Result. */
	private BigDecimal stockTotalBalance;
	
	/** Holds the total stock available balance from Search Result. */
	private BigDecimal stockAvailableBalance;
	
	/** Holds the total cash balance from Search Result. */
	private BigDecimal cashTotalBalance;
	
	/** Holds the total cash available balance from Search Result. */
	private BigDecimal cashAvailableBalance;

	/** The security code. */
	private String securityCode;
	
	/** Holds Details page DataModel. */
	private GenericDataModel<CorporativeProcessDetailsTO> detailSearchDataModel;
	
	/** The joint holders. */
	private GenericDataModel<CorporativeProcessDetailsTO> jointHolders;
	
	/** The selected joint holder. */
	private CorporativeProcessDetailsTO selectedJointHolder;
	
	/** The list Process type. */
	private List<ParameterTable> lstProcessType;
	
	/** Indicates open of adjust button window or adjust link window. */
	private boolean adjustCorporateViewFlag;
	
	/** Holds the adjust total balance. */
	private BigDecimal newTotalAmt;
	
	/** Holds the adjust Custody amount. */
	private BigDecimal newCustodyAmt;
	
	/** Holds the adjust tax amount. */
	private BigDecimal newTaxAmt;
	
	/** Holds the preliminary adjustment data. */
	private CorporateMonitoringDetailSearchTO saveHolderAccountAdjustment;
	
	/** Max date. */
	private Date maxDate;
	
	/** Holds Currency types. */
	private List<ParameterTable> lstCurrency;
	
	/** Holds tprocess State types. */
	private List<ParameterTable> lstCorpProcessState;
	
	private List<ParameterTable> lstCorpProcessTypeDate;
	
	/** The instrumente types. */
	private List<ParameterTable> instrumenteTypes;
	
	/** The key conf depositary. */
	private Integer keyConfDepositary;
	
	/** The key conf issuer. */
	private Integer keyConfIssuer;
	
	/** The key conf paid. */
	private Integer keyConfPaid;
	
	/** The preliminary status. */
	private Integer preliminaryStatus;
	
	/** The interest process. */
	private Integer interestProcess;
	
	/** The amortization process. */
	private Integer amortizationProcess;
	
	/** Holds the login user is issuer. */
	private boolean issuerUser;
	
	/** The depositary user. */
	private boolean depositaryUser;
	
	/** The confirm alert action. */
	private String confirmAlertAction="";
	
	/** The special process. */
	private boolean specialProcess;
	
	/** The cash benefit. */
	private boolean cashBenefit;
	
	/** The exchange cash benefit. */
	private boolean exchangeCashBenefit;
	
	/** The share benefit. */
	private boolean shareBenefit;
	
	/** The remnant benefit. */
	private boolean remnantBenefit;
	
	/** The block balance. */
	private BigDecimal blockBalance;
	
	/** The adjusted balance. */
	private BigDecimal adjustedBalance;
	
	/** The destiny securities. */
	private List<ParameterTable> destinySecurities;
	
	/** The destiny security. */
	private String destinySecurity;
	
	/** The excision. */
	private boolean excision;
	
	/** The holder. */
	private Holder holder;
	
	/** The holder account type. */
	private Integer holderAccountType;
	
	/** The holder adj modified. */
	private boolean holderAdjModified;
	
	/** The exchange rate. */
	private BigDecimal exchangeRate;
	
	/** The corporate state map. */
	private Map<Integer,String> corporateStateMap;
	
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The lst cbo filter securitie class. */
	private List<ParameterTable> lstCboFilterSecuritieClass;
	
	/** The lst cbo filter currency. */
	private List<ParameterTable> lstCboFilterCurrency;
	
	private String labelDateSearch;
	
	/**
	 * Gets the exchange rate.
	 *
	 * @return the exchange rate
	 */
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}
	
	/**
	 * Sets the exchange rate.
	 *
	 * @param exchangeRate the new exchange rate
	 */
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
	/**
	 * Load daily exchange rate.
	 */
	private void loadDailyExchangeRate(){
		try {
			setExchangeRate(corporateServiceFacade.getUSDExchangeRate());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));			 
		}
	}
	
	/**
	 * Initializing managed bean object.
	 */
	public CorporateMonitoringBean() {
		super();		
		corporateMonitoringFilter = new CorporateMonitoringTO();
		corporateMonitoringFilter.setTypeDateCorporative(1);
		corporateMonitoringFilter.setSelectedDate(CommonsUtilities.currentDate());
		//only get depositary placement
		corporateMonitoringFilter.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
		corporateMonitoringFilter.setDeliveryPlacementAux(DeliveryPlacementType.ISSUER.getCode());
		sec = new Security();
		keyConfDepositary = CorporativeSituationType.IDEPOSITARY.getCode();
		keyConfIssuer = CorporativeSituationType.ISSUER.getCode();
		preliminaryStatus = CorporateProcessStateType.PRELIMINARY.getCode();	
		keyConfPaid = CorporativeSituationType.PAID.getCode();
		interestProcess=ImportanceEventType.INTEREST_PAYMENT.getCode();
		amortizationProcess=ImportanceEventType.CAPITAL_AMORTIZATION.getCode();
	}
	
	/**
	 * Init call.
	 */
	@PostConstruct
	public void init() {		
		try{			
			holder = new Holder();
			corporateMonitoringFilter.setIssuer(new Issuer());
			corporateMonitoringFilter.setSecurities(new Security());
			corporateMonitoringFilter.setExpirationDate(CommonsUtilities.currentDate());
			corporateMonitoringFilter.setExpirationDateFinal(CommonsUtilities.currentDate());
//			corporateMonitoringFilter.setExpirationDate(CommonsUtilities.currentDate());
			if (!FacesContext.getCurrentInstance().isPostback()) {				
				if (userInfo.getUserAccountSession().isDepositaryInstitution()){
					depositaryUser=true;
				} 				
				if(userInfo.getUserAccountSession().isIssuerInstitucion()) {
					issuerUser=true;
					IssuerSearcherTO issuerTo = new IssuerSearcherTO();
					issuerTo.setIssuerCode(userInfo.getUserAccountSession().getIssuerCode());
					Issuer issuer =	corporateServiceFacade.getIssuerDetails(issuerTo);
					corporateMonitoringFilter.setIssuer(issuer);			
				}
				fillWindowLoadData();
			
			}
			
			loadCboFilterSecuritieClass();
			loadCboFilterCurency();
			labelDateSearch="Vencimiento";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		//Load dailyExchangeRate
       loadDailyExchangeRate();
       lstCorpProcessTypeDate = getParameterTypeDate();
	}
	
	public void loadCboFilterSecuritieClass() throws ServiceException {
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		lstCboFilterSecuritieClass=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
//		billParametersTableMap(lstCboFilterSecuritieClass,true);
	}
	public void loadCboFilterCurency() throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCboFilterCurrency=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
//		billParametersTableMap(lstCboFilterCurrency, true);
	}
	
	/**
	 * Checks if is adjust corporate view flag.
	 *
	 * @return the adjustCorporateViewFlag
	 */
	public boolean isAdjustCorporateViewFlag() {
		return adjustCorporateViewFlag;
	}

	/**
	 * Sets the adjust corporate view flag.
	 *
	 * @param adjustCorporateViewFlag the adjustCorporateViewFlag to set
	 */
	public void setAdjustCorporateViewFlag(boolean adjustCorporateViewFlag) {
		this.adjustCorporateViewFlag = adjustCorporateViewFlag;
	}
	
	/**
	 * Gets the new total amt.
	 *
	 * @return the newTotalAmt
	 */
	public BigDecimal getNewTotalAmt() {
		return newTotalAmt;
	}
	
	/**
	 * Sets the new total amt.
	 *
	 * @param newTotalAmt the newTotalAmt to set
	 */
	public void setNewTotalAmt(BigDecimal newTotalAmt) {
		this.newTotalAmt = newTotalAmt;
	}
	
	/**
	 * Gets the new custody amt.
	 *
	 * @return the newCustodyAmt
	 */
	public BigDecimal getNewCustodyAmt() {
		return newCustodyAmt;
	}
	
	/**
	 * Sets the new custody amt.
	 *
	 * @param newCustodyAmt the newCustodyAmt to set
	 */
	public void setNewCustodyAmt(BigDecimal newCustodyAmt) {
		this.newCustodyAmt = newCustodyAmt;
	}
	
	/**
	 * Gets the new tax amt.
	 *
	 * @return the newTaxAmt
	 */
	public BigDecimal getNewTaxAmt() {
		return newTaxAmt;
	}
	
	/**
	 * Sets the new tax amt.
	 *
	 * @param newTaxAmt the newTaxAmt to set
	 */
	public void setNewTaxAmt(BigDecimal newTaxAmt) {
		this.newTaxAmt = newTaxAmt;
	}
	
	
	/**
	 * Gets the save holder account adjustment.
	 *
	 * @return the saveHolderAccountAdjustment
	 */
	public CorporateMonitoringDetailSearchTO getSaveHolderAccountAdjustment() {
		return saveHolderAccountAdjustment;
	}

	/**
	 * Sets the save holder account adjustment.
	 *
	 * @param saveHolderAccountAdjustment the saveHolderAccountAdjustment to set
	 */
	public void setSaveHolderAccountAdjustment(
			CorporateMonitoringDetailSearchTO saveHolderAccountAdjustment) {
		this.saveHolderAccountAdjustment = saveHolderAccountAdjustment;
	}
	
	/**
	 * Gets the max date.
	 *
	 * @return the maxDate
	 */
	public Date getMaxDate() {
		return maxDate;
	}
	
	/**
	 * Sets the max date.
	 *
	 * @param maxDate the maxDate to set
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lstCurrency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the lstCurrency to set
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the lst corp process state.
	 *
	 * @return the lstCorpProcessState
	 */
	public List<ParameterTable> getLstCorpProcessState() {
		return lstCorpProcessState;
	}

	/**
	 * Sets the lst corp process state.
	 *
	 * @param lstCorpProcessState the lstCorpProcessState to set
	 */
	public void setLstCorpProcessState(
			List<ParameterTable> lstCorpProcessState) {
		this.lstCorpProcessState = lstCorpProcessState;
	}	
	
	/**
	 * Checks if is issuer user.
	 *
	 * @return the issuerUser
	 */
	public boolean isIssuerUser() {
		return issuerUser;
	}
	
	/**
	 * Sets the issuer user.
	 *
	 * @param issuerUser the issuerUser to set
	 */
	public void setIssuerUser(boolean issuerUser) {
		this.issuerUser = issuerUser;
	}

	/**
	 * Gets the corporate monitoring filter.
	 *
	 * @return the corporateMonitoringFilter
	 */
	public CorporateMonitoringTO getCorporateMonitoringFilter() {
		return corporateMonitoringFilter;
	}
	
	/**
	 * Sets the corporate monitoring filter.
	 *
	 * @param corporateMonitoringFilter the corporateMonitoringFilter to set
	 */
	public void setCorporateMonitoringFilter(
			CorporateMonitoringTO corporateMonitoringFilter) {
		this.corporateMonitoringFilter = corporateMonitoringFilter;
	}
	
	/**
	 * Gets the selected corporative operation.
	 *
	 * @return the selectedCorporativeOperation
	 */
	public CorporativeOperation getSelectedCorporativeOperation() {
		return selectedCorporativeOperation;
	}

	/**
	 * Sets the selected corporative operation.
	 *
	 * @param selectedCorporativeOperation the selectedCorporativeOperation to set
	 */
	public void setSelectedCorporativeOperation(
			CorporativeOperation selectedCorporativeOperation) {
		this.selectedCorporativeOperation = selectedCorporativeOperation;
	}
	
	/**
	 * Gets the check corporative operation.
	 *
	 * @return the checkCorporativeOperation
	 */
	public CorporativeOperation[] getCheckCorporativeOperation() {
		return checkCorporativeOperation;
	}

	/**
	 * Sets the check corporative operation.
	 *
	 * @param checkCorporativeOperation the checkCorporativeOperation to set
	 */
	public void setCheckCorporativeOperation(
			CorporativeOperation[] checkCorporativeOperation) {
		this.checkCorporativeOperation = checkCorporativeOperation;
	}
	

	/**
	 * Gets the corporate data model.
	 *
	 * @return the corporateDataModel
	 */
	public GenericDataModel<CorporativeOperation> getCorporateDataModel() {
		return corporateDataModel;
	}
	
	/**
	 * Sets the corporate data model.
	 *
	 * @param corporateDataModel the corporateDataModel to set
	 */
	public void setCorporateDataModel(
			GenericDataModel<CorporativeOperation> corporateDataModel) {
		this.corporateDataModel = corporateDataModel;
	}
	
	/**
	 * Gets the process list.
	 *
	 * @return the processList
	 */
	public List<Map<String, String>> getProcessList() {
		return processList;
	}
	
	/**
	 * Sets the process list.
	 *
	 * @param processList the processList to set
	 */
	public void setProcessList(List<Map<String, String>> processList) {
		this.processList = processList;
	}

	/**
	 * Gets the sec.
	 *
	 * @return the sec
	 */
	public Security getSec() {
		return sec;
	}
	
	/**
	 * Sets the sec.
	 *
	 * @param sec the sec to set
	 */
	public void setSec(Security sec) {
		this.sec = sec;
	}
	
	/**
	 * Gets the corp process result data model.
	 *
	 * @return the corpProcessResultDataModel
	 */
	public GenericDataModel<CorporateMonitoringDetailSearchTO> getCorpProcessResultDataModel() {
		return corpProcessResultDataModel;
	}
	
	/**
	 * Sets the corp process result data model.
	 *
	 * @param corpProcessResultDataModel the corpProcessResultDataModel to set
	 */
	public void setCorpProcessResultDataModel(
			GenericDataModel<CorporateMonitoringDetailSearchTO> corpProcessResultDataModel) {
		this.corpProcessResultDataModel = corpProcessResultDataModel;
	}
	
	/**
	 * Gets the total balance.
	 *
	 * @return the totalBalance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}
	
	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the totalBalance to set
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	
	/**
	 * Gets the available balance.
	 *
	 * @return the availableBalance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the availableBalance to set
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	
	/**
	 * Gets the custody amount.
	 *
	 * @return the custodyAmount
	 */
	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}
	
	/**
	 * Sets the custody amount.
	 *
	 * @param custodyAmount the custodyAmount to set
	 */
	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}

	/**
	 * Gets the tax amount.
	 *
	 * @return the taxAmount
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	
	/**
	 * Sets the tax amount.
	 *
	 * @param taxAmount the taxAmount to set
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	/**
	 * Gets the stock total balance.
	 *
	 * @return the stockTotalBalance
	 */
	public BigDecimal getStockTotalBalance() {
		return stockTotalBalance;
	}
	
	/**
	 * Sets the stock total balance.
	 *
	 * @param stockTotalBalance the stockTotalBalance to set
	 */
	public void setStockTotalBalance(BigDecimal stockTotalBalance) {
		this.stockTotalBalance = stockTotalBalance;
	}
	
	/**
	 * Gets the stock available balance.
	 *
	 * @return the stockAvailableBalance
	 */
	public BigDecimal getStockAvailableBalance() {
		return stockAvailableBalance;
	}
	
	/**
	 * Sets the stock available balance.
	 *
	 * @param stockAvailableBalance the stockAvailableBalance to set
	 */
	public void setStockAvailableBalance(BigDecimal stockAvailableBalance) {
		this.stockAvailableBalance = stockAvailableBalance;
	}
	
	/**
	 * Gets the cash total balance.
	 *
	 * @return the cashTotalBalance
	 */
	public BigDecimal getCashTotalBalance() {
		return cashTotalBalance;
	}
	
	/**
	 * Sets the cash total balance.
	 *
	 * @param cashTotalBalance the cashTotalBalance to set
	 */
	public void setCashTotalBalance(BigDecimal cashTotalBalance) {
		this.cashTotalBalance = cashTotalBalance;
	}
	
	/**
	 * Gets the cash available balance.
	 *
	 * @return the cashAvailableBalance
	 */
	public BigDecimal getCashAvailableBalance() {
		return cashAvailableBalance;
	}
	
	/**
	 * Sets the cash available balance.
	 *
	 * @param cashAvailableBalance the cashAvailableBalance to set
	 */
	public void setCashAvailableBalance(BigDecimal cashAvailableBalance) {
		this.cashAvailableBalance = cashAvailableBalance;
	}
	
	/**
	 * Gets the detail search data model.
	 *
	 * @return the detailSearchDataModel
	 */
	public GenericDataModel<CorporativeProcessDetailsTO> getDetailSearchDataModel() {
		return detailSearchDataModel;
	}
	
	/**
	 * Sets the detail search data model.
	 *
	 * @param detailSearchDataModel the detailSearchDataModel to set
	 */
	public void setDetailSearchDataModel(
			GenericDataModel<CorporativeProcessDetailsTO> detailSearchDataModel) {
		this.detailSearchDataModel = detailSearchDataModel;
	}
	
	/**
	 * Gets the lst process type.
	 *
	 * @return the lstProcessType
	 */
	public List<ParameterTable> getLstProcessType() {
		return lstProcessType;
	}

	/**
	 * Sets the lst process type.
	 *
	 * @param lstProcessType the lstProcessType to set
	 */
	public void setLstProcessType(List<ParameterTable> lstProcessType) {
		this.lstProcessType = lstProcessType;
	}

	/**
	 * Search CorporativeOperation listener.
	 */
	@LoggerAuditWeb
	public void searchCorporateListener() {
		try {		
			searchCorporatOperation();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Fills with Search result data.
	 */
	@LoggerAuditWeb
	public void searchCorporatOperation() {
		hideDialogs();
		try {
			excision = false;
			destinySecurity = null;
			checkCorporativeOperation = null;
			List<Date> listDate=new ArrayList<Date>();
			
			if (corporateMonitoringFilter.getTypeDateCorporative()!=null){
//				if (corporateMonitoringFilter.getTypeDateCorporative().equals(1)){
//					corporateMonitoringFilter.setExpirationDate(corporateMonitoringFilter.getSelectedDate());
//					listDate.add(corporateMonitoringFilter.getExpirationDate());
//					corporateMonitoringFilter.setListExpirationDate(listDate);
//				}else if (corporateMonitoringFilter.getTypeDateCorporative().equals(2)){
//					corporateMonitoringFilter.setProcessDate(corporateMonitoringFilter.getSelectedDate());
//				}
				List<CorporativeOperation> lstCorporativeOp=corporateServiceFacade.searchCorporativeOperationsByCoupons(corporateMonitoringFilter);
				corporateDataModel = new GenericDataModel<CorporativeOperation>(lstCorporativeOp);
			}
			
			
			//aqui el mensaje de no existen datos
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * View security financial importance event.
	 *
	 * @param securityCode the security code
	 */
	public void viewSecurityFinancialImportanceEvent(String securityCode)
	{
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		  securityData.setSecurityCodePk(securityCode);
		  securityData.setUiComponentName("securityHelp");
		  showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	
	/**
	 * To Clear the data.
	 */
	public void cleanCorporateListener(){
		try {
			
			cleanCorporate();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * To show the details of the selected location on detail page.
	 *
	 * @param ae the ae
	 * @return string
	 */
	public String getSelectedDetails(ActionEvent ae) {
		return "registerImportanceEvent";
	}
	
	/**
	 * Confirm Issuer Action.
	 */
	public void confirmEmisorListener(){
		hideDialogs();	
		try{
			if(userPrivilege.getUserAcctions().isConfirmIssuer()){
				if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){		
					JSFUtilities.showComponent(":idCnfIssuer");
					JSFUtilities.showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null,PropertiesConstants.MSG_DEPOSITARY_PRELIMINARY_CONFIRM, null);						
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
					JSFUtilities.showSimpleValidationDialog();					
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_PRIVILEGE));
				JSFUtilities.showSimpleValidationDialog();				
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Confirm Issuer Action.
	 */
	public void confirmExecuteStock(){
		hideDialogs();	
		try{
			if(userPrivilege.getUserAcctions().isConfirmIssuer()){
				if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){		
					JSFUtilities.showComponent(":idStockCalc");
					JSFUtilities.showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null,PropertiesConstants.STOCK_QUESTION_SAVE, null);						
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
					JSFUtilities.showSimpleValidationDialog();					
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_PRIVILEGE));
				JSFUtilities.showSimpleValidationDialog();				
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Confirm cevaldom action.
	 */
	public void confirmDepositaryListener(){
		hideDialogs();
		try{
			if(userPrivilege.getUserAcctions().isConfirmDepositary()){
				if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){
					JSFUtilities.showComponent(":idCnfDepositary");
					JSFUtilities.showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null,PropertiesConstants.MSG_DEPOSITARY_PRELIMINARY_CONFIRM, null);
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
					JSFUtilities.showSimpleValidationDialog();						
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_PRIVILEGE));
				JSFUtilities.showSimpleValidationDialog();	
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Confirm preliminar listener.
	 */
	@LoggerAuditWeb
	public void confirmPreliminarListener(){
		hideDialogs();	
		boolean isAmortization = false, isInteres=false;
		try{
			if(userPrivilege.getUserAcctions().isPreliminary()){
				if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){
					List<Long> errorProcess = new ArrayList<Long>();
					List<Long> errorProcessAux = new ArrayList<Long>();
					List<Long> errorProcessAux2 = new ArrayList<Long>();
					for(CorporativeOperation process : checkCorporativeOperation){
						if((process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode()))
							&& corporateProcessServiceFacade.validateBalanceForSpecial(process.getSecurities().getIdSecurityCodePk(),CorporateProcessStateType.PRELIMINARY.getCode())){
							errorProcess.add(process.getIdCorporativeOperationPk());
						}
//						if(process.getSecurities().getSecurityClass().equals(SecurityClassType.DPF.getCode()) || 
//								process.getSecurities().getSecurityClass().equals(SecurityClassType.DPA.getCode())){
//							errorProcessAux.add(process.getIdCorporativeOperationPk());
//						}
						if(process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
								&& corporateProcessServiceFacade.validateConvertibilyBoneAction(process.getSecurities().getIdSecurityCodePk())){
							errorProcessAux2.add(process.getIdCorporativeOperationPk());
						}
						
						if((process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())))
							isAmortization = true;
						if((process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())))
							isInteres = true;
						
					}
					if(isAmortization && isInteres){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_PRELIMINAR_NOT_AMORTIZATION_INTERES));
						JSFUtilities.showSimpleValidationDialog();	
						return;
					}
					
					if(errorProcessAux!=null && errorProcessAux.size()>0){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, 
								null, PropertiesConstants.MSG_CORPORATE_EVENT_DPF_DPA_NOT_VALIDATE, new Object[]{StringUtils.join(errorProcessAux,",")});
						JSFUtilities.showSimpleValidationDialog();	
					}else{
						if(errorProcess!=null && errorProcess.size()>0){
							JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, 
									null, PropertiesConstants.MSG_CORPORATE_EVENT_NOT_VALIDATE, new Object[]{StringUtils.join(errorProcess,",")});
							JSFUtilities.showSimpleValidationDialog();	
						}else if(errorProcessAux2!=null && errorProcessAux2.size()>0){
							JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, 
									null, PropertiesConstants.MSG_CORPORATE_EVENT_CONVERTIBILY_BONE_ACTION_VALIDATE, new Object[]{StringUtils.join(errorProcessAux2,",")});
							JSFUtilities.showSimpleValidationDialog();	
						}else{						
							JSFUtilities.showComponent(":idCnfPre");
							showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null,PropertiesConstants.MSG_PRELIMINARY_CONFIRM, null);
//							JSFUtilities.executeJavascriptFunction("PF('idCnfPreWin').show()");
							
						}	
					}									
				}else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
					JSFUtilities.showSimpleValidationDialog();	
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_PRIVILEGE));
				JSFUtilities.showSimpleValidationDialog();	
			}
			}catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	/**
	 * Confirm preliminar.
	 */
	@LoggerAuditWeb
	public void confirmPreliminar(){
//		hideDialogs();
		try{
			if(userPrivilege.getUserAcctions().isPreliminary()){
				if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){	
					List<CorporativeOperation> lstProcesses = new ArrayList<CorporativeOperation>();					
					List<Long> corporateOperationsNotAmount = new ArrayList<>();
					for(CorporativeOperation process : checkCorporativeOperation){
						if(process.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode()) && 
								(interestProcess.equals(process.getCorporativeEventType().getCorporativeEventType()) 
								|| amortizationProcess.equals(process.getCorporativeEventType().getCorporativeEventType()))){
//							if(process.getIssuerConfirmedAmount() == null || (process.getIssuerConfirmedAmount() !=null &&
//									process.getIssuerConfirmedAmount().compareTo(BigDecimal.ZERO) <= 0)){
//								BigDecimal desBalance = corporateProcessServiceFacade.searchDesmaterializedBalanceSecurity(process.getSecurities().getIdSecurityCodePk());
//								if(desBalance.compareTo(BigDecimal.ZERO) > 0 
//										&& corporateProcessServiceFacade.hasBalance(process.getSecurities().getIdSecurityCodePk())){
//									corporateOperationsNotAmount.add(process.getIdCorporativeOperationPk());
//								}
//							}
						}
						lstProcesses.add(process);
					}
					if(corporateOperationsNotAmount.size()>0){
						JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
//						JSFUtilities.executeJavascriptFunction("PF('idDialDepositaryWin').show()");
						JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, null, 
								PropertiesConstants.ISSUER_NOT_ENTER_AMOUNT, new Object[]{StringUtils.join(corporateOperationsNotAmount,",")});
						return;
					}
					corporateProcessServiceFacade.executePreliminaryProcess(lstProcesses);
					JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
//					JSFUtilities.executeJavascriptFunction("PF('idDialDepositaryWin').show()");
						JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_SUCCESS_HED, null,PropertiesConstants.MESSAGE_SUCCESS_PRELIMINAR, null);						
						checkCorporativeOperation = null;
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
					JSFUtilities.showSimpleValidationDialog();	
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_PRIVILEGE));
				JSFUtilities.showSimpleValidationDialog();	
			}
			setPrivilegeComponent();
		} catch(ServiceException esx){
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());				
				JSFUtilities.showSimpleValidationDialog();				
				setPrivilegeComponent();
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Process automatic.
	 */
	@LoggerAuditWeb
	public void processAutomatic(){
		hideDialogs();	
		try{				
		    Map<String, Object> param = new HashMap<String, Object>();				    
		    corporateProcessServiceFacade.registerBatch(param,BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Process definitive automatic.
	 */
	@LoggerAuditWeb
	public void processDefinitiveAutomatic(){
		hideDialogs();	
		try{				
		    Map<String, Object> param = new HashMap<String, Object>();				    
		    corporateProcessServiceFacade.registerBatch(param,BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC_DEFINITIVE.getCode());
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Process stock monthly automatic.
	 */
	@LoggerAuditWeb
	public void processStockMonthlyAutomatic(){
		hideDialogs();	
		try{				
		    Map<String, Object> param = new HashMap<String, Object>();				    
		    corporateProcessServiceFacade.registerBatch(param,BusinessProcessType.STOCK_CALCULATION_AUTOMATIC_EXECUTION_MONTHLY.getCode());
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * This method is used to execute the Definitive Process.
	 * Date : 22nd July,2013
	 */

	private Client createClientJerser() throws NoSuchAlgorithmException,KeyManagementException{
		return Client.create();
	}
	
	@LoggerAuditWeb
	public void createRetirementOperation() {
		
		if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){	
			for(CorporativeOperation process : checkCorporativeOperation){
				
		    	StringBuilder urlResource = new StringBuilder();
		        urlResource.append(serverPath).append("/").append(ModuleWarType.CUSTODY.getValue()).
		        append("/resources/CustodyProcessResource/retirementoperation").
		        append("/").append(process.getIdCorporativeOperationPk());
		        try {
					createClientJerser().resource(urlResource.toString()).type("application/json").get(ClientResponse.class);
				} catch (KeyManagementException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UniformInterfaceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClientHandlerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
			}
		}

	}
	
	@LoggerAuditWeb
	public void confirmDefinitivoListener(){
		hideDialogs();	
		try{
			if(userPrivilege.getUserAcctions().isDefinitive()){
				if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){		
					List<Long> errorProcess = new ArrayList<Long>();
					List<Long> errorProcessAux = new ArrayList<Long>();
					List<Long> errorProcessAux2 = new ArrayList<Long>();
					List<Long> errorProcessAmortization = new ArrayList<Long>();
					for(CorporativeOperation process : checkCorporativeOperation){
						if((process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
								||process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode()))
							&& corporateProcessServiceFacade.validateBalanceForSpecial(process.getSecurities().getIdSecurityCodePk(),CorporateProcessStateType.DEFINITIVE.getCode())){
							errorProcess.add(process.getIdCorporativeOperationPk());
						}
//						if(process.getSecurities().getSecurityClass().equals(SecurityClassType.DPF.getCode()) || 
//								process.getSecurities().getSecurityClass().equals(SecurityClassType.DPA.getCode())){
//							errorProcessAux.add(process.getIdCorporativeOperationPk());
//						}
						//si afecta saldo entonces es la ultima amortizacion - validaremos que esten en definitivo todos los intereses
						if(process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode()) && 
								process.getIndAffectBalance().equals(CorporateProcessConstants.IND_YES) && 
								corporateProcessServiceFacade.validateUltimateAmortization(process.getSecurities().getIdSecurityCodePk())){
							errorProcessAmortization.add(process.getIdCorporativeOperationPk());
						}
						if(process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
								&& corporateProcessServiceFacade.validateConvertibilyBoneAction(process.getSecurities().getIdSecurityCodePk())){
							errorProcessAux2.add(process.getIdCorporativeOperationPk());
						}
					}
//					if(errorProcessAux!=null && errorProcessAux.size()>0){
//						JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, 
//								null, PropertiesConstants.MSG_CORPORATE_EVENT_DPF_DPA_NOT_VALIDATE, new Object[]{StringUtils.join(errorProcessAux,",")});
//						JSFUtilities.showSimpleValidationDialog();	
//					}else 
						if(errorProcessAmortization!=null && errorProcessAmortization.size()>0){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, 
								null, PropertiesConstants.MSG_CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE, new Object[]{StringUtils.join(errorProcessAmortization,",")});
						JSFUtilities.showSimpleValidationDialog();	
					}else if(errorProcess!=null && errorProcess.size()>0){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, 
								null, PropertiesConstants.MSG_CORPORATE_EVENT_NOT_VALIDATE, new Object[]{StringUtils.join(errorProcess,",")});
						JSFUtilities.showSimpleValidationDialog();	
					}else if(errorProcessAux2!=null && errorProcessAux2.size()>0){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, 
								null, PropertiesConstants.MSG_CORPORATE_EVENT_CONVERTIBILY_BONE_ACTION_VALIDATE, new Object[]{StringUtils.join(errorProcessAux2,",")});
						JSFUtilities.showSimpleValidationDialog();	
					}else{						
						JSFUtilities.showComponent(":idCnfDef");
//						JSFUtilities.executeJavascriptFunction("PF('idCnfDefWin').show()");
						JSFUtilities.showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null,PropertiesConstants.MSG_DEFINITIVE_CONFIRM, null);
					}									
				}else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
					JSFUtilities.showSimpleValidationDialog();						
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_PRIVILEGE));
				JSFUtilities.showSimpleValidationDialog();					
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * This method is used to execute the Definitive Process.
	 * Date : 22nd July,2013
	 */
	@LoggerAuditWeb
	public void confirmDefinitivo(){
//		hideDialogs();
		try{
			if(userPrivilege.getUserAcctions().isDefinitive()){
				if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){	
					List<CorporativeOperation> lstProcesses = new ArrayList<CorporativeOperation>();				
					List<Long> corporateOperationsNotAmount = new ArrayList<>();
					for(CorporativeOperation process : checkCorporativeOperation){
						if(process.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode()) && 
								(interestProcess.equals(process.getCorporativeEventType().getCorporativeEventType()) 
								|| amortizationProcess.equals(process.getCorporativeEventType().getCorporativeEventType()))){
//							if(process.getIssuerConfirmedAmount() == null || (process.getIssuerConfirmedAmount() !=null &&
//									process.getIssuerConfirmedAmount().compareTo(BigDecimal.ZERO) <= 0)){
//								BigDecimal desBalance = corporateProcessServiceFacade.searchDesmaterializedBalanceSecurity(process.getSecurities().getIdSecurityCodePk());
//								if(desBalance.compareTo(BigDecimal.ZERO) > 0 
//										&& corporateProcessServiceFacade.hasBalance(process.getSecurities().getIdSecurityCodePk())){
//									corporateOperationsNotAmount.add(process.getIdCorporativeOperationPk());
//								}
//							}
						} 
						lstProcesses.add(process);
					}
					if(corporateOperationsNotAmount.size()>0){
						JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
//						JSFUtilities.executeJavascriptFunction("PF('idDialDepositaryWin').show()");
						JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, null, 
								PropertiesConstants.ISSUER_NOT_ENTER_AMOUNT, new Object[]{StringUtils.join(corporateOperationsNotAmount,",")});
						return;
					}
					corporateProcessServiceFacade.executeDefinitiveProcess(lstProcesses);
					createRetirementOperation();
					JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
//					JSFUtilities.executeJavascriptFunction("PF('idDialDepositaryWin').show()");
					JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_SUCCESS_HED, null,PropertiesConstants.MESSAGE_SUCCESS_DEFINITVE, null);
					checkCorporativeOperation = null;
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
					JSFUtilities.showSimpleValidationDialog();						
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_PRIVILEGE));
				JSFUtilities.showSimpleValidationDialog();					
			}
			setPrivilegeComponent();
		} catch(ServiceException esx){
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();					
				setPrivilegeComponent();
			} 
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}

	
	/**
	 * To clear the data.
	 */
	public void cleanCorporate(){
		corporateMonitoringFilter = new CorporateMonitoringTO();	
		corporateMonitoringFilter.setIssuer(new Issuer());
		corporateMonitoringFilter.setSecurities(new Security());
		corporateMonitoringFilter.setExpirationDate(CommonsUtilities.currentDate());
		corporateMonitoringFilter.setExpirationDateFinal(CommonsUtilities.currentDate());
		corporateMonitoringFilter.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
		cleanCorporateDataModel();
		checkCorporativeOperation = null;
		securitiesHelper.setSecurityDescription(GeneralConstants.EMPTY_STRING);		
		try{
			if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				depositaryUser=true;
				issuerUser=false;
			}
			if( userInfo.getUserAccountSession().isIssuerInstitucion()) {
				issuerUser=true;
				IssuerSearcherTO issuerTo = new IssuerSearcherTO();
				issuerTo.setIssuerCode(userInfo.getUserAccountSession().getIssuerCode());
				Issuer issuer =	corporateServiceFacade.getIssuerDetails(issuerTo);
				corporateMonitoringFilter.setIssuer(issuer);			
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		corporateMonitoringFilter.setTypeDateCorporative(1);
		corporateMonitoringFilter.setSelectedDate(CommonsUtilities.currentDate());
		
	}
	
	/**
	 * Clean corporate data model.
	 */
	public void selectTypeDate(){
		
		
	}
	
	
	public void cleanSelectTypeDate(){
		corporateDataModel = null;
		corporateMonitoringFilter.setSelectedDate(CommonsUtilities.currentDate());
		corporateMonitoringFilter.setSecurityClass(null);
		corporateMonitoringFilter.setSecurityCurrency(null);
		if (corporateMonitoringFilter.getTypeDateCorporative()!=null && corporateMonitoringFilter.getTypeDateCorporative().equals(1)){
			labelDateSearch="Vencimiento";
		}else if (corporateMonitoringFilter.getTypeDateCorporative()!=null && corporateMonitoringFilter.getTypeDateCorporative().equals(2)){
			labelDateSearch="Entrega";
		}else{
			labelDateSearch="";
		}
		
	}
	
	public void cleanCorporateDataModel(){
		corporateDataModel = null;
		corporateMonitoringFilter.setSelectedDate(CommonsUtilities.currentDate());
		corporateMonitoringFilter.setSecurityClass(null);
		corporateMonitoringFilter.setSecurityCurrency(null);
	}
	
	public void cleanCorporateDataModelDate(){
		corporateDataModel = null;
//		corporateMonitoringFilter.setSelectedDate(CommonsUtilities.currentDate());
		corporateMonitoringFilter.setSecurityClass(null);
		corporateMonitoringFilter.setSecurityCurrency(null);
	}
	
	/**
	 * Detail corporate oper.
	 *
	 * @param selectedCorpBean the selected corp bean
	 * @return the string
	 */
	public String detailCorporateOper(CorporativeOperation selectedCorpBean){
		try{
			importanceEventBeanDetail.setMonitoring(true);
			importanceEventBeanDetail.setSelectedCorporativeOpertation(selectedCorpBean);
			importanceEventBeanDetail.loadDetailInfoListener();
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return "detailImportanceEvent";
	}
	
	/**
	 * It hides all show dialogs.
	 */
	//YA NO SE USARA
	private void hideDialogs(){	
		JSFUtilities.hideGeneralDialogues();		
		JSFUtilities.hideComponent("adjustForm:dlgMonto");
		JSFUtilities.hideComponent("idDialDepositary");
		JSFUtilities.hideComponent("idCnfDepositary");
		JSFUtilities.hideComponent("idCnfIssuer");
		JSFUtilities.hideComponent("idCnfPre");
		JSFUtilities.hideComponent("idCnfDef");
		JSFUtilities.hideComponent("adjustForm:idAajustDialog");
		JSFUtilities.hideComponent("adjustForm:cnfBeforeReturn");
		JSFUtilities.hideComponent("adjustForm:cnfBeforeSave");
		
	}
	
	/**
	 * Confirm actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmActions(){		//aler -> ok

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";
		
		if(tmpAction.equals("saveSucess")){
			return null; //pintar algo en caso de redireccionar
		}
		
		return null;
	}
	
	/**
	 * Confirm alert actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmAlertActions(){	// alert -> questions
		
		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";
		
		if(tmpAction.equals("save")){
			//metodo
		}
		
		return "";
	}

	
	/**
	 * Find holder adjustments.
	 */
	public void findHolderAdjustments(){
//		hideDialogs();
		try{
			holderAdjModified = false;
			
			if(holder != null && holder.getIdHolderPk() !=null && holder.getIdHolderPk() > 0){
			
				if((holderAccountType == 0) || (holderAccountType == 1 && selectedJointHolder != null )){
					
					String holderId = "("+holder.getIdHolderPk()+")";
					if(holderAccountType == 1){
						holderId = selectedJointHolder.getParticipant();
					}
					if(!ImportanceEventType.SECURITIES_EXCISION.getCode().equals(selectedCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
						destinySecurity = null;
					}
					
					CorporativeProcessDetailsTO result = corporateServiceFacade.getHolderAdjustmentDetails(selectedCorporativeOperation, destinySecurity, holderId);
					stockTotalBalance = result.getSourceTotalBalance();
					stockAvailableBalance = result.getSourceAvailableBalance();
					cashTotalBalance = result.getDestinyTotalBalance();
					taxAmount = result.getTaxAmount();
					totalBalance = result.getNetTotalBalance();
					custodyAmount = result.getCustodyAmount();
					availableBalance = result.getNetAvailableBalance();
					adjustedBalance = result.getAdjustedBalance();
					detailSearchDataModel = new GenericDataModel<CorporativeProcessDetailsTO>(result.getDetails());
					
					if(detailSearchDataModel.getSize() == 0){	
						holderTypeChange();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_NO_ADJUSTEMTNS));
						JSFUtilities.showSimpleValidationDialog();
					}
				}else{
					holderTypeChange();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.JOINTHOLDER_SELECT_SINGLE_RECORD));
					JSFUtilities.showSimpleValidationDialog();		
				}
			
			}else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_NO_HOLDER));
				JSFUtilities.showSimpleValidationDialog();
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean adjust account details.
	 */
	public void cleanAdjustAccountDetails(){
		try{
			hideDialogs();
			JSFUtilities.resetComponent("adjustForm:flsCorporativeEvent");
			selectedCorporativeOperation=checkCorporativeOperation[0];
			adjustCorporateViewFlag=false;
			holderAdjModified = false;
			holder = new Holder();
			setProcessType();
			setStockSplitDetails();
			holderAccountType = 0;	
			detailSearchDataModel= null;
			jointHolders = null;
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * To populate data in the Preliminary Adjustment window.
	 *
	 * @return the String type
	 */
	public String adjustAccountDetails(){
		hideDialogs();
		String page=null;
		try{
			if(userPrivilege.getUserAcctions().isAdjustments()){
				if(checkCorporativeOperation!=null && checkCorporativeOperation.length==1){
					CorporativeOperation objCorporativeOperation = checkCorporativeOperation[0];
					if(objCorporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
							||objCorporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())
							||objCorporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())
							||objCorporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())
							||objCorporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())
							||objCorporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
						selectedCorporativeOperation=checkCorporativeOperation[0];					
						Integer corporativeState = selectedCorporativeOperation.getState();
						selectedCorporativeOperation.setStateDescription(CorporateProcessStateType.get(corporativeState).getValue());				
						adjustCorporateViewFlag=false;
						holder = new Holder();
						setProcessType();
						setStockSplitDetails();
						holderAccountType = 0;	
						jointHolders = null;
						detailSearchDataModel= null;					
						page="adjustCorporativeMgmt";
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_CORPORATE_EVENT_BENF_NOT_VALIDATE));
						JSFUtilities.showSimpleValidationDialog();	
					}					
				}else if(checkCorporativeOperation!=null && checkCorporativeOperation.length>1){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_SELECT_SINGLE_RECORD));
					JSFUtilities.showSimpleValidationDialog();						
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
					JSFUtilities.showSimpleValidationDialog();						
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_PRIVILEGE));
				JSFUtilities.showSimpleValidationDialog();					
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return page;
	}
	
	/**
	 * Sets the process type.
	 */
	public void setProcessType(){
		shareBenefit = false;
		cashBenefit = false;
		exchangeCashBenefit = false;
		specialProcess = false;
		remnantBenefit = false;
		if(CorporateProcessUtils.isSpecialCorporateProcess(selectedCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			specialProcess = true;
		}else if(CorporateProcessUtils.isCashBenefitCorporateProcess(selectedCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			cashBenefit = true;
			if(selectedCorporativeOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					selectedCorporativeOperation.getCurrency().equals(CurrencyType.DMV.getCode())){
				exchangeCashBenefit = true;
			}			
		}else if(CorporateProcessUtils.isShareBenefitCorporateProcess(selectedCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			shareBenefit = true;
		}
		if(ImportanceEventType.REMANENT_PAYMENT.getCode().equals(selectedCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			remnantBenefit = true;
		}
	}
	
	/**
	 * Sets the stock split details.
	 *
	 * @throws ServiceException the service exception
	 */
	public void setStockSplitDetails() throws ServiceException{
		destinySecurity = null;
		excision = false;
		if(ImportanceEventType.SECURITIES_EXCISION.getCode().equals(selectedCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			destinySecurities = corporateServiceFacade.getExcisionSecurities(selectedCorporativeOperation.getIdCorporativeOperationPk());
			if(destinySecurities != null && !destinySecurities.isEmpty()){
				destinySecurity = destinySecurities.get(0).getParameterTableCd();
				excision = true;
			}
		}
	}
	
	/**
	 * To populate data in Details link page.
	 *
	 * @param selectedCorpOper the selected record details
	 * @param ajusteFlag the ajuste flag
	 * @return the String type
	 */
	public String corporateDetails(CorporativeOperation selectedCorpOper, boolean ajusteFlag){
		hideDialogs();
		String page=null;
		try{		
			selectedCorporativeOperation=selectedCorpOper;
			setProcessType();
			setStockSplitDetails();
			CorporativeProcessDetailsTO detailResults=corporateServiceFacade.getCorporativeProcessDetails(selectedCorporativeOperation, destinySecurity, ajusteFlag);
			if(selectedCorporativeOperation.getIndRound() != null && selectedCorporativeOperation.getIndRound().intValue() == CorporateProcessConstants.IND_ROUND ){
				selectedCorporativeOperation.setIndRoundValue(true);
			}
			if(selectedCorporativeOperation.getIndRemanent() != null && selectedCorporativeOperation.getIndRemanent().intValue() == CorporateProcessConstants.IND_REMAINDER ){
				selectedCorporativeOperation.setIndRemanentValue(true);
			}
			//Instrument
			for(ParameterTable param :instrumenteTypes){
				if(param.getParameterTablePk().equals(selectedCorporativeOperation.getSecurities().getInstrumentType())){
					selectedCorporativeOperation.getSecurities().setInstrumentTypeDescription(param.getParameterName());
				}
			}
			//currency
			for(ParameterTable param: lstCurrency){
				if(param.getParameterTablePk().equals(selectedCorporativeOperation.getSecurities().getCurrency())){
					selectedCorporativeOperation.getSecurities().setCurrencyTypeDescription(param.getDescription());
				}
			}
			//state description
			selectedCorporativeOperation.setStateDescription(corporateStateMap.get(selectedCorporativeOperation.getState()));
			stockTotalBalance = detailResults.getSourceTotalBalance();
			stockAvailableBalance = detailResults.getSourceAvailableBalance();
			cashTotalBalance = detailResults.getDestinyTotalBalance();
			cashAvailableBalance = detailResults.getDestinyAvailableBalance();
			taxAmount = detailResults.getTaxAmount();
			custodyAmount = detailResults.getCustodyAmount();
			totalBalance = detailResults.getNetTotalBalance();
			availableBalance = detailResults.getNetAvailableBalance();
			blockBalance = detailResults.getTotalBlockedBalance();
			adjustedBalance = detailResults.getAdjustedBalance();
			detailSearchDataModel=new GenericDataModel<CorporativeProcessDetailsTO>(detailResults.getDetails());
			if(ajusteFlag){
				page="adjustCorporativeDetailPg";
			}else{
				page="corporativeAdjustDetailPg";
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return page;
	}
	
	/**
	 * Gets the excision security details.
	 *
	 * @param ajusteFlag the ajuste flag
	 * @return the excision security details
	 */
	public void getExcisionSecurityDetails(boolean ajusteFlag){
		hideDialogs();	
		try{
			
			CorporativeProcessDetailsTO detailResults=corporateServiceFacade.getCorporativeProcessDetails(selectedCorporativeOperation, destinySecurity, ajusteFlag);
			stockTotalBalance = detailResults.getSourceTotalBalance();
			stockAvailableBalance = detailResults.getSourceAvailableBalance();
			cashTotalBalance = detailResults.getDestinyTotalBalance();
			cashAvailableBalance = detailResults.getDestinyAvailableBalance();
			taxAmount = detailResults.getTaxAmount();
			custodyAmount = detailResults.getCustodyAmount();
			totalBalance = detailResults.getNetTotalBalance();
			availableBalance = detailResults.getNetAvailableBalance();
			blockBalance = detailResults.getTotalBlockedBalance();
			adjustedBalance = detailResults.getAdjustedBalance();
			detailSearchDataModel=new GenericDataModel<CorporativeProcessDetailsTO>(detailResults.getDetails());

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * It executes when click on the back button.
	 *
	 * @return the String type
	 */
	public String backMonitorPage(){
		hideDialogs();
		selectedCorporativeOperation=null;
		checkCorporativeOperation=null;
		return "corporateMonitoring";
	}
	
	/**
	 * Back ajuste mgmt page.
	 *
	 * @return the string
	 */
	public String backAjusteMgmtPage(){
		hideDialogs();
		if(holderAdjModified){
//			JSFUtilities.showComponent("adjustForm:cnfBeforeReturn");
			return null;
		}else{
			selectedCorporativeOperation=null;
			checkCorporativeOperation=null;
			return "corporateMonitoring";
		}
	}
	
	/**
	 * Save holder adjustmetns listener.
	 */
	public void saveHolderAdjustmetnsListener(){
		hideDialogs();
		if(holderAdjModified){
			JSFUtilities.showComponent("adjustForm:cnfBeforeSave");
		}else{
			JSFUtilities.showComponent("cnfEndTransactionSamePage");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGE_ALERT_HEADER, null, PropertiesConstants.JOINTHOLDER_NO_DATA_MODIFED, null);
		}
	}
	
	/**
	 * Open the preliminary Adjustment amounts modification pop up.
	 *
	 * @param newData the CorporateMonitoringDetailSearchFilter
	 */
	public void showNewMoneyWindow(CorporateMonitoringDetailSearchTO newData){
		hideDialogs();
		saveHolderAccountAdjustment=newData;
		HolderAccountAdjusment accAdj=newData.getHolderAccountAdjusment();
		//Setting existed amounts in modifications pop up
		if(accAdj!=null){
		newCustodyAmt=accAdj.getNewCommissiones();
		newTaxAmt=accAdj.getNewTax();
		newTotalAmt=accAdj.getNewBenefitAmount();
		}
		// remove the previous amounts in pop up 
		else{
			newCustodyAmt=null;
			newTaxAmt=null;
			newTotalAmt=null;
		}
		JSFUtilities.showComponent("adjustForm:dlgMonto");
	}
	
	
	 /**
 	 * It used for loading data from parameter tables.
 	 *
 	 * @param masterPk the Integer
 	 * @return the List<ParameterTable>
 	 * @throws ServiceException the service exception
 	 */
	private List<ParameterTable> getParameter(Integer masterPk) throws ServiceException{
		 ParameterTableTO processStateTo = new ParameterTableTO();
			processStateTo.setMasterTableFk(masterPk);
		return	generalParametersFacade.getListParameterTableServiceBean(processStateTo); 
	 }
	
	private List<ParameterTable> getParameterTypeDate(){
		
		ParameterTable processTypeDateTo;
		List<ParameterTable> listParameterTable = new ArrayList<>();
		
		processTypeDateTo = new ParameterTable();
		processTypeDateTo.setParameterTablePk(1);
		processTypeDateTo.setText1("FECHA VENCIMIENTO");
		listParameterTable.add(processTypeDateTo);
		
		processTypeDateTo = new ParameterTable();
		processTypeDateTo.setParameterTablePk(2);
		processTypeDateTo.setText1("FECHA ENTREGA");
		listParameterTable.add(processTypeDateTo);
		
		return  listParameterTable;
	 }
	 
	 /**
 	 * It fills the window loading data.
 	 *
 	 * @throws ServiceException the service exception
 	 */
	private void fillWindowLoadData()throws ServiceException{
		 maxDate=CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(),4);
		 setPrivilegeComponent();
		 lstCurrency=getParameter(MasterTableType.CURRENCY.getCode());
		 //Loading Corporative Process States 
		 lstCorpProcessState = getParameter(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode());
		 corporateStateMap = new HashMap<Integer, String>();
		 for(ParameterTable parm:lstCorpProcessState){
			 corporateStateMap.put(parm.getParameterTablePk(), parm.getParameterName());
		 }
		 ////Loading Corporative Event types 
		 lstProcessType = getParameter(MasterTableType.CORPORATIVE_EVENT.getCode());
		 //instruments
		 instrumenteTypes = getParameter(MasterTableType.INSTRUMENT_TYPE.getCode());
	 }
	 
 	/**
 	 * Sets the privilege component.
 	 */
 	public void setPrivilegeComponent(){
		 PrivilegeComponent comp = new PrivilegeComponent();
		 if(userPrivilege.getUserAcctions() != null){
			 comp.setBtnAdd(userPrivilege.getUserAcctions().isAdd());
			 comp.setBtnApply(userPrivilege.getUserAcctions().isApply());
			 comp.setBtnApproveView(userPrivilege.getUserAcctions().isApprove());
			 comp.setBtnConfirmView(userPrivilege.getUserAcctions().isConfirm());
			 comp.setBtnReview(userPrivilege.getUserAcctions().isReview());
			 comp.setBtnSearchView(userPrivilege.getUserAcctions().isSearch());
		 }
		 userPrivilege.setPrivilegeComponent(comp);
	 }
	
	/**
	 * Clear confirm depositary.
	 *
	 * @return the string
	 */
	public String clearConfirmDepositary(){
		hideDialogs();
		searchCorporatOperation();
		setPrivilegeComponent();
		return "corporateMonitoring";
	}
	
	/**
	 * It executes the cevaldom confirm operation.
	 */
	@LoggerAuditWeb
	public void confirmDepositaryOperation(){
		try{			
			if(checkCorporativeOperation!=null && checkCorporativeOperation.length>0){	
				List<CorporativeOperation> lstProcesses = new ArrayList<CorporativeOperation>();
				List<Long> corporateOperations = new ArrayList<Long>();				
				List<Long> corporateOperationsNotAmount = new ArrayList<Long>();
				List<String> depositaryNotPaymentSecurity = new ArrayList<String>();
				for(CorporativeOperation process : checkCorporativeOperation){
					if(interestProcess.equals(process.getCorporativeEventType().getCorporativeEventType()) 
							|| amortizationProcess.equals(process.getCorporativeEventType().getCorporativeEventType())){
//						if(process.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode()) && 
//								(process.getIssuerConfirmedAmount() == null || (process.getIssuerConfirmedAmount() !=null &&
//								process.getIssuerConfirmedAmount().compareTo(BigDecimal.ZERO) <= 0))){
//							BigDecimal desBalance = corporateProcessServiceFacade.searchDesmaterializedBalanceSecurity(process.getSecurities().getIdSecurityCodePk());
//							if(desBalance.compareTo(BigDecimal.ZERO) > 0 
//									&& corporateProcessServiceFacade.hasBalance(process.getSecurities().getIdSecurityCodePk())){
//								corporateOperationsNotAmount.add(process.getIdCorporativeOperationPk());
//							}
//						}
						if(process.getSecurities().getIndPaymentBenefit().equals(BooleanType.NO.getCode())
								&& !depositaryNotPaymentSecurity.contains(process.getSecurities().getIdSecurityCodePk())){
							depositaryNotPaymentSecurity.add(process.getSecurities().getIdSecurityCodePk());
						}
					}
					lstProcesses.add(process);
					corporateOperations.add(process.getIdCorporativeOperationPk());
				}
				if(corporateOperationsNotAmount.size()>0){
					JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
					JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, null, 
							PropertiesConstants.ISSUER_NOT_ENTER_AMOUNT, new Object[]{StringUtils.join(corporateOperationsNotAmount,",")});
					return;
				}			
				if(depositaryNotPaymentSecurity.size()>0){
					JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
					JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, null, 
							PropertiesConstants.DEPOSITARY_NOT_PAYMENT, new Object[]{StringUtils.join(depositaryNotPaymentSecurity,",")});
					return;
				}
				corporateProcessServiceFacade.executeConfirmationDepositary(lstProcesses);
				JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
					JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_SUCCESS_HED, null,PropertiesConstants.MSG_DEPOSITARY_PRELIMINARY_SUCC, new Object[]{StringUtils.join(corporateOperations,",")});
					checkCorporativeOperation = null;
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVEID_NOT_SELECT));
				JSFUtilities.showSimpleValidationDialog();	
			}
			setPrivilegeComponent();
		}catch(ServiceException esx){
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();		
				setPrivilegeComponent();
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Disable issuer amount.
	 *
	 * @param state the state
	 * @param situation the situation
	 * @return true, if successful
	 */
	public boolean disableIssuerAmount(Integer state, Integer situation){
		 if(issuerUser){
			 if (situation==null || BooleanType.NO.getCode().equals(situation))
			 {
				 return false;
			 }
			 else{
				 return true;
			 }
 		 }
		 else if(depositaryUser){
			// return (CorporativeSituationType.CEVALDOM.getco)
 			 if(situation==null ||CorporativeSituationType.ISSUER.getCode().equals(situation) ||BooleanType.NO.getCode().equals(situation)){
				 return false;
			 }
 			 else{
 				 return true;
 			 }
		 }
  		return true;
	}
	
	 
	/**
	 * It executes the Issuer confirm operation.
	 */
	@LoggerAuditWeb
	public void confirmIssuerOperation(){
		List<CorporativeOperation> lstProcesses = new ArrayList<CorporativeOperation>();
		List<Long> corporateOperations = new ArrayList<>();
		try{
			List<Long> corporateOperationsNotAmount = new ArrayList<Long>();
			List<String> depositaryNotPaymentSecurity = new ArrayList<String>();
			for(CorporativeOperation process : checkCorporativeOperation){
				if(interestProcess.equals(process.getCorporativeEventType().getCorporativeEventType()) 
						|| amortizationProcess.equals(process.getCorporativeEventType().getCorporativeEventType())){
//					if(process.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode()) && 
//							(process.getIssuerConfirmedAmount() == null || (process.getIssuerConfirmedAmount() !=null &&
//							process.getIssuerConfirmedAmount().compareTo(BigDecimal.ZERO) <= 0))){
//						BigDecimal desBalance = corporateProcessServiceFacade.searchDesmaterializedBalanceSecurity(process.getSecurities().getIdSecurityCodePk());
//						if(desBalance.compareTo(BigDecimal.ZERO) > 0 
//								&& corporateProcessServiceFacade.hasBalance(process.getSecurities().getIdSecurityCodePk())){
//							corporateOperationsNotAmount.add(process.getIdCorporativeOperationPk());
//						}
//					}
					if(process.getSecurities().getIndPaymentBenefit().equals(BooleanType.NO.getCode())
							&& !depositaryNotPaymentSecurity.contains(process.getSecurities().getIdSecurityCodePk())){
						depositaryNotPaymentSecurity.add(process.getSecurities().getIdSecurityCodePk());						
					}
				}
				lstProcesses.add(process);
				corporateOperations.add(process.getIdCorporativeOperationPk());
			}
			if(corporateOperationsNotAmount.size()>0){
				JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
				JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, null, 
						PropertiesConstants.ISSUER_NOT_ENTER_AMOUNT, new Object[]{StringUtils.join(corporateOperationsNotAmount,",")});
				return;
			}
			if(depositaryNotPaymentSecurity.size()>0){
				JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
				JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_WARNING_HED, null, 
						PropertiesConstants.DEPOSITARY_NOT_PAYMENT, new Object[]{StringUtils.join(depositaryNotPaymentSecurity,",")});
				return;
			}
			corporateProcessServiceFacade.executeConfirmationIssuer(lstProcesses);
		 	JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
			JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_SUCCESS_HED, null, 
					PropertiesConstants.MSG_ISSUER_PRELIMINARY_SUCC, new Object[]{StringUtils.join(corporateOperations,",")});
			checkCorporativeOperation = null;
			setPrivilegeComponent();
		}catch(ServiceException esx){
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();		
				setPrivilegeComponent();
			}
		}	catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * It executes the Issuer confirm operation.
	 */
	@LoggerAuditWeb
	public void confirmStockCalculation(){
		List<CorporativeOperation> lstProcesses = new ArrayList<CorporativeOperation>();
		List<Long> corporateOperations = new ArrayList<>();
		
		StockCalculationProcess stockCalculationProcess = new StockCalculationProcess();
		stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
		stockCalculationProcess.setStockType(StockType.CORPORATIVE.getCode());
		stockCalculationProcess.setHolder(new Holder());
		stockCalculationProcess.setParticipant(new Participant());
		stockCalculationProcess.setSecurity(new Security());
//		stockCalculationProcess.setRegistryDate(CommonsUtilities.currentDate());
//		stockCalculationProcess.setCutoffDate(CommonsUtilities.currentDate());
		stockCalculationProcess.setIndAutomactic(BooleanType.NO.getCode());
		stockCalculationProcess.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		try{
			for(CorporativeOperation process : checkCorporativeOperation){
				lstProcesses.add(process);
				corporateOperations.add(process.getIdCorporativeOperationPk());
				
				Security security = new Security();
//				security.setIdSecurityCodePk(process.getSecurities());
				stockCalculationProcess.setSecurity(process.getSecurities());
				stockCalculationProcess.setRegistryDate(process.getRegistryDate());
				stockCalculationProcess.setCutoffDate(process.getCutoffDate());
				stockCalculationProcess.setIdStockCalculationPk(null);
				stockCalculationProcess.setStockState(null);
				
				Long stockId = manageStockCalculationFacade.registerStockCalculation(stockCalculationProcess);
			}
			
//			corporateProcessServiceFacade.executeConfirmationIssuer(lstProcesses);
//		 	
//			JSFUtilities.showMessageOnDialog(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_SUCCESS_HED, null, 
//					PropertiesConstants.MSG_ISSUER_PRELIMINARY_SUCC, new Object[]{StringUtils.join(corporateOperations,",")});
						
			JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
			Object[] stock = {"Corporativo"};
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.STOCK_QUESTION_SUCESS,stock));
			checkCorporativeOperation = null;
			
			setPrivilegeComponent();
		}catch(ServiceException esx){
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();		
				setPrivilegeComponent();
			}
		}	catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Calculate totals.
	 */
	public void calculateTotals(){
		holderAdjModified = true;
		availableBalance = BigDecimal.ZERO;
		totalBalance = BigDecimal.ZERO;
		adjustedBalance = BigDecimal.ZERO;
		if(detailSearchDataModel != null && detailSearchDataModel.isRowAvailable()){
			for(CorporativeProcessDetailsTO data: detailSearchDataModel){
				if(data.getAdjustedBalance() != null){
					adjustedBalance = adjustedBalance.add(data.getAdjustedBalance());
				}
				if(data.getNetTotalBalance() != null){
					totalBalance = totalBalance.add(data.getNetTotalBalance());
				}
				if(data.getNetAvailableBalance() != null){
					availableBalance = availableBalance.add(data.getNetAvailableBalance());
				}
			}
		}		
	}
	
	/**
	 * Balance custody.
	 *
	 * @param detail the detail
	 */
	public void balanceCustody(CorporativeProcessDetailsTO detail){
		detail.setNetAvailableBalance(null);
		holderAdjModified = true;
	}
	
	/**
	 * Balance tax.
	 *
	 * @param detail the detail
	 */
	public void balanceTax(CorporativeProcessDetailsTO detail){
		detail.setNetTotalBalance(null);
		holderAdjModified = true;
	}

	/**
	 * Balance indicator.
	 *
	 * @param detail the detail
	 */
	public void balanceIndicator(CorporativeProcessDetailsTO detail){
		detail.setAdjustedBalance(null);
		holderAdjModified = true;
	}
	
	/**
	 * Find joint owners.
	 */
	public void findJointOwners(){
//		hideDialogs();
		if(holder != null && holder.getIdHolderPk() !=null && holder.getIdHolderPk() > 0){
			selectedJointHolder = null;
			jointHolders = null;
			detailSearchDataModel= null;
			if(holderAccountType.equals(new Integer(1))){
				try{				
						jointHolders = new GenericDataModel<CorporativeProcessDetailsTO>(
								corporateServiceFacade.getJointHolders(selectedCorporativeOperation.getIdCorporativeOperationPk(), holder.getIdHolderPk()));				
				}catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}
			}		
		}else{
			holderTypeChange();
		}
	}
	
	/**
	 * Holder type change.
	 */
	public void holderTypeChange(){
		holderAdjModified = false;
		holder = new Holder();	
		selectedJointHolder = null;
		jointHolders = null;
		detailSearchDataModel= null;
	}
	
	/**
	 * Return to the Administrator page after completing adjustments.
	 *
	 * @return the String type
	 */
	public String saveOrModifySucc(){
		hideDialogs();
		cleanCorporate();		
		return "corporateMonitoring";
	}
	
	/**
	 * Save holder adjustments.
	 */
	@LoggerAuditWeb
	public void saveHolderAdjustments(){
		
		try{
			
//			hideDialogs();
			if(!holderAdjModified){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.JOINTHOLDER_NO_DATA_MODIFED));
				JSFUtilities.showSimpleValidationDialog();	
			}
			if(!ImportanceEventType.SECURITIES_EXCISION.getCode().equals(selectedCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
				destinySecurities = null;
			}	
			if(detailSearchDataModel != null){
				corporateServiceFacade.actualizeHolderAdjustments(detailSearchDataModel.getDataList(), selectedCorporativeOperation, destinySecurity);
			}
			cleanAdjustAccountDetails();
//			JSFUtilities.showComponent(":"+ID_DIALOG_CONFIRM_SUCESS);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALT_HOLDER_ADJUSTMENT_SUCCESS));
			JSFUtilities.showSimpleValidationDialog();				
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * Gets the key conf depositary.
	 *
	 * @return the keyConfDepositary
	 */
	public Integer getKeyConfDepositary() {
		return keyConfDepositary;
	}
	
	/**
	 * Sets the key conf depositary.
	 *
	 * @param keyConfDepositary the keyConfDepositary to set
	 */
	public void setKeyConfDepositary(Integer keyConfDepositary) {
		this.keyConfDepositary = keyConfDepositary;
	}
	
	/**
	 * Gets the key conf issuer.
	 *
	 * @return the keyConfIssuer
	 */
	public Integer getKeyConfIssuer() {
		return keyConfIssuer;
	}
	
	/**
	 * Sets the key conf issuer.
	 *
	 * @param keyConfIssuer the keyConfIssuer to set
	 */
	public void setKeyConfIssuer(Integer keyConfIssuer) {
		this.keyConfIssuer = keyConfIssuer;
	}
	
	/**
	 * Checks if is special process.
	 *
	 * @return the specialProcess
	 */
	public boolean isSpecialProcess() {
		return specialProcess;
	}
	
	/**
	 * Sets the special process.
	 *
	 * @param specialProcess the specialProcess to set
	 */
	public void setSpecialProcess(boolean specialProcess) {
		this.specialProcess = specialProcess;
	}
	
	/**
	 * Checks if is cash benefit.
	 *
	 * @return the cashBenefit
	 */
	public boolean isCashBenefit() {
		return cashBenefit;
	}
	
	/**
	 * Sets the cash benefit.
	 *
	 * @param cashBenefit the cashBenefit to set
	 */
	public void setCashBenefit(boolean cashBenefit) {
		this.cashBenefit = cashBenefit;
	}
	
	/**
	 * Checks if is share benefit.
	 *
	 * @return the shareBenefit
	 */
	public boolean isShareBenefit() {
		return shareBenefit;
	}
	
	/**
	 * Sets the share benefit.
	 *
	 * @param shareBenefit the shareBenefit to set
	 */
	public void setShareBenefit(boolean shareBenefit) {
		this.shareBenefit = shareBenefit;
	}
	
	/**
	 * Gets the block balance.
	 *
	 * @return the blockBalance
	 */
	public BigDecimal getBlockBalance() {
		return blockBalance;
	}
	
	/**
	 * Sets the block balance.
	 *
	 * @param blockBalance the blockBalance to set
	 */
	public void setBlockBalance(BigDecimal blockBalance) {
		this.blockBalance = blockBalance;
	}
	
	/**
	 * Gets the preliminary status.
	 *
	 * @return the preliminaryStatus
	 */
	public Integer getPreliminaryStatus() {
		return preliminaryStatus;
	}
	
	/**
	 * Sets the preliminary status.
	 *
	 * @param preliminaryStatus the preliminaryStatus to set
	 */
	public void setPreliminaryStatus(Integer preliminaryStatus) {
		this.preliminaryStatus = preliminaryStatus;
	}
	
	/**
	 * Gets the destiny securities.
	 *
	 * @return the destinySecurities
	 */
	public List<ParameterTable> getDestinySecurities() {
		return destinySecurities;
	}
	
	/**
	 * Sets the destiny securities.
	 *
	 * @param destinySecurities the destinySecurities to set
	 */
	public void setDestinySecurities(List<ParameterTable> destinySecurities) {
		this.destinySecurities = destinySecurities;
	}
	
	/**
	 * Gets the destiny security.
	 *
	 * @return the destinySecurity
	 */
	public String getDestinySecurity() {
		return destinySecurity;
	}
	
	/**
	 * Sets the destiny security.
	 *
	 * @param destinySecurity the destinySecurity to set
	 */
	public void setDestinySecurity(String destinySecurity) {
		this.destinySecurity = destinySecurity;
	}
	
	/**
	 * Checks if is excision.
	 *
	 * @return the excision
	 */
	public boolean isExcision() {
		return excision;
	}
	
	/**
	 * Sets the excision.
	 *
	 * @param excision the excision to set
	 */
	public void setExcision(boolean excision) {
		this.excision = excision;
	}
	
	/**
	 * Gets the adjusted balance.
	 *
	 * @return the adjustedBalance
	 */
	public BigDecimal getAdjustedBalance() {
		return adjustedBalance;
	}
	
	/**
	 * Sets the adjusted balance.
	 *
	 * @param adjustedBalance the adjustedBalance to set
	 */
	public void setAdjustedBalance(BigDecimal adjustedBalance) {
		this.adjustedBalance = adjustedBalance;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the holder account type.
	 *
	 * @return the holderAccountType
	 */
	public Integer getHolderAccountType() {
		return holderAccountType;
	}
	
	/**
	 * Sets the holder account type.
	 *
	 * @param holderAccountType the holderAccountType to set
	 */
	public void setHolderAccountType(Integer holderAccountType) {
		this.holderAccountType = holderAccountType;
	}
	
	/**
	 * Gets the joint holders.
	 *
	 * @return the jointHolders
	 */
	public GenericDataModel<CorporativeProcessDetailsTO> getJointHolders() {
		return jointHolders;
	}
	
	/**
	 * Sets the joint holders.
	 *
	 * @param jointHolders the jointHolders to set
	 */
	public void setJointHolders(
			GenericDataModel<CorporativeProcessDetailsTO> jointHolders) {
		this.jointHolders = jointHolders;
	}
	
	/**
	 * Gets the selected joint holder.
	 *
	 * @return the selectedJointHolder
	 */
	public CorporativeProcessDetailsTO getSelectedJointHolder() {
		return selectedJointHolder;
	}
	
	/**
	 * Sets the selected joint holder.
	 *
	 * @param selectedJointHolder the selectedJointHolder to set
	 */
	public void setSelectedJointHolder(
			CorporativeProcessDetailsTO selectedJointHolder) {
		this.selectedJointHolder = selectedJointHolder;
	}
	
	/**
	 * Checks if is holder adj modified.
	 *
	 * @return the holderAdjModified
	 */
	public boolean isHolderAdjModified() {
		return holderAdjModified;
	}
	
	/**
	 * Sets the holder adj modified.
	 *
	 * @param holderAdjModified the holderAdjModified to set
	 */
	public void setHolderAdjModified(boolean holderAdjModified) {
		this.holderAdjModified = holderAdjModified;
	}
	
	/**
	 * Checks for adjustments.
	 *
	 * @param processType the process type
	 * @return true, if successful
	 */
	public boolean hasAdjustments(Integer processType){
		return (processType.equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
				||processType.equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())
				||processType.equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())
				||processType.equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())
				||processType.equals(ImportanceEventType.REMANENT_PAYMENT.getCode())
				||processType.equals(ImportanceEventType.ACTION_DIVIDENDS.getCode()));
	}
	
	/**
	 * Depositary confirmed.
	 *
	 * @param processType the process type
	 * @param situation the situation
	 * @return true, if successful
	 */
	public boolean depositaryConfirmed(Integer processType, Integer situation){
		if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(processType) || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(processType) ){
			return (keyConfDepositary.equals(situation) || keyConfPaid.equals(situation) );
		}else{
			return (situation != null && situation > 0);
		}
	}
	
	/**
	 * Checks if is suer confirmed.
	 *
	 * @param processType the process type
	 * @param situation the situation
	 * @return true, if is suer confirmed
	 */
	public boolean issuerConfirmed(Integer processType, Integer situation){
		if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(processType) || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(processType) ){
			return (situation != null && situation > 0);
		}else{
			return (keyConfIssuer.equals(situation) || keyConfPaid.equals(situation) );	
			
		}
	}
	
	/**
	 * Gets the key conf paid.
	 *
	 * @return the keyConfPaid
	 */
	public Integer getKeyConfPaid() {
		return keyConfPaid;
	}
	
	/**
	 * Sets the key conf paid.
	 *
	 * @param keyConfPaid the keyConfPaid to set
	 */
	public void setKeyConfPaid(Integer keyConfPaid) {
		this.keyConfPaid = keyConfPaid;
	}
	
	/**
	 * Gets the interest process.
	 *
	 * @return the interestProcess
	 */
	public Integer getInterestProcess() {
		return interestProcess;
	}
	
	/**
	 * Sets the interest process.
	 *
	 * @param interestProcess the interestProcess to set
	 */
	public void setInterestProcess(Integer interestProcess) {
		this.interestProcess = interestProcess;
	}
	
	/**
	 * Gets the amortization process.
	 *
	 * @return the amortizationProcess
	 */
	public Integer getAmortizationProcess() {
		return amortizationProcess;
	}
	
	/**
	 * Sets the amortization process.
	 *
	 * @param amortizationProcess the amortizationProcess to set
	 */
	public void setAmortizationProcess(Integer amortizationProcess) {
		this.amortizationProcess = amortizationProcess;
	}
	
	/**
	 * Checks if is disable selection.
	 *
	 * @param processType the process type
	 * @param situation the situation
	 * @return true, if is disable selection
	 */
	public boolean isDisableSelection(Integer processType, Integer situation){
		return (issuerUser && (((amortizationProcess.equals(processType) || interestProcess.equals(processType)) && (situation != null && situation.compareTo(0) > 0))
				|| (!amortizationProcess.equals(processType) && !interestProcess.equals(processType) 
						&& (situation == null && (situation != null && !situation.equals(keyConfDepositary))))));
	 	
	}
	
	/**
	 * Checks if is remnant benefit.
	 *
	 * @return the remnantBenefit
	 */
	public boolean isRemnantBenefit() {
		return remnantBenefit;
	}
	
	/**
	 * Sets the remnant benefit.
	 *
	 * @param remnantBenefit the remnantBenefit to set
	 */
	public void setRemnantBenefit(boolean remnantBenefit) {
		this.remnantBenefit = remnantBenefit;
	}
	
	/**
	 * Checks if is depositary user.
	 *
	 * @return true, if is depositary user
	 */
	public boolean isDepositaryUser() {
		return depositaryUser;
	}
	
	/**
	 * Sets the depositary user.
	 *
	 * @param depositaryUser the new depositary user
	 */
	public void setDepositaryUser(boolean depositaryUser) {
		this.depositaryUser = depositaryUser;
	}
	
	/**
	 * Gets the instrumente types.
	 *
	 * @return the instrumente types
	 */
	public List<ParameterTable> getInstrumenteTypes() {
		return instrumenteTypes;
	}
	
	/**
	 * Sets the instrumente types.
	 *
	 * @param instrumenteTypes the new instrumente types
	 */
	public void setInstrumenteTypes(List<ParameterTable> instrumenteTypes) {
		this.instrumenteTypes = instrumenteTypes;
	}
	
	/**
	 * Gets the corporate state map.
	 *
	 * @return the corporate state map
	 */
	public Map<Integer,String> getCorporateStateMap() {
		return corporateStateMap;
	}
	
	/**
	 * Sets the corporate state map.
	 *
	 * @param corporateStateMap the corporate state map
	 */
	public void setCorporateStateMap(Map<Integer,String> corporateStateMap) {
		this.corporateStateMap = corporateStateMap;
	}
	
	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	
	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	/**
	 * Checks if is exchange cash benefit.
	 *
	 * @return true, if is exchange cash benefit
	 */
	public boolean isExchangeCashBenefit() {
		return exchangeCashBenefit;
	}
	
	/**
	 * Sets the exchange cash benefit.
	 *
	 * @param exchangeCashBenefit the new exchange cash benefit
	 */
	public void setExchangeCashBenefit(boolean exchangeCashBenefit) {
		this.exchangeCashBenefit = exchangeCashBenefit;
	}

	public List<ParameterTable> getLstCorpProcessTypeDate() {
		return lstCorpProcessTypeDate;
	}

	public void setLstCorpProcessTypeDate(
			List<ParameterTable> lstCorpProcessTypeDate) {
		this.lstCorpProcessTypeDate = lstCorpProcessTypeDate;
	}

	public List<ParameterTable> getLstCboFilterSecuritieClass() {
		return lstCboFilterSecuritieClass;
	}

	public void setLstCboFilterSecuritieClass(
			List<ParameterTable> lstCboFilterSecuritieClass) {
		this.lstCboFilterSecuritieClass = lstCboFilterSecuritieClass;
	}

	public List<ParameterTable> getLstCboFilterCurrency() {
		return lstCboFilterCurrency;
	}

	public void setLstCboFilterCurrency(List<ParameterTable> lstCboFilterCurrency) {
		this.lstCboFilterCurrency = lstCboFilterCurrency;
	}

	public String getLabelDateSearch() {
		return labelDateSearch;
	}

	public void setLabelDateSearch(String labelDateSearch) {
		this.labelDateSearch = labelDateSearch;
	}
	
	
	
	/**
	 * Verificacion de Calculos de Stock Faltantes
	 */
	
	public void stockMissing(){
		try {
		List<CorporativeOperation> securities = corporateProcessServiceFacade.listSecurityToDefinitive();
		 
		 HashMap<String, CorporativeOperation> distinct = new HashMap<>();
		
		 for(CorporativeOperation stocks : securities){
			 distinct.put(stocks.getSecurities().getIdSecurityCodePk(), stocks);
		 }
			 if(distinct.size()>GeneralConstants.ZERO_VALUE_INT){
				 
				 List<CorporativeOperation> corporative  =  new ArrayList<CorporativeOperation>(distinct.values());
				 
				 String Text = "";
				 Integer count = 0;
				 
				 for(CorporativeOperation corporatives : corporative){
						//verificamos q tenga CStock
						 Long idCalcStock = corporateProcessService.verifyStockCalculationAutomatic(corporatives);
						 if(idCalcStock == null){
							 Text = Text+" / " + corporatives.getSecurities().getIdSecurityCodePk();
							 count  = count + 1;
						 }
				 }		 
			 
				 if(!Text.equals("")){
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
								"CANTIDAD DE STOCKS FALTANTES: "  + count + " DETALLE :" + Text );
						JSFUtilities.showSimpleValidationDialog();		
				 }else{
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
								"NO FALTA NINGUN CALCULO DE STOCK" );
						JSFUtilities.showSimpleValidationDialog();		
				 }
			 }
		} catch (Exception e) {
			 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
						"SobreCarga - Existen mas de 10 Stocks en proceso - Intenta en 10 segundos =)" );
				JSFUtilities.showSimpleValidationDialog();
		}
	}
}
