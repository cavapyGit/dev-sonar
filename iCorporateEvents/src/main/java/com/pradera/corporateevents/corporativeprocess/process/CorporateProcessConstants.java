package com.pradera.corporateevents.corporativeprocess.process;

import java.math.BigDecimal;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface CorporateProcessConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/06/2014
 */
public interface CorporateProcessConstants {
	
	
	/** The cp pawn. */
	int CP_PAWN = 0;

	/** The cp ban. */
	int CP_BAN = 1;

	/** The cp block other. */
	int CP_BLOCK_OTHER = 2;
	
	/** The cp reserve. */
	int CP_RESERVE = 3;
	
	/** The cp opposition. */
	int CP_OPPOSITION = 4;

	/** The cp margin. */
	int CP_MARGIN = 5;
	
	/** The cp reporting. */
	int CP_REPORTING = 6;

	/** The cp available. */
	int CP_AVAILABLE = 7;	
	
	/** The cp balances. */
	int CP_BALANCES = 8;
	
	/** The total balances. */
	int TOTAL_BALANCES = 9;
	
	/** The bl available. */
	int BL_AVAILABLE = 0;
	
	/** The bl pawn. */
	int BL_PAWN = 1;
	
	/** The bl ban. */
	int BL_BAN = 2;
	
	/** The bl other block. */
	int BL_OTHER_BLOCK = 3;
	
	/** The bl reserve. */
	int BL_RESERVE = 4;
	
	/** The bl opposition. */
	int BL_OPPOSITION = 5;
	
	/** The bl margin. */
	int BL_MARGIN = 6;
	
	/** The bl reporting. */
	int BL_REPORTING = 7;
	
	/** The bl reported. */
	int BL_REPORTED = 8;
	
	/** The bl reblock pawn. */
	int BL_REBLOCK_PAWN = 9;
	
	/** The bl reblock ban. */
	int BL_REBLOCK_BAN = 10;
	
	/** The bl reblock other block. */
	int BL_REBLOCK_OTHER_BLOCK = 11;
	
	/** The bl reblock reserve. */
	int BL_REBLOCK_RESERVE = 12;
	
	/** The bl reblock opposition. */
	int BL_REBLOCK_OPPOSITION = 13;
	
	/** The bl reverse available. */
	int BL_REVERSE_AVAILABLE=14;
	
	/** The balance origin. */
	int BALANCE_ORIGIN = 1;
	
	/** The balance destiny. */
	int BALANCE_DESTINY = 2;
	
	/** The exchange destiny. */
	int EXCHANGE_ORIGIN = 3;
	
	/** The exchange destiny. */
	int EXCHANGE_DESTINY = 4;
	
	/** The definitive not processed. */
	int DEFINITIVE_NOT_PROCESSED = 0;
	
	/** The definitive processed. */
	int DEFINITIVE_PROCESSED = 1;
	
	/** The ind no remainder. */
	int IND_NO_REMAINDER = 0;
	
	/** The ind remainder. */
	int IND_REMAINDER = 1;
	
	/** The ind round. */
	int IND_ROUND = 1;
	
	/** The ind floor. */
	int IND_FLOOR = 0;
	
	/** The minimum amount. */
	BigDecimal MINIMUM_AMOUNT = BigDecimal.valueOf(0.01);
	
	/** The amount round. */
	BigDecimal AMOUNT_ROUND = new BigDecimal(100);
	
	/** The ind yes. */
	Integer IND_YES = new Integer(1);
	
	/** The ind no. */
	Integer IND_NO = new Integer(0);
	
	/** The corporate event. */
	String CORPORATE_EVENT="corporateEventType";
	
	public static long MILLSECS_IN_SECS=1000;
	
	/** The millsecs in min. */
	public static long MILLSECS_IN_MIN=1000*60;
	
	/** The min years tax pay. */
	public static Integer MIN_YEARS_TAX_PAY=Integer.valueOf(3);
	
	/** The error preliminary process state. */
	public static Integer ERROR_PRELIMINARY_PROCESS_STATE = new Integer(1);
	
	/** The error preliminary delivery date. */
	public static Integer ERROR_PRELIMINARY_DELIVERY_DATE = new Integer(2);
	
	/** The error preliminary security state. */
	public static Integer ERROR_PRELIMINARY_SECURITY_STATE = new Integer(3);
	
	/** The error preliminary security desmaterialized. */
	public static Integer ERROR_PRELIMINARY_SECURITY_DESMATERIALIZED = new Integer(4);
	
	/** The error preliminary not settlement balance. */
	public static Integer ERROR_PRELIMINARY_NOT_SETTLEMENT_BALANCE = new Integer(5);
	
	/** The error preliminary transit balance. */
	public static Integer ERROR_PRELIMINARY_TRANSIT_BALANCE = new Integer(6);
	
	/** The error preliminary emisor confirm. */
	public static Integer ERROR_PRELIMINARY_EMISOR_CONFIRM = new Integer(7);
	
	/** The error preliminary holder movement. */
	public static Integer ERROR_PRELIMINARY_HOLDER_MOVEMENT = new Integer(8);
	
	/** The error preliminary exchange rate. */
	public static Integer ERROR_PRELIMINARY_EXCHANGE_RATE = new Integer(9);
	
	/** The error preliminary retirement operation. */
	public static Integer ERROR_PRELIMINARY_RETIREMENT_OPERATION = new Integer(10);
	
	/** The error preliminary fail. */
	public static Integer ERROR_PRELIMINARY_FAIL = new Integer(11);
	
	/** The error preliminary fail for time. */
	public static Integer ERROR_PRELIMINARY_FAIL_FOR_TIME = new Integer(12);
	
	/** The msg preliminary end. */
	public static Integer MSG_PRELIMINARY_END = new Integer(13);
	
	/** The error definitive amortization. */
	public static Integer ERROR_DEFINITIVE_AMORTIZATION = new Integer(14);
	
	/** The error definitive security state. */
	public static Integer ERROR_DEFINITIVE_SECURITY_STATE = new Integer(15);
	
	/** The error definitive security desmaterialized. */
	public static Integer ERROR_DEFINITIVE_SECURITY_DESMATERIALIZED = new Integer(16);
	
	/** The error definitive process state. */
	public static Integer ERROR_DEFINITIVE_PROCESS_STATE = new Integer(17);
	
	/** The error definitive situation. */
	public static Integer ERROR_DEFINITIVE_SITUATION = new Integer(18);
	
	/** The error definitive preliminary delivery date. */
	public static Integer ERROR_DEFINITIVE_PRELIMINARY_DELIVERY_DATE = new Integer(19);
	
	/** The error definitive delivery date. */
	public static Integer ERROR_DEFINITIVE_DELIVERY_DATE = new Integer(20);
	
	/** The error definitive execute date. */
	public static Integer ERROR_DEFINITIVE_EXECUTE_DATE = new Integer(21);
	
	/** The error definitive holder account adjustments. */
	public static Integer ERROR_DEFINITIVE_HOLDER_ACCOUNT_ADJUSTMENTS = new Integer(22);
	
	/** The error definitive issuer amount. */
	public static Integer ERROR_DEFINITIVE_ISSUER_AMOUNT = new Integer(23);
	
	/** The error definitive preliminary holder movement. */
	public static Integer ERROR_DEFINITIVE_PRELIMINARY_HOLDER_MOVEMENT = new Integer(24);
	
	/** The error definitive holder movement. */
	public static Integer ERROR_DEFINITIVE_HOLDER_MOVEMENT = new Integer(25);
	
	/** The error definitive accreditation balance. */
	public static Integer ERROR_DEFINITIVE_ACCREDITATION_BALANCE = new Integer(26);
	
	/** The msg definitive end. */
	public static Integer MSG_DEFINITIVE_END = new Integer(27);
	
	/** The error definitive fail. */
	public static Integer ERROR_DEFINITIVE_FAIL = new Integer(28);
	
	/** The error definitive fail for time. */
	public static Integer ERROR_DEFINITIVE_FAIL_FOR_TIME = new Integer(29);
	
	/** The error preliminary accreditation balance. */
	public static Integer ERROR_PRELIMINARY_ACCREDITATION_BALANCE = new Integer(30);
	
	/** The str sysdate. */
	public String STR_SYSDATE ="sysdate";
}
