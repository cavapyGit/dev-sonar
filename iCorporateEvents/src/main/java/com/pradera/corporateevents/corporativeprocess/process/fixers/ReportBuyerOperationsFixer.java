package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.ReportCorporativeResult;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.ParticipantRoleType;

// TODO: Auto-generated Javadoc
/**
 * The Class ReportBuyerOperationsFixer.
 *
 * @author PraderaTechnologies
 */
@ApplicationScoped
public class ReportBuyerOperationsFixer implements BuyerOperationsFixer, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new FixedBlockedDocuments object.
	 */
	public ReportBuyerOperationsFixer() {
	}

	/**
	 * Method will return a list of ReportCorporativeResults that is constructed from given list.
	 *
	 * @param operationsSum  <code>List</code>
	 * @param buyerOperations the buyer operations
	 * @param corpOperation <code>corpOperation</code>
	 * @param round <code>int</code>
	 * @param remnantFlag <code>boolean</code>
	 * @param cashFlag <code>boolean</code>
	 * @param destiny <code>boolean</code>
	 * @param balFixer <code>BalanceFixer</code>
	 * @param exchangeRateDay the exchange rate day
	 * @return <code>List</code>
	 */
	public List<ReportCorporativeResult> fixOperations(List<Object[]> operationsSum,List<Object[]> buyerOperations,CorporativeOperation corpOperation,int round,
			boolean remnantFlag,boolean cashFlag,boolean destiny,BalanceFixer balFixer,DailyExchangeRates exchangeRateDay){
		List<ReportCorporativeResult> buyerOpers = null;
		if(operationsSum != null && operationsSum.size() > 0){
			buyerOpers = new ArrayList<ReportCorporativeResult>();
			BigDecimal sellerSum = null;BigDecimal buyerSum = null;Long operationId = null;
			Integer indOriginDestiny = null;
			for(Object[] oper : operationsSum){
				operationId = (Long)oper[0];
				buyerSum= (BigDecimal)oper[1];
				sellerSum = (BigDecimal)oper[2];
				indOriginDestiny = (Integer)oper[3];
				corpOperation.setIndOriginDestiny(indOriginDestiny);
				BigDecimal sellerRoundSum = BigDecimal.ZERO;
				BigDecimal delFactor = CorporateProcessUtils.divide(sellerSum,buyerSum);
				List<ReportCorporativeResult> buyers = new ArrayList<ReportCorporativeResult>();
				List<Object[]> tmp = new ArrayList<Object[]>();
				for(Object[] operDetails : buyerOperations){
					if(operationId.equals(operDetails[5])){
						operDetails[1]=operDetails[1]!=null?cashFlag?CorporateProcessUtils.multiply((BigDecimal)operDetails[1], CorporateProcessConstants.AMOUNT_ROUND)
								:operDetails[1]:new BigDecimal(0);
						ReportCorporativeResult reportRes = fillOperations(operDetails, corpOperation, delFactor, round, remnantFlag, destiny, true,exchangeRateDay);
						buyers.add(reportRes);
						sellerRoundSum = sellerRoundSum.add(reportRes.getGuaranteeBalance());
						tmp.add(operDetails);
					}
				}
				if(!tmp.isEmpty()){
					buyerOperations.removeAll(tmp);
				}
				buyers = fixOperations(buyers, buyerSum, sellerRoundSum, balFixer);			
				if(buyers != null){
					buyerOpers.addAll(buyers);	
					buyers.clear();
				}
			}
			
			if(cashFlag && !buyerOpers.isEmpty()){
				for(ReportCorporativeResult corpReport : buyerOpers){
					corpReport.setGuaranteeBalance(CorporateProcessUtils.divideForAmount(corpReport.getGuaranteeBalance(), CorporateProcessConstants.AMOUNT_ROUND));
				}
			}			
		}
		return buyerOpers;
	}
	
	/**
	 * Used to fill the ReportCorporativeResults with the given array and CorporativeOperation values.
	 *
	 * @param oper <code>Object[]</code>
	 * @param corpOperation <code>CorporativeOperation</code>
	 * @param delFactor <code>BigDecimal</code>
	 * @param round <code>int</code>
	 * @param remnantFlag <code>boolean</code>
	 * @param destiny <code>boolean</code>
	 * @param target <code>boolean</code>
	 * @param exchangeRateDay the exchange rate day
	 * @return <code>ReportCorporativeResult</code>
	 */
	public ReportCorporativeResult fillOperations(Object[] oper, CorporativeOperation corpOperation, BigDecimal delFactor, int round,
			boolean remnantFlag, boolean destiny, boolean target,DailyExchangeRates exchangeRateDay) {

		ReportCorporativeResult objReportOperRes = new ReportCorporativeResult();
		objReportOperRes.setCorporativeOperation(corpOperation);
		HolderAccount ha = new HolderAccount();
		ha.setIdHolderAccountPk((Long)oper[3]);
		objReportOperRes.setHolderAccount(ha);
		Participant pa = new Participant();
		pa.setIdParticipantPk((Long)oper[2]);
		objReportOperRes.setParticipant(pa);
		GuaranteeType guarType = GuaranteeType.get((Integer) oper[0]);
		objReportOperRes.setGuaranteeType(guarType.getCode());
		objReportOperRes.setOperationRole(ParticipantRoleType.BUY.getCode());
		if(corpOperation.getIndOriginDestiny()!=null && 
				(corpOperation.getIndOriginDestiny().equals(CorporateProcessConstants.EXCHANGE_DESTINY) || 
						corpOperation.getIndOriginDestiny().equals(CorporateProcessConstants.EXCHANGE_ORIGIN))){
			objReportOperRes.setGuaranteeBalance(CorporateProcessUtils.calculateByFactor((BigDecimal) oper[1], delFactor, round));
			objReportOperRes.setGuaranteeBalance(CorporateProcessUtils.calculateByFactor(objReportOperRes.getGuaranteeBalance(), exchangeRateDay.getBuyPrice(), round));
		}else{
			objReportOperRes.setGuaranteeBalance(CorporateProcessUtils.calculateByFactor((BigDecimal) oper[1], delFactor, round));
		}		
		objReportOperRes
				.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		if (remnantFlag) {
			objReportOperRes.setIndRemanent(CorporateProcessConstants.IND_REMAINDER);
		} else {
			objReportOperRes.setIndRemanent(CorporateProcessConstants.IND_NO_REMAINDER);
		}
		if(corpOperation.getIndOriginDestiny()!=null && 
				corpOperation.getIndOriginDestiny().equals(CorporateProcessConstants.EXCHANGE_DESTINY)){
			objReportOperRes.setIndOriginDestiny(CorporateProcessConstants.EXCHANGE_DESTINY);
		}else if(corpOperation.getIndOriginDestiny()!=null && 
				corpOperation.getIndOriginDestiny().equals(CorporateProcessConstants.EXCHANGE_ORIGIN)){
			objReportOperRes.setIndOriginDestiny(CorporateProcessConstants.EXCHANGE_ORIGIN);
		}else if(destiny){
			objReportOperRes.setIndOriginDestiny(CorporateProcessConstants.BALANCE_DESTINY);
		}else{
			objReportOperRes.setIndOriginDestiny(CorporateProcessConstants.BALANCE_ORIGIN);
		}
		if(target && (corpOperation.getTargetSecurity() != null && corpOperation.getTargetSecurity().getIdSecurityCodePk() != null)){
			objReportOperRes.setSecurity(corpOperation.getTargetSecurity());
		}else{
			objReportOperRes.setSecurity(corpOperation.getSecurities());
		}
		MechanismOperation mechOper = new MechanismOperation();
		mechOper.setIdMechanismOperationPk((Long) oper[5]);
		objReportOperRes.setMechanismOperation(mechOper);
		HolderAccountOperation holAccOper = new HolderAccountOperation();
		holAccOper.setIdHolderAccountOperationPk((Long) oper[4]);
		objReportOperRes.setHolderAccountOperation(holAccOper);

		return objReportOperRes;
	}
	
	/**
	 * Used to fill the ReportCorporativeResults with the given list.
	 *
	 * @param operations <code>List</code>
	 * @param reportBalance <code>BigDecimal</code>
	 * @param totalBalance <code>BigDecimal</code>
	 * @param balFixer <code>BalanceFixer</code>
	 * @return <code>List</code>
	 */
	public List<ReportCorporativeResult> fixOperations(List<ReportCorporativeResult> operations, BigDecimal reportBalance,
			BigDecimal totalBalance, BalanceFixer balFixer)  {
		BigDecimal result = reportBalance.subtract(totalBalance);
		ReportCorporativeResult reportOperI = null, reportOperJ = null;
		if (result.compareTo(BigDecimal.ZERO) < 0) {

			for (int i = 0; i < operations.size(); i++) {
				for (int j = i + 1; j < operations.size(); j++) {
					reportOperI = (ReportCorporativeResult) operations
							.get(i);
					reportOperJ = (ReportCorporativeResult) operations
							.get(j);
					if (reportOperI.getGuaranteeBalance().compareTo(reportOperJ.getGuaranteeBalance()) < 0) {
						operations.set(i, reportOperJ);
						operations.set(j, reportOperI);
					}

				}
			}
			BigDecimal[] dlistbb = getBalancesArray(operations);
			dlistbb = balFixer.matchBalances(dlistbb, result,totalBalance);
			operations = setBalancesArray(dlistbb,operations);
			
		}else if (result.compareTo(BigDecimal.ZERO) > 0) {
			for (int i = 0; i < operations.size(); i++) {
				for (int j = i + 1; j < operations.size(); j++) {
					reportOperI = (ReportCorporativeResult) operations
							.get(i);
					reportOperJ = (ReportCorporativeResult) operations
							.get(j);
					if (reportOperI.getGuaranteeBalance().compareTo(reportOperJ.getGuaranteeBalance()) > 0) {
						operations.set(i, reportOperJ);
						operations.set(j, reportOperI);
					}

				}
			}
			BigDecimal[] balances = getBalancesArray(operations);
			balances = balFixer.matchBalances(balances, result,totalBalance);
			operations = setBalancesArray(balances,operations);

		}
		return operations;
	}
	
	/**
	 * Method will return a array that is constructed from given list.
	 *
	 * @param balances <code> List</code>
	 * @return <code> BigDecimal[]</code>
	 */
	private BigDecimal[] getBalancesArray(List<ReportCorporativeResult> balances) {
		BigDecimal[] balancesArray = new BigDecimal[balances.size()];
		for (int i = 0; i < balances.size(); i++) {
			ReportCorporativeResult objResult = balances.get(i);
			balancesArray[i] = objResult.getGuaranteeBalance();
		}
		return balancesArray;
	}

	/**
	 * Method will return the list of given array.
	 *
	 * @param balancesArray <code> BigDecimal[]</code>
	 * @param balances <code> List</code>
	 * @return <code> List</code>
	 */
	private List<ReportCorporativeResult> setBalancesArray(BigDecimal[] balancesArray, List<ReportCorporativeResult> balances) {
		for (int i = 0; i < balances.size(); i++) {
			ReportCorporativeResult objResult = balances.get(i);
			objResult.setGuaranteeBalance(balancesArray[i]);			
		}
		return balances;
	}
	
	/**
	 * Method will return a array that is constructed from given list.
	 *
	 * @param reportOperations <code> List</code>
	 * @param corpOperation <code> List</code>
	 * @param exchangeRateDay the exchange rate day
	 * @return <code> CorporativeOperation</code>
	 */
	public List<ReportCorporativeResult> fillOriginOperations(List<Object[]> reportOperations,	CorporativeOperation corpOperation,DailyExchangeRates exchangeRateDay){
		List<ReportCorporativeResult> result = null;
		if(reportOperations != null && !reportOperations.isEmpty()){
			result = new ArrayList<ReportCorporativeResult>();
			for(Object[] oper : reportOperations){
				result.add(fillOperations(oper, corpOperation, BigDecimal.ONE, 0, false, false, false,exchangeRateDay));
			}
		}
		return result;
	}
}
