package com.pradera.corporateevents.corporativeprocess.importanceevents.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.corporateevents.service.CorporateEventComponentServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.corporateevents.corporativeprocess.importanceevents.service.ImportanceEventServiceBean;
import com.pradera.corporateevents.corporativeprocess.importanceevents.to.CorporativeOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.corporatives.CapitalIncreaseMotive;
import com.pradera.model.corporatives.CorporativeEventType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.SecuritiesCorporative;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;


// TODO: Auto-generated Javadoc
/**
 * Clase fachada de hechos de importacia
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ImportanceEventFacade {

	/** The corporate event component service bean. */
	@EJB
	private CorporateEventComponentServiceBean corporateEventComponentServiceBean;

	/** The importance event service bean. */
	@Inject
	private ImportanceEventServiceBean importanceEventServiceBean;

	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;


	/**
	 * Metodo que valida la concurrencia de los datos.
	 *
	 * @param valOperation the val operation
	 * @throws ServiceException the service exception
	 */
	public void validateConcunrrency(CorporativeOperation valOperation)throws ServiceException{
		importanceEventServiceBean.validateConcunrrency(valOperation);
	}
    
    /**
     * Metodo que registra hecho de importancia.
     *
     * @param operation the operation
     * @return the corporative operation
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.CORPORATIVE_EVENT_AUTOMATIC_REGISTER)
	public CorporativeOperation registerCorporativeOperation(CorporativeOperation operation)throws ServiceException{	
    	//Audit Process
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	 
		return importanceEventServiceBean.registerCorporativeOperation(operation,loggerUser);
	}
    
	
	/**
	 * Metodo que modifica hecho de importancia.
	 *
	 * @param operation the operation
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CORPORATIVE_EVENT_MODIFICATION)
	public CorporativeOperation modifiyCorporativeOperation(CorporativeOperation operation)throws ServiceException{
		//Audit Process
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		//	operation.setAudit(loggerUser);
		operation.setState(CorporateProcessStateType.MODIFIED.getCode());
		if(ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
			List<CapitalIncreaseMotive> tempIncreaseMOtive = operation.getCapitalIncreaseMotives();
			importanceEventServiceBean.deletePreviousCapitalIncreaseMotive(operation.getIdCorporativeOperationPk());
			for(CapitalIncreaseMotive motive: tempIncreaseMOtive){
				motive.setIdCapitalIncreasePk(null);
				 motive.setCorporativeOperation(operation);
				importanceEventServiceBean.create(motive);
			}
			operation.setCapitalIncreaseMotives(tempIncreaseMOtive);
		}
		return importanceEventServiceBean.updateCorporativeOperation(operation);
	}
	
/**
 * Metodo que anula un hecho de importancia.
 *
 * @param operation the operation
 * @throws ServiceException the service exception
 */
@ProcessAuditLogger(process=BusinessProcessType.CORPORATIVE_EVENT_DELETE)
	public void annulateCorporativeOperation(CorporativeOperation operation)throws ServiceException{
		//Audit Process
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		//		operation.setAudit(loggerUser);
		operation.setState(CorporateProcessStateType.ANNULLED.getCode());		
		 importanceEventServiceBean.updateCorporativeOperationState(operation);
	}


	/**
	 * Metodo que consulta un hecho de importancia.
	 *
	 * @param filter the filter
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CORPORATIVE_EVENT_QUERY)
	public CorporativeOperation findCorporativeOperationByFilter(CorporativeOperationTO filter)throws ServiceException{
		CorporativeOperation operation = importanceEventServiceBean.findCorporativeOperationByFilter(filter);

		if(operation!=null){
			if(operation.getCapitalIncreaseMotives()!=null){
				operation.getCapitalIncreaseMotives().size();
			}		
			}

		return operation;	}

	/**
	 * Metodo que retorna lista de valores destino de la escision.
	 *
	 * @param idCorporate the id corporate
	 * @return the list securities corporative
	 * @throws ServiceException the service exception
	 */
	public List<SecuritiesCorporative> getListSecuritiesCorporative(Long idCorporate)throws ServiceException{
		return importanceEventServiceBean.getListSecuritiesCorporative(idCorporate);
	}
	
	/**
	 * Metodo que lista hechos de importancia.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> findCorporativeOperationsByFilter(CorporativeOperationTO filter)throws ServiceException{
		List<CorporativeOperation> list = importanceEventServiceBean.findCorporativeOperationsByFilter(filter);

		return list;
	}

	/**
	 * Metodo que retorna lista de hechos de importancia validos.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> searchCorporativeOperationForValidat(CorporativeOperationTO filter)throws ServiceException{
		return importanceEventServiceBean.searchOperationForValidation(filter);
	}
	

	/**
	 * Metodo que retorna lista de hechos de importancia validos de pagos.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> searchCorporativeOperationForValidatePayment(CorporativeOperationTO filter)throws ServiceException{
		return importanceEventServiceBean.searchOperationForValidationPayment(filter);
	}
	
	/**
	 * Metodo que retorna tipo de evento corporativo.
	 *
	 * @param corpEventTypeFilter the corp event type filter
	 * @return the corporative event type
	 * @throws ServiceException the service exception
	 */
	public CorporativeEventType findCorporativeEventType(CorporativeEventType corpEventTypeFilter) throws ServiceException{
		return corporateEventComponentServiceBean.findCorporativeEventType(corpEventTypeFilter);
	}
	
}
