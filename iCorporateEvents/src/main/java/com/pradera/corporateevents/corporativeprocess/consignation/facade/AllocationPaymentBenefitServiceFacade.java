package com.pradera.corporateevents.corporativeprocess.consignation.facade;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.sql.rowset.serial.SerialException;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.beanio.BeanIOConfigurationException;
import org.beanio.BeanWriter;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.utils.BeanIOUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.swift.facade.SwiftWithdrawlProcess;
import com.pradera.core.component.swift.service.AutomaticReceptionFundsService;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.component.swift.to.CashAccountsSearchTO;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.corporateevents.corporativeprocess.consignation.service.AllocationPaymentBenefitServiceBean;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocateIssuerCashAccountTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationBankResumeTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationCorporativeDetailTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationIssuerResumeTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationPaymentProcessBankTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationPaymentProcessHolderTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationPaymentProcessIssuerTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationPaymentSearchTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationProcessIssuerResumeTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationProcessRegisterTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationProcessTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.RegisterAllocationProcess;
import com.pradera.corporateevents.corporativeprocess.service.CorporateServiceBean;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.FundsComponentService;
import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.corporatives.BenefitPaymentAllocation;
import com.pradera.model.corporatives.BenefitPaymentFile;
import com.pradera.model.corporatives.BlockBenefitDetail;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.HolderCashMovement;
import com.pradera.model.corporatives.PaymentAllocationDetail;
import com.pradera.model.corporatives.PayrollConsignedOperation;
import com.pradera.model.corporatives.type.AllocationProcessStateType;
import com.pradera.model.corporatives.type.AllocationProcessType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.corporatives.type.PayrollStateType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.type.BenefitPaymentType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.HolderFundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.funds.type.HolderCashMovementStateType;
import com.pradera.model.funds.type.HolderFundsOperationStateType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.type.ReportFormatType;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AllocationPaymentBenefitServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 31/10/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AllocationPaymentBenefitServiceFacade extends CrudDaoServiceBean {

	/** The cash account service. */
	@EJB
	CashAccountManagementService cashAccountService;

	/** The allocation benefit service. */
	@EJB
	AllocationPaymentBenefitServiceBean allocationBenefitService;

	/** The batch process service facade. */
	@EJB
	BatchServiceBean batchProcessService;

	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;

	/** The executor component service bean. */
	@Inject
	Instance<AccountsComponentService> executorComponentServiceBean;
	
	/** The funds component service. */
	@Inject
	Instance<FundsComponentService> fundsComponentService;

	/** The swift withdrawl. */
	@Inject
	SwiftWithdrawlProcess swiftWithdrawl;

	/** The reception service. */
	@EJB
	AutomaticReceptionFundsService receptionService;

	/** The report service. */
	@Inject 
	Instance<ReportGenerationService> reportService;
	
	/** The corporate service bean. */
	@EJB
	CorporateServiceBean corporateServiceBean;
	
	/** The parameter service. */
	@EJB
	ParameterServiceBean parameterService;
	
	/** The trn central devolution. */
	@Inject @Configurable
	String idIssuerBC;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The trn central devolution. */
	String trnCentralDevolution;

	@Inject @StageDependent
	protected String serverPath;

	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/**
	 * Metodo que retorna lista de emisores activos.
	 *
	 * @return the issuers
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> getIssuers() throws ServiceException {
		List<Issuer> lstIssuer = cashAccountService.getActivateIssuer();
		return lstIssuer;
	}

	/**
	 * Metodo que retorna monedas BOB y USD
	 *
	 * @return the currency
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getCurrency() throws ServiceException {
		List<ParameterTable> lstCurrency = new ArrayList<ParameterTable>();
		List<Integer> lstParameterTablePk = new ArrayList<Integer>();
		lstParameterTablePk.add(CurrencyType.PYG.getCode());
		lstParameterTablePk.add(CurrencyType.USD.getCode());
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		filter.setLstParameterTablePk(lstParameterTablePk);
		lstCurrency = parameterService.getListParameterTableServiceBean(filter);
		return lstCurrency;
	}

	/**
	 * Metodo que retorna estado/tipo proceso de la consignacion.
	 * preliminar - definitivo
	 *
	 * @return the allocation processes type
	 */
	public List<ParameterTable> getAllocationProcessesType(){
		List<ParameterTable> lstProcessType = allocationBenefitService.getAllocationProcessType();
		return lstProcessType;
	}

	/**
	 * Metodo que ejecuta bachero de registro de consignacion.
	 *
	 * @param lstCorporativeDetail the lst corporative detail
	 * @param commissionExcluded the commission excluded
	 * @param allocationPaymentSearch the allocation payment search
	 * @throws ServiceException the service exception
	 */
	public void registerPreliminarAllocationProcess(List<String> lstCorporativeDetail,boolean commissionExcluded, AllocationPaymentSearchTO allocationPaymentSearch) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//TODO revisar si privilegio es el correcto
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		String corporatives = StringUtils.EMPTY;
		String strCommissionExcluded = StringUtils.EMPTY;
		String issuer = StringUtils.EMPTY;
		String securities = StringUtils.EMPTY;
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeDetail)){
			for(String corporative : lstCorporativeDetail){
				if(StringUtils.EMPTY.equals(corporatives)){
					corporatives = corporative;
				}else{
					corporatives = corporatives + "," + corporative;
				}
			}
		}

		if(commissionExcluded){
			strCommissionExcluded = BooleanType.YES.getCode().toString();
		}else{
			strCommissionExcluded = BooleanType.NO.getCode().toString();
		}
		if(allocationPaymentSearch.getIssuer()!=null && 
				allocationPaymentSearch.getIssuer().getIdIssuerPk()!=null){
			issuer = allocationPaymentSearch.getIssuer().getIdIssuerPk();
		}
		if(allocationPaymentSearch.getSecurity()!=null && 
				allocationPaymentSearch.getSecurity().getIdSecurityCodePk()!=null){
			securities = allocationPaymentSearch.getSecurity().getIdSecurityCodePk();
		}
		Map<String,Object> processParameters= new HashMap<String, Object>();
		processParameters.put("corporatives",corporatives);
		processParameters.put("commissionExcluded",strCommissionExcluded);
		processParameters.put("currency",allocationPaymentSearch.getCurrency().toString());
		processParameters.put("issuer",issuer);
		processParameters.put("security",securities);
		processParameters.put("userId", loggerUser.getUserName());
		BusinessProcess objBusinessProcess= new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		batchProcessService.registerBatchTx(loggerUser.getUserName(), objBusinessProcess, processParameters);
	}

	/**
	 * Metodo que ejecuta bachero de registro definitivo de consignacion.
	 *
	 * @param allocationID the allocation id
	 * @throws ServiceException the service exception
	 */
	public void executeDefinitiveAllocation(String allocationID) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//TODO chekiar el privilegio correcto
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDefinitive());
		Map<String,Object> processParameters= new HashMap<String, Object>();
		processParameters.put("idAllocateProcess",allocationID);
		processParameters.put("userId", loggerUser.getUserName());
		BusinessProcess objBusinessProcess= new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.DEFINITIVE_ALLOCATION_PROCESS.getCode());
		batchProcessService.registerBatchTx(loggerUser.getUserName(), objBusinessProcess, processParameters);
	}
	
	/**
	 * Metodo que realiza el registro de la consignacion.
	 *
	 * @param lstCorporatives the lst corporatives
	 * @param commissionExcluded the commission excluded
	 * @param currency the currency
	 * @param issuer the issuer
	 * @param security the security
	 * @param userID the user id
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	
	private Integer getBlockIndicatorWithCorporativeType(BlockCorporativeResult blockRetentionResult, Integer corporativeType) {
		Integer blockIndicator = null;
		
		if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corporativeType)){
			blockIndicator = blockRetentionResult.getIndAmortization();
		}else if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corporativeType)){
			blockIndicator = blockRetentionResult.getIndInterest();
		}else if(ImportanceEventType.CASH_DIVIDENDS.getCode().equals(corporativeType)){
			blockIndicator = blockRetentionResult.getIndCashDividend();
		}else if(ImportanceEventType.REMANENT_PAYMENT.getCode().equals(corporativeType)){
			blockIndicator = blockRetentionResult.getIndRemanent();
		}else if(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(corporativeType)){
			blockIndicator = blockRetentionResult.getIndSuscription();
		}else if(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode().equals(corporativeType)){
			blockIndicator = blockRetentionResult.getIndContributionReturn();
		}else if(ImportanceEventType.EARLY_PAYMENT.getCode().equals(corporativeType)){
			blockIndicator = blockRetentionResult.getIndRescue();
		}
		
		return blockIndicator;
	}
	
	public RegisterAllocationProcess registerAllocationProcess(List<Long> lstCorporatives, boolean commissionExcluded, Integer currency, String issuer, String security, String userID) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		return allocationBenefitService.registerAllocationProcess(lstCorporatives, commissionExcluded, currency, issuer, security, userID);
	}

	public void createPaymentAllocationDetail(RegisterAllocationProcess registerAllocationProcess) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());

		allocationBenefitService.createPaymentAllocationDetail(registerAllocationProcess);	
	}

	public boolean updateBenefitPayment(Long idBenefitPaymentAllocationPk, boolean commissionExcluded, List<Long> lstCorporatives) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		return allocationBenefitService.updateBenefitPayment(idBenefitPaymentAllocationPk, commissionExcluded, lstCorporatives);
	}
	/*

	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean updateBenefitPayment(Long idBenefitPaymentAllocationPk, boolean commissionExcluded, List<Long> lstCorporatives) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(idBenefitPaymentAllocationPk, BenefitPaymentAllocation.class);
		BigDecimal allocationAmount = allocationBenefitService.getTotalAllocationAmount(benefitPaymentAllocation.getIdBenefitPaymentPK());
		if(!commissionExcluded){
			BigDecimal totalCommissionAmount = allocationBenefitService.getCommissionAmount(lstCorporatives);
			allocationAmount = allocationAmount.add(totalCommissionAmount);
		}
		benefitPaymentAllocation.setAllocationProcessAmount(allocationAmount);
		allocationBenefitService.update(benefitPaymentAllocation);
		
		return true;
	}
	
	public RegisterAllocationProcess registerAllocationProcess(List<Long> lstCorporatives, boolean commissionExcluded, Integer currency, String issuer, String security, String userID)
						throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		//holder accounts que no van a cobrar - que se le retiene el pago por bloqueo de cuenta titular
		List<Long> lstRetentionHolderAccount = new ArrayList<Long>();

		
		//titulares que fueron bloqueados 
		List<Object[]> holderAccountList = allocationBenefitService.getHolderAccountsByCorporatives(lstCorporatives);
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccountList)){
			for(Object[] holderAccount : holderAccountList){
				Long holderAccountPK = Long.valueOf(holderAccount[0].toString());
				Integer holderAccountState = Integer.valueOf(holderAccount[1].toString());
				if(!HolderAccountStatusType.ACTIVE.getCode().equals(holderAccountState)){
					lstRetentionHolderAccount.add(holderAccountPK);
				}
			}
		}

		//lista de result de corporativos bloqueados, exceptuando los titulares que ya se encuentran bloqueados
		List<BlockCorporativeResult> blockCorporativeResultWithoutHolderAccountBlocked = allocationBenefitService.getBlockRetentionCorporativeResult(lstCorporatives,lstRetentionHolderAccount);

		List<CorporativeProcessResult> corporativeResultWithoutHolderAccountBlocked = null; 
		if( blockCorporativeResultWithoutHolderAccountBlocked!=null && blockCorporativeResultWithoutHolderAccountBlocked.size()> 0) {
			corporativeResultWithoutHolderAccountBlocked = new ArrayList<CorporativeProcessResult>();
			for(BlockCorporativeResult blockCPR: blockCorporativeResultWithoutHolderAccountBlocked) {
				corporativeResultWithoutHolderAccountBlocked.add(blockCPR.getCorporativeProcessResult());
			}
		}

		//lista de result de corporativos, de los titulares que ya se encuentran bloqueados (esto incluye CPR bloqueados y no bloqueados, solo es a nivel de titular)
		List<CorporativeProcessResult> corporativeResultWithHolderAccountBlocked = allocationBenefitService.getCorporativeResultWithHolderAccountBlock(lstCorporatives,lstRetentionHolderAccount);
		
		List<Long> lstIdCorporativeProcessResultBloq = null;
		List<CorporativeProcessResult> lstCorporativeResultBlocked = null; 
		if( (corporativeResultWithoutHolderAccountBlocked!=null && corporativeResultWithoutHolderAccountBlocked.size()>0) || (corporativeResultWithHolderAccountBlocked!=null && corporativeResultWithHolderAccountBlocked.size()>0)) {
			lstCorporativeResultBlocked = new ArrayList<CorporativeProcessResult>();
			lstIdCorporativeProcessResultBloq = new ArrayList<Long>();
			
			if(corporativeResultWithoutHolderAccountBlocked!=null && corporativeResultWithoutHolderAccountBlocked.size()>0) {
				for(CorporativeProcessResult cpr: corporativeResultWithoutHolderAccountBlocked) {
					lstCorporativeResultBlocked.add(cpr);	
					lstIdCorporativeProcessResultBloq.add(cpr.getIdCorporativeProcessResult());
				}
			}

			if(corporativeResultWithHolderAccountBlocked!=null && corporativeResultWithHolderAccountBlocked.size()>0) {
				for(CorporativeProcessResult cpr: corporativeResultWithHolderAccountBlocked) {
					lstCorporativeResultBlocked.add(cpr);
					lstIdCorporativeProcessResultBloq.add(cpr.getIdCorporativeProcessResult());
				}
			}
			
		}
		
		
		List<CorporativeProcessResult> corporativeResultWithOutCorporativeProcessResultBlock = 
					allocationBenefitService.getCorporativeResultWithOutCorporativeProcessResultBlock(lstCorporatives, lstIdCorporativeProcessResultBloq);
		

		BenefitPaymentAllocation benefitPaymentAllocation = saveAllocationProcess(issuer,security,currency,lstCorporatives,commissionExcluded);
		
		RegisterAllocationProcess registerAllocationProcess = new RegisterAllocationProcess();
		registerAllocationProcess.setIdBenefitPaymentPK(benefitPaymentAllocation.getIdBenefitPaymentPK());
		registerAllocationProcess.setLstCorporativeResultBlocked(lstCorporativeResultBlocked);
		registerAllocationProcess.setCorporativeResultWithOutCorporativeProcessResultBlock(corporativeResultWithOutCorporativeProcessResultBlock);
		registerAllocationProcess.setCurrency(currency);
		
		return registerAllocationProcess;
	}
	
	public void createPaymentAllocationDetail(RegisterAllocationProcess registerAllocationProcess) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		Long idBenefitPaymentPK = registerAllocationProcess.getIdBenefitPaymentPK();
		List<CorporativeProcessResult> corporativeResultWithOutCorporativeProcessResultBlock = registerAllocationProcess.getCorporativeResultWithOutCorporativeProcessResultBlock();;
		List<CorporativeProcessResult> lstCorporativeResultBlocked = registerAllocationProcess.getLstCorporativeResultBlocked();
		Integer currency = registerAllocationProcess.getCurrency();
		
		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(idBenefitPaymentPK, BenefitPaymentAllocation.class);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeResultBlocked)){
			//generateDetailBlockRetention(lstCorporativeResultBlocked, benefitPaymentAllocation, currency, idepositarySetup.getPaymentType());
			generatePaymentAllocationDetail(true,lstCorporativeResultBlocked, benefitPaymentAllocation, currency, idepositarySetup.getPaymentType());
		}

		if(Validations.validateListIsNotNullAndNotEmpty(corporativeResultWithOutCorporativeProcessResultBlock)){
			//generateDetailCorporativeResultAvailable(corporativeResultWithOutCorporativeProcessResultBlock,benefitPaymentAllocation,currency,idepositarySetup.getPaymentType());
			generatePaymentAllocationDetail(false,corporativeResultWithOutCorporativeProcessResultBlock, benefitPaymentAllocation, currency, idepositarySetup.getPaymentType());
		}
	}
	
	*/

	public boolean registerAllocationProcessBackup(List<Long> lstCorporatives, boolean commissionExcluded, Integer currency, String issuer, String security, String userID)
						throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		//holder accounts que no van a cobrar - que se le retiene el pago por bloqueo de cuenta titular
		List<Long> lstRetentionHolderAccount = new ArrayList<Long>();

		
		//titulares que fueron bloqueados 
		List<Object[]> holderAccountList = allocationBenefitService.getHolderAccountsByCorporatives(lstCorporatives);
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccountList)){
			for(Object[] holderAccount : holderAccountList){
				Long holderAccountPK = Long.valueOf(holderAccount[0].toString());
				Integer holderAccountState = Integer.valueOf(holderAccount[1].toString());
				if(!HolderAccountStatusType.ACTIVE.getCode().equals(holderAccountState)){
					lstRetentionHolderAccount.add(holderAccountPK);
				}
			}
		}
		
		//corporative process result - saldo que se bloquea
		Map<Long,BigDecimal> mpCprWithBlockPayment = new HashMap<Long,BigDecimal>();
		//corporative process result - saldo disponible que no se bloqueo
		Map<Long,BigDecimal> mpCprWithAvailablePayment = new HashMap<Long,BigDecimal>();

		//todos los corporativos process result que tienen indicador de blqoueo
		List<Long> lstIdCprBlockIndicator = new ArrayList<Long>();
		
		//todos los corporativos process result que tienen indicador de blqoueo y tienen saldo bloqueado
		List<Long> lstIdCprBlockIndicatorBlockPayment = new ArrayList<Long>();
		//todos los corporativos process result que tienen indicador de blqoueo y tienen saldo disponible
		List<Long> lstIdCprBlockIndicatorAvailablePayment = new ArrayList<Long>();
		

		//todos los corporativos process result que tienen Titular habilitado y no tienen indicador de bloqueo
		List<Long> lstIdCprNoBlockIndicatorAvailablePayment = new ArrayList<Long>();
		
		//todos los corporativos process result que tienen Titular blockeado y no tienen indicador de bloqueo
		List<Long> lstIdCprNoBlockIndicatorBlockPayment = new ArrayList<Long>();
		

		//lista de corporativos bloqueados en saldo, exceptuando los titulares que ya se encuentran bloqueados
		List<BlockCorporativeResult> blockCorporativeResultWithoutHolderAccountBlocked = null; //allocationBenefitService.getBlockRetentionCorporativeResult(lstCorporatives,lstRetentionHolderAccount,currency);

		//C + D
		if(Validations.validateListIsNotNullAndNotEmpty(blockCorporativeResultWithoutHolderAccountBlocked)){
			for(BlockCorporativeResult blockRetentionResult : blockCorporativeResultWithoutHolderAccountBlocked){
				
				Integer corporativeType = blockRetentionResult.getCorporativeOperation().getCorporativeEventType().getCorporativeEventType();
				Integer blockIndicator = getBlockIndicatorWithCorporativeType(blockRetentionResult, corporativeType);
				BigDecimal totalBlockBalance = BigDecimal.ZERO;
				BigDecimal totalAvailableBalance = BigDecimal.ZERO;
				
				if(BooleanType.YES.getCode().equals(blockIndicator)){
					//saldo bloqueado que no se va a cobrar
					if(blockRetentionResult.getBlockedBalance().doubleValue() > 0){
						totalBlockBalance = blockRetentionResult.getBlockedBalance();
						mpCprWithBlockPayment.put(blockRetentionResult.getCorporativeProcessResult().getIdCorporativeProcessResult(), totalBlockBalance);
						lstIdCprBlockIndicatorBlockPayment.add(blockRetentionResult.getCorporativeProcessResult().getIdCorporativeProcessResult());	//C
					}
					//saldo disponible que si se va a cobrar
					if(blockRetentionResult.getCorporativeProcessResult().getAvailableBalance().doubleValue() > 0){
						totalAvailableBalance = blockRetentionResult.getCorporativeProcessResult().getAvailableBalance();
						mpCprWithAvailablePayment.put(blockRetentionResult.getCorporativeProcessResult().getIdCorporativeProcessResult(),totalAvailableBalance);
						lstIdCprBlockIndicatorAvailablePayment.add(blockRetentionResult.getCorporativeProcessResult().getIdCorporativeProcessResult());	//D
					}
					lstIdCprBlockIndicator.add(blockRetentionResult.getCorporativeProcessResult().getIdCorporativeProcessResult());
				}				
			}
		}

		
		
		//A
		if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
			List<Object[]> lstBlockPayments = allocationBenefitService.getBlockPaymentIndicator(lstRetentionHolderAccount,currency);
			if(Validations.validateListIsNotNullAndNotEmpty(lstBlockPayments)){
				for(Object[] objResult : lstBlockPayments){
					//Integer indHaveBic = Integer.valueOf(objResult[1].toString());
					Long corporateResultPK = Long.valueOf(objResult[0].toString());
					
					lstIdCprNoBlockIndicatorAvailablePayment.add(corporateResultPK);
					
					/*
					if(idepositarySetup.getPaymentType() != null ){
						if(idepositarySetup.getPaymentType().equals(BenefitPaymentType.PDI.getCode())){//BooleanType.YES.getCode().equals(indHaveBic)
							directPaymentList.add(corporateResultPK);
						}else{
							indirectPaymentList.add(corporateResultPK);
						}
					}else {
						if(idepositarySetup.getPaymentType().equals(BenefitPaymentType.PDI.getCode())){//BooleanType.YES.getCode().equals(indHaveBic)
							directPaymentList.add(corporateResultPK);
						}else{
							indirectPaymentList.add(corporateResultPK);
						}
					}
					*/
				}
			}
		}

		//B
		// titulares a los que se le va a pagar, exceptuando los titulares bloqueados, y exceptuamos a los corporativos que tienen bloqueos 
		List<Object[]> lstPayments = allocationBenefitService.getPaymentByCorporatives(lstCorporatives, lstRetentionHolderAccount,lstIdCprBlockIndicator,currency);

		if(Validations.validateListIsNotNullAndNotEmpty(lstPayments)){
			for(Object[] obj : lstPayments){
				Long idCorporateResultID = Long.valueOf(obj[0].toString());
				//Integer indHaveBic = Integer.valueOf(obj[1].toString());
				lstIdCprNoBlockIndicatorBlockPayment.add(idCorporateResultID);
				/*
				if(idepositarySetup.getPaymentType() !=null && idepositarySetup.getPaymentType().equals(BenefitPaymentType.PDI.getCode())){//BooleanType.YES.getCode().equals(indHaveBic)
					directPaymentList.add(idCorporateResultID);
				}else{
					indirectPaymentList.add(idCorporateResultID);
				}*/
			}
		}

		BenefitPaymentAllocation benefitPaymentAllocation = saveAllocationProcess(issuer,security,currency,lstCorporatives,commissionExcluded);
		/*
		//GENERATE THE DETAIL FOR HOLDER ACCOUNTS WHICH WILL HAVE RETENTION IN DIFFERENT CORPORATIVE OPERATIONS
		if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
			generateDetailByHolderAccountDetail(lstCorporatives,lstRetentionHolderAccount,benefitPaymentAllocation);
		}

		//GENERATE THE DETAIL FOR HOLDERS WHICH WILL HAVE RETENTION IN DIFFERENT CORPORATIVE OPERATIONS
		if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolder)){
			generateDetailByHolderDetail(lstCorporatives,lstRetentionHolder,benefitPaymentAllocation);
		}
		*/
		//GENERATE THE DETAIL FOR HOLDER ACCOUNT BANK WHICH WILL HAVE RETENTION IN DIFFERENT CORPORATIVE OPERATIONS
		/*
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountNoBankAccount)){
			generateDetailByNoBankAccount(lstCorporatives,lstHolderAccountNoBankAccount,benefitPaymentAllocation);
		}
		*/

		//GENERATE THE DETAIL FOR BLOCKS WHICH WILL GENERATE RETENTION	-	C
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdCprBlockIndicatorBlockPayment)){
			generateDetailBlockRetention(lstIdCprBlockIndicatorBlockPayment,benefitPaymentAllocation,mpCprWithBlockPayment,currency,true,idepositarySetup.getPaymentType());
		}

		//GENERATE THE DETAIL FOR DIRECT PAYMENTS	-	A
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdCprNoBlockIndicatorAvailablePayment)){
			generateDetailByPayment(lstIdCprNoBlockIndicatorAvailablePayment,benefitPaymentAllocation,currency,idepositarySetup.getPaymentType());
		}

		//GENERATE THE DETAIL FOR INDIRECT PAYMENTS	-	B
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdCprNoBlockIndicatorBlockPayment)){
			generateDetailByPayment(lstIdCprNoBlockIndicatorBlockPayment,benefitPaymentAllocation,currency,idepositarySetup.getPaymentType());
		}

		BigDecimal allocationAmount = allocationBenefitService.getTotalAllocationAmount(benefitPaymentAllocation.getIdBenefitPaymentPK());
		if(!commissionExcluded){
			BigDecimal totalCommissionAmount = allocationBenefitService.getCommissionAmount(lstCorporatives);
			allocationAmount = allocationAmount.add(totalCommissionAmount);
		}
		benefitPaymentAllocation.setAllocationProcessAmount(allocationAmount);
		allocationBenefitService.update(benefitPaymentAllocation);
		
		
		
		return true;
	}
	

	/**
	 * Metodo que ejecuta componente de fondos
	 *
	 * @param centralizerFundOperation the centralizer fund operation
	 * @param idBusinessProcess the id business process
	 * @throws ServiceException the service exception
	 */
	private void executeFundComponent(FundsOperation centralizerFundOperation, Long idBusinessProcess) throws ServiceException{
		FundsComponentTO objComponent = new FundsComponentTO();
		objComponent.setIdBusinessProcess(idBusinessProcess);
		objComponent.setIdFundsOperationType(centralizerFundOperation.getFundsOperationType());
		objComponent.setIdFundsOperation(centralizerFundOperation.getIdFundsOperationPk());
		objComponent.setIdInstitutionCashAccount(centralizerFundOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		objComponent.setCashAmount(centralizerFundOperation.getOperationAmount());
		fundsComponentService.get().executeFundsComponent(objComponent);
	}
	
	private void executeFundComponentState(FundsOperation centralizerFundOperation, Long idBusinessProcess, Integer operationState) throws ServiceException{
		FundsComponentTO objComponent = new FundsComponentTO();
		objComponent.setIdBusinessProcess(idBusinessProcess);
		objComponent.setIdFundsOperationType(centralizerFundOperation.getFundsOperationType());
		objComponent.setIdFundsOperation(centralizerFundOperation.getIdFundsOperationPk());
		objComponent.setIdInstitutionCashAccount(centralizerFundOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		objComponent.setCashAmount(centralizerFundOperation.getOperationAmount());
		objComponent.setOperationState(operationState);
		fundsComponentService.get().executeFundsComponent(objComponent);
	}

	/**
	 * Metodo que retorna codigo de tipo de operacion
	 *
	 * @param corporativeProcessType the corporative process type
	 * @param participantFundOperation the participantcentralizer fund operation
	 * @return the long
	 */
		private Long validatecentralizerFundOperationtype(Integer corporativeProcessType, boolean participantFundOperation){
		Long centralizerFundOperationType = null;
		if(ImportanceEventType.CASH_DIVIDENDS.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				centralizerFundOperationType = ParameterFundsOperationType.INSTITUTIONS_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				centralizerFundOperationType = ParameterFundsOperationType.HOLDER_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				centralizerFundOperationType = ParameterFundsOperationType.INSTITUTIONS_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				centralizerFundOperationType = ParameterFundsOperationType.HOLDER_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.REMANENT_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				centralizerFundOperationType = ParameterFundsOperationType.INSTITUTIONS_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				centralizerFundOperationType = ParameterFundsOperationType.HOLDER_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				centralizerFundOperationType = ParameterFundsOperationType.INSTITUTIONS_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				centralizerFundOperationType = ParameterFundsOperationType.HOLDER_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				centralizerFundOperationType = ParameterFundsOperationType.INSTITUTIONS_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				centralizerFundOperationType = ParameterFundsOperationType.HOLDER_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.EARLY_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				//centralizerFundOperationType = FundsOperationType.
			}else{
				//centralizerFundOperationType = FundsOperationType.
			}
		}
		return centralizerFundOperationType;
	}

	/**
	 * Metodo que registra movimiento de cuenta efectivo del titular a consignar
	 *
	 * @param holderWithdrawl the holder withdrawl
	 * @param fundsOperation the funds operation
	 */
	private void saveHolderCashMovement(AllocationPaymentProcessHolderTO holderWithdrawl, FundsOperation fundsOperation){

		List<Long> lstBlocksBenefitDetail = holderWithdrawl.getLstBlockOperationPK();
		HolderCashMovement holderCashMovement = new HolderCashMovement();

		//IN THE CASE OF RETENTION BECAUSE OF BLOCK BALANCES, WE MUST GENERATE A HOLDER CASH MOVEMENT BY EACH BLOCK DOCUMENT
		if(Validations.validateListIsNotNullAndNotEmpty(lstBlocksBenefitDetail)){
			for(Long blockBenefitDetail : lstBlocksBenefitDetail){

				holderCashMovement = new HolderCashMovement();
				Object[] objBenefitBlockDetail = allocationBenefitService.getBlockOperationPK(blockBenefitDetail);
				Long blockOperationID = Long.valueOf(objBenefitBlockDetail[0].toString());
				BigDecimal grossAmount = new BigDecimal(objBenefitBlockDetail[1].toString());

				holderCashMovement.setFundsOperation(fundsOperation);

				BlockOperation blockOperation = new BlockOperation();
				blockOperation.setIdBlockOperationPk(blockOperationID);
				holderCashMovement.setBlockOperation(blockOperation);

				holderCashMovement.setCurrency(fundsOperation.getCurrency());
				Long lngFundOperationType = ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode();
				com.pradera.model.funds.FundsOperationType fundsOperationType = new com.pradera.model.funds.FundsOperationType();
				fundsOperationType.setIdFundsOperationTypePk(lngFundOperationType);

				holderCashMovement.setFundsOperationType(fundsOperationType);

				if(Validations.validateIsNotNullAndNotEmpty(holderWithdrawl.getBankPK())){
					Bank bank = new Bank();
					bank.setIdBankPk(holderWithdrawl.getBankPK());
					holderCashMovement.setBank(bank);
				}

				if(Validations.validateIsNotNullAndNotEmpty(holderWithdrawl.getHolderPK())){
					Holder holder = new Holder();
					holder.setIdHolderPk(holderWithdrawl.getHolderPK());
					holderCashMovement.setHolder(holder);
				}

				if(Validations.validateIsNotNullAndNotEmpty(holderWithdrawl.getHolderAccountPK())){
					HolderAccount holderAccount = new HolderAccount();
					holderAccount.setIdHolderAccountPk(holderWithdrawl.getHolderAccountPK());
					holderCashMovement.setHolderAccount(holderAccount);
				}

				if(Validations.validateIsNotNullAndNotEmpty(holderWithdrawl.getHolderAccountBankPK())){
					HolderAccountBank holderAccountBank = new HolderAccountBank();
					holderAccountBank.setIdHolderAccountBankPk(holderWithdrawl.getHolderAccountBankPK());
					holderCashMovement.setHolderAccountBank(holderAccountBank);
				}

				holderCashMovement.setHolderAmount(grossAmount);
				holderCashMovement.setBankAccountNumber(holderWithdrawl.getAccountNumber());
				holderCashMovement.setBankAccountType(holderWithdrawl.getAccountType());
				holderCashMovement.setFullName(holderWithdrawl.getName());
				holderCashMovement.setDocumentHolder(holderWithdrawl.getDocumentNumber());
				holderCashMovement.setProcessType(holderWithdrawl.getProcessType());
				holderCashMovement.setMovementDate(CommonsUtilities.currentDate());
				holderCashMovement.setMovementState(HolderCashMovementStateType.RESERVED.getCode());

				allocationBenefitService.create(holderCashMovement);
			}
		}else{
			holderCashMovement = new HolderCashMovement();
			holderCashMovement.setFundsOperation(fundsOperation);

			holderCashMovement.setCurrency(fundsOperation.getCurrency());
			Long centralizerFundOperationType = validatecentralizerFundOperationtype(holderWithdrawl.getCorporativeProcessType(),false);
			com.pradera.model.funds.FundsOperationType objcentralizerFundOperationType = 
					allocationBenefitService.find(centralizerFundOperationType,com.pradera.model.funds.FundsOperationType.class);

			holderCashMovement.setFundsOperationType(objcentralizerFundOperationType);

			if(Validations.validateIsNotNullAndNotEmpty(holderWithdrawl.getBankPK())){
				Bank bank = new Bank();
				bank.setIdBankPk(holderWithdrawl.getBankPK());
				holderCashMovement.setBank(bank);
			}

			if(Validations.validateIsNotNullAndNotEmpty(holderWithdrawl.getHolderPK())){
				Holder holder = new Holder();
				holder.setIdHolderPk(holderWithdrawl.getHolderPK());
				holderCashMovement.setHolder(holder);
			}

			if(Validations.validateIsNotNullAndNotEmpty(holderWithdrawl.getHolderAccountPK())){
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(holderWithdrawl.getHolderAccountPK());
				holderCashMovement.setHolderAccount(holderAccount);
			}

			if(Validations.validateIsNotNullAndNotEmpty(holderWithdrawl.getHolderAccountBankPK())){
				HolderAccountBank holderAccountBank = new HolderAccountBank();
				holderAccountBank.setIdHolderAccountBankPk(holderWithdrawl.getHolderAccountBankPK());
				holderCashMovement.setHolderAccountBank(holderAccountBank);
			}

			holderCashMovement.setHolderAmount(holderWithdrawl.getHolderAmount());
			holderCashMovement.setBankAccountNumber(holderWithdrawl.getAccountNumber());
			holderCashMovement.setBankAccountType(holderWithdrawl.getAccountType());
			holderCashMovement.setFullName(holderWithdrawl.getName());
			holderCashMovement.setDocumentHolder(holderWithdrawl.getDocumentNumber());
			holderCashMovement.setProcessType(holderWithdrawl.getProcessType());
			holderCashMovement.setMovementDate(CommonsUtilities.currentDate());
			holderCashMovement.setMovementState(HolderCashMovementStateType.PAID.getCode());

			allocationBenefitService.create(holderCashMovement);
		}
	}

	/**
	 * Metodo que registra movimiento de cuenta efectivo centralizadora a consignar
	 *
	 * @param lstCentralRegisters the lst central registers
	 * @param centralWithdrawlFundOperation the central withdrawl fund operation
	 */
	private void saveCentralHolderCashMovement(List<AllocationPaymentProcessHolderTO> lstCentralRegisters, FundsOperation centralWithdrawlFundOperation){

		if(Validations.validateListIsNotNullAndNotEmpty(lstCentralRegisters))
		{
			for(AllocationPaymentProcessHolderTO centralCashWithdrawl : lstCentralRegisters){
				List<Long> lstBlocksBenefitDetail = centralCashWithdrawl.getLstBlockOperationPK();
				if(Validations.validateListIsNotNullAndNotEmpty(lstBlocksBenefitDetail))
				{
					for(Long blockBenefitDetailPK : lstBlocksBenefitDetail){
						HolderCashMovement holderCashMovement = new HolderCashMovement();
						Object[] objBenefitBlockDetail = allocationBenefitService.getBlockOperationPK(blockBenefitDetailPK);
						Long blockOperationID = Long.valueOf(objBenefitBlockDetail[0].toString());
						BigDecimal grossAmount = new BigDecimal(objBenefitBlockDetail[1].toString());

						holderCashMovement.setFundsOperation(centralWithdrawlFundOperation);

						if(Validations.validateIsNotNullAndNotEmpty(blockOperationID)){
							BlockOperation blockOperation = new BlockOperation();
							blockOperation.setIdBlockOperationPk(blockOperationID);
							holderCashMovement.setBlockOperation(blockOperation);
						}

						holderCashMovement.setCurrency(centralWithdrawlFundOperation.getCurrency());

						Long lngFundOperationType = ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode();
						com.pradera.model.funds.FundsOperationType fundsOperationType = new com.pradera.model.funds.FundsOperationType();
						fundsOperationType.setIdFundsOperationTypePk(lngFundOperationType);
						holderCashMovement.setFundsOperationType(fundsOperationType);

						if(Validations.validateIsNotNullAndNotEmpty(centralCashWithdrawl.getBankPK())){
							Bank bank = new Bank();
							bank.setIdBankPk(centralCashWithdrawl.getBankPK());
							holderCashMovement.setBank(bank);
						}

						if(Validations.validateIsNotNullAndNotEmpty(centralCashWithdrawl.getHolderPK())){
							Holder holder = new Holder();
							holder.setIdHolderPk(centralCashWithdrawl.getHolderPK());
							holderCashMovement.setHolder(holder);
						}

						if(Validations.validateIsNotNullAndNotEmpty(centralCashWithdrawl.getHolderAccountPK())){
							HolderAccount holderAccount = new HolderAccount();
							holderAccount.setIdHolderAccountPk(centralCashWithdrawl.getHolderAccountPK());
							holderCashMovement.setHolderAccount(holderAccount);
						}

						holderCashMovement.setHolderAmount(grossAmount);

						if(Validations.validateIsNotNullAndNotEmpty(centralCashWithdrawl.getHolderAccountBankPK())){
							HolderAccountBank holderAccountBank = new HolderAccountBank();
							holderAccountBank.setIdHolderAccountBankPk(centralCashWithdrawl.getHolderAccountBankPK());
							holderCashMovement.setHolderAccountBank(holderAccountBank);
						}

						holderCashMovement.setBankAccountNumber(centralCashWithdrawl.getAccountNumber());
						holderCashMovement.setBankAccountType(centralCashWithdrawl.getAccountType());
						holderCashMovement.setFullName(centralCashWithdrawl.getName());
						holderCashMovement.setDocumentHolder(centralCashWithdrawl.getDocumentNumber());
						holderCashMovement.setProcessType(centralCashWithdrawl.getProcessType());
						holderCashMovement.setMovementDate(CommonsUtilities.currentDate());
						holderCashMovement.setMovementState(HolderCashMovementStateType.PAID.getCode());

						allocationBenefitService.create(holderCashMovement);
					}
				}else
				{
					HolderCashMovement holderCashMovement = new HolderCashMovement();
					holderCashMovement.setFundsOperation(centralWithdrawlFundOperation);

					holderCashMovement.setCurrency(centralWithdrawlFundOperation.getCurrency());

					Long centralizerFundOperationType = validatecentralizerFundOperationtype(centralCashWithdrawl.getCorporativeProcessType(),false);
					com.pradera.model.funds.FundsOperationType objcentralizerFundOperationType = 
							allocationBenefitService.find(centralizerFundOperationType,com.pradera.model.funds.FundsOperationType.class);
					holderCashMovement.setFundsOperationType(objcentralizerFundOperationType);

					if(Validations.validateIsNotNullAndNotEmpty(centralCashWithdrawl.getBankPK())){
						Bank bank = new Bank();
						bank.setIdBankPk(centralCashWithdrawl.getBankPK());
						holderCashMovement.setBank(bank);
					}

					if(Validations.validateIsNotNullAndNotEmpty(centralCashWithdrawl.getHolderPK())){
						Holder holder = new Holder();
						holder.setIdHolderPk(centralCashWithdrawl.getHolderPK());
						holderCashMovement.setHolder(holder);
					}

					if(Validations.validateIsNotNullAndNotEmpty(centralCashWithdrawl.getHolderAccountPK())){
						HolderAccount holderAccount = new HolderAccount();
						holderAccount.setIdHolderAccountPk(centralCashWithdrawl.getHolderAccountPK());
						holderCashMovement.setHolderAccount(holderAccount);
					}

					if(Validations.validateIsNotNullAndNotEmpty(centralCashWithdrawl.getHolderAccountBankPK())){
						HolderAccountBank holderAccountBank = new HolderAccountBank();
						holderAccountBank.setIdHolderAccountBankPk(centralCashWithdrawl.getHolderAccountBankPK());
						holderCashMovement.setHolderAccountBank(holderAccountBank);
					}

					holderCashMovement.setHolderAmount(centralCashWithdrawl.getHolderAmount());
					holderCashMovement.setBankAccountNumber(centralCashWithdrawl.getAccountNumber());
					holderCashMovement.setBankAccountType(centralCashWithdrawl.getAccountType());
					holderCashMovement.setFullName(centralCashWithdrawl.getName());
					holderCashMovement.setDocumentHolder(centralCashWithdrawl.getDocumentNumber());
					holderCashMovement.setProcessType(centralCashWithdrawl.getProcessType());
					holderCashMovement.setMovementDate(CommonsUtilities.currentDate());
					holderCashMovement.setMovementState(HolderCashMovementStateType.PAID.getCode());

					allocationBenefitService.create(holderCashMovement);

				}
			}
		}
	}

	/**
	 * Metodo que genera el archivo xml de consignacion de pagos.
	 *
	 * @param xml the xml
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param bankProcess the bank process
	 * @throws SerialException the serial exception
	 */
	private void saveGeneratedXMLFiles(String xml, BenefitPaymentAllocation benefitPaymentAllocation,AllocationPaymentProcessBankTO bankProcess) throws SerialException {
		BenefitPaymentFile benefitPaymentFile = new BenefitPaymentFile();
		benefitPaymentFile.setCreationDate(new Date());
		byte[] objByte = null;
		try {
			objByte = xml.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		benefitPaymentFile.setDocumentFile(objByte);
		Date dtToday = new Date();
		SimpleDateFormat nameFormat = new SimpleDateFormat("ddMMyyyyKKmmSS");
		String timeFormat = nameFormat.format(dtToday);

		String fileName = bankProcess.getBankBicCode() + GeneralConstants.UNDERLINE_SEPARATOR + timeFormat;
		benefitPaymentFile.setFileName(fileName);
		benefitPaymentFile.setFileType(1);
		Long centralizerFundOperationPK = bankProcess.getIdCentralFundOperation();
		FundsOperation centralizerFundOperation = allocationBenefitService.find(centralizerFundOperationPK,FundsOperation.class);
		benefitPaymentFile.setFundsOperation(centralizerFundOperation);
		allocationBenefitService.create(benefitPaymentFile);

		createXmlUserFile(objByte,fileName);
	}

	/**
	 * Valida si existe cuenta efectivo de devolucion de aportes.
	 *
	 * @param currency the currency
	 * @return the integer
	 */
	public Integer validateAllocationProcess(Integer currency){
		Integer validationProcess = null;
		InstitutionCashAccount devolutionAccount = allocationBenefitService.getDevolutionCommercialAccount(currency);
		if(Validations.validateIsNull(devolutionAccount)){
			validationProcess = PropertiesConstants.ERROR_ALLOCATION_PROCESS_NO_COMMERCIAL_RETURN_CASH_ACCOUNT;
		}
		
		return validationProcess;
	}

	/**
	 * Metodo que envia archivo xml de consignacion de pagos a la bitacora.
	 *
	 * @param xml the xml
	 * @param fileName the file name
	 */
	private void createXmlUserFile(byte[] xml, String fileName){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

        ReportUser reportUser = new ReportUser();
        reportUser.setUserName(loggerUser.getUserName());

        ReportFile reportFile = new ReportFile();
        reportFile.setFile(xml);
        reportFile.setPhysicalName(fileName);
        reportFile.setReportType(ReportFormatType.XML.getCode());
        List<ReportFile> reportFilelst = new ArrayList<ReportFile>();
        reportFilelst.add(reportFile);

		try{
			reportService.get().saveReportFile(ReportIdType.ALLOCATION_PAYMENT_BENEFITS_HOLDER_DETAIL.getCode(), reportUser, null, null, loggerUser, reportFilelst);
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que retorna lista de consignaciones de pago en estado preliminar.
	 *
	 * @param objSearch the obj search
	 * @return the preliminar allocation process
	 * @throws ServiceException the service exception
	 */
	public List<AllocationProcessTO> getPreliminarAllocationProcess(AllocationPaymentSearchTO objSearch) throws ServiceException{

		List<AllocationProcessTO> lstAllocationProcess = new ArrayList<AllocationProcessTO>();
		AllocationProcessTO allocationProcess = null;

		List<BenefitPaymentAllocation> lstBenefitAllocation = allocationBenefitService.getBenefitAllocationProcess(objSearch);
		if(!lstBenefitAllocation.isEmpty()){
			Map<Integer,String> consignationProcessState = new HashMap<>();
			Map<Integer,String> currencyDescription = new HashMap<>();
			ParameterTableTO filter = new ParameterTableTO();
			filter.setMasterTableFk(MasterTableType.CONSIGNATION_PROCESS_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)){
				consignationProcessState.put(param.getParameterTablePk(), param.getParameterName());
			}
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)){
				currencyDescription.put(param.getParameterTablePk(), param.getDescription());
			}
			for(BenefitPaymentAllocation benefitPaymentAllocation : lstBenefitAllocation){
				allocationProcess = new AllocationProcessTO();
				allocationProcess.setAllocatedProcessAmount(benefitPaymentAllocation.getAllocationProcessAmount());
				allocationProcess.setAllocatedProcessQuantity(benefitPaymentAllocation.getAllocationQuantityProcess());
				allocationProcess.setIdAllocateProcessState(benefitPaymentAllocation.getAllocationProcessState());
				allocationProcess.setAllocateProcessState(consignationProcessState.get(benefitPaymentAllocation.getAllocationProcessState()));
				allocationProcess.setIdAllocationProcess(benefitPaymentAllocation.getIdBenefitPaymentPK());
				allocationProcess.setCurrency(currencyDescription.get(benefitPaymentAllocation.getCurrency()));
				allocationProcess.setIdCurrency(benefitPaymentAllocation.getCurrency());
				allocationProcess.setPaymentTypeDesc(paymentTypeDescription(lstPaymentTypeInConsignation()));
				if( benefitPaymentAllocation.getIssuer() != null ) {
					allocationProcess.setIssuer(benefitPaymentAllocation.getIssuer().getIdIssuerPk());
					allocationProcess.setIssuerDescription(benefitPaymentAllocation.getIssuer().getBusinessName());
					allocationProcess.setIssuerMnemonic(benefitPaymentAllocation.getIssuer().getMnemonic());
				}
				
				CashAccountsSearchTO parameter = new CashAccountsSearchTO();
				parameter.setCashAccountType(AccountCashFundsType.BENEFIT.getCode());
				parameter.setState(CashAccountStateType.ACTIVATE.getCode());
				parameter.setIssuer(benefitPaymentAllocation.getIssuer().getIdIssuerPk());
				parameter.setCurrency(benefitPaymentAllocation.getCurrency());
				List<InstitutionCashAccount> lst = cashAccountService.getCashAccounts(parameter);
				if(Validations.validateListIsNotNullAndNotEmpty(lst)){
					InstitutionCashAccount issuerCashAccount = lst.get(0);
					allocationProcess.setHaveInstitutionCashAccountDesc("SI");
				}else{
					allocationProcess.setHaveInstitutionCashAccountDesc("NO");
				}
				
				if(BooleanType.YES.getCode().equals(benefitPaymentAllocation.getIndExcludedCommissions())){
					allocationProcess.setTaxIncluded(BooleanType.NO.getValue());
				}else{
					allocationProcess.setTaxIncluded(BooleanType.YES.getValue());
				}
				lstAllocationProcess.add(allocationProcess);
			}
		}
		return lstAllocationProcess;
	}

	/**
	 * Metodo que retorna lista de procesos corporativos en estado definitivo.
	 *
	 * @param allocationPaymentSearch the allocation payment search
	 * @return the corporatives allocate
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> lstPaymentTypeInConsignation(){
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.CORPORATIVE_CONSIGNATION_PAYMENT_TYPE.getCode());
		filter.setState(1);
		return parameterService.getListParameterTableServiceBean(filter);
	}
	
	private String paymentTypeDescription(List<ParameterTable> lstParameter) {
		String paymentType = "";
		Integer countParameter = 0;
		if(lstParameter!=null && lstParameter.size() >0 ) {
			countParameter= lstParameter.size();
			if(countParameter == 1) {
				paymentType = lstParameter.get(0).getDescription();
			}else if(countParameter > 1){
				for(ParameterTable pt: lstParameter) {
					paymentType = pt.getDescription()+"/";
				}
				paymentType = paymentType.substring(0, paymentType.length()-1);
			}
		}
		
		return paymentType;
	}
	
	public List<AllocationProcessIssuerResumeTO> getCorporativesAllocate(AllocationPaymentSearchTO allocationPaymentSearch) throws ServiceException{
		List<AllocationProcessIssuerResumeTO> lstProcessByIssuer = new ArrayList<AllocationProcessIssuerResumeTO>();
		AllocationProcessIssuerResumeTO processByIssuer = null;
		String previousIssuer = null;
		String issuer = null;
		BigDecimal countProcess = BigDecimal.ZERO;
		BigDecimal amountProcess = BigDecimal.ZERO;
		BigDecimal pendingCountProcess = BigDecimal.ZERO;
		BigDecimal pendingAmountProcess = BigDecimal.ZERO;

		List<CorporativeOperation> corporativeOperations = allocationBenefitService.getAllocateCorporative(allocationPaymentSearch);
		if(!corporativeOperations.isEmpty()){
			processByIssuer = new AllocationProcessIssuerResumeTO();
			for(CorporativeOperation corporative : corporativeOperations){
				issuer = corporative.getIssuer().getIdIssuerPk();			
				if(Validations.validateIsNull(previousIssuer)){
					if(BooleanType.YES.getCode().equals(corporative.getIndPayed())){
						countProcess = BigDecimal.ONE;
						amountProcess = corporative.getPaymentAmount();
					}else{
						pendingCountProcess = BigDecimal.ONE;
						pendingAmountProcess = corporative.getPaymentAmount();
					}
				}else{
					if(previousIssuer.equals(issuer)){
						if(BooleanType.YES.getCode().equals(corporative.getIndPayed())){
							countProcess = countProcess.add(BigDecimal.ONE);
							amountProcess = amountProcess.add(corporative.getPaymentAmount());
						}else{
							pendingCountProcess = pendingCountProcess.add(BigDecimal.ONE);
							pendingAmountProcess = pendingAmountProcess.add(corporative.getPaymentAmount());
						}
					}else{
						processByIssuer.setAllocatedProcessQuantity(countProcess);
						processByIssuer.setAllocatedProcessAmount(amountProcess);
						processByIssuer.setAllocateProcessAmount(pendingAmountProcess);
						processByIssuer.setAllocateProcessQuantity(pendingCountProcess);
						processByIssuer.setIssuer(previousIssuer);
						CashAccountsSearchTO parameter = new CashAccountsSearchTO();
						parameter.setCashAccountType(AccountCashFundsType.BENEFIT.getCode());
						parameter.setState(CashAccountStateType.ACTIVATE.getCode());
						parameter.setIssuer(previousIssuer);
						parameter.setCurrency(allocationPaymentSearch.getCurrency());
						List<InstitutionCashAccount> lst = cashAccountService.getCashAccounts(parameter);
						Issuer objIssuer = helperComponentFacade.findIssuer(previousIssuer, null);
						processByIssuer.setIssuerDescription(objIssuer.getBusinessName());
						processByIssuer.setIssuerMnemonic(objIssuer.getMnemonic());
						if(Validations.validateListIsNotNullAndNotEmpty(lst)){
							InstitutionCashAccount issuerCashAccount = lst.get(0);
							processByIssuer.setCashAccountAmount(issuerCashAccount.getAvailableAmount());
						}else{
							processByIssuer.setCashAccountAmount(BigDecimal.ZERO);
						}
						lstProcessByIssuer.add(processByIssuer);
						processByIssuer = new AllocationProcessIssuerResumeTO();
						countProcess = BigDecimal.ZERO;
						amountProcess = BigDecimal.ZERO;
						pendingCountProcess = BigDecimal.ZERO;
						pendingAmountProcess = BigDecimal.ZERO;

						if(BooleanType.YES.getCode().equals(corporative.getIndPayed())){
							countProcess = countProcess.add(BigDecimal.ONE);
							amountProcess = amountProcess.add(corporative.getPaymentAmount());
						}else{
							pendingCountProcess = pendingCountProcess.add(BigDecimal.ONE);
							pendingAmountProcess = pendingAmountProcess.add(corporative.getPaymentAmount());
						}
					}
				}
				previousIssuer = issuer;
			}
			processByIssuer.setAllocatedProcessQuantity(countProcess);
			processByIssuer.setAllocatedProcessAmount(amountProcess);
			processByIssuer.setAllocateProcessAmount(pendingAmountProcess);
			processByIssuer.setAllocateProcessQuantity(pendingCountProcess);

			processByIssuer.setIssuer(previousIssuer);
			CashAccountsSearchTO parameter = new CashAccountsSearchTO();
			parameter.setCashAccountType(AccountCashFundsType.BENEFIT.getCode());
			parameter.setState(CashAccountStateType.ACTIVATE.getCode());
			parameter.setIssuer(previousIssuer);
			parameter.setCurrency(allocationPaymentSearch.getCurrency());
			Issuer objIssuer = helperComponentFacade.findIssuer(previousIssuer, null);
			List<InstitutionCashAccount> lst = cashAccountService.getCashAccounts(parameter);
			processByIssuer.setIssuerDescription(objIssuer.getBusinessName());
			processByIssuer.setIssuerMnemonic(objIssuer.getMnemonic());
			
			String paymentTypeDesc =  paymentTypeDescription(lstPaymentTypeInConsignation());
			if(Validations.validateListIsNotNullAndNotEmpty(lst)){
				InstitutionCashAccount issuerCashAccount = lst.get(0);
				processByIssuer.setCashAccountAmount(issuerCashAccount.getAvailableAmount());
				processByIssuer.setHaveInstitutionCashAccount(1);
				processByIssuer.setHaveInstitutionCashAccountDesc("SI");
			}else{
				processByIssuer.setCashAccountAmount(BigDecimal.ZERO);
				processByIssuer.setHaveInstitutionCashAccount(0);
				processByIssuer.setHaveInstitutionCashAccountDesc("NO");
			}
			processByIssuer.setPaymentTypeDesc(paymentTypeDesc);
			lstProcessByIssuer.add(processByIssuer);
			
		}
		return lstProcessByIssuer;
	}

	/**
	 * Metodo que obtiene lista de detalle de la consignacion de pagos.
	 *
	 * @param idAllocationProcess the id allocation process
	 * @return the corporatives by allocation process
	 * @throws ServiceException the service exception
	 */
	public List<AllocationCorporativeDetailTO> getCorporativesByAllocationProcess(Long idAllocationProcess) throws ServiceException{
		List<AllocationCorporativeDetailTO> lstResult = new ArrayList<AllocationCorporativeDetailTO>();
		AllocationCorporativeDetailTO corporativeDetail = new AllocationCorporativeDetailTO();
		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(idAllocationProcess,BenefitPaymentAllocation.class);
		List<PaymentAllocationDetail> lstAllocationDetail = allocationBenefitService.getCorporativesByAllocationProcess(Integer.valueOf(idAllocationProcess.toString()));
		if(Validations.validateListIsNotNullAndNotEmpty(lstAllocationDetail)){
			Map<Integer,String> corporativeStateDescription = new HashMap<>();
			ParameterTableTO filter = new ParameterTableTO();
			filter.setMasterTableFk(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)){
				corporativeStateDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			for(PaymentAllocationDetail allocationDetail : lstAllocationDetail){
				CorporativeOperation corporative = allocationDetail.getCorporativeOperation();
				corporativeDetail.setBenefitAmount(corporative.getPaymentAmount());
				corporativeDetail.setCorporativeState(corporativeStateDescription.get(corporative.getState()));
				corporativeDetail.setCorporativeType(corporative.getCorporativeEventType().getDescription());
				corporativeDetail.setCustodyAmount(corporative.getCommissionAmount());
				corporativeDetail.setIdCorporativeProcess(corporative.getIdCorporativeOperationPk().toString());
				if(BooleanType.YES.getCode().equals(benefitPaymentAllocation.getIndExcludedCommissions())){
					corporativeDetail.setIncludeCommission(false);
				}else{
					corporativeDetail.setIncludeCommission(true);
				}
				corporativeDetail.setSecurity(corporative.getSecurities().getIdSecurityCodePk());
				corporativeDetail.setTaxAmount(corporative.getTaxAmount());

				lstResult.add(corporativeDetail);
			}
		}

		return lstResult;
	}

	/**
	 * Metodo que obtiene lista de detalle de la consignacion de pagos.
	 *
	 * @param allocateProcessIssuer the allocate process issuer
	 * @param paymentDate the payment date
	 * @return the corporative to allocate
	 * @throws ServiceException the service exception
	 */
	public List<AllocationCorporativeDetailTO> getCorporativeToAllocate(AllocationProcessIssuerResumeTO allocateProcessIssuer, Date paymentDate, Integer paymentCurrency) throws ServiceException{
		List<AllocationCorporativeDetailTO> lstAllocateCorporative = new ArrayList<AllocationCorporativeDetailTO>();
		AllocationCorporativeDetailTO allocateCorporative = null;
			List<CorporativeOperation> corporativeList = allocationBenefitService.getCorporativesByIssuer(allocateProcessIssuer,BooleanType.NO.getCode(),paymentDate,paymentCurrency);
			if(Validations.validateListIsNotNullAndNotEmpty(corporativeList)){
				Map<Integer,String> corporativeStateDescription = new HashMap<>();
				ParameterTableTO filter = new ParameterTableTO();
				filter.setMasterTableFk(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode());
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)){
					corporativeStateDescription.put(param.getParameterTablePk(), param.getParameterName());
				}
				for(CorporativeOperation corporative : corporativeList){
					allocateCorporative = new AllocationCorporativeDetailTO();
					allocateCorporative.setBenefitAmount(corporative.getPaymentAmount());
					allocateCorporative.setCorporativeState(corporativeStateDescription.get(corporative.getState()));
					allocateCorporative.setCorporativeType(corporative.getCorporativeEventType().getDescription());
					allocateCorporative.setCustodyAmount(corporative.getCustodyAmount());
					allocateCorporative.setIdCorporativeProcess(corporative.getIdCorporativeOperationPk().toString());
					allocateCorporative.setSecurity(corporative.getSecurities().getIdSecurityCodePk());
					allocateCorporative.setTaxAmount(corporative.getTaxAmount());

					lstAllocateCorporative.add(allocateCorporative);
				}
			}
		return lstAllocateCorporative;
	}

	/**
	 * Metodo que obtiene lista de detalle de la consignacion de pagos.
	 *
	 * @param allocateProcessIssuer the allocate process issuer
	 * @param paymentDate the payment date
	 * @return the allocated corporatives
	 * @throws ServiceException the service exception
	 */
	public List<AllocationCorporativeDetailTO> getAllocatedCorporatives(AllocationProcessIssuerResumeTO allocateProcessIssuer,Date paymentDate,Integer paymentCurrency) throws ServiceException{
		List<AllocationCorporativeDetailTO> lstAllocatedCorporative = new ArrayList<AllocationCorporativeDetailTO>();
		AllocationCorporativeDetailTO allocatedCorporative = null;
			List<CorporativeOperation> corporativeList = allocationBenefitService.getCorporativesByIssuer(allocateProcessIssuer,BooleanType.YES.getCode(),paymentDate,paymentCurrency);
			if(Validations.validateListIsNotNullAndNotEmpty(corporativeList)){
				Map<Integer,String> corporativeStateDescription = new HashMap<>();
				ParameterTableTO filter = new ParameterTableTO();
				filter.setMasterTableFk(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode());
				for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)){
					corporativeStateDescription.put(param.getParameterTablePk(), param.getParameterName());
				}
				for(CorporativeOperation corporative : corporativeList){
					allocatedCorporative = new AllocationCorporativeDetailTO();
					allocatedCorporative.setBenefitAmount(corporative.getPaymentAmount());
					allocatedCorporative.setCorporateState(corporative.getState());
					allocatedCorporative.setCorporativeState(corporativeStateDescription.get(corporative.getState()));
					allocatedCorporative.setCorporativeType(corporative.getCorporativeEventType().getDescription());
					allocatedCorporative.setCustodyAmount(corporative.getCustodyAmount());
					allocatedCorporative.setIdCorporativeProcess(corporative.getIdCorporativeOperationPk().toString());
					allocatedCorporative.setSecurity(corporative.getSecurities().getIdSecurityCodePk());
					allocatedCorporative.setTaxAmount(corporative.getTaxAmount());

					lstAllocatedCorporative.add(allocatedCorporative);
				}
			}
		return lstAllocatedCorporative;
	}

	/**
	 * Metodo que obtiene lista de consignacion de pagos.
	 *
	 * @param allocationPaymentSearch the allocation payment search
	 * @param allocationProcessIssuerResume the allocation process issuer resume
	 * @return the list
	 * @throws ServiceException the service exception
	 */

	public List<InstitutionCashAccount> getInstCashAccounts(CashAccountsSearchTO parameters) throws ServiceException{
		return cashAccountService.getCashAccounts(parameters);
	}
	
	public List<CorporativeProcessResult> getCorporativeResulByIssuer(AllocationProcessIssuerResumeTO allocateProcessIssuer,Integer indPayed, Date paymentDate, Integer paymentCurrency){
		return allocationBenefitService.getCorporativeResulByIssuer(allocateProcessIssuer, indPayed, paymentDate, paymentCurrency);
	}
	
	public List<AllocationProcessRegisterTO> fillDataRegisterAllocationProcess(AllocationPaymentSearchTO allocationPaymentSearch, 
				AllocationProcessIssuerResumeTO[] allocationProcessIssuerResume) throws ServiceException{
		List<AllocationProcessRegisterTO> lstRegisterProcess = new ArrayList<AllocationProcessRegisterTO>();
		List<AllocationCorporativeDetailTO> lstIssuerCorporatives = new ArrayList<AllocationCorporativeDetailTO>();
		List<AllocateIssuerCashAccountTO> lstIssuerCashAccount = new ArrayList<AllocateIssuerCashAccountTO>();
		AllocationProcessRegisterTO registerProcess = new AllocationProcessRegisterTO();
		Map<Integer,String> corporativeStateDescription = new HashMap<>();
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)){
			corporativeStateDescription.put(param.getParameterTablePk(), param.getParameterName());
		}
			for(AllocationProcessIssuerResumeTO processIssuerResume : allocationProcessIssuerResume){
				AllocateIssuerCashAccountTO issuerCashAccount = new AllocateIssuerCashAccountTO();
				AllocationCorporativeDetailTO corporativeDetail = new AllocationCorporativeDetailTO();
				lstIssuerCorporatives = new ArrayList<AllocationCorporativeDetailTO>();
				lstIssuerCashAccount = new ArrayList<AllocateIssuerCashAccountTO>();
				CashAccountsSearchTO parameter = new CashAccountsSearchTO();
				parameter.setCashAccountType(AccountCashFundsType.BENEFIT.getCode());
				parameter.setState(CashAccountStateType.ACTIVATE.getCode());
				parameter.setIssuer(processIssuerResume.getIssuer());
				parameter.setCurrency(allocationPaymentSearch.getCurrency());
				List<InstitutionCashAccount> lst = cashAccountService.getCashAccounts(parameter);
				if(Validations.validateListIsNotNullAndNotEmpty(lst)){
					InstitutionCashAccount cashAccount = lst.get(0);
					issuerCashAccount.setIdCashAccount(cashAccount.getIdInstitutionCashAccountPk().toString());
					issuerCashAccount.setIssuer(cashAccount.getIssuer().getIdIssuerPk());
					issuerCashAccount.setTotalAmount(cashAccount.getTotalAmount());
					issuerCashAccount.setAvailableAmount(cashAccount.getAvailableAmount());
					issuerCashAccount.setAllocateAmount(BigDecimal.ZERO);
				}else{
					issuerCashAccount.setIdCashAccount("0");
					issuerCashAccount.setIssuer(processIssuerResume.getIssuer());
					issuerCashAccount.setTotalAmount(BigDecimal.ZERO);
					issuerCashAccount.setAvailableAmount(BigDecimal.ZERO);
					issuerCashAccount.setAllocateAmount(BigDecimal.ZERO);
				}
				lstIssuerCashAccount.add(issuerCashAccount);
				AllocationProcessIssuerResumeTO issuerResume = new AllocationProcessIssuerResumeTO();
				issuerResume.setIssuer(processIssuerResume.getIssuer());
				List<CorporativeOperation> corporativeList = allocationBenefitService.getCorporativesByIssuer(issuerResume,BooleanType.NO.getCode(),
																allocationPaymentSearch.getPaymentDate(),allocationPaymentSearch.getCurrency());
				if(Validations.validateListIsNotNullAndNotEmpty(corporativeList)){
					for(CorporativeOperation corporative : corporativeList){
						corporativeDetail = new AllocationCorporativeDetailTO();
						corporativeDetail.setIdCorporativeProcess(corporative.getIdCorporativeOperationPk().toString());
						corporativeDetail.setCorporativeType(corporative.getCorporativeEventType().getDescription());
						corporativeDetail.setBenefitAmount(corporative.getPaymentAmount());
						corporativeDetail.setSecurity(corporative.getSecurities().getIdSecurityCodePk());
						corporativeDetail.setCustodyAmount(corporative.getCustodyAmount());
						corporativeDetail.setTaxAmount(corporative.getTaxAmount());
						Integer corporativeState = corporative.getState();
						corporativeDetail.setCorporateState(corporativeState);
						corporativeDetail.setCorporativeState(corporativeStateDescription.get(corporativeState));
						corporativeDetail.setIssuer(corporative.getIssuer().getIdIssuerPk());
						corporativeDetail.setCommissionAmount(corporative.getCommissionAmount()!=null?corporative.getCommissionAmount():new BigDecimal("0"));

						lstIssuerCorporatives.add(corporativeDetail);
					}
				}
				registerProcess.setIssuerDescription(processIssuerResume.getIssuerMnemonic()+" - "+ processIssuerResume.getIssuer() + " - " + processIssuerResume.getIssuerDescription());
				registerProcess.setLstIssuerCashAccount(lstIssuerCashAccount);
				registerProcess.setLstIssuerAllocations(lstIssuerCorporatives);
				lstRegisterProcess.add(registerProcess);

				registerProcess = new AllocationProcessRegisterTO();
				lstIssuerCashAccount = new ArrayList<AllocateIssuerCashAccountTO>();
				lstIssuerCorporatives = new ArrayList<AllocationCorporativeDetailTO>();
			}
		
		return lstRegisterProcess;
	}

	/**
	 * Metodo que registra consignacion de pago.
	 *
	 * @param issuer the issuer
	 * @param security the security
	 * @param currency the currency
	 * @param corporatives the corporatives
	 * @param commissionExcluded the commission excluded
	 * @return the benefit payment allocation
	 */
	private BenefitPaymentAllocation saveAllocationProcess(String issuer, String security, Integer currency, List<Long> corporatives, boolean commissionExcluded){
		BenefitPaymentAllocation benefitPaymentAllocation = new BenefitPaymentAllocation();
		
		benefitPaymentAllocation.setPaymentType(idepositarySetup.getPaymentType());
		benefitPaymentAllocation.setAllocationProcessDate(CommonsUtilities.currentDateTime());
		benefitPaymentAllocation.setAllocationQuantityProcess(BigDecimal.valueOf(corporatives.size()));
		benefitPaymentAllocation.setAllocationProcessState(AllocationProcessStateType.PRELIMINAR.getCode());
		benefitPaymentAllocation.setCurrency(currency);
		if(commissionExcluded){
			benefitPaymentAllocation.setIndExcludedCommissions(BooleanType.YES.getCode());
		}else{
			benefitPaymentAllocation.setIndExcludedCommissions(BooleanType.NO.getCode());
		}

		Issuer objIssuer = null;
		if(issuer != null) {
			objIssuer = allocationBenefitService.find(issuer,Issuer.class);
		}else {
			if(corporatives!=null && corporatives.size()>0) {
				objIssuer = allocationBenefitService.getIssuerByCorporative(corporatives.get(0));
			}
		}
		benefitPaymentAllocation.setIssuer(objIssuer);
		
		
		allocationBenefitService.create(benefitPaymentAllocation);

		return benefitPaymentAllocation;
	}

	/**
	 * Metodo que registra detalle de consignacion de pago.
	 *
	 * @param lstCorporatives the lst corporatives
	 * @param lstRetentionHolderAccount the lst retention holder account
	 * @param benefitPaymentAllocation the benefit payment allocation
	 */
	private void generateDetailByHolderAccountDetail(List<Long> lstCorporatives, List<Long> lstRetentionHolderAccount, BenefitPaymentAllocation benefitPaymentAllocation){

		List<Object[]> lstCorporativeDetail = allocationBenefitService.getDetailByCorporativeHolderAccount(lstCorporatives,lstRetentionHolderAccount);
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeDetail)){
			for(Object[] corporativeResult : lstCorporativeDetail){
				
				Long holderAccount = Long.valueOf(corporativeResult[0].toString());
				Long corporativeID = Long.valueOf(corporativeResult[1].toString());
				BigDecimal grossAmount = new BigDecimal(corporativeResult[2].toString());
				BigDecimal taxAmount = new BigDecimal(corporativeResult[3].toString());
				BigDecimal custodyAmount = new BigDecimal(corporativeResult[4].toString());				
								
				List<Long> lstHolder = allocationBenefitService.getHolderByAccountParticipant(holderAccount);
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolder)){				
					BigDecimal acumTransactionAmount = BigDecimal.ZERO;
					BigDecimal acumTaxAmount = BigDecimal.ZERO;
					BigDecimal acumCustodyAmount = BigDecimal.ZERO;
					for(int i=0;i<lstHolder.size();i++){						
						PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
						paymentAllocationDetail.setBank(null);
						paymentAllocationDetail.setBenefitPaymentAllocation(benefitPaymentAllocation);
						CorporativeOperation corporativeOperation = new CorporativeOperation();
						corporativeOperation.setIdCorporativeOperationPk(corporativeID);
						paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
						paymentAllocationDetail.setCurrency(benefitPaymentAllocation.getCurrency());
						HolderAccount objHolderAccount = new HolderAccount();
						objHolderAccount.setIdHolderAccountPk(holderAccount);
						paymentAllocationDetail.setHolderAccount(objHolderAccount);				
						paymentAllocationDetail.setHolderAccountBank(null);
						paymentAllocationDetail.setProcessType(AllocationProcessType.HOLDER_ACCOUNT_RETENTION.getCode());
						Long idHolder= (Long)lstHolder.get(i);				
						Holder objHolder = new Holder();
						objHolder.setIdHolderPk(idHolder);
						paymentAllocationDetail.setHolder(objHolder);
						
						if(i==(lstHolder.size()-1)){
							paymentAllocationDetail.setTransactionAmount(grossAmount.subtract(acumTransactionAmount));
							paymentAllocationDetail.setTaxAmount(taxAmount.subtract(acumTaxAmount));
							paymentAllocationDetail.setCustodyAmount(custodyAmount.subtract(acumCustodyAmount));
						}else{							
							paymentAllocationDetail.setTransactionAmount(grossAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumTransactionAmount=acumTransactionAmount.add(paymentAllocationDetail.getTransactionAmount());
							paymentAllocationDetail.setTaxAmount(taxAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumTaxAmount=acumTaxAmount.add(paymentAllocationDetail.getTaxAmount());
							paymentAllocationDetail.setCustodyAmount(custodyAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumCustodyAmount=acumCustodyAmount.add(paymentAllocationDetail.getCustodyAmount());
						}						
						
						allocationBenefitService.create(paymentAllocationDetail);
					}
				}
			}
		}
	}

	/**
	 * Metodo que registra detalle de consignacion de pago.
	 *
	 * @param lstCorporatives the lst corporatives
	 * @param lstRetentionHolder the lst retention holder
	 * @param benefitPaymentAllocation the benefit payment allocation
	 */
	private void generateDetailByHolderDetail(List<Long> lstCorporatives,List<Long> lstRetentionHolder, BenefitPaymentAllocation benefitPaymentAllocation){
		List<Object[]> lstCorporativeDetail = allocationBenefitService.getDetailByCorporativeHolder(lstCorporatives,lstRetentionHolder);
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeDetail)){
			Map mpHolderAccount = new HashMap();
			for(Object[] corporativeResult : lstCorporativeDetail){
				Long holderAccount = Long.valueOf(corporativeResult[0].toString());
				if(!mpHolderAccount.containsKey(holderAccount)){
					
					mpHolderAccount.put(holderAccount,holderAccount);					
					
					Long corporativeID = Long.valueOf(corporativeResult[1].toString());
					BigDecimal grossAmount = new BigDecimal(corporativeResult[2].toString());
					BigDecimal taxAmount = new BigDecimal(corporativeResult[3].toString());
					BigDecimal custodyAmount = new BigDecimal(corporativeResult[4].toString());			
					
					List<Long> lstHolder = allocationBenefitService.getHolderByAccountParticipant(holderAccount);
					if(Validations.validateListIsNotNullAndNotEmpty(lstHolder)){						
						BigDecimal acumTransactionAmount = BigDecimal.ZERO;
						BigDecimal acumTaxAmount = BigDecimal.ZERO;
						BigDecimal acumCustodyAmount = BigDecimal.ZERO;
						for(int i=0;i<lstHolder.size();i++){
							PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
							paymentAllocationDetail.setBank(null);
							paymentAllocationDetail.setBenefitPaymentAllocation(benefitPaymentAllocation);
							CorporativeOperation corporativeOperation = new CorporativeOperation();
							corporativeOperation.setIdCorporativeOperationPk(corporativeID);
							paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
							paymentAllocationDetail.setCurrency(benefitPaymentAllocation.getCurrency());						
							HolderAccount objHolderAccount = new HolderAccount();
							objHolderAccount.setIdHolderAccountPk(holderAccount);
							paymentAllocationDetail.setHolderAccount(objHolderAccount);
							paymentAllocationDetail.setHolderAccountBank(null);
							paymentAllocationDetail.setProcessType(AllocationProcessType.RNT_RETENTION.getCode());
							
							Long idHolder= (Long)lstHolder.get(i);				
							Holder objHolder = new Holder();
							objHolder.setIdHolderPk(idHolder);
							paymentAllocationDetail.setHolder(objHolder);
							
							if(i==(lstHolder.size()-1)){
								paymentAllocationDetail.setTransactionAmount(grossAmount.subtract(acumTransactionAmount));
								paymentAllocationDetail.setTaxAmount(taxAmount.subtract(acumTaxAmount));
								paymentAllocationDetail.setCustodyAmount(custodyAmount.subtract(acumCustodyAmount));
							}else{							
								paymentAllocationDetail.setTransactionAmount(grossAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
								acumTransactionAmount=acumTransactionAmount.add(paymentAllocationDetail.getTransactionAmount());
								paymentAllocationDetail.setTaxAmount(taxAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
								acumTaxAmount=acumTaxAmount.add(paymentAllocationDetail.getTaxAmount());
								paymentAllocationDetail.setCustodyAmount(custodyAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
								acumCustodyAmount=acumCustodyAmount.add(paymentAllocationDetail.getCustodyAmount());
							}						
							
							allocationBenefitService.create(paymentAllocationDetail);	
						}			
					}
				}
			}
		}
	}

	/**
	 * Metodo que registra detalle de consignacion de pago.
	 *
	 * @param lstCorporatives the lst corporatives
	 * @param lstHolderAccountNoBankAccount the lst holder account no bank account
	 * @param benefitPaymentAllocation the benefit payment allocation
	 */
	private void generateDetailByNoBankAccount(List<Long> lstCorporatives,List<Long> lstHolderAccountNoBankAccount,BenefitPaymentAllocation benefitPaymentAllocation){
		List<Object[]> lstCorporativeDetail = allocationBenefitService.getDetailByCorporativeBankAccount(lstCorporatives,lstHolderAccountNoBankAccount);
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeDetail)){
			for(Object[] corporativeResult : lstCorporativeDetail){
				

				Long holderAccount = Long.valueOf(corporativeResult[0].toString());
				Long corporativeID = Long.valueOf(corporativeResult[1].toString());
				BigDecimal grossAmount = new BigDecimal(corporativeResult[2].toString());
				BigDecimal taxAmount = new BigDecimal(corporativeResult[3].toString());
				BigDecimal custodyAmount = new BigDecimal(corporativeResult[4].toString());
					
				PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
				paymentAllocationDetail.setBank(null);
				paymentAllocationDetail.setBenefitPaymentAllocation(benefitPaymentAllocation);
				CorporativeOperation corporativeOperation = new CorporativeOperation();
				corporativeOperation.setIdCorporativeOperationPk(corporativeID);
				paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
				paymentAllocationDetail.setCurrency(benefitPaymentAllocation.getCurrency());
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(holderAccount);
				paymentAllocationDetail.setHolderAccount(objHolderAccount);
				paymentAllocationDetail.setProcessType(AllocationProcessType.NO_BANK_ACCOUNT_RETENTION.getCode());
				
				paymentAllocationDetail.setTransactionAmount(grossAmount);
				paymentAllocationDetail.setTaxAmount(taxAmount);
				paymentAllocationDetail.setCustodyAmount(custodyAmount);

				allocationBenefitService.create(paymentAllocationDetail);
				/*
				
				List<Long> lstHolder = allocationBenefitService.getHolderByAccountParticipant(holderAccount);
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolder)){					
					BigDecimal acumTransactionAmount = BigDecimal.ZERO;
					BigDecimal acumTaxAmount = BigDecimal.ZERO;
					BigDecimal acumCustodyAmount = BigDecimal.ZERO;
					for(int i=0;i<lstHolder.size();i++){
						PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
						paymentAllocationDetail.setBank(null);
						paymentAllocationDetail.setBenefitPaymentAllocation(benefitPaymentAllocation);
						CorporativeOperation corporativeOperation = new CorporativeOperation();
						corporativeOperation.setIdCorporativeOperationPk(corporativeID);
						paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
						paymentAllocationDetail.setCurrency(benefitPaymentAllocation.getCurrency());
						HolderAccount objHolderAccount = new HolderAccount();
						objHolderAccount.setIdHolderAccountPk(holderAccount);
						paymentAllocationDetail.setHolderAccount(objHolderAccount);
						paymentAllocationDetail.setProcessType(AllocationProcessType.NO_BANK_ACCOUNT_RETENTION.getCode());
						
						Long idHolder= (Long)lstHolder.get(i);				
						Holder objHolder = new Holder();
						objHolder.setIdHolderPk(idHolder);
						paymentAllocationDetail.setHolder(objHolder);
						
						if(i==(lstHolder.size()-1)){
							paymentAllocationDetail.setTransactionAmount(grossAmount.subtract(acumTransactionAmount));
							paymentAllocationDetail.setTaxAmount(taxAmount.subtract(acumTaxAmount));
							paymentAllocationDetail.setCustodyAmount(custodyAmount.subtract(acumCustodyAmount));
						}else{							
							paymentAllocationDetail.setTransactionAmount(grossAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumTransactionAmount=acumTransactionAmount.add(paymentAllocationDetail.getTransactionAmount());
							paymentAllocationDetail.setTaxAmount(taxAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumTaxAmount=acumTaxAmount.add(paymentAllocationDetail.getTaxAmount());
							paymentAllocationDetail.setCustodyAmount(custodyAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumCustodyAmount=acumCustodyAmount.add(paymentAllocationDetail.getCustodyAmount());
						}						
						
						allocationBenefitService.create(paymentAllocationDetail);
					}					
				}
					
				*/	
			}
		}
	}

	/**
	 * Metodo que registra detalle de consignaciones de pago bloqueadas
	 *
	 * @param lstBlocks the lst blocks
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param mpBlockAmount the mp block amount
	 * @param currency the currency
	 * @param blBlock the bl block
	 * @throws ServiceException the service exception
	 */
	
	private BigDecimal blockAmmountWithCorporativeProcessResult(CorporativeProcessResult corporativeProcessResult) {
		BigDecimal blockAmount = BigDecimal.ZERO;
		blockAmount = blockAmount.add(corporativeProcessResult.getPawnBalance());
		blockAmount = blockAmount.add(corporativeProcessResult.getBanBalance());
		blockAmount = blockAmount.add(corporativeProcessResult.getOtherBlockBalance());
		
		return blockAmount;
	}
	
	private void generatePaymentAllocationDetail(Boolean hasRetention, List<CorporativeProcessResult> lstCorporativeProcessResult,BenefitPaymentAllocation benefitPaymentAllocation, Integer currency, Integer paymentType) throws ServiceException{
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeProcessResult)){
			for(CorporativeProcessResult corporativeResult : lstCorporativeProcessResult){
				
				Long idHolderAccountPk = corporativeResult.getHolderAccount().getIdHolderAccountPk();
				Long idCorporativeOperationPk = corporativeResult.getCorporativeOperation().getIdCorporativeOperationPk();
				
				BigDecimal totalBlockAmount= (hasRetention)?blockAmmountWithCorporativeProcessResult(corporativeResult):null;
	            BigDecimal grossAmount = (!hasRetention)?corporativeResult.getAvailableBalance():null;
				BigDecimal taxAmount = corporativeResult.getTaxAmount();
				BigDecimal custodyAmount = corporativeResult.getCustodyAmount();
				Participant participant = allocationBenefitService.getParticipantByHolderAccount(idHolderAccountPk);
				participant.getIdParticipantPk();
				
				BenefitPaymentAllocation bpa = allocationBenefitService.find(benefitPaymentAllocation.getIdBenefitPaymentPK(), BenefitPaymentAllocation.class);
				//bpa.setIdBenefitPaymentPK(benefitPaymentAllocation.getIdBenefitPaymentPK());

				CorporativeOperation corporativeOperation = new CorporativeOperation();
				corporativeOperation.setIdCorporativeOperationPk(idCorporativeOperationPk);
				
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(idHolderAccountPk);
				
				Security objSecurity = allocationBenefitService.getSecurityByCorporative(idCorporativeOperationPk);

				PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
				paymentAllocationDetail.setBenefitPaymentAllocation(bpa);
				paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
				paymentAllocationDetail.setCurrency(benefitPaymentAllocation.getCurrency());
				paymentAllocationDetail.setHolderAccount(objHolderAccount);
				paymentAllocationDetail.setParticipant(participant);
				paymentAllocationDetail.setTransactionAmount((hasRetention)?totalBlockAmount:grossAmount);
				paymentAllocationDetail.setTaxAmount(taxAmount);
				paymentAllocationDetail.setCustodyAmount(custodyAmount);
				paymentAllocationDetail.setSecurity(objSecurity);
				
				if(hasRetention) {
					paymentAllocationDetail.setProcessType(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode());
				}else {
					if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
		                paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
		            }else {
		                paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
		            }
				}

				if(paymentType.equals(BenefitPaymentType.PDI.getCode())){
					
					Object[] holderAccountBank = allocationBenefitService.getHolderAccountBank(idHolderAccountPk,currency);
					if(holderAccountBank!=null && holderAccountBank.length>0){
						
						Long holderAccountBankPK = Long.valueOf(holderAccountBank[0].toString());
						Long bankPK = Long.valueOf(holderAccountBank[1].toString());
						String accountNumber = holderAccountBank[2].toString();
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						HolderAccountBank objHolderAccountBank = new HolderAccountBank();
						objHolderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
						
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setHolderAccountBank(objHolderAccountBank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
					}
					
				}else if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
					
					Object[] ParticipantBank = allocationBenefitService.getParticipantBank(participant.getIdParticipantPk(),currency,AccountCashFundsType.BENEFIT.getCode());
					if(ParticipantBank!=null && ParticipantBank.length>0){
						
						Long institutionCashAccountPK = (ParticipantBank[0]!=null)? Long.valueOf(ParticipantBank[0].toString()) : null;
						Long bankPK = (ParticipantBank[1]!=null)? Long.valueOf(ParticipantBank[1].toString()) : null;
						String accountNumber = (ParticipantBank[2]!=null)? ParticipantBank[2].toString() : "";
						Long institutionBankAccountPK = (ParticipantBank[3]!=null)? Long.valueOf(ParticipantBank[3].toString()) : null;

						Bank bank = null;
						if(institutionCashAccountPK!=null) {
							bank = new Bank();
							bank.setIdBankPk(bankPK);
						}
						
						InstitutionCashAccount objInstitutionCashAccount = null;
						if(institutionCashAccountPK!=null) {
							objInstitutionCashAccount = new InstitutionCashAccount();
							objInstitutionCashAccount.setIdInstitutionCashAccountPk(institutionCashAccountPK);
						}

						InstitutionBankAccount objInstitutionBankAccount = null;
						if(institutionBankAccountPK!=null) {
							objInstitutionBankAccount = new InstitutionBankAccount();
							objInstitutionBankAccount.setIdInstitutionBankAccountPk(institutionBankAccountPK);
						}
						
						paymentAllocationDetail.setInstitutionCashAccount(objInstitutionCashAccount);
						paymentAllocationDetail.setInstitutionBankAccount(objInstitutionBankAccount);
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
					}
				}
				
				allocationBenefitService.create(paymentAllocationDetail);
				
				//WE CREATE THE DETAIL FOR BLOCKS WHICH WILL GENERATE RETENTION AMOUNTS
				//Long idProcessCorporateResult = corporativeResult.getIdCorporativeProcessResult();
				//if(hasRetention){
				//	allocationBenefitService.createBlockBenefitDetail(idCorporativeOperationPk, idProcessCorporateResult, paymentAllocationDetail);
				//}
			}
		}
	}
	
	
	
	private void generateDetailBlockRetention(List<CorporativeProcessResult> lstCorporativeProcessResultBlock,BenefitPaymentAllocation benefitPaymentAllocation, Integer currency, Integer paymentType) throws ServiceException{
		//List<CorporativeProcessResult> lstCorporativeDetail = allocationBenefitService.getCorporativeResultList(lstBlocks);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeProcessResultBlock)){
			for(CorporativeProcessResult corporativeResult : lstCorporativeProcessResultBlock){
				
				Long idHolderAccountPk = corporativeResult.getHolderAccount().getIdHolderAccountPk();
				Long idCorporativeOperationPk = corporativeResult.getCorporativeOperation().getIdCorporativeOperationPk();
				Long idProcessCorporateResult = corporativeResult.getIdCorporativeProcessResult();
				BigDecimal totalBlockAmount= blockAmmountWithCorporativeProcessResult(corporativeResult);
				BigDecimal taxAmount = corporativeResult.getTaxAmount();
				BigDecimal custodyAmount = corporativeResult.getCustodyAmount();
				Participant participant = allocationBenefitService.getParticipantByHolderAccount(idHolderAccountPk);
				participant.getIdParticipantPk();
					
				PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
				paymentAllocationDetail.setBank(null);
				
				
				BenefitPaymentAllocation bpa = new BenefitPaymentAllocation();
				bpa.setIdBenefitPaymentPK(benefitPaymentAllocation.getIdBenefitPaymentPK());
				
				paymentAllocationDetail.setBenefitPaymentAllocation(bpa);
				CorporativeOperation corporativeOperation = new CorporativeOperation();
				corporativeOperation.setIdCorporativeOperationPk(idCorporativeOperationPk);
				paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
				paymentAllocationDetail.setCurrency(benefitPaymentAllocation.getCurrency());
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(idHolderAccountPk);
				paymentAllocationDetail.setHolderAccount(objHolderAccount);
				paymentAllocationDetail.setProcessType(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode());
				
				paymentAllocationDetail.setParticipant(participant);
				paymentAllocationDetail.setTransactionAmount(totalBlockAmount);
				paymentAllocationDetail.setTaxAmount(taxAmount);
				paymentAllocationDetail.setCustodyAmount(custodyAmount);

				if(paymentType.equals(BenefitPaymentType.PDI.getCode())){
					
					Object[] holderAccountBank = allocationBenefitService.getHolderAccountBank(idHolderAccountPk,currency);
					if(holderAccountBank!=null && holderAccountBank.length>0){
						
						Long holderAccountBankPK = Long.valueOf(holderAccountBank[0].toString());
						Long bankPK = Long.valueOf(holderAccountBank[1].toString());
						String accountNumber = holderAccountBank[2].toString();
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						HolderAccountBank objHolderAccountBank = new HolderAccountBank();
						objHolderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
						
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setHolderAccountBank(objHolderAccountBank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
					}
					
				}else if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
					
					Object[] ParticipantBank = allocationBenefitService.getParticipantBank(participant.getIdParticipantPk(),currency,AccountCashFundsType.BENEFIT.getCode());
					if(ParticipantBank!=null && ParticipantBank.length>0){
						
						Long institutionCashAccountPK = (ParticipantBank[0]!=null)? Long.valueOf(ParticipantBank[0].toString()) : null;
						Long bankPK = (ParticipantBank[1]!=null)? Long.valueOf(ParticipantBank[1].toString()) : null;
						String accountNumber = (ParticipantBank[2]!=null)? ParticipantBank[2].toString() : "";
						Long institutionBankAccountPK = (ParticipantBank[3]!=null)? Long.valueOf(ParticipantBank[3].toString()) : null;

						Bank bank = null;
						if(institutionCashAccountPK!=null) {
							bank = new Bank();
							bank.setIdBankPk(bankPK);
						}
						
						InstitutionCashAccount objInstitutionCashAccount = null;
						if(institutionCashAccountPK!=null) {
							objInstitutionCashAccount = new InstitutionCashAccount();
							objInstitutionCashAccount.setIdInstitutionCashAccountPk(institutionCashAccountPK);
						}

						InstitutionBankAccount objInstitutionBankAccount = null;
						if(institutionBankAccountPK!=null) {
							objInstitutionBankAccount = new InstitutionBankAccount();
							objInstitutionBankAccount.setIdInstitutionBankAccountPk(institutionBankAccountPK);
						}
						
						paymentAllocationDetail.setInstitutionCashAccount(objInstitutionCashAccount);
						paymentAllocationDetail.setInstitutionBankAccount(objInstitutionBankAccount);
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
					}
				}
				
				Security objSecurity = allocationBenefitService.getSecurityByCorporative(idCorporativeOperationPk);
				paymentAllocationDetail.setSecurity(objSecurity);
				
				allocationBenefitService.create(paymentAllocationDetail);
				
				//WE CREATE THE DETAIL FOR BLOCKS WHICH WILL GENERATE RETENTION AMOUNTS
				//allocationBenefitService.createBlockBenefitDetail(idCorporativeOperationPk, idProcessCorporateResult, paymentAllocationDetail);
			}
		}
	}
	
	private void generateDetailBlockRetention(List<Long> lstBlocks,BenefitPaymentAllocation benefitPaymentAllocation, 
			Map<Long, BigDecimal> mpBlockAmount, Integer currency, boolean blBlock, Integer paymentType) throws ServiceException{
		List<Object[]> lstCorporativeDetail = allocationBenefitService.getCorporativeResultList(lstBlocks);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeDetail)){
			for(Object[] corporativeResult : lstCorporativeDetail){
				
				Long holderAccount = Long.valueOf(corporativeResult[0].toString());
				Long corporativeID = Long.valueOf(corporativeResult[1].toString());
				Long idProcessCorporateResult = new Long(corporativeResult[3].toString());
				BigDecimal totalBlockAmount= mpBlockAmount.get(idProcessCorporateResult);
				BigDecimal taxAmount = new BigDecimal(corporativeResult[8].toString());
				BigDecimal custodyAmount = new BigDecimal(corporativeResult[9].toString());
				Participant participant = allocationBenefitService.getParticipantByHolderAccount(holderAccount);
					
				PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
				paymentAllocationDetail.setBank(null);
				paymentAllocationDetail.setBenefitPaymentAllocation(benefitPaymentAllocation);
				CorporativeOperation corporativeOperation = new CorporativeOperation();
				corporativeOperation.setIdCorporativeOperationPk(corporativeID);
				paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
				paymentAllocationDetail.setCurrency(benefitPaymentAllocation.getCurrency());
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(holderAccount);
				paymentAllocationDetail.setHolderAccount(objHolderAccount);
				paymentAllocationDetail.setProcessType(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode());
				
				paymentAllocationDetail.setTransactionAmount(totalBlockAmount);
				paymentAllocationDetail.setTaxAmount(taxAmount);
				paymentAllocationDetail.setCustodyAmount(custodyAmount);

				if(paymentType.equals(BenefitPaymentType.PDI.getCode())){
					
					Object[] holderAccountBank = allocationBenefitService.getHolderAccountBank(holderAccount,currency);
					if(holderAccountBank!=null && holderAccountBank.length>0){
						
						Long holderAccountBankPK = Long.valueOf(holderAccountBank[0].toString());
						Long bankPK = Long.valueOf(holderAccountBank[1].toString());
						String accountNumber = holderAccountBank[2].toString();
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						HolderAccountBank objHolderAccountBank = new HolderAccountBank();
						objHolderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
						
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setHolderAccountBank(objHolderAccountBank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
					}
					
				}else if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
					
					Object[] ParticipantBank = allocationBenefitService.getParticipantBank(participant.getIdParticipantPk(),currency,AccountCashFundsType.BENEFIT.getCode());
					if(ParticipantBank!=null && ParticipantBank.length>0){
						
						Long institutionCashAccountPK = Long.valueOf(ParticipantBank[0].toString());
						Long bankPK = Long.valueOf(ParticipantBank[1].toString());
						String accountNumber = ParticipantBank[2].toString();
						Long institutionBankAccountPK = Long.valueOf(ParticipantBank[3].toString());
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						InstitutionCashAccount objInstitutionCashAccount = new InstitutionCashAccount();
						objInstitutionCashAccount.setIdInstitutionCashAccountPk(institutionCashAccountPK);
						
						InstitutionBankAccount objInstitutionBankAccount = new InstitutionBankAccount();
						objInstitutionBankAccount.setIdInstitutionBankAccountPk(institutionBankAccountPK);

						paymentAllocationDetail.setInstitutionCashAccount(objInstitutionCashAccount);
						paymentAllocationDetail.setInstitutionBankAccount(objInstitutionBankAccount);
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
					}
				}
				
				Security objSecurity = allocationBenefitService.getSecurityByCorporative(corporativeID);
				paymentAllocationDetail.setSecurity(objSecurity);
				
				allocationBenefitService.create(paymentAllocationDetail);
				
				//WE CREATE THE DETAIL FOR BLOCKS WHICH WILL GENERATE RETENTION AMOUNTS
				List<Object[]> lstBlockOperations = allocationBenefitService.getBlockDetail(corporativeID,idProcessCorporateResult);
				if(Validations.validateListIsNotNullAndNotEmpty(lstBlockOperations)){
					for(Object[] objBlockOperation : lstBlockOperations){
						Long blockOperationPK = Long.valueOf(objBlockOperation[0].toString());
						BigDecimal grossBlockAmount = new BigDecimal(objBlockOperation[1].toString());
						BigDecimal taxBlockAmount = new BigDecimal(objBlockOperation[2].toString());
						BigDecimal custodyBlockAmount = new BigDecimal(objBlockOperation[3].toString());
						
						BlockBenefitDetail blockBenefitDetail = new BlockBenefitDetail();
						blockBenefitDetail.setPaymentAllocationDetail(paymentAllocationDetail);
						BlockOperation blockOperation = new BlockOperation();
						blockOperation.setIdBlockOperationPk(blockOperationPK);
						blockBenefitDetail.setBlockOperation(blockOperation);
						
						blockBenefitDetail.setGrossBlockAmount(grossBlockAmount);
						blockBenefitDetail.setTaxAmount(taxBlockAmount);
						blockBenefitDetail.setCustodyAmount(custodyBlockAmount);
						
						allocationBenefitService.create(blockBenefitDetail);
					}
				}
				
				
				/*
				Long holderAccount = Long.valueOf(corporativeResult[0].toString());
				Long corporativeID = Long.valueOf(corporativeResult[1].toString());
				Long idProcessCorporateResult = new Long(corporativeResult[3].toString());
				BigDecimal totalBlockAmount= mpBlockAmount.get(idProcessCorporateResult);
				BigDecimal taxAmount = new BigDecimal(corporativeResult[8].toString());
				BigDecimal custodyAmount = new BigDecimal(corporativeResult[9].toString());
								
				List<Long> lstHolder = allocationBenefitService.getHolderByAccountParticipant(holderAccount);
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolder)){					
					BigDecimal acumTransactionAmount = BigDecimal.ZERO;
					BigDecimal acumTaxAmount = BigDecimal.ZERO;
					BigDecimal acumCustodyAmount = BigDecimal.ZERO;
					BigDecimal acumGrossBlockAmount = BigDecimal.ZERO;
					BigDecimal acuTaxBlockAmount = BigDecimal.ZERO;
					BigDecimal acumCustodyBlockAmount = BigDecimal.ZERO;
					for(int i=0;i<lstHolder.size();i++){
						PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
						paymentAllocationDetail.setBank(null);
						paymentAllocationDetail.setBenefitPaymentAllocation(benefitPaymentAllocation);
						CorporativeOperation corporativeOperation = new CorporativeOperation();
						corporativeOperation.setIdCorporativeOperationPk(corporativeID);
						paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
						paymentAllocationDetail.setCurrency(currency);						
						HolderAccount objHolderAccount = new HolderAccount();
						objHolderAccount.setIdHolderAccountPk(holderAccount);
						paymentAllocationDetail.setHolderAccount(objHolderAccount);
						paymentAllocationDetail.setProcessType(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode());
			
						Long idHolder= (Long)lstHolder.get(i);				
						Holder objHolder = new Holder();
						objHolder.setIdHolderPk(idHolder);
						paymentAllocationDetail.setHolder(objHolder);
						
						if(i==(lstHolder.size()-1)){
							paymentAllocationDetail.setTransactionAmount(totalBlockAmount.subtract(acumTransactionAmount));
							paymentAllocationDetail.setTaxAmount(taxAmount.subtract(acumTaxAmount));
							paymentAllocationDetail.setCustodyAmount(custodyAmount.subtract(acumCustodyAmount));
						}else{							
							paymentAllocationDetail.setTransactionAmount(totalBlockAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumTransactionAmount=acumTransactionAmount.add(paymentAllocationDetail.getTransactionAmount());
							paymentAllocationDetail.setTaxAmount(taxAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumTaxAmount=acumTaxAmount.add(paymentAllocationDetail.getTaxAmount());
							paymentAllocationDetail.setCustodyAmount(custodyAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumCustodyAmount=acumCustodyAmount.add(paymentAllocationDetail.getCustodyAmount());
						}						
						
						Object[] holderAccountBank = allocationBenefitService.getHolderAccountBank(holderAccount,idHolder,currency);
						if(holderAccountBank!=null && holderAccountBank.length>0){
							Long holderAccountBankPK = Long.valueOf(holderAccountBank[0].toString());
							HolderAccountBank objHolderAccountBank = new HolderAccountBank();
							objHolderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
							paymentAllocationDetail.setHolderAccountBank(objHolderAccountBank);
						}
						
						allocationBenefitService.create(paymentAllocationDetail);
						
						//WE CREATE THE DETAIL FOR BLOCKS WHICH WILL GENERATE RETENTION AMOUNTS
						List<Object[]> lstBlockOperations = allocationBenefitService.getBlockDetail(corporativeID,idProcessCorporateResult);
						if(Validations.validateListIsNotNullAndNotEmpty(lstBlockOperations)){
							for(Object[] objBlockOperation : lstBlockOperations){
								Long blockOperationPK = Long.valueOf(objBlockOperation[0].toString());
								BigDecimal grossBlockAmount = new BigDecimal(objBlockOperation[1].toString());
								BigDecimal taxBlockAmount = new BigDecimal(objBlockOperation[2].toString());
								BigDecimal custodyBlockAmount = new BigDecimal(objBlockOperation[3].toString());
								
								BlockBenefitDetail blockBenefitDetail = new BlockBenefitDetail();
								blockBenefitDetail.setPaymentAllocationDetail(paymentAllocationDetail);
								BlockOperation blockOperation = new BlockOperation();
								blockOperation.setIdBlockOperationPk(blockOperationPK);
								blockBenefitDetail.setBlockOperation(blockOperation);
								
								if(i==(lstHolder.size()-1)){
									blockBenefitDetail.setGrossBlockAmount(grossBlockAmount.subtract(acumGrossBlockAmount));
									blockBenefitDetail.setTaxAmount(taxBlockAmount.subtract(acuTaxBlockAmount));
									blockBenefitDetail.setCustodyAmount(custodyBlockAmount.subtract(acumCustodyBlockAmount));
								}else{
									blockBenefitDetail.setGrossBlockAmount(grossBlockAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
									acumGrossBlockAmount=acumGrossBlockAmount.add(blockBenefitDetail.getGrossBlockAmount());
									blockBenefitDetail.setTaxAmount(taxBlockAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
									acuTaxBlockAmount=acuTaxBlockAmount.add(blockBenefitDetail.getTaxAmount());
									blockBenefitDetail.setCustodyAmount(custodyBlockAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
									acumCustodyBlockAmount=acumCustodyBlockAmount.add(blockBenefitDetail.getCustodyAmount());
								}
								
								allocationBenefitService.create(blockBenefitDetail);
							}
						}			
					}					
				}*/
			}
		}
	}

	/**
	 * Metodo que registra consignaciones de pago DIRECTA.
	 *
	 * @param directPaymentList the direct payment list
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param currency the currency
	 */
	private void generateDetailCorporativeResultAvailable(List<CorporativeProcessResult> lstFinalCorporativeDetail, BenefitPaymentAllocation benefitPaymentAllocation, Integer currency, Integer paymentType){


		if(Validations.validateListIsNotNullAndNotEmpty(lstFinalCorporativeDetail)){
			for(CorporativeProcessResult corporativeResult : lstFinalCorporativeDetail){				

				Long holderAccount = corporativeResult.getHolderAccount().getIdHolderAccountPk();
				Long corporativeID = corporativeResult.getCorporativeOperation().getIdCorporativeOperationPk();
				BigDecimal grossAmount = corporativeResult.getAvailableBalance();
				BigDecimal taxAmount = corporativeResult.getTaxAmount();
				BigDecimal custodyAmount = corporativeResult.getCustodyAmount();	
				Participant participant = allocationBenefitService.getParticipantByHolderAccount(holderAccount);
				participant.getIdParticipantPk();
				
				PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
				CorporativeOperation corporativeOperation = new CorporativeOperation();
				corporativeOperation.setIdCorporativeOperationPk(corporativeID);
				paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(holderAccount);
				paymentAllocationDetail.setHolderAccount(objHolderAccount);
				paymentAllocationDetail.setParticipant(participant);
				paymentAllocationDetail.setCurrency(currency);
				

				BenefitPaymentAllocation bpa = new BenefitPaymentAllocation();
				bpa.setIdBenefitPaymentPK(benefitPaymentAllocation.getIdBenefitPaymentPK());
				
				paymentAllocationDetail.setBenefitPaymentAllocation(bpa);
				
				paymentAllocationDetail.setTransactionAmount(grossAmount);
				paymentAllocationDetail.setTaxAmount(taxAmount);
				paymentAllocationDetail.setCustodyAmount(custodyAmount);
				
				if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
					paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
				}else {
					paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
				}
				
				if(paymentType.equals(BenefitPaymentType.PDI.getCode())){
					
					Object[] holderAccountBank = allocationBenefitService.getHolderAccountBank(holderAccount,currency);
					if(holderAccountBank!=null && holderAccountBank.length>0){
						
						Long holderAccountBankPK = Long.valueOf(holderAccountBank[0].toString());
						Long bankPK = Long.valueOf(holderAccountBank[1].toString());
						String accountNumber = holderAccountBank[2].toString();
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						HolderAccountBank objHolderAccountBank = new HolderAccountBank();
						objHolderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
						
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setHolderAccountBank(objHolderAccountBank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
					}
					
				}else if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
					
					Object[] ParticipantBank = allocationBenefitService.getParticipantBank(participant.getIdParticipantPk(),currency,AccountCashFundsType.CENTRALIZING.getCode());
					if(ParticipantBank!=null && ParticipantBank.length>0){
						
						Long institutionCashAccountPK = Long.valueOf(ParticipantBank[0].toString());
						Long bankPK = Long.valueOf(ParticipantBank[1].toString());
						String accountNumber = ParticipantBank[2].toString();
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						InstitutionCashAccount objInstitutionCashAccount = new InstitutionCashAccount();
						objInstitutionCashAccount.setIdInstitutionCashAccountPk(institutionCashAccountPK);
						
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setInstitutionCashAccount(objInstitutionCashAccount);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
					}
				}

				Security objSecurity = allocationBenefitService.getSecurityByCorporative(corporativeID);
				paymentAllocationDetail.setSecurity(objSecurity);
				allocationBenefitService.create(paymentAllocationDetail);
				
			}
		}
	}
	
	private void generateDetailByPayment(List<Long> directPaymentList, BenefitPaymentAllocation benefitPaymentAllocation, Integer currency, Integer paymentType){

		//TO AVOID ORACLE PROBLEMS ORA-01795 WE MUST DIVIDE THE PROCESS IN SUBLIST TO ADD FINALLY TO A RESULT LIST
		List<Object[]> lstFinalCorporativeDetail = new ArrayList<Object[]>();
		Integer actualListSize = directPaymentList.size();
		List<Long> temporalList = null;
		boolean blFirstTime = true;

		if(actualListSize > 1000){

			while(actualListSize > 1000){
				temporalList = directPaymentList.subList(0,1000);
				if(blFirstTime){
					blFirstTime = false;
					lstFinalCorporativeDetail = allocationBenefitService.getCorporativeResultList(temporalList);
				}else{
					lstFinalCorporativeDetail.addAll(allocationBenefitService.getCorporativeResultList(temporalList));
				}
				directPaymentList = directPaymentList.subList(1000,actualListSize);
				actualListSize = directPaymentList.size();
			}

			lstFinalCorporativeDetail.addAll(allocationBenefitService.getCorporativeResultList(directPaymentList));

		}else{
			lstFinalCorporativeDetail = allocationBenefitService.getCorporativeResultList(directPaymentList);
		}

		if(Validations.validateListIsNotNullAndNotEmpty(lstFinalCorporativeDetail)){
			for(Object[] corporativeResult : lstFinalCorporativeDetail){				

				Long holderAccount = Long.valueOf(corporativeResult[0].toString());
				Long corporativeID = Long.valueOf(corporativeResult[1].toString());
				BigDecimal grossAmount = new BigDecimal(corporativeResult[4].toString());
				BigDecimal taxAmount = new BigDecimal(corporativeResult[8].toString());
				BigDecimal custodyAmount = new BigDecimal(corporativeResult[9].toString());	
				Participant participant = allocationBenefitService.getParticipantByHolderAccount(holderAccount);
				
				PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
				CorporativeOperation corporativeOperation = new CorporativeOperation();
				corporativeOperation.setIdCorporativeOperationPk(corporativeID);
				paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(holderAccount);
				paymentAllocationDetail.setHolderAccount(objHolderAccount);
				paymentAllocationDetail.setCurrency(currency);
				paymentAllocationDetail.setBenefitPaymentAllocation(benefitPaymentAllocation);
				
				paymentAllocationDetail.setTransactionAmount(grossAmount);
				paymentAllocationDetail.setTaxAmount(taxAmount);
				paymentAllocationDetail.setCustodyAmount(custodyAmount);
				paymentAllocationDetail.setParticipant(participant);
				
				if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
					paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
				}else {
					paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
				}
				
				if(paymentType.equals(BenefitPaymentType.PDI.getCode())){
					
					Object[] holderAccountBank = allocationBenefitService.getHolderAccountBank(holderAccount,currency);
					if(holderAccountBank!=null && holderAccountBank.length>0){
						
						Long holderAccountBankPK = Long.valueOf(holderAccountBank[0].toString());
						Long bankPK = Long.valueOf(holderAccountBank[1].toString());
						String accountNumber = holderAccountBank[2].toString();
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						HolderAccountBank objHolderAccountBank = new HolderAccountBank();
						objHolderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
						
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setHolderAccountBank(objHolderAccountBank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
					}
					
				}else if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
					
					Object[] ParticipantBank = allocationBenefitService.getParticipantBank(participant.getIdParticipantPk(),currency,AccountCashFundsType.CENTRALIZING.getCode());
					if(ParticipantBank!=null && ParticipantBank.length>0){
						
						Long institutionCashAccountPK = Long.valueOf(ParticipantBank[0].toString());
						Long bankPK = Long.valueOf(ParticipantBank[1].toString());
						String accountNumber = ParticipantBank[2].toString();
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						InstitutionCashAccount objInstitutionCashAccount = new InstitutionCashAccount();
						objInstitutionCashAccount.setIdInstitutionCashAccountPk(institutionCashAccountPK);
						
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setInstitutionCashAccount(objInstitutionCashAccount);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
					}
				}

				Security objSecurity = allocationBenefitService.getSecurityByCorporative(corporativeID);
				paymentAllocationDetail.setSecurity(objSecurity);
				allocationBenefitService.create(paymentAllocationDetail);
				
				/*
				Long holderAccount = Long.valueOf(corporativeResult[0].toString());
				Long corporativeID = Long.valueOf(corporativeResult[1].toString());
				BigDecimal grossAmount = new BigDecimal(corporativeResult[4].toString());
				BigDecimal taxAmount = new BigDecimal(corporativeResult[8].toString());
				BigDecimal custodyAmount = new BigDecimal(corporativeResult[9].toString());				
				
				List<Long> lstHolder = allocationBenefitService.getHolderByAccountParticipant(holderAccount);
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolder)){					
					BigDecimal acumTransactionAmount = BigDecimal.ZERO;
					BigDecimal acumTaxAmount = BigDecimal.ZERO;
					BigDecimal acumCustodyAmount = BigDecimal.ZERO;
					for(int i=0;i<lstHolder.size();i++){
						PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
						CorporativeOperation corporativeOperation = new CorporativeOperation();
						corporativeOperation.setIdCorporativeOperationPk(corporativeID);
						paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
						HolderAccount objHolderAccount = new HolderAccount();
						objHolderAccount.setIdHolderAccountPk(holderAccount);
						paymentAllocationDetail.setHolderAccount(objHolderAccount);
						paymentAllocationDetail.setCurrency(currency);
						paymentAllocationDetail.setBenefitPaymentAllocation(benefitPaymentAllocation);
						
						Long idHolder= (Long)lstHolder.get(i);				
						Holder objHolder = new Holder();
						objHolder.setIdHolderPk(idHolder);
						paymentAllocationDetail.setHolder(objHolder);
						
						if(i==(lstHolder.size()-1)){
							paymentAllocationDetail.setTransactionAmount(grossAmount.subtract(acumTransactionAmount));
							paymentAllocationDetail.setTaxAmount(taxAmount.subtract(acumTaxAmount));
							paymentAllocationDetail.setCustodyAmount(custodyAmount.subtract(acumCustodyAmount));
						}else{							
							paymentAllocationDetail.setTransactionAmount(grossAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumTransactionAmount=acumTransactionAmount.add(paymentAllocationDetail.getTransactionAmount());
							paymentAllocationDetail.setTaxAmount(taxAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumTaxAmount=acumTaxAmount.add(paymentAllocationDetail.getTaxAmount());
							paymentAllocationDetail.setCustodyAmount(custodyAmount.divide(new BigDecimal(lstHolder.size()),2,RoundingMode.DOWN));
							acumCustodyAmount=acumCustodyAmount.add(paymentAllocationDetail.getCustodyAmount());
						}						
						
						allocationBenefitService.create(paymentAllocationDetail);
						
						Object[] holderAccountBank = allocationBenefitService.getHolderAccountBank(holderAccount,idHolder,currency);
						if(holderAccountBank!=null && holderAccountBank.length>0){
							Long bankPK = Long.valueOf(holderAccountBank[1].toString());
							Long holderAccountBankPK = Long.valueOf(holderAccountBank[0].toString());
							Integer indBIC = Integer.valueOf(holderAccountBank[2].toString());
							
							if(BooleanType.YES.getCode().equals(indBIC)){
								paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
							}else{
								paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
							}
							
							Bank objBank = new Bank();
							objBank.setIdBankPk(bankPK);
							paymentAllocationDetail.setBank(objBank);
							
							HolderAccountBank objHolderAccountBank = new HolderAccountBank();
							objHolderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
							paymentAllocationDetail.setHolderAccountBank(objHolderAccountBank);
						}
					}	
				}*/
			}
		}
	}

	/**
	 * Metodo que retorna lista de detalle de datos de los bancos relacionado a la consignacion.
	 *
	 * @param allocationProcess the allocation process
	 * @return the allocation bank detail
	 */
	public List<AllocationBankResumeTO> getAllocationBankDetail(AllocationProcessTO allocationProcess){
		List<AllocationBankResumeTO> lstBankResume = new ArrayList<AllocationBankResumeTO>();
		AllocationBankResumeTO bankResume = null;
		Long bankID ,previousBankID = null;
		String bankDescription = StringUtils.EMPTY, previousBankDescription = StringUtils.EMPTY;
		BigDecimal bankQuantity = BigDecimal.ZERO;
		List<Integer> lstIndirectMotive = new ArrayList<Integer>();
		lstIndirectMotive.add(AllocationProcessType.INDIRECT_PAYMENT.getCode());

		List<Object[]> lstBankDetail = allocationBenefitService.getAllocationDetail(new Long(allocationProcess.getIdAllocationProcess().toString()),null,null);
		if(Validations.validateListIsNotNullAndNotEmpty(lstBankDetail))
		{
			BigDecimal processAmount = BigDecimal.ZERO;
			bankResume = new AllocationBankResumeTO();
			for(Object[] objResult : lstBankDetail){
				bankID = Long.valueOf(objResult[0].toString());
				bankDescription = objResult[1].toString();

				if(Validations.validateIsNull(previousBankID))
				{
					bankQuantity = BigDecimal.ONE;
					BigDecimal grossAmount = new BigDecimal(objResult[9].toString());
					BigDecimal taxAmount = new BigDecimal(objResult[12].toString());
					BigDecimal custodyAmount = new BigDecimal(objResult[13].toString());

					processAmount = grossAmount.subtract((taxAmount.add(custodyAmount)));
				}else
				{
					if(bankID.equals(previousBankID))
					{
						bankQuantity = bankQuantity.add(BigDecimal.ONE);
						BigDecimal grossAmount = new BigDecimal(objResult[9].toString());
						BigDecimal taxAmount = new BigDecimal(objResult[12].toString());
						BigDecimal custodyAmount = new BigDecimal(objResult[13].toString());

						processAmount = processAmount.add(grossAmount.subtract((taxAmount.add(custodyAmount))));
					}else
					{
						bankResume.setBank(previousBankID + " - " + previousBankDescription);
						bankResume.setBankID(previousBankID);
						bankResume.setHolderQuantity(bankQuantity.toString());
						bankResume.setBankAmount(processAmount.toString());
						lstBankResume.add(bankResume);

						bankResume = new AllocationBankResumeTO();
						bankQuantity = BigDecimal.ONE;

						BigDecimal grossAmount = new BigDecimal(objResult[9].toString());
						BigDecimal taxAmount = new BigDecimal(objResult[12].toString());
						BigDecimal custodyAmount = new BigDecimal(objResult[13].toString());

						processAmount = grossAmount.subtract((taxAmount.add(custodyAmount)));
					}
				}
				previousBankID = bankID;
				previousBankDescription = bankDescription;
			}
			bankResume.setBank(previousBankID + " - " + previousBankDescription);
			bankResume.setHolderQuantity(bankQuantity.toString());
			bankResume.setBankAmount(processAmount.toString());
			bankResume.setBankID(previousBankID);
			lstBankResume.add(bankResume);
		}

		return lstBankResume;
	}

	/**
	 * Metodo que retorna lista de detalle de datos del emisor relacionado a la consignacion.
	 *
	 * @param allocationProcess the allocation process
	 * @param lstMotives the lst motives
	 * @return the allocation resume detail
	 */
	public List<AllocationIssuerResumeTO> getAllocationResumeDetail(AllocationProcessTO allocationProcess, List<Integer> lstMotives){
		List<AllocationIssuerResumeTO> lstAllocationIssuerResume = new ArrayList<AllocationIssuerResumeTO>();
		AllocationIssuerResumeTO allocationIssuerResume = null;
		List<Object[]> lstAllocationDetail = allocationBenefitService.getAllocationDetail(new Long(allocationProcess.getIdAllocationProcess().toString()),
																							allocationProcess.getBankID(),lstMotives);
		if(Validations.validateListIsNotNullAndNotEmpty(lstAllocationDetail)){
			for(Object[] paymentAllocationDetail : lstAllocationDetail){
				allocationIssuerResume = new AllocationIssuerResumeTO();
				allocationIssuerResume.setBank(paymentAllocationDetail[0].toString() + " - " + paymentAllocationDetail[1].toString());
				allocationIssuerResume.setRntDescription(paymentAllocationDetail[2].toString() + " - " + paymentAllocationDetail[3].toString());
				Integer documentType = Integer.parseInt(paymentAllocationDetail[4].toString());
				ParameterTable parameterDocumentType = allocationBenefitService.find(documentType,ParameterTable.class);
				allocationIssuerResume.setDocumentTypeNumber(parameterDocumentType.getIndicator1() + " - " + paymentAllocationDetail[5].toString());
				allocationIssuerResume.setBankAccountNumber(paymentAllocationDetail[6].toString());
				allocationIssuerResume.setCorporativeID(paymentAllocationDetail[7].toString());
				allocationIssuerResume.setSecurity(paymentAllocationDetail[8].toString());

				BigDecimal grossAmount = new BigDecimal(paymentAllocationDetail[9].toString());
				BigDecimal taxAmount = new BigDecimal(paymentAllocationDetail[12].toString());
				BigDecimal custodyAmount = new BigDecimal(paymentAllocationDetail[13].toString());

				allocationIssuerResume.setAmount(grossAmount.subtract((custodyAmount.add(taxAmount))));
				
				Integer motiveType = Integer.parseInt(paymentAllocationDetail[11].toString());
				ParameterTable motive = allocationBenefitService.find(motiveType,ParameterTable.class);
				allocationIssuerResume.setProcessType(motive.getDescription());

				lstAllocationIssuerResume.add(allocationIssuerResume);
			}
		}

		return lstAllocationIssuerResume;
	}

	/**
	 * Metodo que valida consignacion para procesar definitivo.
	 *
	 * @param allocationProcess the allocation process
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validaDefinitiveProcess(AllocationProcessTO allocationProcess) throws ServiceException{
		String messageReturn = null;
		Long idAllocationProcess = Long.valueOf(allocationProcess.getIdAllocationProcess().toString());
		Integer currency = allocationProcess.getIdCurrency();
		if(!allocationProcess.getIdAllocateProcessState().equals(AllocationProcessStateType.DEFINITIVE.getCode())){
			if(receptionService.validateActivatedCentralizingAccount(currency)){
				InstitutionCashAccount objInstitutionCashAccount = receptionService.getActivatedIssuerPaymentCashAccount(allocationProcess.getIssuer(),currency);
				BigDecimal allocationAmount = allocationBenefitService.getAmountByIssuerAllocation(null,idAllocationProcess);
				if(allocationAmount.compareTo(objInstitutionCashAccount.getAvailableAmount())!=1){
					//if(allocationBenefitService.validateDevolutionCommercialBankAccount(currency)){
						//if(allocationBenefitService.validateCommercialInstCashAccountAutoProcess(AccountCashFundsType.RATES.getCode(), currency)){
							//if(allocationBenefitService.validateCommercialInstCashAccountAutoProcess(AccountCashFundsType.TAX.getCode(), currency)){
								List<BigDecimal> lstCorporative = allocationBenefitService.getCorporativeByIDAllocation(idAllocationProcess);
								if(Validations.validateListIsNotNullAndNotEmpty(lstCorporative)){			
									for(BigDecimal corporativeID : lstCorporative){
										CorporativeOperation corporativeOperation = allocationBenefitService.find(new Long(corporativeID.toString()),CorporativeOperation.class);
										Integer corporativeState = corporativeOperation.getState();
										if(CorporateProcessStateType.DEFINITIVE.getCode().equals(corporativeState)){
											String issuer = corporativeOperation.getIssuer().getIdIssuerPk();
											if(receptionService.validateDestinyBenefitCashAccountFunds(AccountCashFundsType.BENEFIT.getCode(),currency,issuer)){								
												InstitutionCashAccount cashAccount = receptionService.getDestinyBenefitCashAccountFunds(AccountCashFundsType.BENEFIT.getCode(),currency,issuer);
												BigDecimal issuerAllocationAmount = allocationBenefitService.getAmountByIssuerAllocation(issuer,idAllocationProcess);
												if(issuerAllocationAmount.compareTo(cashAccount.getAvailableAmount()) != 1);																														
												else{
													messageReturn = PropertiesConstants.ISSUER_CASH_ACCOUNT_NOT_ENOUGH_BALANCES;
													break;
												}
											}else{
												messageReturn = PropertiesConstants.NOT_EXIST_CASH_ACCOUNT_ISSUER;
												break;
											}
										}else{
											messageReturn = PropertiesConstants.MESSAGE_CORPORATIVE_NO_DEFINITIVE;
											break;
										}
									}
								}
							/*}else{
								messageReturn = PropertiesConstants.NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_IMP;				
							}*/
						/*}else{
							messageReturn = PropertiesConstants.NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_TAR;				
						}
					}else{
						messageReturn = PropertiesConstants.NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_DEV;					
					}*/			
				}else{				
					messageReturn = PropertiesConstants.ISSUER_CASH_ACCOUNT_NOT_ENOUGH_BALANCES_CENTRALIZING;
				}
			}else{
				messageReturn = PropertiesConstants.NOT_EXIST_CASH_ACCOUNT_CENTRALIZING;
			}
		}else{
			messageReturn = PropertiesConstants.MESSAGE_ALLOCATION_NO_PRELIMINARY;
		}		
		return messageReturn;
	}


	private Client createClientJerser() throws NoSuchAlgorithmException,KeyManagementException{
		return Client.create();
	}
	
	public void createRetirementOperationConfirm(String IdCorporativeOperationPk) {
		
    	StringBuilder urlResource = new StringBuilder();
        urlResource.append(serverPath).append("/").append(ModuleWarType.CUSTODY.getValue()).
        append("/resources/CustodyProcessResource/retirementoperationConfirm").
        append("/").append(IdCorporativeOperationPk);
        try {
			createClientJerser().resource(urlResource.toString()).type("application/json").get(ClientResponse.class);
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UniformInterfaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Metodo que registra consignacion de pagos definitivo.
	 *
	 * @param allocation the allocation
	 * @param user the user
	 * @throws Exception the exception
	 */
	
	public void benefitPaymentPDI(Long idBenefitPaymentPK , LoggerUser loggerUser) throws Exception {
		
		//lista 1- titulares que tienen saldo bloqueado (entregue o no beneficio)
		//lista 2- titulares que tienen saldo disponible
		//
		//registro operacion de fondo (RETIRO) (total) => para la cuenta de efectivo de PAGO DE DERECHOS del emisor
		//registro operacion de fondo (DEPOSITO) (solo titulares de saldo disponible) => para la cuenta de efectivo de PAGO DE DERECHOS del participante
		//registro operacion de fondo (DEPOSITO) (solo titulares de saldo bloqueado) => para la cuenta de efectivo de DEVOLUCION del emisor
		
		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(idBenefitPaymentPK,BenefitPaymentAllocation.class);
		

		List<Object[]> lstBankParticipant = allocationBenefitService.getBankParticipantAllocationDetail(idBenefitPaymentPK);
		List<Object[]> lstIssuerPayment = allocationBenefitService.getFundsOperationIssuer(idBenefitPaymentPK);

		List<Object[]> lstPaymentParticipant = new ArrayList<Object[]>();
		List<Object[]> lstBlock = new ArrayList<Object[]>();
		
		Integer excludeCommission = benefitPaymentAllocation.getIndExcludedCommissions();
		
		List<Long> lstCorporativeID = new ArrayList<Long>();
		List<BigDecimal> lstCorporatives = allocationBenefitService.getCorporativeByIDAllocation(idBenefitPaymentPK);
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporatives)){
			for(BigDecimal corporativeID : lstCorporatives){
				lstCorporativeID.add(new Long(corporativeID.toString()));
			}
		}
		

		for(Object[] allocationDetail : lstBankParticipant){
			Integer motive = Integer.parseInt(allocationDetail[11].toString());
			
			
			if(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode().equals(motive)){
				//retiro para los emisores por cada corporativo (bloqueado)
				generateRetirementCorporativeIssuerFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName(),true);
				//genero ingreso a la cuenta de devoluciones del emisor para los corporativos bloqueados
				generateDepositBlockCorporativeIssuerFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName());
			}else{
				//retiro para los emisores por cada corporativo (no bloqueado)
				generateRetirementCorporativeIssuerFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName(),false);
				//genero ingreso a la cuenta de pago de derechos del participante para los corporativos
				generateDepositCorporativeHolderAccountFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName());
			}
		}
		
		//actualizo el indicador de los corporativos consignados
		allocationBenefitService.updatePaidIndicator(lstCorporativeID,idBenefitPaymentPK);
		
		//pago las comisiones correspondientes
		genereteFundsOperationForServices(benefitPaymentAllocation, lstCorporativeID, excludeCommission, loggerUser.getUserName());


		for(Long corporativeID : lstCorporativeID){
			createRetirementOperationConfirm(corporativeID.toString());
		}
		
		//actulizo la consignacion a definitivo
		benefitPaymentAllocation.setAllocationProcessState(AllocationProcessStateType.DEFINITIVE.getCode());
		corporateServiceBean.update(benefitPaymentAllocation);
	}
	
	private PayrollConsignedOperation  generatePayrollConsignedOperation(Object[] allocationDetail, Long idBenefitPaymentPK) throws ServiceException{
		
		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(idBenefitPaymentPK,BenefitPaymentAllocation.class);
		Long	idCorporativePk = Long.valueOf(allocationDetail[7].toString());
		CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporativePk,CorporativeOperation.class);
		Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
		Integer currency = Integer.valueOf(allocationDetail[1].toString());
		
		String	idSecurityCodePk = allocationDetail[8].toString();
		String	issuer = allocationDetail[12].toString();
		Long	issuerIdInstitutionCashAccountPk = Long.valueOf(allocationDetail[23].toString());
		Long	issuerIdInstitutionBankAccountPk = Long.valueOf(allocationDetail[24].toString());
		InstitutionBankAccount issuerBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(issuerIdInstitutionBankAccountPk);
		InstitutionCashAccount issuerCashAccount = allocationBenefitService.find(issuerIdInstitutionCashAccountPk, InstitutionCashAccount.class);
		
		Long	idParticipantPk = Long.valueOf(allocationDetail[15].toString());
		Integer	processType = Integer.parseInt(allocationDetail[11].toString());
		Long	participantIdInstitutionBankAccount = Long.valueOf(allocationDetail[6].toString());
		InstitutionBankAccount idInstitutionBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(participantIdInstitutionBankAccount);
		Bank participantBank = (idInstitutionBankAccount!=null)?idInstitutionBankAccount.getProviderBank():null;
		Long idParticipantBank = (participantBank!=null)?participantBank.getIdBankPk():null;
		Long participantIdInstitutionCashAccount = (allocationDetail[26]!=null)?Long.valueOf(allocationDetail[26].toString()):null;
		
		InstitutionBankAccount participantBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(participantIdInstitutionBankAccount);
		InstitutionCashAccount participantCashAccount = allocationBenefitService.find(participantIdInstitutionCashAccount, InstitutionCashAccount.class);
		Participant participant = allocationBenefitService.find(idParticipantPk, Participant.class);
		
		Long idHolderAccountPK = (allocationDetail[2]!=null)?Long.valueOf(allocationDetail[2].toString()):0L;		
		String	holderName =  (allocationDetail[3]!=null)?allocationDetail[3].toString():"";
		Integer	documentType = (allocationDetail[4]!=null)?Integer.parseInt(allocationDetail[4].toString()):null;
		String	documentNumber = (allocationDetail[5]!=null)?allocationDetail[5].toString():"";
		Long idHolderPK = (allocationDetail[25]!=null)?Long.valueOf(allocationDetail[25].toString()):null;
		HolderAccountBank holderAccountBank = allocationBenefitService.getHolderAccountBankJoinBank(idHolderAccountPK, currency);
		Long idHolderAccountBank = (holderAccountBank!=null)?holderAccountBank.getIdHolderAccountBankPk():null;
		Long idHolderBank = (holderAccountBank!=null)?holderAccountBank.getBank().getIdBankPk():null;
		
		BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
		BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
		BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
		BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));
		
		
		PayrollConsignedOperation payrollConsignedOperation = new PayrollConsignedOperation();
		payrollConsignedOperation.setIssuer(new Issuer());
		payrollConsignedOperation.getIssuer().setIdIssuerPk(issuer);
		payrollConsignedOperation.setSecurity(new Security());
		payrollConsignedOperation.getSecurity().setIdSecurityCodePk(idSecurityCodePk);
		payrollConsignedOperation.setCurrencyPayment(currency);
		payrollConsignedOperation.setIdAllocationDetailPk(idBenefitPaymentPK.intValue());
		
		payrollConsignedOperation.setAccountNumberBankSource(issuerBankAccount.getAccountNumber());
		payrollConsignedOperation.setAccountNumberBankTarget(participantBankAccount.getAccountNumber());
		payrollConsignedOperation.setCodeBank(participantBankAccount.getProviderBank().getBicCode());
		payrollConsignedOperation.setInvoiceNumber(participant.getPhoneNumber());
		payrollConsignedOperation.setState(PayrollStateType.REGISTERED.getCode());
		payrollConsignedOperation.setPaymentAmount(processAmount);
		
		payrollConsignedOperation.setParticipant(participant);
		payrollConsignedOperation.setEmailTarget( (participant.getEmail()!=null)?participant.getEmail():"" );
		payrollConsignedOperation.setDocumentTypeTarger( (participant.getDocumentType()!=null)?participant.getDocumentType().toString():"" );
		payrollConsignedOperation.setDocumentNumberTarget( (participant.getDocumentNumber()!=null)?participant.getDocumentNumber():"" );
		
		if(currency!=null) {
			payrollConsignedOperation.setCurrency(CurrencyType.get(currency).getCodeIso());
		}
		
		payrollConsignedOperation.setFullnameTarget(participant.getDescription());
		
		payrollConsignedOperation.setConsignedDate(benefitPaymentAllocation.getAllocationProcessDate());
		payrollConsignedOperation.setProcessDate(CommonsUtilities.currentDate());
		payrollConsignedOperation.setRegistryDate(CommonsUtilities.currentDate());
		
		payrollConsignedOperation.setSituation(processType);
		payrollConsignedOperation.setTransferMotive("Pago por consignacion de corporativo Nro "+corporativeOperation.getIdCorporativeOperationPk());
		payrollConsignedOperation.setPaymentNumber(corporativeOperation.getIdCorporativeOperationPk().intValue());
		payrollConsignedOperation.setGeneratedFrame("");
		payrollConsignedOperation.setSmsMessage("");
		
		
		allocationBenefitService.create(payrollConsignedOperation);
		
		return payrollConsignedOperation;
	}
	

	public void benefitPaymentPDD(Long idBenefitPaymentPK , LoggerUser loggerUser) throws Exception {
		
		//lista 1- titulares que tienen saldo bloqueado (entregue o no beneficio)
		//lista 2- titulares que tienen saldo disponible
		//
		//registro operacion de fondo (RETIRO) (total) => para la cuenta de efectivo de PAGO DE DERECHOS del emisor
		//registro operacion de fondo (DEPOSITO) (solo titulares de saldo disponible) => para la cuenta de efectivo de PAGO DE DERECHOS del participante
		//registro operacion de fondo (DEPOSITO) (solo titulares de saldo bloqueado) => para la cuenta de efectivo de DEVOLUCION del emisor
		
		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(idBenefitPaymentPK,BenefitPaymentAllocation.class);
		

		List<Object[]> lstBankParticipant = allocationBenefitService.getBankParticipantAllocationDetail(idBenefitPaymentPK);
		List<Object[]> lstIssuerPayment = allocationBenefitService.getFundsOperationIssuer(idBenefitPaymentPK);

		List<Object[]> lstPaymentParticipant = new ArrayList<Object[]>();
		List<Object[]> lstBlock = new ArrayList<Object[]>();
		
		Integer excludeCommission = benefitPaymentAllocation.getIndExcludedCommissions();
		
		List<Long> lstCorporativeID = new ArrayList<Long>();
		List<BigDecimal> lstCorporatives = allocationBenefitService.getCorporativeByIDAllocation(idBenefitPaymentPK);
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporatives)){
			for(BigDecimal corporativeID : lstCorporatives){
				lstCorporativeID.add(new Long(corporativeID.toString()));
			}
		}

		for(Object[] allocationDetail : lstBankParticipant){
			Integer motive = Integer.parseInt(allocationDetail[11].toString());
			
			if(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode().equals(motive)){
				//retiro para los emisores por cada corporativo (bloqueado)
				generateRetirementCorporativeIssuerFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName(),true);
				//genero ingreso a la cuenta de devoluciones del emisor para los corporativos bloqueados
				generateDepositBlockCorporativeIssuerFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName());
			}else{
				//retiro para los emisores por cada corporativo (no bloqueado)
				generateRetirementCorporativeIssuerFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName(),false);
				//genero ingreso a la cuenta de pago de derechos del participante para los corporativos
				generateDepositCorporativeParticipantFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName());
				//genero el retiro automatico de pago de derechos del participante para los corporativos
				generateRetirementCorporativeParticipantFund(allocationDetail,idBenefitPaymentPK,loggerUser.getUserName());
				
			}
			//genero los archivos de pago a bancos solo para los depositantes que no tienen saldo bloqueado
			//generatePayrollConsignedOperation(allocationDetail, idBenefitPaymentPK);
			//ahora los genera un procedure
		}
		//salida e ingreso de las comisiones correspondientes de la cuenta de pago de derecho
		genereteFundsOperationForServices(benefitPaymentAllocation, lstCorporativeID, excludeCommission, loggerUser.getUserName());

		//actualizo el indicador de los corporativos consignados
		allocationBenefitService.updatePaidIndicator(lstCorporativeID,idBenefitPaymentPK);


		for(Long corporativeID : lstCorporativeID){
			createRetirementOperationConfirm(corporativeID.toString());
		}
		
		//actulizo la consignacion a definitivo
		benefitPaymentAllocation.setAllocationProcessState(AllocationProcessStateType.DEFINITIVE.getCode());
		corporateServiceBean.update(benefitPaymentAllocation);
	}
	
	/*******************************************************************************************************************************************/
	public void executeDefinitiveAllocation(String allocation,String user) throws Exception{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDefinitive());

		
		Long idBenefitPaymentPK = Long.valueOf(allocation);
		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(idBenefitPaymentPK,BenefitPaymentAllocation.class);
		
		if(idepositarySetup != null && idepositarySetup.getPaymentType().equals(BenefitPaymentType.PDI.getCode())){
			//PDI - PAGO AL INVERSIONISTA
			benefitPaymentPDI(idBenefitPaymentPK, loggerUser);
		}else {
			//PDD - PAGO AL DEPOSITANTE
			benefitPaymentPDD(idBenefitPaymentPK, loggerUser);
		}

	}
	

	private PayrollConsignedOperation payrollConsignedOperationToAllocationDetail(Object[] allocationDetail) throws ServiceException{
		//1		ISSUER REGISTER THE FUND OPERATION
				FundsOperation issuerOperation = new FundsOperation();
				Long idCorporative = Long.valueOf(allocationDetail[7].toString());
				CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporative,CorporativeOperation.class);
				Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
				ParameterTable parameterEvenType = allocationBenefitService.find(corporativeProcessType,ParameterTable.class);
				String issuer = allocationDetail[12].toString();
				Integer currency = Integer.valueOf(allocationDetail[1].toString());
				Long idHolderAccountBank = Long.valueOf(allocationDetail[6].toString());
				HolderAccountBank holderAccountBank = allocationBenefitService.find(idHolderAccountBank,HolderAccountBank.class);
				String holderDescription =  allocationDetail[3].toString();
				Long idBank = Long.valueOf(allocationDetail[0].toString());
				Long holderPK = Long.valueOf(allocationDetail[2].toString());
				String documentNumber = allocationDetail[5].toString();
				Long holderAccountPK = new Long(allocationDetail[5].toString());
				Integer processType = Integer.parseInt(allocationDetail[5].toString());

				BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
				BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
				BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
				BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));
				
				Date allocationProcessDate = (Date)allocationDetail[21];
				String idSecurityCodePk = allocationDetail[8].toString();
				Long idAllocationDetailPK = Long.valueOf(allocationDetail[22].toString());
				

				InstitutionCashAccount benefitCashAccount = allocationBenefitService.getCentralizerActivateBenefitCashAccount(issuer,currency);
				issuerOperation.setInstitutionCashAccount(benefitCashAccount);

				issuerOperation.setHolderDescription(holderDescription);
				Bank bank = receptionService.find(idBank,Bank.class);
				issuerOperation.setBank(bank);
				if(holderAccountBank!=null) {
					issuerOperation.setBankAccountNumber(holderAccountBank.getBankAccountNumber());
				}
				issuerOperation.setIndCentralized(BooleanType.NO.getCode());
				issuerOperation.setWithdrawlSwiftBic(bank.getBicCode());

				Holder holderIssuer = allocationBenefitService.find(holderPK,Holder.class);
				HolderAccount holderHolderAccountCorp = (holderAccountPK!=null)?allocationBenefitService.find(holderAccountPK,HolderAccount.class):null;
				
				
				PayrollConsignedOperation payrollConsignedOperation = new PayrollConsignedOperation();
				payrollConsignedOperation.setIssuer(new Issuer());
				payrollConsignedOperation.getIssuer().setIdIssuerPk(issuer);
				payrollConsignedOperation.setSecurity(new Security());
				payrollConsignedOperation.getSecurity().setIdSecurityCodePk(idSecurityCodePk);
				payrollConsignedOperation.setCurrencyPayment(currency);
				payrollConsignedOperation.setIdAllocationDetailPk(idAllocationDetailPK.intValue());

				payrollConsignedOperation.setAccountNumberBankSource("444444444");
				payrollConsignedOperation.setAccountNumberBankTarget("555555555");
				payrollConsignedOperation.setCodeBank("BANK");
				payrollConsignedOperation.setInvoiceNumber("8774585");
				payrollConsignedOperation.setState(1);
				
				
				payrollConsignedOperation.setParticipant(new Participant());
				payrollConsignedOperation.getParticipant().setIdParticipantPk(1L);
				payrollConsignedOperation.setEmailTarget("email@cavapy.py");
				payrollConsignedOperation.setDocumentTypeTarger("tipoDocumento");
				payrollConsignedOperation.setDocumentNumberTarget("numeroDocumento");
				
				if(currency!=null) {
					payrollConsignedOperation.setCurrency(CurrencyType.get(currency).getCodeIso());
				}
				if(holderHolderAccountCorp != null && holderHolderAccountCorp.getDescription()!=null) {
					payrollConsignedOperation.setFullnameTarget(holderHolderAccountCorp.getDescription());
				}
				payrollConsignedOperation.setConsignedDate(allocationProcessDate);
				payrollConsignedOperation.setProcessDate(CommonsUtilities.currentDate());
				payrollConsignedOperation.setRegistryDate(CommonsUtilities.currentDate());
				
				payrollConsignedOperation.setSituation(processType);
				payrollConsignedOperation.setTransferMotive("Pago por consignacion de corporativo");
				payrollConsignedOperation.setPaymentNumber(0);
				payrollConsignedOperation.setGeneratedFrame("");
				payrollConsignedOperation.setSmsMessage("");
				
				return payrollConsignedOperation;
	}

	/**
	 * Metodo que registra operacion de fondo para consignacion de pagos DIRECTA.
	 *
	 * @param allocationDetail the allocation detail
	 * @param allocationID the allocation id
	 * @param userName the user name
	 * @throws ServiceException the service exception
	 */

	private void generateDepositCorporativeHolderAccountFund(Object[] allocationDetail, Long allocationID, String userName) throws ServiceException{

		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(allocationID,BenefitPaymentAllocation.class);
		Long	idCorporativePk = Long.valueOf(allocationDetail[7].toString());
		CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporativePk,CorporativeOperation.class);
		Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
		Integer currency = Integer.valueOf(allocationDetail[1].toString());
		Long 	fundOperationType = ParameterFundsOperationType.PAYMENT_BENEFITS_FUND_DEPOSITS.getCode();
		
		String	issuer = allocationDetail[12].toString();
		Long	issuerIdInstitutionCashAccountPk = Long.valueOf(allocationDetail[23].toString());
		Long	issuerIdInstitutionBankAccountPk = Long.valueOf(allocationDetail[24].toString());
		InstitutionBankAccount issuerBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(issuerIdInstitutionBankAccountPk);
		InstitutionCashAccount issuerCashAccount = allocationBenefitService.find(issuerIdInstitutionCashAccountPk, InstitutionCashAccount.class);

		Long	idParticipantPk = Long.valueOf(allocationDetail[15].toString());
		Integer	processType = Integer.parseInt(allocationDetail[11].toString());
		Long	participantIdInstitutionBankAccount = Long.valueOf(allocationDetail[6].toString());
		InstitutionBankAccount idInstitutionBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(participantIdInstitutionBankAccount);
		Bank participantBank = (idInstitutionBankAccount!=null)?idInstitutionBankAccount.getBank():null;
		Long idParticipantBank = (participantBank!=null)?participantBank.getIdBankPk():null;
		Long participantIdInstitutionCashAccount = (allocationDetail[26]!=null)?Long.valueOf(allocationDetail[26].toString()):null;

		Long idHolderAccountPK = (allocationDetail[2]!=null)?Long.valueOf(allocationDetail[2].toString()):0L;		
		String	holderName =  allocationDetail[3].toString();
		Integer	documentType = Integer.parseInt(allocationDetail[4].toString());
		String	documentNumber = allocationDetail[5].toString();
		Long idHolderPK = (allocationDetail[25]!=null)?Long.valueOf(allocationDetail[25].toString()):null;
		HolderAccountBank holderAccountBank = allocationBenefitService.getHolderAccountBankJoinBank(idHolderAccountPK, currency);
		Long idHolderAccountBank = (holderAccountBank!=null)?holderAccountBank.getIdHolderAccountBankPk():null;
		Long idHolderBank = (holderAccountBank!=null)?holderAccountBank.getBank().getIdBankPk():null;
		
		BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
		BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
		BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
		BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));
		
		//1		ISSUER REGISTER THE FUND OPERATION
		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		
		FundsOperation centralizerFundOperation = new FundsOperation();
		InstitutionCashAccount participantCashAccount = allocationBenefitService.find(participantIdInstitutionCashAccount, InstitutionCashAccount.class);
		
		centralizerFundOperation.setFundOperationNumber(correlative);
		centralizerFundOperation.setBank(holderAccountBank.getBank());
		centralizerFundOperation.setInstitutionCashAccount(participantCashAccount);
		centralizerFundOperation.setIndCentralized(BooleanType.YES.getCode());
		centralizerFundOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		centralizerFundOperation.setOperationAmount(processAmount);
		centralizerFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralizerFundOperation.setIndAutomatic(BooleanType.YES.getCode());
		centralizerFundOperation.setRegistryUser(userName);
		centralizerFundOperation.setRegistryDate(CommonsUtilities.currentDate());
		centralizerFundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralizerFundOperation.setFundsOperationType(fundOperationType);
		centralizerFundOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		centralizerFundOperation.setCurrency(currency);

		allocationBenefitService.create(centralizerFundOperation);
		executeFundComponent(centralizerFundOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());

	}
	
	private void generateDepositCorporativeParticipantFund(Object[] allocationDetail, Long allocationID, String userName) throws ServiceException{

		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(allocationID,BenefitPaymentAllocation.class);
		Long	idCorporativePk = Long.valueOf(allocationDetail[7].toString());
		CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporativePk,CorporativeOperation.class);
		Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
		Integer currency = Integer.valueOf(allocationDetail[1].toString());
		Long 	fundOperationType = ParameterFundsOperationType.PAYMENT_BENEFITS_FUND_DEPOSITS.getCode();
		
		String	issuer = allocationDetail[12].toString();
		Long	issuerIdInstitutionCashAccountPk = Long.valueOf(allocationDetail[23].toString());
		Long	issuerIdInstitutionBankAccountPk = Long.valueOf(allocationDetail[24].toString());
		InstitutionBankAccount issuerBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(issuerIdInstitutionBankAccountPk);
		InstitutionCashAccount issuerCashAccount = allocationBenefitService.find(issuerIdInstitutionCashAccountPk, InstitutionCashAccount.class);

		Long	idParticipantPk = Long.valueOf(allocationDetail[15].toString());
		Participant participant = allocationBenefitService.find(idParticipantPk, Participant.class);
				
		Integer	processType = Integer.parseInt(allocationDetail[11].toString());
		Long	participantIdInstitutionBankAccount = Long.valueOf(allocationDetail[6].toString());
		InstitutionBankAccount idInstitutionBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(participantIdInstitutionBankAccount);
		Bank participantBank = (idInstitutionBankAccount!=null)?idInstitutionBankAccount.getProviderBank():null;
		Long idParticipantBank = (participantBank!=null)?participantBank.getIdBankPk():null;
		Long participantIdInstitutionCashAccount = (allocationDetail[26]!=null)?Long.valueOf(allocationDetail[26].toString()):null;

		Long idHolderAccountPK = (allocationDetail[2]!=null)?Long.valueOf(allocationDetail[2].toString()):0L;		
		String	holderName =  (allocationDetail[3]!=null)?allocationDetail[3].toString():"";
		Integer	documentType = (allocationDetail[4]!=null)?Integer.parseInt(allocationDetail[4].toString()):null;
		String	documentNumber = (allocationDetail[5]!=null)?allocationDetail[5].toString():"";
		Long idHolderPK = (allocationDetail[25]!=null)?Long.valueOf(allocationDetail[25].toString()):null;
		HolderAccountBank holderAccountBank = allocationBenefitService.getHolderAccountBankJoinBank(idHolderAccountPK, currency);
		Long idHolderAccountBank = (holderAccountBank!=null)?holderAccountBank.getIdHolderAccountBankPk():null;
		Long idHolderBank = (holderAccountBank!=null)?holderAccountBank.getBank().getIdBankPk():null;
		
		BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
		BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
		BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
		BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));
		
		//1		ISSUER REGISTER THE FUND OPERATION
		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode(), FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		
		FundsOperation fundsOperation = new FundsOperation();
		InstitutionCashAccount participantCashAccount = allocationBenefitService.find(participantIdInstitutionCashAccount, InstitutionCashAccount.class);
		fundsOperation.setFundOperationNumber(correlative);
		fundsOperation.setBank(participantBank);
		fundsOperation.setInstitutionCashAccount(participantCashAccount);
		fundsOperation.setIndCentralized(BooleanType.NO.getCode());
		fundsOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		fundsOperation.setOperationAmount(processAmount);
		fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		fundsOperation.setRegistryUser(userName);
		fundsOperation.setRegistryDate(CommonsUtilities.currentDate());
		fundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundsOperation.setFundsOperationType(fundOperationType);
		fundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode());
		fundsOperation.setCurrency(currency);
		fundsOperation.setCorporativeOperation(corporativeOperation);
		
		allocationBenefitService.create(fundsOperation);
		executeFundComponentState(fundsOperation, BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode(), MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		
	}
	
	private void generateRetirementCorporativeParticipantFund(Object[] allocationDetail, Long allocationID, String userName) throws ServiceException{

		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(allocationID,BenefitPaymentAllocation.class);
		Long	idCorporativePk = Long.valueOf(allocationDetail[7].toString());
		CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporativePk,CorporativeOperation.class);
		Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
		Integer currency = Integer.valueOf(allocationDetail[1].toString());
		Long 	fundOperationType = ParameterFundsOperationType.WITHDRAWL_FUND_BENEFITS.getCode();
		
		String	issuer = allocationDetail[12].toString();
		Long	issuerIdInstitutionCashAccountPk = Long.valueOf(allocationDetail[23].toString());
		Long	issuerIdInstitutionBankAccountPk = Long.valueOf(allocationDetail[24].toString());
		InstitutionBankAccount issuerBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(issuerIdInstitutionBankAccountPk);
		InstitutionCashAccount issuerCashAccount = allocationBenefitService.find(issuerIdInstitutionCashAccountPk, InstitutionCashAccount.class);

		Long	idParticipantPk = Long.valueOf(allocationDetail[15].toString());
		Participant participant = allocationBenefitService.find(idParticipantPk, Participant.class);
				
		Integer	processType = Integer.parseInt(allocationDetail[11].toString());
		Long	participantIdInstitutionBankAccount = Long.valueOf(allocationDetail[6].toString());
		InstitutionBankAccount idInstitutionBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(participantIdInstitutionBankAccount);
		Bank participantBank = (idInstitutionBankAccount!=null)?idInstitutionBankAccount.getProviderBank():null;
		Long idParticipantBank = (participantBank!=null)?participantBank.getIdBankPk():null;
		Long participantIdInstitutionCashAccount = (allocationDetail[26]!=null)?Long.valueOf(allocationDetail[26].toString()):null;

		Long idHolderAccountPK = (allocationDetail[2]!=null)?Long.valueOf(allocationDetail[2].toString()):0L;		
		String	holderName =  (allocationDetail[3]!=null)?allocationDetail[3].toString():"";
		Integer	documentType = (allocationDetail[4]!=null)?Integer.parseInt(allocationDetail[4].toString()):null;
		String	documentNumber = (allocationDetail[5]!=null)?allocationDetail[5].toString():"";
		Long idHolderPK = (allocationDetail[25]!=null)?Long.valueOf(allocationDetail[25].toString()):null;
		HolderAccountBank holderAccountBank = allocationBenefitService.getHolderAccountBankJoinBank(idHolderAccountPK, currency);
		Long idHolderAccountBank = (holderAccountBank!=null)?holderAccountBank.getIdHolderAccountBankPk():null;
		Long idHolderBank = (holderAccountBank!=null)?holderAccountBank.getBank().getIdBankPk():null;
		
		BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
		BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
		BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
		BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));
		
		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		
		FundsOperation fundsOperation = new FundsOperation();
		InstitutionCashAccount participantCashAccount = allocationBenefitService.find(participantIdInstitutionCashAccount, InstitutionCashAccount.class);
		fundsOperation.setFundOperationNumber(correlative);
		fundsOperation.setBank(participantBank);
		fundsOperation.setInstitutionCashAccount(participantCashAccount);
		fundsOperation.setInstitutionBankAccount(idInstitutionBankAccount);
		fundsOperation.setParticipant(participant);
		fundsOperation.setIndCentralized(BooleanType.NO.getCode());
		fundsOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		fundsOperation.setOperationAmount(processAmount);
		fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		fundsOperation.setRegistryUser(userName);
		fundsOperation.setRegistryDate(CommonsUtilities.currentDate());
		fundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundsOperation.setFundsOperationType(fundOperationType);
		fundsOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode());
		fundsOperation.setCurrency(currency);
		fundsOperation.setIndBankRetirement(1);
		fundsOperation.setCorporativeOperation(corporativeOperation);
		
		allocationBenefitService.create(fundsOperation);
		executeFundComponentState(fundsOperation, BusinessProcessType.FUNDS_RETIREMENT_CONFIRM.getCode(), MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		
	}
	
	private void generateDepositBlockCorporativeIssuerFund(Object[] allocationDetail, Long allocationID, String userName) throws ServiceException{

		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(allocationID,BenefitPaymentAllocation.class);
		Long	idCorporativePk = Long.valueOf(allocationDetail[7].toString());
		CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporativePk,CorporativeOperation.class);
		Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
		Integer currency = Integer.valueOf(allocationDetail[1].toString());
		Long 	fundOperationType = ParameterFundsOperationType.BLOCK_HOLDER_DEPOSIT_RETENTION.getCode();
		
		String	issuer = allocationDetail[12].toString();
		Long	issuerIdInstitutionCashAccountPk = Long.valueOf(allocationDetail[23].toString());
		Long	issuerIdInstitutionBankAccountPk = Long.valueOf(allocationDetail[24].toString());
		InstitutionBankAccount issuerBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(issuerIdInstitutionBankAccountPk);
		InstitutionCashAccount issuerCashAccount = allocationBenefitService.find(issuerIdInstitutionCashAccountPk, InstitutionCashAccount.class);

		Long	idParticipantPk = Long.valueOf(allocationDetail[15].toString());
		Integer	processType = Integer.parseInt(allocationDetail[11].toString());
		Long	participantIdInstitutionBankAccount = Long.valueOf(allocationDetail[6].toString());
		InstitutionBankAccount idInstitutionBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(participantIdInstitutionBankAccount);
		Bank participantBank = (idInstitutionBankAccount!=null)?idInstitutionBankAccount.getProviderBank():null;
		Long idParticipantBank = (participantBank!=null)?participantBank.getIdBankPk():null;
		Long participantIdInstitutionCashAccount = (allocationDetail[26]!=null)?Long.valueOf(allocationDetail[26].toString()):null;

		Long idHolderAccountPK = (allocationDetail[2]!=null)?Long.valueOf(allocationDetail[2].toString()):0L;		
		String	holderName =  (allocationDetail[3]!=null)?allocationDetail[3].toString():"";
		Integer	documentType = (allocationDetail[4]!=null)?Integer.parseInt(allocationDetail[4].toString()):null;
		String	documentNumber = (allocationDetail[5]!=null)?allocationDetail[5].toString():"";
		Long idHolderPK = (allocationDetail[25]!=null)?Long.valueOf(allocationDetail[25].toString()):null;
		HolderAccountBank holderAccountBank = allocationBenefitService.getHolderAccountBankJoinBank(idHolderAccountPK, currency);
		Long idHolderAccountBank = (holderAccountBank!=null)?holderAccountBank.getIdHolderAccountBankPk():null;
		Long idHolderBank = (holderAccountBank!=null)?holderAccountBank.getBank().getIdBankPk():null;
		
		BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
		BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
		BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
		BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));
		
		//1		ISSUER REGISTER THE FUND OPERATION
		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		
		FundsOperation centralizerFundOperation = new FundsOperation();
		InstitutionCashAccount participantCashAccount = allocationBenefitService.find(participantIdInstitutionCashAccount, InstitutionCashAccount.class);
		
		Long idCavapyParticipantPk = idepositarySetup.getIdParticipantDepositary();
		InstitutionCashAccount cavapyCashAccounts = allocationBenefitService.getParticipantActivateInstitutionCashAccount(idCavapyParticipantPk,AccountCashFundsType.BENEFIT.getCode(), currency);
		
		centralizerFundOperation.setFundOperationNumber(correlative);
		//centralizerFundOperation.setBank(participantBank);
		centralizerFundOperation.setInstitutionCashAccount(cavapyCashAccounts);//cavapyCashAccounts	//participantCashAccount
		centralizerFundOperation.setIndCentralized(BooleanType.NO.getCode());
		centralizerFundOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		centralizerFundOperation.setOperationAmount(processAmount);
		centralizerFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralizerFundOperation.setIndAutomatic(BooleanType.YES.getCode());
		centralizerFundOperation.setRegistryUser(userName);
		centralizerFundOperation.setRegistryDate(CommonsUtilities.currentDate());
		centralizerFundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralizerFundOperation.setFundsOperationType(fundOperationType);
		centralizerFundOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode());
		centralizerFundOperation.setCurrency(currency);
		centralizerFundOperation.setCorporativeOperation(corporativeOperation);

		allocationBenefitService.create(centralizerFundOperation);
		executeFundComponent(centralizerFundOperation, BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode());

	}
	
	private FundsOperation generateRetirementCorporativeIssuerFund(Object[] allocationDetail, Long allocationID, String userName, Boolean isBlockBalance) throws ServiceException{

		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(allocationID,BenefitPaymentAllocation.class);
		Long	idCorporativePk = Long.valueOf(allocationDetail[7].toString());
		CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporativePk,CorporativeOperation.class);
		Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
		Integer currency = Integer.valueOf(allocationDetail[1].toString());
		Long 	fundOperationType = (isBlockBalance)?ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode():validatecentralizerFundOperationtype(corporativeProcessType,true);
		
		String	issuer = allocationDetail[12].toString();
		Long	issuerIdInstitutionCashAccountPk = Long.valueOf(allocationDetail[23].toString());
		Long	issuerIdInstitutionBankAccountPk = Long.valueOf(allocationDetail[24].toString());
		InstitutionBankAccount issuerBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(issuerIdInstitutionBankAccountPk);
		InstitutionCashAccount issuerCashAccount = allocationBenefitService.find(issuerIdInstitutionCashAccountPk, InstitutionCashAccount.class);

		Long	idParticipantPk = Long.valueOf(allocationDetail[15].toString());
		Integer	processType = Integer.parseInt(allocationDetail[11].toString());
		Long	participantIdInstitutionBankAccount = Long.valueOf(allocationDetail[6].toString());
		InstitutionBankAccount idInstitutionBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(participantIdInstitutionBankAccount);
		Bank participantBank = (idInstitutionBankAccount!=null)?idInstitutionBankAccount.getProviderBank():null;
		Long idParticipantBank = (participantBank!=null)?participantBank.getIdBankPk():null;
		Long participantIdInstitutionCashAccount = (allocationDetail[26]!=null)?Long.valueOf(allocationDetail[26].toString()):null;

		Long idHolderAccountPK = (allocationDetail[2]!=null)?Long.valueOf(allocationDetail[2].toString()):0L;		
		String	holderName =  "";//allocationDetail[3].toString();
		Integer	documentType = null;//Integer.parseInt(allocationDetail[4].toString());
		String	documentNumber = "";//allocationDetail[5].toString();
		Long idHolderPK = (allocationDetail[25]!=null)?Long.valueOf(allocationDetail[25].toString()):null;
		HolderAccountBank holderAccountBank = allocationBenefitService.getHolderAccountBankJoinBank(idHolderAccountPK, currency);
		Long idHolderAccountBank = (holderAccountBank!=null)?holderAccountBank.getIdHolderAccountBankPk():null;
		Long idHolderBank = (holderAccountBank!=null)?holderAccountBank.getBank().getIdBankPk():null;
		
		BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
		BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
		BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
		BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));
		
		//1		ISSUER REGISTER THE FUND OPERATION
		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		
		FundsOperation centralizerFundOperation = new FundsOperation();
		centralizerFundOperation.setFundOperationNumber(correlative);
		centralizerFundOperation.setBank(issuerBankAccount.getProviderBank());
		centralizerFundOperation.setInstitutionCashAccount(issuerCashAccount);
		centralizerFundOperation.setIndCentralized(BooleanType.NO.getCode());
		centralizerFundOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		centralizerFundOperation.setOperationAmount(processAmount);
		centralizerFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralizerFundOperation.setIndAutomatic(BooleanType.YES.getCode());
		centralizerFundOperation.setRegistryUser(userName);
		centralizerFundOperation.setRegistryDate(CommonsUtilities.currentDate());
		centralizerFundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralizerFundOperation.setFundsOperationType(fundOperationType);
		centralizerFundOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		centralizerFundOperation.setCurrency(currency);
		centralizerFundOperation.setCorporativeOperation(corporativeOperation);

		Long idBusinessProcessPk = (isBlockBalance)?BusinessProcessType.FUNDS_RETIREMENT_CONFIRM.getCode():BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode();
		
		allocationBenefitService.create(centralizerFundOperation);
		executeFundComponent(centralizerFundOperation, idBusinessProcessPk);		

		return centralizerFundOperation;
	}
	
	
	private void generateParticipantFundAllocation(Object[] allocationDetail, Long allocationID, String userName) throws ServiceException{

		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(allocationID,BenefitPaymentAllocation.class);
		Long	idCorporativePk = Long.valueOf(allocationDetail[7].toString());
		CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporativePk,CorporativeOperation.class);
		Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
		Integer currency = Integer.valueOf(allocationDetail[1].toString());
		Long 	fundOperationType = validatecentralizerFundOperationtype(corporativeProcessType,true);
		
		String	issuer = allocationDetail[12].toString();
		Long	issuerIdInstitutionCashAccountPk = Long.valueOf(allocationDetail[23].toString());
		Long	issuerIdInstitutionBankAccountPk = Long.valueOf(allocationDetail[24].toString());
		InstitutionBankAccount issuerBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(issuerIdInstitutionBankAccountPk);
		InstitutionCashAccount issuerCashAccount = allocationBenefitService.find(issuerIdInstitutionCashAccountPk, InstitutionCashAccount.class);

		Long	idParticipantPk = Long.valueOf(allocationDetail[15].toString());
		Integer	processType = Integer.parseInt(allocationDetail[11].toString());
		Long	participantIdInstitutionBankAccount = Long.valueOf(allocationDetail[6].toString());
		InstitutionBankAccount idInstitutionBankAccount = allocationBenefitService.getInstitutionBankAccountJoinBank(participantIdInstitutionBankAccount);
		Bank participantBank = (idInstitutionBankAccount!=null)?idInstitutionBankAccount.getProviderBank():null;
		Long idParticipantBank = (participantBank!=null)?participantBank.getIdBankPk():null;
		Long participantIdInstitutionCashAccount = (allocationDetail[26]!=null)?Long.valueOf(allocationDetail[26].toString()):null;

		Long idHolderAccountPK = (allocationDetail[2]!=null)?Long.valueOf(allocationDetail[2].toString()):0L;		
		String	holderName =  allocationDetail[3].toString();
		Integer	documentType = Integer.parseInt(allocationDetail[4].toString());
		String	documentNumber = allocationDetail[5].toString();
		Long idHolderPK = (allocationDetail[25]!=null)?Long.valueOf(allocationDetail[25].toString()):null;
		HolderAccountBank holderAccountBank = allocationBenefitService.getHolderAccountBankJoinBank(idHolderAccountPK, currency);
		Long idHolderAccountBank = (holderAccountBank!=null)?holderAccountBank.getIdHolderAccountBankPk():null;
		Long idHolderBank = (holderAccountBank!=null)?holderAccountBank.getBank().getIdBankPk():null;
		
		BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
		BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
		BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
		BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));
		
		//1		ISSUER REGISTER THE FUND OPERATION
		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		
		FundsOperation issuerFundosOperation = new FundsOperation();

		issuerFundosOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		issuerFundosOperation.setFundOperationNumber(correlative);
		issuerFundosOperation.setInstitutionCashAccount(issuerCashAccount);
		issuerFundosOperation.setBank(issuerBankAccount.getBank());
		issuerFundosOperation.setBankAccountNumber(issuerBankAccount.getAccountNumber());
		issuerFundosOperation.setIndCentralized(BooleanType.NO.getCode());
		issuerFundosOperation.setWithdrawlSwiftBic(issuerBankAccount.getBicCodeBank());
		issuerFundosOperation.setOperationAmount(processAmount);
		issuerFundosOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		issuerFundosOperation.setIndAutomatic(BooleanType.NO.getCode());
		issuerFundosOperation.setRegistryUser(userName);
		issuerFundosOperation.setRegistryDate(CommonsUtilities.currentDate());
		issuerFundosOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		issuerFundosOperation.setFundsOperationType(fundOperationType);
		issuerFundosOperation.setPaymentReference(("FIELD_NOT_EXIST.").concat(correlative.toString()));
		issuerFundosOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		issuerFundosOperation.setCurrency(currency);
		issuerFundosOperation.setIndBcrdPayback(BooleanType.NO.getCode());

		allocationBenefitService.create(issuerFundosOperation);
		executeFundComponent(issuerFundosOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		
		
		//2		REGISTER CENTRALIZE FUND OPERATION
		FundsOperation centralizerFundOperation = new FundsOperation();
		InstitutionCashAccount participantCashAccount = allocationBenefitService.find(participantIdInstitutionCashAccount, InstitutionCashAccount.class);
		centralizerFundOperation.setInstitutionCashAccount(participantCashAccount);
		centralizerFundOperation.setIndCentralized(BooleanType.YES.getCode());
		centralizerFundOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		centralizerFundOperation.setOperationAmount(processAmount);
		centralizerFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralizerFundOperation.setIndAutomatic(BooleanType.NO.getCode());
		centralizerFundOperation.setRegistryUser(userName);
		centralizerFundOperation.setRegistryDate(CommonsUtilities.currentDate());
		centralizerFundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralizerFundOperation.setFundsOperationType(fundOperationType);
		centralizerFundOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		centralizerFundOperation.setCurrency(currency);
		centralizerFundOperation.setFundsOperation(issuerFundosOperation);
		centralizerFundOperation.setIndBcrdPayback(BooleanType.NO.getCode());

		allocationBenefitService.create(centralizerFundOperation);
		executeFundComponent(centralizerFundOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());

		
		//WE GENERATE THE HOLDER CASH MOVEMENT RELATED TO THE WITHDRAWL FUND OPERATION
		saveWithdrawlHolderMovement(centralizerFundOperation, corporativeProcessType, idHolderPK, idHolderAccountBank, 
				idHolderBank, holderAccountBank.getBankAccountNumber(), holderAccountBank.getBankAccountType(),holderName, 
				documentNumber,idHolderAccountPK, processType, 
				idParticipantPk, idParticipantBank, issuerIdInstitutionBankAccountPk);

	}

	private void generateDirectFundAllocation(Object[] allocationDetail, Long allocationID, String userName) throws ServiceException{
		
		//1		ISSUER REGISTER THE FUND OPERATION
		FundsOperation issuerOperation = new FundsOperation();
		Long idCorporative = Long.valueOf(allocationDetail[7].toString());
		CorporativeOperation corporativeOperation = allocationBenefitService.find(idCorporative,CorporativeOperation.class);
		Integer corporativeProcessType = corporativeOperation.getCorporativeEventType().getCorporativeEventType();
		ParameterTable parameterEvenType = allocationBenefitService.find(corporativeProcessType,ParameterTable.class);
		String issuer = allocationDetail[12].toString();
		Integer currency = Integer.valueOf(allocationDetail[1].toString());
		Long idHolderAccountBank = Long.valueOf(allocationDetail[6].toString());
		HolderAccountBank holderAccountBank = allocationBenefitService.find(idHolderAccountBank,HolderAccountBank.class);
		String holderDescription =  allocationDetail[3].toString();
		Long idBank = Long.valueOf(allocationDetail[0].toString());
		
		Long holderPK = (allocationDetail[2]!=null)?Long.valueOf(allocationDetail[2].toString()):0L;
		
		String documentNumber = allocationDetail[5].toString();
		Long holderAccountPK = new Long(allocationDetail[5].toString());
		Integer processType = Integer.parseInt(allocationDetail[5].toString());

		BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
		BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
		BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
		BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));

		InstitutionCashAccount benefitCashAccount = allocationBenefitService.getCentralizerActivateBenefitCashAccount(issuer,currency);
		issuerOperation.setInstitutionCashAccount(benefitCashAccount);

//		issuerOperation.setWithdrawlSwiftBic(holderAccountBank.getBicCode());
		issuerOperation.setHolderDescription(holderDescription);
		Bank bank = receptionService.find(idBank,Bank.class);
		issuerOperation.setBank(bank);
		issuerOperation.setBankAccountNumber(holderAccountBank.getBankAccountNumber());
		issuerOperation.setIndCentralized(BooleanType.NO.getCode());
		issuerOperation.setWithdrawlSwiftBic(bank.getBicCode());

		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), 
				FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());

		issuerOperation.setFundOperationNumber(correlative);

		BenefitPaymentAllocation benefitPaymentAllocation = allocationBenefitService.find(allocationID,BenefitPaymentAllocation.class);
		issuerOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		issuerOperation.setOperationAmount(processAmount);
		issuerOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		issuerOperation.setIndAutomatic(BooleanType.NO.getCode());
		issuerOperation.setRegistryUser(userName);
		issuerOperation.setRegistryDate(CommonsUtilities.currentDate());
		issuerOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());

		Long fundOperationType = validatecentralizerFundOperationtype(corporativeProcessType,true);
		issuerOperation.setFundsOperationType(fundOperationType);

//		String trn = receptionService.getTrn(fundOperationType,currency);
//		if(trn!=null){
//			issuerOperation.setPaymentReference(trn.concat(".").concat(correlative.toString()));
//		}else{
			issuerOperation.setPaymentReference(("FIELD_NOT_EXIST.").concat(correlative.toString()));
//		}
		
		issuerOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		issuerOperation.setCurrency(currency);
		issuerOperation.setCorporativeDescription(parameterEvenType.getDescription());
		issuerOperation.setIndBcrdPayback(BooleanType.NO.getCode());

		allocationBenefitService.create(issuerOperation);
		executeFundComponent(issuerOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());

		//2		REGISTER CENTRALIZE FUND OPERATION
		FundsOperation centralizerFundOperation = new FundsOperation();
		InstitutionCashAccount centralCashAccount = receptionService.getActivatedCentralizingAccount(currency);
		centralizerFundOperation.setInstitutionCashAccount(centralCashAccount);
		centralizerFundOperation.setIndCentralized(BooleanType.YES.getCode());
		centralizerFundOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		centralizerFundOperation.setOperationAmount(processAmount);
		centralizerFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralizerFundOperation.setIndAutomatic(BooleanType.NO.getCode());
		centralizerFundOperation.setRegistryUser(userName);
		centralizerFundOperation.setRegistryDate(CommonsUtilities.currentDate());
		centralizerFundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralizerFundOperation.setFundsOperationType(fundOperationType);
		centralizerFundOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		centralizerFundOperation.setCurrency(currency);
		centralizerFundOperation.setCorporativeDescription(parameterEvenType.getDescription());
		centralizerFundOperation.setFundsOperation(issuerOperation);
		centralizerFundOperation.setIndBcrdPayback(BooleanType.NO.getCode());

		allocationBenefitService.create(centralizerFundOperation);
		executeFundComponent(centralizerFundOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		//swiftWithdrawl.executeSwiftWithdrawl(issuerOperation,null,null);

		//WE GENERATE THE HOLDER CASH MOVEMENT RELATED TO THE WITHDRAWL FUND OPERATION
		saveWithdrawlHolderMovement(centralizerFundOperation,corporativeProcessType,holderPK,idHolderAccountBank,idBank,holderAccountBank.getBankAccountNumber(),
				holderAccountBank.getBankAccountType(),holderDescription,documentNumber,holderAccountPK,processType);

	}

	/**
	 * Metodo que retorna lista de procesos de consignacion de pagos INDIRECTA.
	 *
	 * @param lstIndirectPayment the lst indirect payment
	 * @return the list
	 */
	

	private List<AllocationPaymentProcessBankTO> processPayment(List<Object[]> lstIndirectPayment){
		
		return null;
	}
	
	private List<AllocationPaymentProcessBankTO> processIndirectPayment(List<Object[]> lstIndirectPayment){
		List<AllocationPaymentProcessBankTO> lstFileBank = new ArrayList<AllocationPaymentProcessBankTO>();
		AllocationPaymentProcessBankTO paymentBank = new AllocationPaymentProcessBankTO();
		List<AllocationPaymentProcessHolderTO> lstHolders = new ArrayList<AllocationPaymentProcessHolderTO>();
		AllocationPaymentProcessHolderTO holderBank = new AllocationPaymentProcessHolderTO();

		Long bankPK = null; Long previousBankPK = null;
		Integer corporativeEventType = null; Integer previousCorporativeEventType = null;
		BigDecimal totalHolders = BigDecimal.ZERO;BigDecimal totalBankAmount = BigDecimal.ZERO;
		String issuer = StringUtils.EMPTY;
		Long corporativeID = null;
		boolean firstTime = true;

		if(Validations.validateListIsNotNullAndNotEmpty(lstIndirectPayment)){
			Integer currency = null;
			for(Object[] indirectPayment : lstIndirectPayment){

				currency = Integer.valueOf(indirectPayment[1].toString());
				issuer = indirectPayment[12].toString();

				Long idHolderAccountBank = Long.valueOf(indirectPayment[6].toString());
				String documentBankNumber = allocationBenefitService.getBankNumber(idHolderAccountBank);
				String holderBankAccountType = allocationBenefitService.getBankAccountType(idHolderAccountBank);

				Long holderPK = Long.valueOf(indirectPayment[2].toString());
				String fullName = indirectPayment[14].toString();

				bankPK = Long.valueOf(indirectPayment[0].toString());
				Long holderAccountPK = Long.valueOf(indirectPayment[19].toString());
				Integer processType = Integer.valueOf(indirectPayment[20].toString());

				BigDecimal grossAmount = new BigDecimal(indirectPayment[9].toString());
				BigDecimal taxAmount = new BigDecimal(indirectPayment[17].toString());
				BigDecimal custodyAmount = new BigDecimal(indirectPayment[18].toString());
				BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));

				corporativeEventType = Integer.valueOf(indirectPayment[13].toString());
				String corporativeTypeDescription = StringUtils.EMPTY;
				if(firstTime){
					corporativeTypeDescription = allocationBenefitService.getParameterDescription(corporativeEventType);
				}

				String documentNumber = indirectPayment[15].toString();

				if(Validations.validateIsNull(previousBankPK)){

					holderBank = new AllocationPaymentProcessHolderTO();

					holderBank.setAccountNumber(documentBankNumber);
					holderBank.setBankPK(bankPK);
					holderBank.setCorporativeDescription(corporativeTypeDescription);
					holderBank.setCorporativePK(corporativeID);
					holderBank.setCorporativeProcessType(corporativeEventType);
					holderBank.setDocumentNumber(documentNumber);
					holderBank.setHolderAccountBankPK(idHolderAccountBank);
					holderBank.setHolderAmount(processAmount);
					holderBank.setHolderPK(holderPK);
					holderBank.setIssuer(issuer);
					holderBank.setName(fullName);
					holderBank.setStrAccountType(holderBankAccountType);
					holderBank.setHolderAccountPK(holderAccountPK);
					holderBank.setInformacionDetalle1(GeneralConstants.N_A);
					holderBank.setInformacionDetalle2(GeneralConstants.N_A);
					holderBank.setProcessType(processType);

					lstHolders.add(holderBank);

					totalHolders = totalHolders.add(BigDecimal.ONE);
					totalBankAmount = totalBankAmount.add(processAmount);

				}else{
					if(previousBankPK.equals(bankPK)){

						if(corporativeEventType.equals(previousCorporativeEventType)){

							holderBank = new AllocationPaymentProcessHolderTO();
							holderBank.setAccountNumber(documentBankNumber);
							holderBank.setBankPK(bankPK);
							holderBank.setCorporativeDescription(corporativeTypeDescription);
							holderBank.setCorporativePK(corporativeID);
							holderBank.setCorporativeProcessType(corporativeEventType);
							holderBank.setDocumentNumber(documentNumber);
							holderBank.setHolderAccountBankPK(idHolderAccountBank);
							holderBank.setHolderAmount(processAmount);
							holderBank.setHolderPK(holderPK);
							holderBank.setIssuer(issuer);
							holderBank.setStrAccountType(holderBankAccountType);
							holderBank.setName(fullName);
							holderBank.setHolderAccountPK(holderAccountPK);
							holderBank.setInformacionDetalle1(GeneralConstants.N_A);
							holderBank.setInformacionDetalle2(GeneralConstants.N_A);
							holderBank.setProcessType(processType);

							lstHolders.add(holderBank);

							totalHolders = totalHolders.add(BigDecimal.ONE);
							totalBankAmount = totalBankAmount.add(processAmount);

						}else{
							paymentBank = new AllocationPaymentProcessBankTO();
							String bankAccountNumber = allocationBenefitService.getBankAccountNumber(previousBankPK,currency);
							paymentBank.setBankAccountNumber(bankAccountNumber);
							Bank bank = allocationBenefitService.find(previousBankPK,Bank.class);
							paymentBank.setBankBicCode(bank.getBicCode());
							paymentBank.setBankPK(bank.getIdBankPk());
							paymentBank.setCantidadDescargas(GeneralConstants.N_A);
							String bicDepositary = receptionService.getDepositaryBicCode();
							paymentBank.setCevaldomBicCode(bicDepositary);

							Long centralizerFundOperationType = validatecentralizerFundOperationtype(corporativeEventType, false);
							com.pradera.model.funds.FundsOperationType objcentralizerFundOperationType = 
									allocationBenefitService.find(centralizerFundOperationType,com.pradera.model.funds.FundsOperationType.class);
							paymentBank.setConceptoPago(objcentralizerFundOperationType.getName());
							paymentBank.setCurrency(currency);
							ParameterTable currencyParameter = allocationBenefitService.find(currency,ParameterTable.class);
							String currencystr = currencyParameter.getDescription();
							paymentBank.setCurrencyStr(currencystr);
							paymentBank.setEstadoArchivo(GeneralConstants.N_A);
							paymentBank.setFechaGeneracion(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
							paymentBank.setFechaValor(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
							paymentBank.setHoraGeneracion(CommonsUtilities.currentTime());
							paymentBank.setInformacionAdicional(GeneralConstants.N_A);
							paymentBank.setIssuer(issuer);
							paymentBank.setCorporativeProcessType(corporativeEventType);
							ParameterTable parameterEvenType = allocationBenefitService.find(corporativeEventType,ParameterTable.class);
							paymentBank.setTipo(parameterEvenType.getDescription());
							paymentBank.setLstHolders(lstHolders);
							paymentBank.setTotalRegisters(Integer.valueOf(totalHolders.toString()));
//							String trn = receptionService.getTrn(centralizerFundOperationType,currency);
//							if(trn!=null){
//								paymentBank.setTrn(trn);
//							}else{
								paymentBank.setTrn("FIELD_NOT_EXIST.");								
//							}							
							paymentBank.setUsuarioDescargo(GeneralConstants.N_A);
							paymentBank.setWithdrawlAmount(totalBankAmount);
							lstFileBank.add(paymentBank);

							totalBankAmount = BigDecimal.ZERO;
							totalHolders = BigDecimal.ZERO;

							lstHolders = new ArrayList<AllocationPaymentProcessHolderTO>();

							holderBank = new AllocationPaymentProcessHolderTO();
							holderBank.setAccountNumber(documentBankNumber);
							holderBank.setBankPK(bankPK);
							corporativeTypeDescription = allocationBenefitService.getParameterDescription(corporativeEventType);
							holderBank.setCorporativeDescription(corporativeTypeDescription);
							holderBank.setCorporativePK(corporativeID);
							holderBank.setCorporativeProcessType(corporativeEventType);
							holderBank.setDocumentNumber(documentNumber);
							holderBank.setHolderAccountBankPK(idHolderAccountBank);
							holderBank.setStrAccountType(holderBankAccountType);
							holderBank.setHolderAmount(processAmount);
							holderBank.setHolderPK(holderPK);
							holderBank.setIssuer(issuer);
							holderBank.setName(fullName);
							holderBank.setHolderAccountPK(holderAccountPK);
							holderBank.setInformacionDetalle1(GeneralConstants.N_A);
							holderBank.setInformacionDetalle2(GeneralConstants.N_A);
							holderBank.setProcessType(processType);

							lstHolders.add(holderBank);

							totalBankAmount = totalBankAmount.add(processAmount);
							totalHolders = totalHolders.add(BigDecimal.ONE);
						}

					}else{

						paymentBank = new AllocationPaymentProcessBankTO();
						String bankAccountNumber = allocationBenefitService.getBankAccountNumber(previousBankPK,currency);
						paymentBank.setBankAccountNumber(bankAccountNumber);
						Bank bank = allocationBenefitService.find(previousBankPK,Bank.class);
						paymentBank.setBankBicCode(bank.getBicCode());
						paymentBank.setBankPK(bank.getIdBankPk());
						paymentBank.setCantidadDescargas(GeneralConstants.N_A);
						String bicCevaldom = receptionService.getDepositaryBicCode();
						paymentBank.setCevaldomBicCode(bicCevaldom);
						Long centralizerFundOperationType = validatecentralizerFundOperationtype(corporativeEventType, false);
						com.pradera.model.funds.FundsOperationType objcentralizerFundOperationType = 
								allocationBenefitService.find(centralizerFundOperationType,com.pradera.model.funds.FundsOperationType.class);
						paymentBank.setConceptoPago(objcentralizerFundOperationType.getName());
						paymentBank.setCurrency(currency);
						ParameterTable currencyParameter = allocationBenefitService.find(currency,ParameterTable.class);
						String currencystr = currencyParameter.getDescription();
						paymentBank.setCurrencyStr(currencystr);
						paymentBank.setEstadoArchivo(GeneralConstants.N_A);
						paymentBank.setFechaGeneracion(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
						paymentBank.setFechaValor(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
						paymentBank.setHoraGeneracion(CommonsUtilities.currentTime());
						paymentBank.setInformacionAdicional(GeneralConstants.N_A);
						paymentBank.setIssuer(issuer);
						paymentBank.setCorporativeProcessType(corporativeEventType);
						ParameterTable parameterEvenType = allocationBenefitService.find(corporativeEventType,ParameterTable.class);
						paymentBank.setTipo(parameterEvenType.getDescription());
						paymentBank.setLstHolders(lstHolders);
						paymentBank.setTotalRegisters(Integer.valueOf(totalHolders.toString()));
//						String trn = receptionService.getTrn(centralizerFundOperationType,currency);
//						if(trn!=null){
//							paymentBank.setTrn(trn);
//						}else{
							paymentBank.setTrn("FIELD_NOT_EXIST.");								
//						}	
						paymentBank.setUsuarioDescargo(GeneralConstants.N_A);
						paymentBank.setWithdrawlAmount(totalBankAmount);
						lstFileBank.add(paymentBank);

						totalBankAmount = BigDecimal.ZERO;
						totalHolders = BigDecimal.ZERO;

						lstHolders = new ArrayList<AllocationPaymentProcessHolderTO>();

						holderBank = new AllocationPaymentProcessHolderTO();
						holderBank.setAccountNumber(documentBankNumber);
						holderBank.setBankPK(bankPK);
						holderBank.setCorporativeDescription(corporativeTypeDescription);
						holderBank.setCorporativePK(corporativeID);
						holderBank.setCorporativeProcessType(corporativeEventType);
						holderBank.setDocumentNumber(documentNumber);
						holderBank.setFundOperationType(validatecentralizerFundOperationtype(corporativeEventType,false));
						holderBank.setHolderAccountBankPK(idHolderAccountBank);
						holderBank.setHolderAmount(processAmount);
						holderBank.setHolderPK(holderPK);
						holderBank.setIssuer(issuer);
						holderBank.setStrAccountType(holderBankAccountType);
						holderBank.setName(fullName);
						holderBank.setHolderAccountPK(holderAccountPK);
						holderBank.setInformacionDetalle1(GeneralConstants.N_A);
						holderBank.setInformacionDetalle2(GeneralConstants.N_A);
						holderBank.setProcessType(processType);

						lstHolders.add(holderBank);

						totalBankAmount = totalBankAmount.add(processAmount);
						totalHolders = totalHolders.add(BigDecimal.ONE);
					}
				}
				previousBankPK = bankPK;
				previousCorporativeEventType = corporativeEventType;
			}
			//LAST REGISTER FOR LAST PAYMENT
			paymentBank = new AllocationPaymentProcessBankTO();
			String bankAccountNumber = allocationBenefitService.getBankAccountNumber(previousBankPK,currency);
			paymentBank.setBankAccountNumber(bankAccountNumber);
			Bank bank = allocationBenefitService.find(previousBankPK,Bank.class);
			paymentBank.setBankBicCode(bank.getBicCode());
			paymentBank.setBankPK(bank.getIdBankPk());
			paymentBank.setCantidadDescargas(GeneralConstants.N_A);
//			String bicDepositary = receptionService.getDepositaryBicCode();
			
			String bicDepositary = "BIC";
			paymentBank.setCevaldomBicCode(bicDepositary);
			Long centralizerFundOperationType = validatecentralizerFundOperationtype(corporativeEventType, false);
			com.pradera.model.funds.FundsOperationType objcentralizerFundOperationType = 
					allocationBenefitService.find(centralizerFundOperationType,com.pradera.model.funds.FundsOperationType.class);
			paymentBank.setConceptoPago(objcentralizerFundOperationType.getName());
			paymentBank.setCurrency(currency);
			ParameterTable currencyParameter = allocationBenefitService.find(currency,ParameterTable.class);
			String currencystr = currencyParameter.getDescription();
			paymentBank.setCurrencyStr(currencystr);
			paymentBank.setEstadoArchivo(GeneralConstants.N_A);
			paymentBank.setFechaGeneracion(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			paymentBank.setFechaValor(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
			paymentBank.setHoraGeneracion(CommonsUtilities.currentTime());
			paymentBank.setInformacionAdicional(GeneralConstants.N_A);
			paymentBank.setCorporativeProcessType(corporativeEventType);
			ParameterTable parameterEvenType = allocationBenefitService.find(corporativeEventType,ParameterTable.class);
			paymentBank.setTipo(parameterEvenType.getDescription());
			paymentBank.setLstHolders(lstHolders);
			paymentBank.setTotalRegisters(Integer.valueOf(totalHolders.toString()));
//			String trn = receptionService.getTrn(centralizerFundOperationType,currency);
//			if(trn!=null){
//				paymentBank.setTrn(trn);
//			}else{
				paymentBank.setTrn("FIELD_NOT_EXIST.");								
//			}	
			paymentBank.setIssuer(issuer);
			paymentBank.setUsuarioDescargo(GeneralConstants.N_A);
			paymentBank.setWithdrawlAmount(totalBankAmount);
			lstFileBank.add(paymentBank);
		}
		return lstFileBank;
	}

	/**
	 * Metodo que registra operacion de fondo para cuenta efectivo centralizadora, 
	 * ejecuta componente de movimientos de fondos, 
	 * ademas retorna lista de consignaciones procesadas
	 *
	 * @param lstProcessBank the lst process bank
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param userName the user name
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<AllocationPaymentProcessBankTO> generateCentralFundOperations(List<AllocationPaymentProcessBankTO> lstProcessBank,
				BenefitPaymentAllocation benefitPaymentAllocation, String userName) throws ServiceException{

		List<AllocationPaymentProcessBankTO> lstProcessedBank = new ArrayList<AllocationPaymentProcessBankTO>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstProcessBank)){
			for(AllocationPaymentProcessBankTO bankWithdrawl : lstProcessBank){

				//GENERATE THE FUND OPERATION FOR INSTITUTIONS, EXECUTE COMPONENT AND GENERATE THE SWIFT FUND WITHDRAWL
				Integer corporativeType = bankWithdrawl.getCorporativeProcessType();
				FundsOperation centralFundOperation = generateBankFundOperation(bankWithdrawl,benefitPaymentAllocation,true,userName,corporativeType);
				Long fundOperationType = validateFundOperationtype(corporativeType,false);
				String fundOperationTypeDescription = getFundOperationTypeDescription(corporativeType,false);
				centralFundOperation.setFundsOperationType(fundOperationType);
				allocationBenefitService.create(centralFundOperation);
				
				bankWithdrawl.setIdCentralFundOperation(centralFundOperation.getIdFundsOperationPk());
				centralFundOperation.setCorporativeDescription(fundOperationTypeDescription);
				executeFundComponent(centralFundOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
				//swiftWithdrawl.executeSwiftWithdrawl(centralFundOperation,null,null);

				//WE SET THE CORRELATIVE IN THE TRN WHICH WILL BE SET IN THE XML
				bankWithdrawl.setNombreLote("PAGOSUP" + centralFundOperation.getIdFundsOperationPk().toString());

				//WE GENERATE THE HOLDER CASH MOVEMENT BY EACH HOLDER BY BANK
				List<AllocationPaymentProcessHolderTO> lstPaymentHolders = bankWithdrawl.getLstHolders();
				List<AllocationPaymentProcessHolderTO> lstProcessedHolders = new ArrayList<AllocationPaymentProcessHolderTO>();
				if(Validations.validateListIsNotNullAndNotEmpty(lstPaymentHolders)){
					for(AllocationPaymentProcessHolderTO holderPayment : lstPaymentHolders){
						//WE GENERATE THE HOLDER CASH MOVEMENT FOR INDIRECT PAYMENTS BY BANK
						Long holderAccountMovement = saveWithdrawlHolderMovement(centralFundOperation,holderPayment.getCorporativeProcessType(),
								holderPayment.getHolderPK(),holderPayment.getHolderAccountBankPK(),holderPayment.getBankPK(),holderPayment.getAccountNumber(),
								holderPayment.getAccountType(),holderPayment.getName(),holderPayment.getDocumentNumber(),holderPayment.getHolderAccountPK(),
								holderPayment.getProcessType());

						holderPayment.setHolderAccountMovement(holderAccountMovement);
						lstProcessedHolders.add(holderPayment);
					}
				}

				bankWithdrawl.setLstHolders(lstProcessedHolders);
				lstProcessedBank.add(bankWithdrawl);
			}
		}
		return lstProcessedBank;
	}

	/**
	 * Metodo que registra operacion de fondo para el emisor,
	 * ademas ejecuta el componente de movimiento de fondos.
	 *
	 * @param lstProcessIssuer the lst process issuer
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param user the user
	 * @throws ServiceException the service exception
	 */
	private void generateIssuerFundOperations(List<AllocationPaymentProcessIssuerTO> lstProcessIssuer, BenefitPaymentAllocation benefitPaymentAllocation,String user) 
			throws ServiceException{

		if(Validations.validateListIsNotNullAndNotEmpty(lstProcessIssuer)){
			for(AllocationPaymentProcessIssuerTO issuerWithdrawl : lstProcessIssuer){

				//GENERATE THE FUND OPERATION FOR INSTITUTIONS, EXECUTE COMPONENT AND GENERATE THE SWIFT FUND WITHDRAWL
				Integer corporativeType = issuerWithdrawl.getCorporativeEventType();
				FundsOperation issuerFundOperation = generateIssuerFundOperation(issuerWithdrawl,benefitPaymentAllocation,user);
				Long fundOperationType = validateFundOperationtype(corporativeType,false);
				issuerFundOperation.setFundsOperationType(fundOperationType);
				allocationBenefitService.create(issuerFundOperation);
				executeFundComponent(issuerFundOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
			}
		}
	}

	/**
	 * Metodo que registra operacion de fondo para los bancos,
	 * ademas ejecuta el componente de movimiento de fondos.
	 *
	 * @param bankWithdrawl the bank withdrawl
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param isCentral the is central
	 * @param user the user
	 * @param corporativeType the corporative type
	 * @return the funds operation
	 */
	private FundsOperation generateBankFundOperation(AllocationPaymentProcessBankTO bankWithdrawl,BenefitPaymentAllocation benefitPaymentAllocation, 
													boolean isCentral, String user, Integer corporativeType){

		FundsOperation fundOperation = new FundsOperation();

		InstitutionCashAccount centralCashAccount = receptionService.getActivatedCentralizingAccount(bankWithdrawl.getCurrency());
		fundOperation.setInstitutionCashAccount(centralCashAccount);
		fundOperation.setIndCentralized(BooleanType.YES.getCode());

		//WE SET THE CORRELATIVE IN FUNDS OPERATION NUMBER IN ORDER TO THEN GET THE CORRELATIVE AND ADD IT IN THE TRN - XML
		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), 
				FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());

		fundOperation.setFundOperationNumber(correlative);
		fundOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
		fundOperation.setOperationAmount(bankWithdrawl.getWithdrawlAmount());
		fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundOperation.setIndAutomatic(BooleanType.NO.getCode());
		fundOperation.setRegistryUser(user);
		fundOperation.setRegistryDate(CommonsUtilities.currentDate());
		fundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		fundOperation.setCurrency(bankWithdrawl.getCurrency());
		fundOperation.setIndBcrdPayback(BooleanType.NO.getCode());
		fundOperation.setBank(new Bank());
		fundOperation.getBank().setIdBankPk(bankWithdrawl.getBankPK());
		fundOperation.getBank().setBicCode(bankWithdrawl.getBankBicCode());
		fundOperation.setBankAccountNumber(bankWithdrawl.getBankAccountNumber());
		fundOperation.setPaymentReference(bankWithdrawl.getTrn().concat(GeneralConstants.DOT.concat(correlative.toString())));

		return fundOperation;
	}

	/**
	 * Metodo que retorna movimiento de fondos para el emisor.
	 *
	 * @param issuerWithdrawl the issuer withdrawl
	 * @param allocation the allocation
	 * @param user the user
	 * @return the funds operation
	 */
	private FundsOperation generateIssuerFundOperation(AllocationPaymentProcessIssuerTO issuerWithdrawl,BenefitPaymentAllocation allocation,String user){

		FundsOperation fundOperation = new FundsOperation();

		InstitutionCashAccount benefitCashAccount = allocationBenefitService.getActiveCentralizerBenefitPaymentIssuerCashAccount(issuerWithdrawl.getIssuer(),allocation.getCurrency());
		fundOperation.setInstitutionCashAccount(benefitCashAccount);

		fundOperation.setIndCentralized(BooleanType.NO.getCode());
		ParameterTable parameter = receptionService.find(issuerWithdrawl.getCorporativeEventType(),ParameterTable.class);
		fundOperation.setCorporativeDescription(parameter.getParameterName());

		fundOperation.setBenefitPaymentAllocation(allocation);
		fundOperation.setOperationAmount(issuerWithdrawl.getWithdrawlAmount());
		fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundOperation.setIndAutomatic(BooleanType.NO.getCode());
		fundOperation.setRegistryUser(user);
		fundOperation.setRegistryDate(CommonsUtilities.currentDate());
		fundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		fundOperation.setCurrency(allocation.getCurrency());
		fundOperation.setIndBcrdPayback(BooleanType.NO.getCode());

		fundOperation.setIssuer(new Issuer());
		fundOperation.getIssuer().setIdIssuerPk(issuerWithdrawl.getIssuer());
		
		return fundOperation;
	}

	/**
	 * Metodo que retorna el tipo de operacion de fondo para la consignacion respectiva.
	 *
	 * @param corporativeProcessType the corporative process type
	 * @param participantFundOperation the participant fund operation
	 * @return the long
	 */
	private Long validateFundOperationtype(Integer corporativeProcessType, boolean participantFundOperation){
		Long fundOperationType = null;
		if(ImportanceEventType.CASH_DIVIDENDS.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.REMANENT_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode();
			}
		}else if(ImportanceEventType.EARLY_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				//fundOperationType = FundsOperationType.
			}else{
				//fundOperationType = FundsOperationType.
			}
		}
		return fundOperationType;
	}

	/**
	 * Metodo que genera archivo xml de las consignaciones de pago.
	 *
	 * @param allocationPaymentBank the allocation payment bank
	 * @return the string
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String generateXMLFile(AllocationPaymentProcessBankTO allocationPaymentBank) throws BeanIOConfigurationException, IOException{
		String message = StringUtils.EMPTY;
		InputStream inputFile = Thread.currentThread().getContextClassLoader().getResourceAsStream(GeneralConstants.ALLOCATION_HOLDER_PAYMENT_FORMAT);
		StringWriter sw = new StringWriter();
		BeanIOUtils.getInstance().load(inputFile);
		BeanWriter out = BeanIOUtils.getInstance().createWriter("archivo", sw);
		try {
			out.write(allocationPaymentBank);
			List<AllocationPaymentProcessHolderTO> lstHolder = allocationPaymentBank.getLstHolders();
			for (AllocationPaymentProcessHolderTO holderProcess: lstHolder) {
				out.write(holderProcess);
			}
			out.flush();
	        out.close();
	        Source xmlInput = new StreamSource(new StringReader(sw.toString()));
			StreamResult xmlOutput = new StreamResult(new StringWriter());
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(xmlInput, xmlOutput);

			message = xmlOutput.getWriter().toString();
		} catch (BeanIOConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return message;
	}

	/**
	 * Metodo que registra el deposito de la retencion por diferentes motivos.
	 *
	 * @param lstBlockHolderWithdrawl the lst block holder withdrawl
	 * @param withdrawlFundOperation the withdrawl fund operation
	 * @param benefitPayment the benefit payment
	 * @param userName the user name
	 * @throws ServiceException the service exception
	 */
	private void generateDevolutionByFundOperationType(List<AllocationPaymentProcessHolderTO> lstBlockHolderWithdrawl, FundsOperation withdrawlFundOperation, 
			BenefitPaymentAllocation benefitPayment, String userName) throws ServiceException{

		BigDecimal totalBlockHolders = BigDecimal.ZERO;
		BigDecimal totalWOBank = BigDecimal.ZERO;
		BigDecimal totalBlockBenefit = BigDecimal.ZERO;

		BigDecimal availableCentralAmount = BigDecimal.ZERO;
		BigDecimal retentionCentralAmount = BigDecimal.ZERO;

		List<AllocationPaymentProcessHolderTO> lstBenefitCentralWithdrawl = new ArrayList<AllocationPaymentProcessHolderTO>();
		List<AllocationPaymentProcessHolderTO> lstRetentionCentralWithdrawl = new ArrayList<AllocationPaymentProcessHolderTO>();

		for(AllocationPaymentProcessHolderTO allocationPayProcessHol:lstBlockHolderWithdrawl){

			String issuer = allocationPayProcessHol.getIssuer();
			if(idIssuerBC.equals(issuer)){
				Long fundOperationType = allocationPayProcessHol.getFundOperationType();
				if(	ParameterFundsOperationType.BLOCK_HOLDER_DEPOSIT_RETENTION.getCode().equals(fundOperationType) || 
					ParameterFundsOperationType.DEPOSIT_HOLDER_WITHOUT_BANK_ACCOUNT.getCode().equals(fundOperationType) || 
					ParameterFundsOperationType.BAN_DEPOSIT_RETENTION.getCode().equals(fundOperationType))
				{
					lstRetentionCentralWithdrawl.add(allocationPayProcessHol);
					retentionCentralAmount = retentionCentralAmount.add(allocationPayProcessHol.getHolderAmount());
				}else
				{
					lstBenefitCentralWithdrawl.add(allocationPayProcessHol);
					availableCentralAmount = availableCentralAmount.add(allocationPayProcessHol.getHolderAmount());
				}
			}else{
				if(ParameterFundsOperationType.BLOCK_HOLDER_DEPOSIT_RETENTION.getCode().equals(allocationPayProcessHol.getFundOperationType())){
					totalBlockHolders = totalBlockHolders.add(allocationPayProcessHol.getHolderAmount());
				}else if(ParameterFundsOperationType.DEPOSIT_HOLDER_WITHOUT_BANK_ACCOUNT.getCode().equals(allocationPayProcessHol.getFundOperationType())){
					totalWOBank = totalWOBank.add(allocationPayProcessHol.getHolderAmount());
				}else if(ParameterFundsOperationType.BAN_DEPOSIT_RETENTION.getCode().equals(allocationPayProcessHol.getFundOperationType())){
					totalBlockBenefit = totalBlockBenefit.add(allocationPayProcessHol.getHolderAmount());
				}
			}
		}

		InstitutionBankAccount instBankAccount = allocationBenefitService.getDevolutionCommercialBankAccount(withdrawlFundOperation.getCurrency());
		InstitutionCashAccount objInstitutionCashAccount= allocationBenefitService.getDevolutionInstitutionCashAccount(instBankAccount.getIdInstitutionBankAccountPk());
		FundsOperation blockHoldersFundsOperation = null;
		FundsOperation blockBenefitFundsOperation = null;
		FundsOperation blockWOBankFundsOperation = null;

		//WE EXECUTE THE FUND ENTER BY NORMAL ISSUER TO COMMERCIAL BANK ACCOUNT
		if(totalBlockHolders.compareTo(BigDecimal.ZERO) == 1){
			blockHoldersFundsOperation = fillFundsOperationForBlocks(instBankAccount, withdrawlFundOperation, objInstitutionCashAccount);		
			blockHoldersFundsOperation.setFundsOperationType(ParameterFundsOperationType.BLOCK_HOLDER_DEPOSIT_RETENTION.getCode());			
			blockHoldersFundsOperation.setOperationAmount(totalBlockHolders);
			allocationBenefitService.create(blockHoldersFundsOperation);
			executeFundComponent(blockHoldersFundsOperation, BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode());
		}

		if(totalBlockBenefit.compareTo(BigDecimal.ZERO) == 1){
			blockBenefitFundsOperation = fillFundsOperationForBlocks(instBankAccount, withdrawlFundOperation, objInstitutionCashAccount);
			blockBenefitFundsOperation.setFundsOperationType(ParameterFundsOperationType.BAN_DEPOSIT_RETENTION.getCode());
			blockBenefitFundsOperation.setOperationAmount(totalBlockBenefit);
			allocationBenefitService.create(blockBenefitFundsOperation);
			executeFundComponent(blockBenefitFundsOperation, BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode());
		}
		if(totalWOBank.compareTo(BigDecimal.ZERO) ==1){
			blockWOBankFundsOperation = fillFundsOperationForBlocks(instBankAccount, withdrawlFundOperation, objInstitutionCashAccount);
			blockWOBankFundsOperation.setFundsOperationType(ParameterFundsOperationType.DEPOSIT_HOLDER_WITHOUT_BANK_ACCOUNT.getCode());
			blockWOBankFundsOperation.setOperationAmount(totalWOBank);
			allocationBenefitService.create(blockWOBankFundsOperation);
			executeFundComponent(blockWOBankFundsOperation, BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode());
		}

		//GENERATE THE FUND WITHDRAWL FOR CENTRAL ISSUER AVAILABLE PAYMENTS
		if(availableCentralAmount.compareTo(BigDecimal.ZERO) ==1)
		{
			FundsOperation centralFundOperation = generateCentralBlockFundWithdrawl(availableCentralAmount,benefitPayment,userName,false);
			saveCentralHolderCashMovement(lstBenefitCentralWithdrawl,centralFundOperation);
		}
		//GENERATE THE FUND WITHDRAWL FOR CENTRAL ISSUER RETENTION
		if(retentionCentralAmount.compareTo(BigDecimal.ZERO) ==1)
		{
			FundsOperation centralFundOperation = generateCentralBlockFundWithdrawl(retentionCentralAmount,benefitPayment,userName,true);
			saveCentralHolderCashMovement(lstRetentionCentralWithdrawl,centralFundOperation);
		}

		for(AllocationPaymentProcessHolderTO holderAllocation : lstBlockHolderWithdrawl){
			String issuer = holderAllocation.getIssuer();
			if(issuer.equals(idIssuerBC)){
				continue;
			}else{
				HolderFundsOperation holderFundsOperation = new HolderFundsOperation();
				if(ParameterFundsOperationType.BLOCK_HOLDER_DEPOSIT_RETENTION.getCode().equals(holderAllocation.getFundOperationType())){
					holderFundsOperation.setFundsOperation(blockHoldersFundsOperation);
					com.pradera.model.funds.FundsOperationType fundOpeType = new com.pradera.model.funds.FundsOperationType();
					fundOpeType.setIdFundsOperationTypePk(blockHoldersFundsOperation.getFundsOperationType());
					holderFundsOperation.setFundsOperationType(fundOpeType);
				}else if(ParameterFundsOperationType.DEPOSIT_HOLDER_WITHOUT_BANK_ACCOUNT.getCode().equals(holderAllocation.getFundOperationType())){
					holderFundsOperation.setFundsOperation(blockWOBankFundsOperation);
					com.pradera.model.funds.FundsOperationType fundOpeType = new com.pradera.model.funds.FundsOperationType();
					fundOpeType.setIdFundsOperationTypePk(blockWOBankFundsOperation.getFundsOperationType());
					holderFundsOperation.setFundsOperationType(fundOpeType);
				}else if(ParameterFundsOperationType.BAN_DEPOSIT_RETENTION.getCode().equals(holderAllocation.getFundOperationType())){
					holderFundsOperation.setFundsOperation(blockBenefitFundsOperation);
					com.pradera.model.funds.FundsOperationType fundOpeType = new com.pradera.model.funds.FundsOperationType();
					fundOpeType.setIdFundsOperationTypePk(blockBenefitFundsOperation.getFundsOperationType());
					holderFundsOperation.setFundsOperationType(fundOpeType);					
				}else{
					continue;
				}

				holderFundsOperation.setCurrency(withdrawlFundOperation.getCurrency());
				
				if(holderAllocation.getHolderPK()!=null) 
				{
					Holder holder = allocationBenefitService.find(holderAllocation.getHolderPK(),Holder.class);
					holderFundsOperation.setHolder(holder);
				}
				
				if(holderAllocation.getHolderAccountPK()!=null) 
				{
					HolderAccount holderAccount = allocationBenefitService.find(holderAllocation.getHolderAccountPK(),HolderAccount.class);
					holderFundsOperation.setHolderAccount(holderAccount);
				}
				
				if(Validations.validateIsNotNull(holderAllocation.getHolderAccountBankPK()))
				{
					HolderAccountBank holderAccountBank = allocationBenefitService.find(holderAllocation.getHolderAccountBankPK(),HolderAccountBank.class);
					holderFundsOperation.setHolderAccountBank(holderAccountBank);
					holderFundsOperation.setBankAccountType(holderAccountBank.getBankAccountType());
				}

				holderFundsOperation.setFullNameHolder(holderAllocation.getName());
				holderFundsOperation.setHolderDocumentNumber(holderAllocation.getDocumentNumber());
				holderFundsOperation.setHolderOperationAmount(holderAllocation.getHolderAmount());
				holderFundsOperation.setHolderOperationState(HolderFundsOperationStateType.REGISTERED.getCode());
				
				allocationBenefitService.create(holderFundsOperation);

				//SAVE THE HOLDER CASH MOVEMENT
				saveHolderCashMovement(holderAllocation,holderFundsOperation.getFundsOperation());
			}

		}
	}

	/**
	 * Metodo que retorna operacion de fondo.
	 *
	 * @param instBankAccount the inst bank account
	 * @param withdrawlFundOperation the withdrawl fund operation
	 * @param objInstitutionCashAccount the obj institution cash account
	 * @return the funds operation
	 */
	private FundsOperation fillFundsOperationForBlocks(InstitutionBankAccount instBankAccount,FundsOperation withdrawlFundOperation, 
														InstitutionCashAccount objInstitutionCashAccount){
		FundsOperation fundsOperation = new FundsOperation();
		fundsOperation.setFundsOperation(withdrawlFundOperation);
		fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
		fundsOperation.setFundsOperationGroup(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode());
		fundsOperation.setFundsOperationClass(OperationClassType.RECEPTION_FUND.getCode());
		fundsOperation.setInstitutionCashAccount(objInstitutionCashAccount);
		fundsOperation.setCurrency(withdrawlFundOperation.getCurrency());
		fundsOperation.setBank(instBankAccount.getProviderBank());
		fundsOperation.setBankAccountNumber(instBankAccount.getAccountNumber());
		fundsOperation.setIndAutomatic(BooleanType.NO.getCode());
		fundsOperation.setRegistryUser(withdrawlFundOperation.getRegistryUser());
		fundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		fundsOperation.setIndCentralized(BooleanType.NO.getCode());
		fundsOperation.setIndBcrdPayback(BooleanType.NO.getCode());
		return fundsOperation;
	}

	/**
	 * Metodo que retorna lista de titulares que se le bloquearan beneficios por diferentes motivos.
	 *
	 * @param lstResult the lst result
	 * @return the list
	 */
	private List<AllocationPaymentProcessHolderTO> groupBlockHolders(List<Object[]> lstResult){
		List<AllocationPaymentProcessHolderTO> lstBlockHolderWithdrawl = new ArrayList<AllocationPaymentProcessHolderTO>();
		AllocationPaymentProcessHolderTO blockHolderWithdrawl = new AllocationPaymentProcessHolderTO();

		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			for(Object[] allocationDetail : lstResult){

				blockHolderWithdrawl = new AllocationPaymentProcessHolderTO();
				Long holderAccountPK = Long.valueOf(allocationDetail[2].toString());
				HolderAccount holder = allocationBenefitService.find(holderAccountPK,HolderAccount.class);
				Integer documentType = Integer.valueOf(allocationDetail[4].toString());
				String documentNumber = allocationDetail[5].toString();
				String fullName = allocationDetail[3].toString();

				BigDecimal grossAmount = new BigDecimal(allocationDetail[9].toString());
				BigDecimal taxAmount = new BigDecimal(allocationDetail[17].toString());
				BigDecimal custodyAmount = new BigDecimal(allocationDetail[18].toString());
				BigDecimal processAmount = grossAmount.subtract((custodyAmount.add(taxAmount)));

				Integer retentionMotive = Integer.parseInt(allocationDetail[11].toString());
				Integer corporativeEventType = Integer.parseInt(allocationDetail[13].toString());
				String issuer = allocationDetail[12].toString();
				Long allocationDetailPK = Long.valueOf(allocationDetail[16].toString());

				if(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode().equals(retentionMotive)){

					blockHolderWithdrawl.setFundOperationType(ParameterFundsOperationType.BAN_DEPOSIT_RETENTION.getCode());
					List<Long> blockOperationDetailList = allocationBenefitService.getBlockDocumentsDetail(allocationDetailPK);
					blockHolderWithdrawl.setLstBlockOperationPK(blockOperationDetailList);

				}else if(	AllocationProcessType.HOLDER_ACCOUNT_RETENTION.getCode().equals(retentionMotive) || 
							AllocationProcessType.RNT_RETENTION.getCode().equals(retentionMotive)){

					blockHolderWithdrawl.setFundOperationType(ParameterFundsOperationType.BLOCK_HOLDER_DEPOSIT_RETENTION.getCode());

				}else if(AllocationProcessType.NO_BANK_ACCOUNT_RETENTION.getCode().equals(retentionMotive)){

					blockHolderWithdrawl.setFundOperationType(ParameterFundsOperationType.DEPOSIT_HOLDER_WITHOUT_BANK_ACCOUNT.getCode());

				}

				blockHolderWithdrawl.setDocumentNumber(documentNumber);
				blockHolderWithdrawl.setHolderAmount(processAmount);
				blockHolderWithdrawl.setHolderAccountPK(holderAccountPK);
				blockHolderWithdrawl.setName(fullName);
				blockHolderWithdrawl.setIssuer(issuer);
				blockHolderWithdrawl.setCorporativeProcessType(corporativeEventType);

				lstBlockHolderWithdrawl.add(blockHolderWithdrawl);
			}
		}

		return lstBlockHolderWithdrawl;
	}

	/**
	 * Metodo que bloquea beneficios a los titulares.
	 *
	 * @param lstBlockHolderWithdrawl the lst block holder withdrawl
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param userName the user name
	 * @throws ServiceException the service exception
	 */
	private void processBlockHolder(List<AllocationPaymentProcessHolderTO> lstBlockHolderWithdrawl, BenefitPaymentAllocation benefitPaymentAllocation, String userName) 
																						throws ServiceException{

		boolean hasCentralIssuer = false;
		if(Validations.validateListIsNotNullAndNotEmpty(lstBlockHolderWithdrawl)){
			//WE GROUP AMOUNT BY ISSUER AND GET TOTAL AMOUNT TO GENERATE WITHDRAWL FUNDS
			Map<String,BigDecimal> mpIssuerAmounts = new HashMap<String, BigDecimal>();
			BigDecimal totalIssuerAmount = BigDecimal.ZERO;
			for(AllocationPaymentProcessHolderTO alloPaymentProcessHolder:lstBlockHolderWithdrawl){

				String issuer = alloPaymentProcessHolder.getIssuer();

				if(!idIssuerBC.equals(issuer)){
					totalIssuerAmount = totalIssuerAmount.add(alloPaymentProcessHolder.getHolderAmount());
					if(mpIssuerAmounts.containsKey(issuer)){
						BigDecimal currentAmount = mpIssuerAmounts.get(issuer);
						mpIssuerAmounts.put(issuer,currentAmount.add(alloPaymentProcessHolder.getHolderAmount()));
					}else{
						mpIssuerAmounts.put(issuer,alloPaymentProcessHolder.getHolderAmount());
					}
				}else{
					hasCentralIssuer = true;
				}
			}

			if(totalIssuerAmount.compareTo(BigDecimal.ZERO) == 1 || hasCentralIssuer){
				//FIRST WE GENERATE THE FUND WITHDRAWL IN CENTRAL ACCOUNT
				FundsOperation centralFundOperation = generateCentralWithdrawalFundOperation(totalIssuerAmount,benefitPaymentAllocation,userName);

				//THEN WE GENERATE THE FUND WITHDRAWL BY ISSUER FROM CASH ACCOUNT WITH ITS RESPECTIVE SWIFT MESSAGE
				generateIssuerWithdrawalFundOperation(mpIssuerAmounts,benefitPaymentAllocation,userName,centralFundOperation);

				//AFTER THAT WE MUST GENERATE THE FUND DEPOSIT COMMERCIAL BANK IN DEVOLUTION ACCOUNT - REGISTERED STATE
				generateDevolutionByFundOperationType(lstBlockHolderWithdrawl, centralFundOperation,benefitPaymentAllocation,userName);
			}
		}		
	}

	/**
	 *  Metodo que registra retiro de fondo de la cuenta efectivo pago de derecho del emisor.
	 *
	 * @param mpIssuerAmounts the mp issuer amounts
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param userName the user name
	 * @param centralFundsOperation the central funds operation
	 * @throws ServiceException the service exception
	 */
	private void generateIssuerWithdrawalFundOperation(Map<String,BigDecimal> mpIssuerAmounts, BenefitPaymentAllocation benefitPaymentAllocation,String userName,
			FundsOperation centralFundsOperation) throws ServiceException{

		//WE GENERATE ONLY FUNDS OPERATION FOR ISSUER THAT ARE NOT CENTRAL BANK
		for (Map.Entry<String,BigDecimal> entry : mpIssuerAmounts.entrySet()) {
			String issuer = entry.getKey();
			if(!idIssuerBC.equals(issuer)){
				BigDecimal issuerAmount = entry.getValue();
				Integer currency = benefitPaymentAllocation.getCurrency();
				FundsOperation benefitFundOperation = new FundsOperation();

				InstitutionCashAccount benefitCashAccount = allocationBenefitService.getCentralizerActivateBenefitCashAccount(issuer, currency);
				benefitFundOperation.setInstitutionCashAccount(benefitCashAccount);

				InstitutionBankAccount instBankAccount = allocationBenefitService.getDevolutionCommercialBankAccount(currency);
				benefitFundOperation.setBank(instBankAccount.getProviderBank());
				benefitFundOperation.setBankAccountNumber(instBankAccount.getAccountNumber());
				benefitFundOperation.setIndCentralized(BooleanType.NO.getCode());
				benefitFundOperation.setWithdrawlSwiftBic(instBankAccount.getProviderBank().getBicCode());

				//WE SET THE CORRELATIVE IN FUNDS OPERATION NUMBER IN ORDER TO THEN GET THE CORRELATIVE AND ADD IT IN THE TRN - XML
				Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), 
				FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
				benefitFundOperation.setFundOperationNumber(correlative);

				benefitFundOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
				benefitFundOperation.setOperationAmount(issuerAmount);
				benefitFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
				benefitFundOperation.setIndAutomatic(BooleanType.YES.getCode());
				benefitFundOperation.setRegistryUser(userName);
				benefitFundOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				benefitFundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
				benefitFundOperation.setFundsOperationType(ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode());
				benefitFundOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
				benefitFundOperation.setCurrency(currency);
				benefitFundOperation.setIndBcrdPayback(BooleanType.NO.getCode());
				benefitFundOperation.setFundsOperation(centralFundsOperation);
				benefitFundOperation.setIssuer(new Issuer());
				benefitFundOperation.getIssuer().setIdIssuerPk(issuer);

//				String trn = receptionService.getTrn(ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode(),currency);
//				if(trn!=null){
//					benefitFundOperation.setPaymentReference(trn.concat(".").concat(correlative.toString()));
//				}else{
					benefitFundOperation.setPaymentReference("FIELD_NOT_EXIST.");								
//				}		

				allocationBenefitService.create(benefitFundOperation);
				executeFundComponent(benefitFundOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
				//swiftWithdrawl.executeSwiftWithdrawl(benefitFundOperation,null,null);
			}
		}

	}

	/**
	 * Metodo que registra retiro de fondo de la cuenta efectivo centralizadora.
	 *
	 * @param totalAmount the total amount
	 * @param benefitPaymentAllocation the benefit payment allocation
	 * @param user the user
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	private FundsOperation generateCentralWithdrawalFundOperation(BigDecimal totalAmount,BenefitPaymentAllocation benefitPaymentAllocation, String user) 
												throws ServiceException{

		FundsOperation centralFundOperation = new FundsOperation();

		if(totalAmount.compareTo(BigDecimal.ZERO) == 1){
			Integer currency = benefitPaymentAllocation.getCurrency();
			centralFundOperation.setCurrency(currency);

			InstitutionCashAccount centralCashAccount = receptionService.getActivatedCentralizingAccount(currency);
			centralFundOperation.setInstitutionCashAccount(centralCashAccount);

			centralFundOperation.setIndCentralized(BooleanType.YES.getCode());
			centralFundOperation.setBenefitPaymentAllocation(benefitPaymentAllocation);
			centralFundOperation.setOperationAmount(totalAmount);
			centralFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
			centralFundOperation.setIndAutomatic(BooleanType.YES.getCode());
			centralFundOperation.setRegistryUser(user);
			centralFundOperation.setRegistryDate(CommonsUtilities.currentDateTime());
			centralFundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
			centralFundOperation.setFundsOperationType(ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode());
			centralFundOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
			centralFundOperation.setIndBcrdPayback(BooleanType.NO.getCode());

			allocationBenefitService.create(centralFundOperation);
			executeFundComponent(centralFundOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		}

		return centralFundOperation;
	}

	/**
	 * Metodo que registra deposito de fondo por retencion en las cuentas efectivas respectivas.
	 *
	 * @param allocationPayment the allocation payment
	 * @param lstCorporatives the lst corporatives
	 * @param excludeCommissions the exclude commissions
	 * @param userName the user name
	 * @throws Exception the exception
	 */
	private void genereteFundsOperationForServices(BenefitPaymentAllocation allocationPayment, List<Long> lstCorporatives, Integer excludeCommissions, String userName) throws Exception{

		List<Object[]> lstRetentionAmounts = allocationBenefitService.getRetentionAmounts(lstCorporatives);
		Issuer issuer = allocationBenefitService.getIssuerByBenefitPayment(allocationPayment.getIdBenefitPaymentPK());
		if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionAmounts)){
			InstitutionCashAccount instCashAccountBenefits = null, instCashAccountTax = null, instCashAccountRate = null;
			for(Object[] objAmounts:lstRetentionAmounts){
				BigDecimal commissionAmount = new BigDecimal(objAmounts[0].toString());//EMISORES
				BigDecimal taxAmount = new BigDecimal(objAmounts[1].toString());
				BigDecimal custodyAmount = new BigDecimal(objAmounts[2].toString());//CUENTAS TITULARES
				instCashAccountBenefits = allocationBenefitService.getUniqueActivateInstitutionCashAccount(AccountCashFundsType.TAX.getCode(), allocationPayment.getCurrency());//allocationBenefitService.getCentralizerActivateBenefitCashAccount(objAmounts[3].toString(), allocationPayment.getCurrency());//allocationBenefitService.getCentralizerActivateBenefitCashAccount(objAmounts[3].toString(), allocationPayment.getCurrency());
				
				//CUENTA DE TARIFAS
				/*if(BooleanType.NO.getCode().equals(excludeCommissions)){
					if(commissionAmount.compareTo(BigDecimal.ZERO) == 1){
						//Se crea el retiro por COMISIONES
						FundsOperation fundsOperation = createServiceWithdrawal(instCashAccountBenefits, allocationPayment, commissionAmount, userName,
								 							ParameterFundsOperationType.ISSUER_COMISSION_WITHDRAWL_BENEFIT_PAYMENTS.getCode());
						//Se crea el deposito por COMISIONES
						instCashAccountRate = allocationBenefitService.getCommercialInstCashAccountAutoProcess(AccountCashFundsType.TAX.getCode(),allocationPayment.getCurrency());//RATES
						createServiceDeposit(instCashAccountRate, fundsOperation,
				 								ParameterFundsOperationType.ISSUER_BENEFIT_PAYMENTS_COMMISSIONS_DEPOSIT.getCode(), 
				 								FundsOperationGroupType.RATE_FUND_OPERATION.getCode());
					}
				}*/
				//CUENTA DE IMPUESTOS
				if(taxAmount.compareTo(BigDecimal.ZERO) == 1){
					//Se crea el retiro por impuestos
					//TAX_BENEFITS_DEPOSIT_RETENTION
					FundsOperation fundsOperation = createServiceWithdrawal(instCashAccountBenefits, allocationPayment, taxAmount, userName,
														ParameterFundsOperationType.TAX_BENEFITS_WITHDRAWL_RETENTION.getCode(),
														ParameterFundsOperationType.TAX_BENEFITS_DEPOSIT_RETENTION.getCode(),
														issuer);
					//Se crea el deposito por impuestos
					/*instCashAccountTax = allocationBenefitService.getCommercialInstCashAccountAutoProcess(AccountCashFundsType.TAX.getCode(),allocationPayment.getCurrency());
					createServiceDeposit(instCashAccountTax, fundsOperation,
											ParameterFundsOperationType.TAX_BENEFITS_DEPOSIT_RETENTION.getCode(), 
											FundsOperationGroupType.TAX_FUND_OPERATION.getCode());*/
				}
				
				if(custodyAmount.compareTo(BigDecimal.ZERO) == 1){
					//Se crea el retiro por CUSTODIA
					FundsOperation fundsOperation = createServiceWithdrawal(instCashAccountBenefits, allocationPayment, custodyAmount, userName,
														ParameterFundsOperationType.CUSTODY_COMISSION_WITHDRAWL_BENEFIT_PAYMENTS.getCode(),
														ParameterFundsOperationType.CUSTODY_BENEFIT_PAYMENTS_COMMISSIONS_DEPOSIT.getCode(), 
														issuer);
					//Se crea el deposito por CUSTODIA
					/*instCashAccountRate = allocationBenefitService.getCommercialInstCashAccountAutoProcess(AccountCashFundsType.TAX.getCode(),allocationPayment.getCurrency());//RATES
					createServiceDeposit(instCashAccountRate, fundsOperation,
											ParameterFundsOperationType.CUSTODY_BENEFIT_PAYMENTS_COMMISSIONS_DEPOSIT.getCode(), 
											FundsOperationGroupType.RATE_FUND_OPERATION.getCode());*/
				}				
			}			
		}
	}

	/**
	 * Metodo que registra operacion de fondo de retiro.
	 *
	 * @param instCashAccount the inst cash account
	 * @param benPayAllocation the ben pay allocation
	 * @param amount the amount
	 * @param userName the user name
	 * @param fundsOperationTypeSource the funds operation type
	 * @return the funds operation
	 * @throws Exception the exception
	 */
	private FundsOperation createServiceWithdrawal(InstitutionCashAccount instCashAccount, BenefitPaymentAllocation benPayAllocation,
			BigDecimal amount, String userName, Long fundsOperationTypeSource,Long fundsOperationTypeTarget, Issuer issuer) throws Exception{

		InstitutionCashAccount issuerCashAccounts = allocationBenefitService.getCentralizerActivateBenefitCashAccount(issuer.getIdIssuerPk(), benPayAllocation.getCurrency());
		InstitutionCashAccount cavapyCashAccounts = allocationBenefitService.getUniqueActivateInstitutionCashAccount(AccountCashFundsType.TAX.getCode(), benPayAllocation.getCurrency());
		
		FundsOperation centralFundsOperation = new FundsOperation();
		centralFundsOperation.setInstitutionCashAccount(issuerCashAccounts);
		centralFundsOperation.setCurrency(benPayAllocation.getCurrency());
		centralFundsOperation.setOperationAmount(amount);
		centralFundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		centralFundsOperation.setRegistryUser(userName);
		centralFundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		centralFundsOperation.setIndCentralized(BooleanType.YES.getCode());
		centralFundsOperation.setBenefitPaymentAllocation(benPayAllocation);
		centralFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralFundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		centralFundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralFundsOperation.setFundsOperationType(fundsOperationTypeSource);
		allocationBenefitService.create(centralFundsOperation);
		executeFundComponent(centralFundsOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		
		FundsOperation fundsOperation = new FundsOperation();
		fundsOperation.setFundsOperation(centralFundsOperation);
		fundsOperation.setInstitutionCashAccount(cavapyCashAccounts);//allocationBenefitService.getUniqueActivateInstitutionCashAccount(AccountCashFundsType.TAX.getCode(), allocationPayment.getCurrency())
		fundsOperation.setCurrency(benPayAllocation.getCurrency());
		fundsOperation.setOperationAmount(amount);
		fundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		fundsOperation.setRegistryUser(userName);
		fundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		fundsOperation.setIndCentralized(BooleanType.NO.getCode());		
		fundsOperation.setBenefitPaymentAllocation(benPayAllocation);
		fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		fundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundsOperation.setFundsOperationType(fundsOperationTypeTarget);

		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(),
				FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
//		String trn = receptionService.getTrn(fundsOperationType,benPayAllocation.getCurrency());
//		if(trn!=null){
//			fundsOperation.setPaymentReference(trn.concat(".").concat(correlative.toString()));
//		}else{
			fundsOperation.setPaymentReference("FIELD_NOT_EXIST.");						
//		}			

		allocationBenefitService.create(fundsOperation);
		executeFundComponent(fundsOperation, BusinessProcessType.FUNDS_DEPOSIT_CONFIRM.getCode());
		//swiftWithdrawl.executeSwiftWithdrawl(fundsOperation, null, null);

		return fundsOperation;
	}
	
	private FundsOperation createServiceWithdrawal(InstitutionCashAccount instCashAccount, BenefitPaymentAllocation benPayAllocation,
			BigDecimal amount, String userName, Long fundsOperationType) throws Exception{

		InstitutionCashAccount instCashAccountBenefits = allocationBenefitService.getUniqueActivateInstitutionCashAccount(AccountCashFundsType.BENEFIT.getCode(), benPayAllocation.getCurrency());
		
		FundsOperation centralFundsOperation = new FundsOperation();
		centralFundsOperation.setInstitutionCashAccount(receptionService.getActivatedCentralizingAccount(benPayAllocation.getCurrency()));
		centralFundsOperation.setCurrency(benPayAllocation.getCurrency());
		centralFundsOperation.setOperationAmount(amount);
		centralFundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		centralFundsOperation.setRegistryUser(userName);
		centralFundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		centralFundsOperation.setIndCentralized(BooleanType.YES.getCode());
		centralFundsOperation.setBenefitPaymentAllocation(benPayAllocation);
		centralFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralFundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		centralFundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralFundsOperation.setFundsOperationType(fundsOperationType);
		allocationBenefitService.create(centralFundsOperation);
		executeFundComponent(centralFundsOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		
		FundsOperation fundsOperation = new FundsOperation();
		fundsOperation.setFundsOperation(centralFundsOperation);
		fundsOperation.setInstitutionCashAccount(instCashAccount);//allocationBenefitService.getUniqueActivateInstitutionCashAccount(AccountCashFundsType.TAX.getCode(), allocationPayment.getCurrency())
		fundsOperation.setCurrency(benPayAllocation.getCurrency());
		fundsOperation.setOperationAmount(amount);
		fundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		fundsOperation.setRegistryUser(userName);
		fundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		fundsOperation.setIndCentralized(BooleanType.NO.getCode());		
		fundsOperation.setBenefitPaymentAllocation(benPayAllocation);
		fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		fundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundsOperation.setFundsOperationType(fundsOperationType);

		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(),
				FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
//		String trn = receptionService.getTrn(fundsOperationType,benPayAllocation.getCurrency());
//		if(trn!=null){
//			fundsOperation.setPaymentReference(trn.concat(".").concat(correlative.toString()));
//		}else{
			fundsOperation.setPaymentReference("FIELD_NOT_EXIST.");						
//		}			

		allocationBenefitService.create(fundsOperation);
		executeFundComponent(fundsOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		//swiftWithdrawl.executeSwiftWithdrawl(fundsOperation, null, null);

		return fundsOperation;
	}

	/*************/
	private FundsOperation createServiceWithdrawalNotCentralizing(InstitutionCashAccount instCashAccount, BenefitPaymentAllocation benPayAllocation,
			BigDecimal amount, String userName, Long fundsOperationType) throws Exception{

		FundsOperation centralFundsOperation = new FundsOperation();
		centralFundsOperation.setInstitutionCashAccount(receptionService.getActivatedCentralizingAccount(benPayAllocation.getCurrency()));
		centralFundsOperation.setCurrency(benPayAllocation.getCurrency());
		centralFundsOperation.setOperationAmount(amount);
		centralFundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		centralFundsOperation.setRegistryUser(userName);
		centralFundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		centralFundsOperation.setIndCentralized(BooleanType.YES.getCode());
		centralFundsOperation.setBenefitPaymentAllocation(benPayAllocation);
		centralFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralFundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		centralFundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralFundsOperation.setFundsOperationType(fundsOperationType);
		allocationBenefitService.create(centralFundsOperation);
		executeFundComponent(centralFundsOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		
		FundsOperation fundsOperation = new FundsOperation();
		fundsOperation.setFundsOperation(centralFundsOperation);
		fundsOperation.setInstitutionCashAccount(instCashAccount);
		fundsOperation.setCurrency(benPayAllocation.getCurrency());
		fundsOperation.setOperationAmount(amount);
		fundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		fundsOperation.setRegistryUser(userName);
		fundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		fundsOperation.setIndCentralized(BooleanType.NO.getCode());		
		fundsOperation.setBenefitPaymentAllocation(benPayAllocation);
		fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		fundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		fundsOperation.setFundsOperationType(fundsOperationType);

		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(),
				FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
//		String trn = receptionService.getTrn(fundsOperationType,benPayAllocation.getCurrency());
//		if(trn!=null){
//			fundsOperation.setPaymentReference(trn.concat(".").concat(correlative.toString()));
//		}else{
			fundsOperation.setPaymentReference("FIELD_NOT_EXIST.");						
//		}			

		allocationBenefitService.create(fundsOperation);
		executeFundComponent(fundsOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		//swiftWithdrawl.executeSwiftWithdrawl(fundsOperation, null, null);

		return fundsOperation;
	}
	/*************/
	
	
	/**
	 * Metodo que registra operacion de fondo de deposito.
	 *
	 * @param instCashAccount the inst cash account
	 * @param fundsOperationRef the funds operation ref
	 * @param fundsOperationType the funds operation type
	 * @param fundsOperationGroup the funds operation group
	 * @throws ServiceException the service exception
	 */
	private void createServiceDeposit(InstitutionCashAccount instCashAccount, FundsOperation fundsOperationRef, 
			Long fundsOperationType, Integer fundsOperationGroup) throws ServiceException{

		FundsOperation fundsOperation = new FundsOperation();
		fundsOperation.setInstitutionCashAccount(instCashAccount);
		fundsOperation.setCurrency(fundsOperationRef.getCurrency());
		fundsOperation.setIndAutomatic(BooleanType.NO.getCode());
		fundsOperation.setRegistryUser(fundsOperationRef.getRegistryUser());
		fundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		fundsOperation.setIndCentralized(BooleanType.NO.getCode());
		fundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
		fundsOperation.setFundsOperationClass(OperationClassType.RECEPTION_FUND.getCode());
		fundsOperation.setFundsOperationGroup(fundsOperationGroup);
		fundsOperation.setFundsOperationType(fundsOperationType);
		fundsOperation.setOperationAmount(fundsOperationRef.getOperationAmount());
		allocationBenefitService.create(fundsOperation);

		executeFundComponent(fundsOperation, BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER.getCode());
	}

	/**
	 * Metodo que retorna lista de detalle de consiganciones bloqueadas DIRECTAS.
	 *
	 * @param allocationProcess the allocation process
	 * @param lstMotives the lst motives
	 * @return the block direct payment resume detail
	 */
	public List<AllocationIssuerResumeTO> getBlockDirectPaymentResumeDetail(AllocationProcessTO allocationProcess, List<Integer> lstMotives){
		List<AllocationIssuerResumeTO> lstAllocationIssuerResume = new ArrayList<AllocationIssuerResumeTO>();
		AllocationIssuerResumeTO allocationIssuerResume = null;
		List<Object[]> lstAllocationDetail = allocationBenefitService.getBlockDirectDetail(new Long(allocationProcess.getIdAllocationProcess().toString()),lstMotives);
		if(Validations.validateListIsNotNullAndNotEmpty(lstAllocationDetail)){
			for(Object[] paymentAllocationDetail : lstAllocationDetail){
				allocationIssuerResume = new AllocationIssuerResumeTO();
				allocationIssuerResume.setRntDescription(paymentAllocationDetail[0].toString() + " - " + paymentAllocationDetail[1].toString());
				Integer documentType = Integer.parseInt(paymentAllocationDetail[2].toString());
				ParameterTable parameterDocumentType = allocationBenefitService.find(documentType,ParameterTable.class);
				allocationIssuerResume.setDocumentTypeNumber(parameterDocumentType.getIndicator1() + " - " + paymentAllocationDetail[3].toString());
				if(Validations.validateIsNotNull(paymentAllocationDetail[4])){
					allocationIssuerResume.setBankAccountNumber(paymentAllocationDetail[4].toString());
				}else{
					allocationIssuerResume.setBankAccountNumber(StringUtils.EMPTY);
				}
				allocationIssuerResume.setCorporativeID(paymentAllocationDetail[5].toString());
				allocationIssuerResume.setSecurity(paymentAllocationDetail[6].toString());

				BigDecimal grossAmount = new BigDecimal(paymentAllocationDetail[7].toString());
				BigDecimal taxAmount = new BigDecimal(paymentAllocationDetail[10].toString());
				BigDecimal custodyAmount = new BigDecimal(paymentAllocationDetail[11].toString());

				BigDecimal processAmount = grossAmount.subtract((taxAmount.add(custodyAmount)));
				allocationIssuerResume.setAmount(processAmount);

				Integer motiveType = Integer.parseInt(paymentAllocationDetail[9].toString());
				ParameterTable motive = allocationBenefitService.find(motiveType,ParameterTable.class);
				allocationIssuerResume.setProcessType(motive.getDescription());

				lstAllocationIssuerResume.add(allocationIssuerResume);
			}
		}

		return lstAllocationIssuerResume;
	}

	public List<AllocationIssuerResumeTO> getPaymentResumeDetail(AllocationProcessTO allocationProcess, List<Integer> lstMotives){
		List<AllocationIssuerResumeTO> lstAllocationIssuerResume = new ArrayList<AllocationIssuerResumeTO>();
		AllocationIssuerResumeTO allocationIssuerResume = null;
		List<Object[]> lstAllocationDetail = allocationBenefitService.getPayrollDetailAllocation(allocationProcess.getIdAllocationProcess());
		if(Validations.validateListIsNotNullAndNotEmpty(lstAllocationDetail)){
			for(Object[] paymentAllocationDetail : lstAllocationDetail){
				/*
				+ " ha.ID_HOLDER_ACCOUNT_PK, "						//0
				+ " FN_GET_HOLDER_NAME(ha.ID_HOLDER_ACCOUNT_PK), "	//1
				+ " p.id_participant_pk, "							//2
				+ " p.description, ";								//3
				*/
				
				allocationIssuerResume = new AllocationIssuerResumeTO();
				allocationIssuerResume.setRntDescription((paymentAllocationDetail[1]!=null)?paymentAllocationDetail[1].toString():"");
				
				String participantDescription = (paymentAllocationDetail[3]!=null)?paymentAllocationDetail[3].toString():"";
				allocationIssuerResume.setParticipantDescription(participantDescription);
				
				if(Validations.validateIsNotNull(paymentAllocationDetail[4])){
					allocationIssuerResume.setBankAccountNumber(paymentAllocationDetail[4].toString());
				}else{
					allocationIssuerResume.setBankAccountNumber(StringUtils.EMPTY);
				}
				allocationIssuerResume.setCorporativeID(paymentAllocationDetail[5].toString());
				allocationIssuerResume.setSecurity(paymentAllocationDetail[6].toString());

				BigDecimal grossAmount = (paymentAllocationDetail[7]!=null)? new BigDecimal(paymentAllocationDetail[7].toString()) : BigDecimal.ZERO;
				BigDecimal taxAmount = (paymentAllocationDetail[10]!=null)? new BigDecimal(paymentAllocationDetail[10].toString()) : BigDecimal.ZERO;
				BigDecimal custodyAmount = (paymentAllocationDetail[11]!=null)? new BigDecimal(paymentAllocationDetail[11].toString()) : BigDecimal.ZERO;
				String bankDescription = (paymentAllocationDetail[12]!=null)?paymentAllocationDetail[12].toString():"";

				BigDecimal processAmount = grossAmount.subtract((taxAmount.add(custodyAmount)));
				allocationIssuerResume.setAmount(processAmount);
				allocationIssuerResume.setRetentionAmount(taxAmount.add(custodyAmount));
				
				Integer motiveType = Integer.parseInt(paymentAllocationDetail[9].toString());
				ParameterTable motive = allocationBenefitService.find(motiveType,ParameterTable.class);
				allocationIssuerResume.setProcessType(motive.getDescription());
				allocationIssuerResume.setBank(bankDescription);
				
				lstAllocationIssuerResume.add(allocationIssuerResume);
			}
		}

		return lstAllocationIssuerResume;
	}
	
	/**
	 * Metodo que retorna lista de detalle de consiganciones bloqueadas INDIRECTAS.
	 *
	 * @param lstIssuerResume the lst issuer resume
	 * @return the list
	 */
	private List<AllocationPaymentProcessIssuerTO> processIssuerPayment(List<Object[]> lstIssuerResume){
		List<AllocationPaymentProcessIssuerTO> lstIssuerProcess = new ArrayList<AllocationPaymentProcessIssuerTO>();
		AllocationPaymentProcessIssuerTO paymentIssuer = new AllocationPaymentProcessIssuerTO();

		Integer corporativeEventType = null; Integer previousCorporativeEventType = null;
		String issuer = StringUtils.EMPTY;String previousIssuer = StringUtils.EMPTY;

		if(Validations.validateListIsNotNullAndNotEmpty(lstIssuerResume))
		{
			for(Object[] indirectPayment : lstIssuerResume){
				issuer = indirectPayment[0].toString();
				BigDecimal amount = new BigDecimal(indirectPayment[2].toString());
				corporativeEventType = Integer.valueOf(indirectPayment[1].toString());

				if(StringUtils.EMPTY.equals(previousIssuer))
				{
					paymentIssuer.setIssuer(issuer);
					paymentIssuer.setCorporativeEventType(corporativeEventType);
					paymentIssuer.setWithdrawlAmount(amount);
				}else
				{

					if(previousIssuer.equals(issuer))
					{
						if(previousCorporativeEventType.equals(corporativeEventType)){
							paymentIssuer.setWithdrawlAmount(paymentIssuer.getWithdrawlAmount().add(amount));
						}else{
							lstIssuerProcess.add(paymentIssuer);
						}
					}else
					{
						lstIssuerProcess.add(paymentIssuer);

						paymentIssuer = new AllocationPaymentProcessIssuerTO();
						paymentIssuer.setIssuer(issuer);
						paymentIssuer.setCorporativeEventType(corporativeEventType);
						paymentIssuer.setWithdrawlAmount(amount);

					}

				}
				previousIssuer = issuer;
				previousCorporativeEventType = corporativeEventType;
			}
			lstIssuerProcess.add(paymentIssuer);

		}
		return lstIssuerProcess;
	}

	/**
	 * Metodo que registra movimiento de fondos.
	 *
	 * @param withdrawlFundOperation the withdrawl fund operation
	 * @param corporativeProcessType the corporative process type
	 * @param holderPK the holder pk
	 * @param holderAccountBankPK the holder account bank pk
	 * @param bankPK the bank pk
	 * @param bankAccountNumber the bank account number
	 * @param bankAccountType the bank account type
	 * @param fullName the full name
	 * @param documentNumber the document number
	 * @param holderAccountPK the holder account pk
	 * @param processType the process type
	 * @return the long
	 */
	private Long saveWithdrawlHolderMovement(FundsOperation withdrawlFundOperation, Integer corporativeProcessType, Long holderPK, Long holderAccountBankPK, 
			Long bankPK, String bankAccountNumber, Integer bankAccountType,String fullName, String documentNumber,Long holderAccountPK, Integer processType, 
			Long participantPK, Long participantBankPK, Long institutionBankAccountPK){

		HolderCashMovement holderCashMovement = new HolderCashMovement();
		holderCashMovement.setCurrency(withdrawlFundOperation.getCurrency());

		Long lngFundOperationType = validatecentralizerFundOperationtype(corporativeProcessType,false);
		com.pradera.model.funds.FundsOperationType fundOperationType = new com.pradera.model.funds.FundsOperationType();
		fundOperationType.setIdFundsOperationTypePk(lngFundOperationType);
		holderCashMovement.setFundsOperationType(fundOperationType);

		Participant participant = null;
		InstitutionBankAccount institutionBankAccount = null;
		Bank participantBank = null;
		Bank bank = null;
		
		if( institutionBankAccountPK!=null) {
			institutionBankAccount = new InstitutionBankAccount();
			institutionBankAccount.setIdInstitutionBankAccountPk(institutionBankAccountPK);
		}
		
		if( participantPK!=null) {
			participant = new Participant();
			participant.setIdParticipantPk(participantPK);
		}
		
		if( participantBankPK!=null ) {
			participantBank = new Bank();
			participantBank.setIdBankPk(participantBankPK);
		}
		
		if(bankPK!=null) {
			bank = new Bank();
			bank.setIdBankPk(bankPK);
		}
		
		if(holderPK!=null) {
			Holder holder = new Holder();
			holder.setIdHolderPk(holderPK);
			holderCashMovement.setHolder(holder);
		}
		
		if(holderAccountPK!=null) {
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(holderAccountPK);
			holderCashMovement.setHolderAccount(holderAccount);
		}

		HolderAccountBank holderAccountBank = null;
		if(holderAccountBankPK!=null) {
			holderAccountBank = new HolderAccountBank();
			holderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
		}

		holderCashMovement.setBank(bank);
		holderCashMovement.setHolderAmount(withdrawlFundOperation.getOperationAmount());
		holderCashMovement.setHolderAccountBank(holderAccountBank);		
		holderCashMovement.setBankAccountNumber(bankAccountNumber);
		holderCashMovement.setBankAccountType(bankAccountType);
		holderCashMovement.setFullName(fullName);
		holderCashMovement.setDocumentHolder(documentNumber);
		holderCashMovement.setProcessType(processType);
		holderCashMovement.setMovementDate(CommonsUtilities.currentDate());
		holderCashMovement.setMovementState(HolderCashMovementStateType.PAID.getCode());
		holderCashMovement.setFundsOperation(withdrawlFundOperation);
		
		holderCashMovement.setInstitutionBankAccount(institutionBankAccount);
		holderCashMovement.setParticipant(participant);
		holderCashMovement.setParticipantBank(participantBank);

		allocationBenefitService.create(holderCashMovement);

		return holderCashMovement.getIdHolderCashMovementPk();
	}
	
	private Long saveWithdrawlHolderMovement(FundsOperation withdrawlFundOperation, Integer corporativeProcessType, Long holderPK, Long holderAccountBankPK, 
			Long bankPK, String bankAccountNumber, Integer bankAccountType,String fullName, String documentNumber,Long holderAccountPK, Integer processType){

		HolderCashMovement holderCashMovement = new HolderCashMovement();
		holderCashMovement.setCurrency(withdrawlFundOperation.getCurrency());

		Long lngFundOperationType = validatecentralizerFundOperationtype(corporativeProcessType,false);
		com.pradera.model.funds.FundsOperationType fundOperationType = new com.pradera.model.funds.FundsOperationType();
		fundOperationType.setIdFundsOperationTypePk(lngFundOperationType);
		holderCashMovement.setFundsOperationType(fundOperationType);

		Bank bank = new Bank();
		bank.setIdBankPk(bankPK);
		holderCashMovement.setBank(bank);

		if(holderPK!=null) {
			Holder holder = new Holder();
			holder.setIdHolderPk(holderPK);
			holderCashMovement.setHolder(holder);
		}
		
		if(holderAccountPK!=null) {
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(holderAccountPK);
			holderCashMovement.setHolderAccount(holderAccount);
		}
		
		holderCashMovement.setHolderAmount(withdrawlFundOperation.getOperationAmount());

		HolderAccountBank holderAccountBank = new HolderAccountBank();
		holderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
		holderCashMovement.setHolderAccountBank(holderAccountBank);
		
		holderCashMovement.setBankAccountNumber(bankAccountNumber);
		holderCashMovement.setBankAccountType(bankAccountType);
		holderCashMovement.setFullName(fullName);
		holderCashMovement.setDocumentHolder(documentNumber);
		holderCashMovement.setProcessType(processType);
		holderCashMovement.setMovementDate(CommonsUtilities.currentDate());
		holderCashMovement.setMovementState(HolderCashMovementStateType.PAID.getCode());
		holderCashMovement.setFundsOperation(withdrawlFundOperation);

		allocationBenefitService.create(holderCashMovement);

		return holderCashMovement.getIdHolderCashMovementPk();
	}

	/**
	 * Metodo que registra operacio y movimientos de retiro en la cuenta centralizadora.
	 *
	 * @param centralTotalAmount the central total amount
	 * @param benefitPayment the benefit payment
	 * @param userName the user name
	 * @param blRetention the bl retention
	 * @return the funds operation
	 * @throws ServiceException the service exception
	 */
	private FundsOperation generateCentralBlockFundWithdrawl(BigDecimal centralTotalAmount, BenefitPaymentAllocation benefitPayment, String userName, boolean blRetention) 
			throws ServiceException{

		//GENERATE THE WITHDRAWL FOR CENTRAL ACCOUNT FOR CENTRAL BANK
		FundsOperation centralFundOperation = new FundsOperation();
		Integer currency = benefitPayment.getCurrency();
		centralFundOperation.setCurrency(currency);

		InstitutionCashAccount centralCashAccount = receptionService.getActivatedCentralizingAccount(currency);
		centralFundOperation.setInstitutionCashAccount(centralCashAccount);

		centralFundOperation.setIndCentralized(BooleanType.YES.getCode());
		centralFundOperation.setBenefitPaymentAllocation(benefitPayment);
		centralFundOperation.setOperationAmount(centralTotalAmount);
		centralFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		centralFundOperation.setIndAutomatic(BooleanType.YES.getCode());
		centralFundOperation.setRegistryUser(userName);
		centralFundOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		centralFundOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		centralFundOperation.setFundsOperationType(ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode());
		centralFundOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		centralFundOperation.setIndBcrdPayback(BooleanType.NO.getCode());

		Integer correlative = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode(), 
				FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		
		centralFundOperation.setPaymentReference("FIELD_NOT_EXIST.");			

		allocationBenefitService.create(centralFundOperation);
		executeFundComponent(centralFundOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());
		//swiftWithdrawl.executeSwiftWithdrawl(centralFundOperation, null, null);

		//GENERATE THE WITHDRAWL FOR BENEFIT CASH ACCOUNT FOR CENTRAL BANK
		FundsOperation issuerFundsOperation = new FundsOperation();
		issuerFundsOperation.setFundsOperation(centralFundOperation);

		InstitutionCashAccount benefitCashAccount = allocationBenefitService.getCentralizerActivateBenefitCashAccount(idIssuerBC, currency);
		issuerFundsOperation.setInstitutionCashAccount(benefitCashAccount);
		
		issuerFundsOperation.setCurrency(currency);
		issuerFundsOperation.setOperationAmount(centralTotalAmount);
		issuerFundsOperation.setIndAutomatic(BooleanType.YES.getCode());
		issuerFundsOperation.setRegistryUser(userName);
		issuerFundsOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		issuerFundsOperation.setIndCentralized(BooleanType.NO.getCode());
		issuerFundsOperation.setBenefitPaymentAllocation(benefitPayment);
		issuerFundsOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		issuerFundsOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
		issuerFundsOperation.setFundsOperationGroup(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode());
		issuerFundsOperation.setFundsOperationType(ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode());

		allocationBenefitService.create(issuerFundsOperation);
		executeFundComponent(issuerFundsOperation, BusinessProcessType.ALLOCATE_BENEFIT_PAYMENT.getCode());

		return centralFundOperation;
	}

	/**
	 * Metodo que retorna descripcion de operacion de fondo.
	 *
	 * @param corporativeProcessType the corporative process type
	 * @param participantFundOperation the participant fund operation
	 * @return the fund operation type description
	 */
	private String getFundOperationTypeDescription(Integer corporativeProcessType, boolean participantFundOperation){
		String fundOperationType = null;
		if(ImportanceEventType.CASH_DIVIDENDS.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}
		}else if(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}
		}else if(ImportanceEventType.REMANENT_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}
		}else if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}
		}else if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				fundOperationType = ParameterFundsOperationType.INSTITUTIONS_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}else{
				fundOperationType = ParameterFundsOperationType.HOLDER_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getDescription();
			}
		}else if(ImportanceEventType.EARLY_PAYMENT.getCode().equals(corporativeProcessType)){
			if(participantFundOperation){
				//fundOperationType = FundsOperationType.
			}else{
				//fundOperationType = FundsOperationType.
			}
		}
		return fundOperationType;
	}
	
}