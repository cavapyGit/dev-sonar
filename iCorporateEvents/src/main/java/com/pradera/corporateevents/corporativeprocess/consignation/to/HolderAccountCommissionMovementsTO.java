package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



// TODO: Auto-generated Javadoc
/**
 * The Class HolderAccountCommissionMovementsTO.
 * @author Pradera Technologies
 *
 */
public class HolderAccountCommissionMovementsTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The quantity. */
	private BigDecimal quantity;
	
	/** The available quantity. */
	private BigDecimal availableQuantity;
	
	/** The balance type. */
	private Integer balanceType;
	
	/** The movement date. */
	private Date movementDate;
	
	/** The movement class. */
	private Integer movementClass;
	
	/** The holder account. */
	private Long holderAccount;


	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}
	
	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * Gets the balance type.
	 *
	 * @return the balance type
	 */
	public Integer getBalanceType() {
		return balanceType;
	}
	
	/**
	 * Sets the balance type.
	 *
	 * @param balanceType the new balance type
	 */
	public void setBalanceType(Integer balanceType) {
		this.balanceType = balanceType;
	}
	
	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}
	
	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	
	/**
	 * Gets the movement class.
	 *
	 * @return the movement class
	 */
	public Integer getMovementClass() {
		return movementClass;
	}
	
	/**
	 * Sets the movement class.
	 *
	 * @param movementClass the new movement class
	 */
	public void setMovementClass(Integer movementClass) {
		this.movementClass = movementClass;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the available quantity.
	 *
	 * @return the available quantity
	 */
	public BigDecimal getAvailableQuantity() {
		return availableQuantity;
	}
	
	/**
	 * Sets the available quantity.
	 *
	 * @param availableQuantity the new available quantity
	 */
	public void setAvailableQuantity(BigDecimal availableQuantity) {
		this.availableQuantity = availableQuantity;
	}
}