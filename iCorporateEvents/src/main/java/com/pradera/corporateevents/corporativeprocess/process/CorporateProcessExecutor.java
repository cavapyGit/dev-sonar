package com.pradera.corporateevents.corporativeprocess.process;

import java.util.List;

import javax.ejb.Local;

import com.pradera.model.corporatives.CorporativeOperation;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface CorporateProcessExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/06/2014
 */
@Local
public interface CorporateProcessExecutor {
	
	/**
	 * Metodo que ejecuta proceso preliminar de un hecho de importancia.
	 *
	 * @param corpOperation the corp operation
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean executePreliminarProcess(CorporativeOperation corpOperation) throws Exception;

	/**
	 * Metodo que ejecuta proceso definitivo de un hecho de importancia.
	 *
	 * @param corpOperation the corp operation
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean executeDefinitiveProcess(CorporativeOperation corpOperation) throws Exception;
	

}
