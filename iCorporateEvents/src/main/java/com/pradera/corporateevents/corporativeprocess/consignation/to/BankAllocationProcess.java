package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;



// TODO: Auto-generated Javadoc
/**
 * The Class BankAllocationProcess.
 * @author Pradera Technologies
 *
 */
public class BankAllocationProcess implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The bank descripcion. */
	private String bankDescripcion;
	
	/** The bank amount. */
	private String bankAmount;
	
	/** The xml bank file. */
	private String xmlBankFile;
	
	/** The holder detail. */
	private HolderAllocationProcessDetailTO holderDetail;


	/**
	 * Gets the bank descripcion.
	 *
	 * @return the bank descripcion
	 */
	public String getBankDescripcion() {
		return bankDescripcion;
	}
	
	/**
	 * Sets the bank descripcion.
	 *
	 * @param bankDescripcion the new bank descripcion
	 */
	public void setBankDescripcion(String bankDescripcion) {
		this.bankDescripcion = bankDescripcion;
	}
	
	/**
	 * Gets the bank amount.
	 *
	 * @return the bank amount
	 */
	public String getBankAmount() {
		return bankAmount;
	}
	
	/**
	 * Sets the bank amount.
	 *
	 * @param bankAmount the new bank amount
	 */
	public void setBankAmount(String bankAmount) {
		this.bankAmount = bankAmount;
	}
	
	/**
	 * Gets the xml bank file.
	 *
	 * @return the xml bank file
	 */
	public String getXmlBankFile() {
		return xmlBankFile;
	}
	
	/**
	 * Sets the xml bank file.
	 *
	 * @param xmlBankFile the new xml bank file
	 */
	public void setXmlBankFile(String xmlBankFile) {
		this.xmlBankFile = xmlBankFile;
	}
	
	/**
	 * Gets the holder detail.
	 *
	 * @return the holder detail
	 */
	public HolderAllocationProcessDetailTO getHolderDetail() {
		return holderDetail;
	}
	
	/**
	 * Sets the holder detail.
	 *
	 * @param holderDetail the new holder detail
	 */
	public void setHolderDetail(HolderAllocationProcessDetailTO holderDetail) {
		this.holderDetail = holderDetail;
	}
}