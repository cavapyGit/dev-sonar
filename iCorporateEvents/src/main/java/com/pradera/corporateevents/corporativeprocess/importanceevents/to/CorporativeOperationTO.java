package com.pradera.corporateevents.corporativeprocess.importanceevents.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporativeOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class CorporativeOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
	private Long id;
	
	/** The source security code. */
	private String sourceSecurityCode;
	
	/** The target security code. */
	private String targetSecurityCode;
	
	/** The need target security. */
	private Boolean needTargetSecurity =Boolean.FALSE;
	
	/** The state. */
	private Integer state;
	
	/** The ind remanent. */
	private Integer indRemanent;
	
	/** The issuer code. */
	private String issuerCode;
	
	/** The corporative event type. */
	private Integer corporativeEventType;
	
	/** The cut off date. */
	private Date cutOffDate;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The delivery date. */
	private Date deliveryDate;
	
	/** The corporative event sate. */
	private Integer corporativeEventSate;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The first state. */
	private Integer firstState;
	
	/** The second state. */
	private Integer secondState;
	
	/** The state list. */
	private List<Integer> stateList;
	
	/** The event type list. */
	private List<Integer> eventTypeList;
	
	/** The List Security Class Excluded*/
	private List<Integer> listSecurityClassExcluded;
	
	/**
	 * Gets the state list.
	 *
	 * @return the state list
	 */
	public List<Integer> getStateList() {
		return stateList;
	}
	
	/**
	 * Sets the state list.
	 *
	 * @param stateList the new state list
	 */
	public void setStateList(List<Integer> stateList) {
		this.stateList = stateList;
	}
	
	/**
	 * Gets the event type list.
	 *
	 * @return the event type list
	 */
	public List<Integer> getEventTypeList() {
		return eventTypeList;
	}
	
	/**
	 * Sets the event type list.
	 *
	 * @param eventTypeList the new event type list
	 */
	public void setEventTypeList(List<Integer> eventTypeList) {
		this.eventTypeList = eventTypeList;
	}
	
	/**
	 * Gets the first state.
	 *
	 * @return the first state
	 */
	public Integer getFirstState() {
		return firstState;
	}
	
	/**
	 * Sets the first state.
	 *
	 * @param firstState the new first state
	 */
	public void setFirstState(Integer firstState) {
		this.firstState = firstState;
	}
	
	/**
	 * Gets the second state.
	 *
	 * @return the second state
	 */
	public Integer getSecondState() {
		return secondState;
	}
	
	/**
	 * Sets the second state.
	 *
	 * @param secondState the new second state
	 */
	public void setSecondState(Integer secondState) {
		this.secondState = secondState;
	}
	
	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}
	
	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	
	/**
	 * Gets the need target security.
	 *
	 * @return the need target security
	 */
	public Boolean getNeedTargetSecurity() {
		return needTargetSecurity;
	}
	
	/**
	 * Sets the need target security.
	 *
	 * @param needTargetSecurity the new need target security
	 */
	public void setNeedTargetSecurity(Boolean needTargetSecurity) {
		this.needTargetSecurity = needTargetSecurity;
	}	
	
	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return issuerCode;
	}
	
	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	
	/**
	 * Gets the corporative event type.
	 *
	 * @return the corporative event type
	 */
	public Integer getCorporativeEventType() {
		return corporativeEventType;
	}
	
	/**
	 * Sets the corporative event type.
	 *
	 * @param corporativeEventType the new corporative event type
	 */
	public void setCorporativeEventType(Integer corporativeEventType) {
		this.corporativeEventType = corporativeEventType;
	}
	
	/**
	 * Gets the cut off date.
	 *
	 * @return the cut off date
	 */
	public Date getCutOffDate() {
		return cutOffDate;
	}
	
	/**
	 * Sets the cut off date.
	 *
	 * @param cutOffDate the new cut off date
	 */
	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the delivery date.
	 *
	 * @return the delivery date
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	
	/**
	 * Sets the delivery date.
	 *
	 * @param deliveryDate the new delivery date
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	/**
	 * Gets the corporative event sate.
	 *
	 * @return the corporative event sate
	 */
	public Integer getCorporativeEventSate() {
		return corporativeEventSate;
	}
	
	/**
	 * Sets the corporative event sate.
	 *
	 * @param corporativeEventSate the new corporative event sate
	 */
	public void setCorporativeEventSate(Integer corporativeEventSate) {
		this.corporativeEventSate = corporativeEventSate;
	}
	
	/**
	 * Gets the corporative event.
	 *
	 * @return the corporative event
	 */
	public Integer getCorporativeEvent() {
		return corporativeEvent;
	}
	
	/**
	 * Sets the corporative event.
	 *
	 * @param corporativeEvent the new corporative event
	 */
	public void setCorporativeEvent(Integer corporativeEvent) {
		this.corporativeEvent = corporativeEvent;
	}
	
	/** The corporative event. */
	private Integer corporativeEvent;
	
	

	
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the ind remanent.
	 *
	 * @return the ind remanent
	 */
	public Integer getIndRemanent() {
		return indRemanent;
	}
	
	/**
	 * Sets the ind remanent.
	 *
	 * @param indRemanent the new ind remanent
	 */
	public void setIndRemanent(Integer indRemanent) {
		this.indRemanent = indRemanent;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the source security code.
	 *
	 * @return the source security code
	 */
	public String getSourceSecurityCode() {
		return sourceSecurityCode;
	}
	
	/**
	 * Sets the source security code.
	 *
	 * @param sourceSecurityCode the new source security code
	 */
	public void setSourceSecurityCode(String sourceSecurityCode) {
		this.sourceSecurityCode = sourceSecurityCode;
	}
	
	/**
	 * Gets the target security code.
	 *
	 * @return the target security code
	 */
	public String getTargetSecurityCode() {
		return targetSecurityCode;
	}
	
	/**
	 * Sets the target security code.
	 *
	 * @param targetSecurityCode the new target security code
	 */
	public void setTargetSecurityCode(String targetSecurityCode) {
		this.targetSecurityCode = targetSecurityCode;
	}

	/**
	 * @return the listSecurityClassExcluded
	 */
	public List<Integer> getListSecurityClassExcluded() {
		return listSecurityClassExcluded;
	}

	/**
	 * @param listSecurityClassExcluded the listSecurityClassExcluded to set
	 */
	public void setListSecurityClassExcluded(List<Integer> listSecurityClassExcluded) {
		this.listSecurityClassExcluded = listSecurityClassExcluded;
	}
	
}
