package com.pradera.corporateevents.corporativeprocess.batch;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.corporateevents.corporativeprocess.process.CorporateEventProducer;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.reports.CorporateProcessReportsSender;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.HolderAccountBalanceExp;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * Bachero que ejecuta el proceso definitivo del proceso corporativo, 
 * ademas ejecuta los reportes respectivos.
 *
 * @author PraderaTechnologies
 */
@BatchProcess(name="CorporateProcessDefinitiveBatch")
@RequestScoped
public class CorporateProcessDefinitiveBatch  implements JobExecution{

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate event producer. */
	@Inject
	CorporateEventProducer corporateEventProducer;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The corporate process facade. */
	@EJB 
	CorporateProcessServiceFacade corporateProcessFacade;
	
	/** The report sender. */
	@Inject
	CorporateProcessReportsSender reportSender;
	
	/**
	 * Inicio de Ejecucion del Bachero que ejecuta el proceso definitivo del proceso corporativo, 
     * ademas ejecuta los reportes respectivos.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		String userId = null;
		Long lngProcessId = null;
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		//obtenemos codigo de proceso corporativo y usuario quien ejecuto el bachero
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			if(processLoggerDetail.getParameterName().equals("processid")){
				lngProcessId = Long.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("userid")){
				userId = processLoggerDetail.getParameterValue();
			}	
		}
		if(userId != null && lngProcessId != null){	
			//obtenemos evento corporativo
			CorporativeOperation corpOperation = corporateProcessFacade.getCorporateEventType(lngProcessId);
			if(corpOperation != null){
				int state = corpOperation.getState();
				CorporateProcessStateType processState = CorporateProcessStateType.get(state);
				//validamos estado del evento corporativo
				if(CorporateProcessStateType.PRELIMINARY.equals(processState) 
						|| CorporateProcessStateType.DEFINITIVE_FAIL.equals(processState)){
					LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());					
					ThreadLocalContextHolder.put(CorporateProcessConstants.CORPORATE_EVENT, corpOperation.getCorporativeEventType().getCorporativeEventType());
					CorporateProcessExecutor processExecutor = corporateEventProducer.getCorporateType(corpOperation.getCorporativeEventType().getCorporativeEventType());
					try{
						//Lista para guardar en base de datos
						List <HolderAccountBalanceExp> accountBalanceExps = new ArrayList();
						
						//Si es la ultima amortizacion extraemos los saldos antes de vencer el valor
						if(corporateProcessFacade.verifyLatestCouponAmort(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getIdCorporativeOperationPk()).equals(true)){
							accountBalanceExps = corporateProcessFacade.lstHolderAccountaBalanceExp(corpOperation.getSecurities().getIdSecurityCodePk());
						}
						
						//actualizamos estado de evento corporativo (Definitivo en Proceso)
						corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.DEFINITIVE_IN_PROCESS.getCode(), loggerUser);
						//ejecutamos proceso Definitivo
						processExecutor.executeDefinitiveProcess(corpOperation);
						//actualizamos estado de evento corporativo (Definitivo)
						corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.DEFINITIVE.getCode(), loggerUser);
						corpOperation.setState(CorporateProcessStateType.DEFINITIVE.getCode());
						
						//Si existen datos en la lista persistimos en BBDD
						if(accountBalanceExps.size() > GeneralConstants.ZERO_VALUE_INTEGER){
							corporateProcessFacade.regHolderAccountBalanceExp(accountBalanceExps);
						}
						
						//este retiro no aplica
						/*
						RetirementOperation retirementOperation = corporateExecutorService.findRelationRetirementCorporative(corpOperation.getIdCorporativeOperationPk());
						//Se valida si el corporativo esta relacionado a una operacion de retiro de valores
						if(Validations.validateIsNotNullAndNotEmpty(retirementOperation)){
							boolean blAllDefinitive = true;
							for(CorporativeOperation corporativeOperation:retirementOperation.getLstCorporativeOperation()){
								if(!CorporateProcessStateType.DEFINITIVE.getCode().equals(corporativeOperation.getState())){
									blAllDefinitive = false;
									break;
								}
							}
							if(blAllDefinitive){
								retirementOperation.setState(RetirementOperationStateType.CONFIRMED.getCode());
								corporateExecutorService.updateRetirementOperation(retirementOperation, loggerUser);
							}						
						}
						*/
						try{
							//generamos e enviamos reportes
							reportSender.sendReports(corpOperation, loggerUser);
						}catch (Exception e) {
							log.error("Error in sending report for the Process "+corpOperation.getIdCorporativeOperationPk());
							log.error(e.getMessage());
						}
					}catch (Exception e) {
						try{
							corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.DEFINITIVE_FAIL.getCode(), loggerUser);
						}catch (Exception ex) {
							log.error(ex.getMessage());
						}
						log.error(e.getMessage());
						throw new RuntimeException(e);
					}
				}else{
					throw new RuntimeException("Invalied process state, can not execute the preliminary process");
				}
			}else{
				throw new RuntimeException("Invalied process id, No Corporate process registered");
			}
		}else{
			throw new RuntimeException("Invalied parameters for the Corporate process preliminary execution");
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}	
}