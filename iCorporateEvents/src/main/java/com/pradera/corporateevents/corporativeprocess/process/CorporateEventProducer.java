package com.pradera.corporateevents.corporativeprocess.process;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.model.corporatives.type.ImportanceEventType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CorporateEventProducer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/06/2014
 */
@ApplicationScoped
public class CorporateEventProducer {
	
	/** The corporate process executor. */
	@Inject
	@Any
	Instance<CorporateProcessExecutor> corporateProcessExecutor;
	
	/**
	 * Gets the corporate type.
	 *
	 * @param corporateEventType the corporate event type
	 * @return the corporate type
	 */
	public CorporateProcessExecutor getCorporateType(Integer corporateEventType){
		ImportanceEventType eventType = ImportanceEventType.get(corporateEventType);
		switch (eventType) {
		case ACTION_DIVIDENDS:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.STOCK)).get();
		case CAPITAL_AMORTIZATION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.AMORTIZATION)).get();
		case CAPITAL_REDUCTION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SPECIAL)).get();
		case CASH_DIVIDENDS:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.CASH)).get();
		case CHANGE_NOMAL_VALUE_CAP_VAR:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SPECIAL)).get();
		case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SPECIAL)).get();
		case CONVERTIBILY_BONE_TO_ACTION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SPECIAL)).get();
		case END_PREFERRED_SUSCRIPTION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SUBSCRIPTION)).get();
		case INTEREST_PAYMENT:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.CASH)).get();
		case PREFERRED_SUSCRIPTION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SUBSCRIPTION)).get();
		case REMANENT_PAYMENT:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.CASH)).get();
		case EARLY_PAYMENT:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.AMORTIZATION)).get();
		case RETURN_OF_CONTRIBUTION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.CASH)).get();
		case SECURITIES_EXCISION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SPECIAL)).get();
		case SECURITIES_EXCLUSION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SPECIAL)).get();
		case SECURITIES_FUSION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SPECIAL)).get();
		case SECURITIES_UNIFICATION:
			return corporateProcessExecutor.select(new CorporateProcessLiteral(CorporateExecutor.SPECIAL)).get();
		default:
			return null;
		}
	} 

	/**
	 * Instantiates a new corporate event producer.
	 */
	public CorporateEventProducer() {
		// TODO Auto-generated constructor stub
	}

}
