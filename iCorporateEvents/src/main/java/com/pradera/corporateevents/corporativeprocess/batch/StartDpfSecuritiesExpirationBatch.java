package com.pradera.corporateevents.corporativeprocess.batch;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;

@BatchProcess(name="StartDpfSecuritiesExpirationBatch")
public class StartDpfSecuritiesExpirationBatch implements Serializable, JobExecution {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private PraderaLogger logger;
	
	@Inject 
	ClientRestService clientRestService;
	
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	private static final int SEGMENT_PROCESS= 50;
		
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		try {
			SecurityTO objSecurityTO = new SecurityTO();
			objSecurityTO.setExpirationDate(CommonsUtilities.currentDate());
			objSecurityTO.setStateSecurity(SecurityStateType.REGISTERED.getCode());
			List<String> lstSecurities = corporateExecutorService.findSecuritiesExpiredDpf(objSecurityTO);
			if (Validations.validateListIsNotNullAndNotEmpty(lstSecurities)) {
				//we split the securities each 50
				int segmentCount=1;
				if (lstSecurities.size() > SEGMENT_PROCESS) {
					segmentCount= (lstSecurities.size()/SEGMENT_PROCESS) +1;
				}
				
				for (int i=0; i<segmentCount; ++i) {
					String strDpfSecuritiesList= GeneralConstants.EMPTY_STRING;
					
					for (int j=0; j<SEGMENT_PROCESS; ++j) {
						if (Validations.validateListIsNotNullAndNotEmpty(lstSecurities)) {
							strDpfSecuritiesList += lstSecurities.get(0) + GeneralConstants.STR_COMMA_WITHOUT_SPACE;
							lstSecurities.remove(0); // we clear the used securities
						} else {
							break;
						}
					}
					
								
					//we send the DPF expiration process
					BusinessProcess businessProcess= new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.AUTOMATIC_EXPIRATION_DPF.getCode());
					Map<String, Object> processParameters=new HashMap<String, Object>();
					processParameters.put(GeneralConstants.STR_DPF_SECURITIES_LIST, strDpfSecuritiesList);
					batchProcessServiceFacade.registerBatch(processLogger.getIdUserAccount(), businessProcess, processParameters);
				}
			}	
			//we send interest coupon expiration process
			BusinessProcess businessProcess= new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.AUTOMATIC_INTEREST_COUPON_DPF.getCode());
			batchProcessServiceFacade.registerBatch(processLogger.getIdUserAccount(), businessProcess, null);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error :::::::::::: StartDpfSecuritiesExpirationBatch ::::::::::::");			
			String messageNotification = null;
			if (ex instanceof ServiceException) {
				ServiceException se = (ServiceException) ex;
				messageNotification = PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage(), se.getParams(), new Locale("es"));
			} else {
				messageNotification = "OCURRIO UN ERROR EN EL PROCESO DE VENCIMIENTO AUTOMATICO DE VALORES DPFS";
			}						
			UserFilterTO  userFilter = new UserFilterTO();
			userFilter.setInstitutionType(InstitutionType.DEPOSITARY.getCode());
			List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);						
			notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(), lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, messageNotification, NotificationType.EMAIL);		
		}
		
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	@Override
	public boolean sendNotification() {
		return true;
	}

}
