package com.pradera.corporateevents.corporativeprocess.view.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporativeProcessDetailsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class CorporativeProcessDetailsTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant. */
	private String participant;
	
	/** The rnt. */
	private String rnt;
	
	/** The account. */
	private Integer account;
	
	/** The name. */
	private String name;
	
	/** The source total balance. */
	private BigDecimal sourceTotalBalance;
	
	/** The source available balance. */
	private BigDecimal sourceAvailableBalance;
	
	/** The source block balance. */
	private BigDecimal sourceBlockBalance;
	
	/** The source reported balance. */
	private BigDecimal sourceReportedBalance;	
	
	/** The source reporting balance. */
	private BigDecimal sourceReportingBalance;
	
	/** The destiny total balance. */
	private BigDecimal destinyTotalBalance;
	
	/** The destiny available balance. */
	private BigDecimal destinyAvailableBalance;
	
	/** The tax amount. */
	private BigDecimal taxAmount;
	
	/** The custody amount. */
	private BigDecimal custodyAmount;
	
	/** The net total balance. */
	private BigDecimal netTotalBalance;
	
	/** The net available balance. */
	private BigDecimal netAvailableBalance;
	
	/** The total blocked balance. */
	private BigDecimal totalBlockedBalance;
	
	/** The total reported balance. */
	private BigDecimal totalReportedBalance;
	
	/** The total reporting balance. */
	private BigDecimal totalReportingBalance;
	
	/** The adjusted balance. */
	private BigDecimal adjustedBalance;
	
	/** The ind balance. */
	private boolean indBalance;
	
	/** The ind tax. */
	private boolean indTax;
	
	/** The ind custody. */
	private boolean indCustody;
	
	/** The holder account pk. */
	private Long holderAccountPk;
	
	/** The holder adjustment pk. */
	private Long holderAdjustmentPk;
	
	/** The currency description. */
	private String currencyDescription;
	
	/** The details. */
	private List<CorporativeProcessDetailsTO> details;

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public String getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the participant to set
	 */
	public void setParticipant(String participant) {
		this.participant = participant;
	}

	/**
	 * Gets the rnt.
	 *
	 * @return the rnt
	 */
	public String getRnt() {
		return rnt;
	}

	/**
	 * Sets the rnt.
	 *
	 * @param rnt the rnt to set
	 */
	public void setRnt(String rnt) {
		this.rnt = rnt;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Integer getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the account to set
	 */
	public void setAccount(Integer account) {
		this.account = account;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the source total balance.
	 *
	 * @return the sourceTotalBalance
	 */
	public BigDecimal getSourceTotalBalance() {
		return sourceTotalBalance;
	}

	/**
	 * Sets the source total balance.
	 *
	 * @param sourceTotalBalance the sourceTotalBalance to set
	 */
	public void setSourceTotalBalance(BigDecimal sourceTotalBalance) {
		this.sourceTotalBalance = sourceTotalBalance;
	}

	/**
	 * Gets the source available balance.
	 *
	 * @return the sourceAvailableBalance
	 */
	public BigDecimal getSourceAvailableBalance() {
		return sourceAvailableBalance;
	}

	/**
	 * Sets the source available balance.
	 *
	 * @param sourceAvailableBalance the sourceAvailableBalance to set
	 */
	public void setSourceAvailableBalance(BigDecimal sourceAvailableBalance) {
		this.sourceAvailableBalance = sourceAvailableBalance;
	}

	/**
	 * Gets the destiny total balance.
	 *
	 * @return the destinyTotalBalance
	 */
	public BigDecimal getDestinyTotalBalance() {
		return destinyTotalBalance;
	}

	/**
	 * Sets the destiny total balance.
	 *
	 * @param destinyTotalBalance the destinyTotalBalance to set
	 */
	public void setDestinyTotalBalance(BigDecimal destinyTotalBalance) {
		this.destinyTotalBalance = destinyTotalBalance;
	}

	/**
	 * Gets the destiny available balance.
	 *
	 * @return the destinyAvailableBalance
	 */
	public BigDecimal getDestinyAvailableBalance() {
		return destinyAvailableBalance;
	}

	/**
	 * Sets the destiny available balance.
	 *
	 * @param destinyAvailableBalance the destinyAvailableBalance to set
	 */
	public void setDestinyAvailableBalance(BigDecimal destinyAvailableBalance) {
		this.destinyAvailableBalance = destinyAvailableBalance;
	}

	/**
	 * Gets the tax amount.
	 *
	 * @return the taxAmount
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	/**
	 * Sets the tax amount.
	 *
	 * @param taxAmount the taxAmount to set
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * Gets the custody amount.
	 *
	 * @return the custodyAmount
	 */
	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}

	/**
	 * Sets the custody amount.
	 *
	 * @param custodyAmount the custodyAmount to set
	 */
	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}

	/**
	 * Gets the net total balance.
	 *
	 * @return the netTotalBalance
	 */
	public BigDecimal getNetTotalBalance() {
		return netTotalBalance;
	}

	/**
	 * Sets the net total balance.
	 *
	 * @param netTotalBalance the netTotalBalance to set
	 */
	public void setNetTotalBalance(BigDecimal netTotalBalance) {
		this.netTotalBalance = netTotalBalance;
	}

	/**
	 * Gets the net available balance.
	 *
	 * @return the netAvailableBalance
	 */
	public BigDecimal getNetAvailableBalance() {
		return netAvailableBalance;
	}

	/**
	 * Sets the net available balance.
	 *
	 * @param netAvailableBalance the netAvailableBalance to set
	 */
	public void setNetAvailableBalance(BigDecimal netAvailableBalance) {
		this.netAvailableBalance = netAvailableBalance;
	}

	/**
	 * Gets the total blocked balance.
	 *
	 * @return the totalBlockedBalance
	 */
	public BigDecimal getTotalBlockedBalance() {
		return totalBlockedBalance;
	}

	/**
	 * Sets the total blocked balance.
	 *
	 * @param totalBlockedBalance the totalBlockedBalance to set
	 */
	public void setTotalBlockedBalance(BigDecimal totalBlockedBalance) {
		this.totalBlockedBalance = totalBlockedBalance;
	}

	/**
	 * Gets the adjusted balance.
	 *
	 * @return the adjustedBalance
	 */
	public BigDecimal getAdjustedBalance() {
		return adjustedBalance;
	}

	/**
	 * Sets the adjusted balance.
	 *
	 * @param adjustedBalance the adjustedBalance to set
	 */
	public void setAdjustedBalance(BigDecimal adjustedBalance) {
		this.adjustedBalance = adjustedBalance;
	}

	/**
	 * Gets the details.
	 *
	 * @return the details
	 */
	public List<CorporativeProcessDetailsTO> getDetails() {
		return details;
	}

	/**
	 * Sets the details.
	 *
	 * @param details the details to set
	 */
	public void setDetails(List<CorporativeProcessDetailsTO> details) {
		this.details = details;
	}

	/**
	 * Checks if is ind balance.
	 *
	 * @return the indBalance
	 */
	public boolean isIndBalance() {
		return indBalance;
	}

	/**
	 * Sets the ind balance.
	 *
	 * @param indBalance the indBalance to set
	 */
	public void setIndBalance(boolean indBalance) {
		this.indBalance = indBalance;
	}

	/**
	 * Checks if is ind tax.
	 *
	 * @return the indTax
	 */
	public boolean isIndTax() {
		return indTax;
	}

	/**
	 * Sets the ind tax.
	 *
	 * @param indTax the indTax to set
	 */
	public void setIndTax(boolean indTax) {
		this.indTax = indTax;
	}

	/**
	 * Checks if is ind custody.
	 *
	 * @return the indCustody
	 */
	public boolean isIndCustody() {
		return indCustody;
	}

	/**
	 * Sets the ind custody.
	 *
	 * @param indCustody the indCustody to set
	 */
	public void setIndCustody(boolean indCustody) {
		this.indCustody = indCustody;
	}

	/**
	 * Gets the holder account pk.
	 *
	 * @return the holderAccountPk
	 */
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}

	/**
	 * Sets the holder account pk.
	 *
	 * @param holderAccountPk the holderAccountPk to set
	 */
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}

	/**
	 * Gets the holder adjustment pk.
	 *
	 * @return the holderAdjustmentPk
	 */
	public Long getHolderAdjustmentPk() {
		return holderAdjustmentPk;
	}

	/**
	 * Sets the holder adjustment pk.
	 *
	 * @param holderAdjustmentPk the holderAdjustmentPk to set
	 */
	public void setHolderAdjustmentPk(Long holderAdjustmentPk) {
		this.holderAdjustmentPk = holderAdjustmentPk;
	}

	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}

	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	/**
	 * Gets the source block balance.
	 *
	 * @return the source block balance
	 */
	public BigDecimal getSourceBlockBalance() {
		return sourceBlockBalance;
	}

	/**
	 * Sets the source block balance.
	 *
	 * @param sourceBlockBalance the new source block balance
	 */
	public void setSourceBlockBalance(BigDecimal sourceBlockBalance) {
		this.sourceBlockBalance = sourceBlockBalance;
	}

	/**
	 * Gets the source reported balance.
	 *
	 * @return the source reported balance
	 */
	public BigDecimal getSourceReportedBalance() {
		return sourceReportedBalance;
	}

	/**
	 * Sets the source reported balance.
	 *
	 * @param sourceReportedBalance the new source reported balance
	 */
	public void setSourceReportedBalance(BigDecimal sourceReportedBalance) {
		this.sourceReportedBalance = sourceReportedBalance;
	}

	/**
	 * Gets the source reporting balance.
	 *
	 * @return the source reporting balance
	 */
	public BigDecimal getSourceReportingBalance() {
		return sourceReportingBalance;
	}

	/**
	 * Sets the source reporting balance.
	 *
	 * @param sourceReportingBalance the new source reporting balance
	 */
	public void setSourceReportingBalance(BigDecimal sourceReportingBalance) {
		this.sourceReportingBalance = sourceReportingBalance;
	}

	/**
	 * Gets the total reported balance.
	 *
	 * @return the total reported balance
	 */
	public BigDecimal getTotalReportedBalance() {
		return totalReportedBalance;
	}

	/**
	 * Sets the total reported balance.
	 *
	 * @param totalReportedBalance the new total reported balance
	 */
	public void setTotalReportedBalance(BigDecimal totalReportedBalance) {
		this.totalReportedBalance = totalReportedBalance;
	}

	/**
	 * Gets the total reporting balance.
	 *
	 * @return the total reporting balance
	 */
	public BigDecimal getTotalReportingBalance() {
		return totalReportingBalance;
	}

	/**
	 * Sets the total reporting balance.
	 *
	 * @param totalReportingBalance the new total reporting balance
	 */
	public void setTotalReportingBalance(BigDecimal totalReportingBalance) {
		this.totalReportingBalance = totalReportingBalance;
	}

}
