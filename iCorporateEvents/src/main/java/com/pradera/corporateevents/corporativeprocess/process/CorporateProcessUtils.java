package com.pradera.corporateevents.corporativeprocess.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.type.CorporativeEventReportType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporateProcessUtils.
 *
 * @author PraderaTechnologies
 */
public class CorporateProcessUtils {

	/**
	 * Divide.
	 *
	 * @param balance <code>BigDecimal</code>
	 * @param value <code>BigDecimal</code>
	 * @return <code>BigDecimal</code>
	 */
	public static BigDecimal divide(BigDecimal balance, BigDecimal value) {
		return divide(balance, value, 8);
	}

	/**
	 * Divide10 decimals.
	 *
	 * @param balance the balance
	 * @param value the value
	 * @return the big decimal
	 */
	public static BigDecimal divide10Decimals(BigDecimal balance, BigDecimal value) {
		return divide(balance, value, 10);
	}
	
	/**
	 * Divide.
	 *
	 * @param balance <code>BigDecimal</code>
	 * @param value <code>BigDecimal</code>
	 * @param scale <code>int<code>
	 * @return <code>BigDecimal</code>
	 */
	public static BigDecimal divide(BigDecimal balance, BigDecimal value, int scale) {
		return balance.divide(value, scale, BigDecimal.ROUND_HALF_DOWN);
	}
	
	/**
	 * Multiply.
	 *
	 * @param balance <code>BigDecimal</code>
	 * @param value <code>BigDecimal</code>
	 * @return <code>BigDecimal</code>
	 */
	public static BigDecimal multiply(BigDecimal balance, BigDecimal value) {
		return multiply(balance, value, 8);
	}

	/**
	 * Multiply10 decimals.
	 *
	 * @param balance the balance
	 * @param value the value
	 * @return the big decimal
	 */
	public static BigDecimal multiply10Decimals(BigDecimal balance, BigDecimal value) {
		return multiply(balance, value, 10);
	}

	/**
	 * Multiply.
	 *
	 * @param balance <code>BigDecimal</code>
	 * @param value <code>BigDecimal</code>
	 * @param scale <code>int<code>
	 * @return <code>BigDecimal</code>
	 */
	public static BigDecimal multiply(BigDecimal balance, BigDecimal value, int scale) {
		BigDecimal objResult = balance.multiply(value);
		return objResult.setScale(scale, BigDecimal.ROUND_HALF_DOWN);
	}
	
	/**
	 * Divide for amount.
	 *
	 * @param balance <code>BigDecimal</code>
	 * @param value <code>BigDecimal</code>
	 * @return <code>BigDecimal</code>
	 */
	public static BigDecimal divideForAmount(BigDecimal balance, BigDecimal value){
		return divide(balance, value, 2);
	}
	
	/**
	 * Multiply for amount.
	 *
	 * @param balance <code>BigDecimal</code>
	 * @param value <code>BigDecimal</code>
	 * @return <code>BigDecimal</code>
	 */
	public static BigDecimal multiplyForAmount(BigDecimal balance, BigDecimal value){
		return multiply(balance, value, 2);
	}
	
	/**
	 * Returns calculated amount based on the round flag.
	 *
	 * @param amount the amount
	 * @param factor the factor
	 * @param round the round
	 * @return the big decimal
	 */
	public static BigDecimal calculateByFactor(BigDecimal amount, BigDecimal factor,
			int round) {
		if(amount != null && factor != null){
			BigDecimal result = multiply(amount, factor);
			if(round == CorporateProcessConstants.IND_ROUND){
				result = round(result);
			}else if(round == CorporateProcessConstants.IND_FLOOR){
				result = floor(result);
			}
			return result;
		}
		return null;
	}
	
	/**
	 * Returns amount based on the round flag with remainder consideration.
	 *
	 * @param amount the amount
	 * @param factor the factor
	 * @param round the round
	 * @param remnant the remnant
	 * @return the big decimal
	 */
	public static BigDecimal calculateByFactor(BigDecimal amount, BigDecimal factor,
			int round, boolean remnant) {
		if(amount != null && factor != null){
			BigDecimal result = multiply(amount, factor);
			if(remnant){
				result = multiply(result, CorporateProcessConstants.AMOUNT_ROUND);
			}
			if(round == CorporateProcessConstants.IND_ROUND){
				result = round(result);
			}else if(round == CorporateProcessConstants.IND_FLOOR){
				result = floor(result);
			}
			if(remnant){
				result = divideForAmount(result, CorporateProcessConstants.AMOUNT_ROUND);
			}
			return result;
		}
		return null;
	}
	
	/**
	 * Round.
	 *
	 * @param amount the amount
	 * @return the big decimal
	 */
	public static BigDecimal round(BigDecimal amount){
		return amount.setScale(0, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * Floor.
	 *
	 * @param amount the amount
	 * @return the big decimal
	 */
	public static BigDecimal floor(BigDecimal amount){
		return amount.setScale(0, BigDecimal.ROUND_FLOOR);
	}
	
	/**
	 * Used to verify the block balances are existed or not.
	 *
	 * @param corpProcessResult <code>CorporativeProcessResult</code>
	 * @return boolean <code>boolean</code>
	 */
	public static boolean hasBlockBalances(CorporativeProcessResult corpProcessResult){
		return (corpProcessResult.getPawnBalance() != null && corpProcessResult.getPawnBalance().compareTo(BigDecimal.ZERO) > 0) || 
				(corpProcessResult.getBanBalance() != null && corpProcessResult.getBanBalance().compareTo(BigDecimal.ZERO) > 0) ||
				(corpProcessResult.getOtherBlockBalance() != null && corpProcessResult.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0) ||
				(corpProcessResult.getReserveBalance() != null && corpProcessResult.getReserveBalance().compareTo(BigDecimal.ZERO) > 0) ||
				(corpProcessResult.getOppositionBalance() != null && corpProcessResult.getOppositionBalance().compareTo(BigDecimal.ZERO) > 0);
	}
	
	/**
	 * Used to verify the report balances are existed or not.
	 *
	 * @param corpProcessResult <code>CorporativeProcessResult</code>
	 * @return boolean <code>boolean</code>
	 */
	public static boolean hasReportBalances(CorporativeProcessResult corpProcessResult){
		return (corpProcessResult.getMarginBalance() != null && corpProcessResult.getMarginBalance().compareTo(BigDecimal.ZERO) > 0) || 
				(corpProcessResult.getReportingBalance() != null && corpProcessResult.getReportingBalance().compareTo(BigDecimal.ZERO) > 0);
	}
	
	/**
	 * This method is used to check the event type.
	 *
	 * @param procesType code>Integer</code>
	 * @param blockCorpResult code>BlockCorporativeResult</code>
	 * @return boolean <code>boolean</code>
	 */
	public static boolean effectBlockDocuments(Integer procesType, BlockCorporativeResult blockCorpResult){
		boolean flag = false;
		switch (ImportanceEventType.get(procesType)) {		
		case RETURN_OF_CONTRIBUTION:
			flag = blockCorpResult != null && CorporateProcessConstants.IND_YES.equals(blockCorpResult.getIndContributionReturn());
			break;
		case PREFERRED_SUSCRIPTION:
			flag = blockCorpResult != null && CorporateProcessConstants.IND_YES.equals(blockCorpResult.getIndSuscription());
			break;
		case CASH_DIVIDENDS:
			flag = blockCorpResult != null && CorporateProcessConstants.IND_YES.equals(blockCorpResult.getIndCashDividend());
			break;
		case INTEREST_PAYMENT:
			flag = blockCorpResult != null && CorporateProcessConstants.IND_YES.equals(blockCorpResult.getIndInterest());
			break;
		case ACTION_DIVIDENDS:
			flag = blockCorpResult != null && CorporateProcessConstants.IND_YES.equals(blockCorpResult.getIndStockDividend());
			break;
		case CAPITAL_AMORTIZATION:
			flag = blockCorpResult != null && CorporateProcessConstants.IND_YES.equals(blockCorpResult.getIndAmortization());
			break;
		case EARLY_PAYMENT:
			flag = blockCorpResult != null && CorporateProcessConstants.IND_YES.equals(blockCorpResult.getIndRescue());
			break;
		default:
			flag = true;
			break;
		}
		return flag;
	}
	
	/**
	 * This method is used to check the event type.
	 *
	 * @param lngType <code>Integer</code>
	 * @return boolean <code>boolean</code>
	 */
	public static boolean requiredStock(Integer lngType){
		boolean flag = false;
		switch (ImportanceEventType.get(lngType)) {
		case RETURN_OF_CONTRIBUTION:
		case CASH_DIVIDENDS:
		case INTEREST_PAYMENT:
		case PREFERRED_SUSCRIPTION:
		case CAPITAL_AMORTIZATION:
		case ACTION_DIVIDENDS:
			flag= true;
			break;
		default:
			flag = false;
			break;
		}
		return flag;
	}
	
	/**
	 * This method is used to check the event type.
	 *
	 * @param lngType <code>Integer</code>
	 * @return boolean <code>boolean</code>
	 */
	public static boolean isSpecialCorporateProcess(Integer lngType){
		boolean flag = false;
		switch (ImportanceEventType.get(lngType)) {
		case CAPITAL_REDUCTION:
		case CHANGE_NOMAL_VALUE_CAP_VAR:
		case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
		case CONVERTIBILY_BONE_TO_ACTION:
		case SECURITIES_EXCISION:
		case SECURITIES_EXCLUSION:
		case SECURITIES_FUSION:
		case SECURITIES_UNIFICATION:
		case EARLY_PAYMENT:
			flag= true;
			break; 
		default:
			flag = false;
			break;
		}
		return flag;
	}
	
	/**
	 * This method is used to check the event type.
	 *
	 * @param lngType <code>Integer</code>
	 * @return boolean <code>boolean</code>
	 */
	public static boolean isShareBenefitCorporateProcess(Integer lngType){
		boolean flag = false;
		switch (ImportanceEventType.get(lngType)) {
		case ACTION_DIVIDENDS:
		case PREFERRED_SUSCRIPTION:
		case END_PREFERRED_SUSCRIPTION:
			flag= true;
			break; 
		default:
			flag = false;
			break;
		}
		return flag;
	}
	
	/**
	 * This method is used to check the event type.
	 *
	 * @param lngType <code>Integer</code>
	 * @return boolean <code>boolean</code>
	 */
	public static boolean isCashBenefitCorporateProcess(Integer lngType){
		boolean flag = false;
		switch (ImportanceEventType.get(lngType)) {
		case CAPITAL_AMORTIZATION:
		case CASH_DIVIDENDS:
		case INTEREST_PAYMENT:
		case REMANENT_PAYMENT:
		case RETURN_OF_CONTRIBUTION:
			flag= true;
			break; 
		default:
			flag = false;
			break;
		}
		return flag;
	}
	
	/**
	 * This method is used to check the event type.
	 *
	 * @param lngType <code>Integer</code>
	 * @return boolean <code>boolean</code>
	 */
	public static boolean requiredReportStock(Integer lngType){
		boolean flag = false;
		switch (ImportanceEventType.get(lngType)) {
		case RETURN_OF_CONTRIBUTION:
		case CASH_DIVIDENDS:
		case INTEREST_PAYMENT:
		case PREFERRED_SUSCRIPTION:
		case CAPITAL_AMORTIZATION:
		case ACTION_DIVIDENDS:
			flag= true;
			break;
		default:
			flag = false;
			break;
		}
		return flag;
	}
	
	/**
	 * Used to divide the balances with the balances.
	 *
	 * @param cashCorpResults <code>List</code>
	 * @return <code>List</code>
	 */
	public static List<CorporativeProcessResult> divideForCash(List<CorporativeProcessResult> cashCorpResults){
		if(cashCorpResults != null && !cashCorpResults.isEmpty()){
			for(CorporativeProcessResult cashCorpProcResult : cashCorpResults){
				divideForCash(cashCorpProcResult);
			}
		}
		return cashCorpResults;
	}
	
	/**
	 * Used to divide the balances with the balances.
	 *
	 * @param cashCorpProcResult <code>CorporativeProcessResult</code>
	 * @return <code>CorporativeProcessResult</code>
	 */
	public static CorporativeProcessResult divideForCash(CorporativeProcessResult cashCorpProcResult){
		if(Validations.validateIsNotNull(cashCorpProcResult.getReportingBalance())){
			cashCorpProcResult.setReportingBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getReportingBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setReportingBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getReportedBalance())){
			cashCorpProcResult.setReportedBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getReportedBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setReportedBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getAvailableBalance())){
			cashCorpProcResult.setAvailableBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getAvailableBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setAvailableBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getPawnBalance())){
			cashCorpProcResult.setPawnBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getPawnBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setPawnBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getBanBalance())){
			cashCorpProcResult.setBanBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getBanBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setBanBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getOtherBlockBalance())){
			cashCorpProcResult.setOtherBlockBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getOtherBlockBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setOtherBlockBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getReserveBalance())){
			cashCorpProcResult.setReserveBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getReserveBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setReserveBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getOppositionBalance())){
			cashCorpProcResult.setOppositionBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getOppositionBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setOppositionBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getMarginBalance())){
			cashCorpProcResult.setMarginBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getMarginBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setMarginBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getTotalBalance())){
			cashCorpProcResult.setTotalBalance(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getTotalBalance(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setTotalBalance(BigDecimal.ZERO);
		}
		if(Validations.validateIsNotNull(cashCorpProcResult.getExchangeAmount())){
			cashCorpProcResult.setExchangeAmount(CorporateProcessUtils.divideForAmount(cashCorpProcResult.getExchangeAmount(),CorporateProcessConstants.AMOUNT_ROUND));
		}else{
			cashCorpProcResult.setExchangeAmount(BigDecimal.ZERO);
		}
		return cashCorpProcResult;
	}
	
	/**
	 * generate the AccountCommponent.
	 *
	 * @param idCorporateOperation the id corporate operation
	 * @param idCustodyOperation the id custody operation
	 * @param idRefCustodyOperation the id ref custody operation
	 * @param idGuaranteeOperation the id guarantee operation
	 * @param idTradeOperation the id trade operation
	 * @param processType the process type
	 * @param sourceAccountBalanceTOs the source account balance t os
	 * @param targetAccountBalanceTOs the target account balance t os
	 * @param balanceType the balance type
	 * @param indMarketFact the ind market fact
	 * @return the accounts component to
	 */
	public static AccountsComponentTO getAccountsComponentTO(Long idCorporateOperation, Long idCustodyOperation, Long idRefCustodyOperation,
			Long idGuaranteeOperation, Long idTradeOperation, ImportanceEventType processType, 
			List<HolderAccountBalanceTO> sourceAccountBalanceTOs, List<HolderAccountBalanceTO> targetAccountBalanceTOs,
			int balanceType,Integer indMarketFact){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(CorporateProcessUtils.getBusinessProcessCorporate(processType));
		objAccountComponent.setIdCorporativeOperation(idCorporateOperation);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setIdRefCustodyOperation(idRefCustodyOperation);
		objAccountComponent.setIdGuaranteeOperation(idGuaranteeOperation);
		objAccountComponent.setIdTradeOperation(idTradeOperation);
		objAccountComponent.setLstSourceAccounts(sourceAccountBalanceTOs);
		objAccountComponent.setLstTargetAccounts(targetAccountBalanceTOs);
		objAccountComponent.setIdOperationType(CorporateProcessUtils.getOperationType(processType, balanceType));
		objAccountComponent.setIndMarketFact(indMarketFact);
		return objAccountComponent;
	}
	
	/**
	 * Gets the block balance.
	 *
	 * @param blockType the block type
	 * @param blockLevel the block level
	 * @return the block balance
	 */
	public static int getBlockBalance(Integer blockType, Integer blockLevel){
		int bal = -1;
		switch (AffectationType.get(blockType)){
		case PAWN:
			if(blockLevel != null && LevelAffectationType.REBLOCK.getCode().equals(blockLevel)){
				bal = CorporateProcessConstants.BL_REBLOCK_PAWN;
			}else{
				bal = CorporateProcessConstants.BL_PAWN;
			}
			break;
		case BAN:
			if(blockLevel != null && LevelAffectationType.REBLOCK.getCode().equals(blockLevel)){
				bal = CorporateProcessConstants.BL_REBLOCK_BAN;
			}else{
				bal = CorporateProcessConstants.BL_BAN;
			}
			break;
		case OTHERS:
			if(blockLevel != null && LevelAffectationType.REBLOCK.getCode().equals(blockLevel)){
				bal = CorporateProcessConstants.BL_REBLOCK_OTHER_BLOCK;
			}else{
				bal = CorporateProcessConstants.BL_OTHER_BLOCK;
			}
			break;
		case RESERVE:
			if(blockLevel != null && LevelAffectationType.REBLOCK.getCode().equals(blockLevel)){
				bal = CorporateProcessConstants.BL_REBLOCK_RESERVE;
			}else{
				bal = CorporateProcessConstants.BL_RESERVE;
			}
			break;
		case OPPOSITION:
			if(blockLevel != null && LevelAffectationType.REBLOCK.getCode().equals(blockLevel)){
				bal = CorporateProcessConstants.BL_REBLOCK_OPPOSITION;
			}else{
				bal = CorporateProcessConstants.BL_OPPOSITION;
			}
			break;
		}
		return bal;
	}
	
	/**
	 * Gets the business process corporate.
	 *
	 * @param processType the process type
	 * @return the business process corporate
	 */
	public static Long getBusinessProcessCorporate(ImportanceEventType processType){
		Long businessProcess = null;
		switch (processType) {
		case ACTION_DIVIDENDS:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_SHARE_DIVIDENDS.getCode();
			break;
		case CAPITAL_AMORTIZATION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_CAPITAL_AMORTIZATION.getCode();
			break;
		case CAPITAL_REDUCTION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_CAPITAL_REDUCTION.getCode();
			break;
		case CASH_DIVIDENDS:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_CASH_DIVIDENDS.getCode();
			break;
		case CHANGE_NOMAL_VALUE_CAP_VAR:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_CHANGE_NOMAL_VALUE_CAP_VAR.getCode();
			break;
		case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode();
			break;
		case COMMON_ACTIONS:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_COMMON_SHARES.getCode();
			break;
		case CONVERTIBILY_BONE_TO_ACTION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_CONVERTIBILY_BONE_TO_ACTION.getCode();
			break;
		case END_PREFERRED_SUSCRIPTION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_END_PREFERRED_SUSCRIPTION.getCode();
			break;
		case INTEREST_PAYMENT:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_INTEREST_PAYMENT.getCode();
			break;
		case PREFERRED_SUSCRIPTION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_PREFERRED_SUSCRIPTION.getCode();
			break;
		case REMANENT_PAYMENT:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_REMANENT_PAYMEN.getCode();
			break;
		case EARLY_PAYMENT:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_RESCUE_CAPITAL.getCode();
			break;
		case RETURN_OF_CONTRIBUTION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_RETURN_OF_CONTRIBUTION.getCode();
			break;
		case SECURITIES_EXCISION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_SECURITIES_EXCISION.getCode();
			break;
		case SECURITIES_EXCLUSION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_SECURITIES_EXCLUSION.getCode();
			break;
		case SECURITIES_FUSION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_SECURITIES_FUSION.getCode();
			break;
		case SECURITIES_UNIFICATION:
			businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_SECURITIES_UNIFICATION.getCode();
			break;
		}
		return businessProcess;
	}
	
	/**
	 * Gets the operation unblock.
	 *
	 * @param balanceType the balance type
	 * @param blockOperation the block operation
	 * @return the operation unblock
	 */
	public static Long getOperationUnblock(int balanceType,Long blockOperation){
		Long operationType = null;
		switch (balanceType) {
		case CorporateProcessConstants.BL_PAWN:
			operationType = ParameterOperationType.SECURITIES_PAWN_UNBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_BAN:
			operationType = ParameterOperationType.SECURITIES_BAN_UNBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_OTHER_BLOCK:
			operationType = ParameterOperationType.SECURITIES_OTHERS_UNBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_RESERVE:
			operationType = ParameterOperationType.SECURITIES_RESERVE_UNBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_OPPOSITION:
			operationType = ParameterOperationType.SECURITIES_OPPOSITION_UNBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_PAWN:
			operationType = ParameterOperationType.SECURITIES_PAWN_UNBLOCK_REBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_BAN:
			operationType = ParameterOperationType.SECURITIES_BAN_UNBLOCK_REBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_OTHER_BLOCK:
			operationType = ParameterOperationType.SECURITIES_OTHERS_UNBLOCK_REBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_RESERVE:
			operationType = ParameterOperationType.SECURITIES_RESERVE_UNBLOCK_REBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_OPPOSITION:
			operationType = ParameterOperationType.SECURITIES_OPPOSITION_UNBLOCK_REBLOCK.getCode();
			break;
		default:
			//if is nots in this balance get default operation
			operationType = blockOperation;
			break;
		}
		return operationType;
	}
	
	/**
	 * Gets the operation type.
	 *
	 * @param processType the process type
	 * @param balanceType the balance type
	 * @return the operation type
	 */
	public static Long getOperationType(ImportanceEventType processType, int balanceType){
		Long operationType = null;
		switch (balanceType) {
		//available balance is with custom operation by corporate process
		case CorporateProcessConstants.BL_AVAILABLE:
			switch (processType) {
			case ACTION_DIVIDENDS:
				operationType = ParameterOperationType.STOCK_DIVIDENDS_AVAILABLE.getCode();
				break;
			case CAPITAL_AMORTIZATION:
				operationType = ParameterOperationType.AMORTIZATION_AVAILABLE.getCode();
				break;
			case CAPITAL_REDUCTION:
				operationType = ParameterOperationType.REDUCTION_AVAILABLE.getCode();
				break;
//			case CASH_DIVIDENDS:
//				businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_CASH_DIVIDENDS.getCode();
//				break;
			case CHANGE_NOMAL_VALUE_CAP_VAR:
				operationType = ParameterOperationType.CNV_VAR_AVAILABLE.getCode();
				break;
			case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
				operationType = ParameterOperationType.CNV_NO_VAR_AVAILABLE.getCode();
				break;
//			case COMMON_ACTIONS:
//				businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_COMMON_SHARES.getCode();
//				break;
			case CONVERTIBILY_BONE_TO_ACTION:
				operationType = ParameterOperationType.CONVERTIBLE_B2A_AVAILABLE.getCode();
				break;
			case END_PREFERRED_SUSCRIPTION:
				operationType = ParameterOperationType.END_SUSCRIPTION_AVAILABLE.getCode();
				break;
//			case INTEREST_PAYMENT:
//				businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_INTEREST_PAYMENT.getCode();
//				break;
			case PREFERRED_SUSCRIPTION:
				operationType = ParameterOperationType.SUSCRIPTION_AVAILABLE.getCode();
				break;
//			case REMANENT_PAYMENT:
//				businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_REMANENT_PAYMEN.getCode();
//				break;
//			case RESCUE_CAPITAL:
//				businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_RESCUE_CAPITAL.getCode();
//				break;
//			case RETURN_OF_CONTRIBUTION:
//				businessProcess = BusinessProcessType.CORPORATIVE_PROCESS_RETURN_OF_CONTRIBUTION.getCode();
//				break;
			case SECURITIES_EXCISION:
				operationType = ParameterOperationType.EXCISION_AVAILABLE.getCode();
				break;
			case SECURITIES_EXCLUSION:
				operationType = ParameterOperationType.EXCLUSION_AVAILABLE.getCode();
				break;
			case SECURITIES_FUSION:
				operationType = ParameterOperationType.FUSION_AVAILABLE.getCode();
				break;
			case SECURITIES_UNIFICATION:
				operationType = ParameterOperationType.UNIFICATION_AVAILABLE.getCode();
				break;
			}
			break;
		case CorporateProcessConstants.BL_PAWN:
			operationType = ParameterOperationType.SECURITIES_PAWN_BLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_BAN:
			operationType = ParameterOperationType.SECURITIES_BAN_BLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_OTHER_BLOCK:
			operationType = ParameterOperationType.SECURITIES_OTHERS_BLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_RESERVE:
			operationType = ParameterOperationType.SECURITIES_RESERVE_BLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_OPPOSITION:
			operationType = ParameterOperationType.SECURITIES_OPPOSITION_BLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_PAWN:
			operationType = ParameterOperationType.SECURITIES_PAWN_REBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_BAN:
			operationType = ParameterOperationType.SECURITIES_BAN_REBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_OTHER_BLOCK:
			operationType = ParameterOperationType.SECURITIES_OTHERS_REBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_RESERVE:
			operationType = ParameterOperationType.SECURITIES_RESERVE_REBLOCK.getCode();
			break;
		case CorporateProcessConstants.BL_REBLOCK_OPPOSITION:
			operationType = ParameterOperationType.SECURITIES_OPPOSITION_REBLOCK.getCode();
			break;
			
		//for this balances using others operation types
		case CorporateProcessConstants.BL_MARGIN:
			switch (processType) {
			case ACTION_DIVIDENDS:
				operationType = ParameterOperationType.STOCK_DIVIDENDS_GUARANTEE.getCode();
				break;
			case CAPITAL_REDUCTION:
				operationType = ParameterOperationType.REDUCTION_GUARANTEE.getCode();
				break;
			case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
				operationType = ParameterOperationType.CNV_NO_VAR_GUARANTEE.getCode();
				break;
			case SECURITIES_EXCISION:
				operationType = ParameterOperationType.EXCISION_GUARANTEE.getCode();
				break;
			case SECURITIES_FUSION:
				operationType = ParameterOperationType.FUSION_GUARANTEE.getCode();
				break;
			case SECURITIES_UNIFICATION:
				operationType = ParameterOperationType.UNIFICATION_GUARANTEE.getCode();
				break;
			default:
				break;
			}
			
			break;
		case CorporateProcessConstants.BL_REPORTING:
			switch (processType) {
			case ACTION_DIVIDENDS:
				operationType = ParameterOperationType.STOCK_DIVIDENDS_REPORTING.getCode();
				break;
			case CAPITAL_REDUCTION:
				operationType = ParameterOperationType.REDUCTION_REPORTING.getCode();
				break;
			case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
				operationType = ParameterOperationType.CNV_NO_VAR_REPORTING.getCode();
				break;
			case SECURITIES_EXCISION:
				operationType = ParameterOperationType.EXCISION_REPORTING.getCode();
				break;
			case SECURITIES_FUSION:
				operationType = ParameterOperationType.FUSION_REPORTING.getCode();
				break;
			case SECURITIES_UNIFICATION:
				operationType = ParameterOperationType.UNIFICATION_REPORTING.getCode();
				break;
			default:
				break;
			}
			
			break;
		case CorporateProcessConstants.BL_REPORTED:
			switch (processType) {
			case ACTION_DIVIDENDS:
				operationType = ParameterOperationType.STOCK_DIVIDENDS_REPORTED.getCode();
				break;
			case CAPITAL_REDUCTION:
				operationType = ParameterOperationType.REDUCTION_REPORTED.getCode();
				break;
			case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
				operationType = ParameterOperationType.CNV_NO_VAR_REPORTED.getCode();
				break;
			case SECURITIES_EXCISION:
				operationType = ParameterOperationType.EXCISION_REPORTED.getCode();
				break;
			case SECURITIES_FUSION:
				operationType = ParameterOperationType.FUSION_REPORTED.getCode();
				break;
			case SECURITIES_UNIFICATION:
				operationType = ParameterOperationType.UNIFICATION_REPORTED.getCode();
				break;
			default:
				break;
			}
			break;
		}
		return operationType;
	}
	
	/**
	 * Checks for reblock.
	 *
	 * @param processType the process type
	 * @return true, if successful
	 */
	public static boolean hasReblock(ImportanceEventType processType){
		boolean flag = false;
		switch (processType) {
		case CAPITAL_REDUCTION:
		case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
		case CONVERTIBILY_BONE_TO_ACTION:
		case SECURITIES_EXCISION:
		case SECURITIES_FUSION:
		case SECURITIES_UNIFICATION:
			flag = true;
			break; 
		}
		return flag;
	}
	
	/**
	 * Gets the security operation type.
	 *
	 * @param processType the process type
	 * @return the security operation type
	 */
	public static Long getSecurityOperationType(ImportanceEventType processType){
		Long operationType = null;
		switch (processType) {
		case ACTION_DIVIDENDS:
			operationType = ParameterOperationType.SEC_STOCK_DIVIDENDS.getCode();
			break;
		case CAPITAL_AMORTIZATION:
			operationType = ParameterOperationType.SEC_CAPITAL_AMORTIZATION.getCode();
			break;
		case CAPITAL_REDUCTION:
			operationType = ParameterOperationType.SEC_CAPITAL_REDUCTION.getCode();
			break;
		case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
			operationType = ParameterOperationType.SEC_CNV_NO_CAP_VAR.getCode();
			break;
		case CONVERTIBILY_BONE_TO_ACTION:
			operationType = ParameterOperationType.SEC_CONVERTIBILY_BONE_TO_ACTION.getCode();
			break;
		case SECURITIES_EXCISION:
			operationType = ParameterOperationType.SEC_SECURITIES_EXCISION.getCode();
			break;
		case SECURITIES_EXCLUSION:
			operationType = ParameterOperationType.SEC_SECURITIES_EXCLUSION.getCode();
			break;
		case SECURITIES_FUSION:
			operationType = ParameterOperationType.SEC_SECURITIES_FUSION.getCode();
			break;
		case SECURITIES_UNIFICATION:
			operationType = ParameterOperationType.SEC_SECURITIES_UNIFICATION.getCode();
			break;
		case EARLY_PAYMENT:
			operationType = ParameterOperationType.SEC_RESCUE_CAPITAL.getCode();
			break;
		case PREFERRED_SUSCRIPTION:
			operationType = ParameterOperationType.SEC_PREFERED_SUBSCRIPTION.getCode();
			break;
		case END_PREFERRED_SUSCRIPTION:
			operationType = ParameterOperationType.SEC_END_SUBSCRIPTION.getCode();
			break;		
		}
		return operationType;
	}
	
	/**
	 * Gets the securities operation.
	 *
	 * @param corpOpe the corp ope
	 * @param processType the process type
	 * @param amount the amount
	 * @param quantity the quantity
	 * @param security the security
	 * @return the securities operation
	 */
	public static SecuritiesOperation getSecuritiesOperation(CorporativeOperation corpOpe, ImportanceEventType processType,
			BigDecimal amount, BigDecimal quantity, String security){
		SecuritiesOperation securitiesOperation = new SecuritiesOperation();
		securitiesOperation.setSecurities(new Security(security));
		securitiesOperation.setCorporativeOperation(corpOpe);
		securitiesOperation.setOperationDate(CommonsUtilities.currentDate());
		securitiesOperation.setCashAmount(amount);
		securitiesOperation.setStockQuantity(quantity);
		securitiesOperation.setOperationType(getSecurityOperationType(processType));
		return securitiesOperation;
	}
	
	/**
	 * Gets the securities component.
	 *
	 * @param processType the process type
	 * @param securitiesOperation the securities operation
	 * @param physicalQuantity the physical quantity
	 * @param demtQuanity the demt quanity
	 * @param sourceTarget the source target
	 * @return the securities component
	 */
	public static SecuritiesComponentTO getSecuritiesComponent(ImportanceEventType processType, SecuritiesOperation securitiesOperation, 
			BigDecimal physicalQuantity, BigDecimal demtQuanity, Integer sourceTarget){
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();
		if (Validations.validateIsNull(demtQuanity)) {
			demtQuanity= BigDecimal.ZERO;
		}
		if (Validations.validateIsNull(physicalQuantity)) {
			physicalQuantity= BigDecimal.ZERO;
		}
		securitiesComponentTO.setDematerializedBalance(demtQuanity);
		securitiesComponentTO.setIdBusinessProcess(CorporateProcessUtils.getBusinessProcessCorporate(processType));
		securitiesComponentTO.setIdSecurityCode(securitiesOperation.getSecurities().getIdSecurityCodePk());
		securitiesComponentTO.setIdOperationType(securitiesOperation.getOperationType());
		securitiesComponentTO.setIdSecuritiesOperation(securitiesOperation.getIdSecuritiesOperationPk());
		securitiesComponentTO.setPhysicalBalance(physicalQuantity);
		securitiesComponentTO.setCirculationBalance(demtQuanity.add(physicalQuantity));
		securitiesComponentTO.setShareBalance(demtQuanity.add(physicalQuantity));
		if(sourceTarget != null){
			securitiesComponentTO.setIndSourceTarget(Long.valueOf(sourceTarget));
		}
		return securitiesComponentTO;
	}
	
	/**
	 * Gets the holder adjustment factor.
	 *
	 * @param holderAccAdjustments the holder acc adjustments
	 * @param corpProcessResult the corp process result
	 * @return the holder adjustment factor
	 */
	public static BigDecimal getHolderAdjustmentFactor(Map<String, Object[]> holderAccAdjustments, CorporativeProcessResult corpProcessResult){
		return getHolderAdjustmentFactor(holderAccAdjustments, corpProcessResult, false);
	}
	
	/**
	 * Gets the holder adjustment factor.
	 *
	 * @param holderAccAdjustments the holder acc adjustments
	 * @param corpProcessResult the corp process result
	 * @param succesion the succesion
	 * @return the holder adjustment factor
	 */
	public static BigDecimal getHolderAdjustmentFactor(Map<String, Object[]> holderAccAdjustments, CorporativeProcessResult corpProcessResult, boolean succesion){
		BigDecimal tmpDelFactor = null;
		String key = String.valueOf(corpProcessResult.getHolderAccount().getIdHolderAccountPk());
		if(succesion){
			key = key+GeneralConstants.HYPHEN+corpProcessResult.getSecurity().getIdSecurityCodePk();
		}
		if(holderAccAdjustments != null && holderAccAdjustments.containsKey(key)){
			Object[] adjustment = holderAccAdjustments.get(key);
			if(CorporateProcessConstants.IND_YES.equals(adjustment[2])){
				tmpDelFactor = (BigDecimal)adjustment[5];
			}
		}
		return tmpDelFactor;
	}
	
	/**
	 * Gets the holder adjustment custody.
	 *
	 * @param holderAccAdjustments the holder acc adjustments
	 * @param corpProcessResult the corp process result
	 * @return the holder adjustment custody
	 */
	public static BigDecimal getHolderAdjustmentCustody(Map<String, Object[]> holderAccAdjustments, CorporativeProcessResult corpProcessResult){
		BigDecimal custody = null;
		String key = corpProcessResult.getHolderAccount().getIdHolderAccountPk()+GeneralConstants.HYPHEN+corpProcessResult.getSecurity().getIdSecurityCodePk();
		if(holderAccAdjustments != null && holderAccAdjustments.containsKey(key)){
			Object[] adjustment = holderAccAdjustments.get(key);
			if(CorporateProcessConstants.IND_YES.equals(adjustment[4])){
				custody = (BigDecimal)adjustment[7];
			}
		}
		return custody;
	}
	
	/**
	 * Gets the report parameters.
	 *
	 * @param corpOperation the corp operation
	 * @param originSecurity the origin security
	 * @param reportType the report type
	 * @return the report parameters
	 */
	public static Map<String,String> getReportParameters(CorporativeOperation corpOperation, String originSecurity, 
			Integer reportType){
		Map<String,String> parameters = new HashMap<String, String>();
		if(Validations.validateIsNotNullAndPositive(corpOperation.getIdCorporativeOperationPk())){
		parameters.put("process_id", String.valueOf(corpOperation.getIdCorporativeOperationPk()));
		}else{
		parameters.put("process_id","");
		}
		parameters.put("cutoff_date", CommonsUtilities.convertDatetoString(corpOperation.getCutoffDate()));
		parameters.put("delivery_date", CommonsUtilities.convertDatetoString(corpOperation.getDeliveryDate()));
		parameters.put("event_type", String.valueOf(corpOperation.getCorporativeEventType().getIdCorporativeEventTypePk()));
		parameters.put("code_value", corpOperation.getSecurities().getIdSecurityCodePk());
		parameters.put("report_type", String.valueOf(reportType));
		parameters.put("reportFormats", String.valueOf(ReportFormatType.PDF.getCode()));
		parameters.put("state", String.valueOf(corpOperation.getState()));
		
		if(originSecurity != null){
		parameters.put("dest_code_value", originSecurity);
		}else{
		parameters.put("dest_code_value", "");
		}
		if(corpOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT.getCode())
				|| corpOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONSOLIDATED_INTEREST_PAYMENT_ISSUER.getCode())
				|| corpOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT.getCode())
				|| corpOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER.getCode())) {
			if(Validations.validateIsNotNullAndNotEmpty(corpOperation.getIssuer()) &&
					Validations.validateIsNotNullAndNotEmpty(corpOperation.getIssuer().getIdIssuerPk()) ){
				parameters.put("issuer", String.valueOf(corpOperation.getIssuer().getIdIssuerPk()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(corpOperation.getIdParticipantPk())){
				parameters.put("participant", String.valueOf(corpOperation.getIdParticipantPk()));
			}
		}

		return parameters;
	}
	
	/**
	 * Gets the report id.
	 *
	 * @param processType the process type
	 * @param reportType the report type
	 * @param indAffect the ind affect
	 * @return the report id
	 */
	public static Long getReportID(Integer processType, CorporativeEventReportType  reportType,Integer indAffect){
		ImportanceEventType corpProcessType = ImportanceEventType.get(processType);
		Long reportId = null;
		switch(corpProcessType){
		case ACTION_DIVIDENDS:
			switch(reportType){
			case FOR_SECURITY_ISSUER:
				reportId = ReportIdType.SHARE_DIVIDEND_PAY_FOR_SECURTIY_ISSUER.getCode();
				break;
			case HOLDER_ACCOUNT_FOR_PARTICIPANTS:
				reportId = ReportIdType.SHARE_DIVIDEND_PAY_BY_ACCHDR_PART.getCode();
				break;
			case REPURCHASE_BY_MARGIN:
				reportId = ReportIdType.DIVIDENDS_PAID_ON_REPURCHASE_MARGIN.getCode();
				break;
			case BLOCKED_BY_BALANCE:
				reportId = ReportIdType.DIVIDENDS_PAID_BY_BLOCKED_BALANCE.getCode();
				break;
			case CONTROL_OF_PAYMENTS:
				reportId = ReportIdType.SHARE_DIVIDEND_PAYMENT_CONTROL.getCode();
				break;
			}
			break;
		case CAPITAL_AMORTIZATION:
			switch(reportType){
			case FOR_SECURITY_ISSUER:
				if(indAffect.equals(BooleanType.YES.getCode())){
					reportId = ReportIdType.AMORTIZATION_PAYMENT_FINAL_SECURITY_ISSUER.getCode();
				}else{
					reportId = ReportIdType.AMORTIZATION_PAYMENT_SECURITY_ISSUER.getCode();
				}				
				break;
			case HOLDER_ACCOUNT_FOR_PARTICIPANTS:
				if(indAffect.equals(BooleanType.YES.getCode())){
					reportId = ReportIdType.AMORTIZATION_PAYMENT_FINAL_ACCHDR_PART.getCode();
				}else{
					reportId = ReportIdType.AMORTIZATION_PAYMENT_ACCHDR_PART.getCode();
				}				
				break;
			case REPURCHASE_BY_MARGIN:
				if(indAffect.equals(BooleanType.YES.getCode())){
					reportId = ReportIdType.AMORTIZATION_PAY_FINAL_REPURCHASE_MARGIN.getCode();
				}else{
					reportId = ReportIdType.AMORTIZATION_PAY_REPURCHASE_MARGIN.getCode();
				}				
				break;
			case BLOCKED_BY_BALANCE:
				if(indAffect.equals(BooleanType.YES.getCode())){
					reportId = ReportIdType.AMORTIZATION_PAYMENT_FINAL_BALLOCKED.getCode();
				}else{
					reportId = ReportIdType.AMORTIZATION_PAYMENT_BALLOCKED.getCode();
				}				
				break;
			case CONTROL_OF_PAYMENTS:
				if(indAffect.equals(BooleanType.YES.getCode())){
					reportId = ReportIdType.AMORTIZATION_PAY_FINAL_CONTOL_OF_PAYMENTS.getCode();
				}else{
					reportId = ReportIdType.AMORTIZATION_PAY_CONTOL_OF_PAYMENTS.getCode();
				}				
				break;
			case RETIREMENT_HOLDER_ACCOUNT_FOR_PARTICIPANTS:
				reportId = ReportIdType.RETIREMENT_AMORTIZATION_PAYMENT_ACCHDR_PART.getCode();
				break;
			case RETIREMENT_FOR_SECURITY_ISSUER:
				reportId = ReportIdType.RETIREMENT_AMORTIZATION_PAYMENT_SECURITY_ISSUER.getCode();
				break;
			}
			break;
		case CASH_DIVIDENDS:
		case RETURN_OF_CONTRIBUTION:
			switch(reportType){
			case FOR_SECURITY_ISSUER:
				reportId = ReportIdType.CASH_DIVIDENDS_PAID_BY_SECURITY_ISSUER.getCode();
				break;
			case HOLDER_ACCOUNT_FOR_PARTICIPANTS:
				reportId = ReportIdType.CASH_DIVIDENDS_PAY_BY_PARTS_ACCHDR.getCode();
				break;
			case REPURCHASE_BY_MARGIN:
				reportId = ReportIdType.CASH_DIVIDENDS_PAID_REPURCH_MARGIN.getCode();
				break;
			case BLOCKED_BY_BALANCE:
				reportId = ReportIdType.CASH_DIVIDENDS_PAID_BY_BALLOCKED.getCode();
				break;
			case CONTROL_OF_PAYMENTS:
				reportId = ReportIdType.DIVIDEND_PAYMENT_IN_PAYMENTS_CONTROL.getCode();
				break;
			}
			break;
		case CAPITAL_REDUCTION:
		case CHANGE_NOMAL_VALUE_CAP_VAR:
		case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
		case CONVERTIBILY_BONE_TO_ACTION:
		case SECURITIES_EXCISION:
		case SECURITIES_EXCLUSION:
		case SECURITIES_FUSION:
		case SECURITIES_UNIFICATION:
			switch(reportType){
			case FOR_SECURITY_ISSUER:
				//reportId = ReportIdType.CASH_DIVIDENDS_PAID_BY_SECURITY_ISSUER.getCode();
				reportId = ReportIdType.SPECIAL_REPORT_FOR_SECURITY_ISSUER.getCode();
				break;
			case HOLDER_ACCOUNT_FOR_PARTICIPANTS:
				reportId = ReportIdType.SPECIAL_PROCESS_HOLDER_PARTICIPANT.getCode();
				break;
			case REPURCHASE_BY_MARGIN:
				//reportId = ReportIdType.CASH_DIVIDENDS_PAID_REPURCH_MARGIN.getCode();
				reportId = ReportIdType.SPECIAL_REPORT_ON_REPURCHASE_MARGIN.getCode();
				break;
			case BLOCKED_BY_BALANCE:
				//reportId = ReportIdType.CASH_DIVIDENDS_PAID_BY_BALLOCKED.getCode();
				reportId = ReportIdType.SPECIAL_REPORT_BY_BLOCKED_BALANCE.getCode();
				break;
			case SPECIAL_PROCESS_CONTROL_LIST:
				reportId = ReportIdType.SPECIAL_PROCESS_LIST_OF_CONTROL.getCode();
				break;
			}
			break;
		case END_PREFERRED_SUSCRIPTION:
			break;
		case INTEREST_PAYMENT:
			switch(reportType){
			case FOR_SECURITY_ISSUER:
				reportId = ReportIdType.INTEREST_PAYMENT_SECURITY_FOR_ISSUER.getCode();
				break;
			case HOLDER_ACCOUNT_FOR_PARTICIPANTS:
				reportId = ReportIdType.INEREST_PAY_PARTICIPANTS_ACCHDR.getCode();
				break;
			case REPURCHASE_BY_MARGIN:
				reportId = ReportIdType.INTEREST_PAY_REPURCHASE_AND_MARGIN.getCode();
				break;
			case BLOCKED_BY_BALANCE:
				reportId = ReportIdType.INTEREST_PAYMENT_BY_BLOCKED_BALANCE.getCode();
				break;
			case CONTROL_OF_PAYMENTS:
				reportId = ReportIdType.INTEREST_PAYMENT_CONTROL.getCode();
				break;
			case RETIREMENT_HOLDER_ACCOUNT_FOR_PARTICIPANTS:
				reportId = ReportIdType.RETIREMENT_HOLDER_PAYMENT_INTEREST_BY_PARTICIPANT.getCode();
				break;
			case RETIREMENT_FOR_SECURITY_ISSUER:
				reportId = ReportIdType.RETIREMENT_HOLDER_PAYMENT_INTEREST.getCode();
				break;
			}
			break;
		case PREFERRED_SUSCRIPTION:
		switch(reportType){
		case FOR_SECURITY_ISSUER:
			reportId = ReportIdType.SUBSCRIPTION_PAYMT_FOR_SECURITY_ISSUER.getCode();
			break;
		case HOLDER_ACCOUNT_FOR_PARTICIPANTS:
			reportId = ReportIdType.PAYMENT_ACCOUNT_HOLDER_PARTICIPANTS.getCode();
			break;
		case REPURCHASE_BY_MARGIN:
			reportId = ReportIdType.SUBSCRIPTION_PAY_REPURCHASE_MARGIN.getCode();
			break;
		case BLOCKED_BY_BALANCE:
			reportId = ReportIdType.SUBSCRIPTION_PAYMT_BY_BLOCKED_BALANCE.getCode();
			break;
		case CONTROL_OF_PAYMENTS:
			reportId = ReportIdType.SUBSCRIPTION_FEE_PAYMENT_CONTORL.getCode();
			break;
		}
			break;
		case REMANENT_PAYMENT:
			switch(reportType){
			case REMANENT_PAYMENT_ISSUER:
				reportId = ReportIdType.REMANENT_PAYMENT_ISSUER.getCode();
				break;
			}
			break;
		case EARLY_PAYMENT:
			break;
		case CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT:
			switch(reportType){
			case CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT:
				reportId = ReportIdType.CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT.getCode();
				break;
			}
			break;
		case CONSOLIDATED_INTEREST_PAYMENT_ISSUER:
			switch(reportType){
			case CONSOLIDATED_INTEREST_PAYMENT_ISSUER:
				reportId = ReportIdType.CONSOLIDATED_INTEREST_PAYMENT_ISSUER.getCode();
				break;
			}
			break;
		case CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT:
			switch(reportType){
			case CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT:
				reportId = ReportIdType.CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT.getCode();
				break;
			}
			break;
		case CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER:
			switch(reportType){
			case CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER:
				reportId = ReportIdType.CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER.getCode();
				break;
			}
			break;
		}
		
		return reportId;
	} 

	/**
	 * Gets the stock report id.
	 *
	 * @param stockType the stock type
	 * @param reportType the report type
	 * @return the stock report id
	 */
	public static Long getStockReportID(Integer stockType, Long reportType){
		Long reportId = null;
		switch(stockType){
		case 205://STOCK POR VALOR
			if(ReportIdType.SECURITY_STOCK.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.NEGATIVE_STOCK.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.PARTICIPANT_STOCK.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType); 
				break;
			}
			
			if(ReportIdType.CALCULATION_STOCK_BY_OWNER.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.CALCULATION_STOCK_BY_BROKER.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}			
			if(ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_VALUE.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.STOCK_CALCULATION_BY_REPORTED.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				 
			}
			break;
		case 1322://STOCK POR PARTICIPANTE
			if(ReportIdType.PARTICIPANT_STOCK.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType); 
				break;
			}
		case 1323://STOCK POR TITULAR
			if(ReportIdType.HOLDER_STOCK.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);				
			}
			break;
		case 1324://STOCK MENSUAL
			if(ReportIdType.MONTHLY_STOCK.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);				
			}
			if(ReportIdType.NEGATIVE_STOCK_BY_MONTHLY.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);				
			}
			if(ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_MONTHLY.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);				
			}
			break;
		case 1626://STOCK POR CORPORATIVO
			if(ReportIdType.SECURITY_STOCK_FOR_TYPE_CORPORATIVE.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.NEGATIVE_STOCK.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.CALCULATION_STOCK_BY_OWNER.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.CALCULATION_STOCK_BY_BROKER.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}			
			if(ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_VALUE.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.STOCK_CALCULATION_BY_REPORTED.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			if(ReportIdType.STOCK_REPORT_BLOCKED_HOLDER.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			break;
		case 2934://STOCK CONSOLIDADO POR DEPOSITANTE
			if(ReportIdType.CONSOLIDATED_STOCK_PARTICIPANT.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			break;
		case 2935://STOCK CONSOLIDADO POR EMISOR
			if(ReportIdType.CONSOLIDATED_STOCK_ISSUER.getCode().equals(reportType)){
				reportId = Long.valueOf(reportType);
				
			}
			break;
		case 2036://STOCK POR HECHO DE MERCADO
			
			break;
		}
		return reportId;
	}
	
	/**
	 * Checks if is tax exempt.
	 *
	 * @param security the security
	 * @param holder the holder
	 * @return the integer
	 */
	public static Integer isTaxExempt(Security security,Holder holder ){
		boolean haveNit = false;
		Integer result=BooleanType.YES.getCode();
		if(holder!=null && security!=null){
			//validamos codigo NIT de titular
			haveNit = (holder.getDocumentType()!= null && DocumentType.RUC.getCode().equals(holder.getDocumentType()))
					|| (holder.getSecondDocumentType()!=null && DocumentType.RUC.getCode().equals(holder.getSecondDocumentType())); 			
			//Validamos tipo renta y anios de vencimiento del valor
			if(security.isFixedInstrumentTypeAcc()){
				int yearsTerm=CommonsUtilities.getYearsBetween(security.getIssuanceDate(),security.getExpirationDate());
				if(yearsTerm > CorporateProcessConstants.MIN_YEARS_TAX_PAY.intValue()){
					result= BooleanType.NO.getCode(); 
				}else if(haveNit){
					result= BooleanType.NO.getCode(); 
				}
			}else if(haveNit){
				result= BooleanType.NO.getCode(); 
			}
		}
		
		return result;
	}
}
