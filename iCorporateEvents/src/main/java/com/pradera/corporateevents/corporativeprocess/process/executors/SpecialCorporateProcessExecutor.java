/*
 * 
 */
package com.pradera.corporateevents.corporativeprocess.process.executors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.corporativeprocess.process.CorporateExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcess;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.OperationsFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BuyerOperationsFixer;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.ReportCorporativeResult;
import com.pradera.model.corporatives.SecuritiesCorporative;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockoperation.UnblockMarketFactOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.type.BlockMarketFactOperationStateType;
import com.pradera.model.custody.type.UnblockMarketOperationStateType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityHistoryBalance;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SpecialCorporateProcessExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/06/2014
 */
@CorporateProcess(CorporateExecutor.SPECIAL)
@Performance
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@LocalBean
public class SpecialCorporateProcessExecutor implements CorporateProcessExecutor {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The fixer. */
	@Inject
	private BalanceFixer fixer;
	
	/** The document fixer. */
	@Inject
	private DocumentsFixer documentFixer;
	
	/** The operation fixer. */
	@Inject
	private OperationsFixer operationFixer;
	
	/** The seller operation fixer. */
	@Inject
	private BuyerOperationsFixer buyerOperationFixer;
	
	/** The executor component service bean. */
	@Inject
	private Instance<AccountsComponentService> executorComponentServiceBean;
	
	/** The securities component service bean. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentServiceBean;
	
	/** The sc. */
	@Resource
    SessionContext sc;
	
	/** The special corporate process executor. */
	SpecialCorporateProcessExecutor specialCorporateProcessExecutor;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
    public void init(){
        this.specialCorporateProcessExecutor = this.sc.getBusinessObject(SpecialCorporateProcessExecutor.class);
    }
		
	/**
	 * Metodo que ejecuta preliminar de los eventos corporativos especiales.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executePreliminarProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started Subscription preliminary process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());

		corporateExecutorService.deletePreviousPreliminaryInformation(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED
					,true,true);
		if(ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
			List<SecuritiesCorporative> lstSecuritiesCorporative = corporateExecutorService.getExcisionSecurities(corpOperation.getIdCorporativeOperationPk());
			if(lstSecuritiesCorporative!=null){
				corpOperation.setSecuritiesCorporatives(lstSecuritiesCorporative);
				for(SecuritiesCorporative objSecuritiesCorporatives : corpOperation.getSecuritiesCorporatives()){
					objSecuritiesCorporatives.setDeliveryFactor(CorporateProcessUtils.divide(objSecuritiesCorporatives.getDeliveryFactor(), CorporateProcessConstants.AMOUNT_ROUND));
				}
			}			
		}else{
			corpOperation.setDeliveryFactor(CorporateProcessUtils.divide(corpOperation.getDeliveryFactor(), CorporateProcessConstants.AMOUNT_ROUND));
		}		
		List<Object[]> distinctHolders  = corporateExecutorService.getDistinctBalanceHoldersForProcess(corpOperation.getSecurities().getIdSecurityCodePk());
		Security targetSecurity = null;
		if(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.equals(processType) 
				|| ImportanceEventType.SECURITIES_UNIFICATION.equals(processType)
				|| ImportanceEventType.SECURITIES_FUSION.equals(processType)){
			targetSecurity = corporateExecutorService.getTargetSecurity(corpOperation.getIdCorporativeOperationPk());
		}
		if (distinctHolders != null && distinctHolders.size() > 0) {
			//TODO Get the details of unification and change owner participants
			this.specialCorporateProcessExecutor.executePreliminarProcessAlgorithm(distinctHolders, corpOperation, processType, targetSecurity, loggerUser);
		}
		
		return true;
	}
	
	/**
	 * Metodo que ejecuta algoritmo preliminar de los eventos corporativos especiales.
	 *
	 * @param distinctHolders the distinct holders
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param targetSecurity the target security
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executePreliminarProcessAlgorithm(List<Object[]> distinctHolders,CorporativeOperation corpOperation,ImportanceEventType processType,
			Security targetSecurity,LoggerUser loggerUser) throws ServiceException{
		boolean processReportOperations = true, originBalances = true, destinyBalances = true;
		Integer blockLevel = null;
		if(!CorporateProcessUtils.hasReblock(processType)){
			blockLevel = LevelAffectationType.BLOCK.getCode();
		}
		if(ImportanceEventType.SECURITIES_EXCLUSION.equals(processType)){
			processReportOperations = false;
		}
		if(ImportanceEventType.SECURITIES_EXCLUSION.equals(processType) ||
				ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
			destinyBalances = false;
		}
		CorporativeProcessResult totalProcessResult = null;
		List<BlockCorporativeResult> blockDocuments = null;
		List<ReportCorporativeResult> reportOperations = null;				
		Map<String, Object[]> holderAccAdjustments = corporateExecutorService.getHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.IND_YES, ImportanceEventType.SECURITIES_EXCISION.equals(processType));
		for (Object[] arrObj : distinctHolders) {
			totalProcessResult = new CorporativeProcessResult();
			String holderAlternateCode = (String) arrObj[0];
			BigDecimal totalBalance = ((BigDecimal) arrObj[1]);
			BigDecimal roundedTotal = BigDecimal.ZERO;
			if(ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
				for(SecuritiesCorporative objSecuritiesCorporatives : corpOperation.getSecuritiesCorporatives()){
					roundedTotal = CorporateProcessUtils.calculateByFactor(totalBalance,objSecuritiesCorporatives.getDeliveryFactor(), 
							corpOperation.getIndRound());
					objSecuritiesCorporatives.setRoundedTotal(roundedTotal);
				}
			}else{
				roundedTotal = CorporateProcessUtils.calculateByFactor(totalBalance,corpOperation.getDeliveryFactor(), 
						corpOperation.getIndRound());
			}			
			totalProcessResult.setTotalBalance(roundedTotal);
			totalProcessResult.setCorporativeOperation(corpOperation);
			
			List<Object[]> holderAccounts =corporateExecutorService.getBalancesForHolder(corpOperation.getSecurities().getIdSecurityCodePk(), holderAlternateCode);
			
			Security sourceSecurity = null;
			if(targetSecurity != null && targetSecurity.getIdSecurityCodePk() != null){
				totalProcessResult.setSecurity(targetSecurity);
				sourceSecurity = corpOperation.getSecurities();
				corpOperation.setSecurities(targetSecurity);
			}else{				
				totalProcessResult.setSecurity(corpOperation.getSecurities());
			}
			
			if (holderAccounts != null && holderAccounts.size() > 0) {
				List<CorporativeProcessResult> holderAccOrigin = null, holderAccResults = null;				
				if(destinyBalances){
					holderAccResults = fixer.fixBalances(holderAccounts, corpOperation, corpOperation.getDeliveryFactor(), 
						corpOperation.getIndRound(), false, false, true, processType,null);
					if(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.equals(processType) 
							|| ImportanceEventType.SECURITIES_UNIFICATION.equals(processType)
							|| ImportanceEventType.SECURITIES_FUSION.equals(processType)){
						corpOperation.setSecurities(sourceSecurity);
					}
				}
				if(originBalances){
					holderAccOrigin =  fixer.fillProcessResultsForOrigin(holderAccounts, corpOperation);
				}
				BigDecimal tmpDelFactor = null;
				if(holderAccOrigin != null || holderAccResults != null){			
					if(holderAccResults != null && holderAccOrigin != null && (
							ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.equals(processType) 
							|| ImportanceEventType.SECURITIES_UNIFICATION.equals(processType)
							|| ImportanceEventType.SECURITIES_FUSION.equals(processType))){
						holderAccResults.addAll(holderAccOrigin);
						for (CorporativeProcessResult corpProcessResults : holderAccResults){
							tmpDelFactor = corpOperation.getDeliveryFactor();
							if(CorporateProcessUtils.hasBlockBalances(corpProcessResults)){
								List<Object[]> blockedDocs =corporateExecutorService.getBlockBalanceDetails(corpProcessResults.getParticipant().getIdParticipantPk(), 
											corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
											corpOperation.getSecurities().getIdSecurityCodePk(),AffectationStateType.BLOCKED.getCode(), blockLevel);
								
								if(blockedDocs != null && blockedDocs.size() > 0){
									if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_ORIGIN)){
										if(corpProcessResults.getBlockCorporativeResults() == null){
											corpProcessResults.setBlockCorporativeResults(new ArrayList<BlockCorporativeResult>());
										}
										corpProcessResults.getBlockCorporativeResults()
										.addAll(documentFixer.fillOriginBlockCorporativeResult(blockedDocs, corpProcessResults,null));
									}
									if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_DESTINY)){
										blockDocuments = documentFixer.fixDocuments(corpProcessResults, blockedDocs, tmpDelFactor,
												corpOperation.getIndRound(), false, false, true, fixer, processType,null);
										corpProcessResults.setBlockCorporativeResults(blockDocuments);
									}
								}
							}
							if(processReportOperations && CorporateProcessUtils.hasReportBalances(corpProcessResults)){
								List<Object[]> reportOper = corporateExecutorService.getBalancesReportOperations(corpProcessResults.getParticipant().getIdParticipantPk(), 
											corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
											corpOperation.getSecurities().getIdSecurityCodePk());
								
								if(reportOper != null && reportOper.size() > 0){
									if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_ORIGIN)){
										if(corpProcessResults.getReportCorporativeResults() == null){
											corpProcessResults.setReportCorporativeResults(new ArrayList<ReportCorporativeResult>());
										}
										corpProcessResults.getReportCorporativeResults()
											.addAll(operationFixer.fillOriginOperations(reportOper, corpProcessResults,null));
									}
									if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_DESTINY)){
										reportOperations = operationFixer.fixOperations(corpProcessResults, reportOper, tmpDelFactor, 
												corpOperation.getIndRound(), false, false,true, fixer,null);
										corpProcessResults.setReportCorporativeResults(reportOperations);
									}
								}
							}
							
							if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_ORIGIN)){
								corporateExecutorService.create(corpProcessResults, loggerUser);
							}
							if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_DESTINY)){
								corporateExecutorService.create(corpProcessResults, loggerUser);
							}
						}
					}else if(holderAccOrigin != null && ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
						for (CorporativeProcessResult corpProcessResults : holderAccOrigin){
							// creamos result valor origen
							if(CorporateProcessUtils.hasBlockBalances(corpProcessResults)){
								List<Object[]> blockedDocs =corporateExecutorService.getBlockBalanceDetails(corpProcessResults.getParticipant().getIdParticipantPk(), 
											corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
											corpOperation.getSecurities().getIdSecurityCodePk(),AffectationStateType.BLOCKED.getCode(), blockLevel);								
								if(blockedDocs != null && blockedDocs.size() > 0){
									if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_ORIGIN)){
										if(corpProcessResults.getBlockCorporativeResults() == null){
											corpProcessResults.setBlockCorporativeResults(new ArrayList<BlockCorporativeResult>());
										}
										corpProcessResults.getBlockCorporativeResults()
										.addAll(documentFixer.fillOriginBlockCorporativeResult(blockedDocs, corpProcessResults,null));
									}									
								}
							}
							if(processReportOperations && CorporateProcessUtils.hasReportBalances(corpProcessResults)){
								List<Object[]> reportOper = corporateExecutorService.getBalancesReportOperations(corpProcessResults.getParticipant().getIdParticipantPk(), 
											corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
											corpOperation.getSecurities().getIdSecurityCodePk());
								
								if(reportOper != null && reportOper.size() > 0){
									if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_ORIGIN)){
										if(corpProcessResults.getReportCorporativeResults() == null){
											corpProcessResults.setReportCorporativeResults(new ArrayList<ReportCorporativeResult>());
										}
										corpProcessResults.getReportCorporativeResults()
											.addAll(operationFixer.fillOriginOperations(reportOper, corpProcessResults,null));
									}									
								}								
							}
							
							if(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_ORIGIN)){
								corporateExecutorService.create(corpProcessResults, loggerUser);
							}
							
						// creamos results valores destino
						sourceSecurity = corpOperation.getSecurities();	
						for(SecuritiesCorporative objSecuritiesCorporatives : corpOperation.getSecuritiesCorporatives()){
							totalProcessResult.setTotalBalance(objSecuritiesCorporatives.getRoundedTotal());
							totalProcessResult.setCorporativeOperation(objSecuritiesCorporatives.getCorporativeOperation());
							totalProcessResult.setSecurity(objSecuritiesCorporatives.getSecurity());
							corpOperation.setSecurities(objSecuritiesCorporatives.getSecurity());
							holderAccResults = fixer.fixBalances(holderAccounts, corpOperation, objSecuritiesCorporatives.getDeliveryFactor(), 
									corpOperation.getIndRound(), false, false, true, processType,null);
							if(holderAccResults!=null){
								for (CorporativeProcessResult corpProcessResultsAux : holderAccResults){	
									tmpDelFactor = objSecuritiesCorporatives.getDeliveryFactor();
									if(CorporateProcessUtils.hasBlockBalances(corpProcessResultsAux)){
										List<Object[]> blockedDocs =corporateExecutorService.getBlockBalanceDetails(corpProcessResultsAux.getParticipant().getIdParticipantPk(), 
												corpProcessResultsAux.getHolderAccount().getIdHolderAccountPk(), 
												objSecuritiesCorporatives.getSecurity().getIdSecurityCodePk(),AffectationStateType.BLOCKED.getCode(), blockLevel);										
										if(blockedDocs != null && blockedDocs.size() > 0){											
											if(corpProcessResultsAux.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_DESTINY)){
												blockDocuments = documentFixer.fixDocuments(corpProcessResultsAux, blockedDocs, tmpDelFactor,
														corpOperation.getIndRound(), false, false, true, fixer, processType,null);
												corpProcessResultsAux.setBlockCorporativeResults(blockDocuments);
											}
										}
									}
									if(processReportOperations && CorporateProcessUtils.hasReportBalances(corpProcessResultsAux)){
										List<Object[]> reportOper = corporateExecutorService.getBalancesReportOperations(corpProcessResultsAux.getParticipant().getIdParticipantPk(), 
												corpProcessResultsAux.getHolderAccount().getIdHolderAccountPk(), 
												objSecuritiesCorporatives.getSecurity().getIdSecurityCodePk());										
										if(reportOper != null && reportOper.size() > 0){											
											if(corpProcessResultsAux.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_DESTINY)){
												reportOperations = operationFixer.fixOperations(corpProcessResultsAux, reportOper, tmpDelFactor, 
														corpOperation.getIndRound(), false, false,true, fixer,null);
												corpProcessResultsAux.setReportCorporativeResults(reportOperations);
											}
										}
									}	
									if(corpProcessResultsAux.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_DESTINY)){
										corporateExecutorService.create(corpProcessResultsAux, loggerUser);
									}
								}
							}
						}
						corpOperation.setSecurities(sourceSecurity);
					}
					}else{
						for (CorporativeProcessResult corpProcessResults : holderAccResults != null?holderAccResults:holderAccOrigin) {
							CorporativeProcessResult orginCorpProcessResults = null;
							if(holderAccResults != null && holderAccOrigin != null){
								for(CorporativeProcessResult tempRes : holderAccOrigin){
									if(tempRes.getHolderAccount().getIdHolderAccountPk().equals(corpProcessResults.getHolderAccount().getIdHolderAccountPk())){
										orginCorpProcessResults = tempRes;
										break;
									}
								}
							}else{
								orginCorpProcessResults = corpProcessResults;
							}
							if(destinyBalances){
								tmpDelFactor = CorporateProcessUtils.getHolderAdjustmentFactor(holderAccAdjustments, corpProcessResults, ImportanceEventType.SECURITIES_EXCISION.equals(processType));
							}
							if(tmpDelFactor == null){
								tmpDelFactor = corpOperation.getDeliveryFactor();
							}

							if(CorporateProcessUtils.hasBlockBalances(corpProcessResults)){
								List<Object[]> blockedDocs =corporateExecutorService.getBlockBalanceDetails(corpProcessResults.getParticipant().getIdParticipantPk(), 
											corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
											corpOperation.getSecurities().getIdSecurityCodePk(),AffectationStateType.BLOCKED.getCode(), blockLevel);
								
								if(blockedDocs != null && blockedDocs.size() > 0){
									if(originBalances){
										if(orginCorpProcessResults.getBlockCorporativeResults() == null){
											orginCorpProcessResults.setBlockCorporativeResults(new ArrayList<BlockCorporativeResult>());
										}
										orginCorpProcessResults.getBlockCorporativeResults()
										.addAll(documentFixer.fillOriginBlockCorporativeResult(blockedDocs, orginCorpProcessResults,null));
									}
									if(destinyBalances){
										blockDocuments = documentFixer.fixDocuments(corpProcessResults, blockedDocs, tmpDelFactor,
												corpOperation.getIndRound(), false, false, true, fixer, processType,null);
										corpProcessResults.setBlockCorporativeResults(blockDocuments);
									}
								}
							}
							if(processReportOperations && CorporateProcessUtils.hasReportBalances(corpProcessResults)){
								List<Object[]> reportOper = corporateExecutorService.getBalancesReportOperations(corpProcessResults.getParticipant().getIdParticipantPk(), 
											corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
											corpOperation.getSecurities().getIdSecurityCodePk());
								
								if(reportOper != null && reportOper.size() > 0){
									if(originBalances){
										if(orginCorpProcessResults.getReportCorporativeResults() == null){
											orginCorpProcessResults.setReportCorporativeResults(new ArrayList<ReportCorporativeResult>());
										}
										orginCorpProcessResults.getReportCorporativeResults()
											.addAll(operationFixer.fillOriginOperations(reportOper, orginCorpProcessResults,null));
									}
									if(destinyBalances){
										reportOperations = operationFixer.fixOperations(corpProcessResults, reportOper, tmpDelFactor, 
												corpOperation.getIndRound(), false, false,true, fixer,null);
										corpProcessResults.setReportCorporativeResults(reportOperations);
									}
								}
							}
							
							if(originBalances){
								corporateExecutorService.create(orginCorpProcessResults, loggerUser);
							}
							if(destinyBalances){
								corporateExecutorService.create(corpProcessResults, loggerUser);
							}
							
						}
					}
					holderAccounts.clear();
					holderAccounts = null;							
				}
			}
		}
		if(processReportOperations){
			List<Object[]> sumReported = null, buyerOperations = null; 
			
			sumReported = corporateExecutorService.getSellerBuyyerSumBalancesReportOperations(corpOperation.getSecurities().getIdSecurityCodePk(), 
					corpOperation.getIdCorporativeOperationPk(),
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
					ParticipantRoleType.SELL.getCode(), ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.IND_NO_REMAINDER, 
					CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY);
			buyerOperations = corporateExecutorService.getSellerBalanceReportOperations(corpOperation.getSecurities().getIdSecurityCodePk(), 
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(),
					ParticipantRoleType.BUY.getCode());
			
			if(sumReported != null && !sumReported.isEmpty()){
				List<ReportCorporativeResult> orginReportOperations = null;
				if(originBalances){
					orginReportOperations = buyerOperationFixer.fillOriginOperations(buyerOperations, corpOperation,null);
					corporateExecutorService.saveAll(orginReportOperations, loggerUser);
				}
				if(destinyBalances){
					reportOperations = buyerOperationFixer.fixOperations(sumReported, buyerOperations, corpOperation, 
							corpOperation.getIndRound(), false, false, true, fixer,null);
					corporateExecutorService.saveAll(reportOperations, loggerUser);
				}
				corporateExecutorService.updateReportedBalanceofCorporateResults(corpOperation.getIdCorporativeOperationPk(), 
						GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
						ParticipantRoleType.SELL.getCode(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
				corporateExecutorService.updateReportedOperationsofCorporateResult(corpOperation.getIdCorporativeOperationPk(), 
						GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
						ParticipantRoleType.SELL.getCode(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			}
		}
		corporateExecutorService.updateHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO, CorporateProcessConstants.IND_YES, CorporateProcessConstants.IND_YES, loggerUser);

	}

	
	/**
	 * Metodo que ejecuta definitivo de los eventos corporativos especiales.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executeDefinitiveProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started definitive process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());		
		if(!ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.equals(processType)){
			BigDecimal nominalValue = null;
			if(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.equals(processType)){
				nominalValue = corpOperation.getNewNominalValue();
			}
			Security targetSecurity = null;
			if(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.equals(processType) 
					|| ImportanceEventType.SECURITIES_UNIFICATION.equals(processType)
					|| ImportanceEventType.SECURITIES_FUSION.equals(processType)){
				targetSecurity = corporateExecutorService.getTargetSecurity(corpOperation.getIdCorporativeOperationPk());
			}
			if(ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
				List<SecuritiesCorporative> lstSecuritiesCorporative = corporateExecutorService.getExcisionSecurities(corpOperation.getIdCorporativeOperationPk());
				if(lstSecuritiesCorporative!=null){
					corpOperation.setSecuritiesCorporatives(lstSecuritiesCorporative);					
				}			
			}
			List<Object> participants = corporateExecutorService.getDistinctParticpantsForDefinitive(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			if(participants !=null && !participants.isEmpty()){
				for(Object part : participants){
					Long participant = (Long)part;
					//transaction for participant
					this.specialCorporateProcessExecutor.executeDefinitiveProcessAccounts(corpOperation, processType, participant, loggerUser, targetSecurity, nominalValue);
				}
				
			}
			if(ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
				this.specialCorporateProcessExecutor.executeDefinitiveProcessSecuritiesForExcision(corpOperation, loggerUser);
			}else{
				//change security amounts is possible after fail definitive
				BigDecimal totalBalance =(BigDecimal) corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
						CorporateProcessConstants.IND_NO_REMAINDER, 
						CorporateProcessConstants.DEFINITIVE_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY, null);
				if((totalBalance != null && (totalBalance.compareTo(BigDecimal.ZERO) > 0 ) ||(totalBalance ==null && ImportanceEventType.SECURITIES_EXCLUSION.equals(processType)))){
					this.specialCorporateProcessExecutor.executeDefinitiveProcessSecurities(corpOperation, processType, totalBalance, targetSecurity, loggerUser, nominalValue);
				}
			}			
			
			if(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.equals(processType)){
				//obtenemos todos los corporativos activos asociados al valor
				List<CorporativeOperation> lstCorporativeOperation = corporateExecutorService.findCorpOperationRegistered(corpOperation.getSecurities().getIdSecurityCodePk());
				//validamos lista de corporativos activos asociados al valor
				if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)){
					//recorremos lista corporativos activos asociados al valor
					for(CorporativeOperation corporativeOperation:lstCorporativeOperation){
						//actualizamos estado de corporativo (ANULADO)
						corporativeOperation.setState(CorporateProcessStateType.ANNULLED.getCode());
						corporateExecutorService.update(corporativeOperation, loggerUser);
					}
				}	
			}			
		}else{
			List<Object> participants = corporateExecutorService.getDistinctParticpantsForDefinitive(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			if(participants !=null && !participants.isEmpty()){
				for(Object part : participants){
					Long participant = (Long)part;
					//transaction for participant
					this.specialCorporateProcessExecutor.executeDefinitiveProcessUpdates(corpOperation,participant, loggerUser);
				}
			}	
			
			//SAVE SECURITY HISTORY BALANCE
			SecurityHistoryBalance securityHistoryBalance = new SecurityHistoryBalance();
			Security objSecurity = corporateExecutorService.getSecurity(corpOperation.getSecurities().getIdSecurityCodePk());
			securityHistoryBalance.setSecurity(new Security());
			securityHistoryBalance.getSecurity().setIdSecurityCodePk(objSecurity.getIdSecurityCodePk());
			securityHistoryBalance.setUpdateBalanceDate(CommonsUtilities.currentDateTime());
			securityHistoryBalance.setNominalValue(objSecurity.getCurrentNominalValue());
			securityHistoryBalance.setPlacedAmount(objSecurity.getPlacedAmount());
			securityHistoryBalance.setPlacedBalance(objSecurity.getPlacedBalance());
			securityHistoryBalance.setShareCapital(objSecurity.getShareCapital());
			securityHistoryBalance.setShareBalance(objSecurity.getShareBalance());
			securityHistoryBalance.setPhysicalBalance(objSecurity.getPhysicalBalance());
			securityHistoryBalance.setDesmaterializedBalance(objSecurity.getDesmaterializedBalance());
			securityHistoryBalance.setAmortizationAmount(objSecurity.getAmortizationAmount());
			corporateExecutorService.create(securityHistoryBalance);		
		}
		
		return true;
	}
	
	/**
	 *  Metodo que ejecuta movimientos de saldos a nivel de cuenta de los eventos corporativos especiales.
	 *
	 * @param corpOperation the corp operation
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessUpdates(CorporativeOperation corpOperation,Long participant,LoggerUser loggerUser) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		AccountsComponentService accountsComponentService=executorComponentServiceBean.get();
		List<Object[]> blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForUnblockDefinitive(corpOperation.getIdCorporativeOperationPk(), participant, corpOperation.getSecurities().getIdSecurityCodePk(), 
				CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_ORIGIN);
		//movements about block documents
		if(blockDocuments != null && !blockDocuments.isEmpty()){
			for(Object[] data : blockDocuments){
				BigDecimal quantity = (BigDecimal)data[2];
				Long blockOperationId = (Long)data[3];
				HolderAccountBalanceTO account = new HolderAccountBalanceTO();
				account.setIdHolderAccount((Long)data[5]);
				account.setIdParticipant((Long)data[6]);
				account.setIdSecurityCode((String)data[7]);
				account.setStockQuantity(quantity);
				account.setFinalDate(corpOperation.getDeliveryDate());
				List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
				accounts.add(account);
				Long refBlockOperationId = null;
				BlockOperation blockOperation = corporateExecutorService.find(BlockOperation.class, blockOperationId);
				List<BlockMarketFactOperation> blockMarketFatcs = blockOperation.getBlockMarketFactOperations();
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
					//movements marketfact for each blockmarketfact
					MarketFactAccountTO mktFact;
					for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
						if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
							mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(blockMarketFact.getMarketDate());
							mktFact.setMarketPrice(blockMarketFact.getMarketPrice());
							mktFact.setMarketRate(blockMarketFact.getMarketRate());
							mktFact.setQuantity(blockMarketFact.getActualBlockBalance());
							account.getLstMarketFactAccounts().add(mktFact);
						}
					}
				}						
				//movements unblock origin balances
				AccountsComponentTO accountsComponentTO = CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
						blockOperationId, refBlockOperationId, null, null, ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR, accounts, null, CorporateProcessUtils.getBlockBalance((Integer)data[0],(Integer)data[8]),idepositarySetup.getIndMarketFact());
				//change default operationType to unblockOperationType
				accountsComponentTO.setIdOperationType(CorporateProcessUtils.getOperationUnblock(CorporateProcessUtils.getBlockBalance((Integer)data[0],(Integer)data[8]), accountsComponentTO.getIdOperationType()));
				accountsComponentService.executeAccountsComponent(accountsComponentTO);
			}					
		}
		//execute movements in accounts origin(subs) and destiny(add)
		AccountsComponentTO accountsComponentTO = fixer.generateAccountComponent(
				corporateExecutorService.getCorporateResultsForDefinitiveProcess(corpOperation.getIdCorporativeOperationPk(), participant, 
						CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null), ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR, corpOperation);
		accountsComponentService.executeAccountsComponent(accountsComponentTO);							
	
		blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForDefinitive(corpOperation.getIdCorporativeOperationPk(), 
				participant, corpOperation.getSecurities().getIdSecurityCodePk(), CorporateProcessConstants.IND_NO_REMAINDER, 
				CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY);
		
		//create new or update block movements/operations
		if(blockDocuments != null && !blockDocuments.isEmpty()){
				for(Object[] data : blockDocuments){
					 BigDecimal quantity = (BigDecimal)data[22];
					 HolderAccountBalanceTO accountBalanceTO = new HolderAccountBalanceTO();								 
					 accountBalanceTO.setIdParticipant((Long)data[10]);
					 accountBalanceTO.setIdSecurityCode((String)data[11]);
					 accountBalanceTO.setIdHolderAccount((Long)data[12]);
					 accountBalanceTO.setStockQuantity(quantity);
					 List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
					 accounts.add(accountBalanceTO);
					 Long blockOperationId = (Long)data[13];
					 corporateExecutorService.updateBlockOperation(blockOperationId, quantity, false, false, null, corpOperation.getNewNominalValue(), loggerUser);
					 if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
						 BlockOperation blockOperation = corporateExecutorService.find(BlockOperation.class, blockOperationId);
							for(BlockMarketFactOperation blockMarketFact : blockOperation.getBlockMarketFactOperations()){
								if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
									blockMarketFact.setActualBlockBalance(new BigDecimal(0));
									corporateExecutorService.update(blockMarketFact);
								}
							}
						//new one blockMarketFact
						BlockMarketFactOperation blockMarketFactOperation = new BlockMarketFactOperation();
						blockMarketFactOperation.setMarketDate(corpOperation.getMarketDate());
						blockMarketFactOperation.setMarketPrice(corpOperation.getMarketPrice());
						blockMarketFactOperation.setOriginalBlockBalance(quantity);
						blockMarketFactOperation.setActualBlockBalance(quantity);
						blockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
						blockMarketFactOperation.setBlockOperation(blockOperation);
						blockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
						blockMarketFactOperation.setRegistryUser(loggerUser.getUserName());
						corporateExecutorService.create(blockMarketFactOperation);
					 if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
							//movements marketfact
							MarketFactAccountTO mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(corpOperation.getMarketDate());
							mktFact.setMarketPrice(corpOperation.getMarketPrice());
							mktFact.setQuantity(quantity);
							accountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
							accountBalanceTO.getLstMarketFactAccounts().add(mktFact);
						}
					 accountsComponentService.executeAccountsComponent(CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
							blockOperationId, null, null, null, ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR, null, accounts, CorporateProcessUtils.getBlockBalance((Integer)data[0], (Integer)data[24]),idepositarySetup.getIndMarketFact()));
				}
			}
		}
		corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), 
				participant, true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null, loggerUser);		
		BigDecimal circulationCapital = null;
		if(corpOperation.getNewNominalValue()!= null && corpOperation.getNewCirculationBalance()!=null){
			circulationCapital = corpOperation.getNewNominalValue().multiply(corpOperation.getNewCirculationBalance()).setScale(2, RoundingMode.DOWN);
		}
		corporateExecutorService.updateSecurity(corpOperation.getSecurities().getIdSecurityCodePk(), null, 
				null, corpOperation.getNewNominalValue(), corpOperation.getNewShareCapital(),circulationCapital, loggerUser);
		
	}
	
	/**
	 * Metodo que ejecuta movimientos de saldos a nivel de valor de los eventos corporativos especiales.
	 *
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param totalBalance the total balance
	 * @param targetSecurity the target security
	 * @param loggerUser the logger user
	 * @param nominalValue the nominal value
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessSecurities(CorporativeOperation corpOperation,ImportanceEventType processType,BigDecimal totalBalance,
			Security targetSecurity,LoggerUser loggerUser,BigDecimal nominalValue) throws ServiceException{
		SecuritiesComponentService securitiesComponentService=securitiesComponentServiceBean.get();
		fixer.setLoggerUserForTransationRegistry();
		Integer secState = null;
		BigDecimal orgShareCapital = null,orgCirculationCapital=null, desShareCapital = null, circulationCapital = null;
		Date retirementDate = null;
		String orgSecurity = null;
		orgSecurity = corpOperation.getSecurities().getIdSecurityCodePk();
		orgShareCapital = corpOperation.getNewShareCapital();
		if(ImportanceEventType.SECURITIES_EXCLUSION.equals(processType)){
			orgShareCapital = BigDecimal.ZERO;
			orgCirculationCapital = BigDecimal.ZERO;
			retirementDate = CommonsUtilities.currentDateTime();
			secState = SecurityStateType.EXCLUDED.getCode();
		}else if(ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
			boolean validate = false;
			if(corpOperation.getSecuritiesCorporatives()!=null && corpOperation.getSecuritiesCorporatives().size()>1){					
				for(SecuritiesCorporative objSecuritiesCorporative : corpOperation.getSecuritiesCorporatives()){
					if(corpOperation.getSecurities().getIdSecurityCodePk().equals(objSecuritiesCorporative.getSecurity().getIdSecurityCodePk())){
						validate = true;
						break;
					}
				}
			}
			if(!validate){
				orgShareCapital = BigDecimal.ZERO;
				orgCirculationCapital = BigDecimal.ZERO;
				retirementDate = CommonsUtilities.currentDateTime();
				secState = SecurityStateType.ESCISIONADO.getCode();
			}				
		}else{	
			retirementDate = CommonsUtilities.currentDateTime();
			if(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.equals(processType)){
				orgShareCapital = BigDecimal.ZERO;
				orgCirculationCapital = BigDecimal.ZERO;
				secState = SecurityStateType.REDEEMED.getCode();
			}else if(ImportanceEventType.SECURITIES_UNIFICATION.equals(processType)){
				orgShareCapital = BigDecimal.ZERO;
				orgCirculationCapital = BigDecimal.ZERO;
				secState = SecurityStateType.UNIFIED.getCode();
			}else if(ImportanceEventType.SECURITIES_FUSION.equals(processType)){
				orgShareCapital = BigDecimal.ZERO;
				orgCirculationCapital = BigDecimal.ZERO;
				secState = SecurityStateType.FUSED.getCode();
			}				
		}						
		if(orgSecurity != null){
			SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, corpOperation.getSecurities().getShareCapital(), 
					corpOperation.getSecurities().getCirculationBalance(), orgSecurity);
			//origin security is from full Object because error in cache L1
			secOpe.setSecurities(corpOperation.getSecurities());
			corporateExecutorService.create(secOpe, loggerUser);	
			SecuritiesComponentTO securitiesComponent = CorporateProcessUtils.getSecuritiesComponent(processType, secOpe, 
					corpOperation.getSecurities().getPhysicalBalance(), corpOperation.getSecurities().getDesmaterializedBalance(), CorporateProcessConstants.BALANCE_ORIGIN);
			//execute component
			securitiesComponentService.executeSecuritiesComponent(securitiesComponent);
			if(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.equals(processType) 
					|| (ImportanceEventType.CAPITAL_REDUCTION.equals(processType) && secState == null) ){
				BigDecimal circulation = BigDecimal.ZERO , capital = BigDecimal.ZERO;
				if(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.equals(processType)){
					circulation = 	totalBalance;
					BigDecimal shareBalance = totalBalance;
					capital =  shareBalance.multiply(corpOperation.getNewNominalValue());
					orgShareCapital = capital;
					orgCirculationCapital = circulation.multiply(corpOperation.getNewNominalValue());
				}else if(ImportanceEventType.CAPITAL_REDUCTION.equals(processType)){
					circulation = 	totalBalance;
					BigDecimal shareBalance = totalBalance;
					capital =  shareBalance.multiply(corpOperation.getSecurities().getCurrentNominalValue());
					orgShareCapital = capital;
					orgCirculationCapital = circulation.multiply(corpOperation.getSecurities().getCurrentNominalValue());
				}
				
				secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, capital, 
						circulation, orgSecurity);
				secOpe.setSecurities(corpOperation.getSecurities());
				corporateExecutorService.create(secOpe, loggerUser);
				securitiesComponent = CorporateProcessUtils.getSecuritiesComponent(processType, secOpe, 
						circulation.subtract(totalBalance), totalBalance, CorporateProcessConstants.BALANCE_DESTINY);
				//execute component
				securitiesComponentService.executeSecuritiesComponent(securitiesComponent);
			}
		}
		if(targetSecurity != null){
			BigDecimal circulation = 	totalBalance;
			BigDecimal shareBalance = totalBalance;
			BigDecimal capital =  shareBalance.multiply(targetSecurity.getCurrentNominalValue());
			circulationCapital = circulation.multiply(targetSecurity.getCurrentNominalValue());
			if(ImportanceEventType.SECURITIES_FUSION.equals(processType) || 
					ImportanceEventType.SECURITIES_UNIFICATION.equals(processType)){
				desShareCapital = targetSecurity.getShareCapital().add(capital);
				circulationCapital = targetSecurity.getCirculationAmount().add(circulationCapital);
			}else{
				desShareCapital = capital;
			}
			
			SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType,capital, 
					circulation, targetSecurity.getIdSecurityCodePk());
			secOpe.setSecurities(targetSecurity);
			corporateExecutorService.create(secOpe, loggerUser);	
			SecuritiesComponentTO securitiesComponent = CorporateProcessUtils.getSecuritiesComponent(processType, secOpe, 
					circulation.subtract(totalBalance), totalBalance, CorporateProcessConstants.BALANCE_DESTINY);
			//execute component final security destiny
			securitiesComponentService.executeSecuritiesComponent(securitiesComponent);
		}
	
		if(orgSecurity != null && (secState != null || nominalValue != null || orgShareCapital != null || retirementDate != null)){
			corporateExecutorService.updateSecurity(corpOperation.getSecurities().getIdSecurityCodePk(), retirementDate, 
					secState, nominalValue, orgShareCapital, orgCirculationCapital,loggerUser);
		}
		if(targetSecurity != null && desShareCapital != null){
			corporateExecutorService.updateSecurity(targetSecurity.getIdSecurityCodePk(), null, null, null, desShareCapital,circulationCapital, loggerUser);
		}

	}
	
	/**
	 * Metodo que ejecuta movimientos de saldos a nivel de valor de los eventos corporativos de escision.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessSecuritiesForExcision(CorporativeOperation corpOperation,LoggerUser loggerUser) throws ServiceException{
		SecuritiesComponentService securitiesComponentService=securitiesComponentServiceBean.get();
		fixer.setLoggerUserForTransationRegistry();
		Integer secState = null;
		BigDecimal orgShareCapital = null,circulationCapital = null, desShareCapital = null,desCirculationCapital = null ;
		Date retirementDate = null;
		String orgSecurity = null;
		orgSecurity = corpOperation.getSecurities().getIdSecurityCodePk();
		boolean validate = false;
		if(corpOperation.getSecuritiesCorporatives()!=null && corpOperation.getSecuritiesCorporatives().size()>1){					
			for(SecuritiesCorporative objSecuritiesCorporative : corpOperation.getSecuritiesCorporatives()){
				if(corpOperation.getSecurities().getIdSecurityCodePk().equals(objSecuritiesCorporative.getSecurity().getIdSecurityCodePk())){
					validate = true;
					break;
				}
			}
		}
		if(!validate){
			orgShareCapital = BigDecimal.ZERO;
			circulationCapital = BigDecimal.ZERO;
			retirementDate = CommonsUtilities.currentDateTime();
			secState = SecurityStateType.ESCISIONADO.getCode();
		}								
		if(orgSecurity != null){
			SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, ImportanceEventType.SECURITIES_EXCISION, corpOperation.getSecurities().getShareCapital(), 
					corpOperation.getSecurities().getCirculationBalance(), orgSecurity);
			//origin security is from full Object because error in cache L1
			secOpe.setSecurities(corpOperation.getSecurities());
			corporateExecutorService.create(secOpe, loggerUser);	
			SecuritiesComponentTO securitiesComponent = CorporateProcessUtils.getSecuritiesComponent(ImportanceEventType.SECURITIES_EXCISION, secOpe, 
					corpOperation.getSecurities().getPhysicalBalance(), corpOperation.getSecurities().getDesmaterializedBalance(), CorporateProcessConstants.BALANCE_ORIGIN);
			//execute component
			securitiesComponentService.executeSecuritiesComponent(securitiesComponent);
		}
		if(orgSecurity != null && (secState != null|| orgShareCapital != null || retirementDate != null)){
			corporateExecutorService.updateSecurity(corpOperation.getSecurities().getIdSecurityCodePk(), retirementDate, 
					secState, null, orgShareCapital, circulationCapital,loggerUser);
		}
		for(SecuritiesCorporative objSecuritiesCorporatives : corpOperation.getSecuritiesCorporatives()){
			//change security amounts is possible after fail definitive
			BigDecimal totalBalance =(BigDecimal) corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
					CorporateProcessConstants.IND_NO_REMAINDER,CorporateProcessConstants.DEFINITIVE_PROCESSED, 
					CorporateProcessConstants.BALANCE_DESTINY, objSecuritiesCorporatives.getSecurity().getIdSecurityCodePk());
			if(totalBalance != null && (totalBalance.compareTo(BigDecimal.ZERO) > 0 )){
				BigDecimal circulation = 	totalBalance;
				BigDecimal shareBalance = totalBalance;
				BigDecimal capital =  shareBalance.multiply(objSecuritiesCorporatives.getSecurity().getCurrentNominalValue());
				desShareCapital = capital;
				circulationCapital = circulation.multiply(objSecuritiesCorporatives.getSecurity().getCurrentNominalValue());				
				SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, ImportanceEventType.SECURITIES_EXCISION, capital, 
						circulation,  objSecuritiesCorporatives.getSecurity().getIdSecurityCodePk());
				secOpe.setSecurities( objSecuritiesCorporatives.getSecurity());
				corporateExecutorService.create(secOpe, loggerUser);							
				SecuritiesComponentTO securitiesComponent = CorporateProcessUtils.getSecuritiesComponent(ImportanceEventType.SECURITIES_EXCISION, secOpe, 
						circulation.subtract(totalBalance), totalBalance, CorporateProcessConstants.BALANCE_DESTINY);
				//execute component final security destiny
				securitiesComponentService.executeSecuritiesComponent(securitiesComponent);				
				corporateExecutorService.updateSecurity(objSecuritiesCorporatives.getSecurity().getIdSecurityCodePk(), 
						null, null, null,desShareCapital,circulationCapital, loggerUser);
			}
		}
	}
	/**
	 * Metodo que ejecuta movimientos de saldos a nivel de cuenta de los eventos corporativos especiales.
	 *
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @param targetSecurity the target security
	 * @param nominalValue the nominal value
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessAccounts(CorporativeOperation corpOperation,ImportanceEventType processType,Long participant
			,LoggerUser loggerUser,Security targetSecurity,BigDecimal nominalValue) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		AccountsComponentService accountsComponentService=executorComponentServiceBean.get();
		List<Object[]> blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForUnblockDefinitive(corpOperation.getIdCorporativeOperationPk(), participant, corpOperation.getSecurities().getIdSecurityCodePk(), 
				CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_ORIGIN);
		//movements about block documents
		if(blockDocuments != null && !blockDocuments.isEmpty()){
			for(Object[] data : blockDocuments){
				BigDecimal quantity = (BigDecimal)data[2];
				Long blockOperationId = (Long)data[3];
				HolderAccountBalanceTO account = new HolderAccountBalanceTO();
				account.setIdHolderAccount((Long)data[5]);
				account.setIdParticipant((Long)data[6]);
				account.setIdSecurityCode((String)data[7]);
				account.setStockQuantity(quantity);
				account.setFinalDate(corpOperation.getDeliveryDate());
				List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
				accounts.add(account);
				Long refBlockOperationId = null;
				BlockOperation blockOperation = corporateExecutorService.find(BlockOperation.class, blockOperationId);
				List<BlockMarketFactOperation> blockMarketFatcs = blockOperation.getBlockMarketFactOperations();
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
					//movements marketfact for each blockmarketfact
					MarketFactAccountTO mktFact;
					for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
						if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
							mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(blockMarketFact.getMarketDate());
							mktFact.setMarketPrice(blockMarketFact.getMarketPrice());
							mktFact.setMarketRate(blockMarketFact.getMarketRate());
							mktFact.setQuantity(blockMarketFact.getActualBlockBalance());
							account.getLstMarketFactAccounts().add(mktFact);
						}
					}
				}
				if(!ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.equals(processType) && !ImportanceEventType.CAPITAL_REDUCTION.equals(processType)){
					UnblockRequest unblockRequest = documentFixer.prepareUnblockOperation(data, loggerUser.getUserName());	
					if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
						UnblockMarketFactOperation unblockMarketFact;
						for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
							if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
								unblockMarketFact = new UnblockMarketFactOperation();
								unblockMarketFact.setUnblockOperation(unblockRequest.getUnblockOperation().get(0));
								unblockMarketFact.setUnblockQuantity(blockMarketFact.getActualBlockBalance());
								unblockMarketFact.setUnblockMarketState(UnblockMarketOperationStateType.REGISTER.getCode());
								unblockMarketFact.setRegistryUser(loggerUser.getUserName());
								unblockMarketFact.setRegistryDate(CommonsUtilities.currentDateTime());
								unblockMarketFact.setMarketDate(blockMarketFact.getMarketDate());
								unblockMarketFact.setMarketPrice(blockMarketFact.getMarketPrice());
								unblockMarketFact.setMarketRate(blockMarketFact.getMarketRate());
								unblockRequest.getUnblockOperation().get(0).getUnblockMarketFactOperations().add(unblockMarketFact);
							}
						}
					}
					corporateExecutorService.create(unblockRequest, loggerUser);
					corporateExecutorService.updateBlockOperation(blockOperationId, quantity, true, true, AffectationStateType.UNBLOCKED.getCode(), null, loggerUser);
					if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
						for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
							if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
								blockMarketFact.setActualBlockBalance(new BigDecimal(0));
								corporateExecutorService.update(blockMarketFact);
							}
						}
					}
					refBlockOperationId = blockOperationId;
					blockOperationId = unblockRequest.getUnblockOperation().get(0).getIdUnblockOperationPk();
				} 
				//movements unblock origin balances
				AccountsComponentTO accountsComponentTO = CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
						blockOperationId, refBlockOperationId, null, null, processType, accounts, null, CorporateProcessUtils.getBlockBalance((Integer)data[0],(Integer)data[8]),idepositarySetup.getIndMarketFact());
				//change default operationType to unblockOperationType
				accountsComponentTO.setIdOperationType(CorporateProcessUtils.getOperationUnblock(CorporateProcessUtils.getBlockBalance((Integer)data[0],(Integer)data[8]), accountsComponentTO.getIdOperationType()));
				accountsComponentService.executeAccountsComponent(accountsComponentTO);
			}
			
		}
		//execute movements in accounts origin(subs) and destiny(add)
		AccountsComponentTO accountsComponentTO = fixer.generateAccountComponent(
				corporateExecutorService.getCorporateResultsForDefinitiveProcess(corpOperation.getIdCorporativeOperationPk(), participant, 
						CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null), processType, corpOperation);
		accountsComponentService.executeAccountsComponent(accountsComponentTO);
		
		if(ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
			for(SecuritiesCorporative objSecuritiesCorporatives : corpOperation.getSecuritiesCorporatives()){
				String security = objSecuritiesCorporatives.getSecurity().getIdSecurityCodePk();
				blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForDefinitive(corpOperation.getIdCorporativeOperationPk(), 
						participant, security, CorporateProcessConstants.IND_NO_REMAINDER, 
						CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY);
				//create new or update block movements/operations
				if(blockDocuments != null && !blockDocuments.isEmpty()){
					for(Object[] data : blockDocuments){
						 BigDecimal quantity = (BigDecimal)data[22];
						 HolderAccountBalanceTO accountBalanceTO = new HolderAccountBalanceTO();								 
						 accountBalanceTO.setIdParticipant((Long)data[10]);
						 accountBalanceTO.setIdSecurityCode((String)data[11]);
						 accountBalanceTO.setIdHolderAccount((Long)data[12]);
						 accountBalanceTO.setStockQuantity(quantity);
						 List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
						 accounts.add(accountBalanceTO);
						 Long blockOperationId = null;						
						BlockRequest blockRequest = documentFixer.prepareBlockOperation(data, loggerUser.getUserName(), quantity, quantity);	
						if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
							BlockMarketFactOperation blockMarketFactOperation = new BlockMarketFactOperation();
							blockMarketFactOperation.setMarketDate(objSecuritiesCorporatives.getMarketDate());
							blockMarketFactOperation.setMarketPrice(objSecuritiesCorporatives.getMarketPrice());
							blockMarketFactOperation.setOriginalBlockBalance(quantity);
							blockMarketFactOperation.setActualBlockBalance(quantity);
							blockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
							blockMarketFactOperation.setBlockOperation(blockRequest.getBlockOperation().get(0));
							blockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
							blockMarketFactOperation.setRegistryUser(loggerUser.getUserName());
							blockRequest.getBlockOperation().get(0).getBlockMarketFactOperations().add(blockMarketFactOperation);
						}
					 	corporateExecutorService.create(blockRequest, loggerUser);
					 	blockOperationId = blockRequest.getBlockOperation().get(0).getIdBlockOperationPk();						 
						 if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
								//movements marketfact
								MarketFactAccountTO mktFact = new MarketFactAccountTO();
								mktFact.setMarketDate(objSecuritiesCorporatives.getMarketDate());
								mktFact.setMarketPrice(objSecuritiesCorporatives.getMarketPrice());
								mktFact.setQuantity(quantity);
								accountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
								accountBalanceTO.getLstMarketFactAccounts().add(mktFact);
							}
						 accountsComponentService.executeAccountsComponent(CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
								blockOperationId, null, null, null, processType, null, accounts, CorporateProcessUtils.getBlockBalance((Integer)data[0], (Integer)data[24]),idepositarySetup.getIndMarketFact()));
					}
				}
			}
		}else{			
			if(!ImportanceEventType.SECURITIES_EXCLUSION.equals(processType)){
				String security = null;
				if(ImportanceEventType.CAPITAL_REDUCTION.equals(processType) || ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.equals(processType)){
						security = corpOperation.getSecurities().getIdSecurityCodePk();
				}else{
					if(targetSecurity != null && targetSecurity.getIdSecurityCodePk() != null){
						security = targetSecurity.getIdSecurityCodePk();
					}
				}
				blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForDefinitive(corpOperation.getIdCorporativeOperationPk(), 
						participant, security, CorporateProcessConstants.IND_NO_REMAINDER, 
						CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY);
				//create new or update block movements/operations
				if(blockDocuments != null && !blockDocuments.isEmpty()){
					for(Object[] data : blockDocuments){
						 BigDecimal quantity = (BigDecimal)data[22];
						 HolderAccountBalanceTO accountBalanceTO = new HolderAccountBalanceTO();								 
						 accountBalanceTO.setIdParticipant((Long)data[10]);
						 accountBalanceTO.setIdSecurityCode((String)data[11]);
						 accountBalanceTO.setIdHolderAccount((Long)data[12]);
						 accountBalanceTO.setStockQuantity(quantity);
						 List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
						 accounts.add(accountBalanceTO);
						 Long blockOperationId = null;
						 if(!ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.equals(processType) && !ImportanceEventType.CAPITAL_REDUCTION.equals(processType)){
							BlockRequest blockRequest = documentFixer.prepareBlockOperation(data, loggerUser.getUserName(), quantity, quantity);	
							if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
								BlockMarketFactOperation blockMarketFactOperation = new BlockMarketFactOperation();
								blockMarketFactOperation.setMarketDate(corpOperation.getMarketDate());
								blockMarketFactOperation.setMarketPrice(corpOperation.getMarketPrice());
								blockMarketFactOperation.setOriginalBlockBalance(quantity);
								blockMarketFactOperation.setActualBlockBalance(quantity);
								blockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
								blockMarketFactOperation.setBlockOperation(blockRequest.getBlockOperation().get(0));
								blockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
								blockMarketFactOperation.setRegistryUser(loggerUser.getUserName());
								blockRequest.getBlockOperation().get(0).getBlockMarketFactOperations().add(blockMarketFactOperation);
							}
						 	corporateExecutorService.create(blockRequest, loggerUser);
						 	blockOperationId = blockRequest.getBlockOperation().get(0).getIdBlockOperationPk();
						 }else{										 
							 blockOperationId = (Long)data[13];
							 corporateExecutorService.updateBlockOperation(blockOperationId, quantity, false, false, null, nominalValue, loggerUser);
							 if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
								 BlockOperation blockOperation = corporateExecutorService.find(BlockOperation.class, blockOperationId);
									for(BlockMarketFactOperation blockMarketFact : blockOperation.getBlockMarketFactOperations()){
										if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
											blockMarketFact.setActualBlockBalance(new BigDecimal(0));
											corporateExecutorService.update(blockMarketFact);
										}
									}
								//new one blockMarketFact
								BlockMarketFactOperation blockMarketFactOperation = new BlockMarketFactOperation();
								blockMarketFactOperation.setMarketDate(corpOperation.getMarketDate());
								blockMarketFactOperation.setMarketPrice(corpOperation.getMarketPrice());
								blockMarketFactOperation.setOriginalBlockBalance(quantity);
								blockMarketFactOperation.setActualBlockBalance(quantity);
								blockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
								blockMarketFactOperation.setBlockOperation(blockOperation);
								blockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
								blockMarketFactOperation.setRegistryUser(loggerUser.getUserName());
								corporateExecutorService.create(blockMarketFactOperation);
							 }
						 }
						 if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
								//movements marketfact
								MarketFactAccountTO mktFact = new MarketFactAccountTO();
								mktFact.setMarketDate(corpOperation.getMarketDate());
								mktFact.setMarketPrice(corpOperation.getMarketPrice());
								mktFact.setQuantity(quantity);
								accountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
								accountBalanceTO.getLstMarketFactAccounts().add(mktFact);
							}
						 accountsComponentService.executeAccountsComponent(CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
								blockOperationId, null, null, null, processType, null, accounts, CorporateProcessUtils.getBlockBalance((Integer)data[0], (Integer)data[24]),idepositarySetup.getIndMarketFact()));
					}
				}
			}
		}
		
		corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), 
				participant, true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null, loggerUser);

	}
	
}
