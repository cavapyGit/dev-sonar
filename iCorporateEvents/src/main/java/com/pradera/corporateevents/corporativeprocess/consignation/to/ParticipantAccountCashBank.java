package com.pradera.corporateevents.corporativeprocess.consignation.to;

import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;

public class ParticipantAccountCashBank {

	private Long idInstitutionCashAccountPk;
	private Long idInstitutionBankAccountPk;
	private Long idBankPk;
	private String accountNumber;

	private InstitutionCashAccount institutionCashAccount;
	private InstitutionBankAccount institutionBankAccount;
	private Bank bank;
	

	public ParticipantAccountCashBank(InstitutionCashAccount institutionCashAccount, InstitutionBankAccount institutionBankAccount, Bank bank) {
		super();
		this.institutionCashAccount = institutionCashAccount;
		this.institutionBankAccount = institutionBankAccount;
		this.bank = bank;
	}
	
	public ParticipantAccountCashBank(Long idInstitutionCashAccountPk, Long idInstitutionBankAccountPk, Long idBankPk, String accountNumber) {
		super();
		this.idInstitutionCashAccountPk = idInstitutionCashAccountPk;
		this.idInstitutionBankAccountPk = idInstitutionBankAccountPk;
		this.idBankPk = idBankPk;
		this.accountNumber = accountNumber;
	}
	
	public Long getIdInstitutionCashAccountPk() {
		return idInstitutionCashAccountPk;
	}
	public void setIdInstitutionCashAccountPk(Long idInstitutionCashAccountPk) {
		this.idInstitutionCashAccountPk = idInstitutionCashAccountPk;
	}
	public Long getIdInstitutionBankAccountPk() {
		return idInstitutionBankAccountPk;
	}
	public void setIdInstitutionBankAccountPk(Long idInstitutionBankAccountPk) {
		this.idInstitutionBankAccountPk = idInstitutionBankAccountPk;
	}
	public Long getIdBankPk() {
		return idBankPk;
	}
	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public InstitutionCashAccount getInstitutionCashAccount() {
		return institutionCashAccount;
	}
	public void setInstitutionCashAccount(InstitutionCashAccount institutionCashAccount) {
		this.institutionCashAccount = institutionCashAccount;
	}
	public InstitutionBankAccount getInstitutionBankAccount() {
		return institutionBankAccount;
	}
	public void setInstitutionBankAccount(InstitutionBankAccount institutionBankAccount) {
		this.institutionBankAccount = institutionBankAccount;
	}
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
}
