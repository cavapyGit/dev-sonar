package com.pradera.corporateevents.corporativeprocess.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.billing.facade.BillingComponentFacade;
import com.pradera.corporateevents.corporativeprocess.consignation.to.HolderAccountCommissionDataTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.HolderAccountCommissionMovementsTO;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.service.CorporateServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CustodyCommissionServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.CustodyCommission;
import com.pradera.model.corporatives.CustodyCommissionDetail;
import com.pradera.model.corporatives.type.PrimaryBalanceType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.BalanceBehaviourType;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;


// TODO: Auto-generated Javadoc
/**
 * Clase fachada de calculos de retenciones por comision de custodia
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class CustodyCommissionServiceFacade {

	/** The custody commission service. */
	@EJB
	CustodyCommissionServiceBean custodyCommissionService;
	
	/** The corporate service bean. */
	@EJB
	private CorporateServiceBean corporateServiceBean;
	
	/** The billing component facade. */
	@EJB
	private BillingComponentFacade billingComponentFacade;
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Metodo que calcula comisiones por custodia.
	 *
	 * @param user the user
	 * @throws Exception the exception
	 */
	public void calculateCustodyCommission(String user) throws Exception{
		try{
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			//METHOD TO GET THE CORPORATIVE PROCESS WHICH WILL PAY TOMORROW
			List<CorporativeOperation> corporatives = custodyCommissionService.getCorporatives();
			List<Long> corporativeList = new ArrayList<Long>();
			if(Validations.validateListIsNotNullAndNotEmpty(corporatives)){
				for(CorporativeOperation corporative : corporatives){
					corporativeList.add(corporative.getIdCorporativeOperationPk());
				}
			}

			//WE MUST DELETE THE PREVIOUS CALCULATION REALIZED BY CORPORATIVE OPERATION
			if(Validations.validateListIsNotNullAndNotEmpty(corporativeList)){
				List<Long> lstCustodyCommission = custodyCommissionService.getCustodyCommissionID(corporativeList);
				if(Validations.validateListIsNotNullAndNotEmpty(lstCustodyCommission)){
					custodyCommissionService.deletePreviousCustodyCalculation(lstCustodyCommission);
				}
			}

			if(Validations.validateListIsNotNullAndNotEmpty(corporatives)){
				for(CorporativeOperation corporative : corporatives){
					executeCustodyCommissionCalculation(corporative,loggerUser);
				}
			}

		}catch(ServiceException x){
			x.printStackTrace();
		}
	}

	/**
	 * Metodo que ejecuta comisiones por custodia.
	 *
	 * @param corporative the corporative
	 * @param loggerUser the logger user
	 * @throws Exception the exception
	 */
	private void executeCustodyCommissionCalculation(CorporativeOperation corporative, LoggerUser loggerUser) throws Exception{
		List<HolderAccountCommissionDataTO> lstHolderAccountBalance = new ArrayList<HolderAccountCommissionDataTO>();
		Map<Long,Map<Integer,BigDecimal>> mpHolderAccountBalance = new HashMap<Long, Map<Integer,BigDecimal>>();

		Integer interestPaymentModality = corporative.getSecurities().getInterestPaymentModality();
		String securityCode = corporative.getSecurities().getIdSecurityCodePk();
		Long corporativeID = corporative.getIdCorporativeOperationPk();
		if(CapitalPaymentModalityType.AT_MATURITY.getCode().equals(interestPaymentModality)){
			//IF INTEREST PAYMENT ITS IN MATURITY, THE TAX FACTOR AND COLLECT MUST BE BY DAY
			Integer yearType = corporative.getSecurities().getCalendarDays();
			Integer dayType = corporative.getSecurities().getCalendarMonth();

			Date creationDate = custodyCommissionService.getCouponCreationDate(corporative.getIdCorporativeOperationPk());

			Map<Long,BigDecimal> mpHolderAccountCustody = getCustodyTaxes(yearType,securityCode,creationDate,loggerUser);

			saveMaturityCustodyCommission(mpHolderAccountCustody,securityCode,corporativeID,yearType,dayType);

		}else{
			//WE GET THE SECURITY CODE AND GET THE PREVIOUS COUPON PAYMENT TO CALCULATE THIS NEW PAYMENT
			BigDecimal nominalValue = corporative.getSecurities().getCurrentNominalValue();
			Long previousCorporative = custodyCommissionService.getPreviousCorporative(securityCode);

			CustodyCommission previousCommissionDetail = custodyCommissionService.getPreviousCustodyCommission(previousCorporative);

			if(Validations.validateIsNotNull(previousCommissionDetail)){
				//IF EXISTS A PREVIOUS CUSTODY COMMISSION DETAIL, WE APPLY NORMAL WAY TO CALCULATE CUSTODY COMMISSION
				CorporativeOperation previousCorporativeObj = null;
				//GET PREVIOUS BALANCES IN LAST COUPON PAYMENT
				if(Validations.validateIsNotNull(previousCorporative))
				{
					previousCorporativeObj = custodyCommissionService.find(previousCorporative,CorporativeOperation.class);
					mpHolderAccountBalance = getHolderAccountBalances(previousCorporative);
				}

				//GET THE MOVEMENTS BY SECURITY DONE BETWEEN LAST COUPON PAYMENT AND TODAY
				Date lastPaymentDate = null;
				if(Validations.validateIsNotNull(previousCorporativeObj)){
					lastPaymentDate = previousCorporativeObj.getRegistryDate();
				}
				else{
					lastPaymentDate = custodyCommissionService.beginPlacementSegment(securityCode);
				}

				List<Object[]> lstMovements = custodyCommissionService.getMovementsBySecurity(securityCode,lastPaymentDate);
				List<HolderAccountCommissionMovementsTO> holderAccountMovements = groupAccountMovements(lstMovements);

				//WITH PREVIOUS BALANCES AND MOVEMENTS BY HOLDER ACCOUNT, SYSTEM WILL CALCULATE THE CUSTODY COMMISION AMOUNT
				lstHolderAccountBalance = processCalculationCommission(mpHolderAccountBalance,holderAccountMovements);

				saveCustodyCommissionProcess(corporative,lstHolderAccountBalance,lastPaymentDate,securityCode,nominalValue,previousCorporative);
			}
			else{
				List<CorporativeProcessResult> lstCorporateProcessResult = custodyCommissionService.getCorporativeResult(corporativeID);

				CustodyCommission custodyCommission = saveCustodyCommission(corporativeID,securityCode);

				if(Validations.validateListIsNotNullAndNotEmpty(lstCorporateProcessResult)){
					BigDecimal rate = getPercentRate(loggerUser, CommonsUtilities.currentDate());
					Integer periodicity = custodyCommissionService.getSecurityPeriodicity(corporativeID);
					Integer quantityMonths = 0;

					if(InterestPeriodicityType.MONTHLY.getCode().equals(periodicity)){
						quantityMonths = InterestPeriodicityType.MONTHLY.getIndicator1();
					}else if(InterestPeriodicityType.BIMONTHLY.getCode().equals(periodicity)){
						quantityMonths = InterestPeriodicityType.BIMONTHLY.getIndicator1();
					}else if(InterestPeriodicityType.QUARTERLY.getCode().equals(periodicity)){
						quantityMonths = InterestPeriodicityType.QUARTERLY.getIndicator1();
					}else if(InterestPeriodicityType.FOURMONTHLY.getCode().equals(periodicity)){
						quantityMonths = InterestPeriodicityType.FOURMONTHLY.getIndicator1();
					}else if(InterestPeriodicityType.BIANNUAL.getCode().equals(periodicity)){
						quantityMonths = InterestPeriodicityType.BIANNUAL.getIndicator1();
					}else if(InterestPeriodicityType.BIANNUAL.getCode().equals(periodicity)){
						quantityMonths = InterestPeriodicityType.BIANNUAL.getIndicator1();
					}

					BigDecimal bdQuantityMonths = new BigDecimal(quantityMonths);

					//
					List<Object[]> lstHolderAccountBalances = custodyCommissionService.getHolderAccountStock(corporativeID);
					Map<Long,BigDecimal> mpHolderAccountBalances = processHolderAccountBalances(lstHolderAccountBalances);

					//IF EXIST A PROCESS CORPORATE RESULT, WE MUST UPDATE THE CUSTODY COMMISSION
					for(CorporativeProcessResult corporativeProcessResult : lstCorporateProcessResult){
						Long holderAccount = corporativeProcessResult.getHolderAccount().getIdHolderAccountPk();

						BigDecimal holderAccountBalance = mpHolderAccountBalances.get(holderAccount);
						BigDecimal finalRate = bdQuantityMonths.multiply(rate.multiply(nominalValue));
						BigDecimal custodyAmount = holderAccountBalance.multiply(finalRate);

						corporativeProcessResult.setCustodyAmount(custodyAmount);
						custodyCommissionService.update(corporativeProcessResult);

						CustodyCommissionDetail custodyCommissionDetail = new CustodyCommissionDetail();
						custodyCommissionDetail.setCustodyAmount(custodyAmount);
						custodyCommissionDetail.setCustodyCommission(custodyCommission);
						custodyCommissionDetail.setCustodyQuantity(holderAccountBalance);
						custodyCommissionDetail.setCustodyTax(finalRate);

						custodyCommissionDetail.setHolderAccount(new HolderAccount());
						custodyCommissionDetail.getHolderAccount().setIdHolderAccountPk(holderAccount);

						custodyCommissionDetail.setPossessionType(PrimaryBalanceType.SECONDARY_BALANCE.getCode());

						custodyCommissionDetail.setSecurity(new Security());
						custodyCommissionDetail.getSecurity().setIdSecurityCodePk(securityCode);

						custodyCommissionService.create(custodyCommissionDetail);
					}
					BigDecimal custodyAmount = corporateServiceBean.getCustodyAmount(corporativeID);
					corporative.setCustodyAmount(custodyAmount);
					custodyCommissionService.update(corporative);
				}
				else{
					//IF THERE IS NOT A CORPORATIVE PROCESS RESULT, WE WILL REGISTER
					throw new Exception("No se ha corrido el preliminar del Proceso Corporativo con ID: " + corporativeID.toString());
				}
			}
		}
	}

	/**
	 * MMetodo que retorna lista de saldos por cuenta.
	 *
	 * @param previousCorporative the previous corporative
	 * @return the holder account balances
	 */
	private Map<Long,Map<Integer,BigDecimal>> getHolderAccountBalances(Long previousCorporative){
		Map<Long,Map<Integer,BigDecimal>> mpBalances = new HashMap<Long, Map<Integer,BigDecimal>>();
		List<CustodyCommissionDetail> lstPreviousBalances = custodyCommissionService.getCustodyCommissionDetail(previousCorporative);

		if(Validations.validateListIsNotNullAndNotEmpty(lstPreviousBalances)){
			for(CustodyCommissionDetail previousBalance : lstPreviousBalances){
				Long holderAccount = previousBalance.getHolderAccount().getIdHolderAccountPk();
				Integer possessionType = previousBalance.getPossessionType();
				BigDecimal quantity = previousBalance.getCustodyQuantity();

				if(mpBalances.containsKey(holderAccount)){
					Map<Integer,BigDecimal> mpBalancesType = mpBalances.get(holderAccount);

					if(mpBalancesType.containsKey(possessionType)){
						BigDecimal actualQuantity = mpBalancesType.get(possessionType);
						actualQuantity = actualQuantity.add(quantity);
						mpBalancesType.put(possessionType, actualQuantity);
						mpBalances.put(holderAccount, mpBalancesType);
					}else{
						mpBalancesType.put(possessionType, quantity);
						mpBalances.put(holderAccount, mpBalancesType);
					}

				}else{
					Map<Integer,BigDecimal> mpBalancesType = new HashMap<Integer, BigDecimal>();
					mpBalancesType.put(possessionType, quantity);
					mpBalances.put(holderAccount, mpBalancesType);
				}
			}
		}

		return mpBalances;
	}

	/**
	 * Metodo que registra comisiones por custodia.
	 *
	 * @param corporativeOperation the corporative operation
	 * @param lstHolderAccountCommission the lst holder account commission
	 * @param lastPaymentDate the last payment date
	 * @param securityCode the security code
	 * @param nominalValue the nominal value
	 * @param previousCorporative the previous corporative
	 * @throws ServiceException the service exception
	 */
	private void saveCustodyCommissionProcess(CorporativeOperation corporativeOperation, List<HolderAccountCommissionDataTO> lstHolderAccountCommission, 
												Date lastPaymentDate, String securityCode, BigDecimal nominalValue, Long previousCorporative) throws ServiceException
	{

		CustodyCommission custodyCommission = new CustodyCommission();
		custodyCommission.setCorporativeOperation(new CorporativeOperation());
		custodyCommission.getCorporativeOperation().setIdCorporativeOperationPk(corporativeOperation.getIdCorporativeOperationPk());

		Security security = custodyCommissionService.find(securityCode,Security.class);

		Integer dayType = security.getCalendarMonth();
		Integer yearType = security.getCalendarDays();

		Date today = CommonsUtilities.currentDate();
		custodyCommission.setRegisterDate(today);

		boolean leapYear = validateLeapPayment(lastPaymentDate,today,securityCode);
		if(leapYear){
			custodyCommission.setIndBisiesto(BooleanType.YES.getCode());
		}else{
			custodyCommission.setIndBisiesto(BooleanType.NO.getCode());
		}

		custodyCommission.setDayType(dayType);
		custodyCommission.setYearType(yearType);

		custodyCommissionService.create(custodyCommission);

		if(Validations.validateIsNotEmpty(lstHolderAccountCommission)){
			for(HolderAccountCommissionDataTO holderAccountCommission : lstHolderAccountCommission){
				holderAccountCommission = calculateCommissionByAccount(holderAccountCommission,dayType,yearType,lastPaymentDate,leapYear,nominalValue,previousCorporative);
				saveCustodyCommissionDetail(custodyCommission,holderAccountCommission,securityCode);
			}
		}
	}

	/**
	 * Metodo que valida anio bisiesto.
	 *
	 * @return true, if successful
	 */
	private boolean validateLeapYear(){
		Integer currentYear = CommonsUtilities.currentYear();
		if((currentYear % 4 == 0) && ((currentYear % 100 != 0) || (currentYear % 400 == 0))){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Metodo que retorna lista de comisiones de custodia por cuenta.
	 *
	 * @param mpHolderAccountBalances the mp holder account balances
	 * @param lstMovements the lst movements
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountCommissionDataTO> processCalculationCommission(Map<Long,Map<Integer,BigDecimal>> mpHolderAccountBalances, 
												List<HolderAccountCommissionMovementsTO> lstMovements) throws ServiceException{

		List<HolderAccountCommissionDataTO> lstHolderAccountCommission = new ArrayList<HolderAccountCommissionDataTO>();
		HolderAccountCommissionDataTO holderAccountBalances = new HolderAccountCommissionDataTO();
		List<HolderAccountCommissionMovementsTO> lstHolderAccountMovements = new ArrayList<HolderAccountCommissionMovementsTO>();
		BigDecimal primaryPreviousQuantity = BigDecimal.ZERO;
		BigDecimal secondaryPreviousQuantity = BigDecimal.ZERO;
		Long previousHolderAccountPK = null;

		//ITERATE MOVEMENTS AND GET BALANCES ACCORDING WILL BE CALCULATE
		if(Validations.validateListIsNotNullAndNotEmpty(lstMovements)){
			for(HolderAccountCommissionMovementsTO movement : lstMovements){
				Integer balanceType = movement.getBalanceType();
				if(BooleanType.YES.getCode().equals(balanceType)){
					balanceType = PrimaryBalanceType.PRIMARY_BALANCE.getCode();
				}
				else{
					balanceType = PrimaryBalanceType.SECONDARY_BALANCE.getCode();
				}
				Integer movementClass = movement.getMovementClass();
				Long movementHolderAccount = movement.getHolderAccount();
				BigDecimal movementQuantity = movement.getQuantity();

				if(Validations.validateIsNull(previousHolderAccountPK)){
					//GET THE PREVIOUS BALANCES BY HOLDER ACCOUNT - THE FIRST TIME
					if(mpHolderAccountBalances.containsKey(movementHolderAccount)){
						Map<Integer,BigDecimal> mpBalanceType = mpHolderAccountBalances.get(movementHolderAccount);
						for(Map.Entry<Integer,BigDecimal> balances : mpBalanceType.entrySet()){
							balanceType = balances.getKey();
							if(PrimaryBalanceType.PRIMARY_BALANCE.getCode().equals(balanceType)){
								primaryPreviousQuantity = balances.getValue();
							}
							else if(PrimaryBalanceType.SECONDARY_BALANCE.getCode().equals(balanceType)){
								secondaryPreviousQuantity = balances.getValue();
							}
						}
					}
					if(BalanceBehaviourType.SUM.getCode().equals(movementClass)){
						lstHolderAccountMovements.add(movement);
					}
					else if(BalanceBehaviourType.SUBTRACTION.getCode().equals(movementClass)){
						if(PrimaryBalanceType.PRIMARY_BALANCE.getCode().equals(balanceType)){
							if(movementQuantity.compareTo(primaryPreviousQuantity) == 1){
								throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
										"Negative balance for holder account: " + movementHolderAccount.toString() +
										"in movement by quantity: " + movementQuantity.toString());
							}
							else if(movementQuantity.compareTo(primaryPreviousQuantity) == 0){
								holderAccountBalances.setAvailablePrimaryCustody(BigDecimal.ZERO);
							}
							else if(movementQuantity.compareTo(primaryPreviousQuantity) == -1){
								holderAccountBalances.setAvailablePrimaryCustody(primaryPreviousQuantity.subtract(movementQuantity));
							}
						}
						else if(PrimaryBalanceType.SECONDARY_BALANCE.getCode().equals(balanceType)){
							if(movementQuantity.compareTo(secondaryPreviousQuantity) == 1){
								throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
										"Negative balance for holder account: " + movementHolderAccount.toString() +
										"in movement by quantity: " + movementQuantity.toString());
							}
							else if(movementQuantity.compareTo(secondaryPreviousQuantity) == 0){
								holderAccountBalances.setAvailableSecondaryCustody(BigDecimal.ZERO);
							}
							else if(movementQuantity.compareTo(secondaryPreviousQuantity) == -1){
								holderAccountBalances.setAvailableSecondaryCustody(secondaryPreviousQuantity.subtract(movementQuantity));
							}
						}
					}
					previousHolderAccountPK = movementHolderAccount;
				}
				else{
					if(movementHolderAccount.equals(previousHolderAccountPK)){
						if(BalanceBehaviourType.SUM.getCode().equals(movementClass)){
							lstHolderAccountMovements.add(movement);
						}
						else if(BalanceBehaviourType.SUBTRACTION.getCode().equals(movementClass)){
							if(PrimaryBalanceType.PRIMARY_BALANCE.getCode().equals(balanceType)){
								if(movementQuantity.compareTo(primaryPreviousQuantity) == 1){
									if(primaryPreviousQuantity.compareTo(BigDecimal.ZERO) == 1){
										movementQuantity = movementQuantity.subtract(primaryPreviousQuantity);
										primaryPreviousQuantity = BigDecimal.ZERO;
										if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovements)){
											for(HolderAccountCommissionMovementsTO holderAccountMovements : lstHolderAccountMovements){
												BigDecimal availableQuantity = holderAccountMovements.getAvailableQuantity();
												if(availableQuantity.compareTo(BigDecimal.ZERO) == 0){
													continue;
												}
												else{
													if(availableQuantity.compareTo(movementQuantity) == 1){
														holderAccountMovements.setAvailableQuantity(availableQuantity.subtract(movementQuantity));
														movementQuantity = BigDecimal.ZERO;
														break;
													}
													else if(availableQuantity.compareTo(movementQuantity) == 0){
														holderAccountMovements.setAvailableQuantity(BigDecimal.ZERO);
														movementQuantity = BigDecimal.ZERO;
														break;
													}
													else{
														movementQuantity = movementQuantity.subtract(availableQuantity);
														holderAccountMovements.setAvailableQuantity(BigDecimal.ZERO);
													}
												}
											}

											if(movementQuantity.compareTo(BigDecimal.ZERO) == 1){
												throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
														"Negative balance for holder account: " + movementHolderAccount.toString() +
														"in movement by quantity: " + movementQuantity.toString());
											}
										}
									}
									else{
										if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovements)){
											for(HolderAccountCommissionMovementsTO holderAccountMovements : lstHolderAccountMovements){
												BigDecimal availableQuantity = holderAccountMovements.getAvailableQuantity();
												if(availableQuantity.compareTo(movementQuantity) == 1){
													holderAccountMovements.setAvailableQuantity(availableQuantity.subtract(movementQuantity));
													movementQuantity = BigDecimal.ZERO;
													break;
												}
												else if(availableQuantity.compareTo(movementQuantity) == 0){
													holderAccountMovements.setAvailableQuantity(BigDecimal.ZERO);
													movementQuantity = BigDecimal.ZERO;
													break;
												}
												else{
													movementQuantity = movementQuantity.subtract(availableQuantity);
													holderAccountMovements.setAvailableQuantity(BigDecimal.ZERO);
												}
											}

											if(movementQuantity.compareTo(BigDecimal.ZERO) == 1){
												throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
														"Negative balance for holder account: " + movementHolderAccount.toString() +
														"in movement by quantity: " + movementQuantity.toString());
											}
										}
										else{
											throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
													"Negative balance for holder account: " + movementHolderAccount.toString() +
													"by quantity: " + movementQuantity.toString());
										}
									}
								}
								else if(movementQuantity.compareTo(primaryPreviousQuantity) == 0)
								{
									holderAccountBalances.setAvailablePrimaryCustody(BigDecimal.ZERO);
								}
								else if(movementQuantity.compareTo(primaryPreviousQuantity) == -1)
								{
									holderAccountBalances.setAvailablePrimaryCustody(primaryPreviousQuantity.subtract(movementQuantity));
								}
							}
							else if(PrimaryBalanceType.SECONDARY_BALANCE.getCode().equals(balanceType)){
								if(movementQuantity.compareTo(secondaryPreviousQuantity) == 1){
									if(secondaryPreviousQuantity.compareTo(BigDecimal.ZERO) == 1){
										movementQuantity = movementQuantity.subtract(secondaryPreviousQuantity);
										secondaryPreviousQuantity = BigDecimal.ZERO;
										if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovements)){
											for(HolderAccountCommissionMovementsTO holderAccountMovements : lstHolderAccountMovements){
												BigDecimal availableQuantity = holderAccountMovements.getAvailableQuantity();
												if(availableQuantity.compareTo(BigDecimal.ZERO) == 0){
													continue;
												}
												else{
													if(availableQuantity.compareTo(movementQuantity) == 1){
														holderAccountMovements.setAvailableQuantity(availableQuantity.subtract(movementQuantity));
														movementQuantity = BigDecimal.ZERO;
														break;
													}
													else if(availableQuantity.compareTo(movementQuantity) == 0){
														holderAccountMovements.setAvailableQuantity(BigDecimal.ZERO);
														movementQuantity = BigDecimal.ZERO;
														break;
													}
													else{
														movementQuantity = movementQuantity.subtract(availableQuantity);
														holderAccountMovements.setAvailableQuantity(BigDecimal.ZERO);
													}
												}
											}
											
											if(movementQuantity.compareTo(BigDecimal.ZERO) == 1){
												throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
														"Negative balance for holder account: " + movementHolderAccount.toString() +
														"in movement by quantity: " + movementQuantity.toString());
											}
										}
									}
									else{
										if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovements)){
											for(HolderAccountCommissionMovementsTO holderAccountMovements : lstHolderAccountMovements){
												BigDecimal availableQuantity = holderAccountMovements.getAvailableQuantity();
												if(availableQuantity.compareTo(movementQuantity) == 1){
													holderAccountMovements.setAvailableQuantity(availableQuantity.subtract(movementQuantity));
													movementQuantity = BigDecimal.ZERO;
													break;
												}
												else if(availableQuantity.compareTo(movementQuantity) == 0){
													holderAccountMovements.setAvailableQuantity(BigDecimal.ZERO);
													movementQuantity = BigDecimal.ZERO;
													break;
												}
												else{
													movementQuantity = movementQuantity.subtract(availableQuantity);
													holderAccountMovements.setAvailableQuantity(BigDecimal.ZERO);
												}
											}

											if(movementQuantity.compareTo(BigDecimal.ZERO) == 1){
												throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
														"Negative balance for holder account: " + movementHolderAccount.toString() +
														"in movement by quantity: " + movementQuantity.toString());
											}
										}
										else{
											throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
													"Negative balance for holder account: " + movementHolderAccount.toString() +
													"by quantity: " + movementQuantity.toString());
										}
									}
								}
								else if(movementQuantity.compareTo(secondaryPreviousQuantity) == 0){
									holderAccountBalances.setAvailableSecondaryCustody(BigDecimal.ZERO);
								}
								else if(movementQuantity.compareTo(secondaryPreviousQuantity) == -1){
									holderAccountBalances.setAvailableSecondaryCustody(secondaryPreviousQuantity.subtract(movementQuantity));
								}
							}
						}
						previousHolderAccountPK = movementHolderAccount;
					}
					else{
						holderAccountBalances.setAvailablePrimaryCustody(primaryPreviousQuantity);
						holderAccountBalances.setAvailableSecondaryCustody(secondaryPreviousQuantity);
						holderAccountBalances.setHolderAccount(previousHolderAccountPK);
						holderAccountBalances.setHolderAccountMovements(lstHolderAccountMovements);
						lstHolderAccountCommission.add(holderAccountBalances);
						holderAccountBalances = new HolderAccountCommissionDataTO();
						lstHolderAccountMovements = new ArrayList<HolderAccountCommissionMovementsTO>();

						primaryPreviousQuantity = BigDecimal.ZERO;
						secondaryPreviousQuantity = BigDecimal.ZERO;

						if(mpHolderAccountBalances.containsKey(movementHolderAccount)){
							Map<Integer,BigDecimal> mpBalanceType = mpHolderAccountBalances.get(movementHolderAccount);
							for(Map.Entry<Integer,BigDecimal> balances : mpBalanceType.entrySet()){
								balanceType = balances.getKey();
								if(PrimaryBalanceType.PRIMARY_BALANCE.getCode().equals(balanceType)){
									primaryPreviousQuantity = balances.getValue();
								}
								else if(PrimaryBalanceType.SECONDARY_BALANCE.getCode().equals(balanceType)){
									secondaryPreviousQuantity = balances.getValue();
								}
							}
						}
						if(BalanceBehaviourType.SUM.getCode().equals(movementClass)){
							lstHolderAccountMovements.add(movement);
						}
						else if(BalanceBehaviourType.SUBTRACTION.getCode().equals(movementClass)){
							if(PrimaryBalanceType.PRIMARY_BALANCE.getCode().equals(balanceType)){
								if(movementQuantity.compareTo(primaryPreviousQuantity) == 1){
									throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
											"Negative balance for holder account: " + movementHolderAccount.toString() +
											"in movement by quantity: " + movementQuantity.toString());
								}
								else if(movementQuantity.compareTo(primaryPreviousQuantity) == 0){
									holderAccountBalances.setAvailablePrimaryCustody(BigDecimal.ZERO);
								}
								else if(movementQuantity.compareTo(primaryPreviousQuantity) == -1){
									holderAccountBalances.setAvailablePrimaryCustody(primaryPreviousQuantity.subtract(movementQuantity));
								}
							}
							else if(PrimaryBalanceType.SECONDARY_BALANCE.getCode().equals(balanceType)){
								if(movementQuantity.compareTo(secondaryPreviousQuantity) == 1){
									throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
											"Negative balance for holder account: " + movementHolderAccount.toString() +
											"in movement by quantity: " + movementQuantity.toString());
								}
								else if(movementQuantity.compareTo(secondaryPreviousQuantity) == 0){
									holderAccountBalances.setAvailableSecondaryCustody(BigDecimal.ZERO);
								}
								else if(movementQuantity.compareTo(secondaryPreviousQuantity) == -1){
									holderAccountBalances.setAvailableSecondaryCustody(secondaryPreviousQuantity.subtract(movementQuantity));
								}
							}
						}
						previousHolderAccountPK = movementHolderAccount;
					}
				}
			}
			holderAccountBalances.setAvailablePrimaryCustody(primaryPreviousQuantity);
			holderAccountBalances.setAvailableSecondaryCustody(secondaryPreviousQuantity);
			holderAccountBalances.setHolderAccount(previousHolderAccountPK);
			holderAccountBalances.setHolderAccountMovements(lstHolderAccountMovements);
			lstHolderAccountCommission.add(holderAccountBalances);
		}
		else
		{
			//IF THERE ARE NOT MOVEMENTS BETWEEN COUPON PAYMENT DAYS, WE WILL REALIZE THE CALCULATION ACCORDING TO PREVIOUS BALANCES
			for(Map.Entry<Long,Map<Integer,BigDecimal>> entry : mpHolderAccountBalances.entrySet()){
				holderAccountBalances = new HolderAccountCommissionDataTO();
				Long holderAccount = entry.getKey();
				Map<Integer,BigDecimal> mpBalance = entry.getValue();
				holderAccountBalances.setHolderAccount(holderAccount);
				holderAccountBalances.setHolderAccountMovements(null);
				for(Map.Entry<Integer,BigDecimal> balance : mpBalance.entrySet()){
					Integer balanceType = balance.getKey();
					BigDecimal quantity = balance.getValue();
					if(PrimaryBalanceType.PRIMARY_BALANCE.getCode().equals(balanceType)){
						holderAccountBalances.setAvailablePrimaryCustody(quantity);
					}else if(PrimaryBalanceType.SECONDARY_BALANCE.getCode().equals(balanceType)){
						holderAccountBalances.setAvailableSecondaryCustody(quantity);
					}
				}
				lstHolderAccountCommission.add(holderAccountBalances);
			}
		}

		return lstHolderAccountCommission;
	}

	/**
	 * Metodo que retorna lista de movimentos de comisiones de custodia.
	 *
	 * @param lstMovements the lst movements
	 * @return the list
	 */
	private List<HolderAccountCommissionMovementsTO> groupAccountMovements(List<Object[]> lstMovements){
		List<HolderAccountCommissionMovementsTO> lstReturn = new ArrayList<HolderAccountCommissionMovementsTO>();

		if(Validations.validateListIsNotNullAndNotEmpty(lstMovements)){
			for(Object[] obj : lstMovements){
				Integer movementType = Integer.valueOf(obj[0].toString());
				Integer movementClass = Integer.valueOf(obj[1].toString());
				BigDecimal quantity = new BigDecimal(obj[2].toString());
				Date movementDate = (Date) obj[3];
				Long holderAccount = Long.valueOf(obj[4].toString());

				HolderAccountCommissionMovementsTO holderAccountMovement = new HolderAccountCommissionMovementsTO();
				holderAccountMovement.setBalanceType(movementType);
				holderAccountMovement.setMovementDate(movementDate);
				holderAccountMovement.setAvailableQuantity(quantity);
				holderAccountMovement.setQuantity(quantity);
				holderAccountMovement.setMovementClass(movementClass);
				holderAccountMovement.setHolderAccount(holderAccount);

				lstReturn.add(holderAccountMovement);
			}
		}

		return lstReturn;
	}

	/**
	 * Metodo que retorna calculos de comision de custodia por cuenta.
	 *
	 * @param holderAccountCommission the holder account commission
	 * @param dayType the day type
	 * @param yearType the year type
	 * @param lastPaymentDate the last payment date
	 * @param leapYear the leap year
	 * @param nominalValue the nominal value
	 * @param previousCorporative the previous corporative
	 * @return the holder account commission data to
	 * @throws ServiceException the service exception
	 */
	private HolderAccountCommissionDataTO calculateCommissionByAccount(HolderAccountCommissionDataTO holderAccountCommission, Integer dayType, Integer yearType, 
																		Date lastPaymentDate, boolean leapYear, BigDecimal nominalValue, Long previousCorporative) throws ServiceException{
		BigDecimal primaryCommissionAmount = BigDecimal.ZERO;
		BigDecimal secondaryCommissionAmount = BigDecimal.ZERO;

		BigDecimal totalPrimaryBalance = BigDecimal.ZERO;
		BigDecimal totalSecondaryBalance = BigDecimal.ZERO;

		BigDecimal commissionFactor = BigDecimal.ZERO;

		//VERIFY IF EXIST BALANCE TO COLLECT CUSTODY COMMISSION FOR THE WHOLE TIME OF THE COUPON FOR PRIMARY BALANCES
		BigDecimal primaryBalance = holderAccountCommission.getAvailablePrimaryCustody();
		if(Validations.validateIsNull(primaryBalance)){
			primaryBalance = BigDecimal.ZERO;
		}
		BigDecimal secondaryBalance = holderAccountCommission.getAvailableSecondaryCustody();
		if(Validations.validateIsNull(secondaryBalance)){
			secondaryBalance = BigDecimal.ZERO;
		}

		commissionFactor = getCommissionFactor(lastPaymentDate,dayType,yearType,leapYear,previousCorporative);
		holderAccountCommission.setFactor(commissionFactor);

		//FOR PRIMARY BALANCES
		if(primaryBalance.compareTo(BigDecimal.ZERO) == 1){
			primaryCommissionAmount = CorporateProcessUtils.multiply(primaryBalance,commissionFactor,8);
			totalPrimaryBalance = totalPrimaryBalance.add(primaryBalance);
		}

		//FOR SECONDARY BALANCES
		if(secondaryBalance.compareTo(BigDecimal.ZERO) == 1){
			secondaryCommissionAmount = CorporateProcessUtils.multiply(secondaryBalance,commissionFactor,8);
			totalSecondaryBalance = totalSecondaryBalance.add(secondaryBalance);
		}

		List<HolderAccountCommissionMovementsTO> lstMovements = holderAccountCommission.getHolderAccountMovements();
		if(Validations.validateListIsNotNullAndNotEmpty(lstMovements)){
			for(HolderAccountCommissionMovementsTO holderAccountMovement : lstMovements){

				BigDecimal quantity = holderAccountMovement.getAvailableQuantity();
				Date movementDate = holderAccountMovement.getMovementDate();
				commissionFactor = getCommissionFactor(movementDate,dayType,yearType,leapYear,previousCorporative);
				Integer balanceType = holderAccountMovement.getBalanceType();
				if(BooleanType.YES.getCode().equals(balanceType)){
					balanceType = PrimaryBalanceType.PRIMARY_BALANCE.getCode();
				}else{
					balanceType = PrimaryBalanceType.SECONDARY_BALANCE.getCode();
				}

				if(quantity.compareTo(BigDecimal.ZERO) == 1){
					if(PrimaryBalanceType.PRIMARY_BALANCE.getCode().equals(balanceType)){

						primaryCommissionAmount = CorporateProcessUtils.multiply(quantity,commissionFactor,8);
						totalPrimaryBalance = totalPrimaryBalance.add(quantity);

					}else if(PrimaryBalanceType.SECONDARY_BALANCE.getCode().equals(balanceType)){
						secondaryCommissionAmount = CorporateProcessUtils.multiply(quantity,commissionFactor,8);
						totalSecondaryBalance = totalSecondaryBalance.add(quantity);
					}
				}
			}
		}

		holderAccountCommission.setPrimaryCustodyCommission(CorporateProcessUtils.multiply(primaryCommissionAmount,nominalValue));
		holderAccountCommission.setSecondaryCustodyCommission(CorporateProcessUtils.multiply(secondaryCommissionAmount,nominalValue));

		return holderAccountCommission;
	}

	/**
	 * Metodo que registra detalle de comision de custodia. 
	 *
	 * @param custodyCommission the custody commission
	 * @param holderAccountCommission the holder account commission
	 * @param securityCode the security code
	 */
	private void saveCustodyCommissionDetail(CustodyCommission custodyCommission, HolderAccountCommissionDataTO holderAccountCommission, String securityCode){
		CustodyCommissionDetail custodyCommissionDetail = new CustodyCommissionDetail();

		Security security = custodyCommissionService.find(securityCode,Security.class);

		HolderAccount holderAccount = custodyCommissionService.find(holderAccountCommission.getHolderAccount(),HolderAccount.class);

		//SAVE CUSTODY COMMISSION FOR PRIMARY OPERATIONS
		BigDecimal custodyPrimaryBalance = holderAccountCommission.getAvailablePrimaryCustody();
		if(Validations.validateIsNull(custodyPrimaryBalance)){
			custodyPrimaryBalance = BigDecimal.ZERO;
		}
		List<HolderAccountCommissionMovementsTO> lstMovements = holderAccountCommission.getHolderAccountMovements();
		if(Validations.validateListIsNotNullAndNotEmpty(lstMovements)){
			for(HolderAccountCommissionMovementsTO movement : lstMovements){
				if(BooleanType.YES.getCode().equals(movement.getBalanceType())){
					custodyPrimaryBalance = custodyPrimaryBalance.add(movement.getAvailableQuantity());
				}
			}
		}
		BigDecimal primaryCustodyCommission = holderAccountCommission.getPrimaryCustodyCommission();

		if(custodyPrimaryBalance.compareTo(BigDecimal.ZERO) == 1){
			custodyCommissionDetail.setCustodyAmount(primaryCustodyCommission);
			custodyCommissionDetail.setCustodyCommission(custodyCommission);
			custodyCommissionDetail.setCustodyQuantity(custodyPrimaryBalance);
			custodyCommissionDetail.setCustodyTax(holderAccountCommission.getFactor());
			custodyCommissionDetail.setHolderAccount(holderAccount);
			custodyCommissionDetail.setPossessionType(PrimaryBalanceType.PRIMARY_BALANCE.getCode());
			custodyCommissionDetail.setSecurity(security);

			custodyCommissionService.create(custodyCommissionDetail);
			custodyCommissionDetail = new CustodyCommissionDetail();
		}

		//SAVE CUSTODY COMMISSION FOR SECONDARY OPERATIONS
		BigDecimal secondaryCustodyBalance = holderAccountCommission.getAvailableSecondaryCustody();
		if(Validations.validateIsNull(secondaryCustodyBalance)){
			secondaryCustodyBalance = BigDecimal.ZERO;
		}
		if(Validations.validateListIsNotNullAndNotEmpty(lstMovements)){
			for(HolderAccountCommissionMovementsTO movement : lstMovements){
				if(!BooleanType.YES.getCode().equals(movement.getBalanceType())){
					custodyPrimaryBalance = custodyPrimaryBalance.add(movement.getAvailableQuantity());
				}
			}
		}
		BigDecimal secondaryCustodyCommission = holderAccountCommission.getSecondaryCustodyCommission();

		if(secondaryCustodyBalance.compareTo(BigDecimal.ZERO) == 1){
			custodyCommissionDetail.setCustodyAmount(secondaryCustodyCommission);
			custodyCommissionDetail.setCustodyCommission(custodyCommission);
			custodyCommissionDetail.setCustodyQuantity(secondaryCustodyBalance);
			custodyCommissionDetail.setCustodyTax(holderAccountCommission.getFactor());
			custodyCommissionDetail.setHolderAccount(holderAccount);
			custodyCommissionDetail.setPossessionType(PrimaryBalanceType.SECONDARY_BALANCE.getCode());
			custodyCommissionDetail.setSecurity(security);

			custodyCommissionService.create(custodyCommissionDetail);
		}
	}

	/**
	 * Metodo que valida salto de pago de comisiones por custodia.
	 *
	 * @param lastPaymentDate the last payment date
	 * @param today the today
	 * @param securityCode the security code
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("deprecation")
	private boolean validateLeapPayment(Date lastPaymentDate, Date today, String securityCode) throws ServiceException{
		boolean leapPayment = false;
		Date firstMarch = new Date(CommonsUtilities.currentYear(),2,1);
		Long previousCorporative = custodyCommissionService.getPreviousCorporative(securityCode);

		if(Validations.validateIsNotNull(previousCorporative)){
			CustodyCommission previousCustodyCommission = custodyCommissionService.getPreviousCustodyCommission(previousCorporative);
			if(Validations.validateIsNull(previousCustodyCommission)){
				throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA,
											"No existe cálculo previo para el cobro de comisiones de custodia del corporativo : " + previousCorporative.toString());
			}else{
				CorporativeOperation previosCorporativeOperation = custodyCommissionService.find(previousCorporative,CorporativeOperation.class);
				Date previousRegistryDate = previosCorporativeOperation.getRegistryDate();

				if(BooleanType.YES.getCode().equals(previousCustodyCommission.getIndBisiesto())){

					if(today.compareTo(firstMarch) >= 0 && previousRegistryDate.compareTo(firstMarch) < 0){
						leapPayment = false;
					}else{
						leapPayment = true;
					}
				}else if(BooleanType.NO.getCode().equals(previousCustodyCommission.getIndBisiesto())){

					if(today.compareTo(firstMarch) >= 0 && previousRegistryDate.compareTo(firstMarch) < 0){
						leapPayment = true;
					}else{
						leapPayment = false;
					}
				}
			}
		}else{
			Date beginPlacementDate = custodyCommissionService.beginPlacementSegment(securityCode);

			if(today.compareTo(firstMarch) >= 0 && beginPlacementDate.compareTo(firstMarch) < 0){
				if(validateLeapYear()){
					leapPayment = true;
				}else{
					leapPayment = false;
				}
			}else{
				leapPayment = false;
			}
		}

		return leapPayment;
	}

	/**
	 * Metodo para obtener factor de acuerdo - FORMULA: FACTOR = (TASA / BASE) * DIAS
	 * TASA = TASA
	 * BASE = Cantidad de dias en el anio
	 * DIAS = Diferencia entre dias segun el tipo de liquidacion.
	 *
	 * @param movementDate the movement date
	 * @param dayType the day type
	 * @param yearType the year type
	 * @param leapYear the leap year
	 * @param previousCorporative the previous corporative
	 * @return the commission factor
	 * @throws ServiceException the service exception
	 */
	private BigDecimal getCommissionFactor(Date movementDate, Integer dayType, Integer yearType, boolean leapYear, Long previousCorporative) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
		Long MILLSECS_PER_DAY = (long) (24 * 60 * 60 * 1000);
		Integer days30 = 30;
		Integer month12 = 12;
		Integer year360 = 360;

		BigDecimal factor = BigDecimal.ZERO;
		BigDecimal rate = getPercentRate(loggerUser, movementDate);

		//DIAS - formulaDay
		//1° GET THE NUMBER OF DAYS FROM THE MOVEMENT UNTIL TODAY
		BigDecimal formulaDay = BigDecimal.ZERO;
		BigDecimal formulaBase = BigDecimal.ZERO;
		Date today = CommonsUtilities.currentDate();

		if(CalendarMonthType._30.getCode().equals(dayType)){

			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			Integer yearToday = cal.get(Calendar.YEAR);
			Integer monthToday = cal.get(Calendar.MONTH);
			Integer dayToday = cal.get(Calendar.DATE);

			cal = Calendar.getInstance();
			cal.setTime(movementDate);
			Integer yearMovement = cal.get(Calendar.YEAR);
			Integer monthMovement = cal.get(Calendar.MONTH);
			Integer dayMovement = cal.get(Calendar.DATE);

			if(yearToday.equals(yearMovement)){
				if(monthToday.equals(monthMovement)){
					if(dayToday.equals(dayMovement)){
						formulaDay = BigDecimal.ONE;
					}else{
						formulaDay = new BigDecimal(dayMovement - dayToday);
					}
				}else{
					if(dayToday > dayMovement){
						Integer totalMonths = monthToday - monthMovement;
						formulaDay = new BigDecimal(totalMonths * days30);
						formulaDay = formulaDay.add(new BigDecimal(dayToday - dayMovement));
					}else{
						Integer totalMonths = monthToday - monthMovement - 1;
						formulaDay = new BigDecimal(totalMonths * days30);
						formulaDay = formulaDay.add(new BigDecimal(days30 - dayMovement + dayToday)); 
					}
				}
			}else{
				if(monthToday.equals(monthMovement)){
					if(dayToday.equals(dayMovement)){
						formulaDay = new BigDecimal(year360);
					}else if(dayMovement > dayToday){
						formulaDay = new BigDecimal(11 * days30);
						formulaDay = formulaDay.add(new BigDecimal(days30 - dayMovement + dayToday));
					}
				}else if(monthMovement > monthToday){
					if(dayToday.equals(dayMovement)){
						Integer totalMonths = month12 - monthMovement + monthToday;
						formulaDay = new BigDecimal(days30 * totalMonths);
					}else {
						if(dayToday > dayMovement){
							Integer totalMonths = month12 - monthMovement + monthToday;
							formulaDay = new BigDecimal(days30 * totalMonths);
							formulaDay = formulaDay.add(new BigDecimal(dayToday - dayMovement));
						}else{
							Integer totalMonths = month12 - monthMovement + monthToday - 1;
							formulaDay = new BigDecimal(days30 * totalMonths);
							formulaDay = formulaDay.add(new BigDecimal(days30 - dayMovement + dayToday));
						}
					}
				}
			}

		}else if(CalendarMonthType.ACTUAL.getCode().equals(dayType)){
			Long difference = (((CommonsUtilities.currentDate().getTime() - movementDate.getTime())/MILLSECS_PER_DAY)- 1);
			if(difference.equals(new Long(0))){
				formulaDay = new BigDecimal(1);
			}else{
				formulaDay = new BigDecimal(difference);
			}
		}

		//BASE - formulaBase
		//2° GET YEAR BASE TO APPLY THE FORMULA
		if(CalendarDayType._360.getCode().equals(yearType)){
			formulaBase = new BigDecimal(CalendarDayType._360.getIntegerValue());
		}else if(CalendarDayType._365.getCode().equals(yearType)){
			formulaBase = new BigDecimal(CalendarDayType._365.getIntegerValue());
		}else if(CalendarDayType.ACTUAL.getCode().equals(yearType)){
			if(leapYear){
				formulaBase = new BigDecimal(366);
			}else{
				formulaBase = new BigDecimal(CalendarDayType._365.getIntegerValue());
			}
		}

		//IF IT IS THE PAYMENT OF FIRST COUPON, THE CALCULATION OF RATE WILL APPLY WITH THE DAYS APPLIED TO THE FINAL RATE
		if(Validations.validateIsNull(previousCorporative)){
			//FACTOR:(TASA / BASE) * DIAS
			factor = CorporateProcessUtils.divide(CorporateProcessUtils.multiply(rate,formulaDay,10),formulaBase,10);
		}else{
			//OTHERWISE IF PAYMENT IS THE OTHER COUPON, THE RATE WILL BE DIRECTLY
			Integer periodicity = custodyCommissionService.getSecurityPeriodicity(previousCorporative);
			BigDecimal bdPeriodicity = new BigDecimal(periodicity);
			factor = rate.multiply(bdPeriodicity);
		}
		return factor;
	}


	/**
	 * Metodo que retorna porcentaje de tasa.
	 *
	 * @param loggerUser the logger user
	 * @param movementDate the movement date
	 * @return the percent rate
	 */
	@SuppressWarnings("unchecked")
	private BigDecimal getPercentRate(LoggerUser loggerUser, Date movementDate){
		//Creating map to call services from base collection
		Map<String,Object> mapBilling = new HashMap<String,Object>();
//	    mapBilling.put(GeneralConstants.BASE_COLLECTION, BaseCollectionType.CUSTODIA_VALOR_TITULAR.getCode().toString());
	    mapBilling.put(GeneralConstants.CALCULATION_DATE, CommonsUtilities.convertDatetoString(movementDate));

	    /** FINDING SERVICES FROM BASE COLLECTION **/
	    Map<String,Object> map = billingComponentFacade.findBillingService(mapBilling);
	    Object object = map.get(GeneralConstants.BILLING_SERVICE_LIST);
	    List<BillingService> billingServiceList = (List<BillingService>)object;
	    BigDecimal ratePercentent= BigDecimal.ZERO;
	    for (BillingService billingService : billingServiceList) {
	        List<ServiceRate> serviceRateList = billingService.getServiceRates();
	        for (ServiceRate serviceRate : serviceRateList) {
			     if (serviceRate.getRateType().equals(RateType.PORCENTUAL.getCode())){
			    	 //Validating if the server belongs to ISSUERS
//			          if(billingService.getEntityCollection().equals(EntityCollectionType.RNT.getCode())){
//						ratePercentent = serviceRate.getRatePercent();
//					  }
			      }
			 }
	    }
	    return ratePercentent;
	}

	/**
	 * Metodo que registra comision de custodia.
	 *
	 * @param corporativeID the corporative id
	 * @param securityCode the security code
	 * @return the custody commission
	 * @throws ServiceException the service exception
	 */
	private CustodyCommission saveCustodyCommission(Long corporativeID, String securityCode) throws ServiceException{
		CustodyCommission custodyCommission = new CustodyCommission();
		custodyCommission.setCorporativeOperation(new CorporativeOperation());
		custodyCommission.getCorporativeOperation().setIdCorporativeOperationPk(corporativeID);

		Security security = custodyCommissionService.find(securityCode,Security.class);

		Integer dayType = security.getCalendarMonth();
		Integer yearType = security.getCalendarDays();

		Date today = CommonsUtilities.currentDate();
		custodyCommission.setRegisterDate(today);

		boolean leapYear = validateLeapYear();
		if(leapYear){
			custodyCommission.setIndBisiesto(BooleanType.YES.getCode());
		}else{
			custodyCommission.setIndBisiesto(BooleanType.NO.getCode());
		}

		custodyCommission.setDayType(dayType);
		custodyCommission.setYearType(yearType);

		custodyCommissionService.create(custodyCommission);

		return custodyCommission;
	}

	/**
	 * Metodo que retorna lista de saldos totales por cuenta.
	 *
	 * @param lstHolderAccountBalances the lst holder account balances
	 * @return the map
	 */
	private Map<Long,BigDecimal> processHolderAccountBalances(List<Object[]> lstHolderAccountBalances){

		Map<Long,BigDecimal> mpReturn = new HashMap<Long, BigDecimal>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalances)){
			for(Object[] obj : lstHolderAccountBalances){
				Long holderAccount = Long.valueOf(obj[0].toString());
				BigDecimal totalBalance = new BigDecimal(obj[1].toString());

				mpReturn.put(holderAccount,totalBalance);
			}
		}

		return mpReturn;
	}


	/**
	 * Metodo que retorna lista de calculos por impuestos.
	 *
	 * @param monthType the month type
	 * @param strSecurity the str security
	 * @param beginDate the begin date
	 * @param loggerUser the logger user
	 * @return the custody taxes
	 * @throws Exception the exception
	 */
	private Map<Long,BigDecimal> getCustodyTaxes(Integer monthType, String strSecurity, Date beginDate, LoggerUser loggerUser) throws Exception{

		Map<Long,BigDecimal> mpHolderAccountCustodyTax = new HashMap<Long, BigDecimal>();
		if(CalendarMonthType._30.getCode().equals(monthType)){

			throw new Exception("Tipo de año del valor " + strSecurity + " es COMERCIAL. Verifique.");

		}else if(CalendarMonthType.ACTUAL.getCode().equals(monthType)){
			List<Object[]> lstMovements = custodyCommissionService.getMovements(strSecurity,beginDate);
			if(Validations.validateListIsNotNullAndNotEmpty(lstMovements)){

				Long previousHolderAccount = null;
				Date previousDate = null;
				BigDecimal currentCommission = BigDecimal.ZERO;
				BigDecimal currentQuantity = BigDecimal.ZERO;

				for(Object[] movement : lstMovements){
					Integer movementClass = Integer.parseInt(movement[0].toString());
					BigDecimal movementQuantity = new BigDecimal(movement[1].toString());
					Date movementDate = CommonsUtilities.convertStringtoDate(movement[2].toString());
					Long holderAccount = Long.parseLong(movement[3].toString());
					BigDecimal currentNominalValue = new BigDecimal(movement[4].toString());

					if(Validations.validateIsNull(previousHolderAccount)){
						if(BalanceBehaviourType.SUBTRACTION.getCode().equals(movementClass)){
							throw new Exception("Saldo negativo para el valor: " + strSecurity + ". Su primer movimiento es de salida.");
						}else{
							currentQuantity = currentQuantity.add(movementQuantity);
						}
					}else{
						if(previousHolderAccount.equals(holderAccount)){
							if(movementDate.compareTo(previousDate) == 0){
								//WE CALCULATE THE COMMISSION UNTIL THE MOVEMENT DATE
								BigDecimal commission = getCommission(currentQuantity,previousDate,movementDate,currentNominalValue,loggerUser);
								currentCommission = currentCommission.add(commission);
								//FOR SECOND OR NEXT MOVEMENT, SYSTEM MUST CALCULATE AND UPDATE ACTUAL COMMISSION
								if(BalanceBehaviourType.SUBTRACTION.getCode().equals(movementClass)){
									currentQuantity = currentQuantity.subtract(movementQuantity);
								}else{
									currentQuantity = currentQuantity.add(movementQuantity);
								}
							}else{
								mpHolderAccountCustodyTax.put(previousHolderAccount,currentCommission);
								currentCommission = BigDecimal.ZERO;
								currentQuantity = currentQuantity.add(movementQuantity);
							}
						}else{
							mpHolderAccountCustodyTax.put(previousHolderAccount,currentCommission);
							currentQuantity = BigDecimal.ZERO;
							currentQuantity = currentQuantity.add(movementQuantity);
						}
					}
					previousHolderAccount = holderAccount;
					previousDate = movementDate;
				}
				mpHolderAccountCustodyTax.put(previousHolderAccount,currentCommission);
			}
		}

		return mpHolderAccountCustodyTax;
	}

	/**
	 * Metodo que retorna calculo de comision por custodia.
	 *
	 * @param currentQuantity the current quantity
	 * @param previousDate the previous date
	 * @param movementDate the movement date
	 * @param nominalValue the nominal value
	 * @param loggerUser the logger user
	 * @return the commission
	 */
	private BigDecimal getCommission(BigDecimal currentQuantity,Date previousDate,Date movementDate, BigDecimal nominalValue, LoggerUser loggerUser){

		Long MILLSECS_PER_DAY = (long) (24 * 60 * 60 * 1000);

		BigDecimal elapsedDays = new BigDecimal((((movementDate.getTime() - previousDate.getTime())/MILLSECS_PER_DAY)- 1));

		BigDecimal rate = getPercentRate(loggerUser, movementDate);

		BigDecimal commission = currentQuantity.multiply(nominalValue.multiply(elapsedDays.multiply(rate)));


		return commission;
	}

	/**
	 * Metodo que registra comision de custodia madurez.
	 *
	 * @param mpHolderAccountCustody the mp holder account custody
	 * @param securityCode the security code
	 * @param corporativeID the corporative id
	 * @param yearType the year type
	 * @param dayType the day type
	 */
	private void saveMaturityCustodyCommission(Map<Long,BigDecimal> mpHolderAccountCustody, String securityCode, Long corporativeID, Integer yearType, Integer dayType){

		if(mpHolderAccountCustody.size() > 0){

			CustodyCommission custodyCommission = new CustodyCommission();
			custodyCommission.setCorporativeOperation(new CorporativeOperation());
			custodyCommission.getCorporativeOperation().setIdCorporativeOperationPk(corporativeID);

			Date today = CommonsUtilities.currentDate();
			custodyCommission.setRegisterDate(today);

			if(validateLeapYear()){
				custodyCommission.setIndBisiesto(BooleanType.YES.getCode());
			}else{
				custodyCommission.setIndBisiesto(BooleanType.NO.getCode());
			}

			custodyCommission.setDayType(dayType);
			custodyCommission.setYearType(yearType);

			custodyCommissionService.create(custodyCommission);

			for (Map.Entry<Long, BigDecimal> entry : mpHolderAccountCustody.entrySet()){
				Long holderAccount = entry.getKey();
				BigDecimal custodyAmount = entry.getValue();

				//WE SAVE THE DETAIL BY CUSTODY COMMISSION
				CustodyCommissionDetail custodyCommissionDetail = new CustodyCommissionDetail();
				custodyCommissionDetail.setCustodyAmount(custodyAmount);
				custodyCommissionDetail.setCustodyCommission(custodyCommission);
				custodyCommissionDetail.setCustodyQuantity(BigDecimal.ZERO);
				custodyCommissionDetail.setCustodyTax(BigDecimal.ZERO);
				custodyCommissionDetail.setPossessionType(PrimaryBalanceType.SECONDARY_BALANCE.getCode());

				custodyCommissionDetail.setHolderAccount(new HolderAccount());
				custodyCommissionDetail.getHolderAccount().setIdHolderAccountPk(holderAccount);

				custodyCommissionDetail.setSecurity(new Security());
				custodyCommissionDetail.getSecurity().setIdSecurityCodePk(securityCode);

				custodyCommissionService.create(custodyCommissionDetail);

			}
		}
	}
}