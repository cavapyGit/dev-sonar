package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;



// TODO: Auto-generated Javadoc
/**
 * The Class AllocateIssuerCashAccountTO.
 * @author Pradera software
 *
 */
public class BankHolderAllocationDetailTO implements Serializable{

	/** "select "
				+ "pad.bank.idBankPk,"															//[0]
				+ "bpa.currency,"																//[1]
				+ "h.idHolderPk,"																//[2]
				+ "h.fullName,"																	//[3]
				+ "h.documentType,"																//[4]
				+ "h.documentNumber,"															//[5]
				+ "pad.holderAccountBank.idHolderAccountBankPk,"								//[6]
				+ "c.idCorporativeOperationPk,"													//[7]
				+ "c.securities.idSecurityCodePk,"												//[8]
				+ "pad.transactionAmount,"														//[9]
				+ "bpa.indExcludedCommissions,"													//[10]
				+ "pad.processType, "															//[11]
				+ "c.issuer.idIssuerPk, " 														//[12]
				+ "c.corporativeEventType.corporativeEventType, " 								//[13]
				+ "pad.idAllocationDetailPK, " 													//[14]
				+ "nvl(pad.taxAmount,0), " 														//[15]
				+ "nvl(pad.custodyAmount,0), " 													//[16]
				+ "pad.holderAccount.idHolderAccountPk, "										//[17]
				+ "from PaymentAllocationDetail pad, Holder h, CorporativeOperation c, BenefitPaymentAllocation bpa "
				+ "where "
				+ "pad.benefitPaymentAllocation.idBenefitPaymentPK = :idBenefitPayment "
				+ "and bpa.idBenefitPaymentPK = pad.benefitPaymentAllocation.idBenefitPaymentPK "
				+ "and pad.holder.idHolderPk = h.idHolderPk "
				+ "and pad.corporativeOperation.idCorporativeOperationPk = c.idCorporativeOperationPk "
				+ "order by pad.processType,pad.bank.idBankPk,c.corporativeEventType.corporativeEventType,h.idHolderPk ";
	*/
	private static final long serialVersionUID = 1L;
	private Long idBankPk;//0
	private Integer currency;//1
	private Long idHolderPk;//2
	private String fullname;//3
	private Integer documentType;//4
	private String documentNumber;//5
	private Long idHolderAccountBankPk;//6
	private Long idCorporativeOperationPk;//7
	private String idSecurityCodePk;//8
	private BigDecimal transactionAmount;//9
	private Integer indExcludedCommissions;//10
	private Integer processType;//11
	private String idIssuerPk;//12
	private Integer corporativeEventType;//13
	private Long idAllocationDetailPK;//14
	private BigDecimal taxAmount;//15
	private BigDecimal custodyAmount;//16
	private Long idHolderAccountPk;//17
	

	public BankHolderAllocationDetailTO() {
		super();
	}
	
	public BankHolderAllocationDetailTO(Long idBankPk, Integer currency, Long idHolderPk, String fullname,
			Integer documentType, String documentNumber, Long idHolderAccountBankPk, Long idCorporativeOperationPk,
			String idSecurityCodePk, BigDecimal transactionAmount, Integer indExcludedCommissions, Integer processType,
			String idIssuerPk, Integer corporativeEventType, Long idAllocationDetailPK, BigDecimal taxAmount,
			BigDecimal custodyAmount, Long idHolderAccountPk) {
		super();
		this.idBankPk = idBankPk;
		this.currency = currency;
		this.idHolderPk = idHolderPk;
		this.fullname = fullname;
		this.documentType = documentType;
		this.documentNumber = documentNumber;
		this.idHolderAccountBankPk = idHolderAccountBankPk;
		this.idCorporativeOperationPk = idCorporativeOperationPk;
		this.idSecurityCodePk = idSecurityCodePk;
		this.transactionAmount = transactionAmount;
		this.indExcludedCommissions = indExcludedCommissions;
		this.processType = processType;
		this.idIssuerPk = idIssuerPk;
		this.corporativeEventType = corporativeEventType;
		this.idAllocationDetailPK = idAllocationDetailPK;
		this.taxAmount = taxAmount;
		this.custodyAmount = custodyAmount;
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	public Long getIdBankPk() {
		return idBankPk;
	}
	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public Integer getDocumentType() {
		return documentType;
	}
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Long getIdHolderAccountBankPk() {
		return idHolderAccountBankPk;
	}
	public void setIdHolderAccountBankPk(Long idHolderAccountBankPk) {
		this.idHolderAccountBankPk = idHolderAccountBankPk;
	}
	public Long getIdCorporativeOperationPk() {
		return idCorporativeOperationPk;
	}
	public void setIdCorporativeOperationPk(Long idCorporativeOperationPk) {
		this.idCorporativeOperationPk = idCorporativeOperationPk;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public Integer getIndExcludedCommissions() {
		return indExcludedCommissions;
	}
	public void setIndExcludedCommissions(Integer indExcludedCommissions) {
		this.indExcludedCommissions = indExcludedCommissions;
	}
	public Integer getProcessType() {
		return processType;
	}
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	public String getIdIssuerPk() {
		return idIssuerPk;
	}
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	public Integer getCorporativeEventType() {
		return corporativeEventType;
	}
	public void setCorporativeEventType(Integer corporativeEventType) {
		this.corporativeEventType = corporativeEventType;
	}
	public Long getIdAllocationDetailPK() {
		return idAllocationDetailPK;
	}
	public void setIdAllocationDetailPK(Long idAllocationDetailPK) {
		this.idAllocationDetailPK = idAllocationDetailPK;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}
	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
}