package com.pradera.corporateevents.corporativeprocess.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.service.CorporateServiceBean;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporateMonitoringTO;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporativeProcessDetailsTO;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * Clase fachada de procesos corporativos.
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCorporateEvents
 * @Creation_Date :
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class CorporateServiceFacade {
	
	/** The Corporate Monitoring Service Bean. */
	@EJB
	private CorporateServiceBean corporateServiceBean;
	
	/** The batch service. */
	@EJB
	BatchServiceBean batchService;
	
	/** Holds current logger user details. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** Holds the Issuer details. */
	@EJB
	private IssuerQueryServiceBean issuerQueryServiceBean;
	
	
	/**
	 * Metodo que retorna lista de procesos corporativos.
	 *
	 * @param corporateFilter the corporate filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> searchCorporativeOperations(CorporateMonitoringTO corporateFilter) throws ServiceException {
		return corporateServiceBean.searchCorporativeOperations(corporateFilter);
		
	}
	
	/**
	 * Metodo que retorna lista de procesos corporativos con Fecha de Vencimiento por Cuponera de Interes y Amortizacion
	 *
	 * @param corporateFilter the corporate filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> searchCorporativeOperationsByCoupons(CorporateMonitoringTO corporateFilter) throws ServiceException {
		
		List<CorporativeOperation> lstCorporativeOperation = corporateServiceBean.searchCorporativeOperationsByCoupons(corporateFilter);
		for(CorporativeOperation obj : lstCorporativeOperation) {
			
			AccountAnnotationOperation aao = corporateServiceBean.accountAnnotationOperationWithSecurity(obj.getSecurities().getIdSecurityCodePk());
			if(aao!=null) {				
				if(obj.getProgramInterestCoupon()!=null) {//cupon siempre es electronico
					obj.setIndElectronicCupon((aao.getIndElectronicCupon()!=null && aao.getIndElectronicCupon().equals(1)));
				}else {//capital siempre es fisico
					obj.setIndElectronicCupon(false);
				}	
			}else {
				obj.setIndElectronicCupon(true);
			}
			
			List<HolderAccountAdjusment> lst = corporateServiceBean.searchAdjusmentByCorporative(obj.getIdCorporativeOperationPk(),BooleanType.NO.getCode());
			if(Validations.validateListIsNullOrEmpty(lst))
				obj.setIndAdjusmentPending(false);
			else 
				obj.setIndAdjusmentPending(true);
		}
			
		return lstCorporativeOperation;
	}
	
	/**
	 * Metodo que retorna detalle de proceso preliminar/definitivo de evento corporativo
	 *
	 * @param corpOperation the corp operation
	 * @param destinySecurity the destiny security
	 * @param ajustFlag the ajust flag
	 * @return the corporative process details
	 * @throws ServiceException the service exception
	 */
	public CorporativeProcessDetailsTO getCorporativeProcessDetails(CorporativeOperation corpOperation, String destinySecurity, boolean ajustFlag) throws ServiceException{		
		return corporateServiceBean.getCorporativeProcessDetails(corpOperation, destinySecurity, ajustFlag);
	}
	
	/**
	 * Metodo que retorna lista de valores destino de una escision
	 *
	 * @param corpOperationId the corp operation id
	 * @return the excision securities
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getExcisionSecurities(Long corpOperationId) throws ServiceException{
		List<ParameterTable> result = new ArrayList<ParameterTable>();
		List<Object[]> securities = corporateServiceBean.getExcisionSecurities(corpOperationId, true);
		if(securities != null && !securities.isEmpty()){
			for(Object[] data : securities){
				ParameterTable obj = new ParameterTable();
				obj.setParameterTableCd((String)data[0]);
				obj.setDescription((String)data[1]);
				result.add(obj);
			}
		}
		return result;
	}
	
	/**
	 * Metodo que retorna lista de codigo de eventos corporativos
	 *
	 * @param lstProcesses the lst processes
	 * @return List of Long values
	 */
	private List<Long> getProcessIds(List<CorporativeOperation> lstProcesses){
		List<Long> processIds = new ArrayList<Long>();
		for(CorporativeOperation process : lstProcesses){
				processIds.add(process.getIdCorporativeOperationPk());			
		}
		return processIds;
	}
	
	/**
	 * Metodo que retorna split de descripcion de eventos corporativos especiales para mensaje.
	 *
	 * @param securities the securities
	 * @param lstProcesses the lst processes
	 * @return String
	 */
	private String getProcessesString(List<String> securities, List<CorporativeOperation> lstProcesses){
		if(securities != null && !securities.isEmpty()){
			StringBuffer sb = new StringBuffer();
			for(String sec: securities){				
				for(CorporativeOperation process : lstProcesses){
					if(CorporateProcessUtils.isSpecialCorporateProcess(process.getCorporativeEventType().getCorporativeEventType()) 
							&& process.getSecurities().getIdSecurityCodePk().equals(sec)){						
							if(sb.length() > 0){
								sb.append(", ");
							}
							sb.append(sec);
						}
					}
				}
			return sb.toString();
		}
		return null;
	}
	
	/**
	 * Metodo que retorna split de codigo de eventos corporativos para mensaje.
	 *
	 * @param lstProcessIds the lst process ids
	 * @return String
	 */
	private String getProcessesString(List<Long> lstProcessIds){
		if(lstProcessIds != null && lstProcessIds.size() > 0){
			StringBuffer sb = new StringBuffer();			
			for(Long id : lstProcessIds){
				if(sb.length() > 0){
					sb.append(", ");
				}
				sb.append(id);
			}
			return sb.toString();
		}
		return null;
	}	
	
	
	/**
	 * Metodo que retorna emisor.
	 *
	 * @param issuerTo the IssuerSearcherTO
	 * @return the Issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer getIssuerDetails(IssuerSearcherTO issuerTo) throws ServiceException{	 
		return  issuerQueryServiceBean.getIssuerByCode(issuerTo);
		}
	
	/**
	 * Metodo que retorna lista detalle del titular
	 *
	 * @param corpProcessId the corp process id
	 * @param holderId the holder id
	 * @return the joint holders
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeProcessDetailsTO> getJointHolders(Long corpProcessId, Long holderId) throws ServiceException{
 		return corporateServiceBean.getJointHolders(corpProcessId, holderId);
	}
	
	/**
	 * Metodo que retorna detalle de ajuste preliminar de un titular
	 *
	 * @param corpOperation the corp operation
	 * @param security the security
	 * @param idHolderPk the id holder pk
	 * @return the holder adjustment details
	 * @throws ServiceException the service exception
	 */
	public CorporativeProcessDetailsTO getHolderAdjustmentDetails(CorporativeOperation corpOperation, String security, String idHolderPk) throws ServiceException{
		return corporateServiceBean.getHolderAdjustmentDetails(corpOperation, security, idHolderPk);
	}
	
	/**
	 * Metodo que actualiza ajuste preliminar.
	 *
	 * @param details the details
	 * @param corpOper the corp oper
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizeHolderAdjustments(List<CorporativeProcessDetailsTO> details, CorporativeOperation corpOper, String security) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAdjustments());
		corporateServiceBean.actualizeHolderAdjustments(details, corpOper, security);		
	}
	
	
	

	
	/**
	 * Metodo que valida confirmacion de emisor.
	 *
	 * @param lstProcesses the lst processes
	 * @return String
	 * @throws ServiceException the service exception
	 */
	private String verifyEmisorConfirm(List<CorporativeOperation> lstProcesses) throws ServiceException{
		List<Long> processIntAmoIds = new ArrayList<Long>();
		for(CorporativeOperation process : lstProcesses){
			if(process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
				|| process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
				processIntAmoIds.add(process.getIdCorporativeOperationPk());
			}
		}
		return getProcessesString(corporateServiceBean.verifyEmisorConfirm(processIntAmoIds));
		
	}
	
	
	
	
	
	/**
	 * Metodo que valida si existe movimientos despues de ejecutar el preliminar del evento corporativo.
	 *
	 * @param lstProcesses the lst processes
	 * @return String
	 * @throws ServiceException the service exception
	 */
	private String verifyHolderAccountMovementForPreliminary(List<CorporativeOperation> lstProcesses) throws ServiceException{	
		return getProcessesString(corporateServiceBean.verifyHolderAccountMovementsForPreliminary(getProcessIds(lstProcesses)));
	}
	
	/**
	 * Metodo que retorna un evento corporativo.
	 *
	 * @param id the id
	 * @return the corporative operation
	 */
	public CorporativeOperation findCorporativeOperation(Long id){
		CorporativeOperation corporativeOperation = corporateServiceBean.getCorporativeOperation(id);		
		return corporativeOperation;
	}
	
	/**
	 * Metodo que verifica si existe tipo de cambio USD registrado.
	 *
	 * @param lists the lists
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String verifyExchangeRate(List<CorporativeOperation> lists)throws ServiceException{
		for(CorporativeOperation operation: lists){
			if(CurrencyType.USD.getCode().equals(operation.getSecurities().getCurrency())){
				if(Validations.validateIsNull(getUSDExchangeRate())){
					return PropertiesConstants.MESSAGE_NOT_EXIST_ECHANGE_RATE;
				}
			}
			
		}
		return null;
	}
	
	/**
	 * GMetodo que verifica si existe tipo de cambio USD registrado.
	 *
	 * @return the uSD exchange rate
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getUSDExchangeRate()throws ServiceException{
		return corporateServiceBean.getUSDExchangeRate();
	}
	
	/**
	 * Metodo que retorna numero de movimientos de un valor.
	 *
	 * @param idSecurity the id security
	 * @return Quantity Movements For Security
	 * @throws ServiceException the Service Exception
	 */
	public Long getQuantityMovementsForSecurity(String idSecurity)throws ServiceException{
		return corporateServiceBean.getQuantityMovementsForSecurity(idSecurity);
	}
	
	/**
	 * Metodo que retorna lista de valores que tienen stock.
	 *
	 * @param dateCurrent date current
	 * @param state state
	 * @return List Securities Stock By Record Date
	 * @throws ServiceException the Service Exception
	 */
	public LinkedHashMap<String,String> getListSecuritiesStockByRecordDate(Date dateCurrent,Integer state) throws ServiceException{
		return corporateServiceBean.getListSecuritiesStockByRecordDate(dateCurrent,state);
	}
	
	/**
	 * Metodo que retorna codigo de proceso de stock de un valor.
	 *
	 * @param security security
	 * @param dateCurrent date current
	 * @param state state
	 * @return id Stock By Security And Record Date
	 * @throws ServiceException the Service Exception
	 */
	public Long getStockBySecurityAndRecordDate(String security,Date dateCurrent,Integer state) throws ServiceException{
		return corporateServiceBean.getStockBySecurityAndRecordDate(security,dateCurrent,state);
	}
	
	/**
	 * Metodo que retorna si el calculo de Stock para los valores que no tienen cartera, sin Stock Calculation Balance
	 * @param dateCurrent
	 * @param state
	 * @return
	 * @throws ServiceException
	 */
	public boolean  findSecuritiesStockWithoutPortfolioByRecordDate(String security,Date dateCurrent,Integer state) throws ServiceException{
		return corporateServiceBean.findSecuritiesStockWithoutPortfolioByRecordDate(security,dateCurrent,state);
	}
	
}
