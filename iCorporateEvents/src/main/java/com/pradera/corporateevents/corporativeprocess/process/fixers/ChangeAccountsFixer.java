package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;

import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.service.CorporateServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.type.ChangeOwnershipBalanceTypeType;
import com.pradera.model.generalparameter.DailyExchangeRates;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ChangeAccountsFixer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/06/2014
 */
@ApplicationScoped
public class ChangeAccountsFixer implements ChangesFixer, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The corporate service bean. */
	@EJB
	private CorporateServiceBean corporateServiceBean;
	
	/**
	 * Used to update the values in the CorporativeProcesResult.
	 *
	 * @param results <code> List</code>
	 * @param currentRes <code> CorporativeProcessResult</code>
	 * @param loggerUser <code> LoggerUser</code>
	 * @param mpParticipants <code> Map</code>
	 * @param mpChangeOwnerShip <code> List</code>
	 * @param documentFixer <code> DocumentsFixer</code>
	 * @param balanceFixer <code> BalanceFixer</code>
	 * @param round <code> int</code>
	 * @param cashFlag <code> boolean</code>
	 * @param taxFactor <code> BigDecimal</code>
	 * @return the updated result
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeProcessResult> getUpdatedResult(List<CorporativeProcessResult> results, CorporativeProcessResult currentRes,
			LoggerUser loggerUser, Map<Long, Object[]> mpParticipants, Map<Long, List<Object[]>> mpChangeOwnerShip, 
			DocumentsFixer documentFixer, BalanceFixer balanceFixer, int round, boolean cashFlag, BigDecimal taxFactor) throws ServiceException{

		if(mpParticipants != null && mpParticipants.containsKey(currentRes.getHolderAccount().getIdHolderAccountPk())){
			//IN CASE CORPORATIVE OPERATION HAS PARTICIPANT UNIFICATION
			Object[] data = mpParticipants.get(currentRes.getHolderAccount().getIdHolderAccountPk());
			currentRes.getHolderAccount().setIdHolderAccountPk((Long) data[3]);
			currentRes.getParticipant().setIdParticipantPk((Long)data[2]);
			if(!containsCorporativeProcessResult(results,currentRes)){
				results.add(currentRes);
			}
			results = getUpdatedResult(results, currentRes, loggerUser, mpParticipants, mpChangeOwnerShip, documentFixer, balanceFixer, round, cashFlag, taxFactor);
		}
		else if(mpChangeOwnerShip != null && mpChangeOwnerShip.containsKey(currentRes.getHolderAccount().getIdHolderAccountPk())){
			//IN CASE CORPORATIVE OPERATION HAS CHANGE OWNERSHIP
			if(!containsCorporativeProcessResult(results,currentRes)){
				results.add(currentRes);
			}
			//obtenemos datos de las solicitudes de cambio de titularidad por cuenta titular - ORIGEN
			//solicitudes de disponible y bloqueos
			List<Object[]> datas = mpChangeOwnerShip.get(currentRes.getHolderAccount().getIdHolderAccountPk());
			for(Iterator<Object[]> itAux = datas.iterator(); itAux.hasNext();){
				Object[] data = itAux.next();
				//si es cambio de titularidad de disponible
				if(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode().equals(data[3])){
					BigDecimal available = (BigDecimal)data[4];
					BigDecimal availableTotal = BigDecimal.ZERO;
					//obtenemos datos de cambio de titularidad - DESTINO - relacionado al origen
					//obtenemos cuentas y participantes destinos (1 o mas)
					List<Object[]> targetOwners = corporateServiceBean.getTargetAvailableChangeofOwnerShipDetails((Long)data[2], currentRes.getSecurity().getIdSecurityCodePk());					
					//recorremos lista de saldo disponible de cambio de titularidad DESTINO
					for(Iterator<Object[]> it = targetOwners.iterator(); it.hasNext();){
						Object[] target = it.next();
						//preliminar del DESTINO
						CorporativeProcessResult corpRes  = new CorporativeProcessResult();
						corpRes = this.getBalanceZero(corpRes);
						corpRes.setCorporativeOperation(currentRes.getCorporativeOperation());
						corpRes.setIndAdjustment(currentRes.getIndAdjustment());
						corpRes.setIndOriginTarget(currentRes.getIndOriginTarget());
						corpRes.setIndProcessed(currentRes.getIndProcessed());
						corpRes.setIndRemanent(currentRes.getIndRemanent());
						corpRes.setSecurity(currentRes.getSecurity());
						//obtenemos datos del participante y cuenta Destino del cambio de titularidad					
						Participant destPart = new Participant((Long)target[0]);						
						corpRes.setParticipant(destPart);
						HolderAccount ha = new HolderAccount();
						ha.setIdHolderAccountPk((Long)target[1]);
						corpRes.setHolderAccount(ha);		
						//obtenemos disponibles y totales de cada cuenta para su respectivo preliminar
						if(it.hasNext()){
							BigDecimal balance = CorporateProcessUtils.divide((BigDecimal)target[2],available);
							balance = CorporateProcessUtils.calculateByFactor(currentRes.getAvailableBalance(), balance, round);
							availableTotal = availableTotal.add(balance);
							corpRes.setAvailableBalance(balance);
							corpRes.setTotalBalance(balance);							
						}else{
							if(corpRes.getAvailableBalance()==null){
								BigDecimal balance = CorporateProcessUtils.divide((BigDecimal)target[2],available);
								balance = CorporateProcessUtils.calculateByFactor(currentRes.getAvailableBalance(), balance, round);
								availableTotal = availableTotal.add(balance);
								corpRes.setAvailableBalance(balance);
								corpRes.setTotalBalance(balance);				
							}else{
								BigDecimal balance = currentRes.getAvailableBalance().subtract(availableTotal);
								corpRes.setAvailableBalance(balance);
								corpRes.setTotalBalance(balance);
							}	
							if(corpRes.getIndOriginTarget()!=null && 
									corpRes.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY)){
								corpRes.setExchangeAmount(corpRes.getTotalBalance());
							}
							if(cashFlag && taxFactor != null){
								corpRes.setTaxAmount(CorporateProcessUtils.divideForAmount(CorporateProcessUtils.calculateByFactor(
										corpRes.getTotalBalance(),taxFactor,1),CorporateProcessConstants.AMOUNT_ROUND));
							}	
						}
						//agregamos a la lista de preliminares la cuenta DESTINO con sus saldos en disponible y total
						if(!containsCorporativeProcessResult(results,corpRes)){
							results.add(corpRes);
						}
						//recursibidad - actualizamos resultados preliminares para cuenta destino en caso q exista cambio de titularidad 
						results = getUpdatedResult(results, corpRes, loggerUser, mpParticipants, mpChangeOwnerShip, documentFixer, balanceFixer, round, cashFlag, taxFactor);
					}			
					//actualizamos preliminar sus saldo disponible y total a cuenta origen
					currentRes.setTotalBalance(currentRes.getTotalBalance().subtract(currentRes.getAvailableBalance()));
					currentRes.setAvailableBalance(BigDecimal.ZERO);					
				}else if(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode().equals(data[3])){
					//si es cambio de titularidad de bloqueo
					
					//obtenemos factor de gravamen
					BigDecimal pawnFactor = (BigDecimal)data[5];
					if(pawnFactor.compareTo(BigDecimal.ZERO)==1){
						pawnFactor = CorporateProcessUtils.divide(currentRes.getPawnBalance(), (BigDecimal)data[5]);
					}					
					//obtenemos factor de embargo
					BigDecimal banFactor = (BigDecimal)data[6];
					if(banFactor.compareTo(BigDecimal.ZERO)==1){
						banFactor = CorporateProcessUtils.divide(currentRes.getBanBalance(), (BigDecimal)data[6]);
					}				
					//obtenemos factor de otros bloqueos
					BigDecimal otherFactor = (BigDecimal)data[7];
					if(otherFactor.compareTo(BigDecimal.ZERO)==1){
						otherFactor = CorporateProcessUtils.divide(currentRes.getOtherBlockBalance(), (BigDecimal)data[7]);
					}		
					BigDecimal reserveFactor = (BigDecimal)data[8];		
					if(reserveFactor.compareTo(BigDecimal.ZERO)==1){
						reserveFactor = CorporateProcessUtils.divide(currentRes.getReserveBalance(), (BigDecimal)data[8]);
					}	
					BigDecimal oppFactor = (BigDecimal)data[9];
					if(oppFactor.compareTo(BigDecimal.ZERO)==1){
						oppFactor = CorporateProcessUtils.divide(currentRes.getOppositionBalance(), (BigDecimal)data[9]);
					}
					
					//obtenemos saldo de bloqueos dependiendo el tipo de bloqueo
					BigDecimal sumBalance = BigDecimal.ZERO;
					BigDecimal balance = BigDecimal.ZERO;
					
					//obtenemos datos de cambio de titularidad - DESTINO - relacionado al origen
					//obtenemos cuentas y participantes destinos (1 o mas)
					List<Object[]> targetOwners = corporateServiceBean.getTargetBlockChangeofOwnerShipDetails((Long)data[2], currentRes.getSecurity().getIdSecurityCodePk());
					List<BlockCorporativeResult> totalBlocks = new ArrayList<BlockCorporativeResult>();
					List<BlockCorporativeResult> typeBlocks = new ArrayList<BlockCorporativeResult>();
					AffectationType tmpBlockType = null;
					
					for(Iterator<Object[]> it = targetOwners.iterator(); it.hasNext();){
						Object[] target = it.next();
						
						//para el primer registro no realiza ajuste al preliminar
						AffectationType blockType = AffectationType.get((Integer)target[0]);						
						if(tmpBlockType != null && !tmpBlockType.equals(blockType)){
							switch (tmpBlockType){
							case PAWN:
								balance = currentRes.getPawnBalance();							
								break;
							case BAN:
								balance = currentRes.getBanBalance();
								break;
							case OTHERS:
								balance =  currentRes.getOtherBlockBalance();
								break;	
							case RESERVE:
								balance = currentRes.getReserveBalance();
								break;
							case OPPOSITION:
								balance = currentRes.getOppositionBalance();
								break;
							}
							typeBlocks = documentFixer.fixDocuments(typeBlocks, balance, sumBalance, balanceFixer);
							totalBlocks.addAll(typeBlocks);
							typeBlocks.clear();
							sumBalance = BigDecimal.ZERO;
						}
						
						//obtenemos el preliminar a nivel de bloqueo ORIGEN
						Long originOpe = (Long)target[3];
						BlockCorporativeResult bcRes = null;
						for(BlockCorporativeResult tmpBlockRes : currentRes.getBlockCorporativeResults()){
							if(tmpBlockRes.getBlockOperation().getIdBlockOperationPk().equals(originOpe)){
								bcRes = tmpBlockRes;
							}
						}		
						
						if(bcRes != null){
							
							//preliminar a nivel de bloqueo del DESTINO
							BlockCorporativeResult newBlock = new BlockCorporativeResult();
							BlockOperation blockOpe = new BlockOperation();
							blockOpe.setIdBlockOperationPk((Long)target[2]);
							newBlock.setBlockOperation(blockOpe);
							newBlock.setBlockType(blockType.getCode());
							newBlock.setCorporativeOperation(currentRes.getCorporativeOperation());
							Holder h = new Holder();
							h.setIdHolderPk((Long)target[6]);
							newBlock.setHolder(h);
							HolderAccount ha = new HolderAccount();
							ha.setIdHolderAccountPk((Long)target[5]);
							newBlock.setHolderAccount(ha);
							newBlock.setIndCashDividend(bcRes.getIndCashDividend());
							newBlock.setIndContributionReturn(bcRes.getIndContributionReturn());
							newBlock.setIndInterest(bcRes.getIndInterest());
							newBlock.setIndAmortization(bcRes.getIndAmortization());
							newBlock.setIndRescue(bcRes.getIndRescue());
							newBlock.setIndOriginDestiny(bcRes.getIndOriginDestiny());
							newBlock.setIndProcessed(bcRes.getIndProcessed());
							newBlock.setIndRemanent(bcRes.getIndRemanent());
							newBlock.setIndStockDividend(bcRes.getIndStockDividend());
							newBlock.setIndSuscription(bcRes.getIndSuscription());
							newBlock.setParticipant(new Participant((Long)target[4]));
							newBlock.setSecurity(currentRes.getSecurity());
							
							//obtenemos factor
							BigDecimal factor = BigDecimal.ZERO;
							switch (blockType){
							case PAWN:
								factor = pawnFactor;							
								break;
							case BAN:
								factor = banFactor;
								break;
							case OTHERS:
								factor = otherFactor;
								break;	
							case RESERVE:
								factor = reserveFactor;
								break;
							case OPPOSITION:
								factor = oppFactor;
								break;
							}
							
							//guardamos saldo bloqueado para DESTINO
							newBlock.setBlockedBalance(CorporateProcessUtils.calculateByFactor((BigDecimal)target[1], factor, round));
							//acumulamos saldos bloqueados
							sumBalance = sumBalance.add(newBlock.getBlockedBalance());
							//asignamos tipo de bloqueo para el ajuste respectivo
							tmpBlockType = blockType;
							//agregamos a lista de preliminares de bloqueos de DESTINO
							typeBlocks.add(newBlock);
							
							//si es el ultimo preliminar se realiza ajuste al preliminar
							if(!it.hasNext()){
								switch (tmpBlockType){
								case PAWN:
									balance = currentRes.getPawnBalance();							
									break;
								case BAN:
									balance = currentRes.getBanBalance();
									break;
								case OTHERS:
									balance =  currentRes.getOtherBlockBalance();
									break;	
								case RESERVE:
									balance = currentRes.getReserveBalance();
									break;
								case OPPOSITION:
									balance = currentRes.getOppositionBalance();
									break;
								}
								typeBlocks = documentFixer.fixDocuments(typeBlocks, balance, sumBalance, balanceFixer);
								totalBlocks.addAll(typeBlocks);
								typeBlocks.clear();
								sumBalance = BigDecimal.ZERO;
							}
						}						
					}
					//recorremos todos los priliminares DESTINO a nivel de bloqueo 
					//generamos preliminares DESTINO a nivel de cuenta matriz 
					Map<Long, CorporativeProcessResult> holderResults = new HashMap<Long, CorporativeProcessResult>();
					for(BlockCorporativeResult bRes : totalBlocks){
						CorporativeProcessResult holderCorp = null;
						if(holderResults.containsKey(bRes.getHolderAccount().getIdHolderAccountPk())){
							holderCorp = holderResults.get(bRes.getHolderAccount().getIdHolderAccountPk());
						}else{
							holderCorp = new CorporativeProcessResult();
							holderCorp = this.getBalanceZero(holderCorp);
							holderCorp.setAvailableBalance(BigDecimal.ZERO);
							holderCorp.setTotalBalance(BigDecimal.ZERO);
							holderCorp.setCorporativeOperation(currentRes.getCorporativeOperation());
							holderCorp.setIndAdjustment(currentRes.getIndAdjustment());
							holderCorp.setIndOriginTarget(currentRes.getIndOriginTarget());
							holderCorp.setIndProcessed(currentRes.getIndProcessed());
							holderCorp.setIndRemanent(currentRes.getIndRemanent());
							holderCorp.setSecurity(currentRes.getSecurity());					
							holderCorp.setParticipant(bRes.getParticipant());
							holderCorp.setHolderAccount(bRes.getHolderAccount());
							holderCorp.setBlockCorporativeResults(new ArrayList<BlockCorporativeResult>());
							holderResults.put(bRes.getHolderAccount().getIdHolderAccountPk(), holderCorp);
						}
						switch (AffectationType.get(bRes.getBlockType())){
						case PAWN:
							holderCorp.setPawnBalance(holderCorp.getPawnBalance().add(bRes.getBlockedBalance()));							
							break;
						case BAN:
							holderCorp.setBanBalance(holderCorp.getBanBalance().add(bRes.getBlockedBalance()));
							break;
						case OTHERS:
							holderCorp.setOtherBlockBalance(holderCorp.getOtherBlockBalance().add(bRes.getBlockedBalance()));
							break;	
						case RESERVE:
							holderCorp.setReserveBalance(holderCorp.getReserveBalance().add(bRes.getBlockedBalance()));
							break;
						case OPPOSITION:
							holderCorp.setOppositionBalance(holderCorp.getOppositionBalance().add(bRes.getBlockedBalance()));
							break;
						}
						holderCorp.setTotalBalance(holderCorp.getTotalBalance().add(bRes.getBlockedBalance()));						
						holderCorp.getBlockCorporativeResults().add(bRes);
						
					}
					//recorremos preliminares DESTINO a nivel de cuenta matriz 
					for(CorporativeProcessResult finalCpres : holderResults.values()){							
						//agregamos a la lista de preliminares la cuenta DESTINO con sus saldos bloqueados
						if(!containsCorporativeProcessResult(results,finalCpres)){
							if(finalCpres.getIndOriginTarget()!=null && 
									finalCpres.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY)){
								finalCpres.setExchangeAmount(finalCpres.getTotalBalance());
							}
							if(cashFlag && taxFactor != null){
								finalCpres.setTaxAmount(CorporateProcessUtils.divideForAmount(CorporateProcessUtils.calculateByFactor(
										finalCpres.getTotalBalance(),taxFactor,1),CorporateProcessConstants.AMOUNT_ROUND));
							}		
							results.add(finalCpres);
						}else{//si existe preliminar actualizamos saldos
							for(CorporativeProcessResult objCorporativeProcessResult : results){
								if(objCorporativeProcessResult.getSecurity().getIdSecurityCodePk().equals(finalCpres.getSecurity().getIdSecurityCodePk())
										&& objCorporativeProcessResult.getParticipant().getIdParticipantPk().equals(finalCpres.getParticipant().getIdParticipantPk())
										&& objCorporativeProcessResult.getHolderAccount().getIdHolderAccountPk().equals(finalCpres.getHolderAccount().getIdHolderAccountPk())){
									objCorporativeProcessResult.setBanBalance(objCorporativeProcessResult.getBanBalance().add(finalCpres.getBanBalance()));
									objCorporativeProcessResult.setPawnBalance(objCorporativeProcessResult.getPawnBalance().add(finalCpres.getPawnBalance()));
									objCorporativeProcessResult.setOtherBlockBalance(objCorporativeProcessResult.getOtherBlockBalance().add(finalCpres.getOtherBlockBalance()));
									objCorporativeProcessResult.setReserveBalance(objCorporativeProcessResult.getReserveBalance().add(finalCpres.getReserveBalance()));
									objCorporativeProcessResult.setOppositionBalance(objCorporativeProcessResult.getOppositionBalance().add(finalCpres.getOppositionBalance()));
									BigDecimal  blockTotal = objCorporativeProcessResult.getBanBalance().add(objCorporativeProcessResult.getPawnBalance())
											.add(objCorporativeProcessResult.getOtherBlockBalance()).add(objCorporativeProcessResult.getOppositionBalance())
											.add(objCorporativeProcessResult.getReserveBalance());
									objCorporativeProcessResult.setTotalBalance(objCorporativeProcessResult.getAvailableBalance().add(blockTotal));
									if(objCorporativeProcessResult.getIndOriginTarget()!=null && 
											objCorporativeProcessResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY)){
										objCorporativeProcessResult.setExchangeAmount(objCorporativeProcessResult.getTotalBalance());
									}
									if(cashFlag && taxFactor != null){
										objCorporativeProcessResult.setTaxAmount(CorporateProcessUtils.divideForAmount(CorporateProcessUtils.calculateByFactor(
												objCorporativeProcessResult.getTotalBalance(),taxFactor,1),CorporateProcessConstants.AMOUNT_ROUND));
									}	
									objCorporativeProcessResult.setBlockCorporativeResults(finalCpres.getBlockCorporativeResults());
								}
							}
						}						
						//recursibidad - actualizamos resultados preliminares para cuenta destino en caso q exista cambio de titularidad
						results = getUpdatedResult(results, finalCpres, loggerUser, mpParticipants, mpChangeOwnerShip, documentFixer, balanceFixer, round, cashFlag, taxFactor);
					}		
				}		
				//actualizamos preliminar sus saldo disponible y total a cuenta origen
				if(!itAux.hasNext()){
					for(CorporativeProcessResult objCorporativeProcessResult : results){
						if(objCorporativeProcessResult.getSecurity().getIdSecurityCodePk().equals(currentRes.getSecurity().getIdSecurityCodePk())
								&& objCorporativeProcessResult.getParticipant().getIdParticipantPk().equals(currentRes.getParticipant().getIdParticipantPk())
								&& objCorporativeProcessResult.getHolderAccount().getIdHolderAccountPk().equals(currentRes.getHolderAccount().getIdHolderAccountPk()));
						else{
							currentRes.setBanBalance(currentRes.getBanBalance().subtract(objCorporativeProcessResult.getBanBalance()));
							currentRes.setPawnBalance(currentRes.getPawnBalance().subtract(objCorporativeProcessResult.getPawnBalance()));
							currentRes.setOtherBlockBalance(currentRes.getOtherBlockBalance().subtract(objCorporativeProcessResult.getOtherBlockBalance()));
							currentRes.setReserveBalance(currentRes.getReserveBalance().subtract(objCorporativeProcessResult.getReserveBalance()));
							currentRes.setOppositionBalance(currentRes.getOppositionBalance().subtract(objCorporativeProcessResult.getOppositionBalance()));
							BigDecimal  availableTotal = objCorporativeProcessResult.getBanBalance().add(objCorporativeProcessResult.getPawnBalance())
									.add(objCorporativeProcessResult.getOtherBlockBalance()).add(objCorporativeProcessResult.getOppositionBalance())
									.add(objCorporativeProcessResult.getReserveBalance());
							currentRes.setTotalBalance(currentRes.getTotalBalance().subtract(availableTotal));
						}						
					}					
					currentRes.setTaxAmount(BigDecimal.ZERO);	
					currentRes.setExchangeAmount(BigDecimal.ZERO);
				}	
			}		
		}
		return results;
	}
	
	/**
	 * Get Balance Zero.
	 *
	 * @param objCorporativeProcessResult preliminary
	 * @return preliminary balance zero
	 */
	public CorporativeProcessResult getBalanceZero(CorporativeProcessResult objCorporativeProcessResult){
		objCorporativeProcessResult.setAcreditationBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setBanBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setBorrowerBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setBuyBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setCustodyAmount(BigDecimal.ZERO);
		objCorporativeProcessResult.setLenderBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setLoanableBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setMarginBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setOppositionBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setOtherBlockBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setPawnBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setReportedBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setReportingBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setReserveBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setSellBalance(BigDecimal.ZERO);
		objCorporativeProcessResult.setTransitBalance(BigDecimal.ZERO);	
		objCorporativeProcessResult.setTaxAmount(BigDecimal.ZERO);	
		objCorporativeProcessResult.setExchangeAmount(BigDecimal.ZERO);
		return objCorporativeProcessResult;
	}
	
	/**
	 * Contains Corporative Process Result.
	 *
	 * @param results result Corporative Process Result
	 * @param finalCpres Corporative Process Result aux
	 * @return validate Corporative Process Result
	 */
	public boolean containsCorporativeProcessResult(List<CorporativeProcessResult> results,CorporativeProcessResult finalCpres){
		boolean validate = false;
		for(CorporativeProcessResult objCorporativeProcessResult : results){
			if(objCorporativeProcessResult.getSecurity().getIdSecurityCodePk().equals(finalCpres.getSecurity().getIdSecurityCodePk())
					&& objCorporativeProcessResult.getParticipant().getIdParticipantPk().equals(finalCpres.getParticipant().getIdParticipantPk())
					&& objCorporativeProcessResult.getHolderAccount().getIdHolderAccountPk().equals(finalCpres.getHolderAccount().getIdHolderAccountPk())){
				validate = true;	
			}
		}
		return validate;
	}
}
