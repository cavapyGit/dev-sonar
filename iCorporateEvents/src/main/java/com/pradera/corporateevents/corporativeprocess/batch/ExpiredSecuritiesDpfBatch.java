package com.pradera.corporateevents.corporativeprocess.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
// TODO: Auto-generated Javadoc
import com.pradera.model.process.ProcessLoggerDetail;

/**
 * Batch que vence los valores DPF del dia.
 *
 * @author PraderaTechnologies
 */
@BatchProcess(name="ExpiredSecuritiesDpfBatch")
public class ExpiredSecuritiesDpfBatch implements Serializable, JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The corporate process facade. */
	@EJB 
	CorporateProcessServiceFacade corporateProcessFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/**
	 * Inicio de Bachero que redime valores DPF.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		logger.info("INICIO ::::::: ExpiredSecuritiesDpfBatch ");
		// TODO Auto-generated method stub
		try {			
			List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
			String strDpfList= null;
			for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails)
			{
				if ((Validations.validateIsNotNull(objDetail.getParameterName())))
				{
					if (StringUtils.equalsIgnoreCase(GeneralConstants.STR_DPF_SECURITIES_LIST, objDetail.getParameterName())) {
						strDpfList= objDetail.getParameterValue();
					} 
				}
			}
			List<String> lstDpfSecurities= null;
			if (Validations.validateIsNotNullAndNotEmpty(strDpfList)) {
				lstDpfSecurities= new ArrayList<String>();
				String[] arrDpfSecurities= strDpfList.split(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
				for (String idSecurityCode: arrDpfSecurities) {
					lstDpfSecurities.add(idSecurityCode);
				}
			}
			Date currentDate = CommonsUtilities.currentDate();
			corporateProcessFacade.automaticExpiredSecuritiesDpf(currentDate, lstDpfSecurities);			
		} catch (Exception e) {		
			e.printStackTrace();
			/*
			logger.error("Error :::::::::::: ExpiredSecuritiesDpfBatch");			
			String messageNotification = null;
			if (e instanceof ServiceException) {
				ServiceException se = (ServiceException) e;
				messageNotification = PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage(), se.getParams(), new Locale("es"));
			} else {
				messageNotification = "OCURRIO UN ERROR EN EL PROCESO DE VENCIMIENTO AUTOMATICO DE VALORES DPFS";
			}						
			UserFilterTO  userFilter = new UserFilterTO();
			userFilter.setInstitutionType(InstitutionType.DEPOSITARY.getCode());
			List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);						
			notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(), lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, messageNotification, NotificationType.EMAIL);
			*/					
			
		}
		logger.info("FIN ::::::: ExpiredSecuritiesDpfBatch ");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
	
}
