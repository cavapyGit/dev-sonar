package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.math.BigDecimal;
import java.util.List;

import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.ReportCorporativeResult;
import com.pradera.model.generalparameter.DailyExchangeRates;

// TODO: Auto-generated Javadoc
/**
 * The Interface BuyerOperationsFixer.
 *
 * @author PraderaTechnologies
 */
public interface BuyerOperationsFixer {
	
	/**
	 * Fix operations.
	 *
	 * @param operationsSum the operations sum
	 * @param sellerOperations the seller operations
	 * @param corpOperation the corp operation
	 * @param round the round
	 * @param remnantFlag the remnant flag
	 * @param cashFlag the cash flag
	 * @param destiny the destiny
	 * @param balFixer the bal fixer
	 * @param exchangeRateDay the exchange rate day
	 * @return the list
	 */
	public List<ReportCorporativeResult> fixOperations(List<Object[]> operationsSum, List<Object[]> sellerOperations, 
			CorporativeOperation corpOperation, int round, boolean remnantFlag, boolean cashFlag, boolean destiny, BalanceFixer balFixer,
			DailyExchangeRates exchangeRateDay);

	
	/**
	 * Fix operations.
	 *
	 * @param operations the operations
	 * @param reportBalance the report balance
	 * @param totalBalance the total balance
	 * @param balFixer the bal fixer
	 * @return the list
	 */
	public List<ReportCorporativeResult> fixOperations(List<ReportCorporativeResult> operations, BigDecimal reportBalance,
			BigDecimal totalBalance, BalanceFixer balFixer);
	
	/**
	 * Fill origin operations.
	 *
	 * @param reportOperations the report operations
	 * @param corpOperation the corp operation
	 * @param exchangeRateDay the exchange rate day
	 * @return the list
	 */
	public List<ReportCorporativeResult> fillOriginOperations(List<Object[]> reportOperations,	CorporativeOperation corpOperation,DailyExchangeRates exchangeRateDay);
	
}
