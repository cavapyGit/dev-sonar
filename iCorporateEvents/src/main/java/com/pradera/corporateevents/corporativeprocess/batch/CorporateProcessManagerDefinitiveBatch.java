package com.pradera.corporateevents.corporativeprocess.batch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateServiceFacade;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CorporateProcessServiceBean;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporateMonitoringTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
// TODO: Auto-generated Javadoc

/**
 *  Bachero que ejecuta proceso definitivo automatico.
 *
 * @author PraderaTechnologies
 */
@BatchProcess(name="CorporateProcessManagerDefinitiveBatch")
@RequestScoped
public class CorporateProcessManagerDefinitiveBatch implements JobExecution, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst definitive notification. */
	private LinkedHashMap lstSecurity,lstCorporativeProcessAll,flagSecurityProcessDefinitive,flagEndDefinitive,lstTimeDefinitiveProcess,
						lstDefinitiveNotification;
	
	/** The flag mensaje. */
	private Boolean flagCreateArrayEndDefinitive=Boolean.FALSE,flagReprocess = Boolean.FALSE,flagMensaje = Boolean.FALSE;
			
	/** The date process corporate. */
	private Date dateProcessCorporate;
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The batch service. */
	@EJB
	private BatchServiceBean batchService;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate service facade. */
	@EJB
	private CorporateServiceFacade corporateServiceFacade;
	
	/** The obj corporate process service facade. */
	@EJB
	CorporateProcessServiceFacade objCorporateProcessServiceFacade;
	
	/** The corporate process service. */
	@EJB
	private CorporateProcessServiceBean corporateProcessService;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade; 
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The quantity block stock. */
	private int quantityBlockStock;
	
	/** The limit block. */
	private Integer limitBlock = null;//cantidad por bloque
	
	/** The wait time. */
	private Integer waitTime = null;//tiempo de espera despues de lanzar un bloque
	
	/** The wait time process. */
	private Integer waitTimeProcess = null;//tiempo de espera de un proceso enviado que no finaliza
	
	/** The user id. */
	private String userId = null;//usuario demonio
	
	/** The current date. */
	private Date currentDate = null;//fecha dinamica para pruebas (debe ser la fecha del servidor)
	
	/**
	 * Inicio del Bachero que ejecuta proceso definitivo automatico.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger){
		
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		
		//obtenemos variables del procesos automatico
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			if(processLoggerDetail.getParameterName().equals("limitBlock") && processLoggerDetail.getParameterValue()!=null){
				limitBlock = Integer.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("waitTime") && processLoggerDetail.getParameterValue()!=null){
				waitTime = Integer.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("userId")){
				userId = processLoggerDetail.getParameterValue();
			}else if(processLoggerDetail.getParameterName().equals("currentDate") && processLoggerDetail.getParameterValue()!=null){
				currentDate = CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue(),CommonsUtilities.DATE_PATTERN);
			}else if(processLoggerDetail.getParameterName().equals("waitTimeProcess") && processLoggerDetail.getParameterValue()!=null){
				waitTimeProcess = Integer.valueOf(processLoggerDetail.getParameterValue());
			}	
		}
		
		//validamos variables del procesos automatico
		if(Validations.validateIsNotNull(limitBlock) && Validations.validateIsNotNull(waitTime) && Validations.validateIsNotNull(waitTimeProcess)
				&& Validations.validateIsNotNull(userId) && Validations.validateIsNotNull(currentDate)){
			log.info("Execute Corporate process\n");
			try {
				log.info("<<<<<<<<<<<<<<<<< INICIO DE EJECUCION DEL PROCESO DEMONIO DEFINITIVO - 1 : >>>>>>>>>>>>>>>>>>>>>>>>>"+
						"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
				executeDefinitiveCorporateProcess();
				//Se reprocesa para verificar y ejecutar definitivo q no se ejecutaron
				if(flagReprocess.equals(Boolean.FALSE)){
					flagCreateArrayEndDefinitive = Boolean.FALSE;
					lstSecurity= null;
					flagReprocess = Boolean.TRUE;
					log.error("<<<<<<<<<<<<<<<<< INICIO DE EJECUCION DEL PROCESO DEMONIO DEFINITIVO - 2 : >>>>>>>>>>>>>>>>>>>>>>>>>"+
							"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
					executeDefinitiveCorporateProcess();					
				}
			} catch (ServiceException e) {
				log.error(e.getMessage());
				throw new RuntimeException(e);
			}
		}else{
			log.error("Invalied parameters for the corporate process Manager execution\n");
			throw new RuntimeException("Invalied parameters for the corporate process Manager execution");
		}		
	}
	
	/**
	 * Metodo que ejecuta los definitivos de los procesos corporativos en bloques.
	 *
	 * @throws ServiceException the base exception
	 */
	public void executeDefinitiveCorporateProcess()throws ServiceException{
		try{
			corporateProcessDefinitive(limitBlock);			
			while(!endCorporateProcessDefinitive())
			{
				Thread.sleep(waitTime.intValue()*CorporateProcessConstants.MILLSECS_IN_MIN);
				corporateProcessDefinitive(limitBlock.intValue());		
			}
		}catch (Exception e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Metodo que verifica si termino de procesar definitivos de todos los bloques correctamente.
	 *
	 * @return validate end Corporate Process Preliminary
	 * @throws ServiceException the service exception
	 */
	public boolean endCorporateProcessDefinitive()throws ServiceException{
		boolean flag = true;
		//creamos objetos solo una vez al dia
		createObject();
		
		if(flagEndDefinitive.containsValue(Boolean.FALSE)){
			flag = false;
		}			
		
		if(flag==true){
			log.error("<<<<<<<<<<<<<<<<< SE TERMINO DE EJECUTAR EL PROCESO DEMONIO DEFINITIVO: >>>>>>>>>>>>>>>>>>>>>>>>>"+
					"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
		}			
		
		return flag;
	}
	
	
	/**
	 * Metodo que ejecuta definitivo del proceso corporativo.
	 *
	 * @param limit limit block
	 * @throws ServiceException the base exception
	 */
	public void corporateProcessDefinitive(int limit)throws ServiceException{
		try{
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<RESUMEN>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			// El siguiente bachero realizara el proceso corporativo (Definitivo) automatico de los beneficios
			// Primero listara todos los hechos de importancia que estaran encluidos en el proceso
			// Segundo listara los valores de forma priorizada (numero de movimientos) que se ejecutara definitivos por bloques
			// Tercero listara todos los hechos de importancia como ayuda para verificar la ejecucion de los definitivo y envio de notificaciones
						
			// Cuarto ejecutara definitivos en bloque (numero de bloque es paramatizable) - solo se ejecutara definitivos de otro bloque si finaliza el bloque anterior
			// Quinto ejecutara definitivos de los valores que tengan estado preliminar ejecutado - solo se ejecutara definitivos de otro bloque si finaliza el bloque anterior
			
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<01>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//Obtenemos los procesos corporativos en dos listas para ayuda
			//La primera lista es para obtener los valores que se ejecutara los definitivos en la fecha de entrega correspondiente
			//La segunda lista es para verificar si se lanzaron los definitivos correspondientes al valor que tiene preliminares en la fecha de registro y corte correspondiente
			//Se creara listas auxiliares como ayuda para verificar la ejecucion correcta del definitivo y el envio de notificaciones
			if(Validations.validateIsNull(lstSecurity) || (Validations.validateIsNotNull(lstSecurity) && lstSecurity.isEmpty())){
				//creamos objetos 
				
				//se crea lista valores que se ejecutara definitivos (solo una vez en el dia)	
				//se crea lista estado de definitivos en proceso (enviados)
				//se inicializa en cero cantidad de bloques (solo una vez al dia)
				//se crea lista de tiempo de inicio de proceso enviado de definitivo
				//se crea lista de notificacion de definitivos finalizadas (Correctas - Fallidas)
				
				//se crea lista estado de definitivos terminados
				//se crea lista de todos los hechos de importancia como ayuda para verificar la ejecucion de los definitivos y envio de notificaciones
								
				createObject();			
				
				//se crea lista de todos los hechos de importancia para luego obtener todos los valores que se ejecutara los definitivos en bloques
				//se crea lista de valores que se ejecutara los definitivos en bloques
				//se reordenara lista de valores que se ejecutara los definitivos en bloques
				
				loadListSecurityExecution();
			}
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<02>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			//se verificara si todos los valores de un bloque se ejecuto su definitivo 
			if(validateSecurityProcessDefinitive() && Validations.validateIsNotNull(lstSecurity) && !lstSecurity.isEmpty()){	
				//verificamos si el definitivo finalizo y se envia notificaciones de definitivo
				setStateEndDefinitiveAndNotification(lstSecurity);
			}else if (Validations.validateIsNotNull(lstSecurity) && !lstSecurity.isEmpty()){
				//enviamos definitivo en bloques
				executeProcessDefinitive(lstSecurity,limit);
				//monitoreamos definitivos enviados
				monitoringDefinitive(lstSecurity);
			}
		}catch (Exception e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Metodo que verifica el estado del proceso corporativo y envia notificaciones del estado respectivo.
	 *
	 * @param lstSecurity the lst security
	 */
	public void setStateEndDefinitiveAndNotification(LinkedHashMap<String, String> lstSecurity){
		if(flagMensaje==false)
		{
			log.error("<<<<<<<<<<<<<<<<< PROCESANDO NOTIFICACIONES DE DEFINITIVOS (FALLIDO - TERMINADO) >>>>>>>>>>>>>>>>>>>>>>>>>"
		+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			flagMensaje=true;
		}	
		//recorremos lista de todos los valores procesados
		for (Map.Entry<String, String> objSecurity : lstSecurity.entrySet()){
			//obtenemos codigo del valor
			String idSecurity = objSecurity.getKey();	
			//obtenemos lista de procesos corporativos asociados al valor
			List<CorporativeOperation> lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(idSecurity,lstCorporativeProcessAll);
			//recorremos lista de procesos corporativos asosciados al valor
			for(CorporativeOperation objCorporativeOperation : lstCorporativeEventsExecuteDefinitive){
				//verifica si operacion tiene definitivo para no volver a mandar la notificacion
				Boolean flagDefinitive= (Boolean)flagEndDefinitive.get(objCorporativeOperation.getIdCorporativeOperationPk());
				if(flagDefinitive.equals(Boolean.FALSE)){
					CorporativeOperation corporativeOperationAux = corporateServiceFacade.findCorporativeOperation(objCorporativeOperation.getIdCorporativeOperationPk());
					if(corporativeOperationAux.getState().equals(CorporateProcessStateType.DEFINITIVE.getCode())){
						log.error("DEFINITIVO TERMINADO"+corporativeOperationAux.getIdCorporativeOperationPk()+"\n");		
						flagEndDefinitive.put(objCorporativeOperation.getIdCorporativeOperationPk(), Boolean.TRUE);									
						//enviamos notificacion de preliminar fallido
						String id_error = CorporateProcessConstants.MSG_DEFINITIVE_END+GeneralConstants.UNDERLINE_SEPARATOR+objCorporativeOperation.getIdCorporativeOperationPk();
						if(!lstDefinitiveNotification.containsKey(id_error)){						
							lstDefinitiveNotification.put(id_error, objCorporativeOperation.getIdCorporativeOperationPk());
							String strMessage = "Definitivo terminado para el proceso: " + objCorporativeOperation.getIdCorporativeOperationPk()+". Verifique";	
							notificationDefinitiveNotValid(strMessage);
						}	
					}else if(corporativeOperationAux.getState().equals(CorporateProcessStateType.DEFINITIVE_FAIL.getCode()) || 
							corporativeOperationAux.getState().equals(CorporateProcessStateType.PRELIMINARY.getCode())){
						log.error("DEFINITIVO FALLIDO: "+corporativeOperationAux.getIdCorporativeOperationPk()+"\n");		
						flagEndDefinitive.put(objCorporativeOperation.getIdCorporativeOperationPk(), Boolean.TRUE);									
						//enviamos notificacion de preliminar fallido
						String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_FAIL+GeneralConstants.UNDERLINE_SEPARATOR+objCorporativeOperation.getIdCorporativeOperationPk();
						if(!lstDefinitiveNotification.containsKey(id_error)){						
							lstDefinitiveNotification.put(id_error, objCorporativeOperation.getIdCorporativeOperationPk());
							String strMessage = "Error en el proceso de definitivo para el proceso: " + objCorporativeOperation.getIdCorporativeOperationPk()+". Verifique";	
							notificationDefinitiveNotValid(strMessage);
						}		
					}else if(corporativeOperationAux.getState().equals(CorporateProcessStateType.DEFINITIVE_IN_PROCESS.getCode())){
						//verificamos el tiempo de espera del definitivo enviado del valor
						if(lstTimeDefinitiveProcess.containsKey(corporativeOperationAux.getIdCorporativeOperationPk())){
							Date initialTime = (Date)lstTimeDefinitiveProcess.get(corporativeOperationAux.getIdCorporativeOperationPk());
							Date time = CommonsUtilities.currentDateTime();
							int min= (int)((double)(time.getTime() - initialTime.getTime())/(double)(CorporateProcessConstants.MILLSECS_IN_MIN));
							if(min<=waitTimeProcess.intValue());
							else{
								log.error("EXPIRO EL TIEMPO DE ESPERA DEL DEFINITIVO: "+corporativeOperationAux.getIdCorporativeOperationPk()+"\n");		
								flagEndDefinitive.put(objCorporativeOperation.getIdCorporativeOperationPk(), Boolean.TRUE);									
								//enviamos notificacion de preliminar fallido
								String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_FAIL_FOR_TIME+GeneralConstants.UNDERLINE_SEPARATOR+objCorporativeOperation.getIdCorporativeOperationPk();
								if(!lstDefinitiveNotification.containsKey(id_error)){						
									lstDefinitiveNotification.put(id_error, objCorporativeOperation.getIdCorporativeOperationPk());
									String strMessage = "Expiro el tiempo de espera del definitivo para el proceso: " + objCorporativeOperation.getIdCorporativeOperationPk()+". Verifique";	
									notificationDefinitiveNotValid(strMessage);
								}									
							}
						}else{
							lstTimeDefinitiveProcess.put(corporativeOperationAux.getIdCorporativeOperationPk(), CommonsUtilities.currentDateTime());
						}
					}
				}
			}
		}
	}
	
	/**
	 * Metodo que ejecuta definitivos de los procesos corporativos en bloques.
	 *
	 * @param lstSecurity List Security
	 * @param limit the limit
	 * @throws ServiceException the base exception
	 */
	public void executeProcessDefinitive(LinkedHashMap<String, String> lstSecurity,int limit)throws ServiceException{
		//El presente metodo envia los definitivos por bloques. Por cada finalizacion o no finalizacion de un definitivo
		//se enviara notificaciones al usuario respectivo. El dicho proceso
		//se repetetira por cada bloque. El numero de objetos por bloque sera parametrizable, asi mismo el tiempo
		//de espera por cada lanzamiento de un bloque.
		try{
			//inicializamos recorrido para cada bloque de stock
			int counterBlock=0;//contador de numero de objetos por bloques
			int counterDefinitiveProcessForSecurity=0;//contador de numero valores que se ejecutaron definitivos
			int counterInitial = quantityBlockStock*limit;//inicio del bloque
			int count = 0;
			//recorremos los valores en bloques
			for (Map.Entry<String, String> objSecurity : lstSecurity.entrySet()){
				//empezamos a procesar a partir del inicio del bloque
				if(count<counterInitial){
					count++;
				}else{
					//obtenemos codigo del valor
					String idSecurity = objSecurity.getKey();			
					log.error("<<<<<<<<<<<<<<<<< BLOQUE "+(quantityBlockStock+1)+" - VALOR "+ idSecurity
							+ "  >>>>>>>>>>>>>>>>>>>>>>>>>"+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");	
					
					//verificamos si se envio el definitvo de todos los corporativos asociados al valor
					Boolean flagDefinitive= (Boolean)flagSecurityProcessDefinitive.get(idSecurity);
					if(flagDefinitive.equals(Boolean.FALSE)){
						//ejecutamos preliminares de procesos corporativos del valor 
						flagSecurityProcessDefinitive.put(idSecurity, Boolean.TRUE);
						executeDefinitive(idSecurity,lstCorporativeProcessAll);
						counterDefinitiveProcessForSecurity++;		
					}else{
						counterDefinitiveProcessForSecurity++;
					}
					
					counterBlock++;	
					
					//verifica si el numero de valores que ejecutaron definitivo es igual limite del bloque entonces aumentamos cantidad de bloque
					if(counterDefinitiveProcessForSecurity==limit)
						quantityBlockStock++;	
					
					//verifica si el recorrido es igual limite del bloque entonces salimos del recorrido de la lista de valores
					if(counterBlock==limit)
						break;	
				}
			}
		}catch (ServiceException e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Metodo que ejecuta el definitivo del proceso corporativo automatico.
	 *
	 * @param idSecurity id security
	 * @param lstCorporativeEventsAll the lst corporative events all
	 * @throws ServiceException the Service Exception
	 */
	public void executeDefinitive(String idSecurity,LinkedHashMap<Long, CorporativeOperation> lstCorporativeEventsAll)throws ServiceException{		
		//obtenemos lista de procesos corporativos asociados al valor
		List<CorporativeOperation> lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(idSecurity,lstCorporativeEventsAll);
	
		//validamos proceso definitivo
		lstCorporativeEventsExecuteDefinitive = validateDefinitiveProcess(lstCorporativeEventsExecuteDefinitive);
		if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
			//recorremos lista de procesos corporativos asociados al valor y ejecutamos el definitivo
			for (CorporativeOperation objCorporativeOperation : lstCorporativeEventsExecuteDefinitive){	
				 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(objCorporativeOperation.getCorporativeEventType().getCorporativeEventType()) 
						 || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(objCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
					 //Validating if the Security has desmaterialized Balance
					 BigDecimal desmaterializedBalance = corporateProcessService.searchDesmaterializedBalance(objCorporativeOperation.getSecurities().getIdSecurityCodePk());
					 if(desmaterializedBalance.compareTo(BigDecimal.ZERO) > 0 &&
							 corporateProcessService.hasBalance(objCorporativeOperation.getSecurities().getIdSecurityCodePk())){					
						 	BusinessProcess businessProcess = new BusinessProcess();
							businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTE_DEFINITIVE_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
						    Map<String, Object> param = new HashMap<String, Object>();				    
						    param.put("processid", objCorporativeOperation.getIdCorporativeOperationPk());
						    param.put("userid", userId);		
						    param.put("corporateType", objCorporativeOperation.getCorporativeEventType().getDescription());	
						    batchService.registerBatchTx(userId, businessProcess, param);			
					 }else{
						 objCorporateProcessServiceFacade.setDataForCorporativeProcessFail(objCorporativeOperation);
					 }
				 }else{				
					 BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTE_DEFINITIVE_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
					    Map<String, Object> param = new HashMap<String, Object>();				    
					    param.put("processid", objCorporativeOperation.getIdCorporativeOperationPk());
					    param.put("userid", userId);		
					    param.put("corporateType", objCorporativeOperation.getCorporativeEventType().getDescription());	
					    batchService.registerBatchTx(userId, businessProcess, param);		
				 }					
			}
		}				
	}
	
	/**
	 * Metodo que valida proceso corporativo y retorna lista de errores.
	 *
	 * @param lstCorporativeEventsExecuteDefinitive the lst corporative events execute definitive
	 * @return list Corporative Events Execute Definitive
	 */
	public List<CorporativeOperation> validateDefinitiveProcess(List<CorporativeOperation> lstCorporativeEventsExecuteDefinitive){
		try{
			if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
				RecordValidationType objDefinitiveAmortization = corporateProcessService.verifyDefinitiveAmortization(lstCorporativeEventsExecuteDefinitive,true);
				if(Validations.validateIsNotNull(objDefinitiveAmortization)){
					for(Long idCorporativeProcess : objDefinitiveAmortization.getErrorProcess()){
						String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_AMORTIZATION+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
						if(!lstDefinitiveNotification.containsKey(id_error)){						
							lstDefinitiveNotification.put(id_error, idCorporativeProcess);
						}
					}
					notificationDefinitiveNotValid(objDefinitiveAmortization.getErrorDescription());	
					lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objDefinitiveAmortization.getErrorProcess());
				}
				if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
					RecordValidationType objSecurityStatus = corporateProcessService.verifySecurityStatus(lstCorporativeEventsExecuteDefinitive,true);
					if(Validations.validateIsNotNull(objSecurityStatus)){
						for(Long idCorporativeProcess : objSecurityStatus.getErrorProcess()){
							String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_SECURITY_STATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
							if(!lstDefinitiveNotification.containsKey(id_error)){						
								lstDefinitiveNotification.put(id_error, idCorporativeProcess);
							}
						}
						notificationDefinitiveNotValid(objSecurityStatus.getErrorDescription());	
						lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objSecurityStatus.getErrorProcess());
					}
					if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
						RecordValidationType objSecurityDesmaterialized = corporateProcessService.verifySecurityDesmaterialized(lstCorporativeEventsExecuteDefinitive,true);
						if(Validations.validateIsNotNull(objSecurityDesmaterialized)){
							for(Long idCorporativeProcess : objSecurityDesmaterialized.getErrorProcess()){
								String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_SECURITY_DESMATERIALIZED+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
								if(!lstDefinitiveNotification.containsKey(id_error)){						
									lstDefinitiveNotification.put(id_error, idCorporativeProcess);
								}
							}
							notificationDefinitiveNotValid(objSecurityDesmaterialized.getErrorDescription());	
							lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objSecurityDesmaterialized.getErrorProcess());
						}
						if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
							RecordValidationType objProcessStates = corporateProcessService.verifyDefinitiveProcessStates(lstCorporativeEventsExecuteDefinitive,true);
							if(Validations.validateIsNotNull(objProcessStates)){
								for(Long idCorporativeProcess : objProcessStates.getErrorProcess()){
									String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_PROCESS_STATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
									if(!lstDefinitiveNotification.containsKey(id_error)){						
										lstDefinitiveNotification.put(id_error, idCorporativeProcess);
									}
								}
								notificationDefinitiveNotValid(objProcessStates.getErrorDescription());	
								lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objProcessStates.getErrorProcess());
							}
							if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
								RecordValidationType objSituationDefinitiveProcess = corporateProcessService.verifySituationDefinitiveProcess(lstCorporativeEventsExecuteDefinitive,true);
								if(Validations.validateIsNotNull(objSituationDefinitiveProcess)){
									for(Long idCorporativeProcess : objSituationDefinitiveProcess.getErrorProcess()){
										String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_SITUATION+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
										if(!lstDefinitiveNotification.containsKey(id_error)){						
											lstDefinitiveNotification.put(id_error, idCorporativeProcess);
										}
									}
									notificationDefinitiveNotValid(objSituationDefinitiveProcess.getErrorDescription());	
									lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objSituationDefinitiveProcess.getErrorProcess());
								}
								if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
									RecordValidationType objPreliminarDeliveryDate = corporateProcessService.verifyPreliminarDeliveryDate(lstCorporativeEventsExecuteDefinitive,true);
									if(Validations.validateIsNotNull(objPreliminarDeliveryDate)){
										for(Long idCorporativeProcess : objPreliminarDeliveryDate.getErrorProcess()){
											String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_PRELIMINARY_DELIVERY_DATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
											if(!lstDefinitiveNotification.containsKey(id_error)){						
												lstDefinitiveNotification.put(id_error, idCorporativeProcess);
											}
										}
										notificationDefinitiveNotValid(objPreliminarDeliveryDate.getErrorDescription());	
										lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objPreliminarDeliveryDate.getErrorProcess());
									}
									if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
//										RecordValidationType objDefinitiveDeliveryDate = corporateProcessService.verifyDefinitiveDeliveryDate(lstCorporativeEventsExecuteDefinitive,true);
//										if(Validations.validateIsNotNull(objDefinitiveDeliveryDate)){
//											for(Long idCorporativeProcess : objDefinitiveDeliveryDate.getErrorProcess()){
//												String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_DELIVERY_DATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
//												if(!lstDefinitiveNotification.containsKey(id_error)){						
//													lstDefinitiveNotification.put(id_error, idCorporativeProcess);
//												}
//											}
//											notificationDefinitiveNotValid(objDefinitiveDeliveryDate.getErrorDescription());	
//											lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objDefinitiveDeliveryDate.getErrorProcess());
//										}
										if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
											RecordValidationType objProcessPreliminaryExecutedDate = corporateProcessService.verifyProcessPreliminaryExecutedDate(lstCorporativeEventsExecuteDefinitive, CommonsUtilities.currentDate(),true);
											if(Validations.validateIsNotNull(objProcessPreliminaryExecutedDate)){
												for(Long idCorporativeProcess : objProcessPreliminaryExecutedDate.getErrorProcess()){
													String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_EXECUTE_DATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
													if(!lstDefinitiveNotification.containsKey(id_error)){						
														lstDefinitiveNotification.put(id_error, idCorporativeProcess);
													}
												}
												notificationDefinitiveNotValid(objProcessPreliminaryExecutedDate.getErrorDescription());	
												lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objProcessPreliminaryExecutedDate.getErrorProcess());
											}
											if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
												RecordValidationType objHolderAccountAdjustments = corporateProcessService.verifyHolderAccountAdjustments(lstCorporativeEventsExecuteDefinitive,CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,true);
												if(Validations.validateIsNotNull(objHolderAccountAdjustments)){
													for(Long idCorporativeProcess : objHolderAccountAdjustments.getErrorProcess()){
														String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_HOLDER_ACCOUNT_ADJUSTMENTS+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
														if(!lstDefinitiveNotification.containsKey(id_error)){						
															lstDefinitiveNotification.put(id_error, idCorporativeProcess);
														}
													}
													notificationDefinitiveNotValid(objHolderAccountAdjustments.getErrorDescription());	
													lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objHolderAccountAdjustments.getErrorProcess());
												}
												if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
													RecordValidationType objIssuerAmount = corporateProcessService.verifyIssuerAmount(lstCorporativeEventsExecuteDefinitive,true);
													if(Validations.validateIsNotNull(objIssuerAmount)){
														for(Long idCorporativeProcess : objIssuerAmount.getErrorProcess()){
															String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_ISSUER_AMOUNT+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
															if(!lstDefinitiveNotification.containsKey(id_error)){						
																lstDefinitiveNotification.put(id_error, idCorporativeProcess);
															}
														}
														notificationDefinitiveNotValid(objIssuerAmount.getErrorDescription());	
														lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objIssuerAmount.getErrorProcess());
													}
													if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
														RecordValidationType objHolderAccountsMovement = corporateProcessService.verifyHolderAccountsMovement(lstCorporativeEventsExecuteDefinitive,true);
														if(Validations.validateIsNotNull(objHolderAccountsMovement)){
															for(Long idCorporativeProcess : objHolderAccountsMovement.getErrorProcess()){
																String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_PRELIMINARY_HOLDER_MOVEMENT+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
																if(!lstDefinitiveNotification.containsKey(id_error)){						
																	lstDefinitiveNotification.put(id_error, idCorporativeProcess);
																}
															}
															notificationDefinitiveNotValid(objHolderAccountsMovement.getErrorDescription());	
															lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objHolderAccountsMovement.getErrorProcess());
														}
														if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
															RecordValidationType objHolderAccountsMovementDefinitive = corporateProcessService.verifyHolderAccountsMovementDefinitive(lstCorporativeEventsExecuteDefinitive,true);
															if(Validations.validateIsNotNull(objHolderAccountsMovementDefinitive)){
																for(Long idCorporativeProcess : objHolderAccountsMovementDefinitive.getErrorProcess()){
																	String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_HOLDER_MOVEMENT+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
																	if(!lstDefinitiveNotification.containsKey(id_error)){						
																		lstDefinitiveNotification.put(id_error, idCorporativeProcess);
																	}
																}
																notificationDefinitiveNotValid(objHolderAccountsMovementDefinitive.getErrorDescription());	
																lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objHolderAccountsMovementDefinitive.getErrorProcess());
															}
															if(lstCorporativeEventsExecuteDefinitive!=null && lstCorporativeEventsExecuteDefinitive.size()>0){
																RecordValidationType objAccreditationBalance= null;
																//RecordValidationType objAccreditationBalance= corporateProcessService.verifyAccreditationBalance(lstCorporativeEventsExecuteDefinitive,true);
																if(Validations.validateIsNotNull(objAccreditationBalance)){
																	for(Long idCorporativeProcess : objAccreditationBalance.getErrorProcess()){
																		String id_error = CorporateProcessConstants.ERROR_DEFINITIVE_ACCREDITATION_BALANCE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
																		if(!lstDefinitiveNotification.containsKey(id_error)){						
																			lstDefinitiveNotification.put(id_error, idCorporativeProcess);
																		}
																	}
																	notificationDefinitiveNotValid(objAccreditationBalance.getErrorDescription());	
																	lstCorporativeEventsExecuteDefinitive = getListCorporativeEventsExecuteDefinitive(lstCorporativeEventsExecuteDefinitive,objAccreditationBalance.getErrorProcess());
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}catch (ServiceException e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
		return lstCorporativeEventsExecuteDefinitive;
	}
	
	/**
	 * Metodo que notifica proceso corporativo no valido.
	 *
	 * @param strMessage  Message
	 */
	public void notificationDefinitiveNotValid(String strMessage){
		BusinessProcess businessProcessNotification = new BusinessProcess();					
		businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.MANAGER_CORPORATIVE_PROCESS_AUTOMATIC_DEFINITIVE.getCode());
		UserFilterTO  userFilter=new UserFilterTO();
		userFilter.setUserName(userId);
		List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);
		notificationServiceFacade.sendNotificationManual(userId, businessProcessNotification, lstUserAccount,
				GeneralConstants.NOTIFICATION_DEFAULT_HEADER, strMessage, NotificationType.EMAIL);
	}
	
	/**
	 * Metodo que retorna lista de procesos corporativos no validados.
	 *
	 * @param lstCorporativeEventsExecuteDefinitive the lst corporative events execute definitive
	 * @param lstErrorProcess  List Error Process
	 * @return List Corporative Events Execute Definitive
	 */
	public List<CorporativeOperation> getListCorporativeEventsExecuteDefinitive(List<CorporativeOperation> lstCorporativeEventsExecuteDefinitive,List<Long> lstErrorProcess){
		List<CorporativeOperation> lstCorporativeOperation = new ArrayList<CorporativeOperation>();
		for(CorporativeOperation objCorporativeOperation : lstCorporativeEventsExecuteDefinitive){
			if(!lstErrorProcess.contains(objCorporativeOperation.getIdCorporativeOperationPk())){
				lstCorporativeOperation.add(objCorporativeOperation);
			}
		}
		return lstCorporativeOperation;
	}
	
	/**
	 * Metodo que retorna lista de corporativos a validar.
	 *
	 * @param idSecurity id security
	 * @param lstCorporativeEventsAll List Corporative Events All
	 * @return List Corporative Events Execute Definitive
	 */
	public List<CorporativeOperation> getListCorporativeEventsExecuteDefinitive(String idSecurity,LinkedHashMap<Long, CorporativeOperation> lstCorporativeEventsAll){
		List<CorporativeOperation> lstCorporativeEventsExecuteDefinitive = new ArrayList<CorporativeOperation>();
		//recorremos lista de todos los corporativos y llenamos corporativos asociados al valor
		for (Map.Entry<Long, CorporativeOperation> objCorporativeOperation : lstCorporativeEventsAll.entrySet()){
			CorporativeOperation objCorporativeOperationAux = objCorporativeOperation.getValue();
			if(objCorporativeOperationAux.getSecurities().getIdSecurityCodePk().equals(idSecurity)){
				lstCorporativeEventsExecuteDefinitive.add(objCorporativeOperationAux);
			}
		}
		return lstCorporativeEventsExecuteDefinitive;
	}
	
	/**
	 * Metodo que valida envio de definitivos de los procesos corporativos.
	 *
	 * @return Validate Block definitive
	 */
	public boolean validateSecurityProcessDefinitive()
	{
		if(flagSecurityProcessDefinitive.containsValue(Boolean.FALSE)){
			return false;
		}	
		return true;
	}
	
	/**
	 * Metodo que monitorea envios de procesos corporativos.
	 *
	 * @param lstSecurity List Security
	 */
	public void monitoringDefinitive(LinkedHashMap<String, String> lstSecurity){
		log.error("Tamanio lista de valores: "+lstSecurity.size()+"\n");
		log.error("Tamanio flag definitivos Envidados: "+flagSecurityProcessDefinitive.size()+"\n");
		for (Map.Entry<String, String> objProcessCorporate : lstSecurity.entrySet())
		{
			String idSecurity = objProcessCorporate.getKey();
			log.error("flag definitivos Envidado:"+(Boolean)flagSecurityProcessDefinitive.get(idSecurity)+"\n");
		}
	}
	
	/**
	 * Metodo que crea la lista de valores que se ejecutara los definitivos de sus procesos corporativos .
	 *
	 * @throws ServiceException The Service Exception
	 */
	public void loadListSecurityExecution()throws ServiceException{
		
		//obtenemos fecha de registro (Fecha actual parametizable )
		dateProcessCorporate = currentDate;
		
		//obtenemos lista de los beneficios a procesar
		List<CorporativeOperation> lstObjCorporative= loadProcessCorporativeBenefit(dateProcessCorporate);
		
		//validamos lista de beneficios a procesar
		if(lstObjCorporative!=null){
			for(CorporativeOperation objCorp : lstObjCorporative){
				//se llena lista de valores a procesar
				if(!lstSecurity.containsKey(objCorp.getSecurities().getIdSecurityCodePk())){
					lstSecurity.put(objCorp.getSecurities().getIdSecurityCodePk(),objCorp.getSecurities().getIdSecurityCodePk());
				}					
			}
		}			
		
		//reordena los procesos corporativos por valor que tenga mas movimientos(eliminando los repetidos)
		if (Validations.validateIsNotNull(lstSecurity) && !lstSecurity.isEmpty())
		{					
			log.error("<<<<<<<<<<<<<<<<< PRIMERA FOTO DE TOTAL DE HECHOS DE IMPORTANCIA AL EJECUTAR PROCESO EL DEMONIO DEFINITIVO: "+
					+lstCorporativeProcessAll.size()+"  >>>>>>>>>>>>>>>>>>>>>>>>>"+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			lstSecurity=orderSecurityForMovements(lstSecurity);
			log.error("<<<<<<<<<<<<<<<<< PRIMERA FOTO DE TOTAL DE VALORES AL EJECUTAR DEFINITIVOS EL DEMONIO: "+lstSecurity.size()+
					"  >>>>>>>>>>>>>>>>>>>>>>>>>"+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
		}
	}
	
	/**
	 * Metodo que ordena lista de valores por cantidad de movimientos.
	 *
	 * @param lstSecurity List security
	 * @return List order Security
	 * @throws ServiceException the base exception
	 */
	public LinkedHashMap orderSecurityForMovements(LinkedHashMap<String, String> lstSecurity)throws ServiceException
	{	
		LinkedHashMap<String,Long> quantityMovementsForSecurity = new LinkedHashMap<String,Long>();	
		LinkedHashMap lstSecurityAux=null;
		try
		{		
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<01>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>		
			
			//recorremos la lista de valores y obtenemos la cantidad de movimientos				
			log.error("<<<<<<<<<<<<<<<<< INICIO VALORES - DEFINITIVO: >>>>>>>>>>>>>>>>>>>>>>>>>"
					+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			for (Map.Entry<String, String> objSecurity : lstSecurity.entrySet())
			{				
				//obtenemos codigo del valor
				String idSecurity = objSecurity.getKey();		
				log.error("<<<<<<<<<<<<<<<<< VALOR: "+ idSecurity + "  >>>>>>>>>>>>>>>>>>>>>>>>>\n");		
				//llenamos cantidad de movimientos de cada valor
				quantityMovementsForSecurity.put(idSecurity, corporateServiceFacade.getQuantityMovementsForSecurity(idSecurity));	
				//llena lista todas los valores no tienen definitivo en proceso	
				flagSecurityProcessDefinitive.put(idSecurity, Boolean.FALSE);							
			}
			log.error("<<<<<<<<<<<<<<<<< FIN VALORES - DEFINITIVO: >>>>>>>>>>>>>>>>>>>>>>>>>"
					+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<02>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//reordena (mayor a menor-burbuja) los procesos corporativos de acuerdo a la cantidad de movimientos del valor 	
			String auxIdSecurity=null;
			Long movimentos=null;
			for (Map.Entry<String, Long> objQuantityMovements : quantityMovementsForSecurity.entrySet())
			{
				String id = objQuantityMovements.getKey();
				for (Map.Entry<String, Long> objQuantityMovementsAux : quantityMovementsForSecurity.entrySet())
				{				
					String idAux = objQuantityMovementsAux.getKey();
					if(!id.equals(idAux)){
						if((Long)quantityMovementsForSecurity.get(id)<(Long)quantityMovementsForSecurity.get(idAux))
						{
							//se cambia los valores de la cantidad de los movimientos
							movimentos=(Long)quantityMovementsForSecurity.get(id);
							quantityMovementsForSecurity.put(id, (Long)quantityMovementsForSecurity.get(idAux));
							quantityMovementsForSecurity.put(idAux, movimentos);
							
							//se cambia los objetos de hechos de importancia por la cantidad de movimientos
							auxIdSecurity=(String)lstSecurity.get(id);
							lstSecurity.put(id, (String)lstSecurity.get(idAux));
							lstSecurity.put(idAux, auxIdSecurity);
						}
					}					
				}
			}			
			lstSecurityAux=lstSecurity;
		}catch (ServiceException e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
		return lstSecurityAux;
	}	
	
	/**
	 * Metodo que crea objetos de ayuda para procesar el definitivo automatico.
	 *
	 * @throws ServiceException the service exception
	 */
	public void createObject()throws ServiceException{
		try{
			//solo una vez al dia para crear objetos una vez al dia
			if(flagCreateArrayEndDefinitive.equals(Boolean.FALSE))
			{
				lstSecurity = new LinkedHashMap();//se crea lista valores que se ejecutara definitivos (solo una vez en el dia)	
				flagSecurityProcessDefinitive = new LinkedHashMap();//se crea lista estado de definitivos en proceso (enviados)
				quantityBlockStock=0;//se inicializa en cero cantidad de bloques de stock (solo una vez al dia)				
				lstTimeDefinitiveProcess = new LinkedHashMap();//se crea lista de tiempo de inicio de proceso enviado de definitivo
				lstDefinitiveNotification = new LinkedHashMap();//se crea lista de notificacion de definitivos finalizadas (Correctas - Fallidas)
				flagEndDefinitive= new LinkedHashMap();//se crea lista estado de preliminares terminados
				lstCorporativeProcessAll=loadProcessCoporativeAll();//se crea lista de todos los hechos de importancia como ayuda para verificar la ejecucion de los definitivos y envio de notificaciones
				flagCreateArrayEndDefinitive=Boolean.TRUE;
			}	
		}catch (ServiceException e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Metodo que retorna lista de todos procesos corporativos de beneficios.
	 *
	 * @return List Process Coporative All
	 * @throws ServiceException the service exception
	 */ 
	public LinkedHashMap loadProcessCoporativeAll()throws ServiceException
	{
		//se crea lista de hechos de importancia (solo una vez en el dia)
		LinkedHashMap lstProcessCoporativeAll = null;
		Date dateProcessCorporate=null;		
		try
		{
			lstProcessCoporativeAll = new LinkedHashMap();			
			//obtenemos fecha de entrega (Fecha actual parametizable )
			dateProcessCorporate = currentDate;
			
			//obtenemos lista de los beneficios a procesar
			List<CorporativeOperation> lstObjCorporative = loadProcessCorporativeBenefit(dateProcessCorporate);
			
			//validamos lista de beneficios a procesar
			if(lstObjCorporative!=null){				
				for(CorporativeOperation objCorp : lstObjCorporative){
					//se llena lista Map de todos los hechos de importancia a procesar
					lstProcessCoporativeAll.put(objCorp.getIdCorporativeOperationPk(), objCorp);	
					//se llena lista estado de preliminares terminados
					flagEndDefinitive.put(objCorp.getIdCorporativeOperationPk(), Boolean.FALSE);
				}
			}			
		}catch (ServiceException e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
		
		return lstProcessCoporativeAll;
	}
	
	/**
	 * Metodo que retorna una lista de procesos corporativos de beneficios en estado preliminar.
	 *
	 * @param dateProcessCorporate Date Process Corporate
	 * @return List Process Corporative Benefit
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> loadProcessCorporativeBenefit(Date dateProcessCorporate)throws ServiceException{
		List<CorporativeOperation> lstObjCorporative = null;
		CorporateMonitoringTO corporateMonitoringFilter;
		List<Integer> stateProcessCorporative = new ArrayList<Integer>();
		List<Integer> eventProcessCorporative = new ArrayList<Integer>();
		//se verifica si hay hechos de importancia de renta fija por ejecutarse y en estado registrado
		corporateMonitoringFilter = new CorporateMonitoringTO();
//		corporateMonitoringFilter.setProcessDate(dateProcessCorporate);
		corporateMonitoringFilter.setListExpirationDate(Arrays.asList(dateProcessCorporate));
		stateProcessCorporative.add(CorporateProcessStateType.PRELIMINARY.getCode());
		stateProcessCorporative.add(CorporateProcessStateType.DEFINITIVE_FAIL.getCode());
		corporateMonitoringFilter.setLstState(stateProcessCorporative);
		eventProcessCorporative.add(ImportanceEventType.ACTION_DIVIDENDS.getCode());
		eventProcessCorporative.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		eventProcessCorporative.add(ImportanceEventType.CAPITAL_REDUCTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
		eventProcessCorporative.add(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		eventProcessCorporative.add(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.REMANENT_PAYMENT.getCode());
		eventProcessCorporative.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
		corporateMonitoringFilter.setLstCorporativeEvent(eventProcessCorporative);
		try {
			lstObjCorporative= corporateServiceFacade.searchCorporativeOperationsByCoupons(corporateMonitoringFilter);
		} catch (ServiceException e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
		return lstObjCorporative;
	}
	
	/**
	 * Get Limit Block.
	 *
	 * @return Limit Block
	 */
	public Integer getLimitBlock() {
		return limitBlock;
	}
	
	/**
	 * Set Limit Block.
	 *
	 * @param limitBlock Limit Block
	 */
	public void setLimitBlock(Integer limitBlock) {
		this.limitBlock = limitBlock;
	}
	
	/**
	 * Get Wait Time.
	 *
	 * @return Wait Time
	 */
	public Integer getWaitTime() {
		return waitTime;
	}
	
	/**
	 * Set Wait Time.
	 *
	 * @param waitTime Wait Time
	 */
	public void setWaitTime(Integer waitTime) {
		this.waitTime = waitTime;
	}
	
	/**
	 * Get Wait Time Process.
	 *
	 * @return Wait Time Process
	 */
	public Integer getWaitTimeProcess() {
		return waitTimeProcess;
	}
	
	/**
	 * Set Wait Time Process.
	 *
	 * @param waitTimeProcess Wait Time Process
	 */
	public void setWaitTimeProcess(Integer waitTimeProcess) {
		this.waitTimeProcess = waitTimeProcess;
	}
	
	/**
	 * Get User Id.
	 *
	 * @return User Id
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Set User Id.
	 *
	 * @param userId User Id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Get  Current Date.
	 *
	 * @return Current Date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}
	
	/**
	 * Set Current Date.
	 *
	 * @param currentDate Current Date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}	
}
