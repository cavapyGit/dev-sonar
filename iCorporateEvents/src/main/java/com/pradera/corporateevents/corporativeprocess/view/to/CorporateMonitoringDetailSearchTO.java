package com.pradera.corporateevents.corporativeprocess.view.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.corporatives.stockcalculation.StockCalculationBalance;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class CorporateMonitoringDetailSearchFilter
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCorporateEvents
 * @Creation_Date :
 */
public class CorporateMonitoringDetailSearchTO implements Serializable {

	/** Added default serial version uid. */
	private static final long serialVersionUID = 1L;
	
	/** Holds the Corporate Process Result data. */
	private CorporativeProcessResult corporateDetail;	
	
	/** Holds the stock total balance. */
	private BigDecimal stotalBalance;
	
	/** Holds the stock available balance. */
	private BigDecimal savailableBalance;

	/** Holds new total amount. */
	private BigDecimal newBenefitAmount;
	
	/** Holds new tax amount. */
	private BigDecimal newTax;
	
	/** Holds new custody amount. */
	private BigDecimal newCommissiones;
	
	/** Holds the Holder Account Adjustment details. */
	private HolderAccountAdjusment holderAccountAdjusment;
	
	/** Holds the Stock balance details. */
	private StockCalculationBalance stockCalBalance;
	
	/** Holds the Corporate Operation detail. */
	private CorporativeOperation corporateOper;
	
	/**
	 * Gets the corporate detail.
	 *
	 * @return the corporateDetail
	 */
	public CorporativeProcessResult getCorporateDetail() {
		return corporateDetail;
	}
	
	/**
	 * Sets the corporate detail.
	 *
	 * @param corporateDetail the corporateDetail to set
	 */
	public void setCorporateDetail(CorporativeProcessResult corporateDetail) {
		this.corporateDetail = corporateDetail;
	}

	/**
	 * Gets the stotal balance.
	 *
	 * @return the stotalBalance
	 */
	public BigDecimal getStotalBalance() {
		return stotalBalance;
	}
	
	/**
	 * Sets the stotal balance.
	 *
	 * @param stotalBalance the stotalBalance to set
	 */
	public void setStotalBalance(BigDecimal stotalBalance) {
		this.stotalBalance = stotalBalance;
	}
	
	/**
	 * Gets the savailable balance.
	 *
	 * @return the savailableBalance
	 */
	public BigDecimal getSavailableBalance() {
		return savailableBalance;
	}
	
	/**
	 * Sets the savailable balance.
	 *
	 * @param savailableBalance the savailableBalance to set
	 */
	public void setSavailableBalance(BigDecimal savailableBalance) {
		this.savailableBalance = savailableBalance;
	}

	/**
	 * Gets the new benefit amount.
	 *
	 * @return the newBenefitAmount
	 */
	public BigDecimal getNewBenefitAmount() {
		return newBenefitAmount;
	}
	
	/**
	 * Sets the new benefit amount.
	 *
	 * @param newBenefitAmount the newBenefitAmount to set
	 */
	public void setNewBenefitAmount(BigDecimal newBenefitAmount) {
		this.newBenefitAmount = newBenefitAmount;
	}
	
	/**
	 * Gets the new tax.
	 *
	 * @return the newTax
	 */
	public BigDecimal getNewTax() {
		return newTax;
	}
	
	/**
	 * Sets the new tax.
	 *
	 * @param newTax the newTax to set
	 */
	public void setNewTax(BigDecimal newTax) {
		this.newTax = newTax;
	}
	
	/**
	 * Gets the new commissiones.
	 *
	 * @return the newCommissiones
	 */
	public BigDecimal getNewCommissiones() {
		return newCommissiones;
	}
	
	/**
	 * Sets the new commissiones.
	 *
	 * @param newCommissiones the newCommissiones to set
	 */
	public void setNewCommissiones(BigDecimal newCommissiones) {
		this.newCommissiones = newCommissiones;
	}

	/**
	 * Gets the holder account adjusment.
	 *
	 * @return the holderAccountAdjusment
	 */
	public HolderAccountAdjusment getHolderAccountAdjusment() {
		return holderAccountAdjusment;
	}
	
	/**
	 * Sets the holder account adjusment.
	 *
	 * @param holderAccountAdjusment the holderAccountAdjusment to set
	 */
	public void setHolderAccountAdjusment(
			HolderAccountAdjusment holderAccountAdjusment) {
		this.holderAccountAdjusment = holderAccountAdjusment;
	}
	
	/**
	 * Gets the stock cal balance.
	 *
	 * @return the stockCalBalance
	 */
	public StockCalculationBalance getStockCalBalance() {
		return stockCalBalance;
	}
	
	/**
	 * Sets the stock cal balance.
	 *
	 * @param stockCalBalance the stockCalBalance to set
	 */
	public void setStockCalBalance(StockCalculationBalance stockCalBalance) {
		this.stockCalBalance = stockCalBalance;
	}
	
	/**
	 * Gets the corporate oper.
	 *
	 * @return the corporateOper
	 */
	public CorporativeOperation getCorporateOper() {
		return corporateOper;
	}
	
	/**
	 * Sets the corporate oper.
	 *
	 * @param corporateOper the corporateOper to set
	 */
	public void setCorporateOper(CorporativeOperation corporateOper) {
		this.corporateOper = corporateOper;
	}
	
	
	
}
