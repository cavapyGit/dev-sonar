package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.generalparameter.DailyExchangeRates;


// TODO: Auto-generated Javadoc
/**
 * The Interface ChangesFixer.
 *
 * @author : Pradera
 */
public interface ChangesFixer {
	
	/**
	 * Gets the updated result.
	 *
	 * @param results the results
	 * @param currentRes the current res
	 * @param loggerUser the logger user
	 * @param mpParticipants the mp participants
	 * @param mpChangeOwnerShip the mp change owner ship
	 * @param documentFixer the document fixer
	 * @param balanceFixer the balance fixer
	 * @param round the round
	 * @param cashFlag the cash flag
	 * @param taxFactor the tax factor
	 * @return the updated result
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeProcessResult> getUpdatedResult(List<CorporativeProcessResult> results, CorporativeProcessResult currentRes,
			LoggerUser loggerUser, Map<Long, Object[]> mpParticipants, Map<Long, List<Object[]>> mpChangeOwnerShip, 
			DocumentsFixer documentFixer, BalanceFixer balanceFixer, int round, boolean cashFlag, BigDecimal taxFactor) throws ServiceException;
	
}
