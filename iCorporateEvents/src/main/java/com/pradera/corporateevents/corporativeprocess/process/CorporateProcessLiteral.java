package com.pradera.corporateevents.corporativeprocess.process;

import javax.enterprise.util.AnnotationLiteral;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CorporateProcessLiteral.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/06/2014
 */
public class CorporateProcessLiteral extends AnnotationLiteral<CorporateProcess> implements CorporateProcess{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9042006741313855735L;
	
	/** The process. */
	private CorporateExecutor process;

	/**
	 * Instantiates a new corporate process literal.
	 */
	public CorporateProcessLiteral() {
	}
	
	/**
	 * Instantiates a new corporate process literal.
	 *
	 * @param process the process
	 */
	public CorporateProcessLiteral(CorporateExecutor process) {
		this.process = process;
	}

	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.CorporateProcess#value()
	 */
	@Override
	public CorporateExecutor value() {
		// TODO Auto-generated method stub
		return this.process;
	}

}
