package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;



// TODO: Auto-generated Javadoc
/**
 * The Class AllocateIssuerCashAccountTO.
 * @author Pradera software
 *
 */
public class AllocateIssuerCashAccountTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id cash account. */
	private String idCashAccount;
	
	/** The issuer. */
	private String issuer;
	
	/** The total amount. */
	private BigDecimal totalAmount;
	
	/** The available amount. */
	private BigDecimal availableAmount;
	
	/** The allocate amount. */
	private BigDecimal allocateAmount;


	/**
	 * Gets the id cash account.
	 *
	 * @return the id cash account
	 */
	public String getIdCashAccount() {
		return idCashAccount;
	}
	
	/**
	 * Sets the id cash account.
	 *
	 * @param idCashAccount the new id cash account
	 */
	public void setIdCashAccount(String idCashAccount) {
		this.idCashAccount = idCashAccount;
	}
	
	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	
	/**
	 * Sets the total amount.
	 *
	 * @param totalAmount the new total amount
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	/**
	 * Gets the available amount.
	 *
	 * @return the available amount
	 */
	public BigDecimal getAvailableAmount() {
		return availableAmount;
	}
	
	/**
	 * Sets the available amount.
	 *
	 * @param availableAmount the new available amount
	 */
	public void setAvailableAmount(BigDecimal availableAmount) {
		this.availableAmount = availableAmount;
	}
	
	/**
	 * Gets the allocate amount.
	 *
	 * @return the allocate amount
	 */
	public BigDecimal getAllocateAmount() {
		return allocateAmount;
	}
	
	/**
	 * Sets the allocate amount.
	 *
	 * @param allocateAmount the new allocate amount
	 */
	public void setAllocateAmount(BigDecimal allocateAmount) {
		this.allocateAmount = allocateAmount;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
}