package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * The Class AllocationProcessIssuerResumeTO.
 * @author Pradera Technologies
 *
 */
public class AllocationProcessIssuerResumeTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The issuer. */
	private String issuer;
	
	/** The issuer mnemonic. */
	private String issuerMnemonic;
	
	/** The issuer description. */
	private String issuerDescription;
	
	/** The cash account amount. */
	private BigDecimal cashAccountAmount;
	
	/** The allocate process quantity. */
	private BigDecimal allocateProcessQuantity;
	
	/** The allocated process quantity. */
	private BigDecimal allocatedProcessQuantity;
	
	/** The allocate process amount. */
	private BigDecimal allocateProcessAmount;
	
	/** The allocated process amount. */
	private BigDecimal allocatedProcessAmount;

	private Integer haveInstitutionCashAccount;

	private Integer paymentType;

	private String haveInstitutionCashAccountDesc;

	private String paymentTypeDesc;

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the issuer description.
	 *
	 * @return the issuer description
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}
	
	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the new issuer description
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}
	
	/**
	 * Gets the cash account amount.
	 *
	 * @return the cash account amount
	 */
	public BigDecimal getCashAccountAmount() {
		return cashAccountAmount;
	}
	
	/**
	 * Sets the cash account amount.
	 *
	 * @param cashAccountAmount the new cash account amount
	 */
	public void setCashAccountAmount(BigDecimal cashAccountAmount) {
		this.cashAccountAmount = cashAccountAmount;
	}
	
	/**
	 * Gets the allocate process quantity.
	 *
	 * @return the allocate process quantity
	 */
	public BigDecimal getAllocateProcessQuantity() {
		return allocateProcessQuantity;
	}
	
	/**
	 * Sets the allocate process quantity.
	 *
	 * @param allocateProcessQuantity the new allocate process quantity
	 */
	public void setAllocateProcessQuantity(BigDecimal allocateProcessQuantity) {
		this.allocateProcessQuantity = allocateProcessQuantity;
	}
	
	/**
	 * Gets the allocated process quantity.
	 *
	 * @return the allocated process quantity
	 */
	public BigDecimal getAllocatedProcessQuantity() {
		return allocatedProcessQuantity;
	}
	
	/**
	 * Sets the allocated process quantity.
	 *
	 * @param allocatedProcessQuantity the new allocated process quantity
	 */
	public void setAllocatedProcessQuantity(BigDecimal allocatedProcessQuantity) {
		this.allocatedProcessQuantity = allocatedProcessQuantity;
	}
	
	/**
	 * Gets the allocate process amount.
	 *
	 * @return the allocate process amount
	 */
	public BigDecimal getAllocateProcessAmount() {
		return allocateProcessAmount;
	}
	
	/**
	 * Sets the allocate process amount.
	 *
	 * @param allocateProcessAmount the new allocate process amount
	 */
	public void setAllocateProcessAmount(BigDecimal allocateProcessAmount) {
		this.allocateProcessAmount = allocateProcessAmount;
	}
	
	/**
	 * Gets the allocated process amount.
	 *
	 * @return the allocated process amount
	 */
	public BigDecimal getAllocatedProcessAmount() {
		return allocatedProcessAmount;
	}
	
	/**
	 * Sets the allocated process amount.
	 *
	 * @param allocatedProcessAmount the new allocated process amount
	 */
	public void setAllocatedProcessAmount(BigDecimal allocatedProcessAmount) {
		this.allocatedProcessAmount = allocatedProcessAmount;
	}
	
	/**
	 * Gets the issuer mnemonic.
	 *
	 * @return the issuer mnemonic
	 */
	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}
	
	/**
	 * Sets the issuer mnemonic.
	 *
	 * @param issuerMnemonic the new issuer mnemonic
	 */
	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}

	public Integer getHaveInstitutionCashAccount() {
		return haveInstitutionCashAccount;
	}

	public void setHaveInstitutionCashAccount(Integer haveInstitutionCashAccount) {
		this.haveInstitutionCashAccount = haveInstitutionCashAccount;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public String getHaveInstitutionCashAccountDesc() {
		return haveInstitutionCashAccountDesc;
	}

	public void setHaveInstitutionCashAccountDesc(String haveInstitutionCashAccountDesc) {
		this.haveInstitutionCashAccountDesc = haveInstitutionCashAccountDesc;
	}

	public String getPaymentTypeDesc() {
		return paymentTypeDesc;
	}

	public void setPaymentTypeDesc(String paymentTypeDesc) {
		this.paymentTypeDesc = paymentTypeDesc;
	}
	
}