package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



// TODO: Auto-generated Javadoc
/**
 * The Class HolderAccountMovementsCustodyCommission.
 * @author Pradera Technologies
 *
 */
public class HolderAccountMovementsCustodyCommission implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder account. */
	private Long holderAccount;

	/** The original primary balance. */
	private BigDecimal originalPrimaryBalance;
	
	/** The original secondary balance. */
	private BigDecimal originalSecondaryBalance;

	/** The movement date. */
	private Date movementDate;

	/** The primary balance. */
	private BigDecimal primaryBalance;
	
	/** The secondary balance. */
	private BigDecimal secondaryBalance;
	
	
	
	
	/**
	 * Instantiates a new holder account movements custody commission.
	 */
	public HolderAccountMovementsCustodyCommission() {
		
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the original primary balance.
	 *
	 * @return the original primary balance
	 */
	public BigDecimal getOriginalPrimaryBalance() {
		return originalPrimaryBalance;
	}
	
	/**
	 * Sets the original primary balance.
	 *
	 * @param originalPrimaryBalance the new original primary balance
	 */
	public void setOriginalPrimaryBalance(BigDecimal originalPrimaryBalance) {
		this.originalPrimaryBalance = originalPrimaryBalance;
	}
	
	/**
	 * Gets the original secondary balance.
	 *
	 * @return the original secondary balance
	 */
	public BigDecimal getOriginalSecondaryBalance() {
		return originalSecondaryBalance;
	}
	
	/**
	 * Sets the original secondary balance.
	 *
	 * @param originalSecondaryBalance the new original secondary balance
	 */
	public void setOriginalSecondaryBalance(BigDecimal originalSecondaryBalance) {
		this.originalSecondaryBalance = originalSecondaryBalance;
	}
	
	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}
	
	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	
	/**
	 * Gets the primary balance.
	 *
	 * @return the primary balance
	 */
	public BigDecimal getPrimaryBalance() {
		return primaryBalance;
	}
	
	/**
	 * Sets the primary balance.
	 *
	 * @param primaryBalance the new primary balance
	 */
	public void setPrimaryBalance(BigDecimal primaryBalance) {
		this.primaryBalance = primaryBalance;
	}
	
	/**
	 * Gets the secondary balance.
	 *
	 * @return the secondary balance
	 */
	public BigDecimal getSecondaryBalance() {
		return secondaryBalance;
	}
	
	/**
	 * Sets the secondary balance.
	 *
	 * @param secondaryBalance the new secondary balance
	 */
	public void setSecondaryBalance(BigDecimal secondaryBalance) {
		this.secondaryBalance = secondaryBalance;
	}


	
}