package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class AllocationPaymentProcessBankTO.
 * @author Pradera Technologies
 *
 */
public class AllocationPaymentProcessBankTO implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The tipo. */
	private String tipo;//tipo
	
	/** The fecha generacion. */
	private String fechaGeneracion;//fechaGeneracion
	
	/** The hora generacion. */
	private String horaGeneracion;//horaGeneracion
	
	/** The nombre lote. */
	private String nombreLote;//nombreLote
	
	/** The concepto pago. */
	private String conceptoPago;//conceptoPago
	
	/** The cevaldom bic code. */
	private String cevaldomBicCode;//codigoBICentidadDB
	
	/** The bank bic code. */
	private String bankBicCode;//codigoBICentidadCR
	
	/** The trn. */
	private String trn;//trnopcrLBTR
	
	/** The fecha valor. */
	private String fechaValor;//FECHAVALORCRLBTR
	
	/** The total registers. */
	private Integer totalRegisters;//totalRegistrosControl
	
	/** The withdrawl amount. */
	private BigDecimal withdrawlAmount;//totalMontosControl
	
	/** The currency str. */
	private String currencyStr;//moneda
	
	/** The informacion adicional. */
	private String informacionAdicional;//informAdicionalEnc01
	
	/** The estado archivo. */
	private String estadoArchivo;//estadoarchivoenportal
	
	/** The cantidad descargas. */
	private String cantidadDescargas;//cantdescargasarchivo
	
	/** The usuario descargo. */
	private String usuarioDescargo;//usuariodescargoarchivo
	
	/** The fund operation pk. */
	private String fundOperationPK;
	
	/** The corporative process type. */
	private Integer corporativeProcessType;
	
	/** The bank pk. */
	private Long bankPK;
	
	/** The bank account number. */
	private String bankAccountNumber;
	
	/** The currency. */
	private Integer currency;
	
	/** The issuer. */
	private String issuer;
	
	/** The id central fund operation. */
	private Long idCentralFundOperation;
	
	/** The lst holders. */
	private List<AllocationPaymentProcessHolderTO> lstHolders;


	/**
	 * Gets the tipo.
	 *
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * Sets the tipo.
	 *
	 * @param tipo the new tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * Gets the fecha generacion.
	 *
	 * @return the fecha generacion
	 */
	public String getFechaGeneracion() {
		return fechaGeneracion;
	}
	
	/**
	 * Sets the fecha generacion.
	 *
	 * @param fechaGeneracion the new fecha generacion
	 */
	public void setFechaGeneracion(String fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	
	/**
	 * Gets the hora generacion.
	 *
	 * @return the hora generacion
	 */
	public String getHoraGeneracion() {
		return horaGeneracion;
	}
	
	/**
	 * Sets the hora generacion.
	 *
	 * @param horaGeneracion the new hora generacion
	 */
	public void setHoraGeneracion(String horaGeneracion) {
		this.horaGeneracion = horaGeneracion;
	}
	
	/**
	 * Gets the nombre lote.
	 *
	 * @return the nombre lote
	 */
	public String getNombreLote() {
		return nombreLote;
	}
	
	/**
	 * Sets the nombre lote.
	 *
	 * @param nombreLote the new nombre lote
	 */
	public void setNombreLote(String nombreLote) {
		this.nombreLote = nombreLote;
	}
	
	/**
	 * Gets the concepto pago.
	 *
	 * @return the concepto pago
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}
	
	/**
	 * Sets the concepto pago.
	 *
	 * @param conceptoPago the new concepto pago
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	
	/**
	 * Gets the cevaldom bic code.
	 *
	 * @return the cevaldom bic code
	 */
	public String getCevaldomBicCode() {
		return cevaldomBicCode;
	}
	
	/**
	 * Sets the cevaldom bic code.
	 *
	 * @param cevaldomBicCode the new cevaldom bic code
	 */
	public void setCevaldomBicCode(String cevaldomBicCode) {
		this.cevaldomBicCode = cevaldomBicCode;
	}
	
	/**
	 * Gets the bank bic code.
	 *
	 * @return the bank bic code
	 */
	public String getBankBicCode() {
		return bankBicCode;
	}
	
	/**
	 * Sets the bank bic code.
	 *
	 * @param bankBicCode the new bank bic code
	 */
	public void setBankBicCode(String bankBicCode) {
		this.bankBicCode = bankBicCode;
	}
	
	/**
	 * Gets the trn.
	 *
	 * @return the trn
	 */
	public String getTrn() {
		return trn;
	}
	
	/**
	 * Sets the trn.
	 *
	 * @param trn the new trn
	 */
	public void setTrn(String trn) {
		this.trn = trn;
	}
	
	/**
	 * Gets the fecha valor.
	 *
	 * @return the fecha valor
	 */
	public String getFechaValor() {
		return fechaValor;
	}
	
	/**
	 * Sets the fecha valor.
	 *
	 * @param fechaValor the new fecha valor
	 */
	public void setFechaValor(String fechaValor) {
		this.fechaValor = fechaValor;
	}
	
	/**
	 * Gets the total registers.
	 *
	 * @return the total registers
	 */
	public Integer getTotalRegisters() {
		return totalRegisters;
	}
	
	/**
	 * Sets the total registers.
	 *
	 * @param totalRegisters the new total registers
	 */
	public void setTotalRegisters(Integer totalRegisters) {
		this.totalRegisters = totalRegisters;
	}
	
	/**
	 * Gets the withdrawl amount.
	 *
	 * @return the withdrawl amount
	 */
	public BigDecimal getWithdrawlAmount() {
		return withdrawlAmount;
	}
	
	/**
	 * Sets the withdrawl amount.
	 *
	 * @param withdrawlAmount the new withdrawl amount
	 */
	public void setWithdrawlAmount(BigDecimal withdrawlAmount) {
		this.withdrawlAmount = withdrawlAmount;
	}
	
	/**
	 * Gets the currency str.
	 *
	 * @return the currency str
	 */
	public String getCurrencyStr() {
		return currencyStr;
	}
	
	/**
	 * Sets the currency str.
	 *
	 * @param currencyStr the new currency str
	 */
	public void setCurrencyStr(String currencyStr) {
		this.currencyStr = currencyStr;
	}
	
	/**
	 * Gets the informacion adicional.
	 *
	 * @return the informacion adicional
	 */
	public String getInformacionAdicional() {
		return informacionAdicional;
	}
	
	/**
	 * Sets the informacion adicional.
	 *
	 * @param informacionAdicional the new informacion adicional
	 */
	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}
	
	/**
	 * Gets the estado archivo.
	 *
	 * @return the estado archivo
	 */
	public String getEstadoArchivo() {
		return estadoArchivo;
	}
	
	/**
	 * Sets the estado archivo.
	 *
	 * @param estadoArchivo the new estado archivo
	 */
	public void setEstadoArchivo(String estadoArchivo) {
		this.estadoArchivo = estadoArchivo;
	}
	
	/**
	 * Gets the cantidad descargas.
	 *
	 * @return the cantidad descargas
	 */
	public String getCantidadDescargas() {
		return cantidadDescargas;
	}
	
	/**
	 * Sets the cantidad descargas.
	 *
	 * @param cantidadDescargas the new cantidad descargas
	 */
	public void setCantidadDescargas(String cantidadDescargas) {
		this.cantidadDescargas = cantidadDescargas;
	}
	
	/**
	 * Gets the usuario descargo.
	 *
	 * @return the usuario descargo
	 */
	public String getUsuarioDescargo() {
		return usuarioDescargo;
	}
	
	/**
	 * Sets the usuario descargo.
	 *
	 * @param usuarioDescargo the new usuario descargo
	 */
	public void setUsuarioDescargo(String usuarioDescargo) {
		this.usuarioDescargo = usuarioDescargo;
	}
	
	/**
	 * Gets the corporative process type.
	 *
	 * @return the corporative process type
	 */
	public Integer getCorporativeProcessType() {
		return corporativeProcessType;
	}
	
	/**
	 * Sets the corporative process type.
	 *
	 * @param corporativeProcessType the new corporative process type
	 */
	public void setCorporativeProcessType(Integer corporativeProcessType) {
		this.corporativeProcessType = corporativeProcessType;
	}
	
	/**
	 * Gets the bank pk.
	 *
	 * @return the bank pk
	 */
	public Long getBankPK() {
		return bankPK;
	}
	
	/**
	 * Sets the bank pk.
	 *
	 * @param bankPK the new bank pk
	 */
	public void setBankPK(Long bankPK) {
		this.bankPK = bankPK;
	}
	
	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	
	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the lst holders.
	 *
	 * @return the lst holders
	 */
	public List<AllocationPaymentProcessHolderTO> getLstHolders() {
		return lstHolders;
	}
	
	/**
	 * Sets the lst holders.
	 *
	 * @param lstHolders the new lst holders
	 */
	public void setLstHolders(List<AllocationPaymentProcessHolderTO> lstHolders) {
		this.lstHolders = lstHolders;
	}
	
	/**
	 * Gets the fund operation pk.
	 *
	 * @return the fund operation pk
	 */
	public String getFundOperationPK() {
		return fundOperationPK;
	}
	
	/**
	 * Sets the fund operation pk.
	 *
	 * @param fundOperationPK the new fund operation pk
	 */
	public void setFundOperationPK(String fundOperationPK) {
		this.fundOperationPK = fundOperationPK;
	}
	
	/**
	 * Gets the id central fund operation.
	 *
	 * @return the id central fund operation
	 */
	public Long getIdCentralFundOperation() {
		return idCentralFundOperation;
	}
	
	/**
	 * Sets the id central fund operation.
	 *
	 * @param idCentralFundOperation the new id central fund operation
	 */
	public void setIdCentralFundOperation(Long idCentralFundOperation) {
		this.idCentralFundOperation = idCentralFundOperation;
	}
}