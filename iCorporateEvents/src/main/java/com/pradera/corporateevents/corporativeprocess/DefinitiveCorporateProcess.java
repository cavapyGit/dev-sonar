package com.pradera.corporateevents.corporativeprocess;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.reports.CorporateProcessReportsSender;

/**
 * Batch process for definitive corporate process
 * @author PraderaTechnologies
 *
 */
@ApplicationScoped
public class DefinitiveCorporateProcess{

	
	
	
	
	
	@Inject
	@Any
	Instance<CorporateProcessExecutor> corpProcessExcutor;
	
	@Inject
	CorporateProcessReportsSender reportSender;
	
	
	
	/**
	 * Used to start the batch process execution
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	public void processDefinitive(String userId, Long lngProcessId) {		
		
	}
	

}
