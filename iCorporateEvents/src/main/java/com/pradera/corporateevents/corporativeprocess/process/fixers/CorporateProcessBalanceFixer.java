package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.SecuritiesCorporative;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporateProcessBalanceFixer.
 *
 * @author PraderaTechnologies
 */
@ApplicationScoped
public class CorporateProcessBalanceFixer implements BalanceFixer, Serializable {
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;

		
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Used to fix the balances in the CorporativeProcessResults.
	 *
	 * @param holderAccounts <code>List </code>
	 * @param objCorporativeOperation the obj corporative operation
	 * @param delFactor <code>BigDecimal </code>
	 * @param round <code>int </code>
	 * @param cashFlag <code>boolean </code>
	 * @param remnant <code>boolean </code>
	 * @param destiny <code>boolean </code>
	 * @param processType the process type
	 * @param exchangeRateDay the exchange rate day
	 * @return List : List of CorporativeProcessResults
	 */
	public List<CorporativeProcessResult> fixBalances(List<Object[]> holderAccounts,CorporativeOperation objCorporativeOperation, 
			BigDecimal delFactor,int round, boolean cashFlag, boolean remnant, boolean destiny, ImportanceEventType processType,DailyExchangeRates exchangeRateDay) {
		List<CorporativeProcessResult> processResults = new ArrayList<CorporativeProcessResult>();

		BigDecimal tmpDelFactor = null;

		for (Iterator<Object[]> it = holderAccounts.iterator(); it.hasNext();) {
			Object[] data = it.next();
			tmpDelFactor = delFactor;
			CorporativeProcessResult processResult = new CorporativeProcessResult();
			HolderAccount holAccount = new HolderAccount();
			holAccount.setIdHolderAccountPk((Long) data[0]);
			processResult.setHolderAccount(holAccount);
			processResult.setSecurity(objCorporativeOperation.getSecurities());
			processResult.setParticipant(new Participant((Long)data[11]));
			
			processResult.setAvailableBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[1], cashFlag), tmpDelFactor,  round));
			processResult.setPawnBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[2], cashFlag), tmpDelFactor,  round));
			processResult.setBanBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[3], cashFlag), tmpDelFactor,  round));
			processResult.setOtherBlockBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[4], cashFlag), tmpDelFactor,  round));
			processResult.setReserveBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[5], cashFlag), tmpDelFactor,  round));
			processResult.setOppositionBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[6], cashFlag), tmpDelFactor,  round));
			processResult.setMarginBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[7], cashFlag), tmpDelFactor,  round));
			processResult.setReportingBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[8], cashFlag), tmpDelFactor,  round));
			processResult.setReportedBalance(CorporateProcessUtils.calculateByFactor(getBalance((BigDecimal)data[9], cashFlag), tmpDelFactor,  round));
			processResult.setAcreditationBalance(BigDecimal.ZERO);
			processResult.setBorrowerBalance(BigDecimal.ZERO);
			processResult.setLenderBalance(BigDecimal.ZERO);
			processResult.setLoanableBalance(BigDecimal.ZERO);
			processResult.setSellBalance(BigDecimal.ZERO);
			processResult.setTransitBalance(BigDecimal.ZERO);
			processResult.setBuyBalance(BigDecimal.ZERO);
			processResult.setIndAdjustment(CorporateProcessConstants.IND_NO);
			processResult.setCorporativeOperation(objCorporativeOperation);
			processResult.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			
			if(remnant){
				processResult.setIndRemanent(CorporateProcessConstants.IND_REMAINDER);
			}else{
				processResult.setIndRemanent(CorporateProcessConstants.IND_NO_REMAINDER);
			}
			
			BigDecimal totalAmount = processResult.getAvailableBalance().add(processResult.getBanBalance()).add(
					processResult.getOtherBlockBalance()).add(processResult.getPawnBalance()).add(
							processResult.getReportingBalance()).add(processResult.getMarginBalance()).add(
							processResult.getReserveBalance()).add(processResult.getOppositionBalance());
			processResult.setTotalBalance(totalAmount);
			
			if(objCorporativeOperation.getCurrency()!=null &&
					(objCorporativeOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					objCorporativeOperation.getCurrency().equals(CurrencyType.DMV.getCode()))){
				processResult.setIndOriginTarget(CorporateProcessConstants.EXCHANGE_DESTINY);
				processResult.setExchangeAmount(processResult.getTotalBalance());
			}else if(destiny){
				processResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_DESTINY);
			}else{
				processResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_ORIGIN);
			}
			
			if(ImportanceEventType.INTEREST_PAYMENT.equals(processType)){
				processResult.setCustodyAmount((BigDecimal)data[12]);
			}

			processResults.add(processResult);
			
			if(objCorporativeOperation.getCurrency()!=null &&
					(objCorporativeOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					objCorporativeOperation.getCurrency().equals(CurrencyType.DMV.getCode()))){
				CorporativeProcessResult corporativeProcessResultExchange = new CorporativeProcessResult(processResult);
				corporativeProcessResultExchange.setTotalBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getTotalBalance(), exchangeRateDay.getBuyPrice(), round));
				corporativeProcessResultExchange.setAvailableBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getAvailableBalance(), exchangeRateDay.getBuyPrice(), round));
				corporativeProcessResultExchange.setPawnBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getPawnBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setBanBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getBanBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setOtherBlockBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getOtherBlockBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setReserveBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getReserveBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setOppositionBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getOppositionBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setMarginBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getMarginBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setReportingBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getReportingBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setReportedBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getReportedBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setIndOriginTarget(CorporateProcessConstants.BALANCE_DESTINY);
				corporativeProcessResultExchange.setExchangeAmount(BigDecimal.ZERO);
				processResults.add(corporativeProcessResultExchange);
			}
		}
		return processResults;
	}
	
	/**
	 * Used to return a big decimal value that is came from multiply the balance with 100 if balance is not null.
	 *
	 * @param balance <code>BigDecimal </code>
	 * @param cashFlag <code>boolean </code>
	 * @return <code>BigDecimal </code>
	 */
	public BigDecimal getBalance(BigDecimal balance, boolean cashFlag){		
		return balance != null ? cashFlag ? CorporateProcessUtils.multiply(
				balance, CorporateProcessConstants.AMOUNT_ROUND): balance : BigDecimal.ZERO;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer#fillProcessResultsForOrigin(java.util.List, com.pradera.model.corporatives.CorporativeOperation)
	 */
	public List<CorporativeProcessResult> fillProcessResultsForOrigin(List<Object[]> holderAccounts, CorporativeOperation corpOper){
		List<CorporativeProcessResult> resultOrigin = new ArrayList<CorporativeProcessResult>();
		for (Iterator<Object[]> it = holderAccounts.iterator(); it.hasNext();) {
			CorporativeProcessResult processResult = new CorporativeProcessResult();
			Object[] data = it.next();
			HolderAccount holAccount = new HolderAccount();
			holAccount.setIdHolderAccountPk((Long) data[0]);
			processResult.setHolderAccount(holAccount);
			processResult.setSecurity(corpOper.getSecurities());
			processResult.setParticipant(new Participant((Long)data[11]));
			processResult.setAvailableBalance((BigDecimal)data[1]);
			processResult.setPawnBalance((BigDecimal)data[2]);
			processResult.setBanBalance((BigDecimal)data[3]);
			processResult.setOtherBlockBalance((BigDecimal)data[4]);
			processResult.setReserveBalance((BigDecimal)data[5]);
			processResult.setOppositionBalance((BigDecimal)data[6]);			
			processResult.setMarginBalance((BigDecimal)data[7]);
			processResult.setReportingBalance((BigDecimal)data[8]);
			processResult.setReportedBalance((BigDecimal)data[9]);
			processResult.setTotalBalance((BigDecimal)data[10]);
			processResult.setAcreditationBalance(BigDecimal.ZERO);
			processResult.setBorrowerBalance(BigDecimal.ZERO);
			processResult.setLenderBalance(BigDecimal.ZERO);
			processResult.setLoanableBalance(BigDecimal.ZERO);
			processResult.setBuyBalance(BigDecimal.ZERO);
			processResult.setSellBalance(BigDecimal.ZERO);			
			processResult.setTransitBalance(BigDecimal.ZERO);
			processResult.setCorporativeOperation(corpOper);
			processResult.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			processResult.setIndRemanent(CorporateProcessConstants.IND_NO_REMAINDER);
			if(corpOper.getCurrency()!=null &&
					(corpOper.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					corpOper.getCurrency().equals(CurrencyType.DMV.getCode()))){
				processResult.setIndOriginTarget(CorporateProcessConstants.EXCHANGE_ORIGIN);
				processResult.setExchangeAmount(processResult.getTotalBalance());
			}else {
				processResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_ORIGIN);		
			}							
			processResult.setIndAdjustment(CorporateProcessConstants.IND_NO);
			resultOrigin.add(processResult);
			
			if(corpOper.getCurrency()!=null &&
					(corpOper.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					corpOper.getCurrency().equals(CurrencyType.DMV.getCode()))){
				CorporativeProcessResult corporativeProcessResultExchange = new CorporativeProcessResult(processResult);
				corporativeProcessResultExchange.setIndOriginTarget(CorporateProcessConstants.BALANCE_ORIGIN);
				processResult.setExchangeAmount(BigDecimal.ZERO);
				resultOrigin.add(corporativeProcessResultExchange);
			}
		}
		return resultOrigin;
	}

	/**
	 * Used to return a big decimal array of values that are came the CorporativeprocessResult balances.
	 *
	 * @param balances <code>BigDecimal[] </code>
	 * @param result <code>BigDecimal </code>
	 * @param total <code>BigDecimal </code>
	 * @return <code>BigDecimal[] </code>
	 */
	public BigDecimal[] matchBalances(BigDecimal[] balances, BigDecimal result,
			BigDecimal total) {
		BigDecimal amount = new BigDecimal(0);
		BigDecimal remnant = new BigDecimal(0);
		int i = 0;
		while ((Math.abs(remnant.doubleValue()) < Math.abs(result.doubleValue()))) {
			if (i == balances.length - 1) {
				BigDecimal balance = (balances[i].add(result)).subtract(remnant);
				while (balance.compareTo(BigDecimal.ZERO) <= 0 && i > 0) {
					i--;
					balance = (balances[i].add(result)).subtract(remnant);
				}
				if (i >= 0)
					balances[i] = balance;
				remnant= remnant.add(result);
			} else {
				if (balances[i].compareTo(BigDecimal.ZERO) > 0) {
					BigDecimal proportionality = CorporateProcessUtils.divide(balances[i], total);
					amount = CorporateProcessUtils.multiply(proportionality, result, 0);
					balances[i] = balances[i].add(amount);
					remnant = remnant.add(amount);
				}
			}
			i++;
		}
		return balances;
	}

	/**
	 * Used to add the CorporativeProcessResults and subtract them with the totalBalance and process the result.
	 *
	 * @param corpProcessResult <code>CorporativeProcessResult </code>
	 * @return <code>CorporativeProcessResult </code>
	 */
	public CorporativeProcessResult fixBalance(CorporativeProcessResult corpProcessResult) {

		BigDecimal total = corpProcessResult.getAvailableBalance().add(corpProcessResult.getBanBalance()).add(
				corpProcessResult.getOtherBlockBalance()).add(corpProcessResult.getPawnBalance()).add(
				corpProcessResult.getReportingBalance()).add(corpProcessResult.getMarginBalance()).add(
						corpProcessResult.getReserveBalance()).add(corpProcessResult.getOppositionBalance());
		BigDecimal result = corpProcessResult.getTotalBalance().subtract(total);
		BigDecimal[] balances, adjustBalances;
		if (result.compareTo(BigDecimal.ZERO) < 0) {
			balances = this.getBalanceList(corpProcessResult
					.getAvailableBalance(), corpProcessResult
					.getReportingBalance(), corpProcessResult
					.getMarginBalance(), corpProcessResult
					.getOppositionBalance(), corpProcessResult
					.getReserveBalance(), corpProcessResult
					.getOtherBlockBalance(), corpProcessResult
					.getBanBalance(), corpProcessResult
					.getPawnBalance());
			adjustBalances = matchBalances(balances, result,
					total);			
			corpProcessResult = this
					.setBalanceList(
							adjustBalances[CorporateProcessConstants.CP_PAWN],
							adjustBalances[CorporateProcessConstants.CP_BAN],
							adjustBalances[CorporateProcessConstants.CP_BLOCK_OTHER],
							adjustBalances[CorporateProcessConstants.CP_RESERVE],
							adjustBalances[CorporateProcessConstants.CP_OPPOSITION],
							adjustBalances[CorporateProcessConstants.CP_MARGIN],
							adjustBalances[CorporateProcessConstants.CP_REPORTING],
							adjustBalances[CorporateProcessConstants.CP_AVAILABLE],
							corpProcessResult);
		}
		if (result.compareTo(BigDecimal.ZERO) > 0) {
			balances = this.getBalanceList(corpProcessResult.getPawnBalance(), 
					corpProcessResult.getBanBalance(),
					corpProcessResult.getOtherBlockBalance(), corpProcessResult.getReserveBalance(), 
					corpProcessResult.getOppositionBalance(), corpProcessResult
							.getMarginBalance(), corpProcessResult
							.getReportingBalance(), corpProcessResult
							.getAvailableBalance());
			adjustBalances = matchBalances(balances, result,
					total);
			corpProcessResult = this
					.setBalanceList(
							adjustBalances[CorporateProcessConstants.CP_AVAILABLE],
							adjustBalances[CorporateProcessConstants.CP_REPORTING],
							adjustBalances[CorporateProcessConstants.CP_MARGIN],
							adjustBalances[CorporateProcessConstants.CP_OPPOSITION],
							adjustBalances[CorporateProcessConstants.CP_RESERVE],
							adjustBalances[CorporateProcessConstants.CP_BLOCK_OTHER],
							adjustBalances[CorporateProcessConstants.CP_BAN],
							adjustBalances[CorporateProcessConstants.CP_PAWN],
							corpProcessResult);
		}
		return corpProcessResult;
	}

	/**
	 * Set the give values from double array to the ProcessCorproateResult.
	 *
	 * @param bal1 BigDecimal
	 * @param bal2 BigDecimal
	 * @param bal3 BigDecimal
	 * @param bal4 BigDecimal
	 * @param bal5 BigDecimal
	 * @param bal6 BigDecimal
	 * @param bal7 the bal7
	 * @param bal8 the bal8
	 * @param corpProcessResult BigDecimal
	 * @return <code> CorporativeProcessResult</code>
	 */
	public CorporativeProcessResult setBalanceList(BigDecimal bal1, BigDecimal bal2,
			BigDecimal bal3, BigDecimal bal4, BigDecimal bal5, BigDecimal bal6, BigDecimal bal7, BigDecimal bal8,
			CorporativeProcessResult corpProcessResult) {
		corpProcessResult.setAvailableBalance(bal1);
		corpProcessResult.setReportingBalance(bal2);
		corpProcessResult.setMarginBalance(bal3);
		corpProcessResult.setOppositionBalance(bal4);
		corpProcessResult.setReserveBalance(bal5);
		corpProcessResult.setOtherBlockBalance(bal6);
		corpProcessResult.setBanBalance(bal7);
		corpProcessResult.setPawnBalance(bal8);
		return corpProcessResult;
	}

	/**
	 * used to construct the double array by using given param.
	 *
	 * @param bal1 BigDecimal
	 * @param bal2 BigDecimal
	 * @param bal3 BigDecimal
	 * @param bal4 BigDecimal
	 * @param bal5 BigDecimal
	 * @param bal6 BigDecimal
	 * @param bal7 the bal7
	 * @param bal8 the bal8
	 * @return <code> BigDecimal[]</code>
	 */
	public BigDecimal[] getBalanceList(BigDecimal bal1, BigDecimal bal2, BigDecimal bal3,
			BigDecimal bal4, BigDecimal bal5, BigDecimal bal6, BigDecimal bal7, BigDecimal bal8) {
		BigDecimal[] balances = new BigDecimal[CorporateProcessConstants.CP_BALANCES];
		balances[CorporateProcessConstants.CP_PAWN] = bal1;
		balances[CorporateProcessConstants.CP_BAN] = bal2;
		balances[CorporateProcessConstants.CP_BLOCK_OTHER] = bal3;
		balances[CorporateProcessConstants.CP_RESERVE] = bal4;
		balances[CorporateProcessConstants.CP_OPPOSITION] = bal5;
		balances[CorporateProcessConstants.CP_MARGIN] = bal6;
		balances[CorporateProcessConstants.CP_REPORTING] = bal7;
		balances[CorporateProcessConstants.CP_AVAILABLE] = bal8;
		return balances;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer#generateAccountComponent(java.util.List, com.pradera.model.corporatives.type.ImportanceEventType, com.pradera.model.corporatives.CorporativeOperation)
	 */
	public AccountsComponentTO generateAccountComponent(List<Object[]> holderAccounts, 
			ImportanceEventType processType, CorporativeOperation corpOperation){
		if(holderAccounts != null && !holderAccounts.isEmpty()){
			 List<HolderAccountBalanceTO> sourceAccountBalanceTOs = new  ArrayList<HolderAccountBalanceTO>();
			 List<HolderAccountBalanceTO> targetAccountBalanceTOs = new  ArrayList<HolderAccountBalanceTO>();
			 for(Object[] data : holderAccounts){
				 HolderAccountBalanceTO account = new HolderAccountBalanceTO();			
				 account.setIdHolderAccount((Long)data[0]);
				 account.setIdParticipant((Long)data[1]);
				 account.setIdSecurityCode((String)data[2]);
				 account.setStockQuantity((BigDecimal)data[4]);
				 boolean targetAccount = false;
				 if(Integer.valueOf(CorporateProcessConstants.BALANCE_ORIGIN).equals(data[3])){
					 sourceAccountBalanceTOs.add(account);
				 }else{
					 if(ImportanceEventType.SECURITIES_EXCISION.equals(processType)){
						 for(SecuritiesCorporative objSecuritiesCorporatives : corpOperation.getSecuritiesCorporatives()){
							 if(objSecuritiesCorporatives.getSecurity().getIdSecurityCodePk().equals(account.getIdSecurityCode())){
								 corpOperation.setMarketDate(objSecuritiesCorporatives.getMarketDate());
								 corpOperation.setMarketPrice(objSecuritiesCorporatives.getMarketPrice());
								 break;
							 }
						 }
					 }
					 targetAccountBalanceTOs.add(account);
					 targetAccount=true;
				 }
				 //call market fact movements
				 generateMarketFactCorporateHolder(processType, corpOperation, account, targetAccount);
			 }
			
			return CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
					null, null, null, null, processType,
					sourceAccountBalanceTOs, targetAccountBalanceTOs, CorporateProcessConstants.BL_AVAILABLE,idepositarySetup.getIndMarketFact());
		}
		return null;
	}
	
	/**
	 * Generate market fact corporate holder.
	 *
	 * @param processType the process type
	 * @param corpOperation the corp operation
	 * @param account the account
	 * @param destiny the destiny
	 */
	private void generateMarketFactCorporateHolder(ImportanceEventType processType,CorporativeOperation corpOperation,HolderAccountBalanceTO account,boolean destiny){
		if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
			MarketFactAccountTO mktFact;
			List<HolderMarketFactBalance> marketfacts;
			switch (processType) {
			case CAPITAL_AMORTIZATION:
				//only register sub balances in origin
				marketfacts = 
				corporateExecutorService.getHolderMarketFactBalances(account.getIdHolderAccount(),
						account.getIdSecurityCode(), account.getIdParticipant());
				account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for(HolderMarketFactBalance marketBalance : marketfacts){
					mktFact = new MarketFactAccountTO();
					mktFact.setMarketDate(marketBalance.getMarketDate());
					mktFact.setMarketPrice(marketBalance.getMarketPrice());
					mktFact.setQuantity(marketBalance.getTotalBalance());
					account.getLstMarketFactAccounts().add(mktFact);
				}
				break;
			case ACTION_DIVIDENDS:
				//only register one sub balance in destiny
				mktFact = new MarketFactAccountTO();
				mktFact.setMarketDate(corpOperation.getMarketDate());
				mktFact.setMarketPrice(corpOperation.getMarketPrice());
				mktFact.setQuantity(account.getStockQuantity());
				account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				account.getLstMarketFactAccounts().add(mktFact);
				break;
			case SECURITIES_EXCLUSION:
				//only register sub balances in origin
				marketfacts = 
				corporateExecutorService.getHolderMarketFactBalances(account.getIdHolderAccount(),
						account.getIdSecurityCode(), account.getIdParticipant());
				account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for(HolderMarketFactBalance marketBalance : marketfacts){
					mktFact = new MarketFactAccountTO();
					mktFact.setMarketDate(marketBalance.getMarketDate());
					mktFact.setMarketPrice(marketBalance.getMarketPrice());
					mktFact.setQuantity(marketBalance.getTotalBalance());
					account.getLstMarketFactAccounts().add(mktFact);
				}
				break;
				
			case CHANGE_NOMINAL_VALUE_NO_CAP_VAR:
			case CHANGE_NOMAL_VALUE_CAP_VAR:
			case CAPITAL_REDUCTION:
			case CONVERTIBILY_BONE_TO_ACTION:
			case SECURITIES_FUSION:
			case SECURITIES_UNIFICATION:
				if(destiny){
					mktFact = new MarketFactAccountTO();
					mktFact.setMarketDate(corpOperation.getMarketDate());
					mktFact.setMarketPrice(corpOperation.getMarketPrice());
					mktFact.setQuantity(account.getStockQuantity());
					account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
					account.getLstMarketFactAccounts().add(mktFact);	
				} else {
					marketfacts = 
							corporateExecutorService.getHolderMarketFactBalances(account.getIdHolderAccount(),
									account.getIdSecurityCode(), account.getIdParticipant());
							account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
							for(HolderMarketFactBalance marketBalance : marketfacts){
								mktFact = new MarketFactAccountTO();
								mktFact.setMarketDate(marketBalance.getMarketDate());
								mktFact.setMarketPrice(marketBalance.getMarketPrice());
								mktFact.setQuantity(marketBalance.getTotalBalance());
								account.getLstMarketFactAccounts().add(mktFact);
							}
				}
				break;
			
			case PREFERRED_SUSCRIPTION:
				//only register one sub balance in destiny
				mktFact = new MarketFactAccountTO();
				mktFact.setMarketDate(corpOperation.getMarketDate());
				mktFact.setMarketPrice(corpOperation.getMarketPrice());
				mktFact.setQuantity(account.getStockQuantity());
				account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				account.getLstMarketFactAccounts().add(mktFact);
				break;
				
			case END_PREFERRED_SUSCRIPTION:
				//only register sub balances in origin
				marketfacts = 
				corporateExecutorService.getHolderMarketFactBalances(account.getIdHolderAccount(),
						account.getIdSecurityCode(), account.getIdParticipant());
				account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for(HolderMarketFactBalance marketBalance : marketfacts){
					mktFact = new MarketFactAccountTO();
					mktFact.setMarketDate(marketBalance.getMarketDate());
					mktFact.setMarketPrice(marketBalance.getMarketPrice());
					mktFact.setQuantity(marketBalance.getTotalBalance());
					account.getLstMarketFactAccounts().add(mktFact);
				}
				break;
			
			case SECURITIES_EXCISION:
				if(destiny){
					mktFact = new MarketFactAccountTO();
					mktFact.setMarketDate(corpOperation.getMarketDate());
					mktFact.setMarketPrice(corpOperation.getMarketPrice());
					mktFact.setQuantity(account.getStockQuantity());
					account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
					account.getLstMarketFactAccounts().add(mktFact);	
				} else {
					marketfacts = 
							corporateExecutorService.getHolderMarketFactBalances(account.getIdHolderAccount(),
									account.getIdSecurityCode(), account.getIdParticipant());
							account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
							for(HolderMarketFactBalance marketBalance : marketfacts){
								mktFact = new MarketFactAccountTO();
								mktFact.setMarketDate(marketBalance.getMarketDate());
								mktFact.setMarketPrice(marketBalance.getMarketPrice());
								mktFact.setQuantity(marketBalance.getTotalBalance());
								account.getLstMarketFactAccounts().add(mktFact);
							}
				}
				break;
			default:
				break;
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer#setLoggerUserForTransationRegistry()
	 */
	@Interceptors(ContextHolderInterceptor.class)
	public boolean setLoggerUserForTransationRegistry(){
		return true;
	}

}
