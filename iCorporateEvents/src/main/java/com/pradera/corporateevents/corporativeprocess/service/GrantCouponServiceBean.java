package com.pradera.corporateevents.corporativeprocess.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.coupongrant.CouponGrantOperation;
import com.pradera.model.custody.coupongrant.type.CouponGrantStateType;

// TODO: Auto-generated Javadoc
/**
 * The Class GrantCouponServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class GrantCouponServiceBean extends CrudDaoServiceBean{

	/**
	 * Finish grant coupon.
	 *
	 * @param idProgramInterestCoupon the id program interest coupon
	 * @throws ServiceException the service exception
	 */
	public void finishGrantCoupon(Long idProgramInterestCoupon) throws ServiceException
	{
		List<CouponGrantOperation> lstCouponGrantOperations= null;
		
	}
	
	/**
	 * Gets the list coupon grant operation.
	 *
	 * @param idProgramInterestCoupon the id program interest coupon
	 * @param reason the reason
	 * @return the list coupon grant operation
	 */
	public List<CouponGrantOperation> getListCouponGrantOperation(Long idProgramInterestCoupon, Integer reason)
	{
		List<CouponGrantOperation> lstCouponGrantOperations= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT CGO FROM CouponGrantOperation CGO ");
		stringBuffer.append(" WHERE CGO.programInterestCoupon.idProgramInterestPk = :idProgramInterestCoupon ");
		stringBuffer.append(" and CGO.operationState = :operationState ");
		
		TypedQuery<CouponGrantOperation> query = em.createQuery(stringBuffer.toString(), CouponGrantOperation.class);
		query.setParameter("idProgramInterestCoupon", idProgramInterestCoupon);
		query.setParameter("operationState", CouponGrantStateType.CONFIRMED.getCode());
		lstCouponGrantOperations= query.getResultList();
		return lstCouponGrantOperations;
	}

	/**
	 * Gets the coupon grant balances.
	 *
	 * @param corporativeID the corporative id
	 * @param blInclude the bl include
	 * @return the coupon grant balances
	 */
	public List<Object[]> getCouponGrantBalances(Long corporativeID, boolean blInclude){
		List<Object[]> lstObjects = new ArrayList<Object[]>();

		String strQuery = 
				"select "
				+ "cgb.holderAccount.idHolderAccountPk,"							//[0]
				+ "cgb.participant.idParticipantPk,"								//[1]
				+ "cgb.grantedBalance,"												//[2]
				+ "cgb.balanceType "												//[3]
				+ "from CouponGrantBalance cgb, CorporativeOperation co "
				+ "where "
				+ "cgb.programInterestCoupon.idProgramInterestPk = co.programInterestCoupon.idProgramInterestPk "
				+ "and co.idCorporativeOperationPk = :corporativeID "
				+ "order by cgb.balanceType desc";

		Query query = em.createQuery(strQuery);
		query.setParameter("corporativeID",corporativeID);
		lstObjects = query.getResultList();

		return lstObjects;
	}
}
