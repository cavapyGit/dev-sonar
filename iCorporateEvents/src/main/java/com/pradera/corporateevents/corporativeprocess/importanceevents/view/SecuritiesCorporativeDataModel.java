package com.pradera.corporateevents.corporativeprocess.importanceevents.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.corporatives.SecuritiesCorporative;

// TODO: Auto-generated Javadoc
/**
 * The Class SecuritiesCorporativeDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class SecuritiesCorporativeDataModel  extends ListDataModel<SecuritiesCorporative> implements SelectableDataModel<SecuritiesCorporative> {
	
	 

 
		/**
		 * Instantiates a new securities corporative data model.
		 */
		public SecuritiesCorporativeDataModel(){

		}

		/**
		 * Instantiates a new securities corporative data model.
		 *
		 * @param data the data
		 */
		public SecuritiesCorporativeDataModel(List<SecuritiesCorporative> data){
			super(data);
		}

		/* (non-Javadoc)
		 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
		 */
		@Override
		public SecuritiesCorporative getRowData(String rowKey) {

			List<SecuritiesCorporative> operatiosn = (List<SecuritiesCorporative>) getWrappedData();
			for(SecuritiesCorporative operation: operatiosn){
				if(operation.getIdSecuritiesCorporativePk().equals(Long.parseLong(rowKey))){
					return operation;

				}
			}
			return null;

		}

		/* (non-Javadoc)
		 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
		 */
		@Override
		public Object getRowKey(SecuritiesCorporative operation) {
			return operation.getIdSecuritiesCorporativePk();

		}

	


}
