package com.pradera.corporateevents.corporativeprocess.batch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateServiceFacade;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CorporateProcessServiceBean;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporateMonitoringTO;
import com.pradera.corporateevents.stockcalculation.facade.ManageStockCalculationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
// TODO: Auto-generated Javadoc

/**
 * Bachero que ejecuta proceso preliminar automatico.
 *
 * @author PraderaTechnologies
 */
@BatchProcess(name="CorporateProcessManagerStockPreliminaryBatch")
@RequestScoped
public class CorporateProcessManagerStockPreliminaryBatch implements JobExecution, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate service facade. */
	@EJB
	private CorporateServiceFacade corporateServiceFacade;
	
	/** The corporate process service. */
	@EJB
	private CorporateProcessServiceBean corporateProcessService;
	
	/** The manage stock calculation facade. */
	@EJB
	private static ManageStockCalculationServiceFacade manageStockCalculationFacade;
	
	/** The batch service. */
	@EJB
	private BatchServiceBean batchService;
	
	/** The Holder Balance Movement Service Bean*/
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The notification service facade. */
	@EJB
	private NotificationServiceFacade notificationServiceFacade; 
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The obj corporate process service facade. */
	@EJB
	CorporateProcessServiceFacade objCorporateProcessServiceFacade;
	
	/** The stock calculation process. */
	private static StockCalculationProcess stockCalculationProcess;
	
	/** The lst preliminary notification. */
	private LinkedHashMap flagProcessStock=null ,flagSecurityProcessPreliminary=null,flagEndPreliminary=null ,idSecurityEndPreliminary=null,lstCorporativeEventsAll,
			lstTimeStockCalculationProcess = null,lstTimePreliminaryProcess = null,lstStockFailNotification = null , lstPreliminaryNotification=null;

	private LinkedHashMap<String,String>  lstSecurity;
	/** The Map CorporativeOperation By IdSecurityCodePk*/
	private Map<String,CorporativeOperation> mapCorporativeOperation=null;
	
	/** The flag mensaje. */
	private Boolean flagCreateArrayEndPreliminary=Boolean.FALSE,flagReprocess = Boolean.FALSE  ,flagMensaje = Boolean.FALSE;
	
	/** The quantity block stock. */
	private int quantityBlockStock;
	
	/** The date process corporate. */
	private Date dateProcessCorporate;
	
	/** The limit block. */
	private Integer limitBlock = null;//cantidad por bloque
	
	/** The wait time. */
	private Integer waitTime = null;//tiempo de espera despues de lanzar un bloque
	
	/** The wait time process. */
	private Integer waitTimeProcess = null;//tiempo de espera de un proceso enviado que no finaliza
	
	/** The user id. */
	private String userId = null;//usuario demonio
	
	/** The current date. */
	private Date currentDate = null;//fecha dinamica para pruebas (debe ser la fecha del servidor)
	
	/**
	 * Inicio del Bachero que ejecuta proceso preliminar automatico.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger){
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		
		//obtenemos variables del procesos automatico
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			if(processLoggerDetail.getParameterName().equals("limitBlock") && processLoggerDetail.getParameterValue()!=null){
				limitBlock = Integer.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("waitTime") && processLoggerDetail.getParameterValue()!=null){
				waitTime = Integer.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("userId")){
				userId = processLoggerDetail.getParameterValue();
			}else if(processLoggerDetail.getParameterName().equals("currentDate") && processLoggerDetail.getParameterValue()!=null){
				currentDate = CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue(),CommonsUtilities.DATE_PATTERN);
			}else if(processLoggerDetail.getParameterName().equals("waitTimeProcess") && processLoggerDetail.getParameterValue()!=null){
				waitTimeProcess = Integer.valueOf(processLoggerDetail.getParameterValue());
			}
		}
		
		//validamos variables del procesos automatico
		if(Validations.validateIsNotNull(limitBlock) && Validations.validateIsNotNull(waitTime) && Validations.validateIsNotNull(waitTimeProcess)
				&& Validations.validateIsNotNull(userId) && Validations.validateIsNotNull(currentDate)){
			log.info("Execute Corporate process\n");
			try {
				log.info("<<<<<<<<<<<<<<<<< INICIO DE EJECUCION DEL PROCESO DEMONIO - 1 : >>>>>>>>>>>>>>>>>>>>>>>>>"+
						"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
				executeCorporateProcess();
				//Se reprocesa para verificar y ejecutar stock preliminar q no se ejecutaron
				if(flagReprocess.equals(Boolean.FALSE)){
					flagCreateArrayEndPreliminary = Boolean.FALSE;
					lstSecurity= null;
					flagReprocess = Boolean.TRUE;
					log.info("<<<<<<<<<<<<<<<<< INICIO DE EJECUCION DEL PROCESO DEMONIO - 2 : >>>>>>>>>>>>>>>>>>>>>>>>>"+
							"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
					executeCorporateProcess();
				}
			} catch (ServiceException e) {
				log.error(e.getMessage());
				throw new RuntimeException(e);
			}
		}else{
			log.error("Invalied parameters for the corporate process Manager execution\n");
			throw new RuntimeException("Invalied parameters for the corporate process Manager execution");
		}		
	}
	
	/**
	 * Metodo que ejecuta los preliminares de los procesos corporativos en bloques.
	 *
	 * @throws ServiceException the base exception
	 */
	public void executeCorporateProcess()throws ServiceException{
		try{
			//corporateProcessStockPreliminary(limitBlock);
			log.error("voy a ejecutar primero el stock");
			executeAllCorporateProcessStock();
			Thread.sleep(1*CorporateProcessConstants.MILLSECS_IN_MIN);
			log.error("voy a ejecutar primero los preeliminares luego de 1 minuto");
			corporateProcessStockPreliminary(limitBlock);			
			while(!corporateProcessStock() || !endCorporateProcessPreliminary())
			{
				//Se dividio el tiempo de espera por 3
				Thread.sleep(waitTime.intValue()*CorporateProcessConstants.MILLSECS_IN_MIN/20);
				corporateProcessStockPreliminary(limitBlock.intValue());
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	

	public void executeAllCorporateProcessStock()throws ServiceException{
		try {
			
			if(Validations.validateIsNull(lstSecurity) || (Validations.validateIsNotNull(lstSecurity) && lstSecurity.isEmpty())){
	
				createObject();		
	
				loadListSecurityExecution();
				
				for (Map.Entry<String, String> objSecurity : lstSecurity.entrySet() ){
					String idSecurity = objSecurity.getKey();
					executeStock(idSecurity);
				}
			}
			
		}catch (Exception e) {
			log.error(e.getMessage()+"linea 235\n");
			throw new RuntimeException(e);
		}
	}
	/**
	 * Metodo que ejecuta preliminar del proceso corporativo.
	 *
	 * @param limit limit block
	 * @throws ServiceException the base exception
	 */
	public void corporateProcessStockPreliminary(int limit)throws ServiceException{
		try{
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<RESUMEN>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			// El siguiente bachero realizara el proceso corporativo (Stock-Preliminar) automatico de los beneficios
			// Primero listara todos los hechos de importancia que estaran encluidos en el proceso
			// Segundo listara los valores de forma priorizada (numero de movimientos) que se ejecutara el stock y envio de notificaciones
			// Tercero listara todos los hechos de importancia como ayuda para verificar la ejecucion de los preliminares y envio de notificaciones
						
			// Cuarto ejecutara stock de valores en bloque (numero de bloque es paramatizable) - solo se ejecutara stock de otro bloque si finaliza el bloque anterior
			// Quinto ejecutara preliminares de los valores que tengan stock del bloque ejecutado - solo se ejecutara preliminares de otro bloque si finaliza el bloque anterior
			
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<01>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//Obtenemos los procesos corporativos en dos listas para ayuda
			//La primera lista es para obtener los valores que se ejecutara el stock en la fecha de registro y corte correspondiente
			//La segunda lista es para verificar si se lanzaron los priliminares correspondientes al valor que tiene stock en la fecha de registro y corte correspondiente
			//Se creara listas auxiliares como ayuda para verificar la ejecucion correcta del stock y preliminar y el envio de notificaciones 
			
			if(Validations.validateIsNull(lstSecurity) || (Validations.validateIsNotNull(lstSecurity) && lstSecurity.isEmpty())){
				
				//creamos objetos 
				
				//se crea lista valores que se ejecutara stock
				//se crea lista estado de stock en proceso (enviados)	
				//se crea lista estado de preliminares en proceso (enviados)
				//se inicializa en cero cantidad de bloques de stock (solo una vez al dia)
				//se crea lista de tiempo de inicio de proceso enviado de stock y preliminar
				//se crea lista de notificaciones fallidas de stock
				//se crea lista de notificacion de preliminares finalizadas (Correctas - Fallidas)
				
				//se crea lista que verifica valores que no tienen stock o stock fallido - ayuda para no enviar notificaciones de preliminares
				//se crea lista estado de preliminares terminados
				//se crea lista de todos los hechos de importancia como ayuda para verificar la ejecucion de los preliminares y envio de notificaciones
								
				createObject();			
				
				//se crea lista de todos los hechos de importancia para luego obtener todos los valores que se ejecutara el stock
				//se crea lista de valores que se ejecutara el stock
				//se reordenara lista de valores que se ejecutara el stock
				
				loadListSecurityExecution();
			
			}
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<02>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			//se verificara si todos los valores de un bloque se ejecuto su preliminar 
			if(validateBlockPreliminary() && Validations.validateIsNotNull(lstSecurity) && !lstSecurity.isEmpty()){
				//verificamos si el preliminar finalizo y se envia notificaciones de preliminares
				setStateEndPreliminaryAndNotification(lstSecurity,idSecurityEndPreliminary);
			}else if (Validations.validateIsNotNull(lstSecurity) && !lstSecurity.isEmpty()){
				//enviamos stock y preliminares en bloques
				executeStockPreliminary(lstSecurity,limit);
				//monitoreamos stock y preliminares enviados
				monitoringStockPreliminary(lstSecurity);
			}
		}catch (Exception e) {
			log.error(e.getMessage()+"linea 235\n");
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Metodo que monitorea envios de procesos corporativos.
	 *
	 * @param lstSecurity List Security
	 */
	public void monitoringStockPreliminary(LinkedHashMap<String, String> lstSecurity){
		log.info("Tamanio lista de valores: "+lstSecurity.size()+"\n");
		log.info("Tamanio flag Stock Enviados: "+flagProcessStock.size()+"\n");
		log.info("Tamanio flag Stock-Preliminar Envidados: "+flagSecurityProcessPreliminary.size()+"\n");
		for (Map.Entry<String, String> objProcessCorporate : lstSecurity.entrySet())
		{
			String idSecurity = objProcessCorporate.getKey();
			log.info("flag Stock Enviado:"+" "+idSecurity+" "+(Boolean)flagProcessStock.get(idSecurity)+"\n");
			log.info("flag Stock-Preliminar Envidado:"+" "+idSecurity+" "+(Boolean)flagSecurityProcessPreliminary.get(idSecurity)+"\n");
		}
	}
	
	/**
	 * Metodo que ejecuta preliminares de los procesos corporativos en bloques.
	 *
	 * @param lstSecurity List Security
	 * @param limit the limit
	 * @throws ServiceException the base exception
	 */
	public void executeStockPreliminary(LinkedHashMap<String, String> lstSecurity,int limit)throws ServiceException{
		
		//El presente metodo envia los stock (tipo CORPORATIVO por valor) por bloques. Luego de lanzar los stock
		//verificamos cual termino y se envia los preliminares de los procesos corporativos relacionados
		//al valor que se ejecuto el stock. Por cada finalizacion o no finalizacion de un procesos de stock
		//o un proceso preliminar se enviara notificaciones al usuario respectivo. El dicho proceso
		//se repetetira por cada bloque. El numero de objetos por bloque sera parametrizable, asi mismo el tiempo
		//de espera por cada lanzamiento de un bloque.
		
		try {
			//obtenemos los stock por Valor ejecutados en la fecha actual para saber cuales procesos fueron terminados
			LinkedHashMap<String,String> lstSecurityStock = corporateServiceFacade.getListSecuritiesStockByRecordDate(CommonsUtilities.currentDate(), StockStateType.FINISHED.getCode());		
			//inicializamos recorrido para cada bloque de stock
			int counterBlock=0;//contador de numero de objetos por bloques
			int counterPreliminaryProcessForSecurity=0;//contador de numero valores que se ejecutaron preliminares
			int counterInitial = quantityBlockStock*limit;//inicio del bloque
			int count = 0;
			//recorremos los valores en bloques
			for (Map.Entry<String, String> objSecurity : lstSecurity.entrySet()){
				//empezamos a procesar a partir del inicio del bloque
				if(count<counterInitial){
					count++;
				}else{
					//obtenemos codigo del valor
					String idSecurity = objSecurity.getKey();

					log.info("<<<<<<<<<<<<<<<<< BLOQUE "+(quantityBlockStock+1)+" - VALOR "+ idSecurity
							+ "  >>>>>>>>>>>>>>>>>>>>>>>>>"+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");					
					//se verifica si el Valor no tiene stock terminado
					if(Validations.validateIsNull(lstSecurityStock) || (Validations.validateIsNotNull(lstSecurityStock) &&
							(lstSecurityStock.isEmpty() || !lstSecurityStock.containsKey(idSecurity)))){
						log.error("NO TIENE REGISTRADO PROCESO DE STOCK TERMINADO: " +idSecurity+"\n");
						
						/**
						 * Validacion, Si el valor no tiene Cartera.
						 */
						if(Validations.validateIsNullOrNotPositive(holderBalanceMovementsServiceBean.getBalanceHolderFromSecurity(idSecurity, false))){
							List<CorporativeOperation> listCorporativeOperation = loadProcessCorporativeBySecurity(dateProcessCorporate, idSecurity);
							
							if(Validations.validateListIsNotNullAndNotEmpty(listCorporativeOperation)){
								// Crear un Calculo de Stock Ficticio sin Stock Calculation Balance, 
								// al no tener cartera, no crea stock
								if(!corporateServiceFacade.findSecuritiesStockWithoutPortfolioByRecordDate(idSecurity, CommonsUtilities.currentDate(), StockStateType.FINISHED.getCode())){
									createStockCalculationProcess(idSecurity);
								}
								for (CorporativeOperation corporativeOperation : listCorporativeOperation) {
									objCorporateProcessServiceFacade.setDataForCorporativeProcessFail(corporativeOperation);
								}
								log.info(" EL VALOR "+idSecurity+" NO TIENE SALDOS EN CARTERA, CAMBIO A ESTADO EN DEFINITIVO. ");
							}else{
								// Significa que no tiene Corporative Operations en estado Registrado, 
								// porque ya se proceso el estado definitivo
								log.info(" EL VALOR "+idSecurity+" NO TIENE SALDOS EN CARTERA. Y YA SE ENCUENTRA EN ESTADO DEFINITIVO ");
//								counterPreliminaryProcessForSecurity++;//Se ejecuto como definitivo
								counterBlock=0;
							}
							
							
						}else if(executeStock(idSecurity)){
							//si el stock fue enviado y fallo
							counterPreliminaryProcessForSecurity++;
						}
						counterBlock++;
					}else{
						log.info("SI TIENE REGISTRADO PROCESO DE STOCK TERMINADO: " +idSecurity+"\n");
						// en caso que mande mas de una vez calculo de stock en el dia de un mismo valor o ya tenga calculo de stock manual
						flagProcessStock.put(idSecurity, Boolean.TRUE);
						//verificamos si se envio el preliminar de todos los corporativos asociados al valor
						Boolean flagPreliminay= (Boolean)flagSecurityProcessPreliminary.get(idSecurity);
						if(flagPreliminay.equals(Boolean.FALSE)){
							//ejecutamos preliminares de procesos corporativos del valor 
							flagSecurityProcessPreliminary.put(idSecurity, Boolean.TRUE);
							executePreliminary(idSecurity,lstCorporativeEventsAll);
							counterPreliminaryProcessForSecurity++;																												
						}else{
							counterPreliminaryProcessForSecurity++;
						}
						counterBlock++;	
					}
					
					//verifica si el numero de valores que ejecutaron preliminar es igual limite del bloque entonces aumentamos cantidad de bloque
					if(counterPreliminaryProcessForSecurity==limit){
						quantityBlockStock++;
					}

					
					//verifica si el recorrido es igual limite del bloque entonces salimos del recorrido de la lista de valores
					if(counterBlock==limit){
						break;
					}
							
				}
			}
		} catch (ServiceException e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	/**
	 *  Metodo que ejecuta el preliminar del proceso corporativo automatico.
	 *
	 * @param idSecurity id security
	 * @param lstCorporativeEventsAll the lst corporative events all
	 * @throws ServiceException the Service Exception
	 */
	public void executePreliminary(String idSecurity,LinkedHashMap<Long, CorporativeOperation> lstCorporativeEventsAll)throws ServiceException{		
		//obtenemos lista de procesos corporativos asosciados al valor
		List<CorporativeOperation> lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(idSecurity,lstCorporativeEventsAll);
		//validamos stock
		if(validateStock(idSecurity)){
			//validamos proceso preliminar
			//lstCorporativeEventsExecutePreliminary = validatePreliminaryProcess(lstCorporativeEventsExecutePreliminary);
			if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
				//recorremos lista de procesos corporativos asosciados al valor y ejecutamos el preliminar
				for (CorporativeOperation objCorporativeOperation : lstCorporativeEventsExecutePreliminary){
					log.info(objCorporativeOperation.getCorporativeEventType().getDescription()+" al VALOR "+objCorporativeOperation.getSecurities().getIdSecurityCodePk());

					 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(objCorporativeOperation.getCorporativeEventType().getCorporativeEventType()) 
							 || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(objCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
						 //Validating if the Security has desmaterialized Balance
						 BigDecimal desmaterializedBalance = corporateProcessService.searchDesmaterializedBalance(objCorporativeOperation.getSecurities().getIdSecurityCodePk());
						 if(desmaterializedBalance.compareTo(BigDecimal.ZERO) > 0 &&
								 corporateProcessService.hasBalance(objCorporativeOperation.getSecurities().getIdSecurityCodePk())){					
							 	BusinessProcess businessProcess = new BusinessProcess();
								businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
							    Map<String, Object> param = new HashMap<String, Object>();
							    param.put("processid", objCorporativeOperation.getIdCorporativeOperationPk());
							    param.put("userid", userId);		
							    param.put("corporateType", objCorporativeOperation.getCorporativeEventType().getDescription());	
							    batchService.registerBatchTx(userId, businessProcess, param);
						 }else{
							 objCorporateProcessServiceFacade.setDataForCorporativeProcessFail(objCorporativeOperation);	
						 }
					 }else{
						 	BusinessProcess businessProcess = new BusinessProcess();
							businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
						    Map<String, Object> param = new HashMap<String, Object>();				    
						    param.put("processid", objCorporativeOperation.getIdCorporativeOperationPk());
						    param.put("userid", userId);		
						    param.put("corporateType", objCorporativeOperation.getCorporativeEventType().getDescription());	
						    batchService.registerBatchTx(userId, businessProcess, param);			
					 }					
				}
			}else{
				idSecurityEndPreliminary.put(idSecurity,idSecurity);
			}
		}else{
			idSecurityEndPreliminary.put(idSecurity,idSecurity);
			//enviamos notificacion de stock con inconsistencia 
			if(!lstStockFailNotification.containsKey(idSecurity)){
				notificationStockNotValid(idSecurity);
				lstStockFailNotification.put(idSecurity, idSecurity);
			}
		}				
	}
	
	/**
	 * Metodo que retorna lista de preliminares no validados
	 *
	 * @param idSecurity id security
	 * @param lstCorporativeEventsAll List Corporative Events All
	 * @return List Corporative Events Execute Preliminary
	 */
	public List<CorporativeOperation> getListCorporativeEventsExecutePreliminary(String idSecurity,LinkedHashMap<Long, CorporativeOperation> lstCorporativeEventsAll){
		List<CorporativeOperation> lstCorporativeEventsExecutePreliminary = new ArrayList<CorporativeOperation>();
		//recorremos lista de todos los corporativos y llenamos corporativos asociados al valor
		for (Map.Entry<Long, CorporativeOperation> objCorporativeOperation : lstCorporativeEventsAll.entrySet()){
			CorporativeOperation objCorporativeOperationAux = objCorporativeOperation.getValue();
			if(objCorporativeOperationAux.getSecurities().getIdSecurityCodePk().equals(idSecurity)){
				lstCorporativeEventsExecutePreliminary.add(objCorporativeOperationAux);
			}
		}
		return lstCorporativeEventsExecutePreliminary;
	}
	
	/**
	 * Metodo que valida el stock de una valor.
	 *
	 * @param idSecurity id security
	 * @return Validate Stock
	 */
	public boolean validateStock(String idSecurity){
		boolean flagStock=true;
		Long idStockProcess;
		try {
			idStockProcess = corporateServiceFacade.getStockBySecurityAndRecordDate(idSecurity,CommonsUtilities.currentDate(), StockStateType.FINISHED.getCode());		
			if(idStockProcess!=null){
				Long countBalance = corporateProcessService.validateBalanceStockCalculationCorporate(idStockProcess, idSecurity);
				if((countBalance!=null && countBalance==0)||corporateProcessService.validateInconsistencyBalance(idStockProcess, idSecurity)){
					log.error("STOCK CON INCONSISTENCIA: " +idSecurity+"\n");
					flagStock=false;
				}	
			}else{
				flagStock=false;
			}	
		} catch (ServiceException e) {
			log.error(e.getMessage()+" linea 416\n");
			throw new RuntimeException(e);
		}
		return flagStock;
	}
	
	/**
	 * Metodo valida procesos corporativos.
	 *
	 * @param lstCorporativeEventsExecutePreliminary Corporative Events Execute Preliminary
	 * @return list Corporative Events Execute Preliminary
	 */
	public List<CorporativeOperation> validatePreliminaryProcess(List<CorporativeOperation> lstCorporativeEventsExecutePreliminary){
		try{
			if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
				RecordValidationType objPreliminaryProcessStates = corporateProcessService.verifyPreliminaryProcessStates(lstCorporativeEventsExecutePreliminary,true);
				if(Validations.validateIsNotNull(objPreliminaryProcessStates)){
					for(Long idCorporativeProcess : objPreliminaryProcessStates.getErrorProcess()){
						String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_PROCESS_STATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
						if(!lstPreliminaryNotification.containsKey(id_error)){						
							lstPreliminaryNotification.put(id_error, idCorporativeProcess);
						}
					}
					notificationPreliminaryNotValid(objPreliminaryProcessStates.getErrorDescription());	
					lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objPreliminaryProcessStates.getErrorProcess());
				}
				if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
					RecordValidationType objPreliminarDeliveryDate = corporateProcessService.verifyPreliminarDeliveryDate(lstCorporativeEventsExecutePreliminary,true);
					if(Validations.validateIsNotNull(objPreliminarDeliveryDate)){
						for(Long idCorporativeProcess : objPreliminarDeliveryDate.getErrorProcess()){
							String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_DELIVERY_DATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
							if(!lstPreliminaryNotification.containsKey(id_error)){						
								lstPreliminaryNotification.put(id_error, idCorporativeProcess);
							}
						}
						notificationPreliminaryNotValid(objPreliminarDeliveryDate.getErrorDescription());	
						lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objPreliminarDeliveryDate.getErrorProcess());
					}
					if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
						RecordValidationType objSecurityStatus = corporateProcessService.verifySecurityStatus(lstCorporativeEventsExecutePreliminary,true);
						if(Validations.validateIsNotNull(objSecurityStatus)){
							for(Long idCorporativeProcess : objSecurityStatus.getErrorProcess()){
								String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_SECURITY_STATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
								if(!lstPreliminaryNotification.containsKey(id_error)){						
									lstPreliminaryNotification.put(id_error, idCorporativeProcess);
								}
							}
							notificationPreliminaryNotValid(objSecurityStatus.getErrorDescription());	
							lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objSecurityStatus.getErrorProcess());
						}					
						if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
							RecordValidationType objSecurityDesmaterialized = corporateProcessService.verifySecurityDesmaterialized(lstCorporativeEventsExecutePreliminary,true);
							if(Validations.validateIsNotNull(objSecurityDesmaterialized)){
								for(Long idCorporativeProcess : objSecurityDesmaterialized.getErrorProcess()){
									String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_SECURITY_DESMATERIALIZED+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
									if(!lstPreliminaryNotification.containsKey(id_error)){						
										lstPreliminaryNotification.put(id_error, idCorporativeProcess);
									}
								}
								notificationPreliminaryNotValid(objSecurityDesmaterialized.getErrorDescription());	
								lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objSecurityDesmaterialized.getErrorProcess());
							}	
							if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
								RecordValidationType objNotSettledBalancesCorporate = corporateProcessService.verifyNotSettledBalancesCorporate(lstCorporativeEventsExecutePreliminary,true);
								if(Validations.validateIsNotNull(objNotSettledBalancesCorporate)){
									for(Long idCorporativeProcess : objNotSettledBalancesCorporate.getErrorProcess()){
										String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_NOT_SETTLEMENT_BALANCE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
										if(!lstPreliminaryNotification.containsKey(id_error)){						
											lstPreliminaryNotification.put(id_error, idCorporativeProcess);
										}
									}
									notificationPreliminaryNotValid(objNotSettledBalancesCorporate.getErrorDescription());	
									lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objNotSettledBalancesCorporate.getErrorProcess());
								}	
								if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
									RecordValidationType objTransitBalances = corporateProcessService.verifyTransitBalances(lstCorporativeEventsExecutePreliminary,true);
									if(Validations.validateIsNotNull(objTransitBalances)){
										for(Long idCorporativeProcess : objTransitBalances.getErrorProcess()){
											String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_TRANSIT_BALANCE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
											if(!lstPreliminaryNotification.containsKey(id_error)){						
												lstPreliminaryNotification.put(id_error, idCorporativeProcess);
											}
										}
										notificationPreliminaryNotValid(objTransitBalances.getErrorDescription());	
										lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objTransitBalances.getErrorProcess());
									}	
									if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
//										RecordValidationType objEmisorConfirm = corporateProcessService.verifyEmisorConfirm(lstCorporativeEventsExecutePreliminary,true);
//										if(Validations.validateIsNotNull(objEmisorConfirm)){
//											for(Long idCorporativeProcess : objEmisorConfirm.getErrorProcess()){
//												String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_EMISOR_CONFIRM+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
//												if(!lstPreliminaryNotification.containsKey(id_error)){						
//													lstPreliminaryNotification.put(id_error, idCorporativeProcess);
//												}
//											}
//											notificationPreliminaryNotValid(objEmisorConfirm.getErrorDescription());	
//											lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objEmisorConfirm.getErrorProcess());
//										}	
										if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
											RecordValidationType objHolderMovementsPreliminar = corporateProcessService.verifyHolderMovementsPreliminar(lstCorporativeEventsExecutePreliminary,true);
											if(Validations.validateIsNotNull(objHolderMovementsPreliminar)){
												for(Long idCorporativeProcess : objHolderMovementsPreliminar.getErrorProcess()){
													String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_HOLDER_MOVEMENT+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
													if(!lstPreliminaryNotification.containsKey(id_error)){						
														lstPreliminaryNotification.put(id_error, idCorporativeProcess);
													}
												}
												notificationPreliminaryNotValid(objHolderMovementsPreliminar.getErrorDescription());	
												lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objHolderMovementsPreliminar.getErrorProcess());
											}	
											if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
												RecordValidationType objExchangeRate = corporateProcessService.verifyExchangeRate(lstCorporativeEventsExecutePreliminary,true);
												if(Validations.validateIsNotNull(objExchangeRate)){
													for(Long idCorporativeProcess : objExchangeRate.getErrorProcess()){
														String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_EXCHANGE_RATE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
														if(!lstPreliminaryNotification.containsKey(id_error)){						
															lstPreliminaryNotification.put(id_error, idCorporativeProcess);
														}
													}
													notificationPreliminaryNotValid(objExchangeRate.getErrorDescription());	
													lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objExchangeRate.getErrorProcess());
												}	
												if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
													RecordValidationType objCorporateCreatedRetirementOperation = corporateProcessService.verifyCorporateCreatedRetirementOperation(lstCorporativeEventsExecutePreliminary,true);
													if(Validations.validateIsNotNull(objCorporateCreatedRetirementOperation)){
														for(Long idCorporativeProcess : objCorporateCreatedRetirementOperation.getErrorProcess()){
															String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_RETIREMENT_OPERATION+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
															if(!lstPreliminaryNotification.containsKey(id_error)){						
																lstPreliminaryNotification.put(id_error, idCorporativeProcess);
															}
														}
														notificationPreliminaryNotValid(objCorporateCreatedRetirementOperation.getErrorDescription());	
														lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objCorporateCreatedRetirementOperation.getErrorProcess());
													}	
													if(lstCorporativeEventsExecutePreliminary!=null && lstCorporativeEventsExecutePreliminary.size()>0){
														RecordValidationType objAccreditationBalance= null;
														//RecordValidationType objAccreditationBalance= corporateProcessService.verifyAccreditationBalance(lstCorporativeEventsExecutePreliminary,true);
														if(Validations.validateIsNotNull(objAccreditationBalance)){
															for(Long idCorporativeProcess : objAccreditationBalance.getErrorProcess()){
																String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_ACCREDITATION_BALANCE+GeneralConstants.UNDERLINE_SEPARATOR+idCorporativeProcess;
																if(!lstPreliminaryNotification.containsKey(id_error)){						
																	lstPreliminaryNotification.put(id_error, idCorporativeProcess);
																}
															}
															notificationPreliminaryNotValid(objAccreditationBalance.getErrorDescription());	
															lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(lstCorporativeEventsExecutePreliminary,objAccreditationBalance.getErrorProcess());
														}
													}
												}												
											}											
										}
									}									
								}
							}	
						}
					}
				}				
			}					
		}catch (ServiceException e) {
			log.error(e.getMessage()+"linea 559 \n");
			throw new RuntimeException(e);
		}
		return lstCorporativeEventsExecutePreliminary;
	}
	
	/**
	 * Metodo que retorna lista de procesos corporativos no validados.
	 *
	 * @param lstCorporativeEventsExecutePreliminary List Corporative Events Execute Preliminary
	 * @param lstErrorProcess  List Error Process
	 * @return List Corporative Events Execute Preliminary
	 */
	public List<CorporativeOperation> getListCorporativeEventsExecutePreliminary(List<CorporativeOperation> lstCorporativeEventsExecutePreliminary,List<Long> lstErrorProcess){
		List<CorporativeOperation> lstCorporativeOperation = new ArrayList<CorporativeOperation>();
		for(CorporativeOperation objCorporativeOperation : lstCorporativeEventsExecutePreliminary){
			if(!lstErrorProcess.contains(objCorporativeOperation.getIdCorporativeOperationPk())){
				lstCorporativeOperation.add(objCorporativeOperation);
			}
		}
		return lstCorporativeOperation;
	}
	
	/**
	 *  Metodo que ejecuta el stock del valor en forma automatica.
	 *
	 * @param idSecurity id security
	 * @return validate Stock Fail
	 * @throws ServiceException the Service Exception
	 */
	public boolean executeStock(String idSecurity)throws ServiceException{
		boolean validateStockFail = false;
		Boolean flagStock = (Boolean)flagProcessStock.get(idSecurity);
		//verificamos si el stock fue enviado
		if(flagStock.equals(Boolean.FALSE)){
			flagProcessStock.put(idSecurity, Boolean.TRUE);
			log.error("<<<<<<<<<<<<<<<<< VALOR NO TIENE STOCK - EJECUTAR CALCULO DE STOCK: "+idSecurity 
					+ "  >>>>>>>>>>>>>>>>>>>>>>>>>"+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			try{
				//Si Falla un calculo de Stock, los demas continuan
				processStockCalculation(idSecurity, userId);
			}catch(ServiceException se){
				if( ErrorServiceType.STOCK_CALCULATION_PROCESS_EXIST.equals(se.getErrorService())){
					log.error("<<<<<<<<<<<<<<<<< ERROR:  EL VALOR TIENE STOCK REGISTRADO: "+idSecurity );
					return validateStockFail;
				}
			}
			
			
		}else{
			//verificaremos si el stock sigue en proceso. En caso que no existe proceso se setea estado de preliminar enviados por valor 
			//obtenemos los stock por Valor ejecutados en la fecha actual para saber cuales procesos siguen en proceso
			LinkedHashMap lstSecurityStockProcess = corporateServiceFacade.getListSecuritiesStockByRecordDate(CommonsUtilities.currentDate(), StockStateType.PROCESSING.getCode());
			if(Validations.validateIsNull(lstSecurityStockProcess) || (Validations.validateIsNotNull(lstSecurityStockProcess) && lstSecurityStockProcess.isEmpty())){
				log.error("STOCK FUE ENVIADO PERO NO SE REGISTRO EL PROCESO: "+idSecurity+"\n");
				flagSecurityProcessPreliminary.put(idSecurity, Boolean.TRUE);
				idSecurityEndPreliminary.put(idSecurity,idSecurity);
				validateStockFail = true;
				//enviamos notificacion de stock fallido
				if(!lstStockFailNotification.containsKey(idSecurity)){
					notificationStockFail(idSecurity);
					lstStockFailNotification.put(idSecurity, idSecurity);
				}
			}else if(lstSecurityStockProcess.containsKey(idSecurity)){
				log.error("STOCK FUE ENVIADO PERO SIGUE EN PROCESO: "+idSecurity+"\n");
				//verificamos el tiempo de espera del stock enviado del valor
				if(lstTimeStockCalculationProcess.containsKey(idSecurity)){
					Date initialTime = (Date)lstTimeStockCalculationProcess.get(idSecurity);
					Date time = CommonsUtilities.currentDateTime();
					int min= (int)((double)(time.getTime() - initialTime.getTime())/(double)(CorporateProcessConstants.MILLSECS_IN_MIN));
					if(min<=waitTimeProcess.intValue());
					else{
						log.error("EXPIRO EL TIEMPO DE ESPERA DEL PROCESO DE STOCK: "+idSecurity+"\n");
						flagSecurityProcessPreliminary.put(idSecurity, Boolean.TRUE);			
						idSecurityEndPreliminary.put(idSecurity,idSecurity);
						validateStockFail = true;
						//enviamos notificacion de stock fallido
						if(!lstStockFailNotification.containsKey(idSecurity)){
							notificationStockFail(idSecurity);
							lstStockFailNotification.put(idSecurity, idSecurity);
						}
					}
				}else{
					lstTimeStockCalculationProcess.put(idSecurity, CommonsUtilities.currentDateTime());
				}
			}
		}
		return validateStockFail;
	}
	
	public Long createStockCalculationProcess(String idSecurity) throws ServiceException{
		CorporativeOperation corporativeOp=mapCorporativeOperation.get(idSecurity);
		Date registryDate=corporativeOp.getRegistryDate();
		Date cutoffDate=corporativeOp.getCutoffDate();
		stockCalculationProcess = new StockCalculationProcess();
		stockCalculationProcess.setRegistryUser(userId);
		stockCalculationProcess.setRegistryDate(registryDate);
		stockCalculationProcess.setCutoffDate(cutoffDate);
		stockCalculationProcess.setIndAutomactic(BooleanType.YES.getCode());
		stockCalculationProcess.setHolder(new Holder());
		Security security = new Security();
		security.setIdSecurityCodePk(idSecurity);
		stockCalculationProcess.setSecurity(security);
		stockCalculationProcess.setParticipant(new Participant());
		stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
		stockCalculationProcess.setStockType(StockType.CORPORATIVE.getCode());
		
		return manageStockCalculationFacade.registerOnlyStockCalculation(stockCalculationProcess);
	}
	
	/**
	 * Metodo que notifica mensaje de stock no valido.
	 *
	 * @param idSecurity is security
	 */
	public void notificationStockFail(String idSecurity){
		String strMessage = "Error en el proceso de stock para el valor: " + idSecurity+". Verifique";	
		BusinessProcess businessProcessNotification = new BusinessProcess();					
		businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
		UserFilterTO  userFilter=new UserFilterTO();
		userFilter.setUserName(userId);
		List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);
		notificationServiceFacade.sendNotificationManual(userId, businessProcessNotification, lstUserAccount,
				GeneralConstants.NOTIFICATION_DEFAULT_HEADER, strMessage, NotificationType.EMAIL);
	}
	
	/**
	 * Metodo que notifica mensaje de stock no valido.
	 *
	 * @param idSecurity is security
	 */
	public void notificationStockNotValid(String idSecurity){
		String strMessage = "Error en el proceso de stock con inconcistencia para el valor: " + idSecurity+". Verifique";	
		BusinessProcess businessProcessNotification = new BusinessProcess();					
		businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
		UserFilterTO  userFilter=new UserFilterTO();
		userFilter.setUserName(userId);
		List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);
		notificationServiceFacade.sendNotificationManual(userId, businessProcessNotification, lstUserAccount,
				GeneralConstants.NOTIFICATION_DEFAULT_HEADER, strMessage, NotificationType.EMAIL);
	}
	
	/**
	 * Metodo que notifica mensaje de preliminar no valido.
	 *
	 * @param strMessage  Message
	 */
	public void notificationPreliminaryNotValid(String strMessage){
		BusinessProcess businessProcessNotification = new BusinessProcess();					
		businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.MANAGER_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
		UserFilterTO  userFilter=new UserFilterTO();
		userFilter.setUserName(userId);
		List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);
		notificationServiceFacade.sendNotificationManual(userId, businessProcessNotification, lstUserAccount,
				GeneralConstants.NOTIFICATION_DEFAULT_HEADER, strMessage, NotificationType.EMAIL);
	}
	
	/**
	 * Metodo que ejecuta el stock.
	 *
	 * @param idSecurity the id securty
	 * @param dateProcessCorporate the date process corporate
	 * @param strIdUser user demons
	 * @throws ServiceException the base exception
	 */
	@LoggerAuditWeb
	@ProcessAuditLogger(process=BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC)
	public synchronized void processStockCalculation(String idSecurity, String strIdUser)throws ServiceException{
		try{
			CorporativeOperation corporativeOp=mapCorporativeOperation.get(idSecurity);
			Date registryDate=corporativeOp.getRegistryDate();
			Date cutoffDate=corporativeOp.getCutoffDate();
			stockCalculationProcess = new StockCalculationProcess();
			stockCalculationProcess.setRegistryUser(strIdUser);
			stockCalculationProcess.setRegistryDate(registryDate);
			stockCalculationProcess.setCutoffDate(cutoffDate);
			stockCalculationProcess.setIndAutomactic(BooleanType.YES.getCode());
			stockCalculationProcess.setHolder(new Holder());
			Security security = new Security();
			security.setIdSecurityCodePk(idSecurity);
			stockCalculationProcess.setSecurity(security);
			stockCalculationProcess.setParticipant(new Participant());
			stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
			stockCalculationProcess.setStockType(StockType.CORPORATIVE.getCode());
			log.info("Calculo de Stock al Valor: "+ idSecurity +" - Fecha Registro: "+registryDate+" - Fecha de Corte: "+cutoffDate+ "");
			Long stockId = manageStockCalculationFacade.registerStockCalculation(stockCalculationProcess);
			log.info("Valor: "+ idSecurity +" - Stock: "+stockId);
		}catch (ServiceException e) {
			if( ErrorServiceType.STOCK_CALCULATION_PROCESS_EXIST.equals(e.getErrorService())){
				log.error(e.getMessage()+" linnea 701 \n");
				log.error("Existe un calculo de STOCK para valor :"+idSecurity);
				throw new ServiceException(ErrorServiceType.STOCK_CALCULATION_PROCESS_EXIST);
			}
			log.error(e.getMessage()+" linnea 701 \n");
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Metodo que verifica el estado del proceso corporativo y envia notificaciones del estado respectivo.
	 *
	 * @param lstSecurity the lst security
	 * @param idSecurityEndPreliminary the id security end preliminary
	 */
	public void setStateEndPreliminaryAndNotification(LinkedHashMap<String, String> lstSecurity,LinkedHashMap<String, String> idSecurityEndPreliminary){
		if(flagMensaje==false){
			log.error("<<<<<<<<<<<<<<<<< PROCESANDO NOTIFICACIONES DE PRELIMINARES (FALLIDO - TERMINADO) >>>>>>>>>>>>>>>>>>>>>>>>>"
		+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			flagMensaje=true;
		}		
		//recorremos lista de todos los valores procesados
		for (Map.Entry<String, String> objSecurity : lstSecurity.entrySet()){
			//obtenemos codigo del valor
			String idSecurity = objSecurity.getKey();	
			//obtenemos lista de procesos corporativos asosciados al valor
			List<CorporativeOperation> lstCorporativeEventsExecutePreliminary = getListCorporativeEventsExecutePreliminary(idSecurity,lstCorporativeEventsAll);
			//recorremos lista de procesos corporativos asosciados al valor
			for(CorporativeOperation objCorporativeOperation : lstCorporativeEventsExecutePreliminary){
				//verifica si operacion tiene preliminar para no volver a mandar la notificacion
				Boolean flagPreliminay= (Boolean)flagEndPreliminary.get(objCorporativeOperation.getIdCorporativeOperationPk());
				if(flagPreliminay.equals(Boolean.FALSE)){
					//verificamos si existe stock fallido o preliminar fallido para no volver a mandar la notificacion
					boolean flagStockOrPreliminaryFail=false;
					for (Map.Entry<String, String> idSecurityEndPreliminaryAux : idSecurityEndPreliminary.entrySet())
					{
						String codigoValor = idSecurityEndPreliminaryAux.getKey();
						if(codigoValor.equals(objCorporativeOperation.getSecurities().getIdSecurityCodePk()))
						{
							flagEndPreliminary.put(objCorporativeOperation.getIdCorporativeOperationPk(), Boolean.TRUE);
							flagStockOrPreliminaryFail=true;
						}											
					}
					//enviamos notificaciones respectivas (preliminar terminado - preliminar fallido)
					if(!flagStockOrPreliminaryFail){
						CorporativeOperation corporativeOperationAux = corporateServiceFacade.findCorporativeOperation(objCorporativeOperation.getIdCorporativeOperationPk());
						if(corporativeOperationAux.getState().equals(CorporateProcessStateType.PRELIMINARY.getCode())){
							log.error("PRELIMINAR TERMINADO"+corporativeOperationAux.getIdCorporativeOperationPk()+"\n");		
							flagEndPreliminary.put(objCorporativeOperation.getIdCorporativeOperationPk(), Boolean.TRUE);									
							//enviamos notificacion de preliminar fallido
							String id_error = CorporateProcessConstants.MSG_PRELIMINARY_END+GeneralConstants.UNDERLINE_SEPARATOR+objCorporativeOperation.getIdCorporativeOperationPk();
							if(!lstPreliminaryNotification.containsKey(id_error)){						
								lstPreliminaryNotification.put(id_error, objCorporativeOperation.getIdCorporativeOperationPk());
								String strMessage = "Preliminar terminado para el proceso: " + objCorporativeOperation.getIdCorporativeOperationPk()+". Verifique";
								/**Borrar las Notificaciones a Pedido de CCACERES*/
//								notificationPreliminaryNotValid(strMessage);
							}	
						}else if(corporativeOperationAux.getState().equals(CorporateProcessStateType.PRELIMINARY_FAIL.getCode()) || 
								corporativeOperationAux.getState().equals(CorporateProcessStateType.REGISTERED.getCode())){
							log.error("PRELIMINAR FALLIDO: "+corporativeOperationAux.getIdCorporativeOperationPk()+"\n");		
							flagEndPreliminary.put(objCorporativeOperation.getIdCorporativeOperationPk(), Boolean.TRUE);									
							//enviamos notificacion de preliminar fallido
							String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_FAIL+GeneralConstants.UNDERLINE_SEPARATOR+objCorporativeOperation.getIdCorporativeOperationPk();
							if(!lstPreliminaryNotification.containsKey(id_error)){						
								lstPreliminaryNotification.put(id_error, objCorporativeOperation.getIdCorporativeOperationPk());
								String strMessage = "Error en el proceso de preliminar para el proceso: " + objCorporativeOperation.getIdCorporativeOperationPk()+". Verifique";	
								notificationPreliminaryNotValid(strMessage);
							}		
						}else if(corporativeOperationAux.getState().equals(CorporateProcessStateType.PRELIMINARY_IN_PROCESS.getCode())){
							//verificamos el tiempo de espera del preliminar enviado del valor
							if(lstTimePreliminaryProcess.containsKey(corporativeOperationAux.getIdCorporativeOperationPk())){
								Date initialTime = (Date)lstTimePreliminaryProcess.get(corporativeOperationAux.getIdCorporativeOperationPk());
								Date time = CommonsUtilities.currentDateTime();
								int min= (int)((double)(time.getTime() - initialTime.getTime())/(double)(CorporateProcessConstants.MILLSECS_IN_MIN));
								if(min<=waitTimeProcess.intValue());
								else{
									log.error("EXPIRO EL TIEMPO DE ESPERA DEL PRELIMINAR: "+corporativeOperationAux.getIdCorporativeOperationPk()+"\n");		
									flagEndPreliminary.put(objCorporativeOperation.getIdCorporativeOperationPk(), Boolean.TRUE);									
									//enviamos notificacion de preliminar fallido
									String id_error = CorporateProcessConstants.ERROR_PRELIMINARY_FAIL_FOR_TIME+GeneralConstants.UNDERLINE_SEPARATOR+objCorporativeOperation.getIdCorporativeOperationPk();
									if(!lstPreliminaryNotification.containsKey(id_error)){						
										lstPreliminaryNotification.put(id_error, objCorporativeOperation.getIdCorporativeOperationPk());
										String strMessage = "Expiro el tiempo de espera del preliminar para el proceso: " + objCorporativeOperation.getIdCorporativeOperationPk()+". Verifique";	
										notificationPreliminaryNotValid(strMessage);
									}									
								}
							}else{
								lstTimePreliminaryProcess.put(corporativeOperationAux.getIdCorporativeOperationPk(), CommonsUtilities.currentDateTime());
							}
						}
					}					
				}
			}
		}
	}
	
	/**
	 * Metodo que ordena lista de valores por cantidad de movimientos
	 * @param lstSecurity List security
	 * @return List order Security
	 * @throws ServiceException the base exception
	 */
	public LinkedHashMap orderSecurityForMovements(LinkedHashMap<String, String> lstSecurity)throws ServiceException{	
		LinkedHashMap<String,Long> quantityMovementsForSecurity = new LinkedHashMap<String,Long>();	
		LinkedHashMap lstSecurityAux=null;
		try{
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<01>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>		
			
			//recorremos la lista de valores y obtenemos la cantidad de movimientos				
			log.info("<<<<<<<<<<<<<<<<< INICIO VALORES - CALCULO DE STOCK: >>>>>>>>>>>>>>>>>>>>>>>>>"
					+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			for (Map.Entry<String, String> objSecurity : lstSecurity.entrySet()){				
				//obtenemos codigo del valor
				String idSecurity = objSecurity.getKey();		
				log.info("<<<<<<<<<<<<<<<<< VALOR: "+ idSecurity + "  >>>>>>>>>>>>>>>>>>>>>>>>>\n");		
				//llenamos cantidad de movimientos de cada valor
				quantityMovementsForSecurity.put(idSecurity, corporateServiceFacade.getQuantityMovementsForSecurity(idSecurity));
				//llena lista todos los valores no tienen stock en proceso					
				flagProcessStock.put(idSecurity, Boolean.FALSE);	
				//llena lista todas los valores no tienen preliminar en proceso	
				flagSecurityProcessPreliminary.put(idSecurity, Boolean.FALSE);							
			}
			log.info("<<<<<<<<<<<<<<<<< FIN VALORES - CALCULO DE STOCK: >>>>>>>>>>>>>>>>>>>>>>>>>"
					+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<02>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//reordena (mayor a menor-burbuja) los procesos corporativos de acuerdo a la cantidad de movimientos del valor 	
			String auxIdSecurity=null;
			Long movimentos=null;
			for (Map.Entry<String, Long> objQuantityMovements : quantityMovementsForSecurity.entrySet()){
				String id = objQuantityMovements.getKey();
				for (Map.Entry<String, Long> objQuantityMovementsAux : quantityMovementsForSecurity.entrySet()){
					String idAux = objQuantityMovementsAux.getKey();
					if(!id.equals(idAux)){
						if((Long)quantityMovementsForSecurity.get(id)<(Long)quantityMovementsForSecurity.get(idAux)){
							//se cambia los valores de la cantidad de los movimientos
							movimentos=(Long)quantityMovementsForSecurity.get(id);
							quantityMovementsForSecurity.put(id, (Long)quantityMovementsForSecurity.get(idAux));
							quantityMovementsForSecurity.put(idAux, movimentos);
							
							//se cambia los objetos de hechos de importancia por la cantidad de movimientos
							auxIdSecurity=(String)lstSecurity.get(id);
							lstSecurity.put(id, (String)lstSecurity.get(idAux));
							lstSecurity.put(idAux, auxIdSecurity);
						}
					}
				}
			}
			lstSecurityAux=lstSecurity;
		}catch (ServiceException e) {
			log.error(e.getMessage()+"linea 851 \n");
			throw new RuntimeException(e);
		}
		return lstSecurityAux;
	}
	
	/**
	 * Metodo que crea objetos de ayuda para procesar el preliminar automatico.
	 *
	 * @throws ServiceException the service exception
	 */
	public void createObject()throws ServiceException{
		try{
			//solo una vez al dia para crear objetos una vez al dia
			if(flagCreateArrayEndPreliminary.equals(Boolean.FALSE))
			{
				lstSecurity = new LinkedHashMap();//se crea lista valores que se ejecutara stock (solo una vez en el dia)	
				flagProcessStock = new LinkedHashMap();//se crea lista estado de stock en proceso (enviados)	
				flagSecurityProcessPreliminary = new LinkedHashMap();//se crea lista estado de preliminares en proceso (enviados)				
				quantityBlockStock=0;//se inicializa en cero cantidad de bloques de stock (solo una vez al dia)				
				lstTimeStockCalculationProcess = new LinkedHashMap();//se crea lista de tiempo de inicio de proceso enviado
				lstTimePreliminaryProcess = new LinkedHashMap();//se crea lista de tiempo de inicio de proceso enviado
				lstStockFailNotification = new LinkedHashMap();//se crea lista de notificaciones fallidas de stock
				lstPreliminaryNotification = new LinkedHashMap();//se crea lista de notificacion de preliminares finalizadas (Correctas - Fallidas)
				
				idSecurityEndPreliminary= new LinkedHashMap();//se crea lista que verifica valores que no tienen stock o stock fallido - ayuda para no enviar notificaciones de preliminares
				flagEndPreliminary= new LinkedHashMap();//se crea lista estado de preliminares terminados
				lstCorporativeEventsAll=loadProcessCoporativeAll();//se crea lista de todos los hechos de importancia como ayuda para verificar la ejecucion de los preliminares y envio de notificaciones
				flagCreateArrayEndPreliminary=Boolean.TRUE;
			}
		}catch (ServiceException e) {
			log.error(e.getMessage()+" linea 879\n");
			throw new RuntimeException(e);
		}		
	}
	
	/**
	 * Metodo que crea la lista de valores que se ejecutara los definitivos de sus procesos corporativos .
	 *
	 * @throws ServiceException The Service Exception
	 */
	public void loadListSecurityExecution()throws ServiceException{
		
		//obtenemos fecha de Vencimiento (Fecha actual parametizable + 1)
		dateProcessCorporate = CommonsUtilities.addDate(currentDate, 1);
		
		//obtenemos lista de los beneficios a procesar
		List<CorporativeOperation> lstObjCorporative= loadProcessCorporativeBenefit(dateProcessCorporate);
		
		/** Llenar un Mapa de Corporativos*/
		mapCorporativeOperation=fillCorporativeOperation(lstObjCorporative);
		
//		validamos lista de beneficios a procesar
		if(lstObjCorporative!=null){
			for(CorporativeOperation objCorp : lstObjCorporative){
				//se llena lista de valores a procesar
				if(!lstSecurity.containsKey(objCorp.getSecurities().getIdSecurityCodePk())){
					lstSecurity.put(objCorp.getSecurities().getIdSecurityCodePk(),objCorp.getSecurities().getIdSecurityCodePk());
				}
			}
		}
		
		//reordena los procesos corporativos por valor que tenga mas movimientos(eliminando los repetidos)
		if (Validations.validateIsNotNull(lstSecurity) && !lstSecurity.isEmpty()){					
			log.info("<<<<<<<<<<<<<<<<< PRIMERA FOTO DE TOTAL DE HECHOS DE IMPORTANCIA AL EJECUTAR PROCESO EL DEMONIO: "+
					+lstCorporativeEventsAll.size()+"  >>>>>>>>>>>>>>>>>>>>>>>>>"+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
			lstSecurity=orderSecurityForMovements(lstSecurity);
			log.info("<<<<<<<<<<<<<<<<< PRIMERA FOTO DE TOTAL DE VALORES AL EJECUTAR STOCK EL DEMONIO: "+lstSecurity.size()+
					"  >>>>>>>>>>>>>>>>>>>>>>>>>"+"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
		}
	}
	
	/**
	 * Metodo que verifica si termino de procesar stock de todos los bloques correctamente.
	 *
	 * @return validate end Corporate Process Stock
	 */
	public boolean corporateProcessStock(){
		if(flagProcessStock.containsValue(Boolean.FALSE)){
			return false;
		}			
		return true;
	}
	
	/**
	 *  Metodo que valida envio de preliminares de los procesos corporativos.
	 *
	 * @return Validate Block Preliminary
	 */
	public boolean validateBlockPreliminary()
	{
		if(flagSecurityProcessPreliminary.containsValue(Boolean.FALSE)){
			return false;
		}	
		return true;
	}
	
	/**
	 *Metodo que verifica si termino de procesar preliminares de todos los bloques correctamente.
	 *
	 * @return validate end Corporate Process Preliminary
	 * @throws ServiceException the service exception
	 */
	public boolean endCorporateProcessPreliminary()throws ServiceException{
		boolean flag = true;
		//creamos objetos solo una vez al dia
		createObject();
		
		if(flagEndPreliminary.containsValue(Boolean.FALSE)){
			flag = false;
		}			
		
		if(flag==true){
			log.error("<<<<<<<<<<<<<<<<< SE TERMINO DE EJECUTAR EL PROCESO DEMONIO: >>>>>>>>>>>>>>>>>>>>>>>>>"+
					"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
		}			
		
		return flag;
	}
	
	/**
	 * Metodo que lista todos los procesos corporativos de beneficios
	 *
	 * @return List Process Coporative All
	 * @throws ServiceException the service exception
	 */ 
	public LinkedHashMap loadProcessCoporativeAll()throws ServiceException
	{
		//se crea lista de hechos de importancia (solo una vez en el dia)
		LinkedHashMap lstProcessCoporativeAll = null;
		Date dateProcessCorporate=null;		
		try
		{
			lstProcessCoporativeAll = new LinkedHashMap();			
			//obtenemos fecha de Vencimiento del cupon (Fecha actual parametizable + 1 )	
			dateProcessCorporate = CommonsUtilities.addDate(currentDate, 1);
			
			//obtenemos lista de los beneficios a procesar
			List<CorporativeOperation> lstObjCorporative = loadProcessCorporativeBenefit(dateProcessCorporate);
			
			//validamos lista de beneficios a procesar
			if(lstObjCorporative!=null){				
				for(CorporativeOperation objCorp : lstObjCorporative){
					//se llena lista Map de todos los hechos de importancia a procesar
					lstProcessCoporativeAll.put(objCorp.getIdCorporativeOperationPk(), objCorp);	
					//se llena lista estado de preliminares terminados
					flagEndPreliminary.put(objCorp.getIdCorporativeOperationPk(), Boolean.FALSE);
				}
			}			
		}catch (ServiceException e) {
			log.error(e.getMessage()+" linea 988\n");
			throw new RuntimeException(e);
		}
		
		return lstProcessCoporativeAll;
	}
	
	/**
	 * Metodo que retorna una lista de procesos corporativos de beneficios en estado preliminar.
	 *
	 * @param dateProcessCorporate Date Process Corporate
	 * @return List Process Corporative Benefit
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> loadProcessCorporativeBenefit(Date dateProcessCorporate)throws ServiceException{
		List<CorporativeOperation> lstObjCorporative = null;
		CorporateMonitoringTO corporateMonitoringFilter;
		List<Integer> stateProcessCorporative = new ArrayList<Integer>();
		List<Integer> eventProcessCorporative = new ArrayList<Integer>();
		//se verifica si hay hechos de importancia de renta fija por ejecutarse y en estado registrado
		corporateMonitoringFilter = new CorporateMonitoringTO();
		//Los preliminares para hoy 
		List<Date> listDate=new ArrayList<>();
		listDate.add(dateProcessCorporate);
		listDate.add(CommonsUtilities.currentDate());
		//listDate.add(CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1));
		corporateMonitoringFilter.setListExpirationDate(listDate);
		stateProcessCorporative.add(CorporateProcessStateType.REGISTERED.getCode());
		if(flagReprocess.equals(Boolean.FALSE)){
			stateProcessCorporative.add(CorporateProcessStateType.PRELIMINARY.getCode());
		}
		stateProcessCorporative.add(CorporateProcessStateType.PRELIMINARY_FAIL.getCode());
		corporateMonitoringFilter.setLstState(stateProcessCorporative);
		eventProcessCorporative.add(ImportanceEventType.ACTION_DIVIDENDS.getCode());
		eventProcessCorporative.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		eventProcessCorporative.add(ImportanceEventType.CAPITAL_REDUCTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
		eventProcessCorporative.add(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		eventProcessCorporative.add(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.REMANENT_PAYMENT.getCode());
		eventProcessCorporative.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
		corporateMonitoringFilter.setLstCorporativeEvent(eventProcessCorporative);
		try {
			lstObjCorporative= corporateServiceFacade.searchCorporativeOperationsByCoupons(corporateMonitoringFilter);
		} catch (ServiceException e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
		return lstObjCorporative;
	}
	
	/**
	 * Metodo que retorna una lista de procesos corporativos de beneficios en estado preliminar.
	 *
	 * @param dateProcessCorporate Date Process Corporate
	 * @return List Process Corporative Benefit
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> loadProcessCorporativeBySecurity(Date dateProcessCorporate,String idSecurityCodePk)throws ServiceException{
		List<CorporativeOperation> lstObjCorporative = null;
		CorporateMonitoringTO corporateMonitoringFilter;
		List<Integer> stateProcessCorporative = new ArrayList<Integer>();
		List<Integer> eventProcessCorporative = new ArrayList<Integer>();
		//se verifica si hay hechos de importancia de renta fija por ejecutarse y en estado registrado
		corporateMonitoringFilter = new CorporateMonitoringTO();
		corporateMonitoringFilter.setSecurities(new Security(idSecurityCodePk));
		//Los preliminares para hoy 
		List<Date> listDate=new ArrayList<>();
		listDate.add(dateProcessCorporate);
		listDate.add(CommonsUtilities.currentDate());
		//listDate.add(CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1));
		corporateMonitoringFilter.setListExpirationDate(listDate);
		stateProcessCorporative.add(CorporateProcessStateType.REGISTERED.getCode());
		stateProcessCorporative.add(CorporateProcessStateType.PRELIMINARY_FAIL.getCode());
		corporateMonitoringFilter.setLstState(stateProcessCorporative);
		eventProcessCorporative.add(ImportanceEventType.ACTION_DIVIDENDS.getCode());
		eventProcessCorporative.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		eventProcessCorporative.add(ImportanceEventType.CAPITAL_REDUCTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
		eventProcessCorporative.add(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		eventProcessCorporative.add(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode());
		eventProcessCorporative.add(ImportanceEventType.REMANENT_PAYMENT.getCode());
		eventProcessCorporative.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
		corporateMonitoringFilter.setLstCorporativeEvent(eventProcessCorporative);
		try {
			lstObjCorporative= corporateServiceFacade.searchCorporativeOperationsByCoupons(corporateMonitoringFilter);
		} catch (ServiceException e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
		return lstObjCorporative;
	}
	
	/***
	 * Llenar un Map con los CorporativeOperation, por cada valor
	 * @param lstObjCorporative
	 * @return
	 */
	public Map<String,CorporativeOperation> fillCorporativeOperation(List<CorporativeOperation> lstObjCorporative){
		Map<String,CorporativeOperation> mapCorporativeOp=new HashMap<>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstObjCorporative)){
			for (CorporativeOperation corporativeOperation : lstObjCorporative) {
				log.info("<<<<<<<<<<<<<<<<< BUSCO CORPORATIVOS/VALORES PARA EL STOCK/PREELIMINAR =>  IdCorporativeOperationPk: " + corporativeOperation.getIdCorporativeOperationPk() + " - IdSecurityCodePk: " + corporativeOperation.getSecurities().getIdSecurityCodePk());
				String idSecurityCodePk=corporativeOperation.getSecurities().getIdSecurityCodePk();
				String key=idSecurityCodePk;
				mapCorporativeOp.put(key, corporativeOperation);
			}
		}
		
		return mapCorporativeOp;
	}
	/**
	 * Get Limit Block.
	 *
	 * @return Limit Block
	 */
	public Integer getLimitBlock() {
		return limitBlock;
	}
	
	/**
	 * Set Limit Block.
	 *
	 * @param limitBlock Limit Block
	 */
	public void setLimitBlock(Integer limitBlock) {
		this.limitBlock = limitBlock;
	}
	
	/**
	 * Get Wait Time.
	 *
	 * @return Wait Time
	 */
	public Integer getWaitTime() {
		return waitTime;
	}
	
	/**
	 * Set Wait Time.
	 *
	 * @param waitTime Wait Time
	 */
	public void setWaitTime(Integer waitTime) {
		this.waitTime = waitTime;
	}
	
	/**
	 * Get Wait Time Process.
	 *
	 * @return Wait Time Process
	 */
	public Integer getWaitTimeProcess() {
		return waitTimeProcess;
	}
	
	/**
	 * Set Wait Time Process.
	 *
	 * @param waitTimeProcess Wait Time Process
	 */
	public void setWaitTimeProcess(Integer waitTimeProcess) {
		this.waitTimeProcess = waitTimeProcess;
	}
	
	/**
	 * Get User Id.
	 *
	 * @return User Id
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Set User Id.
	 *
	 * @param userId User Id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Get  Current Date.
	 *
	 * @return Current Date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}
	
	/**
	 * Set Current Date.
	 *
	 * @param currentDate Current Date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	
	/**
	 * @return the mapCorporativeOperation
	 */
	public Map<String, CorporativeOperation> getMapCorporativeOperation() {
		return mapCorporativeOperation;
	}

	/**
	 * @param mapCorporativeOperation the mapCorporativeOperation to set
	 */
	public void setMapCorporativeOperation(
			Map<String, CorporativeOperation> mapCorporativeOperation) {
		this.mapCorporativeOperation = mapCorporativeOperation;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}


	
}
