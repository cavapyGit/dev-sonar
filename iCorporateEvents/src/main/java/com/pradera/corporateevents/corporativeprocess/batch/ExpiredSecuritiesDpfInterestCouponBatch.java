package com.pradera.corporateevents.corporativeprocess.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.model.process.ProcessLogger;
// TODO: Auto-generated Javadoc

/**
 * Batch que vence los cupones de interes de valores DPF y DPA.
 *
 * @author PraderaTechnologies
 */
@BatchProcess(name="ExpiredSecuritiesDpfInterestCouponBatch")
public class ExpiredSecuritiesDpfInterestCouponBatch implements Serializable, JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The corporate process facade. */
	@EJB 
	CorporateProcessServiceFacade corporateProcessFacade;
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/**
	 * Inicio de Bachero que redime los cupones de interes de valores DPF.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		logger.info("INICIO ::::::: ExpiredSecuritiesDpfInterestCouponBatch ");
		// TODO Auto-generated method stub
		try {			
			//Date currentDate = CommonsUtilities.currentDate();
			String currentDate = CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate());
			
			corporateProcessFacade.automaticExpiredSecuritiesDpfInterestCoupon(currentDate);			
		} catch (Exception e) {		
			e.printStackTrace();
		}
		logger.info("FIN ::::::: ExpiredSecuritiesDpfInterestCouponBatch ");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
	
}
