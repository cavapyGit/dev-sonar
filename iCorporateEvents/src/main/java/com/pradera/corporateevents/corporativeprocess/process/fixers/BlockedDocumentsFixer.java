package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.affectation.type.RequestAffectationStateType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.blockoperation.UnblockMarketFactOperation;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.reversals.type.MotiveReverlsasType;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.operations.RequestCorrelativeType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockedDocumentsFixer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/06/2014
 */
@ApplicationScoped
public class BlockedDocumentsFixer implements DocumentsFixer, Serializable {
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new FixedBlockedDocuments object.
	 */
	public BlockedDocumentsFixer() {
		
	}
	
	/**
	 * This method is used fix the balances in the BLockCorporativeResults .
	 *
	 * @param corpProcessResult : CorporativeProcessResult table details
	 * @param blockedDocs : List of BlockStockCalculation details
	 * @param delFactor : delivery factor for calculating the balances
	 * @param round : Rounding factor for calculating the balances
	 * @param remnantFlag : if any remnant is available
	 * @param cashFlag : cash flag for the balances
	 * @param destiny : destiny for the balances
	 * @param balFixer : used to fix the balances
	 * @param processType the process type
	 * @param exchangeRateDay the exchange rate day
	 * @return List: List of BlockCorporativeProcessResults
	 */
	public List<BlockCorporativeResult> fixDocuments(CorporativeProcessResult corpProcessResult, List<Object[]> blockedDocs,
			BigDecimal delFactor, int round, boolean remnantFlag, boolean cashFlag, boolean destiny, BalanceFixer balFixer,
			ImportanceEventType processType,DailyExchangeRates exchangeRateDay) {
		List<BlockCorporativeResult> blockResults = new ArrayList<BlockCorporativeResult>();
		if(blockedDocs != null && blockedDocs.size() > 0){
			List<BlockCorporativeResult> DocsforProcess = new ArrayList<BlockCorporativeResult>();
			AffectationType tmpBlockType = null;
			BigDecimal totalBalance = new BigDecimal(0);
			BlockCorporativeResult blockCorpRes = null;
			
			for(Iterator<Object[]> it = blockedDocs.iterator(); it.hasNext();){
				Object[] doc = it.next();
				AffectationType blockType = AffectationType.get((Integer)doc[0]);
				if(tmpBlockType != null && !tmpBlockType.equals(blockType)){
					blockResults.addAll(fixDocuments(corpProcessResult, DocsforProcess, tmpBlockType, totalBalance, balFixer));
					totalBalance = new BigDecimal(0);
					DocsforProcess.clear();
				}
				doc[1]=doc[1]!=null?cashFlag?CorporateProcessUtils.multiply((BigDecimal)doc[1], CorporateProcessConstants.AMOUNT_ROUND):doc[1]:new BigDecimal(0);	
				blockCorpRes = fillBlockCorporativeResult(doc, corpProcessResult, delFactor, round, remnantFlag, destiny,exchangeRateDay);
				if(CorporateProcessUtils.hasReblock(processType) && LevelAffectationType.REBLOCK.getCode().equals(doc[10])){
					blockResults.add(blockCorpRes);
				}else{
					totalBalance = totalBalance.add(blockCorpRes.getBlockedBalance());
					DocsforProcess.add(blockCorpRes);
				}
				tmpBlockType = blockType;
				if(!it.hasNext()){
					blockResults.addAll(fixDocuments(corpProcessResult, DocsforProcess,tmpBlockType, totalBalance, balFixer));
					DocsforProcess.clear();
				}
			}
			
		}
		return blockResults;
	}
	
	
	/**
	 * This method is used update the balances in the CorporativeProcessResults and set those balances to the CorporativeProcessResult table.
	 *
	 * @param corpProcessResult : CorporativeProcessResult table details
	 * @param cashFlag : if it is true, then we have blocked balance, else we don't have blocked balance
	 * @return CorporativeProcessResult
	 */
	public CorporativeProcessResult updateBlockDocumentsEffections(CorporativeProcessResult corpProcessResult, boolean cashFlag){
		List<BlockCorporativeResult> blockResults = corpProcessResult.getBlockCorporativeResults();
		if(blockResults != null && blockResults.size() > 0 ){
			for(int i=0; i<blockResults.size(); i++){
				BlockCorporativeResult result = blockResults.get(i);
				if(cashFlag){
					result.setBlockedBalance(CorporateProcessUtils.divideForAmount(result.getBlockedBalance(), CorporateProcessConstants.AMOUNT_ROUND));
				}
				if(result.isUnblocked() || !CorporateProcessUtils.effectBlockDocuments(corpProcessResult.getCorporativeOperation().getCorporativeEventType().getCorporativeEventType(), result)){
					switch (AffectationType.get(result.getBlockType())){
					case PAWN:
						corpProcessResult.setPawnBalance(corpProcessResult.getPawnBalance().subtract(result.getBlockedBalance()));
						break;
					case BAN:
						corpProcessResult.setBanBalance(corpProcessResult.getBanBalance().subtract(result.getBlockedBalance()));
						break;
					case OTHERS:
						corpProcessResult.setOtherBlockBalance(corpProcessResult.getOtherBlockBalance().subtract(result.getBlockedBalance()));
						break;	
					case RESERVE:
						corpProcessResult.setReserveBalance(corpProcessResult.getReserveBalance().subtract(result.getBlockedBalance()));
						break;
					case OPPOSITION:
						corpProcessResult.setOppositionBalance(corpProcessResult.getOppositionBalance().subtract(result.getBlockedBalance()));
						break;
					}
					corpProcessResult.setAvailableBalance(corpProcessResult.getAvailableBalance().add(result.getBlockedBalance()));
					//result.setBlockedBalance(BigDecimal.ZERO);
				}
				//we don't have to remove the block corporative result 
//				if(result.getBlockedBalance().compareTo(BigDecimal.ZERO) <= 0){
//					blockResults.remove(i);
//					i--;
//				}
			}
		}
		return corpProcessResult;
	}
	
	/**
	 * This method is used to fix the balances from the corporativePRoecssResults .
	 *
	 * @param corpProcessResult : CorporativeProcessResult data
	 * @param DocsforProcess : List of BlockCorporativeREsults data
	 * @param blockType : Type of the balance
	 * @param totalBalance : total balance in the CorporativeProcessResult
	 * @param balFixer : used to fix the balances
	 * @return List of BlockCorporativeResults
	 */
	public List<BlockCorporativeResult> fixDocuments(CorporativeProcessResult corpProcessResult, List<BlockCorporativeResult> DocsforProcess,
			AffectationType blockType, BigDecimal totalBalance, BalanceFixer balFixer){
		BigDecimal balance = null;
		balance = null;
		switch (blockType){
		case PAWN:
			balance = corpProcessResult.getPawnBalance();
			break;
		case BAN:
			balance = corpProcessResult.getBanBalance();
			break;
		case OTHERS:
			balance = corpProcessResult.getOtherBlockBalance();
			break;	
		case RESERVE:
			balance = corpProcessResult.getReserveBalance();
			break;
		case OPPOSITION:
			balance = corpProcessResult.getOppositionBalance();
			break;
		}
		return fixDocuments(DocsforProcess, balance, totalBalance, balFixer);
	}
	
	/**
	 * This method is used to set the values to the BlockCorporativeResults  .
	 *
	 * @param doc : an object array of BlockCorporativeResults
	 * @param corpProcessResult : CorporativeProcessResult data
	 * @param delFactor : delivery factor for calculating the balances
	 * @param round : Rounding factor for calculating the balances
	 * @param remnantFlag : if any remnant is available
	 * @param destiny the destiny
	 * @param exchangeRateDay the exchange rate day
	 * @return BlockCorporativeResult data
	 */
	public BlockCorporativeResult fillBlockCorporativeResult(Object[] doc,CorporativeProcessResult corpProcessResult,BigDecimal delFactor,
			int round,boolean remnantFlag,boolean destiny,DailyExchangeRates exchangeRateDay) {
		BlockCorporativeResult objBlockOperRes = new BlockCorporativeResult();
		if(corpProcessResult.getIndOriginTarget()!=null && corpProcessResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY)){
			objBlockOperRes.setIndOriginDestiny(CorporateProcessConstants.EXCHANGE_DESTINY);
		}else if(corpProcessResult.getIndOriginTarget()!=null && corpProcessResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_ORIGIN)){
			objBlockOperRes.setIndOriginDestiny(CorporateProcessConstants.EXCHANGE_ORIGIN);
		}else if(destiny){
			objBlockOperRes.setIndOriginDestiny(CorporateProcessConstants.BALANCE_DESTINY);
		}else{
			objBlockOperRes.setIndOriginDestiny(CorporateProcessConstants.BALANCE_ORIGIN);
		}
		objBlockOperRes.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		if(remnantFlag){
			objBlockOperRes.setIndRemanent(CorporateProcessConstants.IND_REMAINDER);
		}else{
			objBlockOperRes.setIndRemanent(CorporateProcessConstants.IND_NO_REMAINDER);
		}
		objBlockOperRes.setCorporativeOperation(corpProcessResult.getCorporativeOperation());
		objBlockOperRes.setHolderAccount(corpProcessResult.getHolderAccount());			
		objBlockOperRes.setSecurity(corpProcessResult.getSecurity());
		objBlockOperRes.setParticipant(corpProcessResult.getParticipant());
		objBlockOperRes.setBlockType((Integer)doc[0]);
		if(corpProcessResult.getIndOriginTarget()!=null && (corpProcessResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY) 
				 || corpProcessResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_ORIGIN))){
			objBlockOperRes.setBlockedBalance(CorporateProcessUtils.calculateByFactor(((BigDecimal)doc[1]), delFactor, round));
			objBlockOperRes.setBlockedBalance(CorporateProcessUtils.calculateByFactor(objBlockOperRes.getBlockedBalance(), exchangeRateDay.getBuyPrice(), round));
		}else{
			objBlockOperRes.setBlockedBalance(CorporateProcessUtils.calculateByFactor(((BigDecimal)doc[1]), delFactor, round));
		}	
		BlockOperation blockOper = new BlockOperation();
		blockOper.setIdBlockOperationPk((Long)doc[2]);
		objBlockOperRes.setBlockOperation(blockOper);
		Holder holder = new Holder();
		holder.setIdHolderPk((Long)doc[3]);
		if(holder.getIdHolderPk() != null && holder.getIdHolderPk().compareTo(Long.valueOf(0)) > 0){
			objBlockOperRes.setHolder(holder);
		}
		objBlockOperRes.setIndCashDividend((Integer)doc[4]);
		objBlockOperRes.setIndContributionReturn((Integer)doc[5]);
		objBlockOperRes.setIndInterest((Integer)doc[6]);
		objBlockOperRes.setIndStockDividend((Integer)doc[7]);
		objBlockOperRes.setIndSuscription((Integer)doc[8]);
		objBlockOperRes.setCorporativeProcessResult(corpProcessResult);
		if(AffectationStateType.UNBLOCKED.getCode().equals(doc[11])){
			objBlockOperRes.setUnblocked(true);
		}
		objBlockOperRes.setIndAmortization((Integer)doc[12]);
		objBlockOperRes.setIndRescue((Integer)doc[13]);
		return objBlockOperRes;
	}

	/**
	 * Getting the Fixed Documents.
	 *
	 * @param blockDocs the block docs
	 * @param blockedBalance the blocked balance
	 * @param totalBalance the total balance
	 * @param balFixer the bal fixer
	 * @return List
	 */
	public List<BlockCorporativeResult> fixDocuments(List<BlockCorporativeResult> blockDocs,BigDecimal blockedBalance,BigDecimal totalBalance,BalanceFixer balFixer){
		BlockCorporativeResult blockCorpResI, blockCorpResJ;
		BigDecimal result = blockedBalance!=null?blockedBalance.subtract(totalBalance):BigDecimal.ZERO;

		if (result.compareTo(BigDecimal.ZERO) < 0) {
			for (int i = 0; i < blockDocs.size(); i++) {
				for (int j = i + 1; j < blockDocs.size(); j++) {
					blockCorpResI = (BlockCorporativeResult) blockDocs.get(i);
					blockCorpResJ = (BlockCorporativeResult) blockDocs.get(j);
					if (blockCorpResI.getBlockedBalance().compareTo(blockCorpResJ.getBlockedBalance()) < 0){
						blockDocs.set(i, blockCorpResJ);
						blockDocs.set(j, blockCorpResI);
					}
				}
			}
			BigDecimal[] balances = getBalancesArray(blockDocs);
			balances = balFixer.matchBalances(balances,result,totalBalance);
			blockDocs = setBalancesArray(balances,blockDocs);
		}else if (result.compareTo(BigDecimal.ZERO) > 0) {
			for (int i = 0; i < blockDocs.size(); i++) {
				for (int j = i + 1; j < blockDocs.size(); j++) {
					blockCorpResI = (BlockCorporativeResult) blockDocs.get(i);
					blockCorpResJ = (BlockCorporativeResult) blockDocs.get(j);
					if (blockCorpResI.getBlockedBalance().compareTo(blockCorpResJ.getBlockedBalance()) > 0) {
						blockDocs.set(i, blockCorpResJ);
						blockDocs.set(j, blockCorpResI);
					}
				}
			}
			BigDecimal[] balances = getBalancesArray(blockDocs);
			balances = balFixer.matchBalances(balances,result,totalBalance);
			blockDocs = setBalancesArray(balances,blockDocs);
		}

		return blockDocs;

	}
	
	/**
	 * Method will return a array that is constructed from given list.
	 *
	 * @param balances <code> List</code>
	 * @return <code> BigDecimal[]</code>
	 */
	private BigDecimal[] getBalancesArray(List<BlockCorporativeResult> balances) {
		BigDecimal[] balancesArray = new BigDecimal[balances.size()];
		for (int i = 0; i < balances.size(); i++) {
			BlockCorporativeResult objResult = balances.get(i);
			balancesArray[i] = objResult.getBlockedBalance();
		}
		return balancesArray;
	}

	/**
	 * Method will return the list of given array.
	 *
	 * @param balancesArray <code> BigDecimal[]</code>
	 * @param balances <code> List</code>
	 * @return <code> List</code>
	 */
	private List<BlockCorporativeResult> setBalancesArray(BigDecimal[] balancesArray, List<BlockCorporativeResult> balances) {
		for (int i = 0; i < balances.size(); i++) {
			BlockCorporativeResult objResult = balances.get(i);
			objResult.setBlockedBalance(balancesArray[i]);
		}
		return balances;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer#fillOriginBlockCorporativeResult(java.util.List, com.pradera.model.corporatives.CorporativeProcessResult)
	 */
	public List<BlockCorporativeResult> fillOriginBlockCorporativeResult(List<Object[]> blockedDocs,	CorporativeProcessResult corpProcessResult,DailyExchangeRates exchangeRateDay){
		List<BlockCorporativeResult> result = null;
		if(blockedDocs != null && !blockedDocs.isEmpty()){
			result = new ArrayList<BlockCorporativeResult>();
			for(Object[] doc : blockedDocs){
				result.add(fillBlockCorporativeResult(doc, corpProcessResult, BigDecimal.ONE, 0, false, false,exchangeRateDay));
			}
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer#prepareUnblockOperation(java.lang.Object[], java.lang.String)
	 */
	public UnblockRequest prepareUnblockOperation(Object[] data, String loginUser){
		CustodyOperation custodyOperation = new CustodyOperation();
		if(LevelAffectationType.BLOCK.getCode().equals(data[8])){
			if(AffectationType.PAWN.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_UNBLOCK.getCode());
			}else if(AffectationType.BAN.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_UNBLOCK.getCode());
			}else if(AffectationType.RESERVE.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_UNBLOCK.getCode());
			}else if(AffectationType.OTHERS.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_UNBLOCK.getCode());
			}else if(AffectationType.OPPOSITION.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_UNBLOCK.getCode());
			}
		}else if(LevelAffectationType.REBLOCK.getCode().equals(data[8])){
			if(AffectationType.PAWN.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_UNBLOCK_REBLOCK.getCode());
			}else if(AffectationType.BAN.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_UNBLOCK_REBLOCK.getCode());
			}else if(AffectationType.RESERVE.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_UNBLOCK_REBLOCK.getCode());
			}else if(AffectationType.OTHERS.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_UNBLOCK_REBLOCK.getCode());
			}else if(AffectationType.OPPOSITION.getCode().equals(data[0])){
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_UNBLOCK_REBLOCK.getCode());
			}
		}
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryUser(loginUser);
		custodyOperation.setOperationDate(CommonsUtilities.currentDate());
		Long operationNumber = corporateExecutorService.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_UNBLOCKING);
		custodyOperation.setOperationNumber(operationNumber);
		custodyOperation.setConfirmDate(CommonsUtilities.currentDateTime());
		custodyOperation.setConfirmUser(loginUser);	
		custodyOperation.setState(RequestReversalsStateType.CONFIRMED.getCode());
		UnblockOperation unblockOperation = new UnblockOperation();
		UnblockRequest unblockRequest = new UnblockRequest();
		unblockRequest.setDocumentNumber(((Long)data[4]).toString());
		unblockOperation.setDocumentNumber(operationNumber);
		Holder holder = new Holder();
		holder.setIdHolderPk((Long)data[1]);
		unblockRequest.setHolder(holder);
		unblockRequest.setMotive(MotiveReverlsasType.NORMAL.getCode());				
		unblockRequest.setUnblockRequestState(RequestReversalsStateType.CONFIRMED.getCode());
		unblockRequest.setRequestQuantity((BigDecimal)data[2]);
		unblockRequest.setRegistryUser(loginUser);
		unblockRequest.setRegistryDate(CommonsUtilities.currentDate());
		unblockRequest.setConfirmationDate(CommonsUtilities.currentDate());
		unblockOperation.setCustodyOperation(custodyOperation);
		BlockOperation blockOpe = new BlockOperation();
		blockOpe.setIdBlockOperationPk((Long)data[3]);
		//blockOpe.setBlockState(AffectationStateType.UNBLOCKED.getCode());
		//blockOpe.setOriginalBlockAmount(originalBlockAmount)
		unblockOperation.setBlockOperation(blockOpe);
		unblockOperation.setUnblockRequest(unblockRequest);
		unblockOperation.setUnblockQuantity((BigDecimal)data[2]);
		unblockOperation.setUnblockState(RequestReversalsStateType.CONFIRMED.getCode());
		unblockOperation.setUnblockMarketFactOperations(new ArrayList<UnblockMarketFactOperation>());
		List<UnblockOperation> unblockOperations = new ArrayList<UnblockOperation>();
		unblockOperations.add(unblockOperation);
		unblockRequest.setUnblockOperation(unblockOperations);
		return unblockRequest;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer#prepareBlockOperation(java.lang.Object[], java.lang.String, java.math.BigDecimal, java.math.BigDecimal)
	 */
	public BlockRequest prepareBlockOperation(Object[] data, String loginUser, BigDecimal originalBalance, BigDecimal actualBalance){
		BlockRequest blockRequest = new BlockRequest();
		blockRequest.setBlockOperation(new ArrayList<BlockOperation>());
		blockRequest.setBlockType((Integer)data[0]);			
		blockRequest.setHolder(new Holder());
		blockRequest.getHolder().setIdHolderPk((Long)data[1]);
		blockRequest.setBlockEntity(new BlockEntity());			
		blockRequest.getBlockEntity().setIdBlockEntityPk((Long)data[2]);
		blockRequest.setRegistryUser(loginUser);
		blockRequest.setRegistryDate(CommonsUtilities.currentDate());	
		blockRequest.setBlockRequestState(RequestAffectationStateType.CONFIRMED.getCode());	
		blockRequest.setConfirmationDate(CommonsUtilities.currentDate());
		blockRequest.setConfirmationUser(loginUser);

		blockRequest.setBlockNumber((String)data[3]);
		blockRequest.setBlockNumberDate((Date)data[4]);;
		blockRequest.setBlockForm((Integer)data[5]);
		blockRequest.setOriginBlockAmount(originalBalance);
		blockRequest.setComments((String)data[6]);
		blockRequest.setCurrency((Integer)data[7]);
		blockRequest.setIndRequestDeclaration((Integer)data[8]);
		blockRequest.setCurrentBlockAmount(actualBalance);	
		blockRequest.setExchangeRate((BigDecimal)data[9]);
		blockRequest.setIndIssueRequest(BooleanType.NO.getCode());
		BlockOperation blockOperation = new BlockOperation();
		CustodyOperation custodyOperation = new CustodyOperation();
		custodyOperation.setOperationDate(CommonsUtilities.currentDate());						
		custodyOperation.setRegistryUser(loginUser);
		custodyOperation.setRegistryDate(CommonsUtilities.currentDate());						
		blockOperation.setBlockState(AffectationStateType.BLOCKED.getCode());
		custodyOperation.setState(AffectationStateType.BLOCKED.getCode());
		custodyOperation.setConfirmDate(CommonsUtilities.currentDate());
		custodyOperation.setConfirmUser(loginUser);
		Participant participant = new Participant();
		participant.setIdParticipantPk((Long)data[10]);
		Security security = new Security();
		security.setIdSecurityCodePk((String)data[11]);
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setIdHolderAccountPk((Long)data[12]);

		blockOperation.setBlockRequest(blockRequest);						
		blockOperation.setParticipant(participant);						
		blockOperation.setSecurities(security);						
		blockOperation.setHolderAccount(holderAccount);
		
		blockOperation.setBlockLevel((Integer)data[24]);
		if(AffectationType.RESERVE.getCode().equals(blockRequest.getBlockType()))				
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_BLOCK.getCode());
		else if(AffectationType.PAWN.getCode().equals(blockRequest.getBlockType())){
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_BLOCK.getCode());
		}else if(AffectationType.BAN.getCode().equals(blockRequest.getBlockType()))				
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_BLOCK.getCode());
		else if(AffectationType.OTHERS.getCode().equals(blockRequest.getBlockType()))
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_BLOCK.getCode());
		else
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_BLOCK.getCode());
		
		
		blockOperation.setActualBlockBalance(actualBalance);
		blockOperation.setOriginalBlockBalance(originalBalance);
		BlockOperation refBlock = new BlockOperation();
		refBlock.setIdBlockOperationPk((Long)data[13]);
		blockOperation.setBlockOperationRef(refBlock);
		blockOperation.setIndCashDividend((Integer)data[14]);
		blockOperation.setIndInterest((Integer)data[15]);
		blockOperation.setIndStockDividend((Integer)data[16]);
		blockOperation.setIndSuscription((Integer)data[17]);
		blockOperation.setIndReturnContributions((Integer)data[18]);
		blockOperation.setIndAccreditation((Integer)data[19]);
		blockOperation.setIndAmortization((Integer)data[20]);
		blockOperation.setIndRescue((Integer)data[21]);
		blockOperation.setIndRegisteredDepositary((Integer)data[25]);
		blockOperation.setNominalValue((BigDecimal)data[26]);
		blockOperation.setCustodyOperation(custodyOperation);
		custodyOperation.setOperationNumber(corporateExecutorService.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCKING));
		blockOperation.setDocumentNumber(custodyOperation.getOperationNumber());
		blockOperation.setBlockMarketFactOperations(new ArrayList<BlockMarketFactOperation>());
		blockRequest.getBlockOperation().add(blockOperation);
		
		return blockRequest;	
	}

}
