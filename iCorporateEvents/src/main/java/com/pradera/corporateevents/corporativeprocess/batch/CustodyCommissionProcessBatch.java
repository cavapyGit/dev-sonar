package com.pradera.corporateevents.corporativeprocess.batch;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.corporateevents.corporativeprocess.facade.CustodyCommissionServiceFacade;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * Bachero que ejecuta el proceso de calculo de comisiones.
 *
 * @author PraderaTechnologies
 */
@RequestScoped
@BatchProcess(name="CustodyCommissionProcessBatch")
public class CustodyCommissionProcessBatch  implements JobExecution{

	/** The CorporateServiceBean. */
	@EJB
	CustodyCommissionServiceFacade custodyCommissionFacade;

	/**
	 * Used to start the batch process execution.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		String userId = null;
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		//obtenemos usuario quien ejecuto el bachero
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			if(processLoggerDetail.getParameterName().equals("userid")){
				userId = processLoggerDetail.getParameterValue();
			}
		}
		if(userId != null){
			try {
				custodyCommissionFacade.calculateCustodyCommission(userId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}	
}