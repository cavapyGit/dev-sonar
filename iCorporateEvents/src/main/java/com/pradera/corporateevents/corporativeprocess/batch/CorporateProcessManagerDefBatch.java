package com.pradera.corporateevents.corporativeprocess.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
// TODO: Auto-generated Javadoc

/**
 * Bachero que ejecuta la administracion del proceso definitivo automativo.
 *
 * @author PraderaTechnologies
 */
@BatchProcess(name="CorporateProcessManagerDefBatch")
@RequestScoped
public class CorporateProcessManagerDefBatch  implements JobExecution {

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The batch service. */
	@EJB
	BatchServiceBean batchService;
	
	/** The obj parameter service bean. */
	@EJB
	ParameterServiceBean objParameterServiceBean;
	
	/**
	 * Inicio del Bachero que ejecuta la administracion del proceso definitivo automativo.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger){
		//declaramos variables del procesos automatico
		
		Integer limitBlock = null;//cantidad por bloque
		Integer waitTime = null;//tiempo de espera despues de lanzar un bloque
		String userId = null;//usuario demonio
		String currentDate = null;//fecha dinamica para pruebas
		Integer waitTimeProcess = null;//tiempo de espera de un proceso enviado pero que no finaliza
		
		//obtenemos variables del procesos automatico
		ParameterTableTO filter = new ParameterTableTO();
		filter.setParameterTablePk(MasterTableType.PARAMETER_PROCESS_DEFINITIVE_AUTOMATIC.getCode());
		ParameterTable objParameterTable = objParameterServiceBean.getListParameterTableServiceBean(filter).get(0);
		if(objParameterTable.getIndicator4()!=null){
			limitBlock = objParameterTable.getIndicator4();
		}
		if(objParameterTable.getIndicator5()!=null){
			waitTime = objParameterTable.getIndicator5();
		}
		if(objParameterTable.getIndicator6()!=null){
			waitTimeProcess = objParameterTable.getIndicator6();
		}
		if(processLogger.getIdUserAccount()!=null){
			userId = processLogger.getIdUserAccount();
		}else{
			if(objParameterTable.getText1()!=null){
				userId = objParameterTable.getText1();
			}			
		}
		if(objParameterTable.getText2()!=null){
			String current = objParameterTable.getText2();
			if(current.toUpperCase().equals(CorporateProcessConstants.STR_SYSDATE.toUpperCase())){
				currentDate = CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate());
			}else{
				currentDate = current;
			}
		}		
	
		log.info("Parameters for the corporate process Manager execution:");
		log.info("limitBlock: " + limitBlock);
		log.info("waitTime: "+waitTime);
		log.info("userId: "+userId);
		log.info("currentDate: "+currentDate);
		//validamos variables del procesos automatico
		if(Validations.validateIsNotNull(limitBlock) && Validations.validateIsNotNull(waitTime)
				&& Validations.validateIsNotNull(userId) && Validations.validateIsNotNull(currentDate)
				&& Validations.validateIsNotNull(waitTimeProcess)){
			log.info("Initial Corporate process Manager execution/n");	
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.MANAGER_CORPORATIVE_PROCESS_AUTOMATIC_DEFINITIVE.getCode());
		    Map<String, Object> param = new HashMap<String, Object>();				    
		    param.put("limitBlock", limitBlock);
		    param.put("waitTime", waitTime);		
		    param.put("userId",userId);	
		    param.put("currentDate",currentDate);	
		    param.put("waitTimeProcess",waitTimeProcess);	
		    try {
				batchService.registerBatchTx(userId, businessProcess, param);
			} catch (ServiceException e) {
				log.error(e.getMessage());
				throw new RuntimeException(e);
			}			
		    
		    Integer count = ComponentConstant.ZERO;
		    do{
		    	try {
		    		if(objParameterServiceBean.definitiveCorporativeIsReady(CommonsUtilities.currentDate())){
		    			break;
		    		}
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	count ++;
		    }while(count<5);
		    
		    
		}else{
			log.error("Invalied parameters for the corporate process Manager execution/n");
			throw new RuntimeException("Invalied parameters for the corporate process Manager execution");
		}		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}	
}
