package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.ReportCorporativeResult;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.ParticipantRoleType;

// TODO: Auto-generated Javadoc
/**
 * The Class ReportOperationsFixer.
 *
 * @author PraderaTechnologies
 */
@ApplicationScoped
public class ReportOperationsFixer implements OperationsFixer, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new FixedBlockedDocuments object.
	 */
	public ReportOperationsFixer() {
	}
	
	/**
	 * Used to create a list of ReportCorporativeResults.
	 *
	 * @param corpProcessResults the corp process results
	 * @param reportOperations <code>List </code>
	 * @param delFactor <code>BigDecimal </code>
	 * @param round <code>int </code>
	 * @param remnantFlag <code>boolean </code>
	 * @param cashFlag <code>boolean </code>
	 * @param destiny <code>boolean </code>
	 * @param balFixer <code>BalanceFixer </code>
	 * @param exchangeRateDay the exchange rate day
	 * @return <code>List </code>
	 */
	public List<ReportCorporativeResult> fixOperations(CorporativeProcessResult corpProcessResults,
			List<Object[]> reportOperations, BigDecimal delFactor, int round, boolean remnantFlag, boolean cashFlag, boolean destiny
			, BalanceFixer balFixer,DailyExchangeRates exchangeRateDay){
		List<ReportCorporativeResult> marginOpers = null;
		if(reportOperations != null && reportOperations.size() > 0){
			marginOpers = new ArrayList<ReportCorporativeResult>();
			BigDecimal totalMarginBalance = new BigDecimal(0);
			List<ReportCorporativeResult> reportOpers = new ArrayList<ReportCorporativeResult>();
			BigDecimal totalReportBalance = new BigDecimal(0);
			for(Object[] oper : reportOperations){
				GuaranteeType guarType = GuaranteeType.get((Integer)oper[0]);
				oper[2]=oper[2]!=null?cashFlag?CorporateProcessUtils.multiply((BigDecimal)oper[2], CorporateProcessConstants.AMOUNT_ROUND)
						:oper[2]:new BigDecimal(0);
				ReportCorporativeResult reportOper = fillOperations(oper, corpProcessResults, delFactor, round, remnantFlag, destiny,exchangeRateDay);		
				if(GuaranteeType.MARGIN.equals(guarType)|| GuaranteeType.STOCK_DIVIDENDS_MARGIN.equals(guarType)){
					marginOpers.add(reportOper);
					totalMarginBalance = totalMarginBalance.add(reportOper.getGuaranteeBalance());
				}else if((GuaranteeType.PRINCIPAL.equals(guarType)|| GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.equals(guarType))
						&& ParticipantRoleType.SELL.getCode().equals((Integer)oper[1])){
					//reportante es el vendedor en la parte plazo
					reportOpers.add(reportOper);
					totalReportBalance = totalReportBalance.add(reportOper.getGuaranteeBalance());
				}					
			}
			if (corpProcessResults.getMarginBalance().compareTo(BigDecimal.ZERO) > 0 && !marginOpers.isEmpty()) {
				marginOpers = fixOperations(
						marginOpers, corpProcessResults.getMarginBalance(), totalMarginBalance, balFixer);
			}else{
				marginOpers.clear();
			}
			if (corpProcessResults.getReportingBalance().compareTo(BigDecimal.ZERO) > 0 && !reportOpers.isEmpty()) {
				reportOpers = fixOperations(
						reportOpers, corpProcessResults.getReportingBalance(), totalReportBalance, balFixer);
			}else{
				reportOpers.clear();
			}
			marginOpers.addAll(reportOpers);
			if(cashFlag){
				for(ReportCorporativeResult corpReport : marginOpers){
					corpReport.setGuaranteeBalance(CorporateProcessUtils.divideForAmount(corpReport.getGuaranteeBalance(), CorporateProcessConstants.AMOUNT_ROUND));
				}
			}			
		}
		return marginOpers;
	}
	
	/**
	 * Used to set the values for the ReportCorporativeResult.
	 *
	 * @param oper <code>Object[] </code>
	 * @param corpProcessResults <code>CorporativeProcessResult </code>
	 * @param delFactor <code>BigDecimal </code>
	 * @param round <code>int </code>
	 * @param remnantFlag <code>boolean </code>
	 * @param destiny <code>boolean </code>
	 * @param exchangeRateDay the exchange rate day
	 * @return <code>ReportCorporativeResult </code>
	 */
	private ReportCorporativeResult fillOperations(Object[] oper,	CorporativeProcessResult corpProcessResults, BigDecimal delFactor, int round,
			boolean remnantFlag, boolean destiny,DailyExchangeRates exchangeRateDay) {

		ReportCorporativeResult objReportOperRes = new ReportCorporativeResult();
		objReportOperRes.setCorporativeOperation(corpProcessResults.getCorporativeOperation());
		objReportOperRes.setHolderAccount(corpProcessResults.getHolderAccount());
		objReportOperRes.setParticipant(corpProcessResults.getParticipant());
		GuaranteeType guarType = GuaranteeType.get((Integer) oper[0]);
		objReportOperRes.setGuaranteeType(guarType.getCode());
		objReportOperRes.setOperationRole((Integer) oper[1]);
		if(corpProcessResults.getIndOriginTarget()!=null && 
				(corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY) || 
						corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_ORIGIN))){
			objReportOperRes.setGuaranteeBalance(CorporateProcessUtils.calculateByFactor((BigDecimal) oper[2], delFactor, round));
			objReportOperRes.setGuaranteeBalance(CorporateProcessUtils.calculateByFactor(objReportOperRes.getGuaranteeBalance(), exchangeRateDay.getBuyPrice(), round));
		}else{
			objReportOperRes.setGuaranteeBalance(CorporateProcessUtils.calculateByFactor((BigDecimal) oper[2], delFactor, round));
		}		
		objReportOperRes.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		if (remnantFlag) {
			objReportOperRes.setIndRemanent(CorporateProcessConstants.IND_REMAINDER);
		} else {
			objReportOperRes.setIndRemanent(CorporateProcessConstants.IND_NO_REMAINDER);
		}
		if(corpProcessResults.getIndOriginTarget()!=null && 
				corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY)){
			objReportOperRes.setIndOriginDestiny(CorporateProcessConstants.EXCHANGE_DESTINY);
		}else if(corpProcessResults.getIndOriginTarget()!=null && 
				corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_ORIGIN)){
			objReportOperRes.setIndOriginDestiny(CorporateProcessConstants.EXCHANGE_ORIGIN);
		}else if(destiny){
			objReportOperRes.setIndOriginDestiny(CorporateProcessConstants.BALANCE_DESTINY);
		}else{
			objReportOperRes.setIndOriginDestiny(CorporateProcessConstants.BALANCE_ORIGIN);
		}
		objReportOperRes.setSecurity(corpProcessResults.getSecurity());
		MechanismOperation mechOper = new MechanismOperation();
		mechOper.setIdMechanismOperationPk((Long) oper[4]);
		objReportOperRes.setMechanismOperation(mechOper);
		HolderAccountOperation holAccOper = new HolderAccountOperation();
		holAccOper.setIdHolderAccountOperationPk((Long) oper[3]);
		objReportOperRes.setHolderAccountOperation(holAccOper);
		objReportOperRes.setCorporativeProcessResult(corpProcessResults);

		return objReportOperRes;
	}
	
	
	/**
	 * Used to add the ReportCorporativeResults and subtract them with the totalBalance and process the result.
	 *
	 * @param operations <code>List </code>
	 * @param reportBalance <code>BigDecimal </code>
	 * @param totalBalance <code>BigDecimal </code>
	 * @param balFixer <code>BalanceFixer </code>
	 * @return <code>List </code>
	 */
	public List<ReportCorporativeResult> fixOperations(List<ReportCorporativeResult> operations, BigDecimal reportBalance,
			BigDecimal totalBalance, BalanceFixer balFixer)  {
		BigDecimal result = reportBalance.subtract(totalBalance);
		ReportCorporativeResult reportOperI = null, reportOperJ = null;
		if (result.compareTo(BigDecimal.ZERO) < 0) {

			for (int i = 0; i < operations.size(); i++) {
				for (int j = i + 1; j < operations.size(); j++) {
					reportOperI = (ReportCorporativeResult) operations
							.get(i);
					reportOperJ = (ReportCorporativeResult) operations
							.get(j);
					if (reportOperI.getGuaranteeBalance().compareTo(reportOperJ.getGuaranteeBalance()) < 0) {
						operations.set(i, reportOperJ);
						operations.set(j, reportOperI);
					}

				}
			}
			BigDecimal[] dlistbb = getBalancesArray(operations);
			dlistbb = balFixer.matchBalances(dlistbb, result,totalBalance);
			operations = setBalancesArray(dlistbb,operations);
			
		}else if (result.compareTo(BigDecimal.ZERO) > 0) {
			for (int i = 0; i < operations.size(); i++) {
				for (int j = i + 1; j < operations.size(); j++) {
					reportOperI = (ReportCorporativeResult) operations
							.get(i);
					reportOperJ = (ReportCorporativeResult) operations
							.get(j);
					if (reportOperI.getGuaranteeBalance().compareTo(reportOperJ.getGuaranteeBalance()) > 0) {
						operations.set(i, reportOperJ);
						operations.set(j, reportOperI);
					}

				}
			}
			BigDecimal[] balances = getBalancesArray(operations);
			balances = balFixer.matchBalances(balances, result,totalBalance);
			operations = setBalancesArray(balances,operations);

		}
		return operations;
	}
	
	/**
	 * Method will return a array that is constructed from given list.
	 *
	 * @param balances <code> List</code>
	 * @return <code> BigDecimal[]</code>
	 */
	private BigDecimal[] getBalancesArray(List<ReportCorporativeResult> balances) {
		BigDecimal[] balancesArray = new BigDecimal[balances.size()];
		for (int i = 0; i < balances.size(); i++) {
			ReportCorporativeResult objResult = balances.get(i);
			balancesArray[i] = objResult.getGuaranteeBalance();
		}
		return balancesArray;
	}

	/**
	 * Method will return the list of given array.
	 *
	 * @param balancesArray <code> BigDecimal[]</code>
	 * @param balances <code> List</code>
	 * @return <code> List</code>
	 */
	private List<ReportCorporativeResult> setBalancesArray(BigDecimal[] balancesArray, List<ReportCorporativeResult> balances) {
		for (int i = 0; i < balances.size(); i++) {
			ReportCorporativeResult objResult = balances.get(i);
			objResult.setGuaranteeBalance(balancesArray[i]);			
		}
		return balances;
	}
	
	/**
	 * Method will return a list of ReportCorporativeResults.
	 *
	 * @param reportOperations <code>List</code>
	 * @param corpProcessResults <code>CorporativeProcessResult</code>
	 * @param exchangeRateDay the exchange rate day
	 * @return <code>List</code>
	 */
	public List<ReportCorporativeResult> fillOriginOperations(List<Object[]> reportOperations,	CorporativeProcessResult corpProcessResults,DailyExchangeRates exchangeRateDay){
		List<ReportCorporativeResult> result = null;
		if(reportOperations != null && !reportOperations.isEmpty()){
			result = new ArrayList<ReportCorporativeResult>();
			for(Object[] oper : reportOperations){
				result.add(fillOperations(oper, corpProcessResults, BigDecimal.ONE, 0, false, false,exchangeRateDay));
			}
		}
		return result;
	}
}
