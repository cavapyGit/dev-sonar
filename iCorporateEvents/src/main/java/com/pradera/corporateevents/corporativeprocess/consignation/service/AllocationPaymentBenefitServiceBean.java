package com.pradera.corporateevents.corporativeprocess.consignation.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationPaymentSearchTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationProcessIssuerResumeTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.BankHolderAllocationDetailTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.ParticipantAccountCashBank;
import com.pradera.corporateevents.corporativeprocess.consignation.to.RegisterAllocationProcess;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateDetailType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.corporatives.BenefitPaymentAllocation;
import com.pradera.model.corporatives.BlockBenefitDetail;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.PaymentAllocationDetail;
import com.pradera.model.corporatives.type.AllocationProcessStateType;
import com.pradera.model.corporatives.type.AllocationProcessType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.type.BenefitPaymentType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.DetailCashAccountStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * Clase de servicio para la consignacion de pagos.
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCorporateEvents
 * @Creation_Date :
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AllocationPaymentBenefitServiceBean extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The Constant STR_BLOCK_TYPE_FOUR. */
	private final static String STR_BLOCK_TYPE_FOUR = "4";

	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/**
	 * Metodo que retorna de los procesos corporativos en diferentes estados.
	 *
	 * @param objSearch object search
	 * @return Allocation Benefits
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getAllocationBenefits(AllocationPaymentSearchTO objSearch) throws ServiceException{
		List<Object[]> lstReturn = new ArrayList<Object[]>();
		List<Integer> lstCorporativeType = getBenefitCorporativeType();
		List<Integer> lstCorporativeState = getBenefitCorporativeStates();
		String strQuery = 	
			"select " +
			"iss.idIssuerPk," +																//[0]
			"iss.businessName," +															//[1]
			"co.idCorporativeOperationPk," +												//[2]
			"co.paymentAmount," +															//[3]
			"co.currency," +																//[4]
			"co.situation," +																//[5]
			"co.state," +																	//[6]
			"s.instrumentType, " +															//[7]
			"co.indPayed " +																//[8]
			"from CorporativeOperation co, Issuer iss, Security s " +
			"where " +
			"co.issuer.idIssuerPk = iss.idIssuerPk " +
			"and co.securities.idIsinCodePk = s.idIsinCodePk " +
			"and iss.idIssuerPk = s.issuer.idIssuerPk " +
			"and trunc(co.deliveryDate)=trunc(:paymentDay) " +
			"and co.state in (:states) " +
			"and co.corporativeEventType.corporativeEventType in (:corporativeType) ";
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer()) && 
					Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer().getIdIssuerPk())){
				strQuery += "and iss.idIssuerPk = :issuer ";
			}
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getCurrency())){
				strQuery += "and co.currency = :currency ";
			}
			strQuery += "order by iss.idIssuerPk,co.indPayed,co.currency";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("paymentDay",CommonsUtilities.currentDate());
		query.setParameter("corporativeType",lstCorporativeType);
		query.setParameter("states",lstCorporativeState);
		if(Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer()) && 
				Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer().getIdIssuerPk())){
			query.setParameter("issuer",objSearch.getIssuer().getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(objSearch.getCurrency())){
			query.setParameter("currency",objSearch.getCurrency());
		}
		lstReturn = query.getResultList();
		return lstReturn;
	}

	/**
	 * Metodo que retorna cuenta efectivo de devoluciones de pago.
	 *
	 * @param issuer the issuer
	 * @param currency the currency
	 * @return the centralizer activate benefit cash account
	 */
	public InstitutionCashAccount getCentralizerActivateBenefitCashAccount(String issuer, Integer currency){
		InstitutionCashAccount cashAccount = null;
		try{
			TypedQuery<InstitutionCashAccount> query = em.createQuery(
			"SELECT i FROM InstitutionCashAccount i " +
			"WHERE " +
			"i.accountState = :state and " +
			"i.accountType = :accType and " +
			"i.issuer.idIssuerPk = :issuer and " +
//			"i.indRelatedBcrd = :indBCRD and " +
			"i.currency = :currency ",InstitutionCashAccount.class);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("accType",AccountCashFundsType.BENEFIT.getCode());
			query.setParameter("issuer",issuer);
//			query.setParameter("indBCRD",BooleanType.YES.getCode());
			query.setParameter("currency",currency);
			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.getMessage();
		}
		return cashAccount;
	}
	
	public InstitutionCashAccount getUniqueActivateInstitutionCashAccount(Integer cashAccountFundsType, Integer currency){
		InstitutionCashAccount cashAccount = null;
		try{
			TypedQuery<InstitutionCashAccount> query = em.createQuery(
			"SELECT i FROM InstitutionCashAccount i " +
			"WHERE " +
			"i.accountState = :state and " +
			"i.accountType = :accType and " +
//			"i.issuer.idIssuerPk = :issuer and " +
//			"i.indRelatedBcrd = :indBCRD and " +
			"i.currency = :currency ",InstitutionCashAccount.class);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("accType",cashAccountFundsType);
			//query.setParameter("issuer",issuer);
//			query.setParameter("indBCRD",BooleanType.YES.getCode());
			query.setParameter("currency",currency);
			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.getMessage();
		}
		return cashAccount;
	}
	
	public InstitutionCashAccount getParticipantActivateInstitutionCashAccount(Long idParticipantPk,Integer cashAccountFundsType, Integer currency){
		InstitutionCashAccount cashAccount = null;
		try{
			TypedQuery<InstitutionCashAccount> query = em.createQuery(
			"SELECT i FROM InstitutionCashAccount i " +
			"WHERE " +
			"i.accountState = :state and " +
			"i.accountType = :accType and " +
			"i.participant.idParticipantPk = :idParticipantPk and " +
//			"i.indRelatedBcrd = :indBCRD and " +
			"i.currency = :currency ",InstitutionCashAccount.class);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("accType",cashAccountFundsType);
			query.setParameter("idParticipantPk",idParticipantPk);
//			query.setParameter("indBCRD",BooleanType.YES.getCode());
			query.setParameter("currency",currency);
			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.getMessage();
		}
		return cashAccount;
	}
	
	/**
	 * Metodo que retorna estado de la consignacion.
	 *
	 * @return Allocation Process Type
	 */
	public List<ParameterTable> getAllocationProcessType(){
		List<ParameterTable> lstProcessType = new ArrayList<ParameterTable>();
		TypedQuery<ParameterTable> query = em.createQuery(
				"SELECT p FROM ParameterTable p WHERE p.masterTable.masterTablePk = :allocateProcessType ",ParameterTable.class);

		query.setParameter("allocateProcessType",MasterTableType.ALLOCATION_BENEFIT_PROCESS_TYPE.getCode());
		lstProcessType = query.getResultList();

		return lstProcessType;
	}

	/**
	 * Metodo que retorna de los procesos corporativos en estado definitivo por moneda.
	 *
	 * @param currency the currency
	 * @param issuer the issuer
	 * @return the allocate corporatives
	 */
	public List<Object[]> getAllocateCorporatives(Integer currency, String issuer){
		List<Object[]> lstResult = new ArrayList<Object[]>();
		String strQuery =  "select " +
			"co.idCorporativeOperationPk," +														//[0]
			"co.securities.idIsinCodePk," +															//[1]
			"nvl(co.paymentAmount,0)," +																	//[2]
			"nvl(co.custodyAmount,0)," +																	//[3]
			"nvl(co.taxAmount,0)," +																		//[4]
			"NVL((co.paymentAmount+co.commissionAmount),0)," +										//[5]
			"NVL(co.commissionAmount,0)," +															//[6]
			"p.description," +																		//[7]
			"co.indExcludedCommissions, " +															//[8]
			"s.instrumentType, " +																	//[9]
			"co.situation " +																		//[10]
			"from CorporativeOperation co, ParameterTable p, Security s " +
			"where " +
			"p.parameterTablePk = co.state " +
			"and trunc(co.deliveryDate) = trunc(:today) " +
			"and s.idIsinCodePk = co.securities.idIsinCodePk " +
			"and co.corporativeEventType.corporativeEventType in (:eventType) " +
			"and co.currency = :currency " +
			"and (co.state = :definitive )" +
			"and co.issuer.idIssuerPk = :issuer " +
			"and co.indPayed = :notPayed ";
		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("today",CommonsUtilities.currentDate());
		query.setParameter("eventType",getBenefitCorporativeType());
		query.setParameter("currency",currency);
		query.setParameter("definitive",CorporateProcessStateType.DEFINITIVE.getCode());
		query.setParameter("issuer",issuer);
		query.setParameter("notPayed",BooleanType.NO.getCode());
		lstResult = query.getResultList();

		return lstResult;
	}

	/**
	 * Metodo que retorna tipo de procesos corporativo.
	 *
	 * @return the benefit corporative type
	 */
	private List<Integer> getBenefitCorporativeType(){
		List<Integer> lstCorporativeType = new ArrayList<Integer>();
		lstCorporativeType.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
		lstCorporativeType.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
		lstCorporativeType.add(ImportanceEventType.REMANENT_PAYMENT.getCode());
		lstCorporativeType.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		lstCorporativeType.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		lstCorporativeType.add(ImportanceEventType.EARLY_PAYMENT.getCode());
		return lstCorporativeType;
	}

	/**
	 * Metodo que retorna datos de beneficio del titular.
	 *
	 * @param corporatives the corporatives
	 * @return the benefit holders
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getBenefitHolders(List<Long> corporatives) throws ServiceException{
		List<Object[]> lstResult = new ArrayList<Object[]>();
		String strSQL = "select distinct " +
						"co.ID_ISSUER_FK, " +																				//[0]
						"co.ID_ORIGIN_ISIN_CODE_FK, " +																		//[1]
						"h.ID_HOLDER_PK, " +																				//[2]
						"h.FULL_NAME, " +																					//[3]
						"h.DOCUMENT_NUMBER, " +																				//[4]
						"sum(c.AVAILABLE_BALANCE) - (SUM(d.AVAILABLE_TAX_AMOUNT) + SUM(d.AVAILABLE_CUSTODY_AMOUNT))," +		//[5]
						"hab.ID_BANK_FK, " +																				//[6]
						"hab.BANK_ACCOUNT_NUMBER, " +																		//[7]
						"hab.BANK_ACCOUNT_TYPE, " +																			//[8]
						"co.CORPORATIVE_EVENT_TYPE, " +																		//[9]
						"hab.ID_HOLDER_ACCOUNT_BANK_PK " +																	//[10]
						"from CORPORATIVE_OPERATION co,CORPORATIVE_PROCESS_RESULT c, HOLDER_ACCOUNT ha, HOLDER_ACCOUNT_DETAIL had, "
						+ "HOLDER_ACCOUNT_BANK hab,HOLDER h, CORPORATIVE_RESULT_DETAIL d " +
						"where " +
						"c.ID_CORPORATIVE_OPERATION_FK in (:lstCorporative) " +
						"and c.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK " +
						"AND had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK " +
						"and hab.ID_HOLDER_FK = h.ID_HOLDER_PK " +
						"and c.ID_CORPORATIVE_PROCESS_RESULT = d.ID_CORPORATIVE_PROCESS_RESULT " +
						"and co.ID_CORPORATIVE_OPERATION_PK = c.ID_CORPORATIVE_OPERATION_FK " +
						"and co.currency = hab.currency " +
						"and hab.ID_HOLDER_ACCOUNT_FK = c.ID_HOLDER_ACCOUNT_FK " +
						"and hab.HAVE_BIC = :noBic " +
						"and not exists (select 1 from V_ALLOCATE_BLOCK_HOLDERS v where v.ID_HOLDER = h.ID_HOLDER_PK and v.block_type <> :blockType) " +
						"AND had.IND_REPRESENTATIVE = :indRepre " +
						"group by " +
						"hab.ID_BANK_FK,h.ID_HOLDER_PK,hab.BANK_ACCOUNT_NUMBER,co.ID_ISSUER_FK,co.ID_ORIGIN_ISIN_CODE_FK,h.FULL_NAME," +
						"h.DOCUMENT_NUMBER,hab.BANK_ACCOUNT_TYPE,co.CORPORATIVE_EVENT_TYPE,hab.ID_HOLDER_ACCOUNT_BANK_PK " +
						"order by " +
						"hab.ID_BANK_FK,h.ID_HOLDER_PK";

		Query query = em.createNativeQuery(strSQL);
		query.setParameter("lstCorporative",corporatives);
		query.setParameter("noBic",BooleanType.NO.getCode());
		query.setParameter("indRepre",BooleanType.YES.getCode());
		query.setParameter("blockType", STR_BLOCK_TYPE_FOUR);
		lstResult = query.getResultList();

		return lstResult;
	}

	/**
	 * Metodo que retorna datos de beneficio de la entidad.
	 *
	 * @param corporatives the corporatives
	 * @return the benefit entities
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getBenefitEntities(List<Long> corporatives) throws ServiceException{
		List<Object[]> lstResult = new ArrayList<Object[]>();
		String strSQL = "select distinct " +
						"co.ID_ISSUER_FK," +																				//[0]
						"co.ID_ORIGIN_ISIN_CODE_FK," +																		//[1]
						"h.ID_HOLDER_PK," +																					//[2]
						"h.FULL_NAME," +																					//[3]
						"h.DOCUMENT_NUMBER," +																				//[4]
						"sum(c.AVAILABLE_BALANCE) - (SUM(d.AVAILABLE_TAX_AMOUNT) + SUM(d.AVAILABLE_CUSTODY_AMOUNT))," +		//[5]
						"hab.ID_BANK_FK," +																					//[6]
						"hab.BANK_ACCOUNT_NUMBER, " +																		//[7]
						"hab.BIC_CODE, " +																					//[8]
						"co.CORPORATIVE_EVENT_TYPE, " +																		//[9]
						"co.currency " +																					//[10]
						"from CORPORATIVE_OPERATION co,CORPORATIVE_PROCESS_RESULT c, HOLDER_ACCOUNT ha, HOLDER_ACCOUNT_DETAIL had, "
						+ "HOLDER_ACCOUNT_BANK hab,HOLDER h, CORPORATIVE_RESULT_DETAIL d " +
						"where c.ID_CORPORATIVE_OPERATION_FK in (:lstCorporative) " +
						"and c.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK " +
						"AND had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK " +
						"and had.ID_HOLDER_FK = h.ID_HOLDER_PK " +
						"and c.ID_CORPORATIVE_PROCESS_RESULT = d.ID_CORPORATIVE_PROCESS_RESULT " +
						"and co.ID_CORPORATIVE_OPERATION_PK = c.ID_CORPORATIVE_OPERATION_FK " +
						"and co.currency = hab.currency " +
						"and hab.ID_HOLDER_ACCOUNT_FK = c.ID_HOLDER_ACCOUNT_FK " +
						"and hab.HAVE_BIC = :yesBicCode " +
						"and not exists (select 1 from V_ALLOCATE_BLOCK_HOLDERS v where v.ID_HOLDER = h.ID_HOLDER_PK and v.block_type <> :blockType) " +
						"AND had.IND_REPRESENTATIVE = :indRepre " +
						"group by " +
						"co.ID_ISSUER_FK,co.ID_ORIGIN_ISIN_CODE_FK,h.ID_HOLDER_PK,h.FULL_NAME,h.DOCUMENT_NUMBER,hab.ID_BANK_FK," +
						"hab.BANK_ACCOUNT_NUMBER,hab.BIC_CODE,co.CORPORATIVE_EVENT_TYPE,co.currency";

		Query query = em.createNativeQuery(strSQL);
		query.setParameter("lstCorporative",corporatives);
		query.setParameter("yesBicCode",BooleanType.YES.getCode());
		query.setParameter("indRepre",BooleanType.YES.getCode());
		query.setParameter("blockType", STR_BLOCK_TYPE_FOUR);
		lstResult = query.getResultList();

		return lstResult;
	}

	/**
	 * Metodo que identifica a los titulares que se aplicara retencion.
	 *
	 * @param lngCorporatives the lng corporatives
	 * @return the retention holders
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getRetentionHolders(List<Long> lngCorporatives) throws ServiceException{
		List<Object[]> lstResult = new ArrayList<Object[]>();
		StringBuilder strSQL = new StringBuilder(); 
		strSQL.append("	select	ID_ISSUER,");				//[0]
		strSQL.append("			ID_SECURITY,");				//[1]
		strSQL.append("			ID_HOLDER,");				//[2]
		strSQL.append("			HOLDER_NAME,");				//[3]
		strSQL.append("			DOCUMENT_NUMBER,");			//[4]
		strSQL.append("			ID_CORPORATIVE,");			//[5]
		strSQL.append("			AMOUNT,");					//[6]
		strSQL.append("			ID_BANK,");					//[7]
		strSQL.append("			ACCOUNT_NUMBER,");			//[8]
		strSQL.append("			BLOCK_TYPE,");				//[9]
		strSQL.append("			CORPORATIVE_TYPE,");		//[10]
		strSQL.append("			HOLDER_ACCOUNT_BANK_PK,");	//[11]
		strSQL.append("			IND_BENEFIT");				//[12]
		strSQL.append("			from V_ALLOCATE_BLOCK_HOLDERS where ID_CORPORATIVE in (:lstCorporative)");
		Query query = em.createNativeQuery(strSQL.toString());
		query.setParameter("lstCorporative",lngCorporatives);
		lstResult = query.getResultList();

		return lstResult;
	}
	
	/**
	 * Metodo que retorna beneficios bloqueados para el titular.
	 *
	 * @param lngCorporatives list id corporative
	 * @return block benefits
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getBlockBenefits(List<Long> lngCorporatives) throws ServiceException{
		List<Object[]> lstResult = new ArrayList<Object[]>();
		StringBuilder strSQL = new StringBuilder(); 
		strSQL.append("	select	vab.ID_HOLDER,");			//[1]
		strSQL.append("			hab.HAVE_BIC,");			//[2]
		strSQL.append("			vab.CORPORATIVE_TYPE,");	//[3]
		strSQL.append("			vab.AMOUNT");				//[4]
		strSQL.append("			from V_ALLOCATE_BLOCK_HOLDERS vab, HOLDER_ACCOUNT_DETAIL had,");
		strSQL.append("			CORPORATIVE_PROCESS_RESULT c, HOLDER_ACCOUNT_BANK hab, HOLDER_ACCOUNT ha");
		strSQL.append("			WHERE ID_CORPORATIVE in (:lstCorporative)");
		strSQL.append("			and vab.ID_CORPORATIVE = c.ID_CORPORATIVE_OPERATION_FK");
		strSQL.append("			AND hab.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK");
		strSQL.append("			and hab.ID_HOLDER_FK = vab.ID_HOLDER");
		strSQL.append("			AND had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK");
		strSQL.append("			and had.ID_HOLDER_FK = vab.ID_HOLDER");	
		strSQL.append("			and ha.ID_HOLDER_ACCOUNT_PK = c.ID_HOLDER_ACCOUNT_FK");		
		strSQL.append("			AND vab.BLOCK_TYPE = :blockType");
		strSQL.append("			AND vab.IND_BENEFIT = :no");
		strSQL.append("			AND had.IND_REPRESENTATIVE = :indRepre"); 
		strSQL.append("			GROUP BY vab.ID_HOLDER, hab.HAVE_BIC, vab.CORPORATIVE_TYPE, vab.AMOUNT");
		Query query = em.createNativeQuery(strSQL.toString());
		query.setParameter("lstCorporative",lngCorporatives);
		query.setParameter("no",BooleanType.NO.getCode());
		query.setParameter("indRepre",BooleanType.YES.getCode());
		query.setParameter("blockType", STR_BLOCK_TYPE_FOUR);
		lstResult = query.getResultList();
		return lstResult;
	}
	
	/**
	 * Metodo que retorna beneficios bloqueados para la entidad.
	 *
	 * @param lngCorporatives list id corporative
	 * @return Block Benefits For Entities
	 * @throws ServiceException  the service excpetion
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getBlockBenefitsForEntities(List<Long> lngCorporatives) throws ServiceException{
		List<Object[]> lstResult = new ArrayList<Object[]>();
		StringBuilder strSQL = new StringBuilder(); 
		strSQL.append("	select	ID_ISSUER,");				//[0]
		strSQL.append("			ID_SECURITY,");				//[1]
		strSQL.append("			ID_HOLDER,");				//[2]
		strSQL.append("			HOLDER_NAME,");				//[3]
		strSQL.append("			DOCUMENT_NUMBER,");			//[4]		
		strSQL.append("			AMOUNT,");					//[6]
		strSQL.append("			ID_BANK,");					//[7]
		strSQL.append("			ACCOUNT_NUMBER,");			//[8]
		strSQL.append("			BIC_CODE,");				//[9]
		strSQL.append("			CORPORATIVE_TYPE,");		//[10]
		strSQL.append("			CURRENCY");					//[11]
		strSQL.append("			from V_ALLOCATE_BLOCK_HOLDERS where ID_CORPORATIVE in (:lstCorporative)");
		strSQL.append("			AND BLOCK_TYPE = :blockType");
		strSQL.append("			AND BIC_CODE IS NOT NULL");
		strSQL.append("			AND IND_BENEFIT = :indBene");
		Query query = em.createNativeQuery(strSQL.toString());
		query.setParameter("lstCorporative",lngCorporatives);
		query.setParameter("indBene",BooleanType.NO.getCode());
		query.setParameter("blockType", STR_BLOCK_TYPE_FOUR);
		lstResult = query.getResultList();
		return lstResult;
	}
	
	/**
	 * Metodo que retorna lista de beneficios bloqueados para el titular.
	 *
	 * @param lngCorporatives list id corporative
	 * @return Block Benefits For Holders
	 * @throws ServiceException the service excpetion
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getBlockBenefitsForHolders(List<Long> lngCorporatives) throws ServiceException{
		List<Object[]> lstResult = new ArrayList<Object[]>();
		StringBuilder strSQL = new StringBuilder();
		strSQL.append("	select	ID_ISSUER,");				//[0]
		strSQL.append("			ID_SECURITY,");				//[1]
		strSQL.append("			ID_HOLDER,");				//[2]
		strSQL.append("			HOLDER_NAME,");				//[3]
		strSQL.append("			DOCUMENT_NUMBER,");			//[4]		
		strSQL.append("			AMOUNT,");					//[6]
		strSQL.append("			ID_BANK,");					//[7]
		strSQL.append("			ACCOUNT_NUMBER,");			//[8]
		strSQL.append("			CORPORATIVE_TYPE,");		//[10]
		strSQL.append("			HOLDER_ACCOUNT_BANK_PK");	//[11]
		strSQL.append("			from V_ALLOCATE_BLOCK_HOLDERS where ID_CORPORATIVE in (:lstCorporative)");
		strSQL.append("			AND BLOCK_TYPE = :blockType");
		strSQL.append("			AND BIC_CODE IS NULL");
		strSQL.append("			AND IND_BENEFIT = :indBene");
		Query query = em.createNativeQuery(strSQL.toString());
		query.setParameter("lstCorporative",lngCorporatives);
		query.setParameter("indBene",BooleanType.NO.getCode());
		query.setParameter("blockType", STR_BLOCK_TYPE_FOUR);
		lstResult = query.getResultList();
		return lstResult;
	}

	/**
	 * Metodo que retorna monto total a consignar.
	 *
	 * @param corporatives the corporatives
	 * @return the amount by corporatives
	 */
	public BigDecimal getAmountByCorporatives(List<Long> corporatives){
		BigDecimal processAmount = BigDecimal.ZERO;
		String strSQL = "select sum(co.paymentAmount) " +
						"from CorporativeOperation co " +
						"where co.idCorporativeOperationPk  in (:lstCorporative)";
		TypedQuery<BigDecimal> query = em.createQuery(strSQL,BigDecimal.class);
		query.setParameter("lstCorporative",corporatives);
		processAmount = query.getSingleResult();

		return processAmount;
	}

	/**
	 * Metodo que retorna cuenta efectivo centralizadora por moneda.
	 *
	 * @param issuer the issuer
	 * @param currency the currency
	 * @return the active centralizer issuer cash account
	 */
	public InstitutionCashAccount getActiveCentralizerBenefitPaymentIssuerCashAccount(String issuer, Integer currency){
		InstitutionCashAccount issuerCashAccount = new InstitutionCashAccount();
		TypedQuery<InstitutionCashAccount> query = em.createQuery(
				"SELECT i FROM InstitutionCashAccount i " +
				"WHERE " +
				"i.accountState = :state and " +
				"i.accountType = :accType and " +
				"i.issuer.idIssuerPk = :issuer and " +
				"i.currency = :currency " ,
//				"i.indRelatedBcrd = :indBCRD ",
				InstitutionCashAccount.class);

		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("accType",AccountCashFundsType.BENEFIT.getCode());
		query.setParameter("issuer",issuer);
		query.setParameter("currency",currency);
//		query.setParameter("indBCRD",BooleanType.YES.getCode());
		issuerCashAccount = query.getSingleResult();

		return issuerCashAccount;
	}
	
	public InstitutionBankAccount getInstitutionBankAccountBenefitPaymentSending(Long institutionCashAccount){
		InstitutionBankAccount issuerCashAccount = new InstitutionBankAccount();
		TypedQuery<InstitutionBankAccount> query = em.createQuery(
				" SELECT iba FROM InstitutionBankAccount iba, CashAccountDetail cad, InstitutionCashAccount ica " +
				" WHERE " +
				" iba.idInstitutionBankAccountPk = cad.institutionBankAccount.idInstitutionBankAccountPk " +
				" AND ica.idInstitutionCashAccountPk = cad.institutionCashAccount.idInstitutionCashAccountPk " +
				" AND cad.indSending = 1 " +
				" AND ica.idInstitutionCashAccountPk = :idInstitutionCashAccountPk " ,
				InstitutionBankAccount.class);

		query.setParameter("idInstitutionCashAccountPk",institutionCashAccount);
		issuerCashAccount = query.getSingleResult();

		return issuerCashAccount;
	}

	/**
	 * Metodo que retorna numero de cuenta bancaria por moneda.
	 *
	 * @param bank the bank
	 * @param currency the currency
	 * @return the bank account number
	 */
	public String getBankAccountNumber(Long bank, Integer currency) {
		String bankAccountNumber = StringUtils.EMPTY;
		try{
			StringBuffer stringBuffer= new StringBuffer();
			stringBuffer.append(" SELECT ICA.accountNumber FROM InstitutionBankAccount ICA ");
			stringBuffer.append(" WHERE ICA.currency = :currency ");
			stringBuffer.append(" and ICA.state = :icaState ");
			stringBuffer.append(" and ICA.bank.idBankPk = :bank ");
			stringBuffer.append(" and ICA.bank.state = :bankState ");
			stringBuffer.append(" and ICA.providerBank.bankType = :bankType ");
			
			TypedQuery<String> query = em.createQuery(stringBuffer.toString(),String.class);
			query.setParameter("currency",currency);
			query.setParameter("bankState",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
			query.setParameter("icaState",BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("bankType",BankType.CENTRALIZING.getCode());
			query.setParameter("bank",bank);

			bankAccountNumber = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return bankAccountNumber;
	}

	/**
	 * Metodo que retorna cuenta efectivo de devolucion por moneda.
	 *
	 * @param currency the currency
	 * @return the devolution commercial account
	 */
	public InstitutionCashAccount getDevolutionCommercialAccount(Integer currency) {
		InstitutionCashAccount cashAccount = null;
		try{
			String queryStr = 	"select i from InstitutionCashAccount i " +
					"where " +
					"i.accountType = :accountType " +
					"and i.accountState = :state " +
					"and i.currency = :currency " +
					"and i.indRelatedBcrd = :indBCRD ";
			TypedQuery<InstitutionCashAccount> query = em.createQuery(queryStr,InstitutionCashAccount.class);
			query.setParameter("accountType",AccountCashFundsType.RETURN.getCode());
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("currency",currency);
			query.setParameter("indBCRD",BooleanType.NO.getCode());
			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return cashAccount;
	}
	
	/**
	 * Metodo que valida cantidad de cuenta bancaria relacionada a la cuenta efectivo de devoluciones activos por moneda.
	 *
	 * @param currency currency
	 * @return Devolution Commercial Bank Account
	 */
	public boolean validateDevolutionCommercialBankAccount(Integer currency) {
		boolean validate = false;
		Long count = null;
		try{
			StringBuilder queryStr = new StringBuilder(); 
			queryStr.append(" SELECT count(CAD.institutionBankAccount) FROM CashAccountDetail CAD ");
			queryStr.append(" WHERE CAD.accountState = :cadState ");
			queryStr.append(" and CAD.indReception = :indicatorOne ");
			queryStr.append(" and CAD.institutionCashAccount.indRelatedBcrd = :indicatorZero ");
			queryStr.append(" and CAD.institutionCashAccount.indAutomaticProcess = :indicatorOne ");
			queryStr.append(" and CAD.institutionCashAccount.currency = :currency ");
			queryStr.append(" and CAD.institutionCashAccount.accountType = :accountType ");
			queryStr.append(" and CAD.institutionCashAccount.accountState = :icaState ");
			queryStr.append(" and CAD.institutionBankAccount.state = :ibaState ");
			
			TypedQuery<Long> query = em.createQuery(queryStr.toString(),Long.class);
			query.setParameter("accountType",AccountCashFundsType.RETURN.getCode());
			query.setParameter("ibaState", BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("icaState",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("cadState",DetailCashAccountStateType.ACTIVATE.getCode());
			query.setParameter("currency",currency);
			query.setParameter("indicatorZero",BooleanType.NO.getCode());
			query.setParameter("indicatorOne",BooleanType.YES.getCode());
			count = query.getSingleResult();
			if(count!=null && count>0){
				validate = true;
			}
			return validate;
		}catch(NoResultException x){
			x.printStackTrace();
			return validate;
		}
	}
	
	/**
	 * Metodo que retorna cuenta bancaria relacionada a la cuenta efectivo devoluciones activa por moneda.
	 *
	 * @param currency currency
	 * @return Devolution Commercial Bank Account
	 */
	public InstitutionBankAccount getDevolutionCommercialBankAccount(Integer currency) {
		try{
			StringBuilder queryStr = new StringBuilder(); 
			queryStr.append(" SELECT CAD.institutionBankAccount FROM CashAccountDetail CAD ");
			queryStr.append(" WHERE CAD.accountState = :cadState ");
			queryStr.append(" and CAD.indReception = :indicatorOne ");
			queryStr.append(" and CAD.institutionCashAccount.indRelatedBcrd = :indicatorZero ");
			queryStr.append(" and CAD.institutionCashAccount.indAutomaticProcess = :indicatorOne ");
			queryStr.append(" and CAD.institutionCashAccount.currency = :currency ");
			queryStr.append(" and CAD.institutionCashAccount.accountType = :accountType ");
			queryStr.append(" and CAD.institutionCashAccount.accountState = :icaState ");
			queryStr.append(" and CAD.institutionBankAccount.state = :ibaState ");
			
			TypedQuery<InstitutionBankAccount> query = em.createQuery(queryStr.toString(),InstitutionBankAccount.class);
			query.setParameter("accountType",AccountCashFundsType.RETURN.getCode());
			query.setParameter("ibaState", BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("icaState",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("cadState",DetailCashAccountStateType.ACTIVATE.getCode());
			query.setParameter("currency",currency);
			query.setParameter("indicatorZero",BooleanType.NO.getCode());
			query.setParameter("indicatorOne",BooleanType.YES.getCode());
			return query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
			return null;
		}
	}
	
	public InstitutionBankAccount getInstitutionBankAccountJoinBank(Long idInstitutionBankAccountPk) {
		try{
			StringBuilder queryStr = new StringBuilder(); 
			queryStr.append(" SELECT iba ");
			queryStr.append(" FROM InstitutionBankAccount iba ");
			queryStr.append(" JOIN FETCH iba.providerBank b ");
			queryStr.append(" WHERE iba.idInstitutionBankAccountPk = :idInstitutionBankAccountPk ");
			queryStr.append(" and iba.state = :ibaState ");
			
			TypedQuery<InstitutionBankAccount> query = em.createQuery(queryStr.toString(),InstitutionBankAccount.class);
			query.setParameter("ibaState", BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("idInstitutionBankAccountPk",idInstitutionBankAccountPk);
			return query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
			return null;
		}
	}
	
	public HolderAccountBank getHolderAccountBankJoinBank(Long idHolderAccountPk, Integer currency) {
		try{
			StringBuilder queryStr = new StringBuilder(); 
			queryStr.append(" SELECT hab ");
			queryStr.append(" FROM HolderAccountBank hab ");
			queryStr.append(" JOIN FETCH hab.bank b ");
			queryStr.append(" WHERE hab.stateAccountBank = :stateAccountBank ");
			queryStr.append(" and hab.currency = :currency ");
			queryStr.append(" and hab.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
			
			TypedQuery<HolderAccountBank> query = em.createQuery(queryStr.toString(),HolderAccountBank.class);
			query.setParameter("stateAccountBank", HolderAccountStateBankType.REGISTERED.getCode());
			query.setParameter("currency",currency);
			query.setParameter("idHolderAccountPk",idHolderAccountPk);
			
			List<HolderAccountBank> lstHolderAccountBank = query.getResultList();
			
			if( lstHolderAccountBank!=null && lstHolderAccountBank.size()> 0 ) {
				return lstHolderAccountBank.get(0);
			}else {
				return null;
			}
		}catch(NoResultException x){
			x.printStackTrace();
			return null;
		}
	}
	/**
	 * Metodo que retorna cuenta efectivo de devoluciones activo.
	 *
	 * @param idInstitutionBankAccount id institution bank account
	 * @return Institution Cash Account
	 */
	public InstitutionCashAccount getDevolutionInstitutionCashAccount(Long idInstitutionBankAccount) {
		try{
			StringBuilder queryStr = new StringBuilder(); 
			queryStr.append(" SELECT CAD.institutionCashAccount FROM CashAccountDetail CAD ");
			queryStr.append(" WHERE CAD.institutionBankAccount.idInstitutionBankAccountPk = :idInstitutionBankAccount ");
			queryStr.append(" and CAD.institutionCashAccount.accountType = :accountType ");
			queryStr.append(" and CAD.institutionCashAccount.accountState = :icaState ");
			
			TypedQuery<InstitutionCashAccount> query = em.createQuery(queryStr.toString(),InstitutionCashAccount.class);
			query.setParameter("idInstitutionBankAccount",idInstitutionBankAccount);
			query.setParameter("accountType",AccountCashFundsType.RETURN.getCode());
			query.setParameter("icaState",CashAccountStateType.ACTIVATE.getCode());

			return query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Metodo que retorna codigo y tipo de proceso corporativo.
	 *
	 * @param lngCorporatives list corporative
	 * @return Grouped Corporative Details
	 */
	public List<Object[]> getGroupedCorporativeDetails(List<Long> lngCorporatives){
		List<Object[]> lstObject = new ArrayList<Object[]>();
		String queryStr = 
				"SELECT " +
				"C.idCorporativeOperationPk," +			//[0]
				"C.corporativeEventType.corporativeEventType " +				//[1]
				"FROM CorporativeOperation C " +
				"WHERE C.idCorporativeOperationPk IN (:lngCorporatives) " +
				"ORDER BY C.corporativeEventType.corporativeEventType ";
		TypedQuery<Object[]> query = em.createQuery(queryStr,Object[].class);
		query.setParameter("lngCorporatives",lngCorporatives);
		lstObject = query.getResultList();

		return lstObject;
	}

	/**
	 * Metodo que actualiza indicador de pagado al proceso corporativo.
	 *
	 * @param lngCorporatives the lng corporatives
	 * @param allocationPaymentBenefit the allocation payment benefit
	 */
	public void updatePaidIndicator(List<Long> lngCorporatives, Long allocationPaymentBenefit){
		String queryStr = 	"update CorporativeOperation c set c.indPayed = :indPayed, c.benefitPaymentAllocation.idBenefitPaymentPK = :allocationPayment " +
							"where c.idCorporativeOperationPk in ( :corporatives ) ";
		Query query = em.createQuery(queryStr);
		query.setParameter("indPayed",BooleanType.YES.getCode());
		query.setParameter("corporatives",lngCorporatives);
		query.setParameter("allocationPayment",allocationPaymentBenefit);
		query.executeUpdate();
	}

	/**
	 * Metodo que retorna lista de estado de procesos corporativos (preliminar - definitivo).
	 *
	 * @return the benefit corporative states
	 */
	private List<Integer> getBenefitCorporativeStates(){
		List<Integer> lstCorporativeState = new ArrayList<Integer>();
		lstCorporativeState.add(CorporateProcessStateType.PRELIMINARY.getCode());
		lstCorporativeState.add(CorporateProcessStateType.DEFINITIVE.getCode());

		return lstCorporativeState;
	}

	/**
	 * Metodo que retorna lista de montos de retencion de los procesos corporativos.
	 *
	 * @param lngCorporatives the lng corporatives
	 * @return the retention amounts
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getRetentionAmounts(List<Long> lngCorporatives){
		String queryStr = 
				"select NVL(sum(commissionAmount),0)," +									//[0]
				"		NVL(sum(taxAmount),0)," +											//[1]
				"		NVL(sum(custodyAmount),0)," +										//[2]
				"		c.issuer.idIssuerPk" +												//[3]
				"	from CorporativeOperation c " +
				"	where c.idCorporativeOperationPk in (:corporatives)" +
				"	GROUP BY c.issuer.idIssuerPk";
		Query query = em.createQuery(queryStr);
		query.setParameter("corporatives",lngCorporatives);
		return (List<Object[]>)query.getResultList();
	}

	/**
	 * Metodo que retorna cuentas efectivos asociados a la retencion.
	 *
	 * @param cashAccountType the cash account type
	 * @param curreny the curreny
	 * @return the commercial inst cash account auto process
	 */
	public InstitutionCashAccount getCommercialInstCashAccountAutoProcess(Integer cashAccountType, Integer curreny){
		InstitutionCashAccount cashAccount = null;
		try{
			StringBuilder sbQuery = new StringBuilder(); 
			sbQuery.append("select i from InstitutionCashAccount i ");
			sbQuery.append("	where i.accountType = :type and ");
			sbQuery.append("	i.currency = :currency and ");
			//sbQuery.append("	i.indAutomaticProcess = :indAutomaticProcess and ");
			sbQuery.append("	i.accountState = :state ");
			TypedQuery<InstitutionCashAccount> query = em.createQuery(sbQuery.toString(),InstitutionCashAccount.class);
			query.setParameter("type",cashAccountType);
			query.setParameter("currency",curreny);
			//query.setParameter("indAutomaticProcess",BooleanType.YES.getCode());
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return cashAccount;
	}
	
	/**
	 * Metodo que valida cantidad de cuentas efectivo por tipo cuenta y moneda.
	 *
	 * @param cashAccountType cash Account Type
	 * @param curreny currency
	 * @return Commercial Institution Cash Account Auto Process
	 */
	public boolean validateCommercialInstCashAccountAutoProcess(Integer cashAccountType, Integer curreny){
		boolean validate = false;
		Long count = null;
		try{
			StringBuilder sbQuery = new StringBuilder(); 
			sbQuery.append("select count(i) from InstitutionCashAccount i ");
			sbQuery.append("	where i.accountType = :type and ");
			sbQuery.append("	i.currency = :currency and ");
			sbQuery.append("	i.indAutomaticProcess = :indAutomaticProcess and ");
			sbQuery.append("	i.accountState = :state");
			TypedQuery<Long> query = em.createQuery(sbQuery.toString(),Long.class);
			query.setParameter("type",cashAccountType);
			query.setParameter("currency",curreny);
			query.setParameter("indAutomaticProcess",BooleanType.YES.getCode());
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			count = query.getSingleResult();
			if(count!=null && count>0){
				validate = true;
			}
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return validate;
	}
	
	/**
	 * Metodo que retorna detalle del proceso corporativo por emisor y moneda.
	 *
	 * @param issuer the issuer
	 * @param currency the currency
	 * @return the corporatives detail
	 */
	public List<Object[]> getCorporativesDetail(String issuer,Integer currency){
		List<Object[]> lstObject = new ArrayList<Object[]>();
		List<Integer> lstCorporativeType = getBenefitCorporativeType();
		String queryStr = 
				"select " +
				"co.idCorporativeOperationPk," +													//[0]
				"co.securities.idIsinCodePk," +														//[1]
				"NVL(co.paymentAmount,0)," +														//[2]
				"NVL(co.custodyAmount,0)," +														//[3]
				"NVL(co.taxAmount,0)," +															//[4]
				"NVL(co.commissionAmount,0)," +														//[5]
				"p.description," +																	//[6]
				"co.indPayed, " +																	//[7]
				"co.corporativeEventType.corporativeEventType " +														//[8]
				"from CorporativeOperation co, ParameterTable p " +
				"where " +
				"trunc(co.deliveryDate) = trunc(:date) " +
				"and co.corporativeEventType.corporativeEventType in (:eventType) " +
				"and p.parameterTablePk = co.state ";
		if(Validations.validateIsNotNullAndNotEmpty(issuer)){
			queryStr += "and co.issuer.idIssuerPk = :issuer ";
		}
		if(Validations.validateIsNotNullAndNotEmpty(currency)){
			queryStr += "and co.currency = :currency ";
		}
		TypedQuery<Object[]> query = em.createQuery(queryStr,Object[].class);
		query.setParameter("date",CommonsUtilities.currentDate());
		query.setParameter("eventType",lstCorporativeType);
		if(Validations.validateIsNotNullAndNotEmpty(issuer)){
			query.setParameter("issuer",issuer);
		}
		if(Validations.validateIsNotNullAndNotEmpty(currency)){
			query.setParameter("currency",currency);
		}
		lstObject = query.getResultList();

		return lstObject;
	}
	
	/**
	 * Metodo que retorna consignacion de pagos por filtros respectivos.
	 *
	 * @param objSearch Benefit Allocation Process
	 * @return list Benefit Allocation Process
	 */
	public List<BenefitPaymentAllocation> getBenefitAllocationProcess(AllocationPaymentSearchTO objSearch) {
			String queryStr = 	
					" select bpa from BenefitPaymentAllocation bpa " +
					" join fetch bpa.issuer i " +
					" where " +
					" trunc(bpa.allocationProcessDate) = trunc(:processDate) " +
					" and bpa.currency = :currency ";
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer()) && 
					Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer().getIdIssuerPk())){
				queryStr = queryStr.concat("and bpa.issuer.idIssuerPk = :issuer ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getSecurity()) && 
					Validations.validateIsNotNullAndNotEmpty(objSearch.getSecurity().getIdSecurityCodePk())){
				queryStr = queryStr.concat("and bpa.security.idSecurityCodePk = :security ");
			}

			TypedQuery<BenefitPaymentAllocation> query = em.createQuery(queryStr,BenefitPaymentAllocation.class);
			query.setParameter("processDate",objSearch.getPaymentDate());
			query.setParameter("currency",objSearch.getCurrency());
			
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer()) && 
					Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer().getIdIssuerPk())){
				query.setParameter("issuer",objSearch.getIssuer().getIdIssuerPk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getSecurity()) && 
					Validations.validateIsNotNullAndNotEmpty(objSearch.getSecurity().getIdSecurityCodePk())){
				query.setParameter("security",objSearch.getSecurity().getIdSecurityCodePk());
			}

			return query.getResultList();

	}
	
	/**
	 * Metodo que retorna proceso corporativos por filtros respectivos.
	 *
	 * @param objSearch Allocate Corporative
	 * @return list Allocate Corporative
	 */
	public List<CorporativeOperation> getAllocateCorporative(AllocationPaymentSearchTO objSearch){
			String queryStr = 	
					"select co from CorporativeOperation co "
					+ "where "
					+ "trunc(co.deliveryDate) = trunc(:processDate) "
					+ "and (co.currency = :currency ) "	//or nvl(co.currencyPayment,0) = :currency 
					+ "and co.corporativeEventType.corporativeEventType in (:corporativeType) "
					+ "and not exists "
					+ "(select 1 from PaymentAllocationDetail pad where pad.corporativeOperation.idCorporativeOperationPk = co.idCorporativeOperationPk ) "
					+ "and co.state = :state "
					+ "and co.securities.indPaymentBenefit = :indPaymentBenefit "
					+ "and nvl(co.indPayed,0) = :indPayed "
					+ "and nvl(co.paymentAmount,0) > :paymentAmount ";
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer()) && 
					Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer().getIdIssuerPk())){
				queryStr = queryStr.concat("and co.issuer.idIssuerPk = :issuer ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getSecurity()) && 
					Validations.validateIsNotNullAndNotEmpty(objSearch.getSecurity().getIdSecurityCodePk())){
				queryStr = queryStr.concat("and co.securities.idSecurityCodePk = :security ");
			}
			queryStr = queryStr.concat("order by co.issuer.idIssuerPk ");

			TypedQuery<CorporativeOperation> query = em.createQuery(queryStr,CorporativeOperation.class);
			query.setParameter("processDate",objSearch.getPaymentDate());
			query.setParameter("currency",objSearch.getCurrency());
			query.setParameter("corporativeType",getBenefitCorporativeType());
			
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer()) && 
			Validations.validateIsNotNullAndNotEmpty(objSearch.getIssuer().getIdIssuerPk())){
				query.setParameter("issuer",objSearch.getIssuer().getIdIssuerPk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objSearch.getSecurity()) && 
					Validations.validateIsNotNullAndNotEmpty(objSearch.getSecurity().getIdSecurityCodePk())){
				query.setParameter("security",objSearch.getSecurity().getIdSecurityCodePk());
			}
			query.setParameter("state",CorporateProcessStateType.DEFINITIVE.getCode());
			query.setParameter("indPaymentBenefit",BooleanType.YES.getCode());
			query.setParameter("indPayed",BooleanType.NO.getCode());
			query.setParameter("paymentAmount",BigDecimal.ZERO);
			return query.getResultList();
	}
	
	/**
	 * Metodo que retorna lista de detalle de la consignacion de pagos.
	 *
	 * @param allocationProcessID id allocation Process
	 * @return Corporatives By Allocation Process
	 */
	public List<PaymentAllocationDetail> getCorporativesByAllocationProcess(Integer allocationProcessID){
		List<PaymentAllocationDetail> allocationDetailList = new ArrayList<PaymentAllocationDetail>();
		try{
			String queryStr = 
								"select pad from "
								+ "PaymentAllocationDetail pad "
								+ "inner join fetch pad.corporativeOperation "
								+ "inner join fetch pad.corporativeOperation.securities "
								+ "where "
								+ "pad.benefitPaymentAllocation.idBenefitPaymentPK = :allocationProcess ";
			TypedQuery<PaymentAllocationDetail> query = em.createQuery(queryStr,PaymentAllocationDetail.class);
			query.setParameter("allocationProcess",allocationProcessID);
			allocationDetailList = query.getResultList();

		}catch(NoResultException x){
			x.printStackTrace();
		}
		return allocationDetailList;
	}
	
	public List<CorporativeProcessResult> getCorporativeResulByIssuer(AllocationProcessIssuerResumeTO allocateProcessIssuer,Integer indPayed, Date paymentDate, Integer paymentCurrency){//List<Long> lstCorporativeProcessResultBlocked
		
		String strQuery = 
		" SELECT distinct cpr FROM CorporativeProcessResult cpr "
				+ " JOIN FETCH cpr.corporativeOperation co "
				+ " JOIN FETCH cpr.participant p "
				+ " WHERE 1 = 1 "
				+ " AND cpr.indOriginTarget = :indOriginTarget "
				+ " AND co.issuer.idIssuerPk = :issuer "
				+ " AND nvl(co.indPayed,0) = :indPayed "
				+ " AND trunc(co.deliveryDate) = trunc(:paymentDate) "
				+ " AND co.state = :corporateState "
				+ " AND co.currency = :paymentCurrency "
				+ " AND not exists "
				+ " (select 1 from PaymentAllocationDetail pad where pad.corporativeOperation.idCorporativeOperationPk = co.idCorporativeOperationPk ) "
				+ "  AND nvl(co.paymentAmount,0) > :paymentAmount ";

		TypedQuery<CorporativeProcessResult> query = em.createQuery(strQuery,CorporativeProcessResult.class);
		
		query.setParameter("issuer",allocateProcessIssuer.getIssuer());
		query.setParameter("indPayed",indPayed);
		query.setParameter("paymentDate",paymentDate);
		query.setParameter("paymentCurrency",paymentCurrency);
		query.setParameter("corporateState",CorporateProcessStateType.DEFINITIVE.getCode());
		query.setParameter("paymentAmount",BigDecimal.ZERO);
		query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);

		return query.getResultList();
	}	

	/**
	 * Metodo que retorna lista de procesos corporativos por emisor.
	 *
	 * @param allocateProcessIssuer the allocate process issuer
	 * @param indPayed the ind payed
	 * @param paymentDate the payment date
	 * @return the corporatives by issuer
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> getCorporativesByIssuer(AllocationProcessIssuerResumeTO allocateProcessIssuer,Integer indPayed, Date paymentDate, Integer paymentCurrency) 
			throws ServiceException{
			String queryStr = 
								"select co "
								+ "from CorporativeOperation co "
								+ "inner join fetch co.securities "
								+ "inner join fetch co.issuer "
								+ "where "
								+ "co.issuer.idIssuerPk = :issuer "
								+ "and nvl(co.indPayed,0) = :indPayed "
								+ "and trunc(co.deliveryDate) = trunc(:paymentDate) "
								+ "and co.state = :corporateState "
								+ "and co.currency = :paymentCurrency "
								+ "and not exists "
								+ "(select 1 from PaymentAllocationDetail pad where pad.corporativeOperation.idCorporativeOperationPk = co.idCorporativeOperationPk ) "
								+ "and nvl(co.paymentAmount,0) > :paymentAmount ";
	
			TypedQuery<CorporativeOperation> query = em.createQuery(queryStr,CorporativeOperation.class);
			query.setParameter("issuer",allocateProcessIssuer.getIssuer());
			query.setParameter("indPayed",indPayed);
			query.setParameter("paymentDate",paymentDate);
			query.setParameter("paymentCurrency",paymentCurrency);
			query.setParameter("corporateState",CorporateProcessStateType.DEFINITIVE.getCode());
			query.setParameter("paymentAmount",BigDecimal.ZERO);
			return query.getResultList();
	}
	
	/**
	 * Metodo que retorna lista detalle de las cuentas asociadas la proceso corporativo.
	 *
	 * @param corporatives id corporativos
	 * @return list Holder Accounts By Corporatives
	 */
	public List<Object[]> getHolderAccountsByCorporatives(List<Long> corporatives){
		List<Object[]> holderAccounts = new ArrayList<Object[]>();
			String strQuery = 
						"select ha.idHolderAccountPk,ha.stateAccount from HolderAccount ha, CorporativeProcessResult cpr "
						+ "where "
						+ "cpr.corporativeOperation.idCorporativeOperationPk in (:corporatives) "
						+ "and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and cpr.indOriginTarget = :indOriginTarget ";
			TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
			query.setParameter("corporatives",corporatives);
			query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);
			holderAccounts = query.getResultList();
		return holderAccounts;
	}
	
	/**
	 * Metodo que retorna lista detalle de titulares asociados al proceso corporativo.
	 *
	 * @param corporatives list corporative
	 * @param blockHolderAccounts list blockHolderAccounts
	 * @return list Holder By Corporative
	 */
	public List<Object[]> getHolderByCorporatives(List<Long> corporatives, List<Long> blockHolderAccounts){
			String strQuery = 
						"select h.idHolderPk,h.stateHolder "
						+ "from CorporativeOperation co, CorporativeProcessResult cpr, HolderAccount ha, HolderAccountDetail had, Holder h "
						+ "where "
						+ "co.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
						+ "and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and had.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and had.holder.idHolderPk = h.idHolderPk "
						+ "and ha.participant.idParticipantPk = cpr.participant.idParticipantPk "
						+ "and had.stateAccountDetail = :accountDetailState "
						+ "and had.indRepresentative = :indRepresentant "
						+ "and cpr.indOriginTarget = :indOriginTarget "
						+ "and co.idCorporativeOperationPk in (:corporatives) ";
			if(Validations.validateListIsNotNullAndNotEmpty(blockHolderAccounts)){
				strQuery = strQuery.concat("and ha.idHolderAccountPk not in (:holderAccounts) ");
			}

			TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
			if(Validations.validateListIsNotNullAndNotEmpty(blockHolderAccounts)){
				query.setParameter("holderAccounts",blockHolderAccounts);
			}
			query.setParameter("corporatives",corporatives);
			query.setParameter("indRepresentant",BooleanType.YES.getCode());
			query.setParameter("accountDetailState",HolderAccountStateDetailType.REGISTERED.getCode());
			query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);
			return query.getResultList();
	}
	
	/**
	 * Metodo que retorna lista de cuentas asociados al proceso corporativo.
	 *
	 * @param corporatives list corporative
	 * @param blockHolderAccounts list block Holder Accounts
	 * @param blockedHolders list blocked Holders
	 * @param currency currency
	 * @return list Bank Accounts By Corporative
	 */
	public List<Long> getBankAccountsByCorporative(List<Long> corporatives, List<Long> blockHolderAccounts, List<Long> blockedHolders, Integer currency){
		List<Long> holderAccountBanks = new ArrayList<Long>();
		try{
			String strQuery = 
						"select distinct had.holderAccount.idHolderAccountPk "
						+ "from CorporativeOperation co, CorporativeProcessResult cpr, HolderAccount ha, HolderAccountDetail had, Holder h "
						+ "where "
						+ "co.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
						+ "and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and had.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and had.holder.idHolderPk = h.idHolderPk "
						+ "and ha.participant.idParticipantPk = cpr.participant.idParticipantPk "
						+ "and co.idCorporativeOperationPk in (:corporatives) "
						+ "and had.indRepresentative = :indRepresentant "
						+ "and had.stateAccountDetail = :accountDetailState "
						+ "and cpr.indOriginTarget = :indOriginTarget "
						+ "and not exists "
						+ "(select 1 from HolderAccountBank hab1 where hab1.currency = co.currency and hab1.holderAccount.idHolderAccountPk = ha.idHolderAccountPk and "
						+ "hab1.holder.idHolderPk = h.idHolderPk) ";

			if(Validations.validateListIsNotNullAndNotEmpty(blockHolderAccounts)){
				strQuery = strQuery.concat("and ha.idHolderAccountPk not in (:holderAccounts) ");
			}
			if(Validations.validateListIsNotNullAndNotEmpty(blockedHolders)){
				strQuery = strQuery.concat("and h.idHolderPk not in (:holders) ");
			}

			TypedQuery<Long> query = em.createQuery(strQuery,Long.class);
			if(Validations.validateListIsNotNullAndNotEmpty(blockHolderAccounts)){
				query.setParameter("holderAccounts",blockHolderAccounts);
			}
			if(Validations.validateListIsNotNullAndNotEmpty(blockedHolders)){
				query.setParameter("holders",blockedHolders);
			}

			query.setParameter("corporatives",corporatives);
			query.setParameter("indRepresentant",BooleanType.YES.getCode());
			query.setParameter("accountDetailState",HolderAccountStateDetailType.REGISTERED.getCode());
			query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);

			holderAccountBanks = query.getResultList();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return holderAccountBanks;
	}
	
	/**
	 * Metodo que retorna lista de beneficios bloqueados asociados al procesos corporativo.
	 *
	 * @param lstCorporatives list corporative
	 * @param lstRetentionHolderAccount list Retention Holder Account
	 * @param lstRetentionHolder list Retention Holder
	 * @param retentionBankAccounts list retention Bank Accounts
	 * @param currency currency
	 * @return list Block Retention Corporative Result
	 */
	/*
		List<Long> lstRetentionHolder, List<Long> retentionBankAccounts, 
	 */
	public List<BlockCorporativeResult> getBlockRetentionCorporativeResult(List<Long> lstCorporatives, List<Long> lstRetentionHolderAccount){
			String strQuery = 
			" SELECT distinct bcr FROM BlockCorporativeResult bcr "//CorporativeOperation co, CorporativeProcessResult cpr, HolderAccount ha, 
			+ " JOIN FETCH bcr.corporativeProcessResult cpr "
			+ " JOIN FETCH bcr.corporativeOperation co "
			+ " JOIN FETCH cpr.holderAccount ha "
			+ " WHERE co.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
			+ " and cpr.indOriginTarget = :indOriginTarget "
			+ " and co.idCorporativeOperationPk in (:corporatives) ";

			if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
				strQuery = strQuery.concat("and ha.idHolderAccountPk not in (:holderAccounts) ");
			}

			TypedQuery<BlockCorporativeResult> query = em.createQuery(strQuery,BlockCorporativeResult.class);
			if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
				query.setParameter("holderAccounts",lstRetentionHolderAccount);
			}
			
			query.setParameter("corporatives",lstCorporatives);
			query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);

			return query.getResultList();
	}
	
	public RegisterAllocationProcess registerAllocationProcess(List<Long> lstCorporatives, boolean commissionExcluded, Integer currency, String issuer, String security, String userID) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		//holder accounts que no van a cobrar - que se le retiene el pago por bloqueo de cuenta titular
		List<Long> lstRetentionHolderAccount = new ArrayList<Long>();
		
		
		//titulares que fueron bloqueados 
		List<Object[]> holderAccountList = getHolderAccountsByCorporatives(lstCorporatives);
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccountList)){
		for(Object[] holderAccount : holderAccountList){
			Long holderAccountPK = Long.valueOf(holderAccount[0].toString());
			Integer holderAccountState = Integer.valueOf(holderAccount[1].toString());
			if(!HolderAccountStatusType.ACTIVE.getCode().equals(holderAccountState)){
				lstRetentionHolderAccount.add(holderAccountPK);
			}
		}
		}
		
		//lista de result de corporativos bloqueados, exceptuando los titulares que ya se encuentran bloqueados
		List<BlockCorporativeResult> blockCorporativeResultWithoutHolderAccountBlocked = getBlockRetentionCorporativeResult(lstCorporatives,lstRetentionHolderAccount);
		
		List<CorporativeProcessResult> corporativeResultWithoutHolderAccountBlocked = null; 
		if( blockCorporativeResultWithoutHolderAccountBlocked!=null && blockCorporativeResultWithoutHolderAccountBlocked.size()> 0) {
			corporativeResultWithoutHolderAccountBlocked = new ArrayList<CorporativeProcessResult>();
			for(BlockCorporativeResult blockCPR: blockCorporativeResultWithoutHolderAccountBlocked) {
				corporativeResultWithoutHolderAccountBlocked.add(blockCPR.getCorporativeProcessResult());
			}
		}
		
		//lista de result de corporativos, de los titulares que ya se encuentran bloqueados (esto incluye CPR bloqueados y no bloqueados, solo es a nivel de titular)
		List<CorporativeProcessResult> corporativeResultWithHolderAccountBlocked = getCorporativeResultWithHolderAccountBlock(lstCorporatives,lstRetentionHolderAccount);
		
		List<Long> lstIdCorporativeProcessResultBloq = null;
		List<CorporativeProcessResult> lstCorporativeResultBlocked = null; 
		if( (corporativeResultWithoutHolderAccountBlocked!=null && corporativeResultWithoutHolderAccountBlocked.size()>0) || (corporativeResultWithHolderAccountBlocked!=null && corporativeResultWithHolderAccountBlocked.size()>0)) {
			lstCorporativeResultBlocked = new ArrayList<CorporativeProcessResult>();
			lstIdCorporativeProcessResultBloq = new ArrayList<Long>();
			
			if(corporativeResultWithoutHolderAccountBlocked!=null && corporativeResultWithoutHolderAccountBlocked.size()>0) {
				for(CorporativeProcessResult cpr: corporativeResultWithoutHolderAccountBlocked) {
					lstCorporativeResultBlocked.add(cpr);	
					lstIdCorporativeProcessResultBloq.add(cpr.getIdCorporativeProcessResult());
				}
			}
			
			if(corporativeResultWithHolderAccountBlocked!=null && corporativeResultWithHolderAccountBlocked.size()>0) {
				for(CorporativeProcessResult cpr: corporativeResultWithHolderAccountBlocked) {
					lstCorporativeResultBlocked.add(cpr);
					lstIdCorporativeProcessResultBloq.add(cpr.getIdCorporativeProcessResult());
				}
			}
			
		}
		
		//corporativos no blqoueados
		List<CorporativeProcessResult> corporativeResultWithOutCorporativeProcessResultBlock = 
				getCorporativeResultWithOutCorporativeProcessResultBlock(lstCorporatives, lstRetentionHolderAccount);
		
		
		BenefitPaymentAllocation benefitPaymentAllocation = saveAllocationProcess(issuer,security,currency,lstCorporatives,commissionExcluded);
		
		RegisterAllocationProcess registerAllocationProcess = new RegisterAllocationProcess();
		registerAllocationProcess.setIdBenefitPaymentPK(benefitPaymentAllocation.getIdBenefitPaymentPK());
		registerAllocationProcess.setLstCorporativeResultBlocked(lstCorporativeResultBlocked);
		registerAllocationProcess.setCorporativeResultWithOutCorporativeProcessResultBlock(corporativeResultWithOutCorporativeProcessResultBlock);
		registerAllocationProcess.setCurrency(currency);
		
		return registerAllocationProcess;
	}
	
	public void createPaymentAllocationDetail(RegisterAllocationProcess registerAllocationProcess) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		Long idBenefitPaymentPK = registerAllocationProcess.getIdBenefitPaymentPK();
		List<CorporativeProcessResult> corporativeResultWithOutCorporativeProcessResultBlock = registerAllocationProcess.getCorporativeResultWithOutCorporativeProcessResultBlock();;
		List<CorporativeProcessResult> lstCorporativeResultBlocked = registerAllocationProcess.getLstCorporativeResultBlocked();
		Integer currency = registerAllocationProcess.getCurrency();
		
		BenefitPaymentAllocation benefitPaymentAllocation = find(idBenefitPaymentPK, BenefitPaymentAllocation.class);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeResultBlocked)){
			//generateDetailBlockRetention(lstCorporativeResultBlocked, benefitPaymentAllocation, currency, idepositarySetup.getPaymentType());
			generatePaymentAllocationDetail(true,lstCorporativeResultBlocked, benefitPaymentAllocation, currency, idepositarySetup.getPaymentType(),null);
		}

		if(Validations.validateListIsNotNullAndNotEmpty(corporativeResultWithOutCorporativeProcessResultBlock)){
			//generateDetailCorporativeResultAvailable(corporativeResultWithOutCorporativeProcessResultBlock,benefitPaymentAllocation,currency,idepositarySetup.getPaymentType());
			generatePaymentAllocationDetail(false,corporativeResultWithOutCorporativeProcessResultBlock, benefitPaymentAllocation, currency, idepositarySetup.getPaymentType(), lstCorporativeResultBlocked);
		}
	}
	
	private BigDecimal blockAmmountWithCorporativeProcessResult(CorporativeProcessResult corporativeProcessResult) {
		BigDecimal blockAmount = BigDecimal.ZERO;
		blockAmount = blockAmount.add(corporativeProcessResult.getPawnBalance());
		blockAmount = blockAmount.add(corporativeProcessResult.getBanBalance());
		blockAmount = blockAmount.add(corporativeProcessResult.getOtherBlockBalance());
		return blockAmount;
	}
	
	public boolean updateBenefitPayment(Long idBenefitPaymentAllocationPk, boolean commissionExcluded, List<Long> lstCorporatives) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		
		BenefitPaymentAllocation benefitPaymentAllocation = find(idBenefitPaymentAllocationPk, BenefitPaymentAllocation.class);
		BigDecimal allocationAmount = getTotalAllocationAmount(benefitPaymentAllocation.getIdBenefitPaymentPK());
		if(!commissionExcluded){
			BigDecimal totalCommissionAmount = getCommissionAmount(lstCorporatives);
			allocationAmount = allocationAmount.add(totalCommissionAmount);
		}
		benefitPaymentAllocation.setAllocationProcessAmount(allocationAmount);
		update(benefitPaymentAllocation);
		
		return true;
	}
	
	private void generatePaymentAllocationDetail(Boolean hasRetention, List<CorporativeProcessResult> lstCorporativeProcessResultSaved,BenefitPaymentAllocation benefitPaymentAllocation, Integer currency, Integer paymentType, List<CorporativeProcessResult> lstCorporativeProcessResultBlocked) throws ServiceException{
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeProcessResultSaved)){
			for(CorporativeProcessResult corporativeResult : lstCorporativeProcessResultSaved){
				
				Long idHolderAccountPk = corporativeResult.getHolderAccount().getIdHolderAccountPk();
				Long idCorporativeOperationPk = corporativeResult.getCorporativeOperation().getIdCorporativeOperationPk();
				
				BigDecimal transactionAmountBlock= (hasRetention)?blockAmmountWithCorporativeProcessResult(corporativeResult):BigDecimal.ZERO;
	            BigDecimal transactionAmount = (!hasRetention)?corporativeResult.getAvailableBalance():BigDecimal.ZERO;
	            
				BigDecimal taxAmount = BigDecimal.ZERO;//corporativeResult.getTaxAmount();
				BigDecimal custodyAmount = BigDecimal.ZERO;
				Participant participant = getParticipantByHolderAccount(idHolderAccountPk);
				participant.getIdParticipantPk();

	            HolderAccount holderAccount = find(idHolderAccountPk, HolderAccount.class);
	            //si tiene 0 saldos bloqueados, es porque el bloqueo va al total (motivo: titular bloqueado)
	            if(holderAccount.getStateAccount().equals(HolderAccountStatusType.BLOCK.getCode())) {
		            transactionAmount = (corporativeResult.getAvailableBalance()!=null)?corporativeResult.getAvailableBalance():BigDecimal.ZERO;
		            
            		transactionAmountBlock = transactionAmount.add(transactionAmountBlock);
            		transactionAmount = BigDecimal.ZERO;
            		
            		taxAmount = corporativeResult.getTaxAmount();
            		custodyAmount = corporativeResult.getCustodyAmount();
            		
	            }else {
	            	System.out.println("----------------------------");
            		System.out.println(" !holderAccount.getStateAccount().equals(HolderAccountStatusType.BLOCK.getCode()) ");
	            	taxAmount = corporativeResult.getTaxAmount();
            		custodyAmount = corporativeResult.getCustodyAmount();
	            	
            		BigDecimal totalBalance = corporativeResult.getTotalBalance();///.subtract(taxAmount)
	            	BigDecimal netBalance = corporativeResult.getTotalBalance().subtract(taxAmount.add(custodyAmount));//.subtract(taxAmount)
            		System.out.println(" totalBalance: "+totalBalance+", netBalance: "+netBalance);
	            	
	            	if(hasRetention) {

	            		System.out.println("hasRetention: TRUE => taxAmount: "+taxAmount+", custodyAmount: "+custodyAmount);
	            		BigDecimal tmpBlockAmount = blockAmmountWithCorporativeProcessResult(corporativeResult);
	            		transactionAmountBlock = transactionAmountBlock.multiply(netBalance).divide(totalBalance);
		            	taxAmount = tmpBlockAmount.subtract(transactionAmountBlock);
	            		System.out.println("hasRetention: TRUE => tmpBlockAmount: "+tmpBlockAmount+", transactionAmountBlock: "+transactionAmountBlock+", taxAmount: "+taxAmount);
	            		
	            	}else {
	            		
	            		System.out.println("hasRetention: FALSE => taxAmount: "+taxAmount+", custodyAmount: "+custodyAmount);
	            		BigDecimal tmpGrossAmount = corporativeResult.getAvailableBalance();//new BigDecimal(grossAmount.toString()); 
	            		transactionAmount = transactionAmount.multiply(netBalance).divide(totalBalance);
		            	taxAmount = tmpGrossAmount.subtract(transactionAmount);
	            		System.out.println("hasRetention: FALSE => tmpGrossAmount: "+tmpGrossAmount+", transactionAmount: "+transactionAmount+", taxAmount: "+taxAmount);
	            	}
	            	System.out.println("----------------------------");
            		
		            /*if(!hasRetention) {
	            		taxAmount = corporativeResult.getTaxAmount();
	            		custodyAmount = corporativeResult.getCustodyAmount();
		            }
		            */
	            }
				
				BenefitPaymentAllocation bpa = find(benefitPaymentAllocation.getIdBenefitPaymentPK(), BenefitPaymentAllocation.class);
				//bpa.setIdBenefitPaymentPK(benefitPaymentAllocation.getIdBenefitPaymentPK());

				CorporativeOperation corporativeOperation = new CorporativeOperation();
				corporativeOperation.setIdCorporativeOperationPk(idCorporativeOperationPk);
				
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(idHolderAccountPk);
				
				Security objSecurity = getSecurityByCorporative(idCorporativeOperationPk);

				PaymentAllocationDetail paymentAllocationDetail = new PaymentAllocationDetail();
				paymentAllocationDetail.setBenefitPaymentAllocation(bpa);
				paymentAllocationDetail.setCorporativeOperation(corporativeOperation);
				paymentAllocationDetail.setCurrency(benefitPaymentAllocation.getCurrency());
				paymentAllocationDetail.setHolderAccount(objHolderAccount);
				paymentAllocationDetail.setParticipant(participant);
				paymentAllocationDetail.setTransactionAmount((hasRetention)?transactionAmountBlock:transactionAmount);
				paymentAllocationDetail.setTaxAmount(taxAmount);
				paymentAllocationDetail.setCustodyAmount(custodyAmount);
				paymentAllocationDetail.setSecurity(objSecurity);
				
				Map<Long, ParticipantAccountCashBank> paymentPDDMap = new HashMap<Long, ParticipantAccountCashBank>();
				
				if(hasRetention) {
					paymentAllocationDetail.setProcessType(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode());
				}else {
					if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
		                paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
		            }else {
		                paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
		            }
				}

				if(paymentType.equals(BenefitPaymentType.PDI.getCode())){
					
					Object[] holderAccountBank = getHolderAccountBank(idHolderAccountPk,currency);
					if(holderAccountBank!=null && holderAccountBank.length>0){
						
						Long holderAccountBankPK = Long.valueOf(holderAccountBank[0].toString());
						Long bankPK = Long.valueOf(holderAccountBank[1].toString());
						String accountNumber = holderAccountBank[2].toString();
						
						Bank bank = new Bank();
						bank.setIdBankPk(bankPK);
						
						HolderAccountBank objHolderAccountBank = new HolderAccountBank();
						objHolderAccountBank.setIdHolderAccountBankPk(holderAccountBankPK);
						
						paymentAllocationDetail.setBank(bank);
						paymentAllocationDetail.setHolderAccountBank(objHolderAccountBank);
						paymentAllocationDetail.setAccountNumber(accountNumber);
						//paymentAllocationDetail.setProcessType(AllocationProcessType.DIRECT_PAYMENT.getCode());
					}
					
				}else if(paymentType.equals(BenefitPaymentType.PDD.getCode())){
					
					ParticipantAccountCashBank ParticipantBank = paymentPDDMap.get(participant.getIdParticipantPk());
					
					if(ParticipantBank == null) {
						//si no tiene un registro en el mapa, lo busco en la BD
						ParticipantBank = getParticipantAccountCashBank(participant.getIdParticipantPk(),currency,AccountCashFundsType.BENEFIT.getCode());
						//ingreso el nuevo registro en el mapa
						paymentPDDMap.put(idCorporativeOperationPk, ParticipantBank);
						
					}
					
					if(ParticipantBank!=null){
						paymentAllocationDetail.setInstitutionCashAccount(ParticipantBank.getInstitutionCashAccount());
						paymentAllocationDetail.setInstitutionBankAccount(ParticipantBank.getInstitutionBankAccount());
						paymentAllocationDetail.setBank(ParticipantBank.getBank());
						paymentAllocationDetail.setAccountNumber(ParticipantBank.getAccountNumber());
						//paymentAllocationDetail.setProcessType(AllocationProcessType.INDIRECT_PAYMENT.getCode());
					}
				}
				
				create(paymentAllocationDetail);
			}
		}
	}
	
	
	private BenefitPaymentAllocation saveAllocationProcess(String issuer, String security, Integer currency, List<Long> corporatives, boolean commissionExcluded){
		BenefitPaymentAllocation benefitPaymentAllocation = new BenefitPaymentAllocation();
		
		benefitPaymentAllocation.setPaymentType(idepositarySetup.getPaymentType());
		benefitPaymentAllocation.setAllocationProcessDate(CommonsUtilities.currentDateTime());
		benefitPaymentAllocation.setAllocationQuantityProcess(BigDecimal.valueOf(corporatives.size()));
		benefitPaymentAllocation.setAllocationProcessState(AllocationProcessStateType.PRELIMINAR.getCode());
		benefitPaymentAllocation.setCurrency(currency);
		
		if(commissionExcluded){
			benefitPaymentAllocation.setIndExcludedCommissions(BooleanType.YES.getCode());
		}else{
			benefitPaymentAllocation.setIndExcludedCommissions(BooleanType.NO.getCode());
		}

		Issuer objIssuer = null;
		if(issuer != null) {
			objIssuer = find(issuer,Issuer.class);
		}else {
			if(corporatives!=null && corporatives.size()>0) {
				objIssuer = getIssuerByCorporative(corporatives.get(0));
			}
		}
		benefitPaymentAllocation.setIssuer(objIssuer);

		InstitutionCashAccount institutionCashAccount = getActiveCentralizerBenefitPaymentIssuerCashAccount(objIssuer.getIdIssuerPk(), currency);
		InstitutionBankAccount institutionBankAccount = (institutionCashAccount!=null)?getInstitutionBankAccountBenefitPaymentSending(institutionCashAccount.getIdInstitutionCashAccountPk()):null;
		
		benefitPaymentAllocation.setInstitutionCashAccount(institutionCashAccount);
		benefitPaymentAllocation.setInstitutionBankAccount(institutionBankAccount);
		
		create(benefitPaymentAllocation);

		return benefitPaymentAllocation;
	}
	
	
	public List<CorporativeProcessResult> getCorporativeResultWithHolderAccountBlock(List<Long> lstCorporatives, List<Long> lstRetentionHolderAccount){
		
		String strQuery = 
				" SELECT distinct cpr FROM CorporativeProcessResult cpr "
						+ " JOIN FETCH cpr.corporativeOperation co "
						+ " JOIN FETCH cpr.holderAccount ha "
						+ " WHERE co.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
						+ " AND cpr.indOriginTarget = :indOriginTarget "
						+ " AND co.idCorporativeOperationPk in (:idCorporativeOperationPk) ";

		if(lstRetentionHolderAccount!=null && lstRetentionHolderAccount.size()>0) {
			strQuery += " AND ha.idHolderAccountPk in (:idHolderAccountPk) ";
		}

		TypedQuery<CorporativeProcessResult> query = em.createQuery(strQuery,CorporativeProcessResult.class);
		
		query.setParameter("idCorporativeOperationPk",lstCorporatives);
		query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);

		if(lstRetentionHolderAccount!=null && lstRetentionHolderAccount.size()>0) {
			query.setParameter("idHolderAccountPk",lstRetentionHolderAccount);
		}

		if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
			return query.getResultList();
		}else {
			return null;
		}
	}
	
	public List<CorporativeProcessResult> getCorporativeResultWithOutCorporativeProcessResultBlock(List<Long> lstCorporatives, List<Long> lstRetentionHolderAccount){//List<Long> lstCorporativeProcessResultBlocked
		
		String strQuery = 
		" SELECT distinct cpr FROM CorporativeProcessResult cpr "
				+ " JOIN FETCH cpr.corporativeOperation co "
				+ " JOIN FETCH cpr.holderAccount ha "
				+ " WHERE 1 = 1 "
				+ " AND cpr.indOriginTarget = :indOriginTarget "
				+ " AND co.idCorporativeOperationPk in (:idCorporativeOperationPk) ";
		
		if(lstRetentionHolderAccount!=null && lstRetentionHolderAccount.size()>0) {
			strQuery += " AND ha.idHolderAccountPk not in (:holderAccounts) ";
		}

		TypedQuery<CorporativeProcessResult> query = em.createQuery(strQuery,CorporativeProcessResult.class);
		
		if(lstRetentionHolderAccount!=null && lstRetentionHolderAccount.size()>0) {
			query.setParameter("holderAccounts",lstRetentionHolderAccount);
		}
		query.setParameter("idCorporativeOperationPk",lstCorporatives);
		query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);

		return query.getResultList();
	}
	
	/**
	 * Metodo que retorna lista detalle de los procesos corporativos a consignar.
	 *
	 * @param lstCorporatives list corporative
	 * @param lstRetentionHolderAccount list Retention Holder Account
	 * @param lstRetentionHolder list Retention Holder
	 * @param lstHolderAccountNoBankAccount list Holder Account No Bank Account
	 * @param lstBlocks list Blocks
	 * @param currency currency
	 * @return list Payment By Corporative
	 */
	public List<Object[]> getPaymentByCorporatives(List<Long> lstCorporatives, List<Long> lstRetentionHolderAccount, List<Long> lstBlocks, Integer currency){

		List<Object[]> lstPaymentCorporativeResult = new ArrayList<Object[]>();
		try{
			String strQuery = 
							" select cpr.idCorporativeProcessResult,co.idCorporativeOperationPk "//hab.haveBic
							+ " from CorporativeOperation co, CorporativeProcessResult cpr, HolderAccount ha "//, HolderAccountDetail had, Holder h, HolderAccountBank hab
							+ " where "
							+ " co.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
							+ " and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
							//+ "and had.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
							//+ "and had.holder.idHolderPk = h.idHolderPk "
							+ " and ha.participant.idParticipantPk = cpr.participant.idParticipantPk "
							//+ "and hab.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
							//+ "and hab.holder.idHolderPk = h.idHolderPk "
							//+ "and had.indRepresentative = :indRepresentant "
							//+ "and hab.currency = :currency "
							//+ "and hab.stateAccountBank = :bankAccountState "
							+ " and cpr.indOriginTarget = :indOriginTarget "
							+ " and co.idCorporativeOperationPk in (:corporatives) "
							//+ "and had.stateAccountDetail = :accountDetailState "
							;

			if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
				strQuery = strQuery.concat("and ha.idHolderAccountPk not in (:holderAccounts) ");
			}
			if(Validations.validateListIsNotNullAndNotEmpty(lstBlocks)){
				strQuery = strQuery.concat("and cpr.idCorporativeProcessResult not in (:retentionBankAccounts) ");
			}

			TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
			if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
				query.setParameter("holderAccounts",lstRetentionHolderAccount);
			}
			if(Validations.validateListIsNotNullAndNotEmpty(lstBlocks)){
				query.setParameter("retentionBankAccounts",lstBlocks);
			}

			query.setParameter("corporatives",lstCorporatives);
			//query.setParameter("indRepresentant",BooleanType.YES.getCode());
			//query.setParameter("bankAccountState",HolderAccountStateBankType.REGISTERED.getCode());
			//query.setParameter("currency",currency);
			//query.setParameter("accountDetailState",HolderAccountStateDetailType.REGISTERED.getCode());
			query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);

			lstPaymentCorporativeResult = query.getResultList();
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return lstPaymentCorporativeResult;
	}
	
	/**
	 * Metodo que retorna lista detalle de consignaciones bloqueadas.
	 *
	 * @param lstPaymentRetentionBlocks list Payment Retention Blocks
	 * @param currency currency
	 * @return list Block Payment Indicator
	 */
	public List<Object[]> getBlockPaymentIndicator(List<Long> lstPaymentRetentionBlocks, Integer currency){
		List<Object[]> lstReturn = new ArrayList<Object[]>();

		String strQuery = 
				" select cpr.idCorporativeProcessResult, co.idCorporativeOperationPk "//hab.haveBic
				+ " from CorporativeOperation co, CorporativeProcessResult cpr "//, HolderAccount ha, HolderAccountBank hab , HolderAccountDetail had, Holder h,
				+ " where "
				+ " co.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
				//+ " and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
				//+ " and had.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
				//+ " and had.holder.idHolderPk = h.idHolderPk "
				//+ " and ha.participant.idParticipantPk = cpr.participant.idParticipantPk "
				//+ " and hab.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
				//+ " and hab.holder.idHolderPk = h.idHolderPk "
				//+ "and had.indRepresentative = :indRepresentant "
				//+ " and hab.stateAccountBank = :bankAccountState "
				+ " and cpr.indOriginTarget = :indOriginTarget "
				//+ " and hab.currency = :currency "
				//+ "and had.stateAccountDetail = :accountDetailState "
				+ " and cpr.idCorporativeProcessResult in (:retentionPaymentDetail) ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		//query.setParameter("indRepresentant",BooleanType.YES.getCode());
		//query.setParameter("bankAccountState",HolderAccountStateBankType.REGISTERED.getCode());
		//query.setParameter("accountDetailState",HolderAccountStateDetailType.REGISTERED.getCode());
		query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);
		//query.setParameter("currency",currency);
		query.setParameter("retentionPaymentDetail",lstPaymentRetentionBlocks);
		lstReturn = query.getResultList();

		return lstReturn;
	}
	
	/**
	 * Metodo que retorna lista detalle de cuentas asociadas al proceso corporativo.
	 *
	 * @param lstCorporatives list corporative
	 * @param lstRetentionHolderAccount list Retention Holder Account
	 * @return list Detail By Corporative Holder Account
	 */
	public List<Object[]> getDetailByCorporativeHolderAccount(List<Long> lstCorporatives,List<Long> lstRetentionHolderAccount){
		List<Object[]> lstCorporativeDetail = new ArrayList<Object[]>();

		String strQuery = 
				"select "
				+ "cpr.holderAccount.idHolderAccountPk, "								//[0]
				+ "cpr.corporativeOperation.idCorporativeOperationPk, "					//[1]
				+ "(case when cpr.security.indPaymentReporting = 0 then (nvl(cpr.totalBalance,0) + nvl(cpr.reportedBalance,0) - nvl(cpr.reportingBalance,0)) else nvl(cpr.totalBalance,0) end ), "											//[2]
				+ "nvl(cpr.taxAmount,0), "												//[3]
				+ "nvl(cpr.custodyAmount,0) "											//[4]
				+ "from CorporativeProcessResult cpr "
				+ "where "
				+ "cpr.corporativeOperation.idCorporativeOperationPk in (:corporatives) "
				+ "and cpr.indOriginTarget = :indOriginTarget "
				+ "and cpr.holderAccount.idHolderAccountPk in (:holderAccounts) ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("corporatives",lstCorporatives);
		query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);
		query.setParameter("holderAccounts",lstRetentionHolderAccount);

		lstCorporativeDetail = query.getResultList();

		return lstCorporativeDetail;
	}
	
	public Issuer getIssuerByBenefitPayment(Long idBenefitPaymentPk){

		String strQuery = " Select i "
				+ " From Issuer i "
				+ " Where "
				+ " i.idIssuerPk = ( Select b.issuer.idIssuerPk from BenefitPaymentAllocation b Where b.idBenefitPaymentPK = :idBenefitPaymentPK ) ";

		TypedQuery<Issuer> query = em.createQuery(strQuery,Issuer.class);
		query.setParameter("idBenefitPaymentPK",idBenefitPaymentPk);

		return query.getSingleResult();
	}
	
	public Issuer getIssuerByCorporative(Long idCorporativeOperationPk){

		String strQuery = " Select i "
				+ " From Issuer i "
				+ " Where "
				+ " i.idIssuerPk = ( Select co.issuer.idIssuerPk from CorporativeOperation co Where co.idCorporativeOperationPk = :idCorporativeOperationPk ) ";

		TypedQuery<Issuer> query = em.createQuery(strQuery,Issuer.class);
		query.setParameter("idCorporativeOperationPk",idCorporativeOperationPk);

		return query.getSingleResult();
	}
	
	public Security getSecurityByCorporative(Long idCorporativeOperationPk){

		String strQuery = " Select s "
				+ " From Security s "
				+ " Where "
				+ " s.idSecurityCodePk = ( Select co.securities.idSecurityCodePk from CorporativeOperation co Where co.idCorporativeOperationPk = :idCorporativeOperationPk ) ";

		TypedQuery<Security> query = em.createQuery(strQuery,Security.class);
		query.setParameter("idCorporativeOperationPk",idCorporativeOperationPk);

		return query.getSingleResult();
	}
	
	/**
	 * Metodo que retorna lista de titulares asociados a una cuenta.
	 *
	 * @param holderAccount holder account
	 * @return list Holder By Account Participant
	 */
	public List<Long> getHolderByAccountParticipant(Long holderAccount){
		List<Long> lstHolder = null;
		try{
			String strQuery = 
					"select had.holder.idHolderPk "
					+ "from HolderAccountDetail had "
					+ "where had.holderAccount.idHolderAccountPk = :idHolderAccountPk "
					+ "and had.indRepresentative = :indRepresentative "
					+ "and had.stateAccountDetail = :stateAccountDetail ";
			TypedQuery<Long> query = em.createQuery(strQuery,Long.class);
			query.setParameter("idHolderAccountPk",holderAccount);
			query.setParameter("indRepresentative",BooleanType.YES.getCode());
			query.setParameter("stateAccountDetail",HolderAccountStateDetailType.REGISTERED.getCode());

			lstHolder = query.getResultList();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return lstHolder;
	}
	
	/**
	 * Metodo que retorna lista detalle de cuentas asociadas al proceso corporativo.
	 *
	 * @param lstCorporatives list corporative
	 * @param lstRetentionHolder list Retention Holder
	 * @return list Detail By Corporative Holder
	 */
	public List<Object[]> getDetailByCorporativeHolder(List<Long> lstCorporatives,List<Long> lstRetentionHolder){
		List<Object[]> lstCorporativeDetail = new ArrayList<Object[]>();

		String strQuery = 
				"select "
				+ "cpr.holderAccount.idHolderAccountPk, "								//[0]
				+ "cpr.corporativeOperation.idCorporativeOperationPk, "					//[1]
				+ "decode(cpr.security.indPaymentReporting,0,nvl(cpr.totalBalance,0) + nvl(cpr.reportedBalance,0) - nvl(cpr.reportingBalance,0),nvl(cpr.totalBalance,0)), "											//[2]
				+ "nvl(cpr.taxAmount,0), "												//[3]
				+ "nvl(cpr.custodyAmount,0) "											//[4]
				+ "from CorporativeProcessResult cpr, HolderAccount ha, HolderAccountDetail had, Holder h "
				+ "where "
				+ "cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
				+ "and had.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
				+ "and had.holder.idHolderPk = h.idHolderPk "
				+ "and had.indRepresentative = :indRepresentant "
				+ "and cpr.corporativeOperation.idCorporativeOperationPk in (:corporatives) "
				+ "and had.holder.idHolderPk in (:holders) "
				+ "and cpr.indOriginTarget = :indOriginTarget "
				+ "and had.stateAccountDetail = :accountDetailState ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("corporatives",lstCorporatives);
		query.setParameter("holders",lstRetentionHolder);
		query.setParameter("indRepresentant",BooleanType.YES.getCode());
		query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);
		query.setParameter("accountDetailState",HolderAccountStateDetailType.REGISTERED.getCode());
		lstCorporativeDetail = query.getResultList();

		return lstCorporativeDetail;
	}
	
	/**
	 * Metodo que retorna lista detalle de cuentas asociadas al proceso corporativo.
	 *
	 * @param lstCorporatives list corporative
	 * @param lstHolderAccountNoBankAccount list Holder Account No Bank Account
	 * @return list Detail By Corporative Bank Account
	 */
	public List<Object[]> getDetailByCorporativeBankAccount(List<Long> lstCorporatives,List<Long> lstHolderAccountNoBankAccount){
		List<Object[]> lstCorporativeDetail = new ArrayList<Object[]>();

		String strQuery = 
				"select "
				+ "cpr.holderAccount.idHolderAccountPk, "									//[0]
				+ "cpr.corporativeOperation.idCorporativeOperationPk, "						//[1]
				+ "(case when cpr.security.indPaymentReporting = 0 then (nvl(cpr.totalBalance,0) + nvl(cpr.reportedBalance,0) - nvl(cpr.reportingBalance,0)) else nvl(cpr.totalBalance,0) end ), "//[2]
				+ "nvl(cpr.taxAmount,0), "													//[3]
				+ "nvl(cpr.custodyAmount,0) "												//[4]
				+ "from CorporativeProcessResult cpr, HolderAccount ha "
				+ "where "
				+ "cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
				+ "and cpr.corporativeOperation.idCorporativeOperationPk in (:corporatives) "
				+ "and ha.idHolderAccountPk in (:holderAccount) "
				+ "and cpr.indOriginTarget = :indOriginTarget ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("corporatives",lstCorporatives);
		query.setParameter("holderAccount",lstHolderAccountNoBankAccount);
		query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);
		lstCorporativeDetail = query.getResultList();

		return lstCorporativeDetail;
	}
	
	/**
	 * Metodo que retorna lista de procesos corporativos con consignacion bloqueados.
	 *
	 * @param lstCorporatives list corporative
	 * @param lstRetentionHolderAccount list Retention Holder Account
	 * @param lstRetentionHolder list Retention Holder
	 * @param lstHolderAccountNoBankAccount list Holder Account No Bank Account
	 * @param lstBlocks list blocks
	 * @param currency currency
	 * @return list Block Payment Result
	 */
	public List<Long> getBlockPaymentResult(List<Long> lstCorporatives,List<Long> lstRetentionHolderAccount,List<Long> lstRetentionHolder,
				List<Long> lstHolderAccountNoBankAccount,List<Long> lstBlocks, Integer currency){
		List<Long> lstPaymentBlock = new ArrayList<Long>();

		String strQuery = 
				"select cpr.idCorporativeProcessResult from CorporativeOperation co, CorporativeProcessResult cpr, HolderAccount ha, HolderAccountDetail had, Holder h,"
						+ "HolderAccountBank hab "
						+ "where "
						+ "co.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
						+ "and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and had.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and had.holder.idHolderPk = h.idHolderPk "
						+ "and ha.participant.idParticipantPk = cpr.participant.idParticipantPk "
						+ "and hab.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and hab.holder.idHolderPk = h.idHolderPk "
						+ "and hab.stateAccountBank = :bankAccountState "
						+ "and had.indRepresentative = :indRepresentant "
						+ "and co.idCorporativeOperationPk in (:corporatives) "
						+ "and had.stateAccountDetail = :accountDetailState "
						+ "and cpr.indOriginTarget = :indOriginTarget "
						+ "and cpr.totalBalance - cpr.availableBalance = 0 ";

				if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
					strQuery = strQuery.concat("and ha.idHolderAccountPk not in (:holderAccounts) ");
				}
				if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolder)){
					strQuery = strQuery.concat("and h.idHolderPk not in (:holders) ");
				}
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountNoBankAccount)){
					strQuery = strQuery.concat("and hab.holderAccount.idHolderAccountPk not in (:retentionBankAccounts) ");
				}
				if(Validations.validateListIsNotNullAndNotEmpty(lstBlocks)){
					strQuery = strQuery.concat("and cpr.idCorporativeProcessResult not in (:blockRetentions) ");
				}

				TypedQuery<Long> query = em.createQuery(strQuery,Long.class);
				if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolderAccount)){
					query.setParameter("holderAccounts",lstRetentionHolderAccount);
				}
				if(Validations.validateListIsNotNullAndNotEmpty(lstRetentionHolder)){
					query.setParameter("holders",lstRetentionHolder);
				}
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountNoBankAccount)){
					query.setParameter("retentionBankAccounts",lstHolderAccountNoBankAccount);
				}
				if(Validations.validateListIsNotNullAndNotEmpty(lstBlocks)){
					query.setParameter("blockRetentions",lstBlocks);
				}

				query.setParameter("corporatives",lstCorporatives);
				query.setParameter("indRepresentant",BooleanType.YES.getCode());
				query.setParameter("bankAccountState",HolderAccountStateBankType.REGISTERED.getCode());
				query.setParameter("accountDetailState",HolderAccountStateDetailType.REGISTERED.getCode());
				query.setParameter("indOriginTarget",CorporateProcessConstants.BALANCE_DESTINY);
				lstPaymentBlock = query.getResultList();

		return lstPaymentBlock;
	}
	
	/**
	 * Metodo que retorna lista detalle de cuentas asociadas al proceso corporativo.
	 *
	 * @param lstCorporativeResult list Corporative Result
	 * @return Corporative Result List
	 */
	public List<Object[]> getCorporativeResultList(List<Long> lstCorporativeResult){
		List<Object[]> lstResult = new ArrayList<Object[]>();
		String strQuery = 
				"select "
				+ "cpr.holderAccount.idHolderAccountPk, "												//[0]
				+ "cpr.corporativeOperation.idCorporativeOperationPk, "									//[1]
				+ "(case when cpr.security.indPaymentReporting = 0 then (nvl(cpr.totalBalance,0) + nvl(cpr.reportedBalance,0) - nvl(cpr.reportingBalance,0)) else nvl(cpr.totalBalance,0) end ), "																	//[2]
				+ "cpr.idCorporativeProcessResult, "													//[3]
				+ "cpr.availableBalance, "																//[4]
				+ "cpr.pawnBalance, "																	//[5]
				+ "cpr.banBalance, "																	//[6]
				+ "cpr.otherBlockBalance, "																//[7]
				+ "nvl(cpr.taxAmount,0), "																//[8]
				+ "nvl(cpr.custodyAmount,0) "															//[9]
				+ "from CorporativeProcessResult cpr "
				+ "where "
				+ "cpr.idCorporativeProcessResult in (:corporativeResults) ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("corporativeResults",lstCorporativeResult);
		lstResult = query.getResultList();

		return lstResult;
	}
	
	/**
	 * Metodo que retorna datos cuenta bancaria del titular por moneda.
	 *
	 * @param holderAccount holder account
	 * @param holder holder
	 * @param currency currency
	 * @return Holder Account Bank
	 */
	public Object[] getHolderAccountBank(Long holderAccount,Integer currency){
		Object[] holderAccountBank = null;

		try{
			String strQuery = 
					"select hab.idHolderAccountBankPk, "
					+ "hab.bank.idBankPk, "
					+ "hab.bankAccountNumber "
					+ "from HolderAccountBank hab "
					+ "where hab.holderAccount.idHolderAccountPk = :holderAccount "
					+ "and hab.currency = :currency "
					+ "and hab.stateAccountBank = :bankAccountState ";

			TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
			query.setParameter("holderAccount",holderAccount);
			query.setParameter("currency",currency);
			query.setParameter("bankAccountState",HolderAccountStateBankType.REGISTERED.getCode());
			holderAccountBank = query.getSingleResult();

		}catch(NoResultException x){
			x.printStackTrace();
		}

		return holderAccountBank;
	}
	
	public ParticipantAccountCashBank getParticipantAccountCashBank(Long idParticipantPk, Integer currency, Integer accountType){
		Object[] holderAccountBank = null;
		ParticipantAccountCashBank participantAccountCashBank = null;
		try{
			String strQuery = 
					  " select distinct "
					+ " ica, "
					+ " iba, "
					+ " iba.providerBank "
					+ " from InstitutionCashAccount ica "
					+ " ,CashAccountDetail cad "
					+ " ,InstitutionBankAccount iba "
					+ " where "
					+ " cad.institutionCashAccount.idInstitutionCashAccountPk = ica.idInstitutionCashAccountPk "
					+ " and cad.institutionBankAccount.idInstitutionBankAccountPk = iba.idInstitutionBankAccountPk "
					+ " and ica.accountType = :accountType "
					+ " and ica.currency = :currency "
					+ " and ica.participant.idParticipantPk = :idParticipantPk "
					+ " and cad.accountState = :cadAccountState "
					+ " and cad.indReception = :indReception "
					+ " and ica.accountState = :icaAccountState ";
			
			TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
			query.setParameter("idParticipantPk",idParticipantPk);
			query.setParameter("currency",currency);
			query.setParameter("accountType",accountType);

			query.setParameter("cadAccountState",1926);
			query.setParameter("indReception",1);
			query.setParameter("icaAccountState",683);
			
			holderAccountBank = query.getSingleResult();
			
			InstitutionCashAccount institutionCashAccount = (holderAccountBank[0]!=null)? (InstitutionCashAccount)(holderAccountBank[0]) : null;
			InstitutionBankAccount institutionBankAccountPK = (holderAccountBank[1]!=null)? (InstitutionBankAccount)(holderAccountBank[1]) : null;
			Bank bank = (holderAccountBank[2]!=null)? (Bank)(holderAccountBank[2]) : null;
			
			participantAccountCashBank = new ParticipantAccountCashBank(institutionCashAccount, institutionBankAccountPK, bank);

		}catch(NoResultException x){
			//x.printStackTrace();
			holderAccountBank = null;
			participantAccountCashBank = null;
		}

		return participantAccountCashBank;
	}
	
	public Object[] getParticipantBank(Long idParticipantPk, Integer currency, Integer accountType){
		Object[] holderAccountBank = null;

		try{
			String strQuery = 
					  " select distinct ica.idInstitutionCashAccountPk, "
					+ " iba.providerBank.idBankPk, "
					+ " iba.accountNumber, "
					+ " iba.idInstitutionBankAccountPk "
					+ " from InstitutionCashAccount ica "
					+ " ,CashAccountDetail cad "
					+ " ,InstitutionBankAccount iba "
					+ " where "
					+ " cad.institutionCashAccount.idInstitutionCashAccountPk = ica.idInstitutionCashAccountPk "
					+ " and cad.institutionBankAccount.idInstitutionBankAccountPk = iba.idInstitutionBankAccountPk "
					+ " and ica.accountType = :accountType "
					+ " and ica.currency = :currency "
					+ " and ica.participant.idParticipantPk = :idParticipantPk "
					+ " and cad.accountState = :cadAccountState "
					+ " and cad.indReception = :indReception "
					+ " and ica.accountState = :icaAccountState ";
			
			TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
			query.setParameter("idParticipantPk",idParticipantPk);
			query.setParameter("currency",currency);
			query.setParameter("accountType",accountType);

			query.setParameter("cadAccountState",1926);
			query.setParameter("indReception",1);
			query.setParameter("icaAccountState",683);
			
			holderAccountBank = query.getSingleResult();

		}catch(NoResultException x){
			//x.printStackTrace();
			holderAccountBank = null;
		}

		return holderAccountBank;
	}
	
	/**
	 * Metodo que retorna lista detalle de consignacion de pagos.
	 *
	 * @param idAllocationProcess id allocation process
	 * @param bankID id bank
	 * @param lstMotives list motives
	 * @return list Allocation Detail
	 */
	public List<Object[]> getAllocationDetail(Long idAllocationProcess, Long bankID, List<Integer> lstMotives){
		List<Object[]> lstAllocationDetail = new ArrayList<Object[]>();

		String strQuery = 
				"select "
				+ "0,"																	//[0]b.idBankPk
				+ "'b.description',"																//[1]
				+ "'h.idHolderPk',"																//[2]
				+ "'h.fullName',"																	//[3]
				+ "0,"																//[4]	h.documentType
				+ "'h.documentNumber',"															//[5]
				+ "hab.bankAccountNumber, "														//[6]
				+ "c.idCorporativeOperationPk, "												//[7]
				+ "c.securities.idSecurityCodePk,"												//[8]
				+ "pad.transactionAmount, "														//[9]
				+ "bpa.indExcludedCommissions,"													//[10]
				+ "pad.processType, "															//[11]
				+ "nvl(pad.taxAmount,0), "														//[12]
				+ "nvl(pad.custodyAmount,0) "													//[13]
				+ "from PaymentAllocationDetail pad,HolderAccountBank hab, "	// Bank b, Holder h, 
				+ "CorporativeOperation c, BenefitPaymentAllocation bpa "
				+ "where pad.benefitPaymentAllocation.idBenefitPaymentPK = :idBenefitPayment "
				//+ "and pad.bank.idBankPk = b.idBankPk "
				+ "and bpa.idBenefitPaymentPK = pad.benefitPaymentAllocation.idBenefitPaymentPK ";
				//+ "and pad.holder.idHolderPk = h.idHolderPk ";
		/*if(Validations.validateIsNotNull(bankID)){
			strQuery = strQuery.concat("and b.idBankPk = :bankID ");
		}*/
		if(Validations.validateListIsNotNullAndNotEmpty(lstMotives)){
			strQuery = strQuery.concat("and pad.processType in (:motives) ");
		}
		strQuery = strQuery.concat(
				"and c.idCorporativeOperationPk = pad.corporativeOperation.idCorporativeOperationPk "
				+ "and pad.holderAccountBank.idHolderAccountBankPk = hab.idHolderAccountBankPk "
				//+ "order by b.idBankPk "
				);

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("idBenefitPayment",idAllocationProcess);
		/*if(Validations.validateIsNotNull(bankID)){
			query.setParameter("bankID",bankID);
		}*/
		if(Validations.validateListIsNotNullAndNotEmpty(lstMotives)){
			query.setParameter("motives",lstMotives);
		}
		lstAllocationDetail = query.getResultList();

		return lstAllocationDetail;
	}
	
	/**
	 * Metodo que retorna lista de procesos corporativos por consignacion.
	 *
	 * @param idAllocationProcess id allocation process
	 * @return list Corporative By ID Allocation
	 */
	@SuppressWarnings("unchecked")
	public List<BigDecimal> getCorporativeByIDAllocation(Long idAllocationProcess){

		List<BigDecimal> lstCorporative = new ArrayList<BigDecimal>();
		String strQuery = 
				"select DISTINCT ID_CORPORATIVE_OPERATION_FK "
				+ "from PAYMENT_ALLOCATION_DETAIL "
				+ "where ID_BENEFIT_PAYMENT_FK= :allocationID ";

		Query query = em.createNativeQuery(strQuery);
		query.setParameter("allocationID",idAllocationProcess);
		lstCorporative = query.getResultList();

		return lstCorporative;

	}
	
	/**
	 * Metodo que retorna total de montos a consignar por emisor.
	 *
	 * @param issuer issuer
	 * @param idAllocationProcess id Allocation Process
	 * @return Amount By Issuer Allocation
	 */
	public BigDecimal getAmountByIssuerAllocation(String issuer, Long idAllocationProcess){
		BigDecimal issuerAllocationAmount = BigDecimal.ZERO;

		String strQuery = 
				"select sum(pad.transactionAmount) "
				+ "from PaymentAllocationDetail pad, CorporativeOperation co "
				+ "where "
				+ "pad.corporativeOperation.idCorporativeOperationPk = co.idCorporativeOperationPk ";
				if(issuer!=null){
					strQuery = strQuery + "and co.issuer.idIssuerPk = :issuer ";
				}				
				strQuery = strQuery + "and pad.benefitPaymentAllocation.idBenefitPaymentPK = :idAllocation ";

		TypedQuery<BigDecimal> query = em.createQuery(strQuery,BigDecimal.class);
		if(issuer!=null){
			query.setParameter("issuer",issuer);
		}		
		query.setParameter("idAllocation",idAllocationProcess);
		issuerAllocationAmount = query.getSingleResult();

		return issuerAllocationAmount;
	}
	
	public List<Object[]> getBankParticipantAllocationDetail(Long allocationID) throws ServiceException{
		List<Object[]> lstHolderDetail = new ArrayList<Object[]>();
		String strQuery = 
				" select "
				+ " pad.bank.idBankPk,"															//[0]
				+ " bpa.currency,"																//[1]
				+ " pad.holderAccount.idHolderAccountPk,"										//[2]
				+ " '' as FN_GET_HOLDER_NAME,"													//[3] FN_GET_HOLDER_NAME(pad.holderAccount.idHolderAccountPk)
				+ " 0  as FN_GET_HOLDER_DOC_TYPE ,"												//[4] FN_GET_HOLDER_DOC_TYPE(pad.holderAccount.idHolderAccountPk)
				+ " '' as FN_GET_HOLDER_DOC_NUM ,"												//[5] FN_GET_HOLDER_DOC_NUM(pad.holderAccount.idHolderAccountPk)
				+ " pad.institutionBankAccount.idInstitutionBankAccountPk,"						//[6]
				+ " c.idCorporativeOperationPk,"												//[7]
				+ " pad.security.idSecurityCodePk,"												//[8]
				+ " pad.transactionAmount,"														//[9]
				+ " bpa.indExcludedCommissions,"												//[10]
				+ " pad.processType, "															//[11]
				+ " bpa.issuer.idIssuerPk, " 													//[12]
				+ " c.corporativeEventType.corporativeEventType, " 								//[13]
				+ " pad.institutionCashAccount.idInstitutionCashAccountPk, " 					//[14]
				+ " pad.participant.idParticipantPk, " 											//[15]
				+ " pad.idAllocationDetailPK, " 												//[16]
				+ " nvl(pad.taxAmount,0), " 													//[17]
				+ " nvl(pad.custodyAmount,0), " 												//[18]
				+ " pad.holderAccount.idHolderAccountPk, "										//[19]
				+ " pad.processType, "															//[20]
				+ " bpa.allocationProcessDate, "												//[21]
				+ " pad.idAllocationDetailPK, "													//[22]
				+ " bpa.institutionCashAccount.idInstitutionCashAccountPk, "					//[23]	
				+ " bpa.institutionBankAccount.idInstitutionBankAccountPk, "					//[24]	
				+ " 0 as FN_GET_ID_HOLDER, "													//[25] FN_GET_ID_HOLDER(pad.holderAccount.idHolderAccountPk)
				+ " pad.institutionCashAccount.idInstitutionCashAccountPk "						//[26]
				+ " from PaymentAllocationDetail pad, CorporativeOperation c, BenefitPaymentAllocation bpa "//Holder h, 
				+ " where "
				+ " pad.benefitPaymentAllocation.idBenefitPaymentPK = :idBenefitPayment "
				+ " and bpa.idBenefitPaymentPK = pad.benefitPaymentAllocation.idBenefitPaymentPK "
				//+ "and pad.holder.idHolderPk = h.idHolderPk "
				+ " and pad.corporativeOperation.idCorporativeOperationPk = c.idCorporativeOperationPk "
				+ " order by pad.processType,pad.bank.idBankPk,c.corporativeEventType.corporativeEventType ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("idBenefitPayment",allocationID);

		lstHolderDetail = query.getResultList();

		return lstHolderDetail;
	}
	
	
	/**
	 * Metodo que retorna lista detalle de consignacion de pagos.
	 *
	 * @param allocationID allocation ID
	 * @return list Bank Holder Allocation Detail
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getBankHolderAllocationDetail(Long allocationID) throws ServiceException{
		List<Object[]> lstHolderDetail = new ArrayList<Object[]>();
		String strQuery = 
				"select "
				+ "pad.bank.idBankPk,"															//[0]
				+ "bpa.currency,"																//[1]
				+ "h.idHolderPk,"																//[2]
				+ "h.fullName,"																	//[3]
				+ "h.documentType,"																//[4]
				+ "h.documentNumber,"															//[5]
				+ "pad.holderAccountBank.idHolderAccountBankPk,"								//[6]
				+ "c.idCorporativeOperationPk,"													//[7]
				+ "c.securities.idSecurityCodePk,"												//[8]
				+ "pad.transactionAmount,"														//[9]
				+ "bpa.indExcludedCommissions,"													//[10]
				+ "pad.processType, "															//[11]
				+ "c.issuer.idIssuerPk, " 														//[12]
				+ "c.corporativeEventType.corporativeEventType, " 													//[13]
				+ "h.fullName, " 																//[14]
				+ "h.documentNumber, " 															//[15]
				+ "pad.idAllocationDetailPK, " 													//[16]
				+ "nvl(pad.taxAmount,0), " 															//[17]
				+ "nvl(pad.custodyAmount,0), " 														//[18]
				+ "pad.holderAccount.idHolderAccountPk, "										//[19]
				+ "pad.processType, "															//[20]
				+ "bpa.allocationProcessDate, "													//[21]
				+ "pad.idAllocationDetailPK "													//[22]
				+ "from PaymentAllocationDetail pad, Holder h, CorporativeOperation c, BenefitPaymentAllocation bpa "
				+ "where "
				+ "pad.benefitPaymentAllocation.idBenefitPaymentPK = :idBenefitPayment "
				+ "and bpa.idBenefitPaymentPK = pad.benefitPaymentAllocation.idBenefitPaymentPK "
				+ "and pad.holder.idHolderPk = h.idHolderPk "
				+ "and pad.corporativeOperation.idCorporativeOperationPk = c.idCorporativeOperationPk "
				+ "order by pad.processType,pad.bank.idBankPk,c.corporativeEventType.corporativeEventType,h.idHolderPk ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("idBenefitPayment",allocationID);

		lstHolderDetail = query.getResultList();

		return lstHolderDetail;
	}
	
	public List<BankHolderAllocationDetailTO> getBankHolderAllocationDetailTo(Long allocationID) throws ServiceException{
		List<BankHolderAllocationDetailTO> lstHolderDetail = new ArrayList<BankHolderAllocationDetailTO>();
		String strQuery = 
				"select "
				+" new com.pradera.corporateevents.corporativeprocess.consignation.to.BankHolderAllocationDetailTO("
				+ "pad.bank.idBankPk,"															//[0]
				+ "bpa.currency,"																//[1]
				+ "h.idHolderPk,"																//[2]
				+ "h.fullName,"																	//[3]
				+ "h.documentType,"																//[4]
				+ "h.documentNumber,"															//[5]
				+ "pad.holderAccountBank.idHolderAccountBankPk,"								//[6]
				+ "c.idCorporativeOperationPk,"													//[7]
				+ "c.securities.idSecurityCodePk,"												//[8]
				+ "pad.transactionAmount,"														//[9]
				+ "bpa.indExcludedCommissions,"													//[10]
				+ "pad.processType, "															//[11]
				+ "c.issuer.idIssuerPk, " 														//[12]
				+ "c.corporativeEventType.corporativeEventType, " 								//[13]
				+ "pad.idAllocationDetailPK, " 													//[14]
				+ "nvl(pad.taxAmount,0), " 														//[15]
				+ "nvl(pad.custodyAmount,0), " 													//[16]
				+ "pad.holderAccount.idHolderAccountPk "										//[17]
				+" ) "
				+ "from PaymentAllocationDetail pad, Holder h, CorporativeOperation c, BenefitPaymentAllocation bpa "
				+ "where "
				+ "pad.benefitPaymentAllocation.idBenefitPaymentPK = :idBenefitPayment "
				+ "and bpa.idBenefitPaymentPK = pad.benefitPaymentAllocation.idBenefitPaymentPK "
				+ "and pad.holder.idHolderPk = h.idHolderPk "
				+ "and pad.corporativeOperation.idCorporativeOperationPk = c.idCorporativeOperationPk "
				+ "order by pad.processType,pad.bank.idBankPk,c.corporativeEventType.corporativeEventType,h.idHolderPk ";

		Query query = em.createQuery(strQuery);
		query.setParameter("idBenefitPayment",allocationID);

		lstHolderDetail = query.getResultList();

		return lstHolderDetail;
	}
	
	/**
	 * Metodo que retorna datos del emisor, tipo proceso corporativo y montos a consigar.
	 *
	 * @param allocationID id allocation
	 * @return list Funds Operation Resume Issuer
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getFundsOperationResumeIssuer(Long allocationID) throws ServiceException{
		List<Object[]> lst = new ArrayList<Object[]>();
		String strQuery = 
				"select "
				+ "co.issuer.idIssuerPk,"
				+ "co.corporativeEventType.corporativeEventType,"
				+ "sum(pad.transactionAmount - (pad.taxAmount + pad.custodyAmount)) "
				+ "from CorporativeOperation co, PaymentAllocationDetail pad "
				+ "where pad.benefitPaymentAllocation.idBenefitPaymentPK = :allocationID "
				+ "and pad.corporativeOperation.idCorporativeOperationPk = co.idCorporativeOperationPk "
				+ "and pad.processType = :processType "
				+ "group by co.issuer.idIssuerPk,co.corporativeEventType.corporativeEventType";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("allocationID",allocationID);
		query.setParameter("processType",AllocationProcessType.INDIRECT_PAYMENT.getCode());

		lst = query.getResultList();

		return lst;
	}
	
	public List<Object[]> getFundsOperationIssuer(Long allocationID) throws ServiceException{
		List<Object[]> lst = new ArrayList<Object[]>();
		String strQuery = 
				" select "
				+ " pad.benefitPaymentAllocation.issuer.idIssuerPk, "
				+ " co.corporativeEventType.corporativeEventType, "
				+ " sum(pad.transactionAmount - (pad.taxAmount + pad.custodyAmount)) "
				+ " from CorporativeOperation co, PaymentAllocationDetail pad "
				+ " where pad.benefitPaymentAllocation.idBenefitPaymentPK = :allocationID "
				+ " and pad.corporativeOperation.idCorporativeOperationPk = co.idCorporativeOperationPk "
				+ " group by pad.benefitPaymentAllocation.issuer.idIssuerPk,co.corporativeEventType.corporativeEventType ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("allocationID",allocationID);

		lst = query.getResultList();

		return lst;
	}
	
	/**
	 * Metodo que retorna total de montos a consignar.
	 *
	 * @param allocationID id allocation
	 * @return Total Allocation Amount
	 */
	public BigDecimal getTotalAllocationAmount(Long allocationID){
		BigDecimal allocationAmount = BigDecimal.ZERO;
		String strQuery = 
				"select sum(pad.transactionAmount) "
				+ "from PaymentAllocationDetail pad "
				+ "where pad.benefitPaymentAllocation.idBenefitPaymentPK = :idAllocation ";
		TypedQuery<BigDecimal> query = em.createQuery(strQuery,BigDecimal.class);
		query.setParameter("idAllocation",allocationID);

		allocationAmount = query.getSingleResult();
		return allocationAmount;
	}
	
	/**
	 * Metodo que retorna total de monto comision  a retener.
	 *
	 * @param lstCorporatives list corporative
	 * @return Commission Amount
	 */
	public BigDecimal getCommissionAmount(List<Long> lstCorporatives){
		BigDecimal commissionAmount = BigDecimal.ZERO;

		String strQuery = 
				"select sum(nvl(c.commissionAmount,0)) "
				+ "from CorporativeOperation c "
				+ "where c.idCorporativeOperationPk in (:idCorporatives) ";

		TypedQuery<BigDecimal> query = em.createQuery(strQuery,BigDecimal.class);
		query.setParameter("idCorporatives",lstCorporatives);
		commissionAmount = query.getSingleResult();

		return commissionAmount;
	}
	
	/**
	 * Metodo que retorna lista detalle de consignacion de pagos DIRECTA.
	 *
	 * @param idAllocationProcess id allocation process
	 * @param lstMotives list motives
	 * @return list Block Direct Detail
	 */
	public List<Object[]> getBlockDirectDetail(Long idAllocationProcess, List<Integer> lstMotives){
		List<Object[]> lstAllocationDetail = new ArrayList<Object[]>();

		String strQuery = 
				"select "
				+ "h.ID_HOLDER_PK,"
				+ "h.FULL_NAME,"
				+ "h.DOCUMENT_TYPE,"
				+ "h.DOCUMENT_NUMBER,"
				+ "hab.BANK_ACCOUNT_NUMBER,"
				+ "co.ID_CORPORATIVE_OPERATION_PK,"
				+ "co.ID_ORIGIN_SECURITY_CODE_FK,"
				+ "pad.TRANSACTION_AMOUNT,"
				+ "bpa.IND_EXCLUDED_COMMISSIONS,"
				+ "pad.PROCESS_TYPE,"
				+ "pad.TAX_AMOUNT,"
				+ "pad.CUSTODY_AMOUNT "
				+ "from "
				+ "HOLDER h,CORPORATIVE_OPERATION co,BENEFIT_PAYMENT_ALLOCATION bpa,PAYMENT_ALLOCATION_DETAIL pad "
				+ "left join HOLDER_ACCOUNT_BANK hab on pad.ID_HOLDER_ACCOUNT_BANK_FK=hab.ID_HOLDER_ACCOUNT_BANK_PK "
				+ "where "
				+ "pad.ID_BENEFIT_PAYMENT_FK = :idBenefitPayment "
				+ "and bpa.ID_BENEFIT_PAYMENT_PK=pad.ID_BENEFIT_PAYMENT_FK "
				+ "and pad.ID_HOLDER_FK=h.ID_HOLDER_PK ";

		if(Validations.validateListIsNotNullAndNotEmpty(lstMotives)){
			strQuery = strQuery.concat("and pad.PROCESS_TYPE in (:motives) ");
		}
		strQuery = strQuery.concat("and co.ID_CORPORATIVE_OPERATION_PK=pad.ID_CORPORATIVE_OPERATION_FK ");

		Query query = em.createNativeQuery(strQuery);
		query.setParameter("idBenefitPayment",idAllocationProcess);
		if(Validations.validateListIsNotNullAndNotEmpty(lstMotives)){
			query.setParameter("motives",lstMotives);
		}
		lstAllocationDetail = query.getResultList();

		return lstAllocationDetail;
	}
	
	public List<Object[]> getPayrollDetailAllocation(Long idAllocationProcess){
		List<Object[]> lstAllocationDetail = new ArrayList<Object[]>();

		String strQuery = 
				" select "
				+ " ha.ID_HOLDER_ACCOUNT_PK, "						//0
				+ " FN_GET_HOLDER_NAME(ha.ID_HOLDER_ACCOUNT_PK), "	//1
				+ " p.id_participant_pk, "							//2
				+ " p.description, ";								//3
				if( idepositarySetup.getPaymentType()!= null && idepositarySetup.getPaymentType().equals(BenefitPaymentType.PDD.getCode()) ) {
					strQuery += " iba.ACCOUNT_NUMBER, ";			//4
				}else {
					strQuery += " hab.BANK_ACCOUNT_NUMBER, ";		//4	
				}
				strQuery += " pad.ID_CORPORATIVE_OPERATION_FK, "	//5
				+ " pad.ID_SECURITY_CODE_FK, "			//6
				+ " pad.TRANSACTION_AMOUNT, "			//7
				+ " bpa.IND_EXCLUDED_COMMISSIONS, "		//8
				+ " pad.PROCESS_TYPE, "					//9
				+ " pad.TAX_AMOUNT, "					//10
				+ " pad.CUSTODY_AMOUNT, "				//11
				+ " b.DESCRIPTION as bankDescription "	//12
				+ " from "
				+ " BENEFIT_PAYMENT_ALLOCATION bpa "
				+ " inner join PAYMENT_ALLOCATION_DETAIL pad 	on	 bpa.ID_BENEFIT_PAYMENT_PK=pad.ID_BENEFIT_PAYMENT_FK "//CORPORATIVE_OPERATION co,
				+ " left join HOLDER_ACCOUNT ha 				on	 pad.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK "
				+ " left join PARTICIPANT p 					on	 p.ID_PARTICIPANT_PK = pad.ID_PARTICIPANT_FK ";
				if( idepositarySetup.getPaymentType()!= null && idepositarySetup.getPaymentType().equals(BenefitPaymentType.PDD.getCode()) ) {
					strQuery += " left join INSTITUTION_BANK_ACCOUNT iba on iba.ID_INSTITUTION_BANK_ACCOUNT_PK = pad.ID_INSTITUTION_BANK_ACCOUNT_FK ";
				}else {
					strQuery += " left join HOLDER_ACCOUNT_BANK hab on pad.ID_HOLDER_ACCOUNT_BANK_FK=hab.ID_HOLDER_ACCOUNT_BANK_PK ";
				}
				strQuery += " left join BANK b on pad.ID_BANK_FK = b.ID_BANK_PK ";
				strQuery += " where "
				+ " pad.ID_BENEFIT_PAYMENT_FK = :idBenefitPayment "
				//+ "and pad.ID_HOLDER_FK=h.ID_HOLDER_PK "
				+ " ORDER BY pad.ID_CORPORATIVE_OPERATION_FK, ha.ID_HOLDER_ACCOUNT_PK asc "
				;

		//strQuery = strQuery.concat("and co.ID_CORPORATIVE_OPERATION_PK=pad.ID_CORPORATIVE_OPERATION_FK ");

		Query query = em.createNativeQuery(strQuery);
		query.setParameter("idBenefitPayment",idAllocationProcess);
		lstAllocationDetail = query.getResultList();

		return lstAllocationDetail;
	}
	
	/**
	 * Metodo que retorna numero de cuenta bancaria.
	 *
	 * @param idHolderAccountBank id Holder Account Bank
	 * @return Bank Number
	 */
	public String getBankNumber(Long idHolderAccountBank){
		String accountNumber = StringUtils.EMPTY;
		String strQuery = 
				"select hab.bankAccountNumber "
				+ "from HolderAccountBank hab "
				+ "where hab.idHolderAccountBankPk = :id ";

		TypedQuery<String> query = em.createQuery(strQuery,String.class);
		query.setParameter("id",idHolderAccountBank);

		accountNumber = query.getSingleResult();
		return accountNumber;
	}
	
	/**
	 * Metodo que descripcion de cuenta bancaria.
	 *
	 * @param idHolderAccountBank id Bank Account Type
	 * @return Bank Account Type
	 */
	public String getBankAccountType(Long idHolderAccountBank){
		String accountType = StringUtils.EMPTY;

		String strQuery = 
				"select pt.description "
				+ "from HolderAccountBank hab, ParameterTable pt "
				+ "where hab.idHolderAccountBankPk = :id "
				+ "and pt.parameterTablePk = hab.bankAccountType ";

		TypedQuery<String> query = em.createQuery(strQuery,String.class);
		query.setParameter("id",idHolderAccountBank);

		accountType = query.getSingleResult();
		return accountType;
	}
	
	/**
	 * Metodo que retorna descripcion.
	 *
	 * @param parameterPK parameter pk
	 * @return Parameter Description
	 */
	public String getParameterDescription(Integer parameterPK){
		String accountNumber = StringUtils.EMPTY;
		String strQuery = 
				"select p.description "
				+ "from ParameterTable p "
				+ "where p.parameterTablePk = :id ";

		TypedQuery<String> query = em.createQuery(strQuery,String.class);
		query.setParameter("id",parameterPK);

		accountNumber = query.getSingleResult();
		return accountNumber;
	}

	public void createBlockBenefitDetail(Long idCorporativeOperationPk,Long idProcessCorporateResult, PaymentAllocationDetail paymentAllocationDetail) throws ServiceException{
		
		List<Object[]> lstBlockOperations = getBlockDetail(idCorporativeOperationPk,idProcessCorporateResult);
		if(Validations.validateListIsNotNullAndNotEmpty(lstBlockOperations)){
			for(Object[] objBlockOperation : lstBlockOperations){
				Long blockOperationPK = Long.valueOf(objBlockOperation[0].toString());
				BigDecimal grossBlockAmount = new BigDecimal(objBlockOperation[1].toString());
				BigDecimal taxBlockAmount = new BigDecimal(objBlockOperation[2].toString());
				BigDecimal custodyBlockAmount = new BigDecimal(objBlockOperation[3].toString());
				
				BlockBenefitDetail blockBenefitDetail = new BlockBenefitDetail();
				blockBenefitDetail.setPaymentAllocationDetail(paymentAllocationDetail);
				BlockOperation blockOperation = new BlockOperation();
				blockOperation.setIdBlockOperationPk(blockOperationPK);
				blockBenefitDetail.setBlockOperation(blockOperation);
				
				blockBenefitDetail.setGrossBlockAmount(grossBlockAmount);
				blockBenefitDetail.setTaxAmount(taxBlockAmount);
				blockBenefitDetail.setCustodyAmount(custodyBlockAmount);
				
				create(blockBenefitDetail);
			}
		}
		
	}
	
	/**
	 * Metodo que retorna lista detalle de bloqueos relacionado al proceso corporativo.
	 *
	 * @param corporativeID id corporative
	 * @param idProcessCorporateResult id Process Corporate Result
	 * @return list Block Detail
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getBlockDetail(Long corporativeID,Long idProcessCorporateResult) throws ServiceException{

		List<Object[]> lstBlockDetail = new ArrayList<Object[]>();
		String strQuery = 
				"select "
				+ "bcr.blockOperation.idBlockOperationPk, "					        //[0]
				+ "nvl(bcr.blockedBalance,0), "									    //[1]
				+ "nvl(bcr.taxAmount,0), "											//[2]
				+ "nvl(bcr.custodyAmount,0) "										//[3]
				+ "from BlockCorporativeResult bcr "
				+ "where "
				+ "bcr.corporativeProcessResult.idCorporativeProcessResult = :idCorporativeResult "
				+ "and bcr.corporativeOperation.idCorporativeOperationPk = :idCorporativeOperation ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("idCorporativeOperation",corporativeID);
		query.setParameter("idCorporativeResult",idProcessCorporateResult);
		lstBlockDetail = query.getResultList();

		return lstBlockDetail;
	}
	
	/**
	 * Metodo que retorna lista de consignaciones bloqueadas.
	 *
	 * @param allocationDetailPK allocation detail pk
	 * @return list Block Documents Detail
	 */
	public List<Long> getBlockDocumentsDetail(Long allocationDetailPK){

		List<Long> lstAllocationDetailPK = new ArrayList<Long>();
		String strQuery = 
				"select bbd.idBlockBenefitDetailPk "
				+ "from BlockBenefitDetail bbd "
				+ "where "
				+ "bbd.paymentAllocationDetail.idAllocationDetailPK = :idAllocationDetailID ";

		TypedQuery<Long> query = em.createQuery(strQuery,Long.class);
		query.setParameter("idAllocationDetailID",allocationDetailPK);
		lstAllocationDetailPK = query.getResultList();

		return lstAllocationDetailPK;
	}
	
	/**
	 * Metodo que retorna detalle de consignaciones bloqueadas.
	 *
	 * @param blockBenefitDetail id block Benefit Detail
	 * @return list Block Operation PK
	 */
	public Object[] getBlockOperationPK(Long blockBenefitDetail){
		Object[] objReturn = null;
		String strQuery = 
				"select "
				+ "bbd.blockOperation.idBlockOperationPk,"
				+ "bbd.grossBlockAmount,"
				+ "bbd.taxAmount, "
				+ "bbd.custodyAmount "
				+ "from BlockBenefitDetail bbd "
				+ "where "
				+ "bbd.idBlockBenefitDetailPk = :idBenefitBlockDetail ";

		TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
		query.setParameter("idBenefitBlockDetail",blockBenefitDetail);
		objReturn = query.getSingleResult();

		return objReturn;
	}

	
	public Participant getParticipantByHolderAccount(Long idHolderAccountPk){

		String strQuery = " Select ha From HolderAccount ha JOIN FETCH ha.participant p Where ha.idHolderAccountPk = :idHolderAccountPk ";

		TypedQuery<HolderAccount> query = em.createQuery(strQuery, HolderAccount.class);
		query.setParameter("idHolderAccountPk",idHolderAccountPk);
		
		try {
			List<HolderAccount> lstHolderAccount = query.getResultList();
			if(lstHolderAccount != null && lstHolderAccount.size()>0 ) {
				Participant part = lstHolderAccount.get(0).getParticipant();
				part.getIdParticipantPk();
				return part;		
			}
			return null;
		}catch(NoResultException e) {
			return null;
		}
	}
	
}