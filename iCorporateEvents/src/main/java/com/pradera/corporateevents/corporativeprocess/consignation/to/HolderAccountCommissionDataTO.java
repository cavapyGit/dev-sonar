package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;



// TODO: Auto-generated Javadoc
/**
 * The Class HolderAccountCommissionDataTO.
 * @author Pradera Technologies
 *
 */
public class HolderAccountCommissionDataTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder account. */
	private Long holderAccount;

	/** The factor. */
	private BigDecimal factor;

	/** The primary balance. */
	private BigDecimal primaryBalance;//ORIGIN BEGINNING PRIMARY BALANCE
	
	/** The secondary balance. */
	private BigDecimal secondaryBalance;//ORIGIN BEGINNING SECONDARY BALANCE

	/** The available primary custody. */
	private BigDecimal availablePrimaryCustody;//ORIGIN AVAILABLE PRIMARY BALANCE
	
	/** The available secondary custody. */
	private BigDecimal availableSecondaryCustody;//ORIGIN AVAILABLE SECONDARY BALANCE

	/** The holder account movements. */
	private List<HolderAccountCommissionMovementsTO> holderAccountMovements;//MOVEMENTS BY HOLDER ACCOUNT TO CALCULATE CUSTODY COMMISSION

	/** The primary custody commission. */
	private BigDecimal primaryCustodyCommission;//COMMISSION BY PRIMARY BALANCES
	
	/** The secondary custody commission. */
	private BigDecimal secondaryCustodyCommission;//COMMISSION BY SECONDARY BALANCES
	
	/** The primary custody balance. */
	private BigDecimal primaryCustodyBalance;//PRIMARY BALANCES TO CALCULATE COMMISSION
	
	/** The secondary custody balance. */
	private BigDecimal secondaryCustodyBalance;//SECONDARY BALANCES TO CALCULATE COMMISSION


	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the primary balance.
	 *
	 * @return the primary balance
	 */
	public BigDecimal getPrimaryBalance() {
		return primaryBalance;
	}

	/**
	 * Sets the primary balance.
	 *
	 * @param primaryBalance the new primary balance
	 */
	public void setPrimaryBalance(BigDecimal primaryBalance) {
		this.primaryBalance = primaryBalance;
	}

	/**
	 * Gets the secondary balance.
	 *
	 * @return the secondary balance
	 */
	public BigDecimal getSecondaryBalance() {
		return secondaryBalance;
	}

	/**
	 * Sets the secondary balance.
	 *
	 * @param secondaryBalance the new secondary balance
	 */
	public void setSecondaryBalance(BigDecimal secondaryBalance) {
		this.secondaryBalance = secondaryBalance;
	}

	/**
	 * Gets the available primary custody.
	 *
	 * @return the available primary custody
	 */
	public BigDecimal getAvailablePrimaryCustody() {
		return availablePrimaryCustody;
	}

	/**
	 * Sets the available primary custody.
	 *
	 * @param availablePrimaryCustody the new available primary custody
	 */
	public void setAvailablePrimaryCustody(BigDecimal availablePrimaryCustody) {
		this.availablePrimaryCustody = availablePrimaryCustody;
	}

	/**
	 * Gets the available secondary custody.
	 *
	 * @return the available secondary custody
	 */
	public BigDecimal getAvailableSecondaryCustody() {
		return availableSecondaryCustody;
	}

	/**
	 * Sets the available secondary custody.
	 *
	 * @param availableSecondaryCustody the new available secondary custody
	 */
	public void setAvailableSecondaryCustody(BigDecimal availableSecondaryCustody) {
		this.availableSecondaryCustody = availableSecondaryCustody;
	}

	/**
	 * Gets the holder account movements.
	 *
	 * @return the holder account movements
	 */
	public List<HolderAccountCommissionMovementsTO> getHolderAccountMovements() {
		return holderAccountMovements;
	}

	/**
	 * Sets the holder account movements.
	 *
	 * @param holderAccountMovements the new holder account movements
	 */
	public void setHolderAccountMovements(
			List<HolderAccountCommissionMovementsTO> holderAccountMovements) {
		this.holderAccountMovements = holderAccountMovements;
	}

	/**
	 * Gets the primary custody commission.
	 *
	 * @return the primary custody commission
	 */
	public BigDecimal getPrimaryCustodyCommission() {
		return primaryCustodyCommission;
	}

	/**
	 * Sets the primary custody commission.
	 *
	 * @param primaryCustodyCommission the new primary custody commission
	 */
	public void setPrimaryCustodyCommission(BigDecimal primaryCustodyCommission) {
		this.primaryCustodyCommission = primaryCustodyCommission;
	}

	/**
	 * Gets the secondary custody commission.
	 *
	 * @return the secondary custody commission
	 */
	public BigDecimal getSecondaryCustodyCommission() {
		return secondaryCustodyCommission;
	}

	/**
	 * Sets the secondary custody commission.
	 *
	 * @param secondaryCustodyCommission the new secondary custody commission
	 */
	public void setSecondaryCustodyCommission(BigDecimal secondaryCustodyCommission) {
		this.secondaryCustodyCommission = secondaryCustodyCommission;
	}

	/**
	 * Gets the primary custody balance.
	 *
	 * @return the primary custody balance
	 */
	public BigDecimal getPrimaryCustodyBalance() {
		return primaryCustodyBalance;
	}

	/**
	 * Sets the primary custody balance.
	 *
	 * @param primaryCustodyBalance the new primary custody balance
	 */
	public void setPrimaryCustodyBalance(BigDecimal primaryCustodyBalance) {
		this.primaryCustodyBalance = primaryCustodyBalance;
	}

	/**
	 * Gets the secondary custody balance.
	 *
	 * @return the secondary custody balance
	 */
	public BigDecimal getSecondaryCustodyBalance() {
		return secondaryCustodyBalance;
	}

	/**
	 * Sets the secondary custody balance.
	 *
	 * @param secondaryCustodyBalance the new secondary custody balance
	 */
	public void setSecondaryCustodyBalance(BigDecimal secondaryCustodyBalance) {
		this.secondaryCustodyBalance = secondaryCustodyBalance;
	}

	/**
	 * Gets the factor.
	 *
	 * @return the factor
	 */
	public BigDecimal getFactor() {
		return factor;
	}

	/**
	 * Sets the factor.
	 *
	 * @param factor the new factor
	 */
	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}
}