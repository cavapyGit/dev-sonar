package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.math.BigDecimal;
import java.util.List;

import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.ReportCorporativeResult;
import com.pradera.model.generalparameter.DailyExchangeRates;

// TODO: Auto-generated Javadoc
/**
 * The Interface OperationsFixer.
 *
 * @author PraderaTechnologies
 */
public interface OperationsFixer {
	
	/**
	 * Fix operations.
	 *
	 * @param corpProcessResults the corp process results
	 * @param reportOperations the report operations
	 * @param delFactor the del factor
	 * @param round the round
	 * @param remnantFlag the remnant flag
	 * @param cashFlag the cash flag
	 * @param destiny the destiny
	 * @param balFixer the bal fixer
	 * @param exchangeRateDay the exchange rate day
	 * @return the list
	 */
	public List<ReportCorporativeResult> fixOperations(CorporativeProcessResult corpProcessResults,
			List<Object[]> reportOperations, BigDecimal delFactor, int round, boolean remnantFlag, boolean cashFlag, boolean destiny
			, BalanceFixer balFixer,DailyExchangeRates exchangeRateDay);

	
	/**
	 * Fix operations.
	 *
	 * @param operations the operations
	 * @param reportBalance the report balance
	 * @param totalBalance the total balance
	 * @param balFixer the bal fixer
	 * @return the list
	 */
	public List<ReportCorporativeResult> fixOperations(List<ReportCorporativeResult> operations, BigDecimal reportBalance,
			BigDecimal totalBalance, BalanceFixer balFixer);
	
	/**
	 * Fill origin operations.
	 *
	 * @param reportOperations the report operations
	 * @param corpProcessResults the corp process results
	 * @param exchangeRateDay the exchange rate day
	 * @return the list
	 */
	public List<ReportCorporativeResult> fillOriginOperations(List<Object[]> reportOperations,	CorporativeProcessResult corpProcessResults,DailyExchangeRates exchangeRateDay);
}
