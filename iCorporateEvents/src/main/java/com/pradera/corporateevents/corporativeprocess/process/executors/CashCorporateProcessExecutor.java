package com.pradera.corporateevents.corporativeprocess.process.executors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.services.remote.billing.BillingConexionService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.billing.to.BillingServiceTo;
import com.pradera.core.component.billing.to.ServiceRateTo;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.corporateevents.corporativeprocess.importanceevents.facade.ImportanceEventFacade;
import com.pradera.corporateevents.corporativeprocess.importanceevents.to.CorporativeOperationTO;
import com.pradera.corporateevents.corporativeprocess.process.CorporateExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcess;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BuyerOperationsFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.ChangesFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.OperationsFixer;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CorporateProcessServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CustodyCommissionServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.GrantCouponServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.NationalityType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.component.type.ParameterBalanceType;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.CorporativeResultDetail;
import com.pradera.model.corporatives.CustodyCommissionDetail;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.corporatives.ReportCorporativeResult;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * Clase que ejecuta el interes.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/06/2014
 */
@CorporateProcess(CorporateExecutor.CASH)
@Performance
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@LocalBean
public class CashCorporateProcessExecutor implements CorporateProcessExecutor {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The corporate process service. */
	@EJB
	CorporateProcessServiceBean corporateProcessService;
	
	/** The sc. */
	@Resource
    SessionContext sc;
	
	/** The cash corporate process executor. */
	CashCorporateProcessExecutor cashCorporateProcessExecutor;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The fixer. */
	@Inject
	private BalanceFixer fixer;
	
	/** The document fixer. */
	@Inject
	private DocumentsFixer documentFixer;
	
	/** The operation fixer. */
	@Inject
	private OperationsFixer operationFixer;
	
	/** The seller operation fixer. */
	@Inject
	private BuyerOperationsFixer buyerOperationFixer;

	/** The changes fixer. */
	@Inject
	private ChangesFixer changesFixer;

	/** The custody commission service. */
	@EJB
	private CustodyCommissionServiceBean custodyCommissionService;

	/** The billing service producer. */
	@Inject
	Instance<BillingConexionService> billingServiceProducer;

	/** The coupon service bean. */
	@EJB
	GrantCouponServiceBean couponServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The improtance event facade. */
	@Inject
	private ImportanceEventFacade improtanceEventFacade;
	
	/**
	 * Inicio de la Clase que ejecuta el interes.
	 */
	@PostConstruct
    public void init(){
        this.cashCorporateProcessExecutor = this.sc.getBusinessObject(CashCorporateProcessExecutor.class);
    }
	
	/**
	 * Metodo que obtiene factor de interes.
	 *
	 * @param corpOperation corporative Operation
	 * @return Cash Factor
	 * @throws Exception the exception
	 */
	public BigDecimal getCashFactor(CorporativeOperation corpOperation) throws Exception{
		//obtenemos monto nominal
		BigDecimal cashDelFactor = null;
		List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramInterestCouponActual(corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getDeliveryDate());
		if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
			Long idProgramInterestPk = (Long)lstIdProgramInterestCoupon.get(0);
			ProgramInterestCoupon objProgramInterestCoupon = (ProgramInterestCoupon)corporateExecutorService.getProgramInterestCoupon(idProgramInterestPk);
			cashDelFactor = objProgramInterestCoupon.getCouponAmount();
		}else{
			cashDelFactor = CorporateProcessUtils.multiply(CorporateProcessUtils.divide(corpOperation.getDeliveryFactor(), CorporateProcessConstants.AMOUNT_ROUND), 
					corpOperation.getSecurities().getCurrentNominalValue());
		}
		return cashDelFactor;
	}
	
	/**
	 * Metodo que procesa preliminar de beneficios en efectivo si existe operaciones de retiro.
	 *
	 * @param corpOperation corporate operation
	 * @param retirementOperation retirement operation
	 * @param amountNominal the amount nominal
	 * @param loggerUser user
	 * @throws Exception exception
	 */
	public void processCorporateResultForRetirementOperation(CorporativeOperation corpOperation,RetirementOperation retirementOperation,BigDecimal amountNominal ,LoggerUser loggerUser) throws Exception{
		//eliminamos preliminares previos
		corporateExecutorService.deletePreviousPreliminaryInformation(corpOperation.getIdCorporativeOperationPk(),
				CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,true,true);			
		//new transaction
		this.cashCorporateProcessExecutor.createCorporateResultCash(corpOperation, loggerUser, retirementOperation, amountNominal);		
		//new transaction
		this.cashCorporateProcessExecutor.createCorporateResultSecurities(corpOperation, loggerUser, retirementOperation);
	}
	/**
	 * Metodo que ejecuta proceso prelimiar de beneficios en efectivo.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executePreliminarProcess(CorporativeOperation corpOperation) throws Exception{
		
		log.info("started preliminary process"+corpOperation.getSecurities().getIdSecurityCodePk());
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		
		//declaramos variables
		boolean flag = false ,resultProcess = false;
		Long stockProcess = null , reportStockprocess = null ,OperId = null;
		
		//obtenemos proceso corporativo a procesar
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		
		//obtenemos monto nominal
		BigDecimal amountNominal = getCashFactor(corpOperation);
		
		//comentado por ahora 2022 CAVAPY
		//Se valida si el corporativo esta relacionado a una operacion de retiro de valores
//		RetirementOperation retirementOperation = corporateExecutorService.findRetirementOperation(corpOperation.getIdCorporativeOperationPk());
		RetirementOperation retirementOperation  = null;
		if(Validations.validateIsNotNull(retirementOperation)){
			processCorporateResultForRetirementOperation(corpOperation, retirementOperation, amountNominal, loggerUser);
			flag = true;
		}
		else{			
			//Se valida si es un proceso de pago de remanentes
			if (processType.equals(ImportanceEventType.REMANENT_PAYMENT)){
				resultProcess = true;
				OperId = corporateExecutorService.getRemanentProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(),
						corpOperation.getSecurities().getIdSecurityCodePk());
				//Se valida si es un proceso de devolucion de aportes
			}else if(processType.equals(ImportanceEventType.RETURN_OF_CONTRIBUTION)){
				resultProcess = true;
				OperId = getCorporariveContributionOperationForReturn(corpOperation);
			}else {
				resultProcess = false;
				//se obtiene id del proceso de stock Normal
				stockProcess = corporateExecutorService.getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
						corpOperation.getSecurities().getIdSecurityCodePk(), StockStateType.FINISHED.getCode(), StockClassType.NORMAL.getCode(),
						StockType.CORPORATIVE.getCode());
				//se obtiene id del proceso de stock Report
				reportStockprocess = corporateExecutorService.getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
						corpOperation.getSecurities().getIdSecurityCodePk()	, StockStateType.FINISHED.getCode(), StockClassType.REPORT.getCode(), 
						StockType.CORPORATIVE.getCode());
			}
			//validamos si existe stock NORMAL O si existe remanente
			if ((stockProcess != null && stockProcess > 0) || (OperId != null && OperId > 0)) {
				//eliminamos preliminares previos
				corporateExecutorService.deletePreviousPreliminaryInformation(corpOperation.getIdCorporativeOperationPk(), BooleanType.NO.getCode(),true,true);
				//obtenemos data de las cuentas 
				//(codigo alternativo - promedio total de saldo - tipo titular - 
				//nacionalidad - cantidad de titulares - tipo de cuenta )
				List<Object[]> distinctHolders = null , distinctHoldersAux = null;
				if ((OperId != null && OperId > 0)) {
					if (processType.equals(ImportanceEventType.REMANENT_PAYMENT)){
						distinctHolders = corporateExecutorService.getDistinctHoldersForRemantWithHolderType(OperId);
					}else if(processType.equals(ImportanceEventType.RETURN_OF_CONTRIBUTION)){
						distinctHolders = corporateExecutorService.getDistinctHoldersForContributionReturnWithHolderType(OperId,new Integer(CorporateProcessConstants.BALANCE_ORIGIN));
						distinctHoldersAux = corporateExecutorService.getDistinctHoldersForContributionReturnWithHolderType(OperId,new Integer(CorporateProcessConstants.BALANCE_DESTINY));
						for(Object[] obj : distinctHolders){
							for(Object[] objAux : distinctHoldersAux){
								if(obj[0].toString().equals(objAux[0].toString())){
									BigDecimal totalOrigin = (BigDecimal) obj[1];
									BigDecimal totalTarget = (BigDecimal) objAux[1];
									totalOrigin = totalOrigin.subtract(totalTarget);
									obj[1] = totalOrigin;
								}
							}
						}
					}					
				} else {
					distinctHolders = corporateExecutorService.getDistinctStockHoldersWithHolderType(stockProcess);
				}
				//ejecutamos algoritmo de procesos preliminar
				if (distinctHolders != null && distinctHolders.size() > 0) {
					flag = this.cashCorporateProcessExecutor.executePreliminarProcessAlgorithm(distinctHolders, corpOperation, stockProcess,
							processType, loggerUser, amountNominal, resultProcess, OperId, reportStockprocess);
				}
			}
		}		
		
		return flag;
	}
	 
 	/**
 	 * Metodo que retorna datos del corporativo de reduccion o exclusion asociado a devolucion de aportes.
 	 *
 	 * @param corpOperation the corp operation
 	 * @return the corporarive contribution operation for return
 	 */
 	public Long getCorporariveContributionOperationForReturn(CorporativeOperation corpOperation){
			CorporativeOperationTO to = new CorporativeOperationTO();
			to.setSourceSecurityCode(corpOperation.getSecurities().getIdSecurityCodePk());
			to.setState(CorporateProcessStateType.DEFINITIVE.getCode());
			to.setIndRemanent(BooleanType.NO.getCode());
			to.setCorporativeEvent(ImportanceEventType.CAPITAL_REDUCTION.getCode());
			
			CorporativeOperation operati;
			try {
				operati = improtanceEventFacade.findCorporativeOperationByFilter(to);			
				if (Validations.validateIsNotNull(operati)) {
					return operati.getIdCorporativeOperationPk();
				} else {
					to.setCorporativeEvent(ImportanceEventType.SECURITIES_EXCLUSION.getCode());
					operati = improtanceEventFacade.findCorporativeOperationByFilter(to);
					if (Validations.validateIsNotNull(operati)) {
						return operati.getIdCorporativeOperationPk();
					}			
				}
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	 }
	
	/**
	 * Metodo que ejecuta algoritmo preliminar de beneficios en efectivo.
	 *
	 * @param distinctHolders the distinct holders
	 * @param corpOperation the corp operation
	 * @param stockProcess the stock process
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @param amountNominal the amount nominal
	 * @param resultProcess the result process
	 * @param OperId the oper id
	 * @param reportStockprocess the report stockprocess
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean executePreliminarProcessAlgorithm(List<Object[]> distinctHolders,CorporativeOperation corpOperation,Long stockProcess,
			ImportanceEventType processType,LoggerUser loggerUser,BigDecimal amountNominal,boolean resultProcess,Long OperId,Long reportStockprocess) throws ServiceException{
		
		//declaramos variables
		boolean flag = false;
		Map<Long,BigDecimal> mpHolderAccountTax = new HashMap<Long, BigDecimal>();
		List<CorporativeProcessResult> changedResults = new ArrayList<CorporativeProcessResult>();
		CorporativeProcessResult totalProcessResult = null,totalProcessResultExchange = null;
		List<ReportCorporativeResult> reportOperations = null;
		List<BlockCorporativeResult> blockDocuments = null;
		DailyExchangeRates exchangeRateDay = null;
		
		//validamos factor de impuesto
		BigDecimal taxFactor = BigDecimal.ZERO;
		BigDecimal totalTax = BigDecimal.ZERO;
		boolean calculateTax = false;
		if (corpOperation.getIndTax() != null && corpOperation.getIndTax().equals(BooleanType.YES.getCode())) {
			calculateTax = true;
			//obtenemos factor de impuesto 		
			if(	Validations.validateIsNotNull(corpOperation.getTaxFactor()) 
					&& Validations.validateIsNotNullAndPositive(corpOperation.getTaxFactor())){
				taxFactor = corpOperation.getTaxFactor();
			}
		}
		
		//obtener data de operaciones de participantes unificados
		Map<Long, Object[]> uniParticipants = corporateExecutorService.getUnifiedParticipants(
				corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getRegistryDate());

		//obtener data de operaciones de cambio de titularidad
		Map<Long, List<Object[]>> changeOwnership = corporateExecutorService.getChangeofOwnerShipSucession(
				corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getRegistryDate());
		
		//si la moneda del procesos es ufv o dmv entonces obtenemos tipo de cambio
		if(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
				corpOperation.getCurrency().equals(CurrencyType.DMV.getCode())){	
			exchangeRateDay = parameterServiceBean.getDayExchangeRate(corpOperation.getExchangeDateRate(),corpOperation.getCurrency());
		}
		
		//recorremos la lista de cuentas para el proceso
		for (Object[] arrObj : distinctHolders) {
			
			//obtenemos datos de la cuenta (codigo alterno - cuenta - tipo cuenta )
			String holderAlternateCode = (String) arrObj[0];
			BigDecimal holderCount = new BigDecimal(arrObj[4].toString());
			Integer accountType= new Integer(arrObj[5].toString());
			
			//obtenemos saldo total por cuenta
			BigDecimal totalBalance = CorporateProcessUtils.multiply(((BigDecimal) arrObj[1]),CorporateProcessConstants.AMOUNT_ROUND);
			
			//validamos si tipo de cuenta es MANCOMUNADO ... 
			//calculamos saldo total sin beneficio por cuenta se recalcula (total_saldo/numero_titular)
			if (HolderAccountType.OWNERSHIP.getCode().equals(accountType)) {
				totalBalance = totalBalance.divide(holderCount,2,RoundingMode.HALF_UP);
			}
			
			//calculamos monto total con beneficio por cuenta
			BigDecimal roundedTotal = CorporateProcessUtils.calculateByFactor(totalBalance, amountNominal,corpOperation.getIndRound());	
			
			//creamos proceso resultado totales y guardamos monto totales con benefico por cuenta
			if(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					corpOperation.getCurrency().equals(CurrencyType.DMV.getCode())){				
				totalProcessResultExchange = new CorporativeProcessResult();
				totalProcessResultExchange.setTotalBalance(roundedTotal);
				totalProcessResultExchange.setCorporativeOperation(corpOperation);
				totalProcessResultExchange.setSecurity(corpOperation.getSecurities());
				roundedTotal = CorporateProcessUtils.calculateByFactor(roundedTotal, exchangeRateDay.getBuyPrice(),corpOperation.getIndRound());	
				totalProcessResult = new CorporativeProcessResult();
				totalProcessResult.setTotalBalance(roundedTotal);
				totalProcessResult.setCorporativeOperation(corpOperation);
				totalProcessResult.setSecurity(corpOperation.getSecurities());
			}else{
				totalProcessResult = new CorporativeProcessResult();
				totalProcessResult.setTotalBalance(roundedTotal);
				totalProcessResult.setCorporativeOperation(corpOperation);
				totalProcessResult.setSecurity(corpOperation.getSecurities());
			}			

			//obtenemos cuentas matrices con sus saldos respectivos de cuenta asociada
			List<Object[]> holderAccounts = null,holderAccountsAux=null;
			if(resultProcess){
				if(processType.equals(ImportanceEventType.REMANENT_PAYMENT)){
					holderAccounts = corporateExecutorService.getCorporateResultsForRemnant(OperId, holderAlternateCode, CorporateProcessConstants.IND_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
				}else if(processType.equals(ImportanceEventType.RETURN_OF_CONTRIBUTION)){
					holderAccounts = corporateExecutorService.getCorporateResultsForContributionResult(OperId, holderAlternateCode, new Integer(CorporateProcessConstants.BALANCE_ORIGIN));
					holderAccountsAux = corporateExecutorService.getCorporateResultsForContributionResult(OperId, holderAlternateCode, new Integer(CorporateProcessConstants.BALANCE_DESTINY));
					for(Object[] obj : holderAccounts){
						for(Object[] objAux : holderAccountsAux){
							if(obj[0].toString().equals(objAux[0].toString()) && 
									 new Long(obj[11].toString()).equals(new Long(objAux[11].toString()))){
								BigDecimal availableOrigin = new BigDecimal(obj[1].toString()).subtract(new BigDecimal(objAux[1].toString()));
								BigDecimal pawnOrigin = new BigDecimal(obj[2].toString()).subtract(new BigDecimal(objAux[2].toString()));
								BigDecimal banOrigin = new BigDecimal(obj[3].toString()).subtract(new BigDecimal(objAux[3].toString()));
								BigDecimal otherBlockOrigin = new BigDecimal(obj[4].toString()).subtract(new BigDecimal(objAux[4].toString()));
								BigDecimal reserveOrigin = new BigDecimal(obj[5].toString()).subtract(new BigDecimal(objAux[5].toString()));
								BigDecimal oppositionOrigin = new BigDecimal(obj[6].toString()).subtract(new BigDecimal(objAux[6].toString()));
								BigDecimal marginOrigin = new BigDecimal(obj[7].toString()).subtract(new BigDecimal(objAux[7].toString()));
								BigDecimal reportingOrigin = new BigDecimal(obj[8].toString()).subtract(new BigDecimal(objAux[8].toString()));
								BigDecimal reportedOrigin = new BigDecimal(obj[9].toString()).subtract(new BigDecimal(objAux[9].toString()));
								BigDecimal totalOrigin = new BigDecimal(obj[10].toString()).subtract(new BigDecimal(objAux[10].toString()));								
								obj[1] = availableOrigin;
								obj[2] = pawnOrigin;
								obj[3] = banOrigin;
								obj[4] = otherBlockOrigin;
								obj[5] = reserveOrigin;
								obj[6] = oppositionOrigin;
								obj[7] = marginOrigin;
								obj[8] = reportingOrigin;
								obj[9] = reportedOrigin;
								obj[10] = totalOrigin;
							}
						}
					}
				}				
			}else{
				holderAccounts =corporateExecutorService.getStockCalculationBalances(stockProcess, holderAlternateCode);
			}
			
			//verificamos si existe cuentas matrices para cuenta asociada
			if (holderAccounts != null && holderAccounts.size() > 0) {
			
				//obtenemos lista de CUENTAS MATRICES RECONSTRUIDOS CON MONTOS DE BENEFICIO SIN IMPUESTO de cuenta asociada 
				List<CorporativeProcessResult> holderAccResults = reprocessBalances(holderAccounts, corpOperation, amountNominal, 
						corpOperation.getIndRound(), true, false, true, processType,exchangeRateDay);

				//verificamos si aplica impuesto y calculamos el impuesto en decimales
				BigDecimal tax = null, tmpAmountNominal = null , realTaxFactor = null;
				if(calculateTax){
					realTaxFactor = BigDecimal.ONE.subtract(CorporateProcessUtils.divide(taxFactor, CorporateProcessConstants.AMOUNT_ROUND));
				}
				
				//Recorremos los montos preliminares para guardarlo y trabajar a nivel de operacion
				//(operaciones de bloqueo y operaciones de reporto)
				//ademas validar verificar y calcular monto de impuesto				
				for (Iterator<CorporativeProcessResult> itHolder=holderAccResults.iterator(); itHolder.hasNext();) {
					CorporativeProcessResult corpProcessResults =  itHolder.next();					
												
					//inicializamos en cero variable auxiliar para obtener el monto impuesto
					tax=BigDecimal.ZERO;
							
					//guardamos monto nominal temporal para operaciones de reporte y operaciones de bloqueo
					if(tmpAmountNominal == null){
						tmpAmountNominal = amountNominal;
					}

					//obtenemos monto beneficio total de cuenta matriz
					totalBalance = corpProcessResults.getTotalBalance();
					//verificamos si aplica impuesto por cuenta matriz
					if(calculateTax){			
						Long idHolderAccount = corpProcessResults.getHolderAccount().getIdHolderAccountPk();
						String idSecurity = corpProcessResults.getSecurity().getIdSecurityCodePk();
						Security objSecurity = corporateExecutorService.getSecurity(idSecurity);
						List<Holder> lstHolder = corporateExecutorService.getListHolder(idHolderAccount);
						mpHolderAccountTax = new HashMap<Long, BigDecimal>();
						//recorremos lista de titulares y discriminamos
						for(Holder objHolder : lstHolder){
							//verificamos si titular de la cuenta aplica impuesto
							if(CorporateProcessUtils.isTaxExempt(objSecurity, objHolder).equals(BooleanType.YES.getCode()) && 
									!mpHolderAccountTax.containsKey(corpProcessResults.getHolderAccount().getIdHolderAccountPk())){
								if(itHolder.hasNext()){
									BigDecimal newTotalBalance = CorporateProcessUtils.calculateByFactor(corpProcessResults.getTotalBalance(), realTaxFactor, corpOperation.getIndRound());
									tax = totalBalance.subtract(newTotalBalance);
									tax = CorporateProcessUtils.divide(tax, CorporateProcessConstants.AMOUNT_ROUND);
									totalTax = totalTax.add(tax);		
								}else{
									BigDecimal newTotalBalance = CorporateProcessUtils.calculateByFactor(totalProcessResult.getTotalBalance(), realTaxFactor, corpOperation.getIndRound());
									tax = totalBalance.subtract(newTotalBalance);
									tax = tax.subtract(totalTax);
									tax = CorporateProcessUtils.divide(tax, CorporateProcessConstants.AMOUNT_ROUND);
								}
								mpHolderAccountTax.put(corpProcessResults.getHolderAccount().getIdHolderAccountPk(),realTaxFactor);
								corpProcessResults.setTaxAmount(tax);	
							}
						}						
					}											

					//validamos si cuenta tiene saldos bloqueados
					if(CorporateProcessUtils.hasBlockBalances(corpProcessResults)){
						List<Object[]> blockedDocs = null , blockedDocsAux = null;
						if (resultProcess) {
							if(processType.equals(ImportanceEventType.REMANENT_PAYMENT)){
								blockedDocs = corporateExecutorService.getBlockCorpProcessDetails(OperId, corpProcessResults.getParticipant().getIdParticipantPk(), 
										corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
										corpProcessResults.getSecurity().getIdSecurityCodePk(), 
										CorporateProcessConstants.IND_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
							}else if(processType.equals(ImportanceEventType.RETURN_OF_CONTRIBUTION)){
								blockedDocs = corporateExecutorService.getBlockCorpForContributionReturnProcessDetails(OperId, corpProcessResults.getParticipant().getIdParticipantPk(), 
										corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
										corpProcessResults.getSecurity().getIdSecurityCodePk(), 
										CorporateProcessConstants.DEFINITIVE_PROCESSED,new Integer(CorporateProcessConstants.BALANCE_ORIGIN));
								blockedDocsAux = corporateExecutorService.getBlockCorpForContributionReturnProcessDetails(OperId, corpProcessResults.getParticipant().getIdParticipantPk(), 
										corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
										corpProcessResults.getSecurity().getIdSecurityCodePk(), 
										CorporateProcessConstants.DEFINITIVE_PROCESSED,new Integer(CorporateProcessConstants.BALANCE_DESTINY));
								for(Object[] obj : blockedDocs){
									for(Object[] objAux : blockedDocsAux){
										if(obj[2].toString().equals(objAux[2].toString())){
											BigDecimal totalOrigin = (BigDecimal) obj[1];
											BigDecimal totalTarget = (BigDecimal) objAux[1];
											totalOrigin = totalOrigin.subtract(totalTarget);
											obj[1] = totalOrigin;
										}
									}
								}
							}							
						} else {
							blockedDocs =corporateExecutorService.getBlockStockCalculationDetails(stockProcess, corpProcessResults.getParticipant().getIdParticipantPk(), 
									corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
									corpProcessResults.getSecurity().getIdSecurityCodePk());
						}

						if(blockedDocs != null && blockedDocs.size() > 0){
							blockDocuments = documentFixer.fixDocuments(corpProcessResults,blockedDocs,tmpAmountNominal,corpOperation.getIndRound(),
																		false,false,true,fixer,processType,exchangeRateDay);

							corpProcessResults.setBlockCorporativeResults(blockDocuments);
						}
					}

					//validamos si cuenta tiene saldos reporto (rol venta)
					if(CorporateProcessUtils.hasReportBalances(corpProcessResults)){
						List<Object[]> reportOper = null,reportOperAux = null; 
						if (resultProcess) {
							if(processType.equals(ImportanceEventType.REMANENT_PAYMENT)){
								reportOper = corporateExecutorService.getCorpReportOperations(OperId, corpProcessResults.getParticipant().getIdParticipantPk(), 
										corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
										corpProcessResults.getSecurity().getIdSecurityCodePk(), 
										CorporateProcessConstants.IND_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
							}else if(processType.equals(ImportanceEventType.RETURN_OF_CONTRIBUTION)){
								reportOper = corporateExecutorService.getCorpReportForContributionReturnOperations(OperId, corpProcessResults.getParticipant().getIdParticipantPk(), 
										corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
										corpProcessResults.getSecurity().getIdSecurityCodePk(), 
										CorporateProcessConstants.DEFINITIVE_PROCESSED,new Integer(CorporateProcessConstants.BALANCE_ORIGIN));
								reportOperAux = corporateExecutorService.getCorpReportForContributionReturnOperations(OperId, corpProcessResults.getParticipant().getIdParticipantPk(), 
										corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
										corpProcessResults.getSecurity().getIdSecurityCodePk(), 
										CorporateProcessConstants.DEFINITIVE_PROCESSED,new Integer(CorporateProcessConstants.BALANCE_ORIGIN));
								for(Object[] obj : reportOper){
									for(Object[] objAux : reportOper){
										if(new Long(obj[3].toString()).equals(new Long(objAux[3].toString())) && 
												new Long(obj[4].toString()).equals(new Long(objAux[4].toString()))){
											BigDecimal totalOrigin = (BigDecimal) obj[2];
											BigDecimal totalTarget = (BigDecimal) objAux[2];
											totalOrigin = totalOrigin.subtract(totalTarget);
											obj[2] = totalOrigin;
										}
									}
								}
							}							
						}else{
							reportOper = corporateExecutorService.getStockReportOperations(reportStockprocess, corpProcessResults.getParticipant().getIdParticipantPk(), 
									corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
									corpProcessResults.getSecurity().getIdSecurityCodePk());
						}
						if(reportOper != null && reportOper.size() > 0){
							reportOperations = operationFixer.fixOperations(corpProcessResults, reportOper, tmpAmountNominal, 
									corpOperation.getIndRound(), false, true, true, fixer,exchangeRateDay);
							corpProcessResults.setReportCorporativeResults(reportOperations);
						}
					}

					//actualizar resultado de cuentas matrices por unificacion de participante o cambio de titularidad
					List<CorporativeProcessResult> changesCorp = changesFixer.getUpdatedResult(new ArrayList<CorporativeProcessResult>(), corpProcessResults, 
							loggerUser, uniParticipants, changeOwnership, documentFixer, fixer, corpOperation.getIndRound(), true, realTaxFactor);
					
					//si existe actualizacion resultado de cuentas matrices por unificacion de participante o cambio de titularidad
					if(changesCorp != null && !changesCorp.isEmpty()){						
						changesCorp = CorporateProcessUtils.divideForCash(changesCorp);
						changedResults.addAll(changesCorp);
					}else{//guardamos cuentas matrices con beneficio sin impuesto
						//actualizamos saldos de bloqueos dependiendo los indicadores de pago
						corpProcessResults = documentFixer.updateBlockDocumentsEffections(corpProcessResults, true);
						CorporateProcessUtils.divideForCash(corpProcessResults);						
						corporateExecutorService.create(corpProcessResults, loggerUser);
						corpProcessResults = new CorporativeProcessResult();
					}
				}
			}
			holderAccounts = null;
			flag = true;
		}
	
		if(!changedResults.isEmpty()){
			for(CorporativeProcessResult corpRes : changedResults ){
				if(corpRes.getTotalBalance().compareTo(BigDecimal.ZERO) > 0 || corpRes.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
					corpRes = documentFixer.updateBlockDocumentsEffections(corpRes, false);
					corporateExecutorService.updateOrCreate(corpRes, loggerUser);
				}
			}
			corporateExecutorService.updateBlockCorporateForResult(corpOperation.getIdCorporativeOperationPk(), loggerUser);
		}

		//validamos si cuenta tiene saldos reporto (rol compra)
		List<Object[]> sumReported = null,sumReportedAux = null, buyerOperations = null,buyerOperationsAux = null; 
		if (resultProcess) {
			if(processType.equals(ImportanceEventType.REMANENT_PAYMENT)){
				sumReported = corporateExecutorService.getRemnantSellerBuyyerSumReportOperations(OperId, 
						corpOperation.getIdCorporativeOperationPk(), GuaranteeType.PRINCIPAL.getCode(), 
						GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), ParticipantRoleType.SELL.getCode(), 
						ParticipantRoleType.BUY.getCode(),CorporateProcessConstants.IND_REMAINDER,CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, 
						CorporateProcessConstants.BALANCE_DESTINY, CorporateProcessConstants.BALANCE_DESTINY,
						CorporateProcessConstants.EXCHANGE_DESTINY, CorporateProcessConstants.EXCHANGE_DESTINY);
				buyerOperations = corporateExecutorService.getRemnantBuyerReportOperations(OperId,
						GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
						ParticipantRoleType.BUY.getCode(), 
						CorporateProcessConstants.IND_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			}else if(processType.equals(ImportanceEventType.RETURN_OF_CONTRIBUTION)){
				sumReported = corporateExecutorService.getRemnantSellerBuyyerSumReportOperations(OperId, 
						corpOperation.getIdCorporativeOperationPk(), GuaranteeType.PRINCIPAL.getCode(), 
						GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), ParticipantRoleType.SELL.getCode(), 
						ParticipantRoleType.BUY.getCode(),CorporateProcessConstants.IND_NO_REMAINDER,CorporateProcessConstants.DEFINITIVE_PROCESSED, 
						CorporateProcessConstants.BALANCE_DESTINY, CorporateProcessConstants.BALANCE_DESTINY,
						CorporateProcessConstants.EXCHANGE_DESTINY, CorporateProcessConstants.EXCHANGE_DESTINY);
				sumReportedAux = corporateExecutorService.getRemnantSellerBuyyerSumReportOperations(OperId, 
						corpOperation.getIdCorporativeOperationPk(), GuaranteeType.PRINCIPAL.getCode(), 
						GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), ParticipantRoleType.SELL.getCode(), 
						ParticipantRoleType.BUY.getCode(),CorporateProcessConstants.IND_NO_REMAINDER,CorporateProcessConstants.DEFINITIVE_PROCESSED, 
						CorporateProcessConstants.BALANCE_ORIGIN, CorporateProcessConstants.BALANCE_ORIGIN,
						CorporateProcessConstants.EXCHANGE_ORIGIN, CorporateProcessConstants.EXCHANGE_ORIGIN);
				for(Object[] obj : sumReported){
					for(Object[] objAux : sumReportedAux){
						if(new Long(obj[0].toString()).equals(new Long(objAux[0].toString()))){
							BigDecimal reportOrigin = (BigDecimal) obj[1];
							BigDecimal reportTarget = (BigDecimal) objAux[1];
							reportOrigin = reportOrigin.subtract(reportTarget);
							obj[1] = reportTarget;
							BigDecimal totalOrigin = (BigDecimal) obj[2];
							BigDecimal totalTarget = (BigDecimal) objAux[2];
							totalOrigin = totalOrigin.subtract(totalTarget);
							obj[2] = totalOrigin;
						}
					}
				}
				buyerOperations = corporateExecutorService.getContributionReturnBuyerReportOperations(OperId,
						GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
						ParticipantRoleType.BUY.getCode(), 
						CorporateProcessConstants.DEFINITIVE_PROCESSED,new Integer(CorporateProcessConstants.BALANCE_ORIGIN));
				buyerOperationsAux = corporateExecutorService.getContributionReturnBuyerReportOperations(OperId,
						GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
						ParticipantRoleType.BUY.getCode(), 
						CorporateProcessConstants.DEFINITIVE_PROCESSED,new Integer(CorporateProcessConstants.BALANCE_DESTINY));
				for(Object[] obj : buyerOperations){
					for(Object[] objAux : buyerOperations){
						if(new Long(obj[6].toString()).equals(new Long(objAux[6].toString()))){
							BigDecimal totalOrigin = (BigDecimal) obj[1];
							BigDecimal totalTarget = (BigDecimal) objAux[1];
							totalOrigin = totalOrigin.subtract(totalTarget);
							obj[1] = totalOrigin;
						}
					}
				}
			}			
		}else{
			sumReported = corporateExecutorService.getSellerBuyyerSumReportOperations(reportStockprocess, 
					corpOperation.getIdCorporativeOperationPk(),
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
					ParticipantRoleType.SELL.getCode(), ParticipantRoleType.BUY.getCode(),CorporateProcessConstants.IND_NO_REMAINDER, 
					CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,
					CorporateProcessConstants.BALANCE_DESTINY,CorporateProcessConstants.EXCHANGE_DESTINY);
			buyerOperations = corporateExecutorService.getBuyerStockReportOperations(reportStockprocess, 
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(),
					ParticipantRoleType.BUY.getCode());
		}

		if(sumReported != null && !sumReported.isEmpty()){
			reportOperations = buyerOperationFixer.fixOperations(sumReported,buyerOperations,corpOperation,corpOperation.getIndRound(),
					false,true,true,fixer,exchangeRateDay);
			corporateExecutorService.saveAll(reportOperations, loggerUser);
			
			corporateExecutorService.updateReportedBalanceofCorporateResults(corpOperation.getIdCorporativeOperationPk(), 
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
					ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			corporateExecutorService.updateReportedOperationsofCorporateResult(corpOperation.getIdCorporativeOperationPk(), 
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
					ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		}

		//REALIZE THE COMMISSION CALCULATION
		Map<Long,BigDecimal> mpHolderAccountCustody = new HashMap<Long, BigDecimal>();
		if (processType.equals(ImportanceEventType.INTEREST_PAYMENT)) {
			mpHolderAccountCustody = executeCommissionCalculationProcess(corpOperation.getIdCorporativeOperationPk(),calculateTax);
		}

		//CREATE THE TAX & CUSTODY AMOUNTS DETAILED BY BALANCES AND EXECUTE ADJUSTMENTS PREVIOUSLY REGISTERED
		reprocessCorporativeResults(corpOperation,loggerUser,mpHolderAccountCustody,mpHolderAccountTax,calculateTax);

		corporateExecutorService.updateHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO, CorporateProcessConstants.IND_YES, CorporateProcessConstants.IND_YES, loggerUser);
		
		//WE UPDATE AMOUNTS FOR CORPORATIVE OPERATION ACCORDING TO CORPORATE_PROCESS_RESULT
		corpOperation.setPaymentAmount(corporateExecutorService.getTotalBalance(corpOperation.getIdCorporativeOperationPk()));
		corpOperation.setIssuerConfirmedAmount(corpOperation.getPaymentAmount());
		corpOperation.setCustodyAmount(BigDecimal.ZERO);
		corpOperation.setCommissionAmount(BigDecimal.ZERO);
		corpOperation.setTaxAmount(corporateExecutorService.getTaxAmount(corpOperation.getIdCorporativeOperationPk()));
		corpOperation.setExchangeAmount(corporateExecutorService.getExchangeAmount(corpOperation.getIdCorporativeOperationPk()));
		corpOperation.setIdStockCalculation(stockProcess);
		corporateExecutorService.updateCorporativeCustodyAmounts(corpOperation);
		return flag;
	}
	
	/**
	 * Metodo que registra saldos de preliminar de beneficios en efectivo.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @param retirementOperation the retirement operation
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createCorporateResultSecurities(CorporativeOperation corpOperation,LoggerUser loggerUser,RetirementOperation retirementOperation) throws ServiceException{
		for(RetirementDetail retirementDetail:retirementOperation.getRetirementDetails()){
			//Registro de Valores
			CorporativeProcessResult corporativeProcessResult = new CorporativeProcessResult();
			corporativeProcessResult.setHolderAccount(retirementDetail.getHolderAccount());
			corporativeProcessResult.setSecurity(retirementOperation.getSecurity());
			corporativeProcessResult.setParticipant(retirementDetail.getParticipant());
			corporativeProcessResult.setIndRemanent(corpOperation.getIndRemanent());
			corporativeProcessResult.setTotalBalance(retirementDetail.getAvailableBalance());
			corporativeProcessResult.setAvailableBalance(retirementDetail.getAvailableBalance());					
			corporativeProcessResult.setTransitBalance(BigDecimal.ZERO);
			corporativeProcessResult.setPawnBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBanBalance(BigDecimal.ZERO);
			corporativeProcessResult.setOtherBlockBalance(BigDecimal.ZERO);
			corporativeProcessResult.setAcreditationBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReserveBalance(BigDecimal.ZERO);
			corporativeProcessResult.setOppositionBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBuyBalance(BigDecimal.ZERO);
			corporativeProcessResult.setSellBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReportingBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReportedBalance(BigDecimal.ZERO);
			corporativeProcessResult.setMarginBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBorrowerBalance(BigDecimal.ZERO);
			corporativeProcessResult.setLenderBalance(BigDecimal.ZERO);
			corporativeProcessResult.setLoanableBalance(BigDecimal.ZERO);	
			corporativeProcessResult.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			corporativeProcessResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_ORIGIN);
			corporativeProcessResult.setCorporativeOperation(corpOperation);
			corporateExecutorService.create(corporativeProcessResult, loggerUser);
		}
	}
	
	/**
	 * Metodo que registra montos de preliminar de beneficios en efectivo.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @param retirementOperation the retirement operation
	 * @param cashDelFactor the cash del factor
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createCorporateResultCash(CorporativeOperation corpOperation,LoggerUser loggerUser,RetirementOperation retirementOperation,BigDecimal cashDelFactor) throws ServiceException{
		BigDecimal totalBalance = BigDecimal.ZERO;

		for(RetirementDetail retirementDetail:retirementOperation.getRetirementDetails()){
			//Registro efectivo
			CorporativeProcessResult corporativeProcessResult = new CorporativeProcessResult();
			corporativeProcessResult.setHolderAccount(retirementDetail.getHolderAccount());
			corporativeProcessResult.setSecurity(retirementOperation.getSecurity());
			corporativeProcessResult.setParticipant(retirementDetail.getParticipant());
			corporativeProcessResult.setIndRemanent(corpOperation.getIndRemanent());
			corporativeProcessResult.setTotalBalance(CorporateProcessUtils.calculateByFactor(retirementDetail.getAvailableBalance(), cashDelFactor, corpOperation.getIndRound()));
			totalBalance = corporativeProcessResult.getTotalBalance();
			corporativeProcessResult.setAvailableBalance(CorporateProcessUtils.calculateByFactor(retirementDetail.getAvailableBalance(), cashDelFactor, corpOperation.getIndRound()));
			//verificamos impuesto
			if(BooleanType.YES.getCode().equals(corpOperation.getIndTax()) && 	Validations.validateIsNotNull(corpOperation.getTaxFactor()) 
					&& Validations.validateIsNotNullAndPositive(corpOperation.getTaxFactor())){				
					BigDecimal realFactor = BigDecimal.ONE.subtract(CorporateProcessUtils.divide(corpOperation.getTaxFactor(), CorporateProcessConstants.AMOUNT_ROUND));
					corporativeProcessResult.setTotalBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResult.getTotalBalance(), realFactor, corpOperation.getIndRound()));
					corporativeProcessResult.setAvailableBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResult.getAvailableBalance(), realFactor, corpOperation.getIndRound()));
					corporativeProcessResult.setTaxAmount(totalBalance.subtract(corporativeProcessResult.getTotalBalance()));
			}
			corporativeProcessResult.setTransitBalance(BigDecimal.ZERO);
			corporativeProcessResult.setPawnBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBanBalance(BigDecimal.ZERO);
			corporativeProcessResult.setOtherBlockBalance(BigDecimal.ZERO);
			corporativeProcessResult.setAcreditationBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReserveBalance(BigDecimal.ZERO);
			corporativeProcessResult.setOppositionBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBuyBalance(BigDecimal.ZERO);
			corporativeProcessResult.setSellBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReportingBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReportedBalance(BigDecimal.ZERO);
			corporativeProcessResult.setMarginBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBorrowerBalance(BigDecimal.ZERO);
			corporativeProcessResult.setLenderBalance(BigDecimal.ZERO);
			corporativeProcessResult.setLoanableBalance(BigDecimal.ZERO);	
			corporativeProcessResult.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			if(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					corpOperation.getCurrency().equals(CurrencyType.DMV.getCode())){
				corporativeProcessResult.setIndOriginTarget(CorporateProcessConstants.EXCHANGE_DESTINY);
				corporativeProcessResult.setExchangeAmount(corporativeProcessResult.getTotalBalance());
			}else{
				corporativeProcessResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_DESTINY);
				corporativeProcessResult.setExchangeAmount(BigDecimal.ZERO);
			}			
			corporativeProcessResult.setCorporativeOperation(corpOperation);
			corporativeProcessResult.setCustodyAmount(BigDecimal.ZERO);
			corporateExecutorService.create(corporativeProcessResult, loggerUser);
			//registro tipo cambio
			if(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					corpOperation.getCurrency().equals(CurrencyType.DMV.getCode())){
				DailyExchangeRates exchangeRateDay = parameterServiceBean.getDayExchangeRate(corpOperation.getExchangeDateRate(),corpOperation.getCurrency());
				CorporativeProcessResult corporativeProcessResultExchange = new CorporativeProcessResult();
				corporativeProcessResult.setIdCorporativeProcessResult(null);
				corporativeProcessResultExchange = corporativeProcessResult;
				corporativeProcessResultExchange.setTotalBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getTotalBalance(), exchangeRateDay.getBuyPrice(), corpOperation.getIndRound()));
				corporativeProcessResultExchange.setAvailableBalance(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getAvailableBalance(), exchangeRateDay.getBuyPrice(), corpOperation.getIndRound()));
				corporativeProcessResultExchange.setTaxAmount(CorporateProcessUtils.calculateByFactor(corporativeProcessResultExchange.getTaxAmount(), exchangeRateDay.getBuyPrice(), corpOperation.getIndRound()));
				corporativeProcessResultExchange.setIndOriginTarget(CorporateProcessConstants.BALANCE_DESTINY);
				corporateExecutorService.create(corporativeProcessResultExchange, loggerUser);
			}
			
		}	
				
		corpOperation.setPaymentAmount(corporateExecutorService.getTotalBalance(corpOperation.getIdCorporativeOperationPk()));
		corpOperation.setIssuerConfirmedAmount(corpOperation.getPaymentAmount());
		corpOperation.setCustodyAmount(BigDecimal.ZERO);
		corpOperation.setCommissionAmount(BigDecimal.ZERO);
		corpOperation.setTaxAmount(corporateExecutorService.getTaxAmount(corpOperation.getIdCorporativeOperationPk()));
		corpOperation.setExchangeAmount(corporateExecutorService.getExchangeAmount(corpOperation.getIdCorporativeOperationPk()));
		corporateExecutorService.updateCorporativeCustodyAmounts(corpOperation);
	}
	
	/**
	 * Metodo que ejecuta proceso definitivo de beneficios en efectivo.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executeDefinitiveProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started definitive process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
			corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), 
					null, true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null, loggerUser);
		//actualiza estado del cupon
		List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramInterestCouponActual(corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getDeliveryDate());
		if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
			corporateExecutorService.updateProgramInterestCouponState(lstIdProgramInterestCoupon, ProgramScheduleStateType.EXPIRED.getCode(), loggerUser);
		}	
		return true;
	}

	/**
	 * Metodo que calcula comisiones de beneficios en efectivo.
	 *
	 * @param corporativeOperation the corporative operation
	 * @param calculateTax the calculate tax
	 * @return the map
	 */
	private Map<Long,BigDecimal> executeCommissionCalculationProcess(Long corporativeOperation,boolean calculateTax){

		BigDecimal previousCommissionAmount = BigDecimal.ZERO;
		Map<Long,BigDecimal> mpHolderAccountCustody = new HashMap<Long, BigDecimal>();
		Long holderAccount, previousHolderAccount = null;

		List<CustodyCommissionDetail> lstHolderAccountCommission = custodyCommissionService.getCustodyCommissionDetail(corporativeOperation);
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountCommission)){
			for(CustodyCommissionDetail custodyCommissionDetail : lstHolderAccountCommission){
				holderAccount = custodyCommissionDetail.getHolderAccount().getIdHolderAccountPk();
				BigDecimal commissionAmount = custodyCommissionDetail.getCustodyAmount();

				if(Validations.validateIsNull(previousHolderAccount)){
					previousCommissionAmount = commissionAmount;
				}else{
					if(holderAccount.equals(previousHolderAccount)){
						previousCommissionAmount = previousCommissionAmount.add(commissionAmount);
					}else{

						mpHolderAccountCustody.put(previousHolderAccount,previousCommissionAmount);
						previousCommissionAmount = commissionAmount;
					}
				}
				previousHolderAccount = holderAccount;
			}
			mpHolderAccountCustody.put(previousHolderAccount,previousCommissionAmount);
		}
		return mpHolderAccountCustody;
	}

	/**
	 * Metodo que reprocesa preliminar de beneficios en efectivo.
	 *
	 * @param corporativeOperation the corporative operation
	 * @param loggerUser the logger user
	 * @param mpHolderAccountCustody the mp holder account custody
	 * @param mpHolderAccountTax the mp holder account tax
	 * @param blTax the bl tax
	 * @throws ServiceException the service exception
	 */
	private void reprocessCorporativeResults(CorporativeOperation corporativeOperation, LoggerUser loggerUser, Map<Long,BigDecimal> mpHolderAccountCustody,
			Map<Long,BigDecimal> mpHolderAccountTax, boolean blTax) throws ServiceException{

		List<CorporativeProcessResult> lstCorporateResults = corporateExecutorService.getCorporativeResults(corporativeOperation.getIdCorporativeOperationPk());
		List<HolderAccountAdjusment> adjustmentList = corporateExecutorService.getCorporativeAdjustments(corporativeOperation.getIdCorporativeOperationPk());

		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporateResults)){
			for(CorporativeProcessResult corporativeResult : lstCorporateResults){
				Long holderAccountPK = corporativeResult.getHolderAccount().getIdHolderAccountPk();

				//WE SET CUSTODY AMOUNT CALCULATED PREVIOUSLY TO UPDATE BALANCES
				if(mpHolderAccountCustody.containsKey(holderAccountPK)){
					corporativeResult.setCustodyAmount(mpHolderAccountCustody.get(holderAccountPK));
				}

				if(mpHolderAccountTax.containsKey(holderAccountPK) && blTax){
					BigDecimal taxFactor = mpHolderAccountTax.get(holderAccountPK);
					BigDecimal taxAmount = CorporateProcessUtils.multiply10Decimals(taxFactor,corporativeResult.getTotalBalance());
					taxAmount =  corporativeResult.getTotalBalance().subtract(taxAmount);
					corporativeResult.setTaxAmount(taxAmount);
				}

				//WE VERIFY IF THE HOLDER ACCOUNT HAS AN ADJUSTMENT TO APPLY IT BEFORE WE WILL CALCULATE ITS DETAIL
				if(Validations.validateListIsNotNullAndNotEmpty(adjustmentList)){
					List<HolderAccountAdjusment> lstAdjustmentHolderAccount = new ArrayList<HolderAccountAdjusment>();
					boolean executeAdjustment = false;
					for(HolderAccountAdjusment adjustment : adjustmentList){
						if(adjustment.getHolderAccount().getIdHolderAccountPk().equals(holderAccountPK)){
							lstAdjustmentHolderAccount.add(adjustment);
							executeAdjustment = true;
						}
					}
					if(executeAdjustment){
						corporativeResult = processAdjustments(corporativeResult,lstAdjustmentHolderAccount);
					}
				}

				BigDecimal taxAmount = corporativeResult.getTaxAmount();
				if(Validations.validateIsNull(taxAmount) || !blTax){
					taxAmount = BigDecimal.ZERO;
				}
				BigDecimal custodyAmount = corporativeResult.getCustodyAmount();
				if(Validations.validateIsNull(custodyAmount)){
					custodyAmount = BigDecimal.ZERO;
				}

				BigDecimal grossTotalAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getTotalBalance())){
					grossTotalAmount = corporativeResult.getTotalBalance();
				}
				
				BigDecimal grossMarginAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getMarginBalance())){
					grossMarginAmount = corporativeResult.getMarginBalance();
				}
				
				BigDecimal grossPawnAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getPawnBalance())){
					grossPawnAmount = corporativeResult.getPawnBalance();
				}
				
				BigDecimal grossBanAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getBanBalance())){
					grossBanAmount = corporativeResult.getBanBalance();
				}
				
				BigDecimal grossOtherBlockAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getOtherBlockBalance())){
					grossOtherBlockAmount = corporativeResult.getOtherBlockBalance();
				}
				
				BigDecimal grossReserveAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getReserveBalance())){
					grossReserveAmount = corporativeResult.getReserveBalance();
				}
				
				BigDecimal grossOppositionAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getOppositionBalance())){
					grossOppositionAmount = corporativeResult.getOppositionBalance();
				}
				
				BigDecimal grossReportingAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getReportingBalance())){
					grossReportingAmount = corporativeResult.getReportingBalance();
				}
				
				BigDecimal grossReportedAmount = BigDecimal.ZERO;
				if(Validations.validateIsNotNull(corporativeResult.getReportedBalance())){
					grossReportedAmount = corporativeResult.getTotalBalance();
				}

				//1	CREATE THE CORPORATIVE RESULT DETAIL
				if(grossTotalAmount.compareTo(BigDecimal.ZERO) == 0){
					continue;
				}
				BigDecimal marginFactor = CorporateProcessUtils.divide(grossMarginAmount, grossTotalAmount, 2);
				BigDecimal pawnFactor = CorporateProcessUtils.divide(grossPawnAmount, grossTotalAmount, 2);
				BigDecimal banFactor = CorporateProcessUtils.divide(grossBanAmount, grossTotalAmount, 2);
				BigDecimal otherBlockFactor = CorporateProcessUtils.divide(grossOtherBlockAmount, grossTotalAmount, 2);
				BigDecimal reserveFactor = CorporateProcessUtils.divide(grossReserveAmount, grossTotalAmount, 2);
				BigDecimal oppositionFactor = CorporateProcessUtils.divide(grossOppositionAmount, grossTotalAmount, 2);
				BigDecimal reportingFactor = CorporateProcessUtils.divide(grossReportingAmount, grossTotalAmount, 2);
				BigDecimal reportedFactor = CorporateProcessUtils.divide(grossReportedAmount, grossTotalAmount, 2);

				//CALCULATE THE TAX AMOUNTS BY EACH BALANCE
				BigDecimal marginTax = CorporateProcessUtils.multiply(taxAmount, marginFactor, 2);
				BigDecimal pawnTax = CorporateProcessUtils.multiply(taxAmount, pawnFactor, 2);
				BigDecimal banTax = CorporateProcessUtils.multiply(taxAmount, banFactor, 2);
				BigDecimal otherBlockTax = CorporateProcessUtils.multiply(taxAmount, otherBlockFactor, 2);
				BigDecimal reserveTax = CorporateProcessUtils.multiply(taxAmount, reserveFactor, 2);
				BigDecimal oppositionTax = CorporateProcessUtils.multiply(taxAmount, oppositionFactor, 2);
				BigDecimal reportingTax = CorporateProcessUtils.multiply(taxAmount, reportingFactor, 2);
				BigDecimal reportedTax = CorporateProcessUtils.multiply(taxAmount, reportedFactor, 2);
				BigDecimal availableTax = taxAmount.subtract(
						(marginTax.add(pawnTax.add(banTax.add(otherBlockTax.add(reserveTax.add(oppositionTax.add(reportingTax.add(reportedTax)))))))));

				//CALCULATE THE CUSTODY AMOUNTS BY EACH BALANCE
				BigDecimal marginCustody = CorporateProcessUtils.multiply(custodyAmount, marginFactor, 2);
				BigDecimal pawnCustody = CorporateProcessUtils.multiply(custodyAmount, pawnFactor, 2);
				BigDecimal banCustody = CorporateProcessUtils.multiply(custodyAmount, banFactor, 2);
				BigDecimal otherBlockCustody = CorporateProcessUtils.multiply(custodyAmount, otherBlockFactor, 2);
				BigDecimal reserveCustody = CorporateProcessUtils.multiply(custodyAmount, reserveFactor, 2);
				BigDecimal oppositionCustody = CorporateProcessUtils.multiply(custodyAmount, oppositionFactor, 2);
				BigDecimal reportingCustody = CorporateProcessUtils.multiply(custodyAmount, reportingFactor, 2);
				BigDecimal reportedCustody = CorporateProcessUtils.multiply(custodyAmount, reportedFactor, 2);
				BigDecimal availableCustody = custodyAmount.subtract(
						(marginCustody.add(pawnCustody.add(banCustody.add(otherBlockCustody.add(reserveCustody.add(oppositionCustody.add(reportingCustody.add
								(reportedCustody)))))))));

				//CREATE THE DETAIL AMOUNT BY BALANCE
				CorporativeResultDetail corporativeDetail = new CorporativeResultDetail();
				corporativeDetail.setAvailableCustodyAmount(availableCustody);
				corporativeDetail.setAvailableTaxAmount(availableTax);
				corporativeDetail.setBanCustodyAmount(banCustody);
				corporativeDetail.setBanTaxAmount(banTax);
				corporativeDetail.setMarginCustodyAmount(marginCustody);
				corporativeDetail.setMarginTaxAmount(marginTax);
				corporativeDetail.setOppositionCustodyAmount(oppositionCustody);
				corporativeDetail.setOppositionTaxAmount(oppositionTax);
				corporativeDetail.setOtherCustodyAmount(otherBlockCustody);
				corporativeDetail.setOtherTaxAmount(otherBlockTax);
				corporativeDetail.setPawnCustodyAmount(pawnCustody);
				corporativeDetail.setPawnTaxAmount(pawnTax);
				corporativeDetail.setReportedCustodyAmount(reportedCustody);
				corporativeDetail.setReportedTaxAmount(reportedTax);
				corporativeDetail.setReportingCustodyAmount(reportingCustody);
				corporativeDetail.setReportingTaxAmount(reportingTax);
				corporativeDetail.setReserveCustodyAmount(reserveCustody);
				corporativeDetail.setReserveTaxAmount(reserveTax);
				corporativeDetail.setCorporativeProcessResult(corporativeResult);
				corporativeDetail.setCorporativeOperation(corporativeOperation);
				corporateExecutorService.create(corporativeDetail,loggerUser);

				corporateExecutorService.update(corporativeResult);
				
				corporateExecutorService.updateBlockCorporateForResult(corporativeOperation.getIdCorporativeOperationPk(), loggerUser);
			}
		}
	}

	/**
	 * Metodo que prosea ajuste de preliminar de beneficios en efectivo.
	 *
	 * @param corporativeResult the corporative result
	 * @param lstAdjustment the lst adjustment
	 * @return the corporative process result
	 */
	private CorporativeProcessResult processAdjustments(CorporativeProcessResult corporativeResult, List<HolderAccountAdjusment> lstAdjustment){

		//WE UPDATE THE TOTAL - AVAILABLE AMOUNTS BY THE DIFFERENCE MODIFIES IN THE ADJUSTMENT
		BigDecimal pawnBalance = BigDecimal.ZERO;
		BigDecimal banBalance = BigDecimal.ZERO;
		BigDecimal otherBlockBalance = BigDecimal.ZERO;
		BigDecimal oppositionBalance = BigDecimal.ZERO;
		BigDecimal reserveBalance = BigDecimal.ZERO;

		if(Validations.validateListIsNotNullAndNotEmpty(lstAdjustment)){
			for(HolderAccountAdjusment adjustment : lstAdjustment){
				if(adjustment.getIndDeliverBenefit().equals(BooleanType.YES.getCode())){
					BigDecimal previousAmount = adjustment.getOldBenefitAmount();
					if(Validations.validateIsNull(previousAmount)){
						previousAmount = BigDecimal.ZERO;
					}
					BigDecimal newAmount = adjustment.getNewBenefitAmount();
					if(Validations.validateIsNull(newAmount)){
						newAmount = BigDecimal.ZERO;
					}

					pawnBalance = corporativeResult.getPawnBalance();
					banBalance = corporativeResult.getBanBalance();
					otherBlockBalance = corporativeResult.getOtherBlockBalance();
					oppositionBalance = corporativeResult.getOppositionBalance();
					reserveBalance = corporativeResult.getReserveBalance();

					if(previousAmount.compareTo(newAmount) == 1){
						BigDecimal difference = previousAmount.subtract(newAmount);
						corporativeResult.setTotalBalance(corporativeResult.getTotalBalance().subtract(difference));
					}else if(newAmount.compareTo(previousAmount) == 1){
						BigDecimal difference = newAmount.subtract(previousAmount);
						corporativeResult.setTotalBalance(corporativeResult.getTotalBalance().add(difference));
					}
					if(corporativeResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY)){
						corporativeResult.setExchangeAmount(corporativeResult.getTotalBalance());
					}
					BigDecimal totalBalance = corporativeResult.getTotalBalance();
					BigDecimal blockBalance = reserveBalance.add(oppositionBalance.add(otherBlockBalance.add(banBalance.add(pawnBalance))));
					corporativeResult.setAvailableBalance(totalBalance.subtract(blockBalance));
				}
				//WE UPDATE THE TAX AMOUNT
				if(adjustment.getIndCollectTax().equals(BooleanType.YES.getCode())){
					corporativeResult.setTaxAmount(adjustment.getNewTax());
				}
				//WE UPDATE THE CUSTODY AMOUNT
				if(adjustment.getIndCollectCommissiones().equals(BooleanType.YES.getCode())){
					BigDecimal newCustodyAmount = adjustment.getNewCommissiones();
					corporativeResult.setCustodyAmount(newCustodyAmount);
				}
			}
		}

		return corporativeResult;
	}
	
	/**
	 * Metodo que ejecuta exlusuon de ajuste de beneficios en efectivo.
	 *
	 * @param corporativeID the corporative 
	 * @param lstParticipantExclussion the lst participant exclussion
	 * @param lstIssuerExclussion the lst issuer exclussion
	 * @param security the security
	 * @param loggerUser the logger user
	 * @param lstHolderAccounts the lst holder accounts
	 * @throws ServiceException the service exception
	 */
	private void executeExclussionAdjusment(Long corporativeID, List<Long> lstParticipantExclussion,List<String> lstIssuerExclussion, 
			String security, LoggerUser loggerUser, List<Long> lstHolderAccounts) throws ServiceException{
		//GENERATE THE HOLDER ACCOUNT ADJUSTMENTS BY PARTICIPANT EXCLUSSIONS
		List<Object[]> lstCorporateResult = corporateExecutorService.getDataExclussionAdjustments(corporativeID,lstParticipantExclussion,lstHolderAccounts);
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporateResult)){
			for(Object[] corporateResult : lstCorporateResult){
				Long holderAccountPK = Long.valueOf(corporateResult[0].toString());
				BigDecimal custodyAmount = new BigDecimal(corporateResult[2].toString());
				HolderAccountAdjusment holderAccountAdjusment = new HolderAccountAdjusment();
				holderAccountAdjusment.setIndCollectTax(BooleanType.NO.getCode());
				holderAccountAdjusment.setIndDeliverBenefit(BooleanType.NO.getCode());
				holderAccountAdjusment.setCorporativeOperation(new CorporativeOperation());
				holderAccountAdjusment.getCorporativeOperation().setIdCorporativeOperationPk(corporativeID);
				holderAccountAdjusment.setHolderAccount(new HolderAccount());
				holderAccountAdjusment.getHolderAccount().setIdHolderAccountPk(holderAccountPK);
				holderAccountAdjusment.setIndCollectCommissiones(BooleanType.YES.getCode());
				holderAccountAdjusment.setNewCommissiones(BigDecimal.ZERO);
				holderAccountAdjusment.setOldCommisiones(custodyAmount);
				holderAccountAdjusment.setSecurity(new Security());
				holderAccountAdjusment.getSecurity().setIdSecurityCodePk(security);
				corporateExecutorService.create(holderAccountAdjusment,loggerUser);
			}
		}

		//GENERATE THE HOLDER ACCOUNT ADJUSTMENTS BY ISSUER EXCLUSSIONS
		if(Validations.validateListIsNotNullAndNotEmpty(lstIssuerExclussion)){
			boolean existCorporativeIssuer = false;
			String corporativeIssuer = corporateExecutorService.getIssuerByCorporative(corporativeID);
			for(String excludedIssuer : lstIssuerExclussion){
				if(corporativeIssuer.equals(excludedIssuer)){
					existCorporativeIssuer = true;
					break;
				}
			}

			//IF CORPORATIVE ISSUER IS EXCLUDED OF COMMISSION COLLECTION, WE MUST SET IT AND PAID LIKE AVAILABLE GENERATING PAYMENT LIKE AVAILABLE
			if(existCorporativeIssuer){
				List<Object[]> corporateResultList = corporateExecutorService.getHolderAccountByCorporative(corporativeID);
				if(Validations.validateListIsNotNullAndNotEmpty(corporateResultList)){
					for(Object[] corporateResult : corporateResultList){
						Long holderAccountPK = Long.valueOf(corporateResult[0].toString());
						BigDecimal custodyAmount = new BigDecimal(corporateResult[1].toString());
						HolderAccountAdjusment holderAccountAdjusment = new HolderAccountAdjusment();
						holderAccountAdjusment.setIndCollectTax(BooleanType.NO.getCode());
						holderAccountAdjusment.setIndDeliverBenefit(BooleanType.NO.getCode());
						holderAccountAdjusment.setCorporativeOperation(new CorporativeOperation());
						holderAccountAdjusment.getCorporativeOperation().setIdCorporativeOperationPk(corporativeID);
						holderAccountAdjusment.setHolderAccount(new HolderAccount());
						holderAccountAdjusment.getHolderAccount().setIdHolderAccountPk(holderAccountPK);
						holderAccountAdjusment.setIndCollectCommissiones(BooleanType.YES.getCode());
						holderAccountAdjusment.setNewCommissiones(BigDecimal.ZERO);
						holderAccountAdjusment.setOldCommisiones(custodyAmount);
						holderAccountAdjusment.setSecurity(new Security());
						holderAccountAdjusment.getSecurity().setIdSecurityCodePk(security);
						corporateExecutorService.create(holderAccountAdjusment,loggerUser);
					}
				}
			}
		}
	}

	/**
	 * Metodo que reprocesa saldos preliminares de beneficios en efectivo.
	 *
	 * @param holderAccounts the holder accounts
	 * @param objCorporativeOperation the obj corporative operation
	 * @param delFactor the del factor
	 * @param round the round
	 * @param cashFlag the cash flag
	 * @param remnant the remnant
	 * @param destiny the destiny
	 * @param processType the process type
	 * @param exchangeRateDay the exchange rate day
	 * @return the list
	 */
	private List<CorporativeProcessResult> reprocessBalances(List<Object[]> holderAccounts,CorporativeOperation objCorporativeOperation, 
			BigDecimal delFactor,int round, boolean cashFlag, boolean remnant, boolean destiny, ImportanceEventType processType,DailyExchangeRates exchangeRateDay){

		List<CorporativeProcessResult> processResults = new ArrayList<CorporativeProcessResult>();
		BigDecimal tmpDelFactor = null;
		for (Iterator<Object[]> it = holderAccounts.iterator(); it.hasNext();) {
			
			Object[] data = it.next();
			tmpDelFactor = delFactor;
			CorporativeProcessResult processResult = new CorporativeProcessResult();
			
			Long holderAccountPK = (Long) data[0];
			HolderAccount holAccount = new HolderAccount();
			holAccount.setIdHolderAccountPk(holderAccountPK);
			processResult.setHolderAccount(holAccount);
			processResult.setSecurity(objCorporativeOperation.getSecurities());
			Long participantPK = (Long)data[11];
			processResult.setParticipant(new Participant(participantPK));

			BigDecimal availableBalance = getBalance((BigDecimal)data[1], cashFlag);//(BigDecimal)data[1]; //getBalance((BigDecimal)data[1], cashFlag);
			BigDecimal pawnBalance = getBalance((BigDecimal)data[2], cashFlag);//(BigDecimal)data[2]; //getBalance((BigDecimal)data[2], cashFlag);
			BigDecimal banBalance = getBalance((BigDecimal)data[3], cashFlag);//(BigDecimal)data[3]; //getBalance((BigDecimal)data[3], cashFlag);
			BigDecimal otherBlockBalance = getBalance((BigDecimal)data[4], cashFlag);;//(BigDecimal)data[4]; //getBalance((BigDecimal)data[4], cashFlag);
			BigDecimal reserveBalance = getBalance((BigDecimal)data[5], cashFlag);//(BigDecimal)data[5]; //getBalance((BigDecimal)data[5], cashFlag);
			BigDecimal oppositionBalance = getBalance((BigDecimal)data[6], cashFlag);//(BigDecimal)data[6]; //getBalance((BigDecimal)data[6], cashFlag);
			BigDecimal marginBalance = getBalance((BigDecimal)data[7], cashFlag);//(BigDecimal)data[7]; //getBalance((BigDecimal)data[7], cashFlag);
			BigDecimal reportingBalance = getBalance((BigDecimal)data[8], cashFlag);//(BigDecimal)data[8]; //getBalance((BigDecimal)data[8], cashFlag);
			BigDecimal reportedBalance = getBalance((BigDecimal)data[9], cashFlag);//(BigDecimal)data[9]; //getBalance((BigDecimal)data[9], cashFlag);
		
			//calculateByFactor10Decimals
			processResult.setAvailableBalance(calculateByFactor10Decimals(availableBalance, tmpDelFactor,  round));	//calculateByFactorDecimals
			processResult.setPawnBalance(calculateByFactor10Decimals(pawnBalance, tmpDelFactor,  round));
			processResult.setBanBalance(calculateByFactor10Decimals(banBalance, tmpDelFactor,  round));
			processResult.setOtherBlockBalance(calculateByFactor10Decimals(otherBlockBalance, tmpDelFactor,  round));
			processResult.setReserveBalance(calculateByFactor10Decimals(reserveBalance, tmpDelFactor,  round));
			processResult.setOppositionBalance(calculateByFactor10Decimals(oppositionBalance, tmpDelFactor,  round));
			processResult.setMarginBalance(calculateByFactor10Decimals(marginBalance, tmpDelFactor,  round));
			processResult.setReportingBalance(calculateByFactor10Decimals(reportingBalance, tmpDelFactor,  round));
			processResult.setReportedBalance(calculateByFactor10Decimals(reportedBalance, tmpDelFactor,  round));
			processResult.setAcreditationBalance(BigDecimal.ZERO);
			processResult.setBorrowerBalance(BigDecimal.ZERO);
			processResult.setLenderBalance(BigDecimal.ZERO);
			processResult.setLoanableBalance(BigDecimal.ZERO);
			processResult.setSellBalance(BigDecimal.ZERO);
			processResult.setTransitBalance(BigDecimal.ZERO);
			processResult.setBuyBalance(BigDecimal.ZERO);
			processResult.setIndAdjustment(CorporateProcessConstants.IND_NO);
			processResult.setCorporativeOperation(objCorporativeOperation);
			processResult.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			BigDecimal totalAmount = processResult.getAvailableBalance().add(processResult.getBanBalance()).add(
					processResult.getOtherBlockBalance()).add(processResult.getPawnBalance()).add(
							processResult.getReportingBalance()).add(processResult.getMarginBalance()).add(
							processResult.getReserveBalance()).add(processResult.getOppositionBalance());
			processResult.setTotalBalance(totalAmount);
			
			if(remnant){
				processResult.setIndRemanent(CorporateProcessConstants.IND_REMAINDER);
			}else{
				processResult.setIndRemanent(CorporateProcessConstants.IND_NO_REMAINDER);
			}
			
			if(objCorporativeOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					objCorporativeOperation.getCurrency().equals(CurrencyType.DMV.getCode())){
				processResult.setIndOriginTarget(CorporateProcessConstants.EXCHANGE_DESTINY);
				processResult.setExchangeAmount(processResult.getTotalBalance());
			}else if(destiny){
				processResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_DESTINY);
				processResult.setExchangeAmount(BigDecimal.ZERO);
			}else{
				processResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_ORIGIN);
				processResult.setExchangeAmount(BigDecimal.ZERO);
			}

			processResults.add(processResult);
			
			//bolivia
			/*
			if(objCorporativeOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					objCorporativeOperation.getCurrency().equals(CurrencyType.DMV.getCode())){
				CorporativeProcessResult corporativeProcessResultExchange = new CorporativeProcessResult(processResult);
				corporativeProcessResultExchange.setTotalBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getTotalBalance(), exchangeRateDay.getBuyPrice(), round));
				corporativeProcessResultExchange.setAvailableBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getAvailableBalance(), exchangeRateDay.getBuyPrice(), round));
				corporativeProcessResultExchange.setPawnBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getPawnBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setBanBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getBanBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setOtherBlockBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getOtherBlockBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setReserveBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getReserveBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setOppositionBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getOppositionBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setMarginBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getMarginBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setReportingBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getReportingBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setReportedBalance(calculateByFactor10Decimals(corporativeProcessResultExchange.getReportedBalance(),  exchangeRateDay.getBuyPrice(),  round));
				corporativeProcessResultExchange.setIndOriginTarget(CorporateProcessConstants.BALANCE_DESTINY);
				corporativeProcessResultExchange.setExchangeAmount(BigDecimal.ZERO);
				processResults.add(corporativeProcessResultExchange);
			}*/
		}

		return processResults;
	}

	/**
	 * Gets the balance.
	 *
	 * @param balance the balance
	 * @param cashFlag the cash flag
	 * @return the balance
	 */
	private BigDecimal getBalance(BigDecimal balance, boolean cashFlag){		
		return balance != null ? cashFlag ? CorporateProcessUtils.multiply(
				balance, CorporateProcessConstants.AMOUNT_ROUND): balance : BigDecimal.ZERO;
	}

	/**
	 * Metodo que retorna proceso preliminar.
	 *
	 * @param holderAccountPK the holder account pk
	 * @param participantPK the participant pk
	 * @param security the security
	 * @param balance the balance
	 * @param corporativeOperation the corporative operation
	 * @param deliveryFactor the delivery factor
	 * @param taxRates the tax rates
	 * @param calculateTax the calculate tax
	 * @return the corporative process result
	 * @throws ServiceException the service exception
	 */
	private CorporativeProcessResult fillCorporativeProcessResult(Long holderAccountPK, Long participantPK, String security, BigDecimal balance, 
			CorporativeOperation corporativeOperation, BigDecimal deliveryFactor, BigDecimal taxRates, boolean calculateTax) throws ServiceException{

		CorporativeProcessResult corporativeProcessResult = new CorporativeProcessResult();

		HolderAccount holAccount = new HolderAccount();
		holAccount.setIdHolderAccountPk(holderAccountPK);
		corporativeProcessResult.setHolderAccount(holAccount);

		List<Object[]> lstTaxes = corporateExecutorService.getTaxProcess(holderAccountPK);
		Object[] arrObj = null;
		BigDecimal taxFactor = BigDecimal.ZERO;
		if(Validations.validateListIsNotNullAndNotEmpty(lstTaxes)){
			arrObj = lstTaxes.get(0);
		}
		String residence = StringUtils.EMPTY;
		if(Validations.validateIsNotNull(arrObj)){
			if(arrObj[1].toString().equals(countryResidence.toString())){
				residence = NationalityType.NATIONAL.getCode().toString();
			}else{
				residence = NationalityType.FOREIGN.getCode().toString();
			}
			taxFactor = taxRates;
		}

		corporativeProcessResult.setSecurity(new Security());
		corporativeProcessResult.getSecurity().setIdSecurityCodePk(security);

		corporativeProcessResult.setParticipant(new Participant());
		corporativeProcessResult.getParticipant().setIdParticipantPk(participantPK);

		BigDecimal totalAmount = CorporateProcessUtils.calculateByFactor(balance, deliveryFactor,  corporativeOperation.getIndRound());
		corporativeProcessResult.setTotalBalance(totalAmount);
		corporativeProcessResult.setAvailableBalance(totalAmount);

		if(calculateTax){
			BigDecimal taxAmount = calculateByFactor10Decimals(totalAmount,taxFactor,1);
			corporativeProcessResult.setTaxAmount(taxAmount);
		}else{
			corporativeProcessResult.setTaxAmount(BigDecimal.ZERO);
		}

		corporativeProcessResult.setBanBalance(BigDecimal.ZERO);
		corporativeProcessResult.setOtherBlockBalance(BigDecimal.ZERO);
		corporativeProcessResult.setReserveBalance(BigDecimal.ZERO);
		corporativeProcessResult.setOppositionBalance(BigDecimal.ZERO);
		corporativeProcessResult.setMarginBalance(BigDecimal.ZERO);
		corporativeProcessResult.setReportingBalance(BigDecimal.ZERO);
		corporativeProcessResult.setReportedBalance(BigDecimal.ZERO);
		corporativeProcessResult.setAcreditationBalance(BigDecimal.ZERO);
		corporativeProcessResult.setBorrowerBalance(BigDecimal.ZERO);
		corporativeProcessResult.setLenderBalance(BigDecimal.ZERO);
		corporativeProcessResult.setLoanableBalance(BigDecimal.ZERO);
		corporativeProcessResult.setSellBalance(BigDecimal.ZERO);
		corporativeProcessResult.setTransitBalance(BigDecimal.ZERO);
		corporativeProcessResult.setBuyBalance(BigDecimal.ZERO);
		corporativeProcessResult.setIndAdjustment(CorporateProcessConstants.IND_NO);
		corporativeProcessResult.setCorporativeOperation(corporativeOperation);
		corporativeProcessResult.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		corporativeProcessResult.setIndRemanent(CorporateProcessConstants.IND_NO_REMAINDER);
		corporativeProcessResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_DESTINY);

		return corporativeProcessResult;
	}

	/**
	 * Save grant coupons.
	 *
	 * @param mpGrantCoupon the mp grant coupon
	 * @param loggerUser the logger user
	 * @param securityCode the security code
	 * @param corporativeOperation the corporative operation
	 * @param deliveryFactor the delivery factor
	 * @param taxRates the tax rates
	 * @param calculateTax the calculate tax
	 * @throws ServiceException the service exception
	 */
	private void saveGrantCoupons(Map<String,Map<Long,BigDecimal>> mpGrantCoupon, LoggerUser loggerUser, String securityCode, 
			CorporativeOperation corporativeOperation, BigDecimal deliveryFactor,BigDecimal taxRates,boolean calculateTax) throws ServiceException{
		CorporativeProcessResult newCorporativeProcessResult = new CorporativeProcessResult();
		//WE ADD PROCESS CORPORATE RESULT FOR HOLDER ACCOUNTS THAT DOESNT EXIST IN CORPORATE RESULTS
		for(Map.Entry<String, Map<Long,BigDecimal>> entry : mpGrantCoupon.entrySet()){
			String mapKey = entry.getKey();

			BigDecimal holderAccountBalance = BigDecimal.ZERO;
			Integer posititon = mapKey.indexOf("-");
			Long holderAccount = Long.valueOf(mapKey.substring(0,posititon));
			Long participant = Long.valueOf(mapKey.substring(posititon+1,mapKey.length()));

			Map<Long,BigDecimal> mpBalance = entry.getValue();
			for(Map.Entry<Long, BigDecimal> entry2 : mpBalance.entrySet()){
				Long balanceType = entry2.getKey();
				BigDecimal balance = getBalance(entry2.getValue(),true);

				if(ParameterBalanceType.GRANTED_BALANCE.getCode().equals(balanceType)){
					holderAccountBalance = holderAccountBalance.subtract(balance);
				}else if(ParameterBalanceType.GRANT_BALANCE.getCode().equals(balanceType)){
					holderAccountBalance = holderAccountBalance.add(balance);
				}
			}

			if(holderAccountBalance.compareTo(BigDecimal.ZERO) == 1){

				newCorporativeProcessResult = new CorporativeProcessResult();
				newCorporativeProcessResult = fillCorporativeProcessResult(holderAccount,participant,securityCode,
																			holderAccountBalance,corporativeOperation,deliveryFactor,taxRates,calculateTax);

				CorporateProcessUtils.divideForCash(newCorporativeProcessResult);

				corporateExecutorService.create(newCorporativeProcessResult,loggerUser);
			}
		}
	}

	/**
	 * Process grants coupon.
	 *
	 * @param mpGrantCoupon the mp grant coupon
	 * @param mpAlreadyGrantedCoupon the mp already granted coupon
	 * @return the map
	 */
	private Map<String,Map<Long,BigDecimal>> processGrantsCoupon(Map<String,Map<Long,BigDecimal>> mpGrantCoupon,Map<String,Map<Long,BigDecimal>> mpAlreadyGrantedCoupon){

		Map<String,Map<Long,BigDecimal>> mpSaveCoupon = new HashMap<String, Map<Long,BigDecimal>>();
		if(mpGrantCoupon.size() > 0){
			for(Map.Entry<String, Map<Long,BigDecimal>> entry : mpGrantCoupon.entrySet()){
				String key = entry.getKey();
				if(!mpAlreadyGrantedCoupon.containsKey(key)){
					mpSaveCoupon.put(key,entry.getValue());
				}
			}
		}
		return mpSaveCoupon;
	}

	/**
	 * Find billing service.
	 *
	 * @param parameters the parameters
	 * @return the map
	 */
	private Map<String,Object> findBillingService(Map<String,Object> parameters){
		Date calculationDate = null;
		BillingServiceTo billingServiceTo = new BillingServiceTo();
		String baseCollectionId	=	(String) parameters.get(GeneralConstants.BASE_COLLECTION);		
		billingServiceTo.setBaseCollection(Integer.parseInt(baseCollectionId));
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.CALCULATION_DATE))){
			calculationDate	= CommonsUtilities.convertStringtoDate(parameters.get(GeneralConstants.CALCULATION_DATE).toString());
		}
		ServiceRateTo serviceRateTo = new ServiceRateTo();
		serviceRateTo.setLastEffectiveDate(calculationDate);
		billingServiceTo.setServiceRateToSelected(serviceRateTo);
		List<BillingService> billingServiceList= corporateExecutorService.findBillingService(billingServiceTo);
		Map<String,Object> parameter	= new HashMap<String,Object>();
		parameter.put(GeneralConstants.BILLING_SERVICE_LIST, billingServiceList);				
		return parameter;

	}

	/**
	 * Calculate by factor10 decimals.
	 *
	 * @param amount the amount
	 * @param factor the factor
	 * @param round the round
	 * @return the big decimal
	 */
	public static BigDecimal calculateByFactor10Decimals(BigDecimal amount, BigDecimal factor,
			int round) {
		if(amount != null && factor != null){
			BigDecimal result = multiply10Decimals(amount, factor);
			if(round == CorporateProcessConstants.IND_ROUND){
				result = CorporateProcessUtils.round(result);
			}else if(round == CorporateProcessConstants.IND_FLOOR){
				result = CorporateProcessUtils.floor(result);
			}
			return result;
		}
		return null;
	}

	public static BigDecimal calculateByFactorDecimals(BigDecimal amount, BigDecimal factor,
			int round) {
		if(amount != null && factor != null){
			BigDecimal result = amount.multiply(factor);
			if(round == CorporateProcessConstants.IND_ROUND){
				result = CorporateProcessUtils.round(result);
			}else if(round == CorporateProcessConstants.IND_FLOOR){
				//result = CorporateProcessUtils.floor(result);
			}
			return result;
		}
		return null;
	}

	/**
	 * Multiply10 decimals.
	 *
	 * @param balance the balance
	 * @param value the value
	 * @return the big decimal
	 */
	public static BigDecimal multiply10Decimals(BigDecimal balance, BigDecimal value) {
		return CorporateProcessUtils.multiply(balance, value, 10);
	}
}