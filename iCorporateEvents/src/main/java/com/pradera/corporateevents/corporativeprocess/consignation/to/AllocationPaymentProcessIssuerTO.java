package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;


// TODO: Auto-generated Javadoc
/**
 * The Class AllocationPaymentProcessIssuerTO.
 * @author Pradera Technologies
 *
 */
public class AllocationPaymentProcessIssuerTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The issuer. */
	private String issuer;
	
	/** The corporative event type. */
	private Integer corporativeEventType;
	
	/** The withdrawl amount. */
	private BigDecimal withdrawlAmount;

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the withdrawl amount.
	 *
	 * @return the withdrawl amount
	 */
	public BigDecimal getWithdrawlAmount() {
		return withdrawlAmount;
	}
	
	/**
	 * Sets the withdrawl amount.
	 *
	 * @param withdrawlAmount the new withdrawl amount
	 */
	public void setWithdrawlAmount(BigDecimal withdrawlAmount) {
		this.withdrawlAmount = withdrawlAmount;
	}
	
	/**
	 * Gets the corporative event type.
	 *
	 * @return the corporative event type
	 */
	public Integer getCorporativeEventType() {
		return corporativeEventType;
	}
	
	/**
	 * Sets the corporative event type.
	 *
	 * @param corporativeEventType the new corporative event type
	 */
	public void setCorporativeEventType(Integer corporativeEventType) {
		this.corporativeEventType = corporativeEventType;
	}
}