package com.pradera.corporateevents.corporativeprocess.process;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum CorporateExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/06/2014
 */
public enum CorporateExecutor {
	
	/** The amortization. */
	AMORTIZATION,
	
	/** The cash. */
	CASH,
	
	/** The rescue. */
	EARLY_PAYMENT,
	
	/** The special. */
	SPECIAL,
	
	/** The stock. */
	STOCK,
	
	/** The subscription. */
	SUBSCRIPTION,
	
	/** The split. */
	SPLIT
}
