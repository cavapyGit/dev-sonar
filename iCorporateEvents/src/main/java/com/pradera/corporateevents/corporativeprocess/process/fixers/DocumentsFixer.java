package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.math.BigDecimal;
import java.util.List;

import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.generalparameter.DailyExchangeRates;

// TODO: Auto-generated Javadoc
/**
 * The Interface DocumentsFixer.
 *
 * @author PraderaTechnologies
 */
public interface DocumentsFixer {

	/**
	 * Fix documents.
	 *
	 * @param corpProcessResult the corp process result
	 * @param blockedDocs the blocked docs
	 * @param delFactor the del factor
	 * @param round the round
	 * @param remnantFlag the remnant flag
	 * @param cashFlag the cash flag
	 * @param destiny the destiny
	 * @param balFixer the bal fixer
	 * @param processType the process type
	 * @param exchangeRateDay the exchange rate day
	 * @return the list
	 */
	public List<BlockCorporativeResult> fixDocuments(CorporativeProcessResult corpProcessResult, List<Object[]> blockedDocs,
			BigDecimal delFactor, int round, boolean remnantFlag, boolean cashFlag, boolean destiny, BalanceFixer balFixer, 
			ImportanceEventType processType,DailyExchangeRates exchangeRateDay);
	
	/**
	 * Fix documents.
	 *
	 * @param blockDocs the block docs
	 * @param blockedBalance the blocked balance
	 * @param totalBalance the total balance
	 * @param balFixer the bal fixer
	 * @return the list
	 */
	public List<BlockCorporativeResult> fixDocuments(List<BlockCorporativeResult> blockDocs,
			BigDecimal blockedBalance, BigDecimal totalBalance, BalanceFixer balFixer);
	
	/**
	 * Fill origin block corporative result.
	 *
	 * @param blockedDocs the blocked docs
	 * @param corpProcessResult the corp process result
	 * @param exchangeRateDay the exchange rate day
	 * @return the list
	 */
	public List<BlockCorporativeResult> fillOriginBlockCorporativeResult(List<Object[]> blockedDocs,	CorporativeProcessResult corpProcessResult,DailyExchangeRates exchangeRateDay);
	
	/**
	 * Update block documents effections.
	 *
	 * @param corpProcessResult the corp process result
	 * @param cashFlag the cash flag
	 * @return the corporative process result
	 */
	public CorporativeProcessResult updateBlockDocumentsEffections(CorporativeProcessResult corpProcessResult, boolean cashFlag);
	
	/**
	 * Prepare block operation.
	 *
	 * @param data the data
	 * @param loginUser the login user
	 * @param originalBalance the original balance
	 * @param actualBalance the actual balance
	 * @return the block request
	 */
	public BlockRequest prepareBlockOperation(Object[] data, String loginUser, BigDecimal originalBalance, BigDecimal actualBalance);
	
	/**
	 * Prepare unblock operation.
	 *
	 * @param data the data
	 * @param loginUser the login user
	 * @return the unblock request
	 */
	public UnblockRequest prepareUnblockOperation(Object[] data, String loginUser);
			
}
