package com.pradera.corporateevents.corporativeprocess.importanceevents.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.corporateevents.corporativeprocess.importanceevents.to.CorporativeOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.corporatives.CapitalIncreaseMotive;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.SecuritiesCorporative;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;


// TODO: Auto-generated Javadoc
/**
 * The Class ImportanceEventServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class ImportanceEventServiceBean extends CrudDaoServiceBean {




	/**
	 * Metodo que registra hecho de importancia
	 *
	 * @param operation the operation
	 * @param loggerUser the logger user
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	public CorporativeOperation registerCorporativeOperation(CorporativeOperation operation ,LoggerUser loggerUser)throws ServiceException{
		boolean exists = validateEventExistOnSourceSecurity(
    			operation.getSecurities().getIdSecurityCodePk());
    	if(exists){
    		throw new ServiceException(ErrorServiceType.SECURITY_ALREADY_ON_CORPORATIVE_OPERATION,
    				new  String[]{operation.getSecurities().getIdSecurityCodePk().toString()});
    	}
    	if(!operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())){
    		Map<String,Object> paramate = new HashMap<String,Object>();		
    		paramate.put("idSecurityCodePkParam", operation.getSecurities().getIdSecurityCodePk());
    		Security security = findObjectByNamedQuery(Security.SECURITY_STATE, paramate);
    		if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
    			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
    		}
    	}
    	
    	if((operation.getTargetSecurity()!=null && 
    			!operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
    					&&	Validations.validateIsNotNull(operation.getTargetSecurity().getIdSecurityCodePk()))){
    		boolean existsAux = validateEventExistOnSourceSecurity(
        			operation.getTargetSecurity().getIdSecurityCodePk());
        	if(existsAux){
        		throw new ServiceException(ErrorServiceType.SECURITY_ALREADY_ON_CORPORATIVE_OPERATION_DES,
        				new  String[]{operation.getSecurities().getIdSecurityCodePk().toString()});
        	}
        	Map<String,Object> paramate2 = new HashMap<String,Object>();		
        	paramate2.put("idSecurityCodePkParam", operation.getTargetSecurity().getIdSecurityCodePk());
    		Security security2 = findObjectByNamedQuery(Security.SECURITY_STATE, paramate2);
    		if(!security2.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
    			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
    		}
    	}else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
				&&	operation.getSecuritiesCorporatives()!=null && operation.getSecuritiesCorporatives().size()>0){
    		for(SecuritiesCorporative objSecuritiesCorporative : operation.getSecuritiesCorporatives()){
    			boolean existsAux = validateEventExistOnSourceSecurity(
    					objSecuritiesCorporative.getSecurity().getIdSecurityCodePk());
            	if(existsAux){
            		throw new ServiceException(ErrorServiceType.SECURITY_ALREADY_ON_CORPORATIVE_OPERATION_DES,
            				new  String[]{objSecuritiesCorporative.getSecurity().getIdSecurityCodePk().toString()});
            	}
            	Map<String,Object> paramate3 = new HashMap<String,Object>();		
            	paramate3.put("idSecurityCodePkParam", objSecuritiesCorporative.getSecurity().getIdSecurityCodePk());
        		Security security3 = findObjectByNamedQuery(Security.SECURITY_STATE, paramate3);
        		if(!security3.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
        			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
        		}
    		}
    	}
    	
    	
		setMotiveIncreaseCapitalList(operation);
		operation.setIndExcludedCommissions(BooleanType.NO.getCode());
		operation.setIndPayed(BooleanType.NO.getCode());
		if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())){
			operation.setIndRemanent(BooleanType.NO.getCode());
			operation.setIndPenality(BooleanType.NO.getCode());
			operation.setIndTax(BooleanType.NO.getCode());
			operation.setIndRound(BooleanType.NO.getCode());
			operation.setTargetSecurity(null);
		}
		
		operation.setRegistryUser(loggerUser.getUserName());		 
		operation.setState(CorporateProcessStateType.REGISTERED.getCode());
		if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())){
			validatePaymentRemanentEvent(operation);
		}
		else{
			validateConcunrrency(operation);
		}
		saveCoprorativeOperation(operation);
		
		return operation;
	}
	
	/**
	 * Metodo que actualiza motivos de incremento de evento corporativo dividendo en acciones.
	 *
	 * @param operation the new motive increase capital list
	 * @throws ServiceException the service exception
	 */
	private void setMotiveIncreaseCapitalList(CorporativeOperation operation)throws ServiceException{
		if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
			List<CapitalIncreaseMotive> motiveList = operation.getCapitalIncreaseMotives();
			for(CapitalIncreaseMotive capitalMotive: motiveList){
				capitalMotive.setCorporativeOperation(operation);
			}
		}

	}

	/**
	 * Metodo que valida pago de remanente.
	 *
	 * @param operationValidate the operation validate
	 * @throws ServiceException the service exception
	 */
	public void validatePaymentRemanentEvent(CorporativeOperation operationValidate)throws ServiceException{
		CorporativeOperationTO filter =new CorporativeOperationTO();
		filter.setCorporativeEvent(operationValidate.getCorporativeEventType().getCorporativeEventType());
		filter.setFirstState(CorporateProcessStateType.ANNULLED.getCode());
		List<CorporativeOperation> operationList = searchOperationForValidationPayment(filter);

		for(CorporativeOperation operation: operationList){
			operation.getSecurities();
			if(operation.getRegistryDate().compareTo(operationValidate.getRegistryDate())==FundsType.EQUAL_DATE.getCode()  &&
					operation.getCutoffDate().compareTo(operationValidate.getCutoffDate())==FundsType.EQUAL_DATE.getCode()      &&
					operation.getSecurities().getIdSecurityCodePk().equals(operationValidate.getSecurities().getIdSecurityCodePk())){
				throw new ServiceException(ErrorServiceType.CORPORATIVE_PROCESS_PAYMENT_REMANENT_EXIST);
			}
		}
	}
	
	/**
	 * Metodo que valida concurrencia.
	 *
	 * @param valOperation the val operation
	 * @throws ServiceException the service exception
	 */
	public void validateConcunrrency(CorporativeOperation valOperation)throws ServiceException{
		CorporativeOperationTO filter =new CorporativeOperationTO();
		filter.setCorporativeEvent(valOperation.getCorporativeEventType().getCorporativeEventType());
		List<Integer> stateList = new ArrayList<Integer>();
		stateList.add(CorporateProcessStateType.ANNULLED.getCode());
		stateList.add(CorporateProcessStateType.DEFINITIVE.getCode());
		filter.setStateList(stateList);
		filter.setFirstState(CorporateProcessStateType.ANNULLED.getCode());
 		List<CorporativeOperation> operationList = searchOperationForValidation(filter);

		for(CorporativeOperation operation: operationList){
			operation.getSecurities();
 			if(operation.getRegistryDate().compareTo(valOperation.getRegistryDate())==FundsType.EQUAL_DATE.getCode()  &&
					operation.getCutoffDate().compareTo(valOperation.getCutoffDate())==FundsType.EQUAL_DATE.getCode()      &&
					operation.getSecurities().getIdSecurityCodePk().equals(valOperation.getSecurities().getIdSecurityCodePk())){
				throw new ServiceException(ErrorServiceType.CONCURRENCY_LOCK_UPDATE);
			}
		}
	}
	
	/**
	 * Metodo que registra valores destino de una escision.
	 *
	 * @param operation the operation
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	public CorporativeOperation saveCoprorativeOperation(CorporativeOperation operation)throws ServiceException{
		if(operation.getTargetSecurity()== null || operation.getTargetSecurity().getIdSecurityCodePk()==null){
			operation.setTargetSecurity(null);
		}
		create(operation);
		if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
			for(SecuritiesCorporative objSecuritiesCorporative : operation.getSecuritiesCorporatives()){
				objSecuritiesCorporative.setCorporativeOperation(operation);
				create(objSecuritiesCorporative);
			}
		}		
		return operation;
	}

	/**
	 * Metodo que actualiza valores destino de una escision.
	 *
	 * @param operation the operation
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	public CorporativeOperation updateCorporativeOperation(CorporativeOperation operation)throws ServiceException{
		if(operation.getTargetSecurity()!=null && operation.getTargetSecurity().getIdSecurityCodePk()==null){
			operation.setTargetSecurity(null);
		}
		
		if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
			List<SecuritiesCorporative> lstSecuritiesCorporative = operation.getSecuritiesCorporatives();
			deletePreviousSecuritiesCorporative(operation.getIdCorporativeOperationPk());
			for(SecuritiesCorporative objSecuritiesCorporative : lstSecuritiesCorporative){
				objSecuritiesCorporative.setIdSecuritiesCorporativePk(null);
				objSecuritiesCorporative.setCorporativeOperation(operation);
				create(objSecuritiesCorporative);
			}
			operation.setSecuritiesCorporatives(lstSecuritiesCorporative);
		}	
		
		update(operation);
		
		return operation;
	}
	
	/**
	 * Metodo que elimina los valores destino de una escision.
	 *
	 * @param idOperationPK the id operation pk
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean deletePreviousSecuritiesCorporative(Long idOperationPK)throws ServiceException{
		Query query = em.createQuery("delete from SecuritiesCorporative cim where cim.corporativeOperation.idCorporativeOperationPk=:idOperationPk");
		query.setParameter("idOperationPk", idOperationPK);
		int result = query.executeUpdate();
		return true;
		
	}
	
	/**
	 * Metodo que actualiza estado un hecho de importancia
	 *
	 * @param corpOperation the corp operation
	 * @throws ServiceException the service exception
	 */
	public void updateCorporativeOperationState(CorporativeOperation corpOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update CorporativeOperation s ");
		sbQuery.append(" Set s.state=:statePrm, ");
		sbQuery.append(" s.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" s.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" s.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" s.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append(" where s.idCorporativeOperationPk=:idCorpOperationPrm");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("statePrm",corpOperation.getState());
		query.setParameter("lastModifyDatePrm",corpOperation.getLastModifyDate());
		query.setParameter("lastModifyUserPrm",corpOperation.getLastModifyUser());
		query.setParameter("lastModifyIpPrm",corpOperation.getLastModifyIp());
		query.setParameter("lastModifyAppPrm",corpOperation.getLastModifyApp());
		query.setParameter("idCorpOperationPrm",corpOperation.getIdCorporativeOperationPk());
	    query.executeUpdate();
	}

	/**
	 * Metodo que retorna lista de valores destino de una escision.
	 *
	 * @param idCorporate the id corporate
	 * @return the list securities corporative
	 * @throws ServiceException the service exception
	 */
	public List<SecuritiesCorporative> getListSecuritiesCorporative(Long idCorporate)throws ServiceException{		
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ia ");
			sbQuery.append("	FROM SecuritiesCorporative ia join fetch ia.security");			
			sbQuery.append("	Where ia.corporativeOperation.idCorporativeOperationPk = :idCorporativeOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idCorporativeOperationPk", idCorporate);			
			return (List<SecuritiesCorporative>)query.getResultList();
			} catch(NonUniqueResultException ex){
				   return null;
			}		
	}
	
	/**
	 * Metodo que retorna un hecho de importancia
	 *
	 * @param filter the filter
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	public CorporativeOperation findCorporativeOperationByFilter(CorporativeOperationTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select co FROM CorporativeOperation co join fetch co.corporativeEventType join fetch co.securities join fetch co.issuer");
		if(filter.getNeedTargetSecurity()){
			sbQuery.append(" join fetch co.targetSecurity");
		}
		sbQuery.append(" where 1 = 1");

		if(Validations.validateIsNotNull(filter.getId())){
			sbQuery.append(" and co.idCorporativeOperationPk =:idOperation");
		}

		if(Validations.validateIsNotNull(filter.getSourceSecurityCode())){
			sbQuery.append(" and co.securities.idSecurityCodePk =:idSecurityCodePk");
		}

		if(Validations.validateIsNotNull(filter.getState())){
			sbQuery.append(" and co.state =:state");
		}

		if(Validations.validateIsNotNull(filter.getIndRemanent())){
			sbQuery.append(" and co.indRemanent =:indReman");
		}
		if(Validations.validateIsNotNull(filter.getIssuerCode())){
			sbQuery.append(" and co.issuer.idIssuerPk=:issuer");
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getInstrumentType())){
			sbQuery.append(" and co.securities.instrumentType=:instrumentType");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEvent())){
			sbQuery.append(" and co.corporativeEventType.corporativeEventType=:eventType");
		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getEventTypeList())){
			sbQuery.append(" and co.corporativeEventType.corporativeEventType in (:lstEventType)");
		}
	


		Query query = em.createQuery(sbQuery.toString());

		if(Validations.validateIsNotNull(filter.getId())){
			query.setParameter("idOperation", filter.getId());
		}

		if(Validations.validateIsNotNull(filter.getSourceSecurityCode())){
			query.setParameter("idSecurityCodePk",filter.getSourceSecurityCode());			
		}

		if(Validations.validateIsNotNull(filter.getState())){
			query.setParameter("state", filter.getState());		}

		if(Validations.validateIsNotNull(filter.getIndRemanent())){
			query.setParameter("indReman", filter.getIndRemanent());
		}

		if(Validations.validateIsNotNull(filter.getIssuerCode())){
			query.setParameter("issuer", filter.getIssuerCode());
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getInstrumentType())){
			query.setParameter("instrumentType", filter.getInstrumentType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEvent())){
			query.setParameter("eventType", filter.getCorporativeEvent());
 		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getEventTypeList())){
			query.setParameter("lstEventType", filter.getEventTypeList());
		}

		CorporativeOperation operation =null;

		try{
			operation =(CorporativeOperation) query.getSingleResult();
		}catch (javax.persistence.NoResultException e) {
			return null;

		}	

		return operation;

	}


	/**
	 * Metodo que retorna lista de hecho de importancias
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> findCorporativeOperationsByFilter(CorporativeOperationTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select co FROM CorporativeOperation co join fetch co.corporativeEventType join fetch co.securities join fetch co.issuer ");
		sbQuery.append(" where 1 = 1");
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListSecurityClassExcluded())){
			sbQuery.append(" and not co.securities.securityClass in :listSecurityClassExcluded");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceSecurityCode())){
			sbQuery.append(" and co.securities.idSecurityCodePk =:idSecurityCodePk");
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
			if(filter.getState()!=0){
				sbQuery.append(" and co.state =:state");
			}
		}

		if(Validations.validateIsNotNull(filter.getIndRemanent())){
			sbQuery.append(" and co.indRemanent =:indReman");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerCode())){
			sbQuery.append(" and co.issuer.idIssuerPk=:issuer");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInstrumentType())){
			if(filter.getInstrumentType()!=0){
				sbQuery.append(" and co.securities.instrumentType=:instrumentType");
			}			
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEventType())){
			if(filter.getCorporativeEventType()!=0){
				sbQuery.append(" and co.corporativeEventType.corporativeEventClass=:eventClass");
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEvent())){
			if(filter.getCorporativeEvent()!=0){
				sbQuery.append(" and co.corporativeEventType.corporativeEventType=:eventType");
			}
		}
		
		
		if(Validations.validateIsNotNull(filter.getRegistryDate())){
			sbQuery.append(" and co.registryDate=:registryDate");
		}
		
		if(Validations.validateIsNotNull(filter.getDeliveryDate())){
			sbQuery.append(" and co.deliveryDate=:deliveryDate");

		}
		
		if(Validations.validateIsNotNull(filter.getCutOffDate())){
			sbQuery.append(" and co.cutoffDate=:cutoffDate");

		}
		

		sbQuery.append(" ORDER BY co.idCorporativeOperationPk desc");


		Query query = em.createQuery(sbQuery.toString());

		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListSecurityClassExcluded())){
			query.setParameter("listSecurityClassExcluded", filter.getListSecurityClassExcluded());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInstrumentType())){
			if(filter.getInstrumentType()!=0){
				query.setParameter("instrumentType", filter.getInstrumentType());
			}			
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEventType())){
			if(filter.getCorporativeEventType()!=0){
				query.setParameter("eventClass", filter.getCorporativeEventType());
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEvent())){
			if(filter.getCorporativeEvent()!=0){
				query.setParameter("eventType", filter.getCorporativeEvent());
			}
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getSourceSecurityCode())){
			query.setParameter("idSecurityCodePk",filter.getSourceSecurityCode());			
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
			if(filter.getState()!=0){
				query.setParameter("state", filter.getState());
			}

		}

		if(Validations.validateIsNotNull(filter.getIndRemanent())){
			query.setParameter("indReman", filter.getIndRemanent());
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerCode())){
			query.setParameter("issuer", filter.getIssuerCode());
		}			
		
		if(Validations.validateIsNotNull(filter.getRegistryDate())){
			query.setParameter("registryDate", filter.getRegistryDate());
			
		}
		

		if(Validations.validateIsNotNull(filter.getDeliveryDate())){
 			query.setParameter("deliveryDate", filter.getDeliveryDate());
		}
		
		if(Validations.validateIsNotNull(filter.getCutOffDate())){
 			query.setParameter("cutoffDate", filter.getCutOffDate()); 
		}


		List<CorporativeOperation> list = (List<CorporativeOperation>)query.getResultList();


		return list;

	}
	
	
	/**
	 * Metodo que retorna lista de hechos de importancia validos.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> searchOperationForValidation(CorporativeOperationTO filter)throws ServiceException{
	    Query query = em.createNamedQuery(CorporativeOperation.CORPORATIVE_OPERATION_VALIDATION_PARAMETER);
	     List<CorporativeOperation> list;
	     if(Validations.validateIsNotNull(filter.getCorporativeEvent())){
	    	 query.setParameter("eventParam", filter.getCorporativeEvent());
	     }
	     
 	    	 //query.setParameter("firstState", filter.getFirstState());
 	    	 query.setParameter("stateList", filter.getStateList());
 
	    list   = (List<CorporativeOperation>)query.getResultList();
	    return list;
	}
	
	/**
	 * Metodo que elimina motivos de incremento de un dividendo en acciones
	 *
	 * @param idOperationPK the id operation pk
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean deletePreviousCapitalIncreaseMotive(Long idOperationPK)throws ServiceException{
		Query query = em.createQuery("delete from CapitalIncreaseMotive cim where cim.corporativeOperation.idCorporativeOperationPk=:idOperationPk");
		query.setParameter("idOperationPk", idOperationPK);
		int result = query.executeUpdate();
		return true;
		
	}
	
	/**
	 * Creates the detached entity.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 */
	public <T> void createDetachedEntity(T t){
		em.detach(t);	 
	}
	

	
	/**
	 * Metodo que retorna lista de hechos de importancia validos para pagar.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> searchOperationForValidationPayment(CorporativeOperationTO filter)throws ServiceException{
	    Query query = em.createNamedQuery(CorporativeOperation.CORPORATIVE_OPERATION_VALIDATION_PARAMETER_PAYMENT_REMANENT);
	     List<CorporativeOperation> list;
	     if(Validations.validateIsNotNull(filter.getCorporativeEvent())){
	    	 query.setParameter("eventParam", filter.getCorporativeEvent());
	     }
	     
 	    	 query.setParameter("firstState", filter.getFirstState());
 
	    list   = (List<CorporativeOperation>)query.getResultList();
	    return list;
	}

	/**
	 * Metodo que valida existencia de mas de un corporativo especial para un mismo valor.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return true, if successful
	 */
	public boolean validateEventExistOnSourceSecurity(String idSecurityCodePk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select nvl(count(cop.idCorporativeOperationPk),0) ");
		sbQuery.append("	from CorporativeOperation cop where "); //0
		sbQuery.append("	cop.securities.idSecurityCodePk = :idSecurityCodePk and "); //1
		sbQuery.append("	cop.securities.stateSecurity = :securityState and "); //1
		sbQuery.append("	cop.state in :states and "); //2
		sbQuery.append("	cop.corporativeEventType.corporativeEventType in :types "); //3
		
		List<Integer> restrictedTypes = Arrays.asList(
				ImportanceEventType.SECURITIES_EXCISION.getCode(),
				ImportanceEventType.SECURITIES_FUSION.getCode(),
				ImportanceEventType.SECURITIES_UNIFICATION.getCode());
		
		List<Integer> states = Arrays.asList(
				CorporateProcessStateType.REGISTERED.getCode(),
				CorporateProcessStateType.PRELIMINARY.getCode(),
				CorporateProcessStateType.MODIFIED.getCode());
		
		parameters.put("types",restrictedTypes);
		parameters.put("states",states);
		parameters.put("idSecurityCodePk", idSecurityCodePk);
		parameters.put("securityState", SecurityStateType.REGISTERED.getCode());
		
		Long count = (Long)findObjectByQueryString(sbQuery.toString(), parameters);
		if(count > 0){
			return true;
		}else{
			return false;
		}
	}
}
