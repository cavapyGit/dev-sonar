package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * The Class AllocationPaymentSearchTO.
 * @author Pradera software
 *
 */
public class AllocationPaymentSearchTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The payment date. */
	private Date paymentDate;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The currency. */
	private Integer currency;
	
	/** The currency selected. */
	private String currencySelected;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The security. */
	private Security security;

	/**
	 * Instantiates a new allocation payment search to.
	 */
	public AllocationPaymentSearchTO(){
		
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the payment date.
	 *
	 * @return the payment date
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}
	
	/**
	 * Sets the payment date.
	 *
	 * @param paymentDate the new payment date
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	/**
	 * Gets the currency selected.
	 *
	 * @return the currency selected
	 */
	public String getCurrencySelected() {
		return currencySelected;
	}
	
	/**
	 * Sets the currency selected.
	 *
	 * @param currencySelected the new currency selected
	 */
	public void setCurrencySelected(String currencySelected) {
		this.currencySelected = currencySelected;
	}
}