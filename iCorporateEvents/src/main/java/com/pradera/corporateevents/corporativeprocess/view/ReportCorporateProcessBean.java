package com.pradera.corporateevents.corporativeprocess.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.reports.CorporateProcessReportsSender;
import com.pradera.corporateevents.corporativeprocess.service.CorporateServiceBean;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeEventReportType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;

// TODO: Auto-generated Javadoc
/**
 * The Class ReportCorporateProcessBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ReportCorporateProcessBean extends GenericBaseBean implements Serializable { 
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Corporate MonitoringProcess Service facade. */

	private CorporativeOperation reportCorporativeOperation;
	
	/** The lst event type. */
	private List<ImportanceEventType> lstEventType;
	
	/** The lst reprot type. */
	private List<CorporativeEventReportType> lstReprotType;
	
	/** The lst corporate process state type. */
	private List<CorporateProcessStateType> lstCorporateProcessStateType;
	
	/** The report type. */
	private Integer reportType;	
	
	/** The flag participant. */
	private boolean flagParticipant = false;
	
	/** The flag issuer. */
	private boolean flagIssuer = false;
	
	/** The flag consolidated. */
	private boolean flagConsolidated = false;
	
	/** The list participants. */
	private List<Participant> listParticipants;

	/** The list participants. */
	private List<Issuer> listIssuers;

	/** The report sender. */
	@Inject
	CorporateProcessReportsSender reportSender;
	
	/** The corporate service bean. */
	@EJB
	CorporateServiceBean corporateServiceBean;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/**
	 * Init.
	 */
	@PostConstruct
	public void init() { 
		reportCorporativeOperation = new CorporativeOperation();
		reportCorporativeOperation.setCutoffDate(CommonsUtilities.currentDate());
		reportCorporativeOperation.setDeliveryDate(CommonsUtilities.currentDate());
		reportCorporativeOperation.setSecurities(new Security());
		reportCorporativeOperation.setTargetSecurity(new Security());
		reportCorporativeOperation.setIssuer(new Issuer());
		reportCorporativeOperation.setIdParticipantPk(null);
		listParticipants = new ArrayList<Participant>();
		listIssuers = new ArrayList<Issuer>();
		loadParticipants();
		loadIssuers();
	}
	
	/**
	 * To send CorporativeProcessReport.
	 */
	public void sendCorporativeProcessReports(){
		try{
			LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
			if(Validations.validateIsNotNullAndNotEmpty(reportCorporativeOperation.getSecurities().getIdSecurityCodePk())) {
				CorporativeOperation  objCorporativeOperation =corporateServiceBean.getCorporateOperationForFilters(reportCorporativeOperation.getCutoffDate(), reportCorporativeOperation.getDeliveryDate(),
						reportCorporativeOperation.getSecurities().getIdSecurityCodePk(),reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType(),reportCorporativeOperation.getState());
				if(Validations.validateIsNullOrEmpty(reportCorporativeOperation.getIdCorporativeOperationPk())){
					reportCorporativeOperation.setIdCorporativeOperationPk(objCorporativeOperation.getIdCorporativeOperationPk());
				}			
				reportCorporativeOperation.getCorporativeEventType().setIdCorporativeEventTypePk(objCorporativeOperation.getCorporativeEventType().getIdCorporativeEventTypePk());
				if(ImportanceEventType.SECURITIES_EXCISION.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
					List<Object[]> securities = corporateServiceBean.getExcisionSecurities(reportCorporativeOperation.getIdCorporativeOperationPk(), true);
					if(securities != null && !securities.isEmpty()){
						for(Object[] data : securities){
							String originSecurity = data[0].toString();
							if(Validations.validateIsNullOrNotPositive(reportType)){
								reportSender.sendReport(reportCorporativeOperation, loggerUser, originSecurity, CorporativeEventReportType.FOR_SECURITY_ISSUER);
								reportSender.sendReport(reportCorporativeOperation, loggerUser, originSecurity, CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS);
								reportSender.sendReport(reportCorporativeOperation, loggerUser, originSecurity, CorporativeEventReportType.REPURCHASE_BY_MARGIN);
								reportSender.sendReport(reportCorporativeOperation, loggerUser, originSecurity, CorporativeEventReportType.BLOCKED_BY_BALANCE);
								reportSender.sendReport(reportCorporativeOperation, loggerUser, originSecurity, CorporativeEventReportType.SPECIAL_PROCESS_CONTROL_LIST);
							}else{
								reportSender.sendReport(reportCorporativeOperation,loggerUser,originSecurity,CorporativeEventReportType.get(reportType));
							}						
						}
					}
				}else if(ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())
						|| ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())
						|| ImportanceEventType.CASH_DIVIDENDS.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())
						|| ImportanceEventType.INTEREST_PAYMENT.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())
						|| ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
					
					if(Validations.validateIsNullOrNotPositive(reportType)){
						if(!ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
							reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.FOR_SECURITY_ISSUER);
						}					
						if(!ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
							reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.CONTROL_OF_PAYMENTS);
						}						
						reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.REPURCHASE_BY_MARGIN);
						if(ImportanceEventType.CASH_DIVIDENDS.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType()) || 
								ImportanceEventType.INTEREST_PAYMENT.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType()) || 
								ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType()) || 
								ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType()) || 
								ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
							reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS);
						}						
						reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.BLOCKED_BY_BALANCE);
					}else{
						reportSender.sendReport(reportCorporativeOperation,loggerUser,null,CorporativeEventReportType.get(reportType));
					}				
				}else if(CorporateProcessUtils.isSpecialCorporateProcess(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
					String security = null;
					Security targetSecurity = null;
					try{
						targetSecurity = objCorporativeOperation.getTargetSecurity();
					}catch (Exception e) {
						//Not required the exception
					}
					if(targetSecurity == null && reportCorporativeOperation.getIdCorporativeOperationPk() != null && reportCorporativeOperation.getIdCorporativeOperationPk() > 0){
						targetSecurity = corporateServiceBean.getTargetSecurity(objCorporativeOperation.getIdCorporativeOperationPk());						
					}
					if(targetSecurity != null){
						security = targetSecurity.getIdSecurityCodePk();
					}else{
						security = objCorporativeOperation.getSecurities().getIdSecurityCodePk();
					}
					if(Validations.validateIsNullOrNotPositive(reportType)){					
						reportSender.sendReport(reportCorporativeOperation, loggerUser, security, CorporativeEventReportType.FOR_SECURITY_ISSUER);
						reportSender.sendReport(reportCorporativeOperation, loggerUser, security, CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS);
						reportSender.sendReport(reportCorporativeOperation, loggerUser, security, CorporativeEventReportType.REPURCHASE_BY_MARGIN);
						reportSender.sendReport(reportCorporativeOperation, loggerUser, security, CorporativeEventReportType.BLOCKED_BY_BALANCE);
						reportSender.sendReport(reportCorporativeOperation, loggerUser, security, CorporativeEventReportType.SPECIAL_PROCESS_CONTROL_LIST);
					}else{
						reportSender.sendReport(reportCorporativeOperation,loggerUser,security,CorporativeEventReportType.get(reportType));
					}
				}
			}else {
				if(ImportanceEventType.CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())
						|| ImportanceEventType.CONSOLIDATED_INTEREST_PAYMENT_ISSUER.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())
						|| ImportanceEventType.CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())
						|| ImportanceEventType.CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())) {
					if(Validations.validateIsNullOrNotPositive(reportType)){
						if(ImportanceEventType.CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
							reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT);
						}else if(ImportanceEventType.CONSOLIDATED_INTEREST_PAYMENT_ISSUER.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())) {
							reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.CONSOLIDATED_INTEREST_PAYMENT_ISSUER);
						}else if(ImportanceEventType.CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())) {
							reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT);
						}else if(ImportanceEventType.CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())) {
							reportSender.sendReport(reportCorporativeOperation, loggerUser, null, CorporativeEventReportType.CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER);
						}
					}else{
						reportSender.sendReport(reportCorporativeOperation,loggerUser,null,CorporativeEventReportType.get(reportType));
					}
				}
			}
			JSFUtilities.showComponent(":formPopUp:idDialCevaldom");
			JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.CORPORATIVE_PROCESS_REPORT_SUCCESS, null);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * To clean.
	 */
	public void clean(){
		reportCorporativeOperation = new CorporativeOperation();
		reportCorporativeOperation.setCutoffDate(CommonsUtilities.currentDate());
		reportCorporativeOperation.setDeliveryDate(CommonsUtilities.currentDate());
		reportCorporativeOperation.setSecurities(new Security());
		reportCorporativeOperation.setTargetSecurity(new Security());
		reportCorporativeOperation.setIdParticipantPk(null);
		reportCorporativeOperation.setIssuer(new Issuer());
		reportCorporativeOperation.getCorporativeEventType().setCorporativeEventType(null);
		flagParticipant = false;
		flagIssuer = false;
		flagConsolidated = false;
		reportType = null;
	}
	
	/**
	 * Valid stock type.
	 */
	public void validEventType(){
		if(ImportanceEventType.CONSOLIDATED_INTEREST_PAYMENT_PARTICIPANT.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			flagParticipant = true;
			flagIssuer = false;
			flagConsolidated = true;
		}else if(ImportanceEventType.CONSOLIDATED_INTEREST_PAYMENT_ISSUER.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			flagParticipant = false;
			flagIssuer = true;
			flagConsolidated = true;
		}else if(ImportanceEventType.CONSOLIDATED_AMORTIZATION_PAYMENT_PARTICIPANT.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			flagParticipant = true;
			flagIssuer = false;
			flagConsolidated = true;
		}else if(ImportanceEventType.CONSOLIDATED_AMORTIZATION_PAYMENT_ISSUER.getCode().equals(reportCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			flagParticipant = false;
			flagIssuer = true;
			flagConsolidated = true;
		}else{
			flagParticipant = false;
			flagIssuer = false;
			flagConsolidated = false;
		}
		reportCorporativeOperation.setIdParticipantPk(null);
		reportCorporativeOperation.setIssuer(new Issuer());
	}
	
	/**
	 * Load participants.
	 */
	public void loadParticipants(){
		try {
			listParticipants = new ArrayList<Participant>();
			Participant par = new Participant();
			par.setState(ParticipantStateType.REGISTERED.getCode());
			listParticipants = accountsFacade.getLisParticipantServiceBean(par);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load issuers.
	 */
	public void loadIssuers(){
		try {
			listIssuers = new ArrayList<Issuer>();
			Issuer iss = new Issuer();
			iss.setStateIssuer(IssuerStateType.REGISTERED.getCode());
			listIssuers = accountsFacade.getLisIssuerServiceBean(iss);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gets the report corporative operation.
	 *
	 * @return the reportCorporativeOperation
	 */
	public CorporativeOperation getReportCorporativeOperation() {
		return reportCorporativeOperation;
	}

	/**
	 * Sets the report corporative operation.
	 *
	 * @param reportCorporativeOperation the reportCorporativeOperation to set
	 */
	public void setReportCorporativeOperation(
			CorporativeOperation reportCorporativeOperation) {
		this.reportCorporativeOperation = reportCorporativeOperation;
	}

	/**
	 * Gets the lst event type.
	 *
	 * @return the lstEventType
	 */
	public List<ImportanceEventType> getLstEventType() {
		lstEventType = ImportanceEventType.list;
		return lstEventType;
	}

	/**
	 * Sets the lst event type.
	 *
	 * @param lstEventType the lstEventType to set
	 */
	public void setLstEventType(List<ImportanceEventType> lstEventType) {
		this.lstEventType = lstEventType;
	}

	/**
	 * Gets the lst reprot type.
	 *
	 * @return the lstReprotType
	 */
	public List<CorporativeEventReportType> getLstReprotType() {
		lstReprotType = CorporativeEventReportType.list;
		return lstReprotType;
	}

	/**
	 * Sets the lst reprot type.
	 *
	 * @param lstReprotType the lstReprotType to set
	 */
	public void setLstReprotType(List<CorporativeEventReportType> lstReprotType) {
		this.lstReprotType = lstReprotType;
	}

	/**
	 * Gets the lst corporate process state type.
	 *
	 * @return the lstCorporateProcessStateType
	 */
	public List<CorporateProcessStateType> getLstCorporateProcessStateType() {
		lstCorporateProcessStateType = CorporateProcessStateType.list;
		return lstCorporateProcessStateType;
	}

	/**
	 * Sets the lst corporate process state type.
	 *
	 * @param lstCorporateProcessStateType the lstCorporateProcessStateType to set
	 */
	public void setLstCorporateProcessStateType(
			List<CorporateProcessStateType> lstCorporateProcessStateType) {
		this.lstCorporateProcessStateType = lstCorporateProcessStateType;
	}

	/**
	 * Gets the report type.
	 *
	 * @return the reportType
	 */
	public Integer getReportType() {
		return reportType;
	}

	/**
	 * Sets the report type.
	 *
	 * @param reportType the reportType to set
	 */
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}
	
	public boolean isFlagParticipant() {
		return flagParticipant;
	}

	public void setFlagParticipant(boolean flagParticipant) {
		this.flagParticipant = flagParticipant;
	}

	public boolean isFlagIssuer() {
		return flagIssuer;
	}

	public void setFlagIssuer(boolean flagIssuer) {
		this.flagIssuer = flagIssuer;
	}

	public boolean isFlagConsolidated() {
		return flagConsolidated;
	}

	public void setFlagConsolidated(boolean flagConsolidated) {
		this.flagConsolidated = flagConsolidated;
	}
	
	public List<Participant> getListParticipants() {
		return listParticipants;
	}

	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}

	public List<Issuer> getListIssuers() {
		return listIssuers;
	}

	public void setListIssuers(List<Issuer> listIssuers) {
		this.listIssuers = listIssuers;
	}


	
}
