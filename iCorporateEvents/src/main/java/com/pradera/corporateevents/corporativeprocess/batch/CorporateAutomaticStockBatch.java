package com.pradera.corporateevents.corporativeprocess.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.process.ProcessLogger;
// TODO: Auto-generated Javadoc
import com.pradera.model.process.ProcessLoggerDetail;

/**
 * Batch que vence los valores DPF del dia.
 *
 * @author PraderaTechnologies
 */
@BatchProcess(name="CorporateAutomaticStockBatch")
public class CorporateAutomaticStockBatch implements Serializable, JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The corporate process service facade. */
	@EJB
	CorporateProcessServiceFacade corporateProcessServiceFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/**
	 * Inicio de Bachero que redime valores DPF.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info("INICIO ::::::: ExpiredSecuritiesDpfBatch ");
		// TODO Auto-generated method stub	
			try {
				//obtiene lista de valores con fecha de expiracion actual 
				 List<CorporativeOperation> securities = corporateProcessServiceFacade.listSecurityToDefinitive();
				 
				 HashMap<String, CorporativeOperation> distinct = new HashMap<>();
				
				 for(CorporativeOperation stocks : securities){
					 
					 distinct.put(stocks.getSecurities().getIdSecurityCodePk(), stocks);
				 }
				 if(distinct.size()>GeneralConstants.ZERO_VALUE_INT){
					 
					 List<CorporativeOperation> corporative  =  new ArrayList<CorporativeOperation>(distinct.values());
					 corporateProcessServiceFacade.stocksAutomatic(corporative);
					 
				 }else{
					 log.info(" ============ NO FALTA NINGUN CALCULO DE STOCK =====================");
				 }
				 
				 
			} catch (Exception e) {
				 log.info(" ============ ERROR DESCONOCIDO =====================");
			}
			
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
	
}
