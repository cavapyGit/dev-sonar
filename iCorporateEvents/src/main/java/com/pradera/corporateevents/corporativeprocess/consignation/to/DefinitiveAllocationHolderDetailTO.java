package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class DefinitiveAllocationHolderDetailTO.
 * @author Pradera Technologies
 *
 */
public class DefinitiveAllocationHolderDetailTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The bank. */
	private Long bank;
	
	/** The bank description. */
	private String bankDescription;
	
	/** The holder quantity. */
	private Integer holderQuantity;
	
	/** The bank amount. */
	private BigDecimal bankAmount;
	
	/**
	 * Instantiates a new definitive allocation holder detail to.
	 */
	public DefinitiveAllocationHolderDetailTO() {
		
	}
	
	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public Long getBank() {
		return bank;
	}
	
	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(Long bank) {
		this.bank = bank;
	}
	
	/**
	 * Gets the bank description.
	 *
	 * @return the bank description
	 */
	public String getBankDescription() {
		return bankDescription;
	}
	
	/**
	 * Sets the bank description.
	 *
	 * @param bankDescription the new bank description
	 */
	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}
	
	/**
	 * Gets the holder quantity.
	 *
	 * @return the holder quantity
	 */
	public Integer getHolderQuantity() {
		return holderQuantity;
	}
	
	/**
	 * Sets the holder quantity.
	 *
	 * @param holderQuantity the new holder quantity
	 */
	public void setHolderQuantity(Integer holderQuantity) {
		this.holderQuantity = holderQuantity;
	}
	
	/**
	 * Gets the bank amount.
	 *
	 * @return the bank amount
	 */
	public BigDecimal getBankAmount() {
		return bankAmount;
	}
	
	/**
	 * Sets the bank amount.
	 *
	 * @param bankAmount the new bank amount
	 */
	public void setBankAmount(BigDecimal bankAmount) {
		this.bankAmount = bankAmount;
	}


}