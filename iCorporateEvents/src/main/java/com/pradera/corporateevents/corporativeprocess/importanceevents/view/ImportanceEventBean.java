/*
 * 
 */
package com.pradera.corporateevents.corporativeprocess.importanceevents.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.component.inputtext.InputText;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.core.component.helperui.view.SecurityHelperBean;
import com.pradera.corporateevents.corporativeprocess.importanceevents.facade.ImportanceEventFacade;
import com.pradera.corporateevents.corporativeprocess.importanceevents.to.CorporativeOperationTO;
import com.pradera.corporateevents.corporativeprocess.process.TestingCustodyCommissionBatch;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.corporatives.CapitalIncreaseMotive;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.SecuritiesCorporative;
import com.pradera.model.corporatives.type.CapitalIncreaeMotiveType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeEventType;
import com.pradera.model.corporatives.type.CorporativeSecuritySourceType;
import com.pradera.model.corporatives.type.DeliveryPlacementType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.corporatives.type.NegotiationRankType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * Clase administradora de la vista del hecho de importancia.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , Jul 26, 2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ImportanceEventBean extends GenericBaseBean implements Serializable {

	/** The country residence. */
	@Inject
	@Configurable
	private Integer countryResidence;

	/** The max days of custody request. */
	@Inject
	@Configurable
	private Integer maxDaysOfCustodyRequest;

	/** The obj. */
	@Inject
	private TestingCustodyCommissionBatch obj;
	
	/** The security helper bean. */
	@Inject
	private SecurityHelperBean securityHelperBean;

	/** The security help bean. */
	@Inject
	private SecurityHelpBean securityHelpBean;

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The corporative operation to. */
	private CorporativeOperationTO corporativeOperationTO;

	/** The negotiation rank. */
	private Integer negotiationRank;

	/** The general parameters facade. */
	@Inject
	private GeneralParametersFacade generalParametersFacade;

	/** The all componente facade. */
	@Inject
	private HelperComponentFacade allComponenteFacade;

	/** The improtance event facade. */
	@Inject
	private ImportanceEventFacade improtanceEventFacade;

	/** The user info. */
	@Inject
	private UserInfo userInfo;

	/** The importance event. */
	private CorporativeOperation importanceEvent;

	/** The instrument type. */
	private Integer instrumentType;

	/** The instrument type description. */
	private String instrumentTypeDescription;	

	/** The corporative event type. */
	private Integer corporativeEventType;

	/** The list instrument type. */
	private List<ParameterTable> listInstrumentType;

	/** The list corporative events. */
	private List<ParameterTable> listCorporativeEvents;

	/** The list agreement type. */
	private List<ParameterTable> listAgreementType;

	/** The list currency type. */
	private List<ParameterTable> listCurrencyType;

	/** The list capital increase type. */
	private List<ParameterTable> listCapitalIncreaseType;

	/** The list delivery placetype. */
	private List<ParameterTable> listDeliveryPlacetype;

	/** The list corporative process state type. */
	private List<ParameterTable> listCorporativeProcessStateType;
	
	/** The list corporative process state type. */
	private List<ParameterTable> listCorporativeProcessEventType;

	/** The list corporative operation. */
	private List<CorporativeOperation> listCorporativeOperation;

	/** The corporative operation data model. */
	private CorporativeOperationDataModel corporativeOperationDataModel; 

	/** The list corporative even type. */
	private List<CorporativeEventType> listCorporativeEvenType;
	
	/** The lst cbo corporative even class. */
	private List<ParameterTable> lstCboCorporativeEvenClass;

	/** The list negotiation rank type. */
	private List<NegotiationRankType> listNegotiationRankType;

	/** The corporative operation. */
	private CorporativeOperation corporativeOperation;

	/** The selected corporative opertation. */
	private CorporativeOperation selectedCorporativeOpertation;

	/** The list target securities. */
	private List<SecuritiesTO> listTargetSecurities;

	/** The lst target securities. */
	private List<Security> lstTargetSecurities;

	/** The negotiation rank. */

	/** The issuer. */
	private IssuerTO issuer;

	/** The disabled for excision. */
	private Boolean disabledForExcision;
	
	/** The render security description. */
	private Boolean renderSecurityDescription;

	/** The render securities escision. */
	private Boolean renderSecuritiesEscision;

	/** The render target security. */
	private Boolean renderTargetSecurity;

	/** The render nominal value description. */
	private Boolean renderNominalValueDescription;

	/** The render cap var description. */
	private Boolean renderCapVarDescription;

	/** The render output panel. */
	private Boolean renderOutputPanel;

	/** The disable target securities combo. */
	private Boolean disableTargetSecuritiesCombo;

	/** The render sharecapital. */
	private Boolean renderSharecapital;

	/** The disable share balance. */
	private Boolean disableShareBalance;

	/** The render exchange relationship. */
	private Boolean renderExchangeRelationship;

	/** The render ind remanent. */
	private Boolean renderIndRemanent;

	/** The render opnl target security. */
	private Boolean renderOpnlTargetSecurity;

	/** The render nominal value no cap var. */
	private Boolean renderNominalValueNoCapVar;

	/** The render nominal value cap var. */
	private Boolean renderNominalValueCapVar;

	/** The render cap increase motive. */
	private Boolean renderCapIncreaseMotive;

	/** The render currency type. */
	private Boolean renderCurrencyType;

	/** The render Interest Amortization. */
	private Boolean renderInterestAmortization;
	
	/** The render Amortization. */
	private Boolean renderAmortization;
	
	/** The render Interest. */
	private Boolean renderInterest;
	
	/** The render delivery factor. */
	private Boolean renderDeliveryFactor;

	/** The render delivery factor securities esicion. */
	private Boolean renderDeliveryFactorSecuritiesEsicion;
	
	/** The render securities esicion. */
	private Boolean renderSecuritiesEsicion;
	
	/** The render market fact. */
	private Boolean renderMarketFact;

	/** The require target security. */
	private String requireTargetSecurity = "true";

	/** The disable tax factor. */
	private Boolean disableTaxFactor;	

	/** The render ind tax factor. */
	private Boolean renderIndTaxFactor;

	/** The render tax factor. */
	private Boolean renderTaxFactor;

	/** The render exchange. */
	private Boolean renderExchange;
	
	/** The render preferent suscription panel. */
	private Boolean renderPreferentSuscriptionPanel;

	/** The render remaining cash payment panel. */
	private Boolean renderRemainingCashPaymentPanel;

	/** The disable cutoff date. */
	private Boolean disableCutoffDate;

	/** The end suscription. */
	private Boolean endSuscription;

	/** The render first negotiation date. */
	private Boolean renderFirstNegotiationDate;

	/** The render second negotiation date. */
	private Boolean renderSecondNegotiationDate;

	/** The render third negotiation date. */
	private Boolean renderThirdNegotiationDate;

	/** The render data table. */
	private Boolean renderDataTable;

	/** The disable modify button. */
	private Boolean disableModifyButton;

	/** The disable annular button. */
	private Boolean disableAnnularButton;

	/** The disable revaluation exedent. */
	private Boolean disableRevaluationExedent;

	/** The disable capital reexpresion. */
	private Boolean disableCapitalReexpresion;

	/** The disable reserve capitalization. */
	private Boolean disableReserveCapitalization;

	/** The disable uitilities capitalization. */
	private Boolean disableUitilitiesCapitalization;

	/** The disable fusion. */
	private Boolean disableFusion;

	/** The disable result inflation. */
	private Boolean disableResultInflation;

	/** The disable others. */
	private Boolean disableOthers;

	/** The voluntier revaluation. */
	private Boolean voluntierRevaluation;

	/** The render field set. */
	private Boolean renderFieldSet;

	/** The disable source securitiy helper. */
	private Boolean disableSourceSecuritiyHelper;

	/** The disable delivery place. */
	private Boolean disableDeliveryPlace;

	/** The disable currency. */
	private Boolean disableCurrency;

	/** The monitoring. */
	private Boolean monitoring;
	
	/** The modify for excision. */
	private Boolean modifyForExcision;

	/** The revaluation excedent motive. */
	private CapitalIncreaseMotive revaluationExcedentMotive;

	/** The capital reexpresion motive. */
	private CapitalIncreaseMotive capitalReexpresionMotive;

	/** The reserve capitalization motive. */
	private CapitalIncreaseMotive reserveCapitalizationMotive;

	/** The utilities capitalization motive. */
	private CapitalIncreaseMotive utilitiesCapitalizationMotive;

	/** The fusion motive. */
	private CapitalIncreaseMotive fusionMotive;

	/** The result exposition motive. */
	private CapitalIncreaseMotive resultExpositionMotive;

	/** The others motive. */
	private CapitalIncreaseMotive othersMotive;

	/** The voluntier revaluation motive. */
	private CapitalIncreaseMotive voluntierRevaluationMotive;

	/** The capital increase motivelist. */
	private List<CapitalIncreaseMotive> capitalIncreaseMotivelist = new ArrayList<CapitalIncreaseMotive>();

	/** The event type description. */
	private String eventTypeDescription;

	/** The intrument type description. */
	private String intrumentTypeDescription;

	/** The disable target security helper. */
	private Boolean disableTargetSecurityHelper;

	/** The render motive increase capital label. */
	private Boolean renderMotiveIncreaseCapitalLabel;

	/** The disable corporate event type. */
	private Boolean disableCorporateEventType;

	/** The increase capital motive. */
	private BigDecimal increaseCapitalMotive;

	/** The disable issuer. */
	private Boolean disableIssuer = Boolean.TRUE;

	/** The disable add button. */
	private Boolean disableAddButton;

	/** The securities corporative data model. */
	private SecuritiesCorporativeDataModel securitiesCorporativeDataModel;

	/** The selected securities corporative. */
	private SecuritiesCorporative selectedSecuritiesCorporative;

	/** The map securities corporative. */
	private Map<String,SecuritiesCorporative> mapSecuritiesCorporative;

	/** The temp security. */
	private Security tempSecurity;

	/** The source security. */
	private Security sourceSecurity;
	
	/** The targe security. */
	private Security targeSecurity;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The list securities target. */
	private List<Security> listSecuritiesTarget;
	
	/** The list securities target aux. */
	private List<Security> listSecuritiesTargetAux;
	
	/** The security code. */
	private String securityCode;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The corporative event mgmt. */
	private Integer corporativeEventMgmt;
	
	/** The issuer helper search. */
	private Issuer issuerHelperSearch;
	
	/** The security helper search. */
	private Security securityHelperSearch;
				   
	/** The issuer helper mgmt. */
	private Issuer issuerHelperMgmt;
	
	/** The security source helper mgmt. */
	private Security securitySourceHelperMgmt;
	
	/** The security target helper mgmt. */
	private Security securityTargetHelperMgmt;
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The lst cbo corporative event mgmt. */
	private List<ParameterTable> lstCboCorporativeEventMgmt;
	
	/** The parameters table map. */
	private Map<Integer,Object> parametersTableMap;
	
	/** The parameters table map event. */
	private Map<Integer,Object> parametersTableMapEvent;

	/**
	 * Metodo que asigna valores destino de una escision.
	 *
	 * @param security the security
	 */
	public void assignSecuritiesCorporativeListener(Security security){
		this.hideDialogs();
		executeAction();
		resetComponent();
		if(listSecuritiesTarget!=null){
			for(Security objSecurity:listSecuritiesTarget){
				if(!objSecurity.getIdSecurityCodePk().equals(security.getIdSecurityCodePk())){
					objSecurity.setSelected(false);
				}
			}
		}
		if(selectedSecuritiesCorporative==null || (selectedSecuritiesCorporative.getIdSecuritiesCorporativePk()==null && 
				!getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))){
			if(!security.isSelected()){
				security.setSelected(true);	
				disableTargetSecurityHelper = Boolean.TRUE;
				selectedSecuritiesCorporative = mapSecuritiesCorporative.get(security.getIdSecurityCodePk());
				if(isViewOperationModify() || isViewOperationRegister()){
					if(listSecuritiesTargetAux.contains(security)){
						disabledForExcision = Boolean.TRUE;
					}else{
						disabledForExcision = Boolean.FALSE;
					}
				}
			}else if(security.isSelected() && (isViewOperationAnnul() || isViewOperationDetail() || isViewOperationModify())){
				security.setSelected(false);	
				disableTargetSecurityHelper = Boolean.FALSE;
				selectedSecuritiesCorporative = mapSecuritiesCorporative.get(security.getIdSecurityCodePk());
				disabledForExcision = Boolean.FALSE;
			}			
		}else{
			if(security.isSelected()){
				security.setSelected(false);	
				disableTargetSecurityHelper = Boolean.FALSE;
				selectedSecuritiesCorporative = null;
				disabledForExcision = Boolean.FALSE;
			}else if(!security.isSelected() && (isViewOperationAnnul() || isViewOperationDetail() || isViewOperationModify())){
				security.setSelected(true);	
				disableTargetSecurityHelper = Boolean.TRUE;
				selectedSecuritiesCorporative = mapSecuritiesCorporative.get(security.getIdSecurityCodePk());
				if(isViewOperationModify() || isViewOperationRegister()){
					if(listSecuritiesTargetAux.contains(security)){
						disabledForExcision = Boolean.TRUE;
					}else{
						disabledForExcision = Boolean.FALSE;
					}
				}
			}
		}
		
		if(isViewOperationAnnul() || isViewOperationDetail() || isViewOperationModify()){
			if(selectedSecuritiesCorporative==null){
				selectedCorporativeOpertation.setTargetSecurity(new Security());
				deleteSecurityInfo(selectedCorporativeOpertation, null);
			}else{
				setDataCorporativeOperation(selectedCorporativeOpertation);
				if(isViewOperationModify() && !security.isSelected()){
					selectedCorporativeOpertation.setTargetSecurity(new Security());
					deleteSecurityInfo(selectedCorporativeOpertation, null);
				}
			}
		}else if(selectedSecuritiesCorporative==null){
			corporativeOperation.setTargetSecurity(new Security());
			deleteSecurityInfo(corporativeOperation, null);
		}else{
			setDataCorporativeOperation(corporativeOperation);
		}
	}
	
	/**
	 * Metodo que actualiza datos del hecho de importancia.
	 *
	 * @param operation the new data corporative operation
	 */
	public void setDataCorporativeOperation(CorporativeOperation operation){
		operation.setTargetSecurity(selectedSecuritiesCorporative.getSecurity());
		operation.setOldShareBalance(selectedSecuritiesCorporative.getOldShareBalance());
		operation.setOldCirculationBalance(selectedSecuritiesCorporative.getOldCirculationBalance());
		operation.setOldShareCapital(selectedSecuritiesCorporative.getOldShareCapital());
		operation.setNewCirculationBalance(selectedSecuritiesCorporative.getNewCirculationBalance());
		operation.setNewShareBalance(selectedSecuritiesCorporative.getNewShareBalance());
		operation.setNewShareCapital(selectedSecuritiesCorporative.getNewShareCapital());
		operation.setVariationAmount(selectedSecuritiesCorporative.getVariationAmount());
		operation.setMarketDate(selectedSecuritiesCorporative.getMarketDate());
		operation.setMarketPrice(selectedSecuritiesCorporative.getMarketPrice());
		operation.setExchangeRelaSourceSecurity(selectedSecuritiesCorporative.getOriginRatio());
		operation.setExchangeRelaTargetSecurity(selectedSecuritiesCorporative.getTargetRatio());			
		operation.setOriginRatio(selectedSecuritiesCorporative.getOriginRatio());
		operation.setTargetRatio(selectedSecuritiesCorporative.getTargetRatio());
		operation.setDeliveryFactor(selectedSecuritiesCorporative.getDeliveryFactor());	
	}
	
	/**
	 * Metodo que suma dias a una fecha.
	 *
	 * @param date the date
	 * @param days the days
	 * @return the adds the date
	 */
	public Date getAddDate(Date date, int days){
		return getAlterDateDays(date, days);
	}
	
	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	
	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	/**
	 * Metodo que visualiza detalle de una valor.
	 *
	 * @param securityCode the security code
	 */
	public void viewSecurityFinancialImportanceEvent(String securityCode)
	{
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		  securityData.setSecurityCodePk(securityCode);
		  securityData.setUiComponentName("securityHelp");
		  showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	/**
	 * Lista de dias no habil.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void listHolidays() throws ServiceException {
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Metodo que refresca pantalla.
	 */
	public void resetComponent(){
		JSFUtilities.resetComponent("importanceEventForm:flsCorporativeEvent");
		JSFUtilities.resetComponent("importanceEventForm:flsCorporativeEvenetData");
		JSFUtilities.resetComponent("importanceEventForm:flsCapitalVariation");
	}
	/**
	 * Metodo que ejecuta evento al seleccionar una renta.
	 */
	public void instrumentTypeListener() {
		try {
			this.hideDialogs();
			executeAction();
			resetComponent();
			if (Validations.validateIsNotNullAndPositive(instrumentType)) {
				loadCboCorporativeEventClass();
				disableIssuer = Boolean.TRUE;
				if (instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())) {
					issuerHelperMgmt=new Issuer();
					disableSourceSecuritiyHelper = Boolean.TRUE;
					disableTargetSecurityHelper = Boolean.TRUE;
					securitySourceHelperMgmt=new Security();		
					securityTargetHelperMgmt=new Security();	
					if(corporativeOperation!=null){
						corporativeOperation.setSecurities(new Security());
						corporativeOperation.setTargetSecurity(new Security());
					}				
					this.clearPanels();
					
					corporativeEventType = CorporativeEventType.REORGANIZATION.getCode();
					corporativeEventMgmt=ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode();
					disableCorporateEventType = Boolean.TRUE;
					
					com.pradera.model.corporatives.CorporativeEventType corporativeEventType;
					corporativeEventType=improtanceEventFacade.findCorporativeEventType(new com.pradera.model.corporatives.CorporativeEventType(
							corporativeEventMgmt,
							instrumentType,
							this.corporativeEventType));
					
					corporativeOperation.setCorporativeEventType(corporativeEventType);
					corporativeEventMgmt=corporativeEventType.getCorporativeEventType();
				
					changeCboCorporativeEventClassMgmt();
					if(corporativeOperation!=null){
						changeEventInfoListener();
					}
				} else if (instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode()) 
						|| instrumentType.equals(InstrumentType.MIXED_INCOME.getCode())) {
					disableCorporateEventType = Boolean.FALSE;
					corporativeEventType=null;
					corporativeEventMgmt = null;
					lstCboCorporativeEventMgmt=new ArrayList<ParameterTable>();
					issuerHelperMgmt=new Issuer();
					disableSourceSecuritiyHelper = Boolean.TRUE;
					disableTargetSecurityHelper = Boolean.TRUE;
					securitySourceHelperMgmt=new Security();		
					securityTargetHelperMgmt=new Security();	
					if(corporativeOperation!=null){
						corporativeOperation.setSecurities(new Security());
						corporativeOperation.setTargetSecurity(new Security());
					}				
					this.clearPanels();
				}
			}else{
				disableCorporateEventType = Boolean.FALSE;
				renderMarketFact			= Boolean.FALSE;				
				corporativeEventType=null;
				corporativeEventMgmt = null;
				lstCboCorporativeEvenClass = new ArrayList<ParameterTable>();
				lstCboCorporativeEventMgmt=new ArrayList<ParameterTable>();
				disableIssuer = Boolean.TRUE; 
				issuerHelperMgmt=new Issuer();
				disableSourceSecuritiyHelper = Boolean.TRUE;
				disableTargetSecurityHelper = Boolean.TRUE;
				securitySourceHelperMgmt=new Security();		
				securityTargetHelperMgmt=new Security();	
				if(corporativeOperation!=null){
					corporativeOperation.setSecurities(new Security());
					corporativeOperation.setTargetSecurity(new Security());
				}				
				clearSecurityHelper(); 
				this.clearPanels();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Metodo que crea objeto emisor despues de un proceso.
	 */
	public void afterIssuerHelperSearch(){
		try {
			securityHelperSearch=new Security();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Metodo que limpia objetos de la pantalla.
	 */
	public void clearSearchEntity() {
		this.setCorporativeOperationTO(new CorporativeOperationTO());
		corporativeOperationTO.setDeliveryDate(CommonsUtilities.currentDate());
		cleanImportanceEventModel();
		listCorporativeOperation = new ArrayList<CorporativeOperation>();
		renderFieldSet = Boolean.FALSE;
		renderDataTable = Boolean.FALSE;
		listCorporativeEvents = new ArrayList<ParameterTable>();
	
		JSFUtilities.resetComponent("admImportanceEventForm");
		JSFUtilities.resetViewRoot();
		corporativeOperationTO.setIssuerCode(null);
		issuerHelperSearch=new Issuer();
		securityHelperSearch = new Security();
		selectedCorporativeOpertation = null;
		selectedCorporativeOpertation = new CorporativeOperation();
		clearSecurityHelper();
		increaseCapitalMotive = null;

	}
	
	/**
	 * Metodo que limpia la lista de hecho de importancia.
	 */
	public void cleanImportanceEventModel(){
		corporativeOperationDataModel=null;
		renderDataTable = Boolean.FALSE; 
		renderFieldSet = Boolean.FALSE;
	}
	/**
	 * Metodo que limpia objetos de la pantalla registrar.
	 * 
	 * @param formId
	 *            the form id
	 */
	public void clearRegistry(String formId) {
		this.loadRegisterInformation();
		JSFUtilities.resetComponent(formId);
		this.clearPanels();

		issuerHelperMgmt=new Issuer();
		securitySourceHelperMgmt=new Security();		
		securityTargetHelperMgmt=new Security();	
		increaseCapitalMotive = null;
		disableCorporateEventType = Boolean.FALSE;
		listCorporativeEvents = new ArrayList<ParameterTable>();
		capitalIncreaseMotivelist = new ArrayList<CapitalIncreaseMotive>();
		corporativeOperation.setNewShareCapital(null);
		securitiesCorporativeList = new ArrayList<SecuritiesCorporative>();
		selectedSecuritiesCorporative = new SecuritiesCorporative();
	}

	/**
	 * Metodo q oculta mensaje.
	 */
	public void hideDialogs() {
		JSFUtilities.hideComponent("importanceEventForm:cnfBeforeSave");
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * Metodo que actualiza indicadores de habilitar objetos.
	 */
	public void disableMotiveIncreaseVar() {
		disableRevaluationExedent = Boolean.FALSE;
		disableCapitalReexpresion = Boolean.FALSE;
		disableReserveCapitalization = Boolean.FALSE;
		disableUitilitiesCapitalization = Boolean.FALSE;
		disableFusion = Boolean.FALSE;
		disableResultInflation = Boolean.FALSE;
		disableOthers = Boolean.FALSE;
		voluntierRevaluation = Boolean.FALSE;

	}

	/**
	 * Metodo que valida monto de imcremento de beneficio por dividendo en acciones.
	 * 
	 * @param operation
	 *            the operation
	 * @return the boolean
	 */
	public Boolean validateIncreaseCapitalAmount(CorporativeOperation operation) {
		BigDecimal result = BigDecimal.ZERO;
		try {
			if(isViewOperationModify()){
				result = this.calculateIncreaseAmount(capitalIncreaseMotivelist);
			}else{
				result = this.calculateIncreaseAmount(capitalIncreaseMotivelist);
			}

		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		increaseCapitalMotive = result;
		if (operation.getVariationAmount() != null) {
			if ( result.compareTo(operation.getVariationAmount().setScale(8,BigDecimal.ROUND_HALF_UP)) == 0) {		
				renderMotiveIncreaseCapitalLabel = Boolean.FALSE;
				return Boolean.TRUE;
			} else {
				renderMotiveIncreaseCapitalLabel = Boolean.TRUE;
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;

	}

	/**
	 * Metodo que acepta motivo de incremento dividendo de acciones correctamente. 
	 */
	public void acceptCapitalIncreaeMotive() {
		this.addMotiveToList();
		if (isViewOperationRegister()) {
			if (validateIncreaseCapitalAmount(corporativeOperation)) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveIncrease').hide();");
			}
			else{
				JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveIncrease').hide();");
				JSFUtilities.executeJavascriptFunction("PF('cnfwValidateMotiveIncrease').show();");
				executeAction();
				resetComponent();
			}
		}
		if (isViewOperationModify()) {
			if (validateIncreaseCapitalAmount(selectedCorporativeOpertation)) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveIncrease').hide();");

			}else{
				JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveIncrease').hide();");
				JSFUtilities.executeJavascriptFunction("PF('cnfwValidateMotiveIncrease').show();");
			}
		}
	}

	/**
	 * Metodo que limpia motivo de incremento dividendo de acciones
	 */
	public void clearMotiveModalListener(){
		if(isViewOperationRegister()){
			this.clearMotiveModal(corporativeOperation);
			JSFUtilities.executeJavascriptFunction("PF('cnfwValidateMotiveIncrease').hide();");
		}
		else if(isViewOperationModify()){
			this.clearMotiveModal(selectedCorporativeOpertation);
			JSFUtilities.executeJavascriptFunction("PF('cnfwValidateMotiveIncrease').hide();");
		}

	}



	/**
	 * Metodo que limpia motivo de incremento dividendo de acciones
	 *
	 * @param operation the operation
	 */
	public void clearMotiveModal(CorporativeOperation operation){
		capitalIncreaseMotivelist = new ArrayList<CapitalIncreaseMotive>();
		disableRevaluationExedent = Boolean.FALSE;
		disableCapitalReexpresion = Boolean.FALSE;
		disableReserveCapitalization =Boolean.FALSE;
		disableUitilitiesCapitalization=Boolean.FALSE;
		disableFusion=Boolean.FALSE;
		disableResultInflation=Boolean.FALSE;
		disableOthers=Boolean.FALSE;
		voluntierRevaluation=Boolean.FALSE;
		increaseCapitalMotive = null;
		//Set Motives to null
		revaluationExcedentMotive.setIncreaseQuantity(null);
		capitalReexpresionMotive.setIncreaseQuantity(null);
		reserveCapitalizationMotive.setIncreaseQuantity(null);
		utilitiesCapitalizationMotive.setIncreaseQuantity(null);
		fusionMotive.setIncreaseQuantity(null);
		resultExpositionMotive.setIncreaseQuantity(null);
		othersMotive.setIncreaseQuantity(null);
	}

	/**
	 * Metodo que agrega motivo de incremento dividendo de acciones
	 */
	private void addMotiveToList() {
		if (disableReserveCapitalization) {
			if (!capitalIncreaseMotivelist.contains(reserveCapitalizationMotive)) {
				capitalIncreaseMotivelist.add(reserveCapitalizationMotive);
			}
		} else {
			if (capitalIncreaseMotivelist.contains(reserveCapitalizationMotive)) {
				capitalIncreaseMotivelist.remove(reserveCapitalizationMotive);
			}
		}
		// Utilities Capitalization
		if (disableUitilitiesCapitalization) {
			if (!capitalIncreaseMotivelist.contains(utilitiesCapitalizationMotive)) {
				capitalIncreaseMotivelist.add(utilitiesCapitalizationMotive);
			}
		} else {
			if (capitalIncreaseMotivelist.contains(utilitiesCapitalizationMotive)) {
				capitalIncreaseMotivelist.remove(utilitiesCapitalizationMotive);
			}
		}
		// Revaluation Exedent
		if (disableRevaluationExedent) {
			if (!capitalIncreaseMotivelist.contains(revaluationExcedentMotive)) {
				capitalIncreaseMotivelist.add(revaluationExcedentMotive);
			}
		} else {
			if (capitalIncreaseMotivelist.contains(revaluationExcedentMotive)) {
				capitalIncreaseMotivelist.remove(revaluationExcedentMotive);
			}
		}

		// Fusion
		if (disableFusion) {
			if (!capitalIncreaseMotivelist.contains(fusionMotive)) {
				capitalIncreaseMotivelist.add(fusionMotive);
			}
		} else {
			if (capitalIncreaseMotivelist.contains(fusionMotive)) {
				capitalIncreaseMotivelist.remove(fusionMotive);
			}
		}
		// Others
		if (disableOthers) {
			if (!capitalIncreaseMotivelist.contains(othersMotive)) {
				capitalIncreaseMotivelist.add(othersMotive);
			}
		} else {
			if (capitalIncreaseMotivelist.contains(othersMotive)) {
				capitalIncreaseMotivelist.remove(othersMotive);
			}
		}

		// Capital Reexpresion
		if (disableCapitalReexpresion) {
			if (!capitalIncreaseMotivelist.contains(capitalReexpresionMotive)) {
				capitalIncreaseMotivelist.add(capitalReexpresionMotive);
			}
		} else {
			if (capitalIncreaseMotivelist.contains(capitalReexpresionMotive)) {
				capitalIncreaseMotivelist.remove(capitalReexpresionMotive);
			}
		}

		// Result inflation
		if (disableResultInflation) {
			if (!capitalIncreaseMotivelist.contains(resultExpositionMotive)) {
				capitalIncreaseMotivelist.add(resultExpositionMotive);
			}
		} else {
			if (capitalIncreaseMotivelist.contains(resultExpositionMotive)) {
				capitalIncreaseMotivelist.remove(resultExpositionMotive);
			}
		}

		// Voluntier Revaluation
		if (voluntierRevaluation) {
			if (!capitalIncreaseMotivelist.contains(voluntierRevaluationMotive)) {
				capitalIncreaseMotivelist.add(voluntierRevaluationMotive);
			}
		} else {
			if (capitalIncreaseMotivelist.contains(voluntierRevaluationMotive)) {
				capitalIncreaseMotivelist.remove(voluntierRevaluationMotive);
			}
		}


	}

	/**
	 * Metodo que calcula incremento dividendo de acciones
	 * 
	 * @param listIncreaseMotive
	 *            the list increase motive
	 * @return the big decimal
	 * @throws ServiceException
	 *             the service exception
	 */
	public BigDecimal calculateIncreaseAmount(List<CapitalIncreaseMotive> listIncreaseMotive)throws ServiceException {
		BigDecimal total = BigDecimal.ZERO;
		List<BigDecimal> bigDecimalList = new ArrayList<BigDecimal>();
		for (CapitalIncreaseMotive motive : listIncreaseMotive) {
			bigDecimalList.add(motive.getIncreaseQuantity());
		}
		total = CommonsUtilities.sumBigDecimalList(bigDecimalList);

		return total;
	}

	/**
	 * Metodo que inicia objetos de incremento dividendo de acciones
	 */
	public void initializeCapitalIncreaseMotive() {
		revaluationExcedentMotive = new CapitalIncreaseMotive();
		capitalReexpresionMotive = new CapitalIncreaseMotive();
		reserveCapitalizationMotive = new CapitalIncreaseMotive();
		utilitiesCapitalizationMotive = new CapitalIncreaseMotive();
		fusionMotive = new CapitalIncreaseMotive();
		resultExpositionMotive = new CapitalIncreaseMotive();
		othersMotive = new CapitalIncreaseMotive();
		voluntierRevaluationMotive = new CapitalIncreaseMotive(); 
		revaluationExcedentMotive.setIncreaseMotive(CapitalIncreaeMotiveType.REVALUATION_EXCEDENT.getCode());
		capitalReexpresionMotive.setIncreaseMotive(CapitalIncreaeMotiveType.CAPITAL_REEXPRESION.getCode());
		reserveCapitalizationMotive.setIncreaseMotive(CapitalIncreaeMotiveType.RESERVE_CAPITALIZATION.getCode());
		utilitiesCapitalizationMotive.setIncreaseMotive(CapitalIncreaeMotiveType.UTILITIES_CAPITALIZATION.getCode());
		fusionMotive.setIncreaseMotive(CapitalIncreaeMotiveType.FUSION.getCode());
		resultExpositionMotive.setIncreaseMotive(CapitalIncreaeMotiveType.EXPOSITION_RESULT.getCode());
		othersMotive.setIncreaseMotive(CapitalIncreaeMotiveType.OTHERS.getCode());
		voluntierRevaluationMotive.setIncreaseMotive(CapitalIncreaeMotiveType.VOLUNTIER_REVALUATION.getCode());

		if (isViewOperationDetail() || isViewOperationModify()) {
			if (ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType())) {
				this.disableMotiveIncreaseVar();
				List<CapitalIncreaseMotive> motivelIst = selectedCorporativeOpertation.getCapitalIncreaseMotives();
				for (CapitalIncreaseMotive motive : motivelIst) {
					if(CapitalIncreaeMotiveType.REVALUATION_EXCEDENT.getCode().equals(motive.getIncreaseMotive())){
						revaluationExcedentMotive = motive;
						disableRevaluationExedent = Boolean.TRUE;
					}					
					if(CapitalIncreaeMotiveType.CAPITAL_REEXPRESION.getCode().equals(motive.getIncreaseMotive())){
						disableCapitalReexpresion = Boolean.TRUE;
						capitalReexpresionMotive = motive;
					}
					if (CapitalIncreaeMotiveType.RESERVE_CAPITALIZATION.getCode().equals(motive.getIncreaseMotive())) {
						reserveCapitalizationMotive = motive;
						disableReserveCapitalization = Boolean.TRUE;
					}
					if (CapitalIncreaeMotiveType.UTILITIES_CAPITALIZATION.getCode().equals(motive.getIncreaseMotive())) {
						utilitiesCapitalizationMotive = motive;
						disableUitilitiesCapitalization = Boolean.TRUE;
					}
					if (CapitalIncreaeMotiveType.FUSION.getCode().equals(motive.getIncreaseMotive())) {
						disableFusion = Boolean.TRUE;
						fusionMotive = motive;
					}
					if (CapitalIncreaeMotiveType.EXPOSITION_RESULT.getCode().equals(motive.getIncreaseMotive())) {
						resultExpositionMotive = motive;
						disableResultInflation = Boolean.TRUE;
					}
					if (CapitalIncreaeMotiveType.OTHERS.getCode().equals(motive.getIncreaseMotive())) { 
						othersMotive = motive;
						disableOthers = Boolean.TRUE;
					}
					if (motive.getIncreaseMotive().equals(CapitalIncreaeMotiveType.VOLUNTIER_REVALUATION.getCode())) {
						voluntierRevaluationMotive = motive;
						voluntierRevaluation = Boolean.TRUE;
					}
				}

				try {
					increaseCapitalMotive = this.calculateIncreaseAmount(motivelIst);
				} catch (ServiceException e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}
			}
		}
	}

	/**
	 * Inicio de Clase administradora de la vista del hecho de importancia.
	 */
	@PostConstruct
	public void init() {
		
		parametersTableMap=new HashMap<Integer,Object>();
		parametersTableMapEvent=new HashMap<Integer,Object>();
		ParameterTableTO instrumentParamTo = new ParameterTableTO();
		instrumentParamTo.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		ParameterTableTO processStateTo = new ParameterTableTO();
		processStateTo.setMasterTableFk(MasterTableType.CORPORATIVE_PROCESS_STATE.getCode());
		processStateTo.setState(BooleanType.YES.getCode());
		corporativeOperationTO = new CorporativeOperationTO();
		corporativeOperationTO.setDeliveryDate(CommonsUtilities.currentDate());
		listCorporativeProcessStateType = this.getParameters(processStateTo);
		billParametersTableMap(listCorporativeProcessStateType);
		ParameterTableTO processStateToAux = new ParameterTableTO();
		processStateToAux.setMasterTableFk(MasterTableType.CORPORATIVE_EVENT.getCode());
		listCorporativeProcessEventType = this.getParameters(processStateToAux);
		billParametersTableMapEvent(listCorporativeProcessEventType);
		corporativeOperation = new CorporativeOperation();
		listInstrumentType = this.getParameters(instrumentParamTo);

		loadCorporativeEventsParams();
		
		issuerHelperSearch=new Issuer();
		securityHelperSearch=new Security();
		lstCboCorporativeEvenClass = new ArrayList<ParameterTable>();
		try {
			listHolidays();
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		requireTargetSecurity = "true";

	}

	/**
	 * Metodo que determina intervalo de fechas para la suscripcion.
	 */
	public void determineDaysBetweenDatesListener() {
		this.hideDialogs();
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			this.determineDaysBetweenDates(corporativeOperation);
		}
		if (isViewOperationModify()||isViewOperationDetail()) {
			this.determineDaysBetweenDates(selectedCorporativeOpertation);
		}
	}

	/**
	 * Metodo que retorna maximo dia como limite.
	 * 
	 * @return the date
	 */
	public Date maxDateListener() {
		if (corporativeOperation.getFirstStartDate() != null) {
			return CommonsUtilities.addDaysHabilsToDate(corporativeOperation.getFirstEndDate(), 1);
		}
		return null;

	}

	/**
	 * Metodo que determina rango de fechas.
	 * 
	 * @param operation
	 *            the operation
	 */
	private void determineDaysBetweenDates(CorporativeOperation operation) {
		if (Validations.validateIsNotNull(operation.getFirstStartDate())
				&& Validations.validateIsNotNull(operation.getFirstEndDate())) {			
			if(operation.getFirstEndDate().compareTo(operation.getFirstStartDate())==-1){
				corporativeOperation.setFirstStartDate(null);
			}else if(Validations.validateIsNotNull(operation.getSecondStartDate()) && 
					operation.getFirstEndDate().compareTo(operation.getSecondStartDate())==1){
				corporativeOperation.setSecondStartDate(null);
				corporativeOperation.setSecondEndDate(null);
				corporativeOperation.setThirdStartDate(null);
				corporativeOperation.setThirdEndDate(null);
			}else{
				Integer days = CommonsUtilities.getDaysBetween(
						operation.getFirstStartDate(), operation.getFirstEndDate());
				this.setNegotiationRank(days);	
			}					 
		} 
	}
	
	/**
	 * Metodo que valida inicio del segundo rango para la suscripcion.
	 */
	public void validateSecondEndDate(){
		this.hideDialogs();
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			this.validateSecondEndDateAux(corporativeOperation);
		}
		if (isViewOperationModify()||isViewOperationDetail()) {
			this.validateSecondEndDateAux(selectedCorporativeOpertation);
		}
	}
	
	/**
	 *  Metodo que valida fin del segundo rango para la suscripcion.
	 *
	 * @param operation the operation
	 */
	private void validateSecondEndDateAux(CorporativeOperation operation){
		if (Validations.validateIsNotNull(operation.getSecondStartDate())
				&& Validations.validateIsNotNull(operation.getSecondEndDate())) {			
			if(operation.getSecondEndDate().compareTo(operation.getSecondStartDate())==-1){
				corporativeOperation.setSecondStartDate(null);
			}else if(Validations.validateIsNotNull(operation.getThirdStartDate()) && 
					operation.getSecondEndDate().compareTo(operation.getThirdStartDate())==1){
				corporativeOperation.setThirdStartDate(null);
				corporativeOperation.setThirdEndDate(null);
			}				 
		} 
	}
	
	/**
	 *  Metodo que valida inicio del tercer rango para la suscripcion.
	 */
	public void validateThirdEndDate(){
		this.hideDialogs();
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			this.validateThirdEndDateAux(corporativeOperation);
		}
		if (isViewOperationModify()||isViewOperationDetail()) {
			this.validateThirdEndDateAux(selectedCorporativeOpertation);
		}
	}
	
	/**
	 *  Metodo que valida fin del tercer rango para la suscripcion.
	 *
	 * @param operation the operation
	 */
	private void validateThirdEndDateAux(CorporativeOperation operation){
		if (Validations.validateIsNotNull(operation.getThirdStartDate())
				&& Validations.validateIsNotNull(operation.getThirdEndDate())) {			
			if(operation.getThirdEndDate().compareTo(operation.getThirdStartDate())==-1){
				corporativeOperation.setThirdStartDate(null);
			}			 
		} 
	}
	/**
	 * Metodo que visualiza la confirmacion de la anulacion del hecho de importancia.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmBeforeAnnulate() {
		try {
			
			setViewOperationType(ViewOperationsType.ANULATE.getCode());
			if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
				if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType())||
						ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_ANNULATE_CORPORATE_EVENT));
					JSFUtilities.showSimpleValidationDialog();
				}
				else{
					this.loadSelectedCorporativeOperation();
					if (selectedCorporativeOpertation.getState().equals(CorporateProcessStateType.REGISTERED.getCode()) || 
							selectedCorporativeOpertation.getState().equals(CorporateProcessStateType.PRELIMINARY.getCode())|| 
							selectedCorporativeOpertation.getState().equals(CorporateProcessStateType.MODIFIED.getCode())) {
						
						loadDetailInfoListener();
						setViewOperationType(ViewOperationsType.ANULATE.getCode());
						return "detail";
					} else {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_REQUIRED_ANNULAR));
						JSFUtilities.showSimpleValidationDialog();
					}
	
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_WARNING_ACTION_MESSAGE));
				JSFUtilities.showSimpleValidationDialog();
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
		return "";
	}

	/**
	 * Metodo que acepta la confirmacion de la anulacion del hecho de importancia correctamente.
	 */
	public void confirmAnnulateOperation() {
		if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
			if (isViewOperationAnnul()) {
				Object[] bodyData = new Object[1];
				bodyData[0] = selectedCorporativeOpertation.getIdCorporativeOperationPk().toString();
				showMessageOnDialog(PropertiesConstants.IMPORTANCE_EVENT_ANNULAR_HEADER,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_ANNULATE,bodyData);
				JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmAnnulate').show()");										
			}
		}
	}

	/**
	 * Metodo que anula el hecho de importancia.
	 */
	@LoggerAuditWeb
	public void annulateCorporativeOperation() {
		JSFUtilities.hideComponent("importanceEventDetail:cnfConfirmAnnulate");
		if (isViewOperationAnnul()) {
			try {
				improtanceEventFacade.annulateCorporativeOperation(selectedCorporativeOpertation);
				Object[] bodyData = new Object[1];
				bodyData[0] = selectedCorporativeOpertation.getIdCorporativeOperationPk().toString();
				showMessageOnDialog(PropertiesConstants.IMPORTNACE_EVENT_EXIT_HEADER,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRMED_ANNULATE,bodyData);
		
				JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmedAnnulate').show()");
			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}

	/**
	 * Metodo de finalizacion de un evento.
	 * 
	 * @return the string
	 */
	public String finishAnnulate() {
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		this.search();
		return "search";
	}

	/**
	 * Metodo que visualiza la confirmacion de la modificacion del hecho de importancia.
	 * 
	 * @return the string
	 */
	public String confirmModifyOperation() {
		try {
			
			if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
				
				if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType())||
				   ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_MODIFY_CORPORATE_EVENT));
					JSFUtilities.showSimpleValidationDialog();
				}
				else{
					if (selectedCorporativeOpertation.getState().equals(CorporateProcessStateType.REGISTERED.getCode())
							|| selectedCorporativeOpertation.getState().equals(CorporateProcessStateType.PRELIMINARY.getCode())
							|| selectedCorporativeOpertation.getState().equals(CorporateProcessStateType.MODIFIED.getCode())) {
						this.determineDaysBetweenDatesListener();
	
						loadDetailInfoListener();
						setViewOperationType(ViewOperationsType.MODIFY.getCode());
						enableNegotiationDatesListener();
						return "modify";
					} else {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_VALIDATE_FOR_MODIFY));
						JSFUtilities.showSimpleValidationDialog();
					}
				}
	
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.LBL_WARNING_ACTION_MESSAGE));
				JSFUtilities.showSimpleValidationDialog();
	
			}

			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		return null;
	}

	/**
	 * Metodo que determina la descripcion del hecho de importancia.
	 *
	 * @throws ServiceException the service exception
	 */
	public void determineEventTypeDescription() throws ServiceException {
		if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
			if (Validations.validateIsNotNull(selectedCorporativeOpertation.getSecurities())) {
				intrumentTypeDescription =generalParameterFacade.getParamDetailServiceFacade(selectedCorporativeOpertation.getSecurities().getInstrumentType()).getParameterName();
			}
			
			ParameterTable prmTable=(ParameterTable)parametersTableMap.get( selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType() );
			billParameterTableById(prmTable.getRelatedParameter().getParameterTablePk());
			eventTypeDescription=parametersTableMap.get(prmTable.getRelatedParameter().getParameterTablePk()).toString();
	
		}
	}

	/**
	 * Metodo que deshabilita objetos dependiendo del evento que se realize.
	 */
	private void disableModifiyButton() {
		if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
			if (Validations.validateIsNotNull(selectedCorporativeOpertation.getState())) {
				if (selectedCorporativeOpertation.getState().equals(CorporateProcessStateType.REGISTERED.getCode())) {
					disableModifyButton = Boolean.FALSE;
					disableAnnularButton = Boolean.FALSE;
				} else if (selectedCorporativeOpertation.getState().equals(	CorporateProcessStateType.MODIFIED.getCode())) {
					disableModifyButton = Boolean.FALSE;
					disableAnnularButton = Boolean.FALSE;
				} else if (selectedCorporativeOpertation.getState().equals(CorporateProcessStateType.PRELIMINARY.getCode())) {
					disableModifyButton = Boolean.FALSE;
					disableAnnularButton = Boolean.FALSE;
				}
			}
		}
	}

	/**
	 * Metodo que oculta pantalla de motivo de incremento de dividendo en acciones.
	 */
	public void renderIncreaseCapitalMotiveListener() {
		executeAction();
		resetComponent();
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveIncrease').show();");
	}

	/**
	 * Metodo que limpia motivos de incremento de dividendo en acciones.
	 */
	public void addCapitalIncreaseMotiveListener() {
		if (isViewOperationRegister()) {
			this.clearCapitalIncreaseMotive(corporativeOperation);
		}
		if(isViewOperationModify()){
			this.clearCapitalIncreaseMotive(selectedCorporativeOpertation);
		}
	}	


	/**
	 * Metodo que limpia motivos de incremento de dividendo en acciones.
	 *
	 * @param operation the operation
	 */
	public void clearCapitalIncreaseMotive(CorporativeOperation operation){		
		if(!disableReserveCapitalization){
			reserveCapitalizationMotive.setIncreaseQuantity(null);
		}
		if(!disableRevaluationExedent){
			revaluationExcedentMotive.setIncreaseQuantity(null);
		}		
		if(!disableCapitalReexpresion){
			capitalReexpresionMotive.setIncreaseQuantity(null);
		}
		if(!disableUitilitiesCapitalization){
			utilitiesCapitalizationMotive.setIncreaseQuantity(null);
		}
		if(!disableFusion){
			fusionMotive.setIncreaseQuantity(null);
		}
		if(disableResultInflation){
			resultExpositionMotive.setIncreaseQuantity(null);
		}
		if(!disableOthers){
			othersMotive.setIncreaseQuantity(null);
		}
		if(voluntierRevaluation!=null){
			if(!voluntierRevaluation){
				if(voluntierRevaluationMotive!=null){
					voluntierRevaluationMotive.setIncreaseQuantity(null);
				}
			}
		}}

	/**
	 * Metodo que carga datos del valor seleccionado.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadSecuritiesInformation()throws ServiceException{
		if(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())  ||
				selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode()) ||
				selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())      ||
				selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())      ||
				selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())	   ||
				selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())   ||
				selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
				){
			sourceSecurity = allComponenteFacade.getSecurityHelpServiceFacade(selectedCorporativeOpertation.getSecurities().getIdSecurityCodePk());
			if(selectedCorporativeOpertation.getTargetSecurity()!=null){
				targeSecurity =allComponenteFacade.getSecurityHelpServiceFacade( selectedCorporativeOpertation.getTargetSecurity().getIdSecurityCodePk());
			}else{
				selectedCorporativeOpertation.setTargetSecurity(new Security());
			}
			sourceSecurity.setCorporativeSourceType(CorporativeSecuritySourceType.SOURCE.getCode());
			if(targeSecurity!=null){
				targeSecurity.setCorporativeSourceType(CorporativeSecuritySourceType.TARGET.getCode());
			}else {
				targeSecurity = new Security();
			}
			disableSourceSecuritiyHelper= Boolean.TRUE;
			if(!selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
				disableTargetSecurityHelper = Boolean.TRUE;
			}			
			disableAddButton = Boolean.TRUE;
			IssuerTO to =new IssuerTO();
			to.setIssuerCode(selectedCorporativeOpertation.getIssuer().getIdIssuerPk());
			issuer = to;			
		}
		else{			
			IssuerTO to =new IssuerTO();			
			to.setIssuerCode(selectedCorporativeOpertation.getIssuer().getIdIssuerPk());
			issuer = to;
		}
	}

	/**
	 * Metodo que carga datos del corporativo seleccionado.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public String loadDetailInfoListener() throws ServiceException{

		sourceSecurity = new Security();
		targeSecurity = new Security();
		this.loadSelectedCorporativeOperation();
				
		instrumentType = selectedCorporativeOpertation.getSecurities().getInstrumentType();
		instrumentTypeDescription = InstrumentType.get(instrumentType).getValue();
		
		//Event Type
		billParameterTableById(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType(),true);
					
		disableModifyButton = Boolean.TRUE;
		disableAnnularButton = Boolean.TRUE;

		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
			if (Validations.validateIsNotNull(selectedCorporativeOpertation.getCorporativeEventType())) {				
				SecurityTO filter = new SecurityTO();
				filter.setIdIssuerCodePk(selectedCorporativeOpertation.getIssuer().getIdIssuerPk());
				this.findTargetSecurities(filter);
				this.initializeParameters(null);
				this.disableModifiyButton();
				this.determineEventTypeDescription();
				this.enableNegotiationDatesListener();
				this.initializeCapitalIncreaseMotive();
				this.loadIndicatorValues();
				this.determineDaysBetweenDatesListener();				
				loadSecuritiesInformation();
				loadSecuritiesForExcision(selectedCorporativeOpertation);
				this.renderFields(selectedCorporativeOpertation.getCorporativeEventType());
				selectedCorporativeOpertation.setExchangeRelaSourceSecurity(selectedCorporativeOpertation.getOriginRatio());
				selectedCorporativeOpertation.setExchangeRelaTargetSecurity(selectedCorporativeOpertation.getTargetRatio());
				loadAmountCoupon(selectedCorporativeOpertation);
				findEventForPaymentRemanentEvent(selectedCorporativeOpertation);	
				findEventForContributionReturnEvent(selectedCorporativeOpertation);
				return "detail";
			}
		} else {
		}
		return "";
	}
	
	/**
	 * Metodo que carga monto cupon del interes o amortizacion.
	 *
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	public void loadAmountCoupon(CorporativeOperation operation)throws ServiceException{			
		if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
			List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramInterestCouponActual(operation.getSecurities().getIdSecurityCodePk(),operation.getDeliveryDate());
			Long idProgramInterestPk = (Long)lstIdProgramInterestCoupon.get(0);
			ProgramInterestCoupon objProgramInterestCoupon = (ProgramInterestCoupon)corporateExecutorService.getProgramInterestCoupon(idProgramInterestPk);
			operation.setAmountCoupon(objProgramInterestCoupon.getCouponAmount());
		}
		if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
			List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramAmortizationCoupon(operation.getSecurities().getIdSecurityCodePk(),operation.getDeliveryDate());
			Long idProgramAmortizationPk = (Long)lstIdProgramInterestCoupon.get(0);
			ProgramAmortizationCoupon objProgramAmortizationCoupon = (ProgramAmortizationCoupon)corporateExecutorService.getProgramAmortizationCoupon(idProgramAmortizationPk);
			operation.setAmountCoupon(objProgramAmortizationCoupon.getAmortizationAmount());
		}
	}
	
	/**
	 * Metodo que carga valores destino de la escision.
	 *
	 * @param operation the operation
	 * @throws ServiceException the service exception
	 */
	public void loadSecuritiesForExcision(CorporativeOperation operation)throws ServiceException{
		disabledForExcision = Boolean.FALSE;
		renderSecuritiesEsicion = Boolean.FALSE;
		renderTargetSecurity  = Boolean.FALSE;
		listSecuritiesTarget = new ArrayList<Security>();
		listSecuritiesTargetAux = new ArrayList<Security>();
		mapSecuritiesCorporative = new HashMap<String, SecuritiesCorporative>();
		securitiesCorporativeList = new ArrayList<SecuritiesCorporative>();
		if (operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
			modifyForExcision = Boolean.FALSE;
			renderSecuritiesEsicion = Boolean.TRUE;
			renderTargetSecurity  = Boolean.TRUE;
			List<SecuritiesCorporative> lstSecuritiesCorporative = improtanceEventFacade.getListSecuritiesCorporative(operation.getIdCorporativeOperationPk());
			if(lstSecuritiesCorporative!=null && lstSecuritiesCorporative.size()>0){				
				Security objSecurity = null;
				for (SecuritiesCorporative objSecuritiesCorporative : lstSecuritiesCorporative){
					objSecurity = new Security();
					objSecurity = allComponenteFacade.getSecurityHelpServiceFacade(objSecuritiesCorporative.getSecurity().getIdSecurityCodePk());
					objSecurity.setExchangeRelaSourceSecurity(objSecuritiesCorporative.getOriginRatio());
					objSecurity.setExchangeRelaTargetSecurity(objSecuritiesCorporative.getTargetRatio());
					objSecurity.setDeliveryFactorForCorporateOperation(objSecuritiesCorporative.getDeliveryFactor());
					objSecurity.setShareCapital(objSecuritiesCorporative.getOldShareCapital());
					objSecurity.setShareBalance(objSecuritiesCorporative.getOldShareBalance());
					objSecurity.setCirculationBalance(objSecuritiesCorporative.getOldCirculationBalance());
					objSecurity.setVariationAmount(objSecuritiesCorporative.getVariationAmount());
					objSecurity.setMarketDate(objSecuritiesCorporative.getMarketDate());
					objSecurity.setMarketPrice(objSecuritiesCorporative.getMarketPrice());
					objSecurity.setNewShareCapital(objSecuritiesCorporative.getNewShareCapital());
					objSecurity.setNewShareBalance(objSecuritiesCorporative.getNewShareBalance());
					objSecurity.setNewCirculationBalance(objSecuritiesCorporative.getNewCirculationBalance());
					listSecuritiesTarget.add(objSecurity);
					listSecuritiesTargetAux.add(objSecurity);
					securitiesCorporativeList.add(objSecuritiesCorporative);
					mapSecuritiesCorporative.put(objSecuritiesCorporative.getSecurity().getIdSecurityCodePk(),objSecuritiesCorporative);	
				}
			}			
		}
	}
	/**
	 * Metodo que carga datos del hecho de importancia seleccionado.
	 */
	@LoggerAuditWeb
	public void loadSelectedCorporativeOperation() {
		CorporativeOperationTO filterTO = new CorporativeOperationTO();

		String id =JSFUtilities.getRequestParameterMap(GeneralConstants.IMPORTANCE_EVENT_QUERY_STRING);
		if(Validations.validateIsNotNull(id)){
			filterTO.setId(Long.parseLong(id));			
		}
		else{
			filterTO.setId(selectedCorporativeOpertation.getIdCorporativeOperationPk());

		}

		filterTO.setNeedTargetSecurity(Boolean.FALSE);
		if (Validations.validateIsNotNull(selectedCorporativeOpertation.getTargetSecurity())) {
			filterTO.setNeedTargetSecurity(Boolean.TRUE);
		}
		try {
			selectedCorporativeOpertation = improtanceEventFacade.findCorporativeOperationByFilter(filterTO);
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Metodo que consulta hechos de importancia regsitrados.
	 */
	@LoggerAuditWeb
	public void search() {
		if (Validations.validateIsNotNull(corporativeOperationTO)) {
			try {
				/**Excluded DPF and DPA*/
				List<Integer> listSecurityClassExcluded=new ArrayList<Integer>();
				listSecurityClassExcluded.add(SecurityClassType.DPF.getCode());
				listSecurityClassExcluded.add(SecurityClassType.DPA.getCode());
				corporativeOperationTO.setListSecurityClassExcluded(listSecurityClassExcluded);
				listCorporativeOperation = improtanceEventFacade.findCorporativeOperationsByFilter(corporativeOperationTO);
				if (listCorporativeOperation.size() > 0) {
					renderDataTable = Boolean.TRUE;
				}
				renderFieldSet = Boolean.TRUE;
				corporativeOperationDataModel = new CorporativeOperationDataModel(listCorporativeOperation);
			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}

	}
	
	/**
	 * Metodo que actualiza indicador de escision.
	 */
	public void setIndForExcision(){
		if(isViewOperationModify()){
			modifyForExcision = Boolean.TRUE;
		}		
	}
	/**
	 * Metodo que limpia fechas de rango de una suscripcion si fecha de entrega cambio.
	 */
	public void cleanDateForDeliveryDate(){
		if(isViewOperationRegister()){
			if(corporativeOperation.getDeliveryDate()!=null){
				corporativeOperation.setMarketDate(corporativeOperation.getDeliveryDate());
				corporativeOperation.setMarketPrice(BigDecimal.ONE);
				execution();
			}
			if(corporativeOperation.getMarketDate()!=null){
				if(corporativeOperation.getMarketDate().compareTo(corporativeOperation.getDeliveryDate())==1){
					corporativeOperation.setMarketDate(null);
				}
			}
		}else{
			modifyForExcision = Boolean.TRUE;
			if(selectedCorporativeOpertation.getDeliveryDate()!=null){
				selectedCorporativeOpertation.setMarketDate(selectedCorporativeOpertation.getDeliveryDate());
				selectedCorporativeOpertation.setMarketPrice(BigDecimal.ONE);
				execution();
			}
			if(selectedCorporativeOpertation.getMarketDate()!=null){
				if(selectedCorporativeOpertation.getMarketDate().compareTo(selectedCorporativeOpertation.getDeliveryDate())==1){
					selectedCorporativeOpertation.setMarketDate(null);
				}
			}
		}
		if(renderPreferentSuscriptionPanel){
			if(isViewOperationRegister()){
				corporativeOperation.setFirstStartDate(null);
				corporativeOperation.setFirstEndDate(null);
				corporativeOperation.setSecondStartDate(null);
				corporativeOperation.setSecondEndDate(null);
				corporativeOperation.setThirdStartDate(null);
				corporativeOperation.setThirdEndDate(null);
			}else{
				selectedCorporativeOpertation.setFirstStartDate(null);
				selectedCorporativeOpertation.setFirstEndDate(null);
				selectedCorporativeOpertation.setSecondStartDate(null);
				selectedCorporativeOpertation.setSecondEndDate(null);
				selectedCorporativeOpertation.setThirdStartDate(null);
				selectedCorporativeOpertation.setThirdEndDate(null);
			}			
		}		
	}
	/**
	 * Metodo que deshabilita rangos dependiendo cantidad de rangos.
	 */
	public void enableNegotiationDatesListener() {
		if (isViewOperationRegister()) {
			if (Validations.validateIsNotNull(corporativeOperation.getNegotiationRange())) {
				this.enableNegotiationDates(corporativeOperation);
				this.hideDialogs();
				executeAction();
				resetComponent();
			}
		}
		if (isViewOperationModify()||isViewOperationDetail()) {
			if (Validations.validateIsNotNull(selectedCorporativeOpertation.getNegotiationRange())) {
				this.enableNegotiationDates(selectedCorporativeOpertation);
				this.hideDialogs();
				executeAction();
				resetComponent();
			}
		}

	}

	/**
	 * Metodo que deshabilita rangos dependiendo cantidad de rangos.
	 *
	 * @param operation the operation
	 */
	private void enableNegotiationDates(CorporativeOperation operation) {
		Long negotiationRange = operation.getNegotiationRange();
		if (Validations.validateIsNotNull(negotiationRange)) {
			if (negotiationRange.equals(NegotiationRankType.ONE.getCode())) {
				renderFirstNegotiationDate = Boolean.TRUE;
				renderSecondNegotiationDate = Boolean.FALSE;
				renderThirdNegotiationDate = Boolean.FALSE;
				operation.setSecondStartDate(null);
				operation.setSecondEndDate(null);
				operation.setThirdStartDate(null);
				operation.setThirdEndDate(null);
			} else if (negotiationRange.equals(NegotiationRankType.TWO.getCode())) {
				renderFirstNegotiationDate = Boolean.TRUE;
				renderSecondNegotiationDate = Boolean.TRUE;
				renderThirdNegotiationDate = Boolean.FALSE;
				operation.setThirdStartDate(null);
				operation.setThirdEndDate(null);
			} else if (negotiationRange.equals(NegotiationRankType.THREE.getCode())) {
				renderFirstNegotiationDate = Boolean.TRUE;
				renderSecondNegotiationDate = Boolean.TRUE;
				renderThirdNegotiationDate = Boolean.TRUE;
			} else {
				operation.setSecondStartDate(null);
				operation.setSecondEndDate(null);
				operation.setThirdStartDate(null);
				operation.setThirdEndDate(null);
				operation.setFirstEndDate(null);
				operation.setFirstStartDate(null);
				renderFirstNegotiationDate = Boolean.FALSE;
				renderSecondNegotiationDate = Boolean.FALSE;
				renderThirdNegotiationDate = Boolean.FALSE;
			}
		}

	}

	/**
	 * Metodo ue limpia todos los objetos.
	 */
	public void clearAll() {
		if (isViewOperationModify()) {
			this.search();
			clearSecurityHelper();

		}
		if (isViewOperationRegister()) {
			clearRegistry("importanceEventForm");
			listCorporativeEvents = new ArrayList<ParameterTable>();
			clearSecurityHelper();
			renderDataTable = Boolean.FALSE;
			corporativeOperationDataModel = new CorporativeOperationDataModel();
			setCorporativeOperationTO(new CorporativeOperationTO());
			this.init();
		}
	}

	/**
	 * Metodo que retorna la fecha actual del servidor.
	 * 
	 * @return the current date
	 */
	public Date getcurrentDate() {
		return CommonsUtilities.currentDate();
	}

	/**
	 * Metodo que actualiza indicador de pago de remanente.
	 */
	public void roundRemantenListener() {
		this.hideDialogs();
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			corporativeOperation.setIndRoundValue(Boolean.FALSE);
		} else if (isViewOperationModify()) {
			selectedCorporativeOpertation.setIndRoundValue(Boolean.FALSE);
		}
	}

	/**
	 * Metodo que actualiza indicador de redondeo.
	 */
	public void considerRoundListener() {
		this.hideDialogs();
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			corporativeOperation.setIndRemanentValue(Boolean.FALSE);
		} else if (isViewOperationModify()) {
			modifyForExcision = Boolean.TRUE;
			selectedCorporativeOpertation.setIndRemanentValue(Boolean.FALSE);
		}
	}

	/**
	 * Metodo que habilita/deshabilita indicador de impuesto.
	 */
	public void enableTaxFactorListener() {
		this.hideDialogs();
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			if (corporativeOperation.isIndTaxValue()) {
				renderTaxFactor = Boolean.TRUE;
				BigDecimal value = this.findTaxFactor();
				corporativeOperation.setTaxFactor(value);
			} else if (!corporativeOperation.isIndTaxValue()) {
				renderTaxFactor = Boolean.FALSE;
				corporativeOperation.setTaxFactor(null);
			}
		} else if (isViewOperationDetail() || isViewOperationModify()) {
			if (selectedCorporativeOpertation.isIndTaxValue()) {
				renderTaxFactor = Boolean.TRUE;
				BigDecimal value = this.findTaxFactor();
				selectedCorporativeOpertation.setTaxFactor(value);								
			} else if (!selectedCorporativeOpertation.isIndTaxValue()) {
				renderTaxFactor = Boolean.FALSE; 
				selectedCorporativeOpertation.setTaxFactor(null);
			}
		}
	}

	/**
	 * Metodo que consulta factor de impuesto (IVA)
	 *
	 * @return the big decimal
	 */
	public BigDecimal findTaxFactor(){
		BigDecimal tax = BigDecimal.ZERO;		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setState(ParameterTableStateType.REGISTERED.getCode());
		filter.setParameterTablePk(MasterTableType.PARAMETER_TABLE_IVA_TAX.getCode());
		List<ParameterTable> lstParameterTable= this.getParameters(filter);
		if(lstParameterTable!=null && lstParameterTable.size()>0){
			ParameterTable obj = (ParameterTable)lstParameterTable.get(0);
			if(obj.getIndicator1()!=null && 
					Validations.validateIsNotEmpty(obj.getIndicator1())){
				tax = new BigDecimal(obj.getIndicator1().toString());
			}
		}	
		return tax;
	}

	/**
	 * Metodo que registra hecho de importancia.
	 */
	@LoggerAuditWeb
	public void save() {
		this.settingIndicatorsValues();

		JSFUtilities.hideComponent("importanceEventForm:cnfBeforeSave");
		if (isViewOperationModify()) {
			if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
				JSFUtilities.hideComponent("importanceEventForm:cnfBeforeSave");
				try {
					if(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
						selectedCorporativeOpertation.setCapitalIncreaseMotives(capitalIncreaseMotivelist);
					}else if(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){						
						selectedCorporativeOpertation.setTargetSecurity(new Security());
						selectedCorporativeOpertation.setSecuritiesCorporatives(securitiesCorporativeList);
						this.deleteSecurityInfo(selectedCorporativeOpertation,null);
					}
					improtanceEventFacade.modifiyCorporativeOperation(selectedCorporativeOpertation);
					selectedCorporativeOpertation.setTargetSecurity(new Security());
					increaseCapitalMotive=null;
					Object[] bodyData = new Object[1];
					bodyData[0] = selectedCorporativeOpertation.getIdCorporativeOperationPk().toString();
					showMessageOnDialog(null, null,PropertiesConstants.IMPORTANCE_EVENT_MODIFIED,bodyData);
					JSFUtilities.showComponent("importanceEventForm:cnfSave");
				} catch (ServiceException e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}
			}
		} else {

			if (Validations.validateIsNotNull(corporativeOperation)) {
				try {
					if(corporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
						corporativeOperation.setCapitalIncreaseMotives(capitalIncreaseMotivelist);
					}
					else if(corporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
						this.deleteSecurityInfo(corporativeOperation,null);
						corporativeOperation.setTargetSecurity(new Security());
						corporativeOperation.setSecuritiesCorporatives(securitiesCorporativeList);
					}					
					improtanceEventFacade.registerCorporativeOperation(corporativeOperation);
					corporativeOperation.setTargetSecurity(new Security());
					Object[] bodyData = new Object[1];
					bodyData[0] = String.valueOf(corporativeOperation.getIdCorporativeOperationPk());
					showMessageOnDialog(PropertiesConstants.IMPORTNACE_EVENT_EXIT_HEADER,null,PropertiesConstants.IMPORTANCE_EVENT_REGISTERED,bodyData);
					JSFUtilities.showComponent("importanceEventForm:cnfSave");
				} catch (ServiceException e) {
					if(e.getErrorService().equals(ErrorServiceType.CONCURRENCY_LOCK_UPDATE)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_CONCURRENCY));
						JSFUtilities.showComponent("importanceEventForm:cnfBacTransaction");

					}else if(e.getErrorService().equals(ErrorServiceType.CORPORATIVE_PROCESS_PAYMENT_REMANENT_EXIST)){
						Object[] bodyData = new Object[1];						 
						bodyData[0] = String.valueOf(importanceEvent.getIdCorporativeOperationPk());
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),null, PropertiesConstants.IMPORTANCE_EVENT_PAYMENT_REMANENET_EXIST,bodyData);
						JSFUtilities.showComponent("importanceEventForm:cnfBacTransaction");
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getExceptionMessage(e.getMessage()));
						JSFUtilities.showComponent("importanceEventForm:cnfBacTransaction");
					}

				}


			}
		}
	}

	/**
	 * Metodo que limpia valores destino de la escision.
	 */
	public void setSecuritiesCorporative(){
		List<SecuritiesCorporative> securitiesCorporativesList = new ArrayList<SecuritiesCorporative>();
		SecuritiesCorporative securitieCorporative;

		if(isViewOperationRegister()){
			corporativeOperation.setSecuritiesCorporatives(securitiesCorporativesList);
			corporativeOperation.setTargetSecurity(new Security());
		}
	}

	/**
	 * Metodo que actualiza indicadores que existen en el hecho de importancia.
	 */
	public void settingIndicatorsValues() {
		// Determine if the remante value is true

		if (isViewOperationRegister()) {
			if (corporativeOperation.isIndRemanentValue()) {
				corporativeOperation.setIndRemanent(BooleanType.YES.getCode());
			} else {
				corporativeOperation.setIndRemanent(BooleanType.NO.getCode());
			}
			// Determine if round value is true
			if (corporativeOperation.isIndRoundValue()) {
				corporativeOperation.setIndRound(BooleanType.YES.getCode());
			} else {
				corporativeOperation.setIndRound(BooleanType.NO.getCode());
			}
			// Determine if tax value is true
			if (corporativeOperation.isIndTaxValue()) {
				corporativeOperation.setIndTax(BooleanType.YES.getCode());
			} else {
				corporativeOperation.setIndTax(BooleanType.NO.getCode());
			}
		}

		else if (isViewOperationModify()) {
			if (selectedCorporativeOpertation.isIndRemanentValue()) {
				selectedCorporativeOpertation.setIndRemanent(BooleanType.YES.getCode());
			} else {
				selectedCorporativeOpertation.setIndRemanent(BooleanType.NO.getCode());
			}
			// Determine if round value is true
			if (selectedCorporativeOpertation.isIndRoundValue()) {
				selectedCorporativeOpertation.setIndRound(BooleanType.YES.getCode());
			} else {
				selectedCorporativeOpertation.setIndRound(BooleanType.NO.getCode());
			}
			// Determine if tax value is true
			if (selectedCorporativeOpertation.isIndTaxValue()) {
				selectedCorporativeOpertation.setIndTax(BooleanType.YES.getCode());
			} else {
				selectedCorporativeOpertation.setIndTax(BooleanType.NO.getCode());
			}
		}

	}

	/**
	 * Metodo que actualiza indicadores que existen en el hecho de importancia.
	 */
	public void loadIndicatorValues() {
		if (isViewOperationModify() || isViewOperationDetail()) {
			if (selectedCorporativeOpertation.getIndRemanent() != null) {
				if (selectedCorporativeOpertation.getIndRemanent().equals(BooleanType.YES.getCode())) {
					selectedCorporativeOpertation.setIndRemanentValue(Boolean.TRUE);
				} else {
					selectedCorporativeOpertation.setIndRemanentValue(Boolean.FALSE);
				}
			}
			if (selectedCorporativeOpertation.getIndRound() != null) {
				if (selectedCorporativeOpertation.getIndRound().equals(BooleanType.YES.getCode())) {
					selectedCorporativeOpertation.setIndRoundValue(Boolean.TRUE);
				} else {
					selectedCorporativeOpertation.setIndRoundValue(Boolean.FALSE);
				}
			}

			if (selectedCorporativeOpertation.getIndTax() != null) {
				if (selectedCorporativeOpertation.getIndTax().equals(BooleanType.YES.getCode())) {
					selectedCorporativeOpertation.setIndTaxValue(Boolean.TRUE);
				} else {
					selectedCorporativeOpertation.setIndTaxValue(Boolean.FALSE);
				}
			}
		}
	}

	/**
	 * Metodo que visualiza la confirmacion de registro del hecho de importancia.
	 */
	public void confirmBeforeSave() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("importanceEventForm:cnfBeforeSave");
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);

		Object[] bodyData = new Object[1];
		CorporativeOperation cOperation=null;
		
		if(isViewOperationRegister()){
			cOperation=corporativeOperation;
		}else if(isViewOperationModify()){
			cOperation=selectedCorporativeOpertation;
		}
		if(Validations.validateIsNotNull(cOperation) && cOperation.getCorporativeEventType().getCorporativeEventType().equals( ImportanceEventType.ACTION_DIVIDENDS.getCode() )){
			
			showMessageOnDialog(null,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MESSAGE,null);
			JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
			/**
			 * QUitar la validacion a pedido de cinthya
			 */
//			if(capitalIncreaseMotivelist.size()<=0){
//				showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_ALERT_HEADER),
//						PropertiesUtilities.getGenericMessage(PropertiesConstants.IMPORTANCE_EVENT_REQUIRED_DATA));
//				JSFUtilities.showSimpleValidationDialog();
//				JSFUtilities.addContextMessage("importanceEventForm:motiveIncrease",
//						FacesMessage.SEVERITY_INFO, " ", " ");
//			}else{
//				if (!validateIncreaseCapitalAmount(cOperation)) {
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
//							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_VALIDATE_INCREASE_CAPITAL));
//					JSFUtilities.showSimpleValidationDialog();
//				}else{					
//					if(isViewOperationRegister()){
//						
//						showMessageOnDialog(null,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MESSAGE,null);
//						JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
//					}else if(isViewOperationModify()){
//						bodyData[0] = cOperation.getIdCorporativeOperationPk().toString();
//						showMessageOnDialog(PropertiesConstants.IMPORTANCE_EVENT_MODIFY_HEADER,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MODIFIY,bodyData);
//						JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
//					}
//				}
//			}
		}else{
			
			if (Validations.validateIsNotNull(cOperation) && (cOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode()) || 
					cOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode()))) {
				if (Validations.validateIsNotNull(importanceEvent)) {
					if (Validations.validateIsNotNull(importanceEvent.getIdCorporativeOperationPk())) {
						if(cOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())){
							if(validateDateForPaymentRemanent(cOperation)){
								return;
							}							
						}else if(cOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())){
							if(validateDateForReturnContribution(cOperation)){
								return;
							}	
						}
						this.settingIndicatorsValues();
						showMessageOnDialog(null,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MESSAGE,null);
						
						if(isViewOperationRegister()){
							JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
						}else if(isViewOperationModify()){
							bodyData[0] = cOperation.getIdCorporativeOperationPk().toString();
							showMessageOnDialog(PropertiesConstants.IMPORTANCE_EVENT_MODIFY_HEADER,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MODIFIY,bodyData);
							JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
						}
					}
				}
			} else {
				if(Validations.validateIsNotNull(cOperation) && cOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
					if(validateSecurities()){
						if(validateMinTargetSecurities()){
							this.settingIndicatorsValues();
							showMessageOnDialog(null,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MESSAGE,null);
							if(isViewOperationRegister()){
								JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
							}else if(isViewOperationModify()){
								if(modifyForExcision){
									bodyData[0] = cOperation.getIdCorporativeOperationPk().toString();
									showMessageOnDialog(PropertiesConstants.IMPORTANCE_EVENT_MODIFY_HEADER,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MODIFIY,bodyData);
									JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
								}else{
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
											PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_EVENT_VALIDATE_MODIFY));
									JSFUtilities.showSimpleValidationDialog();
								}								
							}							
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_EVENT_VALIDATE_MIN_TARGET_SECURITY));
							JSFUtilities.showSimpleValidationDialog();
						}						
					}
					else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getGenericMessage(PropertiesConstants.IMPORTANCE_EVENT_REQUIRED_DATA));
						JSFUtilities.showSimpleValidationDialog();

					}
				}
				else{
					showMessageOnDialog(null,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MESSAGE,null);
					if(isViewOperationRegister()){
						JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
					}else if(isViewOperationModify()){
						bodyData[0] = cOperation.getIdCorporativeOperationPk().toString();
						showMessageOnDialog(PropertiesConstants.IMPORTANCE_EVENT_MODIFY_HEADER,null,PropertiesConstants.IMPORTANCE_EVENT_CONFIRM_MODIFIY,bodyData);
						JSFUtilities.showComponent("importanceEventForm:cnfBeforeSave");
					}
				}
			}
			
		}
	}
	
	/**
	 * Metodo que valida minimo de valores destino para una escision.
	 *
	 * @return true, if successful
	 */
	public boolean validateMinTargetSecurities(){
		CorporativeOperation corporativeOp=null;
		if(isViewOperationRegister()){
			corporativeOp=corporativeOperation;
		}else if(isViewOperationModify()){
			corporativeOp=selectedCorporativeOpertation;
		}
		if(Validations.validateIsNotNull(corporativeOp) && corporativeOp.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
			if(securitiesCorporativeList == null || (securitiesCorporativeList!=null && securitiesCorporativeList.size()<2)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Metodo que valida valores destino de una escision.
	 *
	 * @return true, if successful
	 */
	public boolean validateSecurities(){
		CorporativeOperation corporativeOp=null;
		if(isViewOperationRegister()){
			corporativeOp=corporativeOperation;
		}else if(isViewOperationModify()){
			corporativeOp=selectedCorporativeOpertation;
		}

		if(Validations.validateIsNotNull(corporativeOp) && corporativeOp.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
			List<SecuritiesCorporative> list =new ArrayList<SecuritiesCorporative>(mapSecuritiesCorporative.values());
			if(list!=null && list.size()>0){
				for(SecuritiesCorporative sec: list){
					sec.setCorporativeOperation(corporativeOp);
					if(sec.getNewCirculationBalance()==null||
							sec.getNewShareBalance()==null||
							sec.getNewShareCapital()==null||
							sec.getDeliveryFactor()==null
							|| sec.getMarketDate()==null
							|| sec.getMarketPrice()==null){
						return false;
					}

				}
			}else{
				return false;
			}			

			corporativeOp.setSecuritiesCorporatives(list);
			corporativeOp.setTargetSecurity(new Security());
			corporativeOp.setSecurities(sourceSecurity);
			return true;

		}


		return false;

	}

	/**
	 * Metodo que valida existencia de un dividendo en acciones en estado definitivo para asociarlo a un pago de remanente.
	 * 
	 * @param operation
	 *            the operation
	 */
	private void findEventForPaymentRemanentEvent(CorporativeOperation operation) {

		if (operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())) {
			CorporativeOperationTO to = new CorporativeOperationTO();
			to.setSourceSecurityCode(operation.getSecurities().getIdSecurityCodePk());
			to.setState(CorporateProcessStateType.DEFINITIVE.getCode());
			to.setIndRemanent(BooleanType.YES.getCode());
			to.setCorporativeEvent(ImportanceEventType.ACTION_DIVIDENDS.getCode());
			try {
				CorporativeOperation operati = improtanceEventFacade.findCorporativeOperationByFilter(to);
				if (Validations.validateIsNotNull(operati)) {
					importanceEvent = operati;
				} else {
					importanceEvent = new CorporativeOperation();
					operation.setSecurities(new Security());
					securitySourceHelperMgmt=new Security();
					if (isViewOperationRegister()) {						
						JSFUtilities.resetComponent("importanceEventForm:opnlSecurityHelper");;
					}
					if (isViewOperationModify()) {
						JSFUtilities.resetComponent("importanceEventForm:helpSec:securityCodeHelpersourceSecurityHelper");
					}
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_PAYMENT_REMANENT));
					JSFUtilities.showSimpleValidationDialog();
					return ;
				}

			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));

			}
		}
	}
	/**
	 * Metodo que valida existencia de una reduccion de capital o exclusion de valores en estado definitivo para asociarlo a una devolucion de aportes.
	 * 
	 * @param operation
	 *            the operation
	 */
	private void findEventForContributionReturnEvent(CorporativeOperation operation) {

		if (operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())) {
			CorporativeOperationTO to = new CorporativeOperationTO();
			to.setSourceSecurityCode(operation.getSecurities().getIdSecurityCodePk());
			to.setState(CorporateProcessStateType.DEFINITIVE.getCode());
			to.setIndRemanent(BooleanType.NO.getCode());
			to.setCorporativeEvent(ImportanceEventType.CAPITAL_REDUCTION.getCode());
			try {
				CorporativeOperation operati = improtanceEventFacade.findCorporativeOperationByFilter(to);
				if (Validations.validateIsNotNull(operati)) {
					importanceEvent = operati;
				} else {
					to.setCorporativeEvent(ImportanceEventType.SECURITIES_EXCLUSION.getCode());
					operati = improtanceEventFacade.findCorporativeOperationByFilter(to);
					if (Validations.validateIsNotNull(operati)) {
						importanceEvent = operati;
					}else{
						importanceEvent = new CorporativeOperation();
						operation.setSecurities(new Security());
						securitySourceHelperMgmt=new Security();
						if (isViewOperationRegister()) {						
							JSFUtilities.resetComponent("importanceEventForm:opnlSecurityHelper");;
						}
						if (isViewOperationModify()) {
							JSFUtilities.resetComponent("importanceEventForm:helpSec:securityCodeHelpersourceSecurityHelper");
						}
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_PAYMENT_RETURN));
						JSFUtilities.showSimpleValidationDialog();
						return ;
					}					
				}

			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));

			}
		}
	}
	/**
	 * Metodo que determina nueva cantidad de valores.
	 */
	public void determineNewShareBalanceListener() {
		if (isViewOperationRegister()) {
			this.determineNewShareBalance(corporativeOperation);
		}
		if (isViewOperationModify()) {
			this.determineNewShareBalance(selectedCorporativeOpertation);
		}
	}

	/**
	 * Metodo que determina nueva cantidad de valores.
	 * 
	 * @param operation
	 *            the operation
	 */
	private void determineNewShareBalance(CorporativeOperation operation) {
		if (Validations.validateIsNotNull(operation.getTargetSecurity())) {
			if (Validations.validateIsNotNull(operation.getOldNominalValue())) {
				if (Validations.validateIsNotNull(operation.getNewShareCapital())) {
					BigDecimal newShareBalance = operation.getNewShareCapital().divide(operation.getOldNominalValue(), 2,RoundingMode.DOWN);
					operation.setNewShareBalance(newShareBalance);
					BigDecimal shareCapitalVariation = operation.getOldShareCapital().subtract(operation.getOldShareCapital());
					operation.setVariationAmount(shareCapitalVariation);
				}
			}
		}
	}

	/**
	 * Metodo que determina factor de entrega.
	 */
	public void determineDeliveryFactorListener() {	
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			this.determineDeliveryFactor(corporativeOperation);
		}
		if (isViewOperationModify()) {
			this.determineDeliveryFactor(selectedCorporativeOpertation);
		}
	}
	
	/**
	 * Metodo que determina cantidad de valores.
	 */
	public void determineShareBalanceListener(){
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			this.determineShareBalance(corporativeOperation);
		}
		if (isViewOperationModify()) {
			this.determineShareBalance(selectedCorporativeOpertation);
		}
	}
	
	/**
	 * Metodo que determina cantidad de valores.
	 *
	 * @param operation operation
	 */
	public void determineShareBalance(CorporativeOperation operation){
		if(operation.getDeliveryFactor()!=null &&
				operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
			BigDecimal factor = operation.getDeliveryFactor().divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
			BigDecimal shareCapitalAux = operation.getOldShareCapital().multiply(factor);
			operation.setNewShareCapital(operation.getOldShareCapital().add(shareCapitalAux));
			this.calculateShareCapitalVariation(operation);
			operation.setNewCirculationBalance(operation.getNewShareBalance());
		}		
	}
	/**
	 * Metodo que determina factor de entrega.
	 * 
	 * @param operation
	 *            the operation
	 */
	private void determineDeliveryFactor(CorporativeOperation operation) {
		this.hideDialogs();
		BigDecimal factorPercent = new BigDecimal(100);
		operation.setDeliveryFactor(null);
		if(Validations.validateIsNotNull(operation.getCorporativeEventType().getCorporativeEventType())){
			// Determine which event type is selected
			if (operation.getCorporativeEventType().getCorporativeEventType().equals(	ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode()) ||  
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())) {
				if(operation.getSecurities()!=null){

					// /Validate not null the current nominal value of the Security
					if (Validations.validateIsNotNull(operation.getSecurities().getCurrentNominalValue()) && operation.getNewNominalValue()!=null) {
						BigDecimal temp = ((factorPercent.multiply(operation.getSecurities().getCurrentNominalValue())).divide(operation.getNewNominalValue(), 8,	RoundingMode.DOWN));
						operation.setOriginRatio(operation.getExchangeRelaSourceSecurity());
						operation.setTargetRatio(operation.getExchangeRelaTargetSecurity());
						BigDecimal newShareCapital = null;
						BigDecimal newShareBalance = null;
						BigDecimal newCirculationBalance = null;
						if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())){
							operation.setDeliveryFactor(new BigDecimal(100));
							newShareBalance = operation.getOldShareBalance();
							newShareCapital = newShareBalance.multiply( operation.getNewNominalValue());
							newCirculationBalance = operation.getOldShareBalance();
							operation.setNewShareCapital(newShareCapital);
							operation.setNewShareBalance(operation.getOldShareBalance());
							operation.setNewCirculationBalance(newCirculationBalance);							
						}else{
							operation.setDeliveryFactor(temp);		
							operation.setNewShareCapital(operation.getSecurities().getShareCapital());
							newShareBalance = operation.getNewShareCapital().divide(operation.getNewNominalValue(), 0,	RoundingMode.DOWN);
							operation.setNewShareBalance(newShareBalance);
							operation.setNewCirculationBalance(newShareBalance);
						}			
					}else{
						if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())){
							operation.setNewShareCapital(null);
							operation.setNewShareBalance(null);
							operation.setNewCirculationBalance(null);
						}else{
							operation.setNewShareCapital(null);
							operation.setNewCirculationBalance(null);
						}
					}
				}
				// Event change nominal value with no capital variation
				// Determine share balance variation
			}
			
			if (operation.getCorporativeEventType().getCorporativeEventType().equals(	ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())){
				if (Validations.validateIsNotNull(operation.getSecurities().getShareBalance())) {
					if (Validations.validateIsNotNull(operation.getNewShareBalance())) {
						BigDecimal temp =operation.getNewShareBalance().subtract(operation.getSecurities().getShareBalance()).setScale(0,RoundingMode.DOWN);
						operation.setSecuritiesVariationAmount(temp);
					}
				}
			}
			if (operation.getCorporativeEventType().getCorporativeEventType().equals(	ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())){
				if (Validations.validateIsNotNull(operation.getSecurities().getShareCapital())) {
					if (Validations.validateIsNotNull(operation.getNewShareCapital())) {
						BigDecimal temp = operation.getNewShareCapital().subtract(operation.getSecurities().getShareCapital()).setScale(2,RoundingMode.DOWN);
						operation.setVariationAmount(temp);
					}
				}
			}
			if (operation.getCorporativeEventType().getCorporativeEventType().equals(	ImportanceEventType.SECURITIES_FUSION.getCode())|| 
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode()) ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())) {
				if (Validations.validateIsNotNull(operation.getExchangeRelaSourceSecurity())) {
					if (Validations.validateIsNotNull(operation.getExchangeRelaTargetSecurity())) {
						if(operation.getExchangeRelaTargetSecurity().compareTo(BigDecimal.ZERO)==1 
								&& operation.getExchangeRelaSourceSecurity().compareTo(BigDecimal.ZERO)==1){
							operation.setOriginRatio(operation.getExchangeRelaSourceSecurity());
							operation.setTargetRatio(operation.getExchangeRelaTargetSecurity());
							BigDecimal temp = (factorPercent.multiply(operation.getExchangeRelaTargetSecurity())).divide(operation.getExchangeRelaSourceSecurity(), 8,	RoundingMode.DOWN);
							operation.setDeliveryFactor(temp);
							BigDecimal factor = operation.getDeliveryFactor().divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
							if(!operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())){
								if(operation.getOldShareCapital()!=null){
									BigDecimal shareCapitalAux = operation.getOldShareCapital().multiply(factor);
									operation.setNewShareCapital(shareCapitalAux);
									this.calculateShareCapitalVariation(operation);
									operation.setNewCirculationBalance(operation.getNewShareBalance());
								}								
							}else{
								operation.setNewShareCapital(BigDecimal.ZERO);
								this.calculateShareCapitalVariation(operation);
								operation.setNewCirculationBalance(BigDecimal.ZERO);
							}
						}
					}
				}
			}
			if (operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())) {
				if (Validations.validateIsNotNull(operation.getExchangeRelaSourceSecurity())) {
					if (Validations.validateIsNotNull(operation.getExchangeRelaTargetSecurity())) {
						if(operation.getExchangeRelaSourceSecurity().compareTo(BigDecimal.ZERO)==1){
							if(operation.getExchangeRelaTargetSecurity().compareTo(BigDecimal.ZERO)==1){
								if(operation.getExchangeRelaTargetSecurity().compareTo(operation.getExchangeRelaSourceSecurity())==1){
									operation.setExchangeRelaSourceSecurity(null);
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
											PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_TARGET_EXCHANGE));
									JSFUtilities.showSimpleValidationDialog();
								}
								else{
									operation.setOriginRatio(operation.getExchangeRelaSourceSecurity());
									operation.setTargetRatio(operation.getExchangeRelaTargetSecurity());
									BigDecimal temp = (factorPercent.multiply(operation.getExchangeRelaTargetSecurity())).divide(operation.getExchangeRelaSourceSecurity(), 8,	RoundingMode.DOWN);
									operation.setDeliveryFactor(temp);
									BigDecimal factor = operation.getDeliveryFactor().divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
									operation.setNewShareCapital(operation.getOldShareCapital().multiply(factor));
									this.calculateShareCapitalVariation(operation);
									operation.setNewCirculationBalance(operation.getNewShareBalance());
								}
							}
						}
					}
				}

			}
			if (operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())) {
				if(Validations.validateIsNotNull(operation)){
					if (Validations.validateIsNotNull(operation.getExchangeRelaSourceSecurity())) {
						if (Validations.validateIsNotNull(operation.getExchangeRelaTargetSecurity())) {							
							if(operation.getExchangeRelaTargetSecurity().compareTo(BigDecimal.ZERO)==1 
									&& operation.getExchangeRelaSourceSecurity().compareTo(BigDecimal.ZERO)==1){
								operation.setOriginRatio(operation.getExchangeRelaSourceSecurity());
								operation.setTargetRatio(operation.getExchangeRelaTargetSecurity());
								BigDecimal temp = (factorPercent.multiply(operation.getExchangeRelaTargetSecurity())).divide(operation.getExchangeRelaSourceSecurity(), 8,	RoundingMode.DOWN);
								operation.setDeliveryFactor(temp);
								BigDecimal factor = operation.getDeliveryFactor().divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
								if(operation.getSecurities()!=null){
									if(operation.getSecurities().getShareCapital()!=null && operation.getOldShareCapital()!=null){
										BigDecimal shareCapitalAux = operation.getSecurities().getShareCapital().multiply(factor);
										operation.setNewShareCapital(operation.getOldShareCapital().add(shareCapitalAux));
										this.calculateShareCapitalVariation(operation);
										operation.setNewCirculationBalance(operation.getNewShareBalance());
									}
								}								
							}								
						}
					}
				}

			}
		}
	}

	/**
	 * Metodo que calcula variacion de capital.
	 * 
	 * @param operation
	 *            the operation
	 */
	private void calculateShareCapitalVariation(CorporativeOperation operation) {
		this.hideDialogs();
		if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
			BigDecimal temp = null;
			if(Validations.validateIsNotNull(operation)){
				if (Validations.validateIsNotNullAndNotEmpty(operation.getOldShareCapital())) {
					if (Validations.validateIsNotNullAndNotEmpty(operation.getNewShareCapital())) {
						temp = operation.getNewShareCapital().subtract(operation.getOldShareCapital());
						operation.setVariationAmount(temp);

						BigDecimal shareBalance=null;
						shareBalance = operation.getNewShareCapital().divide(operation.getTargetSecurity().getCurrentNominalValue(), 0,
								RoundingMode.DOWN);
						operation.setNewShareBalance(shareBalance);
					}
				}
			}

		}else{
			BigDecimal temp = null;
			if (Validations.validateIsNotNullAndNotEmpty(operation.getOldShareCapital())) {
				if (Validations.validateIsNotNullAndNotEmpty(operation.getNewShareCapital())) {
					temp = operation.getNewShareCapital().subtract(	operation.getOldShareCapital());
					operation.setVariationAmount(temp);
				}
			}

			if (operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())             ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())     ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())) {
				if (Validations.validateIsNotNull(operation.getOldNominalValue())) {
					if (Validations.validateIsNotNull(operation.getNewShareCapital())) {
						operation.setNewShareBalance(operation.getNewShareCapital().divide(operation.getOldNominalValue(), 0,
								RoundingMode.DOWN));
					}
				}
			}

			if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())){
				if(operation.getOldShareCapital()!=null &&operation.getNewShareCapital()!=null){
					if(operation.getNewShareCapital().compareTo(operation.getOldShareCapital())==1){
						showMessageOnDialog("Mensaje de Advertencia", "Nuevo Capital Contable debe de ser menor al capital Contable. Verifique");
						operation.setNewShareCapital(null);
						JSFUtilities.showSimpleValidationDialog();
						operation.setVariationAmount(null);
					}else{
						operation.setNewShareBalance(operation.getNewShareCapital().divide(operation.getOldNominalValue(), 0,
								RoundingMode.DOWN));

					}

				}
			}

		}
	}

	/**
	 * Metodo que valida nuevo valores en circulacion.
	 *
	 * @param corporativeOperation the corporative operation
	 */
	public void validateNewCirculationBalanace(CorporativeOperation corporativeOperation){
		if(corporativeOperation.getNewCirculationBalance()!=null){
			BigDecimal temp = BigDecimal.ZERO;

			if(temp.compareTo(corporativeOperation.getNewCirculationBalance())==0){
				corporativeOperation.setNewCirculationBalance(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_CIRCUlATION_BALANCE));
				JSFUtilities.showSimpleValidationDialog();
				return;

			}else{
				if(Validations.validateIsNotNull(corporativeOperation.getNewShareBalance())){
					if(corporativeOperation.getNewCirculationBalance().compareTo(corporativeOperation.getNewShareBalance())==1){
						corporativeOperation.setNewCirculationBalance(null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_CIRCUlATION));
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}
			}	
		}
	}

	/**
	 * Metodo que valida nuevo monto en circulacion.
	 */
	public void validateNewCirculationBalanceAmountListener(){
		this.hideDialogs();
		executeAction();
		resetComponent();
		this.hideDialogs();
		if(isViewOperationRegister()){
			this.validateNewCirculationBalanace(corporativeOperation);
		}
		if(isViewOperationModify()){
			this.validateNewCirculationBalanace(selectedCorporativeOpertation);
		}


	}

	/**
	 * Execution.
	 */
	public void execution(){
		this.hideDialogs();
		executeAction();
		resetComponent();
	}
	/**
	 * Metood que calcula variacion de capital.
	 */
	public void calculateShareCapitalVariationListener() {
		this.hideDialogs();
		executeAction();
		resetComponent();
		// Calculate for the event Change Nominal Value with Capital Variation
		if (isViewOperationRegister()) {	
			if(Validations.validateIsNotNull(corporativeOperation.getNewShareCapital()) && Validations.validateIsNotNullAndPositive(corporativeOperation.getNewShareCapital())
					&&Validations.validateIsNotNull(corporativeOperation.getOldShareCapital()) &&Validations.validateIsNotNullAndPositive(corporativeOperation.getOldShareCapital())){
				this.calculateShareCapitalVariation(corporativeOperation);
				if(corporativeOperation.getVariationAmount()!= null && 
						!corporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode()) &&
						corporativeOperation.getVariationAmount().compareTo(BigDecimal.ZERO)==-1){
					corporativeOperation.setVariationAmount(null);
					corporativeOperation.setNewShareCapital(null);
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_SHARE_BALANCE));
					JSFUtilities.showSimpleValidationDialog();
				}
				if(increaseCapitalMotive!=null){
					if(!validateIncreaseCapitalAmount(corporativeOperation)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_REQUIRED_INCREASE_MOTIVE));
						JSFUtilities.showSimpleValidationDialog();					
						JSFUtilities.addContextMessage("importanceEventForm:motiveIncrease",
								FacesMessage.SEVERITY_INFO, " ", " ");
					}

				}
			}else if(Validations.validateIsNotNull(corporativeOperation.getNewShareCapital())){
				BigDecimal temp = BigDecimal.ZERO;
				if(temp.compareTo(corporativeOperation.getNewShareCapital())==0){
					corporativeOperation.setNewShareCapital(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_SHARE_ZERO_BALANCE));
					JSFUtilities.showSimpleValidationDialog();
					return;

				}
			}
		}
		if (isViewOperationModify()) {
			if(Validations.validateIsNotNull(selectedCorporativeOpertation.getNewShareCapital()) && Validations.validateIsNotNullAndPositive(selectedCorporativeOpertation.getNewShareCapital())
					&&Validations.validateIsNotNull(selectedCorporativeOpertation.getOldShareCapital()) &&Validations.validateIsNotNullAndPositive(selectedCorporativeOpertation.getOldShareCapital())){
				this.calculateShareCapitalVariation(selectedCorporativeOpertation);
				if(selectedCorporativeOpertation.getVariationAmount().compareTo(BigDecimal.ZERO)==-1){
					selectedCorporativeOpertation.setVariationAmount(null);
					selectedCorporativeOpertation.setNewShareCapital(null);
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_SHARE_BALANCE));
					JSFUtilities.showSimpleValidationDialog();	
				}
				if(increaseCapitalMotive!=null){
					if(!validateIncreaseCapitalAmount(selectedCorporativeOpertation)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_REQUIRED_INCREASE_MOTIVE));
						JSFUtilities.showSimpleValidationDialog();					
						JSFUtilities.addContextMessage("importanceEventForm:motiveIncrease",
								FacesMessage.SEVERITY_INFO, " ", " ");
					}

				}
			}else if(Validations.validateIsNotNull(selectedCorporativeOpertation.getNewShareCapital())){
				BigDecimal temp = BigDecimal.ZERO;
				if(temp.compareTo(selectedCorporativeOpertation.getNewShareCapital())==0){
					selectedCorporativeOpertation.setNewShareCapital(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_SHARE_ZERO_BALANCE));
					JSFUtilities.showSimpleValidationDialog();
					return;

				}
			}
		}
	}

	/**
	 * Metodo que limpia objetos del hecho de importancia.
	 */
	public void clearCorporativeOperation(){
		corporativeOperation = new CorporativeOperation();
		corporativeOperation = new CorporativeOperation();
		corporativeOperation.setIssuer(new Issuer());
		corporativeOperation.setSecurities(new Security());
		corporativeOperation.setCreationDate(CommonsUtilities.currentDate());
		corporativeOperation.setRegistryDate(CommonsUtilities.currentDate());
		corporativeOperation.setCutoffDate(corporativeOperation.getRegistryDate());
		clearHelpers();
		this.disableMotiveIncreaseVar();
		this.initializeCapitalIncreaseMotive();
		increaseCapitalMotive = null;
		disableAddButton=Boolean.TRUE;
		corporativeOperationDataModel = new CorporativeOperationDataModel();
		securitiesCorporativeList =new ArrayList<SecuritiesCorporative>();
		selectedSecuritiesCorporative = null;

	}

	/**
	 * Metodo que habilita/deshabilita objetos dependiendo el hecho de importancia.
	 *
	 * @throws CloneNotSupportedException the clone not supported exception
	 */
	public void changeEventInfoListener() throws CloneNotSupportedException {
		renderOutputPanel = Boolean.TRUE; 
		disableIssuer = Boolean.FALSE; 
		if (isViewOperationRegister()) {
//			
			com.pradera.model.corporatives.CorporativeEventType corpEventType=corporativeOperation.getCorporativeEventType().clone();
			clearCorporativeOperation();
			corporativeOperation.setCorporativeEventType(corpEventType);			
			this.renderFields(corporativeOperation.getCorporativeEventType());
		} 
		if (isViewOperationModify()) {
			this.renderFields(selectedCorporativeOpertation.getCorporativeEventType());
		} 
	}

	/**
	 * Metodo que habilita/deshabilita objetos dependiendo el hecho de importancia.
	 * 
	 * @param eventType
	 *            the event type
	 */
	public void renderFields(com.pradera.model.corporatives.CorporativeEventType eventType) {
		if (eventType.getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())) {
			this.clearPanels();
			renderExchangeRelationship = Boolean.TRUE;
			renderSecurityDescription   = Boolean.TRUE;
			renderIndRemanent           = Boolean.TRUE;
			renderCapIncreaseMotive     = Boolean.TRUE;
			renderNominalValueCapVar    = Boolean.TRUE;
			disableShareBalance         = Boolean.TRUE;
			renderDeliveryFactor        = Boolean.TRUE;
			disableDeliveryPlace        = Boolean.FALSE;
			disableCurrency             = Boolean.FALSE;
			renderMarketFact			= Boolean.TRUE;
			requireTargetSecurity       = "true";
			corporativeOperation.setTargetSecurity(new Security());
		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())) {
			this.clearPanels();		 
			disableCurrency=Boolean.FALSE;
			renderMarketFact			= Boolean.TRUE;
			renderTargetSecurity = Boolean.TRUE;
			renderOpnlTargetSecurity = Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
			disableDeliveryPlace = Boolean.TRUE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "false";
			disableAddButton = Boolean.TRUE;		

			if (Validations.validateIsNotNull(corporativeOperation)) {
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
			}

		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())) {
			this.clearPanels();
			renderExchangeRelationship = Boolean.TRUE;
			renderSecurityDescription = Boolean.TRUE;
			renderNominalValueDescription = Boolean.TRUE;
			renderSharecapital = Boolean.TRUE;
			renderNominalValueNoCapVar = Boolean.TRUE;
			disableShareBalance = Boolean.TRUE;
			corporativeOperation.setNewShareCapital(null);
			corporativeOperation.setTargetSecurity(new Security());
			disableDeliveryPlace = Boolean.TRUE;
			disableCurrency=Boolean.FALSE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "true";
			renderMarketFact			= Boolean.TRUE;
			if (Validations.validateIsNotNull(corporativeOperation)) {
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
			}

		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())) {
			disableCurrency=Boolean.FALSE;

			this.clearPanels();
			renderExchangeRelationship = Boolean.TRUE;
			renderSecurityDescription = Boolean.TRUE;
			renderNominalValueDescription = Boolean.TRUE;
			renderNominalValueCapVar = Boolean.TRUE;
			renderSharecapital = Boolean.TRUE;
			disableShareBalance = Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
			disableDeliveryPlace = Boolean.TRUE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "true";
			renderMarketFact			= Boolean.TRUE;
			if (Validations.validateIsNotNull(corporativeOperation)) {
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
				corporativeOperation.setDeliveryFactor(new BigDecimal(100));
			}

		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())) {
			this.clearPanels();
			renderTargetSecurity = Boolean.TRUE;
			renderOpnlTargetSecurity = Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());			
			disableCurrency=Boolean.FALSE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "false";
			renderMarketFact			= Boolean.TRUE;
			disableDeliveryPlace = Boolean.TRUE;
			corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
			
		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())) {
			this.clearPanels();
			renderTargetSecurity = Boolean.TRUE;
			renderOpnlTargetSecurity = Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
			disableDeliveryPlace = Boolean.TRUE;
			disableCurrency=Boolean.FALSE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "false";
			disableAddButton = Boolean.TRUE;
			renderMarketFact			= Boolean.TRUE;
			if (Validations.validateIsNotNull(corporativeOperation)) {
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
			}

		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())) {
			this.clearPanels();
			renderTargetSecurity = Boolean.TRUE;
			renderOpnlTargetSecurity = Boolean.TRUE;
			disableDeliveryPlace = Boolean.TRUE;
			disableCurrency=Boolean.FALSE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "false";
			if (Validations.validateIsNotNull(corporativeOperation)) {
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
			}
			renderMarketFact			= Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())) {
			this.clearPanels();
			renderTargetSecurity = Boolean.TRUE;
			renderOpnlTargetSecurity = Boolean.FALSE;
			renderSecuritiesEscision=Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
			disableDeliveryPlace = Boolean.TRUE;
			disableCurrency=Boolean.FALSE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.TRUE;
			requireTargetSecurity = "false";
			renderMarketFact			= Boolean.TRUE;
			if(isViewOperationAnnul() || isViewOperationDetail() || isViewOperationModify()){
				renderSecuritiesEsicion = Boolean.TRUE;
			}			
			if (Validations.validateIsNotNull(corporativeOperation)) {				
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
			}

		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())) {
			this.clearPanels();
			renderNominalValueDescription = Boolean.TRUE;
			renderExchangeRelationship=Boolean.TRUE;
			renderOpnlTargetSecurity = Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
			disableDeliveryPlace = Boolean.TRUE;
			disableCurrency=Boolean.FALSE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "false";
			renderSharecapital = Boolean.TRUE;
			renderMarketFact			= Boolean.FALSE;
			if (Validations.validateIsNotNull(corporativeOperation)) {
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
			}
			corporativeOperation.setDeliveryFactor(new BigDecimal(100));

		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.CASH_DIVIDENDS.getCode())) {
			this.clearPanels();
			renderExchangeRelationship=Boolean.TRUE;
			renderCurrencyType = Boolean.TRUE;
			renderDeliveryFactor = Boolean.TRUE;
			renderIndTaxFactor = Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
			disableCurrency=Boolean.TRUE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "true";
			renderMarketFact			= Boolean.FALSE;
		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())) {
			this.clearPanels();
			renderExchangeRelationship=Boolean.TRUE;
			renderDeliveryFactor = Boolean.TRUE;
			renderPreferentSuscriptionPanel = Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
			renderTargetSecurity = Boolean.TRUE;
			disableDeliveryPlace = Boolean.FALSE;
			disableCurrency=Boolean.FALSE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "false";
			renderMarketFact			= Boolean.TRUE;
		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())) {
			this.clearPanels();
			renderExchangeRelationship=Boolean.TRUE;
			renderRemainingCashPaymentPanel = Boolean.TRUE;
			renderDeliveryFactor = Boolean.TRUE;
			renderCurrencyType = Boolean.TRUE;
			renderIndTaxFactor = Boolean.TRUE;
			corporativeOperation.setTargetSecurity(new Security());
			disableDeliveryPlace = Boolean.FALSE;
			disableCurrency=Boolean.TRUE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "true";
			renderMarketFact			= Boolean.FALSE;
		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())) {
			this.clearPanels();
			renderExchangeRelationship=Boolean.TRUE;
			renderRemainingCashPaymentPanel = Boolean.TRUE;
			renderDeliveryFactor = Boolean.TRUE;
			renderCurrencyType = Boolean.TRUE;
			renderIndTaxFactor = Boolean.TRUE;
			setImportanceEvent(new CorporativeOperation());
			corporativeOperation.setTargetSecurity(new Security());
			disableDeliveryPlace = Boolean.TRUE;
			disableCurrency=Boolean.TRUE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "true";
			renderMarketFact			= Boolean.FALSE;
			if (Validations.validateIsNotNull(corporativeOperation)) {
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
			}

		} else if (eventType.getCorporativeEventType().equals(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode())) {
			this.clearPanels();
			renderNominalValueDescription = Boolean.TRUE;
			renderExchangeRelationship=Boolean.TRUE;
			renderOpnlTargetSecurity = Boolean.TRUE;
 			disableCutoffDate = Boolean.TRUE;
			disableDeliveryPlace = Boolean.TRUE;
			disableCurrency=Boolean.FALSE;
			renderDeliveryFactorSecuritiesEsicion=Boolean.FALSE;
			requireTargetSecurity = "true";
			renderSharecapital = Boolean.TRUE;
			renderMarketFact			= Boolean.FALSE;
			if (Validations.validateIsNotNull(corporativeOperation)) {
				corporativeOperation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
				corporativeOperation.setDeliveryFactor(new BigDecimal(100));
			}

		}

		else if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(eventType.getCorporativeEventType())){
			this.clearPanels();
			renderIndTaxFactor=Boolean.TRUE;
			renderTaxFactor=Boolean.TRUE;
			renderCurrencyType=Boolean.TRUE;
			renderInterestAmortization = Boolean.TRUE;
			renderInterest = Boolean.TRUE;
		} else if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(eventType.getCorporativeEventType())){
			this.clearPanels();
			renderInterestAmortization = Boolean.TRUE;			
			renderAmortization = Boolean.TRUE;
			renderCurrencyType=Boolean.TRUE;
			renderIndTaxFactor=Boolean.TRUE;
		}
	}

	/**
	 * Metodo que deshabilita todos los objetos.
	 */
	public void clearPanels() {
		renderMarketFact= Boolean.FALSE;
		renderSecurityDescription = Boolean.FALSE;
		renderTargetSecurity = Boolean.FALSE;
		renderNominalValueDescription = Boolean.FALSE;
		renderCapVarDescription = Boolean.FALSE;
		renderSharecapital = Boolean.FALSE;
		renderExchangeRelationship = Boolean.FALSE;
		renderIndRemanent = Boolean.FALSE;
		renderOpnlTargetSecurity = Boolean.FALSE;
		renderNominalValueNoCapVar = Boolean.FALSE;
		renderNominalValueCapVar = Boolean.FALSE;
		disableShareBalance = Boolean.FALSE;
		renderCapIncreaseMotive = Boolean.FALSE;
		renderCurrencyType = Boolean.FALSE;
		renderInterestAmortization = Boolean.FALSE;
		renderAmortization = Boolean.FALSE;
		renderInterest = Boolean.FALSE;
		renderDeliveryFactor = Boolean.FALSE;
		renderIndTaxFactor = Boolean.FALSE;
		renderTaxFactor = Boolean.FALSE;
		renderPreferentSuscriptionPanel = Boolean.FALSE;
		renderRemainingCashPaymentPanel = Boolean.FALSE;
		disableCutoffDate = Boolean.FALSE;
		endSuscription = Boolean.FALSE;
		renderFirstNegotiationDate = Boolean.FALSE;
		renderSecondNegotiationDate = Boolean.FALSE;
		renderThirdNegotiationDate = Boolean.FALSE;
		renderSecuritiesEscision = Boolean.FALSE;
		renderSecuritiesEsicion = Boolean.FALSE;
		renderExchange = Boolean.FALSE;
	}

	/**
	 * Metodo que consulta valores destino de una escision.
	 * 
	 * @param filter
	 *            the filter
	 */
	public void findTargetSecurities(SecurityTO filter) {
		try {
			lstTargetSecurities = allComponenteFacade.searchSecuritiesByFilter(filter);
			disableTargetSecuritiesCombo = Boolean.FALSE;

		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	// findSecurityDescription
	/**
	 * Metodo que consulta descripcion de un valor.
	 */
	public void findTargetSecurityDescriptionListener() {
 		if (isViewOperationRegister()) {
			this.findTargetSecurityDescription(corporativeOperation);
		}
		if (isViewOperationModify()) {
			this.findTargetSecurityDescription(selectedCorporativeOpertation);
		}
	}
	
	/**
	 * Metodo que carga datos de una valor.
	 *
	 * @param num the num
	 * @throws ServiceException the service exception
	 */
	public void filterSecuritieListener(String num) throws ServiceException{
		this.hideDialogs();
		executeAction();
		resetComponent();
		
		if(isViewOperationRegister()){
			if(Integer.parseInt(num)==1){
				corporativeOperation.setSecurities(securitySourceHelperMgmt);
			}
			if(Integer.parseInt(num)==2){
				corporativeOperation.setTargetSecurity(securityTargetHelperMgmt);
			}			
			this.filterSecuritie(corporativeOperation,num);
		}
		if(isViewOperationModify()){
			if(Integer.parseInt(num)==1){
				selectedCorporativeOpertation.setSecurities(selectedCorporativeOpertation.getSecurities());
			}
			if(Integer.parseInt(num)==2){
				selectedCorporativeOpertation.setTargetSecurity(selectedCorporativeOpertation.getTargetSecurity());
			}	
			
			this.filterSecuritie(selectedCorporativeOpertation,num);
		}
	}
	
	/**
	 * Metodo que valida valor.
	 *
	 * @param security security
	 * @return Validate Security
	 */
	public boolean validateSecurity(Security security){

		if (!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())
				|| !security.getIssuer().getIdIssuerPk().equals(issuer.getIssuerCode())
				|| !security.getInstrumentType().equals(instrumentType)) {
			showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
					PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_STATE_ISSUER_INSTRUMENT));
			JSFUtilities.showSimpleValidationDialog();
			return Boolean.TRUE;
		}
		return Boolean.FALSE;		
	}	
	
	/**
	 * Metodo que valida valor para una devolucion de aportes.
	 *
	 * @param security the security
	 * @return true, if successful
	 */
	public boolean validateSecurityForReturn(Security security){
		if (!((security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())
				|| security.getStateSecurity().equals(SecurityStateType.EXCLUDED.getCode()))			
				&& security.getIssuer().getIdIssuerPk().equals(issuer.getIssuerCode())
				&& security.getInstrumentType().equals(instrumentType))) {
			showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
					PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_STATE_ISSUER_INSTRUMENT));
			JSFUtilities.showSimpleValidationDialog();
			return Boolean.TRUE;
		}
		return Boolean.FALSE;		
	}	
	/**
	 * Metodo que valida saldo desmaterializado de un valor.
	 *
	 * @param security security
	 * @return Validate Balance Desmaterialized
	 */
	public boolean validateBalanceDesmaterialized(Security security)
	{
		if(security.getDesmaterializedBalance()==null ||
				(security.getDesmaterializedBalance()!= null && security.getDesmaterializedBalance().compareTo(BigDecimal.ZERO)==0)){
			clearSecurityHelper();
			showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
					PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_BALANCE_DESMATERIALIZED_SECURITY));
			JSFUtilities.showSimpleValidationDialog();
			return Boolean.TRUE;
		}
		return Boolean.FALSE;	
	}
	
	/**
	 * Metodo que valida intereses exonerados para un valor.
	 *
	 * @param operation process corporate
	 * @param security security
	 */
	public void  validateSecurityInterestExonerated(CorporativeOperation operation, Security security){
		if(Validations.validateIsNotNull(security)){
			if(Validations.validateIsNotNull(security.getIndTaxExempt())){
				if(BooleanType.YES.getCode().equals(security.getIndTaxExempt())){
					disableTaxFactor = Boolean.TRUE;
					operation.setIndTax(BooleanType.NO.getCode());
					operation.setIndTaxValue(Boolean.FALSE);
					operation.setTaxFactor(null);
				}
				else{
					disableTaxFactor=Boolean.FALSE;
				}				
			}
		} 		
	}
	
	/**
	 * Metodo que valida busqueda de un valor.
	 */
	public void validateSecuritySearch(){
		cleanImportanceEventModel();
		if(securityHelperSearch!=null && securityHelperSearch.getIdSecurityCodePk()!=null){
			try{
				tempSecurity = allComponenteFacade.getSecurityHelpServiceFacade(securityHelperSearch.getIdSecurityCodePk());
				if(tempSecurity!=null){
					if (!tempSecurity.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
						tempSecurity=null;
						securityHelperSearch=new Security();
						showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_SECURITY_NOT_REGISTERED));
						JSFUtilities.showSimpleValidationDialog();
					}else{
						corporativeOperationTO.setSourceSecurityCode(securityHelperSearch.getIdSecurityCodePk());
					}
				}else{
					securityHelperSearch=new Security();
					showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_SECURITY_NOT_EX));
					JSFUtilities.showSimpleValidationDialog();
				}
			}catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}			
		}
	}
	
	/**
	 * Metodo que valida y carga datos de un valor.
	 *
	 * @param operation the operation
	 * @param num the num
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void filterSecuritie(CorporativeOperation operation, String num) throws ServiceException{
		this.hideDialogs();
		executeAction();
		resetComponent();
		if(operation.getSecurities()!=null && Integer.parseInt(num)==1){
			operation.setCurrencyPayment(null);
			operation.setExchangeDateRate(null);
			tempSecurity = allComponenteFacade.getSecurityHelpServiceFacade(operation.getSecurities().getIdSecurityCodePk());
			if(tempSecurity!=null){
				tempSecurity.setCorporativeSourceType(CorporativeSecuritySourceType.SOURCE.getCode());
				if(operation.getCorporativeEventType().getCorporativeEventType()!=null){						
						validateSecurityInterestExonerated(operation,tempSecurity);
						calculateCustoffDate();
						if(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
							if (this.validateSecurity(tempSecurity)) {	
								operation.setSecurities(new Security());
								securitySourceHelperMgmt=new Security();
							}else if(this.validateBalanceDesmaterialized(tempSecurity)){
								operation.setSecurities(new Security());
								securitySourceHelperMgmt=new Security();
								return ;
							}else if(targeSecurity!=null){
								if(targeSecurity.getCurrency()!=null 
										&& !targeSecurity.getCurrency().equals(tempSecurity.getCurrency())){
									operation.setSecurities(new Security());
									securitySourceHelperMgmt=new Security();
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
											PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY));
									JSFUtilities.showSimpleValidationDialog();
									return ;
								}								
							}else{
								this.hideDialogs();
								sourceSecurity = tempSecurity;
								disableAddButton = Boolean.FALSE;
							}
						}
						else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CASH_DIVIDENDS.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode())
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode()) 
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){

							operation.setSecurities(tempSecurity);
							//Validate state, issuer relationship and instrument type
							if (operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode()) 
									&& this.validateSecurityForReturn(tempSecurity)) {								
									operation.setSecurities(new Security());
									securitySourceHelperMgmt=new Security();
									return ;																
							}else if (!operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())
									&& this.validateSecurity(tempSecurity)) {								
									operation.setSecurities(new Security());
									securitySourceHelperMgmt=new Security();
									return ;																
//							}else if(!operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode()) 
//									&& this.validateBalanceDesmaterialized(tempSecurity)){								
//									operation.setSecurities(new Security());
//									securitySourceHelperMgmt=new Security();
//									return ;																
							}else if(targeSecurity!=null){
								if(targeSecurity.getCurrency()!=null 
										&& !targeSecurity.getCurrency().equals(tempSecurity.getCurrency())){
									operation.setSecurities(new Security());
									securitySourceHelperMgmt=new Security();
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
											PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY));
									JSFUtilities.showSimpleValidationDialog();
									return ;
								}								
							}else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())&&
									targeSecurity!=null && targeSecurity.getIdSecurityCodePk()!=null 
									&& targeSecurity.getIdSecurityCodePk().equals(tempSecurity.getIdSecurityCodePk())){
								operation.setSecurities(new Security());
								securitySourceHelperMgmt=new Security();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_SECURITY));
								JSFUtilities.showSimpleValidationDialog();
								return ;
							}else if(targeSecurity!=null && operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode()) &&
									 targeSecurity.getIssuer().getIdIssuerPk()!= null && tempSecurity.getIssuer().getIdIssuerPk().equals(targeSecurity.getIssuer().getIdIssuerPk())){
									operation.setSecurities(new Security());
									securitySourceHelperMgmt=new Security();
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
											PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_DIFFERENT_ISSUER));
									JSFUtilities.showSimpleValidationDialog();
									return ;
								}
							else{
								this.hideDialogs();
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CASH_DIVIDENDS.getCode())	       ||
										operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())||
										operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())){
									Integer currency = operation.getSecurities().getCurrency();
									if(currency!=null){
										operation.setCurrency(currency);										
									}									
								}
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())){
									this.findEventForContributionReturnEvent(operation);
								}
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())){
									this.findEventForPaymentRemanentEvent(operation);
								}
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())){
									if (tempSecurity.getIndPaymentBenefit().equals(BooleanType.YES.getCode())) {
										operation.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY.getCode());
										disableDeliveryPlace = Boolean.TRUE;
									}
								}
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())){
 									if (operation.getSecurities().getIndConvertibleStock().equals(BooleanType.NO.getCode())) { 										
 										securitySourceHelperMgmt=new Security();
										operation.setSecurities(new Security());
										showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
												PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_SECURITY_NOT_IND_CONVERTIBLE));
										JSFUtilities.showSimpleValidationDialog();
										return ;
									}
								}								
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode()) ){
									securityTargetHelperMgmt = securitySourceHelperMgmt;
									operation.setTargetSecurity(tempSecurity);									
								}
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode())
									&&	!tempSecurity.getSecurityClass().equals(SecurityClassType.CSP.getCode())){
									securitySourceHelperMgmt=new Security();
									operation.setSecurities(new Security());
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
											PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_SECURITIE_PREFERENT));
									JSFUtilities.showSimpleValidationDialog();
									return ;
								}
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())){
									operation.setNewShareCapital(operation.getOldShareCapital());									
								}						
								if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CASH_DIVIDENDS.getCode())	       ||
										operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())||
										operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())){									
									if(operation.getCurrency() != null){
										if(operation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
												operation.getCurrency().equals(CurrencyType.DMV.getCode())){
											operation.setCurrencyPayment(CurrencyType.PYG.getCode());
											renderExchange = Boolean.TRUE;
										}else{
											renderExchange = Boolean.FALSE;											
										}
									}									
								}
								sourceSecurity = tempSecurity;
								this.setSecuritiesInfo(operation, tempSecurity, null);		
							}
						}
				}
			}

		}else if(operation.getTargetSecurity()!=null && Integer.parseInt(num)==2){
			tempSecurity = allComponenteFacade.getSecurityHelpServiceFacade(operation.getTargetSecurity().getIdSecurityCodePk());
			if(tempSecurity!=null){
				tempSecurity.setCorporativeSourceType(CorporativeSecuritySourceType.TARGET.getCode());
				if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())){
					//Validate state, issuer relationship and instrument type
					if (this.validateSecurity(tempSecurity)) {
						operation.setTargetSecurity(new Security());
						securityTargetHelperMgmt=new Security();
					}
					else if(sourceSecurity!=null){		
						if(sourceSecurity.getCurrency()!=null 
								&& !sourceSecurity.getCurrency().equals(tempSecurity.getCurrency())){
							operation.setTargetSecurity(new Security());
							securityTargetHelperMgmt=new Security();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY));
							JSFUtilities.showSimpleValidationDialog();
							return ;
						}						
					}
					else{
						if(!tempSecurity.getSecurityClass().equals(SecurityClassType.CSP.getCode())){
							clearSecurityHelper();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_SECURITIE_PREFERENT));
							JSFUtilities.showSimpleValidationDialog();
						}
						else{
							targeSecurity = tempSecurity;									
							disableAddButton = Boolean.FALSE;
						}					
					}
				}else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())){						
					if (!tempSecurity.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())|| 
							!tempSecurity.getInstrumentType().equals(instrumentType)) {	
						operation.setTargetSecurity(new Security());
						securityTargetHelperMgmt=new Security();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_STATE_ISSUER));
						JSFUtilities.showSimpleValidationDialog();
						return;					

					}
					if(sourceSecurity!=null &&
						tempSecurity.getIssuer().getIdIssuerPk().equals(sourceSecurity.getIssuer().getIdIssuerPk())){
						operation.setTargetSecurity(new Security());
						securityTargetHelperMgmt=new Security();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_DIFFERENT_ISSUER));
						JSFUtilities.showSimpleValidationDialog();
						return ;
					}else if(sourceSecurity!=null){		
						if(sourceSecurity.getCurrency()!=null 
								&& !sourceSecurity.getCurrency().equals(tempSecurity.getCurrency())){
							operation.setTargetSecurity(new Security());
							securityTargetHelperMgmt=new Security();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY));
							JSFUtilities.showSimpleValidationDialog();
							return ;
						}						
					}
					else{
						targeSecurity = tempSecurity;
						disableAddButton = Boolean.FALSE;	
						if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode()) 
								||operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
							this.setSecuritiesInfo(operation, tempSecurity, null);
						}						
					}

				}else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())){
					if(this.validateSecurity(tempSecurity)){						
						operation.setTargetSecurity(new Security());
						securityTargetHelperMgmt=new Security();
					}else if(sourceSecurity!=null && sourceSecurity.getIdSecurityCodePk()!=null 
							&& sourceSecurity.getIdSecurityCodePk().equals(tempSecurity.getIdSecurityCodePk())){
						operation.setTargetSecurity(new Security());
						securityTargetHelperMgmt=new Security();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_SECURITY));
						JSFUtilities.showSimpleValidationDialog();
						return ;
					}else if(sourceSecurity!=null){		
						if(sourceSecurity.getCurrency()!=null 
								&& !sourceSecurity.getCurrency().equals(tempSecurity.getCurrency())){
							operation.setTargetSecurity(new Security());
							securityTargetHelperMgmt=new Security();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY));
							JSFUtilities.showSimpleValidationDialog();
							return ;
						}						
					}else{						
						targeSecurity = tempSecurity;
						disableAddButton = Boolean.FALSE;
					}
				}
				else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())){
					if (!tempSecurity.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())
							|| !tempSecurity.getIssuer().getIdIssuerPk().equals(issuer.getIssuerCode())
							|| !tempSecurity.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())) {
						operation.setTargetSecurity(new Security());
						
						securityTargetHelperMgmt=new Security();
						
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_FIXED_INCOME));
						JSFUtilities.showSimpleValidationDialog();
					}else if(sourceSecurity!=null){		
						if(sourceSecurity.getCurrency()!=null 
								&& !sourceSecurity.getCurrency().equals(tempSecurity.getCurrency())){
							operation.setTargetSecurity(new Security());
							securityTargetHelperMgmt=new Security();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY));
							JSFUtilities.showSimpleValidationDialog();
							return ;
						}						
					}else{
						targeSecurity = tempSecurity;
						disableAddButton = Boolean.FALSE;	
						this.setSecuritiesInfo(operation, tempSecurity, null);
					}

				} else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
					if(sourceSecurity!=null){		
						if(sourceSecurity.getCurrency()!=null 
								&& !sourceSecurity.getCurrency().equals(tempSecurity.getCurrency())){
							operation.setTargetSecurity(new Security());
							securityTargetHelperMgmt=new Security();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY));
							JSFUtilities.showSimpleValidationDialog();
							return ;
						}						
					}
					targeSecurity = tempSecurity;
					disableAddButton = Boolean.FALSE;					
					this.setSecuritiesInfo(operation, tempSecurity, null);					
					operation.setNewCirculationBalance(null);
					operation.setNewNominalValue(null);
					operation.setNewShareBalance(null);
					operation.setNewShareCapital(null);
					operation.setVariationAmount(null);
					operation.setMarketDate(null);
					operation.setMarketPrice(null);
					operation.setExchangeRelaSourceSecurity(null);
					operation.setExchangeRelaTargetSecurity(null);
					operation.setTargetRatio(null);
					operation.setOriginRatio(null);
					operation.setDeliveryFactor(null);
				}
			}else{
				securityTargetHelperMgmt=new Security();						
				operation.setTargetSecurity(new Security());
				targeSecurity = new Security();
				this.deleteSecurityInfo(operation,null);
			}
		}

	}

	/**
	 * Metodo que refresca helper del valor destino.
	 */
	public void clearTargetSecurityHelper(){
		JSFUtilities.resetComponent("importanceEventForm:delomio:securityCodeHelpersecurityHelperie");

	}

	/**
	 * Metodo que agrega valores destino por la escision.
	 */
	public void addSecurityListener(){
		this.hideDialogs();
		executeAction();
		resetComponent();
		modifyForExcision = Boolean.TRUE;
		if(isViewOperationRegister()){
			this.addSecurity(corporativeOperation);
		}
		if(isViewOperationModify()){
			this.addSecurity(selectedCorporativeOpertation);
		}
	}

	/**
	 * Metodo que elimina valores destino para la escision.
	 *
	 * @param secCorporativos the sec corporativos
	 * @throws ServiceException the service exception
	 */
	public void deleteTargetSecurity(Security secCorporativos){
		this.hideDialogs();
		executeAction();
		resetComponent();
		if((isViewOperationRegister() && selectedSecuritiesCorporative!=null) || (isViewOperationModify() && selectedSecuritiesCorporative!=null)  ||
				(selectedSecuritiesCorporative!=null && selectedSecuritiesCorporative.getIdSecuritiesCorporativePk()!=null)){
			if(secCorporativos.isSelected()){
				targeSecurity = null;		
				mapSecuritiesCorporative.remove(secCorporativos.getIdSecurityCodePk());
				listSecuritiesTarget = new ArrayList<Security>();
				listSecuritiesTargetAux = new ArrayList<Security>();
				securitiesCorporativeList = new ArrayList<SecuritiesCorporative>();
				Iterator it = mapSecuritiesCorporative.keySet().iterator();
				while(it.hasNext()){
					  String key = (String)it.next();
					  SecuritiesCorporative objSecuritiesCorporative = mapSecuritiesCorporative.get(key);
					  Security objSecurity = objSecuritiesCorporative.getSecurity();
						try {
							objSecurity = allComponenteFacade.getSecurityHelpServiceFacade(objSecuritiesCorporative.getSecurity().getIdSecurityCodePk());
						} catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						objSecurity.setExchangeRelaSourceSecurity(objSecuritiesCorporative.getOriginRatio());
						objSecurity.setExchangeRelaTargetSecurity(objSecuritiesCorporative.getTargetRatio());
						objSecurity.setDeliveryFactorForCorporateOperation(objSecuritiesCorporative.getDeliveryFactor());
						objSecurity.setShareCapital(objSecuritiesCorporative.getOldShareCapital());
						objSecurity.setShareBalance(objSecuritiesCorporative.getOldShareBalance());
						objSecurity.setCirculationBalance(objSecuritiesCorporative.getOldCirculationBalance());
						objSecurity.setVariationAmount(objSecuritiesCorporative.getVariationAmount());
						objSecurity.setMarketDate(objSecuritiesCorporative.getMarketDate());
						objSecurity.setMarketPrice(objSecuritiesCorporative.getMarketPrice());
						objSecurity.setNewShareCapital(objSecuritiesCorporative.getNewShareCapital());
						objSecurity.setNewShareBalance(objSecuritiesCorporative.getNewShareBalance());
						objSecurity.setNewCirculationBalance(objSecuritiesCorporative.getNewCirculationBalance());
					  listSecuritiesTarget.add(objSecurity);
					  listSecuritiesTargetAux.add(objSecurity);
					  securitiesCorporativeList.add(objSecuritiesCorporative);
				}
				if(isViewOperationRegister()){
					corporativeOperation.setTargetSecurity(new Security());
					deleteSecurityInfo(corporativeOperation, null);
				}else{
					selectedCorporativeOpertation.setTargetSecurity(new Security());
					deleteSecurityInfo(selectedCorporativeOpertation, null);
				}
				
				selectedSecuritiesCorporative = null;
				disableTargetSecurityHelper = Boolean.FALSE;
				disabledForExcision = Boolean.FALSE;
				modifyForExcision = Boolean.TRUE;
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
			             PropertiesUtilities.getMessage(PropertiesConstants.LBL_WARNING_ACTION_MESSAGE_ACTIVE));
				JSFUtilities.showSimpleValidationDialog();
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
		             PropertiesUtilities.getMessage(PropertiesConstants.LBL_WARNING_ACTION_MESSAGE));
			JSFUtilities.showSimpleValidationDialog();
		}		
	}

	/**
	 * Metodo que agrega valores destino para la escision.
	 *
	 * @param operation the operation
	 */
	public void addSecurity(CorporativeOperation operation){
		this.hideDialogs();
		if(targeSecurity!=null && targeSecurity.getIdSecurityCodePk()!=null){
			if(validateFieldTargetSecurity(operation)){
				if(!mapSecuritiesCorporative.containsKey(targeSecurity.getIdSecurityCodePk())){
					targeSecurity.setExchangeRelaSourceSecurity(operation.getOriginRatio());
					targeSecurity.setExchangeRelaTargetSecurity(operation.getTargetRatio());
					targeSecurity.setDeliveryFactorForCorporateOperation(operation.getDeliveryFactor());
					if(listSecuritiesTarget==null){
						listSecuritiesTarget = new ArrayList<Security>();
						listSecuritiesTargetAux = new ArrayList<Security>();
					}
					listSecuritiesTarget.add(targeSecurity);
					listSecuritiesTargetAux.add(targeSecurity);
					SecuritiesCorporative corporative = new SecuritiesCorporative();
					corporative.setCorporativeOperation(corporativeOperation);
					corporative.setSecurity(targeSecurity);	
					corporative.setMarketDate(operation.getMarketDate());
					corporative.setMarketPrice(operation.getMarketPrice());
					corporative.setNewCirculationBalance(operation.getNewCirculationBalance());
					corporative.setNewShareBalance(operation.getNewShareBalance());
					corporative.setNewShareCapital(operation.getNewShareCapital());
					corporative.setOldCirculationBalance(operation.getOldCirculationBalance());
					corporative.setOldShareBalance(operation.getOldShareBalance());
					corporative.setOldShareCapital(operation.getOldShareCapital());
					corporative.setVariationAmount(operation.getVariationAmount());					
					corporative.setTargetRatio(operation.getOriginRatio());
					corporative.setOriginRatio(operation.getTargetRatio());
					corporative.setDeliveryFactor(operation.getDeliveryFactor());
					securitiesCorporativeList.add(corporative);
					mapSecuritiesCorporative.put(targeSecurity.getIdSecurityCodePk(),corporative);	
					assignSecuritiesCorporativeListener(targeSecurity);
					securityTargetHelperMgmt=new Security();						
					operation.setTargetSecurity(new Security());
					targeSecurity = new Security();
					if(isViewOperationRegister()){
						JSFUtilities.resetComponent("importanceEventForm:opnlSecurityHelper");
					}
					if(isViewOperationModify()){
						JSFUtilities.resetComponent("importanceEventForm:opnlSecurityHelper");
					}
					
				}
				else{
					securityTargetHelperMgmt=new Security();						
					operation.setTargetSecurity(new Security());
					targeSecurity = new Security();
					this.deleteSecurityInfo(operation,null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							             PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_SECURITIT_IN_THE_TABLE));
					JSFUtilities.showSimpleValidationDialog();
				}
			}		
		}		
	}
	
	/**
	 * Metodo que valida valores destino para la escision.
	 *
	 * @param corporativeOperation the corporative operation
	 * @return true, if successful
	 */
	public boolean validateFieldTargetSecurity(CorporativeOperation corporativeOperation){
		boolean validate=true;
		if(!Validations.validateIsNotNullAndPositive(corporativeOperation.getNewShareCapital()) 
				|| !Validations.validateIsNotNullAndPositive(corporativeOperation.getNewCirculationBalance())
				|| !Validations.validateIsNotNullAndPositive(corporativeOperation.getExchangeRelaSourceSecurity())
				|| !Validations.validateIsNotNullAndPositive(corporativeOperation.getExchangeRelaTargetSecurity())
				|| !Validations.validateIsNotNullAndPositive(corporativeOperation.getMarketPrice())
				|| !Validations.validateIsNotNull(corporativeOperation.getMarketDate())){
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getGenericMessage(PropertiesConstants.IMPORTANCE_EVENT_REQUIRED_DATA));
			JSFUtilities.showSimpleValidationDialog();
			if(!Validations.validateIsNotNullAndPositive(corporativeOperation.getNewShareCapital())){
				JSFUtilities.addContextMessage("importanceEventForm:txtNewShareCapitalSE",
						FacesMessage.SEVERITY_INFO, " ", " ");
			}
			if(!Validations.validateIsNotNullAndPositive(corporativeOperation.getNewCirculationBalance())){
				JSFUtilities.addContextMessage("importanceEventForm:txtCirulanceBalancesSE",
						FacesMessage.SEVERITY_INFO, " ", " ");
			}
			if(!Validations.validateIsNotNullAndPositive(corporativeOperation.getExchangeRelaSourceSecurity())){
				JSFUtilities.addContextMessage("importanceEventForm:sourceExchangeSE",
						FacesMessage.SEVERITY_INFO, " ", " ");
			}
			if(!Validations.validateIsNotNullAndPositive(corporativeOperation.getExchangeRelaTargetSecurity())){
				JSFUtilities.addContextMessage("importanceEventForm:targetExchangeSE",
						FacesMessage.SEVERITY_INFO, " ", " ");
			}
			if(!Validations.validateIsNotNullAndPositive(corporativeOperation.getMarketPrice())){
				JSFUtilities.addContextMessage("importanceEventForm:txtMarketPriceSE",
						FacesMessage.SEVERITY_INFO, " ", " ");
			}
			if(!Validations.validateIsNotNull(corporativeOperation.getMarketDate())){
				JSFUtilities.addContextMessage("importanceEventForm:calMarketDateSE",
						FacesMessage.SEVERITY_INFO, " ", " ");
			}			
			validate = false;
		}
		return validate;
	}
	/**
	 * Metodo que valida descripcion del valor destino.
	 * 
	 * @param operation
	 *            the operation
	 */
	private void findTargetSecurityDescription(CorporativeOperation operation) {
		this.hideDialogs();
 		SecurityTO to = new SecurityTO();
		if (Validations.validateIsNotNullAndNotEmpty(operation.getTargetSecurity().getIdSecurityCodePk())) {
			to.setIdIsinCodePk(operation.getTargetSecurity().getIdSecurityCodePk());
 			Security temp = null;
			try {
				temp = allComponenteFacade.getSecurityHelpServiceFacade(operation.getTargetSecurity().getIdSecurityCodePk());
				operation.setTargetSecurity(temp);
				if (!this.validateSameSecurities(operation)) {

					if(!operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){

 						setSecuritiesInfo(operation, operation.getTargetSecurity(),null);
						
						if (operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode())) {
							if (Validations.validateIsNotNull(operation.getTargetSecurity())) {
								if (!operation.getTargetSecurity().getSecurityClass().equals(SecurityClassType.CSP.getCode())) {
									operation.setTargetSecurity(new Security());
									

									JSFUtilities.resetComponent("importanceEventForm:delomio:securityCodeHelpersecurityHelperie");
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
											PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_LBL_PREFERENT_SUSCRIPTION));
									JSFUtilities.showSimpleValidationDialog();

								} else {
								}
							} else {
							}
						}
					}
					//Operations to do in case securities escision event
					else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){			
						disableAddButton=Boolean.FALSE;
					} 

					else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())){
						//Validate state, issuer relationship and instrument type
						if (!temp.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())
								|| !temp.getIssuer().getIdIssuerPk()
								.equals(issuer.getIssuerCode())
								|| !temp.getInstrumentType().equals(instrumentType)) {
							clearSecurityHelper();
							operation.setTargetSecurity(new Security());
							if (isViewOperationRegister()) {
								JSFUtilities.resetComponent("importanceEventForm:delomio:securityCodeHelpersecurityHelperie");
							}
							if (isViewOperationModify()) {
								
							}
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_FIXED_INCOME));
							JSFUtilities.showSimpleValidationDialog();
						}
					}
				}else{
					clearSecurityHelper();
					operation.setTargetSecurity(new Security());
				}

			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			} 
		}
	}

	/**
	 * Metodo valida si los valores origen y destino son los mismos.
	 * 
	 * @param operation
	 *            the operation
	 * @return true, if successful
	 */
	public boolean validateSameSecurities(CorporativeOperation operation) {
	
		if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
			return Boolean.FALSE;
		}
		if (operation.getSecurities() != null	&& operation.getTargetSecurity() != null) {
			if (operation.getSecurities().getIdSecurityCodePk() != null	&& operation.getTargetSecurity().getIdSecurityCodePk() != null) {
				if (operation.getSecurities().getIdSecurityCodePk().equals(operation.getTargetSecurity().getIdSecurityCodePk())) {
				 
					showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_VALIDATE_SAME_SECURITIE));
					if(isViewOperationRegister()){
						JSFUtilities.resetComponent("importanceEventForm:delomio:securityCodeHelpersecurityHelperie");
						JSFUtilities.resetComponent("importanceEventForm:helpSec:securityCodeHelpersourceSecurityHelper");
					}

					JSFUtilities.showSimpleValidationDialog();
					return Boolean.TRUE;
				}
			}
		}

		return Boolean.FALSE;

	}

	
	/**
	 * Metodo que actualiza datos del valor cargado.
	 *
	 * @param operation the operation
	 * @param security the security
	 * Description:
	 * This method set the properties of the security
	 * to a determined operation
	 * @param securitiesCorporatives the securities corporatives
	 */
	private void setSecuritiesInfo(CorporativeOperation operation,Security security,SecuritiesCorporative securitiesCorporatives) {

		if(operation==null){
			if (security.getShareBalance() != null) {
				securitiesCorporatives.setOldShareBalance(security.getShareBalance());
			} else {
				securitiesCorporatives.setOldShareBalance(null);
			}	
			if (security.getCirculationBalance() != null) {
				securitiesCorporatives.setOldCirculationBalance(security.getCirculationBalance());
			} else {
				securitiesCorporatives.setOldCirculationBalance(null);
			}
			if (security.getShareCapital() != null) {
				securitiesCorporatives.setOldShareCapital(security.getShareCapital());
			} else {
				securitiesCorporatives.setOldShareCapital(null);
			}	

		}
		else{

			if(	operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())   ){
				if(	security.getCorporativeSourceType().equals(CorporativeSecuritySourceType.TARGET.getCode())){
					if (security.getShareBalance() != null) {
						operation.setOldShareBalance(security.getShareBalance());
					} else {
						operation.setOldShareBalance(null);
					}
					if (security.getCurrentNominalValue() != null) {
						operation.setOldNominalValue(security.getCurrentNominalValue());
					} else {
						operation.setOldNominalValue(null);
					}

					if (security.getCirculationBalance() != null) {
						operation
						.setOldCirculationBalance(security.getCirculationBalance());
					} else {
						operation.setOldCirculationBalance(null);
					}
					if (security.getShareCapital() != null) {
						operation.setOldShareCapital(security.getShareCapital());
					} else {
						operation.setOldShareCapital(null);
					}	

				}
			}
			else if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode())      ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.ACTION_DIVIDENDS.getCode())              ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())           ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode()) ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode())      ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())   ||	
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode()) ||
					operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())){
				if(security.getCorporativeSourceType().equals(CorporativeSecuritySourceType.SOURCE.getCode())){
					if (security.getShareBalance() != null) {
						operation.setOldShareBalance(security.getShareBalance());
					} else {
						operation.setOldShareBalance(null);
					}
					if (security.getCurrentNominalValue() != null) {
						operation.setOldNominalValue(security.getCurrentNominalValue());
					} else {
						operation.setOldNominalValue(null);
					}

					if (security.getCirculationBalance() != null) {
						operation.setOldCirculationBalance(security.getCirculationBalance());
					} else {
						operation.setOldCirculationBalance(null);
					}
					if (security.getShareCapital() != null) {
						operation.setOldShareCapital(security.getShareCapital());
					} else {
						operation.setOldShareCapital(null);
					}	
				}
			}
		}

	}
	
	/**
	 * Metodo que valida si el valor nuevo es multiplo del valor nominal anterior.
	 *
	 * @param context the context
	 * @param component the component
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 */
	public void validateMultiploCurNominalValue(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(isViewOperationRegister()){
			if(value!=null && corporativeOperation.getNewNominalValue() != null){
				InputText input=(InputText)component;				
				BigDecimal currentValue=(BigDecimal)value;
				Object[] parameters = {input.getLabel(),PropertiesUtilities.getGenericMessage("security.lbl.new.nominal.value")};
				if(currentValue.compareTo(BigDecimal.ZERO)==0){
					return;
				}
				if(currentValue.compareTo(corporativeOperation.getNewNominalValue())==1){
					if(currentValue.remainder(corporativeOperation.getNewNominalValue()).compareTo(BigDecimal.ZERO)!=0){
						String strMsg =  PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MULTIPLO, parameters);
						FacesMessage msg = new FacesMessage(strMsg);
						msg.setSeverity(FacesMessage.SEVERITY_ERROR);
						 corporativeOperation.setNewShareCapital(null);
						JSFUtilities.resetComponent(component.getClientId());
						throw new ValidatorException(msg);
					}
				}	
			}else if(corporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())){
				InputText input=(InputText)component;				
				BigDecimal currentValue=(BigDecimal)value;
				Object[] parameters = {input.getLabel()};
				if(currentValue.compareTo(BigDecimal.ZERO)==0){
					return;
				}
				String strMsg =  PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_NEW_NOMINAL_VALUE, parameters);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				 corporativeOperation.setNewShareCapital(null);
				JSFUtilities.resetComponent(component.getClientId());
				throw new ValidatorException(msg);
			}
		}else{
			if(value!=null && selectedCorporativeOpertation.getNewNominalValue() != null){
				InputText input=(InputText)component;				
				BigDecimal currentValue=(BigDecimal)value;
				Object[] parameters = {input.getLabel(),PropertiesUtilities.getGenericMessage("security.lbl.current.nominal.value")};
				if(currentValue.compareTo(BigDecimal.ZERO)==0){
					return;
				}
				if(currentValue.compareTo(selectedCorporativeOpertation.getNewNominalValue())==1){
					if(currentValue.remainder(selectedCorporativeOpertation.getNewNominalValue()).compareTo(BigDecimal.ZERO)!=0){
						String strMsg =  PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MULTIPLO, parameters);
						FacesMessage msg = new FacesMessage(strMsg);
						msg.setSeverity(FacesMessage.SEVERITY_ERROR);
						selectedCorporativeOpertation.setNewShareCapital(null);
						JSFUtilities.resetComponent(component.getClientId());
						throw new ValidatorException(msg);
					}
				}	
			}else if(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())){
				InputText input=(InputText)component;				
				BigDecimal currentValue=(BigDecimal)value;
				Object[] parameters = {input.getLabel()};
				if(currentValue.compareTo(BigDecimal.ZERO)==0){
					return;
				}
				String strMsg =  PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_NEW_NOMINAL_VALUE, parameters);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				 corporativeOperation.setNewShareCapital(null);
				JSFUtilities.resetComponent(component.getClientId());
				throw new ValidatorException(msg);
			}			
		}
	}

	/**
	 * Metodo que elimina y actualiza datos del hecho de importancia con respecto al valor destino.
	 *
	 * @param operation the operation
	 * @param secCorpo the sec corpo
	 */
	public void deleteSecurityInfo(CorporativeOperation operation,SecuritiesCorporative secCorpo){
		operation.setOldShareBalance(null);
		operation.setOldNominalValue(null);
		operation.setOldCirculationBalance(null);
		operation.setOldShareCapital(null);
		operation.setNewCirculationBalance(null);
		operation.setNewNominalValue(null);
		operation.setNewShareBalance(null);
		operation.setNewShareCapital(null);
		operation.setVariationAmount(null);
		operation.setMarketDate(null);
		operation.setMarketPrice(null);
		operation.setExchangeRelaSourceSecurity(null);
		operation.setExchangeRelaTargetSecurity(null);
		operation.setTargetRatio(null);
		operation.setOriginRatio(null);
		if (!operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
				&& !operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode())
				&& !operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())){
			operation.setDeliveryFactor(null);
		}
		
		if(secCorpo!=null){
			secCorpo.setOldCirculationBalance(null);
			secCorpo.setOldShareBalance(null);
			secCorpo.setOldShareCapital(null);
			secCorpo.setNewCirculationBalance(null);
			secCorpo.setNewShareBalance(null);
			secCorpo.setNewShareCapital(null);
			secCorpo.setDeliveryFactor(null);
			secCorpo.setVariationAmount(null);
			secCorpo.setTargetRatio(null);
			secCorpo.setOriginRatio(null);
			secCorpo.setMarketDate(null);
			secCorpo.setMarketPrice(null);
		}
	}



	/**
	 * Metodo que carga datos del emisor.
	 */
	public void findIssuerInformationListener() {
		this.hideDialogs();
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			corporativeOperation.setIssuer( issuerHelperMgmt );
			this.findIssuerInformation(corporativeOperation);
		}
		if (isViewOperationModify()) {
			selectedCorporativeOpertation.setIssuer( issuerHelperMgmt );
			this.findIssuerInformation(selectedCorporativeOpertation);
		}

	}
	
	/**
	 * Metodo que valida emisor buscado.
	 */
	public void validateIssuerSearch(){
		cleanImportanceEventModel();
		IssuerSearcherTO issuerTO = new IssuerSearcherTO();
		if(issuerHelperSearch!=null && issuerHelperSearch.getIdIssuerPk()!=null){
			issuerTO.setIssuerCode(issuerHelperSearch.getIdIssuerPk());
			try{
				issuer = allComponenteFacade.findIssuerByCode(issuerTO);
				if (Validations.validateIsNotNull(issuer)){
					if (Validations.validateIsNotNull(issuer.getIssuerState())){
						if (!issuer.getIssuerState().equals(IssuerStateType.REGISTERED.getCode())){
							issuerHelperSearch=null;
							issuer=null;
							showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ISSUER_NOT_REGISTERED));
							JSFUtilities.showSimpleValidationDialog();
						}else{
							corporativeOperationTO.setIssuerCode(issuerHelperSearch.getIdIssuerPk());
						}
					}else{
						issuerHelperSearch=null;
						issuer=null;
						showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
								PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ISSUER_NOT_REGISTERED));
						JSFUtilities.showSimpleValidationDialog();
					}
				}else{
					issuerHelperSearch=null;
					showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ISSUER_NOT_EX));
					JSFUtilities.showSimpleValidationDialog();
				}
			}catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}
	/**
	 * Metodo que carga datos del emisor buscado.
	 * 
	 * @param operation
	 *            the operation
	 */
	private void findIssuerInformation(CorporativeOperation operation) {
		this.deleteSecurityInfo(operation,null);

		disableSourceSecuritiyHelper = Boolean.TRUE;
		disableTargetSecurityHelper = Boolean.TRUE;
		renderSecuritiesEsicion = Boolean.FALSE;
		JSFUtilities.hideGeneralDialogues();

		IssuerSearcherTO issuerTO = new IssuerSearcherTO();

		if (Validations.validateIsNotNullAndNotEmpty(operation.getIssuer().getIdIssuerPk())) {
			issuerTO.setIssuerCode(operation.getIssuer().getIdIssuerPk());
			try {
				issuer = allComponenteFacade.findIssuerByCode(issuerTO);
				if (Validations.validateIsNotNull(issuer)) {

					if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())){

					}else{
						sourceSecurity = null;
					}

					targeSecurity = null;
					operation.setSecurities(new Security());
					if (Validations.validateIsNotNull(issuer.getIssuerState())) {
						if (issuer.getIssuerState().equals(IssuerStateType.REGISTERED.getCode())) {
 							
							disableSourceSecuritiyHelper = Boolean.FALSE;
							disableTargetSecurityHelper = Boolean.FALSE;
							if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
								renderSecuritiesEsicion = Boolean.TRUE;
							}							
							// setear propiedades de helper de valores
							if (isViewOperationRegister()) {
								
								securityHelperBean.getSecuritySearchTO().setInstrumentType(instrumentType);
								securityHelperBean.changeInstrumentType();
							}
							if(Validations.validateIsNotNull(securitySourceHelperMgmt) && 
									Validations.validateIsNotNull(securitySourceHelperMgmt.getIdSecurityCodePk())){
								//Validate state, issuer relationship and instrument type
								tempSecurity = allComponenteFacade.getSecurityHelpServiceFacade(securitySourceHelperMgmt.getIdSecurityCodePk());
								if (this.validateSecurity(tempSecurity)) {
									operation.setSecurities(new Security());
									securitySourceHelperMgmt=new Security();
								}
							}
							if(Validations.validateIsNotNull(securityTargetHelperMgmt) && 
									Validations.validateIsNotNull(securityTargetHelperMgmt.getIdSecurityCodePk())){
								//Validate state, issuer relationship and instrument type
								tempSecurity = allComponenteFacade.getSecurityHelpServiceFacade(securityTargetHelperMgmt.getIdSecurityCodePk());
								if (this.validateSecurity(tempSecurity)) {
									operation.setTargetSecurity(new Security());
									securityTargetHelperMgmt=new Security();
								}
							}
							if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode()) ){
								disableTargetSecurityHelper = Boolean.TRUE;
							}
						} else {
							operation.setIssuer(new Issuer());
							operation.setSecurities(new Security());
							operation.setTargetSecurity(new Security());
							clearSecurityHelper(); 
							disableSourceSecuritiyHelper = Boolean.TRUE;
							disableTargetSecurityHelper = Boolean.TRUE;
							if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
								renderSecuritiesEsicion = Boolean.FALSE;
							}
							showMessageOnDialog(PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ALERT_MESSAGE),
									PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_ISSUER_NOT_REGISTERED));
							JSFUtilities.showSimpleValidationDialog();
						}
					} else {
						operation.setSecurities(new Security());
						operation.setTargetSecurity(new Security());
						clearSecurityHelper(); 
						disableSourceSecuritiyHelper = Boolean.TRUE;
						disableTargetSecurityHelper = Boolean.TRUE;
						if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
							renderSecuritiesEsicion = Boolean.FALSE;
						}						
					}
				}else{
					operation.setSecurities(new Security());
					operation.setTargetSecurity(new Security());
					clearSecurityHelper(); 
					if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
						renderSecuritiesEsicion = Boolean.FALSE;
					}
				}
			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}else{
			operation.setSecurities(new Security());
			operation.setTargetSecurity(new Security());
			clearSecurityHelper(); 
			if(operation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())){
				renderSecuritiesEsicion = Boolean.FALSE;
			}
		}
	}


	/**
	 * Metodo que declara y asigna variables para la pantalla de registro de hecho de importancia.
	 */
	public void loadRegisterInformation() {
		
		lstCboCorporativeEventMgmt=new ArrayList<ParameterTable>();
		mapSecuritiesCorporative = new HashMap<String, SecuritiesCorporative>();
		listSecuritiesTarget = new ArrayList<Security>();
		listSecuritiesTargetAux = new ArrayList<Security>();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		
		issuerHelperMgmt=new Issuer();
		securityHelperSearch=new Security();
		
		this.clearPanels();
		disableIssuer = Boolean.TRUE;
		disableTargetSecuritiesCombo = Boolean.TRUE;
		corporativeOperation = new CorporativeOperation();
		corporativeOperation.setIssuer(new Issuer());
		corporativeOperation.setSecurities(new Security());
		instrumentType = null;
		corporativeEventType = null;
 		this.initializeParameters(1);
		this.disableMotiveIncreaseVar();
		this.initializeCapitalIncreaseMotive();
		corporativeOperation.setCreationDate(CommonsUtilities.currentDate());
		corporativeOperation.setRegistryDate(CommonsUtilities.currentDate());
		corporativeOperation.setCutoffDate(corporativeOperation.getRegistryDate());

		disableSourceSecuritiyHelper = Boolean.TRUE;
		disableTargetSecurityHelper = Boolean.TRUE;
		this.clearHelpers();
		disableCurrency =Boolean.FALSE;
		increaseCapitalMotive = null;

		disableAddButton=Boolean.TRUE;
		corporativeOperationDataModel = new CorporativeOperationDataModel();
		securitiesCorporativeList =new ArrayList<SecuritiesCorporative>();
		selectedSecuritiesCorporative = null;

	}
	
	/**
	 * Gets the require target security.
	 *
	 * @return the require target security
	 */
	public String getRequireTargetSecurity() {
		return requireTargetSecurity;
	}

	/**
	 * Clear helpers.
	 */
	public void clearHelpers() {
		clearSecurityHelper();
	}

	/**
	 * Clear security helper.
	 */
	public void clearSecurityHelper(){
		securitySourceHelperMgmt=new Security();
		securityTargetHelperMgmt=new Security();
	}
	
	/**
	 * Clean importance event handler.
	 */
	public void cleanImportanceEventHandler(){
		this.hideDialogs();
		executeAction();
	}
	
	/**
	 * Metodo que limpia al finalizar el proceso de registro de hecho de importancia.
	 *
	 * @return the string
	 */
	public String finishRegister() {
		this.clearHelpers();
		this.init();
		renderFieldSet=Boolean.FALSE;
		listCorporativeEvents = null;
		renderDataTable=Boolean.FALSE;
		return "searchImportanceEvent";
	}

	/**
	 * Metodo que inicializa lista de combos necesarios para el registro de hecho de importancia.
	 *
	 * @param state the state
	 */
	public void initializeParameters(Integer state) {

		// create parameterTableTO's
		ParameterTableTO instrumentParamTo = new ParameterTableTO();
		ParameterTableTO typeAgreementParamTo = new ParameterTableTO();
		ParameterTableTO currencyTypeParamTo = new ParameterTableTO();
		ParameterTableTO increaseCapParamTo = new ParameterTableTO();
		ParameterTableTO deliveryPlaceParamTo = new ParameterTableTO();

		// Set parameterTableTO's values
		instrumentParamTo.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		typeAgreementParamTo.setMasterTableFk(MasterTableType.AGREEMENT_TYPE.getCode());
		currencyTypeParamTo.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		increaseCapParamTo.setMasterTableFk(MasterTableType.INCREASE_CAPITAL_MOTIVE.getCode());
		deliveryPlaceParamTo.setMasterTableFk(MasterTableType.DELIVERY_PLACE.getCode());
	


		if(state!=null){
			typeAgreementParamTo.setState(state);
		}

		listInstrumentType = this.getParameters(instrumentParamTo);
		listAgreementType = this.getParameters(typeAgreementParamTo);
		listCurrencyType = this.getParameters(currencyTypeParamTo);
		listCapitalIncreaseType = this.getParameters(increaseCapParamTo);
		listDeliveryPlacetype = this.getParameters(deliveryPlaceParamTo);

		listCorporativeEvenType = CorporativeEventType.list;
		listNegotiationRankType = NegotiationRankType.list;
	}

	/**
	 * Metodo que valida fechas para pago de remanente con dividendo en acciones.
	 *
	 * @param corporativeOperation the corporative operation
	 * @return true, if successful
	 */
	public boolean validateDateForPaymentRemanent(CorporativeOperation corporativeOperation){
		this.hideDialogs();
		if(corporativeOperation.getCorporativeEventType()!=null){
			if(corporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.REMANENT_PAYMENT.getCode())){
				if(importanceEvent!=null){
					if(importanceEvent.getRegistryDate()!=null && importanceEvent.getCutoffDate()!=null){
						if(corporativeOperation.getRegistryDate().compareTo(importanceEvent.getRegistryDate())==FundsType.EQUAL_DATE.getCode() &&
								corporativeOperation.getCutoffDate().compareTo(importanceEvent.getCutoffDate())==FundsType.EQUAL_DATE.getCode()){					
							return false;

						}
						else{
							Object[] bodyData= new Object[2];
							bodyData[0] = String.valueOf(importanceEvent.getIdCorporativeOperationPk());
							bodyData[1] = ImportanceEventType.ACTION_DIVIDENDS.getValue();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									            PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_PAYMENT_REMANENT_CUTOFF_DATE, bodyData));
							JSFUtilities.showSimpleValidationDialog();
						}
					}

				}

			}
		}

		return true;
	}

	/**
	 * Metodo que valida fechas de devoluviones de aportes con reduccion de capital/exclusion de valores.
	 *
	 * @param corporativeOperation the corporative operation
	 * @return true, if successful
	 */
	public boolean validateDateForReturnContribution(CorporativeOperation corporativeOperation){
		this.hideDialogs();
		if(corporativeOperation.getCorporativeEventType()!=null){
			if(corporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode())){
				if(importanceEvent!=null){
					if(importanceEvent.getRegistryDate()!=null && importanceEvent.getCutoffDate()!=null){
						if(corporativeOperation.getRegistryDate().compareTo(importanceEvent.getRegistryDate())==FundsType.EQUAL_DATE.getCode() &&
								corporativeOperation.getCutoffDate().compareTo(importanceEvent.getCutoffDate())==FundsType.EQUAL_DATE.getCode()){					
							return false;

						}
						else{
							Object[] bodyData= new Object[2];
							bodyData[0] = String.valueOf(importanceEvent.getIdCorporativeOperationPk());
							bodyData[1] = parametersTableMapEvent.get(importanceEvent.getCorporativeEventType().getCorporativeEventType());
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									            PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_PAYMENT_REMANENT_CUTOFF_DATE, bodyData));
							JSFUtilities.showSimpleValidationDialog();
						}
					}

				}

			}
		}

		return true;
	}

	/**
	 * Metodo que valida consurrencia por fechas.
	 */
	public void addRegistryDateFromCutOff() {
		this.hideDialogs();
		executeAction();
		resetComponent();
		if (isViewOperationRegister()) {
			if(validateDateForPaymentRemanent(corporativeOperation)){				
			}

			try{
				this.improtanceEventFacade.validateConcunrrency(corporativeOperation);
			}catch (ServiceException e) {
				if(e.getErrorService().equals(ErrorServiceType.CONCURRENCY_LOCK_UPDATE)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_CONCURRENCY));
					JSFUtilities.showComponent("importanceEventForm:cnfBacTransaction");
				}
			}
		}
		if (isViewOperationModify()) {
			modifyForExcision = Boolean.TRUE;
			if (selectedCorporativeOpertation.getCutoffDate() != null) {
				selectedCorporativeOpertation
				.setRegistryDate(selectedCorporativeOpertation
						.getCutoffDate());
			}
		}

	}

	/**
	 * Metodo que calcula fecha de corte.
	 */
	public void calculateCustoffDate() {
		this.hideDialogs();
		executeAction();
		resetComponent();
		this.hideDialogs();
		if (isViewOperationRegister()) {
			if (Validations.validateIsNotNull(corporativeOperation.getRegistryDate())) {
				if (Validations.validateIsNotNull(corporativeOperation.getCorporativeEventType())) {
					corporativeOperation.setCutoffDate(corporativeOperation.getRegistryDate());
					if (corporativeOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode())) {
						disableCutoffDate = Boolean.TRUE;					}

					try{
						this.improtanceEventFacade.validateConcunrrency(corporativeOperation);
					}catch (ServiceException e) {
						if(e.getErrorService().equals(ErrorServiceType.CONCURRENCY_LOCK_UPDATE)){
							corporativeOperation.setCutoffDate(null);
							corporativeOperation.setSecurities(new Security());
							securitySourceHelperMgmt=new Security();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getMessage(PropertiesConstants.IMPORTANCE_EVENT_CONCURRENCY));
							JSFUtilities.showSimpleValidationDialog();
						}

					}
				}
			}
		}
		if (isViewOperationModify()) {
			modifyForExcision = Boolean.TRUE;
			if (Validations.validateIsNotNull(selectedCorporativeOpertation.getRegistryDate())) {
				selectedCorporativeOpertation.setCutoffDate(selectedCorporativeOpertation.getRegistryDate());
				if (selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode())) {
					disableCutoffDate = Boolean.TRUE;
				}
			}
		}
	}

	/**
	 * Metodo que limpia combo evento corporativo.
	 */
	public void changeCboInstrumentType(){
		try {
			corporativeOperationTO.setCorporativeEventType(null);

		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Metodo que limpia y carga combos dependientes con respecto al tipo de evento.
	 */
	public void changeCboInstrumentTypeSearch(){
		cleanImportanceEventModel();
		if(corporativeOperationTO.getInstrumentType()!=null && 
				Validations.validateIsNotNullAndPositive(corporativeOperationTO.getInstrumentType())){
			corporativeOperationTO.setCorporativeEventType(null);
			corporativeOperationTO.setCorporativeEvent(null);		
			loadCboCorporativeEventClass();
			listCorporativeEvents=new ArrayList<ParameterTable>();
		}else{
			corporativeOperationTO.setCorporativeEventType(null);
			corporativeOperationTO.setCorporativeEvent(null);		
			lstCboCorporativeEvenClass = new ArrayList<ParameterTable>();
			listCorporativeEvents=new ArrayList<ParameterTable>();
		}		
	}
	
	/**
	 * Metodo que limpia y carga objetos dependientes con respecto a la clase de evento.
	 */
	public void changeCboCorpEventClassSearch(){
		cleanImportanceEventModel();
		if(corporativeOperationTO.getCorporativeEventType()!=null && 
				Validations.validateIsNotNullAndPositive(corporativeOperationTO.getCorporativeEventType())){
			ParameterTableTO paramTO = new ParameterTableTO();
			paramTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTO.setMasterTableFk(MasterTableType.CORPORATIVE_EVENT.getCode());
			paramTO.setIdRelatedParameterFk( corporativeOperationTO.getCorporativeEventType() );
			
			if (Validations.validateIsNotNull(corporativeOperationTO.getInstrumentType())) {
				// determine instrument type
				if (corporativeOperationTO.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())) {
					paramTO.setIndicator5(BooleanType.YES.getCode());
				} else if (corporativeOperationTO.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())) {
					paramTO.setIndicator4(BooleanType.YES.getCode());
					if(corporativeOperationTO.getCorporativeEventType().equals(CorporativeEventType.RIGHTS_TO_DELIVER.getCode())){
						paramTO.setState(GeneralConstants.TWO_VALUE_INTEGER);
					}
				}else if(corporativeOperationTO.getInstrumentType().equals(InstrumentType.MIXED_INCOME.getCode())){
					paramTO.setIndicator6(BooleanType.YES.getCode());
				}
			}
			listCorporativeEvents = this.getParameters(paramTO);
		}else{			
			corporativeOperationTO.setCorporativeEvent(null);					
			listCorporativeEvents=new ArrayList<ParameterTable>();
		} 		
	}
	
	
	
	/**
	 * Metodo que carga clase de evento corporativo.
	 */
	public void loadCboCorporativeEventClass(){
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CORPORATIVE_EVENT_CLASS.getCode() );
		lstCboCorporativeEvenClass=getParameters(parameterTableTO);
	}
	
	/**
	 * Metodo que carga tipo de evento corporativo.
	 */
	public void loadCorporativeEventsParams(){
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CORPORATIVE_EVENT.getCode() );
		List<ParameterTable> lstParameters = getParameters(parameterTableTO);
		
		billParametersTableMap(lstParameters,true);
	}
	
	/**
	 * Metodo que retorna lista de objetos.
	 * 
	 * @param filter
	 *            the filter
	 * @return the parameters
	 */
	private List<ParameterTable> getParameters(ParameterTableTO filter) {
		try {
			return generalParametersFacade.getListParameterTableServiceBean(filter);
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
		return null;

	}

	/**
	 * Instantiates a new importance event bean.
	 */
	public ImportanceEventBean() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the importance event.
	 * 
	 * @return the importance event
	 */
	public CorporativeOperation getImportanceEvent() {
		return importanceEvent;
	}

	/**
	 * Sets the importance event.
	 * 
	 * @param importanceEvent
	 *            the new importance event
	 */
	public void setImportanceEvent(CorporativeOperation importanceEvent) {
		this.importanceEvent = importanceEvent;
	}

	/**
	 * Gets the instrument type.
	 * 
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 * 
	 * @param instrumentType
	 *            the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the list instrument type.
	 * 
	 * @return the list instrument type
	 */
	public List<ParameterTable> getListInstrumentType() {
		return listInstrumentType;
	}

	/**
	 * Sets the list instrument type.
	 * 
	 * @param listInstrumentType
	 *            the new list instrument type
	 */
	public void setListInstrumentType(List<ParameterTable> listInstrumentType) {
		this.listInstrumentType = listInstrumentType;
	}

	/**
	 * Gets the corporative event type.
	 * 
	 * @return the corporative event type
	 */
	public Integer getCorporativeEventType() {
		return corporativeEventType;
	}

	/**
	 * Sets the corporative event type.
	 * 
	 * @param corporativeEventType
	 *            the new corporative event type
	 */
	public void setCorporativeEventType(Integer corporativeEventType) {
		this.corporativeEventType = corporativeEventType;
	}

	/**
	 * Gets the list corporative events.
	 * 
	 * @return the list corporative events
	 */
	public List<ParameterTable> getListCorporativeEvents() {
		return listCorporativeEvents;
	}

	/**
	 * Sets the list corporative events.
	 * 
	 * @param listCorporativeEvents
	 *            the new list corporative events
	 */
	public void setListCorporativeEvents(
			List<ParameterTable> listCorporativeEvents) {
		this.listCorporativeEvents = listCorporativeEvents;
	}

	/**
	 * Gets the list corporative even type.
	 * 
	 * @return the list corporative even type
	 */
	public List<CorporativeEventType> getListCorporativeEvenType() {
		return listCorporativeEvenType;
	}

	/**
	 * Sets the list corporative even type.
	 * 
	 * @param listCorporativeEvenType
	 *            the new list corporative even type
	 */
	public void setListCorporativeEvenType(
			List<CorporativeEventType> listCorporativeEvenType) {
		this.listCorporativeEvenType = listCorporativeEvenType;
	}

	/**
	 * Gets the list target securities.
	 * 
	 * @return the list target securities
	 */
	public List<SecuritiesTO> getListTargetSecurities() {
		return listTargetSecurities;
	}

	/**
	 * Sets the list target securities.
	 * 
	 * @param listTargetSecurities
	 *            the new list target securities
	 */
	public void setListTargetSecurities(List<SecuritiesTO> listTargetSecurities) {
		this.listTargetSecurities = listTargetSecurities;
	}

	/**
	 * Gets the corporative operation.
	 * 
	 * @return the corporative operation
	 */
	public CorporativeOperation getCorporativeOperation() {
		return corporativeOperation;
	}

	/**
	 * Sets the corporative operation.
	 * 
	 * @param corporativeOperation
	 *            the new corporative operation
	 */
	public void setCorporativeOperation(
			CorporativeOperation corporativeOperation) {
		this.corporativeOperation = corporativeOperation;
	}

	/**
	 * Gets the list currency type.
	 * 
	 * @return the list currency type
	 */
	public List<ParameterTable> getListCurrencyType() {
		return listCurrencyType;
	}

	/**
	 * Sets the list currency type.
	 * 
	 * @param listCurrencyType
	 *            the new list currency type
	 */
	public void setListCurrencyType(List<ParameterTable> listCurrencyType) {
		this.listCurrencyType = listCurrencyType;
	}

	/**
	 * Gets the list capital increase type.
	 * 
	 * @return the list capital increase type
	 */
	public List<ParameterTable> getListCapitalIncreaseType() {
		return listCapitalIncreaseType;
	}

	/**
	 * Sets the list capital increase type.
	 * 
	 * @param listCapitalIncreaseType
	 *            the new list capital increase type
	 */
	public void setListCapitalIncreaseType(
			List<ParameterTable> listCapitalIncreaseType) {
		this.listCapitalIncreaseType = listCapitalIncreaseType;
	}

	/**
	 * Gets the render target security.
	 * 
	 * @return the render target security
	 */
	public Boolean getRenderTargetSecurity() {
		return renderTargetSecurity;
	}

	/**
	 * Sets the render target security.
	 * 
	 * @param renderTargetSecurity
	 *            the new render target security
	 */
	public void setRenderTargetSecurity(Boolean renderTargetSecurity) {
		this.renderTargetSecurity = renderTargetSecurity;
	}

	/**
	 * Gets the render actions dividends.
	 * 
	 * @return the render actions dividends
	 */
	public Boolean getRenderActionsDividends() {
		return renderSecurityDescription;
	}

	/**
	 * Sets the render actions dividends.
	 * 
	 * @param renderActionsDividends
	 *            the new render actions dividends
	 */
	public void setRenderActionsDividends(Boolean renderActionsDividends) {
		this.renderSecurityDescription = renderActionsDividends;
	}

	/**
	 * Gets the issuer.
	 * 
	 * @return the issuer
	 */
	public IssuerTO getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 * 
	 * @param issuer
	 *            the new issuer
	 */
	public void setIssuer(IssuerTO issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the list agreement type.
	 * 
	 * @return the list agreement type
	 */
	public List<ParameterTable> getListAgreementType() {
		return listAgreementType;
	}

	/**
	 * Sets the list agreement type.
	 * 
	 * @param listAgreementType
	 *            the new list agreement type
	 */
	public void setListAgreementType(List<ParameterTable> listAgreementType) {
		this.listAgreementType = listAgreementType;
	}

	/**
	 * Gets the list delivery placetype.
	 * 
	 * @return the list delivery placetype
	 */
	public List<ParameterTable> getListDeliveryPlacetype() {
		return listDeliveryPlacetype;
	}

	/**
	 * Sets the list delivery placetype.
	 * 
	 * @param listDeliveryPlacetype
	 *            the new list delivery placetype
	 */
	public void setListDeliveryPlacetype(
			List<ParameterTable> listDeliveryPlacetype) {
		this.listDeliveryPlacetype = listDeliveryPlacetype;
	}

	/**
	 * Gets the lst target securities.
	 * 
	 * @return the lst target securities
	 */
	public List<Security> getLstTargetSecurities() {
		return lstTargetSecurities;
	}

	/**
	 * Sets the lst target securities.
	 * 
	 * @param lstTargetSecurities
	 *            the new lst target securities
	 */
	public void setLstTargetSecurities(List<Security> lstTargetSecurities) {
		this.lstTargetSecurities = lstTargetSecurities;
	}

	/**
	 * Gets the render cap var description.
	 * 
	 * @return the render cap var description
	 */
	public Boolean getRenderCapVarDescription() {
		return renderCapVarDescription;
	}

	/**
	 * Sets the render cap var description.
	 * 
	 * @param renderCapVarDescription
	 *            the new render cap var description
	 */
	public void setRenderCapVarDescription(Boolean renderCapVarDescription) {
		this.renderCapVarDescription = renderCapVarDescription;
	}

	/**
	 * Gets the render nominal value description.
	 * 
	 * @return the render nominal value description
	 */
	public Boolean getRenderNominalValueDescription() {
		return renderNominalValueDescription;
	}

	/**
	 * Sets the render nominal value description.
	 * 
	 * @param renderNominalValueDescription
	 *            the new render nominal value description
	 */
	public void setRenderNominalValueDescription(
			Boolean renderNominalValueDescription) {
		this.renderNominalValueDescription = renderNominalValueDescription;
	}

	/**
	 * Gets the render security description.
	 * 
	 * @return the render security description
	 */
	public Boolean getRenderSecurityDescription() {
		return renderSecurityDescription;
	}

	/**
	 * Sets the render security description.
	 * 
	 * @param renderSecurityDescription
	 *            the new render security description
	 */
	public void setRenderSecurityDescription(Boolean renderSecurityDescription) {
		this.renderSecurityDescription = renderSecurityDescription;
	}

	/**
	 * Gets the render output panel.
	 * 
	 * @return the render output panel
	 */
	public Boolean getRenderOutputPanel() {
		return renderOutputPanel;
	}

	/**
	 * Sets the render output panel.
	 * 
	 * @param renderOutputPanel
	 *            the new render output panel
	 */
	public void setRenderOutputPanel(Boolean renderOutputPanel) {
		this.renderOutputPanel = renderOutputPanel;
	}

	/**
	 * Gets the disable target securities combo.
	 * 
	 * @return the disable target securities combo
	 */
	public Boolean getDisableTargetSecuritiesCombo() {
		return disableTargetSecuritiesCombo;
	}

	/**
	 * Sets the disable target securities combo.
	 * 
	 * @param disableTargetSecuritiesCombo
	 *            the new disable target securities combo
	 */
	public void setDisableTargetSecuritiesCombo(
			Boolean disableTargetSecuritiesCombo) {
		this.disableTargetSecuritiesCombo = disableTargetSecuritiesCombo;
	}

	/**
	 * Gets the render sharecapital.
	 * 
	 * @return the render sharecapital
	 */
	public Boolean getRenderSharecapital() {
		return renderSharecapital;
	}

	/**
	 * Sets the render sharecapital.
	 * 
	 * @param renderSharecapital
	 *            the new render sharecapital
	 */
	public void setRenderSharecapital(Boolean renderSharecapital) {
		this.renderSharecapital = renderSharecapital;
	}

	/**
	 * Gets the render ind remanent.
	 * 
	 * @return the render ind remanent
	 */
	public Boolean getRenderIndRemanent() {
		return renderIndRemanent;
	}

	/**
	 * Sets the render ind remanent.
	 * 
	 * @param renderIndRemanent
	 *            the new render ind remanent
	 */
	public void setRenderIndRemanent(Boolean renderIndRemanent) {
		this.renderIndRemanent = renderIndRemanent;
	}

	/**
	 * Gets the render exchange relationship.
	 * 
	 * @return the render exchange relationship
	 */
	public Boolean getRenderExchangeRelationship() {
		return renderExchangeRelationship;
	}

	/**
	 * Sets the render exchange relationship.
	 * 
	 * @param renderExchangeRelationship
	 *            the new render exchange relationship
	 */
	public void setRenderExchangeRelationship(Boolean renderExchangeRelationship) {
		this.renderExchangeRelationship = renderExchangeRelationship;
	}

	/**
	 * Gets the disable share balance.
	 * 
	 * @return the disable share balance
	 */
	public Boolean getDisableShareBalance() {
		return disableShareBalance;
	}

	/**
	 * Sets the disable share balance.
	 * 
	 * @param disableShareBalance
	 *            the new disable share balance
	 */
	public void setDisableShareBalance(Boolean disableShareBalance) {
		this.disableShareBalance = disableShareBalance;
	}

	/**
	 * Gets the render opnl target security.
	 * 
	 * @return the render opnl target security
	 */
	public Boolean getRenderOpnlTargetSecurity() {
		return renderOpnlTargetSecurity;
	}

	/**
	 * Sets the render opnl target security.
	 * 
	 * @param renderOpnlTargetSecurity
	 *            the new render opnl target security
	 */
	public void setRenderOpnlTargetSecurity(Boolean renderOpnlTargetSecurity) {
		this.renderOpnlTargetSecurity = renderOpnlTargetSecurity;
	}

	/**
	 * Gets the render nominal value no cap var.
	 * 
	 * @return the render nominal value no cap var
	 */
	public Boolean getRenderNominalValueNoCapVar() {
		return renderNominalValueNoCapVar;
	}

	/**
	 * Sets the render nominal value no cap var.
	 * 
	 * @param renderNominalValueNoCapVar
	 *            the new render nominal value no cap var
	 */
	public void setRenderNominalValueNoCapVar(Boolean renderNominalValueNoCapVar) {
		this.renderNominalValueNoCapVar = renderNominalValueNoCapVar;
	}

	/**
	 * Gets the render nominal value cap var.
	 * 
	 * @return the render nominal value cap var
	 */
	public Boolean getRenderNominalValueCapVar() {
		return renderNominalValueCapVar;
	}

	/**
	 * Sets the render nominal value cap var.
	 * 
	 * @param renderNominalValueCapVar
	 *            the new render nominal value cap var
	 */
	public void setRenderNominalValueCapVar(Boolean renderNominalValueCapVar) {
		this.renderNominalValueCapVar = renderNominalValueCapVar;
	}

	/**
	 * Gets the render ind tax factor.
	 * 
	 * @return the render ind tax factor
	 */
	public Boolean getRenderIndTaxFactor() {
		return renderIndTaxFactor;
	}

	/**
	 * Sets the render ind tax factor.
	 * 
	 * @param renderIndTaxFactor
	 *            the new render ind tax factor
	 */
	public void setRenderIndTaxFactor(Boolean renderIndTaxFactor) {
		this.renderIndTaxFactor = renderIndTaxFactor;
	}

	/**
	 * Gets the render cap increase motive.
	 * 
	 * @return the render cap increase motive
	 */
	public Boolean getRenderCapIncreaseMotive() {
		return renderCapIncreaseMotive;
	}

	/**
	 * Sets the render cap increase motive.
	 * 
	 * @param renderCapIncreaseMotive
	 *            the new render cap increase motive
	 */
	public void setRenderCapIncreaseMotive(Boolean renderCapIncreaseMotive) {
		this.renderCapIncreaseMotive = renderCapIncreaseMotive;
	}

	/**
	 * Gets the render delivery factor.
	 * 
	 * @return the render delivery factor
	 */
	public Boolean getRenderDeliveryFactor() {
		return renderDeliveryFactor;
	}

	/**
	 * Sets the render delivery factor.
	 * 
	 * @param renderDeliveryFactor
	 *            the new render delivery factor
	 */
	public void setRenderDeliveryFactor(Boolean renderDeliveryFactor) {
		this.renderDeliveryFactor = renderDeliveryFactor;
	}

	/**
	 * Gets the render preferent suscription panel.
	 * 
	 * @return the render preferent suscription panel
	 */
	public Boolean getRenderPreferentSuscriptionPanel() {
		return renderPreferentSuscriptionPanel;
	}

	/**
	 * Sets the render preferent suscription panel.
	 * 
	 * @param renderPreferentSuscriptionPanel
	 *            the new render preferent suscription panel
	 */
	public void setRenderPreferentSuscriptionPanel(
			Boolean renderPreferentSuscriptionPanel) {
		this.renderPreferentSuscriptionPanel = renderPreferentSuscriptionPanel;
	}

	/**
	 * Gets the render currency type.
	 * 
	 * @return the render currency type
	 */
	public Boolean getRenderCurrencyType() {
		return renderCurrencyType;
	}

	/**
	 * Sets the render currency type.
	 * 
	 * @param renderCurrencyType
	 *            the new render currency type
	 */
	public void setRenderCurrencyType(Boolean renderCurrencyType) {
		this.renderCurrencyType = renderCurrencyType;
	}

	/**
	 * Gets the disable cutoff date.
	 * 
	 * @return the disable cutoff date
	 */
	public Boolean getDisableCutoffDate() {
		return disableCutoffDate;
	}

	/**
	 * Sets the disable cutoff date.
	 * 
	 * @param disableCutoffDate
	 *            the new disable cutoff date
	 */
	public void setDisableCutoffDate(Boolean disableCutoffDate) {
		this.disableCutoffDate = disableCutoffDate;
	}

	/**
	 * Gets the render remaining cash payment panel.
	 * 
	 * @return the render remaining cash payment panel
	 */
	public Boolean getRenderRemainingCashPaymentPanel() {
		return renderRemainingCashPaymentPanel;
	}

	/**
	 * Sets the render remaining cash payment panel.
	 * 
	 * @param renderRemainingCashPaymentPanel
	 *            the new render remaining cash payment panel
	 */
	public void setRenderRemainingCashPaymentPanel(
			Boolean renderRemainingCashPaymentPanel) {
		this.renderRemainingCashPaymentPanel = renderRemainingCashPaymentPanel;
	}

	/**
	 * Gets the render tax factor.
	 * 
	 * @return the render tax factor
	 */
	public Boolean getRenderTaxFactor() {
		return renderTaxFactor;
	}

	/**
	 * Sets the render tax factor.
	 * 
	 * @param renderTaxFactor
	 *            the new render tax factor
	 */
	public void setRenderTaxFactor(Boolean renderTaxFactor) {
		this.renderTaxFactor = renderTaxFactor;
	}

	/**
	 * Gets the list negotiation rank type.
	 * 
	 * @return the list negotiation rank type
	 */
	public List<NegotiationRankType> getListNegotiationRankType() {
		return listNegotiationRankType;
	}

	/**
	 * Sets the list negotiation rank type.
	 * 
	 * @param listNegotiationRankType
	 *            the new list negotiation rank type
	 */
	public void setListNegotiationRankType(
			List<NegotiationRankType> listNegotiationRankType) {
		this.listNegotiationRankType = listNegotiationRankType;
	}

	/**
	 * Gets the render first negotiation date.
	 * 
	 * @return the render first negotiation date
	 */
	public Boolean getRenderFirstNegotiationDate() {
		return renderFirstNegotiationDate;
	}

	/**
	 * Sets the render first negotiation date.
	 * 
	 * @param renderFirstNegotiationDate
	 *            the new render first negotiation date
	 */
	public void setRenderFirstNegotiationDate(Boolean renderFirstNegotiationDate) {
		this.renderFirstNegotiationDate = renderFirstNegotiationDate;
	}

	/**
	 * Gets the render second negotiation date.
	 * 
	 * @return the render second negotiation date
	 */
	public Boolean getRenderSecondNegotiationDate() {
		return renderSecondNegotiationDate;
	}

	/**
	 * Sets the render second negotiation date.
	 * 
	 * @param renderSecondNegotiationDate
	 *            the new render second negotiation date
	 */
	public void setRenderSecondNegotiationDate(
			Boolean renderSecondNegotiationDate) {
		this.renderSecondNegotiationDate = renderSecondNegotiationDate;
	}

	/**
	 * Gets the render third negotiation date.
	 * 
	 * @return the render third negotiation date
	 */
	public Boolean getRenderThirdNegotiationDate() {
		return renderThirdNegotiationDate;
	}

	/**
	 * Sets the render third negotiation date.
	 * 
	 * @param renderThirdNegotiationDate
	 *            the new render third negotiation date
	 */
	public void setRenderThirdNegotiationDate(Boolean renderThirdNegotiationDate) {
		this.renderThirdNegotiationDate = renderThirdNegotiationDate;
	}

	/**
	 * Gets the end suscription.
	 * 
	 * @return the end suscription
	 */
	public Boolean getEndSuscription() {
		return endSuscription;
	}

	/**
	 * Sets the end suscription.
	 * 
	 * @param endSuscription
	 *            the new end suscription
	 */
	public void setEndSuscription(Boolean endSuscription) {
		this.endSuscription = endSuscription;
	}

	/**
	 * Gets the negotiation rank.
	 * 
	 * @return the negotiation rank
	 */
	public Integer getNegotiationRank() {
		return negotiationRank;
	}

	/**
	 * Sets the negotiation rank.
	 * 
	 * @param negotiationRank
	 *            the new negotiation rank
	 */
	public void setNegotiationRank(Integer negotiationRank) {
		this.negotiationRank = negotiationRank;
	}

	/**
	 * Gets the corporative operation to.
	 * 
	 * @return the corporative operation to
	 */
	public CorporativeOperationTO getCorporativeOperationTO() {
		return corporativeOperationTO;
	}

	/**
	 * Sets the corporative operation to.
	 * 
	 * @param corporativeOperationTO
	 *            the new corporative operation to
	 */
	public void setCorporativeOperationTO(
			CorporativeOperationTO corporativeOperationTO) {
		this.corporativeOperationTO = corporativeOperationTO;
	}

	/**
	 * Gets the list corporative process state type.
	 * 
	 * @return the list corporative process state type
	 */
	public List<ParameterTable> getListCorporativeProcessStateType() {
		return listCorporativeProcessStateType;
	}

	/**
	 * Sets the list corporative process state type.
	 * 
	 * @param listCorporativeProcessStateType
	 *            the new list corporative process state type
	 */
	public void setListCorporativeProcessStateType(
			List<ParameterTable> listCorporativeProcessStateType) {
		this.listCorporativeProcessStateType = listCorporativeProcessStateType;
	}

	/**
	 * Gets the list corporative operation.
	 * 
	 * @return the list corporative operation
	 */
	public List<CorporativeOperation> getListCorporativeOperation() {
		return listCorporativeOperation;
	}

	/**
	 * Sets the list corporative operation.
	 * 
	 * @param listCorporativeOperation
	 *            the new list corporative operation
	 */
	public void setListCorporativeOperation(
			List<CorporativeOperation> listCorporativeOperation) {
		this.listCorporativeOperation = listCorporativeOperation;
	}

	/**
	 * Gets the corporative operation data model.
	 * 
	 * @return the corporative operation data model
	 */
	public CorporativeOperationDataModel getCorporativeOperationDataModel() {
		return corporativeOperationDataModel;
	}

	/**
	 * Sets the corporative operation data model.
	 * 
	 * @param corporativeOperationDataModel
	 *            the new corporative operation data model
	 */
	public void setCorporativeOperationDataModel(
			CorporativeOperationDataModel corporativeOperationDataModel) {
		this.corporativeOperationDataModel = corporativeOperationDataModel;
	}

	/**
	 * Gets the selected corporative opertation.
	 * 
	 * @return the selected corporative opertation
	 */
	public CorporativeOperation getSelectedCorporativeOpertation() {
		return selectedCorporativeOpertation;
	}

	/**
	 * Sets the selected corporative opertation.
	 * 
	 * @param selectedCorporativeOpertation
	 *            the new selected corporative opertation
	 */
	public void setSelectedCorporativeOpertation(
			CorporativeOperation selectedCorporativeOpertation) {
		System.out.println("Set selected operation");
		this.selectedCorporativeOpertation = selectedCorporativeOpertation;
	}

	/**
	 * Gets the render data table.
	 * 
	 * @return the render data table
	 */
	public Boolean getRenderDataTable() {
		return renderDataTable;
	}

	/**
	 * Sets the render data table.
	 * 
	 * @param renderDataTable
	 *            the new render data table
	 */
	public void setRenderDataTable(Boolean renderDataTable) {
		this.renderDataTable = renderDataTable;
	}

	/**
	 * Gets the disable modify button.
	 * 
	 * @return the disable modify button
	 */
	public Boolean getDisableModifyButton() {
		return disableModifyButton;
	}

	/**
	 * Sets the disable modify button.
	 * 
	 * @param disableModifyButton
	 *            the new disable modify button
	 */
	public void setDisableModifyButton(Boolean disableModifyButton) {
		this.disableModifyButton = disableModifyButton;
	}

	/**
	 * Gets the event type description.
	 * 
	 * @return the event type description
	 */
	public String getEventTypeDescription() {
		return eventTypeDescription;
	}

	/**
	 * Sets the event type description.
	 * 
	 * @param eventTypeDescription
	 *            the new event type description
	 */
	public void setEventTypeDescription(String eventTypeDescription) {
		this.eventTypeDescription = eventTypeDescription;
	}

	/**
	 * Gets the disable annular button.
	 * 
	 * @return the disable annular button
	 */
	public Boolean getDisableAnnularButton() {
		return disableAnnularButton;
	}

	/**
	 * Sets the disable annular button.
	 * 
	 * @param disableAnnularButton
	 *            the new disable annular button
	 */
	public void setDisableAnnularButton(Boolean disableAnnularButton) {
		this.disableAnnularButton = disableAnnularButton;
	}

	/**
	 * Gets the disable revaluation exedent.
	 * 
	 * @return the disable revaluation exedent
	 */
	public Boolean getDisableRevaluationExedent() {
		return disableRevaluationExedent;
	}

	/**
	 * Sets the disable revaluation exedent.
	 * 
	 * @param disableRevaluationExedent
	 *            the new disable revaluation exedent
	 */
	public void setDisableRevaluationExedent(Boolean disableRevaluationExedent) {
		this.disableRevaluationExedent = disableRevaluationExedent;
	}

	/**
	 * Gets the disable capital reexpresion.
	 * 
	 * @return the disable capital reexpresion
	 */
	public Boolean getDisableCapitalReexpresion() {
		return disableCapitalReexpresion;
	}

	/**
	 * Sets the disable capital reexpresion.
	 * 
	 * @param disableCapitalReexpresion
	 *            the new disable capital reexpresion
	 */
	public void setDisableCapitalReexpresion(Boolean disableCapitalReexpresion) {
		this.disableCapitalReexpresion = disableCapitalReexpresion;
	}

	/**
	 * Gets the disable reserve capitalization.
	 * 
	 * @return the disable reserve capitalization
	 */
	public Boolean getDisableReserveCapitalization() {
		return disableReserveCapitalization;
	}

	/**
	 * Sets the disable reserve capitalization.
	 * 
	 * @param disableReserveCapitalization
	 *            the new disable reserve capitalization
	 */
	public void setDisableReserveCapitalization(
			Boolean disableReserveCapitalization) {
		this.disableReserveCapitalization = disableReserveCapitalization;
	}

	/**
	 * Gets the disable uitilities capitalization.
	 * 
	 * @return the disable uitilities capitalization
	 */
	public Boolean getDisableUitilitiesCapitalization() {
		return disableUitilitiesCapitalization;
	}

	/**
	 * Sets the disable uitilities capitalization.
	 * 
	 * @param disableUitilitiesCapitalization
	 *            the new disable uitilities capitalization
	 */
	public void setDisableUitilitiesCapitalization(
			Boolean disableUitilitiesCapitalization) {
		this.disableUitilitiesCapitalization = disableUitilitiesCapitalization;
	}

	/**
	 * Gets the disable fusion.
	 * 
	 * @return the disable fusion
	 */
	public Boolean getDisableFusion() {
		return disableFusion;
	}

	/**
	 * Sets the disable fusion.
	 * 
	 * @param disableFusion
	 *            the new disable fusion
	 */
	public void setDisableFusion(Boolean disableFusion) {
		this.disableFusion = disableFusion;
	}

	/**
	 * Gets the disable result inflation.
	 * 
	 * @return the disable result inflation
	 */
	public Boolean getDisableResultInflation() {
		return disableResultInflation;
	}

	/**
	 * Sets the disable result inflation.
	 * 
	 * @param disableResultInflation
	 *            the new disable result inflation
	 */
	public void setDisableResultInflation(Boolean disableResultInflation) {
		this.disableResultInflation = disableResultInflation;
	}

	/**
	 * Gets the disable others.
	 * 
	 * @return the disable others
	 */
	public Boolean getDisableOthers() {
		return disableOthers;
	}

	/**
	 * Sets the disable others.
	 * 
	 * @param disableOthers
	 *            the new disable others
	 */
	public void setDisableOthers(Boolean disableOthers) {
		this.disableOthers = disableOthers;
	}

	/**
	 * Gets the voluntier revaluation.
	 * 
	 * @return the voluntier revaluation
	 */
	public Boolean getVoluntierRevaluation() {
		return voluntierRevaluation;
	}

	/**
	 * Sets the voluntier revaluation.
	 * 
	 * @param voluntierRevaluation
	 *            the new voluntier revaluation
	 */
	public void setVoluntierRevaluation(Boolean voluntierRevaluation) {
		this.voluntierRevaluation = voluntierRevaluation;
	}

	/**
	 * Gets the improtance event facade.
	 * 
	 * @return the improtance event facade
	 */
	public ImportanceEventFacade getImprotanceEventFacade() {
		return improtanceEventFacade;
	}

	/**
	 * Sets the improtance event facade.
	 * 
	 * @param improtanceEventFacade
	 *            the new improtance event facade
	 */
	public void setImprotanceEventFacade(
			ImportanceEventFacade improtanceEventFacade) {
		this.improtanceEventFacade = improtanceEventFacade;
	}

	/**
	 * Gets the revaluation excedent motive.
	 * 
	 * @return the revaluation excedent motive
	 */
	public CapitalIncreaseMotive getRevaluationExcedentMotive() {
		return revaluationExcedentMotive;
	}

	/**
	 * Sets the revaluation excedent motive.
	 * 
	 * @param revaluationExcedentMotive
	 *            the new revaluation excedent motive
	 */
	public void setRevaluationExcedentMotive(
			CapitalIncreaseMotive revaluationExcedentMotive) {
		this.revaluationExcedentMotive = revaluationExcedentMotive;
	}

	/**
	 * Gets the capital reexpresion motive.
	 * 
	 * @return the capital reexpresion motive
	 */
	public CapitalIncreaseMotive getCapitalReexpresionMotive() {
		return capitalReexpresionMotive;
	}

	/**
	 * Sets the capital reexpresion motive.
	 * 
	 * @param capitalReexpresionMotive
	 *            the new capital reexpresion motive
	 */
	public void setCapitalReexpresionMotive(
			CapitalIncreaseMotive capitalReexpresionMotive) {
		this.capitalReexpresionMotive = capitalReexpresionMotive;
	}

	/**
	 * Gets the reserve capitalization motive.
	 * 
	 * @return the reserve capitalization motive
	 */
	public CapitalIncreaseMotive getReserveCapitalizationMotive() {
		return reserveCapitalizationMotive;
	}

	/**
	 * Sets the reserve capitalization motive.
	 * 
	 * @param reserveCapitalizationMotive
	 *            the new reserve capitalization motive
	 */
	public void setReserveCapitalizationMotive(
			CapitalIncreaseMotive reserveCapitalizationMotive) {
		this.reserveCapitalizationMotive = reserveCapitalizationMotive;
	}

	/**
	 * Gets the utilities capitalization motive.
	 * 
	 * @return the utilities capitalization motive
	 */
	public CapitalIncreaseMotive getUtilitiesCapitalizationMotive() {
		return utilitiesCapitalizationMotive;
	}

	/**
	 * Sets the utilities capitalization motive.
	 * 
	 * @param utilitiesCapitalizationMotive
	 *            the new utilities capitalization motive
	 */
	public void setUtilitiesCapitalizationMotive(
			CapitalIncreaseMotive utilitiesCapitalizationMotive) {
		this.utilitiesCapitalizationMotive = utilitiesCapitalizationMotive;
	}

	/**
	 * Gets the fusion motive.
	 * 
	 * @return the fusion motive
	 */
	public CapitalIncreaseMotive getFusionMotive() {
		return fusionMotive;
	}

	/**
	 * Sets the fusion motive.
	 * 
	 * @param fusionMotive
	 *            the new fusion motive
	 */
	public void setFusionMotive(CapitalIncreaseMotive fusionMotive) {
		this.fusionMotive = fusionMotive;
	}

	/**
	 * Gets the result exposition motive.
	 * 
	 * @return the result exposition motive
	 */
	public CapitalIncreaseMotive getResultExpositionMotive() {
		return resultExpositionMotive;
	}

	/**
	 * Sets the result exposition motive.
	 * 
	 * @param resultExpositionMotive
	 *            the new result exposition motive
	 */
	public void setResultExpositionMotive(
			CapitalIncreaseMotive resultExpositionMotive) {
		this.resultExpositionMotive = resultExpositionMotive;
	}

	/**
	 * Gets the others motive.
	 * 
	 * @return the others motive
	 */
	public CapitalIncreaseMotive getOthersMotive() {
		return othersMotive;
	}

	/**
	 * Sets the others motive.
	 * 
	 * @param othersMotive
	 *            the new others motive
	 */
	public void setOthersMotive(CapitalIncreaseMotive othersMotive) {
		this.othersMotive = othersMotive;
	}

	/**
	 * Gets the voluntier revaluation motive.
	 * 
	 * @return the voluntier revaluation motive
	 */
	public CapitalIncreaseMotive getVoluntierRevaluationMotive() {
		return voluntierRevaluationMotive;
	}

	/**
	 * Sets the voluntier revaluation motive.
	 * 
	 * @param voluntierRevaluationMotive
	 *            the new voluntier revaluation motive
	 */
	public void setVoluntierRevaluationMotive(
			CapitalIncreaseMotive voluntierRevaluationMotive) {
		this.voluntierRevaluationMotive = voluntierRevaluationMotive;
	}

	/**
	 * Gets the disable source securitiy helper.
	 * 
	 * @return the disable source securitiy helper
	 */
	public Boolean getDisableSourceSecuritiyHelper() {
		return disableSourceSecuritiyHelper;
	}

	/**
	 * Sets the disable source securitiy helper.
	 * 
	 * @param disableSourceSecuritiyHelper
	 *            the new disable source securitiy helper
	 */
	public void setDisableSourceSecuritiyHelper(
			Boolean disableSourceSecuritiyHelper) {
		this.disableSourceSecuritiyHelper = disableSourceSecuritiyHelper;
	}

	/**
	 * Gets the render field set.
	 * 
	 * @return the render field set
	 */
	public Boolean getRenderFieldSet() {
		return renderFieldSet;
	}

	/**
	 * Sets the render field set.
	 * 
	 * @param renderFieldSet
	 *            the new render field set
	 */
	public void setRenderFieldSet(Boolean renderFieldSet) {
		this.renderFieldSet = renderFieldSet;
	}

	/**
	 * Gets the intrument type description.
	 * 
	 * @return the intrument type description
	 */
	public String getIntrumentTypeDescription() {
		return intrumentTypeDescription;
	}

	/**
	 * Sets the intrument type description.
	 * 
	 * @param intrumentTypeDescription
	 *            the new intrument type description
	 */
	public void setIntrumentTypeDescription(String intrumentTypeDescription) {
		this.intrumentTypeDescription = intrumentTypeDescription;
	}

	/**
	 * Gets the user info.
	 * 
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 * 
	 * @param userInfo
	 *            the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the render motive increase capital label.
	 * 
	 * @return the render motive increase capital label
	 */
	public Boolean getRenderMotiveIncreaseCapitalLabel() {
		return renderMotiveIncreaseCapitalLabel;
	}

	/**
	 * Sets the render motive increase capital label.
	 * 
	 * @param renderMotiveIncreaseCapitalLabel
	 *            the new render motive increase capital label
	 */
	public void setRenderMotiveIncreaseCapitalLabel(
			Boolean renderMotiveIncreaseCapitalLabel) {
		this.renderMotiveIncreaseCapitalLabel = renderMotiveIncreaseCapitalLabel;
	}

	/**
	 * Gets the disable target security helper.
	 * 
	 * @return the disable target security helper
	 */
	public Boolean getDisableTargetSecurityHelper() {
		return disableTargetSecurityHelper;
	}

	/**
	 * Sets the disable target security helper.
	 * 
	 * @param disableTargetSecurityHelper
	 *            the new disable target security helper
	 */
	public void setDisableTargetSecurityHelper(
			Boolean disableTargetSecurityHelper) {
		this.disableTargetSecurityHelper = disableTargetSecurityHelper;
	}

	/**
	 * Gets the increase capital motive.
	 * 
	 * @return the increase capital motive
	 */
	public BigDecimal getIncreaseCapitalMotive() {
		return increaseCapitalMotive;
	}

	/**
	 * Sets the increase capital motive.
	 * 
	 * @param increaseCapitalMotive
	 *            the new increase capital motive
	 */
	public void setIncreaseCapitalMotive(BigDecimal increaseCapitalMotive) {
		this.increaseCapitalMotive = increaseCapitalMotive;
	}

	/**
	 * Gets the disable corporate event type.
	 * 
	 * @return the disable corporate event type
	 */
	public Boolean getDisableCorporateEventType() {
		return disableCorporateEventType;
	}

	/**
	 * Sets the disable corporate event type.
	 * 
	 * @param disableCorporateEventType
	 *            the new disable corporate event type
	 */
	public void setDisableCorporateEventType(Boolean disableCorporateEventType) {
		this.disableCorporateEventType = disableCorporateEventType;
	}

	/**
	 * Gets the disable delivery place.
	 * 
	 * @return the disable delivery place
	 */
	public Boolean getDisableDeliveryPlace() {
		return disableDeliveryPlace;
	}

	/**
	 * Sets the disable delivery place.
	 * 
	 * @param disableDeliveryPlace
	 *            the new disable delivery place
	 */
	public void setDisableDeliveryPlace(Boolean disableDeliveryPlace) {
		this.disableDeliveryPlace = disableDeliveryPlace;
	}


	/**
	 * Gets the disable currency.
	 *
	 * @return the disable currency
	 */
	public Boolean getDisableCurrency() {
		return disableCurrency;
	}

	/**
	 * Sets the disable currency.
	 *
	 * @param disableCurrency the new disable currency
	 */
	public void setDisableCurrency(Boolean disableCurrency) {
		this.disableCurrency = disableCurrency;
	}

	/**
	 * Gets the securities corporative data model.
	 *
	 * @return the securities corporative data model
	 */
	public SecuritiesCorporativeDataModel getSecuritiesCorporativeDataModel() {
		return securitiesCorporativeDataModel;
	}

	/**
	 * Sets the securities corporative data model.
	 *
	 * @param securitiesCorporativeDataModel the new securities corporative data model
	 */
	public void setSecuritiesCorporativeDataModel(
			SecuritiesCorporativeDataModel securitiesCorporativeDataModel) {
		this.securitiesCorporativeDataModel = securitiesCorporativeDataModel;
	}

	/**
	 * Gets the disable add button.
	 *
	 * @return the disable add button
	 */
	public Boolean getDisableAddButton() {
		return disableAddButton;
	}

	/**
	 * Sets the disable add button.
	 *
	 * @param disableAddButton the new disable add button
	 */
	public void setDisableAddButton(Boolean disableAddButton) {
		this.disableAddButton = disableAddButton;
	}

	/**
	 * Gets the selected securities corporative.
	 *
	 * @return the selected securities corporative
	 */
	public SecuritiesCorporative getSelectedSecuritiesCorporative() {
		return selectedSecuritiesCorporative;
	}

	/**
	 * Sets the selected securities corporative.
	 *
	 * @param selectedSecuritiesCorporative the new selected securities corporative
	 */
	public void setSelectedSecuritiesCorporative(
			SecuritiesCorporative selectedSecuritiesCorporative) {
		this.selectedSecuritiesCorporative = selectedSecuritiesCorporative;
	}

	/**
	 * Gets the source security.
	 *
	 * @return the source security
	 */
	public Security getSourceSecurity() {
		return sourceSecurity;
	}

	/**
	 * Sets the source security.
	 *
	 * @param sourceSecurity the new source security
	 */
	public void setSourceSecurity(Security sourceSecurity) {
		this.sourceSecurity = sourceSecurity;
	}

	/**
	 * Gets the targe security.
	 *
	 * @return the targe security
	 */
	public Security getTargeSecurity() {
		return targeSecurity;
	}

	/**
	 * Sets the targe security.
	 *
	 * @param targeSecurity the new targe security
	 */
	public void setTargeSecurity(Security targeSecurity) {
		this.targeSecurity = targeSecurity;
	}

	/**
	 * Gets the render securities escision.
	 *
	 * @return the render securities escision
	 */
	public Boolean getRenderSecuritiesEscision() {
		return renderSecuritiesEscision;
	}

	/**
	 * Sets the render securities escision.
	 *
	 * @param renderSecuritiesEscision the new render securities escision
	 */
	public void setRenderSecuritiesEscision(Boolean renderSecuritiesEscision) {
		this.renderSecuritiesEscision = renderSecuritiesEscision;
	}

	/**
	 * Sets the require target security.
	 *
	 * @param requireTargetSecurity the new require target security
	 */
	public void setRequireTargetSecurity(String requireTargetSecurity) {
		this.requireTargetSecurity = requireTargetSecurity;
	}

	/**
	 * Gets the render delivery factor securities esicion.
	 *
	 * @return the render delivery factor securities esicion
	 */
	public Boolean getRenderDeliveryFactorSecuritiesEsicion() {
		return renderDeliveryFactorSecuritiesEsicion;
	}

	/**
	 * Sets the render delivery factor securities esicion.
	 *
	 * @param renderDeliveryFactorSecuritiesEsicion the new render delivery factor securities esicion
	 */
	public void setRenderDeliveryFactorSecuritiesEsicion(
			Boolean renderDeliveryFactorSecuritiesEsicion) {
		this.renderDeliveryFactorSecuritiesEsicion = renderDeliveryFactorSecuritiesEsicion;
	}

	/**
	 * Gets the instrument type description.
	 *
	 * @return the instrument type description
	 */
	public String getInstrumentTypeDescription() {
		return instrumentTypeDescription;
	}

	/**
	 * Sets the instrument type description.
	 *
	 * @param instrumentTypeDescription the new instrument type description
	 */
	public void setInstrumentTypeDescription(String instrumentTypeDescription) {
		this.instrumentTypeDescription = instrumentTypeDescription;
	}

	/** The securities corporative list. */
	private List<SecuritiesCorporative> securitiesCorporativeList;

	/**
	 * Gets the securities corporative list.
	 *
	 * @return the securities corporative list
	 */
	public List<SecuritiesCorporative> getSecuritiesCorporativeList() {
		return securitiesCorporativeList;
	}

	/**
	 * Sets the securities corporative list.
	 *
	 * @param securitiesCorporativeList the new securities corporative list
	 */
	public void setSecuritiesCorporativeList(
			List<SecuritiesCorporative> securitiesCorporativeList) {
		this.securitiesCorporativeList = securitiesCorporativeList;
	}


	/**
	 * Gets the disable tax factor.
	 *
	 * @return the disable tax factor
	 */
	public Boolean getDisableTaxFactor() {
		return disableTaxFactor;
	}
	
	/**
	 * Sets the disable tax factor.
	 *
	 * @param disableTaxFactor the new disable tax factor
	 */
	public void setDisableTaxFactor(Boolean disableTaxFactor) {
		this.disableTaxFactor = disableTaxFactor;
	}


	/**
	 * Gets the monitoring.
	 *
	 * @return the monitoring
	 */
	public Boolean getMonitoring() {
		return monitoring;
	}

	/**
	 * Sets the monitoring.
	 *
	 * @param monitoring the monitoring to set
	 */
	public void setMonitoring(Boolean monitoring) {
		this.monitoring = monitoring;
	}

	/**
	 * Execute commission calculate.
	 */
	@LoggerAuditWeb
	public void executeCommissionCalculate(){
		try {
			obj.executeProcess();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the disable issuer.
	 *
	 * @return the disableIssuer
	 */
	public Boolean getDisableIssuer() {
		return disableIssuer;
	}
	
	/**
	 * Sets the disable issuer.
	 *
	 * @param disableIssuer the disableIssuer to set
	 */
	public void setDisableIssuer(Boolean disableIssuer) {
		this.disableIssuer = disableIssuer;
	}
	
	/**
	 * Gets the issuer helper search.
	 *
	 * @return the issuer helper search
	 */
	public Issuer getIssuerHelperSearch() {
		return issuerHelperSearch;
	}
	
	/**
	 * Sets the issuer helper search.
	 *
	 * @param issuerHelperSearch the new issuer helper search
	 */
	public void setIssuerHelperSearch(Issuer issuerHelperSearch) {
		this.issuerHelperSearch = issuerHelperSearch;
	}
	
	/**
	 * Gets the security helper search.
	 *
	 * @return the security helper search
	 */
	public Security getSecurityHelperSearch() {
		return securityHelperSearch;
	}
	
	/**
	 * Sets the security helper search.
	 *
	 * @param securityHelperSearch the new security helper search
	 */
	public void setSecurityHelperSearch(Security securityHelperSearch) {
		this.securityHelperSearch = securityHelperSearch;
	}
	
	/**
	 * Gets the lst cbo corporative even class.
	 *
	 * @return the lst cbo corporative even class
	 */
	public List<ParameterTable> getLstCboCorporativeEvenClass() {
		return lstCboCorporativeEvenClass;
	}
	
	/**
	 * Sets the lst cbo corporative even class.
	 *
	 * @param lstCboCorporativeEvenClass the new lst cbo corporative even class
	 */
	public void setLstCboCorporativeEvenClass(
			List<ParameterTable> lstCboCorporativeEvenClass) {
		this.lstCboCorporativeEvenClass = lstCboCorporativeEvenClass;
	}
	
	/**
	 * Gets the security help bean.
	 *
	 * @return the security help bean
	 */
	public SecurityHelpBean getSecurityHelpBean() {
		return securityHelpBean;
	}
	
	/**
	 * Sets the security help bean.
	 *
	 * @param securityHelpBean the new security help bean
	 */
	public void setSecurityHelpBean(SecurityHelpBean securityHelpBean) {
		this.securityHelpBean = securityHelpBean;
	}
	
	/**
	 * Gets the security source helper mgmt.
	 *
	 * @return the security source helper mgmt
	 */
	public Security getSecuritySourceHelperMgmt() {
		return securitySourceHelperMgmt;
	}
	
	/**
	 * Sets the security source helper mgmt.
	 *
	 * @param securitySourceHelperMgmt the new security source helper mgmt
	 */
	public void setSecuritySourceHelperMgmt(Security securitySourceHelperMgmt) {
		this.securitySourceHelperMgmt = securitySourceHelperMgmt;
	}
	
	/**
	 * Gets the security target helper mgmt.
	 *
	 * @return the security target helper mgmt
	 */
	public Security getSecurityTargetHelperMgmt() {
		return securityTargetHelperMgmt;
	}
	
	/**
	 * Sets the security target helper mgmt.
	 *
	 * @param securityTargetHelperMgmt the new security target helper mgmt
	 */
	public void setSecurityTargetHelperMgmt(Security securityTargetHelperMgmt) {
		this.securityTargetHelperMgmt = securityTargetHelperMgmt;
	}
	
	/**
	 * Gets the issuer helper mgmt.
	 *
	 * @return the issuer helper mgmt
	 */
	public Issuer getIssuerHelperMgmt() {
		return issuerHelperMgmt;
	}
	
	/**
	 * Sets the issuer helper mgmt.
	 *
	 * @param issuerHelperMgmt the new issuer helper mgmt
	 */
	public void setIssuerHelperMgmt(Issuer issuerHelperMgmt) {
		this.issuerHelperMgmt = issuerHelperMgmt;
	}
	
	
	/**
	 * Gets the corporative event mgmt.
	 *
	 * @return the corporative event mgmt
	 */
	public Integer getCorporativeEventMgmt() {
		return corporativeEventMgmt;
	}
	
	/**
	 * Sets the corporative event mgmt.
	 *
	 * @param corporativeEventMgmt the new corporative event mgmt
	 */
	public void setCorporativeEventMgmt(Integer corporativeEventMgmt) {
		this.corporativeEventMgmt = corporativeEventMgmt;
	}
	
	/**
	 * Metodo que limpia y carga objtos dependiente con respecto a la clase del evento corporativo.
	 */
	public void changeCboCorporativeEventClassMgmt(){
		try {
			this.hideDialogs();
			executeAction();
			resetComponent();
	 		ParameterTableTO paramTO = new ParameterTableTO();
			paramTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTO.setMasterTableFk(MasterTableType.CORPORATIVE_EVENT.getCode());
			paramTO.setIdRelatedParameterFk( corporativeEventType );
			disableIssuer = Boolean.TRUE;
			issuerHelperMgmt=new Issuer();
			disableSourceSecuritiyHelper = Boolean.TRUE;
			disableTargetSecurityHelper = Boolean.TRUE;
			securitySourceHelperMgmt=new Security();		
			securityTargetHelperMgmt=new Security();	
			if(corporativeOperation!=null){
				corporativeOperation.setSecurities(new Security());
				corporativeOperation.setTargetSecurity(new Security());
			}				
			this.clearPanels();
			if (Validations.validateIsNotNullAndPositive(instrumentType)) {
				// determine instrument type
				if (instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())) {
					paramTO.setIndicator5(BooleanType.YES.getCode());
				} else if (instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())) {
					paramTO.setIndicator4(BooleanType.YES.getCode());
				}else if(instrumentType.equals(InstrumentType.MIXED_INCOME.getCode())){
					paramTO.setIndicator6(BooleanType.YES.getCode());
				}
			}
			lstCboCorporativeEventMgmt = this.getParameters(paramTO);	
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Metodo que limpia y carga objtos dependiente con respecto a la tipo del evento corporativo.
	 */
	public void changeCorporativeEventMgmt(){
		try {
			this.hideDialogs();
			executeAction();
			resetComponent();
			if(Validations.validateIsNotNull(corporativeEventMgmt) 
					&& Validations.validateIsNotNull(this.corporativeEventType)
					&& Validations.validateIsNotNullAndPositive(corporativeEventMgmt)){
				com.pradera.model.corporatives.CorporativeEventType corporativeEventType;
				corporativeEventType=improtanceEventFacade.findCorporativeEventType(new com.pradera.model.corporatives.CorporativeEventType(
						corporativeEventMgmt,
						instrumentType,
						this.corporativeEventType));	
				if(Validations.validateIsNotNull(corporativeEventType)){
					corporativeOperation.setCorporativeEventType(corporativeEventType);
				}				
				if(corporativeOperation!=null){
					changeEventInfoListener();
				}
			}else{
				disableIssuer = Boolean.TRUE;
				issuerHelperMgmt=new Issuer();
				disableSourceSecuritiyHelper = Boolean.TRUE;
				disableTargetSecurityHelper = Boolean.TRUE;
				securitySourceHelperMgmt=new Security();		
				securityTargetHelperMgmt=new Security();	
				if(corporativeOperation!=null){
					corporativeOperation.setSecurities(new Security());
					corporativeOperation.setTargetSecurity(new Security());
				}				
				this.clearPanels();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}
	
	/**
	 * Bill parameters table map event.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMapEvent(List<ParameterTable> lstParameterTable){
		billParametersTableMapEvent(lstParameterTable,false);
	}
	
	/**
	 * Bill parameters table map event.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMapEvent(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				parametersTableMapEvent.put(pTable.getParameterTablePk(), pTable);
			}else{
				parametersTableMapEvent.put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				parametersTableMap.put(pTable.getParameterTablePk(), pTable);
			}else{
				parametersTableMap.put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Bill parameter table by id.
	 *
	 * @param parameterPk the parameter pk
	 * @throws ServiceException the service exception
	 */
	public void billParameterTableById(Integer parameterPk) throws ServiceException{
		billParameterTableById(parameterPk, false);
	}
	
	/**
	 * Bill parameter table by id.
	 *
	 * @param parameterPk the parameter pk
	 * @param isObj the is obj
	 * @throws ServiceException the service exception
	 */
	public void billParameterTableById(Integer parameterPk, boolean isObj) throws ServiceException{
		ParameterTable pTable = generalParameterFacade.getParamDetailServiceFacade(parameterPk);
		if(pTable!=null){
			if(isObj){
				parametersTableMap.put(pTable.getParameterTablePk()  , pTable);
			}else{
				parametersTableMap.put(pTable.getParameterTablePk()  , pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Gets the lst cbo corporative event mgmt.
	 *
	 * @return the lst cbo corporative event mgmt
	 */
	public List<ParameterTable> getLstCboCorporativeEventMgmt() {
		return lstCboCorporativeEventMgmt;
	}
	
	/**
	 * Sets the lst cbo corporative event mgmt.
	 *
	 * @param lstCboCorporativeEventMgmt the new lst cbo corporative event mgmt
	 */
	public void setLstCboCorporativeEventMgmt(
			List<ParameterTable> lstCboCorporativeEventMgmt) {
		this.lstCboCorporativeEventMgmt = lstCboCorporativeEventMgmt;
	}
	
	/**
	 * Gets the parameters table map event.
	 *
	 * @return the parameters table map event
	 */
	public Map<Integer, Object> getParametersTableMapEvent() {
		return parametersTableMapEvent;
	}
	
	/**
	 * Sets the parameters table map event.
	 *
	 * @param parametersTableMapEvent the parameters table map event
	 */
	public void setParametersTableMapEvent(
			Map<Integer, Object> parametersTableMapEvent) {
		this.parametersTableMapEvent = parametersTableMapEvent;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.view.GenericBaseBean#getParametersTableMap()
	 */
	public Map<Integer, Object> getParametersTableMap() {
		return parametersTableMap;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.view.GenericBaseBean#setParametersTableMap(java.util.Map)
	 */
	public void setParametersTableMap(Map<Integer, Object> parametersTableMap) {
		this.parametersTableMap = parametersTableMap;
	}

	/**
	 * Gets the list securities target.
	 *
	 * @return the list securities target
	 */
	public List<Security> getListSecuritiesTarget() {
		return listSecuritiesTarget;
	}
	
	/**
	 * Sets the list securities target.
	 *
	 * @param listSecuritiesTarget the new list securities target
	 */
	public void setListSecuritiesTarget(List<Security> listSecuritiesTarget) {
		this.listSecuritiesTarget = listSecuritiesTarget;
	}
	
	/**
	 * Gets the render market fact.
	 *
	 * @return the render market fact
	 */
	public Boolean getRenderMarketFact() {
		return renderMarketFact;
	}
	
	/**
	 * Sets the render market fact.
	 *
	 * @param renderMarketFact the new render market fact
	 */
	public void setRenderMarketFact(Boolean renderMarketFact) {
		this.renderMarketFact = renderMarketFact;
	}
	
	/**
	 * Gets the render securities esicion.
	 *
	 * @return the render securities esicion
	 */
	public Boolean getRenderSecuritiesEsicion() {
		return renderSecuritiesEsicion;
	}
	
	/**
	 * Sets the render securities esicion.
	 *
	 * @param renderSecuritiesEsicion the new render securities esicion
	 */
	public void setRenderSecuritiesEsicion(Boolean renderSecuritiesEsicion) {
		this.renderSecuritiesEsicion = renderSecuritiesEsicion;
	}
	
	/**
	 * Gets the render interest amortization.
	 *
	 * @return the render interest amortization
	 */
	public Boolean getRenderInterestAmortization() {
		return renderInterestAmortization;
	}
	
	/**
	 * Sets the render interest amortization.
	 *
	 * @param renderInterestAmortization the new render interest amortization
	 */
	public void setRenderInterestAmortization(Boolean renderInterestAmortization) {
		this.renderInterestAmortization = renderInterestAmortization;
	}
	
	/**
	 * Gets the render exchange.
	 *
	 * @return the render exchange
	 */
	public Boolean getRenderExchange() {
		return renderExchange;
	}
	
	/**
	 * Sets the render exchange.
	 *
	 * @param renderExchange the new render exchange
	 */
	public void setRenderExchange(Boolean renderExchange) {
		this.renderExchange = renderExchange;
	}
	
	/**
	 * Gets the render amortization.
	 *
	 * @return the render amortization
	 */
	public Boolean getRenderAmortization() {
		return renderAmortization;
	}
	
	/**
	 * Sets the render amortization.
	 *
	 * @param renderAmortization the new render amortization
	 */
	public void setRenderAmortization(Boolean renderAmortization) {
		this.renderAmortization = renderAmortization;
	}
	
	/**
	 * Gets the render interest.
	 *
	 * @return the render interest
	 */
	public Boolean getRenderInterest() {
		return renderInterest;
	}
	
	/**
	 * Sets the render interest.
	 *
	 * @param renderInterest the new render interest
	 */
	public void setRenderInterest(Boolean renderInterest) {
		this.renderInterest = renderInterest;
	}

	/**
	 * Gets the list securities target aux.
	 *
	 * @return the list securities target aux
	 */
	public List<Security> getListSecuritiesTargetAux() {
		return listSecuritiesTargetAux;
	}

	/**
	 * Sets the list securities target aux.
	 *
	 * @param listSecuritiesTargetAux the new list securities target aux
	 */
	public void setListSecuritiesTargetAux(List<Security> listSecuritiesTargetAux) {
		this.listSecuritiesTargetAux = listSecuritiesTargetAux;
	}

	/**
	 * Gets the disabled for excision.
	 *
	 * @return the disabled for excision
	 */
	public Boolean getDisabledForExcision() {
		return disabledForExcision;
	}

	/**
	 * Sets the disabled for excision.
	 *
	 * @param disabledForExcision the new disabled for excision
	 */
	public void setDisabledForExcision(Boolean disabledForExcision) {
		this.disabledForExcision = disabledForExcision;
	}

	/**
	 * Gets the modify for excision.
	 *
	 * @return the modify for excision
	 */
	public Boolean getModifyForExcision() {
		return modifyForExcision;
	}

	/**
	 * Sets the modify for excision.
	 *
	 * @param modifyForExcision the new modify for excision
	 */
	public void setModifyForExcision(Boolean modifyForExcision) {
		this.modifyForExcision = modifyForExcision;
	}
	
}