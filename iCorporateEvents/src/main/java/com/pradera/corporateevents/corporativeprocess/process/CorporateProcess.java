package com.pradera.corporateevents.corporativeprocess.process;

import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Qualifier BatchProcess.
 * Qualifier for inject batch process
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE})
public @interface CorporateProcess {

	/**
	 * Value.
	 *
	 * @return the corporate executor
	 */
	CorporateExecutor value();
}
