package com.pradera.corporateevents.corporativeprocess.process.executors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.corporativeprocess.process.CorporateExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcess;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.ChangesFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.OperationsFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BuyerOperationsFixer;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.ReportCorporativeResult;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.type.BlockMarketFactOperationStateType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.type.ParticipantRoleType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * Clase que ejecuta proceso preliminar definitvo de los dividendos en acciones.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/06/2014
 */
@CorporateProcess(CorporateExecutor.STOCK)
@Performance
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@LocalBean
public class StockCorporateProcessExecutor implements CorporateProcessExecutor {
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The sc. */
	@Resource
    SessionContext sc;
	
	/** The stock corporate process executor. */
	StockCorporateProcessExecutor stockCorporateProcessExecutor;
	
	/** The fixer. */
	@Inject
	private BalanceFixer fixer;
	
	/** The document fixer. */
	@Inject
	private DocumentsFixer documentFixer;
	
	/** The operation fixer. */
	@Inject
	private OperationsFixer operationFixer;
	
	/** The seller operation fixer. */
	@Inject
	private BuyerOperationsFixer buyerOperationFixer;
	
	/** The changes fixer. */
	@Inject
	private ChangesFixer changesFixer;
	
	/** The executor component service bean. */
	@Inject
	private Instance<AccountsComponentService> executorComponentServiceBean;
	
	/** The securities componente service. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponenteService;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/**
	 * Inicio de Clase que ejecuta proceso preliminar definitvo de los dividendos en acciones.
	 */
	@PostConstruct
    public void init(){
        this.stockCorporateProcessExecutor = this.sc.getBusinessObject(StockCorporateProcessExecutor.class);
    }
	
	
	/**
	 * Metodo que ejecuta preliminar de dividendos en acciones.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executePreliminarProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started Stock preliminary process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		boolean flag = false;
		Long stockProcess = null;
		Long reportStockprocess = null;	
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		stockProcess = corporateExecutorService.getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
				corpOperation.getSecurities().getIdSecurityCodePk()
				, StockStateType.FINISHED.getCode(), StockClassType.NORMAL.getCode(), StockType.CORPORATIVE.getCode());
		reportStockprocess = corporateExecutorService.getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
				corpOperation.getSecurities().getIdSecurityCodePk()
				, StockStateType.FINISHED.getCode(), StockClassType.REPORT.getCode(), StockType.CORPORATIVE.getCode());
		
		if (stockProcess != null && stockProcess > 0) {
			corporateExecutorService.deletePreviousPreliminaryInformation(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,true,true);
			List<Object[]> distinctHolders  = corporateExecutorService.getDistinctHoldersForProcess(stockProcess);
			if (distinctHolders != null && distinctHolders.size() > 0) {
				//procesing in transaction
				flag = this.stockCorporateProcessExecutor.executePreliminarProcessAlgorithm(distinctHolders, corpOperation, stockProcess, reportStockprocess, processType, loggerUser);
			}
		}
		
		return flag;
	}
	
	/**
	 * Metodo que ejecuta algoritmo preliminar de dividendos en acciones.
	 *
	 * @param distinctHolders the distinct holders
	 * @param corpOperation the corp operation
	 * @param stockProcess the stock process
	 * @param reportStockprocess the report stockprocess
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean executePreliminarProcessAlgorithm(List<Object[]> distinctHolders,CorporativeOperation corpOperation,Long stockProcess,Long reportStockprocess,
			ImportanceEventType processType,LoggerUser loggerUser) throws ServiceException{
			CorporativeProcessResult totalProcessResult = null;
			CorporativeProcessResult remTotalProcessResult = null;
			List<ReportCorporativeResult> reportOperations = null;
			List<BlockCorporativeResult> blockDocuments = null;
			boolean remnantFlag = false;
			BigDecimal remDelFactor = BigDecimal.ZERO;
			corpOperation.setDeliveryFactor(CorporateProcessUtils.divide(corpOperation.getDeliveryFactor(), CorporateProcessConstants.AMOUNT_ROUND));
			BigDecimal delFactor = corpOperation.getDeliveryFactor();
			Map<String, Object[]> holderAccAdjustments = corporateExecutorService.getHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
					CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.IND_YES,false);
			Map<Long, Object[]> uniParticipants = corporateExecutorService.getUnifiedParticipants(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getRegistryDate());
			Map<Long, List<Object[]>> changeOwnership = corporateExecutorService.getChangeofOwnerShipSucession(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getRegistryDate());
			List<CorporativeProcessResult> changedResults = new ArrayList<CorporativeProcessResult>();
			List<CorporativeProcessResult> changedRemResults = new ArrayList<CorporativeProcessResult>();
			for (Object[] arrObj : distinctHolders) {
				totalProcessResult = new CorporativeProcessResult();
				remTotalProcessResult = new CorporativeProcessResult();
				remnantFlag = false;
				String holderAlternateCode = (String) arrObj[0];
				BigDecimal totalBalance = ((BigDecimal) arrObj[1]);
				BigDecimal roundedTotal = CorporateProcessUtils.calculateByFactor(totalBalance,corpOperation.getDeliveryFactor(), 
						corpOperation.getIndRound());
				totalProcessResult.setTotalBalance(roundedTotal);
				totalProcessResult.setCorporativeOperation(corpOperation);
				totalProcessResult.setSecurity(corpOperation.getSecurities());
				if(corpOperation.getIndRemanent().equals(CorporateProcessConstants.IND_REMAINDER)){
					BigDecimal total = CorporateProcessUtils.multiply(totalBalance,corpOperation.getDeliveryFactor());
					BigDecimal remnant = BigDecimal.ZERO;
					if(total.compareTo(roundedTotal) > 0){
						remnant = CorporateProcessUtils.calculateByFactor(total.subtract(roundedTotal), BigDecimal.ONE, corpOperation.getIndRound(), true);
					}
					if(remnant.compareTo(CorporateProcessConstants.MINIMUM_AMOUNT) >= 0){
						remnantFlag = true;
						remDelFactor = CorporateProcessUtils.divide(remnant,totalBalance);
						remTotalProcessResult.setTotalBalance(CorporateProcessUtils.multiply(remnant, CorporateProcessConstants.AMOUNT_ROUND));
						remTotalProcessResult.setCorporativeOperation(corpOperation);
						remTotalProcessResult.setSecurity(corpOperation.getSecurities());
					}
				}
				List<Object[]> holderAccounts =corporateExecutorService.getStockCalculationBalances(stockProcess, holderAlternateCode);
				
				if (holderAccounts != null && holderAccounts.size() > 0) {
					
					List<CorporativeProcessResult> holderAccResults = fixer.fixBalances(holderAccounts, corpOperation, delFactor, 
							corpOperation.getIndRound(), false, false, true, processType,null);
					List<CorporativeProcessResult> remHolderAccResults = null;
					if(remnantFlag){
						remHolderAccResults = fixer.fixBalances(holderAccounts, corpOperation, remDelFactor, 
								corpOperation.getIndRound(), true, true, true, processType,null);
					}
					BigDecimal tmpDelFactor = null;
					for (Iterator<CorporativeProcessResult> itHolder=holderAccResults.iterator(); itHolder.hasNext();) {
						CorporativeProcessResult corpProcessResults =  itHolder.next();
						
						CorporativeProcessResult remCorpProcessResults =  null;
						if(remnantFlag && remHolderAccResults != null && !remHolderAccResults.isEmpty()){
							for(CorporativeProcessResult corpres : remHolderAccResults){
								if(corpProcessResults.getHolderAccount().getIdHolderAccountPk().equals(corpres.getHolderAccount().getIdHolderAccountPk())){
									remCorpProcessResults = corpres;
									break;
								}
							}
						}
						tmpDelFactor = CorporateProcessUtils.getHolderAdjustmentFactor(holderAccAdjustments, corpProcessResults);
						if(tmpDelFactor == null){
							tmpDelFactor = delFactor;
						}
						//Aqui susc. preferente, dividendos efectivo
						if(CorporateProcessUtils.hasBlockBalances(corpProcessResults)){
							List<Object[]> blockedDocs =corporateExecutorService.getBlockStockCalculationDetails(stockProcess, corpProcessResults.getParticipant().getIdParticipantPk(), 
										corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
										corpProcessResults.getSecurity().getIdSecurityCodePk());
							
							if(blockedDocs != null && blockedDocs.size() > 0){
								blockDocuments = documentFixer.fixDocuments(corpProcessResults, blockedDocs, tmpDelFactor,
										corpOperation.getIndRound(), false, false, true, fixer, processType,null);
								corpProcessResults.setBlockCorporativeResults(blockDocuments);
							}
							if(remnantFlag && remCorpProcessResults != null){
								blockDocuments = documentFixer.fixDocuments(remCorpProcessResults, blockedDocs, remDelFactor,
										corpOperation.getIndRound(), true, true, true, fixer, processType,null);
								remCorpProcessResults.setBlockCorporativeResults(blockDocuments);
							}
						}
						if(CorporateProcessUtils.hasReportBalances(corpProcessResults)){
							List<Object[]> reportOper = corporateExecutorService.getStockReportOperations(reportStockprocess, corpProcessResults.getParticipant().getIdParticipantPk(), 
										corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
										corpProcessResults.getSecurity().getIdSecurityCodePk());
							
							if(reportOper != null && reportOper.size() > 0){
								reportOperations = operationFixer.fixOperations(corpProcessResults, reportOper, tmpDelFactor, 
										corpOperation.getIndRound(), false, false,true, fixer,null);
								corpProcessResults.setReportCorporativeResults(reportOperations);
							}
							if(remnantFlag && remCorpProcessResults != null){
								reportOperations = operationFixer.fixOperations(remCorpProcessResults, reportOper, remDelFactor, 
										corpOperation.getIndRound(), true, true, true, fixer,null);
								remCorpProcessResults.setReportCorporativeResults(reportOperations);
							}
						}	
//						blockDocuments = documentFixer.fixDocuments(corpProcessResults, blockedDocs, tmpDelFactor,corpOperation.getIndRound(), false, false, true, fixer, processType,null);
						
						List<CorporativeProcessResult> changesCorp = changesFixer.getUpdatedResult(new ArrayList<CorporativeProcessResult>(), corpProcessResults, 
								loggerUser, uniParticipants, changeOwnership, documentFixer, fixer, corpOperation.getIndRound(), false, null);
						corpProcessResults = documentFixer.updateBlockDocumentsEffections(corpProcessResults, false);
						
						/**
						 * metodo
						 * Method:
						 * 1.- Parametro : tmpDelFactor,  holderAccounts , loggerUser , documentFixer, fixer, corpOperation.getIndRound()
			
						 * 2.- Multiplicar corpOperation.getDeliveryFactor * tmpDelFactor 
						 */
						if(changesCorp != null && !changesCorp.isEmpty()){
							changedResults.addAll(changesCorp);
						}else{
							corporateExecutorService.create(corpProcessResults, loggerUser);
						}
						
						if(remnantFlag && remCorpProcessResults != null){
							changesCorp = changesFixer.getUpdatedResult(new ArrayList<CorporativeProcessResult>(), remCorpProcessResults, 
									loggerUser, uniParticipants, changeOwnership, documentFixer, fixer, corpOperation.getIndRound(), false, null);
							documentFixer.updateBlockDocumentsEffections(remCorpProcessResults, false);
							if(changesCorp != null && !changesCorp.isEmpty()){
								changesCorp = CorporateProcessUtils.divideForCash(changesCorp);
								changedRemResults.addAll(changesCorp);
							}else{
								CorporateProcessUtils.divideForCash(remCorpProcessResults);
								corporateExecutorService.create(remCorpProcessResults, loggerUser);
							}
							
						}
					}
					holderAccounts.clear();
					holderAccounts = null;							
				}
			}
			if(!changedResults.isEmpty() || !changedRemResults.isEmpty()){
				for(CorporativeProcessResult corpRes : changedResults ){
					if(corpRes.getTotalBalance().compareTo(BigDecimal.ZERO) > 0 || corpRes.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
						documentFixer.updateBlockDocumentsEffections(corpRes, false);							
						corporateExecutorService.updateOrCreate(corpRes, loggerUser);
					}
				}
				for(CorporativeProcessResult corpRes : changedRemResults ){
					if(corpRes.getTotalBalance().compareTo(BigDecimal.ZERO) > 0 || corpRes.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
						documentFixer.updateBlockDocumentsEffections(corpRes, true);							
						corporateExecutorService.updateOrCreate(corpRes, loggerUser);
					}
				}
				corporateExecutorService.updateBlockCorporateForResult(corpOperation.getIdCorporativeOperationPk(), loggerUser);
			}
			List<Object[]> sumReported = null, buyerOperations = null; 
			
			sumReported = corporateExecutorService.getSellerBuyyerSumReportOperations(reportStockprocess, 
					corpOperation.getIdCorporativeOperationPk(),
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
					ParticipantRoleType.SELL.getCode(), ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.IND_NO_REMAINDER, 
					CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY,null);
			buyerOperations = corporateExecutorService.getBuyerStockReportOperations(reportStockprocess, 
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(),
					ParticipantRoleType.BUY.getCode());
			
			if(sumReported != null && !sumReported.isEmpty()){
				reportOperations = buyerOperationFixer.fixOperations(sumReported, buyerOperations, corpOperation, 
						corpOperation.getIndRound(), false, false, true, fixer,null);
				corporateExecutorService.saveAll(reportOperations, loggerUser);
				if(remnantFlag){
					sumReported = corporateExecutorService.getSellerBuyyerSumReportOperations(reportStockprocess, 
							corpOperation.getIdCorporativeOperationPk(),
							GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
							ParticipantRoleType.SELL.getCode(), ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.IND_REMAINDER, 
							CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY,null);
					reportOperations = buyerOperationFixer.fixOperations(sumReported, buyerOperations, corpOperation, 
							corpOperation.getIndRound(), true, true, true, fixer,null);
					corporateExecutorService.saveAll(reportOperations, loggerUser);							
				}
				corporateExecutorService.updateReportedBalanceofCorporateResults(corpOperation.getIdCorporativeOperationPk(), 
						GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
						ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
				corporateExecutorService.updateReportedOperationsofCorporateResult(corpOperation.getIdCorporativeOperationPk(), 
						GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
						ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			}
			corporateExecutorService.updateHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
					CorporateProcessConstants.IND_NO, CorporateProcessConstants.IND_YES, CorporateProcessConstants.IND_YES, loggerUser);
			
			corpOperation.setIdStockCalculation(stockProcess);
			corporateExecutorService.updateCorporativeStockCalculation(corpOperation);
			return true;
	}
	
	
	/**
	 * Metodo que ejecuta definitivo de dividendos en acciones.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executeDefinitiveProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started definitive process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		List<Object> participants = corporateExecutorService.getDistinctParticpantsForDefinitive(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		if(participants !=null && !participants.isEmpty()){
			for(Object part : participants){
				Long participant = (Long)part;
			//procesing participants in transaction
			this.stockCorporateProcessExecutor.executeDefinitiveProcessParticipants(participant, corpOperation, loggerUser);
			}
		}
		BigDecimal totalBalance =(BigDecimal) corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_PROCESSED,
				CorporateProcessConstants.BALANCE_DESTINY, null);
		if(totalBalance != null && totalBalance.compareTo(BigDecimal.ZERO) > 0){
			this.stockCorporateProcessExecutor.executeDefinitiveProcessSecurities(totalBalance,corpOperation,processType,loggerUser);
		}
		return true;
	}
	
	/**
	 * Metodo que ejecuta movimientos de saldos a nivel de cuentas para dividendos en acciones.
	 *
	 * @param participant the participant
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessParticipants(Long participant,CorporativeOperation corpOperation,LoggerUser loggerUser) throws ServiceException{
		AccountsComponentService accountsComponentService = executorComponentServiceBean.get();
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		fixer.setLoggerUserForTransationRegistry();
				accountsComponentService.executeAccountsComponent(fixer.generateAccountComponent(
						corporateExecutorService.getCorporateResultsForDefinitiveProcess(corpOperation.getIdCorporativeOperationPk(), participant, 
								CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null), processType, corpOperation));
				// update block operations with balance and generate movements
				List<Object[]> blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForUnblockDefinitive(corpOperation.getIdCorporativeOperationPk()
						, participant, corpOperation.getSecurities().getIdSecurityCodePk(), CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null);
				if(blockDocuments != null && !blockDocuments.isEmpty()){
					for(Object[] data : blockDocuments){
						BigDecimal quantity = (BigDecimal)data[2];
						Long blockOperationId = (Long)data[3];
						HolderAccountBalanceTO account = new HolderAccountBalanceTO();
						account.setFinalDate(corpOperation.getDeliveryDate());
						account.setIdHolderAccount((Long)data[5]);
						account.setIdParticipant((Long)data[6]);
						account.setIdSecurityCode((String)data[7]);
						account.setStockQuantity(quantity);
						List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
						accounts.add(account);
						corporateExecutorService.updateBlockOperation(blockOperationId, quantity, true, false, null, null, loggerUser);
						if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
							 BlockOperation blockOperation = corporateExecutorService.find(BlockOperation.class, blockOperationId);
								for(BlockMarketFactOperation blockMarketFact : blockOperation.getBlockMarketFactOperations()){
									if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
										blockMarketFact.setActualBlockBalance(new BigDecimal(0));
										corporateExecutorService.update(blockMarketFact);
									}
								}
							//new one blockMarketFact
							BlockMarketFactOperation blockMarketFactOperation = new BlockMarketFactOperation();
							blockMarketFactOperation.setMarketDate(corpOperation.getMarketDate());
							blockMarketFactOperation.setMarketPrice(corpOperation.getMarketPrice());
							blockMarketFactOperation.setOriginalBlockBalance(quantity);
							blockMarketFactOperation.setActualBlockBalance(quantity);
							blockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
							blockMarketFactOperation.setBlockOperation(blockOperation);
							blockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
							blockMarketFactOperation.setRegistryUser(loggerUser.getUserName());
							corporateExecutorService.create(blockMarketFactOperation);
						 if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
								//movements marketfact
								MarketFactAccountTO mktFact = new MarketFactAccountTO();
								mktFact.setMarketDate(corpOperation.getMarketDate());
								mktFact.setMarketPrice(corpOperation.getMarketPrice());
								mktFact.setQuantity(quantity);
								account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
								account.getLstMarketFactAccounts().add(mktFact);
							}
						}
						accountsComponentService.executeAccountsComponent(CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
								blockOperationId, null, null, null, processType, null, accounts, CorporateProcessUtils.getBlockBalance((Integer)data[0], null),idepositarySetup.getIndMarketFact()));
					}
				}
				corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), 
						participant, true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null, loggerUser);
	}
	
	/**
	 * Metodo que ejecuta movimientos de saldos a nivel de valor para dividendos en acciones.
	 *
	 * @param totalBalance the total balance
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessSecurities(BigDecimal totalBalance,CorporativeOperation corpOperation,ImportanceEventType processType,LoggerUser loggerUser) throws ServiceException{
			fixer.setLoggerUserForTransationRegistry();
			BigDecimal circulation = 	totalBalance;
			BigDecimal shareBalance = totalBalance;
			BigDecimal capital =  shareBalance.multiply(corpOperation.getSecurities().getCurrentNominalValue());
			SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, capital, circulation, corpOperation.getSecurities().getIdSecurityCodePk());

			secOpe.setSecurities(corpOperation.getSecurities());
			corporateExecutorService.create(secOpe, loggerUser);		
			SecuritiesComponentTO securitiesComponente = CorporateProcessUtils.getSecuritiesComponent(processType, secOpe, circulation.subtract(shareBalance), totalBalance, null);
			securitiesComponenteService.get().executeSecuritiesComponent(securitiesComponente);
	}
	
}
	

