package com.pradera.corporateevents.corporativeprocess.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.process.ProcessLogger;
// TODO: Auto-generated Javadoc
import com.pradera.model.process.ProcessLoggerDetail;

/**
 * Batch que vence los valores DPF del dia.
 *
 * @author PraderaTechnologies
 */
@BatchProcess(name="CorporateAutomaticBatch")
public class CorporateAutomaticBatch implements Serializable, JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The corporate process service facade. */
	@EJB
	CorporateProcessServiceFacade corporateProcessServiceFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/**
	 * Inicio de Bachero que redime valores DPF.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
			
			try{
				//validamos la existencia de tipo de cambio para todas las monedas - Si falta alguno manda excepcion
					if(!corporateProcessServiceFacade.verifyExchangeRate()){
					
					List<CorporativeOperation> lstCorporativeErrorInt = new ArrayList<>();
					List<CorporativeOperation> lstCorporativeErrorAmo = new ArrayList<>();
					List<CorporativeOperation> lstInterest = new ArrayList<>();
					List<CorporativeOperation> lstAmortization = new ArrayList<>();
					
					//obtiene lista de valores con fecha de expiracion actual 
					 List<CorporativeOperation> securities = corporateProcessServiceFacade.listSecurityToDefinitive();
					 
					 //Separamos listas de interes y amortizacion
					for (CorporativeOperation lstCorporatives : securities ){
						if(lstCorporatives.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
								&& !lstCorporatives.getState().equals(CorporateProcessStateType.DEFINITIVE.getCode())){
							lstInterest.add(lstCorporatives);
						}else{
							if(lstCorporatives.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())
									&& !lstCorporatives.getState().equals(CorporateProcessStateType.DEFINITIVE.getCode())
									){
								lstAmortization.add(lstCorporatives);
							}
						}
					}
					
					//Verificar inconsistencias y corregir interes
					if(lstInterest.size()>GeneralConstants.ZERO_VALUE_INTEGER){
						corporateProcessServiceFacade.correctInconsistencyCorporative(lstInterest, false);
					} 
					//Verificar inconsistencias y corregir amortizacion
					if(lstAmortization.size()>GeneralConstants.ZERO_VALUE_INTEGER){
						corporateProcessServiceFacade.correctInconsistencyCorporative(lstAmortization, false);
					}
					
					//Primero enviamos todos los corporativos de interes
					if(lstInterest.size()>GeneralConstants.ZERO_VALUE_INT){
		
						lstCorporativeErrorInt = corporateProcessServiceFacade.definitiveProcessSecurity(lstInterest);
						
						//Reenviamos inconsistencias en caso extremo
						Integer count = 0;
						while(lstCorporativeErrorInt.size()!=GeneralConstants.ZERO_VALUE_INT && count < GeneralConstants.TWO_VALUE_INTEGER){
							//corregimos inconsistencia
							count=count+1;
							corporateProcessServiceFacade.correctInconsistencyCorporative(lstCorporativeErrorInt, true);
							lstCorporativeErrorInt = corporateProcessServiceFacade.definitiveProcessSecurity(lstCorporativeErrorInt);
						}
						
					}
					
					//Si hay un corporativo de interes con preliminar fallo no se envia su amortizacion
					if(lstCorporativeErrorInt.size()>GeneralConstants.ZERO_VALUE_INT){

						for (CorporativeOperation lstNotAmort : lstCorporativeErrorInt){
							
							String security = lstNotAmort.getSecurities().getIdSecurityCodePk();
							
							for (CorporativeOperation lstNotAmorts :lstAmortization){
								
								if(security.equals(lstNotAmorts.getSecurities().getIdSecurityCodePk())){
									lstAmortization.remove(lstNotAmorts);
									log.info("CORPORATIVO RETIRADO: " +lstNotAmort.getIdCorporativeOperationPk());
								}
							}
						}
					}
					
					//Enviamos amortizacion
					if(lstAmortization.size()>GeneralConstants.ZERO_VALUE_INT){
						
						//Enviamos a preliminar y definitivo
						lstCorporativeErrorAmo = corporateProcessServiceFacade.definitiveProcessSecurity(lstAmortization);
						
						Integer count=0;
						//Reenviamos inconsistencias en caso extremo
						while(lstCorporativeErrorAmo.size()!=GeneralConstants.ZERO_VALUE_INT && count < GeneralConstants.TWO_VALUE_INTEGER){
							
							//corregimos inconsistencia solo 2 intentos
							count=count+1;
							corporateProcessServiceFacade.correctInconsistencyCorporative(lstCorporativeErrorAmo, true);
							lstCorporativeErrorAmo = corporateProcessServiceFacade.definitiveProcessSecurity(lstCorporativeErrorAmo);
							}
					}
					
					// EXCEPCION ERRORES
					List <Long> notification = new ArrayList<>();
					
					if(lstCorporativeErrorAmo.size()>GeneralConstants.ZERO_VALUE_INTEGER){
						for (CorporativeOperation lst : lstCorporativeErrorAmo){
							notification.add(lst.getIdCorporativeOperationPk());
						}
					}
					if(lstCorporativeErrorInt.size()>GeneralConstants.ZERO_VALUE_INTEGER){
						for (CorporativeOperation lst : lstCorporativeErrorAmo){
							notification.add(lst.getIdCorporativeOperationPk());
						}
					}
					
					if (notification.size()>GeneralConstants.ZERO_VALUE_INTEGER){
						throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_VALIDATE,new Object[]{StringUtils.join(notification,",")});
					}
					
				}
		}
		 catch (ServiceException e) {
			 if(e.getErrorService().equals(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE_PARAM)){
					throw new RuntimeErrorException(null, "No existe Tipo de Cambio para: "+ StringUtils.join(e.getParams(),",") +" Verifique.");
				}else if(e.getErrorService().equals(ErrorServiceType.CORPORATE_EVENT_NOT_VALIDATE)){
						throw new RuntimeErrorException(null, "Los siguientes corporativos tienen Saldo Negativo: "+ StringUtils.join(e.getParams(),",") +" Verifique.");
					}else{
						e.printStackTrace();
				 }
			 
		}
			 log.info(" ================== FIN CORPORATIVO AUTOMATICO ===================== ");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
	
}
