package com.pradera.corporateevents.corporativeprocess.consignation.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.swift.to.CashAccountsSearchTO;
import com.pradera.corporateevents.corporativeprocess.consignation.facade.AllocationPaymentBenefitServiceFacade;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocateIssuerCashAccountTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationBankResumeTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationCorporativeDetailTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationIssuerResumeTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationPaymentSearchTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationProcessIssuerResumeTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationProcessRegisterTO;
import com.pradera.corporateevents.corporativeprocess.consignation.to.AllocationProcessTO;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.type.AllocationProcessType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * Clase de funcionalidad y administracion de eventos de la pantalla de consignacion de pagos.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 31/10/2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class AllocationPaymentBenefitManagementBean extends GenericBaseBean implements Serializable{

	/** The allocation payment facade. */
	@EJB
	private AllocationPaymentBenefitServiceFacade allocationPaymentFacade;

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The allocation payment search. */
	private AllocationPaymentSearchTO allocationPaymentSearch;

	/** The lst allocation processes. */
	private GenericDataModel<AllocationProcessTO> lstAllocationProcesses;
	
	/** The allocation process. */
	private AllocationProcessTO allocationProcess;
	
	/** The lst bank resume. */
	private GenericDataModel<AllocationBankResumeTO> lstBankResume;
	
	/** The bank resume. */
	private AllocationBankResumeTO bankResume;
	
	/** The ind show corporative. */
	private Integer indShowCorporative;
	
	/** The ind corporative allocate. */
	private Integer indCorporativeAllocate;
	
	/** The ind allocated corporative. */
	private Integer indAllocatedCorporative;
	
	/** The ind show allocation corporatives. */
	private Integer indShowAllocationCorporatives;
	
	/** The ind show allocation bank. */
	private Integer indShowAllocationBank;
	
	/** The ind show direct retention. */
	private Integer indShowDirectRetention;

	/** The lst allocation process issuer resume. */
	private GenericDataModel<AllocationProcessIssuerResumeTO> lstAllocationProcessIssuerResume;
	
	/** The allocation process issuer resume. */
	private AllocationProcessIssuerResumeTO[] allocationProcessIssuerResume;
	
	/** The allocate process issuer. */
	private AllocationProcessIssuerResumeTO allocateProcessIssuer;

	/** The lst corporative allocate. */
	private List<AllocationCorporativeDetailTO> lstCorporativeAllocate;
	
	/** The lst allocated corporative. */
	private List<AllocationCorporativeDetailTO> lstAllocatedCorporative;
	
	/** The corporative allocate. */
	private AllocationCorporativeDetailTO corporativeAllocate;
	
	/** The allocated corporative. */
	private AllocationCorporativeDetailTO allocatedCorporative;

	/** The allocation payment resume. */
	private List<AllocationIssuerResumeTO> allocationPaymentResume;
	
	/** The block allocation resume. */
	private List<AllocationIssuerResumeTO> blockAllocationResume;
	
	/** The selected allocated corporatives. */
	private List<AllocationCorporativeDetailTO> selectedAllocatedCorporatives = new ArrayList<AllocationCorporativeDetailTO>();

	/** The payment date. */
	private String paymentDate;
	
	/** The currency. */
	private String currency;
	
	/** The issuer description. */
	private String issuerDescription;
	
	/** The commission excluded. */
	private boolean commissionExcluded;

	/** The allocation process register. */
	private List<AllocationProcessRegisterTO> allocationProcessRegister;

	/** The security code. */
	private String securityCode;

	/**
	 * Inicio de Clase de funcionalidad y administracion de eventos de la pantalla de consignacion de pagos.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		
		allocationPaymentSearch = new AllocationPaymentSearchTO();
		List<ParameterTable> lstCurrency = allocationPaymentFacade.getCurrency();
		allocationPaymentSearch.setLstCurrency(lstCurrency);
		allocationPaymentSearch.setPaymentDate(CommonsUtilities.currentDate());
		allocationPaymentSearch.setIssuer(new Issuer());
		allocationPaymentSearch.setSecurity(new Security());
	}

	
	public String backToSearch() {
		
		try {
			cleanAllocationPaymentProcess();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "backScreenAllocationProcess";
	}
	
	/**
	 * Metodo que busca procesos corporativos en estado definitivo y consignaciones en estado preliminar y definitivo.
	 */
	@LoggerAuditWeb
	public void searchPaymentCorporatives(){
		try {
			Integer currencySelected = 	allocationPaymentSearch.getCurrency();
			lstAllocationProcesses = null;
			allocationProcess = null;
			lstAllocationProcessIssuerResume = null;
			allocationProcessIssuerResume = null;
			if(Validations.validateIsNotNullAndNotEmpty(currencySelected)){
				 //registros preeliminar -> definitivo
				 List<AllocationProcessTO> lstAllocationProcessTO = allocationPaymentFacade.getPreliminarAllocationProcess(allocationPaymentSearch);
				 if( lstAllocationProcessTO!=null && lstAllocationProcessTO.size()>0 ){
					 lstAllocationProcesses = new GenericDataModel<AllocationProcessTO>(lstAllocationProcessTO);
				 }
				 //registros nuevos -> preeliminar
				 List<AllocationProcessIssuerResumeTO> lstAllocationProcessIssuerResumeTO = allocationPaymentFacade.getCorporativesAllocate(allocationPaymentSearch);
				 if( lstAllocationProcessIssuerResumeTO!=null && lstAllocationProcessIssuerResumeTO.size()>0 ){
					 lstAllocationProcessIssuerResume = new GenericDataModel<AllocationProcessIssuerResumeTO>(lstAllocationProcessIssuerResumeTO);
				 }				
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.CURRENCY_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (ServiceException esx) {
			if(esx.getMessage()!=null){
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}
	
	/**
	 * Metodo que refresca pantalla y busca procesos corporativos en estado definitivo y consignaciones en estado preliminar y definitivo.
	 *
	 * @return the string
	 */
	public String searchForDefinitive(){
		JSFUtilities.hideGeneralDialogues();		
		JSFUtilities.hideComponent(":idConfirmationDefinitive");
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.resetComponent(":frmAllocationPayment");
		searchPaymentCorporatives();
		return "actualScreenAllocationProcess";
	}
	
	/**
	 * Metodo que visualiza detalle del valor.
	 *
	 * @param securityCode the security code
	 */
	public void viewSecurityFinancialImportanceEvent(String securityCode)
	{
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		  securityData.setSecurityCodePk(securityCode);
		  securityData.setUiComponentName("securityHelp");
		  showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	/**
	 * Metodo que limpia la pantalla.
	 *
	 * @throws ServiceException the service exception
	 */
	public void cleanAllocationPaymentProcess() throws ServiceException{
		JSFUtilities.resetViewRoot();
		allocationPaymentSearch = new AllocationPaymentSearchTO();
		List<ParameterTable> lstCurrency = allocationPaymentFacade.getCurrency();
		allocationPaymentSearch.setLstCurrency(lstCurrency);
		allocationPaymentSearch.setPaymentDate(CommonsUtilities.currentDate());
		allocationPaymentSearch.setIssuer(new Issuer());
		allocationPaymentSearch.setSecurity(new Security());
		cleanListAllocationPaymentProcess();
	}
	
	/**
	 * Metodo que limpia listas de ayuda.
	 */
	public void cleanListAllocationPaymentProcess(){
		lstAllocationProcesses = null;
		lstAllocationProcessIssuerResume = null;
		blockAllocationResume = null;
		selectedAllocatedCorporatives = new ArrayList<AllocationCorporativeDetailTO>();
		allocationProcess = null;
		lstBankResume = null;
		indCorporativeAllocate = null;
		indAllocatedCorporative = null;
		indShowAllocationCorporatives = null;
		indShowCorporative = null;
		indShowAllocationBank = null;
		indShowDirectRetention = null;
	}
	/**
	 * Metodo que visualiza las listas de consignacion de pagos.
	 */
	public void showAllocationProcessDetail(){
		List<AllocationCorporativeDetailTO> lstAllocationCorporativeDetail = new ArrayList<AllocationCorporativeDetailTO>();
		try {
			lstAllocationCorporativeDetail = allocationPaymentFacade.getCorporativesByAllocationProcess(allocationProcess.getIdAllocationProcess());
		
			if(Validations.validateListIsNotNullAndNotEmpty(lstAllocationCorporativeDetail)){
				if(indShowAllocationCorporatives == 1){
					indShowAllocationCorporatives = 0;
					allocationProcess.setLstAllocationCorporative(new GenericDataModel<AllocationCorporativeDetailTO>(new ArrayList<AllocationCorporativeDetailTO>()));
				}else{
					indShowAllocationCorporatives = 1;
					allocationProcess.setLstAllocationCorporative(new GenericDataModel<AllocationCorporativeDetailTO>(lstAllocationCorporativeDetail));
				}
			}else{
				indShowAllocationCorporatives = 0;
				allocationProcess.setLstAllocationCorporative(new GenericDataModel<AllocationCorporativeDetailTO>(new ArrayList<AllocationCorporativeDetailTO>()));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Metodo que valida ejecucion de definitivo de la consignacion de pagos.
	 */
	@LoggerAuditWeb
	public void verifyExecutionDefinitiveAllocation(){
		try{
			
			String message = allocationPaymentFacade.validaDefinitiveProcess(allocationProcess);
			if(Validations.validateIsNotNullAndNotEmpty(message)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(message));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				JSFUtilities.showComponent("idConfirmDefinitiveAllocation");	
				JSFUtilities.showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null, PropertiesConstants.MESSAGE_CONFIRMATION_ALLOCATION_DEFINITIVE,null);							
			}
		} catch (ServiceException esx) {
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Metodo que registra consignacin de pago (preliminar)
	 *
	 * @return the string
	 * @throws ServiceException 
	 */
	
	public Boolean validateIssuerCashAccount() throws ServiceException {
		
		CashAccountsSearchTO parameter = new CashAccountsSearchTO();
	    parameter.setCashAccountType(AccountCashFundsType.BENEFIT.getCode());
	    parameter.setState(CashAccountStateType.ACTIVATE.getCode());
	    parameter.setIssuer(allocationProcessIssuerResume[0].getIssuer());
	    parameter.setCurrency(allocationPaymentSearch.getCurrency());
	    List<InstitutionCashAccount> lst = allocationPaymentFacade.getInstCashAccounts(parameter);
	    if(lst != null && lst.size()>0) {
	    	return true;
	    }
	    return false;
	}
	
	public Boolean validateParticipantCorporativeCashAccount() throws ServiceException {
		Boolean allParticipantWithBenefitPayment = true;
		for(AllocationProcessIssuerResumeTO processIssuerResume : allocationProcessIssuerResume){
			
		    List<CorporativeProcessResult> lstCorporativeProcessResult = allocationPaymentFacade.getCorporativeResulByIssuer(processIssuerResume, BooleanType.NO.getCode(), allocationPaymentSearch.getPaymentDate(),allocationPaymentSearch.getCurrency());
		    
		    for(CorporativeProcessResult cpr: lstCorporativeProcessResult) {
		    	
				CashAccountsSearchTO parameter = new CashAccountsSearchTO();
			    parameter.setCashAccountType(AccountCashFundsType.BENEFIT.getCode());
			    parameter.setState(CashAccountStateType.ACTIVATE.getCode());
			    parameter.setParticipant(cpr.getParticipant().getIdParticipantPk().intValue());
			    parameter.setCurrency(allocationPaymentSearch.getCurrency());
			    List<InstitutionCashAccount> lst = allocationPaymentFacade.getInstCashAccounts(parameter);
			    if(lst != null && lst.size()>0) {
			    	allParticipantWithBenefitPayment = true;
			    }else {
			    	return false;
			    }
		    }
		}

	    return allParticipantWithBenefitPayment;
	}
	
	public String registerAllocationProcess(){
		try{
			if(Validations.validateIsNotNull(allocationProcessIssuerResume) && allocationProcessIssuerResume.length > 0){
				paymentDate = CommonsUtilities.convertDatetoString(allocationPaymentSearch.getPaymentDate());
				if(CurrencyType.PYG.getCode().equals(allocationPaymentSearch.getCurrency())){
					currency = CurrencyType.PYG.getCodeIso();
				}else{
					currency = CurrencyType.USD.getCodeIso();
				}

				if( !validateIssuerCashAccount() ) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							"El Emisor no tiene su cuenta de pago de derechos configurada");
					JSFUtilities.showSimpleValidationDialog();
					return null;
				}
				if( !validateParticipantCorporativeCashAccount() ) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							"Los Depositantes no tiene su cuenta de pago de derechos configurada");
					JSFUtilities.showSimpleValidationDialog();
					return null;
				}
				
				allocationProcessRegister = allocationPaymentFacade.fillDataRegisterAllocationProcess(allocationPaymentSearch,allocationProcessIssuerResume);
				allocationProcessIssuerResume = null;
				return "registerAllocationProcess";
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						PropertiesUtilities.getMessage(PropertiesConstants.ALLOCATION_PROCESS_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
		} catch (ServiceException esx) {
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		return null;
	}

	/**
	 * Metodo que visualiza lista detalle de consignacion de pagos.
	 */
	public void showAllocateProcess(){
		try {
			if(Validations.validateIsNull(indCorporativeAllocate)){
				lstCorporativeAllocate = allocationPaymentFacade.getCorporativeToAllocate(allocateProcessIssuer,allocationPaymentSearch.getPaymentDate(), allocationPaymentSearch.getCurrency());
				if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeAllocate)){
					if(Validations.validateIsNull(indCorporativeAllocate)){
						indCorporativeAllocate = 1;
					}else{
						indCorporativeAllocate = 0;
					}
				}else{
					indCorporativeAllocate = 0;
				}
			}else{
				indCorporativeAllocate = null;
			}
		} catch (ServiceException esx) {
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Metodo que visualiza lista detalle de consignacion de pagos.
	 */
	public void showAllocatedProcess(){
		try{
			if(Validations.validateIsNull(indAllocatedCorporative)){
				lstAllocatedCorporative = allocationPaymentFacade.getAllocatedCorporatives(allocateProcessIssuer,allocationPaymentSearch.getPaymentDate(), allocationPaymentSearch.getCurrency());
				if(Validations.validateListIsNotNullAndNotEmpty(lstAllocatedCorporative)){
					if(Validations.validateIsNull(indAllocatedCorporative)){
						indAllocatedCorporative = 1;
					}else{
						indAllocatedCorporative = 0;
					}
				}else{
					indAllocatedCorporative = 0;
				}
			}else{
				indAllocatedCorporative = null;
			}
		} catch (ServiceException esx) {
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Metodo que limpia.
	 */
	public void validateIssuerSearch(){
		cleanResult();
	}
	
	/**
	 * Metodo que limpia.
	 */
	public void validateSecuritySearch(){
		cleanResult();
	}
	
	/**
	 * Metodo que limpia.
	 */
	public void cleanResult(){
		cleanListAllocationPaymentProcess();
	}
	/**
	 * Metodo que regresa a la pantalla principal y limpia.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String backScreenAfterProcess() throws ServiceException{
		cleanAllocationPaymentProcess();
		return "backAllocationPaymentManagement";
	}

	/**
	 * Metodo que regresa a la pantalla principal
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String backScreen() throws ServiceException{
		return "backAllocationPaymentManagement";
	}

	/**
	 * Metodo que regresa a la pantalla principal
	 *
	 * @return the string
	 */
	public String backFromDetail(){
		return "backManagement";
	}

	/**
	 * Metodo que validad registro de la consignacion de pagos.
	 */
	public void validateSaveAllocationProcess(){
		boolean blCorporativeStates = true;
		if(Validations.validateListIsNotNullAndNotEmpty(selectedAllocatedCorporatives)){
			BigDecimal selectedCorporativePaymentAmount = BigDecimal.ZERO;
			for(AllocationCorporativeDetailTO corporativeDetail : selectedAllocatedCorporatives){
				selectedCorporativePaymentAmount = selectedCorporativePaymentAmount.add(corporativeDetail.getBenefitAmount());
				if(!CorporateProcessStateType.DEFINITIVE.getCode().equals(corporativeDetail.getCorporateState())){
					blCorporativeStates = false;
					break;
				}
			}
			
			if( allocationProcessRegister!=null && allocationProcessRegister.size()>0 && 
				allocationProcessRegister.get(0).getLstIssuerCashAccount() !=null && allocationProcessRegister.get(0).getLstIssuerCashAccount().size()>0 
			){
				AllocateIssuerCashAccountTO issuerCA =  allocationProcessRegister.get(0).getLstIssuerCashAccount().get(0);
				BigDecimal availableAmount = (issuerCA.getAvailableAmount()!=null)?issuerCA.getAvailableAmount():BigDecimal.ZERO;
				BigDecimal allocateAmount = issuerCA.getAllocateAmount();
				
				if(availableAmount.compareTo(allocateAmount) == -1 ) {

					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							"El Emisor no tiene el monto suficiente en la cuenta de efectivo. Verifique ");
					JSFUtilities.showSimpleValidationDialog();
					return;
					
				}
				
			}else {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						"El Emisor no tiene la suficiente informacion para poder registrar la consignacion ");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
		}else{
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVES_NOT_SELECTED_TO_ALLOCATE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(blCorporativeStates){
			JSFUtilities.showComponent("idConfirmRegisterAllocation");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null, PropertiesConstants.MESSAGE_CONFIRM_REGISTER_ALLOCATION_PROCESS,null);
		}else{
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.CORPORATIVES_SELECTED_NOT_DEFINITIVE_STATE));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Metodo que ejecuta evento de seleccion.
	 *
	 * @param row the row
	 */
	public void onRowSelect(AllocationCorporativeDetailTO row){
		String issuerSelected = row.getIssuer();
		BigDecimal allocationAmount = BigDecimal.ZERO;
		boolean add = false;

		
		//GET THE ACTUAL ALLOCATION AMOUNT
		if(Validations.validateListIsNotNullAndNotEmpty(allocationProcessRegister)){
			for(AllocationProcessRegisterTO allocationProcess : allocationProcessRegister){
				
				List<AllocateIssuerCashAccountTO> lstCashAccount = allocationProcess.getLstIssuerCashAccount();	
				if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccount)){
					for(AllocateIssuerCashAccountTO cashAccount : lstCashAccount){
						if(cashAccount.getIssuer().equals(issuerSelected)){
							allocationAmount = cashAccount.getAllocateAmount();
						}
					}
				}
				
			}
		}
		

		if(row.isSelection()){
			selectedAllocatedCorporatives.add(row);
			add = true;
		}else{
			selectedAllocatedCorporatives.remove(row);
		}

		if(add){
			
			if(Validations.validateListIsNotNullAndNotEmpty(selectedAllocatedCorporatives)){
				for(AllocationCorporativeDetailTO corporativeDetail : selectedAllocatedCorporatives){
					if(corporativeDetail.getIdCorporativeProcess().equals(row.getIdCorporativeProcess())){
						BigDecimal grossAmount = corporativeDetail.getBenefitAmount();
						BigDecimal commissionAmount = corporativeDetail.getCommissionAmount();

						if(commissionExcluded){
							allocationAmount = allocationAmount.add(grossAmount);
						}else{
							allocationAmount = allocationAmount.add(grossAmount.add(commissionAmount));
						}
					}
				}
			}else{
				allocationAmount = BigDecimal.ZERO;
			}
		}else{
			if(Validations.validateListIsNotNullAndNotEmpty(selectedAllocatedCorporatives)){
				allocationAmount = BigDecimal.ZERO;
				for(AllocationCorporativeDetailTO corporativeDetail : selectedAllocatedCorporatives){
					BigDecimal grossAmount = corporativeDetail.getBenefitAmount();
					BigDecimal commissionAmount = corporativeDetail.getCommissionAmount();

					if(corporativeDetail.getIssuer().equals(issuerSelected)){
						if(commissionExcluded){
							allocationAmount = allocationAmount.add((grossAmount));
						}else{
							allocationAmount = allocationAmount.add(grossAmount.add(commissionAmount));
						}
					}
				}
			}else{
				allocationAmount = BigDecimal.ZERO;
			}
		}

		if(Validations.validateListIsNotNullAndNotEmpty(allocationProcessRegister)){
			for(AllocationProcessRegisterTO allocationProcess : allocationProcessRegister){
				List<AllocateIssuerCashAccountTO> lstCashAccount = allocationProcess.getLstIssuerCashAccount();
				if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccount)){
					for(AllocateIssuerCashAccountTO cashAccount : lstCashAccount){
						if(cashAccount.getIssuer().equals(issuerSelected)){
							cashAccount.setAllocateAmount(allocationAmount);
						}
					}
				}
			}
		}
	}

	/**
	 * Metodo que registra consignacion de pagos.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void saveAllocationProcess() throws ServiceException{
		try{
			List<String> lstCorporativeDetail = new ArrayList<String>();
			if(Validations.validateListIsNotNullAndNotEmpty(selectedAllocatedCorporatives)){
				for(AllocationCorporativeDetailTO allocationRegister : selectedAllocatedCorporatives){
					lstCorporativeDetail.add(allocationRegister.getIdCorporativeProcess());
				}
			}
			allocationPaymentFacade.registerPreliminarAllocationProcess(lstCorporativeDetail,commissionExcluded,allocationPaymentSearch);
			
			cleanResult();
			
			allocationPaymentSearch = new AllocationPaymentSearchTO();
			List<ParameterTable> lstCurrency = allocationPaymentFacade.getCurrency();
			allocationPaymentSearch.setLstCurrency(lstCurrency);
			allocationPaymentSearch.setPaymentDate(CommonsUtilities.currentDate());
			allocationPaymentSearch.setIssuer(new Issuer());
			allocationPaymentSearch.setSecurity(new Security());
			cleanListAllocationPaymentProcess();
			
			
			//searchPaymentCorporatives();
			
			JSFUtilities.showComponent("idConfirmationRegister");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.HEADER_SUCCESSFUL, null, PropertiesConstants.ALLOCATION_PROCESS_REGISTER_SUCCESSFULLY,null);
		} catch (ServiceException esx) {
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Metodo que visualiza listas de consignacion de pagos.
	 */
	public void showAllocationBankDetail(){

		List<Integer> lstBlockMotives = new ArrayList<Integer>();
		lstBlockMotives.add(AllocationProcessType.DIRECT_PAYMENT.getCode());
		lstBlockMotives.add(AllocationProcessType.HOLDER_ACCOUNT_RETENTION.getCode());
		lstBlockMotives.add(AllocationProcessType.RNT_RETENTION.getCode());
		lstBlockMotives.add(AllocationProcessType.BLOCK_BALANCE_RETENTION.getCode());
		lstBlockMotives.add(AllocationProcessType.NO_BANK_ACCOUNT_RETENTION.getCode());

		if(Validations.validateIsNull(indShowAllocationBank)){

			indShowDirectRetention = 1;
			blockAllocationResume = allocationPaymentFacade.getPaymentResumeDetail(allocationProcess,lstBlockMotives);
			/*
			List<AllocationBankResumeTO> bankResumeList = allocationPaymentFacade.getAllocationBankDetail(allocationProcess);
			if(Validations.validateListIsNotNullAndNotEmpty(bankResumeList)){
				lstBankResume = new GenericDataModel<AllocationBankResumeTO>(bankResumeList);
				indShowAllocationBank = 1;
				indShowAllocationCorporatives = 0;
			}else{
				indShowAllocationBank = 0;
				indShowAllocationCorporatives = 0;
			}

			blockAllocationResume = allocationPaymentFacade.getBlockDirectPaymentResumeDetail(allocationProcess,lstBlockMotives);
			if(Validations.validateListIsNotNullAndNotEmpty(blockAllocationResume)){
				indShowDirectRetention = 1;
				indShowAllocationCorporatives = 0;
			}else{
				indShowDirectRetention = 0;
				indShowAllocationCorporatives = 0;
			}
			 */
		}else{
			
			indShowDirectRetention = 0;
			
			blockAllocationResume = allocationPaymentFacade.getPaymentResumeDetail(allocationProcess,lstBlockMotives);
			//blockAllocationResume = allocationPaymentFacade.getBlockDirectPaymentResumeDetail(allocationProcess,lstBlockMotives);
			/*
			if(indShowAllocationBank == 1){
				indShowAllocationBank = 0;
				indShowAllocationCorporatives = 0;
			}else{
				List<AllocationBankResumeTO> bankResumeList = allocationPaymentFacade.getAllocationBankDetail(allocationProcess);
				if(Validations.validateListIsNotNullAndNotEmpty(bankResumeList)){
					lstBankResume = new GenericDataModel<AllocationBankResumeTO>(bankResumeList);
					indShowAllocationBank = 1;
					indShowAllocationCorporatives = 0;
				}else{
					indShowAllocationBank = 0;
					indShowAllocationCorporatives = 0;
				}

				blockAllocationResume = allocationPaymentFacade.getBlockDirectPaymentResumeDetail(allocationProcess,lstBlockMotives);
				if(Validations.validateListIsNotNullAndNotEmpty(blockAllocationResume)){
					indShowDirectRetention = 1;
					indShowAllocationCorporatives = 0;
				}else{
					indShowDirectRetention = 0;
					indShowAllocationCorporatives = 0;
				}
			}
			*/
		}
	}

	/**
	 * Metodo que visualiza lista detalle de consignacion.
	 */
	public void showAllocationHolderDetail(){
		allocationProcess.setBankID(bankResume.getBankID());
		if(Validations.validateIsNull(indShowAllocationCorporatives)){
			List<Integer> lstPaymentMotives = new ArrayList<Integer>();
			lstPaymentMotives.add(AllocationProcessType.INDIRECT_PAYMENT.getCode());
			allocationPaymentResume = allocationPaymentFacade.getAllocationResumeDetail(allocationProcess,lstPaymentMotives);

			if(Validations.validateListIsNotNullAndNotEmpty(allocationPaymentResume)){
				allocationProcess.setHolderQuantity(bankResume.getHolderQuantity());
				allocationProcess.setBankAmount(new BigDecimal(bankResume.getBankAmount()));
				allocationProcess.setBank(bankResume.getBank());
				indShowAllocationCorporatives = 1;
			}else{
				indShowAllocationCorporatives = 0;
			}
		}else{
			if(indShowAllocationCorporatives == 1){
				indShowAllocationCorporatives = 0;
			}else{
				List<Integer> lstPaymentMotives = new ArrayList<Integer>();
				lstPaymentMotives.add(AllocationProcessType.INDIRECT_PAYMENT.getCode());
				allocationPaymentResume = allocationPaymentFacade.getAllocationResumeDetail(allocationProcess,lstPaymentMotives);

				if(Validations.validateListIsNotNullAndNotEmpty(allocationPaymentResume)){
					allocationProcess.setHolderQuantity(bankResume.getHolderQuantity());
					allocationProcess.setBankAmount(new BigDecimal(bankResume.getBankAmount()));
					allocationProcess.setBank(bankResume.getBank());
					indShowAllocationCorporatives = 1;
				}else{
					indShowAllocationCorporatives = 0;
				}

			}
		}
	}

	/**
	 * Metodo que registra consignacion definitiva.
	 */
	@LoggerAuditWeb
	public void executeDefinitiveAllocation(){
		try {
			JSFUtilities.hideComponent(":idConfirmDefinitiveAllocation");
			allocationPaymentFacade.executeDefinitiveAllocation(allocationProcess.getIdAllocationProcess().toString());
			
			allocationPaymentSearch = new AllocationPaymentSearchTO();
			List<ParameterTable> lstCurrency = allocationPaymentFacade.getCurrency();
			allocationPaymentSearch.setLstCurrency(lstCurrency);
			allocationPaymentSearch.setPaymentDate(CommonsUtilities.currentDate());
			allocationPaymentSearch.setIssuer(new Issuer());
			allocationPaymentSearch.setSecurity(new Security());
			cleanListAllocationPaymentProcess();
			
			JSFUtilities.showComponent("idConfirmationDefinitive");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.HEADER_SUCCESSFUL, null, PropertiesConstants.ALLOCATION_PROCESS_DEFINITIVE_SUCCESSFULLY,null);
			return;
		} catch(ServiceException esx){
			if(esx.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	private Client createClientJerser() throws NoSuchAlgorithmException,KeyManagementException{
		return Client.create();
	}
	
	
	public void createRetirementOperationConfirm(String IdCorporativeOperationPk) {
		
    	StringBuilder urlResource = new StringBuilder();
        urlResource.append(serverPath).append("/").append(ModuleWarType.CUSTODY.getValue()).
        append("/resources/CustodyProcessResource/retirementoperationConfirm").
        append("/").append(IdCorporativeOperationPk);
        try {
			createClientJerser().resource(urlResource.toString()).type("application/json").get(ClientResponse.class);
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UniformInterfaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Gets the selected allocated corporatives.
	 *
	 * @return the selectedAllocatedCorporatives
	 */
	public List<AllocationCorporativeDetailTO> getSelectedAllocatedCorporatives() {
		return selectedAllocatedCorporatives;
	}
	
	/**
	 * Sets the selected allocated corporatives.
	 *
	 * @param selectedAllocatedCorporatives the selectedAllocatedCorporatives to set
	 */
	public void setSelectedAllocatedCorporatives(
			List<AllocationCorporativeDetailTO> selectedAllocatedCorporatives) {
		this.selectedAllocatedCorporatives = selectedAllocatedCorporatives;
	}

	/**
	 * Metodo que realiza evento de seleccion de incluir o no comision.
	 */
	public void checkCommissionIncluded(){

		if(Validations.validateListIsNotNullAndNotEmpty(allocationProcessRegister)){
			for(AllocationProcessRegisterTO allocationProcess : allocationProcessRegister){
				BigDecimal allocationAmount = BigDecimal.ZERO;
				List<AllocateIssuerCashAccountTO> lstCashAccount = allocationProcess.getLstIssuerCashAccount();
				if(Validations.validateListIsNotNullAndNotEmpty(lstCashAccount)){
					for(AllocateIssuerCashAccountTO cashAccount : lstCashAccount){
						if(Validations.validateListIsNotNullAndNotEmpty(selectedAllocatedCorporatives)){
							for(AllocationCorporativeDetailTO corporative : selectedAllocatedCorporatives){
								if(cashAccount.getIssuer().equals(corporative.getIssuer())){
									BigDecimal grossAmount = corporative.getBenefitAmount();
									BigDecimal commissionAmount = corporative.getCommissionAmount();

									if(commissionExcluded){
										allocationAmount = allocationAmount.add(grossAmount);
									}else{
										allocationAmount = allocationAmount.add(grossAmount.add(commissionAmount));
									}
								}
							}
							cashAccount.setAllocateAmount(allocationAmount);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Gets the allocation payment facade.
	 *
	 * @return the allocation payment facade
	 */
	public AllocationPaymentBenefitServiceFacade getAllocationPaymentFacade() {
		return allocationPaymentFacade;
	}
	
	/**
	 * Sets the allocation payment facade.
	 *
	 * @param allocationPaymentFacade the new allocation payment facade
	 */
	public void setAllocationPaymentFacade(
			AllocationPaymentBenefitServiceFacade allocationPaymentFacade) {
		this.allocationPaymentFacade = allocationPaymentFacade;
	}
	
	/**
	 * Gets the lst allocation processes.
	 *
	 * @return the lst allocation processes
	 */
	public GenericDataModel<AllocationProcessTO> getLstAllocationProcesses() {
		return lstAllocationProcesses;
	}
	
	/**
	 * Sets the lst allocation processes.
	 *
	 * @param lstAllocationProcesses the new lst allocation processes
	 */
	public void setLstAllocationProcesses(
			GenericDataModel<AllocationProcessTO> lstAllocationProcesses) {
		this.lstAllocationProcesses = lstAllocationProcesses;
	}
	
	/**
	 * Gets the allocation process.
	 *
	 * @return the allocation process
	 */
	public AllocationProcessTO getAllocationProcess() {
		return allocationProcess;
	}
	
	/**
	 * Sets the allocation process.
	 *
	 * @param allocationProcess the new allocation process
	 */
	public void setAllocationProcess(AllocationProcessTO allocationProcess) {
		this.allocationProcess = allocationProcess;
	}
	
	/**
	 * Gets the lst allocation process issuer resume.
	 *
	 * @return the lst allocation process issuer resume
	 */
	public GenericDataModel<AllocationProcessIssuerResumeTO> getLstAllocationProcessIssuerResume() {
		return lstAllocationProcessIssuerResume;
	}
	
	/**
	 * Sets the lst allocation process issuer resume.
	 *
	 * @param lstAllocationProcessIssuerResume the new lst allocation process issuer resume
	 */
	public void setLstAllocationProcessIssuerResume(
			GenericDataModel<AllocationProcessIssuerResumeTO> lstAllocationProcessIssuerResume) {
		this.lstAllocationProcessIssuerResume = lstAllocationProcessIssuerResume;
	}
	
	/**
	 * Gets the allocation process issuer resume.
	 *
	 * @return the allocation process issuer resume
	 */
	public AllocationProcessIssuerResumeTO[] getAllocationProcessIssuerResume() {
		return allocationProcessIssuerResume;
	}
	
	/**
	 * Sets the allocation process issuer resume.
	 *
	 * @param allocationProcessIssuerResume the new allocation process issuer resume
	 */
	public void setAllocationProcessIssuerResume(
			AllocationProcessIssuerResumeTO[] allocationProcessIssuerResume) {
		this.allocationProcessIssuerResume = allocationProcessIssuerResume;
	}
	
		
	/**
	 * Gets the ind corporative allocate.
	 *
	 * @return the ind corporative allocate
	 */
	public Integer getIndCorporativeAllocate() {
		return indCorporativeAllocate;
	}
	
	/**
	 * Sets the ind corporative allocate.
	 *
	 * @param indCorporativeAllocate the new ind corporative allocate
	 */
	public void setIndCorporativeAllocate(Integer indCorporativeAllocate) {
		this.indCorporativeAllocate = indCorporativeAllocate;
	}
	
		
	/**
	 * Gets the allocation payment search.
	 *
	 * @return the allocation payment search
	 */
	public AllocationPaymentSearchTO getAllocationPaymentSearch() {
		return allocationPaymentSearch;
	}
	
	/**
	 * Sets the allocation payment search.
	 *
	 * @param allocationPaymentSearch the new allocation payment search
	 */
	public void setAllocationPaymentSearch(
			AllocationPaymentSearchTO allocationPaymentSearch) {
		this.allocationPaymentSearch = allocationPaymentSearch;
	}
	
	/**
	 * Gets the allocate process issuer.
	 *
	 * @return the allocate process issuer
	 */
	public AllocationProcessIssuerResumeTO getAllocateProcessIssuer() {
		return allocateProcessIssuer;
	}
	
	/**
	 * Sets the allocate process issuer.
	 *
	 * @param allocateProcessIssuer the new allocate process issuer
	 */
	public void setAllocateProcessIssuer(
			AllocationProcessIssuerResumeTO allocateProcessIssuer) {
		this.allocateProcessIssuer = allocateProcessIssuer;
	}
	
	/**
	 * Gets the ind show corporative.
	 *
	 * @return the ind show corporative
	 */
	public Integer getIndShowCorporative() {
		return indShowCorporative;
	}
	
	/**
	 * Sets the ind show corporative.
	 *
	 * @param indShowCorporative the new ind show corporative
	 */
	public void setIndShowCorporative(Integer indShowCorporative) {
		this.indShowCorporative = indShowCorporative;
	}
	
	/**
	 * Gets the ind allocated corporative.
	 *
	 * @return the ind allocated corporative
	 */
	public Integer getIndAllocatedCorporative() {
		return indAllocatedCorporative;
	}
	
	/**
	 * Sets the ind allocated corporative.
	 *
	 * @param indAllocatedCorporative the new ind allocated corporative
	 */
	public void setIndAllocatedCorporative(Integer indAllocatedCorporative) {
		this.indAllocatedCorporative = indAllocatedCorporative;
	}
	
	/**
	 * Gets the ind show allocation corporatives.
	 *
	 * @return the ind show allocation corporatives
	 */
	public Integer getIndShowAllocationCorporatives() {
		return indShowAllocationCorporatives;
	}
	
	/**
	 * Sets the ind show allocation corporatives.
	 *
	 * @param indShowAllocationCorporatives the new ind show allocation corporatives
	 */
	public void setIndShowAllocationCorporatives(
			Integer indShowAllocationCorporatives) {
		this.indShowAllocationCorporatives = indShowAllocationCorporatives;
	}
	
	/**
	 * Gets the lst corporative allocate.
	 *
	 * @return the lst corporative allocate
	 */
	public List<AllocationCorporativeDetailTO> getLstCorporativeAllocate() {
		return lstCorporativeAllocate;
	}
	
	/**
	 * Sets the lst corporative allocate.
	 *
	 * @param lstCorporativeAllocate the new lst corporative allocate
	 */
	public void setLstCorporativeAllocate(
			List<AllocationCorporativeDetailTO> lstCorporativeAllocate) {
		this.lstCorporativeAllocate = lstCorporativeAllocate;
	}
	
	/**
	 * Gets the lst allocated corporative.
	 *
	 * @return the lst allocated corporative
	 */
	public List<AllocationCorporativeDetailTO> getLstAllocatedCorporative() {
		return lstAllocatedCorporative;
	}
	
	/**
	 * Sets the lst allocated corporative.
	 *
	 * @param lstAllocatedCorporative the new lst allocated corporative
	 */
	public void setLstAllocatedCorporative(
			List<AllocationCorporativeDetailTO> lstAllocatedCorporative) {
		this.lstAllocatedCorporative = lstAllocatedCorporative;
	}
	
	/**
	 * Gets the corporative allocate.
	 *
	 * @return the corporative allocate
	 */
	public AllocationCorporativeDetailTO getCorporativeAllocate() {
		return corporativeAllocate;
	}
	
	/**
	 * Sets the corporative allocate.
	 *
	 * @param corporativeAllocate the new corporative allocate
	 */
	public void setCorporativeAllocate(
			AllocationCorporativeDetailTO corporativeAllocate) {
		this.corporativeAllocate = corporativeAllocate;
	}
	
	/**
	 * Gets the allocated corporative.
	 *
	 * @return the allocated corporative
	 */
	public AllocationCorporativeDetailTO getAllocatedCorporative() {
		return allocatedCorporative;
	}
	
	/**
	 * Sets the allocated corporative.
	 *
	 * @param allocatedCorporative the new allocated corporative
	 */
	public void setAllocatedCorporative(
			AllocationCorporativeDetailTO allocatedCorporative) {
		this.allocatedCorporative = allocatedCorporative;
	}
	
	/**
	 * Gets the payment date.
	 *
	 * @return the payment date
	 */
	public String getPaymentDate() {
		return paymentDate;
	}
	
	/**
	 * Sets the payment date.
	 *
	 * @param paymentDate the new payment date
	 */
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the issuer description.
	 *
	 * @return the issuer description
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}
	
	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the new issuer description
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}
	
	/**
	 * Gets the allocation process register.
	 *
	 * @return the allocation process register
	 */
	public List<AllocationProcessRegisterTO> getAllocationProcessRegister() {
		return allocationProcessRegister;
	}
	
	/**
	 * Sets the allocation process register.
	 *
	 * @param allocationProcessRegister the new allocation process register
	 */
	public void setAllocationProcessRegister(
			List<AllocationProcessRegisterTO> allocationProcessRegister) {
		this.allocationProcessRegister = allocationProcessRegister;
	}
	
	/**
	 * Checks if is commission excluded.
	 *
	 * @return true, if is commission excluded
	 */
	public boolean isCommissionExcluded() {
		return commissionExcluded;
	}
	
	/**
	 * Sets the commission excluded.
	 *
	 * @param commissionExcluded the new commission excluded
	 */
	public void setCommissionExcluded(boolean commissionExcluded) {
		this.commissionExcluded = commissionExcluded;
	}
	
	/**
	 * Gets the ind show allocation bank.
	 *
	 * @return the ind show allocation bank
	 */
	public Integer getIndShowAllocationBank() {
		return indShowAllocationBank;
	}
	
	/**
	 * Sets the ind show allocation bank.
	 *
	 * @param indShowAllocationBank the new ind show allocation bank
	 */
	public void setIndShowAllocationBank(Integer indShowAllocationBank) {
		this.indShowAllocationBank = indShowAllocationBank;
	}
	
	/**
	 * Gets the lst bank resume.
	 *
	 * @return the lst bank resume
	 */
	public GenericDataModel<AllocationBankResumeTO> getLstBankResume() {
		return lstBankResume;
	}
	
	/**
	 * Sets the lst bank resume.
	 *
	 * @param lstBankResume the new lst bank resume
	 */
	public void setLstBankResume(
			GenericDataModel<AllocationBankResumeTO> lstBankResume) {
		this.lstBankResume = lstBankResume;
	}
	
	/**
	 * Gets the bank resume.
	 *
	 * @return the bank resume
	 */
	public AllocationBankResumeTO getBankResume() {
		return bankResume;
	}
	
	/**
	 * Sets the bank resume.
	 *
	 * @param bankResume the new bank resume
	 */
	public void setBankResume(AllocationBankResumeTO bankResume) {
		this.bankResume = bankResume;
	}
	
	/**
	 * Gets the allocation payment resume.
	 *
	 * @return the allocation payment resume
	 */
	public List<AllocationIssuerResumeTO> getAllocationPaymentResume() {
		return allocationPaymentResume;
	}
	
	/**
	 * Sets the allocation payment resume.
	 *
	 * @param allocationPaymentResume the new allocation payment resume
	 */
	public void setAllocationPaymentResume(
			List<AllocationIssuerResumeTO> allocationPaymentResume) {
		this.allocationPaymentResume = allocationPaymentResume;
	}
	
	/**
	 * Gets the block allocation resume.
	 *
	 * @return the block allocation resume
	 */
	public List<AllocationIssuerResumeTO> getBlockAllocationResume() {
		return blockAllocationResume;
	}
	
	/**
	 * Sets the block allocation resume.
	 *
	 * @param blockAllocationResume the new block allocation resume
	 */
	public void setBlockAllocationResume(
			List<AllocationIssuerResumeTO> blockAllocationResume) {
		this.blockAllocationResume = blockAllocationResume;
	}
	
	/**
	 * Gets the ind show direct retention.
	 *
	 * @return the ind show direct retention
	 */
	public Integer getIndShowDirectRetention() {
		return indShowDirectRetention;
	}
	
	/**
	 * Sets the ind show direct retention.
	 *
	 * @param indShowDirectRetention the new ind show direct retention
	 */
	public void setIndShowDirectRetention(Integer indShowDirectRetention) {
		this.indShowDirectRetention = indShowDirectRetention;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

}