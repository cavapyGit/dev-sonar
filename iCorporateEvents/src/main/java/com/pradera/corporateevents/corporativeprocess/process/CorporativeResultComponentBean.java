package com.pradera.corporateevents.corporativeprocess.process;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CorporateServiceBean;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporativeProcessResultTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.BlockMovement;
import com.pradera.model.component.MovementBehavior;
import com.pradera.model.component.type.ParameterBalanceType;
import com.pradera.model.corporatives.CorporativeOperation;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporativeResultComponentBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class CorporativeResultComponentBean extends CrudDaoServiceBean{

	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(CorporativeResultComponentBean.class);
	
	/** The corporate service bean. */
	@EJB
	CorporateServiceBean corporateServiceBean;
	
	/**
	 * Checks for new block documents.
	 *
	 * @param objArray the obj array
	 * @param corporativeProcessResultTO the corporative process result to
	 * @return true, if successful
	 */
	public boolean hasNewBlockDocuments(Object[]objArray,CorporativeProcessResultTO corporativeProcessResultTO){
		if(corporativeProcessResultTO.getIdHolderAccount().equals((Long)objArray[0])
				&& corporativeProcessResultTO.getIdParticipant().equals((Long)objArray[11])){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Gets the new block documents.
	 *
	 * @param blockMovements the block movements
	 * @param blockedDocs the blocked docs
	 * @return the new block documents
	 */
	public List<Object[]> getNewBlockDocuments(List<BlockMovement> blockMovements, List<Object[]> blockedDocs){				
		for(BlockMovement blockMovement : blockMovements){
			if(Validations.validateIsNotNull(blockMovement.getBlockOperation())){;
				blockedDocs.add(corporateServiceBean.getBlockDocumentsObjects(blockMovement.getBlockOperation()));
			}else if(Validations.validateIsNotNull(blockMovement.getUnblockOperation())){
				blockedDocs.add(corporateServiceBean.getUnblockDocumentsObjects(blockMovement.getUnblockOperation()));
			}
		}	
		return blockedDocs;
	}
	
	/**
	 * Method to update the balances according the balance Type.
	 *
	 * @param holderAccounts the holder accounts
	 * @param balanceType the balance type
	 * @param stockQuantity the stock quantity
	 * @param behavior the behavior
	 * @return CorporativeProcessResult
	 * @throws ServiceException the service exception
	 */
	private Object[] getNewBalances(Object[]holderAccounts, int balanceType, BigDecimal stockQuantity,Long behavior) throws ServiceException{
		switch (balanceType)
		{
			case ComponentConstant.TOTAL_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.TOTAL_BALANCE.getDescripcion());
				holderAccounts[10] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[10], stockQuantity, balanceType));
			break;
			case ComponentConstant.AVAILABLE_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.AVAILABLE_BALANCE.getDescripcion());
				holderAccounts[1] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[1], stockQuantity, balanceType));
			break;			
			case ComponentConstant.PAWN_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.PAWN_BALANCE.getDescripcion());
				holderAccounts[2] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[2], stockQuantity, balanceType));
			break;
			case ComponentConstant.BAN_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.BAN_BALANCE.getDescripcion());
				holderAccounts[3] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[3], stockQuantity, balanceType));
			break;
			case ComponentConstant.OTHERS_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.OTHERS_BALANCE.getDescripcion());
				holderAccounts[4] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[4], stockQuantity, balanceType));
			break;
			case ComponentConstant.RESERVE_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.RESERVE_BALANCE.getDescripcion());
				holderAccounts[5] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[5], stockQuantity, balanceType));
			break;
			case ComponentConstant.OPPOSITION_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.OPPOSITION_BALANCE.getDescripcion());
				holderAccounts[6] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[6], stockQuantity, balanceType));
			break;

			case ComponentConstant.REPORTING_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.REPORTING_BALANCE.getDescripcion());
				holderAccounts[8] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[8], stockQuantity, balanceType));
			break;
			case ComponentConstant.REPORTED_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.REPORTED_BALANCE.getDescripcion());
				holderAccounts[9] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[9], stockQuantity, balanceType));
			break;
			case ComponentConstant.MARGIN_BALANCE:
				logger.debug("Balance to modify :" + ParameterBalanceType.MARGIN_BALANCE.getDescripcion());
				holderAccounts[7] = (updateBalance(behavior, 
						(BigDecimal) holderAccounts[7], stockQuantity, balanceType));			
			break;
			
		}
		return holderAccounts;
	}
	
	/**
	 * Method to update the balances according the behavior.
	 *
	 * @param behavior the behavior
	 * @param originalBalance the original balance
	 * @param stockQuantity the stock quantity
	 * @param balanceType the balance type
	 * @return BigDecimal
	 * @throws ServiceException the service exception
	 */
	private BigDecimal updateBalance(Long behavior, BigDecimal originalBalance, BigDecimal stockQuantity, int balanceType) throws ServiceException
	{
		BigDecimal resultBalance= new BigDecimal(0);
		try {
			resultBalance= resultBalance.add(originalBalance);
			if (originalBalance.doubleValue() < 0d) {
				throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
															PropertiesUtilities.getExceptionMessage(ErrorServiceType.NEGATIVE_BALANCE.getMessage()));
			}
			if (ComponentConstant.SUM == behavior.longValue()) {
				resultBalance= resultBalance.add(stockQuantity);
			}
			else if (ComponentConstant.SUBTRACTION == behavior.longValue()){
				resultBalance= resultBalance.subtract(stockQuantity);
			}
			if (resultBalance.doubleValue() < 0d) {
				throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE,
						ErrorServiceType.NEGATIVE_BALANCE.getMessage());
			}
		} catch (ServiceException objException){
			logger.error(objException.getMessage());
			logger.error(("method updateBalance ::: balanceType: " +balanceType +", origin balance: "+ originalBalance.toString() + ", quantity operation: "+stockQuantity.toString()));
			throw objException;
		}
		
		return resultBalance;
	}

}
