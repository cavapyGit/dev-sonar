package com.pradera.corporateevents.corporativeprocess.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateServiceFacade;
import com.pradera.corporateevents.corporativeprocess.process.CorporateEventProducer;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporateMonitoringTO;
import com.pradera.corporateevents.stockcalculation.facade.ManageStockCalculationServiceFacade;
import com.pradera.corporateevents.stockcalculation.service.StockCalculationServiceBean;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.component.corporatives.to.CorporateInformationTO;
import com.pradera.integration.component.corporatives.to.InterestCouponPaymentTO;
import com.pradera.integration.component.corporatives.to.InterestPaymentQueryTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.securities.to.CouponObjectTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalanceExp;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.corporatives.CorporativeEventType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.stockcalculation.StockCalculationBalance;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeSituationType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.RequestAffectationStateType;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityHistoryBalance;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.type.ProcessLoggerStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CorporateProcessServiceBen.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/06/2014
 */
@Stateless
public class CorporateProcessServiceBean extends CrudDaoServiceBean {
	
	private static final int MAXIMUM_RANGE = 100; // issue 1322: Para procesar de 1000 en 1000 los vencimientos DPF
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The stock calculation service. */
	@EJB
	private StockCalculationServiceBean stockCalculationService;
	
	/** The Corporate Monitoring Service Bean. */
	@EJB
	private CorporateServiceBean corporateServiceBean;
	
	/** The corporate service facade. */
	@EJB
	private CorporateServiceFacade corporateServiceFacade;
	
	/** The stock calculation process. */
	private StockCalculationProcess stockCalculationProcess;
	
	/** The manage stock calculation facade. */
	@EJB
	ManageStockCalculationServiceFacade manageStockCalculationFacade;
	
	/** The corporate event producer. */
	@Inject
	CorporateEventProducer corporateEventProducer;
		
	/** The corporate process facade. */
	@EJB 
	CorporateProcessServiceFacade corporateProcessFacade;
	
	/** The loader entity service. */
	@EJB
	LoaderEntityServiceBean loaderEntityService;
	
	/** The securities query service. */
	@EJB
	SecuritiesQueryServiceBean securitiesQueryService;

	/** The executor component service bean. */
	@Inject
	Instance<AccountsComponentService> executorComponentServiceBean;
	
	/** The securities component service bean. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentServiceBean;
	
	/** The fixer. */
	@Inject
	private BalanceFixer fixer;
	
	/** The document fixer. */
	@Inject
	private DocumentsFixer documentFixer;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;	
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The Constant DEFAULT_LOCALE. */
	public static final Locale DEFAULT_LOCALE = new Locale("es");
	
	/** The CorporateMonitoringFilter *. */
	private CorporateMonitoringTO corporateMonitoringFilter;	
	
    /**
     * Default constructor. 
     */
    public CorporateProcessServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Verify preliminary process states.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyPreliminaryProcessStates(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{
    	List<Long> process = new ArrayList<>();
    	List<Integer> states = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	states.add(CorporateProcessStateType.REGISTERED.getCode());
		states.add(CorporateProcessStateType.MODIFIED.getCode());
		states.add(CorporateProcessStateType.PRELIMINARY.getCode());
		states.add(CorporateProcessStateType.PRELIMINARY_FAIL.getCode());
		return verifyCorporateProcessNotInStates(process, states,flag);
    }
    
    /**
     * Verify corporate process not in states.
     *
     * @param process the process
     * @param states the states
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyCorporateProcessNotInStates(List<Long> process,List<Integer> states,boolean flag) throws ServiceException {
    	RecordValidationType objRecordValidationTypes = null;
    	StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
		sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
		sbQuery.append(" and cp.state not in (:states)");
		parameters.put("processids", process);
		parameters.put("states", states);
		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flag){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_STATE_NO_VALID,new  Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flag){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_CORPORATE_EVENT_STATE_NO_VALID,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    
    /**
     * Verify state confirm depositary.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @throws ServiceException the service exception
     */
    public void verifyStateConfirmDepositary(List<CorporativeOperation> lstProcesses,boolean flag)  throws ServiceException{
    	List<Long> process = new ArrayList<>();
    	List<Integer> states = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
		states.add(CorporateProcessStateType.PRELIMINARY.getCode());
		states.add(CorporateProcessStateType.DEFINITIVE_FAIL.getCode());
		verifyCorporateProcessNotInStates(process, states,flag);
    }
    
    /**
     * Verify preliminar delivery date.
     *
     * @param lstProcesses the lst processes
     * @param flagDpf the flag dpf
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyPreliminarDeliveryDate(List<CorporativeOperation> lstProcesses,boolean flagDpf) throws ServiceException {
    	RecordValidationType objRecordValidationTypes= null;
    	List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
		List<Long> errorProcess = verifyCorporateDeliveryDate(process, true);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_DELIVERY_DATE_LESS,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_DELIVERY_DATE_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    /**
     * Verify preliminary Expiration date.
     *
     * @param lstProcesses the list processes
     * @param flagDpf the flag DPF
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyPreliminarExpirationDate(List<CorporativeOperation> lstProcesses,boolean flagDpf) throws ServiceException {
    	RecordValidationType objRecordValidationTypes= null;
    	List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
		List<Long> errorProcess = verifyCorporateByExpirationDate(process, true);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_DELIVERY_DATE_LESS,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_DELIVERY_DATE_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    
    /**
     * Verify definitive delivery date.
     *
     * @param lstProcesses the lst processes
     * @param flagDpf the flag dpf
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyDefinitiveDeliveryDate(List<CorporativeOperation> lstProcesses, boolean flagDpf)  throws ServiceException{
    	RecordValidationType objRecordValidationTypes= null;
    	List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
		List<Long> errorProcess = verifyCorporateByExpirationDate(process, false);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){//x pantalla
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_DELIVERY_DATE_MORE,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){//x batchero
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_DELIVERY_DATE_TWO_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    
    /**
     * Verify definitive delivery date.
     *
     * @param lstProcesses the lst processes
     * @param flagDpf the flag dpf
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyDefinitiveExpirationDate(List<CorporativeOperation> lstProcesses, boolean flagDpf)  throws ServiceException{
    	RecordValidationType objRecordValidationTypes= null;
    	List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
		List<Long> errorProcess = verifyCorporateByExpirationDate(process, false);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){//x pantalla
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_DELIVERY_DATE_MORE,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){//x batchero
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_DELIVERY_DATE_TWO_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    
    /**
     * Verify corporate delivery date.
     *
     * @param corporativesOperations the corporatives operations
     * @param isAfter the is after
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Long> verifyCorporateDeliveryDate(List<Long> corporativesOperations, boolean isAfter) throws ServiceException{
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
		sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
		sbQuery.append(" and trunc(cp.deliveryDate) ");
		if(isAfter){
			sbQuery.append(" < ");
		}else{
			sbQuery.append(" > ");
		}
		sbQuery.append(" trunc(:currentDate)");
		parameters.put("processids", corporativesOperations);
		parameters.put("currentDate", CommonsUtilities.currentDate());
		return findListByQueryString(sbQuery.toString(), parameters);
    }
    
    /**
     * Verify corporate delivery date.
     *
     * @param corporativesOperations the corporatives operations
     * @param isAfter the is after
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Long> verifyCorporateByExpirationDate(List<Long> corporativesOperations, boolean isAfter) throws ServiceException{
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	StringBuilder sbQuery = new StringBuilder();
    	StringBuilder sbQuery2 = new StringBuilder();
		sbQuery.append("  SELECT c.idCorporativeOperationPk from CorporativeOperation c   ");
		sbQuery.append("   JOIN c.programInterestCoupon interest	 ");
		sbQuery.append("   WHERE c.idCorporativeOperationPk in (:processids)");
		sbQuery.append("    and trunc(interest.experitationDate) ");
		if(isAfter){
			sbQuery.append(" <= ");
		}else{
			sbQuery.append(" >= ");
		}
		sbQuery.append(" trunc (:currentDate)");
		parameters.put("processids", corporativesOperations);
		parameters.put("currentDate", CommonsUtilities.currentDate());
		
		List<Long> lst = findListByQueryString(sbQuery.toString(), parameters);
		
		sbQuery2.append("   SELECT c.idCorporativeOperationPk from CorporativeOperation c   ");
		sbQuery2.append("    JOIN c.programAmortizationCoupon amortiz ");
		sbQuery2.append("   WHERE c.idCorporativeOperationPk in (:processids)");
		sbQuery2.append("   	and trunc(amortiz.expirationDate)");
		if(isAfter){
			sbQuery2.append(" <= ");
		}else{
			sbQuery2.append(" >= ");
		}
		sbQuery2.append(" trunc(:currentDate)");
		
		
		List<Long> lst1 = findListByQueryString(sbQuery.toString(), parameters);
		lst.addAll(lst1);
		return lst;
    }
    
    /**
     * Verify preliminar stock calculation.
     *
     * @param lstProcesses the lst processes
     * @throws ServiceException the service exception
     */
    public void verifyPreliminarStockCalculation(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	List<Long> errorProcess = new ArrayList<>();
    	List<Long> errorProcessAux = new ArrayList<>();
    	List<Long> errorProcessAux2 = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		corporative = loaderEntityService.loadEntity(corporative, corporative.getIdCorporativeOperationPk());
    		if(corporative.getRetirementOperation() == null || corporative.getRetirementOperation().getIdRetirementOperationPk() == null){
    			Integer stockType = getCorporateRequiredStockCalculation(corporative);
    			if(stockType !=null){
    				Long idStockProcess = validateStockCalculationCorporate(corporative,CommonsUtilities.currentDate(),stockType);
    				if(idStockProcess == null){
    					errorProcess.add(corporative.getIdCorporativeOperationPk());
    				}else {
    					Long countBalance = validateBalanceStockCalculationCorporate(idStockProcess, corporative.getSecurities().getIdSecurityCodePk());
    					if(countBalance!=null && countBalance==0  && 
    							(searchDesmaterializedBalance(corporative.getSecurities().getIdSecurityCodePk()).intValue()>0) && 
    							hasBalance(corporative.getSecurities().getIdSecurityCodePk())){
    						errorProcessAux.add(corporative.getIdCorporativeOperationPk());
    					}else if(validateInconsistencyBalance(idStockProcess, corporative.getSecurities().getIdSecurityCodePk()) && 
    							(searchDesmaterializedBalance(corporative.getSecurities().getIdSecurityCodePk()).intValue()>0) && 
    							hasBalance(corporative.getSecurities().getIdSecurityCodePk())){
    						errorProcessAux2.add(corporative.getIdCorporativeOperationPk());
    					}
    				}
    			}
    		}
    	}
    	if(errorProcess!= null && !errorProcess.isEmpty()){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_STOCK_CALCULATION,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcessAux!= null && !errorProcessAux.isEmpty()){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_BALANCE_STOCK_CALCULATION,new Object[]{StringUtils.join(errorProcessAux,",")});
		}else if(errorProcessAux2!= null && !errorProcessAux2.isEmpty()){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_ICON_BALANCE_STOCK_CALCULATION,new Object[]{StringUtils.join(errorProcessAux2,",")});
		}
    }

    
    /**
     * Gets the corporate required stock calculation.
     *
     * @param corporativeOperation the corporative operation
     * @return the corporate required stock calculation
     */
    public Integer getCorporateRequiredStockCalculation(CorporativeOperation corporativeOperation){
    	Integer stockType = null;
    	if(!loaderEntityService.isLoadedEntity(corporativeOperation, "corporativeEventType")){
    		em.refresh(corporativeOperation);
    		corporativeOperation.getCorporativeEventType().getDescription();
    	}
    	stockType = corporativeOperation.getCorporativeEventType().getStockCalculationType();
    	return stockType;
    }
    
    /**
     * Validate stock calculation corporate.
     *
     * @param corporate the corporate
     * @param currendate the current date
     * @param stockType the stock type
     * @return the long
     */
    public Long validateStockCalculationCorporate(CorporativeOperation corporate,Date currendate,Integer stockType){
    	Long procId = null;
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select sp.idStockCalculationPk from CorporativeOperation cp,StockCalculationProcess sp ");
		sbQuery.append(" where cp.idCorporativeOperationPk = :processids ");
		sbQuery.append(" and trunc(cp.cutoffDate) = trunc(sp.cutoffDate) ");
		sbQuery.append(" and trunc(cp.registryDate) = trunc(sp.registryDate) ");
		sbQuery.append(" and cp.securities.idSecurityCodePk = sp.security.idSecurityCodePk ");
		sbQuery.append(" and sp.stockState = :stockState ");
		sbQuery.append(" and sp.stockClass = :stockClass ");
		sbQuery.append(" and sp.stockType = :stockType ");
		if(currendate != null){
			sbQuery.append(" and trunc(sp.creationDate) = trunc(:current) ");
			parameters.put("current", currendate);
		}
		parameters.put("processids", corporate.getIdCorporativeOperationPk());
		parameters.put("stockState", StockStateType.FINISHED.getCode());
		parameters.put("stockClass", StockClassType.NORMAL.getCode());
		parameters.put("stockType", stockType);
		List<Long> procs = findListByQueryString(sbQuery.toString(), parameters);
		if(!procs.isEmpty()){
			procId = procs.get(0);
		}
		return procId;
    }
   
   /**
    * validate Balance Stock Calculation Corporate.
    *
    * @param idStockProcess id stock process
    * @param idSecurity code securty
    * @return validate Balance Stock Calculation Corporate
    */
    public Long validateBalanceStockCalculationCorporate(Long idStockProcess , String idSecurity){
    	Long procId = null;
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select count(scb) from StockCalculationBalance scb ");
		sbQuery.append(" where scb.stockCalculationProcess.idStockCalculationPk = :idStockCalculationPk ");
		sbQuery.append(" and scb.security.idSecurityCodePk = :idSecurityCodePk ");
		parameters.put("idStockCalculationPk", idStockProcess);
		parameters.put("idSecurityCodePk", idSecurity);
		List<Long> procs = findListByQueryString(sbQuery.toString(), parameters);
		if(!procs.isEmpty()){
			procId = procs.get(0);
		}
		return procId;
    }
    
    /**
     * Verify security status.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifySecurityStatus(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{
    	RecordValidationType objRecordValidationTypes = null;
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
    	sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
       	sbQuery.append(" and cp.securities.stateSecurity != :state  ");
       	parameters.put("processids", process);
		parameters.put("state",SecurityStateType.REGISTERED.getCode());
		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flag){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_SECURITY_NOT_CONFIRM,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flag){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_CORPORATE_EVENT_SECURITY_NOT_CONFIRM,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    
    /**
     * Verify security status.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifySecurityDesmaterialized(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{
    	RecordValidationType objRecordValidationTypes = null;
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		if(!corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())
    				&& !corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())){
    			process.add(corporative.getIdCorporativeOperationPk());
    		}    		
    	}
    	if(process!=null && !process.isEmpty()){
    		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
        	sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
           	sbQuery.append(" and cp.securities.stateSecurity != :state  ");
           	sbQuery.append(" and nvl(cp.securities.desmaterializedBalance,0) = 0  ");
           	parameters.put("processids", process);
    		parameters.put("state",SecurityStateType.REGISTERED.getCode());
    		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
    		if(errorProcess!= null && !errorProcess.isEmpty() && !flag){
    			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_SECURITY_NOT_BALANCE_CONFIRM,new Object[]{StringUtils.join(errorProcess,",")});
    		}else if(errorProcess!= null && !errorProcess.isEmpty() && flag){
    			objRecordValidationTypes= new RecordValidationType();
    			objRecordValidationTypes.setErrorProcess(errorProcess);
    			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
    					PropertiesConstants.MESSAGE_CORPORATE_EVENT_SECURITY_NOT_BALANCE_CONFIRM,  new Object[]{StringUtils.join(errorProcess,",")}));
    		}
    	}    	
		return objRecordValidationTypes;
    }
    
    /**
     * Verify security balance.
     *
     * @param lstProcesses the lst processes
     * @param stateCorporative the state corporative
     * @throws ServiceException the service exception
     */
    public void verifySecurityBalance(List<CorporativeOperation> lstProcesses,Integer stateCorporative) throws ServiceException{    	
		List<Long> process = new ArrayList<>();
		List<Long> processAux = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){    		
    		if((corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCLUSION.getCode())
    				||corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode())
					||corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMINAL_VALUE_NO_CAP_VAR.getCode())
					||corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CHANGE_NOMAL_VALUE_CAP_VAR.getCode())
					||corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_FUSION.getCode())
					||corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_UNIFICATION.getCode())
					||corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.SECURITIES_EXCISION.getCode())
					||corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_REDUCTION.getCode()))
					&& validateBalanceForSpecial(corporative.getSecurities().getIdSecurityCodePk(), stateCorporative)){
    			process.add(corporative.getIdCorporativeOperationPk());
			}  
    		if(corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CONVERTIBILY_BONE_TO_ACTION.getCode()) 
    				&& validateConvertibilyBoneAction(corporative.getSecurities().getIdSecurityCodePk())){
    			processAux.add(corporative.getIdCorporativeOperationPk());
    		}    		
    	}    	
    	if(process!= null && !process.isEmpty()){
    		throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_VALIDATE,new Object[]{StringUtils.join(process,",")});
    	}	
    	if(processAux!= null && !processAux.isEmpty()){
    		throw new ServiceException(ErrorServiceType.MSG_CORPORATE_EVENT_CONVERTIBILY_BONE_ACTION_VALIDATE,new Object[]{StringUtils.join(processAux,",")});
    	}	
    }
    
    /**
     * Verify definitive amortization.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyDefinitiveAmortization(List<CorporativeOperation> lstProcesses,boolean flag)throws ServiceException{
    	RecordValidationType objRecordValidationTypes = null;
    	List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		//si afecta saldo entonces es la ultima amortizacion - validaremos que esten en definitivo todos los intereses
    		if(corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode()) && 
    				corporative.getIndAffectBalance().equals(CorporateProcessConstants.IND_YES) && 
					validateUltimateAmortization(corporative.getSecurities().getIdSecurityCodePk())){
    			process.add(corporative.getIdCorporativeOperationPk());
    		}
    	}
    	if(process!= null && !process.isEmpty() && !flag){
    		throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE,new Object[]{StringUtils.join(process,",")});
    	}else if(process!= null && !process.isEmpty() && flag){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(process);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE,  new Object[]{StringUtils.join(process,",")}));
		}
		return objRecordValidationTypes;
    }
    
    
    /**
     * Validate balance for special.
     *
     * @param idSecurity the id security
     * @param stateCorporative the state corporative
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean validateBalanceForSpecial(String idSecurity,Integer stateCorporative)throws ServiceException{
    	boolean validateAux = false;
    	StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	sbQuery.append("select hab from HolderAccountBalance hab ");
    	sbQuery.append(" where hab.security.idSecurityCodePk = :idSecurityCodePk ");
    	sbQuery.append(" and (hab.transitBalance > 0 ");   	
    	sbQuery.append(" or hab.oppositionBalance > 0 ");
    	sbQuery.append(" or hab.accreditationBalance > 0 ");
    	sbQuery.append(" or hab.purchaseBalance > 0 ");
    	sbQuery.append(" or hab.saleBalance > 0 ");
    	if(stateCorporative.equals(CorporateProcessStateType.DEFINITIVE.getCode())){
    		sbQuery.append(" or hab.reportingBalance > 0 ");
        	sbQuery.append(" or hab.reportedBalance > 0 ");
    	}
    	sbQuery.append(" or hab.marginBalance > 0 ");
    	sbQuery.append(" or hab.borrowerBalance > 0 ");
    	sbQuery.append(" or hab.lenderBalance > 0 ");
    	sbQuery.append(" or hab.loanableBalance > 0 ");    	
    	sbQuery.append(" or hab.reserveBalance > 0) ");
       	parameters.put("idSecurityCodePk", idSecurity);
		List<HolderAccountBalance> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty()){
			validateAux=true;
		}
    	return validateAux;
    }
    
    /**
     * Validate inconsistency balance.
     *
     * @param idStockProcess the id stock process
     * @param idSecurity the id security
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean validateInconsistencyBalance(Long idStockProcess,String idSecurity)throws ServiceException{
    	boolean validateAux = false;
    	StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	sbQuery.append("select stb from StockCalculationBalance stb ");
    	sbQuery.append(" where stb.security.idSecurityCodePk = :idSecurityCodePk ");
    	sbQuery.append(" and stb.stockCalculationProcess.idStockCalculationPk = :idStockProcess ");   	
    	sbQuery.append(" and (stb.totalBalance < 0 ");   
    	sbQuery.append(" or stb.transitBalance < 0 ");   
    	sbQuery.append(" or stb.availableBalance < 0 ");   
    	sbQuery.append(" or stb.oppositionBalance < 0 ");
    	sbQuery.append(" or stb.accreditationBalance < 0 ");
    	sbQuery.append(" or stb.purchaseBalance < 0 ");
    	sbQuery.append(" or stb.saleBalance < 0 ");
    	sbQuery.append(" or stb.reportingBalance < 0 ");
    	sbQuery.append(" or stb.reportedBalance < 0 ");
    	sbQuery.append(" or stb.marginBalance < 0 ");
    	sbQuery.append(" or stb.borrowerBalance < 0 ");
    	sbQuery.append(" or stb.lenderBalance < 0 ");
    	sbQuery.append(" or stb.loanableBalance < 0 ");    	
    	sbQuery.append(" or stb.reserveBalance < 0) ");
       	parameters.put("idSecurityCodePk", idSecurity);
       	parameters.put("idStockProcess", idStockProcess);
		List<StockCalculationBalance> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty()){
			validateAux=true;
		}
    	return validateAux;
    }
    
    /**
     * Checks for balance.
     *
     * @param idSecurity the id security
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean hasBalance(String idSecurity)throws ServiceException{
    	boolean validateAux = false;
    	StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	sbQuery.append("select count(hab) from HolderAccountBalance hab ");
    	sbQuery.append(" where hab.security.idSecurityCodePk = :idSecurityCodePk ");
    	parameters.put("idSecurityCodePk", idSecurity);
		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() 
				&& errorProcess.get(0)>0){
			validateAux=true;
		}
    	return validateAux;
    }
    
    /**
     * Validate convertibily bone action.
     *
     * @param idSecurity the id security
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean validateConvertibilyBoneAction(String idSecurity)throws ServiceException{
    	StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	sbQuery.append("select count(pic) from CorporativeOperation co , ProgramInterestCoupon pic ");
    	sbQuery.append(" where co.programInterestCoupon.idProgramInterestPk = pic.idProgramInterestPk ");
    	sbQuery.append(" and co.securities.idSecurityCodePk = :idSecurityCodePk ");
    	sbQuery.append(" and co.state != :state ");
       	parameters.put("idSecurityCodePk", idSecurity);
       	parameters.put("state", CorporateProcessStateType.DEFINITIVE.getCode());
		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty()
				&& errorProcess.size()>0 && errorProcess.get(0)>0){
			return true;
		}
		StringBuilder sbQueryAux = new StringBuilder();
		Map<String, Object> parametersAux = new HashMap<String, Object>();
		sbQueryAux.append("select co from CorporativeOperation co , ProgramAmortizationCoupon pic ");
		sbQueryAux.append(" where co.programAmortizationCoupon.idProgramAmortizationPk = pic.idProgramAmortizationPk ");
		sbQueryAux.append(" and co.securities.idSecurityCodePk = :idSecurityCodePk ");
		sbQueryAux.append(" and co.state != :state ");
    	parametersAux.put("idSecurityCodePk", idSecurity);
    	parametersAux.put("state", CorporateProcessStateType.DEFINITIVE.getCode());
		List<CorporativeOperation> errorProcessAux = findListByQueryString(sbQueryAux.toString(), parametersAux);
		if(errorProcessAux!= null && !errorProcessAux.isEmpty()
				&& errorProcessAux.size()>0){
			if(errorProcessAux.size() == GeneralConstants.ONE_VALUE_INTEGER.intValue()){
				CorporativeOperation objCorporativeOperation = (CorporativeOperation)errorProcessAux.get(0);
				if(objCorporativeOperation.getIndAffectBalance().equals(CorporateProcessConstants.IND_NO)){
					return true;
				}
			}else{
				return true;
			}
		}
    	return false;
    }
    
    /**
     * Validate ultimate amortization.
     *
     * @param idSecurity the id security
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean validateUltimateAmortization(String idSecurity)throws ServiceException{
    	boolean validateAux = false;
    	StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	sbQuery.append("select count(pic) from CorporativeOperation co , ProgramInterestCoupon pic ");
    	sbQuery.append(" where co.programInterestCoupon.idProgramInterestPk = pic.idProgramInterestPk ");
    	sbQuery.append(" and co.securities.idSecurityCodePk = :idSecurityCodePk ");
    	sbQuery.append(" and co.state != :state ");
       	parameters.put("idSecurityCodePk", idSecurity);
       	parameters.put("state", CorporateProcessStateType.DEFINITIVE.getCode());
		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() && 
				 errorProcess.get(0)>0){
			validateAux=true;
		}
    	return validateAux;
    }
    
    /**
     * Verify not settled balances corporate.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyNotSettledBalancesCorporate(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{
    	RecordValidationType objRecordValidationTypes = null;
    	List<Long> errorProcess = new ArrayList<>();
    	Integer stockType = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		stockType = getCorporateRequiredStockCalculation(corporative);
    		if(stockType != null){
    			if(verifyNotSettledStock(corporative.getIdCorporativeOperationPk(),stockType)!=null){
    				errorProcess.add(corporative.getIdCorporativeOperationPk());
    			}
    		} else {
    			if(verifyNotSettledBalances(corporative.getSecurities().getIdSecurityCodePk()) != null){
    				errorProcess.add(corporative.getIdCorporativeOperationPk());
    			}
    		}
    	}
    	if(errorProcess!= null && !errorProcess.isEmpty() && !flag){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_SETTLEMENT_OPERATIONS,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flag){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_CORPORATE_EVENT_EXIST_SETTLEMENT_OPERATIONS,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    
    /**
     * Verify not settled balances.
     *
     * @param securityCode the security code
     * @return the string
     */
    public String verifyNotSettledBalances(String securityCode){
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select hab.id.idSecurityCodePk from HolderAccountBalance hab ");
		sbQuery.append(" where hab.id.idSecurityCodePk = :isincodes");
		sbQuery.append(" and (hab.purchaseBalance > 0 or hab.saleBalance > 0) ");
		parameters.put("isincodes", securityCode);
		List<String> sec = findListByQueryString(sbQuery.toString(), parameters);
		return sec.isEmpty()?null:sec.get(0);
    }
    
    /**
     * Verify not settled stock.
     *
     * @param corporateId the corporate id
     * @param stockType the stock type
     * @return the long
     */
    public Long verifyNotSettledStock(Long corporateId,Integer stockType){
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select co.idCorporativeOperationPk from CorporativeOperation co, StockCalculationProcess sp, StockCalculationBalance sb ");
		sbQuery.append(" where co.idCorporativeOperationPk = :processids ");
		sbQuery.append(" and trunc(co.cutoffDate) = trunc(sp.cutoffDate) ");
		sbQuery.append(" and trunc(co.registryDate) = trunc(sp.registryDate) ");
		sbQuery.append(" and co.securities.idSecurityCodePk = sp.security.idSecurityCodePk ");
		sbQuery.append(" and sp.stockState = :stockState ");
		sbQuery.append(" and sp.stockClass = :stockClass ");
		sbQuery.append(" and sp.stockType = :stockType ");
		sbQuery.append(" and sp.idStockCalculationPk = sb.stockCalculationProcess.idStockCalculationPk ");
		sbQuery.append(" and (sb.purchaseBalance > 0 or sb.saleBalance > 0) ");
		parameters.put("processids", corporateId);
		parameters.put("stockState", StockStateType.FINISHED.getCode());
		parameters.put("stockClass",  StockClassType.NORMAL.getCode());
		parameters.put("stockType", stockType);
		List<Long> proc = findListByQueryString(sbQuery.toString(), parameters);
		return proc.isEmpty()?null:proc.get(0);
    }
    
    /**
     * Verify transit balances.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyTransitBalances(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{
    	RecordValidationType objRecordValidationTypes = null;
    	List<Long> errorProcess = new ArrayList<>();
    	Integer stockType = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		stockType = getCorporateRequiredStockCalculation(corporative);
    		if(stockType != null){
    			if(verifyExistTransitBalanceStock(corporative.getIdCorporativeOperationPk(),stockType)!=null){
    				errorProcess.add(corporative.getIdCorporativeOperationPk());
    			}
    		}else if(verifyExistTransitBalance(corporative.getSecurities().getIdSecurityCodePk()) != null){
    				errorProcess.add(corporative.getIdCorporativeOperationPk());
    		}    		
    	}
    	if(errorProcess!= null && !errorProcess.isEmpty() &&!flag){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_TRANSIT_BALANCE,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flag){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_CORPORATE_EVENT_EXIST_TRANSIT_BALANCE,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    
    /**
     * Verify exist transit balance.
     *
     * @param securityCode the security code
     * @return the string
     */
    public String verifyExistTransitBalance(String securityCode){
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select hab.id.idSecurityCodePk from HolderAccountBalance hab ");
		sbQuery.append(" where hab.id.idSecurityCodePk = :isincodes");
		sbQuery.append(" and hab.transitBalance > 0  ");
		parameters.put("isincodes", securityCode);
		List<String> sec = findListByQueryString(sbQuery.toString(), parameters);
		return sec.isEmpty()?null:sec.get(0);
    }
    
 
    /**
     * Verify exist transit balance stock.
     *
     * @param corporateId the corporate id
     * @param stockType the stock type
     * @return the long
     */
    public Long verifyExistTransitBalanceStock(Long corporateId,Integer stockType){
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select co.idCorporativeOperationPk from CorporativeOperation co, StockCalculationProcess sp, StockCalculationBalance sb ");
		sbQuery.append(" where co.idCorporativeOperationPk = :processids ");
		sbQuery.append(" and trunc(co.cutoffDate) = trunc(sp.cutoffDate) ");
		sbQuery.append(" and trunc(co.registryDate) = trunc(sp.registryDate) ");
		sbQuery.append(" and co.securities.idSecurityCodePk = sp.security.idSecurityCodePk ");
		sbQuery.append(" and sp.stockState = :stockState ");
		sbQuery.append(" and sp.stockClass = :stockClass ");
		sbQuery.append(" and sp.stockType = :stockType ");
		sbQuery.append(" and sp.idStockCalculationPk = sb.stockCalculationProcess.idStockCalculationPk ");
		sbQuery.append(" and sb.transitBalance > 0 ");
		parameters.put("processids", corporateId);
		parameters.put("stockState", StockStateType.FINISHED.getCode());
		parameters.put("stockClass",  StockClassType.NORMAL.getCode());
		parameters.put("stockType", stockType);
		List<Long> proc = findListByQueryString(sbQuery.toString(), parameters);
		return proc.isEmpty()?null:proc.get(0);
    }
    
    /**
     * Verify emisor confirm.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyEmisorConfirm(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{
    	RecordValidationType objRecordValidationTypes = null;
    	StringBuilder sbQuery = new StringBuilder();
    	List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		if(corporative.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode()) &&
    				(corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
    				|| corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode()))){
    			process.add(corporative.getIdCorporativeOperationPk());
    		}
    	}
    	if(!process.isEmpty()){
    		Map<String, Object>  parameters=new HashMap<String, Object>();
    		sbQuery.append("select co.idCorporativeOperationPk from  CorporativeOperation co ");		
    		sbQuery.append(" where co.idCorporativeOperationPk in (:idprocess) ");
    		sbQuery.append(" and co.situation is null ");
    		parameters.put("idprocess", process);
    		List<Long> errorProcess= findListByQueryString(sbQuery.toString(), parameters);
    		if(errorProcess!= null && !errorProcess.isEmpty()&& !flag){
    			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_CONFIRMATION_ISSUER,new Object[]{StringUtils.join(errorProcess,",")});
    		}else if(errorProcess!= null && !errorProcess.isEmpty() && flag){
    			objRecordValidationTypes= new RecordValidationType();
    			objRecordValidationTypes.setErrorProcess(errorProcess);
    			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
    					PropertiesConstants.MESSAGE_CORPORATE_EVENT_NOT_CONFIRMATION_ISSUER,  new Object[]{StringUtils.join(errorProcess,",")}));
    		}    		
    	}
    	return objRecordValidationTypes;
    }
    
    /**
     * Verify exchange rate.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyExchangeRate(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{
    	RecordValidationType objRecordValidationTypes = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		Security security = securitiesQueryService.getSecurityHelpServiceBean(corporative.getSecurities().getIdSecurityCodePk());
    		if(security.getCurrency().equals(CurrencyType.USD.getCode()) || 
    				security.getCurrency().equals(CurrencyType.UFV.getCode()) || 
    				security.getCurrency().equals(CurrencyType.DMV.getCode())){
    			if(security.getCurrency().equals(CurrencyType.USD.getCode()) && 
    					getLastUSDExchangeRate() == null && !flag){
    				throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST_USD);
    			}else if(security.getCurrency().equals(CurrencyType.UFV.getCode()) && 
    					getUFVExchangeRate() == null && !flag){
    				throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST_UFV);
    			}else if(security.getCurrency().equals(CurrencyType.DMV.getCode()) && 
    					getDMVExchangeRate() == null && !flag){
    				throw new ServiceException(ErrorServiceType.EXCHANGE_RATE_DONT_EXIST_DMV);
    			}else if(security.getCurrency().equals(CurrencyType.UFV.getCode()) && 
    					getUFVExchangeRate() == null && flag){
    				objRecordValidationTypes= new RecordValidationType();
    				List<Long> lstCorporative = new ArrayList<Long>();
    				lstCorporative.add(corporative.getIdCorporativeOperationPk());
    				objRecordValidationTypes.setErrorProcess(lstCorporative);
    				objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(
    						PropertiesConstants.MESSAGE_EXCHANGE_RATE_DONT_EXIST_UFV));
    				return objRecordValidationTypes;
    			}else if(security.getCurrency().equals(CurrencyType.DMV.getCode()) && 
    					getDMVExchangeRate() == null && flag){
    				objRecordValidationTypes= new RecordValidationType();
    				List<Long> lstCorporative = new ArrayList<Long>();
    				lstCorporative.add(corporative.getIdCorporativeOperationPk());
    				objRecordValidationTypes.setErrorProcess(lstCorporative);
    				objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(
    						PropertiesConstants.MESSAGE_EXCHANGE_RATE_DONT_EXIST_DMV));
    				return objRecordValidationTypes;
    			}else if(security.getCurrency().equals(CurrencyType.USD.getCode()) && 
    					getLastUSDExchangeRate() == null && flag){
    				objRecordValidationTypes= new RecordValidationType();
    				List<Long> lstCorporative = new ArrayList<Long>();
    				lstCorporative.add(corporative.getIdCorporativeOperationPk());
    				objRecordValidationTypes.setErrorProcess(lstCorporative);
    				objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(
    						PropertiesConstants.MESSAGE_EXCHANGE_RATE_DONT_EXIST_USD));
    				return objRecordValidationTypes;
    			}    			
    		}
    	}
    	return objRecordValidationTypes;
    }
    
    /**
     * Gets the USD exchange rate.
     *
     * @return the USD exchange rate
     */
    public BigDecimal getUSDExchangeRate(){
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT REFERENCE_PRICE");
		sbQuery.append("   FROM DAILY_EXCHANGE_RATES");
		sbQuery.append("  WHERE TRUNC(DATE_RATE)=TRUNC(:date)");
		sbQuery.append("    AND ID_CURRENCY=:currency");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("currency", CurrencyType.USD.getCode());
		query.setParameter("date", CommonsUtilities.currentDate());	
		try{
			return (BigDecimal) query.getSingleResult();
		}catch(NoResultException nrException){
			return null;
		}
	}
    
    public BigDecimal getLastUSDExchangeRate(){
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT REFERENCE_PRICE");
		sbQuery.append("   FROM DAILY_EXCHANGE_RATES");
		sbQuery.append("  WHERE TRUNC(DATE_RATE) = (SELECT MAX(m.DATE_RATE) FROM DAILY_EXCHANGE_RATES m where m.ID_CURRENCY= :currencyM ) ");
		sbQuery.append("    AND ID_CURRENCY=:currency");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("currency", CurrencyType.USD.getCode());
		query.setParameter("currencyM", CurrencyType.USD.getCode());	
		try{
			return (BigDecimal) query.getSingleResult();
		}catch(NoResultException nrException){
			return null;
		}
	}
    /**
     * Gets the USD exchange rate.
     *
     * @return the USD exchange rate
     */
    public BigDecimal getUFVExchangeRate(){
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT REFERENCE_PRICE");
		sbQuery.append("   FROM DAILY_EXCHANGE_RATES");
		sbQuery.append("  WHERE TRUNC(DATE_RATE)=TRUNC(:date)");
		sbQuery.append("    AND ID_CURRENCY=:currency");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("currency", CurrencyType.UFV.getCode());
		query.setParameter("date", CommonsUtilities.currentDate());	
		try{
			return (BigDecimal) query.getSingleResult();
		}catch(NoResultException nrException){
			return null;
		}
	}
    /**
     * Gets the USD exchange rate.
     *
     * @return the USD exchange rate
     */
    public BigDecimal getDMVExchangeRate(){
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT REFERENCE_PRICE");
		sbQuery.append("   FROM DAILY_EXCHANGE_RATES");
		sbQuery.append("  WHERE TRUNC(DATE_RATE)=TRUNC(:date)");
		sbQuery.append("    AND ID_CURRENCY=:currency");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("currency", CurrencyType.DMV.getCode());
		query.setParameter("date", CommonsUtilities.currentDate());	
		try{
			return (BigDecimal) query.getSingleResult();
		}catch(NoResultException nrException){
			return null;
		}
	}
    
    /**
     * Gets the USD exchange rate.
     *
     * @return the USD exchange rate
     */
    public BigDecimal getEURExchangeRate(){
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT REFERENCE_PRICE");
		sbQuery.append("   FROM DAILY_EXCHANGE_RATES");
		sbQuery.append("  WHERE TRUNC(DATE_RATE)=TRUNC(:date)");
		sbQuery.append("    AND ID_CURRENCY=:currency");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("currency", CurrencyType.EU.getCode());
		query.setParameter("date", CommonsUtilities.currentDate());	
		try{
			return (BigDecimal) query.getSingleResult();
		}catch(NoResultException nrException){
			return null;
		}
	}
    
    /**
     * Verify corporate created retirement operation.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyCorporateCreatedRetirementOperation(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{
    	RecordValidationType objRecordValidationTypes = null;
    	for(CorporativeOperation process : lstProcesses){
    		process = loaderEntityService.loadEntity(process, process.getIdCorporativeOperationPk());
    		if(process.getRetirementOperation() != null && process.getRetirementOperation().getIdRetirementOperationPk() != null){
    			for(RetirementDetail retirementDetail:process.getRetirementOperation().getRetirementDetails()){
    				BigDecimal availableBalance = findAvailableBalance(retirementDetail.getHolderAccount().getIdHolderAccountPk(), process.getSecurities().getIdSecurityCodePk());
					if(retirementDetail.getAvailableBalance().compareTo(availableBalance) > 0 && !flag){
						Object[] proc ={process.getIdCorporativeOperationPk()};
						throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_AVAILABLE_BALANCE, proc);
					}else if(retirementDetail.getAvailableBalance().compareTo(availableBalance) > 0 && flag){
						List <Long> errorProcess = new ArrayList<Long>();
						errorProcess.add(process.getIdCorporativeOperationPk());
						objRecordValidationTypes= new RecordValidationType();
						objRecordValidationTypes.setErrorProcess(errorProcess);
						objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
		    					PropertiesConstants.MESSAGE_CORPORATE_EVENT_NOT_AVAILABLE_BALANCE,  new Object[]{StringUtils.join(errorProcess,",")}));
					}					
    			}
    		}
    	}
    	return objRecordValidationTypes;
    }
    
    /**
     * Find available balance.
     *
     * @param idHolderAccountPk the id holder account pk
     * @param idIsinCodePk the id isin code pk
     * @return the big decimal
     */
    public BigDecimal findAvailableBalance(Long idHolderAccountPk, String idIsinCodePk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT hab.availableBalance");
			sbQuery.append("	FROM HolderAccountBalance hab");
			sbQuery.append("	WHERE hab.holderAccount.idHolderAccountPk = :idHolderAccountPk");
			sbQuery.append("	AND hab.security.idSecurityCodePk = :idIsinCodePk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			query.setParameter("idIsinCodePk", idIsinCodePk);
			return new BigDecimal(query.getSingleResult().toString());
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
	}
    
    /**
     * Gets the corporative operation.
     *
     * @param processId the process id
     * @return the corporative operation
     */
    public CorporativeOperation getCorporativeOperation(Long processId){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT c ");
		sbQuery.append("   FROM CorporativeOperation c "
				+ "INNER JOIN FETCH c.securities s "
				+ "INNER JOIN FETCH c.corporativeEventType ");
		sbQuery.append("  WHERE c.idCorporativeOperationPk =:processId");	
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("processId", processId);
		return (CorporativeOperation)query.getSingleResult();
	}
    
    /**
     * Update corporative operation state.
     *
     * @param operationId the operation id
     * @param state the state
     * @param loggerUser the logger user
     * @param updateSituation the update situation
     * @param updateSituationConfirmedIssuer the update situation confirmed issuer
     * @throws ServiceException the service exception
     */
    public void updateCorporativeOperationState(Long operationId, Integer state, LoggerUser loggerUser, boolean updateSituation,Integer updateSituationConfirmedIssuer) throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update CorporativeOperation co set co.state = :newstate, ");
		if(updateSituation){
			sbQuery.append(" co.situation = null , ");
		}
		if(updateSituationConfirmedIssuer!=null){
			sbQuery.append(" co.situation = :situation , ");
		}	

		if(CorporateProcessStateType.PRELIMINARY.getCode().equals(state)){
			sbQuery.append(" co.preliminaryDate=:preliminary_date , ");
		}
		if(CorporateProcessStateType.DEFINITIVE.getCode().equals(state)){
			sbQuery.append(" co.definitiveDate=:defintive_date , ");
		}
		sbQuery.append(" co.lastModifyUser = :modifyuser, co.lastModifyDate = :modifydate,");
		sbQuery.append(" co.lastModifyIp = :modifyip, co.lastModifyApp = :modifyapp ");
		sbQuery.append(" where co.idCorporativeOperationPk = :corpprocessid ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("newstate", state);
		query.setParameter("modifyuser", loggerUser.getUserName());
		query.setParameter("modifydate", CommonsUtilities.currentDateTime());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("corpprocessid", operationId);
		if(updateSituationConfirmedIssuer!=null){
			query.setParameter("situation", updateSituationConfirmedIssuer);
		}
		if(CorporateProcessStateType.PRELIMINARY.getCode().equals(state)){
			query.setParameter("preliminary_date",CommonsUtilities.currentDateTime());
		}
		if(CorporateProcessStateType.DEFINITIVE.getCode().equals(state)){
			query.setParameter("defintive_date",CommonsUtilities.currentDateTime());
		}
		query.executeUpdate();
		em.flush();
	}
    
    /**
     * Update corporative operation state.
     *
     * @param listCorporatives the list corporatives
     * @param state the state
     * @param loggerUser the logger user
     * @param updateSituation the update situation
     * @param updateSituationConfirmedIssuer the update situation confirmed issuer
     * @throws ServiceException the service exception
     */
    public void updateCorporativeOperationState(List<Long> listCorporatives, Integer state, LoggerUser loggerUser, boolean updateSituation,
    											boolean updateSituationConfirmedIssuer) throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update CorporativeOperation co set co.state = :newstate, ");
		if(updateSituation){
			sbQuery.append(" co.situation = null , ");
		}
		if(updateSituationConfirmedIssuer){
			sbQuery.append(" co.situation = :situation , ");
		}	

		if(CorporateProcessStateType.PRELIMINARY.getCode().equals(state)){
			sbQuery.append(" co.preliminaryDate=:preliminary_date , ");
		}
		if(CorporateProcessStateType.DEFINITIVE.getCode().equals(state)){
			sbQuery.append(" co.definitiveDate=:defintive_date , ");
		}
		sbQuery.append(" co.lastModifyUser = :modifyuser, co.lastModifyDate = :modifydate,");
		sbQuery.append(" co.lastModifyIp = :modifyip, co.lastModifyApp = :modifyapp ");
		sbQuery.append(" where co.idCorporativeOperationPk in (:listCorporatives) ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("newstate", state);
		query.setParameter("modifyuser", loggerUser.getUserName());
		query.setParameter("modifydate", CommonsUtilities.currentDateTime());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("listCorporatives", listCorporatives);
		if(updateSituationConfirmedIssuer){
			query.setParameter("situation", CorporativeSituationType.ISSUER.getCode());
		}
		if(CorporateProcessStateType.PRELIMINARY.getCode().equals(state)){
			query.setParameter("preliminary_date",CommonsUtilities.currentDateTime());
		}
		if(CorporateProcessStateType.DEFINITIVE.getCode().equals(state)){
			query.setParameter("defintive_date",CommonsUtilities.currentDateTime());
		}
		query.executeUpdate();
	}
    
    /**
     * Update corporative operation state.
     *
     * @param operationId the operation id
     * @param state the state
     * @param loggerUser the logger user
     * @throws ServiceException the service exception
     */
    public void updateCorporativeOperationState(Long operationId, Integer state, LoggerUser loggerUser) throws ServiceException {
		updateCorporativeOperationState(operationId, state, loggerUser, false,null);
	}
    
    /**
     * Verify definitive process states.
     *
     * @param lstProcesses the lst processes
     * @param flagDpf the flag dpf
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public  RecordValidationType verifyDefinitiveProcessStates(List<CorporativeOperation> lstProcesses ,boolean flagDpf) throws ServiceException{
    	RecordValidationType objRecordValidationTypes= null;
    	StringBuilder sbQuery = new StringBuilder();
    	List<Long> process = new ArrayList<>();
    	List<Integer> states = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	states.add(CorporateProcessStateType.DEFINITIVE_FAIL.getCode());
		states.add(CorporateProcessStateType.PRELIMINARY.getCode());
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
		sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
		sbQuery.append(" and cp.state not in (:states)");
		parameters.put("processids", process);
		parameters.put("states", states);
		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_STATE_NO_VALID,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_STATE_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		
		return objRecordValidationTypes;
    }
    
    /**
     * Verify situation depositary.
     *
     * @param lstProcesses the lst processes
     * @throws ServiceException the service exception
     */
    public void verifySituationDepositary(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	List<Long> processRegular = new ArrayList<>();
    	List<Long> processIntAmort = new ArrayList<>();
    	List<Long> errorProcess = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		if(!corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
    				&& !corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
    			processRegular.add(corporative.getIdCorporativeOperationPk());
    		} else if(corporative.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode())){
    			processIntAmort.add(corporative.getIdCorporativeOperationPk());
    		}
    	}
    	if(!processRegular.isEmpty()){
    		errorProcess.addAll(verifyRequirementSituationCorporative(processRegular,CorporativeSituationType.IDEPOSITARY,false,true));
    	}
    	if(!processIntAmort.isEmpty()){
    		errorProcess.addAll(verifyRequirementSituationCorporative(processIntAmort,CorporativeSituationType.IDEPOSITARY,true,false));
    	}
    	if(errorProcess!= null && !errorProcess.isEmpty()){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_CONFIRMATION_DEPOSITARY,new Object[]{StringUtils.join(errorProcess,",")});
		}
    }
    
    /**
     * Verify situation definitive process.
     *
     * @param lstProcesses the lst processes
     * @param flagDpf the flag dpf
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifySituationDefinitiveProcess(List<CorporativeOperation> lstProcesses,boolean flagDpf) throws ServiceException{
    	RecordValidationType objRecordValidationTypes= null;
    	List<Long> processRegular = new ArrayList<>();
    	List<Long> processIntAmort = new ArrayList<>();
    	List<Long> errorProcess = new ArrayList<>();
    	List<Long> errorProcessPaid = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		if(!corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
    				&& !corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
    			processRegular.add(corporative.getIdCorporativeOperationPk());
    		} else if(corporative.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode())){
    			processIntAmort.add(corporative.getIdCorporativeOperationPk());
    		}
    	}
    	if(!processRegular.isEmpty()){
    		errorProcess.addAll(verifyRequirementSituationCorporative(processRegular,CorporativeSituationType.ISSUER,false,true));
    	}
    	if(!processIntAmort.isEmpty()){
    		errorProcess.addAll(verifyRequirementSituationCorporative(processIntAmort,CorporativeSituationType.ISSUER,true,false));
    	}
    	if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
    		processRegular.addAll(processIntAmort);
    		errorProcessPaid.addAll(verifySituationPaid(processRegular));
    		if(!errorProcessPaid.isEmpty()){
    			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_PAID_FOR_ERROR_DATA,new Object[]{StringUtils.join(errorProcessPaid,",")});
    		}else{
    			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_EXIST_CONFIRMATION_DEFINITIVE,new Object[]{StringUtils.join(errorProcess,",")});
    		}			
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_SITUATION_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
    	return objRecordValidationTypes;
    }
    
    /**
     * Verify requirement situation corporative.
     *
     * @param corporatives the corporatives
     * @param situationType the situation type
     * @param isInterestAmortization the is interest amortization
     * @param isConfirmEdv the is confirm edv
     * @return the list
     */
    public List<Long> verifyRequirementSituationCorporative(List<Long> corporatives,CorporativeSituationType situationType,boolean isInterestAmortization, boolean isConfirmEdv){
    	List<Long> process= null;
    	Map<String, Object>  parameters=new HashMap<String, Object>();
    	StringBuilder sbQuery = new StringBuilder("select co.idCorporativeOperationPk from  CorporativeOperation co ");
    	sbQuery.append("where co.idCorporativeOperationPk in (:idprocess) ");
    	switch (situationType) {
		case IDEPOSITARY:
			if(isInterestAmortization){
				//is required before confirmation was done for Issuer
				sbQuery.append(" and (co.situation is not null and co.situation != :situationOper) ");
				parameters.put("situationOper", CorporativeSituationType.ISSUER.getCode());
			}else if(isConfirmEdv){
				sbQuery.append(" and co.situation is not null and co.situation != :situationOper ");
				parameters.put("situationOper", CorporativeSituationType.IDEPOSITARY.getCode());
			}else {
				sbQuery.append(" and co.situation is null  ");
			}
			break;

		case ISSUER:
			if(isInterestAmortization){
				sbQuery.append(" and (co.situation is not null and co.situation != :situationOper) ");
				parameters.put("situationOper", CorporativeSituationType.IDEPOSITARY.getCode());
			}else if(isConfirmEdv){
				sbQuery.append(" and (co.situation is null or co.situation != :situationOper) ");
				parameters.put("situationOper", CorporativeSituationType.ISSUER.getCode());
			}else		
			{
				sbQuery.append(" and (co.situation is null or co.situation != :situationOper) ");
				parameters.put("situationOper", CorporativeSituationType.IDEPOSITARY.getCode());
			}
			break;
		default:
			break;
		}
    	parameters.put("idprocess", corporatives);
    	process = findListByQueryString(sbQuery.toString(), parameters);
    	return process;
    }
    
    /**
     * Verify situation paid.
     *
     * @param corporatives the corporatives
     * @return the list
     */
    public List<Long> verifySituationPaid(List<Long> corporatives){
    	List<Long> process= null;
    	Map<String, Object>  parameters=new HashMap<String, Object>();
    	StringBuilder sbQuery = new StringBuilder("select co.idCorporativeOperationPk from  CorporativeOperation co ");
    	sbQuery.append("where co.idCorporativeOperationPk in (:idprocess) ");    	
    	sbQuery.append(" and (co.situation = :situationOper or co.situation = :situationOperAux) ");
		parameters.put("situationOper", CorporativeSituationType.PAID.getCode());
		parameters.put("situationOperAux", CorporativeSituationType.ISSUER_PAID.getCode());		
    	parameters.put("idprocess", corporatives);
    	process = findListByQueryString(sbQuery.toString(), parameters);
    	return process;
    }
    
    /**
     * Verify holder account adjustments.
     *
     * @param lstProcesses the lst processes
     * @param adjustmentState the adjustment state
     * @param flagDpf the flag dpf
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyHolderAccountAdjustments(List<CorporativeOperation> lstProcesses,Integer adjustmentState,boolean flagDpf) throws ServiceException{
    	RecordValidationType objRecordValidationTypes= null;
    	List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	StringBuilder sbQuery = new StringBuilder("select distinct haa.corporativeOperation.idCorporativeOperationPk ");
    	sbQuery.append("from HolderAccountAdjusment haa ").
    	append("where haa.corporativeOperation.idCorporativeOperationPk in (:corpprocessids)  ").
    	append("and haa.adjustmentState = :state ");
		parameters.put("corpprocessids", process);
		parameters.put("state", adjustmentState);
		List<Long> errorProcess = new ArrayList<>();
		errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_ADJUSTMENTS_IN_PRELIMINAR,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_PRELIMINARY_FAIL_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
    }
    
    /**
     * Verify issuer amount.
     *
     * @param lstProcesses the lst processes
     * @param flagDpf the flag dpf
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public RecordValidationType verifyIssuerAmount(List<CorporativeOperation> lstProcesses,boolean flagDpf) throws ServiceException{
    	RecordValidationType objRecordValidationTypes= null;
    	List<Long> process = new ArrayList<>();
    	List<Long> errorProcess = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		if(corporative.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode()) &&
    				(corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
    				|| corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode()))){
    		process.add(corporative.getIdCorporativeOperationPk());
    		}
    	}
    	if(!process.isEmpty()){
	    	Map<String, Object>  parameters=new HashMap<String, Object>();
	    	StringBuilder sbQuery = new StringBuilder("select co.idCorporativeOperationPk from  CorporativeOperation co ");
	    	sbQuery.append("where co.idCorporativeOperationPk in (:idprocess)").
	    	append("and co.issuerConfirmedAmount != co.paymentAmount ");
	    	parameters.put("idprocess", process);
	    	errorProcess = findListByQueryString(sbQuery.toString(), parameters);
			if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
				throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_ISSUER_AMOUNT_NOT_EQUAL,new Object[]{StringUtils.join(errorProcess,",")});
			}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
				objRecordValidationTypes= new RecordValidationType();
				objRecordValidationTypes.setErrorProcess(errorProcess);
				objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
						PropertiesConstants.MESSAGE_ISSUER_AMOUNT_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
			}
    	}
    	return objRecordValidationTypes;
    }
    
    /**
     * Search desmaterialized balance.
     *
     * @param idSecurityCodePk the isin code
     * @return the big decimal
     * @throws ServiceException the service exception
     */
    public BigDecimal searchDesmaterializedBalance(String idSecurityCodePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT ");				
		sbQuery.append("       S.desmaterializedBalance + s.physicalBalance" );
		sbQuery.append("  FROM Security S");
		sbQuery.append(" WHERE S.idSecurityCodePk =:idSecurityCodePk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		BigDecimal bl = (BigDecimal)query.getSingleResult();
		
		
		return (BigDecimal.ONE);
	}
    
    /**
     * Verify state corporative before issuer confirm.
     *
     * @param lstProcesses the lst processes
     * @throws ServiceException the service exception
     */
    public void verifyStateCorporativeBeforeIssuerConfirm(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	List<Long> processIntAmoIds = new ArrayList<Long>();
		List<Long> processIds = new ArrayList<Long>();
		List<Long> errorProcess = new ArrayList<>();
		for(CorporativeOperation process : lstProcesses){
			if(!process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
				&& !process.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
				processIds.add(process.getIdCorporativeOperationPk());		
			}else if(process.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode())){
				processIntAmoIds.add(process.getIdCorporativeOperationPk());
			}
		}
		List<Integer> states = new ArrayList<Integer>();
		states.add(CorporateProcessStateType.PRELIMINARY.getCode());
		states.add(CorporateProcessStateType.DEFINITIVE_FAIL.getCode());
		if(!processIds.isEmpty()){
			errorProcess.addAll(verifyCorporateProcessStates(processIds,states));
		}
		if(!processIntAmoIds.isEmpty()){
//			states.add(CorporateProcessStateType.REGISTERED.getCode());
//			states.add(CorporateProcessStateType.MODIFIED.getCode());
			errorProcess.addAll(verifyCorporateProcessStates(processIntAmoIds,states));
		}
		if(errorProcess!= null && !errorProcess.isEmpty()){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_STATE_NO_VALID,new Object[]{StringUtils.join(errorProcess,",")});
		}
    }
    
    /**
     * Verify corporate process states.
     *
     * @param processIds the process ids
     * @param states the states
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<Long> verifyCorporateProcessStates(List<Long> processIds, List<Integer> states) throws ServiceException {
		if(processIds == null || (processIds != null && processIds.isEmpty())){
			return null;
		}
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
		sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
		sbQuery.append(" and cp.state not in (:states)");
		parameters.put("processids", processIds);
		parameters.put("states", states);
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Verify situation issuer.
     *
     * @param lstProcesses the lst processes
     * @throws ServiceException the service exception
     */
    public void verifySituationIssuer(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	List<Long> processRegular = new ArrayList<>();
    	List<Long> processIntAmort = new ArrayList<>();
    	List<Long> errorProcess = new ArrayList<>();
    	List<Long> errorProcessAux = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		if(!corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.INTEREST_PAYMENT.getCode())
    				&& !corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
    			processRegular.add(corporative.getIdCorporativeOperationPk());
    		}else if(corporative.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode())){
    			processIntAmort.add(corporative.getIdCorporativeOperationPk());
    		}
    	}
    	if(!processRegular.isEmpty()){
    		errorProcess.addAll(verifyRequirementSituationCorporative(processRegular,CorporativeSituationType.ISSUER,false,false));
    	}
    	if(!processIntAmort.isEmpty()){
    		errorProcessAux.addAll(verifyRequirementSituationCorporative(processIntAmort,CorporativeSituationType.IDEPOSITARY,true,false));
    	}
    	if(errorProcess!= null && !errorProcess.isEmpty()){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_EXIST_CONFIRMATION_DEPOSITARY,new Object[]{StringUtils.join(errorProcess,",")});
		}
    	if(errorProcessAux!= null && !errorProcessAux.isEmpty()){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_CONFIRMATION_DEPOSITARY,new Object[]{StringUtils.join(errorProcessAux,",")});
		}
    }
    
	/**
	 * Gets the program from corporative.
	 *
	 * @param idProcess the id process
	 * @return the program from corporative
	 * @throws ServiceException the service exception
	 */
	public ProgramInterestCoupon getProgramFromCorporative(Long idProcess) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();				
		sbQuery.append("SELECT CO.programInterestCoupon");		
		sbQuery.append("  FROM CorporativeOperation CO");
		sbQuery.append(" WHERE CO.idCorporativeOperationPk =:idProcess");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idProcess", idProcess);
		return (ProgramInterestCoupon) query.getSingleResult();
	}
	/**
	 * Gets the program from corporative.
	 *
	 * @param idProcess the id process
	 * @return the program from corporative
	 * @throws ServiceException the service exception
	 */
	public ProgramAmortizationCoupon getProgramAmortizationFromCorporative(Long idProcess) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();				
		sbQuery.append("SELECT CO.programAmortizationCoupon");		
		sbQuery.append("  FROM CorporativeOperation CO");
		sbQuery.append(" WHERE CO.idCorporativeOperationPk =:idProcess");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idProcess", idProcess);
		return (ProgramAmortizationCoupon) query.getSingleResult();
	}
	
	/**
	 * Verify process preliminary executed date or Yesterday.
	 *
	 * @param lstProcesses the lst processes
	 * @param processDate the process date
	 * @param flagDpf the flag dpf
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType verifyProcessPreliminaryExecutedDate(List<CorporativeOperation> lstProcesses,Date processDate,boolean flagDpf) throws ServiceException{
		RecordValidationType objRecordValidationTypes= null;
		List<Long> process = new ArrayList<>();
		List<String> processRegular = new ArrayList<>();
		for(CorporativeOperation corporative : lstProcesses){
			if(corporative.getState().equals(CorporateProcessStateType.PRELIMINARY.getCode())){
				processRegular.add(corporative.getIdCorporativeOperationPk().toString());
				process.add(corporative.getIdCorporativeOperationPk());
			}
		}
		if(!processRegular.isEmpty()){
			StringBuilder sbQuery = new StringBuilder();
			Map<String, Object> parameters = new HashMap<String, Object>();
			sbQuery.append("select distinct pld.parameterValue from ProcessLogger pl, ProcessLoggerDetail pld ");
			sbQuery.append(" where pl.idProcessLoggerPk = pld.processLogger.idProcessLoggerPk ");
			sbQuery.append(" and pl.businessProcess.idBusinessProcessPk in (:busprocessid,:busprocessidAux) ");
			sbQuery.append(" and trunc(pl.finishingDate) in( trunc(:processdate), trunc(:processYesterday))");
			sbQuery.append(" and pl.loggerState = :processstate");
			sbQuery.append(" and pld.parameterName = :paramname");
			sbQuery.append(" and pld.parameterValue in (:paramvals)");

			parameters.put("busprocessid", BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_MANUAL.getCode());
			parameters.put("busprocessidAux", BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
			parameters.put("processdate", processDate);
			parameters.put("processYesterday", CommonsUtilities.addDate(processDate, -1));
			parameters.put("processstate", ProcessLoggerStateType.FINISHED.getCode());
			parameters.put("paramname", "processid");
			parameters.put("paramvals", processRegular);
			List<String> processesExecuted = new ArrayList<String>();
			processesExecuted = findListByQueryString(sbQuery.toString(), parameters);
			if(processesExecuted != null && !processesExecuted.isEmpty()){
				for(String proc : processesExecuted){
					processRegular.remove(proc);
					process.remove(Long.valueOf(proc));
				}
			}
			if(!processRegular.isEmpty() && !flagDpf){
				//only stand not executing
				throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_NOT_EXECUTED_TODAY,new Object[]{StringUtils.join(process,",")});
			}else if(processRegular!= null && !processRegular.isEmpty() && flagDpf){
				objRecordValidationTypes= new RecordValidationType();
				objRecordValidationTypes.setErrorProcess(process);
				objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
						PropertiesConstants.MESSAGE_PRELIMINARY_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(process,",")}));
			}
		}
		return objRecordValidationTypes;
	}
	
	/**
	 * Verify holder accounts movement.
	 *
	 * @param lstProcesses the lst processes
	 * @param flagDpf the flag dpf
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType verifyHolderAccountsMovement(List<CorporativeOperation> lstProcesses,boolean flagDpf) throws ServiceException{
		RecordValidationType objRecordValidationTypes= null;
		List<Long> process = new ArrayList<>();
    	List<Long> errorProcess = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select distinct co.idCorporativeOperationPk ");
		sbQuery.append(" from CorporativeOperation co, HolderAccountMovement ham, MovementType mt ");
		sbQuery.append(" where co.idCorporativeOperationPk in (:corpids)");
		sbQuery.append(" and co.securities.idSecurityCodePk = ham.holderAccountBalance.security.idSecurityCodePk ");
		sbQuery.append(" and ham.movementType.idMovementTypePk = mt.idMovementTypePk ");
		sbQuery.append(" and (mt.indBlockMovement = :indyes or mt.indGuaranteeMovement = :indyes) ");
		sbQuery.append(" and trunc(ham.movementDate) > ANY (select max(trunc(pl.processDate)) ");
		sbQuery.append(" from ProcessLogger pl, ProcessLoggerDetail pld ");
		sbQuery.append(" where pl.idProcessLoggerPk = pld.processLogger.idProcessLoggerPk ");
		sbQuery.append(" and pl.businessProcess.idBusinessProcessPk in (:busprocessid,:busprocessidAux) ");
		sbQuery.append(" and pl.loggerState = :processstate");
		sbQuery.append(" and pld.parameterName = :paramname");
		sbQuery.append(" and pld.parameterValue = cast(co.idCorporativeOperationPk as string)) ");
		parameters.put("corpids", process);
		parameters.put("indyes", CorporateProcessConstants.IND_YES);
		parameters.put("busprocessid", BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_MANUAL.getCode());
		parameters.put("busprocessidAux", BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
		parameters.put("processstate", ProcessLoggerStateType.FINISHED.getCode());
		parameters.put("paramname", "processid");
		errorProcess= findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
		//	throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_BLOCK_GUARANTIES_DEFINITIVE,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_BLOCK_REPORT_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
	}
	
	/**
	 * Verify holder accounts movement definitive.
	 *
	 * @param lstProcesses the lst processes
	 * @param flagDpf the flag dpf
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType verifyHolderAccountsMovementDefinitive(List<CorporativeOperation> lstProcesses,boolean flagDpf) throws ServiceException{
		RecordValidationType objRecordValidationTypes= null;
		List<Long> process = new ArrayList<>();
    	List<Long> errorProcess = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select /*+ INDEX_JOIN(MT) */ distinct co.id_corporative_operation_pk from holder_account_movement ham ");
		sbQuery.append("      inner join  movement_type mt");
		sbQuery.append("         on ham.id_movement_type_fk=mt.id_movement_type_pk + 0");
		sbQuery.append("      inner join holder_account ha");
		sbQuery.append("        on ham.id_holder_account_fk=ha.id_holder_account_pk + 0");
		sbQuery.append("      inner join corporative_process_result cpr");
		sbQuery.append("        on ha.id_holder_account_pk=cpr.id_holder_account_fk + 0 ");
		sbQuery.append("      inner join corporative_operation co");
		sbQuery.append("        on cpr.id_corporative_operation_fk=co.id_corporative_operation_pk + 0 ");
		sbQuery.append(" 	    and trunc(ham.movement_date)>trunc(co.preliminary_date) ");
		sbQuery.append("     	and ham.id_security_code_fk = co.ID_ORIGIN_SECURITY_CODE_FK  || ''  ");	
		sbQuery.append("  where (mt.ind_block_movement=:indyes or mt.ind_guarantee_movement=:indyes) ");
		sbQuery.append("     and co.id_corporative_operation_pk in (:listIds) ");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("listIds", process);
		query.setParameter("indyes", BooleanType.YES.getCode());
		errorProcess = query.getResultList();
		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_BLOCK_GUARANTIES_DEFINITIVE,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_BLOCK_REPORT_TWO_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
	}
	
	/**
	 * Verify accreditation balance. Only for Last Coupon Interes and Amortization
	 *
	 * @param lstProcesses the list processes
	 * @param flagDpf the flag DPF
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType verifyAccreditationBalance(List<CorporativeOperation> lstProcesses,boolean flagDpf) throws ServiceException{
		RecordValidationType objRecordValidationTypes= null;
		List<Long> process = new ArrayList<>();
    	List<Long> errorProcess = null;
    	for(CorporativeOperation corporative : lstProcesses){    		
    		process.add(corporative.getIdCorporativeOperationPk());    		
    	}
    	if(!process.isEmpty()){
    		StringBuilder sbQuery = new StringBuilder();
    		Map<String, Object> parameters = new HashMap<String, Object>();
    		sbQuery.append("select distinct co.idCorporativeOperationPk from ");
    		sbQuery.append(" CorporativeOperation co, HolderAccountBalance hab ");
    		sbQuery.append(" where co.idCorporativeOperationPk in (:processids) ");
    		sbQuery.append(" and co.securities.idSecurityCodePk = hab.security.idSecurityCodePk ");
    		sbQuery.append(" and hab.accreditationBalance > 0 ");
    		parameters.put("processids", process);
    		errorProcess = findListByQueryString(sbQuery.toString(), parameters);
    		if(errorProcess!= null && !errorProcess.isEmpty() && !flagDpf){
    			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_ACCREDITATION_BALANCE,new Object[]{StringUtils.join(errorProcess,",")});
    		}else if(errorProcess!= null && !errorProcess.isEmpty() && flagDpf){
    			objRecordValidationTypes= new RecordValidationType();
    			objRecordValidationTypes.setErrorProcess(errorProcess);
    			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
    					PropertiesConstants.MESSAGE_HAVE_ACCREDITATION_DEFINITIVE_INCORRECT,  new Object[]{StringUtils.join(errorProcess,",")}));
    		}
    	}
    	return objRecordValidationTypes;
	}
	
	/**
	 * Verify holder movements preliminar.
	 *
	 * @param lstProcesses the lst processes
	 * @param flag the flag
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType verifyHolderMovementsPreliminar(List<CorporativeOperation> lstProcesses,boolean flag) throws ServiceException{ 
		RecordValidationType objRecordValidationTypes = null;
		List<Long> process = new ArrayList<>();
    	List<Long> errorProcess = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	StringBuilder sbQuery = new StringBuilder();
    	sbQuery.append("        select distinct co.id_corporative_operation_pk from holder_account_movement ham ");
		sbQuery.append("   	join  movement_type mt");
		sbQuery.append("   on ham.id_movement_type_fk=mt.id_movement_type_pk");
		sbQuery.append("  join holder_account ha");
		sbQuery.append("        on ham.id_holder_account_fk=ha.id_holder_account_pk");
		sbQuery.append("  join corporative_process_result cpr");
		sbQuery.append("   on ha.id_holder_account_pk=cpr.id_holder_account_fk");
		sbQuery.append("   join corporative_operation co");
		sbQuery.append("   on cpr.id_corporative_operation_fk=co.id_corporative_operation_pk");
		sbQuery.append("       where (mt.ind_block_movement=:indyes or mt.ind_guarantee_movement=:indyes)");
		sbQuery.append(" and co.id_corporative_operation_pk in (:listProcess)");
		sbQuery.append("  and trunc(movement_date) > (select trunc(creation_date) from stock_calculation_process");
		sbQuery.append(" where trunc(cutoff_date)=trunc(co.cutoff_date)");
		sbQuery.append(" and trunc(registry_date)=trunc(co.registry_date)");
		sbQuery.append("  and stock_type=:stock_type");
		sbQuery.append(" and stock_class=:stock_class");
		sbQuery.append(" and id_Stock_Calculation_pk=nvl(co.id_Stock_Calculation,0)");
		sbQuery.append(" and id_security_code_fk=co.ID_ORIGIN_SECURITY_CODE_FK)");
		sbQuery.append("");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("stock_class",StockClassType.NORMAL.getCode());
		query.setParameter("stock_type",StockType.CORPORATIVE.getCode());
		query.setParameter("indyes", BooleanType.YES.getCode());
		query.setParameter("listProcess", process);
		errorProcess = query.getResultList();
		if(errorProcess!= null && !errorProcess.isEmpty() && !flag){
			throw new ServiceException(ErrorServiceType.CORPORATE_EVENT_EXIST_BLOCK_GUARANTIES,new Object[]{StringUtils.join(errorProcess,",")});
		}else if(errorProcess!= null && !errorProcess.isEmpty() && flag){
			objRecordValidationTypes= new RecordValidationType();
			objRecordValidationTypes.setErrorProcess(errorProcess);
			objRecordValidationTypes.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE,
					PropertiesConstants.MESSAGE_CORPORATE_EVENT_EXIST_BLOCK_GUARANTIES,  new Object[]{StringUtils.join(errorProcess,",")}));
		}
		return objRecordValidationTypes;
	}
	
	/**
	 * Update program interest coupon.
	 *
	 * @param idProgramInterestCoupon the id program interest coupon
	 * @param stateCoupon the state coupon
	 * @param situationProgramInterest the situation program interest
	 * @param loggerUser the logger user
	 */
	public void updateProgramInterestCoupon(Long idProgramInterestCoupon, Integer stateCoupon,Integer situationProgramInterest, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ProgramInterestCoupon ");
		stringBuffer.append(" SET stateProgramInterest = :stateCoupon ");
		stringBuffer.append(" , situationProgramInterest = :situationProgramInterest ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramInterestPk = :idProgramInterestCoupon ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("stateCoupon", stateCoupon);
		query.setParameter("situationProgramInterest", situationProgramInterest);
		query.setParameter("idProgramInterestCoupon", idProgramInterestCoupon);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update program amortization coupon.
	 *
	 * @param idProgramAmortizationCoupon the id program amortization coupon
	 * @param stateCoupon the state coupon
	 * @param situationProgramAmortization the situation program amortization
	 * @param loggerUser the logger user
	 */
	public void updateProgramAmortizationCoupon(Long idProgramAmortizationCoupon, Integer stateCoupon,Integer situationProgramAmortization, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ProgramAmortizationCoupon ");
		stringBuffer.append(" SET stateProgramAmortization = :stateCoupon ");
		stringBuffer.append(" , situationProgramAmortization = :situationProgramAmortization ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramAmortizationPk = :idProgramAmortizationCoupon ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("stateCoupon", stateCoupon);
		query.setParameter("situationProgramAmortization", situationProgramAmortization);
		query.setParameter("idProgramAmortizationCoupon", idProgramAmortizationCoupon);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update program amortization coupon.
	 *
	 * @param lstProgramAmortizationCoupon the lst program amortization coupon
	 * @param stateCoupon the state coupon
	 * @param situationProgramAmortization the situation program amortization
	 * @param loggerUser the logger user
	 */
	public void updateProgramAmortizationCoupon(List<Long> lstProgramAmortizationCoupon, Integer stateCoupon,Integer situationProgramAmortization, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ProgramAmortizationCoupon ");
		stringBuffer.append(" SET stateProgramAmortization = :stateCoupon ");
		stringBuffer.append(" , situationProgramAmortization = :situationProgramAmortization ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramAmortizationPk in (:lstProgramAmortizationCoupon) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("stateCoupon", stateCoupon);
		query.setParameter("situationProgramAmortization", situationProgramAmortization);
		query.setParameter("lstProgramAmortizationCoupon", lstProgramAmortizationCoupon);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Validate preliminary process.
	 *
	 * @param lstProcesses the lst processes
	 * @throws ServiceException the service exception
	 */
	public void validatePreliminaryProcess(List<CorporativeOperation> lstProcesses) throws ServiceException{
				//check state process
				verifyPreliminaryProcessStates(lstProcesses,false);
				//check delivery date
				//verifyPreliminarDeliveryDate(lstProcesses,false);//PREGUNTAR SI VA A IR LA VALIDACION - JH TMP
				//check stock calculation
				verifyPreliminarStockCalculation(lstProcesses);
				//check securities state
				verifySecurityStatus(lstProcesses,false);
				//check balance desmaterialized 
				verifySecurityDesmaterialized(lstProcesses,false);
				// check securities balance for special
				verifySecurityBalance(lstProcesses,CorporateProcessStateType.PRELIMINARY.getCode());
				//check settelements operations
				verifyNotSettledBalancesCorporate(lstProcesses,false);
				//check transit balances
				verifyTransitBalances(lstProcesses,false);
				//check issuer confirmation
//				verifyEmisorConfirm(lstProcesses,false);
				//Check movements for preliminar
				verifyHolderMovementsPreliminar(lstProcesses,false);
				//check dayli exchange
				verifyExchangeRate(lstProcesses,false);
				//check retirement operations
				verifyCorporateCreatedRetirementOperation(lstProcesses,false);
				//check accreditation balance
				//verifyAccreditationBalance(lstProcesses,false);
	}
	
	/**
	 * Validate confirmation depositary.
	 *
	 * @param lstProcesses the lst processes
	 * @throws ServiceException the service exception
	 */
	public void validateConfirmationDepositary(List<CorporativeOperation> lstProcesses) throws ServiceException{
				//check state process
				verifyStateConfirmDepositary(lstProcesses,false);
				//check confirmations previus
				verifySituationDepositary(lstProcesses);
				//check Adjustments
				verifyHolderAccountAdjustments(lstProcesses,CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,false);
				//check delivery date
				//verifyPreliminarDeliveryDate(lstProcesses,false); //PREGUNTAR SI VA A IR LA VALIDACION - JH TMP 
				//check Issuer amount
				verifyIssuerAmount(lstProcesses,false);
	}
	
	/**
	 * Execute confirmation issuer.
	 *
	 * @param lstProcesses the lst processes
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void executeConfirmationIssuer(List<CorporativeOperation> lstProcesses,LoggerUser loggerUser) throws ServiceException{
		
		//check state of corporatives before issuer confirm
		verifyStateCorporativeBeforeIssuerConfirm(lstProcesses);
		//check situation
		verifySituationIssuer(lstProcesses);
		//check delivery date
		//verifyPreliminarDeliveryDate(lstProcesses,false);//PREGUNTAR SI VA A IR LA VALIDACION - JH TMP
		for(CorporativeOperation operation : lstProcesses){
			 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType()) 
					 || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
				 //Validating if the Security has desmaterialized Balance
				 BigDecimal desmaterializedBalance = searchDesmaterializedBalance(operation.getSecurities().getIdSecurityCodePk());
				 if(desmaterializedBalance.compareTo(BigDecimal.ZERO) > 0 &&
						 hasBalance(operation.getSecurities().getIdSecurityCodePk())){
					 operation.setSituation(CorporativeSituationType.ISSUER.getCode());
					 update(operation);
				 }else{
					 //not desmaterialized
					 operation.setState(CorporateProcessStateType.DEFINITIVE.getCode());
					 operation.setSituation(CorporativeSituationType.PAID.getCode());
					 operation.setIndPayed(BooleanType.YES.getCode());
					 operation.setIssuerConfirmedAmount(new BigDecimal(0));
					 operation.setPaymentAmount(new BigDecimal(0));
					 update(operation);
					 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
						 ProgramInterestCoupon interestCoupon = getProgramFromCorporative(operation.getIdCorporativeOperationPk());
						 interestCoupon.setStateProgramInterest(ProgramScheduleStateType.EXPIRED.getCode());
						 update(interestCoupon);
					 }else if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
						 ProgramAmortizationCoupon amortizationCoupon = getProgramAmortizationFromCorporative(operation.getIdCorporativeOperationPk());
						 amortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.EXPIRED.getCode());
						 update(amortizationCoupon);
						 if(operation.getIndAffectBalance().equals(BooleanType.YES.getCode())){
							//Actualizamos estado de valor (VENCIDO)
							corporateExecutorService.updateSecurity(operation.getSecurities().getIdSecurityCodePk(), operation.getDeliveryDate(), 
									SecurityStateType.EXPIRED.getCode(), null, null,null ,loggerUser);
							//obtenemos emision
							Issuance issuance = corporateExecutorService.findIssuance(operation.getSecurities().getIdSecurityCodePk());	
							issuance.setStateIssuance(IssuanceStateType.EXPIRED.getCode());
							issuance.setExpirationDate(operation.getDeliveryDate());
							corporateExecutorService.update(issuance, loggerUser);	
						 }
					 }					
				 }
			 }else{
				 operation.setSituation(CorporativeSituationType.ISSUER.getCode());
				 update(operation);
			 }
		}
	}
	
	/**
	 * Execute confirmation depositary.
	 *
	 * @param lstProcesses the lst processes
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void executeConfirmationDepositary(List<CorporativeOperation> lstProcesses,LoggerUser loggerUser) throws ServiceException{
		//validate Confirmation Depositary
		validateConfirmationDepositary(lstProcesses);
		
		for(CorporativeOperation operation : lstProcesses){
			 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType()) 
					 || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
				 //Validating if the Security has desmaterialized Balance
				 BigDecimal desmaterializedBalance = searchDesmaterializedBalance(operation.getSecurities().getIdSecurityCodePk());
				 if(desmaterializedBalance.compareTo(BigDecimal.ZERO) > 0 &&
						 hasBalance(operation.getSecurities().getIdSecurityCodePk())){
					 operation.setSituation(CorporativeSituationType.IDEPOSITARY.getCode());
					 update(operation);
				 }else{
					 //not desmaterialized
					 operation.setState(CorporateProcessStateType.DEFINITIVE.getCode());
					 operation.setSituation(CorporativeSituationType.PAID.getCode());
					 operation.setIndPayed(BooleanType.YES.getCode());
					 operation.setIssuerConfirmedAmount(new BigDecimal(0));
					 operation.setPaymentAmount(new BigDecimal(0));
					 update(operation);
					 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
						 ProgramInterestCoupon interestCoupon = getProgramFromCorporative(operation.getIdCorporativeOperationPk());
						 interestCoupon.setStateProgramInterest(ProgramScheduleStateType.EXPIRED.getCode());
						 update(interestCoupon);
					 }else if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
						 ProgramAmortizationCoupon amortizationCoupon = getProgramAmortizationFromCorporative(operation.getIdCorporativeOperationPk());
						 amortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.EXPIRED.getCode());
						 update(amortizationCoupon);
						 if(operation.getIndAffectBalance().equals(BooleanType.YES.getCode())){
							//Actualizamos estado de valor (VENCIDO)
							corporateExecutorService.updateSecurity(operation.getSecurities().getIdSecurityCodePk(), operation.getDeliveryDate(), 
									SecurityStateType.EXPIRED.getCode(), null, null,null ,loggerUser);
							//obtenemos emision
							Issuance issuance = corporateExecutorService.findIssuance(operation.getSecurities().getIdSecurityCodePk());	
							issuance.setStateIssuance(IssuanceStateType.EXPIRED.getCode());
							issuance.setExpirationDate(operation.getDeliveryDate());
							corporateExecutorService.update(issuance, loggerUser);	
						 }
					 }	
				 }
			 }else{
				 operation.setSituation(CorporativeSituationType.IDEPOSITARY.getCode());
				 update(operation);
			 }
		}
	}
	
	/**
	 * Validate definitive process.
	 *
	 * @param lstProcesses the lst processes
	 * @throws ServiceException the service exception
	 */
	public  void validateDefinitiveProcess(List<CorporativeOperation> lstProcesses) throws ServiceException{
				//check validate ultimate amortization
				verifyDefinitiveAmortization(lstProcesses,false);
				// check securities balance for special
				verifySecurityBalance(lstProcesses,CorporateProcessStateType.DEFINITIVE.getCode());
				//check securities state
				verifySecurityStatus(lstProcesses,false);
				//check balance desmaterialized 
				verifySecurityDesmaterialized(lstProcesses,false);
				//check states
				verifyDefinitiveProcessStates(lstProcesses,false);
				//check confirmations
				verifySituationDefinitiveProcess(lstProcesses,false);
				//check open delivery date
				//verifyPreliminarExpirationDate(lstProcesses,false);//PREGUNTAR SI VA A IR LA VALIDACION - JH TMP 
				//check closed delivery date
				//verifyDefinitiveExpirationDate(lstProcesses,false);//PREGUNTAR SI VA A IR LA VALIDACION - JH TMP
				//check preliminary process date
				//verifyProcessPreliminaryExecutedDate(lstProcesses, CommonsUtilities.currentDate(),false);//PREGUNTAR SI VA A IR LA VALIDACION - JH TMP
				//check adjustments
				verifyHolderAccountAdjustments(lstProcesses,CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,false);
				//check Issuer amount
				verifyIssuerAmount(lstProcesses,false);
				//check holder movements
				verifyHolderAccountsMovement(lstProcesses,false);
				//check holder movements definitive
				verifyHolderAccountsMovementDefinitive(lstProcesses,false);
				//check accreditation balance
				//verifyAccreditationBalance(lstProcesses,false);
	}
	
	/**
	 * Test rollback y transaction.
	 *
	 * @throws ServiceException the service exception
	 */
	public void testRollbackYTransaction() throws ServiceException{
		CorporativeOperation corpoOpe = find(CorporativeOperation.class, new Long(6301));
		corpoOpe.setIndAffectBalance(1);
		Security sec =  corpoOpe.getSecurities();
		sec.getDescription();
		update(corpoOpe);
		//roolback transaction
		throw new ServiceException(ErrorServiceType.ERROR_ALLOCATION_BENEFIT_PROCESS);
	}
	
	/**
	 * Gets the list corporative operation interest.
	 *
	 * @param idSecurityCode the id security code
	 * @param couponNumber the coupon number
	 * @param expirationDate the expiration date
	 * @return the list corporative operation interest
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> getListCorporativeOperationInterest(String idSecurityCode, Integer couponNumber , 
																		Date expirationDate)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT co ");
			sbQuery.append("	FROM CorporativeOperation co");
			sbQuery.append(" 	inner join fetch co.programInterestCoupon pic ");
			sbQuery.append(" 	inner join co.securities se ");
			sbQuery.append(" 	inner join co.corporativeEventType cet ");
			sbQuery.append("	Where 1 = 1	");
			sbQuery.append("	and se.idSecurityCodePk = :idSecurityCodePk");
			sbQuery.append("	and pic.couponNumber = :couponNumber");
			sbQuery.append("	and trunc(pic.experitationDate) = trunc(:expirationDate)");
						
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCode);
			query.setParameter("couponNumber", couponNumber);
			query.setParameter("expirationDate", expirationDate);
					
			return (List<CorporativeOperation>)query.getResultList();
			} catch(NonUniqueResultException ex){
				   return null;
		}
	}
	
	/**
	 * Was payed interest coupon.
	 *
	 * @param processDate the process date
	 * @param idCorporativeOperation the id corporative operation
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean wasPayedInterestCoupon(Date processDate, Long idCorporativeOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT.idInterfaceTransactionPk ");
		stringBuffer.append(" FROM InterfaceTransaction IT ");
		stringBuffer.append(" WHERE IT.corporativeOperation.idCorporativeOperationPk = :idCorporativeOperation ");
		stringBuffer.append(" and trunc(IT.interfaceProcess.processDate) = trunc(:processDate) ");
		stringBuffer.append(" and IT.transactionState = :transactionState ");
		stringBuffer.append(" and IT.interfaceProcess.idExternalInterfaceFk.interfaceName = :interfaceName ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idCorporativeOperation", idCorporativeOperation);
		query.setParameter("processDate", processDate);
		query.setParameter("transactionState", BooleanType.YES.getCode());
		query.setParameter("interfaceName", ComponentConstant.INTERFACE_INTEREST_PAYMENT_PCU);
		
		List<Long> lstInterfaceTransaction= query.getResultList();
		if (Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Gets the list corporative amortization.
	 *
	 * @param idSecurityCode the id security code
	 * @param indReverse the ind reverse
	 * @return the list corporative amortization
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListCorporativeAmortization(String idSecurityCode ,boolean indReverse)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT co.idCorporativeOperationPk, "); //0
		sbQuery.append("	co.corporativeEventType.idCorporativeEventTypePk, "); //1
		sbQuery.append("	co.corporativeEventType.corporativeEventType, "); //2
		sbQuery.append("	co.securities.idSecurityCodePk, "); //3
		sbQuery.append("	co.securities.currentNominalValue, "); //4
		sbQuery.append("	co.deliveryDate, "); //5
		sbQuery.append("	co.programAmortizationCoupon.idProgramAmortizationPk, "); //6
		sbQuery.append("	co.securities.issuance.idIssuanceCodePk "); //7
		sbQuery.append("	FROM CorporativeOperation co");
		sbQuery.append("	Where 1 = 1	");
		sbQuery.append("	and co.corporativeEventType.corporativeEventType = :corporativeEventType ");
		sbQuery.append("	and co.securities.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	and co.securities.indIsCoupon = :indIsCoupon");
		if(indReverse){
			sbQuery.append("	and co.state = :stateDefinitive");
		}else{
			sbQuery.append("	and (co.state = :stateRegistered or co.state = :stateModified or co.state = :statePreliminary OR co.state = :stateDefinitive)");
		}			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("corporativeEventType", ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		query.setParameter("idSecurityCodePk", idSecurityCode);
		query.setParameter("indIsCoupon", BooleanType.NO.getCode());
		if(indReverse){
			query.setParameter("stateDefinitive", CorporateProcessStateType.DEFINITIVE.getCode());
		}else{
			query.setParameter("stateRegistered", CorporateProcessStateType.REGISTERED.getCode());
			query.setParameter("stateModified", CorporateProcessStateType.MODIFIED.getCode());
			query.setParameter("statePreliminary", CorporateProcessStateType.PRELIMINARY.getCode());
			query.setParameter("stateDefinitive", CorporateProcessStateType.DEFINITIVE.getCode());
		}
		
		return query.getResultList();
	}
	
	/**
	 * Gets the list corporative operation amortization.
	 *
	 * @param idSecurityCode the id security code
	 * @param indReverse the ind reverse
	 * @return the list corporative operation amortization
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<CorporativeOperation> getListCorporativeOperationAmortization(String idSecurityCode ,boolean indReverse)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT co ");
		sbQuery.append("	FROM CorporativeOperation co");
		sbQuery.append(" 	inner join fetch co.programAmortizationCoupon pac ");
		sbQuery.append(" 	inner join fetch co.securities se ");
		sbQuery.append(" 	inner join fetch co.corporativeEventType cet ");
		sbQuery.append("	Where 1 = 1	");		
		sbQuery.append("	and se.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	and se.indIsCoupon = :indIsCoupon");
		if(indReverse){
			sbQuery.append("	and co.state = :stateDefinitive");
		}else{
			sbQuery.append("	and (co.state = :stateRegistered or co.state = :stateModified or co.state = :statePreliminary)");
		}			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCode);
		query.setParameter("indIsCoupon", BooleanType.NO.getCode());
		if(indReverse){
			query.setParameter("stateDefinitive", CorporateProcessStateType.DEFINITIVE.getCode());
		}else{
			query.setParameter("stateRegistered", CorporateProcessStateType.REGISTERED.getCode());
			query.setParameter("stateModified", CorporateProcessStateType.MODIFIED.getCode());
			query.setParameter("statePreliminary", CorporateProcessStateType.PRELIMINARY.getCode());
		}
		
		return (List<CorporativeOperation>)query.getResultList();
	}
	
	/**
	 * Checks if is last interest coupon.
	 *
	 * @param idSecurityCode the id security code
	 * @param indReverse the ind reverse
	 * @return true, if is last interest coupon
	 * @throws ServiceException the service exception
	 */
	public boolean isLastInterestCoupon(String idSecurityCode , boolean indReverse)throws ServiceException{
		boolean isLast = BooleanType.NO.getBooleanValue();
		List<Long> lstObject = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" SELECT PIC.idProgramInterestPk ");
			sbQuery.append(" FROM ProgramInterestCoupon PIC ");
			sbQuery.append(" WHERE PIC.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk ");
			sbQuery.append(" and PIC.stateProgramInterest = :stateProgramInterest ");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCode);
			query.setParameter("stateProgramInterest", ProgramScheduleStateType.PENDING.getCode());

			lstObject = query.getResultList();
			if(Validations.validateListIsNotNullAndNotEmpty(lstObject)){
				if(lstObject.size() == GeneralConstants.ONE_VALUE_INTEGER.intValue() && !indReverse){
					isLast = BooleanType.YES.getBooleanValue();
				}
			}else{
				if(indReverse){
					isLast = BooleanType.YES.getBooleanValue();
				}
			}
		} catch(NonUniqueResultException ex){
			   return isLast;
		}
		return isLast;
	}
	
	/**
	 * Verify exist balance all.
	 *
	 * @param securityCode the security code
	 * @param isReverse the is reverse
	 * @return true, if successful
	 */
	public boolean verifyExistBalanceAll(String securityCode, boolean isReverse){
		boolean isLast = BooleanType.YES.getBooleanValue();
		List<BigDecimal> lstHolderAccountBalance = null;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	select hab.totalBalance from HolderAccountBalance hab ");
		sbQuery.append("	where hab.id.idSecurityCodePk = :isincodes ");
		if (isReverse) {
			sbQuery.append(" 	and hab.totalBalance = 0 ");
		} else {
			sbQuery.append(" 	and hab.availableBalance > 0 ");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("isincodes", securityCode);
		lstHolderAccountBalance = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalance)){
			isLast = BooleanType.NO.getBooleanValue();
		}
			
		return isLast;
    }
	
	/**
	 * Metodo para extraer los saldos del valor antes del vencimiento
	 * @param securityCode
	 * @return
	 */
	public HolderAccountBalance extractExistBalanceAll(String securityCode){
		
		HolderAccountBalance accountBalance = new HolderAccountBalance();
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	select hab from HolderAccountBalance hab ");
		sbQuery.append("	join fetch hab.participant p");
		sbQuery.append("	join fetch hab.holderAccount ha");
		sbQuery.append("	join fetch hab.security sec");
		sbQuery.append("	where hab.id.idSecurityCodePk = :isincodes ");
		sbQuery.append(" 	and hab.totalBalance > 0 ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("isincodes", securityCode);
		
		accountBalance = (HolderAccountBalance) query.getSingleResult();
			
		return accountBalance;
    }
	
	
	/**
	 * Gets the list holder account balance all.
	 *
	 * @param securityCode the security code
	 * @return the list holder account balance all
	 */
	public List <HolderAccountBalance> getListHolderAccountBalanceAll(String securityCode){
		List<HolderAccountBalance> lstHolderAccountBalance = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	select hab from HolderAccountBalance hab ");
			sbQuery.append("	where hab.id.idSecurityCodePk = :isincodes ");
			sbQuery.append("	and hab.totalBalance > 0 ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("isincodes", securityCode);
			lstHolderAccountBalance = query.getResultList();
			if( lstHolderAccountBalance!= null && lstHolderAccountBalance.size()>0){
				return (List<HolderAccountBalance>)lstHolderAccountBalance;
			}
			} catch(NonUniqueResultException ex){
				   return null;
			}
		return null;
    }
	
	/**
	 * Gets the holder account corporative.
	 *
	 * @param idCorporativeOperation the id corporative operation
	 * @return the holder account corporative
	 */
	public HolderAccountBalance getHolderAccountCorporative(Long idCorporativeOperation){
		HolderAccountBalance holderAccountBalance = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	select distinct hab ");
			sbQuery.append("	from HolderAccountBalance hab, HolderAccountMovement HAM ");
			sbQuery.append("	INNER JOIN FETCH hab.holderAccount ");
			sbQuery.append("	INNER JOIN FETCH hab.security ");
			sbQuery.append("	INNER JOIN FETCH hab.participant ");
			sbQuery.append("	where HAM.corporativeOperation.idCorporativeOperationPk = :idCorporativeOperation ");
			sbQuery.append(" 	and HAM.holderAccountBalance.holderAccount.idHolderAccountPk = hab.holderAccount.idHolderAccountPk ");
			sbQuery.append("	and HAM.holderAccountBalance.security.idSecurityCodePk = hab.security.idSecurityCodePk ");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idCorporativeOperation", idCorporativeOperation);
			holderAccountBalance = (HolderAccountBalance) query.getSingleResult();
		} catch (NoResultException e) {
			
		}
		return holderAccountBalance;
    }
	
	/**
	 * Gets the total balance for corporate process.
	 *
	 * @param security the security
	 * @return the total balance for corporate process
	 */
	public Object getTotalBalanceForCorporateProcess(String security){
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select sum(hab.totalBalance) from HolderAccountBalance hab ");
		sbQuery.append(" where hab.id.idSecurityCodePk = :security");
		parameters.put("security", security);
		
		return findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Execute Accounts Component For Corporative DPF.
	 *
	 * @param corpOperation object corporative operation
	 * @param loggerUser data user
	 * @param processType process type
	 * @param indUnBlock the ind un block
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeAccountsComponentForCorporativeDPF(CorporativeOperation corpOperation,LoggerUser loggerUser,
			ImportanceEventType processType,boolean indUnBlock)throws ServiceException{
		AccountsComponentService accountsComponentService = executorComponentServiceBean.get();
		//obtenemos todas las cuentas matrices asociadas al valor
		List <HolderAccountBalance> lstHolderAccountBalance = getListHolderAccountBalanceAll(corpOperation.getSecurities().getIdSecurityCodePk());
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalance)){
			//recorremos cuentas matrices asociadas al valor
			for(HolderAccountBalance objHolderAccountBalance : lstHolderAccountBalance){
				//si indicador de desbloqueo activo
				if(indUnBlock){
					//obtenemos operaciones de bloqueos asociados a la cuenta matriz
					List<Object[]> blockDocuments = getBlockCorpProcesForUnblockDefinitive(objHolderAccountBalance);
					if(blockDocuments != null && !blockDocuments.isEmpty()){
						for(Object[] data : blockDocuments){
							//registramos solicitud de desbloqueo
							UnblockRequest unblockRequest = documentFixer.prepareUnblockOperation(data, loggerUser.getUserName());
							BigDecimal quantity = (BigDecimal)data[2];
							Long blockOperationId = (Long)data[3];
							HolderAccountBalanceTO account = new HolderAccountBalanceTO();
							account.setIdHolderAccount((Long)data[5]);
							account.setIdParticipant((Long)data[6]);
							account.setIdSecurityCode((String)data[7]);
							account.setStockQuantity(quantity);
							account.setFinalDate(corpOperation.getDeliveryDate());
							List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
							List<MarketFactAccountTO> lstMarketFactAccounts = getListMarketFactAccounts(
									account.getIdHolderAccount(), account.getIdParticipant(), 
									account.getIdSecurityCode(),CorporateProcessUtils.getBlockBalance((Integer)data[0], null));
							account.setLstMarketFactAccounts(lstMarketFactAccounts);
							accounts.add(account);
							corporateExecutorService.create(unblockRequest, loggerUser);
							corporateExecutorService.updateBlockOperation(blockOperationId, quantity, true, true, AffectationStateType.UNBLOCKED.getCode(), null, loggerUser);
							//generamos movimento de saldos (desbloqueo de saldo)
							accountsComponentService.executeAccountsComponent(CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
									unblockRequest.getUnblockOperation().get(0).getIdUnblockOperationPk(), blockOperationId, null, null, processType, accounts, null,
									CorporateProcessUtils.getBlockBalance((Integer)data[0], null),idepositarySetup.getIndMarketFact()));
						}
					}
				}
				 List<HolderAccountBalanceTO> sourceAccountBalanceTOs = new  ArrayList<HolderAccountBalanceTO>();
				 List<HolderAccountBalanceTO> targetAccountBalanceTOs = new  ArrayList<HolderAccountBalanceTO>();
				 HolderAccountBalanceTO account = new HolderAccountBalanceTO();					
				 account.setIdHolderAccount(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
				 account.setIdParticipant(objHolderAccountBalance.getParticipant().getIdParticipantPk());
				 account.setIdSecurityCode(objHolderAccountBalance.getSecurity().getIdSecurityCodePk());
				 account.setStockQuantity(objHolderAccountBalance.getAvailableBalance());
				 List<MarketFactAccountTO> lstMarketFactAccounts = getListMarketFactAccounts(
							account.getIdHolderAccount(), account.getIdParticipant(),
							account.getIdSecurityCode(),CorporateProcessConstants.CP_AVAILABLE);
					account.setLstMarketFactAccounts(lstMarketFactAccounts);
				 sourceAccountBalanceTOs.add(account);
				 accountsComponentService.executeAccountsComponent(CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
						null, null, null, null, processType,
						sourceAccountBalanceTOs, targetAccountBalanceTOs, CorporateProcessConstants.BL_AVAILABLE,idepositarySetup.getIndMarketFact()));
			}
		}
	}
	
	/**
	 * Gets the list market fact accounts.
	 *
	 * @param idHolderAccount the id holder account
	 * @param idParticipant the id participant
	 * @param idSecurity the id security
	 * @param indBalance the ind balance
	 * @return the list market fact accounts
	 */
	public List<MarketFactAccountTO> getListMarketFactAccounts(Long idHolderAccount,Long idParticipant, String idSecurity,int indBalance){
		List<MarketFactAccountTO> lstMarketFactAccounts = null;
		List <HolderMarketFactBalance> lstHolderMarketFact = getListHolderMarketFactBalanceAll(idHolderAccount, idParticipant, idSecurity);
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderMarketFact)){
			lstMarketFactAccounts = new ArrayList<MarketFactAccountTO>();
			MarketFactAccountTO objMarketFactAccountTO = null;
			for(HolderMarketFactBalance objHolderMarketFactBalance : lstHolderMarketFact){
				objMarketFactAccountTO = new MarketFactAccountTO();
				objMarketFactAccountTO.setMarketDate(objHolderMarketFactBalance.getMarketDate());
				objMarketFactAccountTO.setMarketPrice(objHolderMarketFactBalance.getMarketPrice());
				objMarketFactAccountTO.setMarketRate(objHolderMarketFactBalance.getMarketRate());
				if(indBalance == CorporateProcessConstants.BL_AVAILABLE){
					objMarketFactAccountTO.setQuantity(objHolderMarketFactBalance.getAvailableBalance());
				}else if(indBalance == CorporateProcessConstants.BL_PAWN){
					objMarketFactAccountTO.setQuantity(objHolderMarketFactBalance.getPawnBalance());
				}else if(indBalance == CorporateProcessConstants.BL_BAN){
					objMarketFactAccountTO.setQuantity(objHolderMarketFactBalance.getBanBalance());
				}else if(indBalance == CorporateProcessConstants.BL_OTHER_BLOCK){
					objMarketFactAccountTO.setQuantity(objHolderMarketFactBalance.getOtherBlockBalance());
				}else if(indBalance == CorporateProcessConstants.BL_RESERVE){
					objMarketFactAccountTO.setQuantity(objHolderMarketFactBalance.getReserveBalance());
				}else if(indBalance == CorporateProcessConstants.BL_OPPOSITION){
					objMarketFactAccountTO.setQuantity(objHolderMarketFactBalance.getOppositionBalance());
				}else{
					objMarketFactAccountTO.setQuantity(BigDecimal.ONE);
				}
				lstMarketFactAccounts.add(objMarketFactAccountTO);	
			}
		}
		return lstMarketFactAccounts;		
	}
	
	/**
	 * Gets the list holder market fact balance all.
	 *
	 * @param idHolderAccount the id holder account
	 * @param idParticipant the id participant
	 * @param idSecurity the id security
	 * @return the list holder market fact balance all
	 */
	public List <HolderMarketFactBalance> getListHolderMarketFactBalanceAll(Long idHolderAccount,Long idParticipant,String idSecurity){
		List<HolderMarketFactBalance> lstHolderMarketFactBalance = null;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	select hab from HolderMarketFactBalance hab ");
		sbQuery.append("	where hab.security.idSecurityCodePk = :isincodes ");
		sbQuery.append("	and hab.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append("	and hab.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		sbQuery.append("	and hab.indActive = :indActive ");
		sbQuery.append("	and hab.totalBalance > 0 ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("isincodes", idSecurity);
		query.setParameter("idParticipantPk", idParticipant);
		query.setParameter("idHolderAccountPk", idHolderAccount);
		query.setParameter("indActive", BooleanType.YES.getCode());
		lstHolderMarketFactBalance = query.getResultList();
		
		return lstHolderMarketFactBalance;
    }
	
	/**
	 * Gets the block corp proces for unblock definitive.
	 *
	 * @param objHolderAccountBalance the obj holder account balance
	 * @return the block corp proces for unblock definitive
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getBlockCorpProcesForUnblockDefinitive(HolderAccountBalance objHolderAccountBalance)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select br.blockType, br.holder.idHolderPk, bo.actualBlockBalance, ");
		sbQuery.append(" bo.idBlockOperationPk, bo.documentNumber, bo.holderAccount.idHolderAccountPk, ");
		sbQuery.append(" bo.participant.idParticipantPk, bo.securities.idSecurityCodePk, bo.blockLevel ");
		sbQuery.append(" from BlockRequest br, BlockOperation bo ");
		sbQuery.append(" where br.idBlockRequestPk = bo.blockRequest.idBlockRequestPk ");
		sbQuery.append(" and bo.participant.idParticipantPk = :participant ");		
		sbQuery.append(" and bo.securities.idSecurityCodePk = :security ");
		sbQuery.append(" and bo.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		sbQuery.append(" and bo.blockState = :blockState ");
		sbQuery.append(" and br.blockRequestState = :blockRequestState ");
		sbQuery.append(" order by bo.holderAccount.idHolderAccountPk, br.blockType ");

		parameters.put("participant", objHolderAccountBalance.getParticipant().getIdParticipantPk());
		parameters.put("security", objHolderAccountBalance.getSecurity().getIdSecurityCodePk());
		parameters.put("idHolderAccountPk", objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
		parameters.put("blockState",AffectationStateType.BLOCKED.getCode() );
		parameters.put("blockRequestState", RequestAffectationStateType.CONFIRMED.getCode());

		return findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Execute Accounts Component For Corporative DPF.
	 *
	 * @param corpOperation object corporative operation
	 * @param loggerUser data user
	 * @param processType process type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeReverseAccountsComponentForCorporativeDPF(CorporativeOperation corpOperation,LoggerUser loggerUser,
															ImportanceEventType processType)throws ServiceException{
		AccountsComponentService accountsComponentService = executorComponentServiceBean.get();
		HolderAccountBalance holderAccountBalance= getHolderAccountCorporative(corpOperation.getIdCorporativeOperationPk());
		if(Validations.validateIsNotNull(holderAccountBalance)){
			 List<HolderAccountBalanceTO> sourceAccountBalanceTOs = new  ArrayList<HolderAccountBalanceTO>();
			 List<HolderAccountBalanceTO> targetAccountBalanceTOs = new  ArrayList<HolderAccountBalanceTO>();
			 HolderAccountBalanceTO account = new HolderAccountBalanceTO();
			 account.setIdHolderAccount(holderAccountBalance.getHolderAccount().getIdHolderAccountPk());
			 account.setIdParticipant(holderAccountBalance.getParticipant().getIdParticipantPk());
			 account.setIdSecurityCode(holderAccountBalance.getSecurity().getIdSecurityCodePk());
			 account.setStockQuantity(BigDecimal.ONE);
			 List<MarketFactAccountTO> lstMarketFactAccounts = getListMarketFactAccounts(
						account.getIdHolderAccount(), account.getIdParticipant(),
						account.getIdSecurityCode(),CorporateProcessConstants.BL_REVERSE_AVAILABLE);
			 account.setLstMarketFactAccounts(lstMarketFactAccounts);
		     sourceAccountBalanceTOs.add(account);
		     AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
			 objAccountComponent.setIdBusinessProcess(BusinessProcessType.REVERSE_PAYMENT_INTEREST_COUPON_RPC.getCode());
			 objAccountComponent.setIdCorporativeOperation(corpOperation.getIdCorporativeOperationPk());
			 objAccountComponent.setLstSourceAccounts(sourceAccountBalanceTOs);
			 objAccountComponent.setLstTargetAccounts(targetAccountBalanceTOs);
			 objAccountComponent.setIdOperationType(CorporateProcessUtils.getOperationType(processType, CorporateProcessConstants.BL_AVAILABLE));
			 objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
			
			 accountsComponentService.executeAccountsComponent(objAccountComponent);
		}
	}
	
	/**
	 * Execute Security Component For Corporative DPF.
	 *
	 * @param corpOperation object corporative operation
	 * @param loggerUser data user
	 * @param processType process type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeSecurityComponentForCorporativeDPF(CorporativeOperation corpOperation,LoggerUser loggerUser,
			ImportanceEventType processType)throws ServiceException{
		SecuritiesComponentService securitiesComponentService = securitiesComponentServiceBean.get();
		BigDecimal totalBalance =BigDecimal.ONE;
		SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, 
															totalBalance.multiply(corpOperation.getSecurities().getCurrentNominalValue()),
															totalBalance, corpOperation.getSecurities().getIdSecurityCodePk());
		secOpe.setSecurities(corpOperation.getSecurities());
		corporateExecutorService.create(secOpe, loggerUser);	
		securitiesComponentService.executeSecuritiesComponent(CorporateProcessUtils.getSecuritiesComponent(processType, 
													secOpe, null, totalBalance, null));
//		corporateExecutorService.updateSecurity(corpOperation.getSecurities().getIdSecurityCodePk(), null, 
//													SecurityStateType.EXPIRED.getCode(), null, null,null,loggerUser);
//		corporateExecutorService.updateIssuance(corpOperation.getSecurities().getIssuance().getIdIssuanceCodePk(), IssuanceStateType.EXPIRED.getCode(), loggerUser);
//		Issuance issuance = corporateExecutorService.findIssuance(corpOperation.getSecurities().getIdSecurityCodePk());
//		issuance.setStateIssuance(IssuanceStateType.EXPIRED.getCode());
//		corporateExecutorService.update(issuance, loggerUser);
		
		//SAVE SECURITY HISTORY BALANCE
		SecurityHistoryBalance securityHistoryBalance = new SecurityHistoryBalance();
		Security securityAux = corporateExecutorService.find(Security.class, corpOperation.getSecurities().getIdSecurityCodePk());

		securityHistoryBalance.setSecurity(securityAux);

		securityHistoryBalance.setUpdateBalanceDate(CommonsUtilities.currentDateTime());
		securityHistoryBalance.setNominalValue(securityAux.getCurrentNominalValue());
		securityHistoryBalance.setShareCapital(securityAux.getShareCapital());
		securityHistoryBalance.setShareBalance(securityAux.getShareBalance());
		securityHistoryBalance.setPlacedAmount(securityAux.getPlacedAmount());
		securityHistoryBalance.setPlacedBalance(securityAux.getPlacedBalance());
		securityHistoryBalance.setPhysicalBalance(securityAux.getPhysicalBalance());
		securityHistoryBalance.setDesmaterializedBalance(securityAux.getDesmaterializedBalance());
		securityHistoryBalance.setAmortizationAmount(securityAux.getAmortizationAmount());
		corporateExecutorService.create(securityHistoryBalance, loggerUser);
	}
	
	/**
	 * Execute Security Component For Corporative DPF.
	 *
	 * @param corpOperation object corporative operation
	 * @param loggerUser data user
	 * @param processType process type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeReverseSecurityComponentForCorporativeDPF(CorporativeOperation corpOperation,LoggerUser loggerUser,
			ImportanceEventType processType)throws ServiceException{

		SecuritiesComponentService securitiesComponentService = securitiesComponentServiceBean.get();
		BigDecimal totalBalance =BigDecimal.ONE;
		
		SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, 
															totalBalance.multiply(corpOperation.getSecurities().getCurrentNominalValue()), 
															totalBalance, corpOperation.getSecurities().getIdSecurityCodePk());
		secOpe.setSecurities(corpOperation.getSecurities());
		corporateExecutorService.create(secOpe, loggerUser);
		
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();
		securitiesComponentTO.setDematerializedBalance(totalBalance);
		securitiesComponentTO.setCirculationBalance(totalBalance);
		securitiesComponentTO.setShareBalance(totalBalance);
		securitiesComponentTO.setIdBusinessProcess(BusinessProcessType.REVERSE_PAYMENT_INTEREST_COUPON_RPC.getCode());
		securitiesComponentTO.setIdSecurityCode(secOpe.getSecurities().getIdSecurityCodePk());
		securitiesComponentTO.setIdOperationType(secOpe.getOperationType());
		securitiesComponentTO.setIdSecuritiesOperation(secOpe.getIdSecuritiesOperationPk());
		
		securitiesComponentService.executeSecuritiesComponent(securitiesComponentTO);	
		
		Issuance issuance = corporateExecutorService.findIssuance(corpOperation.getSecurities().getIdSecurityCodePk());
		issuance.setStateIssuance(IssuanceStateType.AUTHORIZED.getCode());
		corporateExecutorService.update(issuance, loggerUser);

		//SAVE SECURITY HISTORY BALANCE
		SecurityHistoryBalance securityHistoryBalance = new SecurityHistoryBalance();
		Security securityAux = corporateExecutorService.find(Security.class, corpOperation.getSecurities().getIdSecurityCodePk());

		securityHistoryBalance.setSecurity(securityAux);

		securityHistoryBalance.setUpdateBalanceDate(CommonsUtilities.currentDateTime());
		securityHistoryBalance.setNominalValue(securityAux.getCurrentNominalValue());
		securityHistoryBalance.setShareCapital(securityAux.getShareCapital());
		securityHistoryBalance.setShareBalance(securityAux.getShareBalance());
		securityHistoryBalance.setPlacedAmount(securityAux.getPlacedAmount());
		securityHistoryBalance.setPlacedBalance(securityAux.getPlacedBalance());
		securityHistoryBalance.setPhysicalBalance(securityAux.getPhysicalBalance());
		securityHistoryBalance.setDesmaterializedBalance(securityAux.getDesmaterializedBalance());
		securityHistoryBalance.setAmortizationAmount(securityAux.getAmortizationAmount());
		corporateExecutorService.create(securityHistoryBalance, loggerUser);
	}
	
	/**
	 * Validate interest payment business.
	 *
	 * @param corporateInformationTO the corporate information to
	 * @param lstCorporativeOperations the lst corporative operations
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<RecordValidationType> validateInterestPaymentBusiness(CorporateInformationTO corporateInformationTO,
														List<CorporativeOperation> lstCorporativeOperations) throws Exception {
		List<RecordValidationType> lstRecordValidationType = new ArrayList<RecordValidationType>();
		InterestCouponPaymentTO interestCouponPaymentTO = new InterestCouponPaymentTO();
		List<CorporateInformationTO> lstCorporateInformationTOs = new ArrayList<CorporateInformationTO>();
		lstCorporateInformationTOs.add(corporateInformationTO);
		interestCouponPaymentTO.setLstCorporateInformationTO(lstCorporateInformationTOs);
		//OPERATION CODE
		if (!ComponentConstant.INTERFACE_INTEREST_PAYMENT_PCU.equalsIgnoreCase(corporateInformationTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					interestCouponPaymentTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			corporateInformationTO.setErrorCode(recordValidationType.getErrorCode());
			corporateInformationTO.setErrorDescription(recordValidationType.getErrorDescription());
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//PARTICIPANT
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(corporateInformationTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					interestCouponPaymentTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			corporateInformationTO.setErrorCode(recordValidationType.getErrorCode());
			corporateInformationTO.setErrorDescription(recordValidationType.getErrorDescription());
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			corporateInformationTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//EXISTENCE OPERATION
		List<InterfaceTransaction> lstInterfaceTransactions= null;
		if (Validations.validateIsNotNull(objParticipant)) {
			lstInterfaceTransactions= parameterServiceBean.getListInterfaceTransaction(objParticipant.getIdParticipantPk(), 
																				corporateInformationTO.getOperationNumber(), 
																				corporateInformationTO.getOperationCode());
			if (Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactions)) {
				RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
						interestCouponPaymentTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
				corporateInformationTO.setErrorCode(recordValidationType.getErrorCode());
				corporateInformationTO.setErrorDescription(recordValidationType.getErrorDescription());
				lstRecordValidationType.add(recordValidationType);
				return lstRecordValidationType;
			}
		}
		
		if(Validations.validateListIsNullOrEmpty(lstInterfaceTransactions)) {
			//SECURITY CLASS (DPF - DPA)
			boolean isDpfDpa= false;
			ParameterTableTO filter = new ParameterTableTO();
			filter.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
			for (ParameterTable parameterTable: securityClasses) {
				if (parameterTable.getText1().equalsIgnoreCase(corporateInformationTO.getSecurityObjectTO().getSecurityClassDesc())) {
					isDpfDpa= true;
					break;
				}
			}
			if (!isDpfDpa) {
				RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
						interestCouponPaymentTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
				corporateInformationTO.setErrorCode(recordValidationType.getErrorCode());
				corporateInformationTO.setErrorDescription(recordValidationType.getErrorDescription());
				lstRecordValidationType.add(recordValidationType);
				return lstRecordValidationType;
			}
			
			//SECURITY CODE
			SecurityObjectTO securityObjectTO = corporateInformationTO.getSecurityObjectTO();
			String idSecurityCode = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
			Security objSecurity = find(Security.class, idSecurityCode);
			corporateInformationTO.getSecurityObjectTO().setIdSecurityCode(idSecurityCode);
			if (Validations.validateIsNull(objSecurity)) {
				RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
						interestCouponPaymentTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
				corporateInformationTO.setErrorCode(recordValidationType.getErrorCode());
				corporateInformationTO.setErrorDescription(recordValidationType.getErrorDescription());
				lstRecordValidationType.add(recordValidationType);
				return lstRecordValidationType;
			}						
					
			//COUPON NUMBER
			List<CorporativeOperation> lstProcesses = new ArrayList<CorporativeOperation>();
			if(Validations.validateIsNotNull(objSecurity)) {
				for (CouponObjectTO objCouponObjectTO: corporateInformationTO.getSecurityObjectTO().getLstCouponObjectTOs()) {
					Integer couponNumber = objCouponObjectTO.getCouponNumber();
					Date expirationDate= objCouponObjectTO.getExpirationDate();
					corporateInformationTO.getSecurityObjectTO().setCouponNumberRef(couponNumber);
					List<CorporativeOperation> lstCouponProcesses = getListCorporativeOperationInterest(idSecurityCode, couponNumber, expirationDate); 
					if (Validations.validateListIsNullOrEmpty(lstCouponProcesses)) {
						RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
								interestCouponPaymentTO, GenericPropertiesConstants.MSG_INTERFACE_COUPON_NUMBER_NOT_EXISTS, null);
						corporateInformationTO.setErrorCode(recordValidationType.getErrorCode());
						corporateInformationTO.setErrorDescription(recordValidationType.getErrorDescription());
						lstRecordValidationType.add(recordValidationType);
						return lstRecordValidationType;
					}
					lstProcesses.addAll(lstCouponProcesses);
				}
			}
			
			//EXPIRATION DATE COUPON
			if(Validations.validateListIsNotNullAndNotEmpty(lstProcesses)) {
				for (CorporativeOperation corporativeOperationTemp: lstProcesses) {
//					objCorporativeOperation.setIdCorporativeOperationPk(corporativeOperationTemp.getIdCorporativeOperationPk());
//					objCorporativeOperation.setProgramInterestCoupon(corporativeOperationTemp.getProgramInterestCoupon());
					ProgramInterestCoupon programInterestCoupon= corporativeOperationTemp.getProgramInterestCoupon();
					if (programInterestCoupon.getExperitationDate().compareTo(CommonsUtilities.currentDate()) > 0) {
						RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
								interestCouponPaymentTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE, null);
						corporateInformationTO.setErrorCode(recordValidationType.getErrorCode());
						corporateInformationTO.setErrorDescription(recordValidationType.getErrorDescription());
						lstRecordValidationType.add(recordValidationType);
						return lstRecordValidationType;
					}
				}
				lstCorporativeOperations.addAll(lstProcesses);
			}
			
			//COUPON ALREADY PAYED
			if (Validations.validateListIsNotNullAndNotEmpty(lstProcesses)) { 
				for (CorporativeOperation corporativeOperationTemp: lstProcesses) {
					if (CorporateProcessStateType.DEFINITIVE.getCode().equals(corporativeOperationTemp.getState()) && 
							(CorporativeSituationType.ISSUER_PAID.getCode().equals(corporativeOperationTemp.getSituation()) || 
									CorporativeSituationType.PAID.getCode().equals(corporativeOperationTemp.getSituation())) ) {
						RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
								interestCouponPaymentTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_ALREADY_PAYED, null);
						corporateInformationTO.setErrorCode(recordValidationType.getErrorCode());
						corporateInformationTO.setErrorDescription(recordValidationType.getErrorDescription());
						lstRecordValidationType.add(recordValidationType);
						return lstRecordValidationType;
					}
				}
			}
		}
						
		return lstRecordValidationType;
	}
	
	
	
	/**
	 * Validate reverse interest payment business.
	 *
	 * @param reverseRegisterTO the reverse register to
	 * @param objCorporativeOperation the obj corporative operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> validateReverseInterestPaymentBusiness(ReverseRegisterTO reverseRegisterTO,
													CorporativeOperation objCorporativeOperation) throws ServiceException {
		List<RecordValidationType> lstRecordValidationType= new ArrayList<RecordValidationType>();
		//OPERATION CODE
		if (!ComponentConstant.INTERFACE_REVERSE_INTEREST_PAYMENT_RPC.equalsIgnoreCase(reverseRegisterTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//PARTICIPANT
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(reverseRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			reverseRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//EXISTENCE ORGINAL OPERATION
		List<InterfaceTransaction> lstInterfaceTransactions= null;
		if (Validations.validateIsNotNull(objParticipant)) {
			lstInterfaceTransactions= parameterServiceBean.getListInterfaceTransaction(objParticipant.getIdParticipantPk(), 
																				new Long(reverseRegisterTO.getReverseOperationNumber()), 
																				ComponentConstant.INTERFACE_INTEREST_PAYMENT_PCU);
			if (Validations.validateListIsNullOrEmpty(lstInterfaceTransactions)) {
				RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
						reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_PAYMENT_DATE, null);
				lstRecordValidationType.add(recordValidationType);
				return lstRecordValidationType;
			}
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactions)) {
			//SECURITY CLASS (DPF - DPA)
			boolean isDpfDpa= false;
			ParameterTableTO filter = new ParameterTableTO();
			filter.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
			for (ParameterTable parameterTable: securityClasses) {
				if (parameterTable.getText1().equalsIgnoreCase(reverseRegisterTO.getSecurityObjectTO().getSecurityClassDesc())) {
					isDpfDpa= true;
					break;
				}
			}
			if (!isDpfDpa) {
				RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
						reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
				lstRecordValidationType.add(recordValidationType);
				return lstRecordValidationType;
			}
			
			//SECURITY CODE
			SecurityObjectTO securityObjectTO = reverseRegisterTO.getSecurityObjectTO();
			String idSecurityCode = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
			Security objSecurity = find(Security.class, idSecurityCode);
			reverseRegisterTO.getSecurityObjectTO().setIdSecurityCode(idSecurityCode);
			if (Validations.validateIsNull(objSecurity)) {
				RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
						reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
				lstRecordValidationType.add(recordValidationType);
				return lstRecordValidationType;
			}
			
			if(!reverseRegisterTO.getOperationCode().equals("RPC")){ //issue 158
				//HOLDER ACCOUNT BALANCE
				if (Validations.validateIsNotNull(objParticipant) && Validations.validateIsNotNull(objSecurity)) {
					List<HolderAccountBalance> lstHolderAccountBalances= participantServiceBean.getListHolderAccountBalance(
																					objParticipant.getIdParticipantPk(), objSecurity.getIdSecurityCodePk(), false);
					if (Validations.validateListIsNullOrEmpty(lstHolderAccountBalances)) {
						RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
								reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT, null);
						lstRecordValidationType.add(recordValidationType);
						return lstRecordValidationType;
					}
				}
			}
			
			//COUPON NUMBER
			List<CorporativeOperation> lstProcesses = new ArrayList<CorporativeOperation>();
			if(Validations.validateIsNotNull(objSecurity)) {
				
				if(reverseRegisterTO.getSecurityObjectTO().getLstCouponObjectTOs().size() > 1){
					RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
							reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_REVERSE_SIZE_ONE, null);
					lstRecordValidationType.add(recordValidationType);
					return lstRecordValidationType;
				}
				
				for (CouponObjectTO objCouponObjectTO: reverseRegisterTO.getSecurityObjectTO().getLstCouponObjectTOs()) {
					Integer couponNumber = objCouponObjectTO.getCouponNumber();
					Date expirationDate = objCouponObjectTO.getExpirationDate();
					reverseRegisterTO.getSecurityObjectTO().setCouponNumberRef(couponNumber);
					
					CorporativeOperation objCorpOperValidate = getCorporativeOperationInterestLast(idSecurityCode);
					
					if (Validations.validateIsNullOrEmpty(objCorpOperValidate)) {
						RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
								reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_COUPON_NUMBER_NOT_EXISTS, null);
						lstRecordValidationType.add(recordValidationType);
						return lstRecordValidationType;
					}
					
					List<CorporativeOperation> lstProcessesTemp = new ArrayList<CorporativeOperation>();
					lstProcessesTemp.add(objCorpOperValidate);
//					if(expirationDate.compareTo(objCorpOperValidate.getProgramInterestCoupon().getExperitationDate()) == 0 && 
//							couponNumber.equals(objCorpOperValidate.getProgramInterestCoupon().getCouponNumber())){						
//						lstProcessesTemp.add(objCorpOperValidate);
//					} 
//					else {
//						RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
//								reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER_NOT_IS_LAST, null);
//						lstRecordValidationType.add(recordValidationType);
//						return lstRecordValidationType;
//					}		
					
					lstProcesses.addAll(lstProcessesTemp);
				}
			}
			
			//EXPIRATION DATE COUPON
			if(Validations.validateListIsNotNullAndNotEmpty(lstProcesses)) {
				for (CorporativeOperation corporativeOperationTemp: lstProcesses) {
					objCorporativeOperation.setIdCorporativeOperationPk(corporativeOperationTemp.getIdCorporativeOperationPk());
					objCorporativeOperation.setProgramInterestCoupon(corporativeOperationTemp.getProgramInterestCoupon());
					if (!wasPayedInterestCoupon(CommonsUtilities.currentDate(), corporativeOperationTemp.getIdCorporativeOperationPk())) {
						RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
								reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_PAYMENT_DATE, null);
						lstRecordValidationType.add(recordValidationType);
						return lstRecordValidationType;
					}
				}
			}
			
			//COUPON ALREADY REVERSED
			if (Validations.validateListIsNotNullAndNotEmpty(lstProcesses)) {
				for (CorporativeOperation corporativeOperationTemp: lstProcesses) {
					if (CorporateProcessStateType.REGISTERED.getCode().equals(corporativeOperationTemp.getState()) ||
						CorporateProcessStateType.ANNULLED.getCode().equals(corporativeOperationTemp.getState()) ||
						CorporateProcessStateType.MODIFIED.getCode().equals(corporativeOperationTemp.getState())) {
						RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
								reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INTEREST_ALREADY_REVERSED, null);
						lstRecordValidationType.add(recordValidationType);
						return lstRecordValidationType;
					}
				}
			}
		}
			
		return lstRecordValidationType;
	}
	
	/**
	 * Validate interest payment query information.
	 *
	 * @param interestPaymentQueryTO the interest payment query to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> validateInterestPaymentQueryInformation(InterestPaymentQueryTO interestPaymentQueryTO) throws ServiceException {
		List<RecordValidationType> lstRecordValidationType= new ArrayList<RecordValidationType>();
		//OPERATION CODE
		if (!ComponentConstant.INTERFACE_INTEREST_PAYMENT_QUERY.equalsIgnoreCase(interestPaymentQueryTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					interestPaymentQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//PARTICIPANT
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(interestPaymentQueryTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					interestPaymentQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			interestPaymentQueryTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//SECURITY CLASS (DPF - DPA)
		boolean isDpfDpa= false;
		ParameterTableTO filter = new ParameterTableTO();
		filter.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		for (ParameterTable parameterTable: securityClasses) {
			if (parameterTable.getText1().equalsIgnoreCase(interestPaymentQueryTO.getSecurityClassDesc())) {
				isDpfDpa= true;
				interestPaymentQueryTO.setSecurityClass(parameterTable.getParameterTablePk());
				break;
			}
		}
		if (!isDpfDpa) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					interestPaymentQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
				
		return lstRecordValidationType;
	}
	
	
	/**
	 * Gets the interest payment query to.
	 *
	 * @param interestPaymentQueryTO the interest payment query to
	 * @return the interest payment query to
	 */
	public InterestPaymentQueryTO getInterestPaymentQueryTO(InterestPaymentQueryTO interestPaymentQueryTO) {
		List<CouponObjectTO> lstCouponObjectTO= new ArrayList<CouponObjectTO>();
		List<Object[]> lstInterestPayment= getListDpfInterestPayment(interestPaymentQueryTO.getOperationDate(), 
																	interestPaymentQueryTO.getIdParticipant(),
																	interestPaymentQueryTO.getSecurityClass());
		if (Validations.validateListIsNotNullAndNotEmpty(lstInterestPayment)) {
			for (Object[] objInterestPayment: lstInterestPayment) {
				Long operationNumber= new Long(objInterestPayment[0].toString());
				CorporativeOperation corporativeOperation= (CorporativeOperation) objInterestPayment[1];
				CouponObjectTO couponObjectTO= populateCouponObjectTO(corporativeOperation);
				couponObjectTO.setOperationNumber(operationNumber);
				lstCouponObjectTO.add(couponObjectTO);
			}
			interestPaymentQueryTO.setLstCouponObjectTO(lstCouponObjectTO);
		}
		return interestPaymentQueryTO;
	}
	
	
	/**
	 * Gets the list dpf interest payment.
	 *
	 * @param operationDate the operation date
	 * @param idParticipant the id participant
	 * @param securityClass the security class
	 * @return the list dpf interest payment
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListDpfInterestPayment(Date operationDate, Long idParticipant, Integer securityClass) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT.operationNumber, ");
		stringBuffer.append(" CO ");
		stringBuffer.append(" FROM InterfaceTransaction IT, CorporativeOperation CO ");
		stringBuffer.append(" INNER JOIN FETCH CO.securities ");
		stringBuffer.append(" INNER JOIN FETCH CO.programInterestCoupon ");
		stringBuffer.append(" WHERE CO.idCorporativeOperationPk = IT.corporativeOperation.idCorporativeOperationPk ");
		stringBuffer.append(" and trunc(IT.interfaceProcess.processDate) = trunc(:operationDate) ");
		stringBuffer.append(" and IT.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and IT.transactionState = :transactionState ");
		stringBuffer.append(" and CO.securities.securityClass = :securityClass ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("operationDate", operationDate);
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("transactionState", BooleanType.YES.getCode());
		query.setParameter("securityClass", securityClass);
		
		return query.getResultList();
	}
	
	
	/**
	 * Populate coupon object to.
	 *
	 * @param corporativeOperation the corporative operation
	 * @return the coupon object to
	 */
	public CouponObjectTO populateCouponObjectTO(CorporativeOperation corporativeOperation) {
		CouponObjectTO couponObjectTO= new CouponObjectTO();
		couponObjectTO.setSecuritySerial(corporativeOperation.getSecurities().getIdSecurityCodeOnly());
		couponObjectTO.setCouponNumber(corporativeOperation.getProgramInterestCoupon().getCouponNumber());
		couponObjectTO.setExpirationDate(corporativeOperation.getProgramInterestCoupon().getExperitationDate());
		return couponObjectTO;
	}
	
	/**
	 * Pay dpf interest coupon.
	 *
	 * @param corporateInformationTO the corporate information to
	 * @param objCorporativeOperation the obj corporative operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void payDpfInterestCoupon(CorporateInformationTO corporateInformationTO, CorporativeOperation objCorporativeOperation, 
																						LoggerUser loggerUser) throws ServiceException {
//		List<String> lstExpiredIssuances= new ArrayList<String>();
		
		//verificamos si es ultimo cupon de interes
//		boolean LastInterestCoupon = isLastInterestCoupon(corporateInformationTO.getSecurityObjectTO().getIdSecurityCode(),false);
		//Actualizamos estado y situacion del corporativo de interes 
		updateCorporativeOperationState(objCorporativeOperation.getIdCorporativeOperationPk(),
																CorporateProcessStateType.DEFINITIVE.getCode(), loggerUser, false,
																CorporativeSituationType.ISSUER_PAID.getCode());
		//Actualizamos estado y situacion del cupon de interes
		updateProgramInterestCoupon(objCorporativeOperation.getProgramInterestCoupon().getIdProgramInterestPk(),
															ProgramScheduleStateType.EXPIRED.getCode() , 
															ProgramScheduleSituationType.ISSUER_PAID.getCode(), loggerUser);
		//si es el ultimo de cupon de interes entonces amortizamos
//		if(LastInterestCoupon){
//			// obtenemos corporativo de amortizacion asociado al valor
//			List<CorporativeOperation> lstProcessesAux = getListCorporativeOperationAmortization(
//																	corporateInformationTO.getSecurityObjectTO().getIdSecurityCode(),false);
//			if(Validations.validateListIsNotNullAndNotEmpty(lstProcessesAux)){
//				// obtenemos corporativo de amortizacion asociado al valor
//				CorporativeOperation objCorporativeOperationAux = lstProcessesAux.get(0);
//				//verificamos si el valor tiene cuenta matriz con saldos bloqueados o de reporto
//				boolean verifyExistBalanceAll = verifyExistBalanceAll(corporateInformationTO.getSecurityObjectTO().getIdSecurityCode(), false);
//				if(!verifyExistBalanceAll){					
//					//generamos de movimientos de saldos a nivel de cuenta matriz
//					ImportanceEventType processType = ImportanceEventType.get(objCorporativeOperationAux.getCorporativeEventType().getCorporativeEventType());					
//					
//					executeAccountsComponentForCorporativeDPF(objCorporativeOperationAux,loggerUser, processType, false);
//					//generamos de movimientos de saldos a nivel de valor
//					executeSecurityComponentForCorporativeDPF(objCorporativeOperationAux, loggerUser, processType);
//					// actualizamos estado del valor
//					corporateExecutorService.updateSecurity(objCorporativeOperationAux.getSecurities().getIdSecurityCodePk(), 
//															objCorporativeOperationAux.getDeliveryDate(), 
//															SecurityStateType.EXPIRED.getCode(), null, null ,null, loggerUser);
//					//actualizamos estado de al emision
//					lstExpiredIssuances.add(objCorporativeOperationAux.getSecurities().getIssuance().getIdIssuanceCodePk());
//					corporateExecutorService.updateIssuance(lstExpiredIssuances, IssuanceStateType.EXPIRED.getCode(), loggerUser);
//					
//					//Actualizamos estado y situacion del corporativo de Amortizacion 
//					updateCorporativeOperationState(objCorporativeOperationAux.getIdCorporativeOperationPk(),
//							CorporateProcessStateType.DEFINITIVE.getCode(), loggerUser, false, CorporativeSituationType.ISSUER_PAID.getCode());
//					//Actualizamos estado y situacion del cupon de Amortizacion
//					updateProgramAmortizationCoupon(objCorporativeOperationAux.getProgramAmortizationCoupon().getIdProgramAmortizationPk(),
//																			ProgramScheduleStateType.EXPIRED.getCode() , 
//																			ProgramScheduleSituationType.ISSUER_PAID.getCode(), loggerUser);
//				}else{
//					//actualizamos estado del valor
//					corporateExecutorService.updateSecurity(objCorporativeOperationAux.getSecurities().getIdSecurityCodePk(), null, 
//							SecurityStateType.SUSPENDED.getCode(), null, null ,null, loggerUser);
//				}
//			}				
//		}
	}
	
	/**
	 * Gets the corporative operation interest last.
	 *
	 * @param idSecurityCode the id security code
	 * @return the corporative operation interest last
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public CorporativeOperation getCorporativeOperationInterestLast(String idSecurityCode)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT co ");
			sbQuery.append("	FROM CorporativeOperation co");
			sbQuery.append(" 	inner join fetch co.programInterestCoupon pic ");
			sbQuery.append(" 	inner join co.securities se ");
			sbQuery.append(" 	inner join co.corporativeEventType cet ");
			sbQuery.append("	Where 1 = 1	");
			sbQuery.append("	and se.idSecurityCodePk = :idSecurityCodePk");	
			sbQuery.append("	and pic.situationProgramInterest = :situation");	
			sbQuery.append("	order by pic.couponNumber desc ");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCode);		
			query.setParameter("situation", ProgramScheduleSituationType.ISSUER_PAID.getCode());	
			List<CorporativeOperation> lstCorporativeOperation = (List<CorporativeOperation>) query.getResultList();
			if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)){
				return lstCorporativeOperation.get(0);
			} else {
				return null;
			}			
		} catch(NonUniqueResultException ex){
			return null;
		}
	}
	
		/**
		 * Automatic expired interest coupon dpf.
		 * Vence el cupon de interes de un valor DPF o DPA
		 * @param currentDate the final date
		 * @param objLoggerUser the obj logger user
		 * @param lstSecurity the lst security
		 * @throws ServiceException the service exception
		 */
	public void automaticExpiredSecuritiesDpfInterestCoupon(String finalDate, LoggerUser objLoggerUser) throws ServiceException{
			
			log.info(":::: Inicio Vemcimiento de Cupones de Valores  DPF y DPA ::::");
			List<ProgramInterestCoupon> listProgramInt =  new ArrayList<ProgramInterestCoupon>() ;
			listProgramInt = corporateExecutorService.findSecuritiesExpiredDpfInterestCoupon(finalDate);

			if (listProgramInt.size()>0){
				for(ProgramInterestCoupon programInterestCoupon : listProgramInt){
					log.info(":::: Venciendo el cupon con pk  :::: " + programInterestCoupon.getIdProgramInterestPk());
					updateSecurityDpfCopuponExpired(programInterestCoupon,objLoggerUser);
				}
			}
			log.info(":::: Fin Vemcimiento de Cupones de Valores  DPF y DPA ::::");
	}
	
	/**
	 * Update security dpf coupon interest expired.
	 *
	 * @param idSecurityPk the id security pk
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityDpfCopuponExpired(ProgramInterestCoupon programInterestCoupon, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update ProgramInterestCoupon pic set stateProgramInterest = :stateExpired,  "+		
				" pic.lastModifyUser = :parLastModifyUser, pic.lastModifyDate = :parLastModifyDate, " +
				" pic.lastModifyIp = :parLastModifyIp, pic.lastModifyApp = :parLastModifyApp " +
				" where pic.idProgramInterestPk = :idProgramInterestCoupon ");
		parameters.put("idProgramInterestCoupon", programInterestCoupon.getIdProgramInterestPk());		
		parameters.put("stateExpired", ProgramScheduleStateType.EXPIRED.getCode());		
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	//@TransactionTimeout(unit=TimeUnit.MINUTES,value=30)
	/**
	 * Automatic expired securities dpf.
	 *
	 * @param finalDate the final date
	 * @param objLoggerUser the obj logger user
	 * @param lstSecurity the lst security
	 * @throws ServiceException the service exception
	 */
	synchronized public void automaticExpiredSecuritiesDpf(Date finalDate, LoggerUser objLoggerUser, List<String> lstSecurity) throws ServiceException{
		log.info(":::: Inicio Vemcimiento de Valores ::::");
		List<String> lstExpiredSecurities= new ArrayList<String>();
		List<String> lstSuspendedSecurities= new ArrayList<String>();
		List<Long> lstCorporativeOperation= new ArrayList<Long>();
		List<Long> lstProgramAmortizationCoupon= new ArrayList<Long>();
		List<String> lstExpiredIssuances= new ArrayList<String>();
		List<String> lstSuspendedIssuances= new ArrayList<String>();
		
		List <HolderAccountBalanceExp> holderAccountBalanceExps = new ArrayList<>();
		
		SecurityTO objSecurityTO = new SecurityTO();		
		if(Validations.validateListIsNullOrEmpty(lstSecurity)){
			
			objSecurityTO.setExpirationDate(finalDate);
			objSecurityTO.setStateSecurity(SecurityStateType.REGISTERED.getCode());
			
			// Validate balance acreditation
			
			List<String> lstSecurityAcreditation = corporateExecutorService.validateBalanceAcreditation(objSecurityTO);		
			String strSecurities =  GeneralConstants.EMPTY_STRING;
			if(Validations.validateListIsNotNullAndNotEmpty(lstSecurityAcreditation)) {
				for(String securitiesList : lstSecurityAcreditation){
					if(GeneralConstants.EMPTY_STRING.compareTo(strSecurities) == 0){
						strSecurities = securitiesList;
					} else {
						strSecurities = strSecurities + GeneralConstants.STR_COMMA_WITHOUT_SPACE + securitiesList;
					}					
				}				
				throw new ServiceException(ErrorServiceType.ERROR_EXITS_OPERATIONS_ACREDITATIONS, new Object[]{strSecurities});
			}
						
			lstSecurity = corporateExecutorService.findSecuritiesExpiredDpf(objSecurityTO);
		} 
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstSecurity)){
			
			// issue 1322: split para procesar de 1000 en 1000 los vencimientos de DPFs
			List<String> lstSplit;
			int tamList = lstSecurity.size();
			log.info("Tamano de la Lista Original: " + tamList+ " DPFs a vencer");
			int nroPartition = 0;
			int indexInitial = 0;
			int indexFinal = 0;
			boolean continuar = true;
			while (continuar) {
				lstSplit = new ArrayList<>();
				
				holderAccountBalanceExps = new ArrayList<>();
				lstExpiredSecurities = new ArrayList<>();
				lstSuspendedSecurities = new ArrayList<>();
				lstProgramAmortizationCoupon = new ArrayList<>();
				lstCorporativeOperation = new ArrayList<>();
				lstExpiredIssuances = new ArrayList<>();
				lstSuspendedIssuances = new ArrayList<>();

				if (tamList > MAXIMUM_RANGE) {
					// actualizando el indice final
					indexFinal += MAXIMUM_RANGE;
				} else {
					// indicando se sera la ultima iteracion en el while (ultimo split)
					indexFinal = lstSecurity.size();
					continuar = false;
				}
				
				log.info("::: Particion nro: " + (++nroPartition));
				log.info("::: indice inicial: " + indexInitial);
				log.info("::: indice final: " + indexFinal);
				// llenando la lista particionada
				for (int i = indexInitial; i < indexFinal; i++) {
					lstSplit.add(lstSecurity.get(i));
				}
				
				// procesando la lista particionada lstSplit
				int count = 0;
				int countProcess = 0;
				for(String idSecurityCode: lstSplit){
					log.info(":::: Vencimiento de Valores ::::");
					countProcess = count++;
					//List<CorporativeOperation> lstProcessesAux = getListCorporativeOperationAmortization(idSecurityCode, false);
					List<Object[]> lstProcessAux = getListCorporativeAmortization(idSecurityCode, false);
					if(Validations.validateListIsNotNullAndNotEmpty(lstProcessAux)){
						// obtenemos corporativo de amortizacion asociado al valor
						Object[] arrCorporativeAmortization= lstProcessAux.get(0);
						CorporativeOperation objCorporativeOperationAux= new CorporativeOperation();
						objCorporativeOperationAux.setIdCorporativeOperationPk(new Long(arrCorporativeAmortization[0].toString()));
						objCorporativeOperationAux.setCorporativeEventType(new CorporativeEventType());
						objCorporativeOperationAux.getCorporativeEventType().setIdCorporativeEventTypePk(new Long(arrCorporativeAmortization[1].toString()));
						objCorporativeOperationAux.getCorporativeEventType().setCorporativeEventType(new Integer(arrCorporativeAmortization[2].toString()));
						objCorporativeOperationAux.setSecurities(new Security(arrCorporativeAmortization[3].toString()));
						objCorporativeOperationAux.getSecurities().setCurrentNominalValue(new BigDecimal(arrCorporativeAmortization[4].toString()));
						objCorporativeOperationAux.setDeliveryDate((Date) arrCorporativeAmortization[5]);
						objCorporativeOperationAux.setProgramAmortizationCoupon(new ProgramAmortizationCoupon());
						objCorporativeOperationAux.getProgramAmortizationCoupon().setIdProgramAmortizationPk(new Long(arrCorporativeAmortization[6].toString()));
						objCorporativeOperationAux.getSecurities().setIssuance(new Issuance(arrCorporativeAmortization[7].toString()));
						
						//verificamos si el valor tiene cuenta matriz con saldos bloqueados o de reporto
						boolean verifyExistBalanceAll = verifyExistBalanceAll(idSecurityCode, false);
						if(!verifyExistBalanceAll){	

							//agregamos a la lista para almacenar los saldos
							holderAccountBalanceExps.add(registerHolderAccExp(idSecurityCode));
							
							//generamos de movimientos de saldos a nivel de cuenta matriz
							ImportanceEventType processType = ImportanceEventType.get(objCorporativeOperationAux.getCorporativeEventType().getCorporativeEventType());					
							
							executeAccountsComponentForCorporativeDPF(objCorporativeOperationAux, objLoggerUser, processType, false);
							//generamos de movimientos de saldos a nivel de valor
							executeSecurityComponentForCorporativeDPF(objCorporativeOperationAux, objLoggerUser, processType);
							
							// actualizamos estado del valor
							lstExpiredSecurities.add(idSecurityCode);
							lstExpiredIssuances.add(objCorporativeOperationAux.getSecurities().getIssuance().getIdIssuanceCodePk());
							
							//Actualizamos estado y situacion del corporativo de Amortizacion
							lstCorporativeOperation.add(objCorporativeOperationAux.getIdCorporativeOperationPk());

							//Actualizamos estado y situacion del cupon de Amortizacion
							lstProgramAmortizationCoupon.add(objCorporativeOperationAux.getProgramAmortizationCoupon().getIdProgramAmortizationPk());
						}else{
							//agregamos a la lista para actualizar el estado del valor a Suspendido
							lstSuspendedSecurities.add(idSecurityCode);
							//agregamos a la lista para actualizar el estado de la emision a Suspendido
							lstSuspendedIssuances.add(objCorporativeOperationAux.getSecurities().getIssuance().getIdIssuanceCodePk());
							
							//agregamos a la lista para almacenar los saldos
							holderAccountBalanceExps.add(registerHolderAccExp(idSecurityCode));
						}
					} else {
						updateSecurityDpfExpired(idSecurityCode, objLoggerUser);
						
						//agregamos a la lista para almacenar los saldos
						holderAccountBalanceExps.add(registerHolderAccExp(idSecurityCode));
					}
					log.info(":::: Valor :::: " + idSecurityCode);
					log.info(":::: Cantidad Vencida CANTVENC :::: " + countProcess);
				}
				
				//registramos los saldos antes del vencimiento  //issue 569
				if (Validations.validateListIsNotNullAndNotEmpty(holderAccountBalanceExps)) {
					for(HolderAccountBalanceExp holderAccountBalanceExp : holderAccountBalanceExps){
						corporateExecutorService.create(holderAccountBalanceExp);
					}
				}
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstExpiredSecurities)) {
					corporateExecutorService.updateSecurity(lstExpiredSecurities, CommonsUtilities.currentDate(), SecurityStateType.EXPIRED.getCode(), 
															BigDecimal.ZERO, null, null, objLoggerUser);
				}
				if (Validations.validateListIsNotNullAndNotEmpty(lstSuspendedSecurities)) {
					corporateExecutorService.updateSecurity(lstSuspendedSecurities, CommonsUtilities.currentDate(), SecurityStateType.SUSPENDED.getCode(), 
							null, null, null, objLoggerUser);
				}
				if (Validations.validateListIsNotNullAndNotEmpty(lstProgramAmortizationCoupon)) {
					updateProgramAmortizationCoupon(lstProgramAmortizationCoupon, ProgramScheduleStateType.EXPIRED.getCode(), 
													ProgramScheduleSituationType.PENDING.getCode(), objLoggerUser);
				}
				if (Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)) {
					updateCorporativeOperationState(lstCorporativeOperation, CorporateProcessStateType.DEFINITIVE.getCode(), objLoggerUser,false, true);
				}
				if (Validations.validateListIsNotNullAndNotEmpty(lstExpiredIssuances)) {
					corporateExecutorService.updateIssuance(lstExpiredIssuances, IssuanceStateType.EXPIRED.getCode(), objLoggerUser);
				}
				if (Validations.validateListIsNotNullAndNotEmpty(lstSuspendedIssuances)) {
					corporateExecutorService.updateIssuance(lstSuspendedIssuances, IssuanceStateType.SUSPENDED.getCode(), objLoggerUser);
				}
				
				
				// actulizando el tamano de la lista original
				tamList -= MAXIMUM_RANGE;
				log.info("::: tamano restante: " + tamList + " DPFs");
				// actualizando el indice inicial
				indexInitial = indexFinal;
			}
			//FIN WHILE
		}				
		log.info(":::: Fin Vencimiento de Valores ::::");
		
	}
	
	/**
	 * Metodo para registrar un saldo de DPF en HolderAccountBalanceExp
	 * @param idSecurityCode
	 */
	public HolderAccountBalanceExp registerHolderAccExp (String idSecurityCode){
		
		//Registramos el saldo actual en HolderAccountBalanceExp
		HolderAccountBalanceExp accountBalanceExp = new HolderAccountBalanceExp();
		HolderAccountBalance accountBalance = new HolderAccountBalance();
		
		accountBalance = extractExistBalanceAll(idSecurityCode);
		
		accountBalanceExp.setHolderAccount(accountBalance.getHolderAccount().getIdHolderAccountPk());
		accountBalanceExp.setSecurity(accountBalance.getSecurity().getIdSecurityCodePk());
		accountBalanceExp.setParticipant(accountBalance.getParticipant().getIdParticipantPk());
		accountBalanceExp.setTotalBalance(accountBalance.getTotalBalance());
		
		//si esta bloqueado por cat lo guardamos como Disponible
		if(accountBalance.getAccreditationBalance().compareTo(GeneralConstants.ZERO_VALUE_BIGDECIMAL) > GeneralConstants.ZERO_VALUE_INTEGER){
			accountBalanceExp.setAvailableBalance(accountBalance.getAccreditationBalance());
		}else{
			accountBalanceExp.setAvailableBalance(accountBalance.getAvailableBalance());
		}
		accountBalanceExp.setBanBalance(accountBalance.getBanBalance());
		accountBalanceExp.setPawnBalance(accountBalance.getPawnBalance());
		accountBalanceExp.setOtherBlockBalance(accountBalance.getOtherBlockBalance());
		
		//registramos en la tabla
		return accountBalanceExp;
	}
	
	
	/**
	 * Update security dpf expired.
	 *
	 * @param idSecurityPk the id security pk
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityDpfExpired(String idSecurityPk, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update Security security set security.stateSecurity = :parState , "+				
				" security.lastModifyUser = :parLastModifyUser, security.lastModifyDate = :parLastModifyDate, " +
				" security.lastModifyIp = :parLastModifyIp, security.lastModifyApp = :parLastModifyApp " +
				" where security.idSecurityCodePk = :parIdSecurityCode ");
		parameters.put("parIdSecurityCode", idSecurityPk);		
		parameters.put("parState", SecurityStateType.EXPIRED.getCode());		
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	
	
	
	/**********************************/
	/**
	 * devuelve la primera lista de valores
	 */	
	public List<CorporativeOperation> getListSecurityToDefinitive() throws ServiceException {
		//filtro fecha de corte		
		CorporateMonitoringTO corporateFilter = new CorporateMonitoringTO();		
		List<Date> lstExpira = new ArrayList<>();		
		lstExpira.add(CommonsUtilities.currentDate());		
		corporateFilter.setListExpirationDate(lstExpira);				
		List<CorporativeOperation> lstCorporativeOp = corporateServiceBean.searchCorporativeOperationsByCoupons(corporateFilter);		
		return lstCorporativeOp;
		
	}
	
	/**
	 * obtiene el interes y amortizacion
	 */
	public List<Long> interestAmortizationCalculation(Long idCorporativeOperationPk) throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("  SELECT c.idCorporativeOperationPk from CorporativeOperation c   ");
		sbQuery.append("   JOIN c.programInterestCoupon interest	 ");
		sbQuery.append("   WHERE c.idCorporativeOperationPk in (:processids)  ");
		sbQuery.append("    and trunc(interest.experitationDate) = trunc(:currentDate)  ");
		sbQuery.append("  UNION ");
		sbQuery.append("   SELECT c.idCorporativeOperationPk from CorporativeOperation c   ");
		sbQuery.append("    JOIN c.programAmortizationCoupon amortiz   ");
		sbQuery.append("   WHERE c.idCorporativeOperationPk in (:processids)   ");
		sbQuery.append("   	and trunc(amortiz.expirationDate) = trunc(:currentDate)   ");
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("processids", idCorporativeOperationPk);
		query.setParameter("currentDate", CommonsUtilities.currentDate());	
		
		List<Long> test = (List<Long>)query.getResultList() ;
		
		return test;
    }	
	
		
	/**
	 * calcula stock de una lista de valores.
	 */
	
	public Long executeStockCalcProcessSecurity(CorporativeOperation corporative) throws ServiceException {	
		
		Long stockId = null;
		
		stockCalculationProcess = new StockCalculationProcess();
		stockCalculationProcess.setIndAutomactic(BooleanType.NO.getCode());
		stockCalculationProcess.setStockType(StockType.CORPORATIVE.getCode());		
		stockCalculationProcess.setCutoffDate(corporative.getCutoffDate());
		stockCalculationProcess.setRegistryDate(corporative.getRegistryDate());
		stockCalculationProcess.setSecurity(corporative.getSecurities());
		stockCalculationProcess.setHolder(null);
		stockCalculationProcess.setParticipant(null);
		stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());			
		stockCalculationProcess.setSecurity(corporative.getSecurities());
		try {
			
			stockId = manageStockCalculationFacade.registerStockCalculation(stockCalculationProcess);
			
			log.info("Stock created "+stockId);
			
		} catch(Exception e) { 
			log.error(e.getMessage());			
		}
		return stockId;	
	}
	
	

	/**
	 * STOCKS AUN EN EJECUCION
	 * @return
	 */
	
	public Integer stocksChargeOnBBDD()  {	
	
		
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Select count(*) from STOCK_CALCULATION_PROCESS "
				+ " where 1 = 1 "
				+ " and trunc(CREATION_DATE) = " + "'"+ CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate())+"'"
				+ " and STOCK_CLASS = 1328 " //NORMAL 
				+ " and STOCK_STATE <> 1327 "  //TERMINADO 
				);
		
		Query query = em.createNativeQuery(sbQuery.toString());
			
		Integer stocks = Integer.valueOf(query.getSingleResult().toString());
		
		return stocks;
		
	}
	
	
	
	
	/**
     * Verify preliminar stock calculation.
     *
     * @param lstProcesses the lst processes
     * @throws ServiceException the service exception
     */
    public Long verifyStockCalculationAutomatic(CorporativeOperation corporative) throws ServiceException{
    	
    Long idStockProcess = null;
    
    corporative = loaderEntityService.loadEntity(corporative, corporative.getIdCorporativeOperationPk());
    	if(corporative.getRetirementOperation() == null || corporative.getRetirementOperation().getIdRetirementOperationPk() == null){
    		Integer stockType = getCorporateRequiredStockCalculation(corporative);
    		if(stockType !=null){
    			idStockProcess = validateStockCalculationCorporate(corporative,CommonsUtilities.currentDate(),stockType);
    		}
    	}
    	return idStockProcess;
    }
    
    /**
     * Verify preliminar stock calculation.
     *
     * @param lstProcesses the lst processes
     * @throws ServiceException the service exception
     */
    
    public Boolean verifyStockCalculationAutomaticFinal(Long idStockCalculationPr) throws ServiceException{
    
    	Boolean stockOk = false;	
    	
        	StringBuilder sbQuery = new StringBuilder();
    		sbQuery.append(" select sp from StockCalculationProcess sp ");
    		sbQuery.append(" where sp.idStockCalculationPk = :processids ");
    		sbQuery.append(" and sp.stockState = :stockState ");
    		sbQuery.append(" and sp.stockClass = :stockClass ");

    		Query query = em.createQuery(sbQuery.toString());
    		
    		query.setParameter("processids", idStockCalculationPr);
    		query.setParameter("stockState", StockStateType.FINISHED.getCode());
    		query.setParameter("stockClass", StockClassType.NORMAL.getCode());

    		if(query.getResultList().size() == 0){
    			stockOk=true;
    		}
    		
    	return 	stockOk;
    }
    
    /*======================================== VALIDACIONES PROCESO AUTOMATICO =========================================*/
    
    /**
     * Verify preliminar stock calculation automatic.
     *
     * @param lstProcesses the lst processes
     * @throws ServiceException the service exception
     */
    public List<CorporativeOperation> verifyPreliminarStockCalculationAutomatic(List<CorporativeOperation> lstProcesses){
    	
    	
		//lista corporativos con error para la correcion 
    	List<CorporativeOperation> lstCorporateError = new ArrayList<>();
    	
//    	List<CorporativeOperation> errorProcess = new ArrayList<>();
//    	List<Long> errorProcessAux = new ArrayList<>();
//    	List<Long> errorProcessAux2 = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		corporative = loaderEntityService.loadEntity(corporative, corporative.getIdCorporativeOperationPk());
    		if(corporative.getRetirementOperation() == null || corporative.getRetirementOperation().getIdRetirementOperationPk() == null){
    			Integer stockType = getCorporateRequiredStockCalculation(corporative);
    			if(stockType !=null){
    				Long idStockProcess = validateStockCalculationCorporate(corporative,CommonsUtilities.currentDate(),stockType);
    				if(idStockProcess == null){
    					//Si no tiene calculo de stock
    					lstCorporateError.add(corporative);
    				}else {
    					try {
    						Long countBalance = validateBalanceStockCalculationCorporate(idStockProcess, corporative.getSecurities().getIdSecurityCodePk());
        					if(countBalance!=null && countBalance==0  && 
        							(searchDesmaterializedBalance(corporative.getSecurities().getIdSecurityCodePk()).intValue()>0) && 
        							hasBalance(corporative.getSecurities().getIdSecurityCodePk())){
        						
        					//DEBO REVISAR ESTE ERROR
        					//	corporateevent.not.balance.stock=No existe c\u00E1lculo de stock por falta de movimientos en la fecha corte/registro del valor para los procesos {0}. Verifique.
        					//	errorProcessAux.add(corporative.getIdCorporativeOperationPk());
        					}else if(validateInconsistencyBalance(idStockProcess, corporative.getSecurities().getIdSecurityCodePk()) && 
        							(searchDesmaterializedBalance(corporative.getSecurities().getIdSecurityCodePk()).intValue()>0) && 
        							hasBalance(corporative.getSecurities().getIdSecurityCodePk())){
        					
        						
        					//corporateevent.incon.balance.stock=C\u00E1lculo de stock del valor con inconsistencia para los procesos {0}. Verifique.	
        					//DEBO REVISAR ESTE ERROR
        					//	errorProcessAux2.add(corporative.getIdCorporativeOperationPk());
        					}
						} catch (Exception e) {
							//Capturar la excepcion
							
						}
    				}
    			}
    		}
    	}
    
    	return lstCorporateError;
    }
    
    /**
     * Verify security status.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List <CorporativeOperation> verifySecurityStatusAutomatic(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Long> process = new ArrayList<>();
		
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	sbQuery.append("select cp from CorporativeOperation cp ");
    	sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
       	sbQuery.append(" and cp.securities.stateSecurity != :state  ");
       	parameters.put("processids", process);
		parameters.put("state",SecurityStateType.REGISTERED.getCode());
		List<CorporativeOperation> lstCorporateError = findListByQueryString(sbQuery.toString(), parameters);
		
		//El valor relacionado con los hechos de importancias: {0} no se encuentra en estado CONFIRMADO. Verifique.
		
		return lstCorporateError;
    }
    
    /**
     * Verify security status.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List <CorporativeOperation> verifyIssuanceStatusAutomatic(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Long> process = new ArrayList<>();
		
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	sbQuery.append("select cp from CorporativeOperation cp ");
    	sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
       	sbQuery.append(" and cp.securities.issuance.stateIssuance != :state  ");
       	parameters.put("processids", process);
		parameters.put("state",IssuanceStateType.AUTHORIZED.getCode());
		List<CorporativeOperation> lstCorporateError = findListByQueryString(sbQuery.toString(), parameters);
		
		//El valor relacionado con los hechos de importancias: {0} no se encuentra en estado CONFIRMADO. Verifique.
		
		return lstCorporateError;
    }
    
    /**
     * Verify security status.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List<CorporativeOperation>  verifySecurityDesmaterializedAutomatic(List<CorporativeOperation> lstProcesses) throws ServiceException{

    	List<CorporativeOperation> lstCorporateError = new ArrayList<>();
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    			process.add(corporative.getIdCorporativeOperationPk());
    	}
    	if(process!=null && !process.isEmpty()){
    		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
        	sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
           	sbQuery.append(" and cp.securities.stateSecurity != :state  ");
           	sbQuery.append(" and nvl(cp.securities.desmaterializedBalance,0) = 0  ");
           	parameters.put("processids", process);
    		parameters.put("state",SecurityStateType.REGISTERED.getCode());
    		lstCorporateError = findListByQueryString(sbQuery.toString(), parameters);
    		
    		//El valor relacionado con los hechos de importancias: {0} no existe saldo desmaterializado. Verifique
    	}    	
		return lstCorporateError;
    }
    
    /**
     * Verify not settled balances corporate.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List <CorporativeOperation> verifyNotSettledBalancesCorporateAutomatic(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	
    	List<CorporativeOperation> lstCorporateError = new ArrayList<>();
    	Integer stockType = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		stockType = getCorporateRequiredStockCalculation(corporative);
    		if(stockType != null){
    			if(verifyNotSettledStock(corporative.getIdCorporativeOperationPk(),stockType)!=null){
    				lstCorporateError.add(corporative);
    			}
    		} else {
    			if(verifyNotSettledBalances(corporative.getSecurities().getIdSecurityCodePk()) != null){
    				lstCorporateError.add(corporative);
    			}
    		}
    	}
    	
		return lstCorporateError;
    }
    
    
    /**
     * Verify transit balances.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List <CorporativeOperation> verifyTransitBalancesAutomatic(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	
    	List<CorporativeOperation> lstCorporateError = new ArrayList<>();
    	Integer stockType = null;
    	for(CorporativeOperation corporative : lstProcesses){
    		stockType = getCorporateRequiredStockCalculation(corporative);
    		if(stockType != null){
    			if(verifyExistTransitBalanceStock(corporative.getIdCorporativeOperationPk(),stockType)!=null){
    				lstCorporateError.add(corporative);
    			}
    		}else if(verifyExistTransitBalance(corporative.getSecurities().getIdSecurityCodePk()) != null){
    			lstCorporateError.add(corporative);
    		}    		
    	}
    	
		return lstCorporateError;
    }
    
    /**
	 * Verify holder movements preliminar.
	 *
	 * @param lstProcesses the lst processes
	 * @param flag the flag
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> verifyHolderMovementsPreliminarAutomatic(List<CorporativeOperation> lstProcesses) throws ServiceException{ 
		
		List<Long> process = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		process.add(corporative.getIdCorporativeOperationPk());
    	}
    	StringBuilder sbQuery = new StringBuilder();
    	sbQuery.append("        select distinct co.id_corporative_operation_pk from holder_account_movement ham ");
		sbQuery.append("   	join  movement_type mt");
		sbQuery.append("   on ham.id_movement_type_fk=mt.id_movement_type_pk");
		sbQuery.append("  join holder_account ha");
		sbQuery.append("        on ham.id_holder_account_fk=ha.id_holder_account_pk");
		sbQuery.append("  join corporative_process_result cpr");
		sbQuery.append("   on ha.id_holder_account_pk=cpr.id_holder_account_fk");
		sbQuery.append("   join corporative_operation co");
		sbQuery.append("   on cpr.id_corporative_operation_fk=co.id_corporative_operation_pk");
		sbQuery.append("       where (mt.ind_block_movement=:indyes or mt.ind_guarantee_movement=:indyes)");
		sbQuery.append(" and co.id_corporative_operation_pk in (:listProcess)");
		sbQuery.append("  and trunc(movement_date) > (select trunc(creation_date) from stock_calculation_process");
		sbQuery.append(" where trunc(cutoff_date)=trunc(co.cutoff_date)");
		sbQuery.append(" and trunc(registry_date)=trunc(co.registry_date)");
		sbQuery.append("  and stock_type=:stock_type");
		sbQuery.append(" and stock_class=:stock_class");
		sbQuery.append(" and id_Stock_Calculation_pk=nvl(co.id_Stock_Calculation,0)");
		sbQuery.append(" and id_security_code_fk=co.ID_ORIGIN_SECURITY_CODE_FK)");
		sbQuery.append("");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("stock_class",StockClassType.NORMAL.getCode());
		query.setParameter("stock_type",StockType.CORPORATIVE.getCode());
		query.setParameter("indyes", BooleanType.YES.getCode());
		query.setParameter("listProcess", process);
		List<Long> errorProcess = query.getResultList();
		
		List<CorporativeOperation> lstCorporateError = new ArrayList<>();
		
		if(errorProcess.size()>GeneralConstants.ZERO_VALUE_INT){
			StringBuilder sbQueryC = new StringBuilder();
			Map<String, Object> parameters = new HashMap<String, Object>();
	    	sbQueryC.append("select cp from CorporativeOperation cp ");
	    	sbQueryC.append(" where cp.idCorporativeOperationPk in (:processids) ");
	       	parameters.put("processids", errorProcess);
	        lstCorporateError = findListByQueryString(sbQuery.toString(), parameters);
		}
		
		return lstCorporateError;
	}
	
	 /**
     * Verify corporate created retirement operation.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List <CorporativeOperation> verifyCorporateCreatedRetirementOperationAutomatic(List<CorporativeOperation> lstProcesses) throws ServiceException{
    	
    	List<CorporativeOperation> lstCorporateError = new ArrayList<>();
    	for(CorporativeOperation process : lstProcesses){
    		process = loaderEntityService.loadEntity(process, process.getIdCorporativeOperationPk());
    		if(process.getRetirementOperation() != null && process.getRetirementOperation().getIdRetirementOperationPk() != null){
    			for(RetirementDetail retirementDetail:process.getRetirementOperation().getRetirementDetails()){
    				BigDecimal availableBalance = findAvailableBalance(retirementDetail.getHolderAccount().getIdHolderAccountPk(), process.getSecurities().getIdSecurityCodePk());
					if(retirementDetail.getAvailableBalance().compareTo(availableBalance) > 0 ){
						lstCorporateError.add(process);
					}
    			}
    		}
    	}
    	return lstCorporateError;
    }

    /**
     * Verify definitive amortization.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List<CorporativeOperation> verifyDefinitiveAmortizationAutomatic(List<CorporativeOperation> lstProcesses)throws ServiceException{
    	
    	List <CorporativeOperation> lstCorporateError = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		//si afecta saldo entonces es la ultima amortizacion - validaremos que esten en definitivo todos los intereses
    		if(corporative.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.CAPITAL_AMORTIZATION.getCode()) && 
    				corporative.getIndAffectBalance().equals(CorporateProcessConstants.IND_YES) && 
					validateUltimateAmortization(corporative.getSecurities().getIdSecurityCodePk())){
    			lstCorporateError.add(corporative);
    		}
    	}
    	
		return lstCorporateError;
    }
    
    /**
     * Verify date exchage raten.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List<CorporativeOperation> verifyDateExchangeRateAutomatic(List<CorporativeOperation> lstProcesses)throws ServiceException{
    	
    	List <CorporativeOperation> lstCorporateError = new ArrayList<>();
    	for(CorporativeOperation corporative : lstProcesses){
    		if(corporative.getCurrency().equals(CurrencyType.DMV.getCode())
    				|| corporative.getCurrency().equals(CurrencyType.UFV.getCode())
    					|| corporative.getCurrency().equals(CurrencyType.EU.getCode())){
    			if(CommonsUtilities.truncateDateTime(corporative.getExchangeDateRate())
    					.after(CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()))){
        			lstCorporateError.add(corporative);
        		}
    		}
    	}
		return lstCorporateError;
    }
    
    /**
     * Verify date exchage raten.
     *
     * @param lstProcesses the lst processes
     * @param flag the flag
     * @return the record validation type
     * @throws ServiceException the service exception
     */
    public List<CorporativeOperation> verifyIssuanceSecurityCirBalance(List<CorporativeOperation> lstProcesses)throws ServiceException{
    	
    	List <CorporativeOperation> lstCorporateError = new ArrayList<>();
//    	for(CorporativeOperation corporative : lstProcesses){
//    		if(corporative.getCurrency().equals(CurrencyType.DMV.getCode())
//    				|| corporative.getCurrency().equals(CurrencyType.UFV.getCode())
//    					|| corporative.getCurrency().equals(CurrencyType.EU.getCode())){
//    			if(CommonsUtilities.truncateDateTime(corporative.getExchangeDateRate())
//    					.after(CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()))){
//        			lstCorporateError.add(corporative);
//        		}
//    		}
//    	}
		return lstCorporateError;
    }
    
    
    
    
    /*========================================  CORRECIONES DEL AUTOMATICO =========================================*/
    
    /**
     * Verify preliminar stock calculation automatic.
     *
     * @param lstProcesses the lst processes
     * @throws ServiceException the service exception
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void correctPreliminarStockCalculationAutomatic(List<CorporativeOperation> lstProcesses){
    	
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void correctSecurityStatusAutomatic(List<CorporativeOperation> lstProcesses){
    	
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void correctIssuanceStatusAutomatic(List<CorporativeOperation> lstProcesses, LoggerUser loggerUser){
    	
    	for(CorporativeOperation corporative : lstProcesses){
    		StringBuilder sbQuery = new StringBuilder();
    		sbQuery.append(" update Issuance iss SET iss.stateIssuance = :state , ");
    		sbQuery.append(" iss.lastModifyUser = :modifyuser, iss.lastModifyDate = :modifydate ,");
    		sbQuery.append(" iss.lastModifyIp = :modifyip, iss.lastModifyApp = :modifyapp ");
    		sbQuery.append(" where iss.idIssuanceCodePk = :issuanceId ");

    		Query query = em.createQuery(sbQuery.toString());
    		query.setParameter("state", IssuanceStateType.AUTHORIZED.getCode());
    		query.setParameter("modifyuser", loggerUser.getUserName());
    		query.setParameter("modifydate", CommonsUtilities.currentDateTime());
    		query.setParameter("modifyip", loggerUser.getIpAddress());
    		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
    		query.setParameter("issuanceId", corporative.getSecurities().getIssuance().getIdIssuanceCodePk());
    		
    		query.executeUpdate();
    	}
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
    public void correctIssuanceSecurityCirBalance(List<CorporativeOperation> lstProcesses){
    	
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void correctDateExchangeRateAutomatic(List<CorporativeOperation> lstProcesses, LoggerUser loggerUser){
    	
    	for(CorporativeOperation corporative : lstProcesses){
    		StringBuilder sbQuery = new StringBuilder();
    		sbQuery.append("update CorporativeOperation co set co.exchangeDateRate = :newDate, ");
    		
    		sbQuery.append(" co.lastModifyUser = :modifyuser, co.lastModifyDate = :modifydate,");
    		sbQuery.append(" co.lastModifyIp = :modifyip, co.lastModifyApp = :modifyapp ");
    		sbQuery.append(" where co.idCorporativeOperationPk = :corpprocessid ");

    		Query query = em.createQuery(sbQuery.toString());
    		query.setParameter("newDate", CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()));
    		query.setParameter("modifyuser", loggerUser.getUserName());
    		query.setParameter("modifydate", CommonsUtilities.currentDateTime());
    		query.setParameter("modifyip", loggerUser.getIpAddress());
    		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
    		query.setParameter("corpprocessid", corporative.getIdCorporativeOperationPk());
    		
    		query.executeUpdate();
    	}
    }
    
    
    /**
     * Metodo para saber cuantos valores vigentes o suspendidos tiene una emision
     * @param idIssuance
     * @return
     */
    public List<String> issuanceSecuritiesAssociated(String idIssuance) {
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select sec.idSecurityCodePk from Security sec ");
		sbQuery.append(" where sec.issuance.idIssuanceCodePk =:idIssuance ");
		sbQuery.append(" and sec.stateSecurity in (:state) ");
		parameters.put("idIssuance", idIssuance);
		
		List <Integer> lstStateSecurity = new ArrayList<>();
		lstStateSecurity.add(SecurityStateType.REGISTERED.getCode());
		lstStateSecurity.add(SecurityStateType.SUSPENDED.getCode());
		parameters.put("state", lstStateSecurity);
		
		return findListByQueryString(sbQuery.toString(), parameters);
    }
    
    /**
     * Metodo para verificar si es el ultimo cupon de amortizacion, ejecutandose en un corporativo
     * 
     * @param idSecurityCodePk Clase-Clave de Valor
     * @param idCorporativeOperationPk PK del Objeto CorporativeOperation
     * @return true or false
     */
    public Boolean verifyLatestCouponAmort(String idSecurityCodePk, Long idCorporativeOperationPk){
		
    	Boolean result = false;
    	
    	StringBuilder querySql = new StringBuilder();
		
    	querySql.append("  	SELECT COUNT(1)                                                                                                    ");
    	querySql.append("  	FROM SECURITY SEC                                                                                                  ");
    	querySql.append("  	LEFT JOIN AMORTIZATION_PAYMENT_SCHEDULE APS ON APS.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                   ");
    	querySql.append("  	LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK       ");
    	querySql.append("  	LEFT JOIN CORPORATIVE_OPERATION CO on CO.ID_PROGRAM_AMORTIZATION_FK = PAC.ID_PROGRAM_AMORTIZATION_PK               ");
    	querySql.append("  	WHERE 1 = 1                                                                                                        ");
    	querySql.append("  	AND PAC.STATE_PROGRAM_AMORTIZATON = 1350                                                                           "); //VIGENTE
    	querySql.append("  	AND SEC.ID_SECURITY_CODE_PK = :idSecurityCodePk                                                                    ");
    	querySql.append("  	AND CO.ID_CORPORATIVE_OPERATION_PK = :idCorporativeOperationPk                                                     ");
		
		Query query = em.createNativeQuery(querySql.toString());
		
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idCorporativeOperationPk", idCorporativeOperationPk);
		
		log.info("....::::::: ULT_AMORT: Verificando si se trata de la ultima amortizacion idSecurityCodePk: " + idSecurityCodePk + ", idCorporativeOperationPk: " + idCorporativeOperationPk + " :::::::....");
		BigDecimal quantity = (BigDecimal)query.getSingleResult();		
		
		//Si el resultado es diferente a 1 entonces no es la ultima amortizacion en ejecucion de un corporativo
		if (Integer.valueOf(quantity.intValue()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
			result = true;
		}
		
		return result;
    }
    
    /**
     * Metodo para verificar si es el ultimo cupon de amortizacion
     * @param idSecurityCodePk
     * @return true or false
     */
    public Boolean verifyLatestCouponAmort(String idSecurityCodePk){
		
    	Boolean result = false;
    	
    	StringBuilder querySql = new StringBuilder();
		
    	querySql.append("  	SELECT COUNT(1)                                                                                                    ");
    	querySql.append("  	FROM SECURITY SEC                                                                                                  ");
    	querySql.append("  	LEFT JOIN AMORTIZATION_PAYMENT_SCHEDULE APS ON APS.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK                   ");
    	querySql.append("  	LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK       ");
    	querySql.append("  	WHERE 1 = 1                                                                                                        ");
    	querySql.append("  	AND PAC.STATE_PROGRAM_AMORTIZATON = 1350                                                                           "); //VIGENTE
    	querySql.append("  	AND SEC.ID_SECURITY_CODE_PK = :idSecurityCodePk                                                                    ");
		
		Query query = em.createNativeQuery(querySql.toString());
		
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		
		BigDecimal quantity = (BigDecimal)query.getSingleResult();
		
		//Si el resultado es mayor a 1 entonces no es la ultima amortizacion
		if (Integer.valueOf(quantity.intValue()).equals(GeneralConstants.ONE_VALUE_INTEGER)){
			result = true;
		}
		
		return result;
    }
    
    
    /**
     * Metodo para retornar una lista con los saldos actuales del valor
     * @param idSecurityCodePk
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<HolderAccountBalanceExp> lstHolderAccountaBalanceExp (String idSecurityCodePk){
    	
    	 List<HolderAccountBalanceExp> lstAccountBalanceExps = new ArrayList<>();
    	
 		 StringBuilder sbQuery = new StringBuilder();
 		 sbQuery.append("	select hab from HolderAccountBalance hab ");
 		 sbQuery.append("	join fetch hab.participant p");
 		 sbQuery.append("	join fetch hab.holderAccount ha");
 		 sbQuery.append("	join fetch hab.security sec");
 		 sbQuery.append("	where hab.id.idSecurityCodePk = :isincodes ");
 	 	 sbQuery.append(" 	and hab.totalBalance > 0 ");
 		 
 		 Query query = em.createQuery(sbQuery.toString());
 		 query.setParameter("isincodes", idSecurityCodePk);
 		
 		List<HolderAccountBalance> accountBalance  = (List<HolderAccountBalance>) query.getResultList();
    	 
 		 if(Validations.validateIsNotNullAndNotEmpty(accountBalance)){
 			for(HolderAccountBalance holderAccountBalance : accountBalance ){
 	    		 HolderAccountBalanceExp accountBalanceExp = new HolderAccountBalanceExp();
 	    		 
 	    		 accountBalanceExp.setHolderAccount(holderAccountBalance.getHolderAccount().getIdHolderAccountPk());
 	    		 accountBalanceExp.setSecurity(holderAccountBalance.getSecurity().getIdSecurityCodePk());
 	    		 accountBalanceExp.setParticipant(holderAccountBalance.getParticipant().getIdParticipantPk());
 	    		 accountBalanceExp.setTotalBalance(holderAccountBalance.getTotalBalance());
 	   		
 	    		 //si esta bloqueado por cat lo guardamos como Disponible
 	    		 if(holderAccountBalance.getAccreditationBalance().compareTo(GeneralConstants.ZERO_VALUE_BIGDECIMAL) > GeneralConstants.ZERO_VALUE_INTEGER){
 	    			 accountBalanceExp.setAvailableBalance(holderAccountBalance.getAccreditationBalance());
 	    		 }else{
 	    			 accountBalanceExp.setAvailableBalance(holderAccountBalance.getAvailableBalance());
 	    		 }
 	    		 accountBalanceExp.setBanBalance(holderAccountBalance.getBanBalance());
 	   			 accountBalanceExp.setPawnBalance(holderAccountBalance.getPawnBalance());
 	   			 accountBalanceExp.setOtherBlockBalance(holderAccountBalance.getOtherBlockBalance());
 	   			 
 	    		 lstAccountBalanceExps.add(accountBalanceExp);
 	    	 }
 		 }
    	 
    	 return  lstAccountBalanceExps;
    }
    
    /**
     * Metodo para persistir HolderAccountBalanceExp
     * @param lstHolderAccountaBalanceExp
     */
    public void regHolderAccountBalanceExp (List<HolderAccountBalanceExp> lstHolderAccountaBalanceExp){
    	for(HolderAccountBalanceExp accountBalanceExp : lstHolderAccountaBalanceExp){
    		create(accountBalanceExp);
    	}
    }
    
}
