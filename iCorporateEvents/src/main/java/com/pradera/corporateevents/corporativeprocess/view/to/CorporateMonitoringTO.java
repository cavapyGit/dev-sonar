package com.pradera.corporateevents.corporativeprocess.view.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class CorporateMonitoringFilter
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCorporateEvents
 * @Creation_Date :
 */
public class CorporateMonitoringTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Holds the selected Corporate Process. */
	private Integer processType;
	
	/** Holds the selected Corporate Date. */
	private Date processDate;
	
	/** Holds the selected Cut Off Date. */
	private Date cutoffDate;
	
	/** Holds the selected Register Date. */
	private Date registerdDate;
	
	/** Holds the selected Expiration Date. */
	private Date expirationDate;
	
	private Date expirationDateFinal;
	
	/** Holds the selected List Expiration Date. */
	private List<Date> listExpirationDate;
	
	/** Holds the selected Issuer. */
	private String issuerPk;
	
	/** Holds the selected Security Code. */
	private String idSecurityCode;
	
	/** Holds the selected Currency. */
	private Integer currency;
	
	/** Holds the selected State. */
	private Integer state;
	
	/** The securities. */
	private Security securities = new Security();
	
	/** The issuer. */
	private Issuer issuer = new Issuer();
	
	/** The delivery placement. */
	private Integer deliveryPlacement;
	
	/** The delivery placement aux. */
	private Integer deliveryPlacementAux;
	
	/** The lst state. */
	private List<Integer> lstState;
	
	/** The lst corporative event. */
	private List<Integer> lstCorporativeEvent;
	
	private Integer typeDateCorporative;
	
	private Date selectedDate;
	
	private Integer securityClass;
	
	private Integer securityCurrency;
	
	/**
	 * Gets the process date.
	 *
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * Gets the cutoff date.
	 *
	 * @return the cutoffDate
	 */
	public Date getCutoffDate() {
		return cutoffDate;
	}

	/**
	 * Sets the cutoff date.
	 *
	 * @param cutoffDate the cutoffDate to set
	 */
	public void setCutoffDate(Date cutoffDate) {
		this.cutoffDate = cutoffDate;
	}

	/**
	 * Gets the registerd date.
	 *
	 * @return the registerdDate
	 */
	public Date getRegisterdDate() {
		return registerdDate;
	}

	/**
	 * Sets the registerd date.
	 *
	 * @param registerdDate the registerdDate to set
	 */
	public void setRegisterdDate(Date registerdDate) {
		this.registerdDate = registerdDate;
	}


	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the processType
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the processType to set
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the issuer pk.
	 *
	 * @return the issuer pk
	 */
	public String getIssuerPk() {
		return issuerPk;
	}

	/**
	 * Sets the issuer pk.
	 *
	 * @param issuerPk the new issuer pk
	 */
	public void setIssuerPk(String issuerPk) {
		this.issuerPk = issuerPk;
	}

	/**
	 * Gets the securities.
	 *
	 * @return the securities
	 */
	public Security getSecurities() {
		return securities;
	}

	/**
	 * Sets the securities.
	 *
	 * @param securities the new securities
	 */
	public void setSecurities(Security securities) {
		this.securities = securities;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the delivery placement.
	 *
	 * @return the delivery placement
	 */
	public Integer getDeliveryPlacement() {
		return deliveryPlacement;
	}

	/**
	 * Sets the delivery placement.
	 *
	 * @param deliveryPlacement the new delivery placement
	 */
	public void setDeliveryPlacement(Integer deliveryPlacement) {
		this.deliveryPlacement = deliveryPlacement;
	}

	/**
	 * Gets the delivery placement aux.
	 *
	 * @return the delivery placement aux
	 */
	public Integer getDeliveryPlacementAux() {
		return deliveryPlacementAux;
	}

	/**
	 * Sets the delivery placement aux.
	 *
	 * @param deliveryPlacementAux the new delivery placement aux
	 */
	public void setDeliveryPlacementAux(Integer deliveryPlacementAux) {
		this.deliveryPlacementAux = deliveryPlacementAux;
	}

	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<Integer> getLstState() {
		return lstState;
	}

	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<Integer> lstState) {
		this.lstState = lstState;
	}

	/**
	 * Gets the lst corporative event.
	 *
	 * @return the lst corporative event
	 */
	public List<Integer> getLstCorporativeEvent() {
		return lstCorporativeEvent;
	}

	/**
	 * Sets the lst corporative event.
	 *
	 * @param lstCorporativeEvent the new lst corporative event
	 */
	public void setLstCorporativeEvent(List<Integer> lstCorporativeEvent) {
		this.lstCorporativeEvent = lstCorporativeEvent;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the listExpirationDate
	 */
	public List<Date> getListExpirationDate() {
		
		return listExpirationDate;
	}

	/**
	 * @param listExpirationDate the listExpirationDate to set
	 */
	public void setListExpirationDate(List<Date> listExpirationDate) {
		this.listExpirationDate = listExpirationDate;
	}

	public Integer getTypeDateCorporative() {
		return typeDateCorporative;
	}

	public void setTypeDateCorporative(Integer typeDateCorporative) {
		this.typeDateCorporative = typeDateCorporative;
	}

	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Integer getSecurityCurrency() {
		return securityCurrency;
	}

	public void setSecurityCurrency(Integer securityCurrency) {
		this.securityCurrency = securityCurrency;
	}

	public Date getExpirationDateFinal() {
		return expirationDateFinal;
	}

	public void setExpirationDateFinal(Date expirationDateFinal) {
		this.expirationDateFinal = expirationDateFinal;
	}
	
	
}
