package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;



// TODO: Auto-generated Javadoc
/**
 * The Class HolderAllocationProcessDetailTO.
 * @author Pradera Technologies
 *
 */
public class HolderAllocationProcessDetailTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The isin code. */
	private String isinCode;
	
	/** The holder. */
	private String holder;
	
	/** The holder description. */
	private String holderDescription;
	
	/** The bank account number. */
	private String bankAccountNumber;
	
	/** The holder amount. */
	private BigDecimal holderAmount;


	/**
	 * Gets the isin code.
	 *
	 * @return the isin code
	 */
	public String getIsinCode() {
		return isinCode;
	}
	
	/**
	 * Sets the isin code.
	 *
	 * @param isinCode the new isin code
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public String getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(String holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}
	
	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}
	
	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	
	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	
	/**
	 * Gets the holder amount.
	 *
	 * @return the holder amount
	 */
	public BigDecimal getHolderAmount() {
		return holderAmount;
	}
	
	/**
	 * Sets the holder amount.
	 *
	 * @param holderAmount the new holder amount
	 */
	public void setHolderAmount(BigDecimal holderAmount) {
		this.holderAmount = holderAmount;
	}
}