package com.pradera.corporateevents.corporativeprocess.batch;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.corporateevents.corporativeprocess.facade.CorporateProcessServiceFacade;
import com.pradera.corporateevents.corporativeprocess.process.CorporateEventProducer;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.reports.CorporateProcessReportsSender;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeSituationType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;


/**
 * Bachero que ejecuta el proceso preliminar del proceso corporativo, 
 * ademas ejecuta los reportes respectivos.
 * @author PraderaTechnologies
 *
 */
@BatchProcess(name="CorporateProcessPreliminaryBatch")
@RequestScoped
public class CorporateProcessPreliminaryBatch  implements JobExecution{

	
	/** The corp process excutors. */
	@Inject
	CorporateEventProducer corporateEventProducer;
		
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The report sender. */
	@Inject
	CorporateProcessReportsSender reportSender;
	
	@EJB
	CorporateProcessServiceFacade corporateProcessFacade;
	
	CorporateProcessExecutor corpProcessExcutors;
	
	/**
	 * Inicio de Bachero que ejecuta el proceso preliminar del proceso corporativo, 
     * ademas ejecuta los reportes respectivos.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		String userId = null;
		Long lngProcessId = null;
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		//obtenemos codigo de proceso corporativo y usuario quien ejecuto el bachero
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			if(processLoggerDetail.getParameterName().equals("processid")){
				lngProcessId = Long.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("userid")){
				userId = processLoggerDetail.getParameterValue();
			}	
		}
		if(userId != null && lngProcessId != null){
			//obtenemos evento corporativo
			CorporativeOperation corpOperation = corporateProcessFacade.getCorporateEventType(lngProcessId);		
			if(corpOperation != null){
				int state = corpOperation.getState();
				CorporateProcessStateType processState = CorporateProcessStateType.get(state);
				//validamos estado del evento corporativo
				if(CorporateProcessStateType.REGISTERED.equals(processState)
						|| CorporateProcessStateType.MODIFIED.equals(processState)
						|| CorporateProcessStateType.PRELIMINARY.equals(processState)
						|| CorporateProcessStateType.PRELIMINARY_FAIL.equals(processState)){
					log.info("+++++VALOR+++++"+corpOperation.getSecurities().getIdSecurityCodePk()+"-------------"+corpOperation.getCorporativeEventType().getDescription());
					LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
					CorporateProcessExecutor processExecutor = corporateEventProducer.getCorporateType(corpOperation.getCorporativeEventType().getCorporativeEventType());
					try{
						//actualizamos estado de evento corporativo (Preliminar en Proceso)
						corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY_IN_PROCESS.getCode(), loggerUser);
						//ejecutamos proceso preliminar
						processExecutor.executePreliminarProcess(corpOperation);
						//actualizamos estado de evento corporativo (Preliminar)

						if(corpOperation.getSecurities().getIndPaymentBenefit().equals(BooleanType.NO.getCode()) &&
								(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()) || 
								   ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()))){
							corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY.getCode(), loggerUser, false,CorporativeSituationType.IDEPOSITARY.getCode());
						}else if(corpOperation.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode()) &&
								(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()) || 
										   ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()))){
							corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY.getCode(), loggerUser, false,CorporativeSituationType.ISSUER.getCode());
						}else{
							corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY.getCode(), loggerUser, false,null);
						}
						corpOperation.setState(CorporateProcessStateType.PRELIMINARY.getCode());
						
						try{
							//generamos e enviamos reportes
							reportSender.sendReports(corpOperation, loggerUser);
						}catch (Exception e) {
							log.error("Error in sending report for the Process "+corpOperation.getIdCorporativeOperationPk());
							log.error(e.getMessage());
						}
					}catch (Exception e) {
						try{
							corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY_FAIL.getCode(), loggerUser);
						}catch (Exception ex) {
							log.error(ex.getMessage());
						}
						log.error(e.getMessage());
						throw new RuntimeException(e);
					}
				}else{
					throw new RuntimeException("Invalied process state, can not execute the preliminary process");
				}
			}else{
				throw new RuntimeException("Invalied process id, No Corporate process registered");
			}
		}else{
			throw new RuntimeException("Invalied parameters for the Corporate process preliminary execution");
		}
	}
	
	
	
	
	

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}	
}