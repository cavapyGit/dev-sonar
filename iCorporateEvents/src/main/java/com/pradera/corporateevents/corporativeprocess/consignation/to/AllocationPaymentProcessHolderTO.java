package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;



// TODO: Auto-generated Javadoc
/**
 * The Class AllocationPaymentProcessHolderTO.
 * @author Pradera Technologies
 *
 */
public class AllocationPaymentProcessHolderTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The name. */
	private String name;//nombreBeneficiario
	
	/** The document number. */
	private String documentNumber;//identificacionBeneficiario
	
	/** The holder amount. */
	private BigDecimal holderAmount;//montoaDepositar
	
	/** The account number. */
	private String accountNumber;//numeroCuentaEstandard
	
	/** The str account type. */
	private String strAccountType;//tipoCuenta
	
	/** The corporative description. */
	private String corporativeDescription;//ConceptoDetallado
	
	/** The informacion detalle1. */
	private String informacionDetalle1;//informAdicionalPago01
	
	/** The informacion detalle2. */
	private String informacionDetalle2;//informAdicionalPago02
	
	/** The holder account movement. */
	private Long holderAccountMovement;//digitosControl
	
	/** The issuer. */
	private String issuer;
	
	/** The security. */
	private String security;
	
	/** The holder pk. */
	private Long holderPK;
	
	/** The bank pk. */
	private Long bankPK;
	
	/** The holder account pk. */
	private Long holderAccountPK;
	
	/** The holder account bank pk. */
	private Long holderAccountBankPK;
	
	/** The process type. */
	private Integer processType;
	
	/** The account type. */
	private Integer accountType;
	
	/** The corporative process type. */
	private Integer corporativeProcessType;
	
	/** The corporative pk. */
	private Long corporativePK;
	
	/** The ind benefit. */
	private Integer indBenefit;
	
	/** The fund operation type. */
	private Long fundOperationType;
	
	/** The lst block operation pk. */
	private List<Long> lstBlockOperationPK;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	/**
	 * Gets the holder amount.
	 *
	 * @return the holder amount
	 */
	public BigDecimal getHolderAmount() {
		return holderAmount;
	}
	
	/**
	 * Sets the holder amount.
	 *
	 * @param holderAmount the new holder amount
	 */
	public void setHolderAmount(BigDecimal holderAmount) {
		this.holderAmount = holderAmount;
	}
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the str account type.
	 *
	 * @return the str account type
	 */
	public String getStrAccountType() {
		return strAccountType;
	}
	
	/**
	 * Sets the str account type.
	 *
	 * @param strAccountType the new str account type
	 */
	public void setStrAccountType(String strAccountType) {
		this.strAccountType = strAccountType;
	}
	
	/**
	 * Gets the corporative description.
	 *
	 * @return the corporative description
	 */
	public String getCorporativeDescription() {
		return corporativeDescription;
	}
	
	/**
	 * Sets the corporative description.
	 *
	 * @param corporativeDescription the new corporative description
	 */
	public void setCorporativeDescription(String corporativeDescription) {
		this.corporativeDescription = corporativeDescription;
	}
	
	/**
	 * Gets the informacion detalle1.
	 *
	 * @return the informacion detalle1
	 */
	public String getInformacionDetalle1() {
		return informacionDetalle1;
	}
	
	/**
	 * Sets the informacion detalle1.
	 *
	 * @param informacionDetalle1 the new informacion detalle1
	 */
	public void setInformacionDetalle1(String informacionDetalle1) {
		this.informacionDetalle1 = informacionDetalle1;
	}
	
	/**
	 * Gets the informacion detalle2.
	 *
	 * @return the informacion detalle2
	 */
	public String getInformacionDetalle2() {
		return informacionDetalle2;
	}
	
	/**
	 * Sets the informacion detalle2.
	 *
	 * @param informacionDetalle2 the new informacion detalle2
	 */
	public void setInformacionDetalle2(String informacionDetalle2) {
		this.informacionDetalle2 = informacionDetalle2;
	}
	
	/**
	 * Gets the holder account movement.
	 *
	 * @return the holder account movement
	 */
	public Long getHolderAccountMovement() {
		return holderAccountMovement;
	}
	
	/**
	 * Sets the holder account movement.
	 *
	 * @param holderAccountMovement the new holder account movement
	 */
	public void setHolderAccountMovement(Long holderAccountMovement) {
		this.holderAccountMovement = holderAccountMovement;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public String getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(String security) {
		this.security = security;
	}
	
	/**
	 * Gets the holder pk.
	 *
	 * @return the holder pk
	 */
	public Long getHolderPK() {
		return holderPK;
	}
	
	/**
	 * Sets the holder pk.
	 *
	 * @param holderPK the new holder pk
	 */
	public void setHolderPK(Long holderPK) {
		this.holderPK = holderPK;
	}
	
	/**
	 * Gets the bank pk.
	 *
	 * @return the bank pk
	 */
	public Long getBankPK() {
		return bankPK;
	}
	
	/**
	 * Sets the bank pk.
	 *
	 * @param bankPK the new bank pk
	 */
	public void setBankPK(Long bankPK) {
		this.bankPK = bankPK;
	}
	
	/**
	 * Gets the holder account bank pk.
	 *
	 * @return the holder account bank pk
	 */
	public Long getHolderAccountBankPK() {
		return holderAccountBankPK;
	}
	
	/**
	 * Sets the holder account bank pk.
	 *
	 * @param holderAccountBankPK the new holder account bank pk
	 */
	public void setHolderAccountBankPK(Long holderAccountBankPK) {
		this.holderAccountBankPK = holderAccountBankPK;
	}
	
	/**
	 * Gets the account type.
	 *
	 * @return the account type
	 */
	public Integer getAccountType() {
		return accountType;
	}
	
	/**
	 * Sets the account type.
	 *
	 * @param accountType the new account type
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	
	/**
	 * Gets the corporative process type.
	 *
	 * @return the corporative process type
	 */
	public Integer getCorporativeProcessType() {
		return corporativeProcessType;
	}
	
	/**
	 * Sets the corporative process type.
	 *
	 * @param corporativeProcessType the new corporative process type
	 */
	public void setCorporativeProcessType(Integer corporativeProcessType) {
		this.corporativeProcessType = corporativeProcessType;
	}
	
	/**
	 * Gets the corporative pk.
	 *
	 * @return the corporative pk
	 */
	public Long getCorporativePK() {
		return corporativePK;
	}
	
	/**
	 * Sets the corporative pk.
	 *
	 * @param corporativePK the new corporative pk
	 */
	public void setCorporativePK(Long corporativePK) {
		this.corporativePK = corporativePK;
	}
	
	/**
	 * Gets the ind benefit.
	 *
	 * @return the ind benefit
	 */
	public Integer getIndBenefit() {
		return indBenefit;
	}
	
	/**
	 * Sets the ind benefit.
	 *
	 * @param indBenefit the new ind benefit
	 */
	public void setIndBenefit(Integer indBenefit) {
		this.indBenefit = indBenefit;
	}
	
	/**
	 * Gets the fund operation type.
	 *
	 * @return the fund operation type
	 */
	public Long getFundOperationType() {
		return fundOperationType;
	}
	
	/**
	 * Sets the fund operation type.
	 *
	 * @param fundOperationType the new fund operation type
	 */
	public void setFundOperationType(Long fundOperationType) {
		this.fundOperationType = fundOperationType;
	}
	
	/**
	 * Gets the lst block operation pk.
	 *
	 * @return the lst block operation pk
	 */
	public List<Long> getLstBlockOperationPK() {
		return lstBlockOperationPK;
	}
	
	/**
	 * Sets the lst block operation pk.
	 *
	 * @param lstBlockOperationPK the new lst block operation pk
	 */
	public void setLstBlockOperationPK(List<Long> lstBlockOperationPK) {
		this.lstBlockOperationPK = lstBlockOperationPK;
	}
	
	/**
	 * Gets the holder account pk.
	 *
	 * @return the holder account pk
	 */
	public Long getHolderAccountPK() {
		return holderAccountPK;
	}
	
	/**
	 * Sets the holder account pk.
	 *
	 * @param holderAccountPK the new holder account pk
	 */
	public void setHolderAccountPK(Long holderAccountPK) {
		this.holderAccountPK = holderAccountPK;
	}
	
	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Integer getProcessType() {
		return processType;
	}
	
	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
}