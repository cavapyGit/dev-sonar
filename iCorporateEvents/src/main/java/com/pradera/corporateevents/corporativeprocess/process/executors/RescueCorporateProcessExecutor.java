package com.pradera.corporateevents.corporativeprocess.process.executors;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.corporateevents.corporativeprocess.process.CorporateExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcess;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.type.ImportanceEventType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RescueCorporateProcessExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/06/2014
 */
@CorporateProcess(CorporateExecutor.EARLY_PAYMENT)
@Performance
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@LocalBean
public class RescueCorporateProcessExecutor implements CorporateProcessExecutor {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The fixer. */
	@Inject
	private BalanceFixer fixer;
		
	/** The executor component service bean. */
	@Inject
	private Instance<AccountsComponentService> executorComponentServiceBean;
	
	/** The securities component service bean. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentServiceBean;
	
	/** The sc. */
	@Resource
    SessionContext sc;
	
	/** The rescue corporate process executor. */
	RescueCorporateProcessExecutor rescueCorporateProcessExecutor;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
    public void init(){
        this.rescueCorporateProcessExecutor = this.sc.getBusinessObject(RescueCorporateProcessExecutor.class);
    }
	
	/**
	 * This method is used execute the subscription preliminary process execution.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executePreliminarProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started Subscription preliminary process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		corporateExecutorService.deletePreviousPreliminaryInformation(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, true, false);
		corpOperation.setDeliveryFactor(CorporateProcessUtils.divide(corpOperation.getDeliveryFactor(), CorporateProcessConstants.AMOUNT_ROUND));
		List<Object[]> distinctHolders =  corporateExecutorService.getHolderAccountsForRescueEvent(corpOperation.getIdCorporativeOperationPk());
		if(distinctHolders!=null && distinctHolders.size()>0){
			this.rescueCorporateProcessExecutor.executePreliminarProcessAlgorithm(distinctHolders, corpOperation, processType, loggerUser);
		}
		return true;
	}

	/**
	 * Execute preliminar process algorithm.
	 *
	 * @param distinctHolders the distinct holders
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executePreliminarProcessAlgorithm(List<Object[]> distinctHolders,CorporativeOperation corpOperation,ImportanceEventType processType
			,LoggerUser loggerUser) throws ServiceException{
		CorporativeProcessResult totalProcessResult = null;
		for(Object[] arrObj:distinctHolders){
			totalProcessResult = new CorporativeProcessResult();
			String holderAlternateCode = (String) arrObj[0];
			BigDecimal totalBalance = ((BigDecimal) arrObj[1]);
			BigDecimal roundedTotal = CorporateProcessUtils.calculateByFactor(totalBalance,corpOperation.getDeliveryFactor(), 
					corpOperation.getIndRound());
			totalProcessResult.setTotalBalance(roundedTotal);
			totalProcessResult.setCorporativeOperation(corpOperation);
			totalProcessResult.setSecurity(corpOperation.getSecurities());
			List<Object[]> holderAccounts = corporateExecutorService.getBalancesForEachHolderAccount(corpOperation.getIdCorporativeOperationPk(),holderAlternateCode);
			if (holderAccounts != null && holderAccounts.size() > 0) {
				List<CorporativeProcessResult> holderAccResults = fixer.fixBalances(holderAccounts, corpOperation, corpOperation.getDeliveryFactor(), 
						corpOperation.getIndRound(), false, false, true, processType,null);
				corporateExecutorService.saveAll(holderAccResults, loggerUser);
				holderAccounts.clear();
				holderAccounts = null;								
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor#executeDefinitiveProcess(com.pradera.model.corporatives.CorporativeOperation)
	 */
	@Override
	public boolean executeDefinitiveProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started Rescue Corporative Events Process for Definitivo");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		boolean flag = false;
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		List<Object> participants = corporateExecutorService.getDistinctParticpantsForDefinitive(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.IND_NO_REMAINDER,
				CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		if(participants!=null && !participants.isEmpty()){
			for(Object obj : participants){
				Long participant = (Long)obj;
				//transaction for participant
				this.rescueCorporateProcessExecutor.executeDefinitiveProcessAccounts(corpOperation, participant, processType, loggerUser);
			}
			flag = true;
			BigDecimal totalBalance =(BigDecimal) corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
					CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_PROCESSED,
					CorporateProcessConstants.BALANCE_DESTINY, null);
			if(totalBalance != null && totalBalance.compareTo(BigDecimal.ZERO) > 0){
				this.rescueCorporateProcessExecutor.executeDefinitiveProcessSecurities(corpOperation, processType, loggerUser, totalBalance);
			}
			
		}
		return flag;
	}
	
	/**
	 * Execute definitive process securities.
	 *
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @param totalBalance the total balance
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessSecurities(CorporativeOperation corpOperation,ImportanceEventType processType
			,LoggerUser loggerUser,BigDecimal totalBalance) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		BigDecimal capital = totalBalance.multiply(corpOperation.getSecurities().getCurrentNominalValue());
		SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, capital, totalBalance, corpOperation.getSecurities().getIdSecurityCodePk());
		secOpe.setSecurities(corpOperation.getSecurities());
		corporateExecutorService.create(secOpe, loggerUser);				
		securitiesComponentServiceBean.get().executeSecuritiesComponent(CorporateProcessUtils.getSecuritiesComponent(processType, secOpe, BigDecimal.ZERO, totalBalance, null));
	}
	
	/**
	 * Execute definitive process accounts.
	 *
	 * @param corpOperation the corp operation
	 * @param participant the participant
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessAccounts(CorporativeOperation corpOperation,Long participant,ImportanceEventType processType
			,LoggerUser loggerUser) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		executorComponentServiceBean.get().executeAccountsComponent(fixer.generateAccountComponent(corporateExecutorService.
				getCorporateResultsForDefinitiveProcess(corpOperation.getIdCorporativeOperationPk(),
				participant, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null), processType, corpOperation));
		corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), participant, 
				true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_PROCESSED, null, loggerUser);
	}
	
}
