package com.pradera.corporateevents.corporativeprocess.consignation.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.FilePayrollConsigned;
import com.pradera.model.corporatives.FilePayrollConsignedDet;
import com.pradera.model.corporatives.PayrollConsignedOperation;
import com.pradera.model.corporatives.type.FilePayrollStateType;
import com.pradera.model.corporatives.type.FilePayrollType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * Clase de servicio para la consignacion de pagos.
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCorporateEvents
 * @Creation_Date :
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class FilePayrollConsignedServiceBean extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The log. */
	@Inject
	PraderaLogger log;

	public List<PayrollConsignedOperation> searchPayrollConsignedOper(PayrollConsignedOperation obj) throws ServiceException{
		
		List<PayrollConsignedOperation> lstPayrollConsignedOpe = new ArrayList<>();
		
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT p FROM PayrollConsignedOperation p  ");
		
		strbQuery.append(" WHERE 1 = 1");
		
		if(Validations.validateIsNotNullAndNotEmpty(obj.getSituation())) {
			strbQuery.append(" AND p.situation = :situation ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getState())) {
			strbQuery.append(" AND p.state = :state ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getProcessDate())) {
			strbQuery.append(" AND p.processDate = :processDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getCurrencyPayment())) {
			strbQuery.append(" AND p.currencyPayment = :currencyPayment ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getIssuer().getIdIssuerPk())) {
			strbQuery.append(" AND p.issuer.idIssuerPk = :idIssuerPk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(obj.getFinalDate())) {
			strbQuery.append(" AND p.consignedDate between :initialDate and :finalDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getParticipant().getIdParticipantPk())) {
			strbQuery.append(" AND p.participant.idParticipantPk = :idParticipantPk ");
		}
		
		Query query=em.createQuery(strbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(obj.getSituation())) {
			query.setParameter("situation", obj.getSituation());
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getState())) {
			query.setParameter("state", obj.getState());
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getProcessDate())) {
			query.setParameter("processDate", obj.getProcessDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getCurrencyPayment())) {
			query.setParameter("currencyPayment", obj.getCurrencyPayment());
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getIssuer().getIdIssuerPk())) {
			query.setParameter("idIssuerPk", obj.getIssuer().getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getParticipant().getIdParticipantPk())) {
			query.setParameter("idParticipantPk", obj.getParticipant().getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(obj.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(obj.getFinalDate())) {
			query.setParameter("initialDate", obj.getInitialDate());
			query.setParameter("finalDate", CommonsUtilities.putTimeToDateToEndOfDay(obj.getFinalDate()));
		}
		
		return query.getResultList();		
		
	} 
	
	public List<FilePayrollConsigned> searchFilePayroll(FilePayrollConsigned obj){
		
		List<FilePayrollConsigned> lstFilePayrollConsigned = new ArrayList<>();
		
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT p FROM  FilePayrollConsigned p  ");
		if(obj.getFilePayrollType().equals(FilePayrollType.PAGO_DERECHO.getCode())) {
			strbQuery.append(" inner join fetch p.issuer  ");
		}
		if(obj.getFilePayrollType().equals(FilePayrollType.LIQUIDACION.getCode())) {
			strbQuery.append(" inner join fetch p.participant  ");
		}
		
		strbQuery.append(" WHERE 1 = 1 ");
		
		if(obj.getFilePayrollType().equals(FilePayrollType.PAGO_DERECHO.getCode())) {
			if(Validations.validateIsNotNullAndNotEmpty(obj.getIssuer().getIdIssuerPk())) {
				strbQuery.append(" AND p.issuer.idIssuerPk = :idIssuerPk ");
			}
		}
		if(obj.getFilePayrollType().equals(FilePayrollType.LIQUIDACION.getCode())) {
			if(Validations.validateIsNotNullAndNotEmpty(obj.getParticipant().getIdParticipantPk())) {
				strbQuery.append(" AND p.participant.idParticipantPk = :idParticipantPk ");
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(obj.getCurrency())) {
			strbQuery.append(" AND p.currency = :currency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getInitialDate())) {
			strbQuery.append(" AND p.processDate >= :initialDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getFinalDate())) {
			strbQuery.append(" AND p.processDate <= :finalDate ");
		}
		
		Query query=em.createQuery(strbQuery.toString());
		
		if(obj.getFilePayrollType().equals(FilePayrollType.PAGO_DERECHO.getCode())) {
			if(Validations.validateIsNotNullAndNotEmpty(obj.getIssuer().getIdIssuerPk())) {
				query.setParameter("idIssuerPk", obj.getIssuer().getIdIssuerPk());
			}
		}
		if(obj.getFilePayrollType().equals(FilePayrollType.LIQUIDACION.getCode())) {
			if(Validations.validateIsNotNullAndNotEmpty(obj.getParticipant().getIdParticipantPk())) {
				query.setParameter("idParticipantPk", obj.getParticipant().getIdParticipantPk());
			}
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getCurrency())) {
			query.setParameter("currency", obj.getCurrency());
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getInitialDate())) {
			query.setParameter("initialDate", obj.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(obj.getFinalDate())) {
			query.setParameter("finalDate", obj.getFinalDate());
		}
		
		return query.getResultList();		
		
	} 
	
	public List<FilePayrollConsignedDet> searchFilePayrollDet(FilePayrollConsigned obj){
		
		List<FilePayrollConsigned> FilePayrollConsignedDet = new ArrayList<>();
		
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT p FROM  FilePayrollConsignedDet p  ");
		strbQuery.append(" inner join fetch p.payrollConsignedOperation  ");
		
		strbQuery.append(" WHERE 1 = 1");
		
		if(Validations.validateIsNotNullAndNotEmpty(obj.getIdFilePayrollConsignedPk())) {
			strbQuery.append(" AND p.filePayrollConsigned.idFilePayrollConsignedPk = :id ");
		}
		
		Query query=em.createQuery(strbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(obj.getIdFilePayrollConsignedPk())) {
			query.setParameter("id", obj.getIdFilePayrollConsignedPk());
		}
		
		return query.getResultList();		
		
	} 
	
	public void executePayrollPendingGenerate(PayrollConsignedOperation obj){
   		Query query = em.createNativeQuery(" BEGIN PRADERA_DEPOSITARY.SP_CREATE_PAYROLL_CONSIGNED( :issuerCode, :initialDatePrm , :finalDatePrm ); END; ");
   		query.setParameter("issuerCode", obj.getIssuer().getIdIssuerPk());
   		query.setParameter("initialDatePrm", CommonsUtilities.convertDateToString(obj.getInitialDate(), CommonsUtilities.DATE_PATTERN) );
   		query.setParameter("finalDatePrm", CommonsUtilities.convertDateToString(obj.getFinalDate(), CommonsUtilities.DATE_PATTERN) );
       	query.executeUpdate();
	}
	
	public void executeSettlementPayrollPendingGenerate(PayrollConsignedOperation obj){
   		Query query = em.createNativeQuery(" BEGIN PRADERA_DEPOSITARY.SP_CREATE_PAYROLL_SETTLEMENT( :participantPk, :initialDatePrm , :finalDatePrm ); END; ");
   		query.setParameter("participantPk", obj.getParticipant().getIdParticipantPk());
   		query.setParameter("initialDatePrm", CommonsUtilities.convertDateToString(obj.getInitialDate(), CommonsUtilities.DATE_PATTERN) );
   		query.setParameter("finalDatePrm", CommonsUtilities.convertDateToString(obj.getFinalDate(), CommonsUtilities.DATE_PATTERN) );
       	query.executeUpdate();
	}

	public void save( FilePayrollConsigned filePayrollConsigned) {
		this.create(filePayrollConsigned);
	}
	
	/**
	 * Update State Security 
	 * @param security
	 * @param loggerUser
	 */
	public void updateProcessFilePayroll(Long idFilePayroll, LoggerUser loggerUser) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Update FilePayrollConsigned fpc Set ");
		sbQuery.append(" fpc.state = :state, ");
		sbQuery.append(" fpc.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" fpc.lastModifyDate = :lastModifyDatePrm, ");
		sbQuery.append(" fpc.lastModifyApp = :lastModifyAppPrm, ");
		sbQuery.append(" fpc.lastModifyIp = :lastModifyIpPrm ");
		sbQuery.append(" Where fpc.idFilePayrollConsignedPk = :idFilePayroll ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", FilePayrollStateType.PROCESSED.getCode());
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("idFilePayroll", idFilePayroll);
				
		query.executeUpdate();
	}
	
	/**
	 * Update State Security 
	 * @param security
	 * @param loggerUser
	 */
	public void updateStatePayrollConsigned(Long idFilePayroll, LoggerUser loggerUser, Integer state) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Update PayrollConsignedOperation p Set ");
		sbQuery.append(" p.state = :state, ");
		sbQuery.append(" p.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" p.lastModifyDate = :lastModifyDatePrm, ");
		sbQuery.append(" p.lastModifyApp = :lastModifyAppPrm, ");
		sbQuery.append(" p.lastModifyIp = :lastModifyIpPrm ");
		sbQuery.append(" Where p.idPayrollConsignedOperPk = :idFilePayroll ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", state);
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("idFilePayroll", idFilePayroll);
				
		query.executeUpdate();
	}
	
}