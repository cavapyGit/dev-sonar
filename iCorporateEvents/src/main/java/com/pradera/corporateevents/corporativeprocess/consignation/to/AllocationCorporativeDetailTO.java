package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;



// TODO: Auto-generated Javadoc
/**
 * The Class AllocationCorporativeDetailTO.
 * @author Pradera Technologies
 *
 */
public class AllocationCorporativeDetailTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id corporative process. */
	private String idCorporativeProcess;
	
	/** The corporative type. */
	private String corporativeType;
	
	/** The security. */
	private String security;
	
	/** The issuer. */
	private String issuer;
	
	/** The benefit amount. */
	private BigDecimal benefitAmount;
	
	/** The custody amount. */
	private BigDecimal custodyAmount;
	
	/** The tax amount. */
	private BigDecimal taxAmount;
	
	/** The corporate state. */
	private Integer corporateState;
	
	/** The corporative state. */
	private String corporativeState;
	
	/** The include commission. */
	private boolean includeCommission;
	
	/** The commission amount. */
	private BigDecimal commissionAmount;
	
	/** The selection. */
	private boolean selection;


	/**
	 * Checks if is selection.
	 *
	 * @return the selection
	 */
	public boolean isSelection() {
		return selection;
	}
	
	/**
	 * Sets the selection.
	 *
	 * @param selection the selection to set
	 */
	public void setSelection(boolean selection) {
		this.selection = selection;
	}
	
	/**
	 * Gets the id corporative process.
	 *
	 * @return the id corporative process
	 */
	public String getIdCorporativeProcess() {
		return idCorporativeProcess;
	}
	
	/**
	 * Sets the id corporative process.
	 *
	 * @param idCorporativeProcess the new id corporative process
	 */
	public void setIdCorporativeProcess(String idCorporativeProcess) {
		this.idCorporativeProcess = idCorporativeProcess;
	}
	
	/**
	 * Gets the corporative type.
	 *
	 * @return the corporative type
	 */
	public String getCorporativeType() {
		return corporativeType;
	}
	
	/**
	 * Sets the corporative type.
	 *
	 * @param corporativeType the new corporative type
	 */
	public void setCorporativeType(String corporativeType) {
		this.corporativeType = corporativeType;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public String getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(String security) {
		this.security = security;
	}
	
	/**
	 * Gets the benefit amount.
	 *
	 * @return the benefit amount
	 */
	public BigDecimal getBenefitAmount() {
		return benefitAmount;
	}
	
	/**
	 * Sets the benefit amount.
	 *
	 * @param benefitAmount the new benefit amount
	 */
	public void setBenefitAmount(BigDecimal benefitAmount) {
		this.benefitAmount = benefitAmount;
	}
	
	/**
	 * Gets the custody amount.
	 *
	 * @return the custody amount
	 */
	public BigDecimal getCustodyAmount() {
		return custodyAmount;
	}
	
	/**
	 * Sets the custody amount.
	 *
	 * @param custodyAmount the new custody amount
	 */
	public void setCustodyAmount(BigDecimal custodyAmount) {
		this.custodyAmount = custodyAmount;
	}
	
	/**
	 * Gets the tax amount.
	 *
	 * @return the tax amount
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	
	/**
	 * Sets the tax amount.
	 *
	 * @param taxAmount the new tax amount
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	/**
	 * Gets the corporative state.
	 *
	 * @return the corporative state
	 */
	public String getCorporativeState() {
		return corporativeState;
	}
	
	/**
	 * Sets the corporative state.
	 *
	 * @param corporativeState the new corporative state
	 */
	public void setCorporativeState(String corporativeState) {
		this.corporativeState = corporativeState;
	}
	
	/**
	 * Checks if is include commission.
	 *
	 * @return true, if is include commission
	 */
	public boolean isIncludeCommission() {
		return includeCommission;
	}
	
	/**
	 * Sets the include commission.
	 *
	 * @param includeCommission the new include commission
	 */
	public void setIncludeCommission(boolean includeCommission) {
		this.includeCommission = includeCommission;
	}
	
	/**
	 * Gets the corporate state.
	 *
	 * @return the corporate state
	 */
	public Integer getCorporateState() {
		return corporateState;
	}
	
	/**
	 * Sets the corporate state.
	 *
	 * @param corporateState the new corporate state
	 */
	public void setCorporateState(Integer corporateState) {
		this.corporateState = corporateState;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the commission amount.
	 *
	 * @return the commission amount
	 */
	public BigDecimal getCommissionAmount() {
		return commissionAmount;
	}
	
	/**
	 * Sets the commission amount.
	 *
	 * @param commissionAmount the new commission amount
	 */
	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}
}