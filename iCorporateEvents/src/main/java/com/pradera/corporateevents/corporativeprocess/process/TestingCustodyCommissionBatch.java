package com.pradera.corporateevents.corporativeprocess.process;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.corporateevents.corporativeprocess.facade.CustodyCommissionServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * The Class TestingCustodyCommissionBatch.
 *
 * @author Pradera Technologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class TestingCustodyCommissionBatch{
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The custody commission service facade. */
	@EJB
	CustodyCommissionServiceFacade custodyCommissionServiceFacade;
	
	/**
	 * Execute process.
	 *
	 * @throws Exception the exception
	 */
	public void executeProcess() throws Exception{
		custodyCommissionServiceFacade.calculateCustodyCommission("ADMIN");
	}
}