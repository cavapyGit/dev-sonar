package com.pradera.corporateevents.corporativeprocess.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.CustodyCommission;
import com.pradera.model.corporatives.CustodyCommissionDetail;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.ImportanceEventType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class CorporateServiceBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCorporateEvents
 * @Creation_Date :
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class CustodyCommissionServiceBean extends CrudDaoServiceBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * METHOD TO GET THE CORPORATIVE PROCESSES WHICH WILL PAY TOMORROW.
	 *
	 * @return the corporatives
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> getCorporatives() throws ServiceException{
		List<Integer> corporativeTypes = getBenefitCorporativeType();
		List<CorporativeOperation> corporatives = new ArrayList<CorporativeOperation>();
		String strQuery = StringUtils.EMPTY;
		strQuery = 
				"select c " +
				"from CorporativeOperation c " +
				"where " +
				"trunc(c.deliveryDate) = trunc(:date) " +
				"and c.corporativeEventType.corporativeEventType in (:lstCorporatives) " +
				"and nvl(c.indPayed,0) = 0 ";

		Query query = em.createQuery(strQuery,CorporativeOperation.class);
		query.setParameter("date",CommonsUtilities.currentDate());
		query.setParameter("lstCorporatives",corporativeTypes);
		corporatives = query.getResultList();

		return corporatives;
	}

	/**
	 * PRIVATE METHOD TO GET THE CORPORATIVE PROCESS WHICH MUST BE ALLOCATED.
	 *
	 * @return the benefit corporative type
	 */
	private List<Integer> getBenefitCorporativeType(){
		List<Integer> lstCorporativeType = new ArrayList<Integer>();
		lstCorporativeType.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		return lstCorporativeType;
	}

	/**
	 * METHOD WHICH WILL GET THE PREVIOUS CORPORATIVE PROCESS TO GET PREVIOUS BALANCES.
	 *
	 * @param securityCode the security code
	 * @return the previous corporative
	 */
	public Long getPreviousCorporative(String securityCode){
		Long previousCorporative = null;
		try{
			String strQuery = 
						"select max(c.idCorporativeOperationPk) " +
						"from CorporativeOperation c " +
						"where " +
						"c.securities.idIsinCodePk = :security " +
						"and trunc(c.deliveryDate) < trunc(:date)";
			Query query = em.createQuery(strQuery,Long.class);
			query.setParameter("security",securityCode);
			query.setParameter("date",CommonsUtilities.currentDate());
			previousCorporative = (Long)query.getSingleResult();

		}catch(NoResultException x){
			x.printStackTrace();
		}
		return previousCorporative;
	}

	/**
	 * METHOD TO GET ALL THE MOVEMENTS IN A SECURITY TO CALCULATE THE BALANCE IN DIFFERENT BALANCE TYPES.
	 *
	 * @param securityCode the security code
	 * @param registryDate the registry date
	 * @return the movements by security
	 */
	public List<Object[]> getMovementsBySecurity(String securityCode, Date registryDate){
		List<Object[]> lstMovements = new ArrayList<Object[]>();
		String strQuery = 
					"select " +
					"mt.indPrimarySecondarySettle," +											//[0]
					"mt.indMovementClass," +													//[1]
					"sum(ham.movementQuantity), " +												//[2]
					"trunc(ham.movementDate), " +												//[3]
					"ham.holderAccountBalance.holderAccount.idHolderAccountPk " +				//[4]
					"from HolderAccountMovement ham, MovementType mt " +
					"where " +
					"mt.idMovementTypePk = ham.movementType.idMovementTypePk " +
					"and ham.holderAccountBalance.security.idIsinCodePk = :security " +
					"AND nvl(mt.indPrimarySecondarySettle,0) <> 0 " +
					"and trunc(ham.movementDate) between :registryDate and :today " +
					"group by " +
					"mt.indPrimarySecondarySettle,mt.indMovementClass,ham.movementDate," +
					"ham.holderAccountBalance.holderAccount.idHolderAccountPk " +
					"order by ham.holderAccountBalance.holderAccount.idHolderAccountPk,ham.movementDate asc ";

		Query query = em.createQuery(strQuery,Object[].class);
		query.setParameter("security",securityCode);
		query.setParameter("registryDate",registryDate);
		query.setParameter("today",CommonsUtilities.currentDate());
		lstMovements = query.getResultList();

		return lstMovements;
	}

	/**
	 * METHOD WHICH WILL GET THE BALANCES OF THE PREVIOUS CORPORATIVE OPERATION PAYMENT.
	 *
	 * @param corporative the corporative
	 * @return the custody commission detail
	 */
	public List<CustodyCommissionDetail> getCustodyCommissionDetail(Long corporative){
		List<CustodyCommissionDetail> lstPreviousBalances = new ArrayList<CustodyCommissionDetail>();
		String strQuery = 
					"select ccd " +
					"from CustodyCommissionDetail ccd, CustodyCommission cc " +
					"where " +
					"ccd.custodyCommission.idCustodyCommissionPk = cc.idCustodyCommissionPk " +
					"and cc.corporativeOperation.idCorporativeOperationPk = :corporative " +
					"order by ccd.holderAccount.idHolderAccountPk ";
		Query query = em.createQuery(strQuery,CustodyCommissionDetail.class);
		query.setParameter("corporative",corporative);
		lstPreviousBalances = query.getResultList();

		return lstPreviousBalances;
	}

	/**
	 * METHOD TO GET THE PREVIOUS CORPORATIVES FOR SECURITY.
	 *
	 * @param securityCode the security code
	 * @return the list previous corporative
	 */
	public List<CorporativeOperation> getListPreviousCorporative(String securityCode){
		List<CorporativeOperation> lstCorporativeOperationes = new ArrayList<CorporativeOperation>();
		
		String strQuery = 
					"select c " +
					"from CorporativeOperation c " +
					"where " +
					"c.securities.idIsinCodePk = :security " +
					"and trunc(c.deliveryDate) < trunc(:date) " +
					"order by c.idCorporativeOperationPk desc ";
		Query query = em.createQuery(strQuery,Long.class);
		query.setParameter("security",securityCode);
		query.setParameter("date",CommonsUtilities.currentDate());
		lstCorporativeOperationes = query.getResultList();

		return lstCorporativeOperationes;
	}

	/**
	 * METHOD THAT WILL GET THE BEGIN SEGMENT PLACEMENT.
	 *
	 * @param securityCode the security code
	 * @return the date
	 */
	public Date beginPlacementSegment(String securityCode){
		Date beginSegmentDate = null;

		String strQuery = 
				"select ps.beginingDate " +
				"from PlacementSegmentDetail psd, PlacementSegment ps " +
				"where " +
				"psd.security.idIsinCodePk = :security " +
				"and psd.placementSegment.idPlacementSegmentPk = ps.idPlacementSegmentPk";

		Query query = em.createQuery(strQuery,Date.class);
		query.setParameter("security",securityCode);
		beginSegmentDate = (Date) query.getSingleResult();

		return beginSegmentDate;
	}

	/**
	 * METHOD TO GET THE LAST CUSTODY COMMISSION PROCESS.
	 *
	 * @param corporativeOperation the corporative operation
	 * @return the previous custody commission
	 */
	public CustodyCommission getPreviousCustodyCommission(Long corporativeOperation){
		CustodyCommission previousCustodyCommission = null;
		try{
			String strQuery = 
					"select cc " +
					"from CustodyCommission cc " +
					"where " +
					"cc.corporativeOperation.idCorporativeOperationPk = :corporativeOperation ";

			Query query = em.createQuery(strQuery,CustodyCommission.class);
			query.setParameter("corporativeOperation",corporativeOperation);
			previousCustodyCommission = (CustodyCommission) query.getSingleResult();

		}catch(NoResultException x){
			x.printStackTrace();
		}

		return previousCustodyCommission;
	}

	/**
	 * Delete previous custody calculation.
	 *
	 * @param custodyCommissionID the custody commission id
	 * @throws ServiceException the service exception
	 */
	public void deletePreviousCustodyCalculation(List<Long> custodyCommissionID) throws ServiceException{

		Query query = em.createQuery("delete from CustodyCommissionDetail ccd where ccd.custodyCommission.idCustodyCommissionPk in (:custodyID) ");
		query.setParameter("custodyID", custodyCommissionID);
		query.executeUpdate();

		query = em.createQuery("delete from CustodyCommission cc where cc.idCustodyCommissionPk in (:custodyID) ");
		query.setParameter("custodyID", custodyCommissionID);
		query.executeUpdate();
	}

	/**
	 * Gets the custody commission id.
	 *
	 * @param corporativeList the corporative list
	 * @return the custody commission id
	 * @throws ServiceException the service exception
	 */
	public List<Long> getCustodyCommissionID(List<Long> corporativeList) throws ServiceException{

		List<Long> lstCustodyCommission = new ArrayList<Long>();
		String strQuery = 
				"select cc.idCustodyCommissionPk " +
				"from CustodyCommission cc " +
				"where " +
				"cc.corporativeOperation.idCorporativeOperationPk in (:corporativesOperation) ";

		Query query = em.createQuery(strQuery,Long.class);
		query.setParameter("corporativesOperation",corporativeList);
		lstCustodyCommission = query.getResultList();

		return lstCustodyCommission;
	}

	/**
	 * Gets the security periodicity.
	 *
	 * @param corporativeOperation the corporative operation
	 * @return the security periodicity
	 * @throws ServiceException the service exception
	 */
	public Integer getSecurityPeriodicity(Long corporativeOperation) throws ServiceException{

		Integer periodicity = null;
		BigDecimal bdPeriodicity = null;
		String strQuery = "select s.PERIODICITY "
				+ "from security s, CORPORATIVE_OPERATION co "
				+ "where co.ID_ORIGIN_ISIN_CODE_FK = s.ID_ISIN_CODE_PK "
				+ "and co.ID_CORPORATIVE_OPERATION_PK = :idCorporative ";

		Query query = em.createNativeQuery(strQuery);
		query.setParameter("idCorporative",corporativeOperation);
		bdPeriodicity = (BigDecimal) query.getSingleResult();

		return Integer.parseInt(bdPeriodicity.toString());
	}

	/**
	 * Gets the corporative result.
	 *
	 * @param corporativeID the corporative id
	 * @return the corporative result
	 */
	@SuppressWarnings("unchecked")
	public List<CorporativeProcessResult> getCorporativeResult(Long corporativeID){

		List<CorporativeProcessResult> lstReturn = new ArrayList<CorporativeProcessResult>();
		String strQuery = 
				"select cpr from "
				+ "CorporativeProcessResult cpr inner join fetch cpr.holderAccount "
				+ "where "
				+ "cpr.corporativeOperation.idCorporativeOperationPk = :corporativeID "
				+ "and cpr.indOriginTarget = :destiny ";

		Query query = em.createQuery(strQuery,CorporativeProcessResult.class);
		query.setParameter("corporativeID",corporativeID);
		query.setParameter("destiny",CorporateProcessConstants.BALANCE_DESTINY);
		lstReturn = query.getResultList();

		return lstReturn;
	}

	/**
	 * Gets the holder account stock.
	 *
	 * @param corporativeID the corporative id
	 * @return the holder account stock
	 */
	public List<Object[]> getHolderAccountStock(Long corporativeID){

		List<Object[]> lstHolderAccountBalance = new ArrayList<Object[]>();
		String strQuery = 
				"select distinct scb.holderAccount.idHolderAccountPk ,scb.totalBalance "
				+ "from "
				+ "CorporativeOperation co, StockCalculationProcess scp, "
				+ "CorporativeProcessResult cpr, StockCalculationBalance scb "
				+ "where "
				+ "co.idCorporativeOperationPk = :idCorporative "
				+ "and trunc(co.cutoffDate) = trunc(scp.cutoffDate) "
				+ "and trunc(co.registryDate) = trunc(scp.registryDate) "
				+ "and co.securities.idIsinCodePk = scp.security.idIsinCodePk "
				+ "and scp.stockClass = :stockClass "
				+ "and scp.stockType = :stockType "
				+ "and cpr.corporativeOperation.idCorporativeOperationPk = co.idCorporativeOperationPk "
				+ "and scb.stockCalculationProcess.idStockCalculationPk = scp.idStockCalculationPk ";

		Query query = em.createQuery(strQuery,Object[].class);
		query.setParameter("idCorporative",corporativeID);
		query.setParameter("stockClass",StockClassType.NORMAL.getCode());
		query.setParameter("stockType",StockType.CORPORATIVE.getCode());
		lstHolderAccountBalance = query.getResultList();

		return lstHolderAccountBalance;
	}

	/**
	 * Gets the coupon creation date.
	 *
	 * @param corporativeID the corporative id
	 * @return the coupon creation date
	 */
	public Date getCouponCreationDate(Long corporativeID){

		Date couponCreationDate = null;
		String strQuery = 
				"select pic.beginingDate "
				+ "from ProgramInterestCoupon pic, CorporativeOperation co "
				+ "where "
				+ "co.programInterestCoupon.idProgramInterestPk = pic.idProgramInterestPk "
				+ "and co.idCorporativeOperationPk = :corporativeID ";

		Query query = em.createQuery(strQuery,Object[].class);
		query.setParameter("corporativeID",corporativeID);
		couponCreationDate = (Date) query.getSingleResult();

		return couponCreationDate;
	}

	/**
	 * Gets the movements.
	 *
	 * @param securityCode the security code
	 * @param registryDate the registry date
	 * @return the movements
	 */
	public List<Object[]> getMovements(String securityCode, Date registryDate){
		List<Object[]> lstMovements = new ArrayList<Object[]>();
		String strQuery = 
					"select " +
					"mt.indMovementClass," +													//[0]
					"sum(ham.movementQuantity), " +												//[1]
					"trunc(ham.movementDate), " +												//[2]
					"ham.holderAccountBalance.holderAccount.idHolderAccountPk, " +				//[3]
					"s.currentNominalValue " + 													//[4]
					"from HolderAccountMovement ham, MovementType mt, Security s " +
					"where " +
					"mt.idMovementTypePk = ham.movementType.idMovementTypePk " +
					"and ham.holderAccountBalance.security.idIsinCodePk = :security " +
					"and trunc(ham.movementDate) between :registryDate and :today " +
					"and ham.holderAccountBalance.security.idIsinCodePk = s.idIsinCodePk " +
					"group by " +
					"mt.indMovementClass,trunc(ham.movementDate)," +
					"ham.holderAccountBalance.holderAccount.idHolderAccountPk " +
					"order by ham.holderAccountBalance.holderAccount.idHolderAccountPk,ham.movementDate asc ";

		Query query = em.createQuery(strQuery,Object[].class);
		query.setParameter("security",securityCode);
		query.setParameter("registryDate",registryDate);
		query.setParameter("today",CommonsUtilities.currentDate());
		lstMovements = query.getResultList();

		return lstMovements;
	}
}