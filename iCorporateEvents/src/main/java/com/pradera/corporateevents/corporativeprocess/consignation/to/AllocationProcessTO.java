package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.commons.utils.GenericDataModel;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AllocationProcessTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 31/10/2014
 */
public class AllocationProcessTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id allocation process. */
	private Long idAllocationProcess;
	
	/** The allocated process quantity. */
	private BigDecimal allocatedProcessQuantity;
	
	/** The allocated process amount. */
	private BigDecimal allocatedProcessAmount;
	
	/** The allocate process state. */
	private String allocateProcessState;
	
	/** The allocate process state. */
	private Integer idAllocateProcessState;
	
	/** The tax included. */
	private String taxIncluded;
	
	/** The currency. */
	private String currency;
	
	/** The id currency. */
	private Integer idCurrency;
	
	/** The bank. */
	private String bank;
	
	/** The bank id. */
	private Long bankID;
	
	/** The holder quantity. */
	private String holderQuantity;
	
	/** The bank amount. */
	private BigDecimal bankAmount;

	/** The lst allocation corporative. */
	private GenericDataModel<AllocationCorporativeDetailTO> lstAllocationCorporative;

	private Integer haveInstitutionCashAccount;

	private Integer paymentType;

	private String haveInstitutionCashAccountDesc;

	private String paymentTypeDesc;

	private String issuer;
	
	/** The issuer mnemonic. */
	private String issuerMnemonic;
	
	/** The issuer description. */
	private String issuerDescription;
	
	/**
	 * Gets the id allocation process.
	 *
	 * @return the id allocation process
	 */
	public Long getIdAllocationProcess() {
		return idAllocationProcess;
	}
	
	/**
	 * Sets the id allocation process.
	 *
	 * @param idAllocationProcess the new id allocation process
	 */
	public void setIdAllocationProcess(Long idAllocationProcess) {
		this.idAllocationProcess = idAllocationProcess;
	}
	
	/**
	 * Gets the allocated process quantity.
	 *
	 * @return the allocated process quantity
	 */
	public BigDecimal getAllocatedProcessQuantity() {
		return allocatedProcessQuantity;
	}
	
	/**
	 * Sets the allocated process quantity.
	 *
	 * @param allocatedProcessQuantity the new allocated process quantity
	 */
	public void setAllocatedProcessQuantity(BigDecimal allocatedProcessQuantity) {
		this.allocatedProcessQuantity = allocatedProcessQuantity;
	}
	
	/**
	 * Gets the allocated process amount.
	 *
	 * @return the allocated process amount
	 */
	public BigDecimal getAllocatedProcessAmount() {
		return allocatedProcessAmount;
	}
	
	/**
	 * Sets the allocated process amount.
	 *
	 * @param allocatedProcessAmount the new allocated process amount
	 */
	public void setAllocatedProcessAmount(BigDecimal allocatedProcessAmount) {
		this.allocatedProcessAmount = allocatedProcessAmount;
	}
	
	/**
	 * Gets the allocate process state.
	 *
	 * @return the allocate process state
	 */
	public String getAllocateProcessState() {
		return allocateProcessState;
	}
	
	/**
	 * Sets the allocate process state.
	 *
	 * @param allocateProcessState the new allocate process state
	 */
	public void setAllocateProcessState(String allocateProcessState) {
		this.allocateProcessState = allocateProcessState;
	}
	
	/**
	 * Gets the lst allocation corporative.
	 *
	 * @return the lst allocation corporative
	 */
	public GenericDataModel<AllocationCorporativeDetailTO> getLstAllocationCorporative() {
		return lstAllocationCorporative;
	}
	
	/**
	 * Sets the lst allocation corporative.
	 *
	 * @param lstAllocationCorporative the new lst allocation corporative
	 */
	public void setLstAllocationCorporative(
			GenericDataModel<AllocationCorporativeDetailTO> lstAllocationCorporative) {
		this.lstAllocationCorporative = lstAllocationCorporative;
	}
	
	/**
	 * Gets the tax included.
	 *
	 * @return the tax included
	 */
	public String getTaxIncluded() {
		return taxIncluded;
	}
	
	/**
	 * Sets the tax included.
	 *
	 * @param taxIncluded the new tax included
	 */
	public void setTaxIncluded(String taxIncluded) {
		this.taxIncluded = taxIncluded;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public String getBank() {
		return bank;
	}
	
	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}
	
	/**
	 * Gets the holder quantity.
	 *
	 * @return the holder quantity
	 */
	public String getHolderQuantity() {
		return holderQuantity;
	}
	
	/**
	 * Sets the holder quantity.
	 *
	 * @param holderQuantity the new holder quantity
	 */
	public void setHolderQuantity(String holderQuantity) {
		this.holderQuantity = holderQuantity;
	}
	
	/**
	 * Gets the bank amount.
	 *
	 * @return the bank amount
	 */
	public BigDecimal getBankAmount() {
		return bankAmount;
	}
	
	/**
	 * Sets the bank amount.
	 *
	 * @param bankAmount the new bank amount
	 */
	public void setBankAmount(BigDecimal bankAmount) {
		this.bankAmount = bankAmount;
	}
	
	/**
	 * Gets the bank id.
	 *
	 * @return the bank id
	 */
	public Long getBankID() {
		return bankID;
	}
	
	/**
	 * Sets the bank id.
	 *
	 * @param bankID the new bank id
	 */
	public void setBankID(Long bankID) {
		this.bankID = bankID;
	}
	
	/**
	 * Gets the id currency.
	 *
	 * @return the id currency
	 */
	public Integer getIdCurrency() {
		return idCurrency;
	}
	
	/**
	 * Sets the id currency.
	 *
	 * @param idCurrency the new id currency
	 */
	public void setIdCurrency(Integer idCurrency) {
		this.idCurrency = idCurrency;
	}

	/**
	 * Gets the id allocate process state.
	 *
	 * @return the id allocate process state
	 */
	public Integer getIdAllocateProcessState() {
		return idAllocateProcessState;
	}

	/**
	 * Sets the id allocate process state.
	 *
	 * @param idAllocateProcessState the new id allocate process state
	 */
	public void setIdAllocateProcessState(Integer idAllocateProcessState) {
		this.idAllocateProcessState = idAllocateProcessState;
	}

	public Integer getHaveInstitutionCashAccount() {
		return haveInstitutionCashAccount;
	}

	public void setHaveInstitutionCashAccount(Integer haveInstitutionCashAccount) {
		this.haveInstitutionCashAccount = haveInstitutionCashAccount;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public String getHaveInstitutionCashAccountDesc() {
		return haveInstitutionCashAccountDesc;
	}

	public void setHaveInstitutionCashAccountDesc(String haveInstitutionCashAccountDesc) {
		this.haveInstitutionCashAccountDesc = haveInstitutionCashAccountDesc;
	}

	public String getPaymentTypeDesc() {
		return paymentTypeDesc;
	}

	public void setPaymentTypeDesc(String paymentTypeDesc) {
		this.paymentTypeDesc = paymentTypeDesc;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}

	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}

	public String getIssuerDescription() {
		return issuerDescription;
	}

	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}
	
}