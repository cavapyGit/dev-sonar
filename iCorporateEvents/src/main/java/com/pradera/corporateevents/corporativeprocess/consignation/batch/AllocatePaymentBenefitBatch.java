package com.pradera.corporateevents.corporativeprocess.consignation.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.corporateevents.corporativeprocess.consignation.facade.AllocationPaymentBenefitServiceFacade;
import com.pradera.corporateevents.corporativeprocess.consignation.service.AllocationPaymentBenefitServiceBean;
import com.pradera.corporateevents.corporativeprocess.consignation.to.RegisterAllocationProcess;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.BenefitPaymentAllocation;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * Bachero que ejecuta el proceso preliminar de la consignacion de pagos.
 *
 * @author PraderaTechnologies
 */
@RequestScoped
@BatchProcess(name="AllocatePaymentBenefitBatch")
public class AllocatePaymentBenefitBatch implements JobExecution,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	@Inject
	PraderaLogger logger;
	
	/** The allocation facade. */
	@EJB
	AllocationPaymentBenefitServiceFacade allocationFacade;
	
	/** The allocation benefit service. */
	@EJB
	AllocationPaymentBenefitServiceBean allocationBenefitService;
	
	/**
	 * Inicio de Bachero que ejecuta el proceso preliminar de la consignacion de pagos.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger){
		logger.info("AllocatePaymentBenefitBatch: Starting the process ...");
		try {
			List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
			String corporatives = StringUtils.EMPTY;
			boolean commissionExcluded = false;
			Integer currency = null;
			String issuer = StringUtils.EMPTY;
			String security = StringUtils.EMPTY;
			String userId = StringUtils.EMPTY;
			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
				if(processLoggerDetail.getParameterName().equals("corporatives")){
					corporatives = processLoggerDetail.getParameterValue();
				}else if(processLoggerDetail.getParameterName().equals("commissionExcluded")){
					String booleanCommissionExcluded = processLoggerDetail.getParameterValue();
					if(BooleanType.YES.getCode().toString().equals(booleanCommissionExcluded)){
						commissionExcluded = true;
					}
				}else if(processLoggerDetail.getParameterName().equals("currency")){
					currency = Integer.valueOf(processLoggerDetail.getParameterValue());
				}else if(processLoggerDetail.getParameterName().equals("issuer")){
					issuer = processLoggerDetail.getParameterValue();
				}else if(processLoggerDetail.getParameterName().equals("security")){
					security = processLoggerDetail.getParameterValue();
				}else if(processLoggerDetail.getParameterName().equals("userId")){
					userId = processLoggerDetail.getParameterValue();
				}
			}

			List<Long> lstCorporativeID = new ArrayList<Long>();
			for (String corporativeID : corporatives.split(",")){
				lstCorporativeID.add(new Long(corporativeID));
			}
			
			
			RegisterAllocationProcess registerAllocationProcess = allocationFacade.registerAllocationProcess(lstCorporativeID, commissionExcluded,currency,issuer,security,userId);
			
			allocationFacade.createPaymentAllocationDetail(registerAllocationProcess);
			
			allocationFacade.updateBenefitPayment(registerAllocationProcess.getIdBenefitPaymentPK(), commissionExcluded, lstCorporativeID);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		logger.info("AllocatePaymentBenefitBatch: Finishing the process ...");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		return true;
	}
}