package com.pradera.corporateevents.corporativeprocess.process.executors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.services.remote.billing.BillingConexionService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.corporateevents.corporativeprocess.process.CorporateExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcess;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BuyerOperationsFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.ChangesFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.OperationsFixer;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CorporateProcessServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CustodyCommissionServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.constants.Constants;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.CorporativeResultDetail;
import com.pradera.model.corporatives.CustodyCommissionDetail;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.corporatives.ReportCorporativeResult;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockoperation.UnblockMarketFactOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.SecuritiesClassType;
import com.pradera.model.custody.type.UnblockMarketOperationStateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.guarantees.type.GuaranteeType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;

// TODO: Auto-generated Javadoc
/**
 * Clase que ejecuta la amortizacion parcial o al vencimiento.
 *
 * @author PraderaTechnologies
 */
@CorporateProcess(CorporateExecutor.AMORTIZATION)
@Performance
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@LocalBean
public class AmortizationCorporateProcessExecutor implements CorporateProcessExecutor {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The corporate process service. */
	@EJB
	CorporateProcessServiceBean corporateProcessService;
	
	/** The fixer. */
	@Inject
	private BalanceFixer fixer;
	
	/** The document fixer. */
	@Inject
	private DocumentsFixer documentFixer;
	
	/** The operation fixer. */
	@Inject
	private OperationsFixer operationFixer;
	
	/** The buyer operation fixer. */
	@Inject
	private BuyerOperationsFixer buyerOperationFixer;

	/** The changes fixer. */
	@Inject
	private ChangesFixer changesFixer;
	
	/** The executor component service bean. */
	@Inject
	Instance<AccountsComponentService> executorComponentServiceBean;
	
	/** The securities component service bean. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentServiceBean;
	
	/** The custody commission service. */
	@EJB
	CustodyCommissionServiceBean custodyCommissionService;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The billing service producer. */
	@Inject
	Instance<BillingConexionService> billingServiceProducer;
	
	/** The sc. */
	@Resource
    SessionContext sc;
	
	/** The amortization corporate process executor. */
	AmortizationCorporateProcessExecutor amortizationCorporateProcessExecutor;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
    public void init(){
        this.amortizationCorporateProcessExecutor = this.sc.getBusinessObject(AmortizationCorporateProcessExecutor.class);
    }

	/**
	 * Metodo que ejecuta el preliminar de la amortizacion parcial o al vencimiento.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executePreliminarProcess(CorporativeOperation corpOperation) throws Exception{
		
		log.info("started Stock preliminary process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		
		//declaramos variables
		boolean flag = false;
		Long stockProcess = null, reportStockprocess = null;	
		
		//obtenemos proceso corporativo a procesar
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		
		//se obtiene id del proceso de stock Normal
		stockProcess = corporateExecutorService.getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
				corpOperation.getSecurities().getIdSecurityCodePk()
				, StockStateType.FINISHED.getCode(), StockClassType.NORMAL.getCode(), StockType.CORPORATIVE.getCode());
		
		//se obtiene id del proceso de stock Report
		reportStockprocess = corporateExecutorService.getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
				corpOperation.getSecurities().getIdSecurityCodePk()
				, StockStateType.FINISHED.getCode(), StockClassType.REPORT.getCode(), StockType.CORPORATIVE.getCode());
		
		//validamos si existe stock NORMAL
		if (stockProcess != null && stockProcess > 0) {
			
			//eliminamos preliminares previos
			corporateExecutorService.deletePreviousPreliminaryInformation(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,true,true);
			
			//obtenemos monto nominal
			BigDecimal nominalAmount = null;
			List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramAmortizationCoupon(corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getDeliveryDate());
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
				Long idProgramAmortizationPk = (Long)lstIdProgramInterestCoupon.get(0);
				ProgramAmortizationCoupon objProgramAmortizationCoupon = (ProgramAmortizationCoupon)corporateExecutorService.getProgramAmortizationCoupon(idProgramAmortizationPk);
				nominalAmount = objProgramAmortizationCoupon.getAmortizationAmount();
			}else{
				corpOperation.setDeliveryFactor(CorporateProcessUtils.divide(corpOperation.getDeliveryFactor(), CorporateProcessConstants.AMOUNT_ROUND));
				nominalAmount = CorporateProcessUtils.multiply(corpOperation.getDeliveryFactor(), corpOperation.getSecurities().getInitialNominalValue());
			}			
			
			//calculamos y guardamos nuevo factor de retiro de valores
			BigDecimal retirementFactor = null;
			if(Validations.validateIsNotNull(corpOperation.getRetirementFactor())){
				corpOperation.setRetirementFactor(CorporateProcessUtils.divide(corpOperation.getRetirementFactor(), CorporateProcessConstants.AMOUNT_ROUND));
				retirementFactor = corpOperation.getRetirementFactor();
			}			
			
			//obtenemos factor de entrega para certificados de depositos bancarios
			boolean balanceEffected = corpOperation.getIndAffectBalance() != null && corpOperation.getIndAffectBalance()==1?true:false;
			if(SecuritiesClassType.CERTIFICADOS_DE_DEPOSITOS_BANCARIOS.getCode().equals(corpOperation.getSecurities().getSecurityClass())){
				retirementFactor = BigDecimal.ONE;
				balanceEffected = true;
				if(corpOperation.getDeliveryFactor().compareTo(BigDecimal.ONE) <= 0){
					nominalAmount = corpOperation.getSecurities().getCurrentNominalValue();
				}
			}
			
			//si el corporativo tiene el indicador de penalida activo entonces se recalcula factor de entrega
			BigDecimal penaltyFactor = BigDecimal.ZERO;
			if(BooleanType.YES.getCode().equals(corpOperation.getIndPenality())){
				if(Validations.validateIsNotNullAndNotEmpty(corpOperation.getPenalityRate())){
					corpOperation.setPenalityRate(CorporateProcessUtils.divide(corpOperation.getPenalityRate(), CorporateProcessConstants.AMOUNT_ROUND));
					penaltyFactor = CorporateProcessUtils.multiply(corpOperation.getPenalityRate(), corpOperation.getSecurities().getCurrentNominalValue());
					nominalAmount = nominalAmount.add(penaltyFactor);
				}
			}
			
			//se asigna factor de retiro igual a monto nominal en caso q no exista
			if(retirementFactor == null){
				retirementFactor = nominalAmount;
			}
			
			//obtenemos data de las cuentas 
			//(codigo alternativo - promedio total de saldo - tipo titular - nacionalidad)
			List<Object[]> distinctHolders  = corporateExecutorService.getDistinctHoldersForAmortization(stockProcess);
			//ejecutamos algoritmo de procesos preliminar
			if (distinctHolders != null && distinctHolders.size() > 0) {				
				this.amortizationCorporateProcessExecutor.executePreliminarProcessAlgorithm(distinctHolders, corpOperation, nominalAmount, stockProcess,
						reportStockprocess, retirementFactor, processType, balanceEffected, loggerUser);
				flag=true;
			}
		}
		return flag;
	}
	
	/**
	 * Metodo que ejecuta algoritmo preliminar de la amortizacion.
	 *
	 * @param distinctHolders the distinct holders
	 * @param corpOperation the corp operation
	 * @param nominalAmount the nominal amount
	 * @param stockProcess the stock process
	 * @param reportStockprocess the report stockprocess
	 * @param retirementFactor the retirement factor
	 * @param processType the process type
	 * @param balanceEffected the balance effected
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executePreliminarProcessAlgorithm(List<Object[]> distinctHolders,CorporativeOperation corpOperation,BigDecimal nominalAmount 
			,Long stockProcess,Long reportStockprocess,BigDecimal retirementFactor,ImportanceEventType processType 
			,boolean balanceEffected,LoggerUser loggerUser) throws ServiceException{
			
		//declaramos variables
		CorporativeProcessResult totalProcessResultExchange = null , cashTotalProcessResult = null;
		List<ReportCorporativeResult> reportOperations = null;
		List<BlockCorporativeResult> blockDocuments = null;
		DailyExchangeRates exchangeRateDay = null;
		List<CorporativeProcessResult> changedResults = new ArrayList<CorporativeProcessResult>();
		List<CorporativeProcessResult> changedCashResults = new ArrayList<CorporativeProcessResult>();
		List<Object[]> cashHolderAccounts = null;
		Map<Long,BigDecimal> mpHolderAccountTax = new HashMap<Long, BigDecimal>();
		
		//validamos factor de impuesto
		BigDecimal taxFactor = BigDecimal.ZERO;
		BigDecimal totalTax = BigDecimal.ZERO;
		boolean calculateTax = false;
		if (corpOperation.getIndTax() != null && corpOperation.getIndTax().equals(BooleanType.YES.getCode())) {
			calculateTax = true;
			//obtenemos factor de impuesto 		
			if(	Validations.validateIsNotNull(corpOperation.getTaxFactor()) 
					&& Validations.validateIsNotNullAndPositive(corpOperation.getTaxFactor())){
				taxFactor = corpOperation.getTaxFactor();
			}
		}
			
		//obtener data de operaciones de participantes unificados
		Map<Long, Object[]> uniParticipants = corporateExecutorService.getUnifiedParticipants(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getRegistryDate());
		
		//obtener data de operaciones de cambio de titularidad
		Map<Long, List<Object[]>> changeOwnership = corporateExecutorService.getChangeofOwnerShipSucession(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getRegistryDate());
		
		//si la moneda del procesos es ufv o dmv entonces obtenemos tipo de cambio
		if(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
				corpOperation.getCurrency().equals(CurrencyType.DMV.getCode())){	
			exchangeRateDay = parameterServiceBean.getDayExchangeRate(corpOperation.getExchangeDateRate(),corpOperation.getCurrency());
		}
		
		//recorremos la lista de cuentas para el proceso
		for (Object[] arrObj : distinctHolders) {
			
			BigDecimal totalBalance = BigDecimal.ZERO;
			
			//obtenemos datos de la cuenta (codigo alterno)
			String holderAlternativeCode = (String) arrObj[0];
				
			//obtenemos saldo total por cuenta
			totalBalance = CorporateProcessUtils.multiply(((BigDecimal) arrObj[1]),CorporateProcessConstants.AMOUNT_ROUND);
			
			//no validamos si tipo de cuenta es MANCOMUNADO xq los calcula en el query... 
				
			//calculamos monto total con beneficio por cuenta
			BigDecimal roundedTotal = CorporateProcessUtils.calculateByFactor(totalBalance,nominalAmount,corpOperation.getIndRound());
	
			//creamos proceso resultado totales y guardamos monto totales con benefico por cuenta
			if(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					corpOperation.getCurrency().equals(CurrencyType.DMV.getCode())){				
				totalProcessResultExchange = new CorporativeProcessResult();
				totalProcessResultExchange.setTotalBalance(roundedTotal);
				totalProcessResultExchange.setCorporativeOperation(corpOperation);
				totalProcessResultExchange.setSecurity(corpOperation.getSecurities());
				roundedTotal = CorporateProcessUtils.calculateByFactor(roundedTotal, exchangeRateDay.getBuyPrice(),corpOperation.getIndRound());	
				cashTotalProcessResult = new CorporativeProcessResult();
				cashTotalProcessResult.setTotalBalance(roundedTotal);
				cashTotalProcessResult.setCorporativeOperation(corpOperation);
				cashTotalProcessResult.setSecurity(corpOperation.getSecurities());
			}else{
				cashTotalProcessResult = new CorporativeProcessResult();
				cashTotalProcessResult.setTotalBalance(roundedTotal);
				cashTotalProcessResult.setCorporativeOperation(corpOperation);
				cashTotalProcessResult.setSecurity(corpOperation.getSecurities());
			}

			//obtenemos cuenta matriz con sus saldos respectivos
			List<Object[]> holderAccounts =corporateExecutorService.getStockCalculationBalances(stockProcess, holderAlternativeCode);
			
			if (holderAccounts != null && holderAccounts.size() > 0) {
				
				//TODO Holder Adjustments need to consider for the fixing total
				List<CorporativeProcessResult> holderAccResults = null;
				
				//obtenemos lista de cuentas matrizes recontruidos
				holderAccResults = fixer.fillProcessResultsForOrigin(holderAccounts, corpOperation);
				
				//obtenemos lista de cuentas para  cuentas matrizes recontruidos con beneficios
				cashHolderAccounts = holderAccounts;
				
				//verificamos si aplica impuesto y calculamos el impuesto en decimales
				BigDecimal tax = null, tmpAmountNominal = null , realTaxFactor = null;
				if(calculateTax){
					realTaxFactor = BigDecimal.ONE.subtract(CorporateProcessUtils.divide(taxFactor, CorporateProcessConstants.AMOUNT_ROUND));
				}
				
				//obtenemos lista de cuentas matrizes recontruidos con beneficios auxiliar
				List<CorporativeProcessResult> cashHolderAccRes = fixer.fixBalances(cashHolderAccounts, corpOperation, nominalAmount, 
						corpOperation.getIndRound(), true, false, true, processType, exchangeRateDay);							
				
				for (Iterator<CorporativeProcessResult> itHolder=holderAccResults.iterator(); itHolder.hasNext();) {
					CorporativeProcessResult corpProcessResults =  itHolder.next();
					
					CorporativeProcessResult cashCorpProcResult = null;
					for(CorporativeProcessResult tmpCashCorpProcResult : cashHolderAccRes){
						if(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || corpOperation.getCurrency().equals(CurrencyType.DMV.getCode())){
							if(tmpCashCorpProcResult.getHolderAccount().getIdHolderAccountPk().equals(corpProcessResults.getHolderAccount().getIdHolderAccountPk())
									&& tmpCashCorpProcResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY) 
									&& corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_ORIGIN)){
								cashCorpProcResult = tmpCashCorpProcResult;
								break;
							}else if(tmpCashCorpProcResult.getHolderAccount().getIdHolderAccountPk().equals(corpProcessResults.getHolderAccount().getIdHolderAccountPk())
									&& tmpCashCorpProcResult.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_DESTINY) 
									&& corpProcessResults.getIndOriginTarget().equals(CorporateProcessConstants.BALANCE_ORIGIN)){
								cashCorpProcResult = tmpCashCorpProcResult;
								break;
							}
						}else if(tmpCashCorpProcResult.getHolderAccount().getIdHolderAccountPk().equals(corpProcessResults.getHolderAccount().getIdHolderAccountPk())){
							cashCorpProcResult = tmpCashCorpProcResult;
							break;
						}						
					}									
							
					//inicializamos en cero variable auxiliar para obtener el monto impuesto
					tax=BigDecimal.ZERO;
					
					//guardamos monto nominal temporal para operaciones de reporte y operaciones de bloqueo
					if(tmpAmountNominal == null){
						tmpAmountNominal = nominalAmount;
					}	
				
					//obtenemos monto beneficio total de cuenta matriz
					if(Validations.validateIsNotNull(cashCorpProcResult)){
						totalBalance = cashCorpProcResult.getTotalBalance();
					}
					
					//verificamos si aplica impuesto por cuenta matriz
					if(calculateTax){			
						Long idHolderAccount = cashCorpProcResult.getHolderAccount().getIdHolderAccountPk();
						String idSecurity = cashCorpProcResult.getSecurity().getIdSecurityCodePk();
						Security objSecurity = corporateExecutorService.getSecurity(idSecurity);
						List<Holder> lstHolder = corporateExecutorService.getListHolder(idHolderAccount);
						mpHolderAccountTax = new HashMap<Long, BigDecimal>();
						//recorremos lista de titulares y discriminamos
						for(Holder objHolder : lstHolder){
							//verificamos si titular de la cuenta aplica impuesto
							if(CorporateProcessUtils.isTaxExempt(objSecurity, objHolder).equals(BooleanType.YES.getCode()) && 
									!mpHolderAccountTax.containsKey(cashCorpProcResult.getHolderAccount().getIdHolderAccountPk())){
								if(itHolder.hasNext()){
									BigDecimal newTotalBalance = CorporateProcessUtils.calculateByFactor(cashCorpProcResult.getTotalBalance(), realTaxFactor, corpOperation.getIndRound());
									tax = totalBalance.subtract(newTotalBalance);
									tax = CorporateProcessUtils.divide(tax, CorporateProcessConstants.AMOUNT_ROUND);
									totalTax = totalTax.add(tax);		
								}else{
									BigDecimal newTotalBalance = CorporateProcessUtils.calculateByFactor(cashTotalProcessResult.getTotalBalance(), realTaxFactor, corpOperation.getIndRound());
									tax = totalBalance.subtract(newTotalBalance);
									tax = tax.subtract(totalTax);
									tax = CorporateProcessUtils.divide(tax, CorporateProcessConstants.AMOUNT_ROUND);
								}
								mpHolderAccountTax.put(cashCorpProcResult.getHolderAccount().getIdHolderAccountPk(),realTaxFactor);
								cashCorpProcResult.setTaxAmount(tax);	
							}
						}
					}			
					
					//validamos si cuenta tiene saldos bloqueados
					if(CorporateProcessUtils.hasBlockBalances(corpProcessResults)){
						List<Object[]> blockedDocs =corporateExecutorService.getBlockStockCalculationDetails(stockProcess, corpProcessResults.getParticipant().getIdParticipantPk(), 
								corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
								corpProcessResults.getSecurity().getIdSecurityCodePk());
					
						if(blockedDocs != null && blockedDocs.size() > 0){
							List<Object[]> cashBlockDocuments = null;
							if(balanceEffected){
								blockDocuments = documentFixer.fixDocuments(corpProcessResults, blockedDocs, retirementFactor,
									corpOperation.getIndRound(), false, false, false, fixer, processType,exchangeRateDay);
								cashBlockDocuments = new ArrayList<Object[]>();
								for(BlockCorporativeResult blockCorp : blockDocuments){
									Object[] objCash = new Object[14];
									objCash[0] = blockCorp.getBlockType();
									objCash[1] = blockCorp.getBlockedBalance();
									objCash[2] = blockCorp.getBlockOperation().getIdBlockOperationPk();
									if(Validations.validateIsNotNull(blockCorp.getHolder())){
										objCash[3] = blockCorp.getHolder().getIdHolderPk();
									}else{
										objCash[3] = null;
									}
									objCash[4] = blockCorp.getIndCashDividend();
									objCash[5] = blockCorp.getIndContributionReturn();
									objCash[6] = blockCorp.getIndInterest();
									objCash[7] = blockCorp.getIndStockDividend();
									objCash[8] = blockCorp.getIndSuscription();
									objCash[9] = blockCorp.getParticipant().getIdParticipantPk();
									objCash[10] = LevelAffectationType.BLOCK.getCode();
									if(blockCorp.isUnblocked()){
										objCash[11] = AffectationStateType.UNBLOCKED.getCode();
									}else{
										objCash[11] = AffectationStateType.BLOCKED.getCode();
									}
									objCash[12] = blockCorp.getIndAmortization();
									objCash[13] = blockCorp.getIndRescue();
									cashBlockDocuments.add(objCash);
								}											
							}else{
								blockDocuments = documentFixer.fillOriginBlockCorporativeResult(blockedDocs, corpProcessResults,exchangeRateDay);
								cashBlockDocuments = blockedDocs;
							}										
							corpProcessResults.setBlockCorporativeResults(blockDocuments);
							
							blockDocuments = documentFixer.fixDocuments(cashCorpProcResult, cashBlockDocuments, tmpAmountNominal,
									corpOperation.getIndRound(), false, false, true, fixer, processType,exchangeRateDay);
							cashCorpProcResult.setBlockCorporativeResults(blockDocuments);
						}
					}
					//validamos si cuenta tiene saldos reporto (rol venta)
					if(CorporateProcessUtils.hasReportBalances(corpProcessResults)){									
						List<Object[]> reportOper = corporateExecutorService.getStockReportOperations(reportStockprocess, corpProcessResults.getParticipant().getIdParticipantPk(), 
								corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
								corpProcessResults.getSecurity().getIdSecurityCodePk());
						if(reportOper != null && reportOper.size() > 0){
							List<Object[]> cashReportOper = null;
							if(balanceEffected){
								reportOperations = operationFixer.fixOperations(corpProcessResults, reportOper, retirementFactor, 
										corpOperation.getIndRound(), false, false,false, fixer,exchangeRateDay);	
								cashReportOper = new ArrayList<Object[]>();
								for(ReportCorporativeResult repOperation : reportOperations){
									Object[] objCash = new Object[5];
									objCash[0] = repOperation.getGuaranteeType();
									objCash[1] = repOperation.getOperationRole();
									objCash[2] = repOperation.getGuaranteeBalance();
									objCash[3] = repOperation.getHolderAccountOperation().getIdHolderAccountOperationPk();
									objCash[4] = repOperation.getMechanismOperation().getIdMechanismOperationPk();
									cashReportOper.add(objCash);												
								}
							}else{
								reportOperations = operationFixer.fillOriginOperations(reportOper, corpProcessResults,exchangeRateDay);	
								cashReportOper = reportOper;
							}
							corpProcessResults.setReportCorporativeResults(reportOperations);
							
							reportOperations = operationFixer.fixOperations(cashCorpProcResult, cashReportOper, tmpAmountNominal, 
									corpOperation.getIndRound(), false, true, true, fixer,exchangeRateDay);
							cashCorpProcResult.setReportCorporativeResults(reportOperations);
						}
						
					}
					
					//actualizar resultado de cuentas matrices por unificacion de participante o cambio de titularidad para ORIGEN
					List<CorporativeProcessResult> changesCorp = changesFixer.getUpdatedResult(new ArrayList<CorporativeProcessResult>(), corpProcessResults, 
							loggerUser, uniParticipants, changeOwnership, documentFixer, fixer, corpOperation.getIndRound(), false, null);
										
					//si existe actualizacion resultado de cuentas matrices por unificacion de participante o cambio de titularidad
					if(changesCorp != null && !changesCorp.isEmpty()){
						changedResults.addAll(changesCorp);
					}else{
						//actualizamos saldos de bloqueos dependiendo los indicadores de pago
						corpProcessResults = documentFixer.updateBlockDocumentsEffections(corpProcessResults, false);
						corporateExecutorService.create(corpProcessResults, loggerUser);
					}
					
					//actualizar resultado de cuentas matrices por unificacion de participante o cambio de titularidad para DESTINO
					changesCorp = changesFixer.getUpdatedResult(new ArrayList<CorporativeProcessResult>(), cashCorpProcResult, 
							loggerUser, uniParticipants, changeOwnership, documentFixer, fixer, corpOperation.getIndRound(), true, null);
														
					//si existe actualizacion resultado de cuentas matrices por unificacion de participante o cambio de titularidad
					if(changesCorp != null && !changesCorp.isEmpty()){
						changesCorp = CorporateProcessUtils.divideForCash(changesCorp);
						changedCashResults.addAll(changesCorp);
					}else{
						//actualizamos saldos de bloqueos dependiendo los indicadores de pago
						documentFixer.updateBlockDocumentsEffections(cashCorpProcResult, true);
						CorporateProcessUtils.divideForCash(cashCorpProcResult);
						corporateExecutorService.create(cashCorpProcResult, loggerUser);
					}
					
				}
				holderAccounts.clear();
				holderAccounts = null;							
			}
		}
		if(!changedResults.isEmpty() || !changedCashResults.isEmpty()){
			for(CorporativeProcessResult corpRes : changedResults ){
				if(corpRes.getTotalBalance().compareTo(BigDecimal.ZERO) > 0 || corpRes.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
					documentFixer.updateBlockDocumentsEffections(corpRes, false);							
					corporateExecutorService.updateOrCreate(corpRes, loggerUser);
				}
			}
			for(CorporativeProcessResult corpRes : changedCashResults ){
				if(corpRes.getTotalBalance().compareTo(BigDecimal.ZERO) > 0 || corpRes.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
					documentFixer.updateBlockDocumentsEffections(corpRes, true);							
					corporateExecutorService.updateOrCreate(corpRes, loggerUser);
				}
			}
			corporateExecutorService.updateBlockCorporateForResult(corpOperation.getIdCorporativeOperationPk(), loggerUser);
		}
		
		//validamos si cuenta tiene saldos reporto (rol compra)
		List<Object[]> sumReported = null, buyerOperations = null; 		
		sumReported = corporateExecutorService.getSellerBuyyerSumReportOperations(reportStockprocess, 
				corpOperation.getIdCorporativeOperationPk(),
				GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
				ParticipantRoleType.SELL.getCode(), ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.IND_NO_REMAINDER, 
				CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,CorporateProcessConstants.BALANCE_DESTINY,CorporateProcessConstants.EXCHANGE_DESTINY);
		buyerOperations = corporateExecutorService.getBuyerStockReportOperations(reportStockprocess, 
				GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(),
				ParticipantRoleType.BUY.getCode());
		
		if(sumReported != null && !sumReported.isEmpty()){
			List<Object[]> cashReportOper = null;
			if(balanceEffected){
				reportOperations = buyerOperationFixer.fixOperations(sumReported, buyerOperations, corpOperation, 
					corpOperation.getIndRound(), false, false, false, fixer,exchangeRateDay);
				cashReportOper = new ArrayList<Object[]>();
				for(ReportCorporativeResult repOperation : reportOperations){
					Object[] objCash = new Object[7];
					objCash[0] = repOperation.getGuaranteeType();								
					objCash[1] = repOperation.getGuaranteeBalance();
					objCash[2] = repOperation.getParticipant().getIdParticipantPk();
					objCash[3] = repOperation.getHolderAccount().getIdHolderAccountPk();
					objCash[4] = repOperation.getSecurity().getIdSecurityCodePk();
					objCash[5] = repOperation.getHolderAccountOperation().getIdHolderAccountOperationPk();
					objCash[6] = repOperation.getMechanismOperation().getIdMechanismOperationPk();
					cashReportOper.add(objCash);												
				}
			}else{			
				reportOperations = buyerOperationFixer.fillOriginOperations(buyerOperations, corpOperation,exchangeRateDay);
				cashReportOper = buyerOperations;
			}
			corporateExecutorService.saveAll(reportOperations, loggerUser);
			
			List<ReportCorporativeResult> cashReportOperations =  buyerOperationFixer.fixOperations(sumReported,cashReportOper, corpOperation, 
					corpOperation.getIndRound(), false, true, true, fixer,exchangeRateDay);			
			
			corporateExecutorService.saveAll(cashReportOperations, loggerUser);	
			
			corporateExecutorService.updateReportedBalanceofCorporateResults(corpOperation.getIdCorporativeOperationPk(), 
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
					ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			corporateExecutorService.updateReportedOperationsofCorporateResult(corpOperation.getIdCorporativeOperationPk(), 
					GuaranteeType.PRINCIPAL.getCode(), GuaranteeType.STOCK_DIVIDENDS_PRINCIPAL.getCode(), 
					ParticipantRoleType.BUY.getCode(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		}

		//REALIZE THE COMMISSION CALCULATION
		Map<Long,BigDecimal> mpHolderAccountCustody = new HashMap<Long, BigDecimal>();
		mpHolderAccountCustody = executeCommissionCalculationProcess(corpOperation.getIdCorporativeOperationPk());

		//CREATE THE TAX-CUSTODY AMOUNTS DETAILED BY BALANCES
		reprocessCorporativeResults(corpOperation,mpHolderAccountCustody,loggerUser,mpHolderAccountTax,calculateTax);

		corporateExecutorService.updateHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO, CorporateProcessConstants.IND_YES, CorporateProcessConstants.IND_YES, loggerUser);
		
		corpOperation.setPaymentAmount(corporateExecutorService.getTotalBalance(corpOperation.getIdCorporativeOperationPk()));
		corpOperation.setIssuerConfirmedAmount(corpOperation.getPaymentAmount());
		corpOperation.setCustodyAmount(BigDecimal.ZERO);
		corpOperation.setCommissionAmount(BigDecimal.ZERO);
		corpOperation.setTaxAmount(corporateExecutorService.getTaxAmount(corpOperation.getIdCorporativeOperationPk()));
		corpOperation.setExchangeAmount(corporateExecutorService.getExchangeAmount(corpOperation.getIdCorporativeOperationPk()));
		corpOperation.setIdStockCalculation(stockProcess);
		corporateExecutorService.updateCorporativeCustodyAmounts(corpOperation);
	}
	
	/**
	 * Metodo que registra saldos preliminares de la amortizacion.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @param retirementOperation the retirement operation
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void registerCorporateResultSecurities(CorporativeOperation corpOperation,LoggerUser loggerUser,RetirementOperation retirementOperation) throws ServiceException{
		for(RetirementDetail retirementDetail:retirementOperation.getRetirementDetails()){
			//Registro de Valores
			CorporativeProcessResult corporativeProcessResult = new CorporativeProcessResult();
			corporativeProcessResult.setHolderAccount(retirementDetail.getHolderAccount());
			corporativeProcessResult.setSecurity(retirementOperation.getSecurity());
			corporativeProcessResult.setParticipant(retirementDetail.getParticipant());
			corporativeProcessResult.setIndRemanent(corpOperation.getIndRemanent());
			corporativeProcessResult.setTotalBalance(retirementDetail.getAvailableBalance());
			corporativeProcessResult.setAvailableBalance(retirementDetail.getAvailableBalance());
			corporativeProcessResult.setTransitBalance(BigDecimal.ZERO);
			corporativeProcessResult.setPawnBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBanBalance(BigDecimal.ZERO);
			corporativeProcessResult.setOtherBlockBalance(BigDecimal.ZERO);
			corporativeProcessResult.setAcreditationBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReserveBalance(BigDecimal.ZERO);
			corporativeProcessResult.setOppositionBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBuyBalance(BigDecimal.ZERO);
			corporativeProcessResult.setSellBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReportingBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReportedBalance(BigDecimal.ZERO);
			corporativeProcessResult.setMarginBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBorrowerBalance(BigDecimal.ZERO);
			corporativeProcessResult.setLenderBalance(BigDecimal.ZERO);
			corporativeProcessResult.setLoanableBalance(BigDecimal.ZERO);	
			corporativeProcessResult.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			corporativeProcessResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_ORIGIN);
			corporativeProcessResult.setCorporativeOperation(corpOperation);
			corporateExecutorService.create(corporativeProcessResult, loggerUser);
		}
	}
	
	/**
	 * Metodo que registra montos preliminares de la amortizacion.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @param retirementOperation the retirement operation
	 * @param cashDelFactor the cash del factor
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void registerCorporateResultCash(CorporativeOperation corpOperation,LoggerUser loggerUser
			,RetirementOperation retirementOperation,BigDecimal cashDelFactor) throws ServiceException{
		for(RetirementDetail retirementDetail:retirementOperation.getRetirementDetails()){
			//Registro de Efectivo
			CorporativeProcessResult corporativeProcessResult = new CorporativeProcessResult();
			corporativeProcessResult.setHolderAccount(retirementDetail.getHolderAccount());
			corporativeProcessResult.setSecurity(retirementOperation.getSecurity());
			corporativeProcessResult.setParticipant(retirementDetail.getParticipant());
			corporativeProcessResult.setIndRemanent(corpOperation.getIndRemanent());
			if(BooleanType.YES.getCode().equals(corpOperation.getIndPenality())){
				corpOperation.setPenalityRate(CorporateProcessUtils.divide(corpOperation.getPenalityRate(), CorporateProcessConstants.AMOUNT_ROUND));
				BigDecimal penalityFactor = CorporateProcessUtils.multiply(corpOperation.getPenalityRate(), corpOperation.getSecurities().getCurrentNominalValue());
				corporativeProcessResult.setPenalityAmount(CorporateProcessUtils.calculateByFactor(retirementDetail.getAvailableBalance(), penalityFactor,  corpOperation.getIndRound()));
			}
			corporativeProcessResult.setTotalBalance(CorporateProcessUtils.calculateByFactor(retirementDetail.getAvailableBalance(), cashDelFactor,  corpOperation.getIndRound()));
			corporativeProcessResult.setAvailableBalance(CorporateProcessUtils.calculateByFactor(retirementDetail.getAvailableBalance(), cashDelFactor,  corpOperation.getIndRound()));
			corporativeProcessResult.setTransitBalance(BigDecimal.ZERO);
			corporativeProcessResult.setPawnBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBanBalance(BigDecimal.ZERO);
			corporativeProcessResult.setOtherBlockBalance(BigDecimal.ZERO);
			corporativeProcessResult.setAcreditationBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReserveBalance(BigDecimal.ZERO);
			corporativeProcessResult.setOppositionBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBuyBalance(BigDecimal.ZERO);
			corporativeProcessResult.setSellBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReportingBalance(BigDecimal.ZERO);
			corporativeProcessResult.setReportedBalance(BigDecimal.ZERO);
			corporativeProcessResult.setMarginBalance(BigDecimal.ZERO);
			corporativeProcessResult.setBorrowerBalance(BigDecimal.ZERO);
			corporativeProcessResult.setLenderBalance(BigDecimal.ZERO);
			corporativeProcessResult.setLoanableBalance(BigDecimal.ZERO);
			corporativeProcessResult.setIndProcessed(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			corporativeProcessResult.setIndOriginTarget(CorporateProcessConstants.BALANCE_DESTINY);
			corporativeProcessResult.setCorporativeOperation(corpOperation);
			corporateExecutorService.create(corporativeProcessResult, loggerUser);
		}			
	}

	
	/**
	 * Metodo que ejecuta el definitivo de la amortizacion parcial o al vencimiento.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executeDefinitiveProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started definitive process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		// verificamos si la amortizacion afecta saldo (amortizacion AL vencimiento o indicador afecta saldo activo o redencion anticipada)
		if(corpOperation.getIndAffectBalance().equals(CorporateProcessConstants.IND_YES) || 
				(Validations.validateIsNotNull(corpOperation.getSecurities().getCapitalPaymentModality()) &&
						CapitalPaymentModalityType.AT_MATURITY.getCode().equals(corpOperation.getSecurities().getCapitalPaymentModality())) 
						|| (corpOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.EARLY_PAYMENT.getCode()) && 
								BigDecimal.ZERO.compareTo(corpOperation.getNewNominalValue()) == 0)){
			
			//obtenemos lista de participante involucrados al proceso definitivo
			List<Object> participants = corporateExecutorService.getDistinctParticpantsForDefinitive(corpOperation.getIdCorporativeOperationPk(),
					CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
			//validamos lista de participante involucrados al proceso definitivo
			if(participants !=null && !participants.isEmpty()){
				//recorremos lista de participante involucrados al proceso definitivo
				for(Object part : participants){
					Long participant = (Long)part;
					//ejecutamos componente de saldos (nivel de cuenta matriz)
					this.amortizationCorporateProcessExecutor.executeDefinitiveProcessParticipant(corpOperation, loggerUser, participant, processType);
				}
			}
			
			//ejecutamos componente de saldos (nivel de valor)
			this.amortizationCorporateProcessExecutor.executeDefinitiveProcessSecurities(corpOperation, processType, loggerUser);
			//si es una redencion anticipada le cambiamos la clase de evento (Amortizacion) para que realize sus respectivos movimientos
			if(corpOperation.getCorporativeEventType().getCorporativeEventType().equals(ImportanceEventType.EARLY_PAYMENT.getCode())){
				corpOperation.getCorporativeEventType().setCorporativeEventType(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
			}
			

			//actualiza estado del cupon
			List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramAmortizationCoupon(corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getDeliveryDate());
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
				corporateExecutorService.updateProgramAmortizationCouponState(lstIdProgramInterestCoupon, ProgramScheduleStateType.EXPIRED.getCode(), loggerUser);
			}	
		}else{
			//amortizacion no afecta saldo
			this.amortizationCorporateProcessExecutor.executeDefinitiveProcessAccounts(corpOperation, loggerUser, processType);
			//actualiza estado del cupon
			List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramAmortizationCoupon(corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getDeliveryDate());
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
				corporateExecutorService.updateProgramAmortizationCouponState(lstIdProgramInterestCoupon, ProgramScheduleStateType.EXPIRED.getCode(), loggerUser);
			}	
		}
		return true;
	}
	
	/**
	 * Metodo que ejecuta algoritmo definitivo de la amortizacion.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @param processType the process type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessAccounts(CorporativeOperation corpOperation,LoggerUser loggerUser,ImportanceEventType processType) throws ServiceException{
		//process must update on delivery date the security nomial value as it's batch this will taken care by automatic batch.
		corporateExecutorService.updateSecurity(corpOperation.getSecurities().getIdSecurityCodePk(), null, 
				null, corpOperation.getNewNominalValue(), null, null,loggerUser);
		
		fixer.setLoggerUserForTransationRegistry();
		SecuritiesComponentService securitiesComponentService = securitiesComponentServiceBean.get();
		
		//obtenemos total de saldos
//		BigDecimal totalBalance =(BigDecimal) corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
//				CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,
//				CorporateProcessConstants.BALANCE_ORIGIN, null);
		
		//issue 983: antes de castear se recupera en un Object para que no salga el error:
		//Caused by: java.lang.ClassCastException: java.lang.Integer cannot be cast to java.math.BigDecimal
		Object obj = corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,
				CorporateProcessConstants.BALANCE_ORIGIN, null);
		System.out.println("::: TIPO DE OBJETO: "+obj.getClass());
		BigDecimal totalBalance = new BigDecimal(obj.toString());

		//obtenemos monto nominal
		BigDecimal nominalAmount = null;
		List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramAmortizationCoupon(corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getDeliveryDate());
		if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
			Long idProgramAmortizationPk = (Long)lstIdProgramInterestCoupon.get(0);
			ProgramAmortizationCoupon objProgramAmortizationCoupon = (ProgramAmortizationCoupon)corporateExecutorService.getProgramAmortizationCoupon(idProgramAmortizationPk);
			nominalAmount = objProgramAmortizationCoupon.getAmortizationAmount();
		}else{
			corpOperation.setDeliveryFactor(CorporateProcessUtils.divide(corpOperation.getDeliveryFactor(), CorporateProcessConstants.AMOUNT_ROUND));
			nominalAmount = CorporateProcessUtils.multiply(corpOperation.getDeliveryFactor(), corpOperation.getSecurities().getInitialNominalValue());
		}	

		/**
		 * If Amortizacion Parcial, que no genere movimientos de valor
		 */
		//registramos operacion de valor
		SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, totalBalance.multiply(nominalAmount),
				BigDecimal.ZERO, corpOperation.getSecurities().getIdSecurityCodePk());
		secOpe.setSecurities(corpOperation.getSecurities());
		corporateExecutorService.create(secOpe, loggerUser);	
		//ejecutamos es componente de mueve saldo a nivel de valor
		securitiesComponentService.executeSecuritiesComponent(CorporateProcessUtils.getSecuritiesComponent(processType, secOpe,null, null, null));
		
		corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), 
				null, true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null, loggerUser);
	}
	
	/**
	 * Metodo que ejecuta algoritmo preliminar de la amortizacion para la escision.
	 *
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessSecurities(CorporativeOperation corpOperation,ImportanceEventType processType,LoggerUser loggerUser) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		SecuritiesComponentService securitiesComponentService = securitiesComponentServiceBean.get();
		//obtenemos operacion de retiro
//		RetirementOperation retirementOperation = corporateExecutorService.findRetirementOperation(corpOperation.getIdCorporativeOperationPk());
		
		//obtenemos total de saldos
		BigDecimal totalBalance =(BigDecimal) corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_PROCESSED,
				CorporateProcessConstants.BALANCE_ORIGIN, null);
				
		//process must update on delivery date the security nomial value as it's batch this will taken care by automatic batch.
		corporateExecutorService.updateSecurity(corpOperation.getSecurities().getIdSecurityCodePk(), null, 
				null, corpOperation.getNewNominalValue(), null, null,loggerUser);
		
		SecuritiesOperation secOpe = null;
		//verifica operacion de retiro
//		if(Validations.validateIsNull(retirementOperation)){
			//registramos operacion de valor
			secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, corpOperation.getSecurities().getCirculationAmount(), 
					corpOperation.getSecurities().getCirculationBalance(), corpOperation.getSecurities().getIdSecurityCodePk());
			secOpe.setSecurities(corpOperation.getSecurities());
			corporateExecutorService.create(secOpe, loggerUser);	
			// Recuperando objeto a ser enviado a ejecutar componente Securities
			SecuritiesComponentTO securitiesComponentTO = CorporateProcessUtils.getSecuritiesComponent(processType, secOpe, 
					corpOperation.getSecurities().getCirculationBalance().subtract(corpOperation.getSecurities().getDesmaterializedBalance()), corpOperation.getSecurities().getDesmaterializedBalance(), null);
			
			// verificando si el valor fue parte de un desprendimiento de cupon
			// con el indicador IND_IS_CUPON 
			if(corpOperation.getSecurities().getIndIsCoupon() == 1){
				// setenado flag paa que no se ejecute el componente de emision
				securitiesComponentTO.setIssuanceExecute(false);
			}
			
			//ejecutamos es componente de mueve saldo a nivel de valor
			securitiesComponentService.executeSecuritiesComponent(securitiesComponentTO);
//		}else{
//			//registramos operacion de valor
//			secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, totalBalance.multiply(corpOperation.getNewNominalValue()), 
//					totalBalance, corpOperation.getSecurities().getIdSecurityCodePk());
//			secOpe.setSecurities(corpOperation.getSecurities());
//			corporateExecutorService.create(secOpe, loggerUser);				
//			// Recuperando objeto a ser enviado a ejecutar componente Securities
//			SecuritiesComponentTO securitiesComponentTO = CorporateProcessUtils.getSecuritiesComponent(processType, 
//					secOpe, totalBalance, totalBalance, null);
//			
//			// verificando si el valor fue parte de un desprendimiento de cupon
//			// con el indicador IND_IS_CUPON 
//			if(corpOperation.getSecurities().getIndIsCoupon() == 1){
//				// setenado flag paa que no se ejecute el componente de emision
//				securitiesComponentTO.setIssuanceExecute(false);
//			}
//			
//			//ejecutamos es componente de mueve saldo a nivel de valor
//			securitiesComponentService.executeSecuritiesComponent(securitiesComponentTO);
//			//actualizamos datos del valor (fecha vencimiento y estado a REDIMIDO)
//			corporateExecutorService.updateSecurity(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getDeliveryDate(), 
//					SecurityStateType.REDEEMED.getCode(), null, null,null, loggerUser);
//		}											
		
		//actualiza estado de hecho de importancia
		List<Long> lstIdInterestCorporative= corporateExecutorService.getListIdCorporativeOperation(corpOperation.getSecurities().getIdSecurityCodePk(), ImportanceEventType.INTEREST_PAYMENT.getCode());
		if (Validations.validateListIsNotNullAndNotEmpty(lstIdInterestCorporative)) {
			corporateExecutorService.updateCorporativeOperationState(lstIdInterestCorporative, CorporateProcessStateType.ANNULLED.getCode(), loggerUser);
		}
		//actualiza estado del cupon
		List<Long> lstIdProgramInterestCoupon= corporateExecutorService.getListIdProgramInterestCoupon(corpOperation.getSecurities().getIdSecurityCodePk());
		if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
			corporateExecutorService.updateProgramInterestCouponState(lstIdProgramInterestCoupon, ProgramScheduleStateType.CANCELED.getCode(), loggerUser);
		}

	//verifica que exista operacion de retiro
//	if(Validations.validateIsNotNull(retirementOperation)){
//		//verifica si existe cuenta matriz con saldo
//		boolean blExistBalance = corporateExecutorService.findExistBalance(corpOperation.getSecurities().getIdSecurityCodePk());
//		//no existe cuentas con saldo
//		if(!blExistBalance){
//			//obtenemos todos los corporativos activos asociados al valor
//			List<CorporativeOperation> lstCorporativeOperation = corporateExecutorService.findCorpOperationRegistered(corpOperation.getSecurities().getIdSecurityCodePk());
//			//validamos lista de corporativos activos asociados al valor
//			if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)){
//				//recorremos lista corporativos activos asociados al valor
//				for(CorporativeOperation corporativeOperation:lstCorporativeOperation){
//					//actualizamos estado de corporativo (ANULADO)
//					corporativeOperation.setState(CorporateProcessStateType.ANNULLED.getCode());
//					corporateExecutorService.update(corporativeOperation, loggerUser);
//				}
//			}					
//		}					
//	}else{
		//Actualizamos estado de valor (VENCIDO)
		corporateExecutorService.updateSecurity(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getDeliveryDate(), 
				SecurityStateType.EXPIRED.getCode(), null, null,null ,loggerUser);
		//obtenemos todos los corporativos activos asociados al valor
		List<CorporativeOperation> lstCorporativeOperation = corporateExecutorService.findCorpOperationRegistered(corpOperation.getSecurities().getIdSecurityCodePk());
		//validamos lista de corporativos activos asociados al valor
		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)){
			//recorremos lista corporativos activos asociados al valor
			for(CorporativeOperation corporativeOperation:lstCorporativeOperation){
				//actualizamos estado de corporativo (ANULADO)
				corporativeOperation.setState(CorporateProcessStateType.ANNULLED.getCode());
				corporateExecutorService.update(corporativeOperation, loggerUser);
			}
		}	
//	}
	//obtenemos emision
	String issuanceFind = corporateExecutorService.findIssuanceProcess(corpOperation.getSecurities().getIdSecurityCodePk());
	boolean updateIsuaance = corporateExecutorService.isUpdateIssuance(issuanceFind);
	if (updateIsuaance){
		Issuance issuance = corporateExecutorService.findIssuance(corpOperation.getSecurities().getIdSecurityCodePk());	
		issuance.setStateIssuance(IssuanceStateType.EXPIRED.getCode());
		issuance.setExpirationDate(corpOperation.getDeliveryDate());
		corporateExecutorService.update(issuance, loggerUser);
	}
	

	}
	
	/**
	 * Metodo que ejecuta algoritmo definitivo de la amortizacion a nivel de cuentas.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @param participant the participant
	 * @param processType the process type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessParticipant(CorporativeOperation corpOperation,LoggerUser loggerUser,Long participant,ImportanceEventType processType) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();		
		//Get reference to component service
		AccountsComponentService accountsComponentService = executorComponentServiceBean.get();
		//obtenemos operaciones de bloqueos asociados a la cuenta matriz
		List<Object[]> blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForUnblockDefinitive(corpOperation.getIdCorporativeOperationPk(), participant, corpOperation.getSecurities().getIdSecurityCodePk(), 
				CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_ORIGIN);
		if(blockDocuments != null && !blockDocuments.isEmpty()){
			for(Object[] data : blockDocuments){
				//registramos solicitud de desbloqueo
				UnblockRequest unblockRequest = documentFixer.prepareUnblockOperation(data, loggerUser.getUserName());
				BigDecimal quantity = (BigDecimal)data[2];
				Long blockOperationId = (Long)data[3];
				HolderAccountBalanceTO account = new HolderAccountBalanceTO();
				account.setIdHolderAccount((Long)data[5]);
				account.setIdParticipant((Long)data[6]);
				account.setIdSecurityCode((String)data[7]);
				account.setStockQuantity(quantity);
				account.setFinalDate(corpOperation.getDeliveryDate());
				List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
				accounts.add(account);
				Long refBlockOperationId = null;
				BlockOperation blockOperation = corporateExecutorService.find(BlockOperation.class, blockOperationId);
				List<BlockMarketFactOperation> blockMarketFatcs = blockOperation.getBlockMarketFactOperations();
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
					//movements marketfact for each blockmarketfact
					MarketFactAccountTO mktFact;
					for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
						if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
							mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(blockMarketFact.getMarketDate());
							mktFact.setMarketPrice(blockMarketFact.getMarketPrice());
							mktFact.setMarketRate(blockMarketFact.getMarketRate());
							mktFact.setQuantity(blockMarketFact.getActualBlockBalance());
							account.getLstMarketFactAccounts().add(mktFact);
						}
					}
				}			
				
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					UnblockMarketFactOperation unblockMarketFact;
					for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
						if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
							unblockMarketFact = new UnblockMarketFactOperation();
							unblockMarketFact.setUnblockOperation(unblockRequest.getUnblockOperation().get(0));
							unblockMarketFact.setUnblockQuantity(blockMarketFact.getActualBlockBalance());
							unblockMarketFact.setUnblockMarketState(UnblockMarketOperationStateType.REGISTER.getCode());
							unblockMarketFact.setRegistryUser(loggerUser.getUserName());
							unblockMarketFact.setRegistryDate(CommonsUtilities.currentDateTime());
							unblockMarketFact.setMarketDate(blockMarketFact.getMarketDate());
							unblockMarketFact.setMarketPrice(blockMarketFact.getMarketPrice());
							unblockMarketFact.setMarketRate(blockMarketFact.getMarketRate());
							unblockRequest.getUnblockOperation().get(0).getUnblockMarketFactOperations().add(unblockMarketFact);
						}
					}
				}
				corporateExecutorService.create(unblockRequest, loggerUser);
				corporateExecutorService.updateBlockOperation(blockOperationId, quantity, true, true, AffectationStateType.UNBLOCKED.getCode(), null, loggerUser);
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
						if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
							blockMarketFact.setActualBlockBalance(new BigDecimal(0));
							corporateExecutorService.update(blockMarketFact);
						}
					}
				}
				refBlockOperationId = blockOperationId;
				blockOperationId = unblockRequest.getUnblockOperation().get(0).getIdUnblockOperationPk();
				
				//movements unblock origin balances
				AccountsComponentTO accountsComponentTO = CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
						blockOperationId, refBlockOperationId, null, null, processType, accounts, null, CorporateProcessUtils.getBlockBalance((Integer)data[0],(Integer)data[8]),idepositarySetup.getIndMarketFact());
				//change default operationType to unblockOperationType
				accountsComponentTO.setIdOperationType(CorporateProcessUtils.getOperationUnblock(CorporateProcessUtils.getBlockBalance((Integer)data[0],(Integer)data[8]), accountsComponentTO.getIdOperationType()));
				accountsComponentService.executeAccountsComponent(accountsComponentTO);
				
			}
		}
		
		//generamos de movimientos de saldos a nivel de cuenta matriz
		accountsComponentService.executeAccountsComponent(fixer.generateAccountComponent(
				corporateExecutorService.getCorporateResultsForDefinitiveProcess(corpOperation.getIdCorporativeOperationPk(), participant, 
						CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_ORIGIN), processType, corpOperation));
		//actualizamos datos del procesos definitivo
		corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), 
				participant, true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null, loggerUser);

	}

	/**
	 * Metodo que reprocesa preliminar de la amortizacion.
	 *
	 * @param corporativeOperation the corporative operation
	 * @param mpHolderAccountCustody the mp holder account custody
	 * @param loggerUser the logger user
	 * @param mpHolderAccountTax the mp holder account tax
	 * @param blTax the bl tax
	 * @throws ServiceException the service exception
	 */
	private void reprocessCorporativeResults(CorporativeOperation corporativeOperation, Map<Long,BigDecimal> mpHolderAccountCustody, LoggerUser loggerUser
			,Map<Long,BigDecimal> mpHolderAccountTax, boolean blTax) throws ServiceException{
		List<CorporativeProcessResult> lstCorporateResults = corporateExecutorService.getCorporativeResults(corporativeOperation.getIdCorporativeOperationPk());
		List<HolderAccountAdjusment> adjustmentList = corporateExecutorService.getCorporativeAdjustments(corporativeOperation.getIdCorporativeOperationPk());

		if(Validations.validateListIsNotNullAndNotEmpty(lstCorporateResults)){
			for(CorporativeProcessResult corporativeResult : lstCorporateResults){

				Long corporateHolderAccount = corporativeResult.getHolderAccount().getIdHolderAccountPk();
				if(mpHolderAccountCustody.containsKey(corporateHolderAccount)){
					corporativeResult.setCustodyAmount(mpHolderAccountCustody.get(corporateHolderAccount));
				}
				
				if(mpHolderAccountTax.containsKey(corporateHolderAccount) && blTax){
					BigDecimal taxFactor = mpHolderAccountTax.get(corporateHolderAccount);
					BigDecimal taxAmount = CorporateProcessUtils.multiply10Decimals(taxFactor,corporativeResult.getTotalBalance());
					taxAmount = corporativeResult.getTotalBalance().subtract(taxAmount);
					corporativeResult.setTaxAmount(taxAmount);
				}
				
				if(Validations.validateListIsNotNullAndNotEmpty(adjustmentList)){
					for(HolderAccountAdjusment adjustment : adjustmentList){
						if(adjustment.getHolderAccount().getIdHolderAccountPk().equals(corporativeResult.getHolderAccount().getIdHolderAccountPk())){
							corporativeResult = processAdjustments(corporativeResult,adjustment);
						}
					}
				}
								
				BigDecimal taxAmount = corporativeResult.getTaxAmount();
				if(Validations.validateIsNull(taxAmount)){
					taxAmount = BigDecimal.ZERO;
				}		
				
				BigDecimal custodyAmount = corporativeResult.getCustodyAmount();
				if(Validations.validateIsNull(custodyAmount)){
					custodyAmount = BigDecimal.ZERO;
				}

				BigDecimal netTotalAmount = corporativeResult.getTotalBalance();
				BigDecimal netAvailableAmount = corporativeResult.getAvailableBalance();
				BigDecimal netPawnAmount = corporativeResult.getPawnBalance();
				BigDecimal netBanAmount = corporativeResult.getBanBalance();
				BigDecimal netOtherBlockAmount = corporativeResult.getOtherBlockBalance();
				BigDecimal netReserveAmount = corporativeResult.getReserveBalance();
				BigDecimal netOppositionAmount = corporativeResult.getOppositionBalance();
				BigDecimal netReportingAmount = corporativeResult.getReportingBalance();
				BigDecimal netReportedAmount = corporativeResult.getReportedBalance();
				BigDecimal netMarginAmount = corporativeResult.getMarginBalance();

				if(BigDecimal.ZERO.compareTo(netTotalAmount) == 0){
					continue;
				}
				//1	CREATE THE CORPORATIVE RESULT DETAIL
				BigDecimal availableFactor = CorporateProcessUtils.divide(netAvailableAmount, netTotalAmount, 2);
				BigDecimal pawnFactor = CorporateProcessUtils.divide(netPawnAmount, netTotalAmount, 2);
				BigDecimal banFactor = CorporateProcessUtils.divide(netBanAmount, netTotalAmount, 2);
				BigDecimal otherBlockFactor = CorporateProcessUtils.divide(netOtherBlockAmount, netTotalAmount, 2);
				BigDecimal reserveFactor = CorporateProcessUtils.divide(netReserveAmount, netTotalAmount, 2);
				BigDecimal oppositionFactor = CorporateProcessUtils.divide(netOppositionAmount, netTotalAmount, 2);
				BigDecimal reportingFactor = CorporateProcessUtils.divide(netReportingAmount, netTotalAmount, 2);
				BigDecimal reportedFactor = CorporateProcessUtils.divide(netReportedAmount, netTotalAmount, 2);
				BigDecimal marginFactor = CorporateProcessUtils.divide(netMarginAmount, netTotalAmount, 2);

				//CALCULATE THE TAX AMOUNTS BY EACH BALANCE
				BigDecimal marginTax = CorporateProcessUtils.multiply(taxAmount, marginFactor, 2);
				BigDecimal pawnTax = CorporateProcessUtils.multiply(taxAmount, pawnFactor, 2);
				BigDecimal banTax = CorporateProcessUtils.multiply(taxAmount, banFactor, 2);
				BigDecimal otherBlockTax = CorporateProcessUtils.multiply(taxAmount, otherBlockFactor, 2);
				BigDecimal reserveTax = CorporateProcessUtils.multiply(taxAmount, reserveFactor, 2);
				BigDecimal oppositionTax = CorporateProcessUtils.multiply(taxAmount, oppositionFactor, 2);
				BigDecimal reportingTax = CorporateProcessUtils.multiply(taxAmount, reportingFactor, 2);
				BigDecimal reportedTax = CorporateProcessUtils.multiply(taxAmount, reportedFactor, 2);
				BigDecimal availableTax = taxAmount.subtract(
						(marginTax.add(pawnTax.add(banTax.add(otherBlockTax.add(reserveTax.add(oppositionTax.add(reportingTax.add(reportedTax)))))))));

				//CALCULATE THE CUSTODY AMOUNTS BY EACH BALANCE
				BigDecimal availableCustody = CorporateProcessUtils.multiply(custodyAmount, availableFactor, 2);
				BigDecimal pawnCustody = CorporateProcessUtils.multiply(custodyAmount, pawnFactor, 2);
				BigDecimal banCustody = CorporateProcessUtils.multiply(custodyAmount, banFactor, 2);
				BigDecimal otherBlockCustody = CorporateProcessUtils.multiply(custodyAmount, otherBlockFactor, 2);
				BigDecimal reserveCustody = CorporateProcessUtils.multiply(custodyAmount, reserveFactor, 2);
				BigDecimal oppositionCustody = CorporateProcessUtils.multiply(custodyAmount, oppositionFactor, 2);
				BigDecimal reportingCustody = CorporateProcessUtils.multiply(custodyAmount, reportingFactor, 2);
				BigDecimal reportedCustody = CorporateProcessUtils.multiply(custodyAmount, reportedFactor, 2);
				BigDecimal marginCustody = custodyAmount.subtract(
						(availableCustody.add(pawnCustody.add(banCustody.add(otherBlockCustody.add(reserveCustody.add(oppositionCustody.add(reportingCustody.add
								(reportedCustody)))))))));

				//CREATE THE DETAIL AMOUNT BY BALANCE
				CorporativeResultDetail corporativeDetail = new CorporativeResultDetail();
				corporativeDetail.setAvailableCustodyAmount(availableCustody);
				corporativeDetail.setAvailableTaxAmount(availableTax);
				corporativeDetail.setBanCustodyAmount(banCustody);
				corporativeDetail.setBanTaxAmount(banTax);
				corporativeDetail.setMarginCustodyAmount(marginCustody);
				corporativeDetail.setMarginTaxAmount(marginTax);
				corporativeDetail.setOppositionCustodyAmount(oppositionCustody);
				corporativeDetail.setOppositionTaxAmount(oppositionTax);
				corporativeDetail.setOtherCustodyAmount(otherBlockCustody);
				corporativeDetail.setOtherTaxAmount(otherBlockTax);
				corporativeDetail.setPawnCustodyAmount(pawnCustody);
				corporativeDetail.setPawnTaxAmount(pawnTax);
				corporativeDetail.setReportedCustodyAmount(reportedCustody);
				corporativeDetail.setReportedTaxAmount(reportedTax);
				corporativeDetail.setReportingCustodyAmount(reportingCustody);
				corporativeDetail.setReportingTaxAmount(reportingTax);
				corporativeDetail.setReserveCustodyAmount(reserveCustody);
				corporativeDetail.setReserveTaxAmount(reserveTax);
				corporativeDetail.setCorporativeProcessResult(corporativeResult);
				corporativeDetail.setCorporativeOperation(corporativeOperation);
				corporateExecutorService.create(corporativeDetail,loggerUser);

				corporateExecutorService.update(corporativeResult);
				
				corporateExecutorService.updateBlockCorporateForResult(corporativeOperation.getIdCorporativeOperationPk(), loggerUser);
			}
		}

		//UPDATE THE HOLDER ACCOUNT ADJUSTMENTS FOR NOT PROCESSING AGAIN
		corporateExecutorService.updateHolderAccountAdjustments(corporativeOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO, CorporateProcessConstants.IND_YES, CorporateProcessConstants.IND_YES, loggerUser);
	}

	/**
	 * Metodo que procesa ajuste del preliminar de la amortizacion.
	 *
	 * @param corporativeResult the corporative result
	 * @param adjustment the adjustment
	 * @return the corporative process result
	 */
	private CorporativeProcessResult processAdjustments(CorporativeProcessResult corporativeResult, HolderAccountAdjusment adjustment){
		//WE UPDATE THE TOTAL - AVAILABLE AMOUNTS BY THE DIFFERENCE MODIFIES IN THE ADJUSTMENT
		BigDecimal pawnBalance = BigDecimal.ZERO;
		BigDecimal banBalance = BigDecimal.ZERO;
		BigDecimal otherBlockBalance = BigDecimal.ZERO;
		BigDecimal oppositionBalance = BigDecimal.ZERO;
		BigDecimal reserveBalance = BigDecimal.ZERO;
		
		if(adjustment.getIndDeliverBenefit().equals(BooleanType.YES.getCode())){
			BigDecimal previousAmount = adjustment.getOldBenefitAmount();
			if(Validations.validateIsNull(previousAmount)){
				previousAmount = BigDecimal.ZERO;
			}
			BigDecimal newAmount = adjustment.getNewBenefitAmount();
			if(Validations.validateIsNull(newAmount)){
				newAmount = BigDecimal.ZERO;
			}

			pawnBalance = corporativeResult.getPawnBalance();
			banBalance = corporativeResult.getBanBalance();
			otherBlockBalance = corporativeResult.getOtherBlockBalance();
			oppositionBalance = corporativeResult.getOppositionBalance();
			reserveBalance = corporativeResult.getReserveBalance();

			if(previousAmount.compareTo(newAmount) == 1){
				BigDecimal difference = previousAmount.subtract(newAmount);
				corporativeResult.setTotalBalance(corporativeResult.getTotalBalance().subtract(difference));
			}else if(newAmount.compareTo(previousAmount) == 1){
				BigDecimal difference = newAmount.subtract(previousAmount);
				corporativeResult.setTotalBalance(corporativeResult.getTotalBalance().add(difference));
			}
			if(corporativeResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_DESTINY) || 
					corporativeResult.getIndOriginTarget().equals(CorporateProcessConstants.EXCHANGE_ORIGIN)){
				corporativeResult.setExchangeAmount(corporativeResult.getTotalBalance());
			}
			BigDecimal totalBalance = corporativeResult.getTotalBalance();
			BigDecimal blockBalance = reserveBalance.add(oppositionBalance.add(otherBlockBalance.add(banBalance.add(pawnBalance))));
			corporativeResult.setAvailableBalance(totalBalance.subtract(blockBalance));
		}
		
		//WE UPDATE THE TAX AMOUNT
		if(adjustment.getIndCollectTax().equals(BooleanType.YES.getCode())){
			corporativeResult.setTaxAmount(adjustment.getNewTax());
		}
		//WE UPDATE THE COMMISSION AMOUNT
		if(adjustment.getIndCollectCommissiones().equals(BooleanType.YES.getCode())){
			corporativeResult.setCustodyAmount(adjustment.getNewCommissiones());
		}
		return corporativeResult;
	}

	/**
	 * Metodo que calcula comision de custodia.
	 *
	 * @param corporativeOperation the corporative operation
	 * @return the map
	 */
	private Map<Long,BigDecimal> executeCommissionCalculationProcess(Long corporativeOperation){

		BigDecimal previousCommissionAmount = BigDecimal.ZERO;
		Map<Long,BigDecimal> mpHolderAccountCustody = new HashMap<Long, BigDecimal>();
		Long holderAccount, previousHolderAccount = null;

		List<CustodyCommissionDetail> lstHolderAccountCommission = custodyCommissionService.getCustodyCommissionDetail(corporativeOperation);
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountCommission)){
			for(CustodyCommissionDetail custodyCommissionDetail : lstHolderAccountCommission){
				holderAccount = custodyCommissionDetail.getHolderAccount().getIdHolderAccountPk();
				BigDecimal commissionAmount = custodyCommissionDetail.getCustodyAmount();

				if(Validations.validateIsNull(previousHolderAccount)){
					previousCommissionAmount = commissionAmount;
				}else{
					if(holderAccount.equals(previousHolderAccount)){
						previousCommissionAmount = previousCommissionAmount.add(commissionAmount);
					}else{

						mpHolderAccountCustody.put(previousHolderAccount,previousCommissionAmount);
						previousCommissionAmount = commissionAmount;
					}
				}
				previousHolderAccount = holderAccount;
			}
			mpHolderAccountCustody.put(previousHolderAccount,previousCommissionAmount);
		}
		return mpHolderAccountCustody;
	}

}