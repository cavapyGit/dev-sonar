package com.pradera.corporateevents.corporativeprocess.facade;

import java.math.BigDecimal;
//import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.StringUtils;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.corporateevents.corporativeprocess.process.CorporateEventProducer;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CorporateProcessServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.HolderAccountBalanceExp;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeSituationType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * Clase fachada de los procesos corporativos
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/06/2014
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
@Performance
public class CorporateProcessServiceFacade {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate process service. */
	@EJB
	CorporateProcessServiceBean corporateProcessService;
	
	/** The batch process service facade. */
	@EJB private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The batch service. */
	@EJB
	BatchServiceBean batchService;

	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	@EJB
	CorporateProcessServiceFacade corporateProcessFacade;
	
	CorporateProcessExecutor corpProcessExcutors;
	
	/** The corp process excutors. */
	@Inject
	CorporateEventProducer corporateEventProducer;
	
	/**
	 * Metodo que ejecuta el proceso preliminar.
	 *
	 * @param lstProcesses the lst processes
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public void executePreliminaryProcess(List<CorporativeOperation> lstProcesses) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
		}
		//validate Preliminary Process
		corporateProcessService.validatePreliminaryProcess(lstProcesses);
		for(CorporativeOperation operation : lstProcesses){
			 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType()) 
					 || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
				 //Validating if the Security has desmaterialized Balance
				 BigDecimal desmaterializedBalance = corporateProcessService.searchDesmaterializedBalance(operation.getSecurities().getIdSecurityCodePk());
				 if(desmaterializedBalance.compareTo(BigDecimal.ZERO) > 0 &&
						 hasBalance(operation.getSecurities().getIdSecurityCodePk())){
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_MANUAL.getCode());
					    Map<String, Object> param = new HashMap<String, Object>();				    
					    param.put("processid", operation.getIdCorporativeOperationPk());
					    param.put("userid", loggerUser.getUserName());		
					    param.put("corporateType", operation.getCorporativeEventType().getDescription());	
					    batchService.registerBatchTx(loggerUser.getUserName(), businessProcess, param);						
				 }else{
					 setDataForCorporativeProcessFail(operation);
				 }
			 }else{				
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_MANUAL.getCode());
				    Map<String, Object> param = new HashMap<String, Object>();				    
				    param.put("processid", operation.getIdCorporativeOperationPk());
				    param.put("userid", loggerUser.getUserName());		
				    param.put("corporateType", operation.getCorporativeEventType().getDescription());	
				    batchService.registerBatchTx(loggerUser.getUserName(), businessProcess, param);					
			 }
		}		
	}
	
	/**
	 * Metodo que valida el proceso corporativo de convertibilidad de bonos en acciones.
	 *
	 * @param idSecurity the id security
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateConvertibilyBoneAction(String idSecurity)throws ServiceException{
		return corporateProcessService.validateConvertibilyBoneAction(idSecurity);
	}
	
	/**
	 * Metodo que valida saldos no validos para los procesos corporativos especiales.
	 *
	 * @param idSecurity the id security
	 * @param stateCorporative the state corporative
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateBalanceForSpecial(String idSecurity,Integer stateCorporative)throws ServiceException{
		return corporateProcessService.validateBalanceForSpecial(idSecurity,stateCorporative);
	}
	
	/**
	 * Metodo que valida ultima amortizacion.
	 *
	 * @param idSecurity the id security
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateUltimateAmortization(String idSecurity)throws ServiceException{
		return corporateProcessService.validateUltimateAmortization(idSecurity);
	}
	/**
	 * Metodo que valida confirmacion de la depositaria.
	 *
	 * @param lstProcesses the lst processes
	 * @throws ServiceException the service exception
	 */
	public void executeConfirmationDepositary(List<CorporativeOperation> lstProcesses) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirmDepositary());
		}
		corporateProcessService.executeConfirmationDepositary(lstProcesses,loggerUser);
	}
	
	/**
	 * Metodo que valida confirmacion del emisor.
	 *
	 * @param lstProcesses the lst processes
	 * @throws ServiceException the service exception
	 */
	public void executeConfirmationIssuer(List<CorporativeOperation> lstProcesses) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirmIssuer());
		}
		//check state of corporatives before issuer confirm
		corporateProcessService.executeConfirmationIssuer(lstProcesses,loggerUser);
	}
	
	/**
	 * Metodo que retorna saldo desmaterializado a nivel de valor.
	 *
	 * @param securityPk the security pk
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal searchDesmaterializedBalanceSecurity(String securityPk) throws ServiceException{
		return corporateProcessService.searchDesmaterializedBalance(securityPk);
	}
	
	/**
	 * Metodo valida si tiene saldo mayor a cero.
	 *
	 * @param idSecurity the security pk
	 * @return validate has balance
	 * @throws ServiceException the service exception
	 */
	public boolean hasBalance(String idSecurity)throws ServiceException{
		return corporateProcessService.hasBalance(idSecurity);
	}
	/**
	 * Metodo que retorna proceso corpporativo.
	 *
	 * @param corporateId the corporate id
	 * @return the corporate event type
	 */
	public CorporativeOperation getCorporateEventType(Long corporateId){
		return corporateProcessService.getCorporativeOperation(corporateId);
	}
	
	/**
	 * Metodo que actualiza estado, situacion del proceso corporativo.
	 *
	 * @param operationId the operation id
	 * @param state the state
	 * @param loggerUser the logger user
	 * @param updateSituation the update situation
	 * @param updateSituationConfirmedIssuer the update situation confirmed issuer
	 * @throws ServiceException the service exception
	 */
	
	public void updateCorporativeOperationState(Long operationId, Integer state, LoggerUser loggerUser, boolean updateSituation,Integer updateSituationConfirmedIssuer) throws ServiceException {
		corporateProcessService.updateCorporativeOperationState(operationId, state, loggerUser, updateSituation, updateSituationConfirmedIssuer);
	}
	
	/**
	 * Metodo que actualiza estado, situacion del proceso corporativo AUTOMATICO.
	 *
	 * @param operationId the operation id
	 * @param state the state
	 * @param loggerUser the logger user
	 * @param updateSituation the update situation
	 * @param updateSituationConfirmedIssuer the update situation confirmed issuer
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateCorporativeOperationStateAuto(Long operationId, Integer state, LoggerUser loggerUser, boolean updateSituation,Integer updateSituationConfirmedIssuer) throws ServiceException {
		corporateProcessService.updateCorporativeOperationState(operationId, state, loggerUser, updateSituation, updateSituationConfirmedIssuer);
	}
	
	/**
	 * Metodo que actualiza estado del proceso corporativo.
	 *
	 * @param operationId the operation id
	 * @param state the state
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateCorporativeOperationStateAuto(Long operationId, Integer state, LoggerUser loggerUser) throws ServiceException {
		corporateProcessService.updateCorporativeOperationState(operationId, state, loggerUser);
	}
	
	
	/**
	 * Metodo que actualiza estado del proceso corporativo.
	 *
	 * @param operationId the operation id
	 * @param state the state
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateCorporativeOperationState(Long operationId, Integer state, LoggerUser loggerUser) throws ServiceException {
		corporateProcessService.updateCorporativeOperationState(operationId, state, loggerUser);
	}
	
	
	
	/**
	 * Metodo que ejecuta definitivo del proceso corporativo.
	 *
	 * @param lstProcesses the lst processes
	 * @throws ServiceException the service exception
	 */
	public void executeDefinitiveProcess(List<CorporativeOperation> lstProcesses) throws ServiceException{
		//validate Definitive Process
		corporateProcessService.validateDefinitiveProcess(lstProcesses);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDefinitive());
		}
		for(CorporativeOperation operation : lstProcesses){

			try {
			
				 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType()) 
						 || ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
					 //Validating if the Security has desmaterialized Balance
					 BigDecimal desmaterializedBalance = corporateProcessService.searchDesmaterializedBalance(operation.getSecurities().getIdSecurityCodePk());
					 if(desmaterializedBalance.compareTo(BigDecimal.ZERO) > 0 &&
							 hasBalance(operation.getSecurities().getIdSecurityCodePk())){
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTE_DEFINITIVE_CORPORATIVE_PROCESS_MANUAL.getCode());
					    Map<String, Object> param = new HashMap<String, Object>();				    
					    param.put("processid", operation.getIdCorporativeOperationPk());
					    param.put("userid", loggerUser.getUserName());		
					    param.put("corporateType", operation.getCorporativeEventType().getDescription());	
					    batchService.registerBatchTx(loggerUser.getUserName(), businessProcess, param);
					 }else{
						 setDataForCorporativeProcessFail(operation);
						 
						//Lista para guardar en base de datos
							List <HolderAccountBalanceExp> accountBalanceExps = new ArrayList();
							
							//Si es la ultima amortizacion extraemos los saldos antes de vencer el valor
							if(corporateProcessFacade.verifyLatestCouponAmort(operation.getSecurities().getIdSecurityCodePk(), operation.getIdCorporativeOperationPk()).equals(true)){
								accountBalanceExps = corporateProcessFacade.lstHolderAccountaBalanceExp(operation.getSecurities().getIdSecurityCodePk());
							}
							//Si existen datos en la lista persistimos en BBDD
							if(accountBalanceExps.size() > GeneralConstants.ZERO_VALUE_INTEGER){
								regHolderAccountBalanceExp(accountBalanceExps);
							}
					 }
				 }else{
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTE_DEFINITIVE_CORPORATIVE_PROCESS_MANUAL.getCode());
				    Map<String, Object> param = new HashMap<String, Object>();				    
				    param.put("processid", operation.getIdCorporativeOperationPk());
				    param.put("userid", loggerUser.getUserName());		
				    param.put("corporateType", operation.getCorporativeEventType().getDescription());	
				    batchService.registerBatchTx(loggerUser.getUserName(), businessProcess, param);
				 }
				 //System.out.println("send:: "+CommonsUtilities.currentTime());
				 Thread.sleep(3*CorporateProcessConstants.MILLSECS_IN_SECS/2);
				 //System.out.println("nextStart:: "+CommonsUtilities.currentTime());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Metodo de ayuda de rollback de una transaccion.
	 *
	 * @throws ServiceException the service exception
	 */
	public void testRollbackYTransaction() throws ServiceException{
		corporateProcessService.testRollbackYTransaction();
	}
	
	/**
	 * Metodo que ejecuta el componente de movimiento de saldos a nivel de cuenta participante (cuenta matriz) para valores DPF
	 *
	 * @param corpOperation object corporative operation
	 * @param loggerUser data user
	 * @param processType process type
	 * @param indUnBlock the ind un block
	 * @throws ServiceException the service exception
	 */
	public void executeAccountsComponentForCorporativeDPF(CorporativeOperation corpOperation,LoggerUser loggerUser,ImportanceEventType processType,boolean indUnBlock)throws ServiceException{
		corporateProcessService.executeAccountsComponentForCorporativeDPF(corpOperation, loggerUser, processType,indUnBlock);
	}
	
	/**
	 * Metodo que ejecuta el componente de movimiento de saldos a nivel de valor para valores DPF
	 *
	 * @param corpOperation object corporative operation
	 * @param loggerUser data user
	 * @param processType process type
	 * @throws ServiceException the service exception
	 */
	public void executeSecurityComponentForCorporativeDPF(CorporativeOperation corpOperation,LoggerUser loggerUser,
			ImportanceEventType processType)throws ServiceException{
		corporateProcessService.executeSecurityComponentForCorporativeDPF(corpOperation, loggerUser, processType);
	}
	
	/**
	 *  Metodo que ejecuta el componente de movimiento de saldos a nivel de cuenta participante (cuenta matriz) para valores DPF - REVERSA
	 *
	 * @param corpOperation object corporative operation
	 * @param loggerUser data user
	 * @param processType process type
	 * @throws ServiceException the service exception
	 */
	public void executeReverseAccountsComponentForCorporativeDPF(CorporativeOperation corpOperation,LoggerUser loggerUser,
			ImportanceEventType processType)throws ServiceException{
		corporateProcessService.executeReverseAccountsComponentForCorporativeDPF(corpOperation, loggerUser, processType);
	}
	
	/**
	 * Metodo que ejecuta el componente de movimiento de saldos a nivel de valor para valores DPF - REVERSA
	 *
	 * @param corpOperation object corporative operation
	 * @param loggerUser data user
	 * @param processType process type
	 * @throws ServiceException the service exception
	 */
	public void executeReverseSecurityComponentForCorporativeDPF(CorporativeOperation corpOperation,LoggerUser loggerUser,
			ImportanceEventType processType)throws ServiceException{
		corporateProcessService.executeReverseSecurityComponentForCorporativeDPF(corpOperation, loggerUser, processType);
	}
	
	/**
	 * Metodo que vence o suspende el valor DPF de forma automatica.
	 *
	 * @param finalDate the final date
	 * @param lstSecurity the lst security
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=360) // 6 horas
	synchronized public void automaticExpiredSecuritiesDpf(Date finalDate, List<String> lstSecurity) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(Validations.validateIsNull(loggerUser.getIdPrivilegeOfSystem())){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDefinitive());
		}		
		corporateProcessService.automaticExpiredSecuritiesDpf(finalDate, loggerUser, lstSecurity);
	}
	
	/**
	 * Metodo que vence cupones de valores DPF.
	 *
	 * @param finalDate the final date
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=120)
	public void automaticExpiredSecuritiesDpfInterestCoupon(String currentDate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(Validations.validateIsNull(loggerUser.getIdPrivilegeOfSystem())){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDefinitive());
		}		
		corporateProcessService.automaticExpiredSecuritiesDpfInterestCoupon(currentDate, loggerUser);
	}
	
	/**
	 * Metodo que registra bacehro de procesos automaticos.
	 *
	 * @param details the details
	 * @param automatic the automatic
	 * @throws ServiceException the service exception
	 */
	public void registerBatch(Map<String,Object> details, Long automatic) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		
		BusinessProcess businessProcess = new BusinessProcess();
		if(automatic.equals(BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC_DEFINITIVE.getCode())){
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC_DEFINITIVE.getCode());
		}else if(automatic.equals(BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC.getCode())){
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.EXECUTION_CORPORATIVE_PROCESS_AUTOMATIC.getCode());
		}else if(automatic.equals(BusinessProcessType.STOCK_CALCULATION_AUTOMATIC_EXECUTION_MONTHLY.getCode())){
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.STOCK_CALCULATION_AUTOMATIC_EXECUTION_MONTHLY.getCode());
		}			
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess,details);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo que actualiza datos del proceso corporativo, datos del valor y de la emison por data inconsistente.
	 *
	 * @param objCorporativeOperation corporative process
	 * @throws ServiceException the Service Exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public synchronized void setDataForCorporativeProcessFail(CorporativeOperation objCorporativeOperation)throws ServiceException{
		//not desmaterialized
		 objCorporativeOperation.setState(CorporateProcessStateType.DEFINITIVE.getCode());
//		 objCorporativeOperation.setSituation(CorporativeSituationType.PAID.getCode());
//		 objCorporativeOperation.setIndPayed(BooleanType.YES.getCode());
		 objCorporativeOperation.setIssuerConfirmedAmount(new BigDecimal(0));
		 objCorporativeOperation.setPaymentAmount(new BigDecimal(0));
		 corporateProcessService.update(objCorporativeOperation);
		 if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(objCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			 ProgramInterestCoupon interestCoupon = corporateProcessService.getProgramFromCorporative(objCorporativeOperation.getIdCorporativeOperationPk());
			 interestCoupon.setStateProgramInterest(ProgramScheduleStateType.EXPIRED.getCode());
			 corporateProcessService.update(interestCoupon);
		 }else if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(objCorporativeOperation.getCorporativeEventType().getCorporativeEventType())){
			 ProgramAmortizationCoupon amortizationCoupon = corporateProcessService.getProgramAmortizationFromCorporative(objCorporativeOperation.getIdCorporativeOperationPk());
			 amortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.EXPIRED.getCode());
			 corporateProcessService.update(amortizationCoupon);
			 
			 // Se revierte el issue 562 a solicitud realizada en el mismo issue en el comentario 5336
			
			 //issue 562
//			 if(objCorporativeOperation.getSecurities().isPhysicalIssuanceForm() ){
//				LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//				if(loggerUser.getIdPrivilegeOfSystem() == null){
//				   loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
//				}
				//CALCULAMOS EL VALOR NOMINAL ACTUAL DESPUES DE LA AMORTIZACION Y REDONDEAMOS A 8 DECIMALES
//				BigDecimal currentNomValue = objCorporativeOperation.getSecurities().getCurrentNominalValue()
//						.subtract(amortizationCoupon.getCouponAmount());
//				currentNomValue = currentNomValue.setScale(8, RoundingMode.HALF_UP);
				
				 //ACTUALIZAMOS EL VALOR NOMINAL ACTUAL DEL VALOR
//				corporateExecutorService.updateSecurity(objCorporativeOperation.getSecurities().getIdSecurityCodePk(), null, 
//							null, currentNomValue, null,null ,loggerUser);
			 
//			 }
			 
			 
			 if(objCorporativeOperation.getIndAffectBalance().equals(BooleanType.YES.getCode())){
				LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
				if(loggerUser.getIdPrivilegeOfSystem() == null){
			    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
				}
				//Actualizamos estado de valor (VENCIDO)
				//corporateExecutorService.updateSecurity(objCorporativeOperation.getSecurities().getIdSecurityCodePk(), objCorporativeOperation.getDeliveryDate(), 
				//		SecurityStateType.EXPIRED.getCode(), null, null,null ,loggerUser);
				//obtenemos emision
				Issuance issuance = corporateExecutorService.findIssuance(objCorporativeOperation.getSecurities().getIdSecurityCodePk());	

				//issue 202
				if(!(corporateProcessService.issuanceSecuritiesAssociated(issuance.getIdIssuanceCodePk()).size()
						>GeneralConstants.ONE_VALUE_INTEGER)){
					//issuance.setExpirationDate(objCorporativeOperation.getDeliveryDate());
					issuance.setStateIssuance(IssuanceStateType.EXPIRED.getCode());
					corporateExecutorService.update(issuance, loggerUser);	
				}				
				//Actualizamos estado de valor (VENCIDO)
				corporateExecutorService.updateSecurity(objCorporativeOperation.getSecurities().getIdSecurityCodePk(), objCorporativeOperation.getDeliveryDate(), 
						SecurityStateType.EXPIRED.getCode(), null, null,null ,loggerUser);
			 }
		 }
	}
	
	
	/**
	 * obtiene la primera lista de valores que pasaran a definitivo	 
	 */
	public List<CorporativeOperation> listSecurityToDefinitive() throws ServiceException {
		
		List<CorporativeOperation> listSecurity = corporateProcessService.getListSecurityToDefinitive();	
		return listSecurity;
		
	}	
	
	/**
	 * Metodo corporativos automaticos - 2 hrs de tiempo de transacion
	 * @param lstProcesses
	 * @param intAmor
	 * @throws ServiceException
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=120)
	public synchronized List<CorporativeOperation> definitiveProcessSecurity(List<CorporativeOperation> lstProcesses) throws ServiceException{
	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		//Lisa de corporativos con error para el reproceso
		List <CorporativeOperation> lstCorporativeError = new ArrayList<CorporativeOperation>();
		
		//Contador de corporativos
		Integer count = 0;
		
		log.info("CANTIDAD DE CORPORATIVOS: " + lstProcesses.size());
		
		for(CorporativeOperation corporative : lstProcesses){
			
			count=count+1;
			 
			 //Mensajes en el log
			 if(corporative.getCorporativeEventType().getCorporativeEventType().equals
						(ImportanceEventType.CAPITAL_AMORTIZATION.getCode())){
				 log.info(" CORPORATIVO AMORTIZACION VALOR/EMISION: " + corporative.getIdCorporativeOperationPk() + " / " + corporative.getSecurities().getIdSecurityCodePk() + " CORP "+ count + " de "+  lstProcesses.size()) ;
			 }else{
				 log.info(" CORPORATIVO INTERES DE VALOR/EMISION: " + corporative.getIdCorporativeOperationPk() + " / " + corporative.getSecurities().getIdSecurityCodePk() + " CORP "+ count + " de "+  lstProcesses.size()) ;
			 }
			
				 // Validamos que tenga saldo desmaterializado
				 BigDecimal desmaterializedBalance = corporateProcessService.searchDesmaterializedBalance(corporative.getSecurities().getIdSecurityCodePk());
				 if(desmaterializedBalance.compareTo(BigDecimal.ZERO) > 0 &&
						 hasBalance(corporative.getSecurities().getIdSecurityCodePk())){
					 
					 //verificamos q tenga CStock
					 Long idCalcStock = corporateProcessService.verifyStockCalculationAutomatic(corporative);
					 
					 if(idCalcStock == null){ 
						 //No tiene CStock enviamos CStock  -- VERIFICAR CUANDO YA TIENE UN CALCULO DE STOCK EN PROCESO AHI SE PUEDE CAER
						 idCalcStock = corporateProcessService.executeStockCalcProcessSecurity(corporative);
						 //Verificamos que el CStock termine
						 while(corporateProcessService.verifyStockCalculationAutomaticFinal(idCalcStock)){
							 try {
								 //Si aun no termino esperamos 10 segundos y volvemos a verificar hasta que termine
								Thread.sleep(10000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						 }
					 }
					
					 //Enviamos Preliminar y Definitivo
					 CorporativeOperation corporateError = prelimAndDefinitive(corporative);
					 
					 //Agregamos a la lista para el reproceso Extremo
					 if(corporateError != null){
						 lstCorporativeError.add(corporateError); 
					 }
						 
				 }else{
					//En el caso de no tener Saldo Desmaterializado, va directamente a definitivo
					 setDataForCorporativeProcessFail(corporative);
					 
					 	//Lista para guardar en base de datos
						List <HolderAccountBalanceExp> accountBalanceExps = new ArrayList();
						
						//Si es la ultima amortizacion extraemos los saldos antes de vencer el valor
						if(corporateProcessFacade.verifyLatestCouponAmort(corporative.getSecurities().getIdSecurityCodePk(), corporative.getIdCorporativeOperationPk()).equals(true)){
							accountBalanceExps = corporateProcessFacade.lstHolderAccountaBalanceExp(corporative.getSecurities().getIdSecurityCodePk());
						}
						//Si existen datos en la lista persistimos en BBDD
						if(accountBalanceExps.size() > GeneralConstants.ZERO_VALUE_INTEGER){
							regHolderAccountBalanceExp(accountBalanceExps);
						}
					 
					 log.info("DATA CORPORATIVO INCONSISTENTE=============================");
				 }
			}
		
		return lstCorporativeError;
		}
	
	
	/**
	 * Metodo que manda a preliminar y definitivo de forma automatica
	 * @param corporative Corporativo para procesar
	 * @return lstCorporativeError Todos los que tengan algun error los reenviamos para el reproceso
	 */

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CorporativeOperation  prelimAndDefinitive(CorporativeOperation corporative){
		
		//Lisa de corporativos con error para el reproceso
				CorporativeOperation corporativeError = null;
		
		try{
			
			// Verificamos el estado y enviamos el preliminar
			if(CorporateProcessStateType.REGISTERED.getCode().equals(corporative.getState())
					|| CorporateProcessStateType.MODIFIED.getCode().equals(corporative.getState())
					|| CorporateProcessStateType.PRELIMINARY_FAIL.getCode().equals(corporative.getState())){
				
				//Enviamos el preliminar
				executePreliminary(corporative);
				
				//Si hay un error no va a definitivo, ira a la lista para el reproceso
	 			if(CorporateProcessStateType.PRELIMINARY_FAIL.getCode().equals(corporative.getState())){
	 				corporativeError = corporative;
	 			}
	 					
			}
			
			if(CorporateProcessStateType.PRELIMINARY.getCode().equals(corporative.getState())
				|| CorporateProcessStateType.DEFINITIVE_FAIL.getCode().equals(corporative.getState())){
				
				//Enviamos el definitivo
				executeDefinitive(corporative);
				
				//Si hay un error no va a definitivo, ira a la lista para el reproceso
				if(CorporateProcessStateType.DEFINITIVE_FAIL.getCode().equals(corporative.getState())){
					corporativeError = corporative;
				}
			}
		 } catch (RuntimeException e) {
		 			//Si hay un error no va a definitivo, ira a la lista para el reproceso
		 			if(CorporateProcessStateType.PRELIMINARY_FAIL.getCode().equals(corporative.getState())){
		 				corporativeError = corporative;
		 			}else //Si hay un error no va a definitivo, ira a la lista para el reproceso
						  if(CorporateProcessStateType.DEFINITIVE_FAIL.getCode().equals(corporative.getState())){
							  corporativeError = corporative;
						}else{
							e.printStackTrace();
						}
		 }
		return corporativeError;
	}
		
		
		
	/**
	 * Metodo para enviar un corporativo a Preliminar o Preliminar Fallo	
	 * @param corporative
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public synchronized void executePreliminary(CorporativeOperation corporative){

		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		CorporateProcessExecutor processExecutor = corporateEventProducer.getCorporateType(corporative.getCorporativeEventType().getCorporativeEventType());
		try{

			//causa Bloqueo de tablas  -- actualizamos estado de evento corporativo (Preliminar en Proceso)
			//corporateProcessFacade.updateCorporativeOperationState(corpOperation.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY_IN_PROCESS.getCode(), loggerUser);
			
			//ejecutamos proceso preliminar
			processExecutor.executePreliminarProcess(corporative); // si falla va a preliminar fallo
			
			//actualizamos estado de evento corporativo (Preliminar)
			if(corporative.getSecurities().getIndPaymentBenefit().equals(BooleanType.NO.getCode()) &&
					(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corporative.getCorporativeEventType().getCorporativeEventType()) || 
					   ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corporative.getCorporativeEventType().getCorporativeEventType()))){
				corporateProcessFacade.updateCorporativeOperationStateAuto(corporative.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY.getCode(), loggerUser, false,CorporativeSituationType.IDEPOSITARY.getCode());
			}else if(corporative.getSecurities().getIndPaymentBenefit().equals(BooleanType.YES.getCode()) &&
					(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corporative.getCorporativeEventType().getCorporativeEventType()) || 
							   ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corporative.getCorporativeEventType().getCorporativeEventType()))){
				corporateProcessFacade.updateCorporativeOperationStateAuto(corporative.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY.getCode(), loggerUser, false,CorporativeSituationType.ISSUER.getCode());
			}else{
				corporateProcessFacade.updateCorporativeOperationStateAuto(corporative.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY.getCode(), loggerUser, false,null);
			}
			corporative.setState(CorporateProcessStateType.PRELIMINARY.getCode());
			
		}catch (Exception e) {
			try{
				//En caso de error se vuelve Preliminar Fallo
				corporateProcessFacade.updateCorporativeOperationStateAuto(corporative.getIdCorporativeOperationPk(),CorporateProcessStateType.PRELIMINARY_FAIL.getCode(), loggerUser);
			}catch (Exception ex) {
				log.error(ex.getMessage());
			}
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * Metodo para enviar un corporativo a Definitivo o Definitivo Fallo	
	 * @param corporative
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public synchronized void executeDefinitive(CorporativeOperation corporative){
		
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
			CorporateProcessExecutor processExecutor = corporateEventProducer.getCorporateType(corporative.getCorporativeEventType().getCorporativeEventType());
			try{
				//actualizamos estado de evento corporativo (Definitivo en Proceso)
				//corporateProcessFacade.updateCorporativeOperationState(corporative.getIdCorporativeOperationPk(),CorporateProcessStateType.DEFINITIVE_IN_PROCESS.getCode(), loggerUser);
				//ejecutamos proceso Definitivo
				
				//Lista para guardar en base de datos
				List <HolderAccountBalanceExp> accountBalanceExps = new ArrayList();
				//Si es la ultima amortizacion extraemos el saldo actual
				if(verifyLatestCouponAmort(corporative.getSecurities().getIdSecurityCodePk(), corporative.getIdCorporativeOperationPk()).equals(true)){
					accountBalanceExps = lstHolderAccountaBalanceExp(corporative.getSecurities().getIdSecurityCodePk());
				}
				
				processExecutor.executeDefinitiveProcess(corporative);
				//actualizamos estado de evento corporativo (Definitivo)
				corporateProcessFacade.updateCorporativeOperationStateAuto(corporative.getIdCorporativeOperationPk(),CorporateProcessStateType.DEFINITIVE.getCode(), loggerUser);
				corporative.setState(CorporateProcessStateType.DEFINITIVE.getCode());
				
				//Si existen datos en la lista persistimos en BBDD
				if(accountBalanceExps.size() > GeneralConstants.ZERO_VALUE_INTEGER){
					regHolderAccountBalanceExp(accountBalanceExps);
				}
				
				RetirementOperation retirementOperation = corporateExecutorService.findRelationRetirementCorporative(corporative.getIdCorporativeOperationPk());
				//Se valida si el corporativo esta relacionado a una operacion de retiro de valores
				if(Validations.validateIsNotNullAndNotEmpty(retirementOperation)){
					boolean blAllDefinitive = true;
					for(CorporativeOperation corporativeOperation:retirementOperation.getLstCorporativeOperation()){
						if(!CorporateProcessStateType.DEFINITIVE.getCode().equals(corporativeOperation.getState())){
							blAllDefinitive = false;
							break;
						}
					}
					if(blAllDefinitive){
						retirementOperation.setState(RetirementOperationStateType.CONFIRMED.getCode());
						corporateExecutorService.updateRetirementOperation(retirementOperation, loggerUser);
					}						
				}
			}catch (Exception e) {
				try{
					//En caso de error se vuelve Definitivo Fallo
					corporateProcessFacade.updateCorporativeOperationStateAuto(corporative.getIdCorporativeOperationPk(),CorporateProcessStateType.DEFINITIVE_FAIL.getCode(), loggerUser);
				}catch (Exception ex) {
					log.error(ex.getMessage());
				}
				log.error(e.getMessage());
				throw new RuntimeException(e);
			}
	}
	
	/**
	 * Metodo para corregir inconsistencias en el proceso de corporativos preliminar y definitivo
	 * @param lstCorporate
	 * @param extreme Indicador de reproceso
	 */
	
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=120)
	public void correctInconsistencyCorporative(List<CorporativeOperation> lstCorporate, Boolean extreme){
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		//lista de errores encontrados para corregirlos
		List <CorporativeOperation> lstCorporateError = new ArrayList<>();
		
		//Extremo solo si sucede durante el proceso de preliminar o definitivo
		if(extreme.equals(false)){
		//corregimos en el caso de detectar algun error antes de enviar el preliminar o definitivo
			
			try {
				/*=========== 		PRELIMINAR ==============*/
				//corporateProcessService.validateDefinitiveProcess(lstCorporativeError);
				//corporateProcessService.validatePreliminaryProcess(lstCorporativeError);
				
				//check delivery date - VERIFICACION DE LA FECHA DE ENTREGA
				//corporateProcessService.verifyPreliminarDeliveryDate(lstCorporativeError,false);
				
				HashMap<String, CorporativeOperation> distinct = new HashMap<>();
		    	//sacamos solo los corporativos con valores distintos
				 for(CorporativeOperation stocks : lstCorporate){
					 distinct.put(stocks.getSecurities().getIdSecurityCodePk(), stocks);
				 }
				 //cargamos los valores distintos en la lista para identificacion de stocks faltantes
				 List<CorporativeOperation> lstDistinctCorporative  =  new ArrayList<CorporativeOperation>(distinct.values()); 
				
				//VErifICAR QUE TENGA ALGUN PROBLEMA EN EL CALCULO DE STOCK
				List<CorporativeOperation> lstCorporateErrorStock = (corporateProcessService.verifyPreliminarStockCalculationAutomatic(lstDistinctCorporative)) ;
				//VERIFICAR EL ESTADO DEL VALOR
				List<CorporativeOperation> lstCorporateErrorSecStatus = (corporateProcessService.verifySecurityStatusAutomatic(lstDistinctCorporative));
				//VERIFICAR EL ESTADO DE LA EMISION
				List<CorporativeOperation> lstCorporateErrorIssStatus = (corporateProcessService.verifyIssuanceStatusAutomatic(lstDistinctCorporative));
				//MONTO EN CIRCULACION EMISION VS MONTO EN CIRCULACION DEL VALOR
				List<CorporativeOperation> lstCorporateErrorCirBalance = (corporateProcessService.verifyIssuanceSecurityCirBalance(lstDistinctCorporative));
				// VALOR RELACIONADO CON HM NO TIENE SALDO DESMATERIALIZADO 
				lstCorporateError.addAll(corporateProcessService.verifySecurityDesmaterializedAutomatic(lstCorporate));
				// check securities balance for special - SALDOS ESPECIALES COMPROMETIDOS - Se han encontrado saldos no validos para los siguientes procesos: {0}. Verifique.
				//corporateProcessService.verifySecurityBalance(lstCorporate,CorporateProcessStateType.PRELIMINARY.getCode());
				//check settelements operations - SALDO COMPROMETIDO EN COMPRA/VENTA - Existen operaciones pendientes a liquidar para los procesos: {0}. Verifique. 
				lstCorporateError.addAll(corporateProcessService.verifyNotSettledBalancesCorporateAutomatic(lstCorporate));
				//check transit balances - SALDO EN TRANSITO - Se han encontrado saldos en transito para los siguientes procesos: {0}. Verifique.
				lstCorporateError.addAll(corporateProcessService.verifyTransitBalancesAutomatic(lstCorporate));
				//check issuer confirmation - NO CONFIRMADO POR EL EMISOR -  NO SOMOS AGENTE PAGADOR
				//corporateProcessService.verifyEmisorConfirm(lstCorporate,false);
				//Check movements for preliminar - SALDO BLOQUEADO O EN GARANTIA - Existen operaciones de bloqueo o garantias para el/los procesos : {0}. Por favor ejecutar nuevamente el stock.
				lstCorporateError.addAll(corporateProcessService.verifyHolderMovementsPreliminarAutomatic(lstCorporate));
				//check dayli exchange - VERIFICA EL TIPO DE CAMBIO - UFV y DMV
				List <CorporativeOperation> lstCorporateErrorDateExchange = (corporateProcessService.verifyDateExchangeRateAutomatic(lstCorporate));
				//check retirement operations - SALDO DISPONIBLE TRAS RETIRO DE OPERACIONES - No hay saldo disponible para el siguiente proceso: {0}. Verifique.
				lstCorporateError.addAll(corporateProcessService.verifyCorporateCreatedRetirementOperationAutomatic(lstCorporate));
				//check accreditation balance
				//verifyAccreditationBalance(lstProcesses,false);
				
				/*=========== DEFINITIVO ==============*/
				
				//check validate ultimate amortization
				lstCorporateError.addAll(corporateProcessService.verifyDefinitiveAmortizationAutomatic(lstCorporate));
				// check securities balance for special - SALDOS ESPECIALES
				//corporateProcessService.verifySecurityBalance(lstCorporate,CorporateProcessStateType.DEFINITIVE.getCode());
				//check states - VERIFICAMOS QUE EL ESTADO SEA VALIDO
				//corporateProcessService.verifyDefinitiveProcessStates(lstCorporate,false);
				//check confirmations - VERIFICAMOS QUE SEA INTERES O AMORTIZACION
				//corporateProcessService.verifySituationDefinitiveProcess(lstCorporate,false);
				//check open delivery date - QUE LA FECHA DE ENTREGA SEA LA MISMA QUE LA DE VENCIMIENTO DEL CUPON
				//corporateProcessService.verifyPreliminarExpirationDate(lstCorporate,false); 
				//check closed delivery date  - QUE LA FECHA DE ENTREGA SEA LA MISMA QUE LA DE VENCIMIENTO DEL CUPON
				//corporateProcessService.verifyDefinitiveExpirationDate(lstCorporate,false);
				//check preliminary process date - VERIFICAMOS LA FECHA DE EJECUCION DEL PRELIMINAR
				//corporateProcessService.verifyProcessPreliminaryExecutedDate(lstCorporate, CommonsUtilities.currentDate(),false);
				//check adjustments - VERIFICAMOS QUE NO TENGA ALGUN AJUSTE - NO PASO HASTA AHORA
				//corporateProcessService.verifyHolderAccountAdjustments(lstCorporate,CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,false);
				//check Issuer amount - VERIFICAR EL MONTO CONFIRMADO POR EL EMISOR - NO SOMOS AGENTE PAGADOR
				//corporateProcessService.verifyIssuerAmount(lstCorporate,false);
				//check holder movements - SALDO BLOQUEADO O EN GARANTIA - Existen operaciones de bloqueo o garantias para el/los procesos: {0}. Por favor ejecutar nuevamente el stock y el preliminar
				corporateProcessService.verifyHolderAccountsMovement(lstCorporate,false);
				//check holder movements definitive
				corporateProcessService.verifyHolderAccountsMovementDefinitive(lstCorporate,false);
				//check accreditation balance
				//verifyAccreditationBalance(lstProcesses,false);
				System.out.println("  =================   FIN DE LAS VALIDACIONES   =================");
				
			/*	==============		CORRECCIONES 	========================			*/
				
				//Cuando no tiene calculo de stock
				if(lstCorporateErrorStock.size()>GeneralConstants.ZERO_VALUE_INT){
					corporateProcessService.correctPreliminarStockCalculationAutomatic(lstCorporateErrorStock);
				}
				//Cuando el valor tiene un estado diferente a registrado
				if(lstCorporateErrorSecStatus.size()>GeneralConstants.ZERO_VALUE_INT){
					corporateProcessService.correctSecurityStatusAutomatic(lstCorporateErrorSecStatus);
				}
				//cuando la emision tiene el estado diferente a registrado
				if(lstCorporateErrorIssStatus.size()>GeneralConstants.ZERO_VALUE_INT){
					corporateProcessService.correctIssuanceStatusAutomatic(lstCorporateErrorIssStatus, loggerUser);
				}
				//cuando existe inconsistencia entre los montos en circulacion del valor y de la emision
				if(lstCorporateErrorCirBalance.size()>GeneralConstants.ZERO_VALUE_INT){
					corporateProcessService.correctIssuanceSecurityCirBalance(lstCorporateErrorCirBalance);
				}
				//cuando el corporativo pertenece a un valor con monedas UFV o MVDOL
				if(lstCorporateErrorDateExchange.size()>GeneralConstants.ZERO_VALUE_INT){
					corporateProcessService.correctDateExchangeRateAutomatic(lstCorporateErrorDateExchange,loggerUser);
				}
				
			} catch (ServiceException e) {
				System.out.println();
				log.info("ERROR NO CONTROLADO EN: ");
				e.printStackTrace();
			}
			
		}else{
			//corregimos en caso de tener un error extremo o no contemplado durante el proceso de preliminar o definitivo
		}
	}
	
	/**
	 * Metodo para enviar el calculo de stock automatico
	 * @param lstProcesses
	 * @throws ServiceException
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=120)
	public void stocksAutomatic(List<CorporativeOperation> lstProcesses) throws ServiceException{
		Integer i=0;
		for(CorporativeOperation corporative : lstProcesses){
			//verificamos q tenga CStock
			 Long idCalcStock = corporateProcessService.verifyStockCalculationAutomatic(corporative);
			 
			 if(idCalcStock == null){ 
				 //No tiene CStock enviamos CStock
				 //Mandamos calculos de 10 en 10
					idCalcStock = corporateProcessService.executeStockCalcProcessSecurity(corporative);
					i=i+1;
					if(i==10){
						i=0;
						try {
							log.info(" CORPORATIVO STOCK SLEEP 2 minutos ======================================");
							Thread.sleep(120000);
							//verificamos la sobreCarga de la BBDD si los 10 calculos de stock terminaron sino esperamos 1 minuto mas 
							while(corporateProcessService.stocksChargeOnBBDD()
									>GeneralConstants.TWO_VALUE_INTEGER){
								log.info(" CORPORATIVO STOCK SOBRE CARGA SLEEP 1 minutos ======================================");
								Thread.sleep(60000);
							}
							
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
			 }	
		}
	}
	
	
	/**
	 * Metodo para verificar la existencia del tipo de cambio
	 * @return true Falta tipo de cambio / false todo OK
	 * @throws ServiceException
	 */
	public boolean verifyExchangeRate() throws ServiceException{
		List <String> exchange = new ArrayList<>();
		
			if(corporateProcessService.getDMVExchangeRate() == null){
				exchange.add("DMV");
			}
			if(corporateProcessService.getLastUSDExchangeRate() == null){
				exchange.add("USD");
			}
			if(corporateProcessService.getUFVExchangeRate() == null){
				exchange.add("UFV");
			}
			if(corporateProcessService.getEURExchangeRate() == null){
				exchange.add("EUR");
			}
		if(exchange.size()>GeneralConstants.ZERO_VALUE_INT){
				log.error("NO EXISTE TIPO DE CAMBIO PARA: "+StringUtils.join(exchange,","));
				throw new ServiceException(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE_PARAM,new Object[]{StringUtils.join(exchange,",")});
			}
		
		return false;
	}
	
	/**
     * Metodo para verificar si es el ultimo cupon de amortizacion, ejecutandose en un corporativo
     * 
     * @param idSecurityCodePk Clase-Clave de Valor
     * @param idCorporativeOperationPk PK del Objeto CorporativeOperation
     * @return true or false
     */
	public Boolean verifyLatestCouponAmort (String idSecurityCodePk, Long idCorporativeOperationPk){
		return corporateProcessService.verifyLatestCouponAmort(idSecurityCodePk, idCorporativeOperationPk);
	}
	
	/**
	 * Metodo para verificar si es el ultimo cupon de amortizacion
	 * @param idSecurityCodePk
	 * @return true or false
	 */
	public Boolean verifyLatestCouponAmort (String idSecurityCodePk){
		return corporateProcessService.verifyLatestCouponAmort(idSecurityCodePk);
	}
	
	/**
	 * Metodo para extraer una lista con los saldos actuales del valor
	 * @param idSecurityCodePk
	 * @return
	 */
	public List<HolderAccountBalanceExp> lstHolderAccountaBalanceExp (String idSecurityCodePk){
		List<HolderAccountBalanceExp> lstAccountBalanceExps = new ArrayList<>();
		
		lstAccountBalanceExps = corporateProcessService.lstHolderAccountaBalanceExp(idSecurityCodePk);
		
		return lstAccountBalanceExps;
	}
	
	/**
	 * Metodo para persistir HolderAccountBalanceExp
	 * @param lstHolderAccountaBalanceExp
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void regHolderAccountBalanceExp(List<HolderAccountBalanceExp> lstHolderAccountaBalanceExp){
		corporateProcessService.regHolderAccountBalanceExp(lstHolderAccountaBalanceExp);
	}
	
	
}
