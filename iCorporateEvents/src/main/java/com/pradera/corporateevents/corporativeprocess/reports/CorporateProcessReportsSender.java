package com.pradera.corporateevents.corporativeprocess.reports;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.service.CorporateServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporativeEventReportType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporateProcessReportsSender.
 *
 * @author PraderaTechnologies
 */
@ApplicationScoped
public class CorporateProcessReportsSender implements Serializable {

		
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	

	/** The report service. */
	@Inject
	Instance<ReportGenerationService> reportService;
	
	/** The corporate service bean. */
	@EJB
	CorporateServiceBean corporateServiceBean;
	
	
	
	/**
	 * Send reports.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendReports(CorporativeOperation corpOperation, LoggerUser loggerUser) throws ServiceException{
		if(reportService != null){
			if(!(corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.DPF.getCode())
					|| corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.CHQ.getCode())
					|| corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.FCT.getCode())
					|| corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.PGS.getCode())
					|| corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.LTC.getCode())
					|| corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.LCB.getCode())
					|| corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.LBS.getCode())
					|| corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.LRS.getCode())
					|| corpOperation.getSecurities().getSecurityClass().equals(SecurityClassType.LTS.getCode()))) {
				if(ImportanceEventType.SECURITIES_EXCISION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
					List<Object[]> securities = corporateServiceBean.getExcisionSecurities(corpOperation.getIdCorporativeOperationPk(), true);
					if(securities != null && !securities.isEmpty()){
						for(Object[] data : securities){
							String originSecurity = data[0].toString();
							sendReport(corpOperation, loggerUser, originSecurity, CorporativeEventReportType.FOR_SECURITY_ISSUER);
							sendReport(corpOperation, loggerUser, originSecurity, CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS);
							sendReport(corpOperation, loggerUser, originSecurity, CorporativeEventReportType.REPURCHASE_BY_MARGIN);
							sendReport(corpOperation, loggerUser, originSecurity, CorporativeEventReportType.BLOCKED_BY_BALANCE);
							sendReport(corpOperation, loggerUser, originSecurity, CorporativeEventReportType.SPECIAL_PROCESS_CONTROL_LIST);
						}
					}
				}else{
					if(ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())
							|| ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())
							|| ImportanceEventType.CASH_DIVIDENDS.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())
							|| ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())
							|| ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
						RetirementOperation retirementOperation = corporateServiceBean.findRetirementOperation(corpOperation.getIdCorporativeOperationPk());
						if(Validations.validateIsNullOrEmpty(retirementOperation)){
							if(!ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
								sendReport(corpOperation, loggerUser, null, CorporativeEventReportType.FOR_SECURITY_ISSUER);
							}					
							if(!ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
								sendReport(corpOperation, loggerUser, null, CorporativeEventReportType.CONTROL_OF_PAYMENTS);
							}						
							sendReport(corpOperation, loggerUser, null, CorporativeEventReportType.REPURCHASE_BY_MARGIN);
							if(ImportanceEventType.CASH_DIVIDENDS.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()) || 
									ImportanceEventType.INTEREST_PAYMENT.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()) || 
									ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()) || 
									ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()) || 
									ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
								sendReport(corpOperation, loggerUser, null, CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS);
							}						
							sendReport(corpOperation, loggerUser, null, CorporativeEventReportType.BLOCKED_BY_BALANCE);
						}else{
								sendReport(corpOperation, loggerUser, null, CorporativeEventReportType.RETIREMENT_HOLDER_ACCOUNT_FOR_PARTICIPANTS);
								sendReport(corpOperation, loggerUser, null, CorporativeEventReportType.RETIREMENT_FOR_SECURITY_ISSUER);
						}
					}
					if(ImportanceEventType.REMANENT_PAYMENT.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())) {
						sendReport(corpOperation, loggerUser, null, CorporativeEventReportType.REMANENT_PAYMENT_ISSUER);
						}
					if(CorporateProcessUtils.isSpecialCorporateProcess(corpOperation.getCorporativeEventType().getCorporativeEventType())){
						String security = null;
						Security targetSecurity = null;
						try{
							targetSecurity = corpOperation.getTargetSecurity();
						}catch (Exception e) {
							//Not required the exception
						}
						if(targetSecurity == null && corpOperation.getIdCorporativeOperationPk() != null && corpOperation.getIdCorporativeOperationPk() > 0){
							targetSecurity = corporateServiceBean.getTargetSecurity(corpOperation.getIdCorporativeOperationPk());						
						}
						if(targetSecurity != null){
							security = targetSecurity.getIdSecurityCodePk();
						}else{
							security = corpOperation.getSecurities().getIdSecurityCodePk();
						}
						sendReport(corpOperation, loggerUser, security, CorporativeEventReportType.FOR_SECURITY_ISSUER);
						sendReport(corpOperation, loggerUser, security, CorporativeEventReportType.HOLDER_ACCOUNT_FOR_PARTICIPANTS);
						sendReport(corpOperation, loggerUser, security, CorporativeEventReportType.REPURCHASE_BY_MARGIN);
						sendReport(corpOperation, loggerUser, security, CorporativeEventReportType.BLOCKED_BY_BALANCE);
						sendReport(corpOperation, loggerUser, security, CorporativeEventReportType.SPECIAL_PROCESS_CONTROL_LIST);
					}
				}
			}
		}
	}
	

	/**
	 * Send report.
	 *
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @param originSecurity the origin security
	 * @param reportType the report type
	 * @throws ServiceException the service exception
	 */
	public void sendReport(CorporativeOperation corpOperation, LoggerUser loggerUser, String originSecurity, CorporativeEventReportType reportType) throws ServiceException{
		ReportUser user = new ReportUser();
		user.setUserName(loggerUser.getUserName());
		Long reportId = CorporateProcessUtils.getReportID(corpOperation.getCorporativeEventType().getCorporativeEventType(), reportType,corpOperation.getIndAffectBalance());
		if(reportId != null){
			reportService.get().saveReportExecution(reportId, CorporateProcessUtils.getReportParameters(corpOperation,originSecurity, reportType.getCode()), null, user, loggerUser);
		}
	}

}
