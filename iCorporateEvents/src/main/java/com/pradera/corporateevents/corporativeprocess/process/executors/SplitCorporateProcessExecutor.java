package com.pradera.corporateevents.corporativeprocess.process.executors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.corporativeprocess.process.CorporateExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcess;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.SecuritiesCorporative;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockoperation.UnblockMarketFactOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.type.BlockMarketFactOperationStateType;
import com.pradera.model.custody.type.UnblockMarketOperationStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SplitCorporateProcessExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/06/2014
 */
@CorporateProcess(CorporateExecutor.SPLIT)
@Performance
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@LocalBean
public class SplitCorporateProcessExecutor implements CorporateProcessExecutor {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The fixer. */
	@Inject
	private BalanceFixer fixer;
	
	/** The document fixer. */
	@Inject
	private DocumentsFixer documentFixer;
	
	/** The executor component service bean. */
	@Inject
	private Instance<AccountsComponentService> executorComponentServiceBean;
	
	/** The securities component service. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentService;
	
	/** The sc. */
	@Resource
    SessionContext sc;
	
	/** The split corporate process executor. */
	SplitCorporateProcessExecutor splitCorporateProcessExecutor;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
    public void init(){
        this.splitCorporateProcessExecutor = this.sc.getBusinessObject(SplitCorporateProcessExecutor.class);
    }
	
		
	/**
	 * This method is used execute the special corporate process for preliminary process execution.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executePreliminarProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started Subscription preliminary process");
		boolean flag = false;
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		List<SecuritiesCorporative> destSecurities = corporateExecutorService.getExcisionSecurities(corpOperation.getIdCorporativeOperationPk());
		if(destSecurities == null || (destSecurities != null && destSecurities.isEmpty())){
			log.error("No Destiny securities registered for process: "+corpOperation.getIdCorporativeOperationPk());
			return flag;
		}
		corporateExecutorService.deletePreviousPreliminaryInformation(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED
				,true,true);
		List<Object[]> distinctHolders  = corporateExecutorService.getDistinctBalanceHoldersForProcess(corpOperation.getSecurities().getIdSecurityCodePk());
		if (distinctHolders != null && distinctHolders.size() > 0) {
			this.splitCorporateProcessExecutor.executePreliminarProcessAlgorithm(distinctHolders, corpOperation, loggerUser, processType, destSecurities);
		}
		
		return true;
	}
	
	/**
	 * Execute preliminar process algorithm.
	 *
	 * @param distinctHolders the distinct holders
	 * @param corpOperation the corp operation
	 * @param loggerUser the logger user
	 * @param processType the process type
	 * @param destSecurities the dest securities
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executePreliminarProcessAlgorithm(List<Object[]> distinctHolders,CorporativeOperation corpOperation,LoggerUser loggerUser
			,ImportanceEventType processType,List<SecuritiesCorporative> destSecurities) throws ServiceException{
		BigDecimal delFactor = null;
		Integer blockLevel =  LevelAffectationType.BLOCK.getCode();
		Map<String, BigDecimal> mpDeliveryFactors = new HashMap<String, BigDecimal>();
		CorporativeProcessResult totalProcessResult = null;
		List<BlockCorporativeResult> blockDocuments = null;
		Map<String, Object[]> holderAccAdjustments = corporateExecutorService.getHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.IND_YES,false);
		for (Object[] arrObj : distinctHolders) {
			totalProcessResult = new CorporativeProcessResult();
			String holderAlternateCode = (String) arrObj[0];
			BigDecimal totalBalance = ((BigDecimal) arrObj[1]);
			
			List<Object[]> holderAccounts =corporateExecutorService.getBalancesForHolder(corpOperation.getSecurities().getIdSecurityCodePk(), holderAlternateCode);
			if (holderAccounts != null && holderAccounts.size() > 0) {
				List<CorporativeProcessResult> holderAccOrigin = null;
				List<CorporativeProcessResult> holderAccResults = new ArrayList<CorporativeProcessResult>();
				holderAccOrigin =  fixer.fillProcessResultsForOrigin(holderAccounts, corpOperation);
				for(SecuritiesCorporative  destSecurity : destSecurities){
					delFactor = destSecurity.getDeliveryFactor();
					BigDecimal roundedTotal = CorporateProcessUtils.calculateByFactor(totalBalance,delFactor, 
							corpOperation.getIndRound());
					totalProcessResult.setTotalBalance(roundedTotal);
					totalProcessResult.setCorporativeOperation(corpOperation);
					totalProcessResult.setSecurity(destSecurity.getSecurity());
					mpDeliveryFactors.put(destSecurity.getSecurity().getIdSecurityCodePk(), delFactor);
					holderAccResults.addAll(fixer.fixBalances(holderAccounts, corpOperation, delFactor, 
						corpOperation.getIndRound(), false, false, true, processType,null));
				}
				BigDecimal tmpDelFactor = null;
				if(holderAccOrigin != null && holderAccResults != null){
					for (CorporativeProcessResult orginCorpProcessResults : holderAccOrigin) {								
						
						if(CorporateProcessUtils.hasBlockBalances(orginCorpProcessResults)){
							List<Object[]> blockedDocs =corporateExecutorService.getBlockBalanceDetails(orginCorpProcessResults.getParticipant().getIdParticipantPk(), 
									orginCorpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
									orginCorpProcessResults.getSecurity().getIdSecurityCodePk(),AffectationStateType.BLOCKED.getCode(), blockLevel);									
							
							if(blockedDocs != null && blockedDocs.size() > 0){
								if(orginCorpProcessResults.getBlockCorporativeResults() == null){
									orginCorpProcessResults.setBlockCorporativeResults(new ArrayList<BlockCorporativeResult>());
								}
								orginCorpProcessResults.getBlockCorporativeResults()
								.addAll(documentFixer.fillOriginBlockCorporativeResult(blockedDocs, orginCorpProcessResults,null));
								List<CorporativeProcessResult> secCorpProcessResults  = new ArrayList<CorporativeProcessResult>();
								for(CorporativeProcessResult tempRes : holderAccResults){
									if(tempRes.getHolderAccount().getIdHolderAccountPk().equals(orginCorpProcessResults.getHolderAccount().getIdHolderAccountPk())){
										secCorpProcessResults.add(tempRes);
									}
								}
								
								for(CorporativeProcessResult corpProcessResults : secCorpProcessResults){
									tmpDelFactor = CorporateProcessUtils.getHolderAdjustmentFactor(holderAccAdjustments, corpProcessResults);
									if(tmpDelFactor == null){
										tmpDelFactor = mpDeliveryFactors.get(corpProcessResults.getSecurity().getIdSecurityCodePk());
									}
									blockDocuments = documentFixer.fixDocuments(corpProcessResults, blockedDocs, tmpDelFactor,
											corpOperation.getIndRound(), false, false, true, fixer, processType,null);
									corpProcessResults.setBlockCorporativeResults(blockDocuments);
								}
							}
						}
					}
					corporateExecutorService.saveAll(holderAccOrigin, loggerUser);
					corporateExecutorService.saveAll(holderAccResults, loggerUser);
					holderAccounts.clear();
					holderAccounts = null;							
				}
			}
		}
		corporateExecutorService.updateHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO, CorporateProcessConstants.IND_YES, CorporateProcessConstants.IND_YES, loggerUser);

	}

	
	/**
	 * This method is used execute the special corporate process for Definitive process execution.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executeDefinitiveProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started definitive process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());	
		List<SecuritiesCorporative> destSecurities = corporateExecutorService.getExcisionSecurities(corpOperation.getIdCorporativeOperationPk());
		if(destSecurities == null || (destSecurities != null && destSecurities.isEmpty())){
			log.error("No Destiny securities registered for process: "+corpOperation.getIdCorporativeOperationPk());
			return false;
		}
		List<Object> participants = corporateExecutorService.getDistinctParticpantsForDefinitive(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		if(participants !=null && !participants.isEmpty()){
			for(Object part : participants){
				Long participant = (Long)part;
				//transaction is for participant
				this.splitCorporateProcessExecutor.executeDefinitiveProcessAccounts(corpOperation, participant, processType, loggerUser);
			}
		}
		this.splitCorporateProcessExecutor.executeDefinitiveProcessSecurities(corpOperation, processType, loggerUser, destSecurities);
		return true;
	}
	
	/**
	 * Execute definitive process securities.
	 *
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @param destSecurities the dest securities
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessSecurities(CorporativeOperation corpOperation,ImportanceEventType processType,LoggerUser loggerUser
			,List<SecuritiesCorporative> destSecurities) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		String orgSecurity = corpOperation.getSecurities().getIdSecurityCodePk();				
		SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, 
				corpOperation.getSecurities().getShareCapital(),  corpOperation.getSecurities().getCirculationBalance(), orgSecurity);
		//security is full reference because error in cache L1
		secOpe.setSecurities(corpOperation.getSecurities());
		corporateExecutorService.create(secOpe, loggerUser);
		SecuritiesComponentService securitiesComponent = securitiesComponentService.get();
		SecuritiesComponentTO securitiesComponentTO = CorporateProcessUtils.getSecuritiesComponent(processType, 
				secOpe, corpOperation.getSecurities().getPhysicalBalance(), 
				corpOperation.getSecurities().getDesmaterializedBalance(), CorporateProcessConstants.BALANCE_ORIGIN);
		securitiesComponentTO.setShareBalance(corpOperation.getSecurities().getShareBalance());
		securitiesComponentTO.setCirculationBalance(corpOperation.getSecurities().getCirculationBalance());
		//subtract security balances in origin
		securitiesComponent.executeSecuritiesComponent(securitiesComponentTO);
		for(SecuritiesCorporative  destSecurity : destSecurities){
			BigDecimal totalBalance =(BigDecimal) corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
					CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY, 
					destSecurity.getSecurity().getIdSecurityCodePk());
			//TODO dont remove previus old security balances
//			if(orgSecurity.equals(destSecurity.getSecurity().getIdSecurityCodePk())){
//				orgSecurity = null;
//			}else{
//				secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, destSecurity.getSecurity().getShareCapital(), destSecurity.getSecurity().getCirculationBalance(),
//						destSecurity.getSecurity().getIdSecurityCodePk());
//				secOpe.setSecurities(destSecurity.getSecurity());
//				corporateExecutorService.create(secOpe, loggerUser);				
//				securitiesComponent.executeSecuritiesComponent(CorporateProcessUtils.getSecuritiesComponent(processType, secOpe, 
//						destSecurity.getSecurity().getPhysicalBalance(), destSecurity.getSecurity().getDesmaterializedBalance(), CorporateProcessConstants.BALANCE_ORIGIN));
//			}
			BigDecimal circulation = destSecurity.getNewCirculationBalance().subtract(destSecurity.getOldCirculationBalance());
			BigDecimal capital = destSecurity.getNewShareCapital().subtract(destSecurity.getOldShareCapital());
			if(circulation.compareTo(BigDecimal.ZERO) < 0){
				circulation = circulation.abs();					
			}
			if(circulation.compareTo(totalBalance) < 0){
				circulation = totalBalance;
			}
			secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, capital,circulation, destSecurity.getSecurity().getIdSecurityCodePk());
			secOpe.setSecurities(destSecurity.getSecurity());
			corporateExecutorService.create(secOpe, loggerUser);
			securitiesComponentTO = new SecuritiesComponentTO();
			securitiesComponentTO = CorporateProcessUtils.getSecuritiesComponent(processType, 
					secOpe, destSecurity.getNewCirculationBalance().subtract(totalBalance), totalBalance, CorporateProcessConstants.BALANCE_DESTINY);
			securitiesComponentTO.setShareBalance(totalBalance);
			securitiesComponentTO.setCirculationBalance(circulation);
			securitiesComponent.executeSecuritiesComponent(securitiesComponentTO);
//			corporateExecutorService.updateSecurity(destSecurity.getSecurity().getIdSecurityCodePk(), null, null, null, destSecurity.getNewShareCapital(), loggerUser);
		}
		if(orgSecurity != null){
			corporateExecutorService.updateSecurity(orgSecurity, CommonsUtilities.currentDateTime(), 
					SecurityStateType.ESCISIONADO.getCode(), null, BigDecimal.ZERO,null, loggerUser);
		}
	}
	
	/**
	 * Execute definitive process accounts.
	 *
	 * @param corpOperation the corp operation
	 * @param participant the participant
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessAccounts(CorporativeOperation corpOperation,Long participant,ImportanceEventType processType 
			,LoggerUser loggerUser) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		//Get reference to component
		AccountsComponentService accountsComponentService = executorComponentServiceBean.get();
		List<Object[]> blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForUnblockDefinitive(corpOperation.getIdCorporativeOperationPk(), participant, corpOperation.getSecurities().getIdSecurityCodePk(), 
				CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_ORIGIN);
		if(blockDocuments != null && !blockDocuments.isEmpty()){
			for(Object[] data : blockDocuments){
				BigDecimal quantity = (BigDecimal)data[2];
				Long blockOperationId = (Long)data[3];
				HolderAccountBalanceTO account = new HolderAccountBalanceTO();
				account.setIdHolderAccount((Long)data[5]);
				account.setIdParticipant((Long)data[6]);
				account.setIdSecurityCode((String)data[7]);
				account.setStockQuantity(quantity);
				account.setFinalDate(corpOperation.getDeliveryDate());
				List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
				accounts.add(account);
				Long refBlockOperationId = null;
				BlockOperation blockOperation = corporateExecutorService.find(BlockOperation.class, blockOperationId);
				List<BlockMarketFactOperation> blockMarketFatcs = blockOperation.getBlockMarketFactOperations();
				UnblockRequest unblockRequest = documentFixer.prepareUnblockOperation(data, loggerUser.getUserName());	
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
					//movements marketfact for each blockmarketfact
					MarketFactAccountTO mktFact;
					for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
						if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
							mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(blockMarketFact.getMarketDate());
							mktFact.setMarketPrice(blockMarketFact.getMarketPrice());
							mktFact.setMarketRate(blockMarketFact.getMarketRate());
							mktFact.setQuantity(blockMarketFact.getActualBlockBalance());
							account.getLstMarketFactAccounts().add(mktFact);
						}
					}
					UnblockMarketFactOperation unblockMarketFact;
					for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
						if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
							unblockMarketFact = new UnblockMarketFactOperation();
							unblockMarketFact.setUnblockOperation(unblockRequest.getUnblockOperation().get(0));
							unblockMarketFact.setUnblockQuantity(blockMarketFact.getActualBlockBalance());
							unblockMarketFact.setUnblockMarketState(UnblockMarketOperationStateType.REGISTER.getCode());
							unblockMarketFact.setRegistryUser(loggerUser.getUserName());
							unblockMarketFact.setRegistryDate(CommonsUtilities.currentDateTime());
							unblockMarketFact.setMarketDate(blockMarketFact.getMarketDate());
							unblockMarketFact.setMarketPrice(blockMarketFact.getMarketPrice());
							unblockMarketFact.setMarketRate(blockMarketFact.getMarketRate());
							unblockRequest.getUnblockOperation().get(0).getUnblockMarketFactOperations().add(unblockMarketFact);
						}
					}
				}
				corporateExecutorService.create(unblockRequest, loggerUser);
				corporateExecutorService.updateBlockOperation(blockOperationId, quantity, true, true, AffectationStateType.UNBLOCKED.getCode(), null, loggerUser);
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
						if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
							blockMarketFact.setActualBlockBalance(new BigDecimal(0));
							corporateExecutorService.update(blockMarketFact);
						}
					}
				}
				refBlockOperationId = blockOperationId;
				blockOperationId = unblockRequest.getUnblockOperation().get(0).getIdUnblockOperationPk();
				//movements unblock origin balances
				AccountsComponentTO accountsComponentTO = CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
						blockOperationId, refBlockOperationId, null, null, processType, accounts, null, CorporateProcessUtils.getBlockBalance((Integer)data[0],(Integer)data[8]),idepositarySetup.getIndMarketFact());
				//change default operationType to unblockOperationType
				accountsComponentTO.setIdOperationType(CorporateProcessUtils.getOperationUnblock(CorporateProcessUtils.getBlockBalance((Integer)data[0],null), accountsComponentTO.getIdOperationType()));
				accountsComponentService.executeAccountsComponent(accountsComponentTO);
			}
			
		}
		//Here we are passing the Holder Accounts,securities, participant pk values, origintarget and totalbalances from the CorporativeProcessResults
		AccountsComponentTO accountsComponentTO = fixer.generateAccountComponent(
				corporateExecutorService.getCorporateResultsForDefinitiveProcess(corpOperation.getIdCorporativeOperationPk(), participant, 
						CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null), processType, corpOperation);
		accountsComponentService.executeAccountsComponent(accountsComponentTO);
		
		blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForDefinitive(corpOperation.getIdCorporativeOperationPk(), 
					participant, null, CorporateProcessConstants.IND_NO_REMAINDER, 
					CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.BALANCE_DESTINY);
			
			if(blockDocuments != null && !blockDocuments.isEmpty()){
				for(Object[] data : blockDocuments){
					 BigDecimal quantity = (BigDecimal)data[22];
					 HolderAccountBalanceTO accountBalanceTO = new HolderAccountBalanceTO();								 
					 accountBalanceTO.setIdParticipant((Long)data[10]);
					 accountBalanceTO.setIdSecurityCode((String)data[11]);
					 accountBalanceTO.setIdHolderAccount((Long)data[12]);
					 accountBalanceTO.setStockQuantity(quantity);
					 List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
					 accounts.add(accountBalanceTO);
					 Long blockOperationId = null;
					 BlockRequest blockRequest = documentFixer.prepareBlockOperation(data, loggerUser.getUserName(), quantity, quantity);
					 if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
							BlockMarketFactOperation blockMarketFactOperation = new BlockMarketFactOperation();
							blockMarketFactOperation.setMarketDate(corpOperation.getMarketDate());
							blockMarketFactOperation.setMarketPrice(corpOperation.getMarketPrice());
							blockMarketFactOperation.setOriginalBlockBalance(quantity);
							blockMarketFactOperation.setActualBlockBalance(quantity);
							blockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
							blockMarketFactOperation.setBlockOperation(blockRequest.getBlockOperation().get(0));
							blockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
							blockMarketFactOperation.setRegistryUser(loggerUser.getUserName());
							blockRequest.getBlockOperation().get(0).getBlockMarketFactOperations().add(blockMarketFactOperation);
							//marketfact movement
							//movements marketfact
							MarketFactAccountTO mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(corpOperation.getMarketDate());
							mktFact.setMarketPrice(corpOperation.getMarketPrice());
							mktFact.setQuantity(quantity);
							accountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
							accountBalanceTO.getLstMarketFactAccounts().add(mktFact);
					}
				 	corporateExecutorService.create(blockRequest, loggerUser);
				 	blockOperationId = blockRequest.getBlockOperation().get(0).getIdBlockOperationPk();
					 //call component block movements
					 accountsComponentService.executeAccountsComponent(CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
							blockOperationId, null, null, null, processType, null, accounts, CorporateProcessUtils.getBlockBalance((Integer)data[0], (Integer)data[24]),idepositarySetup.getIndMarketFact()));
				}
			}
							
		corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), 
				participant, true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null, loggerUser);

	}
	
}
