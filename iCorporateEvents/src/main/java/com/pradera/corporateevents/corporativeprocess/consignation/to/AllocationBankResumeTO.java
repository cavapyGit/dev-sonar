package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class AllocationBankResumeTO.
 * @author Pradera Technologies
 *
 */
public class AllocationBankResumeTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The bank id. */
	private Long bankID;
	
	/** The bank. */
	private String bank;
	
	/** The holder quantity. */
	private String holderQuantity;
	
	/** The bank amount. */
	private String bankAmount;


	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public String getBank() {
		return bank;
	}
	
	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}
	
	/**
	 * Gets the holder quantity.
	 *
	 * @return the holder quantity
	 */
	public String getHolderQuantity() {
		return holderQuantity;
	}
	
	/**
	 * Sets the holder quantity.
	 *
	 * @param holderQuantity the new holder quantity
	 */
	public void setHolderQuantity(String holderQuantity) {
		this.holderQuantity = holderQuantity;
	}
	
	/**
	 * Gets the bank amount.
	 *
	 * @return the bank amount
	 */
	public String getBankAmount() {
		return bankAmount;
	}
	
	/**
	 * Sets the bank amount.
	 *
	 * @param bankAmount the new bank amount
	 */
	public void setBankAmount(String bankAmount) {
		this.bankAmount = bankAmount;
	}
	
	/**
	 * Gets the bank id.
	 *
	 * @return the bank id
	 */
	public Long getBankID() {
		return bankID;
	}
	
	/**
	 * Sets the bank id.
	 *
	 * @param bankID the new bank id
	 */
	public void setBankID(Long bankID) {
		this.bankID = bankID;
	}
}