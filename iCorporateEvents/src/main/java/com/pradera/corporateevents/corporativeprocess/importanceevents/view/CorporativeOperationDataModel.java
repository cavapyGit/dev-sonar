package com.pradera.corporateevents.corporativeprocess.importanceevents.view;


import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.corporatives.CorporativeOperation;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporativeOperationDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class CorporativeOperationDataModel extends ListDataModel<CorporativeOperation> implements SelectableDataModel<CorporativeOperation>, Serializable {

	/**
	 * Instantiates a new corporative operation data model.
	 */
	public CorporativeOperationDataModel(){

	}

	/**
	 * Instantiates a new corporative operation data model.
	 *
	 * @param data the data
	 */
	public CorporativeOperationDataModel(List<CorporativeOperation> data){
		super(data);
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public CorporativeOperation getRowData(String rowKey) {

		List<CorporativeOperation> operatiosn = (List<CorporativeOperation>) getWrappedData();
		for(CorporativeOperation operation: operatiosn){
			if(operation.getIdCorporativeOperationPk().equals(Long.parseLong(rowKey))){
				return operation;

			}
		}
		return null;

	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(CorporativeOperation operation) {
		return operation.getIdCorporativeOperationPk();

	}

}
