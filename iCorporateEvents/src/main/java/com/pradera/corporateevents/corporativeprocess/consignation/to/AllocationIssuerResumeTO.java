package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;



// TODO: Auto-generated Javadoc
/**
 * The Class AllocationIssuerResumeTO.
 * @author Pradera Technologies
 *
 */
public class AllocationIssuerResumeTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The bank. */
	private String bank;
	
	/** The rnt description. */
	private String rntDescription;
	
	/** The document type number. */
	private String documentTypeNumber;
	
	/** The bank account number. */
	private String bankAccountNumber;
	
	/** The corporative id. */
	private String corporativeID;
	
	/** The security. */
	private String security;
	
	/** The amount. */
	private BigDecimal amount;
	
	/** The include commission. */
	private String includeCommission;
	
	/** The process type. */
	private String processType;
	
	private String participantDescription;

	private BigDecimal retentionAmount;

	/**
	 * Gets the bank.
	 *
	 * @return the bank
	 */
	public String getBank() {
		return bank;
	}
	
	/**
	 * Sets the bank.
	 *
	 * @param bank the new bank
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}
	
	/**
	 * Gets the rnt description.
	 *
	 * @return the rnt description
	 */
	public String getRntDescription() {
		return rntDescription;
	}
	
	/**
	 * Sets the rnt description.
	 *
	 * @param rntDescription the new rnt description
	 */
	public void setRntDescription(String rntDescription) {
		this.rntDescription = rntDescription;
	}
	
	/**
	 * Gets the document type number.
	 *
	 * @return the document type number
	 */
	public String getDocumentTypeNumber() {
		return documentTypeNumber;
	}
	
	/**
	 * Sets the document type number.
	 *
	 * @param documentTypeNumber the new document type number
	 */
	public void setDocumentTypeNumber(String documentTypeNumber) {
		this.documentTypeNumber = documentTypeNumber;
	}
	
	/**
	 * Gets the bank account number.
	 *
	 * @return the bank account number
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	
	/**
	 * Sets the bank account number.
	 *
	 * @param bankAccountNumber the new bank account number
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	
	/**
	 * Gets the corporative id.
	 *
	 * @return the corporative id
	 */
	public String getCorporativeID() {
		return corporativeID;
	}
	
	/**
	 * Sets the corporative id.
	 *
	 * @param corporativeID the new corporative id
	 */
	public void setCorporativeID(String corporativeID) {
		this.corporativeID = corporativeID;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public String getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(String security) {
		this.security = security;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the include commission.
	 *
	 * @return the include commission
	 */
	public String getIncludeCommission() {
		return includeCommission;
	}
	
	/**
	 * Sets the include commission.
	 *
	 * @param includeCommission the new include commission
	 */
	public void setIncludeCommission(String includeCommission) {
		this.includeCommission = includeCommission;
	}
	
	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public String getProcessType() {
		return processType;
	}
	
	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getParticipantDescription() {
		return participantDescription;
	}

	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}

	public BigDecimal getRetentionAmount() {
		return retentionAmount;
	}

	public void setRetentionAmount(BigDecimal retentionAmount) {
		this.retentionAmount = retentionAmount;
	}
	
}