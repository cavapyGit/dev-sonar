package com.pradera.corporateevents.corporativeprocess.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.billing.to.BillingServiceTo;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateDetailType;
import com.pradera.model.accounts.type.NationalityType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.RateExclusion;
import com.pradera.model.billing.type.RateExceptionStatus;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.corporatives.SecuritiesCorporative;
import com.pradera.model.corporatives.TaxHolderRate;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.ChangeOwnershipMotiveType;
import com.pradera.model.custody.type.ChangeOwnershipStatusType;
import com.pradera.model.custody.type.ParticipantUnionOperationStateType;
import com.pradera.model.guarantees.type.GuaranteeClassType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CorporateExecutorServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12/06/2014
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class CorporateExecutorServiceBean extends CrudDaoServiceBean {
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;

    /**
     * Default constructor. 
     */
    public CorporateExecutorServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Delete previous preliminary information.
     *
     * @param corpProcessId the corp process id
     * @param State the state
     * @param blockFlag the block flag
     * @param reportFlag the report flag
     * @throws ServiceException the service exception
     */
    public void deletePreviousPreliminaryInformation(Long corpProcessId, Integer State, boolean blockFlag, boolean reportFlag) throws ServiceException{

		Query query = em.createQuery("delete from CorporativeResultDetail crd where crd.corporativeOperation.idCorporativeOperationPk = :corpprocessid ");
		query.setParameter("corpprocessid", corpProcessId);	
		query.executeUpdate();  

		if(blockFlag){
			query = em.createQuery("delete from BlockCorporativeResult bcr where bcr.corporativeOperation.idCorporativeOperationPk = :corpprocessid and bcr.indProcessed = :state ");
			query.setParameter("corpprocessid", corpProcessId);	
			query.setParameter("state", State);
			query.executeUpdate();
		}
		if(reportFlag){
			query = em.createQuery("delete from ReportCorporativeResult rcr where rcr.corporativeOperation.idCorporativeOperationPk = :corpprocessid and rcr.indProcessed = :state ");
			query.setParameter("corpprocessid", corpProcessId);	
			query.setParameter("state", State);
			query.executeUpdate();
		}

		query = em.createQuery("delete from CorporativeProcessResult cpr where cpr.corporativeOperation.idCorporativeOperationPk = :corpprocessid and cpr.indProcessed = :state ");
		query.setParameter("corpprocessid", corpProcessId);	
		query.setParameter("state", State);
		query.executeUpdate();

	}
    
    /**
     * Gets the stock calculation process.
     *
     * @param recordDate the record date
     * @param cutoffDate the cutoff date
     * @param isin the isin
     * @param stockState the stock state
     * @param stockClass the stock class
     * @param stockType the stock type
     * @return the stock calculation process
     * @throws ServiceException the service exception
     */
    public Long getStockCalculationProcess(Date recordDate, Date cutoffDate, String isin, Integer stockState, Integer stockClass, Integer stockType)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select max(sp.idStockCalculationPk) from StockCalculationProcess sp ");
		sbQuery.append(" where trunc(sp.registryDate) = trunc(:registryDate)");
		sbQuery.append(" and trunc(sp.cutoffDate) = trunc(:cutoffdate)");
		sbQuery.append(" and sp.security.idSecurityCodePk = :securities");
		sbQuery.append(" and sp.stockState = :state");
		sbQuery.append(" and sp.stockClass = :stockclass");
		sbQuery.append(" and sp.stockType = :stocktype");
		sbQuery.append(" order by sp.idStockCalculationPk desc");

		parameters.put("registryDate", recordDate);
		parameters.put("cutoffdate", cutoffDate);
		parameters.put("securities", isin);
		parameters.put("state", stockState);
		parameters.put("stockclass", stockClass);
		parameters.put("stocktype", stockType);
		List<Long> result = findListByQueryString(sbQuery.toString(), parameters);
		if(result != null && result.size() > 0){
			return result.get(0);
		}else{
			return null;
		}

	}
    
    /**
     * Gets the distinct holders for process.
     *
     * @param stockProcessId the stock process id
     * @return the distinct holders for process
     */
    public List<Object[]> getDistinctHoldersForProcess(Long stockProcessId){
    	Query query = em.createNamedQuery(CorporativeOperation.CORPORATIVE_OPERATION_HOLDER_PROCESS);
    	query.setParameter("stockprocessid", stockProcessId);
    	return query.getResultList();
    }
    
    /**
     * This method is used to get the holder account adjustments from the HolderAccountAdjusment table if we have any holder account adjustments for 
     * the corporative operation.
     *
     * @param CorpProcessId the corp process id
     * @param state the state
     * @param indDeliveryBenefit the ind delivery benefit
     * @param isSuccesion the is succesion
     * @return the holder account adjustments
     * @throws ServiceException the service exception
     */
    public Map<String, Object[]> getHolderAccountAdjustments(Long CorpProcessId, Integer state, Integer indDeliveryBenefit, boolean isSuccesion)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select haa.holderAccount.idHolderAccountPk, haa.security.idSecurityCodePk, haa.indDeliverBenefit, haa.indCollectTax,");
		sbQuery.append(" haa.indCollectCommissiones, haa.newBenefitAmount, haa.newTax, haa.newCommissiones, ");
		sbQuery.append(" haa.oldBenefitAmount, haa.oldTax, haa.oldCommisiones from HolderAccountAdjusment haa ");
		sbQuery.append(" where haa.corporativeOperation.idCorporativeOperationPk = :corpprocessid ");
		//sbQuery.append(" and haa.adjustmentState = :state");
		sbQuery.append(" and (haa.indDeliverBenefit = :indyes or haa.indCollectTax = :indyes or haa.indCollectCommissiones = :indyes) ");
		parameters.put("corpprocessid", CorpProcessId);
		//parameters.put("state", state);
		parameters.put("indyes", indDeliveryBenefit);

		List<Object[]> result = findListByQueryString(sbQuery.toString(), parameters);
		Map<String, Object[]> mp = new HashMap<String, Object[]>();
		if(result != null && !result.isEmpty()){
			for(Object[] data : result){
				String key = String.valueOf((Long)data[0]);
				if(isSuccesion){
					key = key+GeneralConstants.HYPHEN+((String)data[1]);
				}
				mp.put(key, data);
			}
		}		
		return mp;
	}
    
    
    /**
     * This method is used to get the change of ownership values from the CustodyOperation, ChangeOwnershipOperation , ChangeOwnershipDetail tables for processing the 
     * cash corporate process, stock corporate process, amortization processes and SubscriptionCorporateProcessExecutor.
     *
     * @param isinSecurity the isin security
     * @param registryDate the registry date
     * @return the changeof owner ship sucession
     * @throws ServiceException the service exception
     */
    public Map<Long, List<Object[]>> getChangeofOwnerShipSucession(String isinSecurity, Date registryDate)throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cod.participant.idParticipantPk , cod.holderAccount.idHolderAccountPk, cod.idChangeOwnershipDetailPk, ");
		sbQuery.append(" coo.balanceType, cod.availableBalance, cod.pawnBalance, cod.banBalance, cod.otherBlockBalance, ");
		sbQuery.append(" cod.reserveBalance, cod.oppositionBalance ");
		sbQuery.append(" from CustodyOperation co, ChangeOwnershipOperation coo, ChangeOwnershipDetail cod ");
		sbQuery.append(" where co.idCustodyOperationPk = coo.idChangeOwnershipPk ");
		sbQuery.append(" and coo.idChangeOwnershipPk = cod.changeOwnershipOperation.idChangeOwnershipPk ");
		sbQuery.append(" and trunc(co.confirmDate) > trunc(:cuttoffdate) and coo.motive = :motive ");
		sbQuery.append(" and coo.state = :state and cod.indOriginTarget = :origin ");
		sbQuery.append(" and cod.security.idSecurityCodePk = :isin order by cod.holderAccount.idHolderAccountPk, coo.balanceType ");

		parameters.put("cuttoffdate", registryDate);

		parameters.put("motive", ChangeOwnershipMotiveType.SUCESION.getCode());
		parameters.put("state", ChangeOwnershipStatusType.CONFIRMADO.getCode());
		parameters.put("origin", Long.valueOf(CorporateProcessConstants.BALANCE_ORIGIN));
		parameters.put("isin", isinSecurity);

		List<Object[]> result = findListByQueryString(sbQuery.toString(), parameters);
		Map<Long, List<Object[]>> mp = new HashMap<Long, List<Object[]>>();
		if(result != null && !result.isEmpty()){
			for(Object[] data : result){
				Long holder = (Long)data[1];
				if(mp.containsKey(holder)){
					mp.get(holder).add(data);
				}else{
					List<Object[]> datalist = new ArrayList<Object[]>();
					datalist.add(data);
					mp.put(holder, datalist);
				}
			}
		}		
		return mp;
	}	
    
    /**
     * This method is used to get the holder accounts, balances from the stock calculation balances by sending the Stock Calculation Process Id,
     * participant.
     *
     * @param stockProcessId the stock process id
     * @param holderAlternateCode the holder alternate code
     * @return the stock calculation balances
     * @throws ServiceException the service exception
     */
    public List<Object[]> getStockCalculationBalances(Long stockProcessId, String holderAlternateCode) throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select scb.holderAccount.idHolderAccountPk, scb.availableBalance + scb.accreditationBalance, "); //Accredation balance must deliver in available
		sbQuery.append(" scb.pawnBalance, scb.banBalance, scb.otherBlockBalance, scb.reserveBalance, ");
		sbQuery.append(" scb.oppositionBalance, scb.marginBalance, scb.reportingBalance, scb.reportedBalance, scb.totalBalance, " );
		sbQuery.append(" scb.participant.idParticipantPk, scb.custodyAmount, scb.security.idSecurityCodePk ");
		sbQuery.append(" from StockCalculationBalance scb, HolderAccount ha ");
		sbQuery.append(" where scb.stockCalculationProcess.idStockCalculationPk = :stockprocessid");
		sbQuery.append(" and scb.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
		sbQuery.append(" and ha.alternateCode like :alternatecode ");
		sbQuery.append(" order by scb.availableBalance ");

		parameters.put("stockprocessid", stockProcessId);
		parameters.put("alternatecode", '%'+holderAlternateCode+'%');

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the report balances from the ReportStockCalculation table by sending the stock calculation process id, participant id, 
     * holder account id and security.
     *
     * @param stockProcessId the stock process id
     * @param participant the participant
     * @param holderAccount the holder account
     * @param security the security
     * @return the stock report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getStockReportOperations(Long stockProcessId, Long participant, Long holderAccount, String security)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rsc.guaranteeType, rsc.operationRole, rsc.guaranteeBalance, ");
		sbQuery.append(" rsc.holderAccountOperation.idHolderAccountOperationPk , rsc.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append(" from ReportStockCalculation rsc ");
		sbQuery.append(" where rsc.stockCalculationProcess.idStockCalculationPk = :stockprocessid");
		sbQuery.append(" and rsc.participant.idParticipantPk = :participant ");
		sbQuery.append(" and rsc.holderAccount.idHolderAccountPk = :holderaccount ");
		sbQuery.append(" and rsc.security.idSecurityCodePk = :security ");
		sbQuery.append(" order by rsc.guaranteeType, rsc.operationRole, rsc.guaranteeBalance ");

		parameters.put("stockprocessid", stockProcessId);
		parameters.put("participant", participant);
		parameters.put("holderaccount", holderAccount);
		parameters.put("security", security);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the participants with the unified state from ParticipantUnionOperation,ParticipantUnionBalance tables.
     *
     * @param security the security
     * @param registryDate the registry date
     * @return the unified participants
     * @throws ServiceException the service exception
     */
    public Map<Long, Object[]> getUnifiedParticipants(String security, Date registryDate)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select pub.originParticipant.idParticipantPk, pub.originHolderAccount.idHolderAccountPk, ");
		sbQuery.append(" pub.targetParticipant.idParticipantPk, pub.targetHolderAccount.idHolderAccountPk ");
		sbQuery.append(" from ParticipantUnionOperation puo, ParticipantUnionBalance pub ");
		sbQuery.append(" where puo.idPartUnionOperationPk = pub.participantUnionOperation.idPartUnionOperationPk ");
		sbQuery.append(" and trunc(puo.unionDate) > trunc(:cuttoffdate) and puo.state = :state ");
		sbQuery.append(" and pub.securities.idSecurityCodePk = :isin ");

		parameters.put("cuttoffdate", registryDate);
		parameters.put("state", ParticipantUnionOperationStateType.DEFINITIVE.getCode());
		parameters.put("isin", security);

		List<Object[]> result = findListByQueryString(sbQuery.toString(), parameters);
		Map<Long, Object[]> mp = new HashMap<Long, Object[]>();
		if(result != null && !result.isEmpty()){
			for(Object[] data : result){
				mp.put((Long)data[1], data);
			}
		}		
		return mp;
	}
    
    /**
     * This method is used to get the block stock calculation details like block type, block balance etc., from the  BlockStockCalculation table
	 * by sending the Stock Calculation Process Id, participant,holder account and Security
     * @param stockProcessId the stock process id
     * @param participant the participant
     * @param holderAccount the holder account
     * @param security the security
     * @return the block stock calculation details
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBlockStockCalculationDetails(Long stockProcessId, Long participant, Long holderAccount, String security)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select bsc.blockType, bsc.blockBalance, bsc.blockOperation.idBlockOperationPk, ");
		sbQuery.append(" bsc.holder.idHolderPk , bsc.indCashDividend, bsc.indContributionReturn, bsc.indInterest, ");
		sbQuery.append(" bsc.indStockDividend, bsc.indSuscription, bsc.participant.idParticipantPk,  bo.blockLevel, ");
		sbQuery.append(" bo.blockState, bsc.indAmortization, bsc.indRescue ");
		sbQuery.append(" from BlockStockCalculation bsc, BlockOperation bo ");
		sbQuery.append(" where bsc.stockCalculationProcess.idStockCalculationPk = :stockprocessid");
		sbQuery.append(" and bsc.participant.idParticipantPk = :participant ");
		sbQuery.append(" and bsc.holderAccount.idHolderAccountPk = :holderaccount ");
		sbQuery.append(" and bsc.security.idSecurityCodePk = :security ");
		sbQuery.append(" and bsc.blockOperation.idBlockOperationPk = bo.idBlockOperationPk ");
		sbQuery.append(" order by bsc.blockType, bsc.blockBalance ");

		parameters.put("stockprocessid", stockProcessId);
		parameters.put("participant", participant);
		parameters.put("holderaccount", holderAccount);
		parameters.put("security", security);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to update the corporative process results when the change of ownership values are available.
     *
     * @param corpRes the corp res
     * @param loggerUser the logger user
     * @throws ServiceException the service exception
     */
    public void updateOrCreate(CorporativeProcessResult corpRes, LoggerUser loggerUser) throws ServiceException{		
		Integer update = 0;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update CorporativeProcessResult corp set ");
		sbQuery.append(" corp.availableBalance = corp.availableBalance + :availableBalance ,");
		sbQuery.append(" corp.pawnBalance = corp.pawnBalance + :pawnBalance ,");
		sbQuery.append(" corp.banBalance = corp.banBalance + :banBalance ,");
		sbQuery.append(" corp.otherBlockBalance = corp.otherBlockBalance + :otherBlockBalance ,");
		sbQuery.append(" corp.reportedBalance = corp.reportedBalance + :reportedBalance ,");
		sbQuery.append(" corp.reportingBalance = corp.reportingBalance + :reportingBalance ,");
		sbQuery.append(" corp.reserveBalance = corp.reserveBalance + :reserveBalance ,");
		sbQuery.append(" corp.oppositionBalance = corp.oppositionBalance + :oppositionBalance ,");
		sbQuery.append(" corp.marginBalance = corp.marginBalance + :marginBalance ,");
		sbQuery.append(" corp.totalBalance = corp.totalBalance + :totalBalance ,");
		sbQuery.append(" corp.taxAmount = corp.taxAmount + :tax ,");
		sbQuery.append(" corp.exchangeAmount = corp.exchangeAmount + :exchangeAmount ,");
		sbQuery.append(" corp.lastModifyUser = :lastModifyuser"+",");
		sbQuery.append(" corp.lastModifyIp = :lastModifyip"+",");
		sbQuery.append(" corp.lastModifyDate = :lastModifiedDate"+",");
		sbQuery.append(" corp.lastModifyApp = :lastModifiedApp");
		sbQuery.append(" where corp.security.idSecurityCodePk = :sec");
		sbQuery.append(" and corp.participant.idParticipantPk = :part");
		sbQuery.append(" and corp.holderAccount.idHolderAccountPk = :holder");
		sbQuery.append(" and corp.corporativeOperation.idCorporativeOperationPk = :corpId");
		sbQuery.append(" and corp.indOriginTarget = :indOrigin ");
		sbQuery.append(" and corp.indRemanent = :indRem ");

		Query query =em.createQuery(sbQuery.toString());
		query.setParameter("availableBalance", corpRes.getAvailableBalance());
		query.setParameter("pawnBalance", corpRes.getPawnBalance());
		query.setParameter("banBalance", corpRes.getBanBalance());
		query.setParameter("otherBlockBalance", corpRes.getOtherBlockBalance());
		query.setParameter("reportedBalance", corpRes.getReportedBalance());
		query.setParameter("reportingBalance", corpRes.getReportingBalance());
		query.setParameter("reserveBalance", corpRes.getReserveBalance());
		query.setParameter("oppositionBalance", corpRes.getOppositionBalance());
		query.setParameter("marginBalance", corpRes.getMarginBalance());
		query.setParameter("totalBalance", corpRes.getTotalBalance());
		query.setParameter("tax", corpRes.getTaxAmount());
		query.setParameter("exchangeAmount", corpRes.getExchangeAmount());
		query.setParameter("lastModifyuser", loggerUser.getUserName());
		query.setParameter("lastModifyip", loggerUser.getIpAddress());
		query.setParameter("lastModifiedDate",CommonsUtilities.currentDate());
		query.setParameter("lastModifiedApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("sec", corpRes.getSecurity().getIdSecurityCodePk());
		query.setParameter("part", corpRes.getParticipant().getIdParticipantPk());
		query.setParameter("holder", corpRes.getHolderAccount().getIdHolderAccountPk());
		query.setParameter("corpId", corpRes.getCorporativeOperation().getIdCorporativeOperationPk());
		query.setParameter("indOrigin", corpRes.getIndOriginTarget());
		query.setParameter("indRem", corpRes.getIndRemanent());

		update=query.executeUpdate();

		if(update == 0){
			create(corpRes, loggerUser);
		}else{
			saveAll(corpRes.getBlockCorporativeResults(), loggerUser);
		}			
	}
    
    /**
     * Used to update the BlockCorporativeResult.
     *
     * @param CorpProcessId the corp process id
     * @param loggerUser the logger user
     * @return the int
     * @throws ServiceException the service exception
     */
    public int updateBlockCorporateForResult(Long CorpProcessId, LoggerUser loggerUser)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update BlockCorporativeResult bcr set bcr.corporativeProcessResult.idCorporativeProcessResult = (select ");
		sbQuery.append(" cpr.idCorporativeProcessResult from CorporativeProcessResult cpr ");
		sbQuery.append(" where cpr.security.idSecurityCodePk = bcr.security.idSecurityCodePk ");
		sbQuery.append(" and cpr.participant.idParticipantPk = bcr.participant.idParticipantPk ");
		sbQuery.append(" and cpr.holderAccount.idHolderAccountPk = bcr.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and cpr.corporativeOperation.idCorporativeOperationPk = bcr.corporativeOperation.idCorporativeOperationPk ");
		sbQuery.append(" and cpr.indOriginTarget = bcr.indOriginDestiny ");
		sbQuery.append(" and cpr.indRemanent = bcr.indRemanent ), ");
		sbQuery.append(" bcr.lastModifyUser = :lastModifyuser , ");
		sbQuery.append(" bcr.lastModifyIp = :lastModifyip , ");
		sbQuery.append(" bcr.lastModifyDate = :lastModifiedDate , ");
		sbQuery.append(" bcr.lastModifyApp = :lastModifiedApp ");
		sbQuery.append(" where bcr.corporativeOperation.idCorporativeOperationPk = :corpprocessid ");
		sbQuery.append(" and bcr.corporativeProcessResult.idCorporativeProcessResult is null ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lastModifyuser", loggerUser.getUserName());
		query.setParameter("lastModifyip", loggerUser.getIpAddress());
		query.setParameter("lastModifiedDate", CommonsUtilities.currentDateTime());
		query.setParameter("lastModifiedApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("corpprocessid", CorpProcessId);

		return query.executeUpdate();
	}
    
    /**
     * This method is used to get the results mechanism operation and guarantee balance values from the ReportStockCalculation,ReportCorporativeResult tables by sending the
     * seller role,buyer role,remnant,destiny origin.
     *
     * @param reportStockId the report stock id
     * @param corpOperId the corp oper id
     * @param guarantyType1 the guaranty type1
     * @param guarantyType2 the guaranty type2
     * @param sellerRole the seller role
     * @param buyerRole the buyer role
     * @param remanent the remanent
     * @param processed the processed
     * @param originDestiny the origin destiny
     * @param originDestiny1 the origin destiny1
     * @return the seller buyyer sum report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getSellerBuyyerSumReportOperations(Long reportStockId, Long corpOperId, Integer guarantyType1, Integer guarantyType2,
			Integer sellerRole, Integer buyerRole, Integer remanent, Integer processed, Integer originDestiny, Integer originDestiny1)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rcr.mechanismOperation.idMechanismOperationPk, sum(rsc.guaranteeBalance), ");
		sbQuery.append(" sum(rcr.guaranteeBalance),rcr.indOriginDestiny ");
		sbQuery.append(" from ReportStockCalculation rsc, ReportCorporativeResult rcr ");
		sbQuery.append(" where rsc.stockCalculationProcess.idStockCalculationPk = :stockprocessid");
		sbQuery.append(" and rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId");
		sbQuery.append(" and rcr.participant.idParticipantPk = rsc.participant.idParticipantPk ");
		sbQuery.append(" and rcr.holderAccount.idHolderAccountPk = rsc.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and rcr.security.idSecurityCodePk = rsc.security.idSecurityCodePk ");
		sbQuery.append(" and rcr.mechanismOperation.idMechanismOperationPk = rsc.mechanismOperation.idMechanismOperationPk ");	
		sbQuery.append(" and (rsc.guaranteeType = :guaranty1 or rsc.guaranteeType = :guaranty2) ");
		sbQuery.append(" and rsc.operationRole = :buyerRole and rcr.operationRole = :sellerRole ");
		sbQuery.append(" and rcr.indRemanent = :remanent ");
		sbQuery.append(" and rcr.indProcessed = :processed ");
		sbQuery.append(" and (rcr.indOriginDestiny = :origindestiny or rcr.indOriginDestiny = :origindestiny1) ");
		sbQuery.append(" group by rcr.mechanismOperation.idMechanismOperationPk,rcr.indOriginDestiny ");

		parameters.put("stockprocessid", reportStockId);
		parameters.put("corpOperId", corpOperId);
		parameters.put("guaranty1", guarantyType1);
		parameters.put("guaranty2", guarantyType2);
		parameters.put("sellerRole", sellerRole);
		parameters.put("buyerRole", buyerRole);
		parameters.put("remanent", remanent);
		parameters.put("processed", processed);
		parameters.put("origindestiny", originDestiny);
		parameters.put("origindestiny1", originDestiny1);
		
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the report stock calculation values for the seller operations.
     *
     * @param reportStockId the report stock id
     * @param guarantyType1 the guaranty type1
     * @param guarantyType2 the guaranty type2
     * @param sellerRole the seller role
     * @return the seller stock report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBuyerStockReportOperations(Long reportStockId, Integer guarantyType1, Integer guarantyType2,
			Integer sellerRole)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rsc.guaranteeType, rsc.guaranteeBalance, rsc.participant.idParticipantPk, rsc.holderAccount.idHolderAccountPk,");
		sbQuery.append(" rsc.security.idSecurityCodePk, rsc.holderAccountOperation.idHolderAccountOperationPk , rsc.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append(" from ReportStockCalculation rsc ");
		sbQuery.append(" where rsc.stockCalculationProcess.idStockCalculationPk = :reportStockId");
		sbQuery.append(" and (rsc.guaranteeType = :guaranty1 or rsc.guaranteeType = :guaranty2) ");
		sbQuery.append(" and  rsc.operationRole= :role ");
		sbQuery.append(" order by rsc.mechanismOperation.idMechanismOperationPk, rsc.guaranteeBalance ");

		parameters.put("reportStockId", reportStockId);
		parameters.put("guaranty1", guarantyType1);
		parameters.put("guaranty2", guarantyType2);
		parameters.put("role", sellerRole);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     *  This method is used to update the CorporativeProcessResult table if the remnant buyer seller operation is having the data.
     *
     * @param corpProcessId the corp process id
     * @param guarantyType1 the guaranty type1
     * @param guarantyType2 the guaranty type2
     * @param role the role
     * @param state the state
     * @return the int
     * @throws ServiceException the service exception
     */
    public int updateReportedBalanceofCorporateResults(Long corpProcessId, Integer guarantyType1, Integer guarantyType2,
			Integer role, Integer state) throws ServiceException{
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update CorporativeProcessResult cpr set cpr.reportedBalance = ");
		sbQuery.append(" coalesce((select sum(rcr.guaranteeBalance) from  ReportCorporativeResult rcr ");
		sbQuery.append(" where rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId ");
		sbQuery.append(" and (rcr.guaranteeType = :guaranty1 or rcr.guaranteeType = :guaranty2) ");
		sbQuery.append(" and  rcr.operationRole = :role and rcr.indRemanent = cpr.indRemanent ");
		sbQuery.append(" and rcr.indOriginDestiny = cpr.indOriginTarget   ");
		sbQuery.append(" and rcr.indProcessed = :state  ");
		sbQuery.append(" and rcr.security.idSecurityCodePk = cpr.security.idSecurityCodePk ");
		sbQuery.append(" and rcr.participant.idParticipantPk = cpr.participant.idParticipantPk ");
		sbQuery.append(" and rcr.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk), :defalutam) ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpOperId and cpr.indProcessed = :state ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("corpOperId", corpProcessId);
		query.setParameter("guaranty1", guarantyType1);
		query.setParameter("guaranty2", guarantyType2);
		query.setParameter("role", role);
		query.setParameter("state", state);
		query.setParameter("defalutam", BigDecimal.ZERO);
		return query.executeUpdate();
	}
    
    /**
     * This method is used to update the ReportCorporativeResult table if the remnant buyer seller operation is having the data.
     *
     * @param corpProcessId the corp process id
     * @param guarantyType1 the guaranty type1
     * @param guarantyType2 the guaranty type2
     * @param role the role
     * @param state the state
     * @return the int
     * @throws ServiceException the service exception
     */
    public int updateReportedOperationsofCorporateResult(Long corpProcessId, Integer guarantyType1, Integer guarantyType2,
			Integer role, Integer state) throws ServiceException{
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update ReportCorporativeResult rcr  set rcr.corporativeProcessResult.idCorporativeProcessResult = ");
		sbQuery.append(" (select cpr.idCorporativeProcessResult from  CorporativeProcessResult cpr");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpOperId and cpr.indProcessed = :state ");
		sbQuery.append(" and rcr.security.idSecurityCodePk = cpr.security.idSecurityCodePk ");
		sbQuery.append(" and rcr.participant.idParticipantPk = cpr.participant.idParticipantPk ");
		sbQuery.append(" and rcr.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and and rcr.indOriginDestiny = cpr.indOriginTarget and rcr.indRemanent = cpr.indRemanent )");
		sbQuery.append(" where rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId  ");
		sbQuery.append(" and (rcr.guaranteeType = :guaranty1 or rcr.guaranteeType = :guaranty2) ");
		sbQuery.append(" and  rcr.operationRole = :role  ");
		sbQuery.append(" and rcr.indProcessed = :state  ");


		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("corpOperId", corpProcessId);
		query.setParameter("guaranty1", guarantyType1);
		query.setParameter("guaranty2", guarantyType2);
		query.setParameter("role", role);
		query.setParameter("state", state);
		return query.executeUpdate();
	}
    
    /**
     * This method is used to update the Holder account adjustments table.
     *
     * @param CorpProcessId the corp process id
     * @param state the state
     * @param indYes the ind yes
     * @param newState the new state
     * @param loggerUser the logger user
     * @return the int
     * @throws ServiceException the service exception
     */
    public int updateHolderAccountAdjustments(Long CorpProcessId, Integer state, Integer indYes, Integer newState, LoggerUser loggerUser)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update HolderAccountAdjusment haa set haa.adjustmentState = :newstate, ");
		sbQuery.append(" haa.lastModifyUser = :modifyuser, haa.lastModifyDate = :modifydate,");
		sbQuery.append(" haa.lastModifyIp = :modifyip, haa.lastModifyApp = :modifyapp ");
		sbQuery.append(" where haa.corporativeOperation.idCorporativeOperationPk = :corpprocessid ");
		sbQuery.append(" and haa.adjustmentState = :state");
		sbQuery.append(" and (haa.indDeliverBenefit = :indyes or haa.indCollectTax = :indyes or haa.indCollectCommissiones = :indyes) ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("newstate", newState);
		query.setParameter("modifyuser", loggerUser.getUserName());
		query.setParameter("modifydate", CommonsUtilities.currentDateTime());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("corpprocessid", CorpProcessId);
		query.setParameter("state", state);
		query.setParameter("indyes", indYes);

		return query.executeUpdate();
	}
    
    /**
     * Update corporative stock calculation.
     *
     * @param corpOperation the corp operation
     */
    public void updateCorporativeStockCalculation(CorporativeOperation corpOperation){
		//Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("UPDATE CorporativeOperation CO");
		sbQuery.append("    SET CO.idStockCalculation = :idStockCalculation");
		sbQuery.append(" WHERE  CO.idCorporativeOperationPk =:idCorporativeOperationPk");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idStockCalculation", corpOperation.getIdStockCalculation());
		query.setParameter("idCorporativeOperationPk", corpOperation.getIdCorporativeOperationPk());
		query.executeUpdate();
	}
    /**
     * This method is used to get the participant id and sum of total balance from stock calculation balances group by participant for preliminary .
     *
     * @param corpOperationId the corp operation id
     * @param remnante the remnante
     * @param processed the processed
     * @return the distinct particpants for definitive
     * @throws ServiceException the service exception
     */
    public List<Object> getDistinctParticpantsForDefinitive(Long corpOperationId, Integer remnante, Integer processed)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct cpr.participant.idParticipantPk from CorporativeProcessResult cpr ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpoperid");
		sbQuery.append(" and cpr.indRemanent = :remanent");
		sbQuery.append(" and cpr.indProcessed = :processed");

		parameters.put("corpoperid", corpOperationId);
		parameters.put("remanent", remnante);
		parameters.put("processed", processed);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the holder accounts and all the balances from the Corporative Process Results by sending the corporative operation id, remnant, 
     * participant id,processed to execute the preliminary.
     *
     * @param corpProcessId the corp process id
     * @param participant the participant
     * @param remanent the remanent
     * @param processed the processed
     * @param originTarget the origin target
     * @return the corporate results for definitive process
     * @throws ServiceException the service exception
     */
    public List<Object[]> getCorporateResultsForDefinitiveProcess(Long corpProcessId, Long participant, Integer remanent, Integer processed, Integer originTarget)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cpr.holderAccount.idHolderAccountPk, cpr.participant.idParticipantPk, ");
		sbQuery.append(" cpr.security.idSecurityCodePk, cpr.indOriginTarget, cpr.totalBalance ");
		sbQuery.append(" from CorporativeProcessResult cpr ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
		sbQuery.append(" and cpr.indRemanent = :remanent");
		sbQuery.append(" and cpr.indProcessed = :processed");
		sbQuery.append(" and cpr.participant.idParticipantPk = :participant ");
		if(originTarget != null && originTarget > 0){
			sbQuery.append(" and cpr.indOriginTarget = :origintarget ");
			parameters.put("origintarget", originTarget);
		}
		sbQuery.append(" order by cpr.holderAccount.idHolderAccountPk, cpr.indOriginTarget ");

		parameters.put("corpprocessid", corpProcessId);
		parameters.put("remanent", remanent);
		parameters.put("processed", processed);
		parameters.put("participant", participant);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Update block operation.
     *
     * @param blockOperationId the block operation id
     * @param balance the balance
     * @param update the update
     * @param unblock the unblock
     * @param state the state
     * @param nominalValue the nominal value
     * @param loggerUser the logger user
     * @return the int
     * @throws ServiceException the service exception
     */
    public int updateBlockOperation(Long blockOperationId, BigDecimal balance, boolean update, boolean unblock, 
			Integer state, BigDecimal nominalValue, LoggerUser loggerUser) throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update BlockOperation bo set bo.actualBlockBalance = ");
		if(update){
			sbQuery.append("bo.actualBlockBalance ");
			if(unblock){
				sbQuery.append(" - ");
			}else{
				sbQuery.append(" + ");
			}
		}
		sbQuery.append(" :balance , ");
		if(state != null && state > 0){
			sbQuery.append(" bo.blockState = :state, ");
		}
		if(nominalValue != null){
			sbQuery.append(" bo.nominalValue = :nominalvalue, ");
		}
		sbQuery.append(" bo.lastModifyUser = :modifyuser, bo.lastModifyDate = :modifydate,");
		sbQuery.append(" bo.lastModifyIp = :modifyip, bo.lastModifyApp = :modifyapp ");
		sbQuery.append(" where bo.idBlockOperationPk = :bockId ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("balance", balance);
		if(state != null && state > 0){
			query.setParameter("state", state);
		}
		if(nominalValue != null){
			query.setParameter("nominalvalue", nominalValue);
		}
		query.setParameter("modifyuser", loggerUser.getUserName());
		query.setParameter("modifydate", CommonsUtilities.currentDateTime());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("bockId", blockOperationId);


		return query.executeUpdate();
	}
    
    /**
     * Update corporate results for finishing definitive.
     *
     * @param corpProcessId the corp process id
     * @param participant the participant
     * @param blockFlag the block flag
     * @param reportFlag the report flag
     * @param remanent the remanent
     * @param notProcessed the not processed
     * @param orginDestiny the orgin destiny
     * @param loggerUser the logger user
     * @return true, if successful
     */
    public boolean updateCorporateResultsForFinishingDefinitive(Long corpProcessId, Long participant, boolean blockFlag, boolean reportFlag, 
			Integer remanent, Integer notProcessed, Integer orginDestiny, LoggerUser loggerUser){

    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update CorporativeProcessResult cpr set cpr.indProcessed = :processed , ");
		sbQuery.append(" cpr.lastModifyUser = :modifyuser , cpr.lastModifyDate = :modifydate ,");
		sbQuery.append(" cpr.lastModifyIp = :modifyip , cpr.lastModifyApp = :modifyapp ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
		if(participant != null && participant > 0){
			sbQuery.append(" and cpr.participant.idParticipantPk = :participant ");
		}
		sbQuery.append(" and cpr.indProcessed = :prmprocessed ");
		sbQuery.append(" and cpr.indRemanent = :remanent ");
		if(orginDestiny != null && orginDestiny > 0)
			sbQuery.append(" and cpr.indOriginTarget = :orgindest");

		Query query = em.createQuery(sbQuery.toString());
		if(notProcessed.equals(CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED)){
			query.setParameter("processed", CorporateProcessConstants.DEFINITIVE_PROCESSED);
		}else if(notProcessed.equals(CorporateProcessConstants.DEFINITIVE_PROCESSED)){
			query.setParameter("processed", CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		}		
		query.setParameter("modifyuser", loggerUser.getUserName());
		query.setParameter("modifydate", CommonsUtilities.currentDateTime());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("corpprocessid", corpProcessId);
		if(participant != null && participant > 0){
			query.setParameter("participant", participant);
		}
		query.setParameter("prmprocessed", notProcessed);
		query.setParameter("remanent", remanent);
		if(orginDestiny != null && orginDestiny > 0)
			query.setParameter("orgindest", orginDestiny);

		query.executeUpdate();

		if(blockFlag){
			sbQuery = new StringBuilder();
			sbQuery.append("update BlockCorporativeResult bcr set bcr.indProcessed = :processed ,");
			sbQuery.append(" bcr.lastModifyUser = :modifyuser , bcr.lastModifyDate = :modifydate ,");
			sbQuery.append(" bcr.lastModifyIp = :modifyip , bcr.lastModifyApp = :modifyapp ");
			sbQuery.append(" where bcr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
			if(participant != null && participant > 0){
				sbQuery.append(" and bcr.participant.idParticipantPk = :participant ");
			}		
			sbQuery.append(" and bcr.indProcessed = :prmprocessed ");
			sbQuery.append(" and bcr.indRemanent = :remanent ");
			if(orginDestiny != null && orginDestiny > 0)
				sbQuery.append(" and bcr.indOriginTarget = :orgindest");

			query = em.createQuery(sbQuery.toString());
			query.setParameter("processed", CorporateProcessConstants.DEFINITIVE_PROCESSED);
			query.setParameter("modifyuser", loggerUser.getUserName());
			query.setParameter("modifydate", CommonsUtilities.currentDateTime());
			query.setParameter("modifyip", loggerUser.getIpAddress());
			query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
			query.setParameter("corpprocessid", corpProcessId);
			if(participant != null && participant > 0){
				query.setParameter("participant", participant);
			}
			query.setParameter("prmprocessed", notProcessed);
			query.setParameter("remanent", remanent);
			if(orginDestiny != null && orginDestiny > 0)
				query.setParameter("orgindest", orginDestiny);

			query.executeUpdate();
		}
		if(reportFlag){
			sbQuery = new StringBuilder();
			sbQuery.append("update ReportCorporativeResult rcr set rcr.indProcessed = :processed , ");
			sbQuery.append(" rcr.lastModifyUser = :modifyuser , rcr.lastModifyDate = :modifydate ,");
			sbQuery.append(" rcr.lastModifyIp = :modifyip , rcr.lastModifyApp = :modifyapp ");
			sbQuery.append(" where rcr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
			if(participant != null && participant > 0){
				sbQuery.append(" and rcr.participant.idParticipantPk = :participant ");
			}		
			sbQuery.append(" and rcr.indProcessed = :prmprocessed ");
			sbQuery.append(" and rcr.indRemanent = :remanent ");
			if(orginDestiny != null && orginDestiny > 0)
				sbQuery.append(" and rcr.indOriginTarget = :orgindest");

			query = em.createQuery(sbQuery.toString());
			query.setParameter("processed", CorporateProcessConstants.DEFINITIVE_PROCESSED);
			query.setParameter("modifyuser", loggerUser.getUserName());
			query.setParameter("modifydate", CommonsUtilities.currentDateTime());
			query.setParameter("modifyip", loggerUser.getIpAddress());
			query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
			query.setParameter("corpprocessid", corpProcessId);
			if(participant != null && participant > 0){
				query.setParameter("participant", participant);
			}
			query.setParameter("prmprocessed", notProcessed);
			query.setParameter("remanent", remanent);
			if(orginDestiny != null && orginDestiny > 0)
				query.setParameter("orgindest", orginDestiny);

			query.executeUpdate();
		}

		return true;
	}
    
    /**
     * Gets the total balance for corporate process.
     *
     * @param corpProcessId the corp process id
     * @param remanent the remanent
     * @param processed the processed
     * @param orginDestiny the orgin destiny
     * @param security the security
     * @return the total balance for corporate process
     */
    public Object getTotalBalanceForCorporateProcess(Long corpProcessId, Integer remanent, Integer processed, Integer orginDestiny, String security){
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select sum(cpr.totalBalance) from CorporativeProcessResult cpr ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpoperid");
		sbQuery.append(" and cpr.indRemanent = :remanent");
		sbQuery.append(" and cpr.indProcessed = :processed");
		sbQuery.append(" and cpr.indOriginTarget = :orgindest");
		if(security != null && security.length() > 0){
			sbQuery.append(" and cpr.security.idSecurityCodePk = :security");
			parameters.put("security", security);
		}
		parameters.put("corpoperid", corpProcessId);
		parameters.put("remanent", remanent);
		parameters.put("processed", processed);
		parameters.put("orgindest", orginDestiny);

		if (findObjectByQueryString(sbQuery.toString(), parameters) == null)
			return 0;
		else return findObjectByQueryString(sbQuery.toString(), parameters);
		
	}
    /**
     * Gets the block corp process results for unblock definitive.
     *
     * @param corpProcessId the corp process id
     * @param participant the participant
     * @param security the security
     * @param remanent the remanent
     * @param processed the processed
     * @param originTarget the origin target
     * @return the block corp process results for unblock definitive
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBlockCorpProcessResultsForUnblockDefinitive(Long corpProcessId, Long participant, 
			String security, Integer remanent, Integer processed, Integer originTarget)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select bcr.blockType, bcr.holder.idHolderPk, bcr.blockedBalance, ");
		sbQuery.append(" bcr.blockOperation.idBlockOperationPk, bo.documentNumber, bcr.holderAccount.idHolderAccountPk, ");
		sbQuery.append(" bcr.participant.idParticipantPk, bcr.security.idSecurityCodePk, bo.blockLevel ");
		sbQuery.append(" from BlockCorporativeResult bcr, BlockOperation bo ");
		sbQuery.append(" where bcr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
		sbQuery.append(" and bcr.blockOperation.idBlockOperationPk = bo.idBlockOperationPk ");
		sbQuery.append(" and bcr.participant.idParticipantPk = :participant ");		
		sbQuery.append(" and bcr.security.idSecurityCodePk = :security ");
		sbQuery.append(" and bcr.indRemanent = :remanent ");
		sbQuery.append(" and bcr.indProcessed = :processed ");
		if(originTarget != null && originTarget > 0){
			sbQuery.append(" and bcr.indOriginDestiny = :origintarget");
			parameters.put("origintarget", originTarget);
		}

		sbQuery.append(" order by bcr.holderAccount.idHolderAccountPk, bcr.blockType ");

		parameters.put("corpprocessid", corpProcessId);
		parameters.put("participant", participant);
		parameters.put("security", security);
		parameters.put("remanent", remanent);
		parameters.put("processed", processed);


		return findListByQueryString(sbQuery.toString(), parameters);
	}

    /**
     * Find retirement operation.
     *
     * @param idCorporativeOperationPk the id corporative operation pk
     * @return the retirement operation
     * @throws ServiceException the service exception
     */
    public RetirementOperation findRetirementOperation(Long idCorporativeOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT co.retirementOperation");
			sbQuery.append("	FROM CorporativeOperation co");
			sbQuery.append("	INNER JOIN FETCH co.retirementOperation.security");
			sbQuery.append("	LEFT  JOIN FETCH co.retirementOperation.targetSecurity");
			sbQuery.append("	INNER JOIN FETCH co.retirementOperation.retirementDetails rds");		
			sbQuery.append("	INNER JOIN FETCH rds.participant");
			sbQuery.append("	INNER JOIN FETCH rds.holderAccount ha");
			sbQuery.append("	WHERE co.idCorporativeOperationPk = :idCorporativeOperationPk");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idCorporativeOperationPk", idCorporativeOperationPk);
			return (RetirementOperation) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}		
	}
    
    /**
     * Find tax holder rate.
     *
     * @param corporativeEventType the corporative event type
     * @return the list
     */
    public List<TaxHolderRate> findTaxHolderRate(Integer corporativeEventType) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT thr");
		sbQuery.append("	FROM TaxHolderRate thr");
		sbQuery.append("	WHERE thr.corporateEventType = :corporativeEventType");			
		TypedQuery<TaxHolderRate> query = em.createQuery(sbQuery.toString(),TaxHolderRate.class);
		query.setParameter("corporativeEventType", corporativeEventType);
		return query.getResultList();
	}
    
    /**
     * Find holder info.
     *
     * @param idHolderAccountPk the id holder account pk
     * @return the string
     */
    public String findHolderInfo(Long idHolderAccountPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hac.holder.holderType,");
		sbQuery.append("			hac.holder.nationality");
		sbQuery.append("	FROM HolderAccountDetail hac");
		sbQuery.append("	WHERE hac.indRepresentative = :indYes");
		sbQuery.append("	AND hac.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("indYes", BooleanType.YES.getCode());
		List<Object[]> lstResult = (List<Object[]>) query.getResultList();
		return (lstResult.get(0)[0]).toString() + GeneralConstants.DASH + (lstResult.get(0)[1]).toString();
	}
    
    /**
     * This method is used to get the id of corporative operation whether if any remnent process is exists to execute or not.
     *
     * @param recordDate the record date
     * @param cutoffDate the cutoff date
     * @param security the security
     * @return the remanent process
     * @throws ServiceException the service exception
     */
    public Long getRemanentProcess(Date recordDate, Date cutoffDate, String security)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
		sbQuery.append(" where trunc(cp.registryDate) = trunc(:creationdate)");
		sbQuery.append(" and trunc(cp.cutoffDate) = trunc(:cutoffdate)");
		sbQuery.append(" and cp.securities.idSecurityCodePk = :securities");
		sbQuery.append(" and cp.state = :state");
		sbQuery.append(" and cp.indRound = :round");
		sbQuery.append(" and cp.corporativeEventType.corporativeEventType = :proctype");

		parameters.put("creationdate", recordDate);
		parameters.put("cutoffdate", cutoffDate);
		parameters.put("securities", security);
		parameters.put("state", CorporateProcessStateType.DEFINITIVE.getCode());
		parameters.put("round", 0);
		parameters.put("proctype", ImportanceEventType.ACTION_DIVIDENDS.getCode());
		List<Long> result = findListByQueryString(sbQuery.toString(), parameters);
		if(result != null && result.size() > 0){
			return result.get(0);
		}else{
			return null;
		}

	}
    
    /**
     * Gets the holder tax rates.
     *
     * @param corporativeType the corporative type
     * @return the holder tax rates
     * @throws ServiceException the service exception
     */
    public Map<String, BigDecimal> getHolderTaxRates(Integer corporativeType) throws ServiceException{
    	StringBuilder sbQuery=new StringBuilder();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append(" select thr.holderType, thr.nationality, thr.taxRate ");
		sbQuery.append(" from TaxHolderRate thr ");
		sbQuery.append(" where thr.corporateEventType = :eventtype");
		parameters.put("eventtype", corporativeType);	
		List<Object[]> result = findListByQueryString(sbQuery.toString(), parameters);	
		Map<String, BigDecimal> taxRates = new HashMap<String, BigDecimal>();
		if(result != null && !result.isEmpty()){
			for(Object[] data : result){
				taxRates.put(data[0].toString()+GeneralConstants.HYPHEN+data[1].toString(), (BigDecimal)data[2]);
			}
		}
		return taxRates;
	}
    
    /**
     * This method is used to get the holder id and sum of total balance from stock calculation balances group by holder for preliminary 
	 * remnant process by sending the stock process id, remnant value i.e.,1 and process indicator i.e.,0
     * @param corpProcessId the corp process id
     * @return the distinct holders for remant with holder type
     * @throws ServiceException the service exception
     */
    public List<Object[]> getDistinctHoldersForRemantWithHolderType(Long corpProcessId)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select substr(ha.ALTERNATE_CODE, instr(ha.ALTERNATE_CODE, '('), 1+instr(ha.ALTERNATE_CODE,')')-instr(ha.ALTERNATE_CODE, '(')), ");
		sbQuery.append("  sum(cpr.TOTAL_BALANCE) / count( cpr.ID_HOLDER_ACCOUNT_FK), ");
		sbQuery.append("  max(h.HOLDER_TYPE), max(case when h.NATIONALITY = :ntlocal then :nationaltp else :foreigntp end),count(h.ID_HOLDER_PK)  ");
		sbQuery.append(" , ha.ACCOUNT_TYPE ");
		sbQuery.append(" from CORPORATIVE_PROCESS_RESULT cpr,  HOLDER_ACCOUNT ha, ");
		sbQuery.append(" HOLDER_ACCOUNT_DETAIL had, HOLDER h ");
		sbQuery.append(" where cpr.ID_CORPORATIVE_OPERATION_FK = :corpprocessid");
		sbQuery.append(" and cpr.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" and ha.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append(" and had.ID_HOLDER_FK = h.ID_HOLDER_PK ");
		sbQuery.append(" and cpr.IND_REMANENT = :remanent ");
		sbQuery.append(" and cpr.IND_PROCESSED = :processed ");
		sbQuery.append(" group by substr(ha.ALTERNATE_CODE, instr(ha.ALTERNATE_CODE, '('), 1+instr(ha.ALTERNATE_CODE,')')-instr(ha.ALTERNATE_CODE, '(')), ha.ACCOUNT_TYPE");

		parameters.put("ntlocal", countryResidence);
		parameters.put("nationaltp", NationalityType.NATIONAL.getCode());
		parameters.put("foreigntp", NationalityType.FOREIGN.getCode());
		parameters.put("corpprocessid", corpProcessId);
		parameters.put("remanent", CorporateProcessConstants.IND_REMAINDER);
		parameters.put("processed", CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);

		return findByNativeQuery(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the distinct holders for contribution return with holder type.
     *
     * @param corpProcessId the corp process id
     * @param indOriginTarget the ind origin target
     * @return the distinct holders for contribution return with holder type
     * @throws ServiceException the service exception
     */
    public List<Object[]> getDistinctHoldersForContributionReturnWithHolderType(Long corpProcessId,Integer indOriginTarget)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select substr(ha.ALTERNATE_CODE, instr(ha.ALTERNATE_CODE, '('), 1+instr(ha.ALTERNATE_CODE,')')-instr(ha.ALTERNATE_CODE, '(')), ");
		sbQuery.append("  sum(cpr.TOTAL_BALANCE) / count( cpr.ID_HOLDER_ACCOUNT_FK), ");
		sbQuery.append("  max(h.HOLDER_TYPE), max(case when h.NATIONALITY = :ntlocal then :nationaltp else :foreigntp end),count(h.ID_HOLDER_PK)  ");
		sbQuery.append(" , ha.ACCOUNT_TYPE ");
		sbQuery.append(" from CORPORATIVE_PROCESS_RESULT cpr,  HOLDER_ACCOUNT ha, ");
		sbQuery.append(" HOLDER_ACCOUNT_DETAIL had, HOLDER h ");
		sbQuery.append(" where cpr.ID_CORPORATIVE_OPERATION_FK = :corpprocessid");
		sbQuery.append(" and cpr.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" and ha.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append(" and had.ID_HOLDER_FK = h.ID_HOLDER_PK ");
		sbQuery.append(" and cpr.IND_ORIGIN_TARGET = :indOriginDestiny");
		sbQuery.append(" and cpr.IND_PROCESSED = :processed ");
		sbQuery.append(" group by substr(ha.ALTERNATE_CODE, instr(ha.ALTERNATE_CODE, '('), 1+instr(ha.ALTERNATE_CODE,')')-instr(ha.ALTERNATE_CODE, '(')), ha.ACCOUNT_TYPE");

		parameters.put("ntlocal", countryResidence);
		parameters.put("nationaltp", NationalityType.NATIONAL.getCode());
		parameters.put("foreigntp", NationalityType.FOREIGN.getCode());
		parameters.put("indOriginDestiny", indOriginTarget);
		parameters.put("corpprocessid", corpProcessId);
		parameters.put("processed", CorporateProcessConstants.DEFINITIVE_PROCESSED);

		return findByNativeQuery(sbQuery.toString(), parameters);
	}
    /**
     * This method is used to get distinct the holders from holder account and their total balance for the give stock process.
     *
     * @param stockProcessId the stock process id
     * @return the distinct stock holders with holder type
     * @throws ServiceException the service exception
     */
    public List<Object[]> getDistinctStockHoldersWithHolderType(Long stockProcessId)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select substr(ha.ALTERNATE_CODE, instr(ha.ALTERNATE_CODE, '('), 1+instr(ha.ALTERNATE_CODE,')')-instr(ha.ALTERNATE_CODE, '(')), ");
		sbQuery.append("  sum(scb.TOTAL_BALANCE), ");
		sbQuery.append("  max(h.HOLDER_TYPE), max(case when h.NATIONALITY = :ntlocal then :nationaltp else :foreigntp end),count(h.ID_HOLDER_PK) ");
		sbQuery.append(" , ha.ACCOUNT_TYPE ");
		sbQuery.append(" from STOCK_CALCULATION_BALANCE scb,  HOLDER_ACCOUNT ha, ");
		sbQuery.append(" HOLDER_ACCOUNT_DETAIL had, HOLDER h ");
		sbQuery.append(" where scb.ID_STOCK_CALCULATION_FK = :stockprocessid");
		sbQuery.append(" and scb.ID_HOLDER_ACCOUNT_FK=ha.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" and ha.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK ");
		sbQuery.append(" and had.ID_HOLDER_FK = h.ID_HOLDER_PK ");
		sbQuery.append(" group by substr(ha.ALTERNATE_CODE, instr(ha.ALTERNATE_CODE, '('), 1+instr(ha.ALTERNATE_CODE,')')-instr(ha.ALTERNATE_CODE, '(')), ha.ACCOUNT_TYPE");
		parameters.put("ntlocal", countryResidence);
		parameters.put("nationaltp", NationalityType.NATIONAL.getCode());
		parameters.put("foreigntp", NationalityType.FOREIGN.getCode());
		parameters.put("stockprocessid", stockProcessId);
		return findByNativeQuery(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the holder accounts and all the balances from the Corporative Process Results by sending the corporative operation id, remnant, 
     * participant id,processed to execute the preliminary.
     *
     * @param corpProcessId the corp process id
     * @param holderAlternateCode the holder alternate code
     * @param remanent the remanent
     * @param indProcessed the ind processed
     * @return the corporate results for remnant
     * @throws ServiceException the service exception
     */
    public List<Object[]> getCorporateResultsForRemnant(Long corpProcessId, String holderAlternateCode, Integer remanent, Integer indProcessed)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cpr.holderAccount.idHolderAccountPk, cpr.availableBalance, ");
		sbQuery.append(" cpr.pawnBalance, cpr.banBalance, cpr.otherBlockBalance, cpr.reserveBalance, ");
		sbQuery.append(" cpr.oppositionBalance, cpr.marginBalance, cpr.reportingBalance, cpr.reportedBalance, cpr.totalBalance, " );
		sbQuery.append(" cpr.participant.idParticipantPk");
		sbQuery.append(" from CorporativeProcessResult cpr, HolderAccount ha ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
		sbQuery.append(" and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
		sbQuery.append(" and cpr.indRemanent = :remanent");
		sbQuery.append(" and cpr.indProcessed = :processed");
		sbQuery.append(" and ha.alternateCode like :alternatecode ");
		sbQuery.append(" order by cpr.availableBalance");

		parameters.put("corpprocessid", corpProcessId);
		parameters.put("remanent", remanent);
		parameters.put("processed", indProcessed);
		parameters.put("alternatecode", '%'+holderAlternateCode+'%');

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the corporate results for contribution result.
     *
     * @param corpProcessId the corp process id
     * @param holderAlternateCode the holder alternate code
     * @param indOriginTarget the ind origin target
     * @return the corporate results for contribution result
     * @throws ServiceException the service exception
     */
    public List<Object[]> getCorporateResultsForContributionResult(Long corpProcessId, String holderAlternateCode, Integer indOriginTarget)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cpr.holderAccount.idHolderAccountPk, cpr.availableBalance, ");
		sbQuery.append(" cpr.pawnBalance, cpr.banBalance, cpr.otherBlockBalance, cpr.reserveBalance, ");
		sbQuery.append(" cpr.oppositionBalance, cpr.marginBalance, cpr.reportingBalance, cpr.reportedBalance, cpr.totalBalance, " );
		sbQuery.append(" cpr.participant.idParticipantPk");
		sbQuery.append(" from CorporativeProcessResult cpr, HolderAccount ha ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
		sbQuery.append(" and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
		sbQuery.append(" and cpr.indOriginTarget = :indOriginTarget");
		sbQuery.append(" and cpr.indProcessed = :processed");
		sbQuery.append(" and ha.alternateCode like :alternatecode ");
		sbQuery.append(" order by cpr.availableBalance");

		parameters.put("corpprocessid", corpProcessId);
		parameters.put("indOriginTarget", indOriginTarget);
		parameters.put("processed", CorporateProcessConstants.DEFINITIVE_PROCESSED);
		parameters.put("alternatecode", '%'+holderAlternateCode+'%');

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    /**
     * This method is used to get the blockCorporative results details like block type, block balance etc., from the  BlockCorporativeResult table
	 * by sending the Stock Calculation Process Id, participant,holder account,remnant,processed id and Security
     * @param remnantId the remnant id
     * @param participant the participant
     * @param holderAccount the holder account
     * @param security the security
     * @param indRemanent the ind remanent
     * @param indProcessed the ind processed
     * @return the block corp process details
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBlockCorpProcessDetails(Long remnantId, Long participant, Long holderAccount, String security, Integer indRemanent, Integer indProcessed)throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select bcr.blockType, bcr.blockedBalance, bcr.blockOperation.idBlockOperationPk, ");
		sbQuery.append(" bcr.holder.idHolderPk , bcr.indCashDividend, bcr.indContributionReturn, bcr.indInterest, ");
		sbQuery.append(" bcr.indStockDividend, bcr.indSuscription, bcr.participant.idParticipantPk, bo.blockLevel, ");
		sbQuery.append(" bo.blockState, bcr.indAmortization, bcr.indRescue ");
		sbQuery.append(" from BlockCorporativeResult bcr, BlockOperation bo ");
		sbQuery.append(" where bcr.corporativeOperation.idCorporativeOperationPk = :remnantid");
		sbQuery.append(" and bcr.participant.idParticipantPk = :participant ");
		sbQuery.append(" and bcr.holderAccount.idHolderAccountPk = :holderaccount ");
		sbQuery.append(" and bcr.security.idSecurityCodePk = :security ");
		sbQuery.append(" and bcr.blockOperation.idBlockOperationPk = bo.idBlockOperationPk ");
		sbQuery.append(" and bcr.indRemanent = :remanent ");
		sbQuery.append(" and bcr.indProcessed = :processed ");
		sbQuery.append(" order by bcr.blockType, bcr.blockedBalance ");
		parameters.put("remnantid", remnantId);
		parameters.put("participant", participant);
		parameters.put("holderaccount", holderAccount);
		parameters.put("security", security);
		parameters.put("remanent", indRemanent);
		parameters.put("processed", indProcessed);
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the block corp for contribution return process details.
     *
     * @param opeId the ope id
     * @param participant the participant
     * @param holderAccount the holder account
     * @param security the security
     * @param indProcessed the ind processed
     * @param indOriginTarget the ind origin target
     * @return the block corp for contribution return process details
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBlockCorpForContributionReturnProcessDetails(Long opeId, Long participant, Long holderAccount, String security, Integer indProcessed,Integer indOriginTarget)throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select bcr.blockType, bcr.blockedBalance, bcr.blockOperation.idBlockOperationPk, ");
		sbQuery.append(" bcr.holder.idHolderPk , bcr.indCashDividend, bcr.indContributionReturn, bcr.indInterest, ");
		sbQuery.append(" bcr.indStockDividend, bcr.indSuscription, bcr.participant.idParticipantPk, bo.blockLevel, ");
		sbQuery.append(" bo.blockState, bcr.indAmortization, bcr.indRescue ");
		sbQuery.append(" from BlockCorporativeResult bcr, BlockOperation bo ");
		sbQuery.append(" where bcr.corporativeOperation.idCorporativeOperationPk = :opeId");
		sbQuery.append(" and bcr.participant.idParticipantPk = :participant ");
		sbQuery.append(" and bcr.holderAccount.idHolderAccountPk = :holderaccount ");
		sbQuery.append(" and bcr.security.idSecurityCodePk = :security ");
		sbQuery.append(" and bcr.blockOperation.idBlockOperationPk = bo.idBlockOperationPk ");
		sbQuery.append(" and bcr.indOriginDestiny = :indOriginDestiny ");
		sbQuery.append(" and bcr.indProcessed = :processed ");
		sbQuery.append(" order by bcr.blockType, bcr.blockedBalance ");
		parameters.put("opeId", opeId);
		parameters.put("participant", participant);
		parameters.put("holderaccount", holderAccount);
		parameters.put("security", security);
		parameters.put("indOriginDestiny", indOriginTarget);
		parameters.put("processed", indProcessed);
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    /**
     * This method is used to get the ReportCorporativeResult table by sending the .
     *
     * @param corpOperId the corp oper id
     * @param participant the participant
     * @param holderAccount the holder account
     * @param security the security
     * @param indRemanent the ind remanent
     * @param indProcessed the ind processed
     * @return the corp report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getCorpReportOperations(Long corpOperId, Long participant, Long holderAccount, String security, Integer indRemanent, Integer indProcessed)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rcr.guaranteeType, rcr.operationRole, rcr.guaranteeBalance, ");
		sbQuery.append(" rcr.holderAccountOperation.idHolderAccountOperationPk , rcr.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append(" from ReportCorporativeResult rcr ");
		sbQuery.append(" where rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId");
		sbQuery.append(" and rcr.participant.idParticipantPk = :participant ");
		sbQuery.append(" and rcr.holderAccount.idHolderAccountPk = :holderaccount ");
		sbQuery.append(" and rcr.security.idSecurityCodePk = :security ");
		sbQuery.append(" and rcr.indRemanent = :remanent ");
		sbQuery.append(" and rcr.indProcessed = :processed ");
		sbQuery.append(" order by rcr.guaranteeType, rcr.operationRole, rcr.guaranteeBalance ");

		parameters.put("corpOperId", corpOperId);
		parameters.put("participant", participant);
		parameters.put("holderaccount", holderAccount);
		parameters.put("security", security);
		parameters.put("remanent", indRemanent);
		parameters.put("processed", indProcessed);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the corp report for contribution return operations.
     *
     * @param corpOperId the corp oper id
     * @param participant the participant
     * @param holderAccount the holder account
     * @param security the security
     * @param indProcessed the ind processed
     * @param indOriginDestiny the ind origin destiny
     * @return the corp report for contribution return operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getCorpReportForContributionReturnOperations(Long corpOperId, Long participant, Long holderAccount, String security, Integer indProcessed,Integer indOriginDestiny)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rcr.guaranteeType, rcr.operationRole, rcr.guaranteeBalance, ");
		sbQuery.append(" rcr.holderAccountOperation.idHolderAccountOperationPk , rcr.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append(" from ReportCorporativeResult rcr ");
		sbQuery.append(" where rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId");
		sbQuery.append(" and rcr.participant.idParticipantPk = :participant ");
		sbQuery.append(" and rcr.holderAccount.idHolderAccountPk = :holderaccount ");
		sbQuery.append(" and rcr.security.idSecurityCodePk = :security ");
		sbQuery.append(" and rcr.indOriginDestiny = :indOriginDestiny ");
		sbQuery.append(" and rcr.indProcessed = :processed ");
		sbQuery.append(" order by rcr.guaranteeType, rcr.operationRole, rcr.guaranteeBalance ");

		parameters.put("corpOperId", corpOperId);
		parameters.put("participant", participant);
		parameters.put("holderaccount", holderAccount);
		parameters.put("security", security);
		parameters.put("indOriginDestiny", indOriginDestiny);
		parameters.put("processed", indProcessed);

		return findListByQueryString(sbQuery.toString(), parameters);
	}

    /**
     * This method is used to get the results mechanism operation and guarantee balance values from the ReportCorporativeResult table data by sending the
     * seller role,buyer role,remnant,destiny origin.
     *
     * @param remnantOperationId the remnant operation id
     * @param corpOperId the corp oper id
     * @param guarantyType1 the guaranty type1
     * @param guarantyType2 the guaranty type2
     * @param sellerRole the seller role
     * @param buyerRole the buyer role
     * @param indRemnant the ind remnant
     * @param indProcessed the ind processed
     * @param origindest1 the origindest1
     * @param origindest2 the origindest2
     * @param origindest3 the origindest3
     * @param origindest4 the origindest4
     * @return the remnant seller buyyer sum report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getRemnantSellerBuyyerSumReportOperations(Long remnantOperationId, Long corpOperId, Integer guarantyType1, Integer guarantyType2,
			Integer sellerRole, Integer buyerRole, Integer indRemnant, Integer indProcessed, Integer origindest1, Integer origindest2, 
			Integer origindest3, Integer origindest4)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rcr.mechanismOperation.idMechanismOperationPk, sum(rem.guaranteeBalance), ");
		sbQuery.append(" sum(rcr.guaranteeBalance) ");
		sbQuery.append(" from ReportCorporativeResult rem, ReportCorporativeResult rcr ");
		sbQuery.append(" where rem.corporativeOperation.idCorporativeOperationPk = :remnantId");
		sbQuery.append(" and rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId");
		sbQuery.append(" and rcr.participant.idParticipantPk = rem.participant.idParticipantPk ");
		sbQuery.append(" and rcr.holderAccount.idHolderAccountPk = rem.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and rcr.security.idSecurityCodePk = rem.security.idSecurityCodePk ");
		sbQuery.append(" and rcr.mechanismOperation.idMechanismOperationPk = rem.mechanismOperation.idMechanismOperationPk ");	
		sbQuery.append(" and (rem.guaranteeType = :guaranty1 or rem.guaranteeType = :guaranty2) ");
		sbQuery.append(" and (rcr.guaranteeType = :guaranty1 or rcr.guaranteeType = :guaranty2) ");
		sbQuery.append(" and rem.operationRole = :buyerRole and rcr.operationRole = :sellerRole ");
		sbQuery.append(" and rem.indRemanent = :remanent ");
		sbQuery.append(" and ((rem.indOriginDestiny = :origindestiny1 and rcr.indOriginDestiny = :origindestiny2) ");
		sbQuery.append(" or (rem.indOriginDestiny = :origindestiny3 and rcr.indOriginDestiny = :origindestiny4)) ");
		sbQuery.append(" and rem.indProcessed = :processed ");
		sbQuery.append(" group by rcr.mechanismOperation.idMechanismOperationPk ");

		parameters.put("remnantId", remnantOperationId);
		parameters.put("corpOperId", corpOperId);
		parameters.put("guaranty1", guarantyType1);
		parameters.put("guaranty2", guarantyType2);
		parameters.put("sellerRole", sellerRole);
		parameters.put("buyerRole", buyerRole);
		parameters.put("remanent", indRemnant);
		parameters.put("origindestiny1", origindest1);
		parameters.put("origindestiny2", origindest2);
		parameters.put("origindestiny3", origindest3);
		parameters.put("origindestiny4", origindest4);
		parameters.put("processed", indProcessed);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the remnant seller report operations from the report corporative result table.
     *
     * @param remnantCorpId : Corporative Operation ID
     * @param guarantyType1 : Guarantee type is principle
     * @param guarantyType2 : Guarantee type is stock dividend principle
     * @param sellerRole : role is seller
     * @param indRemanent the ind remanent
     * @param indProcessed the ind processed
     * @return the remnant seller report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getRemnantBuyerReportOperations(Long remnantCorpId, Integer guarantyType1, Integer guarantyType2,
			Integer sellerRole, Integer indRemanent, Integer indProcessed)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rcr.guaranteeType, rcr.guaranteeBalance, rcr.participant.idParticipantPk, rcr.holderAccount.idHolderAccountPk, ");
		sbQuery.append(" rcr.security.idSecurityCodePk, rcr.holderAccountOperation.idHolderAccountOperationPk , rcr.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append(" from ReportCorporativeResult rcr ");
		sbQuery.append(" where rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId");
		sbQuery.append(" and (rcr.guaranteeType = :guaranty1 or rcr.guaranteeType = :guaranty2) ");
		sbQuery.append(" and  rcr.operationRole = :role ");
		sbQuery.append(" and rcr.indRemanent = :remanent ");
		sbQuery.append(" and rcr.indProcessed = :processed ");
		sbQuery.append(" order by rcr.mechanismOperation.idMechanismOperationPk, rcr.guaranteeBalance ");

		parameters.put("corpOperId", remnantCorpId);
		parameters.put("guaranty1", guarantyType1);
		parameters.put("guaranty2", guarantyType2);
		parameters.put("role", sellerRole);
		parameters.put("remanent", indRemanent);
		parameters.put("processed", indProcessed);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the contribution return buyer report operations.
     *
     * @param remnantCorpId the remnant corp id
     * @param guarantyType1 the guaranty type1
     * @param guarantyType2 the guaranty type2
     * @param sellerRole the seller role
     * @param indProcessed the ind processed
     * @param indOriginDestiny the ind origin destiny
     * @return the contribution return buyer report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getContributionReturnBuyerReportOperations(Long remnantCorpId, Integer guarantyType1, Integer guarantyType2,
			Integer sellerRole, Integer indProcessed, Integer indOriginDestiny)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rcr.guaranteeType, rcr.guaranteeBalance, rcr.participant.idParticipantPk, rcr.holderAccount.idHolderAccountPk, ");
		sbQuery.append(" rcr.security.idSecurityCodePk, rcr.holderAccountOperation.idHolderAccountOperationPk , rcr.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append(" from ReportCorporativeResult rcr ");
		sbQuery.append(" where rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId");
		sbQuery.append(" and (rcr.guaranteeType = :guaranty1 or rcr.guaranteeType = :guaranty2) ");
		sbQuery.append(" and  rcr.operationRole = :role ");
		sbQuery.append(" and rcr.indOriginDestiny = :indOriginDestiny ");
		sbQuery.append(" and rcr.indProcessed = :processed ");
		sbQuery.append(" order by rcr.mechanismOperation.idMechanismOperationPk, rcr.guaranteeBalance ");

		parameters.put("corpOperId", remnantCorpId);
		parameters.put("guaranty1", guarantyType1);
		parameters.put("guaranty2", guarantyType2);
		parameters.put("role", sellerRole);
		parameters.put("indOriginDestiny",indOriginDestiny);
		parameters.put("processed", indProcessed);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    /**
     * Gets the total balance.
     *
     * @param corpProcessId the corp process id
     * @return the total balance
     */
    public BigDecimal getTotalBalance(Long corpProcessId){
    	StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT SUM(nvl(CPR.totalBalance,0))");
		sbQuery.append("   FROM CorporativeProcessResult CPR");
		sbQuery.append("  WHERE CPR.corporativeOperation.idCorporativeOperationPk =:corpProcessId");
		sbQuery.append("    AND CPR.indOriginTarget = "+CorporateProcessConstants.BALANCE_DESTINY);
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("corpProcessId", corpProcessId);	
		return (BigDecimal) query.getSingleResult();
	}
    
    /**
     * Validate exist previous adjustments.
     *
     * @param corporativeID the corporative id
     * @return the list
     */
    public List<Long> validateExistPreviousAdjustments(Long corporativeID){
		List<Long> lstHolderAccount = new ArrayList<Long>();
		String strQuery = 
		"select "
		+ "haa.holderAccount.idHolderAccountPk "
		+ "from HolderAccountAdjusment haa "
		+ "where "
		+ "haa.corporativeOperation.idCorporativeOperationPk = :corporativeID "
		+ "and haa.indCollectCommissiones = :indYes ";
		Query query = em.createQuery(strQuery);
		query.setParameter("corporativeID",corporativeID);
		query.setParameter("indYes",BooleanType.YES.getCode());
		lstHolderAccount = query.getResultList();
		return lstHolderAccount;
	}
    
    /**
     * Gets the custody amount.
     *
     * @param corpProcessId the corp process id
     * @return the custody amount
     */
    public BigDecimal getCustodyAmount(Long corpProcessId){
    	StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT SUM(nvl(CPR.custodyAmount,0))");
		sbQuery.append("   FROM CorporativeProcessResult CPR");
		sbQuery.append("  WHERE CPR.corporativeOperation.idCorporativeOperationPk =:corpProcessId");
		sbQuery.append("    AND CPR.indOriginTarget = "+CorporateProcessConstants.BALANCE_DESTINY);
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("corpProcessId", corpProcessId);	
		return (BigDecimal) query.getSingleResult();
	}
    
    /**
     * Gets the tax amount.
     *
     * @param corpProcessId the corp process id
     * @return the tax amount
     */
    public BigDecimal getTaxAmount(Long corpProcessId){
    	StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT SUM(nvl(CPR.taxAmount,0))");
		sbQuery.append("   FROM CorporativeProcessResult CPR");
		sbQuery.append("  WHERE CPR.corporativeOperation.idCorporativeOperationPk =:corpProcessId");
		sbQuery.append("    AND CPR.indOriginTarget = "+CorporateProcessConstants.BALANCE_DESTINY);
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("corpProcessId", corpProcessId);	
		return (BigDecimal) query.getSingleResult();
	}
    /**
     * Gets the exchange amount.
     *
     * @param corpProcessId the corp process id
     * @return the exchange amount
     */
    public BigDecimal getExchangeAmount(Long corpProcessId){
    	StringBuilder sbQuery=new StringBuilder();
		sbQuery.append(" SELECT SUM(nvl(CPR.exchangeAmount,0))");
		sbQuery.append("   FROM CorporativeProcessResult CPR");
		sbQuery.append("  WHERE CPR.corporativeOperation.idCorporativeOperationPk =:corpProcessId");
		sbQuery.append("    AND CPR.indOriginTarget = "+CorporateProcessConstants.EXCHANGE_DESTINY);
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("corpProcessId", corpProcessId);	
		return (BigDecimal) query.getSingleResult();
	}
    /**
     * Update corporative custody amounts.
     *
     * @param corpOperation the corp operation
     */
    public void updateCorporativeCustodyAmounts(CorporativeOperation corpOperation){
		//Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("UPDATE CorporativeOperation CO");
		sbQuery.append("   SET CO.custodyAmount = :custodyAmount,");
		sbQuery.append("   CO.commissionAmount = :commissionAmount,");
		sbQuery.append("   CO.paymentAmount = :paymentAmount,");
		sbQuery.append("   CO.taxAmount = :taxAmount,");
		sbQuery.append("   CO.issuerConfirmedAmount = :issuerConfirmedAmount,");
		if(corpOperation.getIdStockCalculation()!=null){
			sbQuery.append("   CO.idStockCalculation = :idStockCalculation,");
		}
		sbQuery.append("   CO.exchangeAmount = :exchangeAmount");
		sbQuery.append(" WHERE  CO.idCorporativeOperationPk =:idCorporativeOperationPk");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("custodyAmount", corpOperation.getCustodyAmount());
		query.setParameter("paymentAmount", corpOperation.getPaymentAmount());
		query.setParameter("commissionAmount", corpOperation.getCommissionAmount());
		query.setParameter("taxAmount", corpOperation.getTaxAmount());
		query.setParameter("issuerConfirmedAmount", corpOperation.getIssuerConfirmedAmount());
		
		if(corpOperation.getIdStockCalculation()!=null){
			query.setParameter("idStockCalculation", corpOperation.getIdStockCalculation());
		}
		query.setParameter("exchangeAmount", corpOperation.getExchangeAmount());
		query.setParameter("idCorporativeOperationPk", corpOperation.getIdCorporativeOperationPk());
		query.executeUpdate();
		em.flush();
	}
    
    /**
     * update Corporative Situation Depositary Not Benefit.
     *
     * @param corpOperation corpOperation
     */
    public void updateCorporativeSituationDepositaryNotBenefit(CorporativeOperation corpOperation){
    	
    }
    /**
     * Gets the corporative results.
     *
     * @param corporativeID the corporative id
     * @return the corporative results
     * @throws ServiceException the service exception
     */
    public List<CorporativeProcessResult> getCorporativeResults(Long corporativeID) throws ServiceException{
		List<CorporativeProcessResult> lstCorporativeResult = new ArrayList<CorporativeProcessResult>();
			String queryStr = 
					"select cpr " +
							"from CorporativeProcessResult cpr inner join fetch cpr.holderAccount " +
							"where cpr.corporativeOperation.idCorporativeOperationPk = :idCorporative "
							+ "and cpr.indOriginTarget in ( :indTarget , :indExchange )";
			Query query = em.createQuery(queryStr,CorporativeProcessResult.class);
			query.setParameter("idCorporative",corporativeID);
			query.setParameter("indTarget",CorporateProcessConstants.BALANCE_DESTINY);
			query.setParameter("indExchange",CorporateProcessConstants.EXCHANGE_DESTINY);
			lstCorporativeResult = query.getResultList();
		return lstCorporativeResult;
	}
    
    /**
     * Gets the corporative adjustments.
     *
     * @param corporativeID the corporative id
     * @return the corporative adjustments
     * @throws ServiceException the service exception
     */
    public List<HolderAccountAdjusment> getCorporativeAdjustments(Long corporativeID) throws ServiceException{
		List<HolderAccountAdjusment> adjustments = new ArrayList<HolderAccountAdjusment>();
			String strQuery = 
					"select haa "
							+ "from HolderAccountAdjusment haa inner join fetch haa.holderAccount ha "
							+ "where haa.corporativeOperation.idCorporativeOperationPk = :idCorporative";
			Query query = em.createQuery(strQuery,HolderAccountAdjusment.class);
			query.setParameter("idCorporative",corporativeID);
			adjustments = query.getResultList();
		return adjustments;
	}
    
    /**
     * Gets the exclusion rates.
     *
     * @param serviceRatePK the service rate pk
     * @param security the security
     * @return the exclusion rates
     */
    public List<RateExclusion> getExclusionRates(Long serviceRatePK,String security){
		List<RateExclusion> lstExclusions = new ArrayList<RateExclusion>();
		String strQuery = 
				"select re from RateExclusion re "
				+ "left join fetch re.securities "
				+ "left join fetch re.participant "
				+ "left join fetch re.issuer "
				+ "left join fetch re.holder "
				+ "where re.serviceRate.idServiceRatePk = :rateID "
				+ "and re.exclusionState = :rateState "
				+ "and (re.securities.idSecurityCodePk = :security or re.securities.idSecurityCodePk is null) ";

		Query query = em.createQuery(strQuery);
		query.setParameter("rateID",serviceRatePK);
		query.setParameter("rateState",RateExceptionStatus.REGISTER.getCode());
		query.setParameter("security",security);
		lstExclusions = query.getResultList();
		return lstExclusions;
	}

    /**
     * Gets the data exclussion adjustments.
     *
     * @param corporativeID the corporative id
     * @param lstParticipantExclussion the lst participant exclussion
     * @param lstHolderAccounts the lst holder accounts
     * @return the data exclussion adjustments
     */
    public List<Object[]> getDataExclussionAdjustments(Long corporativeID, List<Long> lstParticipantExclussion, List<Long> lstHolderAccounts){
		List<Object[]> lstReturn = new ArrayList<Object[]>();
		String strQuery = 
				"select "
				+ "cpr.holderAccount.idHolderAccountPk,"			//[0]
				+ "nvl(cpr.totalBalance,0),"						//[1]
				+ "nvl(cpr.custodyAmount,0) "						//[2]
				+ "from CorporativeProcessResult cpr "
				+ "where cpr.corporativeOperation.idCorporativeOperationPk = :corporativeID ";
		if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantExclussion)){
			strQuery += "and cpr.participant.idParticipantPk in (:lstParticipantExclussion) ";
		}
		strQuery += "and cpr.indOriginTarget = :destiny ";
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccounts)){
			strQuery += "and cpr.holderAccount.idHolderAccountPk not in (:lstHolderAccounts) ";
		}
		Query query = em.createQuery(strQuery);
		query.setParameter("corporativeID",corporativeID);
		if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantExclussion)){
			query.setParameter("lstParticipantExclussion",lstParticipantExclussion);
		}
		query.setParameter("destiny",CorporateProcessConstants.BALANCE_DESTINY);
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccounts)){
			query.setParameter("lstHolderAccounts",lstHolderAccounts);
		}
		lstReturn = query.getResultList();
		return lstReturn;
	}
    
    /**
     * Gets the issuer by corporative.
     *
     * @param corporativeID the corporative id
     * @return the issuer by corporative
     */
    public String getIssuerByCorporative(Long corporativeID){
		String issuer = null;
		String strQuery = 
				"select co.issuer.idIssuerPk "
				+ "from CorporativeOperation co "
				+ "where co.idCorporativeOperationPk = :corporativeID ";
		Query query = em.createQuery(strQuery);
		query.setParameter("corporativeID",corporativeID);
		issuer = (String) query.getSingleResult();
		return issuer;
	}
    
    /**
     * Gets the holder account by corporative.
     *
     * @param corporativeID the corporative id
     * @return the holder account by corporative
     */
    public List<Object[]> getHolderAccountByCorporative(Long corporativeID){
		List<Object[]> lstCorporateResults = new ArrayList<Object[]>();
		String strQuery = 
				"select "
				+ "cpr.holderAccount.idHolderAccountPk,"			//[0]
				+ "cpr.custodyAmount "								//[1]
				+ "from CorporativeProcessResult cpr "
				+ "where cpr.corporativeOperation.idCorporativeOperationPk = :corporativeID "
				+ "and cpr.indOriginTarget = :destiny";
		Query query = em.createQuery(strQuery);
		query.setParameter("corporativeID",corporativeID);
		query.setParameter("destiny",CorporateProcessConstants.BALANCE_DESTINY);
		lstCorporateResults = query.getResultList();
		return lstCorporateResults;
	}
    
    /**
     * Gets the tax process.
     *
     * @param holderAccount the holder account
     * @return the tax process
     * @throws ServiceException the service exception
     */
    public List<Object[]> getTaxProcess(Long holderAccount) throws ServiceException{
		List<Object[]> lstTaxes = new ArrayList<Object[]>();
		String strQuery = 
							"select h.holderType,h.nationality "
							+ "from HolderAccountDetail had, Holder h "
							+ "where "
							+ "had.holder.idHolderPk = h.idHolderPk "
							+ "and had.indRepresentative = :indRepresentante "
							+ "and had.stateAccountDetail = :accountStateDetail "
							+ "and had.holderAccount.idHolderAccountPk = :holderAccount";
		Query query = em.createQuery(strQuery);
		query.setParameter("indRepresentante",BooleanType.YES.getCode());
		query.setParameter("accountStateDetail",HolderAccountStateDetailType.REGISTERED.getCode());
		query.setParameter("holderAccount", holderAccount);
		lstTaxes = query.getResultList();
		return lstTaxes;
	}
    
    /**
     * Find billing service.
     *
     * @param billingServiceTo the billing service to
     * @return the list
     */
    public List<BillingService> findBillingService(BillingServiceTo billingServiceTo){
		List<BillingService> listBillingService =null;
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo)){
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select distinct billingService From BillingService billingService left outer join fetch billingService.serviceRates srs ");
			sbQuery.append(" Where  billingService.baseCollection = :baseCollection ");
			sbQuery.append(" and billingService.serviceState="+Integer.valueOf(1546));
			if(Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected()) && 
					Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected().getLastEffectiveDate())){
				sbQuery.append(" and  trunc(:effectiveDate)  between trunc(srs.initialEffectiveDate) and trunc(srs.endEffectiveDate)" );
				sbQuery.append(" and ( srs.rateState = :stateExpired      or  srs.rateState = :stateActived )  ");
			}else {
				sbQuery.append(" and srs.rateState="+RateStateType.ACTIVED.getCode());
			}
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("baseCollection", billingServiceTo.getBaseCollection()); 
			if (Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected()) && 
					Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected().getLastEffectiveDate())  ){
				query.setParameter("effectiveDate", billingServiceTo.getServiceRateToSelected().getLastEffectiveDate(),TemporalType.DATE);  
				query.setParameter("stateExpired", RateStateType.EXPIRED.getCode() ); 		
				query.setParameter("stateActived", RateStateType.ACTIVED.getCode() ); 
			}
			try{
				listBillingService=query.getResultList();
			}catch(NoResultException nrex){
				nrex.printStackTrace();
			}
		}
		return listBillingService;
	}
    
    /**
     * Gets the target security.
     *
     * @param corporateId the corporate id
     * @return the target security
     * @throws ServiceException the service exception
     */
    public Security getTargetSecurity(Long corporateId) throws ServiceException{
    	StringBuilder sbQuery=new StringBuilder();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append(" select cp.targetSecurity ");
		sbQuery.append(" from CorporativeOperation cp ");
		sbQuery.append(" where cp.idCorporativeOperationPk = :idCorporateFk");
		parameters.put("idCorporateFk", corporateId);	
		return (Security)findObjectByQueryString(sbQuery.toString(), parameters);		
	}
    
    /**
     * This method is used to get the alternate code and total balances from the HolderAccountBalance and HolderAccount tables.
     * @param security the security
     * @return the distinct balance holders for process
     * @throws ServiceException the service exception
     */
    public List<Object[]> getDistinctBalanceHoldersForProcess(String security)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,1)-locate('(',ha.alternateCode,1))), sum(hab.totalBalance) ");
		sbQuery.append(" from HolderAccountBalance hab, HolderAccount ha ");
		sbQuery.append(" where hab.id.idSecurityCodePk = :isincode");
		sbQuery.append(" and hab.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
		sbQuery.append(" and (hab.totalBalance > 0 or hab.reportedBalance > 0 or hab.saleBalance > 0)");
		sbQuery.append(" group by substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,1)-locate('(',ha.alternateCode,1))) ");
		parameters.put("isincode", security);
		return findListByQueryString(sbQuery.toString(), parameters);
	}	
    
    /**
     * This method is used to get the balances of holder from holder account balances.
     *
     * @param security the security
     * @param holderAlternateCode the holder alternate code
     * @return the balances for holder
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBalancesForHolder(String security, String holderAlternateCode)throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select hab.id.idHolderAccountPk, hab.availableBalance, hab.pawnBalance,");
		sbQuery.append(" hab.banBalance, hab.otherBlockBalance, hab.reserveBalance, hab.oppositionBalance, ");
		sbQuery.append(" hab.marginBalance, hab.reportingBalance, hab.reportedBalance, hab.totalBalance, " );
		sbQuery.append(" hab.id.idParticipantPk ");
		sbQuery.append(" from HolderAccountBalance hab, HolderAccount ha  ");
		sbQuery.append(" where hab.id.idSecurityCodePk = :isincode");
		sbQuery.append(" and hab.holderAccount.idHolderAccountPk = ha.idHolderAccountPk  ");
		sbQuery.append(" and (hab.totalBalance > 0 or hab.reportedBalance > 0 or hab.saleBalance > 0)");
		sbQuery.append(" and ha.alternateCode like :holderalternatecode ");
		sbQuery.append(" order by hab.availableBalance ");
		parameters.put("isincode", security);
		parameters.put("holderalternatecode", "%"+holderAlternateCode+"%");
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the block operation details to process the block operation in special corporative events.
     *
     * @param participant the participant
     * @param holderAccount the holder account
     * @param security the security
     * @param status the status
     * @param blockLevel the block level
     * @return the block balance details
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBlockBalanceDetails(Long participant, Long holderAccount, String security, Integer status, Integer blockLevel)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select br.blockType, bo.actualBlockBalance, bo.idBlockOperationPk, ");
		sbQuery.append(" br.holder.idHolderPk , bo.indCashDividend, bo.indReturnContributions, bo.indInterest, ");
		sbQuery.append(" bo.indStockDividend, bo.indSuscription, bo.participant.idParticipantPk, bo.blockLevel, " );
		sbQuery.append(" bo.blockState, bo.indAmortization, bo.indRescue ");
		sbQuery.append(" from BlockOperation bo, BlockRequest br ");
		sbQuery.append(" where bo.blockRequest.idBlockRequestPk = br.idBlockRequestPk ");
		if(participant != null && participant > 0){
			sbQuery.append(" and bo.participant.idParticipantPk = :participant ");
			parameters.put("participant", participant);
		}
		sbQuery.append(" and bo.holderAccount.idHolderAccountPk = :holderaccount ");
		sbQuery.append(" and bo.securities.idSecurityCodePk = :security ");
		sbQuery.append(" and bo.blockState = :blockstate ");
		if(blockLevel != null && blockLevel > 0){
			sbQuery.append(" and bo.blockLevel = :blocklevel ");
			parameters.put("blocklevel", blockLevel);
		}
		sbQuery.append(" order by br.blockType, bo.actualBlockBalance ");

		parameters.put("holderaccount", holderAccount);
		parameters.put("security", security);
		parameters.put("blockstate", status);
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Update security.
     *
     * @param security the security
     * @param retirmentDate the retirment date
     * @param state the state
     * @param currentNominal the current nominal
     * @param shareCapital the share capital
     * @param circulationCapital the circulation capital
     * @param loggerUser the logger user
     * @return the int
     * @throws ServiceException the service exception
     */
    public int updateSecurity(String security, Date retirmentDate, Integer state, BigDecimal currentNominal, 
		BigDecimal shareCapital,BigDecimal circulationCapital, LoggerUser loggerUser) throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update Security sec set ");
		if(retirmentDate != null){
			sbQuery.append(" sec.retirementDate = :retirement , ");
		}
		if(state != null && state > 0){
			sbQuery.append(" sec.stateSecurity = :state , ");
		}
		if(currentNominal != null ){
			sbQuery.append(" sec.currentNominalValue = :currentnominal , ");
		}
		if(shareCapital != null){
			sbQuery.append(" sec.shareCapital = :sharecapital , ");
		}
		if(circulationCapital != null){
			sbQuery.append(" sec.circulationAmount = :circulationCapital , ");
		}
		sbQuery.append(" sec.lastModifyUser = :modifyuser, sec.lastModifyDate = :modifydate,");
		sbQuery.append(" sec.lastModifyIp = :modifyip, sec.lastModifyApp = :modifyapp ");
		sbQuery.append(" where sec.idSecurityCodePk = :isin ");

		Query query = em.createQuery(sbQuery.toString());

		if(retirmentDate != null){
			query.setParameter("retirement", retirmentDate);
		}
		if(state != null && state > 0){
			query.setParameter("state", state);
		}
		if(currentNominal != null ){
			query.setParameter("currentnominal", currentNominal);
		}
		if(shareCapital != null){
			query.setParameter("sharecapital", shareCapital);
		}
		if(circulationCapital != null){
			query.setParameter("circulationCapital", circulationCapital);
		}
		query.setParameter("modifyuser", loggerUser.getUserName());
		query.setParameter("modifydate", new Date());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("isin", security);
		return query.executeUpdate();
	}
    
    /**
     * Update security.
     *
     * @param lstSecurities the lst securities
     * @param retirmentDate the retirment date
     * @param state the state
     * @param currentNominal the current nominal
     * @param shareCapital the share capital
     * @param circulationCapital the circulation capital
     * @param loggerUser the logger user
     * @return the int
     * @throws ServiceException the service exception
     */
    public int updateSecurity(List<String> lstSecurities, Date retirmentDate, Integer state, BigDecimal currentNominal, 
    		BigDecimal shareCapital,BigDecimal circulationCapital, LoggerUser loggerUser) throws ServiceException {
        	StringBuilder sbQuery = new StringBuilder();
    		sbQuery.append("update Security sec set ");
    		if(retirmentDate != null){
    			sbQuery.append(" sec.retirementDate = :retirement , ");
    		}
    		if(state != null && state > 0){
    			sbQuery.append(" sec.stateSecurity = :state , ");
    		}
    		if(currentNominal != null ){
    			sbQuery.append(" sec.currentNominalValue = :currentnominal , ");
    		}
    		if(shareCapital != null){
    			sbQuery.append(" sec.shareCapital = :sharecapital , ");
    		}
    		if(circulationCapital != null){
    			sbQuery.append(" sec.circulationAmount = :circulationCapital , ");
    		}
    		sbQuery.append(" sec.lastModifyUser = :modifyuser, sec.lastModifyDate = :modifydate,");
    		sbQuery.append(" sec.lastModifyIp = :modifyip, sec.lastModifyApp = :modifyapp ");
    		sbQuery.append(" where sec.idSecurityCodePk in (:lstSecurities) ");

    		Query query = em.createQuery(sbQuery.toString());

    		if(retirmentDate != null){
    			query.setParameter("retirement", retirmentDate);
    		}
    		if(state != null && state > 0){
    			query.setParameter("state", state);
    		}
    		if(currentNominal != null ){
    			query.setParameter("currentnominal", currentNominal);
    		}
    		if(shareCapital != null){
    			query.setParameter("sharecapital", shareCapital);
    		}
    		if(circulationCapital != null){
    			query.setParameter("circulationCapital", circulationCapital);
    		}
    		query.setParameter("modifyuser", loggerUser.getUserName());
    		query.setParameter("modifydate", new Date());
    		query.setParameter("modifyip", loggerUser.getIpAddress());
    		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
    		query.setParameter("lstSecurities", lstSecurities);
    		return query.executeUpdate();
    	}
    
    /**
     * Update issuance.
     *
     * @param lstIssuances the lst issuances
     * @param issuanceState the issuance state
     * @param loggerUser the logger user
     */
    public void updateIssuance(List<String> lstIssuances, Integer issuanceState, LoggerUser loggerUser) {
    	StringBuffer stringBuffer = new StringBuffer();
    	stringBuffer.append(" UPDATE Issuance ");
    	stringBuffer.append(" SET stateIssuance = :issuanceState ");
    	stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
    	stringBuffer.append(" WHERE idIssuanceCodePk in (:lstIssuances) ");
    	
    	Query query= em.createQuery(stringBuffer.toString());
    	query.setParameter("issuanceState", issuanceState);
    	query.setParameter("lstIssuances", lstIssuances);
    	query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
		em.flush();
    }
    
    /**
     * Gets the excision securities.
     *
     * @param corpOperation the corp operation
     * @return the excision securities
     * @throws ServiceException the service exception
     */
    public List<SecuritiesCorporative> getExcisionSecurities(Long corpOperation)throws ServiceException{
    	StringBuilder sbQuery=new StringBuilder();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select sc ");
		sbQuery.append(" from SecuritiesCorporative sc ");
		sbQuery.append(" INNER JOIN FETCH sc.security s ");
		sbQuery.append(" where sc.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" order by sc.idSecuritiesCorporativePk ");
		parameters.put("idCorporateFk", corpOperation);	
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the block corp process results for definitive.
     *
     * @param corpProcessId the corp process id
     * @param participant the participant
     * @param security the security
     * @param indRemanent the ind remanent
     * @param indProcessed the ind processed
     * @param originTarget the origin target
     * @return the block corp process results for definitive
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBlockCorpProcessResultsForDefinitive(Long corpProcessId, Long participant, String security, 
			Integer indRemanent, Integer indProcessed, Integer originTarget)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select bcr.blockType, bcr.holder.idHolderPk, br.blockEntity.idBlockEntityPk, br.blockNumber, br.blockNumberDate, ");
		sbQuery.append(" br.blockForm, br.otherBlockMotive, br.currency, br.indRequestDeclaration, br.exchangeRate, ");
		sbQuery.append(" bcr.participant.idParticipantPk, bcr.security.idSecurityCodePk, bcr.holderAccount.idHolderAccountPk, ");
		sbQuery.append(" bcr.blockOperation.idBlockOperationPk, bcr.indCashDividend, ");
		sbQuery.append(" bcr.indInterest, bcr.indStockDividend, bcr.indSuscription, bcr.indContributionReturn, bo.indAccreditation, " );
		sbQuery.append(" bo.indAmortization, bo.indRescue, bcr.blockedBalance, bo.originalBlockBalance, bo.blockLevel,bo.indRegisteredDepositary,bo.nominalValue ");
		sbQuery.append(" from BlockCorporativeResult bcr, BlockOperation bo, BlockRequest br  ");
		sbQuery.append(" where bcr.blockOperation.idBlockOperationPk = bo.idBlockOperationPk ");
		sbQuery.append(" and bo.blockRequest.idBlockRequestPk = br.idBlockRequestPk ");
		sbQuery.append(" and bcr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
		sbQuery.append(" and bcr.participant.idParticipantPk = :participant ");		
		if(security != null && security.length() > 0){
			sbQuery.append(" and bcr.security.idSecurityCodePk = :security ");
			parameters.put("security", security);
		}
		sbQuery.append(" and bcr.indRemanent = :remanent ");
		sbQuery.append(" and bcr.indProcessed = :processed ");
		if(originTarget != null && originTarget > 0){
			sbQuery.append(" and bcr.indOriginDestiny = :origintarget");
			parameters.put("origintarget", originTarget);
		}
		sbQuery.append(" order by bcr.holderAccount.idHolderAccountPk, bcr.blockType ");
		parameters.put("corpprocessid", corpProcessId);
		parameters.put("participant", participant);
		parameters.put("remanent", indRemanent);
		parameters.put("processed", indProcessed);
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the report balances from the guarantee operation and holder account operations to
     * process the report operations for special corporative events.
     *
     * @param participant the participant
     * @param holderAccount the holder account
     * @param security the security
     * @return the balances report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getBalancesReportOperations(Long participant, Long holderAccount, String security)
			throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select go.guaranteeType, hao.role, go.guaranteeBalance, ");
		sbQuery.append(" hao.idHolderAccountOperationPk , hao.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append(" from GuaranteeOperation go, HolderAccountOperation hao ");
		sbQuery.append(" where go.holderAccountOperation.idHolderAccountOperationPk = hao.idHolderAccountOperationPk ");
		sbQuery.append(" and go.participant.idParticipantPk = :participant ");
		sbQuery.append(" and go.holderAccount.idHolderAccountPk = :holderaccount ");
		sbQuery.append(" and go.securities.idSecurityCodePk = :security ");
		sbQuery.append(" and go.guaranteeClass = :guaranteeclass ");
		sbQuery.append(" and hao.holderAccountState != :state and go.guaranteeBalance > 0 ");
		sbQuery.append(" order by go.guaranteeType, hao.role, go.guaranteeBalance ");
		parameters.put("participant", participant);
		parameters.put("holderaccount", holderAccount);
		parameters.put("security", security);
		parameters.put("guaranteeclass", GuaranteeClassType.SECURITIES.getCode());
		parameters.put("state", HolderAccountOperationStateType.CANCELLED.getCode());
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    
    /**
     * This method is used to get the results mechanism operation and guarantee balance values from the ReportCorporativeResult table data by sending the
     * seller role,buyer role,remnant,destiny origin.
     *
     * @param security the security
     * @param corpOperId the corp oper id
     * @param guarantyType1 Guarantee type is principle
     * @param guarantyType2 Guarantee type is stock dividend principle
     * @param sellerRole the seller role
     * @param buyerRole the buyer role
     * @param indRemanent the ind remanent
     * @param indProcessed the ind processed
     * @param originDestiny destiny value we sent as 2
     * @return the seller buyyer sum balances report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getSellerBuyyerSumBalancesReportOperations(String security, Long corpOperId, Integer guarantyType1, Integer guarantyType2,
			Integer sellerRole, Integer buyerRole, Integer indRemanent, Integer indProcessed, Integer originDestiny)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rcr.mechanismOperation.idMechanismOperationPk, sum(go.guaranteeBalance), ");
		sbQuery.append(" sum(rcr.guaranteeBalance) ");
		sbQuery.append(" from GuaranteeOperation go, HolderAccountOperation hao, ReportCorporativeResult rcr ");
		sbQuery.append(" where go.holderAccountOperation.idHolderAccountOperationPk = hao.idHolderAccountOperationPk");
		sbQuery.append(" and go.securities.idSecurityCodePk = :security");
		sbQuery.append(" and rcr.corporativeOperation.idCorporativeOperationPk = :corpOperId");
		sbQuery.append(" and rcr.participant.idParticipantPk = go.participant.idParticipantPk ");
		sbQuery.append(" and rcr.holderAccount.idHolderAccountPk = go.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and rcr.mechanismOperation.idMechanismOperationPk = hao.mechanismOperation.idMechanismOperationPk ");	
		sbQuery.append(" and (go.guaranteeType = :guaranty1 or go.guaranteeType = :guaranty2) ");
		sbQuery.append(" and (rcr.guaranteeType = :guaranty1 or rcr.guaranteeType = :guaranty2) ");
		sbQuery.append(" and hao.role = :buyerRole and rcr.operationRole = :sellerRole ");
		sbQuery.append(" and go.guaranteeClass = :guaranteeclass ");
		sbQuery.append(" and hao.holderAccountState != :state and go.guaranteeBalance > 0 ");
		sbQuery.append(" and rcr.indRemanent = :remanent ");
		sbQuery.append(" and rcr.indProcessed = :processed ");
		sbQuery.append(" and rcr.indOriginDestiny = :origindestiny ");
		sbQuery.append(" group by rcr.mechanismOperation.idMechanismOperationPk ");
		parameters.put("security", security);
		parameters.put("corpOperId", corpOperId);
		parameters.put("guaranty1", guarantyType1);
		parameters.put("guaranty2", guarantyType2);
		parameters.put("sellerRole", sellerRole);
		parameters.put("buyerRole", buyerRole);
		parameters.put("remanent", indRemanent);
		parameters.put("processed", indProcessed);
		parameters.put("origindestiny", originDestiny);
		parameters.put("guaranteeclass", GuaranteeClassType.SECURITIES.getCode());
		parameters.put("state", HolderAccountOperationStateType.CANCELLED.getCode());
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get the results from the GuaranteeOperation,HolderAccountOperation tables to process the Special Corporative events.
     *
     * @param security the security
     * @param guarantyType1 Guarantee type is principle
     * @param guarantyType2 Guarantee type is stock dividend principle
     * @param sellerRole the seller role
     * @return the seller balance report operations
     * @throws ServiceException the service exception
     */
    public List<Object[]> getSellerBalanceReportOperations(String security, Integer guarantyType1, Integer guarantyType2,
			Integer sellerRole)
					throws ServiceException {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select go.guaranteeType, go.guaranteeBalance, go.participant.idParticipantPk, go.holderAccount.idHolderAccountPk,");
		sbQuery.append(" hao.idHolderAccountOperationPk , hao.mechanismOperation.idMechanismOperationPk ");
		sbQuery.append(" from GuaranteeOperation go, HolderAccountOperation hao ");
		sbQuery.append(" where go.holderAccountOperation.idHolderAccountOperationPk = hao.idHolderAccountOperationPk ");
		sbQuery.append(" and go.securities.idSecurityCodePk = :security");
		sbQuery.append(" and (go.guaranteeType = :guaranty1 or go.guaranteeType = :guaranty2) ");
		sbQuery.append(" and  hao.role= :role ");
		sbQuery.append(" and go.guaranteeClass = :guaranteeclass ");
		sbQuery.append(" and hao.holderAccountState != :state and go.guaranteeBalance > 0 ");
		sbQuery.append(" order by hao.mechanismOperation.idMechanismOperationPk, go.guaranteeBalance ");
		parameters.put("security", security);
		parameters.put("guaranty1", guarantyType1);
		parameters.put("guaranty2", guarantyType2);
		parameters.put("role", sellerRole);
		parameters.put("guaranteeclass", GuaranteeClassType.SECURITIES.getCode());
		parameters.put("state", HolderAccountOperationStateType.CANCELLED.getCode());

		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the holder accounts for rescue event.
     *
     * @param idCorporativeOperationPk the id corporative operation pk
     * @return the holder accounts for rescue event
     */
    public List<Object[]> getHolderAccountsForRescueEvent(
			Long idCorporativeOperationPk) {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), sum(rd.accountBalance)"); 
		sbQuery.append(" from RetirementDetail rd, RetirementOperation ro,HolderAccount ha ");
		sbQuery.append(" where rd.retirementOperation.idRetirementOperationPk = ro.idRetirementOperationPk");
		sbQuery.append(" and rd.holderAccount.idHolderAccountPk = ha.idHolderAccountPk");
		sbQuery.append(" and ro.corporativeOperation.idCorporativeOperationPk = :corpId");
		sbQuery.append(" group by substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,1)-locate('(',ha.alternateCode,1))) ");
		parameters.put("corpId", idCorporativeOperationPk);
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the balances for each holder account.
     *
     * @param idCorporativeOperationPk the id corporative operation pk
     * @param holderAlternateCode the holder alternate code
     * @return the balances for each holder account
     */
    public List<Object[]> getBalancesForEachHolderAccount(
			Long idCorporativeOperationPk, String holderAlternateCode) {
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select rd.holderAccount.idHolderAccountPk,rd.accountBalance,0,0,0,0,0,0,0,0,rd.accountBalance,ro.idParticipantFk.idParticipantPk ");
		sbQuery.append(" from RetirementDetail rd, RetirementOperation ro,HolderAccount ha ");
		sbQuery.append(" where rd.retirementOperation.idRetirementOperationPk = ro.idRetirementOperationPk");
		sbQuery.append(" and rd.holderAccount.idHolderAccountPk = ha.idHolderAccountPk");
		sbQuery.append(" and ro.corporativeOperation.idCorporativeOperationPk = :corpId");
		sbQuery.append(" and ha.alternateCode like :holderalternatecode ");
		sbQuery.append(" order by rd.accountBalance");
		parameters.put("corpId", idCorporativeOperationPk);
		parameters.put("holderalternatecode", "%"+holderAlternateCode+"%");
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * This method is used to get distinct the holders from holder account and their total balance for the give stock process.
     *
     * @param stockProcessId the stock process id
     * @return the distinct holders for amortization
     * @throws ServiceException the service exception
     */
    public List<Object[]> getDistinctHoldersForAmortization(Long stockProcessId)
			throws ServiceException {
    	//TODO replace hard code
    	StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), ");
		sbQuery.append(" (sum(scb.totalBalance) / count(h.idHolderPk)), max(h.holderType),max(case when h.nationality = 2404 then 1486 else 1487 end) ");
		sbQuery.append(" from StockCalculationBalance scb, HolderAccount ha, HolderAccountDetail had, Holder h ");
		sbQuery.append(" where scb.stockCalculationProcess.idStockCalculationPk = :stockprocessid");
		sbQuery.append(" and scb.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
		sbQuery.append(" and ha.idHolderAccountPk = had.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and had.holder.idHolderPk = h.idHolderPk ");
		sbQuery.append(" group by substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,1)-locate('(',ha.alternateCode,1))) ");
		parameters.put("stockprocessid", stockProcessId);
		return findListByQueryString(sbQuery.toString(), parameters);
	}
    
    /**
     * Gets the list id corporative operation.
     *
     * @param security the security
     * @param corporativeEventType the corporative event type
     * @return the list id corporative operation
     */
    public List<Long> getListIdCorporativeOperation(String security, Integer corporativeEventType) {
		List<Long> lstIdCorporativeOperation= null;
		StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT CO.idCorporativeOperationPk FROM CorporativeOperation CO ");
		stringBuffer.append(" WHERE CO.securities.idSecurityCodePk = :idIsinCode ");
		stringBuffer.append(" and CO.corporativeEventType.corporativeEventType = :corporativeEventType ");
		stringBuffer.append(" and CO.state in (:lstCorporativeState) ");
		stringBuffer.append(" and trunc(CO.deliveryDate) > trunc(:currentDate) ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", security);
		query.setParameter("corporativeEventType", corporativeEventType);
		query.setParameter("currentDate", CommonsUtilities.currentDate());
		List<Integer> lstCorporativeState= new ArrayList<Integer>();
		lstCorporativeState.add(CorporateProcessStateType.REGISTERED.getCode());
		lstCorporativeState.add(CorporateProcessStateType.MODIFIED.getCode());
		query.setParameter("lstCorporativeState", lstCorporativeState);
		lstIdCorporativeOperation = query.getResultList();
		return lstIdCorporativeOperation;
	}
    
    /**
     * Update corporative operation state.
     *
     * @param lstIdCorporativeOperation the lst id corporative operation
     * @param corporativeOperationState the corporative operation state
     * @param loggerUser the logger user
     */
    public void updateCorporativeOperationState(List<Long> lstIdCorporativeOperation, Integer corporativeOperationState, LoggerUser loggerUser)	{
    	StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" UPDATE CorporativeOperation SET state = :corporativeOperationState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idCorporativeOperationPk in (:lstIdCorporativeOperation)");
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("corporativeOperationState", corporativeOperationState);
		query.setParameter("lstIdCorporativeOperation", lstIdCorporativeOperation);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.executeUpdate();
	}
    
    /**
     * Gets the list id program interest coupon.
     *
     * @param security the security
     * @return the list id program interest coupon
     */
    public List<Long> getListIdProgramInterestCoupon(String security)	{
		List<Long> lstIdProgramInterestCoupon= null;
		StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC.idProgramInterestPk FROM ProgramInterestCoupon PIC ");
		stringBuffer.append(" WHERE PIC.interestPaymentSchedule.security.idSecurityCodePk = :idIsinCode ");
		stringBuffer.append(" and PIC.stateProgramInterest = :stateProgramInterest ");
		stringBuffer.append(" and trunc(PIC.paymentDate) > trunc(:currentDate) ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", security);
		query.setParameter("stateProgramInterest", ProgramScheduleStateType.PENDING.getCode());
		query.setParameter("currentDate", CommonsUtilities.currentDate());
		lstIdProgramInterestCoupon = query.getResultList();
		return lstIdProgramInterestCoupon;
	}
    
    /**
     * Gets the list id program interest coupon.
     *
     * @param security the security
     * @param deliveryDate the delivery date
     * @return the list id program interest coupon
     */
    public List<Long> getListIdProgramInterestCouponActual(String security,Date deliveryDate)	{
		List<Long> lstIdProgramInterestCoupon= null;
		StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC.idProgramInterestPk FROM ProgramInterestCoupon PIC ");
		stringBuffer.append(" WHERE PIC.interestPaymentSchedule.security.idSecurityCodePk = :idIsinCode ");
		stringBuffer.append(" and trunc(PIC.paymentDate) = trunc(:currentDate) ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", security);
		query.setParameter("currentDate", deliveryDate);
		lstIdProgramInterestCoupon = query.getResultList();
		return lstIdProgramInterestCoupon;
	}
    
    /**
     * Gets the program interest coupon.
     *
     * @param idProgramInterestPk the id program interest pk
     * @return the program interest coupon
     */
    public ProgramInterestCoupon getProgramInterestCoupon(Long idProgramInterestPk){
    	Object objProgramInterestCoupon = null;
    	StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC FROM ProgramInterestCoupon PIC ");
		stringBuffer.append(" WHERE PIC.idProgramInterestPk = :idProgramInterestPk ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idProgramInterestPk", idProgramInterestPk);
		objProgramInterestCoupon = query.getSingleResult();
		if(objProgramInterestCoupon!=null){
			return (ProgramInterestCoupon)objProgramInterestCoupon;
		}
		return null;
    }
    
    /**
     * Gets the program amortization coupon.
     *
     * @param idProgramAmortizationPk the id program amortization pk
     * @return the program amortization coupon
     */
    public ProgramAmortizationCoupon getProgramAmortizationCoupon(Long idProgramAmortizationPk){
    	Object objProgramAmortizationCoupon = null;
    	StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC FROM ProgramAmortizationCoupon PIC ");
		stringBuffer.append(" WHERE PIC.idProgramAmortizationPk = :idProgramAmortizationPk ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idProgramAmortizationPk", idProgramAmortizationPk);
		objProgramAmortizationCoupon = query.getSingleResult();
		if(objProgramAmortizationCoupon!=null){
			return (ProgramAmortizationCoupon)objProgramAmortizationCoupon;
		}
		return null;
    }
    
    /**
     * Gets the list id program interest coupon.
     *
     * @param security the security
     * @param deliveryDate the delivery date
     * @return the list id program interest coupon
     */
    public List<Long> getListIdProgramAmortizationCoupon(String security,Date deliveryDate)	{
		List<Long> lstIdProgramInterestCoupon= null;
		StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC.idProgramAmortizationPk FROM ProgramAmortizationCoupon PIC ");
		stringBuffer.append(" WHERE PIC.amortizationPaymentSchedule.security.idSecurityCodePk = :idIsinCode ");
		stringBuffer.append(" and trunc(PIC.paymentDate) = trunc(:currentDate) ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", security);
		query.setParameter("currentDate", deliveryDate);
		lstIdProgramInterestCoupon = query.getResultList();
		return lstIdProgramInterestCoupon;
	}
    /**
     * Update program interest coupon state.
     *
     * @param lstIdProgramInterestCoupon the lst id program interest coupon
     * @param programInterestCouponState the program interest coupon state
     * @param loggerUser the logger user
     */
    public void updateProgramInterestCouponState(List<Long> lstIdProgramInterestCoupon, Integer programInterestCouponState, LoggerUser loggerUser){
    	StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" UPDATE ProgramInterestCoupon SET stateProgramInterest = :programInterestCouponState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramInterestPk in (:lstIdProgramInterestCoupon) ");
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("programInterestCouponState", programInterestCouponState);
		query.setParameter("lstIdProgramInterestCoupon", lstIdProgramInterestCoupon);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.executeUpdate();
	}
    
    /**
     * Update program amortization coupon state.
     *
     * @param lstIdProgramAmortizationCoupon the lst id program amortization coupon
     * @param programAmortizationCouponState the program amortization coupon state
     * @param loggerUser the logger user
     */
    public void updateProgramAmortizationCouponState(List<Long> lstIdProgramAmortizationCoupon, Integer programAmortizationCouponState, LoggerUser loggerUser){
    	StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" UPDATE ProgramAmortizationCoupon SET stateProgramAmortization = :programAmortizationCouponState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramAmortizationPk in (:lstIdProgramAmortizationCoupon) ");
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("programAmortizationCouponState", programAmortizationCouponState);
		query.setParameter("lstIdProgramAmortizationCoupon", lstIdProgramAmortizationCoupon);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.executeUpdate();
	}
    /**
     * Find exist balance.
     *
     * @param security the security
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean findExistBalance(String security) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT COUNT(*)");
			sbQuery.append("		FROM HolderAccountBalance hab");
			sbQuery.append("		WHERE hab.security.idSecurityCodePk = :idIsinCodePk");
			sbQuery.append("		AND hab.totalBalance > 0");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idIsinCodePk", security);
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult())){
				Integer total =  Integer.parseInt(query.getSingleResult().toString());
				if(total > 0)
					return true;
			}
		} catch (NoResultException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
    
    /**
     * Find corp operation registered.
     *
     * @param security the security
     * @return the list
     * @throws ServiceException the service exception
     */
    public List<CorporativeOperation> findCorpOperationRegistered(String security) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT co");
		sbQuery.append("		FROM CorporativeOperation co");
		sbQuery.append("		WHERE co.securities.idSecurityCodePk = :idIsinCodePk");
		sbQuery.append("		  AND co.state != :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idIsinCodePk", security);
		query.setParameter("state", CorporateProcessStateType.DEFINITIVE.getCode());
		return (List<CorporativeOperation>)query.getResultList();
	}
    
    @SuppressWarnings("unchecked")
	public String findIssuanceProcess(String security) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT se.issuance.idIssuanceCodePk ");
			sbQuery.append("	FROM Security se ");
//			sbQuery.append("	INNER JOIN FETCH se.issuance iss ");
			sbQuery.append("	WHERE se.idSecurityCodePk = :idIsinCodePk ");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idIsinCodePk", security);
			
			List<String> listIssuance = query.getResultList();
			return (String) listIssuance.get(0);
		} catch (NoResultException e) {
			return null;
		}		
	}
    
    @SuppressWarnings("unchecked")
	public boolean isUpdateIssuance(String idIssuanceCodePk) {
		try {
			boolean updateExipration =false;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT s ");
			sbQuery.append("	FROM Security s ");
			sbQuery.append("	WHERE s.issuance.idIssuanceCodePk = :idIssuanceCodePk ");			
//			sbQuery.append("	AND  s.stateSecurity != :state ");
			sbQuery.append("	ORDER BY s.expirationDate ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idIssuanceCodePk", idIssuanceCodePk);
//			query.setParameter("state", SecurityStateType.EXPIRED.getCode());
			List<Security> listSecurities = query.getResultList();
			List<Security> listaSecurity = new ArrayList<Security>();
			for (Security security : listSecurities){
				if (!security.getStateSecurity().equals(SecurityStateType.EXPIRED.getCode())){
					listaSecurity.add(security);
				}
			}
			if (listaSecurity.size()==1){
				if (CommonsUtilities.truncateDateTime(listaSecurity.get(0).getExpirationDate()).
						equals(CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()))){
					updateExipration = true;
				}
			}else{
				updateExipration = false;
			}
			return updateExipration;
		} catch (NoResultException e) {
			return false;
		}		
	}
    
    /**
     * Find issuance.
     *
     * @param security the security
     * @return the issuance
     */
    public Issuance findIssuance(String security) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT se.issuance");
			sbQuery.append("	FROM Security se");
			sbQuery.append("	INNER JOIN FETCH se.issuance.securities");
			sbQuery.append("	WHERE se.idSecurityCodePk = :idIsinCodePk");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idIsinCodePk", security);
			return (Issuance) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}		
	}
    
    /**
     * Find relation retirement corporative.
     *
     * @param idCorporativeOperationPk the id corporative operation pk
     * @return the retirement operation
     */
    public RetirementOperation findRelationRetirementCorporative(Long idCorporativeOperationPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT co.retirementOperation");
			sbQuery.append("	FROM CorporativeOperation co");
			sbQuery.append("	INNER JOIN FETCH co.retirementOperation.lstCorporativeOperation");
			sbQuery.append("	WHERE co.idCorporativeOperationPk = :idCorporativeOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idCorporativeOperationPk", idCorporativeOperationPk);
			return (RetirementOperation) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
    
    /**
     * Update retirement operation.
     *
     * @param retirementOperation the retirement operation
     * @param loggerUser the logger user
     */
    public void updateRetirementOperation(RetirementOperation retirementOperation, LoggerUser loggerUser) {
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update RetirementOperation ro set ro.state = :state,");
		sbQuery.append(" ro.lastModifyUser = :modifyuser, ro.lastModifyDate = :modifydate,");
		sbQuery.append(" ro.lastModifyIp = :modifyip, ro.lastModifyApp = :modifyapp where ro.idRetirementOperationPk = :idRetirementOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", retirementOperation.getState());
		query.setParameter("idRetirementOperationPk", retirementOperation.getIdRetirementOperationPk());
		query.setParameter("modifyuser", loggerUser.getUserName());
		query.setParameter("modifydate", new Date());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.executeUpdate();
	}
    
    /**
     * Gets the holder market fact balances.
     *
     * @param holderAccount the holder account
     * @param security the security
     * @param participant the participant
     * @return the holder market fact balances
     */
    public List<HolderMarketFactBalance> getHolderMarketFactBalances(Long holderAccount,String security,Long participant){
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select hmb from HolderMarketFactBalance hmb where  ");
		sbQuery.append(" hmb.holderAccount.idHolderAccountPk = :holderAccountPkPrm");
		sbQuery.append(" And hmb.security.idSecurityCodePk = :securityCodePkPrm");
		sbQuery.append(" And hmb.participant.idParticipantPk = :participantPkPrm");
		sbQuery.append(" and hmb.indActive = :active ");
		TypedQuery<HolderMarketFactBalance> query = em.createQuery(sbQuery.toString(), HolderMarketFactBalance.class);
		query.setParameter("holderAccountPkPrm", holderAccount);
		query.setParameter("securityCodePkPrm", security);
		query.setParameter("participantPkPrm", participant);
		query.setParameter("active", BooleanType.YES.getCode());
		return query.getResultList();
    }
    
    /**
     * Get Security.
     *
     * @param idSecurity code security
     * @return object security
     * @throws ServiceException the Service Exception
     */
    public Security getSecurity (String idSecurity)throws ServiceException{
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select se from Security se where  ");
		sbQuery.append(" se.idSecurityCodePk = :idSecurity");
		TypedQuery<Security> query = em.createQuery(sbQuery.toString(), Security.class);
		query.setParameter("idSecurity", idSecurity);
		return query.getSingleResult();
    }
    
    /**
     * Get List Holder.
     *
     * @param idHolderAccount id holder account
     * @return List Holder
     * @throws ServiceException the Service Exception
     */
    public List<Holder> getListHolder (Long idHolderAccount)throws ServiceException{
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select ho from HolderAccount ha,HolderAccountDetail had, Holder ho where  ");
		sbQuery.append(" ha.idHolderAccountPk = had.holderAccount.idHolderAccountPk ");
		sbQuery.append(" And had.holder.idHolderPk = ho.idHolderPk ");
		sbQuery.append(" And ha.idHolderAccountPk = :idHolderAccountPk");
		TypedQuery<Holder> query = em.createQuery(sbQuery.toString(), Holder.class);
		query.setParameter("idHolderAccountPk", idHolderAccount);
		return query.getResultList();
    }
    
    /**
     * Find securities expired dpf.
     *
     * @param securityTO the security to
     * @return the list
     * @throws ServiceException the service exception
     */
    @SuppressWarnings("unchecked")
   	public List<ProgramInterestCoupon> findSecuritiesExpiredDpfInterestCoupon(String finalDate) throws ServiceException{		
   		//Oscar
    	
    	try {
        		
   			StringBuilder sbQuery = new StringBuilder();
   			sbQuery.append(" select pic.ID_PROGRAM_INTEREST_PK, pic.STATE_PROGRAM_INTEREST  "
   					+ " from PROGRAM_INTEREST_COUPON pic "
   					+ " inner join INTEREST_PAYMENT_SCHEDULE ips on pic.ID_INT_PAYMENT_SCHEDULE_FK=ips.ID_INT_PAYMENT_SCHEDULE_PK "
   					+ " inner join SECURITY sec on ips.ID_SECURITY_CODE_FK=sec.ID_SECURITY_CODE_PK "
   					+ " where 1=1 "
   					+ " and pic.STATE_PROGRAM_INTEREST= :state  " //vigente
   					+ " and trunc(pic.EXPERITATION_DATE) <= " + "'"+finalDate+"'"
   					+ " and sec.SECURITY_CLASS in (:lstSecurityClass) "
   					+ " order by  "
   					+ " pic.ID_INT_PAYMENT_SCHEDULE_FK desc "
   					+ " ,pic.EXPERITATION_DATE,pic.COUPON_NUMBER ");	
   			
   			Query query = em.createNativeQuery(sbQuery.toString()); 

//   	    	query.setParameter("finalDate", finalDate);
   	    	query.setParameter("state",  ProgramScheduleStateType.PENDING.getCode() );
   	    	List<Integer> lstSecurityClass= new ArrayList<Integer>();
   			lstSecurityClass.add(SecurityClassType.DPA.getCode());
   			lstSecurityClass.add(SecurityClassType.DPF.getCode());
   			query.setParameter("lstSecurityClass",  lstSecurityClass);
   			
   			List<Object> objectResultQuery = query.getResultList();   		
   			
   			List<ProgramInterestCoupon> lstProgramInterest = new ArrayList<ProgramInterestCoupon>();
   			
   			for (int i = 0; i < objectResultQuery.size(); i++) {
				
   				Object[] sResults = (Object[])objectResultQuery.get(i);
   				ProgramInterestCoupon program = new ProgramInterestCoupon();
   				program.setIdProgramInterestPk(Long.valueOf(sResults[0].toString()));
   				program.setStateProgramInterest(Integer.valueOf(sResults[1].toString()));
   				lstProgramInterest.add(program);
			}
   	    	return  lstProgramInterest;
   		} catch(NoResultException ex){
   			return null;
   		}		
   	}
    
    
    /**
     * Find securities expired dpf.
     *
     * @param securityTO the security to
     * @return the list
     * @throws ServiceException the service exception
     */
    @SuppressWarnings("unchecked")
   	public List<String> findSecuritiesExpiredDpf(SecurityTO securityTO) throws ServiceException{		
   		try {
   			StringBuilder sbQuery = new StringBuilder();
   			sbQuery.append(" select  se.idSecurityCodePk  From Security se ");				
   			sbQuery.append("  WHERE   1=1 ");			
   			if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
   				sbQuery.append(" and trunc(se.expirationDate) <= trunc(:expirationDate) ");
   			}	
   			if(Validations.validateIsNotNullAndNotEmpty( securityTO.getStateSecurity() )){
   				sbQuery.append(" and se.stateSecurity = :stateSecurity ");
   			}
   			sbQuery.append(" and se.securityClass in (:lstSecurityClass) ");
   			
   	    	Query query = em.createQuery(sbQuery.toString());    	    	
   			if(Validations.validateIsNotNullAndNotEmpty(securityTO.getExpirationDate())){
   				query.setParameter("expirationDate",  securityTO.getExpirationDate() );
   			}
   			if(Validations.validateIsNotNullAndNotEmpty(securityTO.getStateSecurity())){
   				query.setParameter("stateSecurity",  securityTO.getStateSecurity() );
   			}
   			List<Integer> lstSecurityClass= new ArrayList<Integer>();
   			lstSecurityClass.add(SecurityClassType.DPA.getCode());
   			lstSecurityClass.add(SecurityClassType.DPF.getCode());
   			query.setParameter("lstSecurityClass",  lstSecurityClass);
   			
   	    	return  ((List<String>)query.getResultList());
   		} catch(NoResultException ex){
   			return null;
   		}		
   	}
    
    /**
     * Validate balance acreditation.
     *
     * @param securityTO the security to
     * @return the list
     * @throws ServiceException the service exception
     */
   	@SuppressWarnings("unchecked")
	public List<String> validateBalanceAcreditation(SecurityTO securityTO) throws ServiceException{		
   		try {
   			StringBuilder sbQuery = new StringBuilder();
   			sbQuery.append(" select  sec.idSecurityCodePk From HolderAccountBalance hab ");
   			sbQuery.append(" inner join hab.security sec 								");		
   			sbQuery.append("  WHERE 1=1					 								");
   			sbQuery.append("  and hab.accreditationBalance > 0 							");	
   			if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
   				sbQuery.append(" and trunc(sec.expirationDate) <= trunc(:expirationDate) ");
   			}	
   			if(Validations.validateIsNotNullAndNotEmpty(securityTO.getStateSecurity() )){
   				sbQuery.append(" and sec.stateSecurity = :stateSecurity ");
   			}
   			sbQuery.append(" and sec.securityClass in (:lstSecurityClass) ");
   			
   	    	Query query = em.createQuery(sbQuery.toString());    	    	
   			if(Validations.validateIsNotNullAndNotEmpty(securityTO.getExpirationDate())){
   				query.setParameter("expirationDate",  securityTO.getExpirationDate() );
   			}
   			if(Validations.validateIsNotNullAndNotEmpty(securityTO.getStateSecurity())){
   				query.setParameter("stateSecurity",  securityTO.getStateSecurity() );
   			}
   			List<Integer> lstSecurityClass= new ArrayList<Integer>();
   			lstSecurityClass.add(SecurityClassType.DPA.getCode());
   			lstSecurityClass.add(SecurityClassType.DPF.getCode());
   			query.setParameter("lstSecurityClass",  lstSecurityClass);   			
   			
   			return  ((List<String>)query.getResultList());
   			
   		} catch(NoResultException ex){
   			return null;
   		}   		
   	}
   	
   	
   	
    
}
