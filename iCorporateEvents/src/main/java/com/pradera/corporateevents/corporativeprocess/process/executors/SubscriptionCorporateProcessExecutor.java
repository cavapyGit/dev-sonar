package com.pradera.corporateevents.corporativeprocess.process.executors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.corporativeprocess.process.CorporateExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcess;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.process.fixers.BalanceFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.ChangesFixer;
import com.pradera.corporateevents.corporativeprocess.process.fixers.DocumentsFixer;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.blockoperation.UnblockMarketFactOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.type.BlockMarketFactOperationStateType;
import com.pradera.model.custody.type.UnblockMarketOperationStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SubscriptionCorporateProcessExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/06/2014
 */
@CorporateProcess(CorporateExecutor.SUBSCRIPTION)
@Performance
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@LocalBean
public class SubscriptionCorporateProcessExecutor implements CorporateProcessExecutor {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The fixer. */
	@Inject
	private BalanceFixer fixer;
	
	/** The document fixer. */
	@Inject
	private DocumentsFixer documentFixer;
		
	/** The changes fixer. */
	@Inject
	private ChangesFixer changesFixer;
	
	/** The executor component service bean. */
	@Inject
	private Instance<AccountsComponentService> executorComponentServiceBean;
	
	/** The securities component service. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentService;
	
	/** The sc. */
	@Resource
    SessionContext sc;
	
	/** The subscription corporate process executor. */
	SubscriptionCorporateProcessExecutor subscriptionCorporateProcessExecutor;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
    public void init(){
        this.subscriptionCorporateProcessExecutor = this.sc.getBusinessObject(SubscriptionCorporateProcessExecutor.class);
    }
	

	/**
	 * This method is used execute the subscription preliminary process execution.
	 *
	 * @param corpOperation : CorporativeOperation details
	 * @return boolean value, if the process is executed successfully returns true else false
	 * @throws Exception the exception
	 */
	@Override
	public boolean executePreliminarProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started Subscription preliminary process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		boolean flag = false;
		Long stockProcess = null;
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		stockProcess = corporateExecutorService.getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
				corpOperation.getSecurities().getIdSecurityCodePk()
				, StockStateType.FINISHED.getCode(), StockClassType.NORMAL.getCode(), StockType.CORPORATIVE.getCode());
		if ((ImportanceEventType.PREFERRED_SUSCRIPTION.equals(processType) 
				&& stockProcess != null && stockProcess > 0) || ImportanceEventType.END_PREFERRED_SUSCRIPTION.equals(processType)) {
				corporateExecutorService.deletePreviousPreliminaryInformation(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, true, false);
			BigDecimal delFactor = CorporateProcessUtils.divide(corpOperation.getDeliveryFactor(), CorporateProcessConstants.AMOUNT_ROUND);
			List<Object[]> distinctHolders  = null;
			Security targetSecurity = null;
			Integer indReporting = corpOperation.getSecurities().getIndPaymentReporting();
			if(ImportanceEventType.PREFERRED_SUSCRIPTION.equals(processType)){
				distinctHolders = corporateExecutorService.getDistinctHoldersForProcess(stockProcess);
				targetSecurity = corporateExecutorService.getTargetSecurity(corpOperation.getIdCorporativeOperationPk());
			}else{
				distinctHolders = corporateExecutorService.getDistinctBalanceHoldersForProcess(corpOperation.getSecurities().getIdSecurityCodePk());
			}
			
			if (distinctHolders != null && distinctHolders.size() > 0) {
				this.subscriptionCorporateProcessExecutor.executePreliminarProcessAlgorithm(distinctHolders, processType, loggerUser, delFactor,
						corpOperation, targetSecurity, stockProcess , indReporting);
				flag = true;
			}
		}
		
		return flag;
	}
	
	/**
	 * Execute preliminar process algorithm.
	 *
	 * @param distinctHolders the distinct holders
	 * @param processType the process type
	 * @param loggerUser the logger user
	 * @param delFactor the del factor
	 * @param corpOperation the corp operation
	 * @param targetSecurity the target security
	 * @param stockProcess the stock process
	 * @param indReporting the ind reporting
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executePreliminarProcessAlgorithm(List<Object[]> distinctHolders,ImportanceEventType processType ,LoggerUser loggerUser
			,BigDecimal delFactor,CorporativeOperation corpOperation,Security targetSecurity,Long stockProcess,Integer indReporting ) throws ServiceException{
		CorporativeProcessResult totalProcessResult = null;
		List<BlockCorporativeResult> blockDocuments = null;
		Map<String, Object[]> holderAccAdjustments = null;
		Map<Long, Object[]> uniParticipants = null;
		Map<Long, List<Object[]>> changeOwnership = null;
		List<CorporativeProcessResult> changedResults = new ArrayList<CorporativeProcessResult>();
		if(ImportanceEventType.PREFERRED_SUSCRIPTION.equals(processType)){
			holderAccAdjustments = corporateExecutorService.getHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, CorporateProcessConstants.IND_YES,false);
			uniParticipants = corporateExecutorService.getUnifiedParticipants(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getRegistryDate());
			changeOwnership = corporateExecutorService.getChangeofOwnerShipSucession(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getRegistryDate());
		}
		
		for (Object[] arrObj : distinctHolders) {
			totalProcessResult = new CorporativeProcessResult();
			String holderAlternateCode = (String) arrObj[0];
			BigDecimal totalBalance = ((BigDecimal) arrObj[1]);
			BigDecimal roundedTotal = CorporateProcessUtils.calculateByFactor(totalBalance, delFactor, 
					corpOperation.getIndRound());
			totalProcessResult.setTotalBalance(roundedTotal);
			totalProcessResult.setCorporativeOperation(corpOperation);
			boolean blDestiny = false;
			Security sourceSecurity = null;
			if(ImportanceEventType.END_PREFERRED_SUSCRIPTION.equals(processType)){
				totalProcessResult.setSecurity(corpOperation.getSecurities());
				blDestiny = false;
			}else{
				sourceSecurity = corpOperation.getSecurities();
				corpOperation.setSecurities(targetSecurity);
				totalProcessResult.setSecurity(targetSecurity);
				blDestiny = true;
			}
			
			List<Object[]> holderAccounts =null;
			if(ImportanceEventType.PREFERRED_SUSCRIPTION.equals(processType)){
				holderAccounts = corporateExecutorService.getStockCalculationBalances(stockProcess, holderAlternateCode);
			}else{
				holderAccounts = corporateExecutorService.getBalancesForHolder(corpOperation.getSecurities().getIdSecurityCodePk(), holderAlternateCode);
			}
		
			if (holderAccounts != null && holderAccounts.size() > 0) {
				List<CorporativeProcessResult> holderAccResults = fixer.fixBalances(holderAccounts, corpOperation, delFactor, 
						corpOperation.getIndRound(), false, false, blDestiny, processType,null);
				if(ImportanceEventType.PREFERRED_SUSCRIPTION.equals(processType)){
					corpOperation.setSecurities(sourceSecurity);
				}
				BigDecimal tmpDelFactor = null;
				for (Iterator<CorporativeProcessResult> itHolder=holderAccResults.iterator(); itHolder.hasNext();) {
					CorporativeProcessResult corpProcessResults =  itHolder.next();
					
					tmpDelFactor = CorporateProcessUtils.getHolderAdjustmentFactor(holderAccAdjustments, corpProcessResults);
					if(tmpDelFactor == null){
						tmpDelFactor = delFactor;
					}
					if(CorporateProcessUtils.hasBlockBalances(corpProcessResults)){
						List<Object[]> blockedDocs = null;
						if(ImportanceEventType.PREFERRED_SUSCRIPTION.equals(processType)){
							blockedDocs =corporateExecutorService.getBlockStockCalculationDetails(stockProcess, corpProcessResults.getParticipant().getIdParticipantPk(), 
									corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
									corpOperation.getSecurities().getIdSecurityCodePk());
						}else{
							blockedDocs = corporateExecutorService.getBlockBalanceDetails(corpProcessResults.getParticipant().getIdParticipantPk(), 
									corpProcessResults.getHolderAccount().getIdHolderAccountPk(), 
									corpOperation.getSecurities().getIdSecurityCodePk(),AffectationStateType.BLOCKED.getCode(), null);
						}
						
						if(blockedDocs != null && blockedDocs.size() > 0){
							blockDocuments = documentFixer.fixDocuments(corpProcessResults, blockedDocs, tmpDelFactor,
									corpOperation.getIndRound(), false, false, blDestiny, fixer, processType,null);
							corpProcessResults.setBlockCorporativeResults(blockDocuments);
						}
					}
					corpProcessResults.setAvailableBalance(corpProcessResults.getAvailableBalance().add(corpProcessResults.getMarginBalance()));
					if(indReporting.equals(BooleanType.NO.getCode())){
						corpProcessResults.setAvailableBalance(corpProcessResults.getAvailableBalance().add(corpProcessResults.getReportedBalance()));
						corpProcessResults.setTotalBalance(corpProcessResults.getTotalBalance().add(corpProcessResults.getReportedBalance()));
						corpProcessResults.setTotalBalance(corpProcessResults.getTotalBalance().subtract(corpProcessResults.getReportingBalance()));
					}
					
					List<CorporativeProcessResult> changesCorp = null;
					if(ImportanceEventType.PREFERRED_SUSCRIPTION.equals(processType)){
						changesCorp = changesFixer.getUpdatedResult(new ArrayList<CorporativeProcessResult>(), corpProcessResults, 
							loggerUser, uniParticipants, changeOwnership, documentFixer, fixer, corpOperation.getIndRound(), false, null);
						corpProcessResults = documentFixer.updateBlockDocumentsEffections(corpProcessResults, false);
					}
					if(changesCorp != null && !changesCorp.isEmpty()){
						changedResults.addAll(changesCorp);
					}else{
						corporateExecutorService.create(corpProcessResults, loggerUser);
					}
					
				}
				holderAccounts.clear();
				holderAccounts = null;								
			}
		}
		if(!changedResults.isEmpty()){
			for(CorporativeProcessResult corpRes : changedResults ){
				if(corpRes.getTotalBalance().compareTo(BigDecimal.ZERO) > 0 || corpRes.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
					documentFixer.updateBlockDocumentsEffections(corpRes, false);							
					corporateExecutorService.updateOrCreate(corpRes, loggerUser);
				}
			}
			
			corporateExecutorService.updateBlockCorporateForResult(corpOperation.getIdCorporativeOperationPk(), loggerUser);
		}
		corporateExecutorService.updateHolderAccountAdjustments(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO, CorporateProcessConstants.IND_YES, CorporateProcessConstants.IND_YES, loggerUser);
		
		corpOperation.setIdStockCalculation(stockProcess);
		corporateExecutorService.updateCorporativeStockCalculation(corpOperation);
	}

	/* (non-Javadoc)
	 * @see com.pradera.corporateevents.corporativeprocess.process.CorporateProcessExecutor#executeDefinitiveProcess(com.pradera.model.corporatives.CorporativeOperation)
	 */
	@Override
	public boolean executeDefinitiveProcess(CorporativeOperation corpOperation) throws Exception{
		log.info("started definitive process");
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		ImportanceEventType processType = ImportanceEventType.get(corpOperation.getCorporativeEventType().getCorporativeEventType());
		Integer originTarget = null;
		Security targetSecurity = null;
		if(ImportanceEventType.END_PREFERRED_SUSCRIPTION.equals(processType)){
			originTarget = CorporateProcessConstants.BALANCE_ORIGIN;
		}else{
			originTarget = CorporateProcessConstants.BALANCE_DESTINY;
			targetSecurity = corporateExecutorService.getTargetSecurity(corpOperation.getIdCorporativeOperationPk()); 
		}
		List<Object> participants = corporateExecutorService.getDistinctParticpantsForDefinitive(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		if(participants !=null && !participants.isEmpty()){
			for(Object part : participants){
				Long participant = (Long)part;
				//new Transaction for Participant
				this.subscriptionCorporateProcessExecutor.executeDefinitiveProcessAccount(corpOperation, processType, participant,
						originTarget, targetSecurity, loggerUser);
			}
		}
		BigDecimal totalBalance =(BigDecimal) corporateExecutorService.getTotalBalanceForCorporateProcess(corpOperation.getIdCorporativeOperationPk(), 
				CorporateProcessConstants.IND_NO_REMAINDER,
				CorporateProcessConstants.DEFINITIVE_PROCESSED, originTarget, null);
		if(totalBalance != null && totalBalance.compareTo(BigDecimal.ZERO) > 0){
			//new Transaction
			this.subscriptionCorporateProcessExecutor.executeDefiniteveProcessSecurities(corpOperation, processType, totalBalance,
					targetSecurity, loggerUser, originTarget);
		}
		
		return true;
	}
	
	/**
	 * Execute definiteve process securities.
	 *
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param totalBalance the total balance
	 * @param targetSecurity the target security
	 * @param loggerUser the logger user
	 * @param originTarget the origin target
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefiniteveProcessSecurities(CorporativeOperation corpOperation,ImportanceEventType processType,BigDecimal totalBalance,
			Security targetSecurity,LoggerUser loggerUser,Integer originTarget) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		BigDecimal circulation = null;
		BigDecimal capital = null;
		BigDecimal physical = null;
		BigDecimal demtAmount = null;
		BigDecimal shareBalance = null;
		String isin = null;
		Security security=null;
		if(ImportanceEventType.END_PREFERRED_SUSCRIPTION.equals(processType)){
			circulation = corpOperation.getSecurities().getCirculationBalance();
			capital = corpOperation.getSecurities().getShareCapital();
			physical = circulation.subtract(totalBalance);
			demtAmount = corpOperation.getSecurities().getDesmaterializedBalance();
			shareBalance = corpOperation.getSecurities().getShareBalance();
			isin = corpOperation.getSecurities().getIdSecurityCodePk();
			security = corpOperation.getSecurities();
		}else{
			circulation = totalBalance;
			shareBalance = totalBalance;
			physical = circulation.subtract(totalBalance);
			demtAmount = totalBalance;
			capital = totalBalance.multiply(targetSecurity.getCurrentNominalValue());
			isin = targetSecurity.getIdSecurityCodePk();
			security = targetSecurity;
		}
		
		SecuritiesOperation secOpe = CorporateProcessUtils.getSecuritiesOperation(corpOperation, processType, capital, circulation, isin);
		secOpe.setSecurities(security);
		corporateExecutorService.create(secOpe, loggerUser);	
		SecuritiesComponentTO securitiesComponent = CorporateProcessUtils.getSecuritiesComponent(processType, secOpe,physical, demtAmount, originTarget);
		securitiesComponent.setShareBalance(shareBalance);
		securitiesComponent.setCirculationBalance(circulation);
		securitiesComponentService.get().executeSecuritiesComponent(securitiesComponent);
		if(ImportanceEventType.END_PREFERRED_SUSCRIPTION.equals(processType)){
			corporateExecutorService.updateSecurity(isin, null, 
					SecurityStateType.EXPIRED.getCode(), null, BigDecimal.ZERO, null,loggerUser);
		}
	}
	
	/**
	 * Execute definitive process account.
	 *
	 * @param corpOperation the corp operation
	 * @param processType the process type
	 * @param participant the participant
	 * @param originTarget the origin target
	 * @param targetSecurity the target security
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void executeDefinitiveProcessAccount(CorporativeOperation corpOperation,ImportanceEventType processType,Long participant
			,Integer originTarget,Security targetSecurity,LoggerUser loggerUser) throws ServiceException{
		fixer.setLoggerUserForTransationRegistry();
		//Get reference to component 
		AccountsComponentService accountsComponentService = executorComponentServiceBean.get();
		if(ImportanceEventType.END_PREFERRED_SUSCRIPTION.equals(processType)){
			List<Object[]> blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForUnblockDefinitive(corpOperation.getIdCorporativeOperationPk(), participant, corpOperation.getSecurities().getIdSecurityCodePk(), 
					CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, originTarget);
			if(blockDocuments != null && !blockDocuments.isEmpty()){
				for(Object[] data : blockDocuments){
					BigDecimal quantity = (BigDecimal)data[2];
					Long blockOperationId = (Long)data[3];
					HolderAccountBalanceTO account = new HolderAccountBalanceTO();
					account.setIdHolderAccount((Long)data[5]);
					account.setIdParticipant((Long)data[6]);
					account.setIdSecurityCode((String)data[7]);
					account.setStockQuantity(quantity);
					account.setFinalDate(corpOperation.getDeliveryDate());
					List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
					accounts.add(account);
					BlockOperation blockOperation = corporateExecutorService.find(BlockOperation.class, blockOperationId);
					List<BlockMarketFactOperation> blockMarketFatcs = blockOperation.getBlockMarketFactOperations();
					UnblockRequest unblockRequest = documentFixer.prepareUnblockOperation(data, loggerUser.getUserName());
					if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
						account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
						//movements marketfact for each blockmarketfact
						MarketFactAccountTO mktFact;
						for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
							if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
								mktFact = new MarketFactAccountTO();
								mktFact.setMarketDate(blockMarketFact.getMarketDate());
								mktFact.setMarketPrice(blockMarketFact.getMarketPrice());
								mktFact.setMarketRate(blockMarketFact.getMarketRate());
								mktFact.setQuantity(blockMarketFact.getActualBlockBalance());
								account.getLstMarketFactAccounts().add(mktFact);
							}
						}
						UnblockMarketFactOperation unblockMarketFact;
						for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
							if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
								unblockMarketFact = new UnblockMarketFactOperation();
								unblockMarketFact.setUnblockOperation(unblockRequest.getUnblockOperation().get(0));
								unblockMarketFact.setUnblockQuantity(blockMarketFact.getActualBlockBalance());
								unblockMarketFact.setUnblockMarketState(UnblockMarketOperationStateType.REGISTER.getCode());
								unblockMarketFact.setRegistryUser(loggerUser.getUserName());
								unblockMarketFact.setRegistryDate(CommonsUtilities.currentDateTime());
								unblockMarketFact.setMarketDate(blockMarketFact.getMarketDate());
								unblockMarketFact.setMarketPrice(blockMarketFact.getMarketPrice());
								unblockMarketFact.setMarketRate(blockMarketFact.getMarketRate());
								unblockRequest.getUnblockOperation().get(0).getUnblockMarketFactOperations().add(unblockMarketFact);
							}
						}
					}
					corporateExecutorService.create(unblockRequest, loggerUser);
					corporateExecutorService.updateBlockOperation(blockOperationId, quantity, true, true, AffectationStateType.UNBLOCKED.getCode(), null, loggerUser);
					if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
						for(BlockMarketFactOperation blockMarketFact : blockMarketFatcs){
							if(blockMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
								blockMarketFact.setActualBlockBalance(new BigDecimal(0));
								corporateExecutorService.update(blockMarketFact);
							}
						}
					}
					//movements unblock origin balances
					AccountsComponentTO accountsComponentTO = CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
							unblockRequest.getUnblockOperation().get(0).getIdUnblockOperationPk(), blockOperationId, null, null, processType, accounts, null, CorporateProcessUtils.getBlockBalance((Integer)data[0],null),idepositarySetup.getIndMarketFact());
					//change default operationType to unblockOperationType
					accountsComponentTO.setIdOperationType(CorporateProcessUtils.getOperationUnblock(CorporateProcessUtils.getBlockBalance((Integer)data[0],null), accountsComponentTO.getIdOperationType()));
					accountsComponentService.executeAccountsComponent(accountsComponentTO);
				}
				
			}
		}
		//execute movements in accounts origin(subs) and destiny(add)
		accountsComponentService.executeAccountsComponent(fixer.generateAccountComponent(
				corporateExecutorService.getCorporateResultsForDefinitiveProcess(corpOperation.getIdCorporativeOperationPk(), participant, 
						CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null), processType, corpOperation));
		
		if(ImportanceEventType.PREFERRED_SUSCRIPTION.equals(processType)){
			List<Object[]> blockDocuments = corporateExecutorService.getBlockCorpProcessResultsForDefinitive(corpOperation.getIdCorporativeOperationPk(), 
					participant, targetSecurity.getIdSecurityCodePk(), CorporateProcessConstants.IND_NO_REMAINDER, 
					CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, originTarget);
			if(blockDocuments != null && !blockDocuments.isEmpty()){
				for(Object[] data : blockDocuments){
					BigDecimal quantity = (BigDecimal)data[22];
					BlockRequest blockRequest = documentFixer.prepareBlockOperation(data, loggerUser.getUserName(), quantity, quantity);
					 HolderAccountBalanceTO account = new HolderAccountBalanceTO();
					 account.setIdHolderAccount((Long)data[12]);
					 account.setIdParticipant((Long)data[10]);
					 account.setIdSecurityCode((String)data[11]);
					 account.setStockQuantity(quantity);
					 List<HolderAccountBalanceTO> accounts = new ArrayList<HolderAccountBalanceTO>();
					 accounts.add(account);
					 if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
							BlockMarketFactOperation blockMarketFactOperation = new BlockMarketFactOperation();
							blockMarketFactOperation.setMarketDate(corpOperation.getMarketDate());
							blockMarketFactOperation.setMarketPrice(corpOperation.getMarketPrice());
							blockMarketFactOperation.setOriginalBlockBalance(quantity);
							blockMarketFactOperation.setActualBlockBalance(quantity);
							blockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
							blockMarketFactOperation.setBlockOperation(blockRequest.getBlockOperation().get(0));
							blockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
							blockMarketFactOperation.setRegistryUser(loggerUser.getUserName());
							blockRequest.getBlockOperation().get(0).getBlockMarketFactOperations().add(blockMarketFactOperation);
							//marketfact movement
							//movements marketfact
							MarketFactAccountTO mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(corpOperation.getMarketDate());
							mktFact.setMarketPrice(corpOperation.getMarketPrice());
							mktFact.setQuantity(quantity);
							account.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
							account.getLstMarketFactAccounts().add(mktFact);
					}
					corporateExecutorService.create(blockRequest, loggerUser);
					accountsComponentService.executeAccountsComponent(CorporateProcessUtils.getAccountsComponentTO(corpOperation.getIdCorporativeOperationPk(),
							blockRequest.getBlockOperation().get(0).getIdBlockOperationPk(), null, null, null, processType, null, accounts, CorporateProcessUtils.getBlockBalance((Integer)data[0], null),idepositarySetup.getIndMarketFact()));
				}
			}
		}
		
		corporateExecutorService.updateCorporateResultsForFinishingDefinitive(corpOperation.getIdCorporativeOperationPk(), 
				participant, true, true, CorporateProcessConstants.IND_NO_REMAINDER, CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED, null, loggerUser);
		
	}
	
}
