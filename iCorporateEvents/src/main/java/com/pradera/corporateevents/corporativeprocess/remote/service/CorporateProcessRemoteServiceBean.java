package com.pradera.corporateevents.corporativeprocess.remote.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.AsynchronousLoggerInterceptor;
import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CorporateExecutorServiceBean;
import com.pradera.corporateevents.corporativeprocess.service.CorporateProcessServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService;
import com.pradera.integration.component.corporatives.to.CorporateInformationTO;
import com.pradera.integration.component.corporatives.to.InterestCouponPaymentTO;
import com.pradera.integration.component.corporatives.to.InterestPaymentQueryTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeSituationType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporateProcessRemoteServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class CorporateProcessRemoteServiceBean extends CrudDaoServiceBean implements CorporateProcessRemoteService {

	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The corporate process service. */
	@EJB
	CorporateProcessServiceBean corporateProcessService;
	
	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService#payInterestCoupon(com.pradera.integration.component.corporatives.to.CorporateInformationTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> payInterestCoupon(CorporateInformationTO corporateInformationTO) throws Exception {
		logger.info(":::: payInterestCoupon ::::");
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(Validations.validateIsNull(loggerUser.getIdPrivilegeOfSystem())){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDefinitive());
		}
		Integer transactionState= BooleanType.NO.getCode();
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		List<CorporativeOperation> lstCorporativeOperations= new ArrayList<CorporativeOperation>();
		try {
			lstRecordValidationTypes= corporateProcessService.validateInterestPaymentBusiness(corporateInformationTO, lstCorporativeOperations);
			
			if(Validations.validateListIsNullOrEmpty(lstRecordValidationTypes))	{	
				InterestCouponPaymentTO interestCouponPaymentTO= new InterestCouponPaymentTO();
				List<CorporateInformationTO> lstCorporateInformationTOs= new ArrayList<CorporateInformationTO>();
				for (CorporativeOperation objCorporativeOperation: lstCorporativeOperations) {
					corporateProcessService.payDpfInterestCoupon(corporateInformationTO, objCorporativeOperation, loggerUser);
					corporateInformationTO.setIdCorporativeOperation(objCorporativeOperation.getIdCorporativeOperationPk());
					transactionState= BooleanType.YES.getCode();
					interfaceComponentServiceBean.get().saveInterfaceTransaction(corporateInformationTO, transactionState, loggerUser);
					
					CorporateInformationTO corporateInformationTOClone= corporateInformationTO.clone();
					corporateInformationTOClone.setSecurityObjectTO(corporateInformationTO.getSecurityObjectTO().clone());
					corporateInformationTOClone.getSecurityObjectTO().setCouponNumberRef(objCorporativeOperation.getProgramInterestCoupon().getCouponNumber());
					RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(corporateInformationTOClone, 
																			GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
					corporateInformationTOClone.setErrorCode(objRecordValidationType.getErrorCode());
					corporateInformationTOClone.setErrorDescription(objRecordValidationType.getErrorDescription());
					lstCorporateInformationTOs.add(corporateInformationTOClone);
				}
				interestCouponPaymentTO.setLstCorporateInformationTO(lstCorporateInformationTOs);
				RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(interestCouponPaymentTO, 
																			GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
			} else {
				transactionState= BooleanType.NO.getCode();
				interfaceComponentServiceBean.get().saveInterfaceTransaction(corporateInformationTO, transactionState, loggerUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
			transactionState= BooleanType.NO.getCode();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(corporateInformationTO, transactionState, loggerUser);
			throw e;
		}
		return lstRecordValidationTypes;
	}

	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService#reversePaymentInterestCoupon(com.pradera.integration.component.custody.to.ReverseRegisterTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> reversePaymentInterestCoupon(ReverseRegisterTO reverseRegisterTO) throws ServiceException {
		logger.info(":::: reversePaymentInterestCoupon ::::");
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDefinitive());
		}
		Integer transactionState= BooleanType.NO.getCode();
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		CorporativeOperation objCorporativeOperation = new CorporativeOperation();
		try {
			lstRecordValidationTypes= corporateProcessService.validateReverseInterestPaymentBusiness(reverseRegisterTO, objCorporativeOperation);
			
			if(Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)){
				//verificamos si es ultimo cupon de interes
//				boolean LastInterestCoupon = corporateProcessService.isLastInterestCoupon(
//																		reverseRegisterTO.getSecurityObjectTO().getIdSecurityCode(),true);
				//Actualizamos estado y situacion del corporativo de interes 
				corporateProcessService.updateCorporativeOperationState(objCorporativeOperation.getIdCorporativeOperationPk(),
						CorporateProcessStateType.REGISTERED.getCode(), loggerUser, false, CorporativeSituationType.ISSUER.getCode());
				//Actualizamos estado y situacion del cupon de interes
				corporateProcessService.updateProgramInterestCoupon(objCorporativeOperation.getProgramInterestCoupon().getIdProgramInterestPk(),
																	ProgramScheduleStateType.PENDING.getCode() , 
																	ProgramScheduleSituationType.PENDING.getCode(), loggerUser);
				//si es el ultimo de cupon de interes entonces amortizamos
//				if(LastInterestCoupon){
//					// obtenemos corporativo de amortizacion asociado al valor
//					List<CorporativeOperation> lstProcessesAux = corporateProcessService.getListCorporativeOperationAmortization(
//																		reverseRegisterTO.getSecurityObjectTO().getIdSecurityCode(),true);
//					if(Validations.validateListIsNotNullAndNotEmpty(lstProcessesAux)){
//						// obtenemos corporativo de amortizacion asociado al valor
//						CorporativeOperation objCorporativeOperationAux = (CorporativeOperation)lstProcessesAux.get(0);
//						//verificamos si el valor tiene cuenta matriz con saldos bloqueados o de reporto
//						boolean verifyExistBalanceAll = corporateProcessService.verifyExistBalanceAll(
//																		reverseRegisterTO.getSecurityObjectTO().getIdSecurityCode(), true);
//						if(!verifyExistBalanceAll){
//							//generamos de movimientos de saldos a nivel de cuenta matriz
//							ImportanceEventType processType = ImportanceEventType.get(objCorporativeOperationAux.getCorporativeEventType().getCorporativeEventType());
//							corporateProcessService.executeReverseAccountsComponentForCorporativeDPF(objCorporativeOperationAux,loggerUser, processType);	
//							//generamos de movimientos de saldos a nivel de valor
//							corporateProcessService.executeReverseSecurityComponentForCorporativeDPF(objCorporativeOperationAux, loggerUser, processType);
//							//Actualizamos estado y situacion del corporativo de Amortizacion 
//							corporateProcessService.updateCorporativeOperationState(objCorporativeOperationAux.getIdCorporativeOperationPk(),
//									CorporateProcessStateType.REGISTERED.getCode(), loggerUser, false, CorporativeSituationType.ISSUER.getCode());
//							//Actualizamos estado y situacion del cupon de Amortizacion
//							corporateProcessService.updateProgramAmortizationCoupon(objCorporativeOperationAux.getProgramAmortizationCoupon().getIdProgramAmortizationPk(),
//																					ProgramScheduleStateType.PENDING.getCode() , 
//																					ProgramScheduleSituationType.PENDING.getCode(), loggerUser);
//						}
//						//actualizamos estado del valor
//						corporateExecutorService.updateSecurity(objCorporativeOperationAux.getSecurities().getIdSecurityCodePk(), null, 
//								SecurityStateType.REGISTERED.getCode(), null, null ,null, loggerUser);
//					}
//				}
				RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				reverseRegisterTO.setIdCorporativeOperation(objCorporativeOperation.getIdCorporativeOperationPk());
				transactionState= BooleanType.YES.getCode();
			}
			interfaceComponentServiceBean.get().saveInterfaceTransaction(reverseRegisterTO, transactionState, loggerUser);
		} catch (Exception e) {
			e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, transactionState, loggerUser);
			throw e;
		}
		return lstRecordValidationTypes;
	}


	/* (non-Javadoc)
	 * @see com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService#getListDpfInterestPaymentCoupon(com.pradera.integration.component.corporatives.to.InterestPaymentQueryTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getListDpfInterestPaymentCoupon(InterestPaymentQueryTO interestPaymentQueryTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        interestPaymentQueryTO.setOperationNumber(interestPaymentQueryTO.getQueryNumber());
        List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
        try {
        	lstRecordValidationTypes= corporateProcessService.validateInterestPaymentQueryInformation(interestPaymentQueryTO);
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		interestPaymentQueryTO= corporateProcessService.getInterestPaymentQueryTO(interestPaymentQueryTO);
        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(interestPaymentQueryTO, 
										GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        	interfaceComponentServiceBean.get().saveInterfaceTransaction(interestPaymentQueryTO, transactionState, loggerUser);
        } catch (Exception e) {
        	e.printStackTrace();
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(interestPaymentQueryTO, transactionState, loggerUser);
        	throw e;
        }
		return lstRecordValidationTypes;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService#executeExpiredSecuritiesDpf(java.lang.String)
	 */
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public Object executeExpiredSecuritiesDpf(LoggerUser loggerUser, String isSecurityCodePk) throws ServiceException{			
		try {
			List<String> lstSecurity = new ArrayList<String>();
			lstSecurity.add(isSecurityCodePk);
			corporateProcessService.automaticExpiredSecuritiesDpf(CommonsUtilities.currentDate(), loggerUser, lstSecurity);			
		} catch(ServiceException e){			
			RecordValidationType recordError = CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_UNDEFINED_ERROR, null);
			recordError.setErrorDescription(PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS,"es", e.getMessage()));		
			return recordError;
		}
		return null;
	}
	
}
