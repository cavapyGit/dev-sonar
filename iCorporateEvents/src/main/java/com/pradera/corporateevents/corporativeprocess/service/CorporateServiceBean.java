package com.pradera.corporateevents.corporativeprocess.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.poi.ss.formula.functions.Value;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.monitoring.PerformanceAuditor;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporateMonitoringTO;
import com.pradera.corporateevents.corporativeprocess.view.to.CorporativeProcessDetailsTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.corporatives.BlockCorporativeResult;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.CorporativeEventType;
import com.pradera.model.corporatives.type.CorporativeSituationType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.changeownership.BlockChangeOwnership;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.type.ProcessLoggerStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class CorporateServiceBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCorporateEvents
 * @Creation_Date :
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class CorporateServiceBean extends CrudDaoServiceBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The corporate executor service. */
	@EJB
	CorporateExecutorServiceBean corporateExecutorService;
	
	
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;

	/**
	 * To get the CorporateOperacion data.
	 *
	 * @param corporativeFilter the CorporateMonitoringFilter
	 * @return the List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	@PerformanceAuditor
	public List<CorporativeOperation> searchCorporativeOperations(CorporateMonitoringTO corporativeFilter) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder();			
		Map<String, Object> parameters = new HashMap<String, Object>();
		String query = " select c from CorporativeOperation c INNER JOIN FETCH c.securities s INNER JOIN FETCH s.issuer INNER JOIN FETCH c.corporativeEventType cet" +
				"	LEFT JOIN FETCH c.retirementOperation";	
		if(Validations.validateIsNotNullAndPositive(corporativeFilter.getCurrency())){
			sbQuery.append(" and c.currency= :currency ");
			parameters.put("currency",corporativeFilter.getCurrency());
		}
		if (Validations.validateIsNotNull(corporativeFilter.getProcessDate())) {
			sbQuery.append(" and trunc(c.deliveryDate) = trunc(:deliveryDate)");
			parameters.put("deliveryDate",corporativeFilter.getProcessDate());
		}
		if (Validations.validateIsNotNull(corporativeFilter.getRegisterdDate())) {
			sbQuery.append(" and trunc(c.registryDate) = trunc(:registryDate)"); 
			parameters.put("registryDate",corporativeFilter.getRegisterdDate());
		}
		if (Validations.validateIsNotNull(corporativeFilter.getCutoffDate())) {
			sbQuery.append(" and trunc(c.cutoffDate) = trunc(:cutoffDate)");
			parameters.put("cutoffDate",corporativeFilter.getCutoffDate());	
		}
		if (Validations.validateIsNotNullAndPositive(corporativeFilter.getProcessType())){
			sbQuery.append(" and c.corporativeEventType.corporativeEventType= :corporativeEventType");
			parameters.put("corporativeEventType",corporativeFilter.getProcessType());	
		}
		
		if (Validations.validateIsNotNull(corporativeFilter.getLstCorporativeEvent())){
			sbQuery.append(" and c.corporativeEventType.corporativeEventType in ( :lstCorporativeEvent )");
			parameters.put("lstCorporativeEvent",corporativeFilter.getLstCorporativeEvent());	
		}
		
		if (Validations.validateIsNotNullAndPositive(corporativeFilter.getState())){
			sbQuery.append(" and c.state= :state ");
			parameters.put("state",corporativeFilter.getState());	
		}
		
		if(Validations.validateIsNotNull(corporativeFilter.getLstState())){
			sbQuery.append(" and c.state in (:lstState) ");
			parameters.put("lstState",corporativeFilter.getLstState());	
		}
		
		if (Validations.validateIsNotNull(corporativeFilter.getSecurities()) && Validations.validateIsNotNullAndNotEmpty(corporativeFilter.getSecurities().getIdSecurityCodePk())){
			sbQuery.append(" and c.securities.idSecurityCodePk= :idSecurityCodePk");
			parameters.put("idSecurityCodePk",corporativeFilter.getSecurities().getIdSecurityCodePk());
		}
		
		sbQuery.append(" and c.securities.securityClass  not in (:securityClassDPF,:securityClassDPA) ");
		parameters.put("securityClassDPF",SecurityClassType.DPF.getCode());
		parameters.put("securityClassDPA",SecurityClassType.DPA.getCode());
		
		if (Validations.validateIsNotNull(corporativeFilter.getIssuer()) && Validations.validateIsNotNullAndNotEmpty(corporativeFilter.getIssuer().getIdIssuerPk()) ){
			sbQuery.append(" and c.issuer.idIssuerPk= :idIssuerPk");
			parameters.put("idIssuerPk",corporativeFilter.getIssuer().getIdIssuerPk());
		}
		
		if(corporativeFilter.getDeliveryPlacement() != null && corporativeFilter.getDeliveryPlacementAux() != null){
			sbQuery.append(" and c.deliveryPlacement in (:deliveryPlacement , :deliveryPlacementAux)");
			parameters.put("deliveryPlacement", corporativeFilter.getDeliveryPlacement());
			parameters.put("deliveryPlacementAux", corporativeFilter.getDeliveryPlacementAux());
		}

		if(sbQuery.length() > 0){
			query = query + " where " + sbQuery.toString();
			query = query.replaceFirst(" and", "");
		}
		query = query+" order by cet.idCorporativeEventTypePk,  c.idCorporativeOperationPk desc";
		return findListByQueryString(query, parameters);
	}

	@SuppressWarnings("unchecked")
	@PerformanceAuditor
	public List<HolderAccountAdjusment> searchAdjusmentByCorporative(Long idCorporativeOperationPk, Integer state) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select haa from HolderAccountAdjusment haa ");
		sbQuery.append(" where haa.adjustmentState = :state");
		sbQuery.append(" and haa.corporativeOperation.idCorporativeOperationPk = :idCorporativeOperationPk");

		parameters.put("idCorporativeOperationPk", idCorporativeOperationPk);
		parameters.put("state", state);

		List<HolderAccountAdjusment> result = findListByQueryString(sbQuery.toString(), parameters);
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@PerformanceAuditor
	public AccountAnnotationOperation accountAnnotationOperationWithSecurity(String idSecurityCodePk) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select aao from AccountAnnotationOperation aao ");
		sbQuery.append(" where ");
		sbQuery.append(" aao.security.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append(" and aao.state not in (:state) ");
		sbQuery.append(" and aao.indImmobilization  = :indImmobilization ");
		sbQuery.append(" order by aao.idAnnotationOperationPk DESC ");

		parameters.put("idSecurityCodePk", idSecurityCodePk);
		parameters.put("indImmobilization", 1);
		List<Integer> stateNotIn = new ArrayList<Integer>();
		stateNotIn.add(DematerializationStateType.ANNULLED.getCode());
		stateNotIn.add(DematerializationStateType.RECTIFIED.getCode());
		stateNotIn.add(DematerializationStateType.CANCELLED.getCode());
		parameters.put("state", stateNotIn);
		
		try {
			List<AccountAnnotationOperation> result = findListByQueryString(sbQuery.toString(), parameters);	
			if(result!=null && result.size()>0) {
				return result.get(0);
			}
		} catch (Exception e) {
			return null;
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@PerformanceAuditor
	public AccountAnnotationCupon accountAnnotationCuponWithInteresProgram(Long idProgramInterestPk) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select c from AccountAnnotationCupon c ");
		sbQuery.append(" where ");
		sbQuery.append(" c.programInterestCoupon.idProgramInterestPk = :idProgramInterestPk ");
		sbQuery.append(" order by c.programInterestCoupon.idProgramInterestPk DESC ");

		parameters.put("idProgramInterestPk", idProgramInterestPk);

		try {
			List<AccountAnnotationCupon> result = findListByQueryString(sbQuery.toString(), parameters);	
			if(result!=null && result.size()>0) {
				return result.get(0);
			}
		} catch (Exception e) {
			return null;
		}
		
		return null;
	}
	
	/**
	 * To get the CorporateOperacion data. By Expiration Date to 
	 * Program Interest Coupon Or
	 * Program Amortization Coupon 
	 * For Preliminary
	 *
	 * @param corporativeFilter the CorporateMonitoringFilter
	 * @return the List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	@PerformanceAuditor
	public List<CorporativeOperation> searchCorporativeOperationsByCoupons(CorporateMonitoringTO corporativeFilter) throws ServiceException{

		StringBuilder sbQueryInteres = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQueryInteres.append(" SELECT distinct c from CorporativeOperation c ");
		sbQueryInteres.append("	INNER JOIN FETCH c.securities s ");
		sbQueryInteres.append("	INNER JOIN FETCH s.issuer ");
		sbQueryInteres.append("	INNER JOIN FETCH c.corporativeEventType cet");
		//sbQueryInteres.append("	LEFT JOIN FETCH c.retirementOperation ret");
		sbQueryInteres.append("	LEFT JOIN c.programInterestCoupon interest	 "); 
		sbQueryInteres.append("	LEFT JOIN c.programAmortizationCoupon amortiz ");
		sbQueryInteres.append("	");
		sbQueryInteres.append(" WHERE 1=1 ");
		
		sbQueryInteres.append(" and s.stateSecurity not in (:exclusive) ");
		parameters.put("exclusive",SecurityStateType.GUARDA_EXCLUSIVE.getCode());
		
		if(Validations.validateIsNotNullAndPositive(corporativeFilter.getCurrency())){
			sbQueryInteres.append(" and c.currency= :currency ");
			parameters.put("currency",corporativeFilter.getCurrency());
		}
		if (Validations.validateIsNotNull(corporativeFilter.getProcessDate())) {
			sbQueryInteres.append(" and trunc(c.deliveryDate) = trunc(:deliveryDate)");
			parameters.put("deliveryDate",corporativeFilter.getProcessDate());
		}
		if (Validations.validateListIsNotNullAndNotEmpty(corporativeFilter.getListExpirationDate())) {
			/*sbQueryInteres.append(" and  ");
			sbQueryInteres.append(" (  ");
			sbQueryInteres.append(" trunc(interest.experitationDate) in (:listExpirationDate) ");
			sbQueryInteres.append(" 	or   ");
			sbQueryInteres.append(" trunc(amortiz.expirationDate) in (:listExpirationDate)  ");
			sbQueryInteres.append(" )  ");*/
			sbQueryInteres.append(" and trunc(c.deliveryDate) in (:listExpirationDate) ");
			parameters.put("listExpirationDate",corporativeFilter.getListExpirationDate());
		}
		
		if (Validations.validateIsNotNull(corporativeFilter.getExpirationDate()) && Validations.validateIsNotNull(corporativeFilter.getExpirationDateFinal()) ) {
			sbQueryInteres.append(" and ");
			sbQueryInteres.append(" trunc(c.deliveryDate) between (:expirationDateInitial) and  (:expirationDateFinal)");
			
			parameters.put("expirationDateInitial",corporativeFilter.getExpirationDate());
			parameters.put("expirationDateFinal",corporativeFilter.getExpirationDateFinal());
		}
		if (Validations.validateIsNotNull(corporativeFilter.getRegisterdDate())) {
			sbQueryInteres.append(" and trunc(c.registryDate) = trunc(:registryDate)");
			parameters.put("registryDate",corporativeFilter.getRegisterdDate());
		}
		if (Validations.validateIsNotNull(corporativeFilter.getCutoffDate())) {
			sbQueryInteres.append(" and trunc(c.cutoffDate) = trunc(:cutoffDate)");
			parameters.put("cutoffDate",corporativeFilter.getCutoffDate());	
		}
		if (Validations.validateIsNotNullAndPositive(corporativeFilter.getProcessType())){
			sbQueryInteres.append(" and c.corporativeEventType.corporativeEventType= :corporativeEventType");
			parameters.put("corporativeEventType",corporativeFilter.getProcessType());	
		}
		
		if (Validations.validateIsNotNull(corporativeFilter.getLstCorporativeEvent())){
			sbQueryInteres.append(" and c.corporativeEventType.corporativeEventType in ( :lstCorporativeEvent )");
			parameters.put("lstCorporativeEvent",corporativeFilter.getLstCorporativeEvent());	
		}
		
		if (Validations.validateIsNotNullAndPositive(corporativeFilter.getState())){
			sbQueryInteres.append(" and c.state= :state ");
			parameters.put("state",corporativeFilter.getState());	
		}else{		
			/**Excluded Corporative Operation Annulled If not Exits Filter in Combo */
			sbQueryInteres.append(" and c.state != :corportiveAnull ");
			parameters.put("corportiveAnull",CorporateProcessStateType.ANNULLED.getCode());
		}
		
		if(Validations.validateIsNotNull(corporativeFilter.getLstState())){
			sbQueryInteres.append(" and c.state in (:lstState) ");
			parameters.put("lstState",corporativeFilter.getLstState());	
		}
		
		//nuevos filtros
		if(Validations.validateIsNotNull(corporativeFilter.getSecurityCurrency())){
			sbQueryInteres.append(" and c.currency = :securityCurrency ");
		}
		if(Validations.validateIsNotNull(corporativeFilter.getSecurityClass())){
			sbQueryInteres.append(" and s.securityClass = :securityClass ");
		}
		// fin de nuevos filtros
		
		if (Validations.validateIsNotNull(corporativeFilter.getSecurities()) && Validations.validateIsNotNullAndNotEmpty(corporativeFilter.getSecurities().getIdSecurityCodePk())){
			sbQueryInteres.append(" and c.securities.idSecurityCodePk= :idSecurityCodePk");
			parameters.put("idSecurityCodePk",corporativeFilter.getSecurities().getIdSecurityCodePk());
		}
	/*	
		sbQueryInteres.append(" and c.securities.securityClass  not in (:securityClassDPF,:securityClassDPA) ");
		parameters.put("securityClassDPF",SecurityClassType.DPF.getCode());
		parameters.put("securityClassDPA",SecurityClassType.DPA.getCode());
	 */
			
		if (Validations.validateIsNotNull(corporativeFilter.getIssuer()) && Validations.validateIsNotNullAndNotEmpty(corporativeFilter.getIssuer().getIdIssuerPk()) ){
			sbQueryInteres.append(" and c.issuer.idIssuerPk= :idIssuerPk");
			parameters.put("idIssuerPk",corporativeFilter.getIssuer().getIdIssuerPk());
		}
		
		if(corporativeFilter.getDeliveryPlacement() != null && corporativeFilter.getDeliveryPlacementAux() != null){
			sbQueryInteres.append(" and c.deliveryPlacement in (:deliveryPlacement , :deliveryPlacementAux)");
			parameters.put("deliveryPlacement", corporativeFilter.getDeliveryPlacement());
			parameters.put("deliveryPlacementAux", corporativeFilter.getDeliveryPlacementAux());
		}
		
		//nuevos filtros
		if(Validations.validateIsNotNull(corporativeFilter.getSecurityCurrency())){
			parameters.put("securityCurrency", corporativeFilter.getSecurityCurrency());
		}
		if(Validations.validateIsNotNull(corporativeFilter.getSecurityClass())){
			parameters.put("securityClass",corporativeFilter.getSecurityClass());
		}
		// fin de nuevos filtros

		sbQueryInteres.append(" order by cet.idCorporativeEventTypePk, s.idSecurityCodePk desc ");
		return findListByQueryString(sbQueryInteres.toString(), parameters);
		
	}
	
	/**
	 * This method is used to get the id of corporative operation whether if any remanent process is exists to execute or not.
	 *
	 * @param recordDate the record date
	 * @param cutoffDate the cutoff date
	 * @param isin the isin
	 * @return Long value
	 * @throws ServiceException if query execution got failed
	 */
	public Long getRemanentProcess(Date recordDate, Date cutoffDate, String isin)
			throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
		sbQuery.append(" where trunc(cp.registryDate) = trunc(:creationdate)");
		sbQuery.append(" and trunc(cp.cutoffDate) = trunc(:cutoffdate)");
		sbQuery.append(" and cp.securities.idSecurityCodePk = :securities");
		sbQuery.append(" and cp.state = :state");
		sbQuery.append(" and cp.indRound = :round");
		sbQuery.append(" and cp.corporativeEventType.corporativeEventType =  :proctype");

		parameters.put("creationdate", recordDate);
		parameters.put("cutoffdate", cutoffDate);
		parameters.put("securities", isin);
		parameters.put("state", CorporateProcessStateType.DEFINITIVE.getCode());
		parameters.put("round", 0);
		parameters.put("proctype", ImportanceEventType.ACTION_DIVIDENDS.getCode());

		List<Long> result = findListByQueryString(sbQuery.toString(), parameters);
		if(result != null && result.size() > 0){
			return result.get(0);
		}else{
			return null;
		}

	}	
	
	/**
	 * Gets the contribution return process.
	 *
	 * @param recordDate the record date
	 * @param cutoffDate the cutoff date
	 * @param isin the isin
	 * @param cortype the cortype
	 * @return the contribution return process
	 * @throws ServiceException the service exception
	 */
	public Long getContributionReturnProcess(Date recordDate, Date cutoffDate, String isin , Integer cortype)
			throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");
		sbQuery.append(" where trunc(cp.registryDate) = trunc(:creationdate)");
		sbQuery.append(" and trunc(cp.cutoffDate) = trunc(:cutoffdate)");
		sbQuery.append(" and cp.securities.idSecurityCodePk = :securities");
		sbQuery.append(" and cp.state = :state");
		sbQuery.append(" and cp.corporativeEventType.corporativeEventType =  :proctype");

		parameters.put("creationdate", recordDate);
		parameters.put("cutoffdate", cutoffDate);
		parameters.put("securities", isin);
		parameters.put("state", CorporateProcessStateType.DEFINITIVE.getCode());
		parameters.put("proctype", cortype);

		List<Long> result = findListByQueryString(sbQuery.toString(), parameters);
		if(result != null && result.size() > 0){
			return result.get(0);
		}else{
			return null;
		}

	}	
	/**
	 * Get the Details link data.
	 *
	 * @param corpOperation the corp operation
	 * @param remCorpOperation the rem corp operation
	 * @param originDestiny the origin destiny
	 * @param originDestiny1 the origin destiny1
	 * @param remnant the remnant
	 * @return The List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getRemnantCorporativeProcessDetails(Long corpOperation,Long remCorpOperation, Integer originDestiny,Integer originDestiny1, Integer remnant)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select p.mnemonic||' - '||p.idParticipantPk, substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), ");
		sbQuery.append(" ha.accountNumber, cprr.totalBalance, cprr.availableBalance, cpr.totalBalance, cpr.availableBalance, ");
		sbQuery.append(" cpr.taxAmount, cpr.custodyAmount, (cpr.totalBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))), ");
		sbQuery.append(" (cpr.availableBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))), ");
		sbQuery.append(" (coalesce(cpr.pawnBalance,0) + coalesce(cpr.banBalance,0)+ coalesce(cpr.otherBlockBalance,0)+ coalesce(cpr.reserveBalance,0)+ coalesce(cpr.oppositionBalance,0)), ");
		sbQuery.append(" coalesce((select (coalesce(haa.newBenefitAmount,0)+coalesce(haa.newCommissiones,0)+coalesce(haa.newTax,0)) ");
		sbQuery.append(" from HolderAccountAdjusment haa  where haa.corporativeOperation.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk");
		sbQuery.append(" and haa.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk and haa.adjustmentState = :hanoprocess ),0),0,0, cpr.indOriginTarget,0,0,0,0,0 ");
		sbQuery.append(" from CorporativeProcessResult cprr, HolderAccount ha, Participant p, ");
		sbQuery.append(" CorporativeProcessResult cpr ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cprr.corporativeOperation.idCorporativeOperationPk = :remidCorporateFk ");
		sbQuery.append(" and cprr.participant.idParticipantPk = cpr.participant.idParticipantPk ");			
		sbQuery.append(" and cprr.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk");
		sbQuery.append(" and cpr.participant.idParticipantPk = p.idParticipantPk");
		sbQuery.append(" and (cpr.indOriginTarget = :origin or cpr.indOriginTarget = :origin1) and cpr.indRemanent = :noremnant and cprr.indRemanent = :remnant");
		sbQuery.append(" order by 1, 2, 3");
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("remidCorporateFk", remCorpOperation);
		parameters.put("origin", originDestiny);	
		parameters.put("origin1", originDestiny1);
		parameters.put("noremnant", remnant);
		parameters.put("remnant", CorporateProcessConstants.IND_REMAINDER);
		parameters.put("hanoprocess", CorporateProcessConstants.IND_NO);

		List<Object[]> detailSearchResult=findListByQueryString(sbQuery.toString(), parameters);

		return detailSearchResult;		
	}
	
	/**
	 * Gets the contribution return corporative process details.
	 *
	 * @param corpOperation the corp operation
	 * @param originDestiny the origin destiny
	 * @param originDestiny1 the origin destiny1
	 * @param OperId the oper id
	 * @return the contribution return corporative process details
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getContributionReturnCorporativeProcessDetails(Long corpOperation, Integer originDestiny,Integer originDestiny1 , Long OperId)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select p.mnemonic||' - '||p.idParticipantPk, substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), ");
		sbQuery.append(" ha.accountNumber,  ");
		
		sbQuery.append(" (select ( ");
		sbQuery.append(" (SELECT cpraux1.totalBalance  FROM CorporativeProcessResult cpraux1 where cpraux1.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux1.indOriginTarget = 1 ");
		sbQuery.append(" and cpraux1.security.idSecurityCodePk = cpr.security.idSecurityCodePk  and cpraux1.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux1.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" - ");
		sbQuery.append(" (SELECT cpraux2.totalBalance  FROM CorporativeProcessResult cpraux2 where cpraux2.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux2.indOriginTarget = 2  ");
		sbQuery.append(" and cpraux2.security.idSecurityCodePk = cpr.security.idSecurityCodePk and cpraux2.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux2.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" ) from CorporativeProcessResult  where corporativeOperation.idCorporativeOperationPk = :OperId ");
		sbQuery.append(" group by corporativeOperation.idCorporativeOperationPk), ");
				
		sbQuery.append(" (select ( ");
		sbQuery.append(" (SELECT cpraux1.availableBalance  FROM CorporativeProcessResult cpraux1 where cpraux1.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux1.indOriginTarget = 1 ");
		sbQuery.append(" and cpraux1.security.idSecurityCodePk = cpr.security.idSecurityCodePk and cpraux1.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux1.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" - ");
		sbQuery.append(" (SELECT cpraux2.availableBalance  FROM CorporativeProcessResult cpraux2 where cpraux2.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux2.indOriginTarget = 2  ");
		sbQuery.append(" and cpraux2.security.idSecurityCodePk = cpr.security.idSecurityCodePk and cpraux2.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux2.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" ) from CorporativeProcessResult  where corporativeOperation.idCorporativeOperationPk = :OperId ");
		sbQuery.append(" group by corporativeOperation.idCorporativeOperationPk), ");
		
		sbQuery.append(" cpr.totalBalance, cpr.availableBalance, cpr.taxAmount, cpr.custodyAmount, (cpr.totalBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))), ");
		sbQuery.append(" (cpr.availableBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))), ");
		sbQuery.append(" (coalesce(cpr.pawnBalance,0) + coalesce(cpr.banBalance,0)+ coalesce(cpr.otherBlockBalance,0)+ coalesce(cpr.reserveBalance,0)+ coalesce(cpr.oppositionBalance,0)), ");
		sbQuery.append(" coalesce((select (coalesce(haa.newBenefitAmount,0)+coalesce(haa.newCommissiones,0)+coalesce(haa.newTax,0)) ");
		sbQuery.append(" from HolderAccountAdjusment haa  where haa.corporativeOperation.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk");
		sbQuery.append(" and haa.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk and haa.adjustmentState = :hanoprocess ),0),0,0, cpr.indOriginTarget, ");
				
		sbQuery.append(" (select ( ");
		sbQuery.append(" (SELECT (nvl(cpraux1.pawnBalance,0)+nvl(cpraux1.banBalance,0)+nvl(cpraux1.otherBlockBalance,0))  FROM CorporativeProcessResult cpraux1 where cpraux1.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux1.indOriginTarget = 1 ");
		sbQuery.append(" and cpraux1.security.idSecurityCodePk = cpr.security.idSecurityCodePk  and cpraux1.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux1.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" - ");
		sbQuery.append(" (SELECT (nvl(cpraux2.pawnBalance,0)+nvl(cpraux2.banBalance,0)+nvl(cpraux2.otherBlockBalance,0))  FROM CorporativeProcessResult cpraux2 where cpraux2.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux2.indOriginTarget = 2  ");
		sbQuery.append(" and cpraux2.security.idSecurityCodePk = cpr.security.idSecurityCodePk and cpraux2.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux2.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" ) from CorporativeProcessResult  where corporativeOperation.idCorporativeOperationPk = :OperId ");
		sbQuery.append(" group by corporativeOperation.idCorporativeOperationPk), ");
		
		sbQuery.append(" (select ( ");
		sbQuery.append(" (SELECT cpraux1.reportedBalance  FROM CorporativeProcessResult cpraux1 where cpraux1.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux1.indOriginTarget = 1 ");
		sbQuery.append(" and cpraux1.security.idSecurityCodePk = cpr.security.idSecurityCodePk and cpraux1.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux1.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" - ");
		sbQuery.append(" (SELECT cpraux2.reportedBalance  FROM CorporativeProcessResult cpraux2 where cpraux2.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux2.indOriginTarget = 2  ");
		sbQuery.append(" and cpraux2.security.idSecurityCodePk = cpr.security.idSecurityCodePk and cpraux2.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux2.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" ) from CorporativeProcessResult  where corporativeOperation.idCorporativeOperationPk = :OperId ");
		sbQuery.append(" group by corporativeOperation.idCorporativeOperationPk), ");
		
		sbQuery.append(" (select ( ");
		sbQuery.append(" (SELECT cpraux1.reportingBalance  FROM CorporativeProcessResult cpraux1 where cpraux1.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux1.indOriginTarget = 1 ");
		sbQuery.append(" and cpraux1.security.idSecurityCodePk = cpr.security.idSecurityCodePk and cpraux1.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux1.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" - ");
		sbQuery.append(" (SELECT cpraux2.reportingBalance  FROM CorporativeProcessResult cpraux2 where cpraux2.corporativeOperation.idCorporativeOperationPk = :OperId and cpraux2.indOriginTarget = 2  ");
		sbQuery.append(" and cpraux2.security.idSecurityCodePk = cpr.security.idSecurityCodePk and cpraux2.participant.idParticipantPk =cpr.participant.idParticipantPk and cpraux2.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk) ");
		sbQuery.append(" ) from CorporativeProcessResult  where corporativeOperation.idCorporativeOperationPk = :OperId ");
		sbQuery.append(" group by corporativeOperation.idCorporativeOperationPk), ");	
		
		sbQuery.append(" cpr.reportedBalance, cpr.reportingBalance from HolderAccount ha, Participant p, ");
		sbQuery.append(" CorporativeProcessResult cpr ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk");
		sbQuery.append(" and cpr.participant.idParticipantPk = p.idParticipantPk");
		sbQuery.append(" and (cpr.indOriginTarget = :origin or cpr.indOriginTarget = :origin1) ");
		sbQuery.append(" order by 1, 2, 3");
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("OperId", OperId);	
		parameters.put("origin", originDestiny);	
		parameters.put("origin1", originDestiny1);
		parameters.put("hanoprocess", CorporateProcessConstants.IND_NO);

		List<Object[]> detailSearchResult=findListByQueryString(sbQuery.toString(), parameters);

		return detailSearchResult;		
	}
	/**
	 * Get the Details link data.
	 *
	 * @param corpOperation the corp operation
	 * @param remnant the remnant
	 * @return The List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getNoDestinyCorporativeProcessDetails(Long corpOperation, Integer remnant)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select p.mnemonic||' - '||p.idParticipantPk, substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), ");
		sbQuery.append(" ha.accountNumber, cpro.totalBalance, cpro.availableBalance, 0, 0, ");
		sbQuery.append(" 0, 0, 0, ");
		sbQuery.append(" 0, 0, 0, 0, 0,0,0,0,0,0,0 ");
		sbQuery.append(" from CorporativeProcessResult cpro, HolderAccount ha, Participant p ");
		sbQuery.append(" where cpro.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cpro.holderAccount.idHolderAccountPk = ha.idHolderAccountPk");
		sbQuery.append(" and cpro.participant.idParticipantPk = p.idParticipantPk");
		sbQuery.append(" and cpro.indOriginTarget = :origin and cpro.indRemanent = :remnant ");
		sbQuery.append(" order by 1, 2, 3");
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("origin", CorporateProcessConstants.BALANCE_ORIGIN);	
		parameters.put("remnant", remnant);

		List<Object[]> detailSearchResult=findListByQueryString(sbQuery.toString(), parameters);

		return detailSearchResult;		
	}
	
	/**
	 * Get the Details link data.
	 *
	 * @param corpOperation the corp operation
	 * @param remnant the remnant
	 * @param destinySecurity the destiny security
	 * @return The List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getSpecialCorporativeProcessDetails(Long corpOperation, Integer remnant, String destinySecurity)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select p.mnemonic||' - '||p.idParticipantPk, substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), ");
		sbQuery.append(" ha.accountNumber, cpro.totalBalance, cpro.availableBalance, cprd.totalBalance, cprd.availableBalance, ");
		sbQuery.append(" 0, 0, 0, 0, (coalesce(cprd.pawnBalance,0) + coalesce(cprd.banBalance,0)+ coalesce(cprd.otherBlockBalance,0)+ coalesce(cprd.reserveBalance,0)+ coalesce(cprd.oppositionBalance,0)), ");
		sbQuery.append(" coalesce((select (coalesce(haa.newBenefitAmount,0)) ");
		sbQuery.append(" from HolderAccountAdjusment haa  where haa.corporativeOperation.idCorporativeOperationPk = cprd.corporativeOperation.idCorporativeOperationPk");
		sbQuery.append(" and haa.holderAccount.idHolderAccountPk = cprd.holderAccount.idHolderAccountPk and haa.adjustmentState = :hanoprocess ");
		if(destinySecurity != null && destinySecurity.length() > 0){
			sbQuery.append(" and haa.security.idSecurityCodePk = :desisin ");
		}
		sbQuery.append(" ),0),0,0,0,0,0,0,0,0 from CorporativeProcessResult cpro, HolderAccount ha, Participant p, ");
		sbQuery.append(" CorporativeProcessResult cprd ");
		sbQuery.append(" where cpro.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cprd.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cpro.participant.idParticipantPk = cprd.participant.idParticipantPk ");			
		sbQuery.append(" and cpro.holderAccount.idHolderAccountPk = cprd.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and cpro.holderAccount.idHolderAccountPk = ha.idHolderAccountPk");
		sbQuery.append(" and cpro.participant.idParticipantPk = p.idParticipantPk and cprd.indRemanent = :remnant");
		sbQuery.append(" and cpro.indOriginTarget = :origin and cpro.indRemanent = :remnant and cprd.indOriginTarget = :destiny");
		if(destinySecurity != null && destinySecurity.length() > 0){
			sbQuery.append(" and cprd.security.idSecurityCodePk = :desisin");
			parameters.put("desisin", destinySecurity);	
		}
		sbQuery.append(" order by 1, 2, 3");
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("destiny", CorporateProcessConstants.BALANCE_DESTINY);
		parameters.put("origin", CorporateProcessConstants.BALANCE_ORIGIN);	
		parameters.put("remnant", remnant);
		parameters.put("hanoprocess", CorporateProcessConstants.IND_NO);

		List<Object[]> detailSearchResult=findListByQueryString(sbQuery.toString(), parameters);

		return detailSearchResult;		
	}
	
	/**
	 * This method is used to get the Stock Calculation Process Id to verify whether the stock is existed or not for processing the preliminary.
	 *
	 * @param recordDate the record date
	 * @param cutoffDate the cutoff date
	 * @param isin the isin
	 * @param stockState the stock state
	 * @param stockClass the stock class
	 * @param stockType the stock type
	 * @return Long value
	 * @throws ServiceException if query execution got failed
	 */
	public Long getStockCalculationProcess(Date recordDate, Date cutoffDate, String isin, Integer stockState, Integer stockClass, Integer stockType)
			throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select max(sp.idStockCalculationPk) from StockCalculationProcess sp ");
		sbQuery.append(" where trunc(sp.registryDate) = trunc(:registryDate)");
		sbQuery.append(" and trunc(sp.cutoffDate) = trunc(:cutoffdate)");
		sbQuery.append(" and sp.security.idSecurityCodePk = :securities");
		sbQuery.append(" and sp.stockState = :state");
		sbQuery.append(" and sp.stockClass = :stockclass");
		sbQuery.append(" and sp.stockType = :stocktype");
		sbQuery.append(" order by sp.idStockCalculationPk desc");

		parameters.put("registryDate", recordDate);
		parameters.put("cutoffdate", cutoffDate);
		parameters.put("securities", isin);
		parameters.put("state", stockState);
		parameters.put("stockclass", stockClass);
		parameters.put("stocktype", stockType);

		List<Long> result = findListByQueryString(sbQuery.toString(), parameters);
		if(result != null && result.size() > 0){
			return result.get(0);
		}else{
			return null;
		}

	}
	
	/**
	 * Get the Details link data.
	 *
	 * @param corpOperation the corp operation
	 * @param idStocKProcess Long type
	 * @param originDestiny the origin destiny
	 * @param originDestiny1 the origin destiny1
	 * @param remnant the remnant
	 * @return The List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getStockCorporativeProcessDetails(Long corpOperation,Long idStocKProcess, Integer originDestiny,Integer originDestiny1, Integer remnant)throws ServiceException{

		Map<String, Object>  parameters=new HashMap<String, Object>();
		String sql = "select "
				+ "p.mnemonic||' - '||p.idParticipantPk, "																								//[0]
				+ "substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))),"		//[1]
				+ "ha.accountNumber, "																													//[2]
				+ "scb.totalBalance, "																													//[3]
				+ "scb.availableBalance, "																												//[4]
				+ "cpr.totalBalance, "																													//[5]
				+ "cpr.availableBalance,"																												//[6]
				+ "cpr.taxAmount, "																														//[7]
				+ "cpr.custodyAmount, "																													//[8]
				+ "(cpr.totalBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))),"													//[9]
				+ "(cpr.availableBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))),"												//[10]
				+ "(coalesce(cpr.pawnBalance,0) + coalesce(cpr.banBalance,0)+ coalesce(cpr.otherBlockBalance,0)+ coalesce(cpr.reserveBalance,0)+ "
				+ "coalesce(cpr.oppositionBalance,0)),"																									//[11]
				+ "coalesce((select (coalesce(haa.newBenefitAmount,0)+coalesce(haa.newCommissiones,0)+coalesce(haa.newTax,0)) "
				+ "from HolderAccountAdjusment haa  where haa.corporativeOperation.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
				+ "and haa.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk and haa.adjustmentState = :hanoprocess ),0), "			//[12]
				+ "d.availableTaxAmount, "																												//[13]
				+ "d.availableCustodyAmount,cpr.indOriginTarget, "																						//[14][15]
				+ "(nvl(scb.pawnBalance,0)+nvl(scb.banBalance,0)+nvl(scb.otherBlockBalance,0)), "														//[16]
				+ "scb.reportedBalance, "																												//[17]
				+ "scb.reportingBalance, "																												//[18]
				+ "cpr.reportedBalance, "																												//[19]
				+ "cpr.reportingBalance "																												//[20]
				+ "from StockCalculationBalance scb, HolderAccount ha, Participant p,CorporativeResultDetail d right join d.corporativeProcessResult cpr "
				+ "where cpr.corporativeOperation.idCorporativeOperationPk = :idCorporateFk "
				+ "and scb.stockCalculationProcess.idStockCalculationPk = :idstockProcessFk "
				+ "and scb.participant.idParticipantPk = cpr.participant.idParticipantPk "
				+ "and scb.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk "
				+ "and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
				+ "and cpr.participant.idParticipantPk = p.idParticipantPk "
				+ "and (cpr.indOriginTarget = :origin or cpr.indOriginTarget = :origin1) and cpr.indRemanent = :remnant "
				+ "order by 1, 2, 3";

		parameters.put("hanoprocess", CorporateProcessConstants.IND_NO);	
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("idstockProcessFk", idStocKProcess);
		parameters.put("origin", originDestiny);	
		parameters.put("origin1", originDestiny1);	
		parameters.put("remnant", remnant);

		List<Object[]> detailSearchResult=findListByQueryString(sql, parameters);

		return detailSearchResult;		
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getGenericCorporativeProcessDetails(Long corpOperation, Integer originDestiny,Integer originDestiny1, Integer remnant)throws ServiceException{
		
					StringBuffer sbQuery=new StringBuffer();
					Map<String, Object>  parameters=new HashMap<String, Object>();
					sbQuery.append("select ");
					sbQuery.append("p.mnemonic||' - '||p.idParticipantPk, ");																								//[0]
					sbQuery.append("substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))),"); 		//[1]
					sbQuery.append("ha.accountNumber, ");																													//[2]
					//sbQuery.append("cpr.totalBalance, ");																													//[3]
					//sbQuery.append("cpr.availableBalance, ");																												//[4]
					sbQuery.append("scb.totalBalance, ");																													//[3]
					sbQuery.append("scb.availableBalance, ");																												//[4]
					sbQuery.append("cpr.totalBalance, ");																													//[5]
					sbQuery.append("cpr.availableBalance,");																												//[6]
					sbQuery.append("cpr.taxAmount, ");																														//[7]
					sbQuery.append("cpr.custodyAmount, ");																													//[8]
					sbQuery.append("(cpr.totalBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))) as sub1,");											//[9]
					sbQuery.append("(cpr.availableBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))) as sub2,");										//[10]
					sbQuery.append("(coalesce(cpr.pawnBalance,0) + coalesce(cpr.banBalance,0)+ coalesce(cpr.otherBlockBalance,0)+ coalesce(cpr.reserveBalance,0)+ ");
					sbQuery.append("coalesce(cpr.oppositionBalance,0)) as sub3,");																							//[11]
					sbQuery.append("coalesce((select (coalesce(haa.newBenefitAmount,0)+coalesce(haa.newCommissiones,0)+coalesce(haa.newTax,0)) as sub4 ");
					sbQuery.append("from HolderAccountAdjusment haa  where haa.corporativeOperation.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk ");
					sbQuery.append("and haa.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk and haa.adjustmentState = :hanoprocess ),0) as sub5, ");	//[12]
					sbQuery.append("0, ");																																	//[13]
					sbQuery.append("0,cpr.indOriginTarget, ");																												//[14][15]
					sbQuery.append("(COALESCE(cpr.pawnBalance,0)+COALESCE(cpr.banBalance,0)+COALESCE(cpr.otherBlockBalance,0)) as sub6, ");									//[16]
					sbQuery.append("cpr.reportedBalance, ");																												//[17]
					sbQuery.append("cpr.reportingBalance, ");																												//[18]
					sbQuery.append("cpr.reportedBalance, ");																												//[19]
					sbQuery.append("cpr.reportingBalance ");																												//[20]
					
					sbQuery.append(" from CorporativeProcessResult cpr, HolderAccount ha, Participant p ");
					//---------
					sbQuery.append(" ,StockCalculationBalance scb ");
					//---------
					sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
					sbQuery.append(" and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk");
					sbQuery.append(" and cpr.participant.idParticipantPk = p.idParticipantPk");
					sbQuery.append(" and (cpr.indOriginTarget = :origin or cpr.indOriginTarget = :origin1) ");
					//-------------
					sbQuery.append(" and scb.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk ");
					sbQuery.append(" and scb.participant.idParticipantPk = cpr.participant.idParticipantPk ");
					sbQuery.append(" and scb.security.idSecurityCodePk = cpr.security.idSecurityCodePk ");
					
					sbQuery.append(" and scb.stockCalculationProcess.cutoffDate = cpr.corporativeOperation.cutoffDate ");
					sbQuery.append(" and scb.stockCalculationProcess.registryDate = cpr.corporativeOperation.registryDate ");
					//-------------
					sbQuery.append(" order by 1, 2, 3");
					parameters.put("idCorporateFk", corpOperation);	
					parameters.put("origin", originDestiny);	
					parameters.put("origin1", originDestiny1);
					parameters.put("hanoprocess", CorporateProcessConstants.IND_NO);

					List<Object[]> detailSearchResult=findListByQueryString(sbQuery.toString(), parameters);

					return detailSearchResult;
		
	}
	
	/**
	 * Gets the corporative process details.
	 *
	 * @param corpOperation the corp operation
	 * @param originDestiny the origin destiny
	 * @param originDestiny1 the origin destiny1
	 * @param remnant the remnant
	 * @return the corporative process details
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getCorporativeProcessDetails(Long corpOperation, Integer originDestiny,Integer originDestiny1, Integer remnant)throws ServiceException{

		Map<String, Object>  parameters=new HashMap<String, Object>();
		String sql = "select "
				+ "p.mnemonic||' - '||p.idParticipantPk, "																								//[0]
				+ "substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))),"		//[1]
				+ "ha.accountNumber, "																													//[2]
				+ "cpr.totalBalance, "																													//[3]
				+ "cpr.availableBalance, "																												//[4]
				+ "cpr.totalBalance, "																													//[5]
				+ "cpr.availableBalance,"																												//[6]
				+ "cpr.taxAmount, "																														//[7]
				+ "cpr.custodyAmount, "																													//[8]
				+ "(cpr.totalBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))),"													//[9]
				+ "(cpr.availableBalance - (coalesce(cpr.taxAmount,0) + coalesce(cpr.custodyAmount,0))),"												//[10]
				+ "(coalesce(cpr.pawnBalance,0) + coalesce(cpr.banBalance,0)+ coalesce(cpr.otherBlockBalance,0)+ coalesce(cpr.reserveBalance,0)+ "
				+ "coalesce(cpr.oppositionBalance,0)),"																									//[11]
				+ "coalesce((select (coalesce(haa.newBenefitAmount,0)+coalesce(haa.newCommissiones,0)+coalesce(haa.newTax,0)) "
				+ "from HolderAccountAdjusment haa  where haa.corporativeOperation.idCorporativeOperationPk = cpr.corporativeOperation.idCorporativeOperationPk "
				+ "and haa.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk and haa.adjustmentState = :hanoprocess ),0), "			//[12]
				+ "d.availableTaxAmount, "																												//[13]
				+ "d.availableCustodyAmount,cpr.indOriginTarget, "																						//[14][15]
				+ "(nvl(cpr.pawnBalance,0)+nvl(cpr.banBalance,0)+nvl(cpr.otherBlockBalance,0)), "														//[16]
				+ "cpr.reportedBalance, "																												//[17]
				+ "cpr.reportingBalance, "																												//[18]
				+ "cpr.reportedBalance, "																												//[19]
				+ "cpr.reportingBalance "																												//[20]
				+ "from  HolderAccount ha, Participant p,CorporativeResultDetail d join d.corporativeProcessResult cpr "
				+ "where cpr.corporativeOperation.idCorporativeOperationPk = :idCorporateFk "
				+ "and cpr.participant.idParticipantPk = cpr.participant.idParticipantPk "
				+ "and cpr.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk "
				+ "and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
				+ "and cpr.participant.idParticipantPk = p.idParticipantPk "
				+ "and (cpr.indOriginTarget = :origin or cpr.indOriginTarget = :origin1) and cpr.indRemanent = :remnant "
				+ "order by 1, 2, 3";

		parameters.put("hanoprocess", CorporateProcessConstants.IND_NO);	
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("origin", originDestiny);	
		parameters.put("origin1", originDestiny1);	
		parameters.put("remnant", remnant);

		List<Object[]> detailSearchResult=findListByQueryString(sql, parameters);

		return detailSearchResult;		
	}
	
	/**
	 * Get the Details link data.
	 *
	 * @param corpOperation the corp operation
	 * @param remnant the remnant
	 * @param destinySecurity the destiny security
	 * @return The List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getSpecialCorporativeProcessAdjustmentsDetails(Long corpOperation, Integer remnant, String destinySecurity)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select p.mnemonic||' - '||p.idParticipantPk, substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), ");
		sbQuery.append(" ha.accountNumber, cpro.totalBalance, cpro.availableBalance, cprd.totalBalance, cprd.availableBalance, ");
		sbQuery.append(" 0, 0, 0, 0, 0, ");
		sbQuery.append(" haa.newBenefitAmount,0,0,0,0,0,0,0 ");
		sbQuery.append(" from CorporativeProcessResult cpro, HolderAccount ha, Participant p, ");
		sbQuery.append(" CorporativeProcessResult cprd, HolderAccountAdjusment haa ");
		sbQuery.append(" where cpro.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cprd.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cpro.participant.idParticipantPk = cprd.participant.idParticipantPk ");			
		sbQuery.append(" and cpro.holderAccount.idHolderAccountPk = cprd.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and cpro.holderAccount.idHolderAccountPk = ha.idHolderAccountPk");
		sbQuery.append(" and cpro.participant.idParticipantPk = p.idParticipantPk and cprd.indRemanent = :remnant");
		sbQuery.append(" and cpro.indOriginTarget = :origin and cpro.indRemanent = :remnant and cprd.indOriginTarget = :destiny");
		sbQuery.append(" and haa.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cprd.holderAccount.idHolderAccountPk = haa.holderAccount.idHolderAccountPk ");
		//sbQuery.append(" and haa.adjustmentState = :hanoprocess ");
		if(destinySecurity != null && destinySecurity.length() > 0){
			sbQuery.append(" and cprd.security.idSecurityCodePk = :desisin and haa.security.idSecurityCodePk = :desisin");
			parameters.put("desisin", destinySecurity);	
		}
		sbQuery.append(" order by 1, 2, 3");
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("destiny", CorporateProcessConstants.BALANCE_DESTINY);
		parameters.put("origin", CorporateProcessConstants.BALANCE_ORIGIN);	
		parameters.put("remnant", remnant);
		//parameters.put("hanoprocess", CorporateProcessConstants.IND_NO);

		List<Object[]> detailSearchResult=findListByQueryString(sbQuery.toString(), parameters);

		return detailSearchResult;		
	}
	
	/**
	 * Get the Details link data.
	 *
	 * @param corpOperation the corp operation
	 * @param idStocKProcess Long type
	 * @param originDestiny the origin destiny
	 * @param remnant the remnant
	 * @return The List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getStockCorporativeProcessAdjustmentDetails(Long corpOperation,Long idStocKProcess, Integer originDestiny, Integer remnant)throws ServiceException{
		Map<String, Object>  parameters=new HashMap<String, Object>();
		String strSql = 
				"select distinct "
						+ "p.mnemonic||' - '||p.idParticipantPk, "																								//[0]
						+ "substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,2)-locate('(',ha.alternateCode,1))), "		//[1]
						+ "ha.accountNumber, "																													//[2]
						+ "scb.totalBalance, "																													//[3]
						+ "scb.availableBalance, "																												//[4]
						+ "cpr.totalBalance, "																													//[5]
						+ "cpr.availableBalance, "																												//[6]
						+ "cpr.taxAmount, "																														//[7]
						+ "cpr.custodyAmount, "																													//[8]
						+ "haa.newTax,"																															//[9]
						+ "haa.newCommissiones, "																												//[10]
						+ "0, "																																	//[11]
						+ "haa.newBenefitAmount, "																												//[12]
						+ "d.availableTaxAmount, "																												//[13]
						+ "d.availableCustodyAmount,cpr.indOriginTarget, "																						//[14][15]
						+ "(nvl(scb.pawnBalance,0)+nvl(scb.banBalance,0)+nvl(scb.otherBlockBalance,0)), "														//[16]
						+ "scb.reportedBalance, "																												//[17]
						+ "scb.reportingBalance, "																												//[18]
						+ "cpr.reportedBalance, "																												//[19]
						+ "cpr.reportingBalance "																												//[20]
						+ "from StockCalculationBalance scb, HolderAccount ha, Participant p, "
						+ "CorporativeResultDetail d right join d.corporativeProcessResult cpr "
						+ "left join cpr.corporativeOperation.holderAccountAdjusments haa "
						+ "where cpr.corporativeOperation.idCorporativeOperationPk = :idCorporateFk "
						+ "and scb.stockCalculationProcess.idStockCalculationPk = :idstockProcessFk "
						+ "and scb.participant.idParticipantPk = cpr.participant.idParticipantPk "
						+ "and scb.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk "
						+ "and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ "and cpr.participant.idParticipantPk = p.idParticipantPk "
						+ "and cpr.indOriginTarget = :origin and cpr.indRemanent = :remnant "
						+ "and haa.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk  "
						+ "order by 1, 2, 3";

		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("idstockProcessFk", idStocKProcess);
		parameters.put("origin", originDestiny);	
		parameters.put("remnant", remnant);

		List<Object[]> detailSearchResult=findListByQueryString(strSql, parameters);

		return detailSearchResult;		
	}
	
	/**
	 * Gets the holder names.
	 *
	 * @param corpOperation the corp operation
	 * @return the holder names
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getHolderNames(Long corpOperation)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select distinct h.idHolderPk, coalesce((case when h.holderType = :juridictype then h.fullName else ");
		sbQuery.append(" concat(h.name,' ',h.firstLastName,' ',h.secondLastName) end ),:defaultval) ");
		sbQuery.append(" from CorporativeProcessResult cpro, HolderAccountDetail had, Holder h ");
		sbQuery.append(" where cpro.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cpro.holderAccount.idHolderAccountPk = had.holderAccount.idHolderAccountPk");
		sbQuery.append(" and had.holder.idHolderPk = h.idHolderPk ");
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("juridictype", PersonType.JURIDIC.getCode());	
		parameters.put("defaultval", "");	
		return findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the excision securities.
	 *
	 * @param corpOperation the corp operation
	 * @param description the description
	 * @return the excision securities
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getExcisionSecurities(Long corpOperation, boolean description)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select s.idSecurityCodePk ");
		if(description){
			sbQuery.append(" , s.idSecurityCodePk||' - '||s.description ");
		}		
		sbQuery.append(" from SecuritiesCorporative sc, Security s ");
		sbQuery.append(" where sc.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and sc.security.idSecurityCodePk = s.idSecurityCodePk ");
		sbQuery.append(" order by s.idSecurityCodePk ");
		parameters.put("idCorporateFk", corpOperation);	

		return (List<Object[]>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Find retirement operation.
	 *
	 * @param idCorporativeOperationPk the id corporative operation pk
	 * @return the retirement operation
	 * @throws ServiceException the service exception
	 */
	public RetirementOperation findRetirementOperation(Long idCorporativeOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ro");
			sbQuery.append("	FROM CorporativeOperation co,RetirementOperation ro");
			sbQuery.append("	WHERE co.retirementOperation.idRetirementOperationPk = ro.idRetirementOperationPk");
			sbQuery.append("	AND co.idCorporativeOperationPk = :idCorporativeOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idCorporativeOperationPk", idCorporativeOperationPk);
			return (RetirementOperation) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	/**
	 * Find available balance.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param idSecurityCodePk the id security code pk
	 * @return the big decimal
	 */
	public BigDecimal findAvailableBalance(Long idHolderAccountPk, String idSecurityCodePk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT hab.availableBalance");
			sbQuery.append("	FROM HolderAccountBalance hab");
			sbQuery.append("	WHERE hab.holderAccount.idHolderAccountPk = :idHolderAccountPk");
			sbQuery.append("	AND hab.security.idSecurityCodePk = :idSecurityCodePk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			return new BigDecimal(query.getSingleResult().toString());
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
	}
	
	/**
	 * This method is used to validate the delivery date is less than the current date validation to execute the preliminary process.
	 *
	 * @param processIds the process ids
	 * @param currentDate the current date
	 * @param after the after
	 * @return List of Long values
	 * @throws ServiceException if query execution got failed
	 */
	public List<Long> verifyPreliminaryProcessDeliveryDate(List<Long> processIds, Date currentDate, boolean after)
			throws ServiceException {
		if(processIds == null || (processIds != null && processIds.isEmpty())){
			return null;
		}
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cp.idCorporativeOperationPk from CorporativeOperation cp ");

		sbQuery.append(" where cp.idCorporativeOperationPk in (:processids) ");
		sbQuery.append(" and trunc(cp.deliveryDate) ");
		if(after){
			sbQuery.append(" < ");
		}else{
			sbQuery.append(" > ");
		}
		sbQuery.append(" trunc(:currentDate)");
		parameters.put("processids", processIds);
		parameters.put("currentDate", currentDate);

		return findListByQueryString(sbQuery.toString(), parameters);

	}
	

	/**
	 * This method is used to update the CorporativeOperation table.
	 *
	 * @param operationId the operation id
	 * @param issuerAmount the issuer amount
	 * @param loggerUser : logger user object to save the last modified user details
	 * @return integer: return the no. of records updated
	 * @throws ServiceException the service exception
	 */
	public int updateCorporativeOperationsIssuerAmount(Long operationId, BigDecimal issuerAmount, LoggerUser loggerUser) throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("update CorporativeOperation co set co.issuerConfirmedAmount = :issueramount, ");
		sbQuery.append(" co.lastModifyUser = :modifyuser, co.lastModifyDate = :modifydate,");
		sbQuery.append(" co.lastModifyIp = :modifyip, co.lastModifyApp = :modifyapp ");
		sbQuery.append(" where co.idCorporativeOperationPk = :corpprocessid ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("issueramount", issuerAmount);
		query.setParameter("modifyuser", loggerUser.getUserName());
		query.setParameter("modifydate", new Date());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("corpprocessid", operationId);

		return query.executeUpdate();
	}

	/**
	 * Update corporative operations no desmaterialized.
	 *
	 * @param operationId the operation id
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateCorporativeOperationsNoDesmaterialized(Long operationId,LoggerUser loggerUser) throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("update CorporativeOperation co set "
				+ "			   co.situation = :newsituation, "
				+ "			   co.issuerConfirmedAmount = :issuerConfirmedAmount,"
				+ "			   co.paymentAmount = :paymentAmount,"
				+ "			   co.indPayed = :indPayed,");
		sbQuery.append(" co.lastModifyUser = :modifyuser, co.lastModifyDate = :modifydate,");
		sbQuery.append(" co.lastModifyIp = :modifyip, co.lastModifyApp = :modifyapp ");
		sbQuery.append(" where co.idCorporativeOperationPk =:corpprocessid ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("newsituation", CorporativeSituationType.PAID.getCode());
		query.setParameter("indPayed", BooleanType.YES.getCode());
		query.setParameter("issuerConfirmedAmount", new BigDecimal(0));
		query.setParameter("paymentAmount", new BigDecimal(0));

		query.setParameter("modifyuser", loggerUser.getUserName());

		query.setParameter("modifydate", new Date());
		query.setParameter("modifyip", loggerUser.getIpAddress());
		query.setParameter("modifyapp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("corpprocessid", operationId);

		return query.executeUpdate();
	}
	
	/**
	 * Gets the program from corporative.
	 *
	 * @param idProcess the id process
	 * @return the program from corporative
	 */
	public ProgramInterestCoupon getProgramFromCorporative(Long idProcess){
		StringBuilder sbQuery = new StringBuilder();				
		sbQuery.append("SELECT CO.programInterestCoupon");		
		sbQuery.append("  FROM CorporativeOperation CO");
		sbQuery.append(" WHERE CO.idCorporativeOperationPk =:idProcess");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idProcess", idProcess);

		return (ProgramInterestCoupon) query.getSingleResult();
	}
	
	
	
	/**
	 * Used to verify the process logger and process logger details dates.
	 *
	 * @param processIds the process ids
	 * @param processDate the process date
	 * @return List of Long values
	 * @throws ServiceException if query execution got failed
	 */
	public List<Long> verifyPreliminaryProcessDate(List<Long> processIds, Date processDate)
			throws ServiceException {
		if(processIds == null || (processIds != null && processIds.isEmpty())){
			return null;
		}
		List<String> processes = new ArrayList<String>();
		for(Long id : processIds){
			processes.add(id.toString());
		}
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select distinct pld.parameterValue from ProcessLogger pl, ProcessLoggerDetail pld ");
		sbQuery.append(" where pl.idProcessLoggerPk = pld.processLogger.idProcessLoggerPk ");
		sbQuery.append(" and pl.businessProcess.idBusinessProcessPk = :busprocessid ");
		sbQuery.append(" and trunc(pl.finishingDate) = trunc(:processdate)");
		sbQuery.append(" and pl.loggerState = :processstate");
		sbQuery.append(" and pld.parameterName = :paramname");
		sbQuery.append(" and pld.parameterValue in (:paramvals)");

		parameters.put("busprocessid", BusinessProcessType.EXECUTE_PRELIMINAR_CORPORATIVE_PROCESS_MANUAL.getCode());
		parameters.put("processdate", processDate);
		parameters.put("processstate", ProcessLoggerStateType.FINISHED.getCode());
		parameters.put("paramname", "processid");
		parameters.put("paramvals", processes);

		processes = findListByQueryString(sbQuery.toString(), parameters);
		if(processes != null && !processes.isEmpty()){
			for(String proc : processes){
				processIds.remove(Long.valueOf(proc));
			}
		}
		return processIds;

	}
	

	
	
	/**
	 * This method is used to validate the Issuer processing.
	 *
	 * @param corpProcessId the corp process id
	 * @param holderId the holder id
	 * @return List of Long : id's of Corporative operation
	 * @throws ServiceException the service exception
	 */
	public List<Object> getJointHolderDetails(Long corpProcessId, Long holderId) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append("select distinct substring(ha.alternateCode,locate('(',ha.alternateCode,1),1+(locate(')',ha.alternateCode,1)-locate('(',ha.alternateCode,1)))");
		sbQuery.append(" from CorporativeProcessResult cpr, HolderAccount ha, HolderAccountDetail had ");
		sbQuery.append(" where cpr.corporativeOperation.idCorporativeOperationPk = :corpprocessid");
		sbQuery.append(" and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
		sbQuery.append(" and ha.idHolderAccountPk = had.holderAccount.idHolderAccountPk");
		sbQuery.append(" and ha.accountType = :coptype ");
		sbQuery.append(" and had.holder.idHolderPk = :holderid");
		sbQuery.append(" and cpr.indRemanent = :remanent");
		sbQuery.append(" and cpr.indProcessed = :processed");
		sbQuery.append(" and cpr.indOriginTarget = :origintarget ");


		parameters.put("corpprocessid", corpProcessId);
		parameters.put("holderid", holderId);
		parameters.put("remanent", CorporateProcessConstants.IND_NO_REMAINDER);
		parameters.put("processed", CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED);
		parameters.put("origintarget", CorporateProcessConstants.BALANCE_DESTINY);
		parameters.put("coptype", HolderAccountType.OWNERSHIP.getCode());

		return  findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the holder names.
	 *
	 * @param holders the holders
	 * @return the holder names
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getHolderNames(List<Long> holders) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append("select h.idHolderPk, coalesce((case when h.holderType = :juridictype then h.fullName else ");
		sbQuery.append(" concat(h.name,' ',h.firstLastName,' ',h.secondLastName) end ),:defaultval) ");
		sbQuery.append(" from Holder h ");
		sbQuery.append(" where h.idHolderPk in (:holderid)");		

		parameters.put("holderid", holders);
		parameters.put("juridictype", PersonType.JURIDIC.getCode());
		parameters.put("defaultval", "");

		return findListByQueryString(sbQuery.toString(), parameters);

	}
	
	/**
	 * Get the Details link data.
	 *
	 * @param corpOperation the corp operation
	 * @param remnant the remnant
	 * @param destinySecurity the destiny security
	 * @param holderCode the holder code
	 * @return The List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getSpecialCorporativeProcessHolderDetails(Long corpOperation, Integer remnant, String destinySecurity, String holderCode)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();			
		sbQuery.append("select p.mnemonic||' - '||p.idParticipantPk, ");	//0
		sbQuery.append(" ha.accountNumber, "								//1
					 + " cpro.totalBalance, "								//2
					 + " cpro.availableBalance, "							//3
					 + " cprd.totalBalance, "								//4
					 + " cprd.availableBalance, "							//5
					 + " cpro.holderAccount.idHolderAccountPk, "			//6
					 + " 0, "												//7
					 + " 0  ");												//8
		sbQuery.append(" from   CorporativeProcessResult cpro, "
							+ " HolderAccount ha, "
							+ " Participant p, "
							+ " CorporativeProcessResult cprd ");
		sbQuery.append(" where cpro.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cprd.corporativeOperation.idCorporativeOperationPk = :idCorporateFk ");
		sbQuery.append(" and cpro.participant.idParticipantPk = cprd.participant.idParticipantPk ");			
		sbQuery.append(" and cpro.holderAccount.idHolderAccountPk = cprd.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and cpro.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
		sbQuery.append(" and cprd.participant.idParticipantPk = p.idParticipantPk "
					 + " and cprd.indRemanent = :remnant ");
		sbQuery.append(" and cpro.indOriginTarget = :origin "
					 + " and cpro.indRemanent = :remnant "
					 + " and cprd.indOriginTarget = :destiny ");
		sbQuery.append(" and ha.alternateCode like :alternatecode ");
		if(destinySecurity != null && destinySecurity.length() > 0){
			sbQuery.append(" and cprd.security.idSecurityCodePk = :desisin ");
			parameters.put("desisin", destinySecurity);	
		}
		sbQuery.append(" order by 1, 2 ");	//p.mnemonic||' - '||p.idParticipantPk, ha.accountNumber
		parameters.put("idCorporateFk", corpOperation);	
		parameters.put("destiny", CorporateProcessConstants.BALANCE_DESTINY);
		parameters.put("origin", CorporateProcessConstants.BALANCE_ORIGIN);	
		parameters.put("remnant", remnant);
		parameters.put("alternatecode", "%"+holderCode+"%");

		List<Object[]> detailSearchResult=findListByQueryString(sbQuery.toString(), parameters);

		return detailSearchResult;		
	}
	
	/**
	 * Get the Details link data.
	 *
	 * @param corpOperation the corp operation
	 * @param idStocKProcess Long type
	 * @param originDestiny the origin destiny
	 * @param remnant the remnant
	 * @param holderCode the holder code
	 * @return The List<Object[]>
	 * @throws ServiceException if query execution got failed
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getStockCorporativeProcessHolderDetails(Long corpOperation,Long idStocKProcess, Integer originDestiny, 
			Integer remnant, String holderCode)throws ServiceException{
		Map<String, Object>  parameters=new HashMap<String, Object>();
		String sql = 
				"select "
						+ 	"p.mnemonic||' - '||p.idParticipantPk,"									//[0]
						+ 	"ha.accountNumber, "													//[1]
						+ 	"scb.totalBalance, "													//[2]
						+ 	"scb.availableBalance, "												//[3]
						+ 	"cpr.totalBalance, "													//[4]
						+ 	"cpr.availableBalance, "												//[5]
						+ 	"cpr.holderAccount.idHolderAccountPk,"									//[6]
						+ 	"cpr.taxAmount, "														//[7]
						+ 	"cpr.custodyAmount "													//[8]
						+ 	"from StockCalculationBalance scb, "
						+   	" HolderAccount ha, "
						+ 		" Participant p,"
						+ 		" CorporativeProcessResult cpr "
						+ 	"where cpr.corporativeOperation.idCorporativeOperationPk = :idCorporateFk "
						+ 	"and scb.stockCalculationProcess.idStockCalculationPk = :idstockProcessFk "
						+ 	"and scb.participant.idParticipantPk = cpr.participant.idParticipantPk "
						+ 	"and scb.holderAccount.idHolderAccountPk = cpr.holderAccount.idHolderAccountPk "
						+ 	"and cpr.holderAccount.idHolderAccountPk = ha.idHolderAccountPk "
						+ 	"and cpr.participant.idParticipantPk = p.idParticipantPk "
						+ 	"and cpr.indOriginTarget = :origin and cpr.indRemanent = :remnant "
						+ 	"and ha.alternateCode like :alternatecode "
						+ 	"order by 1, 2, 3";

		parameters.put("idCorporateFk", corpOperation);
		parameters.put("idstockProcessFk", idStocKProcess);
		parameters.put("origin", originDestiny);
		parameters.put("remnant", remnant);
		parameters.put("alternatecode", "%"+holderCode+"%");

		return findListByQueryString(sql, parameters);		
	}

	/**
	 * This method is used to get the holder account adjustments from the HolderAccountAdjusment table if we have any holder account adjustments for
	 * the corporative operation.
	 *
	 * @param CorpProcessId : Corporative Operation Id
	 * @param state : state is 0,definitive not processed state
	 * @param security the security
	 * @param holderCode the holder code
	 * @return map : map of holder account id's and get the delivery benefits,collect commissions, benefit tax, tax, commissions
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountAdjusment> getHolderAccountAdjustmentsForHolder(Long CorpProcessId, Integer state, String security, String holderCode) throws ServiceException {
		List<HolderAccountAdjusment> lstHolderAdjustments = new ArrayList<HolderAccountAdjusment>();
		String sql	= 	" select haa "
					  + " from HolderAccountAdjusment haa inner join fetch haa.holderAccount ha "
					  + " where haa.corporativeOperation.idCorporativeOperationPk = :corpprocessid "
					  + " and ha.alternateCode like :alternatecode ";

		if(Validations.validateIsNotNullAndNotEmpty(security)){
			sql = sql.concat(" and haa.security.idSecurityCodePk = :security ");
		}

		Query query = em.createQuery(sql, HolderAccountAdjusment.class);
		query.setParameter("corpprocessid", CorpProcessId);
		query.setParameter("alternatecode", "%"+holderCode+"%");
		if(Validations.validateIsNotNullAndNotEmpty(security)){
			query.setParameter("security", security);
		}
		lstHolderAdjustments = query.getResultList();

		return lstHolderAdjustments;

	}

	/**
	 * Delete holder adjustments.
	 *
	 * @param holderAdj the holder adj
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int deleteHolderAdjustments(List<Long> holderAdj) throws ServiceException{
		Query query = em.createQuery("delete from HolderAccountAdjusment haa where haa.idHolderAccountAdjusmentPk in (:holaccpk) ");
		query.setParameter("holaccpk", holderAdj);	

		return query.executeUpdate();
	}
	

	
	/**
	 * This method is used to validate the Issuer processing.
	 *
	 * @param idsProcesss the ids processs
	 * @return List of Long : id's of Corporative operation
	 * @throws ServiceException the service exception
	 */
	public List<Long> verifyEmisorConfirm(List<Long> idsProcesss) throws ServiceException{
		if(idsProcesss == null || (idsProcesss != null && idsProcesss.isEmpty())){
			return null;
		}
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append("select co.idCorporativeOperationPk from  CorporativeOperation co ");		
		sbQuery.append(" where co.idCorporativeOperationPk in (:idprocess) ");
		sbQuery.append(" and co.situation is null ");

		parameters.put("idprocess", idsProcesss);
		List<Long> corporateList= findListByQueryString(sbQuery.toString(), parameters);
		return corporateList;

	}
	
	
	
	

	/**
	 * Verify holder account movements for preliminary.
	 *
	 * @param processIds the process ids
	 * @return the list
	 */
	public List<Long> verifyHolderAccountMovementsForPreliminary(List<Long> processIds){
		if(processIds == null || (processIds != null && processIds.isEmpty())){
			return null;
		}


		List<Long> corporateList = new ArrayList<Long>();
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("        select distinct co.id_corporative_operation_pk from holder_account_movement ham ");
		sbQuery.append("   	join  movement_type mt");
		sbQuery.append("   on ham.id_movement_type_fk=mt.id_movement_type_pk");
		sbQuery.append("  join holder_account ha");
		sbQuery.append("        on ham.id_holder_account_fk=ha.id_holder_account_pk");
		sbQuery.append("  join corporative_process_result cpr");
		sbQuery.append("   on ha.id_holder_account_pk=cpr.id_holder_account_fk");
		sbQuery.append("   join corporative_operation co");
		sbQuery.append("   on cpr.id_corporative_operation_fk=co.id_corporative_operation_pk");
		sbQuery.append("       where (mt.ind_block_movement=1 or mt.ind_guarantee_movement=1)");
		sbQuery.append(" and co.id_corporative_operation_pk in (:listProcess)");
		sbQuery.append("  and trunc(movement_date)>( select trunc(creation_date) from stock_calculation_process");
		sbQuery.append(" where trunc(cutoff_date)=trunc(co.cutoff_date)");
		sbQuery.append(" and trunc(registry_date)=trunc(co.registry_date)");
		sbQuery.append("  and stock_type=:stock_type");
		sbQuery.append(" and stock_class=:stock_class");
		sbQuery.append(" and id_isin_code_fk=co.ID_ORIGIN_ISIN_CODE_FK)");
		sbQuery.append("");

		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("stock_class",StockClassType.NORMAL.getCode());
		query.setParameter("stock_type",StockType.CORPORATIVE.getCode());
		query.setParameter("listProcess", processIds);


		try{
			List<BigDecimal> list =(List<BigDecimal>) query.getResultList();
			for(BigDecimal temp:list){
				corporateList.add(temp.longValue());
			}
			return corporateList;
		}catch(NoResultException ex){
			return corporateList;
		}
	}

	/**
	 * Gets the corporative operation.
	 *
	 * @param processId the process id
	 * @return the corporative operation
	 */
	public CorporativeOperation getCorporativeOperation(Long processId){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT c ");
		sbQuery.append("   FROM CorporativeOperation c "
				+ "INNER JOIN FETCH c.securities s "
				+ "INNER JOIN FETCH s.issuer ");
		sbQuery.append("  WHERE c.idCorporativeOperationPk =:processId");	
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("processId", processId);
		return (CorporativeOperation)query.getSingleResult();
	}
	
	/**
	 * Gets the uSD exchange rate.
	 *
	 * @return the uSD exchange rate
	 */
	public BigDecimal getUSDExchangeRate(){
		StringBuffer sbQuery=new StringBuffer();
		sbQuery.append(" SELECT REFERENCE_PRICE");
		sbQuery.append("   FROM DAILY_EXCHANGE_RATES");
		sbQuery.append("  WHERE TRUNC(DATE_RATE)=TRUNC(sysdate)");
		sbQuery.append("    AND ID_CURRENCY=:currency");
		sbQuery.append("    AND ID_SOURCE_INFORMATION =:sourceInformation");
		
		Query query = em.createNativeQuery(sbQuery.toString());

		query.setParameter("currency", CurrencyType.USD.getCode());	
		query.setParameter("sourceInformation", InformationSourceType.BCRD.getCode());	
		try{
			return (BigDecimal) query.getSingleResult();
		}catch(NoResultException nrException){
			return null;
		}
	}
	
	/**
	 * Gets the custody amount.
	 *
	 * @param corpProcessId the corp process id
	 * @return the custody amount
	 */
	public BigDecimal getCustodyAmount(Long corpProcessId){
		StringBuffer sbQuery=new StringBuffer();

		sbQuery.append(" SELECT SUM(nvl(CPR.custodyAmount,0))");
		sbQuery.append("   FROM CorporativeProcessResult CPR");
		sbQuery.append("  WHERE CPR.corporativeOperation.idCorporativeOperationPk =:corpProcessId");
		sbQuery.append("    AND CPR.indOriginTarget = 2");

		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("corpProcessId", corpProcessId);	

		return (BigDecimal) query.getSingleResult();
	}
	
	/**
	 * Gets the block documents objects.
	 *
	 * @param blockOperation the block operation
	 * @return the block documents objects
	 */
	public Object[] getBlockDocumentsObjects(BlockOperation blockOperation){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select 		BR.blockType, "
				+ "BO.originalBlockBalance, "
				+ "BO.idBlockOperationPk, ");
		sbQuery.append(" 	   		BR.holder.idHolderPk ,"
				+ "BO.indCashDividend, "
				+ "BO.indReturnContributions, "
				+ "BO.indInterest, ");
		sbQuery.append(" 	   		BO.indStockDividend, "
				+ "BO.indSuscription, "
				+ "BO.participant.idParticipantPk, "
				+ "BO.blockLevel, ");
		sbQuery.append("	   		BO.blockState, "
				+ "BO.indAmortization, "
				+ "BO.indRescue ");
		sbQuery.append(" 	   FROM BlockOperation BO");
		sbQuery.append(" INNER JOIN BO.blockRequest BR");
		sbQuery.append(" 	  WHERE BO.idBlockOperationPk =:idBlockOperation");
		parameters.put("idBlockOperation", blockOperation.getIdBlockOperationPk());

		Query query = this.em.createQuery(sbQuery.toString());
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return (Object[]) query.getSingleResult();
	}
	
	/**
	 * Gets the unblock documents objects.
	 *
	 * @param unblockOperation the unblock operation
	 * @return the unblock documents objects
	 */
	public Object[] getUnblockDocumentsObjects(UnblockOperation unblockOperation){
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select 		BR.blockType, "
				+ "BO.originalBlockBalance, "
				+ "BO.idBlockOperationPk, ");
		sbQuery.append(" 	   		BR.holder.idHolderPk ,"
				+ "BO.indCashDividend, "
				+ "BO.indReturnContributions, "
				+ "BO.indInterest, ");
		sbQuery.append(" 	   		BO.indStockDividend, "
				+ "BO.indSuscription, "
				+ "BO.participant.idParticipantPk, "
				+ "BO.blockLevel, ");
		sbQuery.append("	   		BO.blockState, "
				+ "BO.indAmortization, "
				+ "BO.indRescue ");
		sbQuery.append(" 	   FROM BlockOperation UO");
		sbQuery.append(" INNER JOIN UO.blockOperation BO");
		sbQuery.append(" INNER JOIN BO.blockRequest BR");
		sbQuery.append(" 	  WHERE UO.idUnblockOperationPk =:idUnblockOperation");
		parameters.put("idUnblockOperation", unblockOperation.getIdUnblockOperationPk());
		Query query = this.em.createQuery(sbQuery.toString());
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return (Object[]) query.getSingleResult();
	}
	
	/**
	 * This method is used to get the target available change of ownership values from the ChangeOwnershipDetail table.
	 *
	 * @param originOwner : change ownership detail value
	 * @param isin : Security for the participant
	 * @return List of Objects
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getTargetAvailableChangeofOwnerShipDetails(Long originOwner, String isin)throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select cod.participant.idParticipantPk , cod.holderAccount.idHolderAccountPk, ");
		sbQuery.append(" cod.availableBalance ");
		sbQuery.append(" from ChangeOwnershipDetail cod ");
		sbQuery.append(" where cod.sourceChangeOwnershipDetail.idChangeOwnershipDetailPk = :originowner ");
		sbQuery.append(" and cod.indOriginTarget = :origin ");
		sbQuery.append(" and cod.security.idSecurityCodePk = :isin ");

		parameters.put("originowner", originOwner);
		parameters.put("origin", Long.valueOf(CorporateProcessConstants.BALANCE_DESTINY));
		parameters.put("isin", isin);

		return findListByQueryString(sbQuery.toString(), parameters);
	}	
	
	/**
	 * This method is used to get the target block change of ownership values from the ChangeOwnershipDetail table.
	 *
	 * @param originOwner : change ownership detail value
	 * @param isin : Security for the participant
	 * @return List of Objects
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getTargetBlockChangeofOwnerShipDetails(Long originOwner, String isin)throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select br.blockType, bo.actualBlockBalance, ");
		sbQuery.append(" bo.idBlockOperationPk, bco.blockOperation.idBlockOperationPk, ");
		sbQuery.append(" bo.participant.idParticipantPk,bo.holderAccount.idHolderAccountPk, br.holder.idHolderPk ");
		sbQuery.append(" from BlockChangeOwnership bco, BlockOperation bo , BlockRequest br ");
		sbQuery.append(" where bo.blockRequest.idBlockRequestPk = br.idBlockRequestPk ");
		sbQuery.append(" and bco.changeOwnershipDetail.idChangeOwnershipDetailPk = :originowner ");
		sbQuery.append(" and bo.idBlockOperationPk = bco.refBlockOperation.idBlockOperationPk ");
		sbQuery.append(" and bo.securities.idSecurityCodePk = :isin ");
		sbQuery.append(" order by br.blockType ");

		parameters.put("originowner", originOwner);
		parameters.put("isin", isin);

		return findListByQueryString(sbQuery.toString(), parameters);
	}
	/**
	 * Gets the target security.
	 *
	 * @param corporateId the corporate id
	 * @return the target security
	 * @throws ServiceException the service exception
	 */
	public Security getTargetSecurity(Long corporateId) throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append(" select cp.targetSecurity ");
		sbQuery.append(" from CorporativeOperation cp ");
		sbQuery.append(" where cp.idCorporativeOperationPk = :idCorporateFk");
		parameters.put("idCorporateFk", corporateId);	
		return (Security)findObjectByQueryString(sbQuery.toString(), parameters);		
	}

	/**
	 * Gets the corporative process details.
	 *
	 * @param corpOperation the corp operation
	 * @param destinySecurity the destiny security
	 * @param ajustFlag the ajust flag
	 * @return the corporative process details
	 * @throws ServiceException the service exception
	 */
	public CorporativeProcessDetailsTO getCorporativeProcessDetails(CorporativeOperation corpOperation, String destinySecurity, boolean ajustFlag) throws ServiceException{
		CorporativeProcessDetailsTO details =  new CorporativeProcessDetailsTO();
		details.setSourceAvailableBalance(BigDecimal.ZERO);
		details.setSourceTotalBalance(BigDecimal.ZERO);
		details.setSourceBlockBalance(BigDecimal.ZERO);
		details.setSourceReportedBalance(BigDecimal.ZERO);
		details.setSourceReportingBalance(BigDecimal.ZERO);
		details.setDestinyAvailableBalance(BigDecimal.ZERO);
		details.setDestinyTotalBalance(BigDecimal.ZERO);
		details.setAdjustedBalance(BigDecimal.ZERO);
		details.setCustodyAmount(BigDecimal.ZERO);
		details.setNetAvailableBalance(BigDecimal.ZERO);
		details.setNetTotalBalance(BigDecimal.ZERO);
		details.setTaxAmount(BigDecimal.ZERO);
		details.setTotalBlockedBalance(BigDecimal.ZERO);
		details.setTotalReportedBalance(BigDecimal.ZERO);
		details.setTotalReportingBalance(BigDecimal.ZERO);
		details.setDetails(new ArrayList<CorporativeProcessDetailsTO>());
		CorporativeProcessDetailsTO corpData = null;
		List<Object[]> corpDetails = null;
		if(ajustFlag){
			corpDetails = getCorporativeProcessResultAdjustmentDetails(corpOperation, destinySecurity);
		}else{
			corpDetails = getCorporativeProcessResultDetails(corpOperation, destinySecurity);
		}
		if(corpDetails != null && corpDetails.size() > 0){
			Map<Long, String> holderNames = getHolderName(corpOperation.getIdCorporativeOperationPk());
			for(Object[] data : corpDetails){
				corpData = new CorporativeProcessDetailsTO();
				if(data[0]!=null){
					corpData.setParticipant((String)data[0]);
				}				
				if(data[1]!=null){
					String rntCode = (String)data[1];
					StringBuffer rnt = new StringBuffer();
					StringBuffer name = new StringBuffer();
					if(rntCode.contains("(")){
						rntCode = rntCode.replaceAll("\\(", "");
					}
					if(rntCode.contains(")")){
						rntCode = rntCode.replaceAll("\\)", "");
					}
					if(rntCode.contains(",")){
						String[] codes = rntCode.split("\\,");
						for(String code : codes){
							if(rnt.length() > 0){
								rnt.append("<br/>"); 
								name.append("<br/>");
							}
							rnt.append(code);
							name.append(holderNames.get(Long.valueOf(code)));
						}
					}else{
						rnt.append(rntCode);
						if(rntCode != null && rntCode.length() > 0){
							name.append(holderNames.get(Long.valueOf(rntCode)));
						}
					}
					corpData.setRnt(rnt.toString());
					corpData.setName(name.toString());
				}
				if(data[2]!=null){
					corpData.setAccount((Integer)data[2]);
				}
				//TOTAL STOCK BALANCE - AVAILABLE STOCK BALANCE
				if(data[3]!=null){
					BigDecimal totalStock = getAmount(data[3]);
					corpData.setSourceTotalBalance(totalStock);
				}
				if(data[4]!=null){
					BigDecimal availableStock = getAmount(data[4]);
					corpData.setSourceAvailableBalance(availableStock);
				}
				if(data[16]!=null){
					BigDecimal blockStock = getAmount(data[16]);
					corpData.setSourceBlockBalance(blockStock);
				}
				if(data[17]!=null){
					BigDecimal reportaredStock = getAmount(data[17]);
					corpData.setSourceReportedBalance(reportaredStock);
				}
				if(data[18]!=null){
					BigDecimal reportingStock = getAmount(data[18]);
					corpData.setSourceReportingBalance(reportingStock);
				}
				if(data[11]!=null){
					BigDecimal blockAmount = getAmount(data[11]);
					corpData.setTotalBlockedBalance(blockAmount);
				}				
				if(data[19]!=null){
					BigDecimal reportaredAmount = getAmount(data[19]);
					corpData.setTotalReportedBalance(reportaredAmount);
				}	
				if(data[20]!=null){
					BigDecimal reportingAmount = getAmount(data[20]);
					corpData.setTotalReportingBalance(reportingAmount);
				}	
				if(data[12]!=null){
					BigDecimal adjustmentAmount = getAmount(data[12]);
					corpData.setAdjustedBalance(adjustmentAmount);
				}
				boolean flagDestiny = false;
				Integer indOriginDestiny = null;
				if(corpOperation.getCurrency()!=null &&
						(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode()) || 
						corpOperation.getCurrency().equals(CurrencyType.DMV.getCode()))){
					if(data[15]!=null){
						indOriginDestiny = (Integer)data[15];
						if(indOriginDestiny.equals(CorporateProcessConstants.EXCHANGE_DESTINY)){
							if(corpOperation.getCurrency().equals(CurrencyType.UFV.getCode())){
								corpData.setCurrencyDescription(CurrencyType.UFV.getCodeIso());
							}else{
								corpData.setCurrencyDescription(CurrencyType.DMV.getCodeIso());
							}							
						}else{
							corpData.setCurrencyDescription(CurrencyType.PYG.getCodeIso());
							flagDestiny = true;
						}
					}
				}
				
				if(indOriginDestiny==null){
					flagDestiny = true;
				}
				//TOTAL AMOUNT - AVAILABLE AMOUNT
				BigDecimal totalCorporative=null;
				if(data[5]!=null){
					totalCorporative = getAmount(data[5]);					
					BigDecimal availableCorporative = null;
					if(data[6]!=null){
						 availableCorporative = getAmount(data[6]);					 
						//TAX - CUSTODY AMOUNTS
						if(data[7]!=null){
							BigDecimal taxAmount = getAmount(data[7]);
							if(data[8]!=null){
								BigDecimal custodyAmount = getAmount(data[8]);
								BigDecimal totalNetAmount = BigDecimal.ZERO;
								if(corpOperation.getSecurities().getIndPaymentReporting().equals(BooleanType.NO.getCode())){
									totalNetAmount = (totalCorporative.add(corpData.getTotalReportedBalance())).subtract((taxAmount.add(custodyAmount).add(corpData.getTotalReportingBalance())));
								}else{
									totalNetAmount = totalCorporative.subtract((taxAmount.add(custodyAmount)));
								}								
								corpData.setDestinyTotalBalance(totalCorporative);	
								corpData.setNetTotalBalance(totalNetAmount);
								if(data[13]!=null && data[14]!=null){
									BigDecimal taxAvailableAmount = getAmount(data[13]);
									BigDecimal custodyAvailableAmount = getAmount(data[14]);																
									BigDecimal availableNetAmount = availableCorporative.subtract((taxAvailableAmount.add(custodyAvailableAmount)));
									corpData.setDestinyAvailableBalance(availableCorporative);
									corpData.setTaxAmount(taxAmount);
									corpData.setCustodyAmount(custodyAmount);												
									corpData.setNetAvailableBalance(availableNetAmount);	
								}else{							
									corpData.setDestinyAvailableBalance(availableCorporative);
									corpData.setTaxAmount(BigDecimal.ZERO);
									corpData.setCustodyAmount(BigDecimal.ZERO);
									corpData.setNetAvailableBalance(BigDecimal.ZERO);	
								}								
							}else{
								corpData.setNetTotalBalance(BigDecimal.ZERO);
								corpData.setDestinyTotalBalance(BigDecimal.ZERO);
								corpData.setDestinyAvailableBalance(BigDecimal.ZERO);
								corpData.setTaxAmount(BigDecimal.ZERO);
								corpData.setCustodyAmount(BigDecimal.ZERO);
								corpData.setNetAvailableBalance(BigDecimal.ZERO);		
							}												
						}else{
							corpData.setNetTotalBalance(BigDecimal.ZERO);
							corpData.setDestinyTotalBalance(BigDecimal.ZERO);
							corpData.setDestinyAvailableBalance(BigDecimal.ZERO);
							corpData.setTaxAmount(BigDecimal.ZERO);
							corpData.setCustodyAmount(BigDecimal.ZERO);
							corpData.setNetAvailableBalance(BigDecimal.ZERO);						
						}		
					}else{
						corpData.setNetTotalBalance(BigDecimal.ZERO);
						corpData.setDestinyTotalBalance(BigDecimal.ZERO);
						corpData.setDestinyAvailableBalance(BigDecimal.ZERO);
						corpData.setTaxAmount(BigDecimal.ZERO);
						corpData.setCustodyAmount(BigDecimal.ZERO);
						corpData.setNetAvailableBalance(BigDecimal.ZERO);					
					}				
				}else{
					corpData.setNetTotalBalance(BigDecimal.ZERO);
					corpData.setDestinyTotalBalance(BigDecimal.ZERO);
					corpData.setDestinyAvailableBalance(BigDecimal.ZERO);
					corpData.setTaxAmount(BigDecimal.ZERO);
					corpData.setCustodyAmount(BigDecimal.ZERO);
					corpData.setNetAvailableBalance(BigDecimal.ZERO);	
				}								
				//GROUPING AMOUNTS TO SHOW TOTAL INFORMATION
				//TOTAL STOCK BALANCE
				if(Validations.validateIsNotNull(corpData.getSourceTotalBalance()) && flagDestiny){
					details.setSourceTotalBalance(details.getSourceTotalBalance().add(corpData.getSourceTotalBalance()));
				}
				//AVAILABLE STOCK BALANCE
				if(Validations.validateIsNotNull(corpData.getSourceAvailableBalance()) && flagDestiny){
					details.setSourceAvailableBalance(details.getSourceAvailableBalance().add(corpData.getSourceAvailableBalance()));
				}
				//BLOCK STOCK BALANCE
				if(Validations.validateIsNotNull(corpData.getSourceBlockBalance()) && flagDestiny){
					details.setSourceBlockBalance(details.getSourceBlockBalance().add(corpData.getSourceBlockBalance()));
				}
				//REPORTARED STOCK BALANCE
				if(Validations.validateIsNotNull(corpData.getSourceReportedBalance()) && flagDestiny){
					details.setSourceReportedBalance(details.getSourceReportedBalance().add(corpData.getSourceReportedBalance()));
				}
				//REPORTING STOCK BALANCE
				if(Validations.validateIsNotNull(corpData.getSourceReportingBalance()) && flagDestiny){
					details.setSourceReportingBalance(details.getSourceReportingBalance().add(corpData.getSourceReportingBalance()));
				}
				//CORPORATIVE GROSS TOTAL AMOUNT
				if(Validations.validateIsNotNull(totalCorporative) && flagDestiny){
					details.setDestinyTotalBalance(details.getDestinyTotalBalance().add(totalCorporative));
				}
				//CORPORATIVE GROSS AVAILABLE AMOUNT
				if(Validations.validateIsNotNull(corpData.getDestinyAvailableBalance()) && flagDestiny){
					details.setDestinyAvailableBalance(details.getDestinyAvailableBalance().add(corpData.getDestinyAvailableBalance()));
				}
				//TAX AMOUNT
				if(Validations.validateIsNotNull(corpData.getTaxAmount()) && flagDestiny){
					details.setTaxAmount(details.getTaxAmount().add(corpData.getTaxAmount()));
				}
				//CUSTODY AMOUNT
				if(Validations.validateIsNotNull(corpData.getCustodyAmount()) && flagDestiny){
					details.setCustodyAmount(details.getCustodyAmount().add(corpData.getCustodyAmount()));
				}
				//CORPORATIVE NET TOTAL AMOUNT
				if(Validations.validateIsNotNull(corpData.getNetTotalBalance()) && flagDestiny){
					details.setNetTotalBalance(details.getNetTotalBalance().add(corpData.getNetTotalBalance()));
				}
				//CORPORATIVE NET AVAILABLE AMOUNT
				if(Validations.validateIsNotNull(corpData.getNetAvailableBalance()) && flagDestiny){
					details.setNetAvailableBalance(details.getNetAvailableBalance().add(corpData.getNetAvailableBalance()));
				}
				//CORPORATIVE BLOCK AMOUNTS
				if(Validations.validateIsNotNull(corpData.getTotalBlockedBalance()) && flagDestiny){
					details.setTotalBlockedBalance(details.getTotalBlockedBalance().add(corpData.getTotalBlockedBalance()));
				}
				if(Validations.validateIsNotNull(corpData.getAdjustedBalance()) && flagDestiny){
					details.setAdjustedBalance(details.getAdjustedBalance().add(corpData.getAdjustedBalance()));
				}
				details.getDetails().add(corpData);				
			}
		}

		return details;
	}
	
	/**
	 * Gets the corporative process result details.
	 *
	 * @param corpOperation the corp operation
	 * @param destinySecurity the destiny security
	 * @return the corporative process result details
	 * @throws ServiceException the service exception
	 */
	private List<Object[]> getCorporativeProcessResultDetails(CorporativeOperation corpOperation, String destinySecurity) throws ServiceException{
		List<Object[]> corpDetails = null;
		if(ImportanceEventType.REMANENT_PAYMENT.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
			Long remnantOperId = getRemanentProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
					corpOperation.getSecurities().getIdSecurityCodePk());
			corpDetails = getRemnantCorporativeProcessDetails(corpOperation.getIdCorporativeOperationPk(), 
					remnantOperId, CorporateProcessConstants.BALANCE_DESTINY,CorporateProcessConstants.EXCHANGE_DESTINY, CorporateProcessConstants.IND_NO_REMAINDER);
		}else if(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
			Long OperId = getContributionReturnProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
					corpOperation.getSecurities().getIdSecurityCodePk(),ImportanceEventType.CAPITAL_REDUCTION.getCode());
			if(OperId==null){
				OperId = getContributionReturnProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
						corpOperation.getSecurities().getIdSecurityCodePk(),ImportanceEventType.SECURITIES_EXCLUSION.getCode());
			}
			corpDetails = getContributionReturnCorporativeProcessDetails(corpOperation.getIdCorporativeOperationPk(), 
					 CorporateProcessConstants.BALANCE_DESTINY,CorporateProcessConstants.EXCHANGE_DESTINY,OperId);
		}else if(CorporateProcessUtils.isSpecialCorporateProcess(corpOperation.getCorporativeEventType().getCorporativeEventType()) || 
				ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
			if(ImportanceEventType.SECURITIES_EXCLUSION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType()) || 
					ImportanceEventType.END_PREFERRED_SUSCRIPTION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
				corpDetails = getNoDestinyCorporativeProcessDetails(corpOperation.getIdCorporativeOperationPk(), 
						CorporateProcessConstants.IND_NO_REMAINDER);
			}else{
				corpDetails = getSpecialCorporativeProcessDetails(corpOperation.getIdCorporativeOperationPk(), 
					CorporateProcessConstants.IND_NO_REMAINDER, destinySecurity);
			}
		}else{
			if( corpOperation.getCorporativeEventType().getIndCashProcess().equals(0) ) {
				corpDetails = getGenericCorporativeProcessDetails(corpOperation.getIdCorporativeOperationPk(), CorporateProcessConstants.BALANCE_DESTINY,CorporateProcessConstants.EXCHANGE_DESTINY, CorporateProcessConstants.IND_NO_REMAINDER);
			}else {
				//obtener data de operaciones de cambio de titularidad
				Map<Long, List<Object[]>> changeOwnership = corporateExecutorService.getChangeofOwnerShipSucession(
						corpOperation.getSecurities().getIdSecurityCodePk(),corpOperation.getRegistryDate());
				//obtener data de operaciones de participantes unificados
				Map<Long, Object[]> uniParticipants = corporateExecutorService.getUnifiedParticipants(corpOperation.getSecurities().getIdSecurityCodePk(), corpOperation.getRegistryDate());
				if((changeOwnership!=null && changeOwnership.size()>0)||
						uniParticipants!=null && uniParticipants.size()>0){
					corpDetails = getCorporativeProcessDetails(corpOperation.getIdCorporativeOperationPk(), 
							CorporateProcessConstants.BALANCE_DESTINY,CorporateProcessConstants.EXCHANGE_DESTINY, CorporateProcessConstants.IND_NO_REMAINDER);
				}else{
					Long stockProcess = getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
							corpOperation.getSecurities().getIdSecurityCodePk()
							, StockStateType.FINISHED.getCode(), StockClassType.NORMAL.getCode(), StockType.CORPORATIVE.getCode());
					corpDetails = getStockCorporativeProcessDetails(corpOperation.getIdCorporativeOperationPk(), 
							stockProcess, CorporateProcessConstants.BALANCE_DESTINY,CorporateProcessConstants.EXCHANGE_DESTINY, CorporateProcessConstants.IND_NO_REMAINDER);
				}
			}
			
			
		}
		return corpDetails;
	}
	
	/**
	 * Gets the corporative process result adjustment details.
	 *
	 * @param corpOperation the corp operation
	 * @param destinySecurity the destiny security
	 * @return the corporative process result adjustment details
	 * @throws ServiceException the service exception
	 */
	private List<Object[]> getCorporativeProcessResultAdjustmentDetails(CorporativeOperation corpOperation, String destinySecurity) throws ServiceException{
		List<Object[]> corpDetails = null;
		if(CorporateProcessUtils.isSpecialCorporateProcess(corpOperation.getCorporativeEventType().getCorporativeEventType())){
			corpDetails = getSpecialCorporativeProcessAdjustmentsDetails(corpOperation.getIdCorporativeOperationPk(), 
					CorporateProcessConstants.IND_NO_REMAINDER, destinySecurity);
		}else if(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode().equals(corpOperation.getCorporativeEventType().getCorporativeEventType())){
			Long OperId = getContributionReturnProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
					corpOperation.getSecurities().getIdSecurityCodePk(),ImportanceEventType.CAPITAL_REDUCTION.getCode());
			if(OperId==null){
				OperId = getContributionReturnProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
						corpOperation.getSecurities().getIdSecurityCodePk(),ImportanceEventType.SECURITIES_EXCLUSION.getCode());
			}
			corpDetails = getContributionReturnCorporativeProcessDetails(corpOperation.getIdCorporativeOperationPk(), 
					 CorporateProcessConstants.BALANCE_DESTINY,CorporateProcessConstants.EXCHANGE_DESTINY,OperId);
		}else{
			Long stockProcess = getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
					corpOperation.getSecurities().getIdSecurityCodePk()
					, StockStateType.FINISHED.getCode(), StockClassType.NORMAL.getCode(), StockType.CORPORATIVE.getCode());
			corpDetails = getStockCorporativeProcessAdjustmentDetails(corpOperation.getIdCorporativeOperationPk(), 
					stockProcess, CorporateProcessConstants.BALANCE_DESTINY, CorporateProcessConstants.IND_NO_REMAINDER);
		}
		return corpDetails;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @param amount the amount
	 * @return the amount
	 */
	private BigDecimal getAmount(Object amount){
		if(amount != null && amount instanceof BigDecimal){
			return (BigDecimal)amount;
		}else{
			return BigDecimal.ZERO;
		}
	}
	
	/**
	 * Gets the holder name.
	 *
	 * @param corpOperationId the corp operation id
	 * @return the holder name
	 * @throws ServiceException the service exception
	 */
	private Map<Long, String> getHolderName(Long corpOperationId) throws ServiceException{
		Map<Long, String> holderNames = new HashMap<Long, String>();
		List<Object[]> result = getHolderNames(corpOperationId);
		if(result != null && !result.isEmpty()){
			for(Object[] data : result){
				holderNames.put((Long)data[0], (String)data[1]);
			}
		}
		return holderNames;
	}
	
	/**
	 * Gets the joint holders.
	 *
	 * @param corpProcessId the corp process id
	 * @param holderId the holder id
	 * @return the joint holders
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeProcessDetailsTO> getJointHolders(Long corpProcessId, Long holderId) throws ServiceException{
		List<CorporativeProcessDetailsTO> jointHolders = new ArrayList<CorporativeProcessDetailsTO>();
		List<Object> holders = getJointHolderDetails(corpProcessId, holderId);
		if(holders != null && !holders.isEmpty()){
			List<Long> holderids = new ArrayList<Long>();
			for(Object holder : holders){
				CorporativeProcessDetailsTO corpHolder = new CorporativeProcessDetailsTO();
				String holderCode = holder != null ? holder.toString() : "";
				corpHolder.setParticipant(holderCode);
				StringBuffer rnt = new StringBuffer();
				if(holderCode.contains("(")){
					holderCode = holderCode.replaceAll("\\(", "");
				}
				if(holderCode.contains(")")){
					holderCode = holderCode.replaceAll("\\)", "");
				}
				if(holderCode.contains(",")){
					String[] codes = holderCode.split("\\,");
					for(String code : codes){
						if(rnt.length() > 0){
							rnt.append("<br/>"); 
						}
						rnt.append(code);
						holderids.add(Long.valueOf(code));
					}
				}else{
					rnt.append(holderCode);
					holderids.add(Long.valueOf(holderCode));
				}
				corpHolder.setRnt(rnt.toString());
				jointHolders.add(corpHolder);
			}
			List<Object[]> holderNames = getHolderNames(holderids);
			Map<Long, String> holNames = new HashMap<Long, String>();
			if(holderNames != null && !holderNames.isEmpty()){
				for(Object[] data : holderNames){
					holNames.put((Long)data[0], (String)data[1]);
				}
			}
			
			for(CorporativeProcessDetailsTO details : jointHolders){
				String rnt[] = details.getRnt().split("<br/>");
				StringBuffer sb = new StringBuffer();
				for(String code : rnt){
					if(code != null && code.length() > 0){
						if(sb.length() > 0){
							sb.append("<br/>"); 
						}
						sb.append(holNames.get(Long.valueOf(code)));
					}
				}
				details.setName(sb.toString());
			}
		}
 		return jointHolders;
	}

/**
 * Gets the holder adjustment details.
 *
 * @param corpOperation the corp operation
 * @param security the security
 * @param idHolderPk the id holder pk
 * @return the holder adjustment details
 * @throws ServiceException the service exception
 */
public CorporativeProcessDetailsTO getHolderAdjustmentDetails(CorporativeOperation corpOperation, String security, String idHolderPk) throws ServiceException{
		
		CorporativeProcessDetailsTO result = new CorporativeProcessDetailsTO();
		List<CorporativeProcessDetailsTO> holderAdjustments = new ArrayList<CorporativeProcessDetailsTO>();
		List<Object[]> corpDetails = null;
		
		if(CorporateProcessUtils.isSpecialCorporateProcess(corpOperation.getCorporativeEventType().getCorporativeEventType())){
			corpDetails = getSpecialCorporativeProcessHolderDetails(corpOperation.getIdCorporativeOperationPk(), 
																						 CorporateProcessConstants.IND_NO_REMAINDER, security, idHolderPk);
		}else{
			
			Long stockProcess = getStockCalculationProcess(corpOperation.getRegistryDate(), corpOperation.getCutoffDate(), 
																				corpOperation.getSecurities().getIdSecurityCodePk(), 
																				StockStateType.FINISHED.getCode(), 
																				StockClassType.NORMAL.getCode(), 
																				StockType.CORPORATIVE.getCode());
			
			corpDetails = getStockCorporativeProcessHolderDetails(corpOperation.getIdCorporativeOperationPk(), 
																					   stockProcess, CorporateProcessConstants.BALANCE_DESTINY, 
																					   CorporateProcessConstants.IND_NO_REMAINDER, idHolderPk);
		}
		
		List<HolderAccountAdjusment> holderAccountAdj = getHolderAccountAdjustmentsForHolder(corpOperation.getIdCorporativeOperationPk(), 
																												  CorporateProcessConstants.DEFINITIVE_NOT_PROCESSED,
																												  security, idHolderPk);

		result.setAdjustedBalance(BigDecimal.ZERO);
		result.setCustodyAmount(BigDecimal.ZERO);
		result.setDestinyTotalBalance(BigDecimal.ZERO);
		result.setDestinyAvailableBalance(BigDecimal.ZERO);
		result.setNetAvailableBalance(BigDecimal.ZERO);
		result.setNetTotalBalance(BigDecimal.ZERO);
		result.setTaxAmount(BigDecimal.ZERO);
		result.setSourceAvailableBalance(BigDecimal.ZERO);
		result.setSourceTotalBalance(BigDecimal.ZERO);

		if(Validations.validateListIsNotNullAndNotEmpty(corpDetails)){
			
			for(Object[] corporative : corpDetails){
				CorporativeProcessDetailsTO corp = new CorporativeProcessDetailsTO();
				corp.setParticipant((String)corporative[0]);
				corp.setAccount((Integer)corporative[1]);
				corp.setSourceTotalBalance((BigDecimal)corporative[2]);
				corp.setSourceAvailableBalance((BigDecimal)corporative[3]);
				corp.setDestinyTotalBalance((BigDecimal)corporative[4]);
				corp.setDestinyAvailableBalance((BigDecimal)corporative[5]);
				corp.setTaxAmount(getAmount(corporative[7]));
				corp.setCustodyAmount(getAmount(corporative[8]));				
				corp.setHolderAccountPk((Long)corporative[6]);

				//WE VERIFY IF EXIST AN ADJUSTMENT FOR HOLDER ACCOUNT SELECTED
				HolderAccountAdjusment currentHolderAdjustment = null;
				for(HolderAccountAdjusment adjustment : holderAccountAdj){
					Long currentHA = adjustment.getHolderAccount().getIdHolderAccountPk();
					if(corp.getHolderAccountPk().equals(currentHA)){
						currentHolderAdjustment = adjustment;
						break;
					}
				}

				if(Validations.validateIsNotNull(currentHolderAdjustment)){
					
					corp.setAdjustedBalance(currentHolderAdjustment.getNewBenefitAmount());
					if(CorporateProcessConstants.IND_YES.equals(currentHolderAdjustment.getIndDeliverBenefit())){
						corp.setIndBalance(true);
						corp.setDestinyTotalBalance(currentHolderAdjustment.getOldBenefitAmount());
						result.setAdjustedBalance(result.getAdjustedBalance().add(corp.getAdjustedBalance()));
					}else{
						corp.setIndBalance(false);
					}
					
					corp.setNetTotalBalance(currentHolderAdjustment.getNewTax());
					if(CorporateProcessConstants.IND_YES.equals(currentHolderAdjustment.getIndCollectTax())){
						corp.setIndTax(true);
						result.setNetTotalBalance(result.getNetTotalBalance().add(corp.getNetTotalBalance()));
					}else{
						corp.setIndTax(false);
					}
					
					corp.setNetAvailableBalance(currentHolderAdjustment.getNewCommissiones());
					if(CorporateProcessConstants.IND_YES.equals(currentHolderAdjustment.getIndCollectCommissiones())){
						corp.setIndCustody(true);
						result.setNetAvailableBalance(result.getNetAvailableBalance().add(corp.getNetAvailableBalance()));
					}else{
						corp.setIndCustody(false);
					}
					
					corp.setHolderAdjustmentPk(currentHolderAdjustment.getIdHolderAccountAdjusmentPk());
				}else{
					corp.setAdjustedBalance(BigDecimal.ZERO);
					corp.setIndBalance(false);
					corp.setIndTax(false);
					corp.setIndCustody(false);
					corp.setNetTotalBalance(BigDecimal.ZERO);
					corp.setNetAvailableBalance(BigDecimal.ZERO);
					corp.setTotalBlockedBalance(BigDecimal.ZERO);
				}
				
				holderAdjustments.add(corp);
				result.setCustodyAmount(result.getCustodyAmount().add(corp.getCustodyAmount()));
				result.setDestinyAvailableBalance(result.getDestinyAvailableBalance().add(result.getDestinyAvailableBalance()));
				result.setDestinyTotalBalance(result.getDestinyTotalBalance().add(corp.getDestinyTotalBalance()));
				result.setSourceAvailableBalance(result.getSourceAvailableBalance().add(corp.getSourceAvailableBalance()));
				result.setSourceTotalBalance(result.getSourceTotalBalance().add(corp.getSourceTotalBalance()));
				result.setTaxAmount(result.getTaxAmount().add(corp.getTaxAmount()));					
			}
			
		}
		
		result.setDetails(holderAdjustments);
		
		return result;
	}

	/**
	 * Actualize holder adjustments.
	 *
	 * @param details the details
	 * @param corpOper the corp oper
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizeHolderAdjustments(List<CorporativeProcessDetailsTO> details, CorporativeOperation corpOper, String security) throws ServiceException{
		if(details != null && !details.isEmpty()){
			List<Long> holderAdjDelete = new ArrayList<Long>();
			for(CorporativeProcessDetailsTO corpDetail : details){
				if(corpDetail.getHolderAdjustmentPk() != null && corpDetail.getHolderAdjustmentPk() > 0){
					if(!saveOrUpdateHolderAdjustments(corpDetail, corpOper, security, true)){
						holderAdjDelete.add(corpDetail.getHolderAdjustmentPk());						
					}
				}else{
					saveOrUpdateHolderAdjustments(corpDetail, corpOper, security, false);
				}
			}
			if(!holderAdjDelete.isEmpty()){
				deleteHolderAdjustments(holderAdjDelete);
			}
		}
		
	}
	
	/**
	 * Save or update holder adjustments.
	 *
	 * @param corpDetail the corp detail
	 * @param corpOper the corp oper
	 * @param security the security
	 * @param update the update
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private boolean saveOrUpdateHolderAdjustments(CorporativeProcessDetailsTO corpDetail,CorporativeOperation corpOper, String security, boolean update)throws ServiceException{
		HolderAccountAdjusment holAdj = new HolderAccountAdjusment();
		holAdj.setCorporativeOperation(corpOper);
		holAdj.setHolderAccount(new HolderAccount(corpDetail.getHolderAccountPk()));
		holAdj.setAdjustmentState(CorporateProcessConstants.IND_NO);
		holAdj.setIndDeliverBenefit(CorporateProcessConstants.IND_NO);
		holAdj.setIndCollectTax(CorporateProcessConstants.IND_NO);
		holAdj.setIndCollectCommissiones(CorporateProcessConstants.IND_NO);
		if(security != null && security.length() > 0){
			holAdj.setSecurity(new Security(security));
		}else{
			holAdj.setSecurity(corpOper.getSecurities());
		}
		if(corpDetail.isIndBalance()){
			holAdj.setIndDeliverBenefit(CorporateProcessConstants.IND_YES);
			holAdj.setNewBenefitAmount(corpDetail.getAdjustedBalance());
			holAdj.setOldBenefitAmount(corpDetail.getDestinyTotalBalance());
		}
		if(corpDetail.isIndTax()){
			holAdj.setIndCollectTax(CorporateProcessConstants.IND_YES);
			holAdj.setNewTax(corpDetail.getNetTotalBalance());
			holAdj.setOldTax(corpDetail.getTaxAmount());
		}
		if(corpDetail.isIndCustody()){
			holAdj.setIndCollectCommissiones(CorporateProcessConstants.IND_YES);
			holAdj.setNewCommissiones(corpDetail.getNetAvailableBalance());
			holAdj.setOldCommisiones(corpDetail.getCustodyAmount());
		}
		if(update){
			if(corpDetail.isIndCustody() || corpDetail.isIndTax() || corpDetail.isIndBalance()){
				holAdj.setIdHolderAccountAdjusmentPk(corpDetail.getHolderAdjustmentPk());
				update(holAdj);
			}else{
				return false;
			}
		}else{
			create(holAdj);
		}
		return true;
		

	}	
	
	/**
	 * get Quantity Movements For Security.
	 *
	 * @param idSecurity the id security
	 * @return Quantity Movements For Security
	 * @throws ServiceException the Service Exception
	 */
	public Long getQuantityMovementsForSecurity(String idSecurity)throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append(" select count(ham) ");
		sbQuery.append(" from HolderAccountMovement ham ");
		sbQuery.append(" where ham.holderAccountBalance.security.idSecurityCodePk = :idSecurityCodePk ");
		parameters.put("idSecurityCodePk", idSecurity);	
		return (Long)findObjectByQueryString(sbQuery.toString(), parameters);	
	}
	
	/**
	 * Get List Securities Stock By Record Date.
	 *
	 * @param dateCurrent date current
	 * @param state state
	 * @return List Securities Stock By Record Date
	 * @throws ServiceException the Service Exception
	 */
	public LinkedHashMap<String,String> getListSecuritiesStockByRecordDate(Date dateCurrent,Integer state) throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Object obj = null;
		List<String> lstSecurity = null;
		LinkedHashMap<String,String> lstSecurityStock=null;
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append(" select distinct scb.security.idSecurityCodePk ");
		sbQuery.append(" from StockCalculationProcess scp,StockCalculationBalance scb ");
		sbQuery.append(" where scp.idStockCalculationPk = scb.stockCalculationProcess.idStockCalculationPk ");
		sbQuery.append(" and scp.stockState = :stockState ");
		sbQuery.append(" and scp.stockClass = :stockClass ");
		sbQuery.append(" and scp.stockType = :stockType ");
		sbQuery.append(" and trunc(scp.creationDate) = trunc(:creationDate) ");
		parameters.put("stockState",state);	
		parameters.put("stockClass", StockClassType.NORMAL.getCode());	
		parameters.put("stockType", StockType.CORPORATIVE.getCode());	
		parameters.put("creationDate", dateCurrent);	
		obj = findListByQueryString(sbQuery.toString(), parameters);
		if(obj!=null){
			lstSecurity = (List<String>)obj;
			lstSecurityStock=new LinkedHashMap<String, String>();
			for(String idSecurity : lstSecurity){
				lstSecurityStock.put(idSecurity, idSecurity);
			}
		}
		return lstSecurityStock;
	}
	
	/**
	 * Get Stock By Security And Record Date.
	 *
	 * @param security security
	 * @param dateCurrent date current
	 * @param state state
	 * @return id Stock By Security And Record Date
	 * @throws ServiceException the Service Exception
	 */
	public Long getStockBySecurityAndRecordDate(String security,Date dateCurrent,Integer state) throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append(" select distinct(scp.idStockCalculationPk) ");
		sbQuery.append(" from StockCalculationProcess scp,StockCalculationBalance scb ");
		sbQuery.append(" where scp.idStockCalculationPk = scb.stockCalculationProcess.idStockCalculationPk ");
		sbQuery.append(" and scp.stockState = :stockState ");
		sbQuery.append(" and scp.stockClass = :stockClass ");
		sbQuery.append(" and scp.stockType = :stockType ");
		sbQuery.append(" and scp.security.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append(" and trunc(scp.creationDate) = trunc(:creationDate) ");
		parameters.put("stockState",state);	
		parameters.put("stockClass", StockClassType.NORMAL.getCode());	
		parameters.put("stockType", StockType.CORPORATIVE.getCode());	
		parameters.put("idSecurityCodePk", security);	
		parameters.put("creationDate", dateCurrent);	
		return (Long)findObjectByQueryString(sbQuery.toString(), parameters);	
	}
	
	
	/**
	 * Gets the corporate operation for filters.
	 *
	 * @param cutoffDate the cutoff date
	 * @param deliveryDate the delivery date
	 * @param securityCode the security code
	 * @param eventType the event type
	 * @param state the state
	 * @return the corporate operation for filters
	 * @throws ServiceException the service exception
	 */
	public CorporativeOperation getCorporateOperationForFilters(Date cutoffDate , Date deliveryDate,String securityCode,Integer eventType,Integer state) throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append(" select co ");
		sbQuery.append(" from CorporativeOperation co INNER JOIN FETCH co.corporativeEventType ");
		sbQuery.append(" where co.corporativeEventType.corporativeEventType= :eventType ");
		sbQuery.append(" and co.cutoffDate = :cutoffDate ");
		sbQuery.append(" and co.deliveryDate = :deliveryDate ");
		sbQuery.append(" and co.registryDate = :registryDate ");
		sbQuery.append(" and co.state = :state ");
		sbQuery.append(" and co.securities.idSecurityCodePk = :idSecurityCodePk ");
		parameters.put("cutoffDate",cutoffDate);	
		parameters.put("deliveryDate", deliveryDate);	
		parameters.put("registryDate", cutoffDate);	
		parameters.put("eventType", eventType);	
		parameters.put("state", state);	
		parameters.put("idSecurityCodePk",securityCode);
		return (CorporativeOperation)findObjectByQueryString(sbQuery.toString(), parameters);	
	}
	
	/**
	 * Get List Securities Stock By Record Date.
	 *
	 * @param dateCurrent date current
	 * @param state state
	 * @return List Securities Stock By Record Date
	 * @throws ServiceException the Service Exception
	 */
	public boolean findSecuritiesStockWithoutPortfolioByRecordDate(String security,Date dateCurrent,Integer state) throws ServiceException{
		StringBuffer sbQuery=new StringBuffer();
		Object obj = null;
		List<String> lstSecurity = null;
		Map<String, Object>  parameters=new HashMap<String, Object>();
		sbQuery.append(" select distinct scp.security.idSecurityCodePk ");
		sbQuery.append(" from StockCalculationProcess scp  ");
		sbQuery.append(" where  ");
		sbQuery.append(" scp.stockState = :stockState ");
		sbQuery.append(" and scp.stockClass = :stockClass ");
		sbQuery.append(" and scp.stockType = :stockType ");
		sbQuery.append(" and scp.security.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append(" and trunc(scp.creationDate) = trunc(:creationDate) ");
		parameters.put("stockState",state);	
		parameters.put("stockClass", StockClassType.NORMAL.getCode());	
		parameters.put("stockType", StockType.CORPORATIVE.getCode());	
		parameters.put("creationDate", dateCurrent);
		parameters.put("idSecurityCodePk", security);
		obj = findListByQueryString(sbQuery.toString(), parameters);
		if(obj!=null){
			lstSecurity = (List<String>)obj;
			if(Validations.validateListIsNotNullAndNotEmpty(lstSecurity)){
				return true;
			}
		}
		return false;
	}
}