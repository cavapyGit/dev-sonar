package com.pradera.corporateevents.corporativeprocess.consignation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.corporateevents.corporativeprocess.consignation.facade.FilePayrollConsignedServiceFacade;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.corporatives.FilePayrollConsigned;
import com.pradera.model.corporatives.FilePayrollConsignedDet;
import com.pradera.model.corporatives.PayrollConsignedOperation;
import com.pradera.model.corporatives.type.FilePayrollType;
import com.pradera.model.corporatives.type.PayrollStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * Clase de funcionalidad y administracion de eventos de la pantalla de consignacion de pagos.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 31/10/2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class FilePayrollConsignedMgmtBean extends GenericBaseBean implements Serializable{

	/** The allocation payment facade. */
	@EJB
	private FilePayrollConsignedServiceFacade filePayrollConsignedServiceFacade;
	
	@EJB
	private AccountsFacade accountsFacade;
	
	@Inject
	private GeneralParametersFacade generalParametersFacade;

	private FilePayrollConsigned filePayrollConsignedSearch;

	private FilePayrollConsigned filePayrollConsignedRegister;
	
	@Inject
	private UserInfo userInfo;
	
	private GenericDataModel<FilePayrollConsigned> lstDtFilePayrollConsigned;
	
	private GenericDataModel<PayrollConsignedOperation> lstDtPayrollConsignedOperation;
	
	private GenericDataModel<FilePayrollConsignedDet> lstDtFilePayrollConsigDet;
	
	private PayrollConsignedOperation[] payrollConsignedOperations;
	
	private FilePayrollConsignedDet[] filePayrollConsignedDets;
	
	private List<ParameterTable> lstCurrency;	

	private List<ParameterTable> lstFilePayrollTypes;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	private Date currentDate;
	
	@PostConstruct
	public void init() throws ServiceException{
		
		filePayrollConsignedSearch= new FilePayrollConsigned();
		filePayrollConsignedSearch.setIssuer(new Issuer());
		filePayrollConsignedSearch.setParticipant(new Participant());
		filePayrollConsignedSearch.setInitialDate(CommonsUtilities.currentDate());
		filePayrollConsignedSearch.setFinalDate(CommonsUtilities.currentDate());
		initialVariables();
		
	}

	public void initialVariables() throws ServiceException {
		ParameterTableTO processStateTo = new ParameterTableTO();
		processStateTo.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		processStateTo.setState(1);
		lstCurrency = generalParametersFacade.getListParameterTableServiceBean(processStateTo); 
		
		processStateTo.setMasterTableFk(MasterTableType.PAYROLL_FILE_TYPE.getCode());
		lstFilePayrollTypes = generalParametersFacade.getListParameterTableServiceBean(processStateTo); 
		
		loadParticipants();
		currentDate = CommonsUtilities.currentDate();
	}
	
	public void loadParticipants() throws ServiceException {
		Participant participantFilter = new Participant();		
		participantFilter.setState(ParticipantStateType.REGISTERED.getCode());
		
		participantList = accountsFacade.getLisParticipantServiceBean(participantFilter);
	}
	/**
	 * Metodo que busca procesos corporativos en estado definitivo y consignaciones en estado preliminar y definitivo.
	 */
	@LoggerAuditWeb
	public void searchFilePayrollCondigned(){
		try {
				 List<FilePayrollConsigned> lstFilePayrollConsigned = filePayrollConsignedServiceFacade.searchFilePayroll(filePayrollConsignedSearch);
				 if(lstFilePayrollConsigned!=null){
					 lstDtFilePayrollConsigned = new GenericDataModel<FilePayrollConsigned>(lstFilePayrollConsigned);
				 }
		} catch (ServiceException esx) {
			if(esx.getMessage()!=null){
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}

	@LoggerAuditWeb
	public void searchPayrollOperationConsigned(){
		try {
			if(Validations.validateIsNullOrEmpty(filePayrollConsignedRegister.getCurrency())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.CURRENCY_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			PayrollConsignedOperation obj = new PayrollConsignedOperation();
			if(filePayrollConsignedRegister.getCurrency()!=null) {
				obj.setCurrencyPayment(filePayrollConsignedRegister.getCurrency());
			} 
			obj.setIssuer(new Issuer());
			obj.setParticipant(new Participant());			
				
			if(Validations.validateIsNotNull(filePayrollConsignedRegister.getFilePayrollType())) {
				if(filePayrollConsignedRegister.getFilePayrollType().equals(FilePayrollType.PAGO_DERECHO.getCode())) {
					if(Validations.validateIsNotNullAndNotEmpty(filePayrollConsignedRegister.getIssuer().getIdIssuerPk())){						
						
						if(filePayrollConsignedRegister.getIssuer()!=null) {
							obj.setIssuer(filePayrollConsignedRegister.getIssuer());
						}
						obj.setState(PayrollStateType.REGISTERED.getCode());
						obj.setInitialDate(filePayrollConsignedRegister.getInitialDate());
						obj.setFinalDate(filePayrollConsignedRegister.getFinalDate());
						
						filePayrollConsignedServiceFacade.executePayrollPendingGenerate(obj);
						
						List<PayrollConsignedOperation> lstPayrollConsignedOper = filePayrollConsignedServiceFacade.searchPayrollConsignedOper(obj);
						if(lstPayrollConsignedOper!=null){
							 lstDtPayrollConsignedOperation = new GenericDataModel<PayrollConsignedOperation>(lstPayrollConsignedOper);
						}
					}else{
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_NOT_SELECTED));
						JSFUtilities.showSimpleValidationDialog();
					}
				} else if(filePayrollConsignedRegister.getFilePayrollType().equals(FilePayrollType.LIQUIDACION.getCode())) {
					if(Validations.validateIsNotNullAndNotEmpty(filePayrollConsignedRegister.getParticipant().getIdParticipantPk())){
						
						if(filePayrollConsignedRegister.getParticipant()!=null) {
							obj.setParticipant(filePayrollConsignedRegister.getParticipant());
						}
						obj.setState(PayrollStateType.REGISTERED.getCode());
						obj.setInitialDate(filePayrollConsignedRegister.getInitialDate());
						obj.setFinalDate(filePayrollConsignedRegister.getFinalDate());
						
						filePayrollConsignedServiceFacade.executeSettlementPayrollPendingGenerate(obj);
						
						List<PayrollConsignedOperation> lstPayrollConsignedOper = filePayrollConsignedServiceFacade.searchPayrollConsignedOper(obj);
						if(lstPayrollConsignedOper!=null){
							 lstDtPayrollConsignedOperation = new GenericDataModel<PayrollConsignedOperation>(lstPayrollConsignedOper);
						}
					} else {
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.PARTICIPANT_NOT_SELECTED));
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			}
		} catch (ServiceException esx) {
			if(esx.getMessage()!=null){
				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}		
	}
	
	public void registryFilePayrollConsigned() {
		filePayrollConsignedRegister = new FilePayrollConsigned();
		filePayrollConsignedRegister.setIssuer(new Issuer());
		filePayrollConsignedRegister.setParticipant(new Participant());
		filePayrollConsignedRegister.setInitialDate(CommonsUtilities.currentDate());
		filePayrollConsignedRegister.setFinalDate(CommonsUtilities.currentDate());
		lstDtPayrollConsignedOperation  = new GenericDataModel<>(null);
		lstDtFilePayrollConsigDet = new GenericDataModel<>();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		
	}
	
	public String confirmFilePayrollConsigperation() throws ServiceException{
		if(filePayrollConsignedRegister!=null) {
			//if(filePayrollConsignedRegister.getState().equals(FilePayrollStateType.REGISTERED.getCode())) {
			if( !filePayrollConsignedRegister.getOperationProcessed().equals(filePayrollConsignedRegister.getOperationTotal()) ){	
				List<FilePayrollConsignedDet> lst = filePayrollConsignedServiceFacade.searchFilePayrollDet(filePayrollConsignedRegister);
				lstDtFilePayrollConsigDet  = new GenericDataModel<>(lst);
				setViewOperationType(ViewOperationsType.CONFIRM.getCode());
				filePayrollConsignedDets = null;
				return "confirmFilePayrollCondigned";
			}else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						"Ya fueron confirmados todos los registros");
				JSFUtilities.showSimpleValidationDialog();	
			}
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.FILE_PAYROLL_OPERATION_REGISTER));
			JSFUtilities.showSimpleValidationDialog();	
		}
		return "";
	}
	

	public void generateFilePayrollConsigned() throws ServiceException{
		if(filePayrollConsignedRegister!=null) {
			List<FilePayrollConsignedDet> lst = filePayrollConsignedServiceFacade.searchFilePayrollDet(filePayrollConsignedRegister);
			lstDtFilePayrollConsigDet  = new GenericDataModel<>(lst);
			JSFUtilities.showComponent(":idCnfSave");
			showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null,PropertiesConstants.MESSAGE_CONFIRM_FILE_PYROLL, null);
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.FILE_PAYROLL_OPERATION_REGISTER));
			JSFUtilities.showSimpleValidationDialog();	
		}
	}
	
	public String stateDescription(Integer state) {
		if(PayrollStateType.get(state)!=null) {
			return PayrollStateType.get(state).getValue();
		}
		return "";
	}
	
	public String detailFilePayroll(FilePayrollConsigned filePayroll) throws ServiceException {
		if(filePayroll!=null) {
			filePayrollConsignedRegister = filePayroll;
			List<FilePayrollConsignedDet> lst = filePayrollConsignedServiceFacade.searchFilePayrollDet(filePayrollConsignedRegister);
			lstDtFilePayrollConsigDet  = new GenericDataModel<>(lst);
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			return "confirmFilePayrollCondigned";
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.FILE_PAYROLL_OPERATION_REGISTER));
			JSFUtilities.showSimpleValidationDialog();	
		}
		return "";
	}
	
	public void cleanFilePayrollSearch() throws ServiceException{
		filePayrollConsignedSearch = new FilePayrollConsigned();
		filePayrollConsignedSearch.setIssuer(new Issuer());
		filePayrollConsignedSearch.setParticipant(new Participant());
		lstDtFilePayrollConsigned = new GenericDataModel<>();
		filePayrollConsignedSearch.setInitialDate(CommonsUtilities.currentDate());
		filePayrollConsignedSearch.setFinalDate(CommonsUtilities.currentDate());
	}
	
	public void cleanFilePayrollRegister() throws ServiceException{
		filePayrollConsignedRegister = new FilePayrollConsigned();
		filePayrollConsignedRegister.setIssuer(new Issuer());
		filePayrollConsignedRegister.setParticipant(new Participant());
		lstDtPayrollConsignedOperation = new GenericDataModel<>();
	}
	/**
	 * Metodo que visualiza lista detalle de consignacion de pagos.
	 */
	public void showAllocateProcess(){
//		try {
//			if(Validations.validateIsNull(indCorporativeAllocate)){
//				lstCorporativeAllocate = allocationPaymentFacade.getCorporativeToAllocate(allocateProcessIssuer,allocationPaymentSearch.getPaymentDate(), allocationPaymentSearch.getCurrency());
//				if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeAllocate)){
//					if(Validations.validateIsNull(indCorporativeAllocate)){
//						indCorporativeAllocate = 1;
//					}else{
//						indCorporativeAllocate = 0;
//					}
//				}else{
//					indCorporativeAllocate = 0;
//				}
//			}else{
//				indCorporativeAllocate = null;
//			}
//		} catch (ServiceException esx) {
//			if(esx.getMessage()!=null){
//				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
//				showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, esx.getErrorService().getMessage(), esx.getParams());
//				JSFUtilities.showSimpleValidationDialog();
//			}
//		} catch (Exception ex) {
//			excepcion.fire(new ExceptionToCatchEvent(ex));
//		}
	}


	public String backScreen() throws ServiceException{
		return "backAllocationPaymentManagement";
	}

	/**
	 * Metodo que regresa a la pantalla principal
	 *
	 * @return the string
	 */
	public String backFromDetail(){
		return "backManagement";
	}

	public void validateIssuerSearch() {
		
	}

	public void beforeFilePayrollCondigned() throws ServiceException{
		if(payrollConsignedOperations!=null && payrollConsignedOperations.length>0){
			JSFUtilities.showComponent(":idCnfSave");
			showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null,PropertiesConstants.MESSAGE_CONFIRM_FILE_PYROLL, null);
//			JSFUtilities.executeJavascriptFunction("PF('idCnfSave').show()");
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.FILE_PAYROLL_OPERATION_REGISTER));
			JSFUtilities.showSimpleValidationDialog();	
		}
	}
	
	public void beforeConfirmFilePayrollCondigned() throws ServiceException{
		if(filePayrollConsignedDets!=null && filePayrollConsignedDets.length>0){
			JSFUtilities.showComponent(":idCnfSave");
			showMessageOnDialog(PropertiesConstants.BEFORE_SAVE_HEADER, null,PropertiesConstants.MESSAGE_CONFIRM_FILE_PYROLL_CONFIRM, null);
//			JSFUtilities.executeJavascriptFunction("PF('idCnfSave').show()");
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					PropertiesUtilities.getMessage(PropertiesConstants.FILE_PAYROLL_OPERATION_REGISTER));
			JSFUtilities.showSimpleValidationDialog();	
		}
	}
	
	
	/**
	 * Metodo que registra consignacion de pagos.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void saveFilePayrollCondigned() throws ServiceException{ //grabar
//		try{
			List<PayrollConsignedOperation> lstPayrollConsignedOperation = new ArrayList<PayrollConsignedOperation>();
		if(payrollConsignedOperations!=null && payrollConsignedOperations.length>0){	
			for (PayrollConsignedOperation obj: payrollConsignedOperations) {
				lstPayrollConsignedOperation.add(obj);
			}
			filePayrollConsignedServiceFacade.saveFilePayroll(lstPayrollConsignedOperation, filePayrollConsignedRegister);
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				"Se registro satisfactoriamente el archivo de pagos");
		
		JSFUtilities.showComponent(":idDialDepositary");
	}
	
	@LoggerAuditWeb
	public void confirmFilePayrollCondigned() throws ServiceException{
//		try{
			List<FilePayrollConsignedDet> lstFilePayrollConsignedDet = new ArrayList<FilePayrollConsignedDet>();
		if(filePayrollConsignedDets!=null && filePayrollConsignedDets.length>0){	
			for (FilePayrollConsignedDet obj: filePayrollConsignedDets) {
				lstFilePayrollConsignedDet.add(obj);
			}
			filePayrollConsignedRegister.setFilePayrollConsignedDets(lstDtFilePayrollConsigDet.getDataList());
			filePayrollConsignedServiceFacade.confirmFilePayroll(filePayrollConsignedRegister,lstFilePayrollConsignedDet);
		}

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				"Se confirmo satisfactoriamente el archivo de pagos");
		
		JSFUtilities.showComponent(":idDialDepositary");
	}

	public FilePayrollConsigned getFilePayrollConsignedSearch() {
		return filePayrollConsignedSearch;
	}

	public void setFilePayrollConsignedSearch(FilePayrollConsigned filePayrollConsignedSearch) {
		this.filePayrollConsignedSearch = filePayrollConsignedSearch;
	}

	public FilePayrollConsigned getFilePayrollConsignedRegister() {
		return filePayrollConsignedRegister;
	}

	public void setFilePayrollConsignedRegister(FilePayrollConsigned filePayrollConsignedRegister) {
		this.filePayrollConsignedRegister = filePayrollConsignedRegister;
	}

	public GenericDataModel<FilePayrollConsigned> getLstDtFilePayrollConsigned() {
		return lstDtFilePayrollConsigned;
	}

	public void setLstDtFilePayrollConsigned(GenericDataModel<FilePayrollConsigned> lstDtFilePayrollConsigned) {
		this.lstDtFilePayrollConsigned = lstDtFilePayrollConsigned;
	}

	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public GenericDataModel<PayrollConsignedOperation> getLstDtPayrollConsignedOperation() {
		return lstDtPayrollConsignedOperation;
	}

	public void setLstDtPayrollConsignedOperation(
			GenericDataModel<PayrollConsignedOperation> lstDtPayrollConsignedOperation) {
		this.lstDtPayrollConsignedOperation = lstDtPayrollConsignedOperation;
	}

	public PayrollConsignedOperation[] getPayrollConsignedOperations() {
		return payrollConsignedOperations;
	}

	public void setPayrollConsignedOperations(PayrollConsignedOperation[] payrollConsignedOperations) {
		this.payrollConsignedOperations = payrollConsignedOperations;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public GenericDataModel<FilePayrollConsignedDet> getLstDtFilePayrollConsigDet() {
		return lstDtFilePayrollConsigDet;
	}

	public void setLstDtFilePayrollConsigDet(GenericDataModel<FilePayrollConsignedDet> lstDtFilePayrollConsigDet) {
		this.lstDtFilePayrollConsigDet = lstDtFilePayrollConsigDet;
	}

	public FilePayrollConsignedDet[] getFilePayrollConsignedDets() {
		return filePayrollConsignedDets;
	}

	public void setFilePayrollConsignedDets(FilePayrollConsignedDet[] filePayrollConsignedDets) {
		this.filePayrollConsignedDets = filePayrollConsignedDets;
	}

	public List<ParameterTable> getLstFilePayrollTypes() {
		return lstFilePayrollTypes;
	}

	public void setLstFilePayrollTypes(List<ParameterTable> lstFilePayrollTypes) {
		this.lstFilePayrollTypes = lstFilePayrollTypes;
	}

	public List<Participant> getParticipantList() {
		return participantList;
	}

	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

}