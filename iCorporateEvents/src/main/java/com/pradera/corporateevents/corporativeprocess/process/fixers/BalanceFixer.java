package com.pradera.corporateevents.corporativeprocess.process.fixers;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.DailyExchangeRates;

// TODO: Auto-generated Javadoc
/**
 * The Interface BalanceFixer.
 *
 * @author : Pradera
 */
public interface BalanceFixer {
	
	/**
	 * Metodo que fija saldos.
	 *
	 * @param holderAccounts the holder accounts
	 * @param objCorporativeOperation the obj corporative operation
	 * @param delFactor the del factor
	 * @param round the round
	 * @param cashFlag the cash flag
	 * @param remnant the remnant
	 * @param destiny the destiny
	 * @param processType the process type
	 * @param exchangeRateDay the exchange rate day
	 * @return the list
	 */
	public List<CorporativeProcessResult> fixBalances(List<Object[]> holderAccounts,
			CorporativeOperation objCorporativeOperation, BigDecimal delFactor,
			int round, boolean cashFlag, boolean remnant, boolean destiny, ImportanceEventType processType,DailyExchangeRates exchangeRateDay);

	/**
	 * Metodo que ajusta saldos.
	 *
	 * @param balances the balances
	 * @param result the result
	 * @param total the total
	 * @return the big decimal[]
	 */
	public BigDecimal[] matchBalances(BigDecimal[] balances, BigDecimal result,
			BigDecimal total);

	/**
	 * Metodo que fija saldos.
	 *
	 * @param corpProcessResult the corp process result
	 * @return the corporative process result
	 */
	public CorporativeProcessResult fixBalance(CorporativeProcessResult corpProcessResult);
	
	/**
	 * Metodo que retorna lista de preliminares origen.
	 *
	 * @param holderAccounts the holder accounts
	 * @param corpOper the corp oper
	 * @return the list
	 */
	public List<CorporativeProcessResult> fillProcessResultsForOrigin(List<Object[]> holderAccounts, CorporativeOperation corpOper);
	
	/**
	 * Metodo que genera datos necesarios para ejecutar el componente de movimientos de saldos.
	 *
	 * @param holderAccounts the holder accounts
	 * @param processType the process type
	 * @param corpOperation the corp operation
	 * @return the accounts component to
	 */
	public AccountsComponentTO generateAccountComponent(List<Object[]> holderAccounts, 
			ImportanceEventType processType, CorporativeOperation corpOperation);
	
	/**
	 * Sets the logger user for transation registry.
	 *
	 * @return true, if successful
	 */
	public boolean setLoggerUserForTransationRegistry();
}
