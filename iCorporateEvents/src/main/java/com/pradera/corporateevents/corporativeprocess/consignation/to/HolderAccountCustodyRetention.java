package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;



// TODO: Auto-generated Javadoc
/**
 * The Class HolderAccountCustodyRetention.
 * @author Pradera Technologies
 *
 */
public class HolderAccountCustodyRetention implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder account. */
	private Long holderAccount;

	/** The primary balance. */
	private BigDecimal primaryBalance;
	
	/** The secondary balance. */
	private BigDecimal secondaryBalance;

	/** The previous primary balance. */
	private BigDecimal previousPrimaryBalance;
	
	/** The previous secondary balance. */
	private BigDecimal previousSecondaryBalance;

	/** The primary custody commission. */
	private BigDecimal primaryCustodyCommission;
	
	/** The secondary custody commission. */
	private BigDecimal secondaryCustodyCommission;
	
	/** The movements. */
	List<Object[]> movements;


	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the primary balance.
	 *
	 * @return the primary balance
	 */
	public BigDecimal getPrimaryBalance() {
		return primaryBalance;
	}
	
	/**
	 * Sets the primary balance.
	 *
	 * @param primaryBalance the new primary balance
	 */
	public void setPrimaryBalance(BigDecimal primaryBalance) {
		this.primaryBalance = primaryBalance;
	}
	
	/**
	 * Gets the secondary balance.
	 *
	 * @return the secondary balance
	 */
	public BigDecimal getSecondaryBalance() {
		return secondaryBalance;
	}
	
	/**
	 * Sets the secondary balance.
	 *
	 * @param secondaryBalance the new secondary balance
	 */
	public void setSecondaryBalance(BigDecimal secondaryBalance) {
		this.secondaryBalance = secondaryBalance;
	}
	
	/**
	 * Gets the primary custody commission.
	 *
	 * @return the primary custody commission
	 */
	public BigDecimal getPrimaryCustodyCommission() {
		return primaryCustodyCommission;
	}
	
	/**
	 * Sets the primary custody commission.
	 *
	 * @param primaryCustodyCommission the new primary custody commission
	 */
	public void setPrimaryCustodyCommission(BigDecimal primaryCustodyCommission) {
		this.primaryCustodyCommission = primaryCustodyCommission;
	}
	
	/**
	 * Gets the secondary custody commission.
	 *
	 * @return the secondary custody commission
	 */
	public BigDecimal getSecondaryCustodyCommission() {
		return secondaryCustodyCommission;
	}
	
	/**
	 * Sets the secondary custody commission.
	 *
	 * @param secondaryCustodyCommission the new secondary custody commission
	 */
	public void setSecondaryCustodyCommission(BigDecimal secondaryCustodyCommission) {
		this.secondaryCustodyCommission = secondaryCustodyCommission;
	}
	
	/**
	 * Gets the movements.
	 *
	 * @return the movements
	 */
	public List<Object[]> getMovements() {
		return movements;
	}
	
	/**
	 * Sets the movements.
	 *
	 * @param movements the new movements
	 */
	public void setMovements(List<Object[]> movements) {
		this.movements = movements;
	}
	
	/**
	 * Gets the previous primary balance.
	 *
	 * @return the previous primary balance
	 */
	public BigDecimal getPreviousPrimaryBalance() {
		return previousPrimaryBalance;
	}
	
	/**
	 * Sets the previous primary balance.
	 *
	 * @param previousPrimaryBalance the new previous primary balance
	 */
	public void setPreviousPrimaryBalance(BigDecimal previousPrimaryBalance) {
		this.previousPrimaryBalance = previousPrimaryBalance;
	}
	
	/**
	 * Gets the previous secondary balance.
	 *
	 * @return the previous secondary balance
	 */
	public BigDecimal getPreviousSecondaryBalance() {
		return previousSecondaryBalance;
	}
	
	/**
	 * Sets the previous secondary balance.
	 *
	 * @param previousSecondaryBalance the new previous secondary balance
	 */
	public void setPreviousSecondaryBalance(BigDecimal previousSecondaryBalance) {
		this.previousSecondaryBalance = previousSecondaryBalance;
	}
}