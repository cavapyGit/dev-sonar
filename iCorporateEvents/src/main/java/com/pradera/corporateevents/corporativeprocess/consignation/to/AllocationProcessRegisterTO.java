package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.io.Serializable;
import java.util.List;



// TODO: Auto-generated Javadoc
/**
 * The Class AllocationProcessRegisterTO.
 * @author PERU Pradera Technologies
 *
 */
public class AllocationProcessRegisterTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The issuer description. */
	private String issuerDescription;
	
	/** The issuer. */
	private String issuer;
	
	/** The lst issuer allocations. */
	private List<AllocationCorporativeDetailTO> lstIssuerAllocations;
	
	/** The lst issuer cash account. */
	private List<AllocateIssuerCashAccountTO> lstIssuerCashAccount;
	
	/** The corporative to allocate. */
	private AllocationCorporativeDetailTO[] corporativeToAllocate;
	
	/** The corporative allocate. */
	private AllocationCorporativeDetailTO corporativeAllocate;

	/**
	 * Gets the lst issuer allocations.
	 *
	 * @return the lst issuer allocations
	 */
	public List<AllocationCorporativeDetailTO> getLstIssuerAllocations() {
		return lstIssuerAllocations;
	}
	
	/**
	 * Sets the lst issuer allocations.
	 *
	 * @param lstIssuerAllocations the new lst issuer allocations
	 */
	public void setLstIssuerAllocations(
			List<AllocationCorporativeDetailTO> lstIssuerAllocations) {
		this.lstIssuerAllocations = lstIssuerAllocations;
	}
	
	/**
	 * Gets the lst issuer cash account.
	 *
	 * @return the lst issuer cash account
	 */
	public List<AllocateIssuerCashAccountTO> getLstIssuerCashAccount() {
		return lstIssuerCashAccount;
	}
	
	/**
	 * Sets the lst issuer cash account.
	 *
	 * @param lstIssuerCashAccount the new lst issuer cash account
	 */
	public void setLstIssuerCashAccount(
			List<AllocateIssuerCashAccountTO> lstIssuerCashAccount) {
		this.lstIssuerCashAccount = lstIssuerCashAccount;
	}
	
	/**
	 * Gets the issuer description.
	 *
	 * @return the issuer description
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}
	
	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the new issuer description
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}
	
	/**
	 * Gets the corporative to allocate.
	 *
	 * @return the corporative to allocate
	 */
	public AllocationCorporativeDetailTO[] getCorporativeToAllocate() {
		return corporativeToAllocate;
	}
	
	/**
	 * Sets the corporative to allocate.
	 *
	 * @param corporativeToAllocate the new corporative to allocate
	 */
	public void setCorporativeToAllocate(
			AllocationCorporativeDetailTO[] corporativeToAllocate) {
		this.corporativeToAllocate = corporativeToAllocate;
	}
	
	/**
	 * Gets the corporative allocate.
	 *
	 * @return the corporative allocate
	 */
	public AllocationCorporativeDetailTO getCorporativeAllocate() {
		return corporativeAllocate;
	}
	
	/**
	 * Sets the corporative allocate.
	 *
	 * @param corporativeAllocate the new corporative allocate
	 */
	public void setCorporativeAllocate(
			AllocationCorporativeDetailTO corporativeAllocate) {
		this.corporativeAllocate = corporativeAllocate;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
}