package com.pradera.corporateevents.corporativeprocess.consignation.to;

import java.util.List;

import com.pradera.model.corporatives.CorporativeProcessResult;

public class RegisterAllocationProcess {

	private Long idBenefitPaymentPK;
	
	private List<CorporativeProcessResult> corporativeResultWithOutCorporativeProcessResultBlock;
	
	private List<CorporativeProcessResult> lstCorporativeResultBlocked;

	private Integer currency;
	
	public Long getIdBenefitPaymentPK() {
		return idBenefitPaymentPK;
	}

	public void setIdBenefitPaymentPK(Long idBenefitPaymentPK) {
		this.idBenefitPaymentPK = idBenefitPaymentPK;
	}

	public List<CorporativeProcessResult> getCorporativeResultWithOutCorporativeProcessResultBlock() {
		return corporativeResultWithOutCorporativeProcessResultBlock;
	}

	public void setCorporativeResultWithOutCorporativeProcessResultBlock(
			List<CorporativeProcessResult> corporativeResultWithOutCorporativeProcessResultBlock) {
		this.corporativeResultWithOutCorporativeProcessResultBlock = corporativeResultWithOutCorporativeProcessResultBlock;
	}

	public List<CorporativeProcessResult> getLstCorporativeResultBlocked() {
		return lstCorporativeResultBlocked;
	}

	public void setLstCorporativeResultBlocked(List<CorporativeProcessResult> lstCorporativeResultBlocked) {
		this.lstCorporativeResultBlocked = lstCorporativeResultBlocked;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
}
