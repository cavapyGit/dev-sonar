package com.pradera.corporateevents.corporativeprocess.consignation.batch;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.corporateevents.corporativeprocess.consignation.facade.AllocationPaymentBenefitServiceFacade;
import com.pradera.corporateevents.corporativeprocess.consignation.service.AllocationPaymentBenefitServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * Bachero que ejecuta el proceso definitivo de la consignacion de pagos.
 *
 * @author PraderaTechnologies
 */
@RequestScoped
@BatchProcess(name="AllocatePaymentDefinitiveBatch")
public class AllocatePaymentDefinitiveBatch implements JobExecution,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	@Inject
	PraderaLogger  logger;

	/** The allocation facade. */
	@EJB
	AllocationPaymentBenefitServiceFacade allocationFacade;
	
	/** The allocation benefit service. */
	@EJB
	AllocationPaymentBenefitServiceBean allocationBenefitService;
	
	/** The report service. */
	@Inject 
	Instance<ReportGenerationService> reportService;
	
	/**
	 * Inicio de Bachero que ejecuta el proceso definitivo de la consignacion de pagos.
	 *
	 * @param processLogger <code>ProcessLogger object</code>
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger){
		logger.info("AllocatePaymentBenefitBatch: Starting the process ...");
			List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
			String allocationID = StringUtils.EMPTY;
			String userId = StringUtils.EMPTY;
		try{
			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
				if(processLoggerDetail.getParameterName().equals("idAllocateProcess")){
					allocationID = processLoggerDetail.getParameterValue();
				}else if(processLoggerDetail.getParameterName().equals("userId")){
					userId = processLoggerDetail.getParameterValue();
				}
			}

			allocationFacade.executeDefinitiveAllocation(allocationID,userId);
		} catch(ServiceException ex){
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		/*
		try{
			sendReport(allocationID, ReportIdType.HOLDER_CONSOLIDATE_BY_BANK.getCode());
			sendReport(allocationID, ReportIdType.TAX_AND_CUSTODY_RETENTIONS_BY_HOLDER.getCode());
			sendReport(allocationID, ReportIdType.FUNDS_RETENTIONS_TO_HOLDERS.getCode());
			sendReport(allocationID, ReportIdType.CONSOLIDATE_BY_BANK.getCode());
		} catch(ServiceException ex){
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		*/
		
		logger.info("AllocatePaymentBenefitBatch: Finishing the process ...");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		return true;
	}
	
	/**
	 * Send Report.
	 *
	 * @param allocationID id allocation
	 * @param idReport id report
	 * @throws ServiceException the service exception
	 */
	public void sendReport(String allocationID, Long idReport) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("process", allocationID);
		parameters.put("process_date", CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(), GeneralConstants.DATE_FORMAT_PATTERN));
		parameters.put("reportFormats", String.valueOf(ReportFormatType.PDF.getCode()));
		ReportUser user = new ReportUser();
		user.setUserName(loggerUser.getUserName());
		reportService.get().saveReportExecution(idReport, parameters, null, user, loggerUser);
	}
}