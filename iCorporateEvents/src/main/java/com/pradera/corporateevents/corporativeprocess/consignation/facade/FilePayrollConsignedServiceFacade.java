package com.pradera.corporateevents.corporativeprocess.consignation.facade;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.corporateevents.corporativeprocess.consignation.service.FilePayrollConsignedServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.FilePayrollConsigned;
import com.pradera.model.corporatives.FilePayrollConsignedDet;
import com.pradera.model.corporatives.PayrollConsignedOperation;
import com.pradera.model.corporatives.type.FilePayrollStateType;
import com.pradera.model.corporatives.type.PayrollStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AllocationPaymentBenefitServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 31/10/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class FilePayrollConsignedServiceFacade extends CrudDaoServiceBean {
	
	/** The parameter service. */
	@EJB
	ParameterServiceBean parameterService;
	
	/** The trn central devolution. */
	@Inject @Configurable
	String idIssuerBC;
	
	/** The helper component facade. */
	@EJB
	FilePayrollConsignedServiceBean filePayrollConsignedServiceBean;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	
	/** The trn central devolution. */
	String trnCentralDevolution;

	

	/**
	 * Metodo que retorna monedas BOB y USD
	 *
	 * @return the currency
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getCurrency() throws ServiceException {
		List<ParameterTable> lstCurrency = new ArrayList<ParameterTable>();
		List<Integer> lstParameterTablePk = new ArrayList<Integer>();
		lstParameterTablePk.add(CurrencyType.PYG.getCode());
		lstParameterTablePk.add(CurrencyType.USD.getCode());
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		filter.setLstParameterTablePk(lstParameterTablePk);
		lstCurrency = parameterService.getListParameterTableServiceBean(filter);
		return lstCurrency;
	}

	public void saveFilePayroll(List<PayrollConsignedOperation> lstPayrollConsigned, FilePayrollConsigned objFilePayroll) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		
		FilePayrollConsignedDet objPayrollConsDet = new FilePayrollConsignedDet();
		List<FilePayrollConsignedDet> lstobjPayrollConsDet = new ArrayList<>();
		FilePayrollConsigned objPayrollSave = new FilePayrollConsigned();
		objPayrollSave.setAudit(loggerUser);
		objPayrollSave.setCurrency(objFilePayroll.getCurrency());
		if(Validations.validateIsNotNull(objFilePayroll.getIssuer().getIdIssuerPk())) {
			objPayrollSave.setIssuer(objFilePayroll.getIssuer());
		}
		if(Validations.validateIsNotNull(objFilePayroll.getParticipant().getIdParticipantPk())) {
			objPayrollSave.setParticipant(objFilePayroll.getParticipant());
		}
		objPayrollSave.setProcessDate(CommonsUtilities.currentDate());
		objPayrollSave.setOperationTotal(lstPayrollConsigned.size());
		objPayrollSave.setOperationProcessed(GeneralConstants.ZERO_VALUE_INT);
		objPayrollSave.setNameFile(CommonsUtilities.convertDateToString(CommonsUtilities.currentDate())+GeneralConstants.DOT +"TXT");
		objPayrollSave.setState(FilePayrollStateType.REGISTERED.getCode());
		
		for(PayrollConsignedOperation obj : lstPayrollConsigned) {
			objPayrollConsDet = new FilePayrollConsignedDet();
			objPayrollConsDet.setAudit(loggerUser);
			objPayrollConsDet.setFilePayrollConsigned(objPayrollSave);
			objPayrollConsDet.setPayrollConsignedOperation(obj);
			objPayrollConsDet.setRegistryDate(CommonsUtilities.currentDate());
			objPayrollConsDet.setRegistryUser(loggerUser.getUserName());
			objPayrollConsDet.setState(PayrollStateType.GENERATED.getCode());
			objPayrollConsDet.setGeneratedFrame(buildFrame(obj));
			lstobjPayrollConsDet.add(objPayrollConsDet);
			
			filePayrollConsignedServiceBean.updateStatePayrollConsigned(obj.getIdPayrollConsignedOperPk(), loggerUser, PayrollStateType.GENERATED.getCode());
		}
		
		objPayrollSave.setFilePayrollConsignedDets(lstobjPayrollConsDet);
		objPayrollSave.setFile(generateReportTXT(lstobjPayrollConsDet));
		filePayrollConsignedServiceBean.create(objPayrollSave);
		//registramos el 
		
	}
	
	private String buildFrame(PayrollConsignedOperation obj ) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(GeneralConstants.STR_COMILLA + obj.getAccountNumberBankSource() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);	//1 cuenta origen
		sbQuery.append(GeneralConstants.STR_COMILLA + obj.getAccountNumberBankTarget() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);	//2 cuenta destino
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getCodeBank() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);				//3 codigo banco
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getCurrency() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);				//4moneda
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getDocumentTypeTarger() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);	//5 tipo de documento
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getDocumentNumberTarget() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);		//6 numero de documento
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getFullnameTarget() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);		//7 titular de cuenta 
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getConsignedDate() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);//8  Fecha de proceso
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getPaymentAmount() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);		//9 monto de pago
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getTransferMotive() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);	//10 motivo de transferencia
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getEmailTarget() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);		//11 email
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getSmsMessage() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);		//12 sms
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getPaymentNumber() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);		//13 numero de pago
		sbQuery.append(GeneralConstants.STR_COMILLA +obj.getInvoiceNumber() + GeneralConstants.STR_COMILLA + GeneralConstants.STR_SEMI_COLON_WITHOUT_SPACE);		//14 sms
		
		return sbQuery.toString();
	}
	
	
	private byte[] generateReportTXT(List<FilePayrollConsignedDet> lst) throws ServiceException {
		
		//Declaramos variables
		StringBuilder sbTxtFormat = new StringBuilder();
		//Query con la informacion que saldra en el reporte
//		List <FilePayrollConsignedDet>  lst = objFile.getFilePayrollConsignedDets();
		//Si el reporte tiene datos 
		if(lst.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			for (FilePayrollConsignedDet obj : lst){
				sbTxtFormat.append(obj.getGeneratedFrame());
				sbTxtFormat.append(GeneralConstants.STR_END_LINE_TXT);
			}
			//Generamos el archivo 
			return sbTxtFormat.toString().getBytes();
		}else{
			//Si el reporte no tiene datos
			sbTxtFormat.append(GeneralConstants.STR_COMILLA + "SIN DATOS" + GeneralConstants.STR_COMILLA);
			return sbTxtFormat.toString().getBytes();
		}
	}
	
	public void confirmFilePayroll(FilePayrollConsigned objFilePayroll, List<FilePayrollConsignedDet> lstFilePayrollConsignedDet) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		int count = 0;
		for(FilePayrollConsignedDet obj : objFilePayroll.getFilePayrollConsignedDets()) {
			FilePayrollConsignedDet objUpdate = filePayrollConsignedServiceBean.find(FilePayrollConsignedDet.class, obj.getIdFilePayrollConsigDetPk());
			
			if(validateFilePayrollDet(lstFilePayrollConsignedDet, obj.getIdFilePayrollConsigDetPk()) && objUpdate.getState().equals(PayrollStateType.GENERATED.getCode())) {
				filePayrollConsignedServiceBean.updateStatePayrollConsigned(objUpdate.getPayrollConsignedOperation().getIdPayrollConsignedOperPk(), loggerUser, PayrollStateType.CONFIRMED.getCode());
				objUpdate.setState(PayrollStateType.CONFIRMED.getCode());
				count ++;
			}/*else {
				objUpdate.setState(PayrollStateType.ANNULLED.getCode());
				filePayrollConsignedServiceBean.updateStatePayrollConsigned(objUpdate.getPayrollConsignedOperation().getIdPayrollConsignedOperPk(), loggerUser, PayrollStateType.REGISTERED.getCode());
			}*/
			objUpdate.setAudit(loggerUser);
			filePayrollConsignedServiceBean.update(objUpdate);
		}
		
		FilePayrollConsigned objFileUpdate = filePayrollConsignedServiceBean.find(FilePayrollConsigned.class, objFilePayroll.getIdFilePayrollConsignedPk());
		objFileUpdate.setOperationProcessed(objFileUpdate.getOperationProcessed()+count);
		objFileUpdate.setState(FilePayrollStateType.PROCESSED.getCode());
		objFileUpdate.setAudit(loggerUser);
		objFileUpdate.setFile(generateReportTXT(lstFilePayrollConsignedDet));
		filePayrollConsignedServiceBean.update(objFileUpdate);
		
//		filePayrollConsignedServiceBean.updateProcessFilePayroll(objFilePayroll.getIdFilePayrollConsignedPk(), loggerUser);
			
	}
	public boolean validateFilePayrollDet( List<FilePayrollConsignedDet> lstFilePayrollConsignedDet, Long id) {
		for (FilePayrollConsignedDet obj : lstFilePayrollConsignedDet) {
			if(obj.getIdFilePayrollConsigDetPk().equals(id)) {
				return true;
			}
		}
		return false;
	}
	
	public List<FilePayrollConsigned> searchFilePayroll(FilePayrollConsigned obj) throws ServiceException{
		return filePayrollConsignedServiceBean.searchFilePayroll(obj);
	}
	
	public List<PayrollConsignedOperation> searchPayrollConsignedOper(PayrollConsignedOperation obj) throws ServiceException{
		
		 return filePayrollConsignedServiceBean.searchPayrollConsignedOper(obj);
	} 
	
	public List<FilePayrollConsignedDet> searchFilePayrollDet(FilePayrollConsigned obj) throws ServiceException{
		return filePayrollConsignedServiceBean.searchFilePayrollDet(obj);
	}
	
	public void executePayrollPendingGenerate(PayrollConsignedOperation obj) throws ServiceException{
		 filePayrollConsignedServiceBean.executePayrollPendingGenerate(obj);
	} 
	
	public void executeSettlementPayrollPendingGenerate(PayrollConsignedOperation obj) throws ServiceException{
		 filePayrollConsignedServiceBean.executeSettlementPayrollPendingGenerate(obj);
	} 
}