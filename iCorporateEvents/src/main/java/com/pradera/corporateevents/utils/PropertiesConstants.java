package com.pradera.corporateevents.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class PropertiesConstants {

	/**
	 * Instantiates a new properties constants.
	 */
	public PropertiesConstants() {
		
	}
	
	/** The Constant VALOR_DETAILS_LABEL. */
	public static final String VALOR_DETAILS_LABEL = "valor.details.label";
	
	/** The Constant CORPORATIVEID_NOT_SELECT. */
	public static final String CORPORATIVEID_NOT_SELECT="corporateid.not.select";
	
	public static final String CORPORATIVEID_PRELIMINAR_NOT_AMORTIZATION_INTERES="corporateid.preliminar.not.amortization.interes";

	
	/** The Constant CORPORATIVEID_NOT_PRIVILEGE. */
	public static final String CORPORATIVEID_NOT_PRIVILEGE="corporate.no.privilage";
	
	/** The Constant CORPORATIVEID_SELECT_SINGLE_RECORD. */
	public static final String CORPORATIVEID_SELECT_SINGLE_RECORD="corporateid.select.single.record";
	
	/** The Constant JOINTHOLDER_SELECT_SINGLE_RECORD. */
	public static final String JOINTHOLDER_SELECT_SINGLE_RECORD="jointowner.select.single.record";
	
	/** The Constant JOINTHOLDER_NO_DATA_MODIFED. */
	public static final String JOINTHOLDER_NO_DATA_MODIFED="alt.data.not.modified";
	
	/** The Constant ALT_HOLDER_ADJUSTMENT_SUCCESS. */
	public static final String ALT_HOLDER_ADJUSTMENT_SUCCESS="alt.hol.adjust.succuess";
	
	/** The Constant ALT_HOLDER_ADJUSTMENT_SUCCESS_HED. */
	public static final String ALT_HOLDER_ADJUSTMENT_SUCCESS_HED="alt.hol.adjust.succuess.header";
	
	/** The Constant ALT_HOLDER_ADJUSTMENT_WARNING_HED. */
	public static final String ALT_HOLDER_ADJUSTMENT_WARNING_HED="alt.hol.adjust.warning.header";
	
	/** The Constant CORPORATIVEID_NO_EMISOR. */
	public static final String CORPORATIVEID_NO_EMISOR="corporateid.no.emisor";
	
	/** The Constant CORPORATE_NO_ADJUSTMENTS. */
	public static final String CORPORATE_NO_ADJUSTMENTS="corporate.no.ajustes";
	
	/** The Constant CORPORATE_NO_ADJUSTMENTS_STATE. */
	public static final String CORPORATE_NO_ADJUSTMENTS_STATE="corporate.no.ajustes.pstate";
	
	/** The Constant CORPORATE_NO_ADJUSTMENTS_SITUATION. */
	public static final String CORPORATE_NO_ADJUSTMENTS_SITUATION="corporate.no.ajustes.psituation";
	
	/** The Constant CORPORATE_NO_ADJUSTMENTS_SITUATION_CEVALDOM. */
	public static final String CORPORATE_NO_ADJUSTMENTS_SITUATION_CEVALDOM="corporate.no.ajustes.cevaldom.psituation";
	
	/** The Constant IMPORTANCE_EVENT_CONCURRENCY. */
	public static final String IMPORTANCE_EVENT_CONCURRENCY="importance.event.concurrency";

	/** The Constant IMPORTANCE_EVENT_CONFIRM_MESSAGE. */
	public static final String IMPORTANCE_EVENT_CONFIRM_MESSAGE="importance.event.confirmMessage"; 
	
	/** The Constant IMPORTANCE_EVENT_CONFIRM_MODIFIY. */
	public static final String IMPORTANCE_EVENT_CONFIRM_MODIFIY="importance.event.confirmModifiy";
	
	/** The Constant IMPORTANCE_EVENT_MODIFIED. */
	public static final String IMPORTANCE_EVENT_MODIFIED="importance.event.modifiedOperation";
	
	/** The Constant IMPORTANCE_EVENT_CONFIRM_ANNULATE. */
	public static final String IMPORTANCE_EVENT_CONFIRM_ANNULATE="importance.event.confirmAnnulate";
	
	/** The Constant IMPORTANCE_EVENT_REQUIRED_INCREASE_MOTIVE. */
	public static final String IMPORTANCE_EVENT_REQUIRED_INCREASE_MOTIVE="importance.event.requiredIncreaseMotive";
	
	/** The Constant IMPORTANCE_EVENT_CONFIRMED_ANNULATE. */
	public static final String IMPORTANCE_EVENT_CONFIRMED_ANNULATE="importance.event.annulateConfirmed";
	
	/** The Constant IMPORTANCE_EVENT_REGISTERED. */
	public static final String IMPORTANCE_EVENT_REGISTERED="importance.event.registered";
	
	/** The Constant IMPORTANCE_EVENT_SECURITY_NOT_REGISTERED. */
	public static final String IMPORTANCE_EVENT_SECURITY_NOT_REGISTERED="importance.event.securityNotRegistered";
	
	/** The Constant IMPORTANCE_EVENT_SECURITY_NOT_EX. */
	public static final String IMPORTANCE_EVENT_SECURITY_NOT_EX="importance.event.securityNotEx";	
	
	/** The Constant IMPORTANCE_EVENT_ISSUER_NOT_REGISTERED. */
	public static final String IMPORTANCE_EVENT_ISSUER_NOT_REGISTERED="importance.event.issuerNotRegistered";
	
	/** The Constant IMPORTANCE_EVENT_ISSUER_NOT_EX. */
	public static final String IMPORTANCE_EVENT_ISSUER_NOT_EX="importance.event.issuerNotEx";
	
	/** The Constant IMPORTANCE_EVENT_LBL_WARNING. */
	public static final String IMPORTANCE_EVENT_LBL_WARNING="msg.lbl.warning.message";
	
	/** The Constant IMPORTANCE_EVENT_PAYMENT_REMANENET_EXIST. */
	public static final String IMPORTANCE_EVENT_PAYMENT_REMANENET_EXIST="importance.event.payment.remanent.exit";
	
	/** The Constant IMPORTANCE_EVENT_LBL_PREFERENT_SUSCRIPTION. */
	public static final String IMPORTANCE_EVENT_LBL_PREFERENT_SUSCRIPTION="importance.event.securitie.preferentSuscription";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_SECURITIE. */
	public static final String IMPORTANCE_EVENT_VALIDATE_SECURITIE="importance.event.validate.securitie";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_SECURITIE_PREFERENT. */
	public static final String IMPORTANCE_EVENT_VALIDATE_SECURITIE_PREFERENT="importance.event.validate.preferent.securitie";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_TARGET_EXCHANGE. */
	public static final String IMPORTANCE_EVENT_VALIDATE_TARGET_EXCHANGE="importance.event.validate.exchange.target.security";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_STATE_ISSUER. */
	public static final String IMPORTANCE_EVENT_VALIDATE_STATE_ISSUER="importance.event.instrument.state.validattion";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_STATE_ISSUER_INSTRUMENT. */
	public static final String IMPORTANCE_EVENT_VALIDATE_STATE_ISSUER_INSTRUMENT="importance.event.instrument.state.issue.validattion";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_BALANCE_DESMATERIALIZED_SECURITY. */
	public static final String IMPORTANCE_EVENT_VALIDATE_BALANCE_DESMATERIALIZED_SECURITY="importance.event.balance.desmaterialized.security";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_DIFFERENT_ISSUER. */
	public static final String IMPORTANCE_EVENT_VALIDATE_DIFFERENT_ISSUER="importance.event.securitie.different.issuer";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_EQUALS_SECURITY. */
	public static final String IMPORTANCE_EVENT_VALIDATE_EQUALS_SECURITY="importance.event.securitie.equals.issuer";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY. */
	public static final String IMPORTANCE_EVENT_VALIDATE_EQUALS_CURRENCY="importance.event.securitie.equals.currency";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_FIXED_INCOME. */
	public static final String IMPORTANCE_EVENT_VALIDATE_FIXED_INCOME="importance.event.securitie.fixedincome.issuer";
	
	/** The Constant IMPORTANCE_PAYMENT_REMANENT_CUTOFF_DATE. */
	public static final String IMPORTANCE_PAYMENT_REMANENT_CUTOFF_DATE="improtance.event.cutoff.date.reigstry.date";
	
	/** The Constant IMPORTANCE_ANNULATE_CORPORATE_EVENT. */
	public static final String IMPORTANCE_ANNULATE_CORPORATE_EVENT="importance.event.cannot.annulate";
	
	/** The Constant IMPORTANCE_MODIFY_CORPORATE_EVENT. */
	public static final String IMPORTANCE_MODIFY_CORPORATE_EVENT="importance.event.cannot.modify";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_SAME_SECURITIE. */
	public static final String IMPORTANCE_EVENT_VALIDATE_SAME_SECURITIE="importance.event.same.securitie";
	
	/** The Constant IMPORTANCE_EVENT_SECURITIT_IN_THE_TABLE. */
	public static final String IMPORTANCE_EVENT_SECURITIT_IN_THE_TABLE="importance.event.securitie.already.in.table";
	
	/** The Constant IMPORTANCE_EVENT_REQUIRED_DATA. */
	public static final String IMPORTANCE_EVENT_REQUIRED_DATA="message.data.required";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_SHARE_BALANCE. */
	public static final String IMPORTANCE_EVENT_VALIDATE_SHARE_BALANCE="importance.event.validate.share.balance";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_CIRCUlATION_BALANCE. */
	public static final String IMPORTANCE_EVENT_VALIDATE_CIRCUlATION_BALANCE="importance.event.validate.circulation.balance";
	
	/** The Constant IMPORTANCE_EVENT_VALIDATE_SHARE_ZERO_BALANCE. */
	public static final String IMPORTANCE_EVENT_VALIDATE_SHARE_ZERO_BALANCE="importance.event.validate.share.balance";
	
	/** The Constant IMPORTANCE_EVENT_CIRCUlATION. */
	public static final String IMPORTANCE_EVENT_CIRCUlATION="importance.event.validate.great.circulation.balance";
	
	/** The Constant IMPORTANCE_VALIDATE_FOR_MODIFY. */
	public static final String IMPORTANCE_VALIDATE_FOR_MODIFY="importance.event.validate.for.modify";
	
	/** The Constant IMPORTANCE_VALIDATE_INCREASE_CAPITAL. */
	public static final String IMPORTANCE_VALIDATE_INCREASE_CAPITAL="importance.event.validate.increase.capital";
	
	/** The Constant ADJUSTMENT_SAVE_SUCC. */
	public static final String ADJUSTMENT_SAVE_SUCC="msg.adj.save.succ";
	
	/** The Constant MSG_DEPOSITARY_PRELIMINARY_CONFIRM. */
	public static final String MSG_DEPOSITARY_PRELIMINARY_CONFIRM="msg.depositary.preliminary.confirm";
	
	/** The Constant MSG_PRELIMINARY_CONFIRM. */
	public static final String MSG_PRELIMINARY_CONFIRM="alt.conf.preliminar.process";
	
	/** The Constant MSG_DEFINITIVE_CONFIRM. */
	public static final String MSG_DEFINITIVE_CONFIRM="alt.conf.definitive.process";
	
	/** The Constant MSG_DEPOSITARY_PRELIMINARY_SUCC. */
	public static final String MSG_DEPOSITARY_PRELIMINARY_SUCC="msg.depositary.preliminary.succ";
	
	/** The Constant MSG_ISSUER_PRELIMINARY_SUCC. */
	public static final String MSG_ISSUER_PRELIMINARY_SUCC="msg.issuer.preliminary.succ";
	
	/** The Constant MSG_CORPORATE_EVENT_NOT_VALIDATE. */
	public static final String MSG_CORPORATE_EVENT_NOT_VALIDATE="alt.corporateevent.notvalid";
	
	/** The Constant MSG_CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE. */
	public static final String MSG_CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE="alt.corporateevent.amortization.notvalid";
	
	/** The Constant MSG_CORPORATE_EVENT_BENF_NOT_VALIDATE. */
	public static final String MSG_CORPORATE_EVENT_BENF_NOT_VALIDATE="alt.corporateevent.benef.notvalid";
	
	/** The Constant MSG_CORPORATE_EVENT_DPF_DPA_NOT_VALIDATE. */
	public static final String MSG_CORPORATE_EVENT_DPF_DPA_NOT_VALIDATE="alt.corporateevent.dpf.dpa.notvalid";
	
	/** The Constant MSG_CORPORATE_EVENT_CONVERTIBILY_BONE_ACTION_VALIDATE. */
	public static final String MSG_CORPORATE_EVENT_CONVERTIBILY_BONE_ACTION_VALIDATE="alt.corporateevent.convertibily.bone.action.notvalid";
	
	/** The Constant IMPORTANCE_EVENT_PAYMENT_REMANENT. */
	public static final String IMPORTANCE_EVENT_PAYMENT_REMANENT="importance.event.payment.remanent.no.exist";
	
	/** The Constant IMPORTANCE_EVENT_PAYMENT_RETURN. */
	public static final String IMPORTANCE_EVENT_PAYMENT_RETURN="importance.event.payment.return.no.exist";
	
	/** The Constant MSG_ATLEAST_ONE. */
	public static final String MSG_ATLEAST_ONE="msg.enter.least.least.one";
	
	/** The Constant IMPORTANCE_EVENT_ALERT_MESSAGE. */
	public static final String IMPORTANCE_EVENT_ALERT_MESSAGE="importance.event.alertHeader";
	
	/** The Constant IMPORTNACE_EVENT_EXIT_HEADER. */
	public static final String IMPORTNACE_EVENT_EXIT_HEADER="improtance.event.exitHeader";
	
	/** The Constant IMPORTANCE_EVENT_MODIFY_HEADER. */
	public static final String IMPORTANCE_EVENT_MODIFY_HEADER="importance.event.modifyHeader";
	
	/** The Constant IMPORTANCE_EVENT_ANNULAR_HEADER. */
	public static final String IMPORTANCE_EVENT_ANNULAR_HEADER="importance.event.anularHeader";
	
	/** The Constant IMPORTANCE_EVENT_REQUIRED_ANNULAR. */
	public static final String IMPORTANCE_EVENT_REQUIRED_ANNULAR="importance.event.requiredForAnulate";
	
	/** The Constant LBL_WARNING_ACTION_MESSAGE. */
	public static final String LBL_WARNING_ACTION_MESSAGE="lbl.warning.action.message";
	
	/** The Constant LBL_WARNING_ACTION_MESSAGE_ACTIVE. */
	public static final String LBL_WARNING_ACTION_MESSAGE_ACTIVE="lbl.warning.action.message.active";	
	
	/** The Constant INI_DATE_GREATER_THAN_END_DATE. */
	public final static String INI_DATE_GREATER_THAN_END_DATE="error.initialDate.greater.than.endDate";
	
	/** The Constant END_DATE_GREATER_THAN_SYSTEM_DATE. */
	public final static String END_DATE_GREATER_THAN_SYSTEM_DATE="error.end.greater.than.systemDate";
	
	/** The Constant BEFORE_SAVE_HEADER. */
	public final static String BEFORE_SAVE_HEADER="alt.ask.before.holadj.save.header";
	
	/** The Constant MESSAGE_ALERT_HEADER. */
	public final static String MESSAGE_ALERT_HEADER="alt.data.modified.header";
	
	/** The Constant ISSUER_NOT_ENTER_AMOUNT. */
	public final static String ISSUER_NOT_ENTER_AMOUNT="alt.issuer.not.enter.amount";
	
	/** The Constant DEPOSITARY_NOT_PAYMENT. */
	public final static String DEPOSITARY_NOT_PAYMENT="alt.depositary.not.payment";
	
	/** The Constant MESSAGE_NO_ADJUSTEMTNS. */
	public final static String MESSAGE_NO_ADJUSTEMTNS="lbl.search.no.result.ajuste";
	
	public final static String MESSAGE_NO_HOLDER="lbl.search.no.holder";
	
	/** The Constant MESSAGE_EVENT_VALIDATE_MIN_TARGET_SECURITY. */
	public final static String MESSAGE_EVENT_VALIDATE_MIN_TARGET_SECURITY="importance.event.validate.min.target.security";
	
	/** The Constant MESSAGE_EVENT_VALIDATE_MODIFY. */
	public final static String MESSAGE_EVENT_VALIDATE_MODIFY="importance.event.validate.modify";
	
	/** The Constant MESSAGE_EVENT_VALIDATE_PORCENT_TARGET_SECURITY. */
	public final static String MESSAGE_EVENT_VALIDATE_PORCENT_TARGET_SECURITY="importance.event.validate.porcent.target.security";
	
	/** The Constant STOCK_DONT_PRIVILEGES. */
	public final static String STOCK_DONT_PRIVILEGES = "stock.message.dont.privileges";
	
	/** The Constant STOCK_DONT_RNT. */
	public final static String STOCK_DONT_RNT = "stock.message.dont.rnt";
	
	/** The Constant STOCK_DONT_SECURITY. */
	public final static String STOCK_DONT_SECURITY = "stock.message.dont.security";
	
	/** The Constant STOCK_DONT_PARTICIPANT. */
	public final static String STOCK_DONT_PARTICIPANT = "stock.message.dont.participant";
	
	/** The Constant STOCK_QUESTION_SAVE. */
	public final static String STOCK_QUESTION_SAVE = "stock.message.question.save";
	
	/** The Constant STOCK_QUESTION_SUCESS. */
	public final static String STOCK_QUESTION_SUCESS = "stock.message.question.sucess";
	
	/** The Constant IMPORTANCE_EVENT_SECURITY_NOT_IND_CONVERTIBLE. */
	public final static String IMPORTANCE_EVENT_SECURITY_NOT_IND_CONVERTIBLE="importance.event.securityIndConvertible";
	
	/** The Constant STOCK_REPORT_SUCESS. */
	public final static String STOCK_REPORT_SUCESS = "stock.message.report.sucess";
	
	/** The Constant MSG_CORPORATE_EVENT_PROCESS_VALUATOR. */
	public static final String MSG_CORPORATE_EVENT_PROCESS_VALUATOR="stock.message.procees.valuator";
	
	/** The Constant MSG_CORPORATE_EVENT_STOCK_FIRST_DAY. */
	public static final String MSG_CORPORATE_EVENT_STOCK_FIRST_DAY="stock.message.stock.first.day";
	
	/** The Constant CORPORATIVE_PROCESS_REPORT_SUCCESS. */
	public final static String CORPORATIVE_PROCESS_REPORT_SUCCESS = "corporate.process.msg.report.success";
	

	/** The Constant HEADER_WARNING. */
	public static final String HEADER_WARNING="msg.lbl.warning.message";
	
	/** The Constant HEADER_ERROR. */
	public static final String HEADER_ERROR="message.error";
	
	/** The Constant HEADER_SUCCESSFUL. */
	public static final String HEADER_SUCCESSFUL="header.success";
	
	/** The Constant MESSAGE_ALERT_REGISTER_NOT_SELECT. */
	public static final String MESSAGE_ALERT_REGISTER_NOT_SELECT="message.alert.register.notselected";
	
	/** The Constant CURRENCY_NOT_SELECTED. */
	public static final String CURRENCY_NOT_SELECTED="message.alert.currency.notselect";
	
	/** The Constant CORPORATIVES_NOT_SELECTED_TO_ALLOCATE. */
	public static final String CORPORATIVES_NOT_SELECTED_TO_ALLOCATE="message.alert.corporatives.notselect";
	
	/** The Constant CORPORATIVES_SELECTED_NOT_DEFINITIVE_STATE. */
	public static final String CORPORATIVES_SELECTED_NOT_DEFINITIVE_STATE="message.alert.corporatives.notdefinitive";
	
	/** The Constant MESSAGE_CONFIRM_REGISTER_ALLOCATION_PROCESS. */
	public static final String MESSAGE_CONFIRM_REGISTER_ALLOCATION_PROCESS="message.confirm.register.allocation";
	
	/** The Constant ALLOCATION_PROCESS_NOT_SELECTED. */
	public static final String ALLOCATION_PROCESS_NOT_SELECTED="message.alert.allocation.process.notselect";
	
	/** The Constant ALLOCATION_PROCESS_REGISTER_SUCCESSFULLY. */
	public static final String ALLOCATION_PROCESS_REGISTER_SUCCESSFULLY="message.confirm.allocation";
	
	/** The Constant ALLOCATION_PROCESS_DEFINITIVE_SUCCESSFULLY. */
	public static final String ALLOCATION_PROCESS_DEFINITIVE_SUCCESSFULLY="confirmed.definitive.allocation";
	
	/** The Constant BENEFIT_CASH_ACCOUNT_NOT_EXIST. */
	public static final String BENEFIT_CASH_ACCOUNT_NOT_EXIST="message.alert.cashaccount.notexist";
	
	/** The Constant MESSAGE_ERROR_NO_CORPORATIVE_SELECTED. */
	public static final String MESSAGE_ERROR_NO_CORPORATIVE_SELECTED="message.error.corporative.notselected";
	
	/** The Constant ALLOCATION_BENEFIT_PROCESS_SUCCESSFULLY_SENT. */
	public static final String ALLOCATION_BENEFIT_PROCESS_SUCCESSFULLY_SENT="message.successful.process.sent";
	
	/** The Constant MESSAGE_ERROR_NOT_BALANCE_ENOUGH. */
	public static final String MESSAGE_ERROR_NOT_BALANCE_ENOUGH="message.error.balance.not.enough";
	
	/** The Constant MESSAGE_WARNING_NO_DATA_FOUND. */
	public static final String MESSAGE_WARNING_NO_DATA_FOUND="message.warning.not.data.found";
	
	/** The Constant MESSAGE_WARNING_PREVIOUS_ALLOCATION_PAYMENT. */
	public static final String MESSAGE_WARNING_PREVIOUS_ALLOCATION_PAYMENT="message.validation.allocation.process";
	
	/** The Constant MESSAGE_ERROR_NOT_FOUND_TAX_ACCOUNT. */
	public static final String MESSAGE_ERROR_NOT_FOUND_TAX_ACCOUNT="message.error.notfound.tax.account";
	
	/** The Constant MESSAGE_ERROR_NOT_FOUND_RATE_ACCOUNT. */
	public static final String MESSAGE_ERROR_NOT_FOUND_RATE_ACCOUNT="message.error.notfound.rate.account";
	
	/** The Constant MESSAGE_ERROR_NOT_CURRENCY. */
	public static final String MESSAGE_ERROR_NOT_CURRENCY="message.error.no.currency";
	
	/** The Constant MESSAGE_NOT_EXIST_ECHANGE_RATE. */
	public static final String MESSAGE_NOT_EXIST_ECHANGE_RATE="lbl.not.exist.exchange";
	
	/** The Constant ISSUER_CASH_ACCOUNT_NOT_ENOUGH_BALANCES. */
	public static final String ISSUER_CASH_ACCOUNT_NOT_ENOUGH_BALANCES="message.error.no.enough.balance";
	
	/** The Constant ISSUER_CASH_ACCOUNT_NOT_ENOUGH_BALANCES_CENTRALIZING. */
	public static final String ISSUER_CASH_ACCOUNT_NOT_ENOUGH_BALANCES_CENTRALIZING="message.error.no.enough.balance.bank";
	
	/** The Constant NOT_EXIST_CASH_ACCOUNT_CENTRALIZING. */
	public static final String NOT_EXIST_CASH_ACCOUNT_CENTRALIZING="message.error.no.cash.account.bank";
	
	/** The Constant NOT_EXIST_CASH_ACCOUNT_ISSUER. */
	public static final String NOT_EXIST_CASH_ACCOUNT_ISSUER="message.error.no.cash.account";
	
	/** The Constant NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_DEV. */
	public static final String NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_DEV="message.error.no.cash.account.depositary.dev";
	
	/** The Constant NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_TAR. */
	public static final String NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_TAR="message.error.no.cash.account.depositary.tar";
	
	/** The Constant NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_IMP. */
	public static final String NOT_EXIST_CASH_ACCOUNT_DEPOSITARY_IMP="message.error.no.cash.account.depositary.imp";
	
	/** The Constant MESSAGE_CORPORATIVE_NO_DEFINITIVE. */
	public static final String MESSAGE_CORPORATIVE_NO_DEFINITIVE="message.error.no.state.definitive";
	
	/** The Constant MESSAGE_ALLOCATION_NO_PRELIMINARY. */
	public static final String MESSAGE_ALLOCATION_NO_PRELIMINARY="message.error.confirmed.definitive.allocation";

	/** The Constant MESSAGE_CONFIRMATION_ALLOCATION_DEFINITIVE. */
	public static final String MESSAGE_CONFIRMATION_ALLOCATION_DEFINITIVE="message.confirm.definitive.allocation";
	

	/** The Constant ERROR_ALLOCATION_PROCESS_NO_COMMERCIAL_RETURN_CASH_ACCOUNT. */
	public static final Integer ERROR_ALLOCATION_PROCESS_NO_COMMERCIAL_RETURN_CASH_ACCOUNT = Integer.valueOf("1");
	
	/** The Constant MESSAGE_ALLOCATION_PROCESS_NO_COMMERCIAL_RETURN_CASH_ACCOUNT. */
	public static final String MESSAGE_ALLOCATION_PROCESS_NO_COMMERCIAL_RETURN_CASH_ACCOUNT = "message.error.no.commercial.return.cash.account";
	
	/** The Constant MESSAGE_SUCCESS_PRELIMINAR. */
	public static final String MESSAGE_SUCCESS_PRELIMINAR="alt.preliminary.sent.successfully";
	
	/** The Constant MESSAGE_SUCCESS_DEFINITVE. */
	public static final String MESSAGE_SUCCESS_DEFINITVE="alt.definitive.sent.successfully";
	
	/** The Constant MESSAGE_STATE_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_STATE_DEFINITIVE_INCORRECT ="alt.definitivo.state.incorrect";
	
	/** The Constant MESSAGE_SITUATION_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_SITUATION_DEFINITIVE_INCORRECT ="alt.definitivo.situation.incorrect";
	
	/** The Constant MESSAGE_DELIVERY_DATE_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_DELIVERY_DATE_DEFINITIVE_INCORRECT ="alt.definitive.deliverydate.closed";
	
	/** The Constant MESSAGE_DELIVERY_DATE_TWO_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_DELIVERY_DATE_TWO_DEFINITIVE_INCORRECT ="alt.preliminary.deliverydate.open";
	
	/** The Constant MESSAGE_PRELIMINARY_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_PRELIMINARY_DEFINITIVE_INCORRECT ="alt.definitive.preliminary.no.today";
	
	/** The Constant MESSAGE_PRELIMINARY_FAIL_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_PRELIMINARY_FAIL_DEFINITIVE_INCORRECT ="msg.cevaldom.preliminary.fail";
	
	/** The Constant MESSAGE_ISSUER_AMOUNT_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_ISSUER_AMOUNT_DEFINITIVE_INCORRECT ="alt.corporateevent.issuer.amount.not.equal";
	
	/** The Constant MESSAGE_BLOCK_REPORT_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_BLOCK_REPORT_DEFINITIVE_INCORRECT ="alt.preliminary.block.report.move";
	
	/** The Constant MESSAGE_BLOCK_REPORT_TWO_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_BLOCK_REPORT_TWO_DEFINITIVE_INCORRECT ="alt.definitive.block.report.move";
	
	/** The Constant MESSAGE_HAVE_ACCREDITATION_DEFINITIVE_INCORRECT. */
	public static final String MESSAGE_HAVE_ACCREDITATION_DEFINITIVE_INCORRECT ="alt.definitive.has.accreditation.bal";
	
	/** The Constant MESSAGE_CORPORATIVE_EVENTS_NOT_INCORRECT. */
	public static final String MESSAGE_CORPORATIVE_EVENTS_NOT_INCORRECT ="alt.corporativeevents.no";
	
	/** The Constant MESSAGE_CORPORATIVE_EVENTS_NOT_REVERSE_INCORRECT. */
	public static final String MESSAGE_CORPORATIVE_EVENTS_NOT_REVERSE_INCORRECT ="alt.corporativeevents.no.reverse";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_STATE_NO_VALID. */
	public static final String MESSAGE_CORPORATE_EVENT_STATE_NO_VALID = "corporateevent.state.notvalid";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_SECURITY_NOT_CONFIRM. */
	public static final String MESSAGE_CORPORATE_EVENT_SECURITY_NOT_CONFIRM = "corporateevent.security.not.confirm";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_SECURITY_NOT_BALANCE_CONFIRM. */
	public static final String MESSAGE_CORPORATE_EVENT_SECURITY_NOT_BALANCE_CONFIRM = "corporateevent.security.not.balance.confirm";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_EXIST_SETTLEMENT_OPERATIONS. */
	public static final String MESSAGE_CORPORATE_EVENT_EXIST_SETTLEMENT_OPERATIONS="corporateevent.exists.settlement.operations";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_EXIST_TRANSIT_BALANCE. */
	public static final String MESSAGE_CORPORATE_EVENT_EXIST_TRANSIT_BALANCE="corporateevent.exists.transit.balance";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_NOT_CONFIRMATION_ISSUER. */
	public static final String MESSAGE_CORPORATE_EVENT_NOT_CONFIRMATION_ISSUER="corporateevent.not.confirmation.issue";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_EXIST_BLOCK_GUARANTIES. */
	public static final String MESSAGE_CORPORATE_EVENT_EXIST_BLOCK_GUARANTIES="corporateevent.exist.block.guarantees";
	
	/** The Constant MESSAGE_EXCHANGE_RATE_DONT_EXIST_USD. */
	public static final String MESSAGE_EXCHANGE_RATE_DONT_EXIST_USD="settlement.process.exchange.rate.usd";
	
	/** The Constant MESSAGE_EXCHANGE_RATE_DONT_EXIST_UFV. */
	public static final String MESSAGE_EXCHANGE_RATE_DONT_EXIST_UFV="settlement.process.exchange.rate.ufv";
	
	/** The Constant MESSAGE_EXCHANGE_RATE_DONT_EXIST_DMV. */
	public static final String MESSAGE_EXCHANGE_RATE_DONT_EXIST_DMV="settlement.process.exchange.rate.dmv";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_NOT_AVAILABLE_BALANCE. */
	public static final String MESSAGE_CORPORATE_EVENT_NOT_AVAILABLE_BALANCE="corporateevent.not.available.balance";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE. */
	public static final String MESSAGE_CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE = "alt.corporateevent.amortization.notvalid";
	
	/** The Constant MESSAGE_CORPORATE_EVENT_AMORTIZATION_NOT_VALIDATE. */
	public static final String MESSAGE_NOT_EXIST_SECURITY = "message.not.exist.security";
	
	public static final String ISSUER_NOT_SELECTED="message.alert.issuer.notselect";
	
	public static final String PARTICIPANT_NOT_SELECTED="message.alert.participant.notselect";

	public static final String FILE_PAYROLL_OPERATION_REGISTER ="lbl.file.payroll.select.register";
	
	public static final String MESSAGE_CONFIRM_FILE_PYROLL="alt.file.payroll.sent.save";
	
	public static final String MESSAGE_SUCCESS_FILE_PYROLL="alt.file.payroll.sent.successfully";
	
	public static final String MESSAGE_CONFIRM_FILE_PYROLL_CONFIRM="alt.file.payroll.sent.confirm";
	
	public static final String MESSAGE_SUCCESS_FILE_PYROLL_SAVE="alt.file.payroll.sent.confirm.successfully";
}
