package com.pradera.corporateevents.stockcalculation.facade;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.commons.services.remote.stockcalculation.StockCalculationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.stockcalculation.service.ControllerStockServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class StockCalculationRemoteServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/06/2014
 */
@Stateless
public class StockCalculationRemoteServiceFacade implements StockCalculationService {
	
	/** The controller stock service. */
	@EJB
	ControllerStockServiceBean controllerStockService;

    /**
     * Default constructor. 
     */
    public StockCalculationRemoteServiceFacade() {
        // TODO Auto-generated constructor stub
    }

	/* (non-Javadoc)
	 * @see com.pradera.commons.services.remote.stockcalculation.StockCalculationBoundary#executeStockCalculationMonthly(java.util.Date, java.util.Date, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public Long executeStockCalculationMonthly(Date cutoffDate,Date registryDate, LoggerUser loggerUser) throws ServiceException {
		//Only generation stock monthly at a time
		StockCalculationProcess stockCalculationProcess = new StockCalculationProcess();
		stockCalculationProcess.setCreationDate(CommonsUtilities.currentDateTime());
		stockCalculationProcess.setCutoffDate(cutoffDate);
		stockCalculationProcess.setRegistryDate(registryDate);
		stockCalculationProcess.setRegistryUser(loggerUser.getUserName());
		stockCalculationProcess.setIndAutomactic(BooleanType.YES.getCode());
		stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
		stockCalculationProcess.setStockType(StockType.MONTHLY.getCode());
		return controllerStockService.executeStockMonthly(stockCalculationProcess, loggerUser);
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.services.remote.stockcalculation.StockCalculationService#executeStockCalculationMarketFactByCutDate(java.util.Date, java.util.Date, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public Long executeStockCalculationMarketFactByCutDate(Date cutoffDate,Date registryDate, LoggerUser loggerUser) throws ServiceException {
		//Only generation stock monthly at a time
		StockCalculationProcess stockCalculationProcess = new StockCalculationProcess();
		stockCalculationProcess.setCreationDate(CommonsUtilities.currentDateTime());
		stockCalculationProcess.setCutoffDate(cutoffDate);
		stockCalculationProcess.setRegistryDate(registryDate);
		stockCalculationProcess.setRegistryUser(loggerUser.getUserName());
		stockCalculationProcess.setIndAutomactic(BooleanType.YES.getCode());
		stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
		stockCalculationProcess.setStockType(StockType.MONTHLY.getCode());
		return controllerStockService.executeStockMonthly(stockCalculationProcess, loggerUser);
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.services.remote.stockcalculation.StockCalculationService#executeStockMarketFact(java.lang.Long, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void executeStockMarketFact(Long stockCalculationId, LoggerUser loggerUser) throws ServiceException {
		// TODO Auto-generated method stub
		controllerStockService.executeStockMarketFact(stockCalculationId, loggerUser);
	}

}
