package com.pradera.corporateevents.stockcalculation.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessConstants;
import com.pradera.corporateevents.stockcalculation.StockCalculationConstants;
import com.pradera.corporateevents.stockcalculation.reports.StockCalculationReportsSender;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * The Class StockCalculationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@RequestScoped
@BatchProcess(name="StockCalculationBatch")
public class StockCalculationBatch implements JobExecution,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1796221847372665773L;

	/** The stock calculation service. */
	@EJB
	private StockCalculationServiceBean stockCalculationService;

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The loader entity service. */
	@Inject
	private LoaderEntityServiceBean loaderEntityService;


	/** The report sender. */
	@Inject 
	private StockCalculationReportsSender reportSender;

	/**
	 * Instantiates a new stock calculation batch.
	 */
	public StockCalculationBatch() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	public void startJob(ProcessLogger processLogger) {
		// TODO Example call service remote restful
		Long stockCalculationPk = null;
		if (loaderEntityService.isLoadedEntity(processLogger, "processLoggerDetails")){
			for(ProcessLoggerDetail plDetail : processLogger.getProcessLoggerDetails()){
				if(plDetail.getParameterName().equals(StockCalculationConstants.STOCK_ID)){
					stockCalculationPk = new Long(plDetail.getParameterValue());
				}
			}
		}

		if(Validations.validateIsNotNull(stockCalculationPk)){
			try { 
				StockCalculationProcess objStockCalculationProcess = stockCalculationService.findCalculationProcess(stockCalculationPk);
				
				if(StockType.MONTHLY.getCode().equals(objStockCalculationProcess.getStockType())) {
					stockCalculationService.executeBatchStockCalculationMonthly(stockCalculationPk);
				} else {
					stockCalculationService.executeBatchStockCalculation(stockCalculationPk);
				}	
				
				StockCalculationProcess stockProcess = stockCalculationService.findCalculationProcess(stockCalculationPk);
				
				for(ProcessLoggerDetail plDetail : processLogger.getProcessLoggerDetails()) {
					if(plDetail.getParameterName() != null && plDetail.getParameterName().equals("reportFormats")) {
						if(plDetail.getParameterValue().equals(ReportFormatType.XLS.getCode().toString())) {
							stockProcess.setReportType(ReportFormatType.XLS.getCode());
						} else if (plDetail.getParameterValue().equals(ReportFormatType.PDF.getCode().toString())) {
							stockProcess.setReportType(ReportFormatType.PDF.getCode());
						}
					}
				}
				
				if(stockProcess.getStockState().equals(StockStateType.FINISHED.getCode())){
					if(StockClassType.NORMAL.getCode().equals(stockProcess.getStockClass())){
						if(StockType.HOLDER.getCode().equals(stockProcess.getStockType())){
							reportSender.sendReports(stockProcess, ReportIdType.HOLDER_STOCK.getCode());
						}else if(StockType.PARTICIPANT.getCode().equals(stockProcess.getStockType())){
							reportSender.sendReports(stockProcess, ReportIdType.PARTICIPANT_STOCK.getCode());
						}else if(StockType.SECURITY.getCode().equals(stockProcess.getStockType())){ 
							reportSender.sendReports(stockProcess, ReportIdType.SECURITY_STOCK.getCode());
							reportSender.sendReports(stockProcess, ReportIdType.NEGATIVE_STOCK.getCode());
							reportSender.sendReports(stockProcess, ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE.getCode());
						}if(StockType.MONTHLY.getCode().equals(stockProcess.getStockType())){
							reportSender.sendReports(stockProcess, ReportIdType.MONTHLY_STOCK.getCode());
							reportSender.sendReports(stockProcess, ReportIdType.NEGATIVE_STOCK_BY_MONTHLY.getCode());
							reportSender.sendReports(stockProcess, ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_MONTHLY.getCode());
						}else if(StockType.CORPORATIVE.getCode().equals(stockProcess.getStockType())
			    				&& !(stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())
			    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.CHQ.getCode())
			    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.FCT.getCode())
			    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.PGS.getCode())
			    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LTC.getCode())
			    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LCB.getCode())
			    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LBS.getCode())
			    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LRS.getCode())
			    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LTS.getCode()))){
							reportSender.sendReports(stockProcess, ReportIdType.SECURITY_STOCK_FOR_TYPE_CORPORATIVE.getCode());
							reportSender.sendReports(stockProcess, ReportIdType.STOCK_CALCULATION_BY_REPORTED.getCode());
							reportSender.sendReports(stockProcess, ReportIdType.CALCULATION_STOCK_BY_OWNER.getCode());
							reportSender.sendReports(stockProcess, ReportIdType.NEGATIVE_STOCK.getCode());
							reportSender.sendReports(stockProcess, ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE.getCode());
						}
					}
				}else{
					Integer waitTime =  new Integer(1);
					Integer waitTimeProcess = new Integer(30);
					Date initialTime = CommonsUtilities.currentDateTime();
					int min = 0;
					while(min<=waitTimeProcess.intValue()){
						Thread.sleep(waitTime.intValue()*CorporateProcessConstants.MILLSECS_IN_MIN);
						Date time = CommonsUtilities.currentDateTime();
						min= (int)((double)(time.getTime() - initialTime.getTime())/(double)(CorporateProcessConstants.MILLSECS_IN_MIN));
						stockProcess = stockCalculationService.findCalculationProcess(stockCalculationPk);
						
						for(ProcessLoggerDetail plDetail : processLogger.getProcessLoggerDetails()) {
							if(plDetail.getParameterName() != null && plDetail.getParameterName().equals("reportFormats")) {
								if(plDetail.getParameterValue().equals(ReportFormatType.XLS.getCode().toString())) {
									stockProcess.setReportType(ReportFormatType.XLS.getCode());
								} else if (plDetail.getParameterValue().equals(ReportFormatType.PDF.getCode().toString())) {
									stockProcess.setReportType(ReportFormatType.PDF.getCode());
								}
							}
						}
						
						if(stockProcess.getStockState().equals(StockStateType.FINISHED.getCode())){
							if(StockClassType.NORMAL.getCode().equals(stockProcess.getStockClass())){
								if(StockType.HOLDER.getCode().equals(stockProcess.getStockType())){
									reportSender.sendReports(stockProcess, ReportIdType.HOLDER_STOCK.getCode());
								}else if(StockType.PARTICIPANT.getCode().equals(stockProcess.getStockType())){
									reportSender.sendReports(stockProcess, ReportIdType.PARTICIPANT_STOCK.getCode());
								}else if(StockType.SECURITY.getCode().equals(stockProcess.getStockType())){ 
									reportSender.sendReports(stockProcess, ReportIdType.SECURITY_STOCK.getCode());
									reportSender.sendReports(stockProcess, ReportIdType.NEGATIVE_STOCK.getCode());
									reportSender.sendReports(stockProcess, ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE.getCode());
								}if(StockType.MONTHLY.getCode().equals(stockProcess.getStockType())){
									reportSender.sendReports(stockProcess, ReportIdType.MONTHLY_STOCK.getCode());
									reportSender.sendReports(stockProcess, ReportIdType.NEGATIVE_STOCK_BY_MONTHLY.getCode());
									reportSender.sendReports(stockProcess, ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_MONTHLY.getCode());
								}else if(StockType.CORPORATIVE.getCode().equals(stockProcess.getStockType())
					    				&& !(stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())
						    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.CHQ.getCode())
						    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.FCT.getCode())
						    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.PGS.getCode())
						    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LTC.getCode())
						    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LCB.getCode())
						    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LBS.getCode())
						    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LRS.getCode())
						    					|| stockProcess.getSecurity().getSecurityClass().equals(SecurityClassType.LTS.getCode()))){
									reportSender.sendReports(stockProcess, ReportIdType.SECURITY_STOCK_FOR_TYPE_CORPORATIVE.getCode());
									reportSender.sendReports(stockProcess, ReportIdType.STOCK_CALCULATION_BY_REPORTED.getCode());
									reportSender.sendReports(stockProcess, ReportIdType.CALCULATION_STOCK_BY_OWNER.getCode());
									reportSender.sendReports(stockProcess, ReportIdType.NEGATIVE_STOCK.getCode());
									reportSender.sendReports(stockProcess, ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE.getCode());
								}
							}
							break;
						}
					}
				}
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}catch (Exception e) {
				throw new RuntimeException(e);
			} 
		}else{
			log.info("No se capturo el dato de stockCalculationPk");
		}
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
