package com.pradera.corporateevents.stockcalculation.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.corporateevents.stockcalculation.StockCalculationConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.type.ProcessLoggerStateType;
import com.pradera.model.report.type.ReportFormatType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class StockCalculationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22/05/2014
 */
@Stateless
public class StockCalculationServiceBean extends CrudDaoServiceBean {

	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The batch service. */
	@EJB
	BatchServiceBean batchService;
	   
	/** The parameter service. */
	@EJB 
	ParameterServiceBean parameterService;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
    /**
     * Default constructor. 
     */
    public StockCalculationServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Fire batch stock calculation.
     *
     * @param stockCalculation the stock calculation
     * @throws ServiceException the service exception
     */
    public void fireBatchStockCalculation(StockCalculationProcess stockCalculation) throws ServiceException{
    	//send batch running stock
		BusinessProcess process= new BusinessProcess();
		if(stockCalculation.getIndAutomactic().equals(BooleanType.YES.getCode())){
			process.setIdBusinessProcessPk(BusinessProcessType.STOCK_CALCULATION_AUTOMATIC_EXECUTION.getCode());
		}else{
			process.setIdBusinessProcessPk(BusinessProcessType.STOCK_CALCULATION_MANUAL_EXECUTION.getCode());
		}		
		Map<String, Object> parameters= new HashMap<String, Object>();
		parameters.put(StockCalculationConstants.STOCK_ID, stockCalculation.getIdStockCalculationPk());
		parameters.put(StockCalculationConstants.CUT_OFF_DATE, CommonsUtilities.convertDatetoString(stockCalculation.getCutoffDate()));
		parameters.put(StockCalculationConstants.REGISTER_DATE, CommonsUtilities.convertDatetoString(stockCalculation.getRegistryDate()));
		ParameterTable parameter = parameterService.getParameterTableById(stockCalculation.getStockType());
		parameters.put(StockCalculationConstants.TYPE_STOCK_ID, parameter.getParameterName());
		parameter = parameterService.getParameterTableById(stockCalculation.getStockClass());
		parameters.put(StockCalculationConstants.CLASS_STOCK, parameter.getParameterName());
		String depositary =GeneralConstants.EMPTY_STRING;
		if(stockCalculation.getStockType().equals(StockType.MONTHLY.getCode())){
			if(stockCalculation.getMonthlyType()!=null && Validations.validateIsNotNullAndPositive(stockCalculation.getMonthlyType())){
				InternationalDepository international = parameterService.find(InternationalDepository.class, stockCalculation.getMonthlyType());
				depositary = international.getDescription();
			}
		}
		parameters.put(StockCalculationConstants.DEPOSITARY_INTERNATIONAL, depositary);
		String idHolder = GeneralConstants.EMPTY_STRING;
		if(stockCalculation.getHolder()!=null && Validations.validateIsNotNullAndPositive(stockCalculation.getHolder().getIdHolderPk())){
			idHolder=stockCalculation.getHolder().getIdHolderPk().toString();
		}			
		parameters.put(StockCalculationConstants.HOLDER, idHolder);
		String idSecurity = GeneralConstants.EMPTY_STRING;
		if(stockCalculation.getSecurity()!=null && Validations.validateIsNotNullAndNotEmpty(stockCalculation.getSecurity().getIdSecurityCodePk())){
			idSecurity=stockCalculation.getSecurity().getIdSecurityCodePk();
		}
		parameters.put(StockCalculationConstants.SECURITY, idSecurity);
		String idParticipant = GeneralConstants.EMPTY_STRING;
		if(stockCalculation.getParticipant()!=null && Validations.validateIsNotNullAndPositive(stockCalculation.getParticipant().getIdParticipantPk())){
			idParticipant=stockCalculation.getParticipant().getIdParticipantPk().toString();
		}
		parameters.put(StockCalculationConstants.PARTICIPANT,idParticipant);
		if(stockCalculation.getReportType() != null && stockCalculation.getReportType().equals(ReportFormatType.XLS.getCode())) {
			parameters.put("reportFormats", ReportFormatType.XLS.getCode());
		}else {
			parameters.put("reportFormats", ReportFormatType.PDF.getCode());
		}
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		batchService.registerBatchTx(loggerUser.getUserName(), process, parameters);
    }
    
    /**
     * Execute batch stock calculation.
     *
     * @param idStockCalculationPk the id stock calculation pk
     * @throws ServiceException the service exception
     */
    @Performance
    @TransactionTimeout(unit=TimeUnit.HOURS,value=2)
    public void executeBatchStockCalculation(Long idStockCalculationPk) throws ServiceException {
   		Query query = em.createNativeQuery("BEGIN PKG_STOCK_CALCULATION.SP_MAIN_STOCK_CALCULATION(:idStockCalculationPrm); END;");
       	query.setParameter("idStockCalculationPrm", idStockCalculationPk);
       	query.executeUpdate();
    }
    
    /**
     * Execute batch stock calculation.
     *
     * @param idStockCalculationPk the id stock calculation pk
     * @throws ServiceException the service exception
     */
    @Performance
    @TransactionTimeout(unit=TimeUnit.HOURS,value=2)
    public void executeBatchStockCalculationMonthly(Long idStockCalculationPk) throws ServiceException {
   		Query query = em.createNativeQuery("BEGIN PKG_STOCK_CALCULATION.SP_MAIN_STOCK_CALC_MONTHLY(:idStockCalculationPrm); END;");
       	query.setParameter("idStockCalculationPrm", idStockCalculationPk);
       	query.executeUpdate();
    }

	
	/**
	 * Clean filter stock.
	 * clean parameters like holder,security,participant
	 * @param stock the stock
	 * @throws ServiceException the service exception
	 */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void cleanFilterStock(StockCalculationProcess stock) throws ServiceException {
    	if(Validations.validateIsNotNull(stock.getHolder())){
    		if(Validations.validateIsNotNull(stock.getHolder().getIdHolderPk())){
    			if(!Validations.validateIsNotNullAndPositive(stock.getHolder().getIdHolderPk()))
    			stock.setHolder(null);
    		}else{
    			stock.setHolder(null);
    		}
    	}
    	if(Validations.validateIsNotNull(stock.getParticipant())){
    		if(Validations.validateIsNotNull(stock.getParticipant().getIdParticipantPk())){
    			if(!Validations.validateIsNotNullAndPositive(stock.getParticipant().getIdParticipantPk()))
    			stock.setParticipant(null);
    		}else{
    			stock.setParticipant(null);
    		}
    	}
    	if(Validations.validateIsNotNull(stock.getSecurity())){
    		if(Validations.validateIsNullOrEmpty(stock.getSecurity().getIdSecurityCodePk())){
    			stock.setSecurity(null);
    		}
    	}
    }

	/**
	 * Validate stock calculation if exist in some state.
	 *
	 * @param stock the stock
	 * @param stockStatesQuery the stock states query
	 * @return the stock calculation process
	 * @throws ServiceException the service exception
	 */
	public StockCalculationProcess validateExistStockCalculation(StockCalculationProcess stock,List<Integer> stockStatesQuery) throws ServiceException{
		
		
		Object obj = null;
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select scp from StockCalculationProcess scp ");
		sbQuery.append(" where  ");
		sbQuery.append(" trunc(scp.cutoffDate) = trunc(:cutoffDate) ");
		sbQuery.append(" and trunc(scp.registryDate) = trunc(:registryDate) ");
		sbQuery.append(" and scp.stockState in (:processState) ");
		sbQuery.append(" and scp.stockType = :stockType ");
		sbQuery.append(" and scp.stockClass = :stockClass ");
		if(Validations.validateIsNotNull(stock.getParticipant()) && Validations.validateIsNotNullAndPositive(stock.getParticipant().getIdParticipantPk())){
			sbQuery.append(" and scp.participant.idParticipantPk = :participant ");
			parameters.put("participant", stock.getParticipant().getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(stock.getSecurity()) && Validations.validateIsNotNullAndNotEmpty(stock.getSecurity().getIdSecurityCodePk())){
			sbQuery.append(" and scp.security.idSecurityCodePk = :security ");
			parameters.put("security",stock.getSecurity().getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(stock.getHolder()) && Validations.validateIsNotNullAndPositive(stock.getHolder().getIdHolderPk())){
			sbQuery.append(" and scp.holder.idHolderPk = :holder ");
			parameters.put("holder",stock.getHolder().getIdHolderPk());
		}
		if(stock.getMonthlyType()!=null && Validations.validateIsNotNullAndPositive(stock.getMonthlyType())){
			sbQuery.append(" and scp.monthlyType = :monthly ");
			parameters.put("monthly", stock.getMonthlyType());
		} else {
			sbQuery.append(" and scp.monthlyType is null ");
		}

		parameters.put("cutoffDate",stock.getCutoffDate());
		parameters.put("registryDate",stock.getRegistryDate());
		parameters.put("processState",stockStatesQuery);
		parameters.put("stockType",stock.getStockType());
		parameters.put("stockClass",stock.getStockClass());

		try {
			obj = findObjectByQueryString(sbQuery.toString(), parameters);
			if(obj!=null){
				log.error("validateExistStockCalculation for ID: "+((StockCalculationProcess)obj).getIdStockCalculationPk());
				return (StockCalculationProcess)obj;
			}
				
		} catch (NoResultException ne) {
			return null;
		} catch (NonUniqueResultException nu) {
			throw new RuntimeException("Se encontraron multiples calculos de stock");
		}
		return null;
	}

	/**
	 * Gets the securities for delivery.
	 *
	 * @param deliveryDate the delivery date
	 * @return the securities for delivery
	 */
	public List<String> getSecuritiesForDelivery(Date deliveryDate) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select co.securities.idIsinCodePk from CorporativeOperation co where ");
		sbQuery.append(" co.deliveryDate = :deliveryDate and ");
		sbQuery.append(" co.state in :states and ");
		sbQuery.append(" co.corporativeEventType.corporativeEventType in :eventTypes ");
		
		List<Integer> states = new ArrayList<Integer>();
		states.add(CorporateProcessStateType.REGISTERED.getCode());
		states.add(CorporateProcessStateType.PRELIMINARY.getCode());
		states.add(CorporateProcessStateType.MODIFIED.getCode());
		
		List<Integer> eventTypes = new ArrayList<Integer>();
		eventTypes.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		eventTypes.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		
		parameters.put("deliveryDate",deliveryDate);
		parameters.put("states",states);
		parameters.put("eventTypes",eventTypes);
		
		return findListByQueryString(sbQuery.toString(), parameters);
	} 
 	 
/**
 * Find calculation process.
 *
 * @param idStockCalculationProcess the id stock calculation process
 * @return the stock calculation process
 * @throws ServiceException the service exception
 */
public StockCalculationProcess findCalculationProcess(Long idStockCalculationProcess) throws ServiceException{
		  
		Object obj = null;  
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select scp from StockCalculationProcess scp ");
		sbQuery.append(" left join fetch scp.security se ");
		sbQuery.append(" left join fetch scp.holder hol");
		sbQuery.append(" left join fetch scp.participant par");
		sbQuery.append(" where  ");
		sbQuery.append(" scp.idStockCalculationPk = :idStockCalculationPk ");
		

		parameters.put("idStockCalculationPk", idStockCalculationProcess);

		try {
			obj = findObjectByQueryString(sbQuery.toString(), parameters);
			if(obj!=null) {
				return (StockCalculationProcess)obj;
			}
				
		} catch (NoResultException ne) {
			return null;
		} catch (NonUniqueResultException nu) {
			return null;
		} 		
		
		return null;
	}
	
	/**
	 * validate Valuator Process.
	 *
	 * @return validate Valuator Process
	 * @throws ServiceException the Service Exception
	 */
	public boolean validateValuatorProcess () throws ServiceException{
		boolean validateAux = false;
    	StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	sbQuery.append("select count(pl) from ProcessLogger pl ");
    	sbQuery.append(" where pl.businessProcess.idBusinessProcessPk = :idBusinessProcessPk ");
    	sbQuery.append(" and pl.loggerState = :loggerState "); 
    	sbQuery.append(" and pl.finishingDate is null ");
    	sbQuery.append(" and trunc(pl.processDate) = trunc(:processDate) ");
    	parameters.put("idBusinessProcessPk", BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS.getCode());
       	parameters.put("loggerState", ProcessLoggerStateType.PROCESSING.getCode());
       	parameters.put("processDate", CommonsUtilities.currentDate());
		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() && errorProcess.get(0)>0){
			validateAux=true;
		}
    	return validateAux;
	}
	
	/**
	 * validate Stock First Day.
	 *
	 * @return validate Stock First Day
	 * @throws ServiceException the Service Exception
	 */
	public boolean validateStockFirstDay() throws ServiceException{
		boolean validateAux = true;
		StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	sbQuery.append("select count(ham) from HolderAccountMovement ham ");
    	sbQuery.append(" where trunc(ham.movementDate) < trunc(:movementDate) ");
    	parameters.put("movementDate", CommonsUtilities.currentDate());
		List<Long> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty() && errorProcess.get(0)>0){
			validateAux=false;
		}
		return validateAux;
	}
	
	public boolean validateSecurityService(String idSecuriryCodePk) throws ServiceException{
		boolean validateSecurity = false;
		
		StringBuilder jpqlQuery = new StringBuilder();
		jpqlQuery.append(" select s.idSecurityCodePk from Security s ");
		jpqlQuery.append(" where s.idSecurityCodePk = :idSecuriryCodePk ");
		Query query = em.createQuery(jpqlQuery.toString());
		query.setParameter("idSecuriryCodePk", idSecuriryCodePk);
		try {
			String security = (String)query.getSingleResult();
			if (security!=null){
				validateSecurity=true;
			}
		} catch (NoResultException e) {
			validateSecurity=false;
		}
		return validateSecurity;
	}
	
	
}