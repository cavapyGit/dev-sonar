package com.pradera.corporateevents.stockcalculation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.corporateevents.stockcalculation.facade.ManageStockCalculationServiceFacade;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class StockCalculationProcessBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 27/05/2014
 */
@ViewDepositaryBean
public class StockCalculationProcessBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3567198998879589732L;
	
	/** The country residence. */
	@Inject 
	@Configurable 
	Integer countryResidence;
	
	/** The general parameter facade. */
	@EJB
	GeneralParametersFacade generalParameterFacade;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The manage stock calculation facade. */
	@EJB
	ManageStockCalculationServiceFacade manageStockCalculationFacade;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/** The list participants. */
	private List<Participant> listParticipants;
	
	/** The list stock type. */
	private List<ParameterTable> listStockType;
	
	/** The list report type. */
	private List<ParameterTable> listReportType;
	
	/** The list depositary. */
	private List<InternationalDepository> listDepositary;
	
	/** The stock calculation process. */
	private StockCalculationProcess stockCalculationProcess;
	
	/** The flag participant. */
	private boolean flagParticipant = false;
	
	/** The flag participant req. */
	private boolean flagParticipantReq = false;
	
	/** The flag holder. */
	private boolean flagHolder = false;
	
	/** The flag security. */
	private boolean flagSecurity = false;
	
	/** The flag security req. */
	private boolean flagSecurityReq = false;
	
	/** The flag monthly. */
	private boolean flagMonthly = false;
	
	/** The blparticipant. */
	private boolean blparticipant;
	
	private String securitiesCorporative;
	
	private List<String> listSecurityValidate;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		stockCalculationProcess = new StockCalculationProcess();
		stockCalculationProcess.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		stockCalculationProcess.setRegistryDate(CommonsUtilities.currentDate());
		stockCalculationProcess.setCutoffDate(CommonsUtilities.currentDate());
		stockCalculationProcess.setIndAutomactic(BooleanType.NO.getCode());
		stockCalculationProcess.setHolder(new Holder());
		stockCalculationProcess.setSecurity(new Security());
		stockCalculationProcess.setParticipant(new Participant());
		//load list
		loadParticipants();
		loadStockType();
		loadDepositaries();
		loadReportType();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			stockCalculationProcess.setStockType(StockType.PARTICIPANT.getCode());
			stockCalculationProcess.getParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			blparticipant=true;
		}
		securitiesCorporative="";
	}
	
	/**
	 * Clean stock.
	 */
	public void cleanStock() {
			flagParticipant = false;
			flagParticipantReq = false;
			flagHolder = false;
			flagSecurity = false;
			flagSecurityReq = false;
			flagMonthly = false;
			stockCalculationProcess = new StockCalculationProcess();
			stockCalculationProcess.setHolder(new Holder());
			stockCalculationProcess.setParticipant(new Participant());
			stockCalculationProcess.setSecurity(new Security());
			stockCalculationProcess.setRegistryDate(CommonsUtilities.currentDate());
			stockCalculationProcess.setCutoffDate(CommonsUtilities.currentDate());
			stockCalculationProcess.setIndAutomactic(BooleanType.NO.getCode());
			stockCalculationProcess.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			RequestContext.getCurrentInstance().reset("stockForm");
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				stockCalculationProcess.setStockType(StockType.PARTICIPANT.getCode());
				stockCalculationProcess.getParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			}
		}
	
	/**
	 * Before register stock calculation.
	 */
	public void beforeRegisterStockCalculation(){
		//ok bean validation
		//show confirmation acction
		try{
			
			if (StockType.CORPORATIVE.getCode().equals(stockCalculationProcess.getStockType()) || StockType.SECURITY.getCode().equals(stockCalculationProcess.getStockType())){
				
				/*
				String[] valoresIngresados = securitiesCorporative.split(",");
				List<String> listaValoresIngresados = null;
				
				if(valoresIngresados != null){
					listaValoresIngresados = Arrays.asList(valoresIngresados);
				}*/
				
				listSecurityValidate = new ArrayList<>();
				/*for (String list : listaValoresIngresados ){
					listSecurityValidate.add(list.trim());
				}
				*/
				listSecurityValidate.add(stockCalculationProcess.getSecurity().getIdSecurityCodePk());
				
				boolean securitySuccess =false;
				securitiesCorporative=null;
				
				for (String security : listSecurityValidate){
					if (manageStockCalculationFacade.validateSecurity(security)){
						securitySuccess=true;
					}else{
						securitySuccess=false;
						break;
					}
				}
				
				if (!securitySuccess){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_NOT_EXIST_SECURITY));
					JSFUtilities.showSimpleValidationDialog();	
					return;
				}
			}
			
			
//			if(manageStockCalculationFacade.validateStockFirstDay()){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
//						PropertiesUtilities.getMessage(PropertiesConstants.MSG_CORPORATE_EVENT_STOCK_FIRST_DAY));
//				JSFUtilities.showSimpleValidationDialog();	
//				return;
//			}
			
			
//			if(manageStockCalculationFacade.validateValuatorProcess()){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
//						PropertiesUtilities.getMessage(PropertiesConstants.MSG_CORPORATE_EVENT_PROCESS_VALUATOR));
//				JSFUtilities.showSimpleValidationDialog();	
//				return;
//			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
					PropertiesUtilities.getMessage(PropertiesConstants.STOCK_QUESTION_SAVE) );
			JSFUtilities.executeJavascriptFunction("PF('confirmAlertWidget').show()");
			
		}catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(sex.getErrorService().getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Register stock calculation.
	 */
	@LoggerAuditWeb
	@ProcessAuditLogger(process=BusinessProcessType.STOCK_CALCULATION_MANUAL_EXECUTION)
	public void registerStockCalculation(){
		try{
			stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
			
			Integer institutionCode = null;
			
			
			if (StockType.CORPORATIVE.getCode().equals(stockCalculationProcess.getStockType()) || StockType.SECURITY.getCode().equals(stockCalculationProcess.getStockType())){
				for (String securityStock :listSecurityValidate){
					Security security = new Security();
					security.setIdSecurityCodePk(securityStock);
					security = manageStockCalculationFacade.getSecurity(securityStock);
					stockCalculationProcess.setSecurity(security);

					stockCalculationProcess.setIdStockCalculationPk(null);
					stockCalculationProcess.setStockState(null);
					
					Long stockId = manageStockCalculationFacade.registerStockCalculation(stockCalculationProcess);
					log.info("Stock created "+stockId);
				}

			}else{
				Long stockId = manageStockCalculationFacade.registerStockCalculation(stockCalculationProcess);
				log.info("Stock created "+stockId);
			}
			
			String stockTypeLabel = "";
			for(ParameterTable parameter : listStockType){
				if(parameter.getParameterTablePk().equals(stockCalculationProcess.getStockType())){
					stockTypeLabel = parameter.getParameterName();
					break;
				}
			}
			
			Object[] stock = {stockTypeLabel};
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.STOCK_QUESTION_SUCESS,stock));
			JSFUtilities.executeJavascriptFunction("PF('sucessAlertWidget').show()");
		}catch(ServiceException sex){
			if(sex.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(sex.getErrorService().getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}catch(Exception e){
			log.error(e.getMessage());
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate stock type.
	 */
	public void validateStockType(){
		if(stockCalculationProcess.getStockType()!= null){
			StockType stockEnum = StockType.get(stockCalculationProcess.getStockType());
			switch (stockEnum) {
			case SECURITY:
				flagSecurity = true;
				flagSecurityReq = false;
				flagParticipant = false;
				flagParticipantReq = false;
				flagHolder = false;
				flagMonthly = false;
				break;
			case CORPORATIVE:
				flagSecurity = true;
				flagSecurityReq = false;
				flagParticipant = false;
				flagParticipantReq = false;
				flagHolder = false;
				flagMonthly = false;
				break;
			case PARTICIPANT :
				flagSecurity = false;
				flagSecurityReq = true;
				flagParticipant = true;
				flagParticipantReq = true;
				flagHolder = false;
				flagMonthly = false;
				break;
				
			case HOLDER :
				flagSecurity = false;
				flagSecurityReq = true;
				flagParticipant = true;
				flagParticipantReq = false;
				flagHolder = true;
				flagMonthly = false;
				break;
			case MONTHLY :
				flagSecurity = false;
				flagSecurityReq = true;
				flagParticipant = false;
				flagParticipantReq = false;
				flagHolder = false;
				flagMonthly = true;
				break;
			default:
				flagParticipant = false;
				flagParticipantReq = false;
				flagHolder = false;
				flagSecurity = false;
				flagSecurityReq = true;
				flagMonthly = false;
				break;
			}
			RequestContext.getCurrentInstance().reset("stockForm");
		} else {
			flagParticipant = false;
			flagParticipantReq = false;
			flagHolder = false;
			flagSecurity = false;
			flagMonthly = false;
		}
	}
	
	/**
	 * Load depositaries.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadDepositaries() throws ServiceException{
		if(listDepositary == null){
			listDepositary = new ArrayList<>();
		}
		InternationalDepository inter = new InternationalDepository();
		//not filters show all
		listDepositary = accountsFacade.getLisInternationalDepositoryServiceBean(inter);
	}
	
	/**
	 * Load stock type.
	 *
	 * @throws ServiceException the service exception
	 */
	
	/**
	 * Load stock type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadStockType() throws ServiceException{
		if(listStockType == null){
			listStockType = new ArrayList<>();
		}
		ParameterTableTO parameterTO = new ParameterTableTO();
		parameterTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTO.setMasterTableFk(MasterTableType.STOCK_CALCULATION_TYPE.getCode());
		parameterTO.setIndicator4(Integer.valueOf(1));
		listStockType = generalParameterFacade.getListParameterTableServiceBean(parameterTO);
	}
	
	/**
	 * Load report type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadReportType() throws ServiceException{
		if(listReportType == null){
			listReportType = new ArrayList<>();
		}
		ParameterTableTO parameterTO = new ParameterTableTO();
		parameterTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTO.setMasterTableFk(MasterTableType.REPORT_FORMAT.getCode());
		parameterTO.setIndicator1("1");
		listReportType = generalParameterFacade.getListParameterTableServiceBean(parameterTO);
	}
	
	/**
	 * Change date.
	 *
	 * @param selectEvent the select event
	 */
	public void changeDate(SelectEvent selectEvent){
		if(StockType.MONTHLY.getCode().equals(stockCalculationProcess.getStockType())){
			stockCalculationProcess.setRegistryDate((Date) selectEvent.getObject());
			stockCalculationProcess.setCutoffDate((Date) selectEvent.getObject());
		}
	}
	
	/**
	 * Load participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipants() throws ServiceException{
		if(listParticipants ==null){
			listParticipants = new ArrayList<>();
		}
			Participant par = new Participant();
			par.setState(ParticipantStateType.REGISTERED.getCode());
			listParticipants = accountsFacade.getLisParticipantServiceBean(par);
	}

	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 */
	public List<Participant> getListParticipants() {
		return listParticipants;
	}

	/**
	 * Sets the list participants.
	 *
	 * @param listParticipants the new list participants
	 */
	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}

	/**
	 * Gets the list stock type.
	 *
	 * @return the list stock type
	 */
	public List<ParameterTable> getListStockType() {
		return listStockType;
	}

	/**
	 * Sets the list stock type.
	 *
	 * @param listStockType the new list stock type
	 */
	public void setListStockType(List<ParameterTable> listStockType) {
		this.listStockType = listStockType;
	}

	/**
	 * Gets the list depositary.
	 *
	 * @return the list depositary
	 */
	public List<InternationalDepository> getListDepositary() {
		return listDepositary;
	}

	/**
	 * Sets the list depositary.
	 *
	 * @param listDepositary the new list depositary
	 */
	public void setListDepositary(List<InternationalDepository> listDepositary) {
		this.listDepositary = listDepositary;
	}

	/**
	 * Gets the stock calculation process.
	 *
	 * @return the stock calculation process
	 */
	public StockCalculationProcess getStockCalculationProcess() {
		return stockCalculationProcess;
	}

	/**
	 * Sets the stock calculation process.
	 *
	 * @param stockCalculationProcess the new stock calculation process
	 */
	public void setStockCalculationProcess(
			StockCalculationProcess stockCalculationProcess) {
		this.stockCalculationProcess = stockCalculationProcess;
	}

	/**
	 * Checks if is flag participant.
	 *
	 * @return true, if is flag participant
	 */
	public boolean isFlagParticipant() {
		return flagParticipant;
	}

	/**
	 * Sets the flag participant.
	 *
	 * @param flagParticipant the new flag participant
	 */
	public void setFlagParticipant(boolean flagParticipant) {
		this.flagParticipant = flagParticipant;
	}

	/**
	 * Checks if is flag holder.
	 *
	 * @return true, if is flag holder
	 */
	public boolean isFlagHolder() {
		return flagHolder;
	}

	/**
	 * Sets the flag holder.
	 *
	 * @param flagHolder the new flag holder
	 */
	public void setFlagHolder(boolean flagHolder) {
		this.flagHolder = flagHolder;
	}

	/**
	 * Checks if is flag security.
	 *
	 * @return true, if is flag security
	 */
	public boolean isFlagSecurity() {
		return flagSecurity;
	}

	/**
	 * Sets the flag security.
	 *
	 * @param flagSecurity the new flag security
	 */
	public void setFlagSecurity(boolean flagSecurity) {
		this.flagSecurity = flagSecurity;
	}

	/**
	 * Checks if is flag monthly.
	 *
	 * @return true, if is flag monthly
	 */
	public boolean isFlagMonthly() {
		return flagMonthly;
	}

	/**
	 * Sets the flag monthly.
	 *
	 * @param flagMonthly the new flag monthly
	 */
	public void setFlagMonthly(boolean flagMonthly) {
		this.flagMonthly = flagMonthly;
	}

	/**
	 * Checks if is flag participant req.
	 *
	 * @return true, if is flag participant req
	 */
	public boolean isFlagParticipantReq() {
		return flagParticipantReq;
	}

	/**
	 * Sets the flag participant req.
	 *
	 * @param flagParticipantReq the new flag participant req
	 */
	public void setFlagParticipantReq(boolean flagParticipantReq) {
		this.flagParticipantReq = flagParticipantReq;
	}

	/**
	 * Checks if is flag security req.
	 *
	 * @return true, if is flag security req
	 */
	public boolean isFlagSecurityReq() {
		return flagSecurityReq;
	}

	/**
	 * Sets the flag security req.
	 *
	 * @param flagSecurityReq the new flag security req
	 */
	public void setFlagSecurityReq(boolean flagSecurityReq) {
		this.flagSecurityReq = flagSecurityReq;
	}

	/**
	 * Checks if is blparticipant.
	 *
	 * @return true, if is blparticipant
	 */
	public boolean isBlparticipant() {
		return blparticipant;
	}

	/**
	 * Sets the blparticipant.
	 *
	 * @param blparticipant the new blparticipant
	 */
	public void setBlparticipant(boolean blparticipant) {
		this.blparticipant = blparticipant;
	}

	public String getSecuritiesCorporative() {
		return securitiesCorporative;
	}

	public void setSecuritiesCorporative(String securitiesCorporative) {
		this.securitiesCorporative = securitiesCorporative;
	}

	public List<ParameterTable> getListReportType() {
		return listReportType;
	}

	public void setListReportType(List<ParameterTable> listReportType) {
		this.listReportType = listReportType;
	}
	
}
