package com.pradera.corporateevents.stockcalculation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class StockCalculationConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/05/2014
 */
public class StockCalculationConstants {
	
	/** The Constant TYPE_STOCK_ID. */
	public static final String TYPE_STOCK_ID = "typeStock";

	/** The Constant SECURITY. */
	public static final String SECURITY = "securityCode";

	/** The Constant PARTICIPANT. */
	public static final String PARTICIPANT = "participantCode";

	/** The Constant HOLDER. */
	public static final String HOLDER = "holderCode";

	/** The Constant CUT_OFF_DATE. */
	public static final String CUT_OFF_DATE = "cutOffDate";

	/** The Constant REGISTER_DATE. */
	public static final String REGISTER_DATE = "registerDate"; 

	/** The Constant IND_MONTHLY. */
	public static final String DEPOSITARY_INTERNATIONAL = "depositaryInternational";
	
	/** The Constant CLASS_STOCK. */
	public static final String CLASS_STOCK = "stockClass";
	
	/** The Constant STOCK_ID. */
	public static final String STOCK_ID ="stockIdProcess";

}
