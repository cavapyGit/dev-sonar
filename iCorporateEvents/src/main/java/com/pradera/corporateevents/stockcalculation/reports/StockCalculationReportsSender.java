package com.pradera.corporateevents.stockcalculation.reports;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.corporativeprocess.process.CorporateProcessUtils;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * The Class StockCalculationReportsSender.
 *
 * @author PraderaTechnologies
 */
@ApplicationScoped
public class StockCalculationReportsSender implements Serializable {

		
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	 

	/** The report service. */
	@Inject
	Instance<ReportGenerationService> reportService;
	
	
	/**
	 * Send reports.
	 *
	 * @param stockProcess the stock process
	 * @param reportType the report type
	 * @throws ServiceException the service exception
	 */
	public void sendReports(StockCalculationProcess stockProcess, Long reportType) throws ServiceException{
		Date cutoffDate = stockProcess.getCutoffDate();
		Date registryDate = stockProcess.getRegistryDate();
		Integer stockType = stockProcess.getStockType();
		Long participantPk = null;
		String securityCodePk = null;
		String idIssuerPk = null;
		Long cuiCodePk = null;
		if(reportService != null){
			LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
			Map<String,String> parameters = new HashMap<String, String>();
			ReportUser user = new ReportUser();
			user.setUserName(loggerUser.getUserName());
			if(StockType.MONTHLY.getCode().equals(stockType)){
				parameters.put("cutoff_date", CommonsUtilities.convertDatetoString(cutoffDate));
				parameters.put("registry_date", CommonsUtilities.convertDatetoString(registryDate));
				//parameters.put("process_stock", stockProcess.getIdStockCalculationPk().toString());			
			}else if(StockType.SECURITY.getCode().equals(stockType)){
				securityCodePk = stockProcess.getSecurity().getIdSecurityCodePk();
				parameters.put("securityCode", securityCodePk);
				parameters.put("cutoffDate", CommonsUtilities.convertDatetoString(cutoffDate));
				parameters.put("registryDate", CommonsUtilities.convertDatetoString(registryDate));
				//parameters.put("process_stock", stockProcess.getIdStockCalculationPk().toString());
			}else if(StockType.CORPORATIVE.getCode().equals(stockType)){
				securityCodePk = stockProcess.getSecurity().getIdSecurityCodePk();
				parameters.put("securityCode", securityCodePk);
				parameters.put("cutoffDate", CommonsUtilities.convertDatetoString(cutoffDate));
				parameters.put("registryDate", CommonsUtilities.convertDatetoString(registryDate));
				//parameters.put("process_stock", stockProcess.getIdStockCalculationPk().toString());				
			}else if(StockType.PARTICIPANT.getCode().equals(stockType)){
				if(ReportIdType.PARTICIPANT_STOCK.getCode().equals(reportType)){
					if(	Validations.validateIsNotNullAndNotEmpty(stockProcess.getParticipant()) && 
							Validations.validateIsNotNullAndNotEmpty(stockProcess.getParticipant().getIdParticipantPk())){
							participantPk = stockProcess.getParticipant().getIdParticipantPk();
							parameters.put("participant", String.valueOf(participantPk));
						}
						if(	Validations.validateIsNotNullAndNotEmpty(stockProcess.getSecurity()) && 
							Validations.validateIsNotNullAndNotEmpty(stockProcess.getSecurity().getIdSecurityCodePk())){
							securityCodePk = stockProcess.getSecurity().getIdSecurityCodePk();
							parameters.put("securityCode", securityCodePk);
						}
						parameters.put("cutoffDate", CommonsUtilities.convertDatetoString(cutoffDate));
						parameters.put("registryDate", CommonsUtilities.convertDatetoString(registryDate));
				}else{
					
				}
			}else if(StockType.HOLDER.getCode().equals(stockType)){
				if(	Validations.validateIsNotNullAndNotEmpty(stockProcess.getHolder()) && 
					Validations.validateIsNotNullAndNotEmpty(stockProcess.getHolder().getIdHolderPk())){
					cuiCodePk = stockProcess.getHolder().getIdHolderPk();
					parameters.put("cuiCode", String.valueOf(cuiCodePk));
				}
				parameters.put("cutoffDate", CommonsUtilities.convertDatetoString(cutoffDate));
				parameters.put("registryDate", CommonsUtilities.convertDatetoString(registryDate));
			}else if(StockType.CONSOLIDATED_PARTICIPANT.getCode().equals(stockType)){
				if(	Validations.validateIsNotNullAndNotEmpty(stockProcess.getParticipant()) && 
						Validations.validateIsNotNullAndNotEmpty(stockProcess.getParticipant().getIdParticipantPk())){
						participantPk = stockProcess.getParticipant().getIdParticipantPk();
						parameters.put("participant", String.valueOf(participantPk));
				}
				parameters.put("cutoffDate", CommonsUtilities.convertDatetoString(cutoffDate));
				parameters.put("registryDate", CommonsUtilities.convertDatetoString(registryDate));
			}else if(StockType.CONSOLIDATED_ISSUER.getCode().equals(stockType)){
				if(	Validations.validateIsNotNullAndNotEmpty(stockProcess.getIdIssuerPk())){
						idIssuerPk = stockProcess.getIdIssuerPk();
						parameters.put("issuer", String.valueOf(idIssuerPk));
				}
				parameters.put("cutoffDate", CommonsUtilities.convertDatetoString(cutoffDate));
				parameters.put("registryDate", CommonsUtilities.convertDatetoString(registryDate));
			}
			parameters.put("stockType", stockType.toString());
			parameters.put("reportType", reportType.toString());
	    
		    if(stockProcess.getReportType() != null && stockProcess.getReportType() == ReportFormatType.XLS.getCode()) {
		    	parameters.put("reportFormats", String.valueOf(ReportFormatType.XLS.getCode()));
		    } else {
		    	parameters.put("reportFormats", String.valueOf(ReportFormatType.PDF.getCode()));
		    }
		    
			Long reportId = CorporateProcessUtils.getStockReportID(stockType,reportType);
			reportService.get().saveReportExecution(reportId, parameters, null, user, loggerUser);
		}
	}
}