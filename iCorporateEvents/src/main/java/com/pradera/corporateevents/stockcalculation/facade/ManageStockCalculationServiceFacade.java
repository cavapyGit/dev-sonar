package com.pradera.corporateevents.stockcalculation.facade;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.corporateevents.stockcalculation.service.ControllerStockServiceBean;
import com.pradera.corporateevents.stockcalculation.service.StockCalculationServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ManageStockCalculationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/05/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageStockCalculationServiceFacade {
	
   /** The log. */
   @Inject
   PraderaLogger log;
	
   /** The transaction registry. */
   @Resource
   TransactionSynchronizationRegistry transactionRegistry;
	
   /** The controller stock service bean. */
   @EJB
   ControllerStockServiceBean controllerStockServiceBean;
   
   /** The stock calculation service. */
   @EJB
   StockCalculationServiceBean stockCalculationService;
   
 
	
    /**
     * Instantiates a new manage stock calculation service facade.
     */
    public ManageStockCalculationServiceFacade() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Register Only Stock Calculation withOut Package
     * @param stockCalculation
     * @return
     * @throws ServiceException
     */
    public Long registerOnlyStockCalculation(StockCalculationProcess stockCalculation) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		  //How is required_new transaction dont propagate transactionRegistry, so we propagate with threadLocal
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        stockCalculation.setCreationDate(CommonsUtilities.currentDateTime());
        stockCalculation.setRegistryUser(loggerUser.getUserName());
        stockCalculationService.cleanFilterStock(stockCalculation);
    	stockCalculation.setStockState(StockStateType.FINISHED.getCode());
		stockCalculationService.create(stockCalculation);
    	return stockCalculation.getIdStockCalculationPk();
    }
    /**
     * Register stock calculation with Batch process.
     *
     * @param stockCalculation the stock calculation
     * @return the long
     * @throws ServiceException the service exception
     */
    public Long registerStockCalculation(StockCalculationProcess stockCalculation) throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
        //How is required_new transaction dont propagate transactionRegistry, so we propagate with threadLocal
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        stockCalculation.setCreationDate(CommonsUtilities.currentDateTime());
        stockCalculation.setRegistryUser(loggerUser.getUserName());
    	Long stockCalculationId = controllerStockServiceBean.registerStockCalculation(stockCalculation);
    	if(stockCalculationId != null){
    		stockCalculationService.fireBatchStockCalculation(stockCalculation);
    		//if type is for Security fire stock class report
    		if(stockCalculation.getStockType().equals(StockType.CORPORATIVE.getCode())){
    			StockCalculationProcess stockCalculationProcess = new StockCalculationProcess();
    			stockCalculationProcess.setCreationDate(CommonsUtilities.currentDateTime());
    			stockCalculationProcess.setCutoffDate(stockCalculation.getCutoffDate());
    			stockCalculationProcess.setRegistryDate(stockCalculation.getRegistryDate());
    			stockCalculationProcess.setRegistryUser(stockCalculation.getRegistryUser());
    			stockCalculationProcess.setIndAutomactic(stockCalculation.getIndAutomactic());
    			stockCalculationProcess.setStockClass(StockClassType.REPORT.getCode());
    			stockCalculationProcess.setStockType(stockCalculation.getStockType());
    			stockCalculationProcess.setSecurity(stockCalculation.getSecurity());
    			stockCalculationProcess.setHolder(stockCalculation.getHolder());
    			stockCalculationProcess.setParticipant(stockCalculation.getParticipant());
    			stockCalculationProcess.setReportType(stockCalculation.getReportType());
    			if(controllerStockServiceBean.registerStockCalculation(stockCalculationProcess) != null){
    				stockCalculationService.fireBatchStockCalculation(stockCalculationProcess);
    			}
    		}
    	}
    	return stockCalculationId;
    }
    
    
    /**
     * validate Valuator Process.
     *
     * @return validate Valuator Process
     * @throws ServiceException the Service Exception
     */
	public boolean validateValuatorProcess () throws ServiceException{
		return stockCalculationService.validateValuatorProcess();
	}
	
	/**
     * validate Valuator Process.
     *
     * @return validate Valuator Process
     * @throws ServiceException the Service Exception
     */
	public Security getSecurity (String idSecurity) throws ServiceException{
		return controllerStockServiceBean.getSecurity(idSecurity);
	}
	
	/**
	 * validate Stock First Day.
	 *
	 * @return validate Stock First Day
	 * @throws ServiceException the Service Exception
	 */
	public boolean validateStockFirstDay() throws ServiceException{
		return stockCalculationService.validateStockFirstDay();
	}
	
	/**
	 * Execute Stock Calculation Monthly.
	 *
	 * @return id Stock Calculation Monthly
	 * @throws ServiceException the Service Exception
	 */
    public Long executeStockCalculationMonthly() throws ServiceException{
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		StockCalculationProcess stockCalculationProcess = new StockCalculationProcess();
		stockCalculationProcess.setCreationDate(CommonsUtilities.currentDateTime());
		stockCalculationProcess.setCutoffDate(CommonsUtilities.currentDate());
		stockCalculationProcess.setRegistryDate(CommonsUtilities.currentDate());
		stockCalculationProcess.setRegistryUser(loggerUser.getUserName());
		stockCalculationProcess.setIndAutomactic(BooleanType.YES.getCode());
		stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
		stockCalculationProcess.setStockType(StockType.MONTHLY.getCode());
		return controllerStockServiceBean.executeStockMonthly(stockCalculationProcess, loggerUser);
    }
    
    public boolean validateSecurity(String idSecurityCodePk) throws ServiceException{
    	return stockCalculationService.validateSecurityService(idSecurityCodePk);
    }
}
