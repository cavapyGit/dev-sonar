package com.pradera.corporateevents.stockcalculation.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.corporateevents.stockcalculation.facade.ManageStockCalculationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class StockCalculationMonthlyBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="StockCalculationMonthlyBatch")
@RequestScoped
public class StockCalculationMonthlyBatch implements JobExecution, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The obj manage stock calculation service facade. */
	@EJB
	ManageStockCalculationServiceFacade objManageStockCalculationServiceFacade;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * start Job.
	 *
	 * @param processLogger the process logger
	 */
	@Override
	public void startJob(ProcessLogger processLogger){
		log.error("Execute Stock Calculation Monthly\n");
		try {
			executeStockCalculationMonthly();
		} catch (ServiceException e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Execute Stock Calculation Monthly.
	 *
	 * @throws ServiceException the base exception
	 */
	public void executeStockCalculationMonthly()throws ServiceException{
		log.error("<<<<<<<<<<<<<<<<< INICIO DE EJECUCION DEL PROCESO DEMONIO DE CALCULO DE STOCK MENSUAL: >>>>>>>>>>>>>>>>>>>>>>>>>"+
				"Fecha:"+CommonsUtilities.currentDate()+"Hora:"+CommonsUtilities.currentTime()+"\n");
		try{
			Long stockCalculationId = objManageStockCalculationServiceFacade.executeStockCalculationMonthly();
			if(Validations.validateIsNull(stockCalculationId)){
				log.error("Error in Execute Stock Calculation Monthly\n");
			}
		}catch (Exception e) {
			log.error(e.getMessage()+"\n");
			throw new RuntimeException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}	
}
