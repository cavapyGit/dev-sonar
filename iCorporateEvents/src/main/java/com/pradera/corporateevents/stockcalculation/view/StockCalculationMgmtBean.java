package com.pradera.corporateevents.stockcalculation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.log4j.Logger;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.corporateevents.stockcalculation.facade.ManageStockCalculationServiceFacade;
import com.pradera.corporateevents.stockcalculation.reports.StockCalculationReportsSender;
import com.pradera.corporateevents.utils.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockClassType;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * The Class StockCalculationMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class StockCalculationMgmtBean extends GenericBaseBean implements Serializable {

	//Services
	/** The parameters facade. */
	@EJB
	GeneralParametersFacade parametersFacade;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;

	/** The batch service facade. */
	@EJB
	BatchProcessServiceFacade batchServiceFacade;
	
	/** The stock service facade. */
	@EJB
	ManageStockCalculationServiceFacade stockServiceFacade;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The report sender. */
	@Inject
	StockCalculationReportsSender reportSender;
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(StockCalculationMgmtBean.class);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4219150912574636719L;
	
	/*Listas*/
	/** The list type stocks. */
	private List<SelectItem> listTypeStocks;
	
	/** The list type montlhy. */
	private List<SelectItem> listTypeMontlhy;
	
	/** The list participants. */
	private List<Participant> listParticipants;
	
	/** The list participants. */
	private List<Issuer> listIssuers;
	
	/** The list depositary. */
	private List<InternationalDepository> listDepositary;
	/*Variables del negocio*/
	/** The stock calculation process. */
	private StockCalculationProcess stockCalculationProcess;
	
	/** The international depositary. */
	private InternationalDepository internationalDepositary;
	/*User Info*/
	/** The participant login. */
	private Long participantLogin = new Long(0);
	
	/** The is participant. */
	private boolean isParticipant = false;
	
	/** The is cevaldom. */
	private boolean isCevaldom = false;
	/*Campos Habilitados*/
	/** The flag participant. */
	private boolean flagParticipant = false;
	
	/** The flag issuer. */
	private boolean flagIssuer = false;
	
	/** The flag holder. */
	private boolean flagHolder = false;
	
	/** The flag security. */
	private boolean flagSecurity = false;
	
	/** The flag security req. */
	private boolean flagSecurityReq = false;
	
	/** The flag monthly. */
	private boolean flagMonthly = false;
	/*variables logicas*/
	/** The str redirect. */
	private String strRedirect="";
	
	/** The now time. */
	private Date nowTime = new Date();
	
	/** The min time. */
	private Date minTime = new Date();
	
	/** The max days of custody request. */
	private Integer maxDaysOfCustodyRequest=null;
	
	/** The stock rep type. */
	private Long stockRepType = null;
	
	/** The report types. */
	private List<ReportIdType> reportTypes = null;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() { 
		try {
			loadUserInfo();
//			if(!isCevaldom){
//				String propertieConstant = PropertiesConstants.STOCK_DONT_PRIVILEGES;
//				alert(PropertiesUtilities.getMessage(propertieConstant));
//				strRedirect="defaultPageRule";
//			}
			listTypeStocks = new ArrayList<SelectItem>();
			listTypeMontlhy = new ArrayList<SelectItem>();
			listParticipants = new ArrayList<Participant>();
			listIssuers = new ArrayList<Issuer>();
			listDepositary = new ArrayList<InternationalDepository>();

			loadParticipants();
			loadIssuers();
			loadStockType();
			loadDepositaries();

			nowTime = CommonsUtilities.currentDate();
			maxDaysOfCustodyRequest = 365;
			internationalDepositary = new InternationalDepository();
			stockCalculationProcess = new StockCalculationProcess();
			stockCalculationProcess.setRegistryDate(CommonsUtilities.currentDate());
			stockCalculationProcess.setCutoffDate(CommonsUtilities.currentDate());
			stockCalculationProcess.setHolder(new Holder());
			stockCalculationProcess.setParticipant(new Participant());
			stockCalculationProcess.setSecurity(new Security());
			reportTypes = new ArrayList<ReportIdType>();
		} catch(Exception ex) {
			log.error(ex.getMessage());
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Load user info.
	 */
	public void loadUserInfo(){
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			isParticipant = true;
			if( Validations.validateIsNotNullAndPositive(userInfo.getUserAccountSession().getParticipantCode()) )
				participantLogin = userInfo.getUserAccountSession().getParticipantCode();
		}	
		
		if(userInfo.getUserAccountSession().isDepositaryInstitution())
			isCevaldom = true;
	}
	
	/**
	 * Load participants.
	 */
	public void loadParticipants(){
		try {
			listParticipants = new ArrayList<Participant>();
			Participant par = new Participant();
			par.setState(ParticipantStateType.REGISTERED.getCode());
			listParticipants = accountsFacade.getLisParticipantServiceBean(par);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load issuers.
	 */
	public void loadIssuers(){
		try {
			listIssuers = new ArrayList<Issuer>();
			Issuer iss = new Issuer();
			iss.setStateIssuer(IssuerStateType.REGISTERED.getCode());
			listIssuers = accountsFacade.getLisIssuerServiceBean(iss);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load stock type.
	 */
	public void loadStockType(){
		try {
		ParameterTableTO parameterTO = new ParameterTableTO();
		parameterTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTO.setMasterTableFk(MasterTableType.STOCK_CALCULATION_TYPE.getCode());
			for(ParameterTable parameter : parametersFacade.getListParameterTableServiceBean(parameterTO)) {
				SelectItem item = new SelectItem();
				item.setLabel(parameter.getParameterName());
				item.setValue(parameter.getParameterTablePk());
				listTypeStocks.add(item);
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load depositaries.
	 */
	public void loadDepositaries(){
		try {
			listDepositary = new ArrayList<InternationalDepository>();
			InternationalDepository inter = new InternationalDepository();
			listDepositary = accountsFacade.getLisInternationalDepositoryServiceBean(inter);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Valid stock type.
	 */
	public void validStockType(){
		if(reportTypes == null){
			reportTypes = new ArrayList<ReportIdType>();
		}
		reportTypes.clear();
		if(StockType.HOLDER.getCode().equals(stockCalculationProcess.getStockType())){
			flagParticipant = true;
			flagIssuer = false;
			flagHolder = true;
			flagSecurity = true;
			flagSecurityReq = true;
			flagMonthly = false;
			reportTypes.add(ReportIdType.HOLDER_STOCK);
		}else if(StockType.PARTICIPANT.getCode().equals(stockCalculationProcess.getStockType())){
			flagParticipant = true;
			flagIssuer = false;
			flagHolder = false;
			flagSecurity = true;
			flagSecurityReq = true;
			flagMonthly = false;
			reportTypes.add(ReportIdType.PARTICIPANT_STOCK);
		}else if(StockType.SECURITY.getCode().equals(stockCalculationProcess.getStockType())){
			flagParticipant = false;
			flagIssuer = false;
			flagHolder = false;
			flagSecurity = true;
			flagSecurityReq = false;
			flagMonthly = false;
			reportTypes.add(ReportIdType.SECURITY_STOCK);
			//This is the new report for security(POR VALOR)
			reportTypes.add(ReportIdType.NEGATIVE_STOCK);
			reportTypes.add(ReportIdType.CALCULATION_STOCK_BY_OWNER);
			reportTypes.add(ReportIdType.CALCULATION_STOCK_BY_BROKER);
			reportTypes.add(ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_VALUE);
			reportTypes.add(ReportIdType.STOCK_CALCULATION_BY_REPORTED);
			reportTypes.add(ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE);
		}else if(StockType.MONTHLY.getCode().equals(stockCalculationProcess.getStockType())){
			flagParticipant = false;
			flagIssuer = false;
			flagHolder = false;
			flagSecurity = false;
			flagSecurityReq = true;
			flagMonthly = true;
			//This is the new report for monthly
			reportTypes.add(ReportIdType.MONTHLY_STOCK);
			reportTypes.add(ReportIdType.NEGATIVE_STOCK_BY_MONTHLY);
			reportTypes.add(ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_MONTHLY);
		} else if(StockType.CORPORATIVE.getCode().equals(stockCalculationProcess.getStockType())){
			flagParticipant = false;
			flagIssuer = false;
			flagHolder = false;
			flagSecurity = true;
			flagSecurityReq = false;
			flagMonthly = false;
			reportTypes.add(ReportIdType.SECURITY_STOCK_FOR_TYPE_CORPORATIVE);
			//This is the new report for security(POR VALOR)
			reportTypes.add(ReportIdType.NEGATIVE_STOCK);
			reportTypes.add(ReportIdType.CALCULATION_STOCK_BY_OWNER);
			reportTypes.add(ReportIdType.CALCULATION_STOCK_BY_BROKER);
			reportTypes.add(ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_VALUE);
			reportTypes.add(ReportIdType.STOCK_CALCULATION_BY_REPORTED);
			reportTypes.add(ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE);
		} else if(StockType.CONSOLIDATED_PARTICIPANT.getCode().equals(stockCalculationProcess.getStockType())){
			flagParticipant = true;
			flagIssuer = false;
			flagHolder = false;
			flagSecurity = false;
			flagSecurityReq = true;
			flagMonthly = false;
			reportTypes.add(ReportIdType.CONSOLIDATED_STOCK_PARTICIPANT);
			//This is the new report for security(POR VALOR)
//			reportTypes.add(ReportIdType.NEGATIVE_STOCK);
//			reportTypes.add(ReportIdType.CALCULATION_STOCK_BY_OWNER);
//			reportTypes.add(ReportIdType.CALCULATION_STOCK_BY_BROKER);
//			reportTypes.add(ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_VALUE);
//			reportTypes.add(ReportIdType.STOCK_CALCULATION_BY_REPORTED);
//			reportTypes.add(ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE);
		} else if(StockType.CONSOLIDATED_ISSUER.getCode().equals(stockCalculationProcess.getStockType())){
			flagParticipant = false;
			flagIssuer = true;
			flagHolder = false;
			flagSecurity = false;
			flagSecurityReq = true;
			flagMonthly = false;
			reportTypes.add(ReportIdType.CONSOLIDATED_STOCK_ISSUER);
			//This is the new report for security(POR VALOR)
//			reportTypes.add(ReportIdType.NEGATIVE_STOCK);
//			reportTypes.add(ReportIdType.CALCULATION_STOCK_BY_OWNER);
//			reportTypes.add(ReportIdType.CALCULATION_STOCK_BY_BROKER);
//			reportTypes.add(ReportIdType.STOCK_SUMMARY_NOT_SETTELLED_BY_VALUE);
//			reportTypes.add(ReportIdType.STOCK_CALCULATION_BY_REPORTED);
//			reportTypes.add(ReportIdType.STOCK_INCONSISTENCIES_BY_VALUE);
		}else{
			flagParticipant = false;
			flagIssuer = false;
			flagHolder = false;
			flagSecurity = false;
			flagSecurityReq = true;
			flagMonthly = false;
		}
		/*
		if(!flagParticipant){
			stockCalculationProcess.setParticipant(null);
		}
		*/
		internationalDepositary = new InternationalDepository();
		stockCalculationProcess.setHolder(new Holder());
		stockCalculationProcess.setParticipant(new Participant());
		stockCalculationProcess.setSecurity(new Security());
	}
	
	/**
	 * Confirm register stock.
	 */
	public void confirmRegisterStock(){
		
		/*Validaciones generales*/
		
		/*validaciones especificas*/
		if( stockCalculationProcess.getStockType().equals(StockType.HOLDER.getCode()) ){
			if(Validations.validateIsNotNull(stockCalculationProcess.getHolder())){
				if(!Validations.validateIsNotNull(stockCalculationProcess.getHolder().getIdHolderPk())){
					String propertieConstant = PropertiesConstants.STOCK_DONT_RNT;
					alert(PropertiesUtilities.getMessage(propertieConstant));
					return;
				}
			}else{
				String propertieConstant = PropertiesConstants.STOCK_DONT_RNT;
				alert(PropertiesUtilities.getMessage(propertieConstant));
				return;
			}
		}else if( stockCalculationProcess.getStockType().equals(StockType.PARTICIPANT.getCode()) ){
			if(Validations.validateIsNotNull(stockCalculationProcess.getParticipant())){
				if(!Validations.validateIsNotNullAndPositive(stockCalculationProcess.getParticipant().getIdParticipantPk())){
					String propertieConstant = PropertiesConstants.STOCK_DONT_PARTICIPANT;
					alert(PropertiesUtilities.getMessage(propertieConstant));
					return;
				}
			}else{
				String propertieConstant = PropertiesConstants.STOCK_DONT_PARTICIPANT;
				alert(PropertiesUtilities.getMessage(propertieConstant));
				return;
			}
		}else if( stockCalculationProcess.getStockType().equals(StockType.SECURITY.getCode()) ){
			if(Validations.validateIsNotNull(stockCalculationProcess.getSecurity())){
				if(!Validations.validateIsNotNull(stockCalculationProcess.getSecurity().getIdSecurityCodePk())){
					String propertieConstant = PropertiesConstants.STOCK_DONT_SECURITY;
					alert(PropertiesUtilities.getMessage(propertieConstant));
					return;
				}
			}else{
				String propertieConstant = PropertiesConstants.STOCK_DONT_SECURITY;
				alert(PropertiesUtilities.getMessage(propertieConstant));
				return;
			}
		}else if( stockCalculationProcess.getStockType().equals(StockType.CORPORATIVE.getCode()) ){
			if(Validations.validateIsNotNull(stockCalculationProcess.getSecurity())){
				if(!Validations.validateIsNotNull(stockCalculationProcess.getSecurity().getIdSecurityCodePk())){
					String propertieConstant = PropertiesConstants.STOCK_DONT_SECURITY;
					alert(PropertiesUtilities.getMessage(propertieConstant));
					return;
				}
			}else{
				String propertieConstant = PropertiesConstants.STOCK_DONT_SECURITY;
				alert(PropertiesUtilities.getMessage(propertieConstant));
				return;
			}
		}else if( stockCalculationProcess.getStockType().equals(StockType.MONTHLY.getCode()) ){
			
		}
		String propertieConstant = PropertiesConstants.STOCK_QUESTION_SAVE;
		alertConfirm(PropertiesUtilities.getMessage(propertieConstant));
	}
	
	/**
	 * Register stock.
	 */
	@LoggerAuditWeb
	public void registerStock() {
	
		try {
			/*Llamo al batch*/
			if(registerStockProcess(StockClassType.NORMAL)){				
				if(!registerStockProcess(StockClassType.REPORT)){
					alert("Ocurrio un error inesperado");
					return;
				}
			}else{
				alert("Ocurrio un error inesperado");
				return;
			}
			/*Limpio*/
			cleanStock();
			/*Envio Alerta*/
			String propertieConstant = PropertiesConstants.STOCK_QUESTION_SUCESS;
			alert(PropertiesUtilities.getMessage(propertieConstant));
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Register stock process.
	 *
	 * @param classType the class type
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerStockProcess(StockClassType classType) throws ServiceException{
		stockCalculationProcess.setIdStockCalculationPk(null);
		stockCalculationProcess.setStockState(StockStateType.REGISTERED.getCode());
		stockCalculationProcess.setCreationDate(CommonsUtilities.currentDateTime());
		stockCalculationProcess.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		stockCalculationProcess.setStockClass(classType.getCode());
		stockCalculationProcess.setIndAutomactic(BooleanType.YES.getCode());
		
//		stockServiceFacade.createStockCalculation(stockCalculationProcess);
		log.info("Pk del proceso que se enviara: "+stockCalculationProcess.getIdStockCalculationPk());
		
		if(Validations.validateIsNotNullAndPositive(stockCalculationProcess.getIdStockCalculationPk())){
			
			BusinessProcess objBusinessProcess= new BusinessProcess();
			objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.STOCK_CALCULATION_MANUAL_EXECUTION.getCode());
			
			Map<String, Object> mpParameters= new HashMap<String, Object>();
			mpParameters.put(ComponentConstant.STOCK_CALCULATION_PK, stockCalculationProcess.getIdStockCalculationPk());
			batchServiceFacade.registerBatch( userInfo.getUserAccountSession().getUserName() , objBusinessProcess, mpParameters);
			//userInfo.getUserAccountSession().getFullName()
		}else{
			return false;
		}
		return true;
	}
	
	/**
	 * Clean stock.
	 */
	public void cleanStock() {
		
		if(reportTypes != null){
			reportTypes.clear();
		}
		flagParticipant = false;
		flagIssuer = false;
		flagHolder = false;
		flagSecurity = false;
		flagSecurityReq = false;
		flagMonthly = false;
		stockCalculationProcess = new StockCalculationProcess();
		internationalDepositary = new InternationalDepository();
		stockCalculationProcess.setHolder(new Holder());
		stockCalculationProcess.setParticipant(new Participant());
		stockCalculationProcess.getParticipant().setIdParticipantPk(new Long(-1));
		stockCalculationProcess.setSecurity(new Security());
		stockCalculationProcess.setRegistryDate(CommonsUtilities.currentDate());
		stockCalculationProcess.setCutoffDate(CommonsUtilities.currentDate());
	}

	/**
	 * Instantiates a new stock calculation mgmt bean.
	 */
	public StockCalculationMgmtBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Alert.
	 *
	 * @param strBody the str body
	 */
	public void alert(String strBody){
		String strHeader=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,
				 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				 GeneralConstants.LBL_HEADER_ALERT_WARNING);
		
		showMessageOnDialog(strHeader,strBody); 
		JSFUtilities.showComponent("formPopUp:alterValidation"); 
	}
	
	/**
	 * Alert confirm.
	 *
	 * @param strBody the str body
	 */
	public void alertConfirm(String strBody){
		String strHeader=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,
				 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				 GeneralConstants.LBL_HEADER_ALERT_REGISTER);
		
		showMessageOnDialog(strHeader,strBody); 
		JSFUtilities.showComponent("formPopUp:confirmAlert"); 
	}
	
	/**
	 * Alert.
	 *
	 * @param strBody the str body
	 * @param strHeader the str header
	 */
	public void alert(String strBody,String strHeader){
		showMessageOnDialog(strHeader,strBody); 
		JSFUtilities.showComponent("formPopUp:alterValidation"); 
	}
	
	/**
	 * Removes the alert.
	 */
	public void removeAlert(){
		JSFUtilities.hideComponent("formPopUp:alterValidation");
		JSFUtilities.hideComponent("formPopUp:confirmAlert");
	}
	
	/**
	 * Confirm actions.
	 *
	 * @return the string
	 */
	public String confirmActions(){
		/*si es nullo, se queda en la misma pantalla*/
		String redirectVar=null;
		
		if( strRedirect.equals("defaultPageRule") ){
			redirectVar = strRedirect;
			strRedirect = "";
		}
		
		/*remuevo la alerta*/
		removeAlert();
		return redirectVar;
	}
	
	/**
	 * Onchange initial date.
	 */
	public void onchangeInitialDate(){
		Date initialDate = stockCalculationProcess.getCutoffDate();
		Date endDate = 	stockCalculationProcess.getRegistryDate();
		minTime = CommonsUtilities.addDate(endDate, -maxDaysOfCustodyRequest);
		String propertieConstant = PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE;
		if(evaluateDates(initialDate,endDate)){
			alert(PropertiesUtilities.getMessage(propertieConstant));
		}
	}
	
	/**
	 * Onchange end date.
	 */
	public void onchangeEndDate(){
		Date initialDate = stockCalculationProcess.getCutoffDate();
		Date endDate = 	stockCalculationProcess.getRegistryDate();
		minTime = CommonsUtilities.addDate(endDate, -maxDaysOfCustodyRequest);
		
		if(evaluateDates(initialDate,endDate)){
			String propertieConstant = PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE;
			alert(PropertiesUtilities.getMessage(propertieConstant));
			return;
		}
		if(evaluateDates(endDate,CommonsUtilities.currentDateTime())){
			String propertieConstant = PropertiesConstants.END_DATE_GREATER_THAN_SYSTEM_DATE;
			alert(PropertiesUtilities.getMessage(propertieConstant));
			return;
		}
	}
	
	/**
	 * Evaluate dates.
	 * Evaluate if firstDate is greater than secondDate
	 *
	 * @param firstDate the first date
	 * @param secondDate the second date
	 * @return the boolean
	 */
	public Boolean evaluateDates(Date firstDate, Date secondDate) {
		Boolean greaterDate = Boolean.FALSE;
		if (firstDate.after(secondDate)) {
			greaterDate = Boolean.TRUE;
		}
		return greaterDate;
	}
	
	/**
	 * Getters And Setters*.
	 *
	 * @return the list type stocks
	 */

	public List<SelectItem> getListTypeStocks() {
		return listTypeStocks;
	}

	/**
	 * Sets the list type stocks.
	 *
	 * @param listTypeStocks the new list type stocks
	 */
	public void setListTypeStocks(List<SelectItem> listTypeStocks) {
		this.listTypeStocks = listTypeStocks;
	}

	/**
	 * Gets the list type montlhy.
	 *
	 * @return the list type montlhy
	 */
	public List<SelectItem> getListTypeMontlhy() {
		return listTypeMontlhy;
	}

	/**
	 * Sets the list type montlhy.
	 *
	 * @param listTypeMontlhy the new list type montlhy
	 */
	public void setListTypeMontlhy(List<SelectItem> listTypeMontlhy) {
		this.listTypeMontlhy = listTypeMontlhy;
	}

	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 */
	public List<Participant> getListParticipants() {
		return listParticipants;
	}

	/**
	 * Sets the list participants.
	 *
	 * @param listParticipants the new list participants
	 */
	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}
	
	/**
	 * Gets the list issuers.
	 *
	 * @return the list issuers
	 */
	public List<Issuer> getListIssuers() {
		return listIssuers;
	}
	
	/**
	 * Sets the list issuers.
	 *
	 * @param listIssuers the new list issuers
	 */
	public void setListIssuers(List<Issuer> listIssuers) {
		this.listIssuers = listIssuers;
	}

	/**
	 * Gets the stock calculation process.
	 *
	 * @return the stock calculation process
	 */
	public StockCalculationProcess getStockCalculationProcess() {
		return stockCalculationProcess;
	}

	/**
	 * Sets the stock calculation process.
	 *
	 * @param stockCalculationProcess the new stock calculation process
	 */
	public void setStockCalculationProcess(
			StockCalculationProcess stockCalculationProcess) {
		this.stockCalculationProcess = stockCalculationProcess;
	}

	/**
	 * Gets the participant login.
	 *
	 * @return the participant login
	 */
	public Long getParticipantLogin() {
		return participantLogin;
	}

	/**
	 * Sets the participant login.
	 *
	 * @param participantLogin the new participant login
	 */
	public void setParticipantLogin(Long participantLogin) {
		this.participantLogin = participantLogin;
	}

	/**
	 * Checks if is participant.
	 *
	 * @return true, if is participant
	 */
	public boolean isParticipant() {
		return isParticipant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param isParticipant the new participant
	 */
	public void setParticipant(boolean isParticipant) {
		this.isParticipant = isParticipant;
	}

	/**
	 * Checks if is cevaldom.
	 *
	 * @return true, if is cevaldom
	 */
	public boolean isCevaldom() {
		return isCevaldom;
	}

	/**
	 * Sets the cevaldom.
	 *
	 * @param isCevaldom the new cevaldom
	 */
	public void setCevaldom(boolean isCevaldom) {
		this.isCevaldom = isCevaldom;
	}

	/**
	 * Checks if is flag participant.
	 *
	 * @return true, if is flag participant
	 */
	public boolean isFlagParticipant() {
		return flagParticipant;
	}

	/**
	 * Sets the flag participant.
	 *
	 * @param flagParticipant the new flag participant
	 */
	public void setFlagParticipant(boolean flagParticipant) {
		this.flagParticipant = flagParticipant;
	}
	
	/**
	 * Checks if is flag issuer.
	 *
	 * @return true, if is flag issuer
	 */
	public boolean isFlagIssuer() {
		return flagIssuer;
	}
	
	/**
	 * Sets the flag issuer.
	 *
	 * @param flagIssuer the new flag issuer
	 */
	public void setFlagIssuer(boolean flagIssuer) {
		this.flagIssuer = flagIssuer;
	}

	/**
	 * Checks if is flag holder.
	 *
	 * @return true, if is flag holder
	 */
	public boolean isFlagHolder() {
		return flagHolder;
	}

	/**
	 * Sets the flag holder.
	 *
	 * @param flagHolder the new flag holder
	 */
	public void setFlagHolder(boolean flagHolder) {
		this.flagHolder = flagHolder;
	}

	/**
	 * Checks if is flag security.
	 *
	 * @return true, if is flag security
	 */
	public boolean isFlagSecurity() {
		return flagSecurity;
	}

	/**
	 * Sets the flag security.
	 *
	 * @param flagSecurity the new flag security
	 */
	public void setFlagSecurity(boolean flagSecurity) {
		this.flagSecurity = flagSecurity;
	}
	
	/**
	 * Checks if is flag security req.
	 *
	 * @return true, if is flag security req
	 */
	public boolean isFlagSecurityReq() {
		return flagSecurityReq;
	}
	
	/**
	 * Sets the flag security req.
	 *
	 * @param flagSecurity the new flag security req
	 */
	public void setFlagSecurityReq(boolean flagSecurityReq) {
		this.flagSecurityReq = flagSecurityReq;
	}

	/**
	 * Checks if is flag monthly.
	 *
	 * @return true, if is flag monthly
	 */
	public boolean isFlagMonthly() {
		return flagMonthly;
	}

	/**
	 * Sets the flag monthly.
	 *
	 * @param flagMonthly the new flag monthly
	 */
	public void setFlagMonthly(boolean flagMonthly) {
		this.flagMonthly = flagMonthly;
	}

	/**
	 * Gets the international depositary.
	 *
	 * @return the international depositary
	 */
	public InternationalDepository getInternationalDepositary() {
		return internationalDepositary;
	}

	/**
	 * Sets the international depositary.
	 *
	 * @param internationalDepositary the new international depositary
	 */
	public void setInternationalDepositary(
			InternationalDepository internationalDepositary) {
		this.internationalDepositary = internationalDepositary;
	}

	/**
	 * Gets the list depositary.
	 *
	 * @return the list depositary
	 */
	public List<InternationalDepository> getListDepositary() {
		return listDepositary;
	}

	/**
	 * Sets the list depositary.
	 *
	 * @param listDepositary the new list depositary
	 */
	public void setListDepositary(List<InternationalDepository> listDepositary) {
		this.listDepositary = listDepositary;
	}

	/**
	 * Gets the now time.
	 *
	 * @return the now time
	 */
	public Date getNowTime() {
		return nowTime;
	}

	/**
	 * Sets the now time.
	 *
	 * @param nowTime the new now time
	 */
	public void setNowTime(Date nowTime) {
		this.nowTime = nowTime;
	}

	/**
	 * Gets the min time.
	 *
	 * @return the min time
	 */
	public Date getMinTime() {
		return minTime;
	}

	/**
	 * Sets the min time.
	 *
	 * @param minTime the new min time
	 */
	public void setMinTime(Date minTime) {
		this.minTime = minTime;
	}

	
	
	
	

	/**
	 * Gets the stock rep type.
	 *
	 * @return the stockRepType
	 */
	public Long getStockRepType() {
		return stockRepType;
	}

	/**
	 * Sets the stock rep type.
	 *
	 * @param stockRepType the stockRepType to set
	 */
	public void setStockRepType(Long stockRepType) {
		this.stockRepType = stockRepType;
	}

	/**
	 * Send stock reports.
	 */
	@LoggerAuditWeb
	public void sendStockReports(){
		try{
			stockCalculationProcess.setStockClass(StockClassType.NORMAL.getCode());
			reportSender.sendReports(stockCalculationProcess, stockRepType);
			JSFUtilities.showComponent(":formPopUp:idDialCevaldom");
			JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.STOCK_REPORT_SUCESS, null);
			return;
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}

	/**
	 * Gets the report types.
	 *
	 * @return the reportTypes
	 */
	public List<ReportIdType> getReportTypes() {
		return reportTypes;
	}

	/**
	 * Sets the report types.
	 *
	 * @param reportTypes the reportTypes to set
	 */
	public void setReportTypes(List<ReportIdType> reportTypes) {
		this.reportTypes = reportTypes;
	}
	
}
