package com.pradera.corporateevents.stockcalculation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.corporatives.stockcalculation.StockCalculationProcess;
import com.pradera.model.corporatives.stockcalculation.type.StockStateType;
import com.pradera.model.corporatives.stockcalculation.type.StockType;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ControllerStockServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22/05/2014
 */
@Singleton
@AccessTimeout(unit=TimeUnit.MINUTES,value=10)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class ControllerStockServiceBean {
	
	/** The stock calculation service. */
	@EJB
	StockCalculationServiceBean stockCalculationService;
	
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The em. */
	@Inject @DepositaryDataBase
	protected EntityManager em;
	

    /**
     * Default constructor. 
     */
    public ControllerStockServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Register stock calculation.
     *
     * @param stockCalculation the stock calculation
     * @return the long
     * @throws ServiceException the service exception
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Interceptors(ContextHolderInterceptor.class)
    public Long registerStockCalculation(StockCalculationProcess stockCalculation) throws ServiceException{
    	Long idStockCalculation = null;
    	//Validate if exist same stock running
    	stockCalculationService.cleanFilterStock(stockCalculation);
    	List<Integer> lstStockState = new ArrayList<Integer>();
		lstStockState.add(StockStateType.PROCESSING.getCode());
		lstStockState.add(StockStateType.REGISTERED.getCode());
		StockCalculationProcess stockOld = stockCalculationService.validateExistStockCalculation(stockCalculation, lstStockState);
		if(stockOld == null){
			stockCalculation.setStockState(StockStateType.REGISTERED.getCode());
			stockCalculationService.create(stockCalculation);
		} else {
			log.error("ERROR: Tiene un calculo de Stock: "+stockOld.getIdStockCalculationPk());
			//throw message already same stock running 
//			log.error("ERROR: Existe un calculo de stock del valor: "+stockCalculation.getSecurity().getIdSecurityCodePk());
			throw new ServiceException(ErrorServiceType.STOCK_CALCULATION_PROCESS_EXIST);
		}
		if(stockCalculation.getIdStockCalculationPk()!=null)
			idStockCalculation=(Long)stockCalculation.getIdStockCalculationPk();
		return idStockCalculation;
    }
    
    /**
     * Get Security.
     *
     * @param idSecurity code security
     * @return object security
     * @throws ServiceException the Service Exception
     */
    public Security getSecurity (String idSecurity)throws ServiceException{
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select se from Security se where  ");
		sbQuery.append(" se.idSecurityCodePk = :idSecurity");
		TypedQuery<Security> query = em.createQuery(sbQuery.toString(), Security.class);
		query.setParameter("idSecurity", idSecurity);
		return query.getSingleResult();
    }
    
    /**
     * Execute stock monthly.
     *
     * @param stockCalculation the stock calculation
     * @param loggerUser the logger user
     * @return the long
     * @throws ServiceException the service exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long executeStockMonthly(StockCalculationProcess stockCalculation,LoggerUser loggerUser) throws ServiceException{
    	Long stockProcessId = null;
    	stockCalculationService.cleanFilterStock(stockCalculation);
    	List<Integer> lstStockState = new ArrayList<Integer>();
    	lstStockState.add(StockStateType.FINISHED.getCode());
    	StockCalculationProcess stockOld = stockCalculationService.validateExistStockCalculation(stockCalculation, lstStockState);
    	if(stockOld == null){
//    		throw new ServiceException(ErrorServiceType.STOCK_CALCULATION_PROCESS_NOT_EXIST);
    		//register new stock
    		stockCalculation.setStockState(StockStateType.REGISTERED.getCode());
    		//commit new register
    		stockCalculationService.create(stockCalculation, loggerUser);
    		stockProcessId = stockCalculation.getIdStockCalculationPk();
    		//now executeStock
    		stockCalculationService.executeBatchStockCalculation(stockProcessId);
    	} else {
    		//return existing stock
    		stockProcessId= stockOld.getIdStockCalculationPk();
    	}
    	return stockProcessId;
    }
    
    /**
     * Execute stock market fact.
     *
     * @param stockCalculationId the stock calculation id
     * @param loggerUser the logger user
     * @throws ServiceException the service exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void executeStockMarketFact(Long stockCalculationId, LoggerUser loggerUser) throws ServiceException {
    	StockCalculationProcess stockCalculation = stockCalculationService.find(StockCalculationProcess.class, stockCalculationId);
    	if(stockCalculation==null || stockCalculation.getIdStockCalculationPk()==null){
    		throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
    	}
    	if(stockCalculation.getStockType().equals(StockType.MARKET_FACT.getCode())){
    		throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
    	}
    	stockCalculation.setStockState(StockStateType.REGISTERED.getCode());
    	stockCalculationService.executeBatchStockCalculation(stockCalculationId);
    }

}
