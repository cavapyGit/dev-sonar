//package com.pradera.accounting.common;
//
//import java.io.File;
//
//import org.apache.log4j.Logger;
//import org.jboss.arquillian.container.test.api.Deployment;
//import org.jboss.arquillian.junit.Arquillian;
//import org.jboss.shrinkwrap.api.ShrinkWrap;
//import org.jboss.shrinkwrap.api.asset.StringAsset;
//import org.jboss.shrinkwrap.api.formatter.Formatters;
//import org.jboss.shrinkwrap.api.spec.WebArchive;
//import org.jboss.shrinkwrap.descriptor.api.Descriptors;
//import org.jboss.shrinkwrap.descriptor.api.beans10.BeansDescriptor;
//import org.jboss.shrinkwrap.resolver.api.maven.Maven;
//import org.jboss.shrinkwrap.resolver.api.maven.ScopeType;
//import org.junit.runner.RunWith;
//
//import com.pradera.integration.contextholder.LoggerUser;
//
//
//
//
//
//
//@RunWith(Arquillian.class)
//public abstract  class AccountingServiceBeanTest {
//
//	public static Logger logger = Logger.getLogger(AccountingServiceBeanTest.class);
//	public LoggerUser loggerUser;
//	
//	
//	@Deployment
//    public static 	WebArchive createDeployment() {						
//		//PropertyConfigurator.configure("properties/log.properties");	
//				
//		File[] libs = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies()
//					  .importDependencies(ScopeType.TEST, ScopeType.PROVIDED,ScopeType.IMPORT,ScopeType.RUNTIME)
//					  
//					  .resolve()					
//					  .withTransitivity()					  
//					  .asFile();
//		File[] libsFinal = new File[libs.length-1];
//		
//		int sizeOrigin=0;
//		int sizeFinal=0;		
//		for (File file : libs) {
//			//System.out.println(file.getName());
//			if (!file.getName().contains("antlr") /*&& !file.getName().contains("iModel")*/ ){
//				libsFinal[sizeFinal]=file;
//				sizeFinal++;
//			}
//			sizeOrigin++;
//		}
//		
// 		BeansDescriptor beans = Descriptors.create(BeansDescriptor.class)
// 				 .getOrCreateInterceptors().clazz("com.pradera.commons.interceptores.PerformanceInterceptor").up()
// 				 .getOrCreateInterceptors().clazz("com.pradera.commons.contextholder.interceptor.RegisterThreadLocalInterceptor").up()
// 				 .getOrCreateInterceptors().clazz("com.pradera.core.framework.extension.transaction.jta.TransactionalInterceptor").up()
// 				 .getOrCreateInterceptors().clazz("com.pradera.core.framework.audit.AuditProcessLoggerInterceptor").up()
// 				 .getOrCreateInterceptors().clazz("com.pradera.commons.contextholder.interceptor.RegisterThreadConstructorInterceptor").up()
// 				 .getOrCreateInterceptors().clazz("com.pradera.core.framework.audit.AuditProcessLoggerInterceptor").up()
// 				 .getOrCreateAlternatives().clazz("org.apache.myfaces.extensions.cdi.jsf.impl.scope.conversation.ClientSideWindowHandler").up();
//		
//		
// 		WebArchive war = ShrinkWrap.create(WebArchive.class, "PraderaAccounting.war")
//			.addAsLibraries(libsFinal)
//			.addPackages(true, "com/pradera/accounting")	
//			.addAsWebInfResource(new StringAsset(beans.exportAsString()), "src/main/webapp/WEB-INF/beans.xml");
// 		
// 		logger.info("Objeto de prueba inicializado");
//		System.out.println(war.toString((Formatters.VERBOSE)));		
//		return war;
//    }
//	
//	public abstract void loadUserInfo();
//	
//	
//}
