var valueDefault;
var valueReference;								
var positionField;
var indexTarget;
var codeFormulate;
var indexFormulate;	

const SETTING_VALUE = '1'; 
const REMOVE_VALUE = '2';
const SETTING_REFERENCE = '3';


$(document).ready(function(){

	initDND();
});
 

function initDND() {

	  $( ".formulateClass .ui-orderlist-controls .ui-orderlist-button-move-up span" ).removeClass( "ui-icon-arrow-1-n" ).addClass( "ui-icon-arrow-1-w" );
	  $( ".formulateClass .ui-orderlist-controls .ui-orderlist-button-move-top span" ).removeClass( "ui-icon-arrowstop-1-n" ).addClass( "ui-icon-arrowstop-1-w" );
	  $( ".formulateClass .ui-orderlist-controls .ui-orderlist-button-move-down span" ).removeClass( "ui-icon-arrow-1-s" ).addClass( "ui-icon-arrow-1-e" );
	  $( ".formulateClass .ui-orderlist-controls .ui-orderlist-button-move-bottom span" ).removeClass( "ui-icon-arrowstop-1-s" ).addClass( "ui-icon-arrowstop-1-e" );

	  
	  /**List to List**/
	  $( ".formulateClass .ui-orderlist-controls" ).click(function() {
		  reOrder();
		});
		
		$('.ui-orderlist-item.ui-corner-all').draggable({
		
		   helper: 'clone',
		   scope: 'varToFormulate',  
		   revert: 'invalid',
		           zIndex: ++PrimeFaces.zindex  
				   
		});
  
        $('.formulateClass ul.ui-widget-content').droppable({
	       activeClass: 'ui-orderlist-list',  
	       hoverClass: 'ui-state-highlight',  
	       tolerance: 'pointer',  
	       scope: 'varToFormulate',  
	       drop: function(event, ui)   { 
		   var nameSource = ui.draggable.find("span[id*='nameSource']").text(),
		    codeSource= ui.draggable.find("span[id*='idHidden']").text(),
            droppedColumnId = $(this).parents('.formulateClass').attr('id');  
	                    
	                   addToList(nameSource,codeSource);
	               }
            });  
  
            $('.formulateClass ul.ui-widget-content li').draggable({  
		       scope: 'formulateRemove',  
		       zIndex: ++PrimeFaces.zindex  
		    });  

	
    $('.removeFormulateClass').droppable({  
       scope: 'formulateRemove',  
       activeClass: 'ui-state-active',  
       hoverClass: 'ui-state-highlight',  
       
       drop: function(event, ui) {
		   var codeFormulate = ui.draggable.find("span[id*='codeFormulate']").text(),
		   indexFormulate= ui.draggable.find("span[id*='indexFormulate']").text();
			removeToList(codeFormulate,indexFormulate);
       }
    });
    
    
    
    
	/**AccountingSource: DataTable to DataTable**/
    $('.fieldSourceClass tr td.codeClass').draggable({
	
       helper: 'clone',  
       scope: 'sourcetoformula',  
	   revert: 'invalid',
       zIndex: ++PrimeFaces.zindex  
	   
    });

    $('.SourceClass tr td.classReference').droppable({  
       activeClass: 'ui-orderlist-list',  
       hoverClass: 'ui-state-highlight',  
       tolerance: 'pointer',  
       scope: 'sourcetoformula',  
       drop: function(event, ui)   { 																															
		 
		 valueDefault = $(this).parent('tr').children().find('.valueDefaultClass').val();		
		 valueReference=$( this ).find('.variableClassReference').text();
	 																						
		
		 positionField= ui.draggable.find("span[id*='idPositionField']").text();
		 indexTarget= $( this ).find("span[id*='indexTarget']").text();  										   
		 addToListTable(positionField,indexTarget);																					
		 var iNum = parseInt(valueDefault); 
			
		 if (isNaN(iNum)==false &&  iNum!='0' ) {											
				
				document.getElementById('frmSearchBookAccounts:identifyValidationId').value  =SETTING_REFERENCE;
				validateSettingValues();											
		  }else{											  
			addToFormulate();
		  }
																								   
       }
    });

    /**Drag to Reference to Delete***/
    $('.SourceClass tr td.classReference').draggable({
        helper: 'clone',  
       scope: 'sourceRemove',  
	   revert: 'invalid',
        zIndex: ++PrimeFaces.zindex  
     });  

    /**Drop to Reference to Delete***/
     $('.removeFormulateSourceClass').droppable({ 
       scope: 'sourceRemove',   
        tolerance: 'pointer',  
        activeClass: 'ui-state-active',  
        hoverClass: 'ui-state-highlight',  
        
        drop: function(event, ui) {
			    codeFormulate = ui.draggable.find("span[id*='codeFormulate']").text(),
			    indexFormulate= ui.draggable.find("span[id*='indexTarget']").text();
				removeToTable(codeFormulate,indexFormulate);
        }
     }); 
	
	
	
	$('.valueDefaultClass').bind( "blur", function() {
	 
	 valueDefault = $(this).val();
	 valueReference = $(this).parent().parent().children().find('.variableClassReference').text();		
	 indexTarget= $(this).parent().parent().children().find("span[id*='indexTarget']").text(); 
	 var iNum = parseInt(valueDefault);
	 if( isNaN(iNum)==false &&  iNum!='0' )  {
		if (valueReference!='')  {
			
			document.getElementById('frmSearchBookAccounts:identifyValidationId').value  =SETTING_VALUE;
			document.getElementById('frmSearchBookAccounts:variableHiddenIndexId').value = indexTarget;
			validateSettingValues();
	  }
	}
	 });	
    
    
    
    
    
    
}


function addToList(nameSource,codeSource) {
	document.getElementById('formMemory:variableHiddenName').value = nameSource;
	document.getElementById('formMemory:variableHiddenCode').value = codeSource;	
	addFormulate();
}

function removeToList(codeFormulate,indexFormulate){
	document.getElementById('formMemory:variableHiddenCode').value = codeFormulate;
	document.getElementById('formMemory:variableHiddenIndex').value = indexFormulate;
	callRemove();
}







																

function addToListTable(positionField,indexTarget) {
	
	document.getElementById('frmSearchBookAccounts:variableHiddenCodeId').value = positionField;	
	document.getElementById('frmSearchBookAccounts:variableHiddenIndexId').value = indexTarget;																		        
}

function removeToTable(codeFormulate,indexFormulate){
	document.getElementById('frmSearchBookAccounts:variableHiddenCodeId').value = codeFormulate;
	document.getElementById('frmSearchBookAccounts:variableHiddenIndexId').value = indexFormulate;
	
	callRemove();
}

	 
	
// clean variables	 
function cleanVariables() {

	document.getElementById('frmSearchBookAccounts:variableHiddenNameId').value = '';
	document.getElementById('frmSearchBookAccounts:variableHiddenCodeId').value = '';
	document.getElementById('frmSearchBookAccounts:variableHiddenIndexId').value = '';
	document.getElementById('frmSearchBookAccounts:identifyValidationId').value = '';
}

function afterValidateSettingValuesOptNo() {	
	
	var op = document.getElementById('frmSearchBookAccounts:identifyValidationId').value;
	
	if (op==SETTING_VALUE){											
		document.getElementById('frmSearchBookAccounts:identifyValidationId').value = REMOVE_VALUE;
	    addToFormulate();
	}else{
		cleanVariables();
	}

}