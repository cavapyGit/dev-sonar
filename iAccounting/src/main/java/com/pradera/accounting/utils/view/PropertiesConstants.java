package com.pradera.accounting.utils.view;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class PropertiesConstants {
	
	
	
	/**
	 * Instantiates a new properties constants.
	 */
	public PropertiesConstants() {
		super();
	}
	
	/** The Constant WARNING_COMPLETE_DATA_EXCEPTION. */
	public static final String  WARNING_COMPLETE_DATA_EXCEPTION="warning.complete.data.exception";
	
	/** The Constant WARNING_CLEAN_DATA_EXCEPTION. */
	public static final String  WARNING_CLEAN_DATA_EXCEPTION="warning.clean.data.exception";
	
	
	/**  Begin the constant generic. */
	public static final Integer CURRENCY_TYPE_PK = Integer.valueOf(53);
	
	/** The Constant PROPERTYFILE. */
	public static final String PROPERTYFILE = "Messages";
	
	/** The Constant CONSTANTS. */
	public static final String CONSTANTS = "Constants";
	
	/**  End the constant generic. */
	
	public static final String VERTICAL="|";
	
	
	/** The Constant YES. */
	public static final Integer YES=Integer.valueOf(1);
	
	/** The Constant NOT. */
	public static final Integer NOT=Integer.valueOf(0);
	
	/** The Constant RDB_ONE. */
	public static final Integer RDB_ONE=Integer.valueOf(1);
	
	/** The Constant RDB_ALL. */
	public static final Integer RDB_ALL=Integer.valueOf(0);
	
	/** The Constant SETTING_VALUE. */
	public static final String  SETTING_VALUE="1";
	
	/** The Constant REMOVE_VALUE. */
	public static final String  REMOVE_VALUE="2";
	
	/** The Constant SETTING_REFERENCE. */
	public static final String  SETTING_REFERENCE="3";
	
	
	/** Messages*. */
	public static final String HEADER_MESSAGE_ERROR="header.message.error";
	
	/** The Constant BODY_MESSAGE_WARNING_DOCUMENT_ERROR. */
	public static final String BODY_MESSAGE_WARNING_DOCUMENT_ERROR="body.message.warning.document";
	
	/** *. */
	public static final String CONFIRM_BOOK_ACCOUNT_SAVE="confirm.book.account.save_confirm";
	
	/** The Constant CONFIRM_BOOK_ACCOUNT_UPDATE. */
	public static final String CONFIRM_BOOK_ACCOUNT_UPDATE="confirm.book.account.update_confirm";
	
	/** The Constant WARNING_BOOK_ACCOUNT_. */
	public static final String WARNING_BOOK_ACCOUNT_="warning.billing.service.cancel";
	
	/** The Constant MODIFY_BOOK_ACCOUNT_NO_SELECTED. */
	public static final String MODIFY_BOOK_ACCOUNT_NO_SELECTED="confirm.type.service.book.account.no.selected";
	
	/** The Constant WARNING_BOOK_ACCOUNT_CANCELLED. */
	public static final String WARNING_BOOK_ACCOUNT_CANCELLED="warning.book.account.cancel";
	
	/** The Constant PARAMETER_TYPE. */
	public static final Integer PARAMETER_TYPE = Integer.valueOf(543);//
	
	/** The Constant ACCOUNTING_ACCOUNT_STATE. */
	public static final Integer ACCOUNTING_ACCOUNT_STATE = Integer.valueOf(544);//
 
	/** The Constant WARNING_DATA_SAVE_PARAMETER. */
	public static final String WARNING_DATA_SAVE_PARAMETER ="warning.data.save.parameter";
	
	/** The Constant CONFIRM_SAVE_PARAMETER. */
	public static final String CONFIRM_SAVE_PARAMETER ="confirm.save.parameter";
	
	/** The Constant SUCCESS_SAVE_PARAMETER. */
	public static final String SUCCESS_SAVE_PARAMETER ="success.save.parameter";

	/** The Constant WARNING_PARAMETER_DELETED. */
	public static final String WARNING_PARAMETER_DELETED ="warning.parameter.deleted";
	
	/** The Constant WARNING_PARAMETER_LOCK. */
	public static final String WARNING_PARAMETER_LOCK ="warning.parameter.lock";
	
	/** The Constant WARNING_PARAMETER_ACTIVE. */
	public static final String WARNING_PARAMETER_ACTIVE ="warning.parameter.actived";
	
	/** The Constant WARNING_PARAMETER_REGISTERED. */
	public static final String WARNING_PARAMETER_REGISTERED ="warning.parameter.registered";
	
	/** The Constant WARNING_PARAMETER_NO_SELECTED. */
	public static final String WARNING_PARAMETER_NO_SELECTED ="warning.modify.not.select";
	
	
	

	/** The Constant CONFIRM_PARAMETER_ACTIVED. */
	public static final String CONFIRM_PARAMETER_ACTIVED ="confirm.parameter.actived";
	
	/** The Constant CONFIRM_PARAMETER_LOCKED. */
	public static final String CONFIRM_PARAMETER_LOCKED ="confirm.parameter.locked"; 
	
	/** The Constant CONFIRM_PARAMETER_UNLOCKED. */
	public static final String CONFIRM_PARAMETER_UNLOCKED ="confirm.parameter.unlocked";
	
	/** The Constant CONFIRM_PARAMETER_DELETED. */
	public static final String CONFIRM_PARAMETER_DELETED ="confirm.parameter.deleted";
	
	
	/** The Constant SUCCESS_ACCOUNTING_ACCOUNT_SAVE. */
	public static final String SUCCESS_ACCOUNTING_ACCOUNT_SAVE="success.accounting.account.save";
	
	/** The Constant EXIST_ACCOUNTING_ACCOUNT_SAVED. */
	public static final String EXIST_ACCOUNTING_ACCOUNT_SAVED="exist.accounting.account.saved";

	/** The Constant SUCCESS_ACCOUNTING_ACCOUNT_UPDATE. */
	public static final String SUCCESS_ACCOUNTING_ACCOUNT_UPDATE="success.accounting.account.update";
	
	
	/** The Constant LONG_SEQUENCE. */
	public static final Integer LONG_SEQUENCE	= Integer.valueOf(8);
	
	/** The Constant LONG_ACCOUNTING_SEQUENCE. */
	public static final Integer LONG_ACCOUNTING_SEQUENCE	= Integer.valueOf(7);
		
	/** The Constant WARNING_BACK_EXCEPTION_SEARCH. */
	public static final String  WARNING_BACK_EXCEPTION_SEARCH="warning.back.search.excepction";


 
	/**  End the constant generic. */
	
	
	/** Begin Parameters **/
	
	public static final Integer PARAMETER_ACCOUNTING_STATUS = Integer.valueOf(548);//
	
	/** The Constant CONFIRM_MODIFY_PARAMETER. */
	public static final String CONFIRM_MODIFY_PARAMETER ="confirm.modify.parameter";
	
	/** The Constant SUCCESS_MODIFY_PARAMETER. */
	public static final String SUCCESS_MODIFY_PARAMETER ="success.modify.parameter";
	
	
	/** The Constant ALERT_MODIFY_PARAMETER. */
	public static final String ALERT_MODIFY_PARAMETER ="alert.modify.parameter";


	/**  Begin Schema *. */
	public static final Integer PARAMETER_SCHEMA_STATUS = Integer.valueOf(551);//
	
	/** The Constant PARAMETER_SOURCE_STATUS. */
	public static final Integer PARAMETER_SOURCE_STATUS = Integer.valueOf(552);//
	
	/** The Constant PARAMETER_ACCOUNTING_BRANCH. */
	public static final Integer PARAMETER_ACCOUNTING_BRANCH = Integer.valueOf(2092);///
	
	/** The Constant PARAMETER_ACCOUNTING_OPERATOS. */
	public static final Integer PARAMETER_ACCOUNTING_OPERATOS = Integer.valueOf(2091);//
	
	/** The Constant DAILY_AUXILIARY. */
	public static final Integer DAILY_AUXILIARY= Integer.valueOf(2097);//
	
	/** The Constant ASSOCIATE_CODE. */
	public static final Integer ASSOCIATE_CODE= Integer.valueOf(2100);///
	
	/** The Constant ASSOCIATE_CODE. */
	public static final Integer RESTART_TYPE= Integer.valueOf(2513);///
	
	/** The Constant CONFIRM_SAVE_SCHEMA. */
	public static final String CONFIRM_SAVE_SCHEMA ="confirm.save.schema";
	
	/** The Constant SUCCESS_SAVE_SCHEMA. */
	public static final String SUCCESS_SAVE_SCHEMA ="success.save.schema";
	
	/** The Constant SUCCESS_SAVE_SCHEMA_AND_MATRIX. */
	public static final String SUCCESS_SAVE_SCHEMA_AND_MATRIX ="success.save.schema.and.matrix";
	
	/** The Constant WARNING_SCHEMA_CANCELED. */
	public static final String WARNING_SCHEMA_CANCELED ="warning.schema.canceled";
	
	/** The Constant WARNING_SCHEMA_REGISTERED. */
	public static final String WARNING_SCHEMA_REGISTERED ="warning.schema.registered";
	
	/** The Constant WARNING_SCHEMA_LOCK. */
	public static final String WARNING_SCHEMA_LOCK ="warning.schema.lock";
	
	/** The Constant WARNING_SCHEMA_DONT_HAVE_MATRIX. */
	public static final String WARNING_SCHEMA_DONT_HAVE_MATRIX ="warning.schema.dont.have.matrix";
	
	/** The Constant CONFIRM_SCHEMA_CANCELED. */
	public static final String CONFIRM_SCHEMA_CANCELED ="confirm.schema.canceled"; 
	
	/** The Constant CONFIRM_SCHEMA_LOCKED. */
	public static final String CONFIRM_SCHEMA_LOCKED ="confirm.schema.lock";
	
	/** The Constant CONFIRM_SCHEMA_UNLOCKED. */
	public static final String CONFIRM_SCHEMA_UNLOCKED ="confirm.schema.unlocked";
	
	/** The Constant SUCCESS_SCHEMA_MODIFY. */
	public static final String SUCCESS_SCHEMA_MODIFY="success.schema.modify";
	
	/** The Constant ALERT_MODIFY_SCHEMA. */
	public static final String ALERT_MODIFY_SCHEMA="alert.modify.schema";
	
	/** The Constant CONFIRM_MODIFY_SCHEMA. */
	public static final String CONFIRM_MODIFY_SCHEMA ="confirm.modify.schema";
	
	/** The Constant SUCCESS_MODIFY_SCHEMA. */
	public static final String SUCCESS_MODIFY_SCHEMA ="success.modify.schema";
	
	/** The Constant SUCCESS_ASSOCIATED_ACCOUNT_SCHEMA. */
	public static final String SUCCESS_ASSOCIATED_ACCOUNT_SCHEMA ="success.associated.account.schema";
	
	/** The Constant ALERT_WARNING_MODIFY_SCHEMA. */
	public static final String ALERT_WARNING_MODIFY_SCHEMA ="alert.warning.modify.schema";
	
	/** The Constant ALERT_WARNING_ADD_ACCOUNT_BACK. */
	public static final String ALERT_WARNING_ADD_ACCOUNT_BACK ="alert.warning.add.accout.back";
	
	/** The Constant CONFIRM_ADD_ACCOUNTING_ACCOUNT. */
	public static final String CONFIRM_ADD_ACCOUNTING_ACCOUNT ="confirm.add.accounting.account";
	
	/** The Constant WARNING_NOT_ACCOUNT_TO_ADD. */
	public static final String WARNING_NOT_ACCOUNT_TO_ADD ="warning.not.account.to.add";
	
	/** The Constant WARNING_NOT_ACCOUNT_TO_MODIFY. */
	public static final String WARNING_NOT_ACCOUNT_TO_MODIFY ="warning.not.account.to.modify";
	
	/** The Constant WARNING_NOT_FORMULA_VALIDATE. */
	public static final String WARNING_NOT_FORMULA_VALIDATE ="warning.not.formula.validate";
	
	/** The Constant WARNING_MISSING_FORMULA_OR_SOURCE. */
	public static final String WARNING_MISSING_FORMULA_OR_SOURCE ="warning.missing.formula.or.source";
	
	
	
	/** The Constant CONFIRM_VALIDATE_FORMULATE. */
	public static final String CONFIRM_VALIDATE_FORMULATE ="confirm.validate.formulate";
	
	/** The Constant WARNING_MISSING_FORMULATE. */
	public static final String WARNING_MISSING_FORMULATE ="warning.missing.formulate";
	
	/** The Constant ALERT_WARNING_NOT_FORMULATE. */
	public static final String ALERT_WARNING_NOT_FORMULATE ="alert.warning.not.formulate";
	
	/** The Constant WARNING_VALIDATE_FORMULATE. */
	public static final String WARNING_VALIDATE_FORMULATE ="warning.validate.formulate";
	
	/** The Constant ALERT_WARNING_BACK_FORMULATE. */
	public static final String ALERT_WARNING_BACK_FORMULATE ="alert.warning.back.formulate";
	
	/** The Constant WARNING_ERROR_REPEAT. */
	public static final String WARNING_ERROR_REPEAT ="warning.error.repeat";
	
	/** The Constant WARNING_ERROR_NATURE. */
	public static final String WARNING_ERROR_NATURE ="warning.error.nature";
	
	/** The Constant WARNING_ERROR_NUMBER_ACCOUNT. */
	public static final String WARNING_ERROR_NUMBER_ACCOUNT ="warning.error.number.account";
	
	/** The Constant WARNING_ERROR_OTHER. */
	public static final String WARNING_ERROR_OTHER ="warning.error.other";
	
	/** The Constant WARNING_NO_FORMULATE. */
	public static final String WARNING_NO_FORMULATE ="warning.no.formulate";
	
	/** The Constant WARNING_NO_FORMULATE_NOR_SOURCE. */
	public static final String WARNING_NO_FORMULATE_NOR_SOURCE ="warning.no.source.nor.formulate";
	
	/** The Constant CONFIRM_PARAMETERIZE_TEMPORAL. */
	public static final String CONFIRM_PARAMETERIZE_TEMPORAL ="confirm.schema.parameterize.temporal";
	
	/** The Constant ALERT_WARNING_BACK_SOURCE. */
	public static final String ALERT_WARNING_BACK_SOURCE ="alert.warning.back.source";
			
	
	
	
	

	/** * Branch. */
	public static final Long CONTABILIDAD_DE_VALORIZADO 	= Long.valueOf(1);
	
	/** The Constant CONTABILIDAD_DE_SERVICIOS. */
	public static final Long CONTABILIDAD_DE_SERVICIOS	= Long.valueOf(2);
	
	/** The Constant CONTABILIDAD_DE_FONDOS_MARGEN. */
	public static final Long CONTABILIDAD_DE_FONDOS_MARGEN	= Long.valueOf(3);
	
	/** The Constant CONTABILIDAD_EN_CANTIDADES_DE_VALORES. */
	public static final Long CONTABILIDAD_EN_CANTIDADES_DE_VALORES	= Long.valueOf(4);
	
	/** The Constant CONTABILIDAD_DE_FONDOS_COMPENSACION. */
	public static final Long CONTABILIDAD_DE_FONDOS_COMPENSACION	= Long.valueOf(5);
	
	/** The Constant CONTABILIDAD_DE_FONDOS_COORPERATIVO. */
	public static final Long CONTABILIDAD_DE_FONDOS_COORPERATIVO	= Long.valueOf(6);
	

	/** The Constant FONDO. */
	public static final Integer FONDO	= Integer.valueOf(11);
	
	/** The Constant VALOR. */
	public static final Integer VALOR 	= Integer.valueOf(12);
	
	/** The Constant ADM. */
	public static final Integer ADM		= Integer.valueOf(13);
	
	
	/** The Constant CONFIRM_SAVE_SOURCE. */
	public static final String CONFIRM_SAVE_SOURCE ="confirm.save.source";
	
	/** The Constant CONFIRM_MODIFY_SOURCE. */
	public static final String CONFIRM_MODIFY_SOURCE ="confirm.modify.source";
	
	/** The Constant CONFIRM_CLEAN_SOURCE. */
	public static final String CONFIRM_CLEAN_SOURCE ="confirm.clean.source";
	
	/** The Constant CONFIRM_BACK_SOURCE. */
	public static final String CONFIRM_BACK_SOURCE ="confirm.back.source";
	
	/** The Constant SUCCESS_SAVE_SOURCE. */
	public static final String SUCCESS_SAVE_SOURCE ="success.save.source";

	/** The Constant CONFIRM_PROCESS_ACCOUNTING_SEND_GENERATION. */
	public static final String CONFIRM_PROCESS_ACCOUNTING_SEND_GENERATION ="confirm.process.accounting.send.generation";
	
	/** The Constant SUCCESS_PROCESS_ACCOUNTING_SEND_GENERATION. */
	public static final String SUCCESS_PROCESS_ACCOUNTING_SEND_GENERATION ="success.process.accounting.send.generation";
	
	public static final String REGISTER_MODIFY_DATA_ACCOUNTNG ="register.modify.data.accounting";
	
	public static final String SUCCESS_MODIFY_DATA_ACCOUNTNG ="sucess.modify.data.accounting";
	
	/** The Constant WARNING_BACK_ADD_ACCOUNT. */
	public static final String  WARNING_BACK_ADD_ACCOUNT="warning.back.add.account";
	
	
	
	 
	/**  Begin Accounting Parameters *. */
	
	public static final Integer VARIABLE_FORMULA	= Integer.valueOf(2087);
	
	/** The Constant OPERADOR_FORMULA. */
	public static final Integer OPERADOR_FORMULA = Integer.valueOf(2091);
	
	/** The Constant SUCURSALES. */
	public static final Integer SUCURSALES			= Integer.valueOf(2092);
	
	/** The Constant FUENTE_FONDOS. */
	public static final Integer FUENTE_FONDOS 	= Integer.valueOf(2093);
	
	/** The Constant FUENTE_DE_VALORES. */
	public static final Integer FUENTE_DE_VALORES	= Integer.valueOf(2094);
	
	/** The Constant TIPO_CUENTA. */
	public static final Integer TIPO_CUENTA 		= Integer.valueOf(2095);
	
	/** The Constant NATURALEZA_DE_LA_CUENTA. */
	public static final Integer NATURALEZA_DE_LA_CUENTA	= Integer.valueOf(2096);
	
	/** The Constant TIPO_DIARIO_AUXILIAR. */
	public static final Integer TIPO_DIARIO_AUXILIAR 	= Integer.valueOf(2097);
	
	/** The Constant TIPO_AJUSTE. */
	public static final Integer TIPO_AJUSTE	= Integer.valueOf(2098);
	
	/** The Constant CENTRO_COSTOS. */
	public static final Integer CENTRO_COSTOS	= Integer.valueOf(2099);
	
	/** The Constant NUMERO_HILOS. */
	public static final Integer NUMERO_HILOS	= Integer.valueOf(2101);
	
	
	
	/**  End Accounting Parameters *. */
	 
	
	
	public static final String WARNING_ACCOUNTING_ACCOUNT_DELETED ="warning.accounting.account.deleted";
	
	/** The Constant CONFIRM_ACCOUNTING_ACCOUNT_DELETED. */
	public static final String CONFIRM_ACCOUNTING_ACCOUNT_DELETED ="confirm.accounting.account.deleted"; 
	
	/** The Constant WARNING_ACCOUNTING_ACCOUNT_IS_USED. */
	public static final String WARNING_ACCOUNTING_ACCOUNT_IS_USED ="warning.accounting.account.is.used";
	
	/** The Constant WARNING_SOURCE_DELETED. */
	public static final String WARNING_SOURCE_DELETED ="warning.source.deleted";
	
	/** The Constant WARNING_SOURCE_NO_SELECTED. */
	public static final String WARNING_SOURCE_NO_SELECTED ="warning.source.not.select";
	
	/** The Constant WARNING_BACK_SOURCE. */
	public static final String WARNING_BACK_SOURCE="warning.back.source"; 
	
	/** The Constant SUCCESS_DELETED_SOURCE. */
	public static final String SUCCESS_DELETED_SOURCE ="success.deleted.source";
	
	/** The Constant CONFIRM_DELETED_SOURCE. */
	public static final String CONFIRM_DELETED_SOURCE ="confirm.deleted.source";  

	/** The Constant WARNING_SAVE_SOURCE. */
	public static final String WARNING_SAVE_SOURCE ="warning.save.source";
	
	
	/**  Use on register Accounting Account *. */			/** Accounting Parameter Pk's**/
	public static final Long CUENTAS_ORDEN			= Long.valueOf(7);
	
	/** The Constant CUENTAS_FUERA_ORDEN. */
	public static final Long CUENTAS_FUERA_ORDEN	= Long.valueOf(8);

	/** The Constant DEBIT. */
	public static final String DEBIT = "DEBITO";
	
	/** The Constant CREDIT. */
	public static final String CREDIT = "CREDITO"; 
	
	/** 	Use con Matrix	*. */
	public static final String ALERT_DELETE_ACCOUNT_ON_MATRIX ="alert.delete.account.on.matrix";
	
	/** The Constant SUCCESS_MODIFY_MATRIX_DETAIL. */
	public static final String SUCCESS_MODIFY_MATRIX_DETAIL ="success.modify.matrix.detail";
	
	/** The Constant CONFIRM_MODIFY_MATRIX_DETAIL. */
	public static final String CONFIRM_MODIFY_MATRIX_DETAIL ="confirm.modify.matrix.detailt";
	
	/** The Constant ACCOUNTING_SOURCE_WRONG_STRUCTURED. */
	public static final String ACCOUNTING_SOURCE_WRONG_STRUCTURED ="accounting.source.wrong.structured";
	
	/** The Constant ACCOUNTING_SOURCE_VALUE_FORMAT. */
	public static final String ACCOUNTING_SOURCE_VALUE_FORMAT ="accounting.source.value.format";
	
	/** The Constant ACCOUNTING_SOURCE_VISIBILITY_FORMAT. */
	public static final String ACCOUNTING_SOURCE_VISIBILITY_FORMAT ="accounting.source.visibility.format";
	
	/** The Constant ACCOUNTING_SOURCE_POSITION_FORMAT. */
	public static final String ACCOUNTING_SOURCE_POSITION_FORMAT ="accounting.source.position.format";
	
	/** The Constant ACCOUNTING_SOURCE_TYPE_FORMAT. */
	public static final String ACCOUNTING_SOURCE_TYPE_FORMAT ="accounting.source.type.format";
	
	/** The Constant ACCOUNTING_SOURCE_TYPE_ASSOCIATE. */
	public static final String ACCOUNTING_SOURCE_TYPE_ASSOCIATE="accounting.source.type.associate";
	
	/** The Constant CONFIRM_DELETE_SOURCE_ON_MATRIX. */
	public static final String CONFIRM_DELETE_SOURCE_ON_MATRIX ="confirm.delete.source.on.matrix";
	
	/** The Constant ALERT_WARNING_MODIFY_ACCOUNT_BACK. */
	public static final String ALERT_WARNING_MODIFY_ACCOUNT_BACK ="alert.warning.modify.accout.back";
	
	
	/** The Constant ACCOUNTING_SOURCE_INFO. */
	public static final String ACCOUNTING_SOURCE_INFO="accountingSourceInfo";
	
	/** The Constant ACCOUNTING_SOURCE_FIELD. */
	public static final String ACCOUNTING_SOURCE_FIELD="field";
	
	/** The Constant ACCOUNTING_SOURCE_VISIBILITY. */
	public static final String ACCOUNTING_SOURCE_VISIBILITY="visibility";
	
	/** The Constant ACCOUNTING_SOURCE_TYPE. */
	public static final String ACCOUNTING_SOURCE_TYPE="type";
	
	/** The Constant ACCOUNTING_SOURCE_POSICION. */
	public static final String ACCOUNTING_SOURCE_POSICION="posicion";
	
	/** The Constant ACCOUNTING_SOURCE_VALUE. */
	public static final String ACCOUNTING_SOURCE_VALUE="value";
	
	/** The Constant ACCOUNTING_SOURCE_QUERY. */
	public static final String ACCOUNTING_SOURCE_QUERY="query";
	
	/** The Constant ACCOUNTING_SOURCE_DATE. */
	public static final String ACCOUNTING_SOURCE_DATE="date";  
	
	/** The Constant ACCOUNTING_SOURCE_NAME. */
	public static final String ACCOUNTING_SOURCE_NAME="name";
	
	/** The Constant ACCOUNTING_SOURCE_DESCRIPTION. */
	public static final String ACCOUNTING_SOURCE_DESCRIPTION="description";
	
	/** The Constant ACCOUNTING_SOURCE_QUERY_ASSOCIATION. */
	public static final String ACCOUNTING_SOURCE_QUERY_ASSOCIATION="association";
	
	/** The Constant ACCOUNTING_SOURCE_TYPE_ASSOCIATION. */
	public static final String ACCOUNTING_SOURCE_TYPE_ASSOCIATION="typeAssociation";
	
	/** The Constant ACCOUNTING_SOURCE_CURRENCY. */
	public static final String ACCOUNTING_SOURCE_CURRENCY="isCurrency";
	
	/** The Constant ACCOUNTING_SOURCE_INSTRUMENT_TYPE. */
	public static final String ACCOUNTING_SOURCE_INSTRUMENT_TYPE="isInstrument";
	

	/** The Constant ACCOUNTING_FUTURE_FINISH. */
	public static final String ACCOUNTING_FUTURE_FINISH="accounting_future_finish";
	
	/** The Constant ACCOUNTING_THREAD. */
	public static final String ACCOUNTING_THREAD="accounting_thread";
	
	/** The Constant EXPIRATION_SECURITY_MESSAGE. */
	public static final String EXPIRATION_SECURITY_MESSAGE="tiene fecha de vencimiento";
	
	/** The Constant COUPON_INTERES_MESSAGE. */
	public static final String COUPON_INTERES_MESSAGE="desprendera el cupon N° ";
	
	/** The Constant COUPON_AMORTIZATION_MESSAGE. */
	public static final String COUPON_AMORTIZATION_MESSAGE="tendra una amortizacion del cupon N° ";
	
	/**Para el registro de cuentas de oreder de manera masiva*/
	public static final String RECORD_LABEL_FOR_ACCOUNTS="record.label.for.accounts";
	
	/**Alerta para la notificacion de que existe un participante*/
	public static final String LABEL_FOR_ACCOUNT_RECORDS_COUNT="label.for.account.records.count";
	
	/**Participante*/
	public static final String PARTICIPANT="lbl.participant.search";	
	/** ENVIO DE DOCUMENTOS CONTABLES */
	
	/** The Constant CONFIRM_PROCESS_ACCOUNTING_SEND_GENERATION. */
	public static final String CONFIRM_SENDING_ACCOUNTING ="confirm.sending.accounting";
	
	/** The Constant SUCCESS_PROCESS_ACCOUNTING_SEND_GENERATION. */
	public static final String SUCCESS_SENDING_ACCOUNTING ="success.sending.accounting";
	
}
