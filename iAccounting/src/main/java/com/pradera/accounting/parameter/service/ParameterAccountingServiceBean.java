package com.pradera.accounting.parameter.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingParameter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ParameterAccountingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ParameterAccountingServiceBean  extends CrudDaoServiceBean{
	
	
	 
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Save parameter accounting.
	 *
	 * @param parameterAccounting  Method for save one entity
	 */
	
	
	public void saveParameterAccounting(Object parameterAccounting){
		create(parameterAccounting);
	}
	
	
	/**
	 * Find nr correlative.
	 *
	 * @param parameterType the parameter type
	 * @return the integer
	 */
	public Integer findNrCorrelative(Integer parameterType){
		
		BigDecimal nrCorrelative =  null ;
		Integer	   ii	 		 =  null;
		Integer    iiShow		 =  null ;
		
		StringBuffer sbQuery=new StringBuffer();
		sbQuery.append(" SELECT COUNT(NR_CORRELATIVE) AS TOTAL_CORRELATIVE FROM ACCOUNTING_PARAMETER ");
		sbQuery.append(" Where 1 = 1 ");

		
		if(Validations.validateIsNotNullAndNotEmpty(parameterType)){
			sbQuery.append(" And PARAMETER_TYPE =  :parameterType  ");
		}
		
		Query query = em.createNativeQuery(sbQuery.toString());   
		
		if(Validations.validateIsNotNullAndNotEmpty(parameterType)){
			query.setParameter("parameterType", parameterType);			
		}
		 
		nrCorrelative =  (BigDecimal) query.getSingleResult();
		ii = nrCorrelative.intValue();
		iiShow = new Integer(ii) ;
			
		try{
			return  iiShow ;  
			
		}catch(NoResultException nrException){
			
			return iiShow ;
 		}	
		
	}
	
	
	/**
	 * Find list parameter accounting.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingParameter> findListParameterAccounting(AccountingParameterTO filter){
		 		
	 	
		StringBuilder stringBuilderSqlBase = new StringBuilder();		
		stringBuilderSqlBase.append(" SELECT  AC.ID_ACCOUNTING_PARAMETER_PK ");
		stringBuilderSqlBase.append(" ,( select pt.description from parameter_table pt where AC.PARAMETER_TYPE=pt.parameter_table_pk ) as description_parameter_type " );
		stringBuilderSqlBase.append(" ,AC.NR_CORRELATIVE  ");
		stringBuilderSqlBase.append(" ,AC.PARAMETER_NAME  ");
		stringBuilderSqlBase.append(" ,AC.DESCRIPTION     ");		 
		stringBuilderSqlBase.append(" ,( select pt.description from parameter_table pt where AC.STATUS=pt.parameter_table_pk ) as description_accounting_type " );
		stringBuilderSqlBase.append(" ,AC.REGISTER_DATE  ");
		
		stringBuilderSqlBase.append(" ,AC.LAST_MODIFY_USER  ");	
		stringBuilderSqlBase.append(" ,AC.LAST_MODIFY_DATE  ");	
		stringBuilderSqlBase.append(" ,AC.REGISTER_USER  ");	
		stringBuilderSqlBase.append(" ,AC.PARAMETER_TYPE  ");
		
		stringBuilderSqlBase.append(" FROM ACCOUNTING_PARAMETER  AC ");

		stringBuilderSqlBase.append(" WHERE 1=1 ");   
		
		if ( Validations.validateIsNotNullAndPositive(filter.getParameterType()) ){
			stringBuilderSqlBase.append(" AND AC.PARAMETER_TYPE = :parameterType    ");
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getParameterName()) ){
			stringBuilderSqlBase.append(" AND AC.PARAMETER_NAME = :parameterName    ");
		}
		if ( Validations.validateIsNotNull(filter.getIdAccountingParameterPk()) ){
			stringBuilderSqlBase.append(" AND AC.ID_ACCOUNTING_PARAMETER_PK =:idAccountingParameterPk   ");
		}		
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			stringBuilderSqlBase.append(" AND AC.STATUS = :status    ");
		}
		 
		stringBuilderSqlBase.append(" ORDER BY AC.PARAMETER_TYPE , AC.NR_CORRELATIVE ");
		
		Query query = em.createNativeQuery(stringBuilderSqlBase.toString());	
		
		if ( Validations.validateIsNotNullAndPositive(filter.getParameterType()) ){
			query.setParameter("parameterType", filter.getParameterType() );
		}		
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getParameterName()) ){
			query.setParameter("parameterName", filter.getParameterName() );
		} 
		if ( Validations.validateIsNotNull(filter.getIdAccountingParameterPk()) ){
			query.setParameter("idAccountingParameterPk", filter.getIdAccountingParameterPk() );
		} 
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			query.setParameter("status", filter.getStatus() );
		} 
		
		
		
		List<Object[]>  objectList = query.getResultList();
		
		
		List<AccountingParameter> listAccountingParameters  = new ArrayList<AccountingParameter>();

		
		for(int i=0;i<objectList.size();++i){	
			Object[] sResults = (Object[])objectList.get(i);
			AccountingParameter accountingParameter = new AccountingParameter();
			
			accountingParameter.setIdAccountingParameterPk(Integer.parseInt(sResults[0]==null?"":sResults[0].toString())); 
			accountingParameter.setDescriptionParameterType(sResults[1]==null?"":sResults[1].toString()); 			
			accountingParameter.setNrCorrelative(sResults[2]==null?null:Integer.parseInt(sResults[2].toString()));
			accountingParameter.setParameterName(sResults[3]==null?"":sResults[3].toString());			
			accountingParameter.setDescription(sResults[4]==null?"":sResults[4].toString());
			accountingParameter.setDescriptionParameterStatus(sResults[5]==null?"":sResults[5].toString()); 
			accountingParameter.setRegisterDate((Date)sResults[6]);
			  
			accountingParameter.setLastModifyUser(sResults[7]==null?"":sResults[7].toString()); 
			accountingParameter.setLastModifyDate((Date)sResults[8]);
			accountingParameter.setRegisterUser(sResults[9]==null?"":sResults[9].toString());
			accountingParameter.setParameterType(sResults[10]==null?null:Integer.parseInt(sResults[10].toString()));
			accountingParameter.setNameWithDescription(sResults[3] + " - " +sResults[4].toString());		
			
			listAccountingParameters.add(accountingParameter);
			 
		}
		return listAccountingParameters;
	}
	
/**
 * Search parameter accounting to.
 *
 * @param filter the filter
 * @return the list
 */
public List<AccountingParameterTO> searchParameterAccountingTo(AccountingParameterTO filter){
		
 	
	StringBuilder stringBuilderSqlBase = new StringBuilder();		
	stringBuilderSqlBase.append(" SELECT  AC.ID_ACCOUNTING_PARAMETER_PK ");
	stringBuilderSqlBase.append(" ,( select pt.description from parameter_table pt where AC.PARAMETER_TYPE=pt.parameter_table_pk ) as description_parameter_type " );
	
	stringBuilderSqlBase.append(" ,AC.PARAMETER_NAME  ");
	stringBuilderSqlBase.append(" ,AC.DESCRIPTION     ");		 
	stringBuilderSqlBase.append(" ,( select pt.description from parameter_table pt where AC.STATUS=pt.parameter_table_pk ) as description_accounting_type " );

	stringBuilderSqlBase.append(" ,AC.PARAMETER_TYPE  ");
	
	stringBuilderSqlBase.append(" FROM ACCOUNTING_PARAMETER  AC ");

	stringBuilderSqlBase.append(" WHERE 1=1 ");   
	
	if ( Validations.validateIsNotNullAndPositive(filter.getParameterType()) ){
		stringBuilderSqlBase.append(" AND AC.PARAMETER_TYPE = :parameterType    ");
	}
	if ( Validations.validateIsNotNullAndNotEmpty(filter.getParameterName()) ){
		stringBuilderSqlBase.append(" AND AC.PARAMETER_NAME = :parameterName    ");
	}
	if ( Validations.validateIsNotNull(filter.getIdAccountingParameterPk()) ){
		stringBuilderSqlBase.append(" AND AC.ID_ACCOUNTING_PARAMETER_PK =:idAccountingParameterPk   ");
	}		
	if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
		stringBuilderSqlBase.append(" AND AC.STATUS = :status    ");
	}
	 	
	stringBuilderSqlBase.append(" ORDER BY AC.ID_ACCOUNTING_PARAMETER_PK  ");
	
	Query query = em.createNativeQuery(stringBuilderSqlBase.toString());	
	
	if ( Validations.validateIsNotNullAndPositive(filter.getParameterType()) ){
		query.setParameter("parameterType", filter.getParameterType() );
	}		
	if ( Validations.validateIsNotNullAndNotEmpty(filter.getParameterName()) ){
		query.setParameter("parameterName", filter.getParameterName() );
	} 
	if ( Validations.validateIsNotNull(filter.getIdAccountingParameterPk()) ){
		query.setParameter("idAccountingParameterPk", filter.getIdAccountingParameterPk() );
	} 
	if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
		query.setParameter("status", filter.getStatus() );
	} 
		
	List<Object[]>  objectList = query.getResultList();
	
	
	List<AccountingParameterTO> listAccountingParameters  = new ArrayList<AccountingParameterTO>();

	
	for(int i=0;i<objectList.size();++i){	
		Object[] sResults = (Object[])objectList.get(i);
		AccountingParameterTO accountingParameter = new AccountingParameterTO();
		
		accountingParameter.setIdAccountingParameterPk(Integer.parseInt(sResults[0]==null?"":sResults[0].toString()));
		
		accountingParameter.setDescriptionParameterType(sResults[1]==null?"":sResults[1].toString()); 			
		accountingParameter.setParameterName(sResults[2]==null?"":sResults[2].toString());			
		accountingParameter.setDescription(sResults[3]==null?"":sResults[3].toString());
		accountingParameter.setDescriptionParameterStatus(sResults[4]==null?"":sResults[4].toString());
		accountingParameter.setParameterType(sResults[5]==null?null:Integer.parseInt(sResults[5].toString()));		
		
		listAccountingParameters.add(accountingParameter);
		 
	}
	return listAccountingParameters;
}


	/**
	 * Search parameter accounting by name.
	 *
	 * @param name the name
	 * @return the accounting parameter
	 */
	public AccountingParameter searchParameterAccountingByName(String name){
		
		StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select distinct ap From AccountingParameter ap  " );

        sbQuery.append(" where 1=1 " );
        
        if(Validations.validateIsNotNullAndNotEmpty(name)){
            sbQuery.append(" and  ap.parameterName = :name ");
        }
        
        Query query = em.createQuery(sbQuery.toString());
       
        if(Validations.validateIsNotNullAndNotEmpty(name)){
            query.setParameter("name", name);
        }
        
        AccountingParameter accountingParameter = null;
        accountingParameter = (AccountingParameter) query.getSingleResult();
		
		return accountingParameter;
	}
	 
 	/**
 	 * *
 	 * findAccountingParameterToByName.
 	 *
 	 * @param name the name
 	 * @return the accounting parameter to
 	 */
	public AccountingParameterTO findAccountingParameterToByName(String name){
		StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select ap.ID_ACCOUNTING_PARAMETER_PK ,  " );
        sbQuery.append("  ap.PARAMETER_TYPE,  " );
        sbQuery.append("  ap.NR_CORRELATIVE,  " );
        sbQuery.append("  ap.PARAMETER_NAME,  " );
        sbQuery.append("  ap.DESCRIPTION,     " );
        sbQuery.append("  ap.STATUS           " );
        sbQuery.append(" from Accounting_Parameter ap" );
        sbQuery.append("  " );
        sbQuery.append(" where 1=1 " );
        
        if(Validations.validateIsNotNullAndNotEmpty(name)){
            sbQuery.append(" and  ap.PARAMETER_NAME = :name ");
        }
        
        Query query = em.createNativeQuery(sbQuery.toString());
       
        if(Validations.validateIsNotNullAndNotEmpty(name)){
            query.setParameter("name", name);
        }
        
        
        List<Object[]>  objectList = query.getResultList();
    	
		Object[] sResults = (Object[])objectList.get(0);
		AccountingParameterTO accountingParameter = new AccountingParameterTO();
		
	 
		
		accountingParameter.setIdAccountingParameterPk(Integer.parseInt(sResults[0]==null?"":sResults[0].toString()));
		accountingParameter.setParameterType(Integer.parseInt(sResults[1]==null?"":sResults[1].toString()));	
		accountingParameter.setCorrelativeCode(sResults[2]==null?null:Integer.parseInt(sResults[2].toString()));
		accountingParameter.setParameterName(sResults[3]==null?"":sResults[3].toString());			
		accountingParameter.setDescription(sResults[4]==null?"":sResults[4].toString());
		accountingParameter.setStatus(sResults[5]==null?null:Integer.parseInt(sResults[5].toString())); 
         
		
		return accountingParameter;
	}
}
