package com.pradera.accounting.parameter.to;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingParameterTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingParameterTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3632201120647874452L;
	
	
	
	/** The id accounting parameter pk. */
	private Integer idAccountingParameterPk;
	
	/** The parameter name. */
	private String parameterName;
	
	/** The parameter name consult. */
	private String parameterNameConsult;
	
	/** The description. */
	private String description;
	
	/** The parameter type. */
	private Integer parameterType;
	
	/** The correlative code. */
	private Integer correlativeCode;
	
	/** The status. */
	private Integer status;
	
	/** The register date. */
	private String registerDate;
	
	/** The modify date. */
	private String modifyDate;
	
	/** The register user. */
	private String registerUser;
	
	/** The modify user. */
	private String modifyUser;
	 
 
 
	
	
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Gets the parameter name.
	 *
	 * @return the parameter name
	 */
	public String getParameterName() {
		return parameterName;
	}
	
	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the new parameter name
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the correlative code.
	 *
	 * @return the correlative code
	 */
	public Integer getCorrelativeCode() {
		return correlativeCode;
	}
	
	/**
	 * Sets the correlative code.
	 *
	 * @param correlativeCode the new correlative code
	 */
	public void setCorrelativeCode(Integer correlativeCode) {
		this.correlativeCode = correlativeCode;
	}
	
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public String getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the modify date.
	 *
	 * @return the modify date
	 */
	public String getModifyDate() {
		return modifyDate;
	}
	
	/**
	 * Sets the modify date.
	 *
	 * @param modifyDate the new modify date
	 */
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	/**
	 * Gets the register user.
	 *
	 * @return the register user
	 */
	public String getRegisterUser() {
		return registerUser;
	}
	
	/**
	 * Sets the register user.
	 *
	 * @param registerUser the new register user
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}
	
	/**
	 * Gets the modify user.
	 *
	 * @return the modify user
	 */
	public String getModifyUser() {
		return modifyUser;
	}
	
	/**
	 * Sets the modify user.
	 *
	 * @param modifyUser the new modify user
	 */
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	
	/**
	 * Gets the parameter type.
	 *
	 * @return the parameter type
	 */
	public Integer getParameterType() {
		return parameterType;
	}
	
	/**
	 * Sets the parameter type.
	 *
	 * @param parameterType the new parameter type
	 */
	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
	/**
	 * Gets the id accounting parameter pk.
	 *
	 * @return the id accounting parameter pk
	 */
	public Integer getIdAccountingParameterPk() {
		return idAccountingParameterPk;
	}
	
	/**
	 * Sets the id accounting parameter pk.
	 *
	 * @param idAccountingParameterPk the new id accounting parameter pk
	 */
	public void setIdAccountingParameterPk(Integer idAccountingParameterPk) {
		this.idAccountingParameterPk = idAccountingParameterPk;
	}
	
	/**
	 * Gets the parameter name consult.
	 *
	 * @return the parameter name consult
	 */
	public String getParameterNameConsult() {
		return parameterNameConsult;
	}
	
	/**
	 * Sets the parameter name consult.
	 *
	 * @param parameterNameConsult the new parameter name consult
	 */
	public void setParameterNameConsult(String parameterNameConsult) {
		this.parameterNameConsult = parameterNameConsult;
	}
		
	

}
