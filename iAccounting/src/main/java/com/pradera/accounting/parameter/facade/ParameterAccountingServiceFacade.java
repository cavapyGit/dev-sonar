package com.pradera.accounting.parameter.facade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounting.common.utils.SequenceGeneratorCode;
import com.pradera.accounting.parameter.service.ParameterAccountingServiceBean;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.type.AccountingParameterStateType;
 

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ParameterAccountingServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ParameterAccountingServiceFacade implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;	
	
	/** The parameter accounting service bean. */
	@EJB
	ParameterAccountingServiceBean parameterAccountingServiceBean;	
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sequence generator code. */
	@EJB
	SequenceGeneratorCode  				sequenceGeneratorCode;
	

	/**
	 * Save parameter accounting.
	 *
	 * @param parameterAccounting the parameter accounting
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void saveParameterAccounting(AccountingParameter parameterAccounting){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		//generate NR correlative
		Integer correlative = parameterAccountingServiceBean.findNrCorrelative(parameterAccounting.getParameterType());
			 
		correlative = correlative+1;
		 
		// Setting correlative Nr if ZERO 
		parameterAccounting.setNrCorrelative(correlative);
		
		parameterAccounting.setRegisterUser(loggerUser.getUserName());
		parameterAccounting.setRegisterDate(loggerUser.getAuditTime());
			
		
		parameterAccountingServiceBean.saveParameterAccounting(parameterAccounting);
		
	}
	
	
	/**
	 * Find list parameter accounting.
	 *
	 * @param parameter the parameter
	 * @return the list
	 */
	public List<AccountingParameter> findListParameterAccounting(AccountingParameterTO parameter){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingParameter> listAccountingParameters = new ArrayList<AccountingParameter>();
		
		listAccountingParameters = parameterAccountingServiceBean.findListParameterAccounting(parameter);  
		
		return listAccountingParameters;
	}	 
	
	/**
	 * Search parameter accounting to.
	 *
	 * @param parameter the parameter
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingParameterTO> searchParameterAccountingTO(AccountingParameterTO parameter){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingParameterTO> listAccountingParameters = new ArrayList<AccountingParameterTO>();
		
		listAccountingParameters = parameterAccountingServiceBean.searchParameterAccountingTo(parameter);  
		
		return listAccountingParameters;
	}	
	
	
	/**
	 * Gets the accounting parameter by pk.
	 *
	 * @param id the id
	 * @return the accounting parameter by pk
	 */
	// Find  AccountingParameter by Pk
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingParameter getAccountingParameterByPk(Integer id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return parameterAccountingServiceBean.find(AccountingParameter.class, id);
	}
	
	
	/**
	 * Update parameter status.
	 *
	 * @param accountingParameter the accounting parameter
	 * @throws ServiceException the service exception
	 */
	// CHANGE STATUS 
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateParameterStatus(AccountingParameter accountingParameter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		
		if(accountingParameter.getStatus().equals(AccountingParameterStateType.DELETED.getCode())){
			
			accountingParameter.setCancelDate(loggerUser.getAuditTime());
			accountingParameter.setCancelUser(loggerUser.getUserName());
			
		}
		
		accountingParameter.setUpdateDate(loggerUser.getAuditTime());
		accountingParameter.setUpdateUser(loggerUser.getUserName());		
		
		parameterAccountingServiceBean.update(accountingParameter);
	}
	
	
	
	/**
	 * Update accounting parameter.
	 *
	 * @param accountingParameter the accounting parameter
	 * @throws ServiceException the service exception
	 */
	// UPDATE ACCOUNTING_PARAMETER 
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateAccountingParameter(AccountingParameter accountingParameter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		parameterAccountingServiceBean.update(accountingParameter);
	}
	
	/**
	 * Find accounting parameter to by name.
	 *
	 * @param name the name
	 * @return the accounting parameter to
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingParameterTO findAccountingParameterToByName(String name){
		
		return parameterAccountingServiceBean.findAccountingParameterToByName(name);
		
	}
}
