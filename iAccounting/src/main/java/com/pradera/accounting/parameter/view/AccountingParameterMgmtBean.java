package com.pradera.accounting.parameter.view;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingParameterMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class AccountingParameterMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade	generalParametersFacade;

	/** The parameter accounting service facade. */
	@EJB
	ParameterAccountingServiceFacade	parameterAccountingServiceFacade;

	/**  parameters used on page search. */
	private List<ParameterTable> listParameterType;	
	
	/** The list accounting status. */
	private List<ParameterTable> listAccountingStatus;
	
	/** The accounting parameter to. */
	private AccountingParameterTO accountingParameterTo;		
	
	/** The parameter type selected. */
	private Integer parameterTypeSelected;		
	
	/** The accounting parameter. */
	private AccountingParameter accountingParameter;		
	
	/** The accounting parameter selection. */
	private AccountingParameter accountingParameterSelection;	
	
	/** The register date. */
	private Date registerDate;	
	
	/** The list accounting parameter. */
	private List<AccountingParameter> listAccountingParameter = new ArrayList<AccountingParameter>();	
	
	/** The accounting parameter data model. */
	private GenericDataModel<AccountingParameter> accountingParameterDataModel; 
	
	/** The consult. */
	private Boolean consult = Boolean.FALSE;
		
	
	
	
	
	/**
	 * Gets the list parameter type.
	 *
	 * @return the list parameter type
	 */
	public List<ParameterTable> getListParameterType() {
		return listParameterType;
	}	
	
	/**
	 * Sets the list parameter type.
	 *
	 * @param listParameterType the new list parameter type
	 */
	public void setListParameterType(List<ParameterTable> listParameterType) {
		this.listParameterType = listParameterType;
	}		
 
	/**
	 * Gets the accounting parameter to.
	 *
	 * @return the accounting parameter to
	 */
	public AccountingParameterTO getAccountingParameterTo() {
		return accountingParameterTo;
	}
	
	/**
	 * Sets the accounting parameter to.
	 *
	 * @param accountingParameterTo the new accounting parameter to
	 */
	public void setAccountingParameterTo(AccountingParameterTO accountingParameterTo) {
		this.accountingParameterTo = accountingParameterTo;
	}
	
	/**
	 * Gets the parameter type selected.
	 *
	 * @return the parameter type selected
	 */
	public Integer getParameterTypeSelected() {
		return parameterTypeSelected;
	}
	
	/**
	 * Sets the parameter type selected.
	 *
	 * @param parameterTypeSelected the new parameter type selected
	 */
	public void setParameterTypeSelected(Integer parameterTypeSelected) {
		this.parameterTypeSelected = parameterTypeSelected;
	}
	
	/**
	 * Gets the accounting parameter.
	 *
	 * @return the accounting parameter
	 */
	public AccountingParameter getAccountingParameter() {
		return accountingParameter;
	}
	
	/**
	 * Sets the accounting parameter.
	 *
	 * @param accountingParameter the new accounting parameter
	 */
	public void setAccountingParameter(AccountingParameter accountingParameter) {
		this.accountingParameter = accountingParameter;
	}
	
	/**
	 * Gets the accounting parameter data model.
	 *
	 * @return the accounting parameter data model
	 */
	public GenericDataModel<AccountingParameter> getAccountingParameterDataModel() {
		return accountingParameterDataModel;
	}
	
	/**
	 * Sets the accounting parameter data model.
	 *
	 * @param accountingParameterDataModel the new accounting parameter data model
	 */
	public void setAccountingParameterDataModel(
			GenericDataModel<AccountingParameter> accountingParameterDataModel) {
		this.accountingParameterDataModel = accountingParameterDataModel;
	}
	
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the accounting parameter selection.
	 *
	 * @return the accounting parameter selection
	 */
	public AccountingParameter getAccountingParameterSelection() {
		return accountingParameterSelection;
	}
	
	/**
	 * Sets the accounting parameter selection.
	 *
	 * @param accountingParameterSelection the new accounting parameter selection
	 */
	public void setAccountingParameterSelection(
			AccountingParameter accountingParameterSelection) {
		this.accountingParameterSelection = accountingParameterSelection;
	}
	
	/**
	 * Gets the list accounting status.
	 *
	 * @return the list accounting status
	 */
	public List<ParameterTable> getListAccountingStatus() {
		return listAccountingStatus;
	}
	
	/**
	 * Sets the list accounting status.
	 *
	 * @param listAccountingStatus the new list accounting status
	 */
	public void setListAccountingStatus(List<ParameterTable> listAccountingStatus) {
		this.listAccountingStatus = listAccountingStatus;
	}
	
	/**
	 * Gets the list accounting parameter.
	 *
	 * @return the list accounting parameter
	 */
	public List<AccountingParameter> getListAccountingParameter() {
		return listAccountingParameter;
	}
	
	/**
	 * Sets the list accounting parameter.
	 *
	 * @param listAccountingParameter the new list accounting parameter
	 */
	public void setListAccountingParameter(
			List<AccountingParameter> listAccountingParameter) {
		this.listAccountingParameter = listAccountingParameter;
	}
	
	
	
	
	/**
	 * Gets the consult.
	 *
	 * @return the consult
	 */
	public Boolean getConsult() {
		return consult;
	}
	
	/**
	 * Sets the consult.
	 *
	 * @param consult the new consult
	 */
	public void setConsult(Boolean consult) {
		this.consult = consult;
	}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException  {
		
		if (!FacesContext.getCurrentInstance().isPostback()){
			 
			this.registerDate =  CommonsUtilities.currentDate();
			this.accountingParameterTo = new AccountingParameterTO();
			
			// Call to load cmbData
			loadCmbDataSearch();
			
		}
		
	}
	
	
	/**
	 * 	 Load cmbData 	*.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCmbDataSearch() throws ServiceException {
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		
		this.consult = Boolean.FALSE;

		if (Validations.validateListIsNullOrEmpty(listParameterType)) {
			
			parameterTableTO.setState(1);
			parameterTableTO.setMasterTableFk(PropertiesConstants.PARAMETER_TYPE);
			listParameterType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} 
		if(Validations.validateListIsNullOrEmpty(listAccountingStatus)){
			
			parameterTableTO.setState(1);
			parameterTableTO.setMasterTableFk(PropertiesConstants.PARAMETER_ACCOUNTING_STATUS);
			listAccountingStatus = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}  
		if(Validations.validateIsNotNullAndNotEmpty(parameterTypeSelected)){
			
			parameterTypeSelected = -1 ;
		}
		
		accountingParameterTo = new AccountingParameterTO();
		this.accountingParameterTo.setRegisterUser(this.userInfo.getUserAccountSession().getUserName());

		
	}	 
	  
	  


	/**
	 * ALL BEFORE.
	 */

	
	// Save
	public void beforeRegisterParameter(){
		
		if(Validations.validateIsNotNullAndPositive(parameterTypeSelected) || 
				   Validations.validateIsNotNullAndNotEmpty(this.accountingParameterTo.getParameterName()) ||
				   Validations.validateIsNotNullAndNotEmpty(this.accountingParameterTo.getDescription()) 	  ){	
		
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SAVE_PARAMETER,""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSave.show();");
			 
		}  
		
	}
	
	
	/**
	 * Before modify parameter.
	 */
	@LoggerAuditWeb
	public void beforeModifyParameter(){
		
		AccountingParameter accountingParameter = parameterAccountingServiceFacade. 
				getAccountingParameterByPk(this.accountingParameterSelection.getIdAccountingParameterPk());
		
		if(accountingParameter.getParameterName().equals(this.accountingParameterSelection.getParameterName()) &&
		   accountingParameter.getDescription().equals(this.accountingParameterSelection.getDescription())	   &&
		   accountingParameter.getParameterType().equals(parameterTypeSelected)			){
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MODIFY_PARAMETER ,""));
			JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
			
			return ;
			
		}else	
		
 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_MODIFY_PARAMETER,""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmModify.show();");
			 
		 
	}
	
 
	/**
	 * Before actived parameter accounting.
	 *
	 * @param event the event
	 */
	// Active
	public void beforeActivedParameterAccounting(ActionEvent event){

		
		if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
			Object name = accountingParameterSelection.getParameterName();
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.DELETED.getValue()))
			{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.LOCK.getValue()))
			{				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_LOCK ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.ACTIVED.getValue()))
			{									
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_ACTIVE ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			// When is register
		 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PARAMETER_ACTIVED ,name));
			JSFUtilities.executeJavascriptFunction("cnfwParameterActived.show();");
			
		}
		
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");			
		}
  
		
	}	
	
	
	 
	/**
	 * Before locked parameter accounting.
	 *
	 * @param event the event
	 */
	// Locked
	public void beforeLockedParameterAccounting(ActionEvent event){

		
		if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
			Object name = accountingParameterSelection.getParameterName();
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.DELETED.getValue()))
			{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.LOCK.getValue()))
			{				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_LOCK ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			
			// When is registered and actived
		 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PARAMETER_LOCKED ,name));
			JSFUtilities.executeJavascriptFunction("cnfwParameterLocked.show();");
			
		}
		
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");			
		}
  
		
	}	
	
	
	
	/**
	 * Before unlocked parameter accounting.
	 *
	 * @param event the event
	 */
	// Unclocked
	public void beforeUnlockedParameterAccounting(ActionEvent event){

		
		if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
			Object name = accountingParameterSelection.getParameterName();
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.DELETED.getValue()))
			{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.REGISTERED.getValue()))
			{				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_REGISTERED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.ACTIVED.getValue()))
			{									
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_ACTIVE ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			
			// When is locked
		 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PARAMETER_UNLOCKED ,name));
			JSFUtilities.executeJavascriptFunction("cnfwParameterUnlcoked.show();");
			
		}
		
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");			
		}
  
		
	}	
	
	
	/**
	 * Before deleted parameter accounting.
	 *
	 * @param event the event
	 */
	// Deleted
	public void beforeDeletedParameterAccounting(ActionEvent event){

		
		if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
			Object name = accountingParameterSelection.getParameterName();
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.DELETED.getValue()))
			{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.ACTIVED.getValue()))
			{									
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_ACTIVE ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			} 
			// When is register
		 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PARAMETER_DELETED ,name));
			JSFUtilities.executeJavascriptFunction("cnfwParameterDeleted.show();");
			
		}
		
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");			
		}
  
		
	}	
	
	
	/**
	 * END BEFORE.
	 */ 

	
	/***
	 * Save Accounting Parameter 
	 */
	@LoggerAuditWeb
	public void saveAccountingParameter(){
		
		accountingParameter = new AccountingParameter();
		
		accountingParameter.setParameterType(this.parameterTypeSelected);
		accountingParameter.setDescription(this.accountingParameterTo.getDescription());
		accountingParameter.setParameterName(this.accountingParameterTo.getParameterName().trim());
		accountingParameter.setStatus(AccountingParameterStateType.REGISTERED.getCode());
		
		parameterAccountingServiceFacade.saveParameterAccounting(accountingParameter);		
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_SAVE_PARAMETER,""));
		JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");

		// Clean
		accountingParameterDataModel = null ;
		accountingParameterTo = new AccountingParameterTO();
		parameterTypeSelected = -1 ;
		
		return; 	
		
	}
	
	/**
	 * *
	 * Modify Accounting Parameter.
	 */
	@LoggerAuditWeb
	public void modifyAccountingParameter(){
		 		
		try {
			
			AccountingParameter accountingParameter = parameterAccountingServiceFacade. 
				getAccountingParameterByPk(this.accountingParameterSelection.getIdAccountingParameterPk());
		
			
			accountingParameter.setParameterType(parameterTypeSelected);
			accountingParameter.setParameterName(this.accountingParameterSelection.getParameterName().trim());
			accountingParameter.setDescription(this.accountingParameterSelection.getDescription());
			
			
			parameterAccountingServiceFacade.updateAccountingParameter(accountingParameter);
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_MODIFY_PARAMETER,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");

			// Clean
			accountingParameterDataModel = new GenericDataModel<AccountingParameter>();
			accountingParameterTo = new AccountingParameterTO();
			parameterTypeSelected = -1 ;
			
			return; 
			
			
		} catch (ServiceException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	/**
	 * Search parameter accounting.
	 */
	@LoggerAuditWeb
	public void searchParameterAccounting(){		
		 
		List<AccountingParameter> listAccountingParameter  ;
		
		if(Validations.validateIsNotNullAndNotEmpty(this.accountingParameterTo)){
			
			listAccountingParameter = parameterAccountingServiceFacade.findListParameterAccounting(this.accountingParameterTo);	
			accountingParameterDataModel = new GenericDataModel<AccountingParameter>(listAccountingParameter);
		
		}
		/** clean selected on data table */
		this.accountingParameterSelection = null;		
		if(accountingParameterDataModel.getRowCount()>0){
			setTrueValidButtons();
		}
	}
		
	
	
	/**
	 * Update parameter accounting actived.
	 */
	@LoggerAuditWeb
	public void updateParameterAccountingActived(){
			
		try {
			
			if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
				
				AccountingParameter accountingParameter = parameterAccountingServiceFacade.
						getAccountingParameterByPk(this.accountingParameterSelection.getIdAccountingParameterPk());
				
				accountingParameter.setStatus(AccountingParameterStateType.ACTIVED.getCode());
				
				parameterAccountingServiceFacade.updateParameterStatus(accountingParameter);
				
				accountingParameterDataModel = null ;
				searchParameterAccounting();
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
 
	
	/**
	 * Update parameter accounting locked.
	 */
	@LoggerAuditWeb
	public void updateParameterAccountingLocked(){
			
		try {
			
			if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
				
				AccountingParameter accountingParameter = parameterAccountingServiceFacade.
						getAccountingParameterByPk(this.accountingParameterSelection.getIdAccountingParameterPk());
				
				accountingParameter.setStatus(AccountingParameterStateType.LOCK.getCode());
				
				parameterAccountingServiceFacade.updateParameterStatus(accountingParameter);
				
				accountingParameterDataModel = null ;
				searchParameterAccounting();
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	
	/**
	 * Update parameter accounting unlocked.
	 */
	@LoggerAuditWeb
	public void updateParameterAccountingUnlocked(){
			
		try {
			
			if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
				
				AccountingParameter accountingParameter = parameterAccountingServiceFacade.
						getAccountingParameterByPk(this.accountingParameterSelection.getIdAccountingParameterPk());
				
				accountingParameter.setStatus(AccountingParameterStateType.REGISTERED.getCode());
				
				parameterAccountingServiceFacade.updateParameterStatus(accountingParameter);
				
				accountingParameterDataModel = null ;
				searchParameterAccounting();
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	
	/**
	 * Update parameter accounting deleted.
	 */
	@LoggerAuditWeb
	public void updateParameterAccountingDeleted(){
			
		try {
			
			if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
				
				AccountingParameter accountingParameter = parameterAccountingServiceFacade.
						getAccountingParameterByPk(this.accountingParameterSelection.getIdAccountingParameterPk());
				
				accountingParameter.setStatus(AccountingParameterStateType.DELETED.getCode());
				
				parameterAccountingServiceFacade.updateParameterStatus(accountingParameter);
				
				accountingParameterDataModel = null ;
				searchParameterAccounting();
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	
	
	/**
	 * Accounting parameter on consult.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void accountingParameterOnConsult(ActionEvent event) throws ServiceException{
		
		if (event != null) {
			
		 
	
			AccountingParameter  accountingParameter = (AccountingParameter) event.getComponent()
					.getAttributes().get("blockInformation");
			
			this.consult = Boolean.TRUE;
			
			this.accountingParameterTo.setRegisterDate(accountingParameter.getRegisterDate().toString());
			this.accountingParameterTo.setParameterNameConsult(accountingParameter.getParameterName());
			this.accountingParameterTo.setDescription(accountingParameter.getDescription());
			this.accountingParameterTo.setModifyDate(accountingParameter.getLastModifyDate().toString());
			this.accountingParameterTo.setCorrelativeCode(accountingParameter.getNrCorrelative());
			this.accountingParameterTo.setRegisterUser(accountingParameter.getRegisterUser());
			this.accountingParameterTo.setModifyUser(accountingParameter.getLastModifyUser());
			this.parameterTypeSelected = accountingParameter.getParameterType();
			
		}
		
		
	}
	
	
	
	
	
	/**
	 * 	CLEAN PARAMTER TO.
	 */
	
	public void cleanParamterTo(){
		
		
		// Message before clean when is register a parameter accounting
		if( (Validations.validateIsNotNullAndNotEmpty(accountingParameterTo.getParameterName())) ||
			    (Validations.validateIsNotNullAndNotEmpty(accountingParameterTo.getDescription())) ||
			    (Validations.validateIsNotNullAndPositive(parameterTypeSelected)) ){
			
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CLEAN_DATA_EXCEPTION ,"" ));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanRegister.show();") ;
			
			
		}else
		
			cleanRegisterParameterAccounting();
		
	}
	
	
	/**
	 * Clean mgmt parameter accounting.
	 */
	public void cleanMgmtParameterAccounting(){		
		
		accountingParameterDataModel = null ;
		accountingParameterTo.setParameterName(null);
		parameterTypeSelected = -1 ;
		cleanParamterTo();		
	}
	
	/**
	 * Clean register parameter accounting.
	 */
	public void cleanRegisterParameterAccounting(){		
		
		accountingParameterTo = new AccountingParameterTO();
		parameterTypeSelected = -1 ;		
		this.accountingParameterTo.setRegisterUser(this.userInfo.getUserAccountSession().getUserName());

	}
	
	
	/**
	 * Clean result accounting parameter data model.
	 */
	public void cleanResultAccountingParameterDataModel(){

		accountingParameterDataModel = null ;
		 
	}
	
	
	
	
	
	/**
	 *  OTHER.
	 *
	 * @return the string
	 */
	
	public String beforeBackToSearch(){
		
		if( (Validations.validateIsNotNullAndNotEmpty(accountingParameterTo.getParameterName())) ||
			    (Validations.validateIsNotNullAndNotEmpty(accountingParameterTo.getDescription())) ||
			    (Validations.validateIsNotNullAndPositive(parameterTypeSelected)) ){
			 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BACK_EXCEPTION_SEARCH ,"" ));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanBack.show();") ;
			
			return "";
			
		}else
			
			return "parameterSearch";
		
	}
	
	/**
	 * *
	 * Before Modify Back To Search.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beforeModifyBackToSearch(){
		
		AccountingParameter accountingParameter = parameterAccountingServiceFacade. 
				getAccountingParameterByPk(this.accountingParameterSelection.getIdAccountingParameterPk());
		
		if(accountingParameter.getParameterName().equals(this.accountingParameterSelection.getParameterName()) &&
		   accountingParameter.getDescription().equals(this.accountingParameterSelection.getDescription())	   &&
		   accountingParameter.getParameterType().equals(parameterTypeSelected)			){
			
			 
		  return "parameterSearch";
			
		}else
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BACK_EXCEPTION_SEARCH ,"" ));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanBack.show();") ;
			
			return "";			
		
	}
	
	
	/**
	 * Go to back to search.
	 *
	 * @return the string
	 */
	public String goToBackToSearch(){
		
		cleanRegisterParameterAccounting();
		
		return "parameterSearch";
		
	}
	
	
	/**
	 * Before modify parameter accounting.
	 *
	 * @return the string
	 */
	// Validate before Modify Accounting Parameter 
	public String beforeModifyParameterAccounting(){
		
				
		if (Validations.validateIsNotNull(this.accountingParameterSelection)) {	
			Object name = accountingParameterSelection.getParameterName();
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.DELETED.getValue()))
			{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return "";
			} 
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.LOCK.getValue()))
			{				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_LOCK ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return "";
			} 
			if (accountingParameterSelection.getDescriptionParameterStatus().equals(AccountingParameterStateType.ACTIVED.getValue()))
			{				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_ACTIVE ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return "";
			} 
			
			
			// When is registered
		 
			return "parameterModify";
			
		}
		
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
			
			return ""; 
		}
  
		
		
		
	}
	
	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons(){        

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);    
		}
		if(userPrivilege.getUserAcctions().isBlock()){
			privilegeComponent.setBtnBlockView(true);
		}
		if(userPrivilege.getUserAcctions().isUnblock()){
			privilegeComponent.setBtnUnblockView(true);
		}
		if(userPrivilege.getUserAcctions().isDelete()){
			privilegeComponent.setBtnDeleteView(true);
		}
		if(userPrivilege.getUserAcctions().isActivate()){
			privilegeComponent.setBtnActivate(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent); 

	}

	
}




