package com.pradera.accounting.source.to;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.pradera.accounting.parameter.to.AccountingParameterTo;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingSourceInfo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingSourceInfo  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The name file. */
	private String 						nameFile ;
	
	/** The accounting parameter to. */
	private AccountingParameterTo 		accountingParameterTo;
	
	/** The fields. */
	public Map<Integer, FieldSourceTo>  fields = new HashMap<Integer, FieldSourceTo>();
	
	/** The query source. */
	private QuerySourceTo 				querySource;
	
	/** The status. */
	private Integer						status;
	
	/** The execution date. */
	private Date						executionDate;
	
	/** Mnemonic. */
	private String						typeAssociation;
	
	/** The associate code. */
	private Integer						associateCode;
	
	/** The instrument type. */
	private Integer						instrumentType;
	
	/** The currency. */
	private Integer						currency;
	
	/**
	 * Gets the name file.
	 *
	 * @return the name file
	 */
	public String getNameFile() {
		return nameFile;
	}
	
	/**
	 * Sets the name file.
	 *
	 * @param nameFile the new name file
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}
	
	/**
	 * Gets the accounting parameter to.
	 *
	 * @return the accounting parameter to
	 */
	public AccountingParameterTo getAccountingParameterTo() {
		return accountingParameterTo;
	}
	
	/**
	 * Sets the accounting parameter to.
	 *
	 * @param accountingParameterTo the new accounting parameter to
	 */
	public void setAccountingParameterTo(AccountingParameterTo accountingParameterTo) {
		this.accountingParameterTo = accountingParameterTo;
	}
	
	/**
	 * Gets the fields.
	 *
	 * @return the fields
	 */
	public Map<Integer, FieldSourceTo> getFields() {
		return fields;
	}
	
	/**
	 * Sets the fields.
	 *
	 * @param fields the fields
	 */
	public void setFields(Map<Integer, FieldSourceTo> fields) {
		this.fields = fields;
	}
			
	
	/**
	 * Sets the field source to.
	 *
	 * @param position the position
	 * @param fieldSourceTo the field source to
	 */
	public void setFieldSourceTo(Integer position , FieldSourceTo fieldSourceTo){
		fields.put(position, fieldSourceTo);
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * Gets the query source.
	 *
	 * @return the query source
	 */
	public QuerySourceTo getQuerySource() {
		return querySource;
	}
	
	/**
	 * Sets the query source.
	 *
	 * @param querySource the new query source
	 */
	public void setQuerySource(QuerySourceTo querySource) {
		this.querySource = querySource;
	}
	
	/**
	 * Gets the execution date.
	 *
	 * @return the execution date
	 */
	public Date getExecutionDate() {
		return executionDate;
	}
	
	/**
	 * Sets the execution date.
	 *
	 * @param executionDate the new execution date
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}
	
	/**
	 * Gets the associate code.
	 *
	 * @return the associateCode
	 */
	public Integer getAssociateCode() {
		return associateCode;
	}
	
	/**
	 * Sets the associate code.
	 *
	 * @param associateCode the associateCode to set
	 */
	public void setAssociateCode(Integer associateCode) {
		this.associateCode = associateCode;
	}
	
	/**
	 * Gets the type association.
	 *
	 * @return the typeAssociation
	 */
	public String getTypeAssociation() {
		return typeAssociation;
	}
	
	/**
	 * Sets the type association.
	 *
	 * @param typeAssociation the typeAssociation to set
	 */
	public void setTypeAssociation(String typeAssociation) {
		this.typeAssociation = typeAssociation;
	}
	
	/**
	 * Gets the instrument type.
	 *
	 * @return the instrumentType
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}
	
	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the instrumentType to set
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	
	
	
	
	
}
