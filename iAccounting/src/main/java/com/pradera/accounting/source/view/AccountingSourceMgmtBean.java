package com.pradera.accounting.source.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.event.FileUploadEvent;

import com.pradera.accounting.account.facade.AccountAccountingServiceFacade;
import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.formulate.to.ParameterFormulateTo;
import com.pradera.accounting.formulate.to.VariableFormulateTo;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.matrix.to.AccountingMatrixDetailTo;
import com.pradera.accounting.matrix.to.AccountingMatrixTo;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.schema.facade.SchemaAccountingServiceFacade;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.accounting.source.facade.SourceAccountingServiceFacade;
import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.accounting.source.to.FieldSourceTo;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounting.account.to.AccountingSourceTo;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.AccountingSource;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingSourceStateType;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingSourceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class AccountingSourceMgmtBean extends GenericBaseBean  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

//	@EJB
//	BillingServiceProducer billingServiceProducer;

	/** The parameter accounting service facade. */
@EJB
	ParameterAccountingServiceFacade parameterAccountingServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade	generalParametersFacade;

	/** The schema accounting service facade. */
	@EJB
	SchemaAccountingServiceFacade schemaAccountingServiceFacade;
	
	/** The account accounting service facade. */
	@EJB
	AccountAccountingServiceFacade accountAccountingServiceFacade;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade accountingMatrixServiceFacade;
	
	/** The source accounting service facade. */
	@EJB
	SourceAccountingServiceFacade sourceAccountingServiceFacade;
	
	
	/**  parameters used on page search. */
	private List<ParameterTable> listParameterSchema;
	
	/** The list schema status. */
	private List<ParameterTable> listSchemaStatus;
	
	/** The list source status. */
	private List<ParameterTable> listSourceStatus;
	
	/** The list parameter branch. */
	private List<AccountingParameter> listParameterBranch;
	
	/** The list parameter auxiliary type. */
	private List<AccountingParameter> listParameterAuxiliaryType;
	
	/** The accounting parameter to. */
	private AccountingParameterTO accountingParameterTo = new AccountingParameterTO();
	
	/** The accounting schema to. */
	private AccountingSchemaTo accountingSchemaTo;
	
	/** The parameter branch selected. */
	private Integer parameterBranchSelected;
	
	/** The parameter auxiliary type selected. */
	private Integer parameterAuxiliaryTypeSelected;
	
	/** The accounting schema selection. */
	private AccountingSchema accountingSchemaSelection;
	
	/** The accounting source selection. */
	private AccountingSource accountingSourceSelection;
	
	/** The accounting schema. */
	private AccountingSchema accountingSchema;
	
	/** The register date. */
	private Date registerDate;
	
	/** The accounting schema data model. */
	private GenericDataModel<AccountingSchema> accountingSchemaDataModel;
	
	/** The accounting source data model. */
	private GenericDataModel<AccountingSource> accountingSourceDataModel;

	/** The consult. */
	private Boolean consult = Boolean.FALSE;
	
	/** The accept formulate. */
	private Boolean acceptFormulate = Boolean.FALSE;
	
	/** The disable formulate. */
	private Boolean disableFormulate = Boolean.FALSE;
	
	/** The associated variable. */
	private Boolean associatedVariable = Boolean.FALSE;

	/**  Accounting Account To by Helper *. */
	private AccountingAccountTo accountingAccountTo;

	/**  Accounting Parameter for Formulate *. */
	private List<ParameterFormulateTo> listParameterFormulateTo;
	
	/** The variable formulate to selected. */
	private ParameterFormulateTo[] variableFormulateToSelected;
	
	/** The variable formulate to. */
	private VariableFormulateTo variableFormulateTo;
	
	/** The list variable formulate to. */
	private List<VariableFormulateTo> listVariableFormulateTo;

	/**  Accounting Account Temp to formulate *. */
	private AccountingAccountTo accountingAccountToTemp;

	/** The accounting matrix to. */
	private AccountingMatrixTo accountingMatrixTo;
	
	/** The list accounting matrix detail to. */
	private List<AccountingMatrixDetailTo> listAccountingMatrixDetailTo;

	/** The list source accounting source. */
	private List<ParameterFormulateTo> listSourceAccountingSource;
	
	/** *. */

	private AccountingMatrixDetailTo accountingMatrixDetailSelection;
	
	/** The accounting matrix detail to. */
	private AccountingMatrixDetailTo accountingMatrixDetailTo;
	
	/** The list accounting sources. */
	private List<AccountingSource> listAccountingSources;

	/** The auxiliary type selected. */
	private Integer auxiliaryTypeSelected;

	/**  Data Model FieldSourceTo *. */
	private GenericDataModel<FieldSourceTo> fieldSourceToDataModel;
	
	/** The list accounting account to. */
	private List<AccountingAccountTo> listAccountingAccountTo = new ArrayList<AccountingAccountTo>();
	
	/** The accounting account data model. */
	private GenericDataModel<AccountingAccountTo> accountingAccountDataModel;
	
	/** The accounting matrix detail data model. */
	private GenericDataModel<AccountingMatrixDetailTo> accountingMatrixDetailDataModel;

	/** *. */
	private String stringFinalFormula;

	/**  VARIABLES DE FORMULA DZ *. */
	private List<AccountingParameterTO> listVariable = new ArrayList<AccountingParameterTO>();
	
	/** The list operator. */
	private List<AccountingParameterTO> listOperator = new ArrayList<AccountingParameterTO>();

	/** The list variable tmp. */
	private List<AccountingParameterTO> listVariableTmp = new ArrayList<AccountingParameterTO>();
	
	/** The list operator tmp. */
	private List<AccountingParameterTO> listOperatorTmp = new ArrayList<AccountingParameterTO>();

	/** The formula final. */
	private List<AccountingParameterTO> formulaFinal = new ArrayList<AccountingParameterTO>();

	/** The variable hidden name. */
	private String variableHiddenName = "";
	
	/** The variable hidden code. */
	private String variableHiddenCode = "";
	
	/** The variable hidden index. */
	private String variableHiddenIndex = "";

	/** The auxiliary type selection to source. */
	private Integer auxiliaryTypeSelectionToSource = null ; 
	
	/** The accounting source. */
	private AccountingSource accountingSource;
	
	/** The accounting source to. */
	private AccountingSourceTo accountingSourceTo;
	
	/** The schema name display. */
	private String schemaNameDisplay;
	
	/** The accounting source info. */
	private AccountingSourceInfo accountingSourceInfo;
	
	
	/** The upload file size. */
	private String fUploadFileSize = "2000000"; 
	
	/** The register source. */
	private Boolean registerSource = Boolean.FALSE;
	
/**
 * Save accounting source.
 */
// Save Source
	@LoggerAuditWeb
	public void saveAccountingSource() {

		this.accountingSource.setDescriptionSource(this.accountingSourceTo.getDescriptionSource());
		this.accountingSource.setStatus(AccountingSourceStateType.REGISTERED.getCode());
		this.accountingSource.setAuxiliaryType(this.accountingSourceTo.getAuxiliaryType());
		
		sourceAccountingServiceFacade.saveAccountingSource(this.accountingSource);
		
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(
						PropertiesConstants.SUCCESS_SAVE_SOURCE, ""));
		JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");

		
		this.accountingSource = new AccountingSource();
		cleanSource();
		loadSources();

	}

	
	/**
	 * Clean source.
	 */
	public void cleanSource() {

		if (Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo)) {

			this.accountingSourceTo = new AccountingSourceTo();
			this.setSchemaNameDisplay(null);
			accountingSourceSelection = null ;
			this.auxiliaryTypeSelected = -1 ;
			schemaNameDisplay = null ;
		}
	}
	
	
	
	/**
	 * Load sources.
	 */
	@LoggerAuditWeb
	public void loadSources() {

		AccountingSourceTo accountingSourceToTemp = new AccountingSourceTo();
		listAccountingSources = new ArrayList<AccountingSource>();
		
		if(Validations.validateIsNotNullAndNotEmpty(auxiliaryTypeSelectionToSource)   
				&& Validations.validateIsNotNullAndPositive(auxiliaryTypeSelectionToSource)){
			
			accountingSourceToTemp.setAuxiliaryType(auxiliaryTypeSelectionToSource);
		}
		
		// Filter to search
		accountingSourceToTemp.setFileSourceName(this.accountingSourceInfo.getNameFile());
		accountingSourceToTemp.setStatus(this.accountingSourceInfo.getStatus());
		accountingSourceToTemp.setAuxiliaryType(this.auxiliaryTypeSelected);
		 
		listAccountingSources = sourceAccountingServiceFacade.loadAccountingSource(accountingSourceToTemp);

		if (Validations.validateIsNotNull(accountingSourceDataModel)) {
			accountingSourceDataModel = new GenericDataModel<AccountingSource>();
		}
		if (Validations.validateIsNotNullAndNotEmpty(listAccountingSources)) {
			accountingSourceDataModel = new GenericDataModel<AccountingSource>(listAccountingSources);
		}
		
		/** clean selected on data table */
		this.accountingSourceSelection = null;
		
		if(accountingSourceDataModel.getRowCount()>0){
			setTrueValidButtons();
		}
		
	}

	
	
	
	/**
	 * Adds the source on schema.
	 */
	public void addSourceOnSchema() { 
		
		if(registerSource.equals(Boolean.FALSE)){
			
			this.accountingSource = new AccountingSource();
			schemaNameDisplay = null ;
			this.auxiliaryTypeSelected = -1 ;
			
		}

		JSFUtilities.executeJavascriptFunction("cnIdFlagAddSource.show()");
		
	}
	
	
	/**
	 * Document attach participant file.
	 *
	 * @param event the event
	 */
	public void documentAttachParticipantFile(FileUploadEvent event) {

		String fDisplayName = fUploadValidateFile(event.getFile(), null, null);

		this.accountingSource.setFileXml(event.getFile().getContents());
		this.accountingSource.setFileSourceName(event.getFile().getFileName());

		if (fDisplayName != null) {
			schemaNameDisplay = fDisplayName;
		}

	}
	
	
	
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {

		ParameterTableTO parameterTableTO = new ParameterTableTO();
		
		this.accountingSourceInfo = new AccountingSourceInfo();
		this.registerDate = CommonsUtilities.currentDate();
		this.accountingSchemaTo = new AccountingSchemaTo();
		this.accountingAccountTo = new AccountingAccountTo();
		this.accountingAccountDataModel = new GenericDataModel<AccountingAccountTo>();
		this.accountingSourceTo = new AccountingSourceTo();
		this.accountingSource = new AccountingSource();

		this.consult = Boolean.FALSE;
		listAccountingMatrixDetailTo = new ArrayList<AccountingMatrixDetailTo>();
		accountingMatrixTo = new AccountingMatrixTo();
		
		
		// load dailyAuxiliaryType
		if (Validations.validateListIsNullOrEmpty(listParameterAuxiliaryType)) {

			this.accountingParameterTo.setParameterType(PropertiesConstants.DAILY_AUXILIARY);
			this.accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			this.listParameterAuxiliaryType = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
		}
		if (Validations.validateListIsNullOrEmpty(listSourceStatus)) {

			parameterTableTO.setState(1);
			parameterTableTO.setMasterTableFk(PropertiesConstants.PARAMETER_SOURCE_STATUS);
			listSourceStatus = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
	}
	
	
	
	/**
	 * Before register source.
	 */
	public void beforeRegisterSource() {

		if (Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo) && (
				Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo.getDescriptionSource()) &&
				Validations.validateIsNotNullAndNotEmpty(schemaNameDisplay) &&
				!(Validations.validateIsNullOrNotPositive(this.accountingSourceTo.getAuxiliaryType())) )){
			
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SAVE_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceSave.show();");
			
			return ; 

		}
		else
		{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE ,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}

	}
	
	
	
	/**
	 * Before modify source.
	 */
	public void beforeModifySource() {
  
		if (Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo) && (
				Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo.getDescriptionSource()) &&
				Validations.validateIsNotNullAndNotEmpty(schemaNameDisplay) &&
				!(Validations.validateIsNullOrNotPositive(this.accountingSourceTo.getAuxiliaryType()))  
			 
				)){

			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(
							PropertiesConstants.CONFIRM_MODIFY_SOURCE, ""));
							JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceModify.show();");

		}
		else
		{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE ,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}
		
	}
	
	
	
	/**
	 * Validatebefore deleted accounting source.
	 *
	 * @param e the e
	 */
	// Validate before Delete an Accounting Source 
	public void validatebeforeDeletedAccountingSource(ActionEvent e){
		
		
		if (Validations.validateIsNotNull(this.accountingSourceSelection)) {	
			Object name = accountingSourceSelection.getFileSourceName();
			 
			this.accountingSourceTo.setDescriptionSource(accountingSourceSelection.getDescriptionSource());
			this.accountingSourceTo.setAuxiliaryType(accountingSourceSelection.getAuxiliaryType());			
			this.setSchemaNameDisplay(accountingSourceSelection.getFileSourceName());   
			
			if (accountingSourceSelection.getStatus().equals(AccountingSourceStateType.DELETED.getCode())) {
								
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return ;
			} 
			// Validate relation on matrix before delete an Source.
			
			registerSource = Boolean.TRUE;
			JSFUtilities.executeJavascriptFunction("cnIdFlagAddSource.show()");
			
		}		
		else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_NO_SELECTED));
					JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
			
			return ; 
		}	
		
	}

	
	
	
	/**
	 * Show associated variables.
	 */
	@LoggerAuditWeb
	public void showAssociatedVariables() {
		AccountingSourceInfo accountingSourceInfo = null;
		List<FieldSourceTo> listFieldSourceTo = new ArrayList<FieldSourceTo>();

		if (Validations.validateIsNotNullAndNotEmpty(this.accountingSourceSelection)) {
		
			byte[] file = sourceAccountingServiceFacade.getAccountingSourceContent(accountingSourceSelection.getIdAccountingSourcePk());
			try {
				accountingSourceInfo = AccountingUtils.loadXMLToObjectSource(file);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			if (accountingSourceInfo != null) {

				for (Integer key : accountingSourceInfo.getFields().keySet()) {
					FieldSourceTo fieldSourceTo = accountingSourceInfo.getFields().get(key);

					if (fieldSourceTo.getVisibility()) {

						listFieldSourceTo.add(fieldSourceTo);
					}
				}

				fieldSourceToDataModel = new GenericDataModel<FieldSourceTo>(listFieldSourceTo);
				associatedVariable = Boolean.TRUE;
			}
			else if (Validations.validateIsNullOrEmpty(accountingSourceInfo)) {
				
				fieldSourceToDataModel = null;
				associatedVariable = Boolean.FALSE;
				
				if(Validations.validateListIsNotNullAndNotEmpty(listVariableFormulateTo))
					listVariableFormulateTo.clear();
				
			}
				

			
		}
		
		
	}
	
	
	
	/**
	 * Before clean source.
	 */
	public void beforeCleanSource(){  
		
		if ( !(Validations.validateIsNullOrNotPositive(this.accountingSourceTo.getAuxiliaryType()))     ||
				Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo.getDescriptionSource()) ||
				!(Validations.validateIsNullOrEmpty(this.schemaNameDisplay)) ) {
			 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_CLEAN_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceClean.show();");
			
		}else
		
			cleanSource();
	}
	
	 

	/**
	 * Before back source on schema.
	 */
	public void beforeBackSourceOnSchema() {
		
		if (Validations.validateIsNullOrNotPositive(this.accountingSourceTo.getAuxiliaryType())  
				&&  Validations.validateIsNotNull(this.accountingSourceTo.getDescriptionSource()) ) {
			 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BACK_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceBack.show();");
			
		}else
			
			backSourceOnSchema();

	}

	
	/**
	 * Back source on schema.
	 */
	public void backSourceOnSchema() {

		cleanSource();
		JSFUtilities.executeJavascriptFunction("cnIdFlagAddSource.hide()");

	}
	
	
	/**
	 * Before back to source.
	 */
	public void beforeBackToSource(){
		
		if(Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo) && (
				Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo.getDescriptionSource()) ||
				Validations.validateIsNotNullAndNotEmpty(this.accountingSourceTo.getFileSourceName()) ||
				!(Validations.validateIsNullOrNotPositive(this.accountingSourceTo.getAuxiliaryType())) )  ) {
			
			showMessageOnDialog( PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage( PropertiesConstants.WARNING_BACK_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanBack.show();");
			
			return ;
		}else
			{
				registerSource = Boolean.FALSE;
				JSFUtilities.executeJavascriptFunction("cnIdFlagAddSource.hide()");
			}
	}
	
	
	/**
	 * Go back to source.
	 */
	public void goBackToSource(){
		
		this.accountingSourceTo = new AccountingSourceTo();
		schemaNameDisplay = null ;
		this.auxiliaryTypeSelected = -1 ;
		
		registerSource = Boolean.FALSE ;
		
		JSFUtilities.executeJavascriptFunction("cnIdFlagAddSource.hide()");
		
	}
	
	
	/**
	 * Before modify accounting source.
	 *
	 * @return the string
	 */
	// Validate before Modify Accounting Source 
	public String beforeModifyAccountingSource(){
					
		if (Validations.validateIsNotNull(this.accountingSourceSelection)) {	
			Object name = accountingSourceSelection.getFileSourceName();
			if (accountingSourceSelection.getStatus().equals(AccountingSourceStateType.DELETED.getCode()))
			{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return "";
			} 
			
			return "sourceRegister";
			
		}
		
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
			
			return ""; 
		}
   
		
	}
	
	
	/**
	 * Before deleted accounting source.
	 *
	 * @param e the e
	 */
	// Validate before Delete an Accounting Source 
	public void beforeDeletedAccountingSource(ActionEvent e){
		
		
		if (Validations.validateIsNotNull(this.accountingSourceSelection)) {	
			Object name = accountingSourceSelection.getFileSourceName();
			if (accountingSourceSelection.getStatus().equals(AccountingSourceStateType.DELETED.getCode()))
			{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return ;
			} 
			 
			
			// Validate relation on matrix before delete an Source.
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DELETED_SOURCE, name));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceDeleted.show();");	
			
			
		}
		
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
			
			return ; 
		}	
		
	}
	
	
	/**
	 * Delete accounting source.
	 */
	@LoggerAuditWeb
	public void deleteAccountingSource(){
		
		try {
			
			this.accountingSource.setStatus(AccountingSourceStateType.DELETED.getCode());
			this.accountingSource.setIdAccountingSourcePk(this.accountingSourceSelection.getIdAccountingSourcePk());
			
		
			sourceAccountingServiceFacade.updateAccountingSource(this.accountingSource);
			

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_DELETED_SOURCE,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");
			
		
			this.accountingSource = new AccountingSource();
			loadSources();
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * Modify accounting source.
	 */
	@LoggerAuditWeb
	public void modifyAccountingSource(){
		
		try {
			
			this.accountingSource.setDescriptionSource(this.accountingSourceTo.getDescriptionSource());
			this.accountingSource.setAuxiliaryType(this.accountingSourceTo.getAuxiliaryType());
			this.accountingSource.setIdAccountingSourcePk(this.accountingSourceSelection.getIdAccountingSourcePk());
			
			
			sourceAccountingServiceFacade.updateAccountingSource(this.accountingSource);
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_MODIFY_PARAMETER,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");
			 
	
			
			this.accountingSource = new AccountingSource();
			cleanSource();
			loadSources();
		
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	/**
	 * Clean accounting source.
	 */
	public void cleanAccountingSource(){
		
		accountingSourceDataModel = null;
		this.accountingSourceInfo = new AccountingSourceInfo() ;
		this.auxiliaryTypeSelected = -1 ;
				
	}
	
	
	
	
	
	

	/**
	 * Gets the list parameter schema.
	 *
	 * @return the list parameter schema
	 */
	public List<ParameterTable> getListParameterSchema() {
		return listParameterSchema;
	}


	/**
	 * Sets the list parameter schema.
	 *
	 * @param listParameterSchema the new list parameter schema
	 */
	public void setListParameterSchema(List<ParameterTable> listParameterSchema) {
		this.listParameterSchema = listParameterSchema;
	}


	/**
	 * Gets the list schema status.
	 *
	 * @return the list schema status
	 */
	public List<ParameterTable> getListSchemaStatus() {
		return listSchemaStatus;
	}


	/**
	 * Sets the list schema status.
	 *
	 * @param listSchemaStatus the new list schema status
	 */
	public void setListSchemaStatus(List<ParameterTable> listSchemaStatus) {
		this.listSchemaStatus = listSchemaStatus;
	}


	/**
	 * Gets the list parameter branch.
	 *
	 * @return the list parameter branch
	 */
	public List<AccountingParameter> getListParameterBranch() {
		return listParameterBranch;
	}


	/**
	 * Sets the list parameter branch.
	 *
	 * @param listParameterBranch the new list parameter branch
	 */
	public void setListParameterBranch(List<AccountingParameter> listParameterBranch) {
		this.listParameterBranch = listParameterBranch;
	}


	/**
	 * Gets the list parameter auxiliary type.
	 *
	 * @return the list parameter auxiliary type
	 */
	public List<AccountingParameter> getListParameterAuxiliaryType() {
		return listParameterAuxiliaryType;
	}


	/**
	 * Sets the list parameter auxiliary type.
	 *
	 * @param listParameterAuxiliaryType the new list parameter auxiliary type
	 */
	public void setListParameterAuxiliaryType(
			List<AccountingParameter> listParameterAuxiliaryType) {
		this.listParameterAuxiliaryType = listParameterAuxiliaryType;
	}


	/**
	 * Gets the accounting parameter to.
	 *
	 * @return the accounting parameter to
	 */
	public AccountingParameterTO getAccountingParameterTo() {
		return accountingParameterTo;
	}


	/**
	 * Sets the accounting parameter to.
	 *
	 * @param accountingParameterTo the new accounting parameter to
	 */
	public void setAccountingParameterTo(AccountingParameterTO accountingParameterTo) {
		this.accountingParameterTo = accountingParameterTo;
	}


	/**
	 * Gets the accounting schema to.
	 *
	 * @return the accounting schema to
	 */
	public AccountingSchemaTo getAccountingSchemaTo() {
		return accountingSchemaTo;
	}


	/**
	 * Sets the accounting schema to.
	 *
	 * @param accountingSchemaTo the new accounting schema to
	 */
	public void setAccountingSchemaTo(AccountingSchemaTo accountingSchemaTo) {
		this.accountingSchemaTo = accountingSchemaTo;
	}


	/**
	 * Gets the parameter branch selected.
	 *
	 * @return the parameter branch selected
	 */
	public Integer getParameterBranchSelected() {
		return parameterBranchSelected;
	}


	/**
	 * Sets the parameter branch selected.
	 *
	 * @param parameterBranchSelected the new parameter branch selected
	 */
	public void setParameterBranchSelected(Integer parameterBranchSelected) {
		this.parameterBranchSelected = parameterBranchSelected;
	}


	/**
	 * Gets the parameter auxiliary type selected.
	 *
	 * @return the parameter auxiliary type selected
	 */
	public Integer getParameterAuxiliaryTypeSelected() {
		return parameterAuxiliaryTypeSelected;
	}


	/**
	 * Sets the parameter auxiliary type selected.
	 *
	 * @param parameterAuxiliaryTypeSelected the new parameter auxiliary type selected
	 */
	public void setParameterAuxiliaryTypeSelected(
			Integer parameterAuxiliaryTypeSelected) {
		this.parameterAuxiliaryTypeSelected = parameterAuxiliaryTypeSelected;
	}


	/**
	 * Gets the accounting schema selection.
	 *
	 * @return the accounting schema selection
	 */
	public AccountingSchema getAccountingSchemaSelection() {
		return accountingSchemaSelection;
	}


	/**
	 * Sets the accounting schema selection.
	 *
	 * @param accountingSchemaSelection the new accounting schema selection
	 */
	public void setAccountingSchemaSelection(
			AccountingSchema accountingSchemaSelection) {
		this.accountingSchemaSelection = accountingSchemaSelection;
	}


	/**
	 * Gets the accounting source selection.
	 *
	 * @return the accounting source selection
	 */
	public AccountingSource getAccountingSourceSelection() {
		return accountingSourceSelection;
	}


	/**
	 * Sets the accounting source selection.
	 *
	 * @param accountingSourceSelection the new accounting source selection
	 */
	public void setAccountingSourceSelection(
			AccountingSource accountingSourceSelection) {
		this.accountingSourceSelection = accountingSourceSelection;
	}


	/**
	 * Gets the accounting schema.
	 *
	 * @return the accounting schema
	 */
	public AccountingSchema getAccountingSchema() {
		return accountingSchema;
	}


	/**
	 * Sets the accounting schema.
	 *
	 * @param accountingSchema the new accounting schema
	 */
	public void setAccountingSchema(AccountingSchema accountingSchema) {
		this.accountingSchema = accountingSchema;
	}


	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}


	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}


	/**
	 * Gets the accounting schema data model.
	 *
	 * @return the accounting schema data model
	 */
	public GenericDataModel<AccountingSchema> getAccountingSchemaDataModel() {
		return accountingSchemaDataModel;
	}


	/**
	 * Sets the accounting schema data model.
	 *
	 * @param accountingSchemaDataModel the new accounting schema data model
	 */
	public void setAccountingSchemaDataModel(
			GenericDataModel<AccountingSchema> accountingSchemaDataModel) {
		this.accountingSchemaDataModel = accountingSchemaDataModel;
	}


	/**
	 * Gets the accounting source data model.
	 *
	 * @return the accounting source data model
	 */
	public GenericDataModel<AccountingSource> getAccountingSourceDataModel() {
		return accountingSourceDataModel;
	}


	/**
	 * Sets the accounting source data model.
	 *
	 * @param accountingSourceDataModel the new accounting source data model
	 */
	public void setAccountingSourceDataModel(
			GenericDataModel<AccountingSource> accountingSourceDataModel) {
		this.accountingSourceDataModel = accountingSourceDataModel;
	}


	/**
	 * Gets the consult.
	 *
	 * @return the consult
	 */
	public Boolean getConsult() {
		return consult;
	}


	/**
	 * Sets the consult.
	 *
	 * @param consult the new consult
	 */
	public void setConsult(Boolean consult) {
		this.consult = consult;
	}


	/**
	 * Gets the accept formulate.
	 *
	 * @return the accept formulate
	 */
	public Boolean getAcceptFormulate() {
		return acceptFormulate;
	}


	/**
	 * Sets the accept formulate.
	 *
	 * @param acceptFormulate the new accept formulate
	 */
	public void setAcceptFormulate(Boolean acceptFormulate) {
		this.acceptFormulate = acceptFormulate;
	}


	/**
	 * Gets the disable formulate.
	 *
	 * @return the disable formulate
	 */
	public Boolean getDisableFormulate() {
		return disableFormulate;
	}


	/**
	 * Sets the disable formulate.
	 *
	 * @param disableFormulate the new disable formulate
	 */
	public void setDisableFormulate(Boolean disableFormulate) {
		this.disableFormulate = disableFormulate;
	}


	/**
	 * Gets the associated variable.
	 *
	 * @return the associated variable
	 */
	public Boolean getAssociatedVariable() {
		return associatedVariable;
	}


	/**
	 * Sets the associated variable.
	 *
	 * @param associatedVariable the new associated variable
	 */
	public void setAssociatedVariable(Boolean associatedVariable) {
		this.associatedVariable = associatedVariable;
	}


	/**
	 * Gets the accounting account to.
	 *
	 * @return the accounting account to
	 */
	public AccountingAccountTo getAccountingAccountTo() {
		return accountingAccountTo;
	}


	/**
	 * Sets the accounting account to.
	 *
	 * @param accountingAccountTo the new accounting account to
	 */
	public void setAccountingAccountTo(AccountingAccountTo accountingAccountTo) {
		this.accountingAccountTo = accountingAccountTo;
	}


	/**
	 * Gets the list parameter formulate to.
	 *
	 * @return the list parameter formulate to
	 */
	public List<ParameterFormulateTo> getListParameterFormulateTo() {
		return listParameterFormulateTo;
	}


	/**
	 * Sets the list parameter formulate to.
	 *
	 * @param listParameterFormulateTo the new list parameter formulate to
	 */
	public void setListParameterFormulateTo(
			List<ParameterFormulateTo> listParameterFormulateTo) {
		this.listParameterFormulateTo = listParameterFormulateTo;
	}


	/**
	 * Gets the variable formulate to selected.
	 *
	 * @return the variable formulate to selected
	 */
	public ParameterFormulateTo[] getVariableFormulateToSelected() {
		return variableFormulateToSelected;
	}


	/**
	 * Sets the variable formulate to selected.
	 *
	 * @param variableFormulateToSelected the new variable formulate to selected
	 */
	public void setVariableFormulateToSelected(
			ParameterFormulateTo[] variableFormulateToSelected) {
		this.variableFormulateToSelected = variableFormulateToSelected;
	}


	/**
	 * Gets the variable formulate to.
	 *
	 * @return the variable formulate to
	 */
	public VariableFormulateTo getVariableFormulateTo() {
		return variableFormulateTo;
	}


	/**
	 * Sets the variable formulate to.
	 *
	 * @param variableFormulateTo the new variable formulate to
	 */
	public void setVariableFormulateTo(VariableFormulateTo variableFormulateTo) {
		this.variableFormulateTo = variableFormulateTo;
	}


	/**
	 * Gets the list variable formulate to.
	 *
	 * @return the list variable formulate to
	 */
	public List<VariableFormulateTo> getListVariableFormulateTo() {
		return listVariableFormulateTo;
	}


	/**
	 * Sets the list variable formulate to.
	 *
	 * @param listVariableFormulateTo the new list variable formulate to
	 */
	public void setListVariableFormulateTo(
			List<VariableFormulateTo> listVariableFormulateTo) {
		this.listVariableFormulateTo = listVariableFormulateTo;
	}


	/**
	 * Gets the accounting account to temp.
	 *
	 * @return the accounting account to temp
	 */
	public AccountingAccountTo getAccountingAccountToTemp() {
		return accountingAccountToTemp;
	}


	/**
	 * Sets the accounting account to temp.
	 *
	 * @param accountingAccountToTemp the new accounting account to temp
	 */
	public void setAccountingAccountToTemp(
			AccountingAccountTo accountingAccountToTemp) {
		this.accountingAccountToTemp = accountingAccountToTemp;
	}


	/**
	 * Gets the accounting matrix to.
	 *
	 * @return the accounting matrix to
	 */
	public AccountingMatrixTo getAccountingMatrixTo() {
		return accountingMatrixTo;
	}


	/**
	 * Sets the accounting matrix to.
	 *
	 * @param accountingMatrixTo the new accounting matrix to
	 */
	public void setAccountingMatrixTo(AccountingMatrixTo accountingMatrixTo) {
		this.accountingMatrixTo = accountingMatrixTo;
	}


	/**
	 * Gets the list accounting matrix detail to.
	 *
	 * @return the list accounting matrix detail to
	 */
	public List<AccountingMatrixDetailTo> getListAccountingMatrixDetailTo() {
		return listAccountingMatrixDetailTo;
	}


	/**
	 * Sets the list accounting matrix detail to.
	 *
	 * @param listAccountingMatrixDetailTo the new list accounting matrix detail to
	 */
	public void setListAccountingMatrixDetailTo(
			List<AccountingMatrixDetailTo> listAccountingMatrixDetailTo) {
		this.listAccountingMatrixDetailTo = listAccountingMatrixDetailTo;
	}


	/**
	 * Gets the list source accounting source.
	 *
	 * @return the list source accounting source
	 */
	public List<ParameterFormulateTo> getListSourceAccountingSource() {
		return listSourceAccountingSource;
	}


	/**
	 * Sets the list source accounting source.
	 *
	 * @param listSourceAccountingSource the new list source accounting source
	 */
	public void setListSourceAccountingSource(
			List<ParameterFormulateTo> listSourceAccountingSource) {
		this.listSourceAccountingSource = listSourceAccountingSource;
	}


	/**
	 * Gets the accounting matrix detail selection.
	 *
	 * @return the accounting matrix detail selection
	 */
	public AccountingMatrixDetailTo getAccountingMatrixDetailSelection() {
		return accountingMatrixDetailSelection;
	}


	/**
	 * Sets the accounting matrix detail selection.
	 *
	 * @param accountingMatrixDetailSelection the new accounting matrix detail selection
	 */
	public void setAccountingMatrixDetailSelection(
			AccountingMatrixDetailTo accountingMatrixDetailSelection) {
		this.accountingMatrixDetailSelection = accountingMatrixDetailSelection;
	}


	/**
	 * Gets the accounting matrix detail to.
	 *
	 * @return the accounting matrix detail to
	 */
	public AccountingMatrixDetailTo getAccountingMatrixDetailTo() {
		return accountingMatrixDetailTo;
	}


	/**
	 * Sets the accounting matrix detail to.
	 *
	 * @param accountingMatrixDetailTo the new accounting matrix detail to
	 */
	public void setAccountingMatrixDetailTo(
			AccountingMatrixDetailTo accountingMatrixDetailTo) {
		this.accountingMatrixDetailTo = accountingMatrixDetailTo;
	}


	/**
	 * Gets the list accounting sources.
	 *
	 * @return the list accounting sources
	 */
	public List<AccountingSource> getListAccountingSources() {
		return listAccountingSources;
	}


	/**
	 * Sets the list accounting sources.
	 *
	 * @param listAccountingSources the new list accounting sources
	 */
	public void setListAccountingSources(
			List<AccountingSource> listAccountingSources) {
		this.listAccountingSources = listAccountingSources;
	}

	/**
	 * Gets the field source to data model.
	 *
	 * @return the field source to data model
	 */
	public GenericDataModel<FieldSourceTo> getFieldSourceToDataModel() {
		return fieldSourceToDataModel;
	}


	/**
	 * Sets the field source to data model.
	 *
	 * @param fieldSourceToDataModel the new field source to data model
	 */
	public void setFieldSourceToDataModel(
			GenericDataModel<FieldSourceTo> fieldSourceToDataModel) {
		this.fieldSourceToDataModel = fieldSourceToDataModel;
	}


	/**
	 * Gets the list accounting account to.
	 *
	 * @return the list accounting account to
	 */
	public List<AccountingAccountTo> getListAccountingAccountTo() {
		return listAccountingAccountTo;
	}


	/**
	 * Sets the list accounting account to.
	 *
	 * @param listAccountingAccountTo the new list accounting account to
	 */
	public void setListAccountingAccountTo(
			List<AccountingAccountTo> listAccountingAccountTo) {
		this.listAccountingAccountTo = listAccountingAccountTo;
	}


	/**
	 * Gets the accounting account data model.
	 *
	 * @return the accounting account data model
	 */
	public GenericDataModel<AccountingAccountTo> getAccountingAccountDataModel() {
		return accountingAccountDataModel;
	}


	/**
	 * Sets the accounting account data model.
	 *
	 * @param accountingAccountDataModel the new accounting account data model
	 */
	public void setAccountingAccountDataModel(
			GenericDataModel<AccountingAccountTo> accountingAccountDataModel) {
		this.accountingAccountDataModel = accountingAccountDataModel;
	}


	/**
	 * Gets the accounting matrix detail data model.
	 *
	 * @return the accounting matrix detail data model
	 */
	public GenericDataModel<AccountingMatrixDetailTo> getAccountingMatrixDetailDataModel() {
		return accountingMatrixDetailDataModel;
	}


	/**
	 * Sets the accounting matrix detail data model.
	 *
	 * @param accountingMatrixDetailDataModel the new accounting matrix detail data model
	 */
	public void setAccountingMatrixDetailDataModel(
			GenericDataModel<AccountingMatrixDetailTo> accountingMatrixDetailDataModel) {
		this.accountingMatrixDetailDataModel = accountingMatrixDetailDataModel;
	}


	/**
	 * Gets the string final formula.
	 *
	 * @return the string final formula
	 */
	public String getStringFinalFormula() {
		return stringFinalFormula;
	}


	/**
	 * Sets the string final formula.
	 *
	 * @param stringFinalFormula the new string final formula
	 */
	public void setStringFinalFormula(String stringFinalFormula) {
		this.stringFinalFormula = stringFinalFormula;
	}


	/**
	 * Gets the list variable.
	 *
	 * @return the list variable
	 */
	public List<AccountingParameterTO> getListVariable() {
		return listVariable;
	}


	/**
	 * Sets the list variable.
	 *
	 * @param listVariable the new list variable
	 */
	public void setListVariable(List<AccountingParameterTO> listVariable) {
		this.listVariable = listVariable;
	}


	/**
	 * Gets the list operator.
	 *
	 * @return the list operator
	 */
	public List<AccountingParameterTO> getListOperator() {
		return listOperator;
	}


	/**
	 * Sets the list operator.
	 *
	 * @param listOperator the new list operator
	 */
	public void setListOperator(List<AccountingParameterTO> listOperator) {
		this.listOperator = listOperator;
	}


	/**
	 * Gets the list variable tmp.
	 *
	 * @return the list variable tmp
	 */
	public List<AccountingParameterTO> getListVariableTmp() {
		return listVariableTmp;
	}


	/**
	 * Sets the list variable tmp.
	 *
	 * @param listVariableTmp the new list variable tmp
	 */
	public void setListVariableTmp(List<AccountingParameterTO> listVariableTmp) {
		this.listVariableTmp = listVariableTmp;
	}


	/**
	 * Gets the list operator tmp.
	 *
	 * @return the list operator tmp
	 */
	public List<AccountingParameterTO> getListOperatorTmp() {
		return listOperatorTmp;
	}


	/**
	 * Sets the list operator tmp.
	 *
	 * @param listOperatorTmp the new list operator tmp
	 */
	public void setListOperatorTmp(List<AccountingParameterTO> listOperatorTmp) {
		this.listOperatorTmp = listOperatorTmp;
	}


	/**
	 * Gets the formula final.
	 *
	 * @return the formula final
	 */
	public List<AccountingParameterTO> getFormulaFinal() {
		return formulaFinal;
	}


	/**
	 * Sets the formula final.
	 *
	 * @param formulaFinal the new formula final
	 */
	public void setFormulaFinal(List<AccountingParameterTO> formulaFinal) {
		this.formulaFinal = formulaFinal;
	}


	/**
	 * Gets the variable hidden name.
	 *
	 * @return the variable hidden name
	 */
	public String getVariableHiddenName() {
		return variableHiddenName;
	}


	/**
	 * Sets the variable hidden name.
	 *
	 * @param variableHiddenName the new variable hidden name
	 */
	public void setVariableHiddenName(String variableHiddenName) {
		this.variableHiddenName = variableHiddenName;
	}


	/**
	 * Gets the variable hidden code.
	 *
	 * @return the variable hidden code
	 */
	public String getVariableHiddenCode() {
		return variableHiddenCode;
	}


	/**
	 * Sets the variable hidden code.
	 *
	 * @param variableHiddenCode the new variable hidden code
	 */
	public void setVariableHiddenCode(String variableHiddenCode) {
		this.variableHiddenCode = variableHiddenCode;
	}


	/**
	 * Gets the variable hidden index.
	 *
	 * @return the variable hidden index
	 */
	public String getVariableHiddenIndex() {
		return variableHiddenIndex;
	}


	/**
	 * Sets the variable hidden index.
	 *
	 * @param variableHiddenIndex the new variable hidden index
	 */
	public void setVariableHiddenIndex(String variableHiddenIndex) {
		this.variableHiddenIndex = variableHiddenIndex;
	}


	/**
	 * Gets the auxiliary type selection to source.
	 *
	 * @return the auxiliary type selection to source
	 */
	public Integer getAuxiliaryTypeSelectionToSource() {
		return auxiliaryTypeSelectionToSource;
	}


	/**
	 * Sets the auxiliary type selection to source.
	 *
	 * @param auxiliaryTypeSelectionToSource the new auxiliary type selection to source
	 */
	public void setAuxiliaryTypeSelectionToSource(
			Integer auxiliaryTypeSelectionToSource) {
		this.auxiliaryTypeSelectionToSource = auxiliaryTypeSelectionToSource;
	}


	/**
	 * Gets the accounting source.
	 *
	 * @return the accounting source
	 */
	public AccountingSource getAccountingSource() {
		return accountingSource;
	}


	/**
	 * Sets the accounting source.
	 *
	 * @param accountingSource the new accounting source
	 */
	public void setAccountingSource(AccountingSource accountingSource) {
		this.accountingSource = accountingSource;
	}


	/**
	 * Gets the accounting source to.
	 *
	 * @return the accounting source to
	 */
	public AccountingSourceTo getAccountingSourceTo() {
		return accountingSourceTo;
	}


	/**
	 * Sets the accounting source to.
	 *
	 * @param accountingSourceTo the new accounting source to
	 */
	public void setAccountingSourceTo(AccountingSourceTo accountingSourceTo) {
		this.accountingSourceTo = accountingSourceTo;
	}


	/**
	 * Gets the schema name display.
	 *
	 * @return the schema name display
	 */
	public String getSchemaNameDisplay() {
		return schemaNameDisplay;
	}


	/**
	 * Sets the schema name display.
	 *
	 * @param schemaNameDisplay the new schema name display
	 */
	public void setSchemaNameDisplay(String schemaNameDisplay) {
		this.schemaNameDisplay = schemaNameDisplay;
	}


	/**
	 * Gets the accounting source info.
	 *
	 * @return the accounting source info
	 */
	public AccountingSourceInfo getAccountingSourceInfo() {
		return accountingSourceInfo;
	}


	/**
	 * Sets the accounting source info.
	 *
	 * @param accountingSourceInfo the new accounting source info
	 */
	public void setAccountingSourceInfo(AccountingSourceInfo accountingSourceInfo) {
		this.accountingSourceInfo = accountingSourceInfo;
	}


	/* (non-Javadoc)
	 * @see com.pradera.commons.view.GenericBaseBean#getfUploadFileSize()
	 */
	public String getfUploadFileSize() {
		return fUploadFileSize;
	}


	/* (non-Javadoc)
	 * @see com.pradera.commons.view.GenericBaseBean#setfUploadFileSize(java.lang.String)
	 */
	public void setfUploadFileSize(String fUploadFileSize) {
		this.fUploadFileSize = fUploadFileSize;
	}





	/**
	 * Gets the register source.
	 *
	 * @return the register source
	 */
	public Boolean getRegisterSource() {
		return registerSource;
	}


	/**
	 * Sets the register source.
	 *
	 * @param registerSource the new register source
	 */
	public void setRegisterSource(Boolean registerSource) {
		this.registerSource = registerSource;
	}


	/**
	 * Gets the list source status.
	 *
	 * @return the list source status
	 */
	public List<ParameterTable> getListSourceStatus() {
		return listSourceStatus;
	}


	/**
	 * Sets the list source status.
	 *
	 * @param listSourceStatus the new list source status
	 */
	public void setListSourceStatus(List<ParameterTable> listSourceStatus) {
		this.listSourceStatus = listSourceStatus;
	}


	/**
	 * Gets the auxiliary type selected.
	 *
	 * @return the auxiliary type selected
	 */
	public Integer getAuxiliaryTypeSelected() {
		return auxiliaryTypeSelected;
	}


	/**
	 * Sets the auxiliary type selected.
	 *
	 * @param auxiliaryTypeSelected the new auxiliary type selected
	 */
	public void setAuxiliaryTypeSelected(Integer auxiliaryTypeSelected) {
		this.auxiliaryTypeSelected = auxiliaryTypeSelected;
	}
 
	 
	
	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons(){        

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);    
		}
		if(userPrivilege.getUserAcctions().isDelete()){
			privilegeComponent.setBtnDeleteView(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent); 

	}

	
	
	
	
	
	
}