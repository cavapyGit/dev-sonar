package com.pradera.accounting.source.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounting.common.utils.SequenceGeneratorCode;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.schema.service.SchemaAccountingServiceBean;
import com.pradera.accounting.source.service.SourceAccountingServiceBean;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounting.account.to.AccountingSourceTo;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingMatrixDetail;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.AccountingSource;
import com.pradera.model.accounting.type.AccountingSchemaStateType;
import com.pradera.model.accounting.type.AccountingSourceStateType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SourceAccountingServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SourceAccountingServiceFacade { 

	/** The user info. */
	@Inject
	UserInfo userInfo;	
	
	/** The schema accounting service bean. */
	@EJB
	SchemaAccountingServiceBean schemaAccountingServiceBean;
		
	/** The source accounting service bean. */
	@EJB
	SourceAccountingServiceBean sourceAccountingServiceBean;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade accountingMatrixServiceFacade;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sequence generator code. */
	@EJB
	SequenceGeneratorCode  				sequenceGeneratorCode;
	
 
	/**
	 * Save accounting source.
	 *
	 * @param accountingSource the accounting source
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void saveAccountingSource(AccountingSource accountingSource){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		sourceAccountingServiceBean.saveAccountingSource(accountingSource);
	}
	
	
	/**
	 * Load accounting source.
	 *
	 * @param accountingTo the accounting to
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingSource> loadAccountingSource(AccountingSourceTo accountingTo){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingSource> listAccountingSource = new ArrayList<AccountingSource>();
		
		listAccountingSource = sourceAccountingServiceBean.searchAllAccountingSource(accountingTo); 
		
		return listAccountingSource;
	}
	
	
	
   /**
    * Gets the accounting source content.
    *
    * @param idAccountingSourcePk the id accounting source pk
    * @return the accounting source content
    */
   public byte[] getAccountingSourceContent(Long idAccountingSourcePk) {
	   
	   Map<String, Object> parameters = new HashMap<String, Object>();
	   parameters.put("idAccountingSourcePk", idAccountingSourcePk);
	   return (byte[])sourceAccountingServiceBean.findObjectByNamedQuery(AccountingSource.ACCOUNTING_SOURCE_CONTENT_BY_PK, parameters);
   }	   
   
   
   
	/**
	 * Update accounting source.
	 *
	 * @param accountingSourceUpdate the accounting source update
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateAccountingSource(AccountingSource accountingSourceUpdate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		AccountingSource accountingSource = sourceAccountingServiceBean.find(AccountingSource.class, accountingSourceUpdate.getIdAccountingSourcePk());
		
		if( Validations.validateIsNotNullAndNotEmpty(accountingSourceUpdate.getStatus()) && 
				!(accountingSourceUpdate.getStatus().equals(accountingSource.getStatus())) ) {
			
				accountingSource.setStatus(AccountingSourceStateType.DELETED.getCode());
				//Set Audit Process
				accountingSource.setAudit(loggerUser);
				// Disassociate source from the Matrix Detail
				List<AccountingMatrixDetail> listAccountingMatrixDetail = accountingMatrixServiceFacade.findAccountingMatrixDetail(accountingSource.getIdAccountingSourcePk());
				
				if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetail)){
					for (AccountingMatrixDetail accountingMatrixDetail : listAccountingMatrixDetail) {
//						accountingMatrixDetail.setAccountingSource(null);
						accountingMatrixDetail.setAudit(loggerUser);
						sourceAccountingServiceBean.update(accountingMatrixDetail);
						// Change the status on Schema
						Long idAccountingSchema = accountingMatrixDetail.getAccountingMatrix().getAccountingSchema().getIdAccountingSchemaPk();
						AccountingSchema accountingSchema = schemaAccountingServiceBean.find(AccountingSchema.class,idAccountingSchema);
						accountingSchema.setStatus(AccountingSchemaStateType.WITH_OUT_SOURCE.getCode());
						accountingSchema.setAudit(loggerUser);
						schemaAccountingServiceBean.update(accountingSchema);
					}
					
					
					
				}
		}else {
		
				accountingSource.setDescriptionSource(accountingSourceUpdate.getDescriptionSource());
				accountingSource.setFileSourceName(accountingSourceUpdate.getFileSourceName());
				accountingSource.setAuxiliaryType(accountingSourceUpdate.getAuxiliaryType());
				//Set Audit Process
				accountingSource.setAudit(loggerUser);
				if (Validations.validateIsNotNull(accountingSourceUpdate.getFileXml())){
					accountingSource.setFileXml(accountingSourceUpdate.getFileXml());
				}
		}
	
	}
	
       	 
}
