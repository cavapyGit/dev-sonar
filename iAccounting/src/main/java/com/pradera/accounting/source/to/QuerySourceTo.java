package com.pradera.accounting.source.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class QuerySourceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class QuerySourceTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/**  Query that will be  executed *. */
	private  String 	query;
	
	/** If exists Associate. */
	private Boolean associate;
	
	/** The type Associate, as a parameter sent. */
	private String  typeAssociate;
	
	/**  Indicator if query should setting  dynamics date *. */
	private Boolean  date = Boolean.TRUE;

	
	/**  Indicator if query should setting  dynamics currency  *. */
	private Boolean  isCurrency;
	
	/**  Indicator if query should setting  dynamics Instrument Type *. */
	private Boolean  isInstrument;
	
	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Sets the query.
	 *
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Boolean getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the date to set
	 */
	public void setDate(Boolean date) {
		this.date = date;
	}

	/**
	 * Gets the associate.
	 *
	 * @return the associate
	 */
	public Boolean getAssociate() {
		return associate;
	}

	/**
	 * Sets the associate.
	 *
	 * @param associate the associate to set
	 */
	public void setAssociate(Boolean associate) {
		this.associate = associate;
	}

	/**
	 * Gets the type associate.
	 *
	 * @return the typeAssociate
	 */
	public String getTypeAssociate() {
		return typeAssociate;
	}

	/**
	 * Sets the type associate.
	 *
	 * @param typeAssociate the typeAssociate to set
	 */
	public void setTypeAssociate(String typeAssociate) {
		this.typeAssociate = typeAssociate;
	}

	/**
	 * Gets the checks if is currency.
	 *
	 * @return the isCurrency
	 */
	public Boolean getIsCurrency() {
		return isCurrency;
	}

	/**
	 * Sets the checks if is currency.
	 *
	 * @param isCurrency the isCurrency to set
	 */
	public void setIsCurrency(Boolean isCurrency) {
		this.isCurrency = isCurrency;
	}

	/**
	 * Gets the checks if is instrument.
	 *
	 * @return the isInstrument
	 */
	public Boolean getIsInstrument() {
		return isInstrument;
	}

	/**
	 * Sets the checks if is instrument.
	 *
	 * @param isInstrument the isInstrument to set
	 */
	public void setIsInstrument(Boolean isInstrument) {
		this.isInstrument = isInstrument;
	}
	
	
	
	
	
	
}
