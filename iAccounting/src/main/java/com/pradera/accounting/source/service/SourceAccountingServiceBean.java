package com.pradera.accounting.source.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounting.account.to.AccountingSourceTo;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingSource;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SourceAccountingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SourceAccountingServiceBean extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	

	 
	/**
	 * Save accounting source.
	 *
	 * @param accountingSource the accounting source
	 */
	public void saveAccountingSource(Object accountingSource){
		create(accountingSource);
	}
	
	
	/**
	 * Search all accounting source.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingSource> searchAllAccountingSource(AccountingSourceTo filter){
		
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT ACS.ID_ACCOUNTING_SOURCE_PK, ACS.FILE_SOURCE_NAME, ACS.DESCRIPTION_SOURCE , ACS.STATUS ,   ");
		stringBuilderSql.append(" ( select pt.description from parameter_table pt where pt.parameter_table_pk = ACS.STATUS ) as DESCRIPTION_STATUS  ,  "); //4
		stringBuilderSql.append(" ACS.LAST_MODIFY_DATE ,  ");//5
		stringBuilderSql.append(" ( SELECT  AP.PARAMETER_NAME|| '-' ||AP.DESCRIPTION  FROM ACCOUNTING_PARAMETER AP WHERE ACS.AUXILIARY_TYPE=AP.ID_ACCOUNTING_PARAMETER_PK ) as DESCRIPTION_AUXILIARY_TYPE ,    ");//6
		stringBuilderSql.append(" ACS.AUXILIARY_TYPE  ");//7
		stringBuilderSql.append(" FROM   ACCOUNTING_SOURCE ACS  WHERE 1=1  ");
		
		if ( Validations.validateIsNotNullAndPositive(filter.getIdAccountingSourcePk()) ){
			stringBuilderSql.append(" AND  ACS.ID_ACCOUNTING_SOURCE_PK = :idAccountingSourcePk    ");
		}
		if ( Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType()) ){
			stringBuilderSql.append(" AND  AUXILIARY_TYPE = :auxiliaryType    ");
		}
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			stringBuilderSql.append(" AND  STATUS = :status    ");
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getFileSourceName()) ){
			stringBuilderSql.append(" AND  FILE_SOURCE_NAME = :fileSourceName    ");
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getDescriptionSource()) ){
			stringBuilderSql.append(" AND  DESCRIPTION_SOURCE like :descriptionSource    ");
		}
		
		stringBuilderSql.append("ORDER BY  DESCRIPTION_AUXILIARY_TYPE , DESCRIPTION_STATUS DESC  ");
		
		Query query = em.createNativeQuery(stringBuilderSql.toString());
		
		if ( Validations.validateIsNotNullAndPositive(filter.getIdAccountingSourcePk()) ){
			query.setParameter("idAccountingSourcePk", filter.getIdAccountingSourcePk());
		}
		if ( Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType()) ){
			query.setParameter("auxiliaryType", filter.getAuxiliaryType()	);
		}
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			query.setParameter("status",  filter.getStatus() );
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getFileSourceName()) ){
			query.setParameter("fileSourceName", filter.getFileSourceName() );
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getDescriptionSource()) ){
			query.setParameter("descriptionSource", '%'+filter.getDescriptionSource()+'%');
		}
		
		
		
		List<Object[]> objectList = query.getResultList();
		
		List<AccountingSource> listAccountingSource = new ArrayList<AccountingSource>();
		
		for (int i=0; i<objectList.size();++i) {
			Object[] sResults = (Object[])objectList.get(i);
			AccountingSource accountingSource = new AccountingSource();
			
			accountingSource.setIdAccountingSourcePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));
			accountingSource.setFileSourceName(sResults[1]==null?"":sResults[1].toString());
			accountingSource.setDescriptionSource(sResults[2]==null?"":sResults[2].toString());
			accountingSource.setStatus(Integer.parseInt(sResults[3].toString())); 
			accountingSource.setDescriptionSourceStatus(sResults[4]==null?"":sResults[4].toString());
			accountingSource.setLastModifyDate((Date)sResults[5]==null?null:(Date)sResults[5]);
			accountingSource.setDescriptionAuxiliaryType(sResults[6]==null?"":sResults[6].toString());
			accountingSource.setAuxiliaryType(sResults[6]==null?null:Integer.parseInt(sResults[7].toString())); 
			
			
			listAccountingSource.add(accountingSource);
		}
		
		
		return listAccountingSource;
	}  
	
	
	
 

}
