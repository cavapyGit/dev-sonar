package com.pradera.accounting.documentSender.facade;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.documentSender.service.DocumentSenderInterfaceService;
import com.pradera.accounting.documentSender.to.SendingInterfaceTo;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.ClientSoapService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.to.DetailTo;
import com.pradera.commons.to.RegistryTO;
import com.pradera.commons.to.TransactionTO;
import com.pradera.commons.to.VoucherTO;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.auditprocess.type.StateSapResponseType;
import com.pradera.model.billing.SendInterfaceXmlSap;
import com.pradera.model.generalparameter.MasterTable;
import com.pradera.model.generalparameter.externalinterface.ExtensionFileType;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceExecutionType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.ArrayOfEIRESAD;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.EIRESAD;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.EIRESAS;
import com.edv.org.tempuri.ServAsientoResponse;

@Stateless
@Performance
public class DocumentSenderInterfaceFacade {

	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	@Inject
	private DocumentSenderInterfaceService documentSenderInterfaceService;

	@Inject
	private ClientSoapService clientSoapService;

	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;


	
	/**
	 * Metodo que parsea el formato anterior del envio de asientos contables al
	 * nuevo formato establecido por SAP
	 * 
	 * @param idAccountingProcessPk
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public TransactionTO getOBjectAccounting(Long idAccountingProcessPk) throws ParserConfigurationException, SAXException, IOException {
		byte[] xmlData = documentSenderInterfaceService.getAccoutingProcessFileContent(idAccountingProcessPk);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document documentXml = (Document) builder.parse(new ByteArrayInputStream(xmlData));

		Element root = documentXml.getDocumentElement();
		System.out.println("TAG RAIZ:" + root.getNodeName());
		TransactionTO transaction = new TransactionTO();
		VoucherTO voucherTO = null;
		NodeList listVoucherTo = documentXml.getElementsByTagName("COMPROBANTE");

		List<VoucherTO> listVoucherTOs = new ArrayList<>();
		for (int i = 0; i < listVoucherTo.getLength(); i++) {
			Node node = listVoucherTo.item(i);
			Element element = (Element) node;
			voucherTO = new VoucherTO();

			Integer numVoucher = Integer.parseInt(element.getElementsByTagName("NUMCBTE").item(0).getTextContent());
			String gloss = element.getElementsByTagName("GLOSA").item(0).getTextContent();
			BigDecimal exchangeRate = new BigDecimal(element.getElementsByTagName("TIPOCAMBIOCAB").item(0).getTextContent());
			Date date = CommonsUtilities.convertStringtoDate(element.getElementsByTagName("FECHA").item(0).getTextContent(), "yyyyMMdd");
			String type = element.getElementsByTagName("TIPO").item(0).getTextContent();

			voucherTO.setNumVoucher(numVoucher);
			voucherTO.setGloss(gloss);
			voucherTO.setExchangeRate(exchangeRate);
			voucherTO.setDate(date);
			voucherTO.setType(type);

			// System.out.println("************************COMPROBANTE**************************************");
			// System.out.println("NUMCBTE:" + voucherTO.getNumVoucher());
			// System.out.println("GLOSA:" + voucherTO.getGloss());
			// System.out.println("TIPOCAMBIOCAB:" +
			// voucherTO.getExchangeRate().toString());
			// System.out.println("FECHA:" +
			// CommonsUtilities.convertDatetoString(date));
			// System.out.println("TIPO:" + voucherTO.getType());

			NodeList listNodeDetail = element.getElementsByTagName("DETALLE");
			Element elementDetail = (Element) listNodeDetail.item(0);
			// System.out.println("--- DETALLE");
			NodeList listRegistry = elementDetail.getElementsByTagName("REGISTRO");
			RegistryTO registryTO = null;
			List<RegistryTO> listRegistryTOs = new ArrayList<>();

			for (int j = 0; j < listRegistry.getLength(); j++) {
				Element elementRegistry = (Element) listRegistry.item(j);
				registryTO = new RegistryTO();

				Integer regNumber = Integer.parseInt(elementRegistry.getElementsByTagName("NUMREG").item(0).getTextContent());
				String account = elementRegistry.getElementsByTagName("CUENTA").item(0).getTextContent();
				BigDecimal amount = new BigDecimal(elementRegistry.getElementsByTagName("MONTO").item(0).getTextContent());
				String operation = elementRegistry.getElementsByTagName("OPERACION").item(0).getTextContent();
				String particularGloss = elementRegistry.getElementsByTagName("GLOSAPARTICULAR").item(0).getTextContent();

				registryTO.setNumReg(regNumber);
				registryTO.setAccount(account);
				registryTO.setAmount(amount);
				registryTO.setOperation(operation);
				registryTO.setGloss(particularGloss);

				listRegistryTOs.add(registryTO);

				// System.out.println("----- REGISTRO");
				// System.out.println("---------- NUMREG: " +
				// registryTO.getNumReg());
				// System.out.println("---------- CUENTA: " +
				// registryTO.getAccount());
				// System.out.println("---------- MONTO: " +
				// registryTO.getAmount());
				// System.out.println("---------- OPERACION: " +
				// registryTO.getOperation());
				// System.out.println("---------- GLOSA PARTICULAR: " +
				// registryTO.getGloss());
			}

			DetailTo detailTo = new DetailTo();
			detailTo.setLisRegistryTOs(listRegistryTOs);
			voucherTO.setDetailTo(detailTo);
			listVoucherTOs.add(voucherTO);
		}
		transaction.setListVoucherTOs(listVoucherTOs);
		return transaction;
	}

	public SendingInterfaceTo getSendingInterface(Integer auxiliaryTypeSelected, Date cutDate) throws Exception {

		List<AccountingProcess> accountingReceiptList = documentSenderInterfaceService.searchAccountingProcess(auxiliaryTypeSelected, cutDate);

		if (!accountingReceiptList.isEmpty()) {
			
			AccountingProcess  accountingProcess = documentSenderInterfaceService.find(AccountingProcess.class,accountingReceiptList.get(0).getIdAccountingProcessPk());
			
			//Esto se usa para convertir los archivos con el formato antiguo al nuevo
			//con la modificacion de contabilidad mensual, ya no es necesario porque ya se genera con el nuevo formato
			//TransactionTO transactionTO = getOBjectAccounting(accountingReceiptList.get(0).getIdAccountingProcessPk());
			//SOAPMessage soapMessageSend = createSoapEnvelope(transactionTO);

			SendingInterfaceTo sendingInterfaceTo = new SendingInterfaceTo();
			SOAPMessage soapMessageSend = getSOAPMessageByByteArray(accountingProcess.getFileSap());

//			soapMessageSendAux = soapMessageSend;
			
			sendingInterfaceTo.setIdAccountingProcessPk(accountingReceiptList.get(0).getIdAccountingProcessPk());
			sendingInterfaceTo.setTotalRecords(1);
			sendingInterfaceTo.setFileXmlSent(getByteArrayBySOAPMessage(soapMessageSend));
			sendingInterfaceTo.setFileNameXmlSent("SAP" + CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(), "yyyyMMddHHmm") + ".xml");

			return sendingInterfaceTo;
		} else
			return null;
	}

	private SOAPMessage createSoapEnvelope(TransactionTO transactionTO) throws SOAPException {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String myNamespace = "tem";
		String myNamespaceURI = "http://tempuri.org/";

		String myNameSpaceChild = "ied";
		String myNamespaceURIchild = "http://schemas.datacontract.org/2004/07/IEDVServicios";
		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
		envelope.addNamespaceDeclaration(myNameSpaceChild, myNamespaceURIchild);

		SOAPBody soapBody = envelope.getBody();
		SOAPElement servAsiento = soapBody.addChildElement("ServAsiento", myNamespace);
		SOAPElement asientoC = servAsiento.addChildElement("asientoC", myNamespace);
		SOAPElement AsientoC, Detalle, AsientoD;
		
		//obteniendo el tokken
		StringBuilder sql = new StringBuilder();
		sql.append(" select pt.description from ParameterTable pt where pt.masterTable.masterTablePk = "+MasterTableType.TOKKEN_CODE_SAP.getCode());
		@SuppressWarnings("unchecked")
		List<String> listDesc = parameterServiceBean.findListByQueryString(sql.toString());
		String tokken = listDesc.get(0);

		for (VoucherTO voucherTO : transactionTO.getListVoucherTOs()) {
			try {
				AsientoC = asientoC.addChildElement("AsientoC", myNameSpaceChild);
				Detalle = AsientoC.addChildElement("Detalle", myNameSpaceChild);
				for (RegistryTO registryTO : voucherTO.getDetailTo().getLisRegistryTOs()) {
					AsientoD = Detalle.addChildElement("AsientoD", myNameSpaceChild);
					AsientoD.addChildElement("CUENTA", myNameSpaceChild).addTextNode(registryTO.getAccount());
					AsientoD.addChildElement("GLOSAPARTICULAR", myNameSpaceChild).addTextNode(registryTO.getGloss());
					AsientoD.addChildElement("MONTO", myNameSpaceChild).addTextNode(registryTO.getAmount() + "");
					AsientoD.addChildElement("NUMREG", myNameSpaceChild).addTextNode(registryTO.getNumReg() + "");
					AsientoD.addChildElement("OPERACION", myNameSpaceChild).addTextNode(registryTO.getOperation());
				}
				AsientoC.addChildElement("FECHA", myNameSpaceChild).addTextNode(CommonsUtilities.convertDateToString(voucherTO.getDate(), "yyyyMMdd"));
				AsientoC.addChildElement("GLOSA", myNameSpaceChild).addTextNode(voucherTO.getGloss());
				AsientoC.addChildElement("NUMCBTE", myNameSpaceChild).addTextNode(voucherTO.getNumVoucher() + "");
				AsientoC.addChildElement("TIPO", myNameSpaceChild).addTextNode(voucherTO.getType());
				AsientoC.addChildElement("TIPOCAMBIOCAB", myNameSpaceChild).addTextNode(voucherTO.getExchangeRate() + "");
				AsientoC.addChildElement("TOKKEN", myNameSpaceChild).addTextNode(tokken + "");
			} catch (SOAPException e) {
				e.printStackTrace();
			}
		}
		return soapMessage;
	}

	/**
	 * 
	 * @param sendingInterfaceTo
	 * @throws IOException 
	 */
	public void sendToSAP(SendingInterfaceTo sendingInterfaceTo){

		SOAPMessage soapMessageReceived = null;

		try {
			//Obteniendo el Endpoint
			StringBuilder sql = new StringBuilder();
			sql.append(" select pt.description from ParameterTable pt where pt.masterTable.masterTablePk = " + MasterTableType.END_POINT_SAP.getCode());
			@SuppressWarnings("unchecked")
			List<String> listDesc = parameterServiceBean.findListByQueryString(sql.toString());
			String endPoint = listDesc.get(0);
			
			String strMsg = new String(sendingInterfaceTo.getFileXmlSent());
			System.out.println("MENSAJE QUE SE ENVIA DESDE LA PANTALLA:" + strMsg);
			soapMessageReceived = clientSoapService.sendSapClient(sendingInterfaceTo.getFileXmlSent(),endPoint);
			sendingInterfaceTo.setFileXmlReceived(getByteArrayBySOAPMessage(soapMessageReceived));
			ServAsientoResponse object;

			try {
				JAXBContext jc;
				jc = JAXBContext.newInstance(ServAsientoResponse.class);
				Unmarshaller um = jc.createUnmarshaller();
				object = (ServAsientoResponse) um.unmarshal(soapMessageReceived.getSOAPBody().extractContentAsDocument());

				JAXBElement<EIRESAS> jaxbElements = object.getServAsientoResult();
				EIRESAS eiresas = jaxbElements.getValue();
				JAXBElement<ArrayOfEIRESAD> jaxbElement2 = eiresas.getDetalle();
				ArrayOfEIRESAD arrayOfEIRESAD = jaxbElement2.getValue();
				
				int acceptedRecords = 0;
				int rejectRecords = 0;
				int duplicateRecords = 0;
				String mensaje = "";
				
				if(arrayOfEIRESAD != null){
					List<EIRESAD> listFinal = arrayOfEIRESAD.getEIRESAD();
					for (EIRESAD eiresad : listFinal) {
						JAXBElement<String> element = eiresad.getEstado();
						System.out.println("ESTADO:" + element.getValue());
						
						if(element.getValue().equalsIgnoreCase(StateSapResponseType.CORRECT.getValue())){
							acceptedRecords ++;
							mensaje += "NUMCBTE " + eiresad.getNumcbte() + ": " + StateSapResponseType.CORRECT.getValue() + "#";
						}
						if(element.getValue().equalsIgnoreCase(StateSapResponseType.DUPLICATE.getValue())){
							duplicateRecords ++;
							mensaje += "NUMCBTE " + eiresad.getNumcbte() + ": " + StateSapResponseType.DUPLICATE.getValue() + "#";
						}
						if(element.getValue().equalsIgnoreCase(StateSapResponseType.ERROR.getValue())){
							rejectRecords ++;
							mensaje += "NUMCBTE " + eiresad.getNumcbte() + ": " + StateSapResponseType.ERROR.getValue() + "#";
						}
					}
				}else{
					rejectRecords = sendingInterfaceTo.getTotalRecords();
					mensaje = eiresas.getMsgrespuesta().getValue();
				}
				
				sendingInterfaceTo.setAcceptedRecords(acceptedRecords);
				sendingInterfaceTo.setDuplicateRecords(duplicateRecords);
				sendingInterfaceTo.setRejectRecords(rejectRecords);
				sendingInterfaceTo.setResponseValue(mensaje);

			} catch (JAXBException e) {
				e.printStackTrace();
			}

			// Creando el objeto a guardar


			// Guardando

			ExternalInterface externalInterface = parameterServiceBean.getExternalInterface(ComponentConstant.SENDING_ACCOUNTING_INTERFACE, false);
			LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);

			if (loggerUser == null || loggerUser.getIdPrivilegeOfSystem() == null) {
				if (loggerUser == null) {
					loggerUser = new LoggerUser();
					loggerUser.setUserAction(new UserAcctions());
				}
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			}

			InterfaceProcess interfaceProcess = new InterfaceProcess();
			interfaceProcess.setProcessDate(CommonsUtilities.currentDate());
			interfaceProcess.setIdExternalInterfaceFk(externalInterface);
			interfaceProcess.setAudit(loggerUser);
			interfaceProcess.setFileName(sendingInterfaceTo.getFileNameXmlSent());
			interfaceProcess.setIdExtensionFileTypeFk(new ExtensionFileType(5L));// XML new ExtensionFileType(InterfaceExtensionFileType.XML.getCode().longValue())
			interfaceProcess.setRegistryDate(CommonsUtilities.currentDate());
			interfaceProcess.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			interfaceProcess.setProcessType(ExternalInterfaceExecutionType.MANUAL.getCode());
			interfaceProcess.setFileUpload(sendingInterfaceTo.getFileXmlSent());
			interfaceProcess.setFileResponse(sendingInterfaceTo.getFileXmlReceived());
			interfaceProcess.setProcessState(ExternalInterfaceReceptionStateType.CORRECT.getCode());
			interfaceProcess.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			interfaceProcess.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			interfaceProcess.setLastModifyDate(CommonsUtilities.currentDate());
			interfaceProcess.setLastModifyApp(userInfo.getLastModifyPriv());
			documentSenderInterfaceService.createWithFlush(interfaceProcess);
			
			InterfaceTransaction interfaceTransaction = new InterfaceTransaction();
			interfaceTransaction.setInterfaceProcess(interfaceProcess);
			interfaceTransaction.setTransactionState(BooleanType.YES.getCode());
			// interfaceTransaction.setParticipant(new Participant("EDB"));
			interfaceTransaction.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			interfaceTransaction.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			interfaceTransaction.setLastModifyDate(CommonsUtilities.currentDate());
			interfaceTransaction.setLastModifyApp(userInfo.getLastModifyPriv());
			documentSenderInterfaceService.createWithFlush(interfaceTransaction);

			SendInterfaceXmlSap interfaceXmlSap = new SendInterfaceXmlSap();
			interfaceXmlSap.setIdAccountingProcessFk(sendingInterfaceTo.getIdAccountingProcessPk());
			interfaceXmlSap.setIdInterfacesTransactionFk(interfaceTransaction);
			interfaceXmlSap.setResponseValue(sendingInterfaceTo.getResponseValue());
			interfaceXmlSap.setAcceptedRecords(sendingInterfaceTo.getAcceptedRecords());
			interfaceXmlSap.setRejectRecords(sendingInterfaceTo.getRejectRecords()+sendingInterfaceTo.getDuplicateRecords());
			interfaceXmlSap.setTotalRecords(sendingInterfaceTo.getTotalRecords());
			interfaceXmlSap.setState(StateSapResponseType.CORRECT.getCode());
			interfaceXmlSap.setRegistryDate(CommonsUtilities.currentDate());
			interfaceXmlSap.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			interfaceXmlSap.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			interfaceXmlSap.setLastModifyDate(CommonsUtilities.currentDate());
			interfaceXmlSap.setLastModifyApp(userInfo.getLastModifyPriv());
			documentSenderInterfaceService.createWithFlush(interfaceXmlSap);

//			return interfaceXmlSap;

		} catch (SOAPException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}
	
	/**
	 * Search Send Interface SAP
	 * @param searchAuxiliaryTypeSelected
	 * @param searchInitialDate
	 * @param searchFinalDate
	 * @return
	 */
	public List<SendInterfaceXmlSap> searchSendInterfaceXmlSap(Integer searchAuxiliaryTypeSelected, Date searchInitialDate, Date searchFinalDate){
		
		return documentSenderInterfaceService.findSendInterfaceXmlSap(searchAuxiliaryTypeSelected, searchInitialDate, searchFinalDate);
	}

	public byte[] getByteArrayBySOAPMessage(SOAPMessage soapMessage) throws SOAPException, IOException {
		byte[] byteArray;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		soapMessage.writeTo(out);
		byteArray = out.toByteArray();

		String reconstitutedString = new String(byteArray);
		byteArray = reconstitutedString.getBytes();

		return byteArray;
	}

	public SOAPMessage getSOAPMessageByByteArray(byte[] byteArray) throws IOException, SOAPException {
		SOAPMessage soapMessage;
		InputStream is = new ByteArrayInputStream(byteArray);
		soapMessage = MessageFactory.newInstance().createMessage(null, is);

		return soapMessage;
	}
	
	/**
	 * Crear archivo de SAP
	 * 1) Leer query de voucher
	 * 2) Insertarlo en la estructura de envio
	 * 3) Guardar en AccountingProcess
	 * @param accountingProcessTo
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void createFileToSap(AccountingProcessTo accountingProcessTo)  {
		
		try {
			byte[] file=fillObjectToSending(accountingProcessTo);
			AccountingProcess accountingProcess = documentSenderInterfaceService.find(AccountingProcess.class, accountingProcessTo.getIdAccountingProcessPk());
			accountingProcess.setLastModifyDate(CommonsUtilities.currentDate());
			
			accountingProcess.setFileSap(file);
			documentSenderInterfaceService.update(accountingProcess);
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (SOAPException | IOException e) {
			e.printStackTrace();
		}

	}
	
	
	public byte[] fillObjectToSending(AccountingProcessTo accountingProcessTo) throws SOAPException, IOException {
		List<Object[]> listObject=documentSenderInterfaceService.findAccountingVouchersMonthly(accountingProcessTo);
		if(Validations.validateListIsNullOrEmpty(listObject)) {
			return null;
		}
		List<VoucherTO> listVoucher=fillVoucherByObject(listObject);
		TransactionTO transactionTO=new TransactionTO();
		transactionTO.setListVoucherTOs(listVoucher);
		SOAPMessage	soapMessageSend = createSoapEnvelope(transactionTO);
		return getByteArrayBySOAPMessage(soapMessageSend);

	}
	
	public List<VoucherTO> fillVoucherByObject(List<Object[]> listObject){

		
		/** The index tipocambiocab. */
		int INDEX_TIPOCAMBIOCAB=7;//
		
		/** The index fecha. */
		int INDEX_FECHA=6;//
		
		/** The index tipo. */
		int INDEX_TIPO=9;

		
		/** The index cuenta. */
		int INDEX_CUENTA=1;//
		
		/** The index monto. */
		int INDEX_MONTO=2;//
		
		/** The index operacion. */
		int INDEX_OPERACION=3;//
		
		/** The index glosaparticular. */
		int INDEX_GLOSAPARTICULAR=8;//
	  
		List<VoucherTO> listAccountingVoucherTo=new ArrayList<VoucherTO>();
		VoucherTO accountingVoucherTo=new VoucherTO();
		List<RegistryTO> listAccountingVoucherRegistryTo=new ArrayList<>();
		RegistryTO accountingVoucherRegistryTo=null;
		int i=0;
		for (Object[] objeto : listObject) {
			if(i==0){//El primero
				accountingVoucherTo=new VoucherTO();
				accountingVoucherTo.setExchangeRate(new BigDecimal(objeto[INDEX_TIPOCAMBIOCAB].toString()));
				accountingVoucherTo.setDate((Date)objeto[INDEX_FECHA]);
				accountingVoucherTo.setGloss(objeto[INDEX_GLOSAPARTICULAR].toString());
				accountingVoucherTo.setType(objeto[INDEX_TIPO].toString());
				accountingVoucherTo.setNumVoucher(i+1);

				listAccountingVoucherRegistryTo=new ArrayList<>();
				accountingVoucherTo.setDetailTo(new DetailTo(listAccountingVoucherRegistryTo));
				listAccountingVoucherTo.add(accountingVoucherTo);
				i++;
			}
			/**
			    BASE.TIPO, [0] BASE.CUENTA,[1] SUM(BASE.MONTO),[2] 
				BASE.OPERACION,[3] BASE.INSTRUMENT_TYPE,[4] 
				BASE.CURRENCY_TYPE [5], BASE.EXECUTION_DATE [6]
			 */
			//Reemplazar saldo total 
			accountingVoucherRegistryTo=new RegistryTO();
			accountingVoucherRegistryTo.setAccount(objeto[INDEX_CUENTA].toString());
			accountingVoucherRegistryTo.setAmount(new BigDecimal(objeto[INDEX_MONTO].toString()));
			accountingVoucherRegistryTo.setGloss(objeto[INDEX_GLOSAPARTICULAR].toString());
			accountingVoucherRegistryTo.setOperation(objeto[INDEX_OPERACION].toString());
			accountingVoucherRegistryTo.setNumReg(i);
			i++;
			listAccountingVoucherRegistryTo.add(accountingVoucherRegistryTo);
		}
		
		
		return listAccountingVoucherTo;
		
	}
}
