package com.pradera.accounting.documentSender.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.primefaces.model.StreamedContent;

import com.pradera.accounting.documentSender.facade.DocumentSenderInterfaceFacade;
import com.pradera.accounting.documentSender.to.SendingInterfaceTo;
import com.pradera.accounting.notification.facade.AccountingNotificationServiceFacade;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.billing.SendInterfaceXmlSap;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;

/**
 * 
 * @author jquino
 * 
 */
@DepositaryWebBean
@LoggerCreateBean
public class SendingAccountingMgmtBean extends GenericBaseBean {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5222378272513878921L;

	/** The user info. */
	@Inject
	UserInfo userInfo;

	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	/** The log. */
	@Inject
	transient PraderaLogger log;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The parameter accounting service facade. */
	@EJB
	ParameterAccountingServiceFacade parameterAccountingServiceFacade;

	/** The accounting notification service facade. */
	@EJB
	AccountingNotificationServiceFacade accountingNotificationServiceFacade;

	/** The document sender interface facade */
	@EJB
	DocumentSenderInterfaceFacade documentSenderInterfaceFacade;
	
	/** The parameter accounting service facade. */
	@EJB
	ParameterServiceBean parameterServiceBean;

	// /** The accounting parameter to. */
	// private AccountingParameterTO accountingParameterTo = new
	// AccountingParameterTO();
	//
	// /** The parameter auxiliary type selected. */
	// // TIPO DIARIO AUXILIAR
	// private Integer parameterAuxiliaryTypeSelected;
	//
	// /** The list accounting process. */
	// private List<AccountingProcess> listAccountingProcess;
	//
	// private List<AccountingDataMonitorTo> listAccountingDataMonitorTos;
	//
	// private List<AccountingDataMonitorTo> listAccountingDataMonitorToAux;
	//
	// /** The accounting process data model. */
	// private GenericDataModel<AccountingProcess> accountingProcessDataModel;
	//
	// private GenericDataModel<AccountingDataMonitorTo> accountingDataModel;
	//
	// private GenericDataModel<AccountingDataMonitorTo>
	// accountingDataModifyModel;
	//
	// private AccountingProcess accountingProcessSelection;
	//
	// private AccountingDataMonitorTo accountingDataMonitorTo;
	//
	// /** The execution date. */
	// private Date executionDate;
	//
	// private Date accountingDate;
	//
	// private String idCustodyOperation;
	//
	// private String idNegotiationOperation;
	//
	// private String idCorporativeOperation;
	//
	// private String idSecurityCodePk;

	/** SEARCH */

	/** Aux Daily Filter */
	private Integer searchAuxiliaryTypeSelected;

	/** Initial Date Filter */
	private Date searchInitialDate = CommonsUtilities.currentDate();

	/** Final Date Filter */
	private Date searchFinalDate = CommonsUtilities.currentDate();

	/** The list parameter auxiliary type. */
	private List<AccountingParameter> listParameterAuxiliaryType;

	/** List */
	private List<SendInterfaceXmlSap> listSendInterfaceXmlSap;

	/** Selection */
	private SendInterfaceXmlSap sendInterfaceXmlSapSelected;

	/** REGISTRY */

	/** Aux Daily Filter */
	private Integer regAuxiliaryTypeSelected;

	/** Cut Date Filter */
	private Date regCutDate;

	/** Sending Interface To */
	private SendingInterfaceTo sendingInterfaceTo;

	/**
	 * Inits the class.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		loadCmbDataSearch();
		searchAuxiliaryTypeSelected = null;
		searchInitialDate = new Date();
		searchFinalDate = new Date();
	}

	/**
	 * Load cmb data search.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	@LoggerAuditWeb
	public void loadCmbDataSearch() throws ServiceException {

		// load dailyAuxiliaryType
		if (Validations.validateListIsNullOrEmpty(listParameterAuxiliaryType)) {

			AccountingParameterTO accountingParameterTo = new AccountingParameterTO();
			accountingParameterTo.setParameterType(PropertiesConstants.DAILY_AUXILIARY);
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			this.listParameterAuxiliaryType = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);

		}
	}

	/**
	 * Gets the streamed content.
	 * 
	 * @param file
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	@LoggerAuditWeb
	public StreamedContent getStreamedContent(byte[] file, String fileName) throws IOException {
		byte[] fileContent = file;
		return getStreamedContentFromFile(fileContent, null, fileName);
	}
	
	/**
	 * Gets the streamed content.
	 *
	 * @param accountingProcess the accounting process
	 * @return the streamed content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@LoggerAuditWeb
	public StreamedContent getStreamedContentFileSend(InterfaceProcess interfaceProcess) throws IOException{
		byte[] fileContent = parameterServiceBean.getInterfaceUploadedFileContent(interfaceProcess.getIdInterfaceProcessPk());
		return getStreamedContentFromFile(fileContent, null, interfaceProcess.getFileName());
	}
	
	/**
	 * Gets the streamed content.
	 *
	 * @param accountingProcess the accounting process
	 * @return the streamed content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@LoggerAuditWeb
	public StreamedContent getStreamedContentFileReceived(InterfaceProcess interfaceProcess) throws IOException{
		byte[] fileContent = parameterServiceBean.getInterfaceResponseFileContent(interfaceProcess.getIdInterfaceProcessPk());
		String nombre = interfaceProcess.getFileName();
		String[] parts = nombre.split("\\.");
		nombre  = parts[0]+"_resp."+parts[1];
		
		return getStreamedContentFromFile(fileContent, null,nombre);
	}
	
	public List<String> getListaDetalles(String detalle){
		if (detalle != null && !detalle.isEmpty()){
			String [] detalles = detalle.split("#");
			List<String> detallesList = Arrays.asList(detalles);
			return detallesList;
		}
		return new ArrayList<String>();
	}
	
	public String getState(Integer statePk){
		return parameterServiceBean.find(ParameterTable.class, statePk).getDescription();
	}

	/**
	 * SEARCH
	 */


	/**
	 * Search documents.
	 */
	@LoggerAuditWeb
	public void searchDocuments() {
		
		listSendInterfaceXmlSap = new ArrayList<SendInterfaceXmlSap>();
		AccountingProcess accountingProcessTemp = new AccountingProcess();
		
		if (Validations.validateIsNotNullAndPositive(searchAuxiliaryTypeSelected) && Validations.validateIsNotNull(searchFinalDate)
			&& Validations.validateIsNotNull(searchInitialDate)) {
			
			listSendInterfaceXmlSap = documentSenderInterfaceFacade.searchSendInterfaceXmlSap(searchAuxiliaryTypeSelected, searchInitialDate, searchFinalDate);
			
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}

		/** clean selected on data table */
		this.sendInterfaceXmlSapSelected = null;

	}

	/**
	 * REGISTRY
	 */

	/**
	 * Load registration page.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	@LoggerAuditWeb
	public void loadRegistrationPage() throws ServiceException {
		loadCmbDataSearch();
		sendingInterfaceTo = null;
		regAuxiliaryTypeSelected = null;
		regCutDate = new Date();
	}

	/** REGISTRY */

	/**
	 * Search Accounting Process.
	 */
	@LoggerAuditWeb
	public void searchAccounting() {

		if (Validations.validateIsNotNullAndPositive(regAuxiliaryTypeSelected) && Validations.validateIsNotNull(regCutDate)) {
			try {
				sendingInterfaceTo = documentSenderInterfaceFacade.getSendingInterface(regAuxiliaryTypeSelected, regCutDate);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}
	}

	/**
	 * Before sent account.
	 */
	public void beforeSendAccounting() {
		// TODO: validaciones
		if (sendingInterfaceTo != null) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SENDING_ACCOUNTING));
			JSFUtilities.executeJavascriptFunction("cnfwProcessSendGeneration.show();");
		}
	}

	/**
	 * Search.
	 */
	@LoggerAuditWeb
	public void sentAccounting() {

		log.info("Sending Accounting ::::> SENDING ACCOUNTING TO SAP ");

		// TODO: ENVIO AL WS DE SAP
		documentSenderInterfaceFacade.sendToSAP(sendingInterfaceTo);

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_SENDING_ACCOUNTING));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");

	}

	/**
	 * Accounting Process Seach
	 */
	// @LoggerAuditWeb
	// public void searchDataAccounting() {
	//
	// this.accountingDataModel = null;
	// this.accountingDataModifyModel = null;
	//
	// listAccountingDataMonitorTos = new ArrayList<AccountingDataMonitorTo>();
	//
	// AccountingDataMonitorTo accountingDataMonitorTo = new
	// AccountingDataMonitorTo();
	//
	// if (Validations.validateIsNotNull(accountingDate)
	// /*
	// * Validations.validateIsNotNullAndPositive(parameterAuxiliaryTypeSelected
	// * ) &&
	// */
	// ) {
	//
	// if (Validations.validateIsNotNull(accountingDataModel)) {
	// accountingDataModel = new GenericDataModel<AccountingDataMonitorTo>();
	// }
	//
	// /* Seteamos los filtros de la pantalla */
	// accountingDataMonitorTo.setExecutionDate(accountingDate);
	// accountingDataMonitorTo.setIdCustodyOperation(idCustodyOperation);
	// accountingDataMonitorTo.setIdNegotiationOperation(idNegotiationOperation);
	// accountingDataMonitorTo.setIdCorporativeOperation(idCorporativeOperation);
	// accountingDataMonitorTo.setIdSecurityCodePk(idSecurityCodePk);
	//
	// /* Ejecutamos la consulta */
	// listAccountingDataMonitorTos =
	// documentAccountingServiceFacade.loadDataAccounting(accountingDataMonitorTo);
	//
	// if
	// (Validations.validateIsNotNullAndNotEmpty(listAccountingDataMonitorTos))
	// {
	//
	// accountingDataModel = new
	// GenericDataModel<AccountingDataMonitorTo>(listAccountingDataMonitorTos);
	// }
	// } else {
	// showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
	// PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE,
	// ""));
	// JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
	// }
	//
	// /** clean selected on data table */
	// this.accountingProcessSelection = null;
	//
	// }

	/**
	 * Before process preliminary execution.
	 */
	public void beforeProcessPreliminaryExecution() {

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PROCESS_ACCOUNTING_SEND_GENERATION));
		JSFUtilities.executeJavascriptFunction("cnfwProcessSendGeneration.show();");
	}

	/**
	 * Clean documents.
	 */
	public void cleanDocuments() {
		JSFUtilities.resetComponent("fmrBillingProcess");
		JSFUtilities.resetComponent("fmrBillingProcess2");
		// this.accountingProcessDataModel = null;
		// this.accountingDataModel = null;
		// this.parameterAuxiliaryTypeSelected = null;
		// this.listAccountingProcess = null;

	}

	/**
	 * Clean documents.
	 */
	public void cleanDataAccounting() {
		JSFUtilities.resetComponent("fmrBillingProcess");
		JSFUtilities.resetComponent("fmrBillingProcess2");
		// this.accountingDataModel = null;
		// this.accountingDataModifyModel = null;
		// this.listAccountingDataMonitorTos = null;
		// this.idSecurityCodePk = null;
		// this.idCustodyOperation = null;
		// this.idNegotiationOperation = null;
		// this.idCorporativeOperation = null;

	}

	/**
	 * Valid clean form search.
	 */
	public void validCleanFormSearch() {
		cleanDocuments();
	}

	/** GETTERS AND SETTERS */

	public Integer getRegAuxiliaryTypeSelected() {
		return regAuxiliaryTypeSelected;
	}

	public void setRegAuxiliaryTypeSelected(Integer regAuxiliaryTypeSelected) {
		this.regAuxiliaryTypeSelected = regAuxiliaryTypeSelected;
	}

	public Date getRegCutDate() {
		return regCutDate;
	}

	public void setRegCutDate(Date regCutDate) {
		this.regCutDate = regCutDate;
	}

	public SendingInterfaceTo getSendingInterface() {
		return sendingInterfaceTo;
	}

	public void setSendingInterface(SendingInterfaceTo sendingInterfaceTo) {
		this.sendingInterfaceTo = sendingInterfaceTo;
	}

	public Integer getSearchAuxiliaryTypeSelected() {
		return searchAuxiliaryTypeSelected;
	}

	public void setSearchAuxiliaryTypeSelected(Integer searchAuxiliaryTypeSelected) {
		this.searchAuxiliaryTypeSelected = searchAuxiliaryTypeSelected;
	}

	public Date getSearchInitialDate() {
		return searchInitialDate;
	}

	public void setSearchInitialDate(Date searchInitialDate) {
		this.searchInitialDate = searchInitialDate;
	}

	public Date getSearchFinalDate() {
		return searchFinalDate;
	}

	public void setSearchFinalDate(Date searchFinalDate) {
		this.searchFinalDate = searchFinalDate;
	}

	public List<AccountingParameter> getListParameterAuxiliaryType() {
		return listParameterAuxiliaryType;
	}

	public void setListParameterAuxiliaryType(List<AccountingParameter> listParameterAuxiliaryType) {
		this.listParameterAuxiliaryType = listParameterAuxiliaryType;
	}

	public List<SendInterfaceXmlSap> getListSendInterfaceXmlSap() {
		return listSendInterfaceXmlSap;
	}

	public void setListSendInterfaceXmlSap(List<SendInterfaceXmlSap> listSendInterfaceXmlSap) {
		this.listSendInterfaceXmlSap = listSendInterfaceXmlSap;
	}

	public SendInterfaceXmlSap getSendInterfaceXmlSapSelected() {
		return sendInterfaceXmlSapSelected;
	}

	public void setSendInterfaceXmlSapSelected(SendInterfaceXmlSap sendInterfaceXmlSapSelected) {
		this.sendInterfaceXmlSapSelected = sendInterfaceXmlSapSelected;
	}
	
}
