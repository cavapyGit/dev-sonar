package com.pradera.accounting.documentSender.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.billing.SendInterfaceXmlSap;

@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DocumentSenderInterfaceService extends CrudDaoServiceBean {

	/** The dao service. */
	@EJB
	CrudDaoServiceBean daoService;
	
	/**
	 * Search document accounting process.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingProcess> searchAccountingProcess(Integer auxiliaryTypeSelected, Date cutDate){
		
		StringBuilder stringBuilderSqlBase = new StringBuilder();		
		stringBuilderSqlBase.append(" SELECT AP ");
		stringBuilderSqlBase.append(" FROM AccountingProcess AP WHERE 1=1  ");
		stringBuilderSqlBase.append(" AND AP.startType = :startType    ");
		
		if ( Validations.validateIsNotNullAndPositive(auxiliaryTypeSelected))
			stringBuilderSqlBase.append(" AND AP.processType = :processType    ");
		if( Validations.validateIsNotNullAndNotEmpty(cutDate))
			stringBuilderSqlBase.append(" AND trunc(AP.executionDate) = trunc(:execution) ");
		 
		Query query = em.createQuery(stringBuilderSqlBase.toString());
		query.setParameter("startType", AccountingStartType.MONTHLY.getCode());
		
		if ( Validations.validateIsNotNullAndPositive(auxiliaryTypeSelected))
			query.setParameter("processType", auxiliaryTypeSelected);
		if( Validations.validateIsNotNullAndNotEmpty(cutDate))
			query.setParameter("execution", cutDate);
		return query.getResultList();
	}
	
	/**
	 * 
	 * @param idAccountingProcess
	 * @return
	 */
	public List<SendInterfaceXmlSap> findSendInterfaceXmlSap(Integer searchAuxiliaryTypeSelected, Date initialDate, Date finalDate){
		
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select si ");
        sbQuery.append(" From SendInterfaceXmlSap si ");
        sbQuery.append(" Join Fetch si.idInterfacesTransactionFk it " );
        sbQuery.append(" Join Fetch it.interfaceProcess ip  " );
        sbQuery.append(" Where ip.processDate between :initialDate and :finalDate ");
        sbQuery.append(" and ip.idExternalInterfaceFk.interfaceName = :externalInterface ");
        sbQuery.append(" order by si.idSendInterfaceXmlSapPk desc ");
        
        Query query = em.createQuery(sbQuery.toString());
        query.setParameter("initialDate", initialDate);
        query.setParameter("finalDate", finalDate);
        query.setParameter("externalInterface",ComponentConstant.SENDING_ACCOUNTING_INTERFACE);
       
        List<SendInterfaceXmlSap> SendInterfaceXmlSapList = null;
        SendInterfaceXmlSapList = query.getResultList();
        return SendInterfaceXmlSapList;
	}

	public byte[] getAccoutingProcessFileContent(Long idAccountingProcessPk) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idAccountingProcessPk", idAccountingProcessPk);
		return (byte[]) daoService.findObjectByNamedQuery(AccountingProcess.ACCOUNTING_PROCESS_CONTENT_BY_PK, parameters);
	}
	
	
	public List<Object[]> findAccountingVouchersMonthly(AccountingProcessTo accountingProcessTo){

		
		StringBuilder sbQuery=new StringBuilder();
		 /**PK the entity*/

		 
		sbQuery.append("SELECT                                  ").
				append("  FILEX.TIPO,  								    ").                   //0
				append("  DECODE(FILEX.IND_REFERENCE,0,FILEX.CUENTA,  	").                       
				append("  SUBSTR(FILEX.CUENTA,0,14)) AS CUENTA,  		").                       //1
				append("  FILEX.MONTO,  								").                       //2
				append("  FILEX.OPERACION,  							").                       //3
				append("  FILEX.INSTRUMENT_TYPE,  						").                       //4	
				append("  FILEX.CURRENCY_TYPE,  						").                       //5
				append("  FILEX.EXECUTION_DATE,  						").                       //6
				append("  FILEX.BUY_PRICE,  							").                       //7
				append("  FILEX.MOVEMENT_NAME,  						").                       //8
				append("  FILEX.PARAMETER_NAME  						").                       //9
				append("  FROM (                                        ");
		 
		sbQuery.append("  SELECT                   ").
				append("  	BASE.TIPO AS TIPO, 	     ").
				append("  	BASE.CUENTA, 			 ").
				append("  	SUM(BASE.MONTO) MONTO,   ").
				append("  	BASE.OPERACION,          ").
				append("  	BASE.INSTRUMENT_TYPE,    ").
				append("  	BASE.CURRENCY_TYPE, 	 ").
				append("  	BASE.EXECUTION_DATE, 	 ").
				append("  	BASE.BUY_PRICE, 		 ").
				append("  	BASE.MOVEMENT_NAME,      ").
				append("  	BASE.PARAMETER_NAME,     ").
				append("     BASE.IND_REFERENCE      ").
				append("  	                                                                                 ").
				append("  FROM (                                                                             ").
				append("                                                                                     ").
				append("                                                                                     ").
				append("	SELECT                                                                           ").
				append("		AMR.MOVEMENT_TYPE AS TIPO,                                                   ").
				append("		ACC.IND_REFERENCE,                                                           ").
				append("		ACC.ACCOUNT_CODE||'.'||PA.MNEMONIC AS CUENTA,                                ").
				append("		TRUNC(AMR.AMOUNT,2) AS MONTO ,                                               ").
				append("		(SELECT ACP.PARAMETER_NAME FROM ACCOUNTING_PARAMETER ACP                     ").
				append("			WHERE ACP.ID_ACCOUNTING_PARAMETER_PK=AMD.DYNAMIC_TYPE ) AS OPERACION,    ").
				append("		ACC.INSTRUMENT_TYPE,                                                         ").
				append("		ACC.CURRENCY_TYPE,                                                           ").
				append("		AP.EXECUTION_DATE AS EXECUTION_DATE ,                    ").
				append("		DER.BUY_PRICE,                                                               ").
				append("		DECODE(MT.ID_MOVEMENT_TYPE_PK,200042,'SALDO TOTAL VALORES FISICOS',   		 ").
				append("			300223,'SALDO TOTAL CARTERA',                                         	 ").
				append("			300224,'SALDO TOTAL GRAVAMEN',                                        	 ").
				append("			300633,'SALDO TOTAL REPORTANTE',                                       	 ").
				append("			300842,'SALDO VALORES NO COLOCADOS') MOVEMENT_NAME,                    	 ").
				append("		PT.PARAMETER_NAME                                                            ").
				append("		FROM   ACCOUNTING_PROCESS AP                                                 ").
				append("		INNER JOIN ACCOUNTING_RECEIPT AR                                             ").
				append("			ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK               ").
				append("		INNER JOIN ACCOUNTING_MONTH_RESULT AMR                                       ").
				append("			ON amr.id_accounting_receipt_Fk=ar.id_accounting_receipts_pk             ").
				append("		INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD                                      ").
				append("			ON AMR.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK ").
				append("		INNER JOIN ACCOUNTING_ACCOUNT ACC                                            ").
				append("			ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK             ").
				append("		INNER JOIN PARTICIPANT PA	                                                 ").
				append("			ON PA.ID_PARTICIPANT_PK=amr.id_participant_fk                            ").
				append("		INNER JOIN DAILY_EXCHANGE_RATES DER                                          ").
				append("			ON (DER.DATE_RATE=AP.EXECUTION_DATE AND DER.ID_CURRENCY=430)             ").
				append("		INNER JOIN MOVEMENT_TYPE MT                                                  ").
				append("			ON MT.ID_MOVEMENT_TYPE_PK=AMR.MOVEMENT_TYPE                              ").
				append("		INNER JOIN PARAMETER_TABLE PT                                                ").
				append("			ON PT.PARAMETER_TABLE_PK= MT.ACCOUNTING_SCHEMA                           ");
																									 
		

        		sbQuery.append(" WHERE  AP.START_TYPE = :startType ");

		
			    if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
			    	sbQuery.append(" AND AP.ID_ACCOUNTING_PROCESS_PK=:processAccountingPk  ");	
			    }
			    if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
			        
		            sbQuery.append(" AND   AP.PROCESS_TYPE = :processType ");
		        }
		        
		        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
		            
		            sbQuery.append(" AND  trunc(AP.EXECUTION_DATE) = :executionDate ");
		        }
		
		
		
				
		sbQuery.append(" ORDER BY  TIPO, ACC.INSTRUMENT_TYPE, ACC.CURRENCY_TYPE,OPERACION  ").
			append("		                                                               ").
			append("	)  BASE                                                            ").
			append("	GROUP BY  BASE.TIPO, BASE.CUENTA, BASE.OPERACION,                  ").
			append("	BASE.INSTRUMENT_TYPE, BASE.CURRENCY_TYPE,                          ").
			append("	BASE.EXECUTION_DATE, BASE.BUY_PRICE,                               ").
			append("	BASE.MOVEMENT_NAME, BASE.PARAMETER_NAME, BASE.IND_REFERENCE        ").
			append("                                                                       ").
			append("ORDER BY BASE.EXECUTION_DATE, BASE.TIPO, BASE.INSTRUMENT_TYPE,         ").
			append("	BASE.CURRENCY_TYPE,BASE.OPERACION                                  ");
		   
			
		sbQuery.append(" 	) FILEX                                                      ").
			append("ORDER BY FILEX.EXECUTION_DATE, FILEX.TIPO, FILEX.INSTRUMENT_TYPE,    ").
			append("FILEX.CURRENCY_TYPE,FILEX.OPERACION	                                 ");
			
			
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    query.setParameter("startType", AccountingStartType.MONTHLY.getCode());
	    
	    
	    if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
	    	query.setParameter("processAccountingPk", accountingProcessTo.getIdAccountingProcessPk());
	    }
	    
	    if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
	    	query.setParameter("processType", accountingProcessTo.getProcessType());
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
        	query.setParameter("executionDate", accountingProcessTo.getExecutionDate() );
        }

	    return query.getResultList();
	
	}
}
