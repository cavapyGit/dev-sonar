package com.pradera.accounting.documentSender.to;

import java.io.Serializable;
import java.util.Date;


public class SendingInterfaceTo  implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private Long idAccountingProcessPk;
	
	private Date sendDate;
	private Date receivedDate;
	
	private String FileNameXmlSent;
	private byte[] fileXmlSent;
	private byte[] fileXmlReceived;
	
	private String responseValue;
	private Integer totalRecords;
	private Integer acceptedRecords;
	private Integer rejectRecords;
	private Integer duplicateRecords;
	public Long getIdAccountingProcessPk() {
		return idAccountingProcessPk;
	}
	public void setIdAccountingProcessPk(Long idAccountingProcessPk) {
		this.idAccountingProcessPk = idAccountingProcessPk;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public String getFileNameXmlSent() {
		return FileNameXmlSent;
	}
	public void setFileNameXmlSent(String fileNameXmlSent) {
		FileNameXmlSent = fileNameXmlSent;
	}
	public byte[] getFileXmlSent() {
		return fileXmlSent;
	}
	public void setFileXmlSent(byte[] fileXmlSent) {
		this.fileXmlSent = fileXmlSent;
	}
	public byte[] getFileXmlReceived() {
		return fileXmlReceived;
	}
	public void setFileXmlReceived(byte[] fileXmlReceived) {
		this.fileXmlReceived = fileXmlReceived;
	}
	public String getResponseValue() {
		return responseValue;
	}
	public void setResponseValue(String responseValue) {
		this.responseValue = responseValue;
	}
	public Integer getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}
	public Integer getAcceptedRecords() {
		return acceptedRecords;
	}
	public void setAcceptedRecords(Integer acceptedRecords) {
		this.acceptedRecords = acceptedRecords;
	}
	public Integer getRejectRecords() {
		return rejectRecords;
	}
	public void setRejectRecords(Integer rejectRecords) {
		this.rejectRecords = rejectRecords;
	}
	public Integer getDuplicateRecords() {
		return duplicateRecords;
	}
	public void setDuplicateRecords(Integer duplicateRecords) {
		this.duplicateRecords = duplicateRecords;
	}
	
}
