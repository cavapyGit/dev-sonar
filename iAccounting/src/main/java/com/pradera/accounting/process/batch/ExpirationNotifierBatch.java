package com.pradera.accounting.process.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.accounting.common.facade.BusinessLayerCommonFacade;
import com.pradera.accounting.document.service.sender.SenderAccountigService;
import com.pradera.accounting.notification.facade.AccountingNotificationServiceFacade;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExpirationNotifierBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@BatchProcess(name="ExpirationNotifierBatch")
@RequestScoped
public class ExpirationNotifierBatch implements JobExecution,Serializable {

	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade 		businessLayerCommonFacade;
	
	/** The sender accountig service. */
	@EJB
    SenderAccountigService 			senderAccountigService;
	
	/** The accounting notification service facade. */
	@EJB
	AccountingNotificationServiceFacade accountingNotificationServiceFacade;
	
	/** The log. */
	@Inject
	transient PraderaLogger log;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		/**
		 * Parameter input Date
		 */
		
		// TODO Auto-generated method stub
		
		log.info(" BEGIN PROCESS GENERATION DOCUMENTS ACCOUNTING");

		/**getting details ProcessLoggerDetails */
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		Date executionDate=null;
		
		if (Validations.validateListIsNotNullAndNotEmpty(processLoggerDetails)){

			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
				
				/**
				 *   FONDOS , VALORES o ADMINISTRACION
				 */
				if(processLoggerDetail.getParameterName().equals("executionDate")||
						GeneralConstants.PARAMETER_DATE.equals(processLoggerDetail.getParameterName())){
					 executionDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
				}
				
				if(GeneralConstants.PARAMETER_SCHEDULE_ID.equals(processLoggerDetail.getParameterName())){
					executionDate=CommonsUtilities.currentDate();
					break;
				}
			}
		
		}else{
			executionDate=CommonsUtilities.currentDate();
		}

		
		/**
		 * If Before Calculation Day is Holiday, reply
		 */
		TreeSet<Date> listDate=new TreeSet<Date>();
		Date afterCalculationDate=CommonsUtilities.addDate(executionDate, 1);
		try {
			listDate.add(afterCalculationDate);
			while(businessLayerCommonFacade.ifDayHoliday(afterCalculationDate)){
				listDate.add(afterCalculationDate);
				afterCalculationDate=CommonsUtilities.addDate(afterCalculationDate, 1);
			}
		} catch (ServiceException e) {
			log.debug(e.getMessage());
		}
		
		accountingNotificationServiceFacade.executeExpirationNotifier(new ArrayList<Date>(listDate));

		
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
	
	

	
}
