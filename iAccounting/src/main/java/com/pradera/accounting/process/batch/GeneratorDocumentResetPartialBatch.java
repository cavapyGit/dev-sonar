package com.pradera.accounting.process.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.accounting.common.facade.BusinessLayerCommonFacade;
import com.pradera.accounting.document.facade.DocumentAccountingServiceFacade;
import com.pradera.accounting.document.service.sender.SenderAccountingPartialService;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.matrix.to.AccountingMatrixTo;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcessSchema;
import com.pradera.model.accounting.type.AccountingMatrixStateType;
import com.pradera.model.accounting.type.AccountingSchemaStateType;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class GeneratorDocumentBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@BatchProcess(name="GeneratorDocumentResetPartialBatch")
@RequestScoped
public class GeneratorDocumentResetPartialBatch implements JobExecution,Serializable {

	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade 		businessLayerCommonFacade;
	
	/** The sender accountig service. */
	@EJB
    SenderAccountingPartialService 			senderAccountingPartialService;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade	accountingMatrixServiceFacade;
	
	/** The document accounting service facade. */
	@EJB
	DocumentAccountingServiceFacade	documentAccountingServiceFacade;
	
	/** The crud dao service bean. */
	@EJB
	private CrudDaoServiceBean crudDaoServiceBean;
	
	/** The log. */
	@Inject
	transient PraderaLogger log;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		
		log.info(" BEGIN PROCESS GENERATION DOCUMENTS ACCOUNTING");
		
		AccountingMatrixTo accountingMatrixTo = null;
		Date executionDate =null;
		Long idAccountingProcessPk=null;
		Long idAccountingSchemaPk = null;
		Long idAccountingProcessSchemaPk = null;


		
		/**getting details ProcessLoggerDetails */
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		List<AccountingMatrixTo>	listAccountingMatrixTo=new ArrayList<AccountingMatrixTo>();
		Boolean processAutomatic=Boolean.FALSE;
		
		if (Validations.validateListIsNotNullAndNotEmpty(processLoggerDetails)){
			/** Enter here where the calculation automatic are executed manual**/
			
			accountingMatrixTo = new AccountingMatrixTo();
			accountingMatrixTo.setStatus(AccountingMatrixStateType.REGISTERED.getCode());
			accountingMatrixTo.setStatusSchema(AccountingSchemaStateType.REGISTERED.getCode());
			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			
				/**
				 * If Batch Execution For Execute Process
				 */
				if(GeneralConstants.PARAMETER_DATE.equals(processLoggerDetail.getParameterName())){
					executionDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
					processAutomatic=Boolean.TRUE;
					break;
				}
				/**Si es una Tarea Programada*/
				if(GeneralConstants.PARAMETER_SCHEDULE_ID.equals(processLoggerDetail.getParameterName())){
					executionDate=CommonsUtilities.currentDate();
					processAutomatic=Boolean.TRUE;
					break;
				}
				/**
				 *   FONDOS , VALORES o ADMINISTRACION
				 */
				if(processLoggerDetail.getParameterName().equals("processType")){
					Integer processType = Integer.parseInt(processLoggerDetail.getParameterValue());		
					accountingMatrixTo.setAuxiliaryType(processType);
				}
				
				if(processLoggerDetail.getParameterName().equals("executionDate")){
					 executionDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
				}
				
				if(processLoggerDetail.getParameterName().equals("startType")){
					accountingMatrixTo.setStartType(AccountingStartType.START.getCode());
				}
				if(processLoggerDetail.getParameterName().equals("idAccountingProcessPk")){
					idAccountingProcessPk=Long.parseLong(processLoggerDetail.getParameterValue());
				}
				if(processLoggerDetail.getParameterName().equals("idAccountingProcessSchemaPk")){
					idAccountingProcessSchemaPk=Long.parseLong(processLoggerDetail.getParameterValue());
				}
				if(processLoggerDetail.getParameterName().equals("idSchemaPk")){
					idAccountingSchemaPk=Long.parseLong(processLoggerDetail.getParameterValue());
					AccountingSchemaTo AccountingSchemaTo=new AccountingSchemaTo();
					AccountingSchemaTo.setIdAccountingSchemaPk(idAccountingSchemaPk);
					accountingMatrixTo.setAccountingSchemaTo(AccountingSchemaTo);
					
				}
				
			}
			
			/*Si es automatico*/
			if(processAutomatic){
				/**
				 * Get AccountingParameter with ProcessType and 
				 * create 1 New AccountingMatrix for each
				 */
				List<AccountingParameter>  listAccountingParameter=accountingMatrixServiceFacade.getParameterAuxiliaryType();
				for (AccountingParameter accountingParameter : listAccountingParameter) {
			
					accountingMatrixTo = new AccountingMatrixTo();
					accountingMatrixTo.setStatus(AccountingMatrixStateType.REGISTERED.getCode());
					accountingMatrixTo.setStatusSchema(AccountingSchemaStateType.REGISTERED.getCode());
					accountingMatrixTo.setAuxiliaryType(accountingParameter.getIdAccountingParameterPk());
					accountingMatrixTo.setStartType(AccountingStartType.START.getCode());
					listAccountingMatrixTo.add(accountingMatrixTo);
				}
			}else{
				listAccountingMatrixTo.add(accountingMatrixTo);
			}
		
		}else{
			
			log.info(" BEGIN PROCESS GENERATION DOCUMENTS ACCOUNTING AUTOMATIC");
			/**
			 * Get AccountingParameter with ProcessType and 
			 * create 1 New AccountingMatrix for each
			 */
			executionDate=CommonsUtilities.currentDate();
			List<AccountingParameter>  listAccountingParameter=accountingMatrixServiceFacade.getParameterAuxiliaryType();
			for (AccountingParameter accountingParameter : listAccountingParameter) {
		
				accountingMatrixTo = new AccountingMatrixTo();
				accountingMatrixTo.setStatus(AccountingMatrixStateType.REGISTERED.getCode());
				accountingMatrixTo.setStatusSchema(AccountingSchemaStateType.REGISTERED.getCode());
				accountingMatrixTo.setAuxiliaryType(accountingParameter.getIdAccountingParameterPk());
				accountingMatrixTo.setStartType(AccountingStartType.START.getCode());
				listAccountingMatrixTo.add(accountingMatrixTo);
			}
			
			log.info(" FINISH PROCESS GENERATION DOCUMENTS ACCOUNTING AUTOMATIC");
		}
		
		for (AccountingMatrixTo accountingMatrixFilter : listAccountingMatrixTo){
			List<AccountingMatrix> accountingMatrixList = accountingMatrixServiceFacade.findAccountingMatrixByParameter(accountingMatrixFilter);
			
			AccountingProcessTo accountingProcessTo = new  AccountingProcessTo();
			accountingProcessTo.setIdAccountingProcessPk(idAccountingProcessPk);
			accountingProcessTo.setProcessType(accountingMatrixFilter.getAuxiliaryType());
			accountingProcessTo.setAccountingMatrixList(accountingMatrixList);
			accountingProcessTo.setExecutionDate(executionDate);
			accountingProcessTo.setStartType(AccountingStartType.START.getCode());
			accountingProcessTo.setIdAccountingSchemaPk(idAccountingSchemaPk);
			
			LoggerUser loggerUser = businessLayerCommonFacade.getLoggerUser();
			accountingProcessTo.setLoggerUser(loggerUser);
			
			/**Prepare Generate Document**/
			senderAccountingPartialService.prepareServiceForGenerateDocument(accountingProcessTo);
		}
		
		/*Actualizando estado al terminar de procesar*/
		//2122 procesado
		AccountingProcessSchema accountingProcessSchema = crudDaoServiceBean.find(AccountingProcessSchema.class, idAccountingProcessSchemaPk) ;
		accountingProcessSchema.setStatus(2122);
		try {
			crudDaoServiceBean.updateWithOutFlush(accountingProcessSchema);
		} catch (ServiceException e) {
			log.info("CONTABILIDAD: No se se puso actualizar el estado al finalizar el proceso");
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
	
	

	
}
