package com.pradera.accounting.notification.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounting.common.facade.BusinessLayerCommonFacade;
import com.pradera.accounting.notification.service.AccountingNotificationServiceBean;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingNotificationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AccountingNotificationServiceFacade  implements Serializable {  


    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The user info. */
    @Inject
	UserInfo userInfo;

	/** The log. */
	@Inject
	PraderaLogger log;

    /** The transaction registry. */
    @Resource
	TransactionSynchronizationRegistry 		transactionRegistry;
    
    /** The accounting notification service bean. */
    @EJB
    AccountingNotificationServiceBean		accountingNotificationServiceBean;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade 				businessLayerCommonFacade;
	
	
	/** The batch service bean. */
	@EJB 
	BatchServiceBean 						batchServiceBean;
    
    
    
    
    /**
     * Send process batch generate document.
     *
     * @param date the date
     * @throws ServiceException the service exception
     */
    @ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void sendProcessBatchGenerateDocument(Date date) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		Map<String,Object> parameters = new HashMap<String, Object>();	
		
		Long codeProcess=null;

		String strDate = CommonsUtilities.convertDatetoString(date);
		parameters.put("executionDate", strDate);
				
		codeProcess = BusinessProcessType.ACCOUNTING_EXPIRATION_NOTIFIER.getCode();
		BusinessProcess process = accountingNotificationServiceBean.find(BusinessProcess.class,codeProcess);	

		/** SAVE ON DB FOR THAT CAN BE GETTING FOR BATCH **/
		batchServiceBean.registerBatchTx(userInfo.getUserAccountSession().getUserName(), process, parameters);
	}
    
    /**
     * Find expiration security by date.
     *
     * @param listDate the list date
     * @return the list
     */
    @ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
    public List<Object> findExpirationSecurityByDate(List<Date> listDate){
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
        return accountingNotificationServiceBean.findExpirationSecurityByDate(listDate);
    }

    /**
     * Find coupon interes security by date.
     *
     * @param listDate the list date
     * @return the list
     */
    @ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
    public List<Object> findCouponInteresSecurityByDate(List<Date> listDate){
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
        return accountingNotificationServiceBean.findCouponInteresSecurityByDate(listDate);
    }

    /**
     * Find coupon amortization security by date.
     *
     * @param listDate the list date
     * @return the list
     */
    @ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
    public List<Object> findCouponAmortizationSecurityByDate(List<Date> listDate){
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
        return accountingNotificationServiceBean.findCouponAmortizationSecurityByDate(listDate);
    }
    
    
    /**
     * Execute expiration notifier.
     *
     * @param listDate the list date
     */
    public void executeExpirationNotifier(List<Date> listDate){
    	
    	BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCOUNTING_EXPIRATION_NOTIFIER.getCode());
		
    	List<Object> expiration= findExpirationSecurityByDate(listDate);
    	
    	for (Object object : expiration) {
			String  	security       		= (String)((Object[])object)[0].toString();
			Date 		operationDate		= (Date)(( (Object[])object )[4]);
			BigDecimal 	amount	   			= ((BigDecimal)((Object[])object)[5]);
			Object[] parameters={security,PropertiesConstants.EXPIRATION_SECURITY_MESSAGE,operationDate,amount};
			businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null,parameters);
		}
		

    	List<Object> couponInteres= findCouponInteresSecurityByDate(listDate);
    	
    	for (Object object : couponInteres) {
			String  	security       		= (String)((Object[])object)[0].toString();
    		Integer  	coupon		 		= Integer.parseInt(((BigDecimal)((Object[])object)[1]).toString());
			Date 		operationDate		= (Date)(( (Object[])object )[2]);
			BigDecimal 	amount	   			= ((BigDecimal)((Object[])object)[3]);
			Object[] parameters={security,PropertiesConstants.COUPON_INTERES_MESSAGE+coupon,operationDate,amount};
			businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null,parameters);
		}

    	List<Object> couponAmortizations= findCouponAmortizationSecurityByDate(listDate);
    	
    	for (Object object : couponAmortizations) {
    		String  	security       		= (String)((Object[])object)[0].toString();
    		Integer  	coupon		 		= Integer.parseInt(((BigDecimal)((Object[])object)[1]).toString());
			Date 		operationDate		= (Date)(( (Object[])object )[2]);
			BigDecimal 	amount	   			= ((BigDecimal)((Object[])object)[3]);
			Object[] parameters={security,PropertiesConstants.COUPON_AMORTIZATION_MESSAGE+coupon,operationDate,amount};
			businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null,parameters);
		}
    	
    }

 }
