package com.pradera.accounting.notification;

import java.io.Serializable;

import javax.ejb.Stateful;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NotificationInfo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateful
public class NotificationInfo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Number total Schemas sending by Session. */ 
	private Integer totalNumberSchemas;
	
	/** Number Accumulative Schemas sending by Session. */
	private Integer accumulativeNumberSchemas;
	
	/**
	 * Instantiates a new notification info.
	 *
	 * @return the totalNumberSchemas
	 */
	
	public NotificationInfo(){
		
	}
	
	/**
	 * Gets the total number schemas.
	 *
	 * @return the total number schemas
	 */
	public Integer getTotalNumberSchemas() {
		return totalNumberSchemas;
	}
	
	/**
	 * Sets the total number schemas.
	 *
	 * @param totalNumberSchemas the Number total Schemas sending by Session to Set
	 */
	public void setTotalNumberSchemas(Integer totalNumberSchemas) {
		this.totalNumberSchemas = totalNumberSchemas;
	}
	
	/**
	 * Gets the accumulative number schemas.
	 *
	 * @return the accumulativeNumberSchemas
	 */
	public Integer getAccumulativeNumberSchemas() {
		return accumulativeNumberSchemas;
	}
	
	/**
	 * Sets the accumulative number schemas.
	 *
	 * @param accumulativeNumberSchemas the  Number Accumulative Schemas sending by Session to Set
	 */
	public void setAccumulativeNumberSchemas(Integer accumulativeNumberSchemas) {
		this.accumulativeNumberSchemas = accumulativeNumberSchemas;
	}
	
	
	
}
