package com.pradera.accounting.notification.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountinNotificationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
public class AccountingNotificationServiceBean extends CrudDaoServiceBean {  

	/** The log. */
	@Inject
    private PraderaLogger log;
	
	/** The cui edv. */
	@Inject	
	@Configurable("cuiEDV")
	private Long cuiEdv;
	
	
    /**
     * Find expiration security by date.
     *
     * @param listDate the list date
     * @return the list
     */
    public List<Object> findExpirationSecurityByDate(List<Date> listDate) {
    
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select se.idSecurityCodePk,  " ).
        		append("  	se.stateSecurity, ").
        		append("	pa.idParticipantPk, ").
        		append("	pa.mnemonic, ").
        		append("	se.expirationDate, ").
        		append("	hab.availableBalance*se.currentNominalValue").
        		append("").
        		append(" From HolderAccountBalance hab ").
        		append(" 	join hab.security se ").
        		append(" 	join hab.holderAccount ha ").
        		append("	join hab.participant pa").
        		append(" Where ha.accountNumber=").append(cuiEdv).
        		append(" 	And se.stateSecurity<>").append(SecurityStateType.EXPIRED.getCode()).
        		append("");
        
        if(Validations.validateListIsNotNullAndNotEmpty(listDate)){

            sbQuery.append(" and  trunc(se.expirationDate) in (:listDate) ");
        }
        

        Query query = em.createQuery(sbQuery.toString());
        if(Validations.validateListIsNotNullAndNotEmpty(listDate)){

            query.setParameter("listDate", listDate);
        }
       

        
        return query.getResultList();
    }
    
    /**
     * Find coupon interes security by date.
     *
     * @param listDate the list date
     * @return the list
     */
    public List<Object> findCouponInteresSecurityByDate(List<Date> listDate) {
        
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" SELECT " ).
        		append("  	SE.ID_SECURITY_CODE_PK, ").
        		append("	PIC.COUPON_NUMBER AS nCupon,     ").
        		append("	PIC.EXPERITATION_DATE, ").
        		append("	PIC.COUPON_AMOUNT ").
        		append(" FROM HOLDER_ACCOUNT_BALANCE HAB").
        		append(" INNER JOIN HOLDER_ACCOUNT HA").
        		append("	ON HA.ID_HOLDER_ACCOUNT_PK=HAB.ID_HOLDER_ACCOUNT_PK").
        		append(" INNER JOIN  SECURITY SE").
        		append("	ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK").
        		append(" INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS").
        		append("	ON SE.ID_SECURITY_CODE_PK=IPS.ID_SECURITY_CODE_FK").
        		append(" INNER JOIN PROGRAM_INTEREST_COUPON PIC").
        		append("	ON PIC.ID_INT_PAYMENT_SCHEDULE_FK=IPS.ID_INT_PAYMENT_SCHEDULE_PK").
        		append("").
        		append(" WHERE PIC.STATE_PROGRAM_INTEREST=").append(ProgramScheduleStateType.PENDING.getCode()).
        		append("	AND SE.IND_IS_COUPON=0").
        		append("	AND SE.IND_IS_DETACHED=0").
        		append("	AND HA.ACCOUNT_NUMBER=").append(cuiEdv).
        		append("	AND se.state_security<>").append(SecurityStateType.EXPIRED.getCode()).
        		append("");
        
        if(Validations.validateListIsNotNullAndNotEmpty(listDate)){

            sbQuery.append(" and  trunc(PIC.EXPERITATION_DATE) in (:listDate) ");
            
        }
        
        sbQuery.append(" ORDER BY PIC.EXPERITATION_DATE ");
        Query query = em.createNativeQuery(sbQuery.toString());
        if(Validations.validateListIsNotNullAndNotEmpty(listDate)){

            query.setParameter("listDate", listDate);
        }

        return query.getResultList();
    }
    

    /**
     * Find coupon amortization security by date.
     *
     * @param listDate the list date
     * @return the list
     */
    public List<Object> findCouponAmortizationSecurityByDate(List<Date> listDate) {
        
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select " ).
        		append("	SE.ID_SECURITY_CODE_PK,").
        		append("	PAC.COUPON_NUMBER,").
        		append("	PAC.EXPRITATION_DATE,").
        		append("	PAC.COUPON_AMOUNT").
        		append(" FROM HOLDER_ACCOUNT_BALANCE HAB").
        		append(" INNER JOIN HOLDER_ACCOUNT HA").
        		append("	ON HA.ID_HOLDER_ACCOUNT_PK=HAB.ID_HOLDER_ACCOUNT_PK").
        		append(" INNER JOIN  SECURITY SE").
        		append("	ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK").
        		append(" LEFT JOIN AMORTIZATION_PAYMENT_SCHEDULE APS").
        		append("	ON SE.ID_SECURITY_CODE_PK=APS.ID_SECURITY_CODE_FK").
        		append(" LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC").
        		append("	ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK=APS.ID_AMO_PAYMENT_SCHEDULE_PK").
        		append(" WHERE").
        		append("	PAC.STATE_PROGRAM_AMORTIZATON=").append(ProgramScheduleStateType.PENDING.getCode()).
        		append("	AND SE.IND_IS_COUPON=0").
        		append("	AND SE.IND_IS_DETACHED=0").
        		append("	AND HA.ACCOUNT_NUMBER=").append(cuiEdv).
        		append("	AND se.state_security<>").append(SecurityStateType.EXPIRED.getCode()).
        		append("");
        
        if(Validations.validateListIsNotNullAndNotEmpty(listDate)){
        	
            sbQuery.append(" and  trunc(PAC.EXPRITATION_DATE) in (:listDate) ");
            
        }
        sbQuery.append(" ORDER BY PAC.EXPRITATION_DATE ");

        Query query = em.createNativeQuery(sbQuery.toString());
        if(Validations.validateListIsNotNullAndNotEmpty(listDate)){

            query.setParameter("listDate", listDate);
        }
       
        return query.getResultList();
    }

}
