package com.pradera.accounting.notification;

import java.util.Iterator;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.Future;

import com.pradera.accounting.document.to.AccountingProcessTo;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NotificatorTimerExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class NotificatorTimerExecutor extends TimerTask {


	/** The is succes. */
	private volatile Boolean isSucces=Boolean.FALSE;
 
	/** The map. */
	private final Map map;
	
	/** The accounting process to. */
	private final AccountingProcessTo accountingProcessTo;
	
	
	/**
	 * Instantiates a new notificator timer executor.
	 *
	 * @param mapFuture the map future
	 * @param accountingProcessTo the accounting process to
	 */
	public NotificatorTimerExecutor(Map mapFuture,AccountingProcessTo  accountingProcessTo ){
		this.map= mapFuture;
		this.accountingProcessTo=accountingProcessTo;
	}

	
	/* (non-Javadoc)
	 * @see java.util.TimerTask#run()
	 */
	@Override
	public void run() {
		
		 /***
	     * For each the map of future variables,
	     * expected to end up all
	     */
	    Iterator iteratorFuture = map.entrySet().iterator();
	    while (iteratorFuture.hasNext()) {
	    	Map.Entry e = (Map.Entry)iteratorFuture.next();
	    	Future<String> resultFuture=(Future<String>)e.getValue();
	        /**
	         * wait to resultFuture isDone
	         */
	        while(! resultFuture.isDone()){
	        	try{
		    	    Thread.sleep(5000);
		    	}catch(Exception ex)
		    	{
		    	   ex.printStackTrace();
		    	}
		    }
	    }
	    isSucces=Boolean.TRUE;
	}

	/**
	 * Gets the checks if is succes.
	 *
	 * @return the isSucces
	 */
	public Boolean getIsSucces() {
		return isSucces;
	}

	/**
	 * Sets the checks if is succes.
	 *
	 * @param isSucces the isSucces to set
	 */
	public void setIsSucces(Boolean isSucces) {
		this.isSucces = isSucces;
	}

	
	

}
