package com.pradera.accounting.notification;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.pradera.accounting.document.to.AccountingProcessTo;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NotificationListenerBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@ApplicationScoped
public class NotificationListenerBean  implements Serializable  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The notification accounting document executor. */
	@Inject
	NotificationAccountingDocumentExecutor notificationAccountingDocumentExecutor;

	/**
	 * New process parallel.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	public void newProcessParallel(@Observes  AccountingProcessTo accountingProcessTo) {
		
		notificationAccountingDocumentExecutor.verifySendingNotificator(accountingProcessTo);
		
	}
	
	
}
