package com.pradera.accounting.notification;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;

import com.pradera.accounting.common.facade.BusinessLayerCommonFacade;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NotificationAccountingDocumentExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Singleton
public class NotificationAccountingDocumentExecutor {
	
	/**The session context. */
	@Resource
	SessionContext sessionContext;
	
	/** The notification info. */
	@EJB
	NotificationInfo notificationInfo;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;
	
	/**
	 * Verify sending notificator.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	public void verifySendingNotificator(AccountingProcessTo accountingProcessTo){
		
		
		businessLayerCommonFacade.initNotification(accountingProcessTo);

		/**
		 * Send Report
		 */
		BusinessProcess businessProcess = new BusinessProcess();

		businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCOUNTING_DOCUMENT_GENERATE.getCode());


		Object[] parameters={ BusinessProcessType.ACCOUNTING_DOCUMENT_GENERATE.getDescription(),CommonsUtilities.currentDateTime().toString()};
		businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null, parameters); 

		 
		if (Validations.validateIsNotNull(accountingProcessTo.getIdAccountingProcessPk())){
			try {
				if(AccountingStartType.START.getCode().equals(accountingProcessTo.getStartType())) {
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_VOUCHER_REPORT.getCode());
					
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_SQUARING_START_REPORT.getCode());
				}else if(AccountingStartType.MONTHLY.getCode().equals(accountingProcessTo.getStartType())) {					
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_SQUARING_MONTHLY_REPORT.getCode());
				}else {
	
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_VOUCHER_REPORT.getCode());
					
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_SEATS_REPORT.getCode());
					
	//				businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
	//						ReportIdType.ACCOUNTING_SQUARING_REPORT.getCode());
					
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_BALANCE_HISTORY_REPORT.getCode());
					
					/**PDF*/
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_SEATS_REPORT_PDF.getCode());
					
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_BALANCE_HISTORY_REPORT_PDF.getCode());
					
	//				businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
	//						ReportIdType.ACCOUNTING_SQUARING_REPORT_PDF.getCode());
					
					businessLayerCommonFacade.sendingReportOnAccounting(accountingProcessTo.getIdAccountingProcessPk(),
							ReportIdType.ACCOUNTING_SQUARING_START_REPORT.getCode());
					//ACCOUNTING_SQUARING_START_REPORT
					
				
				}
			} catch (ServiceException e) {
				
				e.printStackTrace();
			}

		}
		
		
	}
	 

}
