package com.pradera.accounting.document.executor;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.accounting.common.facade.BusinessLayerCommonFacade;
import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.common.utils.SequenceGeneratorCode;
import com.pradera.accounting.document.facade.DocumentAccountingServiceFacade;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.document.to.AccountingReceiptDetailTo;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.schema.facade.SchemaAccountingServiceFacade;
import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingCorrelative;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingMatrixDetail;
import com.pradera.model.accounting.AccountingMonthResult;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.AccountingReceipt;
import com.pradera.model.accounting.AccountingReceiptDetail;
import com.pradera.model.accounting.type.AccountingCorrelativeType;
import com.pradera.model.accounting.type.AccountingNatureAccountType;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingParameterType;
import com.pradera.model.accounting.type.AccountingReceiptStateType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingDocumentExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
public class AccountingDocumentExecutor {

	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The document accounting service facade. */
	@EJB
	DocumentAccountingServiceFacade	documentAccountingServiceFacade;
	
	/** The schema accounting service facade. */
	@EJB
	SchemaAccountingServiceFacade   schemaAccountingServiceFacade;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade	accountingMatrixServiceFacade;
	
	/** The loader entity service. */
	@EJB
	LoaderEntityServiceBean 		loaderEntityService;
	
	/** The parameter accounting service facade. */
	@EJB
	ParameterAccountingServiceFacade parameterAccountingServiceFacade;
	
	/** The sequence generator code. */
	@EJB
	SequenceGeneratorCode			sequenceGeneratorCode;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean 			crudDaoServiceBean;
	

	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;

	
	/**
	 * Method generate Accounting Document from AccountingMatrixList .
	 *
	 * @param accountingMatrixList the accounting matrix list
	 * @param accountingProcessTo the accounting process to
	 * @throws Exception the exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void generateDocumentAccountingWithOutAsynchronous(List<AccountingMatrix> accountingMatrixList , AccountingProcessTo accountingProcessTo )throws Exception{
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), accountingProcessTo.getLoggerUser());	
		
	try{
		/**
		 * Reading All Files Source
		 */
		log.info("------------INICIO LECTURA DE ARCHIVOS FUENTES-------------");
		Map<Long,AccountingSourceInfo>  mapSource=accountingMatrixServiceFacade.loadAllFileSource(accountingProcessTo);		
		log.info("------------FIN DE LECTURA DE ARCHIVOS FUENTES-------------");
		
		for (AccountingMatrix accountingMatrix : accountingMatrixList) {
			generateDocumentAccounting(accountingMatrix , accountingProcessTo.getAccountingProcess(),mapSource);

		}
		
		accountingProcessTo.setNumberSchemas(accountingMatrixList.size());

		 
       }catch(Exception e){
    	   /**Log Error*/
		   log.error(e.getMessage());
   
       }
		
	}
	

	
	/**
	 * Method generate Accounting Document from AccountingMatrixList .
	 *
	 * @param accountingMatrixList the accounting matrix list
	 * @param accountingProcessTo the accounting process to
	 * @throws Exception the exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void generateDocumentAccountingMonthlyWithOutAsynchronous(List<AccountingMatrix> accountingMatrixList , AccountingProcessTo accountingProcessTo )throws Exception{
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), accountingProcessTo.getLoggerUser());	
		
	try{
		/**
		 * Reading All Files Source
		 */
		log.info("------------INICIO LECTURA DE ARCHIVOS FUENTES-------------");
		Map<Long,AccountingSourceInfo>  mapSource=accountingMatrixServiceFacade.loadAllFileSource(accountingProcessTo);		
		log.info("------------FIN DE LECTURA DE ARCHIVOS FUENTES-------------");
		
		for (AccountingMatrix accountingMatrix : accountingMatrixList) {
			generateDocumentAccountingMonthly(accountingMatrix , accountingProcessTo.getAccountingProcess(),mapSource);

		}
		
		accountingProcessTo.setNumberSchemas(accountingMatrixList.size());

		 
       }catch(Exception e){
    	   /**Log Error*/
		   log.error(e.getMessage());
   
       }
		
	}

	/**
	 * Method asynchronous, generate Accounting Document from AccountingMatrixList .
	 *
	 * @param accountingMatrixList the accounting matrix list
	 * @param accountingProcessTo the accounting process to
	 * @param mapSource the map source
	 * @return the future
	 * @throws Exception the exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	@Asynchronous
	public Future generateDocumentAccountingAsynchronous(List<AccountingMatrix> accountingMatrixList , 
														AccountingProcessTo accountingProcessTo, 
														Map<Long,AccountingSourceInfo> mapSource )throws Exception{
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), accountingProcessTo.getLoggerUser());	
		
	try{
		
		
		
		for (AccountingMatrix accountingMatrix : accountingMatrixList) {
			generateDocumentAccounting(accountingMatrix , accountingProcessTo.getAccountingProcess(),mapSource);

		}
		
		accountingProcessTo.setNumberSchemas(accountingMatrixList.size());

		 
       }catch(Exception e){
           
    	   /**Log Error*/
		   log.error(e.getMessage());
           return new AsyncResult(PropertiesConstants.ACCOUNTING_FUTURE_FINISH); 
   
       }
		
		return new AsyncResult(PropertiesConstants.ACCOUNTING_FUTURE_FINISH);
	}
	
	/**
	 * Generate Accounting Document from AccountingMatrix.
	 *
	 * @param accountingMatrix the accounting matrix
	 * @param accountingProcess the accounting process
	 * @param mapSource the map source
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void generateDocumentAccounting(AccountingMatrix accountingMatrix , AccountingProcess accountingProcess,
										Map<Long,AccountingSourceInfo> mapSource) {
				
		List<AccountingReceipt> listAccountingReceipt=new ArrayList<AccountingReceipt>();
		List<AccountingMatrixDetail>  accountingMatrixDetailList	= accountingMatrix.getAccountingMatrixDetail();
		Integer associateCode=accountingMatrix.getAccountingSchema().getAssociateCode();


		List<AccountingReceiptDetailTo>	listAccountingReceiptDetailTo=new ArrayList<AccountingReceiptDetailTo>();
		log.info("::::::PROCESANDO ESQUEMA "+accountingMatrix.getAccountingSchema().getDescription());
		System.out.println("::::::PROCESANDO ESQUEMA "+accountingMatrix.getAccountingSchema().getDescription());
		/**
		 * For each Matrix Detail
		 * **/ 
		List<AccountingReceiptDetailTo>	listAccountingReceiptDetailTmp=null;
		
		for (AccountingMatrixDetail accountingMatrixDetail : accountingMatrixDetailList) {
			
			 
		try {
			
//				if (accountingMatrixDetail.getIdAccountingMatrixDetailPk().equals(4763L)){
//					System.out.println("aqui el valor:");
//				}
				 /***
				  * Generated a list of detail from the detail document matrix.
				  * This detailed document is input for the construction of Documents
				  */
				listAccountingReceiptDetailTmp= documentAccountingServiceFacade.calculateDocumentDetail(accountingMatrixDetail,accountingProcess.getExecutionDate(),associateCode,mapSource);
				
			} catch (Exception e) {
				String schemaDescription=accountingMatrix.getAccountingSchema().getSchemaCode().concat(GeneralConstants.HYPHEN.concat(accountingMatrix.getAccountingSchema().getDescription()));
				/**Log Error*/
				log.error(e.getMessage());
				ErrorServiceType error = null;
				
				if(e instanceof ServiceException){
					if((((ServiceException) e).getErrorService()!=null&&
								 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT))){
							 
							continue;
					}
					
					error =  ((ServiceException) e).getErrorService();
					
					if (error ==null){
						error = ErrorServiceType.ACCOUNTING_OTHER_ERROR;
					}
					
				}
				else{
					error = ErrorServiceType.ACCOUNTING_OTHER_ERROR;
				}
				
				/**Send Notification EMAIL*/
				 sendNotificationAccountingError(BusinessProcessType.ACCOUNTING_DOCUMENT_GENERATE,
						 PropertiesConstants.BODY_MESSAGE_WARNING_DOCUMENT_ERROR,
						 schemaDescription,
						 error);
				
				
				
				/**Save Accounting Receipt without detail receipt, with state ERROR **/
				AccountingReceipt accountingReceipt=  new AccountingReceipt();
				accountingReceipt.setAccountingProcess(accountingProcess);
				accountingReceipt.setStatus(AccountingReceiptStateType.ERRADO.getCode());
				accountingReceipt.setReceiptDate(accountingProcess.getExecutionDate());
				accountingReceipt.setNumberReceipt(GeneralConstants.ZERO_VALUE_STRING);
				accountingReceipt.setAssociateCode(0);
				accountingReceipt.setKeyAssociate(AccountingReceiptStateType.ERRADO.getValue());
				documentAccountingServiceFacade.saveAccountingReceipt(accountingReceipt);
				return;
				
			}

			 listAccountingReceiptDetailTo.addAll(listAccountingReceiptDetailTmp);
			 			
		}// END FOR  accountingMatrixDetailList
		
		
		
		try {
			
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingReceiptDetailTo)){
	
				log.info("");
				log.info("----------------------------GENERANDO LOS RECIBOS------------------");
				/***
				 * Generate List AccountingReceipt by
				 */
				listAccountingReceipt=generateListAccountingReceipt(listAccountingReceiptDetailTo,accountingProcess,associateCode);
				
				log.info("----------------------------CALCULANDO LOS MONTOS---------------------");

				/** VERIFY AMOUNT ON DEBIT AND CREDIT ***/
				calculateAmount(listAccountingReceipt);
				log.info("----------------------------ANTES DE GRABAR----------------------------");
				/***
				 * Save All List AccountingReceipt
				 */
				documentAccountingServiceFacade.saveListAccountingReceiptWithDetail(listAccountingReceipt);
				log.info("----------------------------DESPUES DE GRABAR----------------------------");
				
				
			}
			
		} catch (Exception e) {
			
			log.error(":::::::::::The Processed To Save Accounting Receipt Failed::::::::::::::::"+e.getMessage());
			
		}
		log.error("::::::::::: Proceso Finalizado MDz ");		
	}
	
	

	
	/**
	 * Generate Accounting Document from AccountingMatrix.
	 *
	 * @param accountingMatrix the accounting matrix
	 * @param accountingProcess the accounting process
	 * @param mapSource the map source
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void generateDocumentAccountingMonthly(AccountingMatrix accountingMatrix , AccountingProcess accountingProcess,
										Map<Long,AccountingSourceInfo> mapSource) {
				
		List<AccountingReceipt> listAccountingReceipt=new ArrayList<AccountingReceipt>();
		List<AccountingMatrixDetail>  accountingMatrixDetailList	= accountingMatrix.getAccountingMatrixDetail();
		Integer associateCode=accountingMatrix.getAccountingSchema().getAssociateCode();


		//Resultado Contable
		List<AccountingMonthResult>	listAccountingMonthResult=new ArrayList<AccountingMonthResult>();
		log.info("::::::PROCESANDO ESQUEMA "+accountingMatrix.getAccountingSchema().getDescription());
		System.out.println("::::::PROCESANDO ESQUEMA "+accountingMatrix.getAccountingSchema().getDescription());
		/**
		 * For each Matrix Detail
		 * **/ 
		List<AccountingMonthResult>	listAccountingMonthResultTmp=null;
		
		for (AccountingMatrixDetail accountingMatrixDetail : accountingMatrixDetailList) {
			
			 
			try {
	
				 /***
				  * Generated a list of detail from the detail document matrix.
				  * This detailed document is input for the construction of Documents
				  */
				listAccountingMonthResultTmp= documentAccountingServiceFacade.calculateDocumentDetailMonthly(accountingMatrixDetail,accountingProcess.getExecutionDate(),associateCode,mapSource);
				
			} catch (Exception e) {
				String schemaDescription=accountingMatrix.getAccountingSchema().getSchemaCode().concat(GeneralConstants.HYPHEN.concat(accountingMatrix.getAccountingSchema().getDescription()));
				/**Log Error*/
				log.error(e.getMessage());
				ErrorServiceType error = null;
				
				if(e instanceof ServiceException){
					if((((ServiceException) e).getErrorService()!=null&&
								 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT))){
							 
							continue;
					}
					
					error =  ((ServiceException) e).getErrorService();
					
					if (error ==null){
						error = ErrorServiceType.ACCOUNTING_OTHER_ERROR;
					}
					
				}
				else{
					error = ErrorServiceType.ACCOUNTING_OTHER_ERROR;
				}
				
				/**Send Notification EMAIL*/
				 sendNotificationAccountingError(BusinessProcessType.ACCOUNTING_DOCUMENT_GENERATE,
						 PropertiesConstants.BODY_MESSAGE_WARNING_DOCUMENT_ERROR,
						 schemaDescription,
						 error);
				
				
				
				/**Save Accounting Receipt without detail receipt, with state ERROR **/
				AccountingReceipt accountingReceipt=  new AccountingReceipt();
				accountingReceipt.setAccountingProcess(accountingProcess);
				accountingReceipt.setStatus(AccountingReceiptStateType.ERRADO.getCode());
				accountingReceipt.setReceiptDate(accountingProcess.getExecutionDate());
				accountingReceipt.setNumberReceipt(GeneralConstants.ZERO_VALUE_STRING);
				accountingReceipt.setAssociateCode(0);
				accountingReceipt.setKeyAssociate(AccountingReceiptStateType.ERRADO.getValue());
				documentAccountingServiceFacade.saveAccountingReceipt(accountingReceipt);
				return;
				
			}

			listAccountingMonthResult.addAll(listAccountingMonthResultTmp);
			 			
		}// END FOR  accountingMatrixDetailList
		
		try {
			
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMonthResult)){
	
				log.info("");
				log.info("----------------------------GENERANDO LOS RECIBOS------------------");
				/***
				 * Generate List AccountingReceipt by
				 */
				listAccountingReceipt=generateListAccountingReceiptMonth(listAccountingMonthResult,accountingProcess,associateCode);
				
				log.info("----------------------------CALCULANDO LOS MONTOS---------------------");

				/** VERIFY AMOUNT ON DEBIT AND CREDIT ***/
				calculateAmountMonthly(listAccountingReceipt);
				log.info("----------------------------ANTES DE GRABAR----------------------------");
				/***
				 * Save All List AccountingReceipt
				 */
				documentAccountingServiceFacade.saveListAccountingReceiptWithResult(listAccountingReceipt);
				log.info("----------------------------DESPUES DE GRABAR----------------------------");
				
				
			}
			
		} catch (Exception e) {
			
			log.error(":::::::::::The Processed To Save Accounting Receipt Failed::::::::::::::::"+e.getMessage());
			
		}
		log.error("::::::::::: Proceso Finalizado MDz ");		
	}
	
	/**
	 * *
	 * Verify state Receipt is DESCUADRADO.
	 *
	 * @param listAccountingReceipt the list accounting receipt
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void calculateAmount(List<AccountingReceipt> listAccountingReceipt){
		
		AccountingParameterTO accountingParameterTO = new AccountingParameterTO();
		accountingParameterTO.setStatus(AccountingParameterStateType.ACTIVED.getCode());
		accountingParameterTO.setParameterType(AccountingParameterType.NATURALEZA_DE_LA_CUENTA.getCode());
							
		List<AccountingParameter> accountingParameterList = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTO);
		
		AccountingParameter parameterDebit = null;
		AccountingParameter parameterCredit = null;
		Integer dynamicType;
		
		for (AccountingParameter accountingParameter : accountingParameterList) {
			if (accountingParameter.getIdAccountingParameterPk().equals(AccountingNatureAccountType.DEBIT.getCode())){
				parameterDebit =accountingParameter;
				continue;
			} else  if (accountingParameter.getIdAccountingParameterPk().equals(AccountingNatureAccountType.ASSETS.getCode())){
				parameterCredit =accountingParameter;
				continue;
			}
		}
		
		for (AccountingReceipt accountingReceipt : listAccountingReceipt) {
			BigDecimal mountDebit = BigDecimal.ZERO;
			BigDecimal mountCredit = BigDecimal.ZERO;
			
			for (AccountingReceiptDetail accountingReceiptDetail : accountingReceipt.getAccountingReceiptDetail()) {
		
				dynamicType = accountingReceiptDetail.getAccountingMatrixDetail().getDynamicType();
				 if (dynamicType.equals(parameterDebit.getIdAccountingParameterPk())){
					 
					 mountDebit =  AccountingUtils.addTwoDecimal(accountingReceiptDetail.getAmount(), mountDebit, 2);
				 } else  if (dynamicType.equals(parameterCredit.getIdAccountingParameterPk())){
					 
					 mountCredit= AccountingUtils.addTwoDecimal(accountingReceiptDetail.getAmount(), mountCredit, 2);
				 }
				 
			}
			
			if ( ! mountCredit.toString().equals(mountDebit.toString())){
				accountingReceipt.setStatus(AccountingReceiptStateType.DESCUADRADO.getCode());
			}
			
		}

		
		
	}
	

	
	/**
	 * *
	 * Verify state Receipt is DESCUADRADO.
	 *
	 * @param listAccountingReceipt the list accounting receipt
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void calculateAmountMonthly(List<AccountingReceipt> listAccountingReceipt){
		
		AccountingParameterTO accountingParameterTO = new AccountingParameterTO();
		accountingParameterTO.setStatus(AccountingParameterStateType.ACTIVED.getCode());
		accountingParameterTO.setParameterType(AccountingParameterType.NATURALEZA_DE_LA_CUENTA.getCode());
							
		List<AccountingParameter> accountingParameterList = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTO);
		
		AccountingParameter parameterDebit = null;
		AccountingParameter parameterCredit = null;
		Integer dynamicType;
		
		for (AccountingParameter accountingParameter : accountingParameterList) {
			if (accountingParameter.getIdAccountingParameterPk().equals(AccountingNatureAccountType.DEBIT.getCode())){
				parameterDebit =accountingParameter;
				continue;
			} else  if (accountingParameter.getIdAccountingParameterPk().equals(AccountingNatureAccountType.ASSETS.getCode())){
				parameterCredit =accountingParameter;
				continue;
			}
		}
		
		for (AccountingReceipt accountingReceipt : listAccountingReceipt) {
			BigDecimal mountDebit = BigDecimal.ZERO;
			BigDecimal mountCredit = BigDecimal.ZERO;
			
			for (AccountingMonthResult accountingMonthResult : accountingReceipt.getAccountingMonthResult()) {
		
				dynamicType = accountingMonthResult.getAccountingMatrixDetail().getDynamicType();
				 if (dynamicType.equals(parameterDebit.getIdAccountingParameterPk())){
					 
					 mountDebit =  AccountingUtils.addTwoDecimal(accountingMonthResult.getAmount(), mountDebit, 2);
				 } else  if (dynamicType.equals(parameterCredit.getIdAccountingParameterPk())){
					 
					 mountCredit= AccountingUtils.addTwoDecimal(accountingMonthResult.getAmount(), mountCredit, 2);
				 }
				 
			}
			
			if ( ! mountCredit.toString().equals(mountDebit.toString())){
				accountingReceipt.setStatus(AccountingReceiptStateType.DESCUADRADO.getCode());
			}
			
		}

		
		
	}
	
	/**
	 * *
	 * Obtener Listado de Documentos.
	 *
	 * @param listAccountingReceiptDetailTo the list accounting receipt detail to
	 * @param accountingProcess the accounting process
	 * @param associateCode the associate code
	 * @return the list
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<AccountingReceipt> generateListAccountingReceipt(List<AccountingReceiptDetailTo> listAccountingReceiptDetailTo,AccountingProcess accountingProcess, Integer associateCode){
		
		AccountingReceipt accountingReceipt=  new AccountingReceipt();
		
		List<AccountingReceiptDetail> listAccountingReceiptDetail=new ArrayList<AccountingReceiptDetail>();
		
		List<AccountingReceipt> listAccountingReceipt=new ArrayList<AccountingReceipt>();
		
		String stringSequence ="";
//				generateCodeForReceipt(accountingProcess.getExecutionDate(), accountingProcess.getProcessType());

		 Collections.sort(listAccountingReceiptDetailTo);
		 
		 String keyAssociate=null;
		 for (AccountingReceiptDetailTo accReceiptDetailTo : listAccountingReceiptDetailTo) {
			 AccountingReceiptDetail accReceiptDetail=accReceiptDetailTo.getAccountingReceiptDetail();
			 
			 /**
			  * Add One more to Sequence StringSequence
			  */
			 if(Validations.validateIsNotNullAndNotEmpty(keyAssociate)&&
					 !accReceiptDetail.getKeyAssociateCode().equals(keyAssociate)){
				 /**New Associate Code*/
				accountingReceipt =  new AccountingReceipt(); 
//				stringSequence=sequenceGeneratorCode.addLastSequenceForReceipt(stringSequence);

				 accountingReceipt.setAccountingProcess(accountingProcess);
				 
				 accountingReceipt.setStatus(AccountingReceiptStateType.GENERADO.getCode());
				 accountingReceipt.setReceiptDate(accountingProcess.getExecutionDate());
				 accountingReceipt.setAssociateCode(associateCode);
				 accountingReceipt.setKeyAssociate(accReceiptDetail.getKeyAssociateCode());
				 accountingReceipt.setNumberReceipt(stringSequence);
				 
				listAccountingReceiptDetail=new ArrayList<AccountingReceiptDetail>();
				listAccountingReceipt.add(accountingReceipt);
			 }
			 else if(Validations.validateIsNotNullAndNotEmpty(keyAssociate)&&
					 !accReceiptDetail.getKeyAssociateCode().equals(keyAssociate)){
				 /**If Equals Associate Code**/
			 }
			 else if(Validations.validateIsNullOrEmpty(keyAssociate)){
				 /**First Time */
//				 stringSequence=sequenceGeneratorCode.addLastSequenceForReceipt(stringSequence);

				 accountingReceipt.setAccountingProcess(accountingProcess);
				 
				 accountingReceipt.setStatus(AccountingReceiptStateType.GENERADO.getCode());
				 accountingReceipt.setReceiptDate(accountingProcess.getExecutionDate());
				 accountingReceipt.setAssociateCode(associateCode);
				 accountingReceipt.setKeyAssociate(accReceiptDetail.getKeyAssociateCode());
				 accountingReceipt.setNumberReceipt(stringSequence);
				 
				listAccountingReceipt.add(accountingReceipt); 
			 }
			 
			 
			 listAccountingReceiptDetail.add(accReceiptDetail);
			 /**Add List Receipt Detail**/
			 accountingReceipt.setAccountingReceiptDetail(listAccountingReceiptDetail);
			 
			 
			 
			 keyAssociate=accReceiptDetail.getKeyAssociateCode();

			 /**
			  *  Setting accountingReceiptDetail and after save your logger with loggers'detail
			  */
			 accReceiptDetail.setAccountingReceipt(accountingReceipt);	
			 accReceiptDetail.setDocumentDate(accountingReceipt.getReceiptDate());
		}

		 /**
		  * Save Last Correlative
		  */
//		 saveLastSequenceCorrelative(accountingProcess,stringSequence);
		 
		 
	 return listAccountingReceipt;
	}
	

	
	/**
	 * *
	 * Obtener Listado de Documentos.
	 *
	 * @param listAccountingMonthResult the list accounting receipt detail to
	 * @param accountingProcess the accounting process
	 * @param associateCode the associate code
	 * @return the list
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<AccountingReceipt> generateListAccountingReceiptMonth(List<AccountingMonthResult> listAccountingMonthResult,
							AccountingProcess accountingProcess, Integer associateCode){
		
		
		AccountingReceipt accountingReceipt=  new AccountingReceipt();
		
		
		List<AccountingReceipt> listAccountingReceipt=new ArrayList<AccountingReceipt>();
		List<AccountingMonthResult> listAccountingMonthResultDet=new ArrayList<>();
		String stringSequence ="";

		
		Collections.sort(listAccountingMonthResult, new Comparator<AccountingMonthResult>() {
	        @Override
	        public int compare(AccountingMonthResult result1, AccountingMonthResult result2)
	        {

	            return  result1.getKeyAssociateCode().compareTo(result2.getKeyAssociateCode());
	        }
	    });

		 String keyAssociate=null;
		 for (AccountingMonthResult accountingMonthlyResult : listAccountingMonthResult) {
			 
			 /**
			  * Add One more to Sequence StringSequence
			  */
			 if(Validations.validateIsNotNullAndNotEmpty(keyAssociate)&&
					 !accountingMonthlyResult.getKeyAssociateCode().equals(keyAssociate)){
				 /**New Associate Code*/
				accountingReceipt =  new AccountingReceipt(); 
//				stringSequence=sequenceGeneratorCode.addLastSequenceForReceipt(stringSequence);

				 accountingReceipt.setAccountingProcess(accountingProcess);
				 listAccountingMonthResultDet= new ArrayList<>();
				 accountingReceipt.setStatus(AccountingReceiptStateType.GENERADO.getCode());
				 accountingReceipt.setReceiptDate(accountingProcess.getExecutionDate());
				 accountingReceipt.setAssociateCode(associateCode);
				 accountingReceipt.setKeyAssociate(accountingMonthlyResult.getKeyAssociateCode());
				 accountingReceipt.setNumberReceipt(stringSequence);
				 
				listAccountingReceipt.add(accountingReceipt);
			 }
			 else if(Validations.validateIsNotNullAndNotEmpty(keyAssociate)&&
					 !accountingMonthlyResult.getKeyAssociateCode().equals(keyAssociate)){
				 /**If Equals Associate Code**/
			 }
			 else if(Validations.validateIsNullOrEmpty(keyAssociate)){
				 /**First Time */
//				 stringSequence=sequenceGeneratorCode.addLastSequenceForReceipt(stringSequence);

				 accountingReceipt.setAccountingProcess(accountingProcess);
				 
				 accountingReceipt.setStatus(AccountingReceiptStateType.GENERADO.getCode());
				 accountingReceipt.setReceiptDate(accountingProcess.getExecutionDate());
				 accountingReceipt.setAssociateCode(associateCode);
				 accountingReceipt.setKeyAssociate(accountingMonthlyResult.getKeyAssociateCode());
				 accountingReceipt.setNumberReceipt(stringSequence);
				 
				listAccountingReceipt.add(accountingReceipt); 
			 }
			 
			 listAccountingMonthResultDet.add(accountingMonthlyResult);
			 /**Add List Receipt Detail**/
			 accountingReceipt.setAccountingMonthResult(listAccountingMonthResultDet);
		
			 keyAssociate=accountingMonthlyResult.getKeyAssociateCode();
			 accountingMonthlyResult.setAccountingReceipt(accountingReceipt);
		}
	 
	 return listAccountingReceipt;
	}

	/**
	 * Save last sequence correlative.
	 *
	 * @param accountingProcess the accounting process
	 * @param stringSequence the string sequence
	 */
	public void saveLastSequenceCorrelative(AccountingProcess accountingProcess,String  stringSequence){
		
		AccountingCorrelative accountingCorrelativeFilter=new AccountingCorrelative();
		try { 
			
			String strDate       	=CommonsUtilities.convertDateToString(accountingProcess.getExecutionDate(),AccountingUtils.DATE_PATTERN_yyyy_MM_dd);
			String strYearMonth  	=AccountingUtils.getPatternYearMonth(strDate, AccountingUtils.DATE_PATTERN_yyyy_MM_dd);							
	 
			accountingCorrelativeFilter.setMonth(AccountingUtils.getPatternMonth(strYearMonth,AccountingUtils.DATE_PATTERN_yyyy_MM));
			accountingCorrelativeFilter.setYear(AccountingUtils.getPatternYear(strYearMonth,AccountingUtils.DATE_PATTERN_yyyy_MM));
			accountingCorrelativeFilter.setCorrelativeCD(AccountingCorrelativeType.get(accountingProcess.getProcessType()).getValue());
			
			String correlative=stringSequence.substring(2, stringSequence.length());
			Long number=new Long(correlative);
			AccountingCorrelative accountingCorrelative=sequenceGeneratorCode.findAccountingCorrelative(accountingCorrelativeFilter);
			if(Validations.validateIsNotNull(accountingCorrelative)){
				
				accountingCorrelative.setCorrelativeNumber(number);
				crudDaoServiceBean.update(accountingCorrelative);
				
			}else{

				accountingCorrelativeFilter.setCorrelativeNumber(number);
				crudDaoServiceBean.create(accountingCorrelativeFilter);
				
			}
	
			
			
			
			
		} catch (ServiceException e2) {
			
			e2.printStackTrace();
		}
		
	}
	
	/**
	 * *
	 * Generate Code Correlative.
	 *
	 * @param executionDate the execution date
	 * @param processType the process type
	 * @return the string
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String generateCodeForReceipt(Date executionDate,Integer processType){
		String stringSequence=null;
		AccountingCorrelative accountingCorrelativeFilter=new AccountingCorrelative();
			try { 
				
				String strDate       	=CommonsUtilities.convertDateToString(executionDate,AccountingUtils.DATE_PATTERN_yyyy_MM_dd);
				String strYearMonth  	=AccountingUtils.getPatternYearMonth(strDate, AccountingUtils.DATE_PATTERN_yyyy_MM_dd);							
		 
				accountingCorrelativeFilter.setMonth(AccountingUtils.getPatternMonth(strYearMonth,AccountingUtils.DATE_PATTERN_yyyy_MM));
				accountingCorrelativeFilter.setYear(AccountingUtils.getPatternYear(strYearMonth,AccountingUtils.DATE_PATTERN_yyyy_MM));
				accountingCorrelativeFilter.setCorrelativeCD(AccountingCorrelativeType.get(processType).getValue());
				stringSequence = sequenceGeneratorCode.getLastSequenceForReceipt(accountingCorrelativeFilter);
			} catch (ServiceException e2) {
				
				e2.printStackTrace();
				return null;
			}
			
			return stringSequence;
	}
	
	
	/**
	 * Send notification accounting error.
	 *
	 * @param businessProcessType the business process type
	 * @param messageProperties the message properties
	 * @param schemaCode the schema code
	 * @param error the error
	 */
	public void sendNotificationAccountingError(BusinessProcessType businessProcessType,String messageProperties,String schemaCode,ErrorServiceType error){
		/** Send Notification**/
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(businessProcessType.getCode());
		String subject = messageConfiguration.getProperty(PropertiesConstants.HEADER_MESSAGE_ERROR);
		 
		String messageError=messageConfiguration.getProperty(error.getMessage());
		
		String message = messageConfiguration.getProperty(messageProperties);					 
	  	message = MessageFormat.format(message, businessProcessType.getDescription(),CommonsUtilities.currentDateTime().toString(),schemaCode,messageError); 
	  	
	  	List<UserAccountSession> listUser=this.businessLayerCommonFacade.getListUsserByBusinessProcess(businessProcessType.getCode());
		businessLayerCommonFacade.sendNotificationManual(businessProcess,listUser, subject, message, NotificationType.EMAIL);
	}

	/**
	 * Update accounting process.
	 *
	 * @param accountingProcess the accounting process
	 * @throws ServiceException the service exception
	 */
	public void updateAccountingProcess(AccountingProcess accountingProcess) throws ServiceException{
		
		documentAccountingServiceFacade.updateAccountingProcess(accountingProcess);
		
	}
	
	/**
	 * Find accounting process by code.
	 *
	 * @param idAccountingProcessPk the id accounting process pk
	 * @return the accounting process
	 * @throws ServiceException the service exception
	 */
	public AccountingProcess findAccountingProcessByCode(Long idAccountingProcessPk) throws ServiceException{
		
		return documentAccountingServiceFacade.findAccountingProcessByCode(idAccountingProcessPk);
		
	}
	

	
	
}
