package com.pradera.accounting.document.to;

import java.io.Serializable;

import com.pradera.model.accounting.AccountingCorrelative;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingCorrelativeTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingCorrelativeTo implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id accounting correlative pk. */
	private Long idAccountingCorrelativePk;

	/** The description. */
	private String description;

	/** The correlative cd. */
	private String correlativeCD;
	
	/** The correlative number. */
	private Long correlativeNumber;
	
	/** The month. */
	private Integer month;

	/** The year. */
	private Integer year;

	
	/**
	 * Instantiates a new accounting correlative to.
	 */
	public AccountingCorrelativeTo() {
		
	}
	
	/**
	 * Instantiates a new accounting correlative to.
	 *
	 * @param accountingCorrelative the accounting correlative
	 */
	public AccountingCorrelativeTo(AccountingCorrelative accountingCorrelative){
		
		this.setCorrelativeCD(accountingCorrelative.getCorrelativeCD());
		this.setCorrelativeNumber(accountingCorrelative.getCorrelativeNumber());
		this.setDescription(accountingCorrelative.getCorrelativeCD());
		this.setIdAccountingCorrelativePk(accountingCorrelative.getIdAccountingCorrelativePk());
		this.setMonth(accountingCorrelative.getMonth());
		this.setYear(accountingCorrelative.getYear());
		
	}
	
	/**
	 * Gets the id accounting correlative pk.
	 *
	 * @return the idAccountingCorrelativePk
	 */
	public Long getIdAccountingCorrelativePk() {
		return idAccountingCorrelativePk;
	}

	/**
	 * Sets the id accounting correlative pk.
	 *
	 * @param idAccountingCorrelativePk the idAccountingCorrelativePk to set
	 */
	public void setIdAccountingCorrelativePk(Long idAccountingCorrelativePk) {
		this.idAccountingCorrelativePk = idAccountingCorrelativePk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the correlative cd.
	 *
	 * @return the correlativeCD
	 */
	public String getCorrelativeCD() {
		return correlativeCD;
	}

	/**
	 * Sets the correlative cd.
	 *
	 * @param correlativeCD the correlativeCD to set
	 */
	public void setCorrelativeCD(String correlativeCD) {
		this.correlativeCD = correlativeCD;
	}

	/**
	 * Gets the correlative number.
	 *
	 * @return the correlativeNumber
	 */
	public Long getCorrelativeNumber() {
		return correlativeNumber;
	}

	/**
	 * Sets the correlative number.
	 *
	 * @param correlativeNumber the correlativeNumber to set
	 */
	public void setCorrelativeNumber(Long correlativeNumber) {
		this.correlativeNumber = correlativeNumber;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * Sets the month.
	 *
	 * @param month the month to set
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * Sets the year.
	 *
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}
	

}

