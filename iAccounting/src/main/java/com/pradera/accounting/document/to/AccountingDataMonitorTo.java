package com.pradera.accounting.document.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Transient;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * CSDCORE 2020.</li>
 * </ul>
 * 
 * The Class AccountingDataMonitorTo.
 *
 */
public class AccountingDataMonitorTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6421676968709535097L;
	
	private String movementName;
	
	private String idSecurityCodePk;
	
	private String participantSource;
	
	private String participantTarget;
	
	private String holderAccountTarget;
	
	private Integer stockQuantity;
	
	private String idCustodyOperation;
	
	private String idNegotiationOperation;
	
	private String idCorporativeOperation;
	
	private Date executionDate;
	
	@Transient
    private boolean selected;
	
	
	private Long idHolderAccountMovementPk;
	
	private Long idSourceParticipantPk;
	
	private Long idTargetParticipantPk;
	
	private Long idHolderAccountPk;
	
	private Long movementQuantity;
	
	/**
	 * Instantiates a new accounting receipt detail to.
	 */
	public AccountingDataMonitorTo() {
	}

	public String getMovementName() {
		return movementName;
	}

	public void setMovementName(String movementName) {
		this.movementName = movementName;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public String getParticipantSource() {
		return participantSource;
	}

	public void setParticipantSource(String participantSource) {
		this.participantSource = participantSource;
	}

	public String getParticipantTarget() {
		return participantTarget;
	}

	public void setParticipantTarget(String participantTarget) {
		this.participantTarget = participantTarget;
	}

	public String getHolderAccountTarget() {
		return holderAccountTarget;
	}

	public void setHolderAccountTarget(String holderAccountTarget) {
		this.holderAccountTarget = holderAccountTarget;
	}

	public Integer getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(Integer stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public String getIdCustodyOperation() {
		return idCustodyOperation;
	}

	public void setIdCustodyOperation(String idCustodyOperation) {
		this.idCustodyOperation = idCustodyOperation;
	}

	public String getIdNegotiationOperation() {
		return idNegotiationOperation;
	}

	public void setIdNegotiationOperation(String idNegotiationOperation) {
		this.idNegotiationOperation = idNegotiationOperation;
	}

	public String getIdCorporativeOperation() {
		return idCorporativeOperation;
	}

	public void setIdCorporativeOperation(String idCorporativeOperation) {
		this.idCorporativeOperation = idCorporativeOperation;
	}

	public Long getIdHolderAccountMovementPk() {
		return idHolderAccountMovementPk;
	}

	public void setIdHolderAccountMovementPk(Long idHolderAccountMovementPk) {
		this.idHolderAccountMovementPk = idHolderAccountMovementPk;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public Long getIdSourceParticipantPk() {
		return idSourceParticipantPk;
	}

	public void setIdSourceParticipantPk(Long idSourceParticipantPk) {
		this.idSourceParticipantPk = idSourceParticipantPk;
	}

	public Long getIdTargetParticipantPk() {
		return idTargetParticipantPk;
	}

	public void setIdTargetParticipantPk(Long idTargetParticipantPk) {
		this.idTargetParticipantPk = idTargetParticipantPk;
	}

	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	public Long getMovementQuantity() {
		return movementQuantity;
	}

	public void setMovementQuantity(Long movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

}
