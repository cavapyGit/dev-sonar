package com.pradera.accounting.document.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.accounting.schema.to.AccountingSchemaTo;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ACCOUNTING_PROCESS database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22-ago-2015
 */

public class AccountingProcessSchemaTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 538549331400873660L;
	
	private Long idAccountingProcessSchemaPk;

  	private AccountingProcessTo accountingProcess;

  	private AccountingSchemaTo accountingSchema;
  	
  	private Date executionDate;
  	
	private Integer startType;

	private Integer status;

	private String descriptionProcessType;

	private String descriptionStatus;

	public AccountingProcessSchemaTo() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdAccountingProcessSchemaPk() {
		return idAccountingProcessSchemaPk;
	}

	public void setIdAccountingProcessSchemaPk(Long idAccountingProcessSchemaPk) {
		this.idAccountingProcessSchemaPk = idAccountingProcessSchemaPk;
	}

	public AccountingProcessTo getAccountingProcess() {
		return accountingProcess;
	}

	public void setAccountingProcess(AccountingProcessTo accountingProcess) {
		this.accountingProcess = accountingProcess;
	}

	public AccountingSchemaTo getAccountingSchema() {
		return accountingSchema;
	}

	public void setAccountingSchema(AccountingSchemaTo accountingSchema) {
		this.accountingSchema = accountingSchema;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public Integer getStartType() {
		return startType;
	}

	public void setStartType(Integer startType) {
		this.startType = startType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescriptionProcessType() {
		return descriptionProcessType;
	}

	public void setDescriptionProcessType(String descriptionProcessType) {
		this.descriptionProcessType = descriptionProcessType;
	}

	public String getDescriptionStatus() {
		return descriptionStatus;
	}

	public void setDescriptionStatus(String descriptionStatus) {
		this.descriptionStatus = descriptionStatus;
	}
}
