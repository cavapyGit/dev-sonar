package com.pradera.accounting.document.to;

import java.io.Serializable;
import java.util.Date;

public class AccountingProcessReStartTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1391473317112409140L;
	
	private Long id;
	
	private String origenDesc;
	
	private Date operationDate;
	
	private String proccessTypeDesc;
	
	private Integer processType;
	
	private String origenAccountingDesc;
	
	private String statusDesc;
	
	private String userRegister;
	
	public AccountingProcessReStartTO() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrigenDesc() {
		return origenDesc;
	}

	public void setOrigenDesc(String origenDesc) {
		this.origenDesc = origenDesc;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getProccessTypeDesc() {
		return proccessTypeDesc;
	}

	public void setProccessTypeDesc(String proccessTypeDesc) {
		this.proccessTypeDesc = proccessTypeDesc;
	}

	public Integer getProcessType() {
		return processType;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	public String getOrigenAccountingDesc() {
		return origenAccountingDesc;
	}

	public void setOrigenAccountingDesc(String origenAccountingDesc) {
		this.origenAccountingDesc = origenAccountingDesc;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getUserRegister() {
		return userRegister;
	}

	public void setUserRegister(String userRegister) {
		this.userRegister = userRegister;
	}
}
