package com.pradera.accounting.document.service.sender;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.accounting.document.executor.AccountingDocumentExecutor;
import com.pradera.accounting.document.facade.DocumentAccountingServiceFacade;
import com.pradera.accounting.document.generator.AccountingDocumentGenerator;
import com.pradera.accounting.document.to.AccountingProcessSchemaTo;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.AccountingProcessSchema;
import com.pradera.model.accounting.type.AccountingProcessStateType;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.process.BusinessProcess;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SenderAccountingServiceGenerator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class SenderAccountingPartialServiceGenerator implements SenderAccountingPartialService{

	

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The batch service bean. */
	@EJB 
	BatchServiceBean 				batchServiceBean;
	

	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean 				crudDaoServiceBean;
	
	/** The accounting document executor. */
	@EJB
	AccountingDocumentExecutor		accountingDocumentExecutor;
	

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade	accountingMatrixServiceFacade;
	
	/** The document accounting service facade. */
	@EJB
	DocumentAccountingServiceFacade	documentAccountingServiceFacade;
	
	/** The accounting document generator. */
	@EJB
	AccountingDocumentGenerator		accountingDocumentGenerator;
	

	
	/* (non-Javadoc)
	 * @see com.pradera.accounting.document.service.sender.SenderAccountigService#sendProcessBatchGenerateDocument(com.pradera.accounting.document.to.AccountingProcessTo)
	 */
	@Performance	
	@Override
	public void sendProcessBatchGenerateDocument(AccountingProcessTo accountingProcessTo) throws ServiceException {
		
		log.info(" SENDING DATA FOR EXECUTION OF PROCESS GENERATION DOCUMENT ACCOUNTING");
		
		Map<String,Object> parameters = new HashMap<String, Object>();	
		
		Long codeProcess=null;
		String strDate = CommonsUtilities.convertDatetoString(accountingProcessTo.getExecutionDate());
		
		parameters.put("processType", accountingProcessTo.getProcessType());
		parameters.put("executionDate", strDate);
				
		codeProcess = BusinessProcessType.ACCOUNTING_DOCUMENT_GENERATE.getCode();
		BusinessProcess process = crudDaoServiceBean.find(BusinessProcess.class,codeProcess);	

		/** SAVE ON DB FOR THAT CAN BE GETTING FOR BATCH **/
		batchServiceBean.registerBatchTx(userInfo.getUserAccountSession().getUserName(), process, parameters);
		
	}
	

	
	/* (non-Javadoc)
	 * @see com.pradera.accounting.document.service.sender.SenderAccountigService#sendProcessBatchGenerateDocument(com.pradera.accounting.document.to.AccountingProcessTo)
	 */
	@Performance	
	@Override
	public void sendProcessBatchReStartGenerateDocument(AccountingProcessSchemaTo accountingProcessTo) throws ServiceException {
		
		log.info(" SENDING DATA FOR EXECUTION OF PROCESS GENERATION DOCUMENT ACCOUNTING");
		
		Map<String,Object> parameters = new HashMap<String, Object>();	
		
		Long codeProcess=null;
		
		parameters.put("idAccountingProcessPk", accountingProcessTo.getAccountingProcess().getIdAccountingProcessPk());
		parameters.put("idSchemaPk", accountingProcessTo.getAccountingSchema().getIdAccountingSchemaPk());
		parameters.put("startType", AccountingStartType.START.getCode());
		parameters.put("executionDate", CommonsUtilities.convertDatetoString(accountingProcessTo.getExecutionDate()));
		parameters.put("idAccountingProcessSchemaPk", accountingProcessTo.getIdAccountingProcessSchemaPk());
		//parameters.put("processType", accountingProcessTo.getAccountingProcess().getProcessType());
		
		codeProcess = BusinessProcessType.START_ACCOUNTING_DOCUMENT_GENERATE_PARTIAL.getCode();
		BusinessProcess process = crudDaoServiceBean.find(BusinessProcess.class,codeProcess);	
		
		AccountingProcessSchema accountingProcessSchema = null;
		accountingProcessSchema = crudDaoServiceBean.find(AccountingProcessSchema.class, accountingProcessTo.getIdAccountingProcessSchemaPk());
		accountingProcessSchema.setStatus(2514);//Estado procesando

		/** SAVE ON DB FOR THAT CAN BE GETTING FOR BATCH **/
		batchServiceBean.registerBatchTx(userInfo.getUserAccountSession().getUserName(), process, parameters);
		
	}
	
	
	
	/** 
	 * Prepare Accounting Schema for generation Accounting Document
	 * 
	 * 1.- Find All Accounting Process With same Execution Date And Process Type.
	 * 
	 * 2.- Delete The Accounting Process the Step 1.
	 * 
	 * 3.- Execute Refresh View Reports 
	 * 
	 * 4.- Create one New Accounting Process in Status Pending(2138).
	 * 
	 * 5.- By Type Auxiliary Daily Find The Accounting Schemas and Processing Formulate.
	 * 
	 * 6.- Create Accounting receipt with Detail
	 * 
	 * 7.- Notification Sending Email
	 * 
	 * 8.- Send Reporting Accounting  
	 * 
	 * @param accountingProcessTo Filter Accounting Process
	 * @see com.pradera.accounting.document.service.sender.SenderAccountigService#prepareServiceForGenerateDocument(com.pradera.accounting.document.to.AccountingProcessTo)
	 */
	@Override
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void prepareServiceForGenerateDocument(AccountingProcessTo accountingProcessTo)  {
	
		/** REMOVE ACCUNTING PROCESS ON DATE
		 *
		 *   HERE SAVE AccountingProcess
		 */
		List<AccountingMatrix> accountingMatrixList	= accountingProcessTo.getAccountingMatrixList();
		
		AccountingProcess accountingProcess = crudDaoServiceBean.find(AccountingProcess.class, accountingProcessTo.getIdAccountingProcessPk());
		accountingProcess.setStatus(AccountingProcessStateType.PENDIENTE.getCode());

		if (Validations.validateListIsNotNullAndNotEmpty(accountingMatrixList)){
			if(accountingProcess.getCountReceipt()==null) {
				accountingProcess.setCountReceipt(0);
			}
			accountingProcess.setCountReceipt(accountingProcess.getCountReceipt()+accountingMatrixList.size());
		}
		
		accountingProcessTo.setAccountingProcess(accountingProcess);	
		accountingProcessTo.setProcessType(accountingProcess.getProcessType());
		
		/***SENDING MATRIX ACCOUNTING'S ***/
		senderServiceForGenerateDocument(accountingProcessTo);
	}


	/**
	 * *.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	@Override
	@Performance
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	public void senderServiceForGenerateDocument(AccountingProcessTo accountingProcessTo) {
		
		//Procso contable, estoy omitiendo este metodo porque ya cuadro el reporte de cuadre contable
		accountingDocumentGenerator.senderAccountingMatrixForGenerateDocument(accountingProcessTo);

		/**
		 * Envio de interfaz, llevar a un enum las variables
		 */
		if(accountingProcessTo.getIdAccountingSchemaPk().intValue()==246){
			try {
				accountingDocumentGenerator.senderDocument(accountingProcessTo);
			} catch (Exception e) {
				//accountingDocumentGenerator.updateProcessStatus(accountingProcessTo.getIdAccountingProcessPk(),AccountingProcessStateType.ERRADO.getCode());
				return;
			}
			/*Actualizar estado de registro*/
			//accountingDocumentGenerator.updateProcessStatus(accountingProcessTo.getIdAccountingProcessPk(),AccountingProcessStateType.PROCESADO.getCode());
			
		}
	}
}