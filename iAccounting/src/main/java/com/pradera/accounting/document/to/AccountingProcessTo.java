package com.pradera.accounting.document.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingProcessTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingProcessTo implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id accounting process pk. */
	private Long idAccountingProcessPk;
	
	private Long idAccountingSchemaPk;
	
	/** The process type. */
	private Integer processType;
	
	/** The start type. */
	private Integer startType;
	
	/**Origen de datos, true = cartera, false = contabilidad*/
	private boolean portofolioOrigenAccount;
	
	/** The status. */
	private Integer status;
		
	/** The execution date. */
	private Date executionDate;
	
	/** The logger user. */
	private LoggerUser loggerUser;

	/** The accounting matrix list. */
	private List<AccountingMatrix> accountingMatrixList;
	
	/** The accounting process. */
	private AccountingProcess accountingProcess;
	
	/** The number schemas. */
	private Integer numberSchemas;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The list accounting receipt to. */
	private List<AccountingReceiptTo> listAccountingReceiptTo;
	
	
	public AccountingProcessTo(){
		
	}


	public Long getIdAccountingProcessPk() {
		return idAccountingProcessPk;
	}


	public void setIdAccountingProcessPk(Long idAccountingProcessPk) {
		this.idAccountingProcessPk = idAccountingProcessPk;
	}


	public Long getIdAccountingSchemaPk() {
		return idAccountingSchemaPk;
	}


	public void setIdAccountingSchemaPk(Long idAccountingSchemaPk) {
		this.idAccountingSchemaPk = idAccountingSchemaPk;
	}


	public Integer getProcessType() {
		return processType;
	}


	public void setProcessType(Integer processType) {
		this.processType = processType;
	}


	public Integer getStartType() {
		return startType;
	}


	public void setStartType(Integer startType) {
		this.startType = startType;
	}


	public boolean isPortofolioOrigenAccount() {
		return portofolioOrigenAccount;
	}


	public void setPortofolioOrigenAccount(boolean portofolioOrigenAccount) {
		this.portofolioOrigenAccount = portofolioOrigenAccount;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public Date getExecutionDate() {
		return executionDate;
	}


	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}


	public LoggerUser getLoggerUser() {
		return loggerUser;
	}


	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}


	public List<AccountingMatrix> getAccountingMatrixList() {
		return accountingMatrixList;
	}


	public void setAccountingMatrixList(List<AccountingMatrix> accountingMatrixList) {
		this.accountingMatrixList = accountingMatrixList;
	}


	public AccountingProcess getAccountingProcess() {
		return accountingProcess;
	}


	public void setAccountingProcess(AccountingProcess accountingProcess) {
		this.accountingProcess = accountingProcess;
	}


	public Integer getNumberSchemas() {
		return numberSchemas;
	}


	public void setNumberSchemas(Integer numberSchemas) {
		this.numberSchemas = numberSchemas;
	}


	public Date getInitialDate() {
		return initialDate;
	}


	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}


	public Date getFinalDate() {
		return finalDate;
	}


	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}


	public List<AccountingReceiptTo> getListAccountingReceiptTo() {
		return listAccountingReceiptTo;
	}


	public void setListAccountingReceiptTo(List<AccountingReceiptTo> listAccountingReceiptTo) {
		this.listAccountingReceiptTo = listAccountingReceiptTo;
	}
}
