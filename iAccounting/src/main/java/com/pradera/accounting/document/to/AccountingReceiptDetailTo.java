package com.pradera.accounting.document.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.model.accounting.AccountingReceiptDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingReceiptDetailTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingReceiptDetailTo implements Serializable, Comparable<AccountingReceiptDetailTo> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6421676968709535097L;
	
	/** The id accounting account pk. */
	private Long idAccountingAccountPk;
	
	/** The account code. */
	private String accountCode;
	
	/** The ammount debit. */
	private BigDecimal ammountDebit;
	
	/** The ammount assets. */
	private BigDecimal ammountAssets;
	
	/** The ind sub account. */
	private Integer indSubAccount;
	
	/** The nature type. */
	private Integer natureType;
	
	/** The gloss. */
	private String gloss;
	
	/** The accounting receipt detail. */
	private AccountingReceiptDetail	accountingReceiptDetail;
	
	/** The key associate code. */
	private String keyAssociateCode;
	
	/**
	 * Instantiates a new accounting receipt detail to.
	 */
	public AccountingReceiptDetailTo() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compareTo(AccountingReceiptDetailTo o) {
		AccountingReceiptDetailTo accountingReceiptDetailTo=(AccountingReceiptDetailTo) o;
		String keyAssociate=accountingReceiptDetailTo.keyAssociateCode;
		String keyAssociateThis=this.keyAssociateCode;
		return keyAssociateThis.compareTo(keyAssociate);
	}

	public Long getIdAccountingAccountPk() {
		return idAccountingAccountPk;
	}

	public void setIdAccountingAccountPk(Long idAccountingAccountPk) {
		this.idAccountingAccountPk = idAccountingAccountPk;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public BigDecimal getAmmountDebit() {
		return ammountDebit;
	}

	public void setAmmountDebit(BigDecimal ammountDebit) {
		this.ammountDebit = ammountDebit;
	}

	public BigDecimal getAmmountAssets() {
		return ammountAssets;
	}

	public void setAmmountAssets(BigDecimal ammountAssets) {
		this.ammountAssets = ammountAssets;
	}

	public Integer getIndSubAccount() {
		return indSubAccount;
	}

	public void setIndSubAccount(Integer indSubAccount) {
		this.indSubAccount = indSubAccount;
	}

	public Integer getNatureType() {
		return natureType;
	}

	public void setNatureType(Integer natureType) {
		this.natureType = natureType;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public AccountingReceiptDetail getAccountingReceiptDetail() {
		return accountingReceiptDetail;
	}

	public void setAccountingReceiptDetail(AccountingReceiptDetail accountingReceiptDetail) {
		this.accountingReceiptDetail = accountingReceiptDetail;
	}

	public String getKeyAssociateCode() {
		return keyAssociateCode;
	}

	public void setKeyAssociateCode(String keyAssociateCode) {
		this.keyAssociateCode = keyAssociateCode;
	}

}
