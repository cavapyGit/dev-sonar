package com.pradera.accounting.document.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.accounting.document.executor.AccountingDocumentExecutor;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.notification.NotificatorTimerExecutor;
import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.type.AccountingProcessStateType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingDocumentGenerator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Singleton
public class AccountingDocumentGenerator {
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The em. */
	@Inject @DepositaryDataBase
	private EntityManager em;
	
	/** The accounting document executor. */
	@EJB
	AccountingDocumentExecutor		accountingDocumentExecutor;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade	accountingMatrixServiceFacade;
//	@EJB 
//	NotificationInfo notificationInfo;
	
	/**The session context. */
	@javax.annotation.Resource
	SessionContext sessionContext;
	
	/** The batch notification. */
	@Inject
	@Any
	Event<AccountingProcessTo> notificationAccounting;
 
	/**
	 * Delete accounting.
	 */
	public void deleteAccounting(){
		
	}
	
	/**
	 * *
	 * Sender Accounting Schemas for Generate Document.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void senderAccountingMatrixForGenerateDocument(AccountingProcessTo accountingProcessTo) {
		
		List<AccountingMatrix> accountingMatrixList	= accountingProcessTo.getAccountingMatrixList();
		senderServiceForGenerateDocument(accountingMatrixList, accountingProcessTo);

	}
	
	/**
	 * *
	 * Sender Accounting Schemas for Generate Document.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void senderAccountingMatrixPartialForGenerateDocument(AccountingProcessTo accountingProcessTo) {
		
		List<AccountingMatrix> accountingMatrixList	= accountingProcessTo.getAccountingMatrixList();
		senderServiceForGenerateDocumentPartial(accountingMatrixList, accountingProcessTo);

	}

	
	/**
	 * *
	 * Sender Accounting Schemas for Generate Document. Monthly mdz
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void senderAccountingMatrixMonthlyForGenerateDocument(AccountingProcessTo accountingProcessTo) {
		
		List<AccountingMatrix> accountingMatrixList	= accountingProcessTo.getAccountingMatrixList();
		senderServiceForGenerateDocumentMonthly(accountingMatrixList, accountingProcessTo);

	}
	
	/**
	 * 
	 * Split List Matrix detail in Thread, parameter DB numberThread
	 * For Method asynchronous
	 * For method Asyncro.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=8)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void splitThreadAccounting(AccountingProcessTo accountingProcessTo) {
		
		List<AccountingMatrix> accountingMatrixList	= accountingProcessTo.getAccountingMatrixList();
		/**Set List */
		List<AccountingMatrix> accountingMatrixListSending=new ArrayList<AccountingMatrix>();
		/**Map with List Accounting Matrix**/
		Map<Integer, List<AccountingMatrix>> mapListAccountingList=new HashMap<Integer, List<AccountingMatrix>>();
		
		
		/**Variable TO DB**/
		Integer numberThread=null;
//		try {
//			ParameterTableTO parameterTableTO = new ParameterTableTO();
//			parameterTableTO.setState(1);
//			parameterTableTO.setParameterTablePk(PropertiesConstants.NUMERO_HILOS);
//			numberThread = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO).get(0).getShortInteger();
//		}catch (ServiceException e) {
//			e.printStackTrace();
//		}
		
		numberThread=15;//valor parametrico
		log.debug(" Number the Matrix ::: "+accountingMatrixList.size());

		/**split  quotient**/
		int quotient=accountingMatrixList.size()/numberThread;
		/**Mod Size Matrix List and Number Thread*/
		int residue=accountingMatrixList.size()%numberThread;

		/**
		 * 
		 * Split the list in numberThread, parameter database
		 * if quotient >0 create list and residue
		 * Ex: accountingMatrixList.size()=10
		 * 
		 * 
		 * 
		 * */
		if(quotient>0){
			
			for (int i = 0; i < accountingMatrixList.size()&& accountingMatrixList.size()>=i+numberThread; i=i+numberThread) {
				if(i==0){
					for (int j = 0; j < numberThread; j++) {
						accountingMatrixListSending=new ArrayList<AccountingMatrix>();
						accountingMatrixListSending.add(accountingMatrixList.get(i+j));
						mapListAccountingList.put(j, accountingMatrixListSending);
						
					}
				}else{
					for (int j = 0; j < numberThread; j++) {
						accountingMatrixListSending=mapListAccountingList.get(j);
						accountingMatrixListSending.add(accountingMatrixList.get(i+j));
						mapListAccountingList.put(j, accountingMatrixListSending);
						
					}
				}
				
			}

			for(int i=0;i<residue;i++){
				accountingMatrixListSending=mapListAccountingList.get(i);
				accountingMatrixListSending.add(accountingMatrixList.get(i+quotient*numberThread));
				mapListAccountingList.put(i, accountingMatrixListSending);
			}

		}
		else{
			for(int i=0;i<residue;i++){
				accountingMatrixListSending=new ArrayList<AccountingMatrix>();
				accountingMatrixListSending.add(accountingMatrixList.get(i));
				mapListAccountingList.put(i, accountingMatrixListSending);
			}
		}


		senderServiceForGenerateDocumentFuture(mapListAccountingList, accountingProcessTo);

		
	}
	
	/**
	 * Send Service For Generate Document.
	 *
	 * @param accountingMatrixList the accounting matrix list
	 * @param accountingProcessTo the accounting process to
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void senderServiceForGenerateDocument(List<AccountingMatrix> accountingMatrixList, AccountingProcessTo accountingProcessTo){
		
		AccountingProcess accountingProcess=null;
		
		try {
			//accouting matrix revisar
			accountingProcess=accountingDocumentExecutor.findAccountingProcessByCode(accountingProcessTo.getAccountingProcess().getIdAccountingProcessPk());
			accountingDocumentExecutor.generateDocumentAccountingWithOutAsynchronous(accountingMatrixList , accountingProcessTo);	
		    
		    /**
		     * Change Status Accounting Process to Process
		     */
	    	accountingProcess.setStatus(AccountingProcessStateType.PROCESADO.getCode());
			em.flush();
			em.clear();
		} catch (Exception e) {
			AccountingProcess accountingProcessError=new AccountingProcess();
	    	accountingProcessError.setStatus(AccountingProcessStateType.ERRADO.getCode());
			e.printStackTrace();			
		} finally{
			em.flush();
		}
		
		
	}
	
	/**
	 * Send Service For Generate Document.
	 *
	 * @param accountingMatrixList the accounting matrix list
	 * @param accountingProcessTo the accounting process to
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void senderServiceForGenerateDocumentPartial(List<AccountingMatrix> accountingMatrixList, AccountingProcessTo accountingProcessTo){
		
		AccountingProcess accountingProcess=null;
		
		try {
			
			accountingProcess=accountingDocumentExecutor.findAccountingProcessByCode(accountingProcessTo.getAccountingProcess().getIdAccountingProcessPk());
			accountingDocumentExecutor.generateDocumentAccountingWithOutAsynchronous(accountingMatrixList , accountingProcessTo);	
		    
		    /**
		     * Change Status Accounting Process to Process
		     */
	    	accountingProcess.setStatus(AccountingProcessStateType.PROCESADO.getCode());
			em.flush();
			em.clear();
		} catch (Exception e) {
			AccountingProcess accountingProcessError=new AccountingProcess();
	    	accountingProcessError.setStatus(AccountingProcessStateType.ERRADO.getCode());
			e.printStackTrace();			
		} finally{
			em.flush();
		}
		
		
	}
	

	
	/**
	 * Send Service For Generate Document. Monthly mdz
	 *
	 * @param accountingMatrixList the accounting matrix list
	 * @param accountingProcessTo the accounting process to
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void senderServiceForGenerateDocumentMonthly(List<AccountingMatrix> accountingMatrixList, AccountingProcessTo accountingProcessTo){
		
		AccountingProcess accountingProcess=null;
		
		try {
			
			accountingProcess=accountingDocumentExecutor.findAccountingProcessByCode(accountingProcessTo.getAccountingProcess().getIdAccountingProcessPk());
			accountingDocumentExecutor.generateDocumentAccountingMonthlyWithOutAsynchronous(accountingMatrixList , accountingProcessTo);	
		    
		    /**
		     * Change Status Accounting Process to Process
		     */
	    	accountingProcess.setStatus(AccountingProcessStateType.PROCESADO.getCode());
			em.flush();
			em.clear();
		} catch (Exception e) {
			AccountingProcess accountingProcessError=new AccountingProcess();
	    	accountingProcessError.setStatus(AccountingProcessStateType.ERRADO.getCode());
			e.printStackTrace();			
		} finally{
			em.flush();
		}
		
		
	}
	
	/**
	 * Sending queue for matrix
	 * for Method asynchronous.
	 *
	 * @param mapListAccountingList the map list accounting list
	 * @param accountingProcessTo the accounting process to
	 */
	public void senderServiceForGenerateDocumentFuture(Map mapListAccountingList,AccountingProcessTo accountingProcessTo){

		Iterator it = mapListAccountingList.entrySet().iterator();
//		Boolean outWhile=Boolean.FALSE;
		
		Future<String> result=null;
		Map mapFuture = new HashMap<Integer, Future<String>>();
		Integer indice = 0;
		log.info("------------INICIO LECTURA DE ARCHIVOS FUENTES-------------");
		Map<Long,AccountingSourceInfo> mapSource=null;
		mapSource=accountingMatrixServiceFacade.loadAllFileSource(accountingProcessTo);
		log.info("------------FIN DE LECTURA DE ARCHIVOS FUENTES-------------");
		/**
		 * Generate Accounting Document and return Future Variable
		 * the result: variable Future add To Map
		 */
	    while (it.hasNext()) {
	        Map.Entry e = (Map.Entry)it.next();
	        List<AccountingMatrix> listAccountingMatrix=(List<AccountingMatrix>)e.getValue();
	        try {
	        	
	        	
				result =  accountingDocumentExecutor.generateDocumentAccountingAsynchronous(listAccountingMatrix , accountingProcessTo, mapSource);
			
				
	        } catch (Exception e1) {
	        	log.error(e1.getMessage());
			}
	        
	        log.debug("["+e.getKey() + "=" + e.getValue()+"]");
	        mapFuture.put(indice, result);
	        indice++;
	   }
	    
	    /**Timer for Performance
	     * */
	    NotificatorTimerExecutor notificatorTimerExecutor=new NotificatorTimerExecutor(mapFuture,accountingProcessTo);
	    Timer timer=new Timer(PropertiesConstants.ACCOUNTING_THREAD,true);
	    timer.schedule(notificatorTimerExecutor, 0);

	    /**Wait To Finish Processing All Accounting Schemas*/
	    while (!(Validations.validateIsNotNull(notificatorTimerExecutor.getIsSucces())&& notificatorTimerExecutor.getIsSucces())) {

	    }
		

    	/**
	     * Send notification
	     */
	    senderDocument(accountingProcessTo);
	    /**
	     * Change Status Accounting Process to Process
	     */
	    
	    try {
	    	AccountingProcess accountingProcess=accountingDocumentExecutor.findAccountingProcessByCode(accountingProcessTo.getAccountingProcess().getIdAccountingProcessPk());
	    	accountingProcess.setStatus(AccountingProcessStateType.PROCESADO.getCode());
	    	em.flush();
			
		} catch (ServiceException e) {
			log.error(e.getMessage());
		}
		
	
	    	
	  }
        
	    
	    
	    
	
	/**
	 * Sender document.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	@TransactionTimeout(unit=TimeUnit.HOURS,value=5)
	public void senderDocument(AccountingProcessTo accountingProcessTo){
		notificationAccounting.fire(accountingProcessTo);
	}

	public void updateProcessStatus(Long idAccountingProcessPk,Integer state){
		try {
			Thread.sleep(900000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		StringBuilder queryString = new StringBuilder();
		queryString.append(" UPDATE  ACCOUNTING_PROCESS ");
		queryString.append(" SET STATUS = "+state+",NAME_FILE = 'CSDCOREEPRT322_RE_INICIO_CONTABLE"+idAccountingProcessPk.toString()+".xml'");
		queryString.append(" WHERE 1 = 1 ");
		queryString.append(" AND ID_ACCOUNTING_PROCESS_PK = "+idAccountingProcessPk); 
		
		Query query = em.createNativeQuery(queryString.toString());
		query.executeUpdate();
		em.flush();
		em.clear();
	}
	
	
	
	
	
}
