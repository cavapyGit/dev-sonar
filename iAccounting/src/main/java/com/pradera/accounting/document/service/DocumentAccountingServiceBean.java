package com.pradera.accounting.document.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pradera.accounting.document.to.AccountingDataMonitorTo;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.document.to.FilterStartBalanceTO;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.AccountingReceipt;
import com.pradera.model.accounting.AccountingReceiptDetail;
import com.pradera.model.accounting.type.AccountingReceiptStateType;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.generalparameter.type.CurrencyType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DocumentAccountingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DocumentAccountingServiceBean  extends CrudDaoServiceBean{
		
	 
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Save parameter accounting.
	 *
	 * @param parameterAccounting  Method for save one entity
	 */
	
	
	public void saveParameterAccounting(Object parameterAccounting){
		create(parameterAccounting);
	}
	
	
	
	/**
	 * Gets the find usd exchange rate.
	 *
	 * @param date the date
	 * @return the find usd exchange rate
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getFindUSDExchangeRate(Date date)throws ServiceException{
		
		BigDecimal change = null ;
		
		StringBuffer sbQuery=new StringBuffer();
		sbQuery.append(" SELECT REFERENCE_PRICE  ")
				.append("   FROM DAILY_EXCHANGE_RATES  ")
				.append("   WHERE TRUNC(DATE_RATE) = :date ")
				.append("	AND ID_CURRENCY = ")
				.append(CurrencyType.USD.getCode());
 
		Query query = em.createNativeQuery(sbQuery.toString()); 		
		
		query.setParameter("date", date );		
		
		try{
			return (BigDecimal) query.getSingleResult();
		}catch(NoResultException nrException){
			return change ;
 		}	
		
 	}
	 
	
	
	
	
	
	
	/**
	 * Search document accounting process.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingProcess> searchDocumentAccountingProcess(AccountingProcess filter){
		
		StringBuilder stringBuilderSqlBase = new StringBuilder();		
		stringBuilderSqlBase.append(" SELECT DISTINCT   TO_CHAR(AP.EXECUTION_DATE,'DD/MM/YYYY HH24:MI:SS')  AS EXECUTION_DATE, ");
		stringBuilderSqlBase.append(" (SELECT DESCRIPTION FROM ACCOUNTING_PARAMETER WHERE ID_ACCOUNTING_PARAMETER_PK = AP.PROCESS_TYPE) DESCRIPTION_PROCESS_TYPE, ");
		stringBuilderSqlBase.append(" (SELECT COUNT(ID_ACCOUNTING_RECEIPTS_PK) FROM ACCOUNTING_RECEIPT WHERE STATUS =  "+AccountingReceiptStateType.GENERADO.getCode());
		stringBuilderSqlBase.append("  AND ID_ACCOUNTING_PROCESS_FK=AP.ID_ACCOUNTING_PROCESS_PK) AS GENERADO , ");
		stringBuilderSqlBase.append(" (SELECT COUNT(ID_ACCOUNTING_RECEIPTS_PK) FROM ACCOUNTING_RECEIPT WHERE STATUS =  "+AccountingReceiptStateType.DESCUADRADO.getCode());
		stringBuilderSqlBase.append(" AND ID_ACCOUNTING_PROCESS_FK=AP.ID_ACCOUNTING_PROCESS_PK) AS DESCUADRADO, ");
		stringBuilderSqlBase.append(" (SELECT COUNT(ID_ACCOUNTING_RECEIPTS_PK) FROM ACCOUNTING_RECEIPT WHERE STATUS =  "+AccountingReceiptStateType.ERRADO.getCode());
		stringBuilderSqlBase.append(" AND ID_ACCOUNTING_PROCESS_FK=AP.ID_ACCOUNTING_PROCESS_PK) AS ERRADO,  ");
		stringBuilderSqlBase.append(" (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = AP.STATUS ) AS DESCRIPTION_STATUS  ");
		stringBuilderSqlBase.append(" FROM ACCOUNTING_PROCESS AP WHERE 1=1  ");
		
		if ( Validations.validateIsNotNullAndPositive(filter.getProcessType()) ){
			stringBuilderSqlBase.append(" AND AP.PROCESS_TYPE = :processType    ");
		}
		if( Validations.validateIsNotNullAndNotEmpty(filter.getExecutionDate())){
			stringBuilderSqlBase.append(" AND trunc(AP.EXECUTION_DATE) = :execution    ");
		}
		 
		Query query = em.createNativeQuery(stringBuilderSqlBase.toString());	
		
		if ( Validations.validateIsNotNullAndPositive(filter.getProcessType()) ){
			query.setParameter("processType", filter.getProcessType() );
		}
		if( Validations.validateIsNotNullAndNotEmpty(filter.getExecutionDate())){
			query.setParameter("execution", filter.getExecutionDate() );
		}
		  
		List<Object[]>  objectList = query.getResultList();
		
		List<AccountingProcess> listAccountingProcess  = new ArrayList<AccountingProcess>();
		
		for(int i=0;i<objectList.size();++i){	
			Object[] sResults = (Object[])objectList.get(i);
			AccountingProcess accountingProcess = new AccountingProcess();
			accountingProcess.setDescriptionStatus(sResults[5]==null?"":sResults[5].toString()); 
			
			accountingProcess.setCalculateDate(sResults[0].toString()); 
			accountingProcess.setDescriptionProcessType(sResults[1]==null?"":sResults[1].toString()); 
			accountingProcess.setGenerado(sResults[2]==null?null:Integer.parseInt(sResults[2].toString()));
			
			accountingProcess.setDescuadrado(sResults[3]==null?null:Integer.parseInt(sResults[3].toString()));	
			accountingProcess.setErrado(sResults[4]==null?null:Integer.parseInt(sResults[4].toString()));
			
			listAccountingProcess.add(accountingProcess);
			 
		}
		
		return listAccountingProcess;
	}
	
	
	public List<AccountingSchemaTo> getLisAccountingSchemas(FilterStartBalanceTO filterStartBalanceTO){
		StringBuilder stringBuilder = new StringBuilder();	
		stringBuilder.append(" select * from ACCOUNTING_SCHEMA ");
		stringBuilder.append(" where 1 = 1 ");
		stringBuilder.append(" and START_TYPE = 79 ");

		Query query = em.createNativeQuery(stringBuilder.toString());
		
		List<Object[]>  objectList = query.getResultList();
		
		
		List<AccountingSchemaTo> listAccountingSchemaTos = new ArrayList<>();
		AccountingSchemaTo accountingSchemaTo;
		for (Object[] objects : objectList) {
			accountingSchemaTo = new AccountingSchemaTo();
			accountingSchemaTo.setIdAccountingSchemaPk(new BigDecimal(objects[0].toString()).longValue());
			accountingSchemaTo.setSchemaCode(objects[1].toString());
			accountingSchemaTo.setDescription(objects[2].toString());
			listAccountingSchemaTos.add(accountingSchemaTo);
		}
		return listAccountingSchemaTos;
	}
	/**
	 * Search document accounting process.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<AccountingDataMonitorTo> searchDataAccounting(AccountingDataMonitorTo filter){
		
		StringBuilder stringBuilder = new StringBuilder();		
		
		stringBuilder.append("  SELECT                                                                                                ");
		stringBuilder.append("                                                                                                        ");
		stringBuilder.append("  MT.MOVEMENT_NAME MOVIMIENTO,                                                                          ");
		stringBuilder.append("  HAM.ID_SECURITY_CODE_FK VALOR,                                                                        ");
		stringBuilder.append("  P_ORIG.MNEMONIC P_ORIGEN,                                                                             ");
		stringBuilder.append("  P_DEST.MNEMONIC P_DESTINO,                                                                            ");
		stringBuilder.append("  HA.ACCOUNT_NUMBER || ' / ' || HA.ALTERNATE_CODE CUENTA_DEST,                                          ");
		stringBuilder.append("  HAM.MOVEMENT_QUANTITY CANTIDAD,                                                                       ");
		stringBuilder.append("  NVL(TO_CHAR(HAM.ID_CUSTODY_OPERATION_FK),'NO') OP_CUSTODIA,                                           ");
		stringBuilder.append("  NVL(TO_CHAR(HAM.ID_TRADE_OPERATION_FK),'NO') OP_NEGOCIACION,                                          ");
		stringBuilder.append("  NVL(TO_CHAR(HAM.ID_CORPORATIVE_OPERATION_FK),'NO') OP_CORPORATIVO,                                    ");
		stringBuilder.append("  HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK,                                                                    ");
		stringBuilder.append("  HAM.ID_SOURCE_PARTICIPANT,                                                                            ");
		stringBuilder.append("  HAM.ID_TARGET_PARTICIPANT,                                                                            ");
		stringBuilder.append("  HAM.ID_HOLDER_ACCOUNT_FK,                                                                             ");
		stringBuilder.append("  HAM.MOVEMENT_QUANTITY                                                                                 ");
		stringBuilder.append("                                                                                                        ");
		stringBuilder.append("  FROM HOLDER_ACCOUNT_MOVEMENT HAM                                                                      ");
		stringBuilder.append("  INNER JOIN MOVEMENT_TYPE MT ON MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK                       ");
		stringBuilder.append("  LEFT JOIN HOLDER_ACCOUNT HA ON HAM.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                     ");
		stringBuilder.append("  LEFT JOIN PARTICIPANT P_ORIG ON P_ORIG.ID_PARTICIPANT_PK = HAM.ID_SOURCE_PARTICIPANT                  ");
		stringBuilder.append("  LEFT JOIN PARTICIPANT P_DEST ON P_DEST.ID_PARTICIPANT_PK = HAM.ID_TARGET_PARTICIPANT                  ");
		stringBuilder.append("  WHERE 1 = 1                                                                                           ");
		stringBuilder.append("                                                                                                        ");
		stringBuilder.append("   AND HAM.MOVEMENT_DATE >= TRUNC (TO_DATE (:execution, 'DD/MM/YYYY'), 'DD')                            ");
		stringBuilder.append("      AND HAM.MOVEMENT_DATE < TRUNC (TO_DATE (:execution, 'DD/MM/YYYY'), 'DD') + 1                      ");
		stringBuilder.append("      AND TRUNC (TO_DATE (:execution, 'DD/MM/YYYY'), 'DD') = TO_DATE (:execution, 'DD/MM/YYYY')         ");
		stringBuilder.append("                                                                                                        ");
		stringBuilder.append("  AND MT.ID_MOVEMENT_TYPE_PK                                                                            ");
		stringBuilder.append("      NOT IN (300692,300693,300694,300695,300696,300697,300698, /* VALORACION */                        ");
		stringBuilder.append("  			300699,300700,300701,300704,300705,300706,300707                                          ");
		stringBuilder.append("               ,300793 /* RENOVACION AUTO*/                                                             ");
		stringBuilder.append("               ,300027 /* ACV POR DESMA*/                                                               ");
		stringBuilder.append("               ,300373 /* AMORT VALORES DPF */                                                          ");
		stringBuilder.append("               )                                                                                        ");
		stringBuilder.append("                                                                                                        ");
		
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			stringBuilder.append("  AND HAM.ID_SECURITY_CODE_FK = :idSecurityCodePk                                                   ");
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getIdCustodyOperation())){
			stringBuilder.append("  AND HAM.ID_CUSTODY_OPERATION_FK = :idCustodyOperation                                             ");
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getIdNegotiationOperation())){
			stringBuilder.append("  AND HAM.ID_TRADE_OPERATION_FK = :idNegotiationOperation                                           ");
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getIdCorporativeOperation())){
			stringBuilder.append("  AND HAM.ID_CORPORATIVE_OPERATION_FK = :idCorporativeOperation                                     ");
		}
		 
		stringBuilder.append("  ORDER BY  OP_CUSTODIA, OP_NEGOCIACION, OP_CORPORATIVO, HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK              ");
		
		Query query = em.createNativeQuery(stringBuilder.toString());	
		
		if( Validations.validateIsNotNullAndNotEmpty(filter.getExecutionDate())){
			query.setParameter("execution", CommonsUtilities.convertDatetoString(filter.getExecutionDate()));
		}
		if( Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityCodePk())){
			query.setParameter("idSecurityCodePk", filter.getIdSecurityCodePk());
		}
		if( Validations.validateIsNotNullAndNotEmpty(filter.getIdCustodyOperation())){
			query.setParameter("idCustodyOperation", Long.valueOf(filter.getIdCustodyOperation()));
		}
		if( Validations.validateIsNotNullAndNotEmpty(filter.getIdNegotiationOperation())){
			query.setParameter("idNegotiationOperation", Long.valueOf(filter.getIdNegotiationOperation()));
		}
		if( Validations.validateIsNotNullAndNotEmpty(filter.getIdCorporativeOperation())){
			query.setParameter("idCorporativeOperation", Long.valueOf(filter.getIdCorporativeOperation()));
		}
		
		List<Object[]>  objectList = query.getResultList();
		
		List<AccountingDataMonitorTo> listAccountingDataMonitor  = new ArrayList<AccountingDataMonitorTo>();
		
		for(int i=0;i<objectList.size();++i){	
			
			Object[] sResults = (Object[])objectList.get(i);
			
			AccountingDataMonitorTo accountingDataMonitorTo = new AccountingDataMonitorTo();
			
			accountingDataMonitorTo.setMovementName(sResults[0] == null ? "" : sResults[0].toString()); 
			accountingDataMonitorTo.setIdSecurityCodePk(sResults[1] == null ? "" :sResults[1].toString()); 
			accountingDataMonitorTo.setParticipantSource(sResults[2] == null ? "" :sResults[2].toString()); 
			accountingDataMonitorTo.setParticipantTarget(sResults[3] == null? "" :sResults[3].toString()); 
			accountingDataMonitorTo.setHolderAccountTarget(sResults[4] == null? "" :sResults[4].toString()); 
			accountingDataMonitorTo.setStockQuantity(sResults[5] == null ? null : Integer.parseInt(sResults[5].toString()));
			accountingDataMonitorTo.setIdCustodyOperation(sResults[6] == null ? "" : sResults[6].toString()); 
			accountingDataMonitorTo.setIdNegotiationOperation(sResults[7] == null ? "" : sResults[7].toString()); 
			accountingDataMonitorTo.setIdCorporativeOperation(sResults[8] == null ? "" : sResults[8].toString()); 
			accountingDataMonitorTo.setIdHolderAccountMovementPk(Long.valueOf(sResults[9].toString()));
			
			accountingDataMonitorTo.setIdSourceParticipantPk(Long.valueOf(sResults[10].toString()));
			accountingDataMonitorTo.setIdTargetParticipantPk(Long.valueOf(sResults[11].toString()));
			accountingDataMonitorTo.setIdHolderAccountPk(Long.valueOf(sResults[12].toString()));
			accountingDataMonitorTo.setMovementQuantity(Long.valueOf(sResults[13].toString()));
			
			listAccountingDataMonitor.add(accountingDataMonitorTo);
			 
		}
		
		return listAccountingDataMonitor;
	}
	 
	/**
	 * Save accounting process.
	 *
	 * @param accountingProcess the accounting process
	 * @throws ServiceException the service exception
	 */
	public void saveAccountingProcess(AccountingProcess accountingProcess) throws ServiceException{
		create(accountingProcess);
	}
	
	/**
	 * Update accounting process.
	 *
	 * @param accountingProcess the accounting process
	 * @throws ServiceException the service exception
	 */
	public void updateAccountingProcess(AccountingProcess accountingProcess) throws ServiceException{
		update(accountingProcess);
	}
	
	/**
	 * Save accounting receipt.
	 *
	 * @param accountingReceipt the accounting receipt
	 */
	public void saveAccountingReceipt(AccountingReceipt accountingReceipt) {
		create(accountingReceipt);
	}
	
	/**
	 * Save accounting receipt detail.
	 *
	 * @param accountingReceiptDetail the accounting receipt detail
	 * @throws ServiceException the service exception
	 */
	public void saveAccountingReceiptDetail(AccountingReceiptDetail accountingReceiptDetail) throws ServiceException{
		create(accountingReceiptDetail);
	}
	
	/**
	 * Find accounting process.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<Object[]> findAccountingProcess(AccountingProcessTo filter){

		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select acp.id_accounting_process_pk,acp.execution_date, ");// 0   1
		sbQuery.append("   acr.id_accounting_receipts_pk, acr.receipt_amount,acr.receipt_date,  "); //  2  3  4
		sbQuery.append("   acrd.number_receipt_detail,acrd.document_date, acrd.amount, acrd.gloss,  ");//  5  6  7  8
		sbQuery.append("   aca.id_accounting_account_pk,aca.account_code,aca.description,aca.nature_type   ");//  9  10  11  12
		sbQuery.append("   from accounting_process acp  ");
		sbQuery.append("   inner join accounting_receipt acr on acr.id_accounting_process_fk=acp.id_accounting_process_pk  ");
		sbQuery.append("   inner join accounting_receipt_detail acrd on acrd.id_accounting_receipts_fk=acr.id_accounting_receipts_pk  ");
		sbQuery.append("   inner join accounting_matrix_detail acmd on acmd.ID_ACCOUNTING_MATRIX_DETAIL_PK=acrd.ID_ACCOUNTING_MATRIX_DETAIL_FK  ");
		sbQuery.append("   inner join accounting_matrix acm on acmd.ID_ACCOUNTING_MATRIX_FK=acm.ID_ACCOUNTING_MATRIX_PK  ");
		sbQuery.append("   inner join accounting_account aca on aca.id_accounting_account_pk=acmd.id_accounting_account_fk  ");
		
		sbQuery.append("  WHERE   1=1 ");
		if(Validations.validateIsNotNull(filter.getExecutionDate())){
			sbQuery.append(" acp.execution_date=:executionDate  " );
		}
		sbQuery.append("   " );
    	Query query = em.createNativeQuery(sbQuery.toString());
    	if(Validations.validateIsNotNull(filter.getExecutionDate())){
    		query.setParameter("executionDate", filter.getExecutionDate());
	    	
		}
		
		
    	return (List<Object[]>)query.getResultList();
    	
	}
	
	
	/**
	 * Find accunting receipt by id accounting process.
	 *
	 * @param idAccountingProcess the id accounting process
	 * @return the list
	 */
	public List<AccountingReceipt> findAccuntingReceiptByIdAccountingProcess(Long idAccountingProcess ){
		
		
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select distinct ar From AccountingReceipt ar " );
     
        if(Validations.validateIsNotNull(idAccountingProcess)){
        
            sbQuery.append(" Where  ar.accountingProcess.idAccountingProcessPk = :idAccountingProcessPk ");
        }
        
        Query query = em.createQuery(sbQuery.toString());
        if(Validations.validateIsNotNull(idAccountingProcess)){
        
            query.setParameter("idAccountingProcessPk", idAccountingProcess);
        }
        
       
        List<AccountingReceipt> accountingReceiptList = null;
        accountingReceiptList = query.getResultList();
        return accountingReceiptList;
	}
	
	
	/**
	 * Find accounting process with accunting receipt.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the list
	 */
	public List<AccountingProcess> findAccountingProcessWithAccuntingReceipt(AccountingProcessTo accountingProcessTo ){

		log.info(" GETTING ALL ACCOUNTING RECEIPT WITH DETAILS AND LOGGERS ");
		
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select distinct ap From AccountingProcess ap left join fetch ap.accountingReceipt ar  " );
        sbQuery.append(" left join  ar.accountingReceiptDetail ard left join ard.accountingSourceLogger asl  " );
        sbQuery.append(" left join  asl.accountingSourceDetail asd  " );
        sbQuery.append(" where 1=1 " );
        
        if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())) {
        	
        	 sbQuery.append(" and   ap.idAccountingProcessPk = :idAccountingProcessPk ");
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
        
            sbQuery.append(" and   ap.processType = :processType ");
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
            
            sbQuery.append(" and  trunc(ap.executionDate) = :executionDate ");
        }
        
        
        Query query = em.createQuery(sbQuery.toString());
        
        if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())) {
        	
        	query.setParameter("idAccountingProcessPk", accountingProcessTo.getIdAccountingProcessPk());
        	
        }
        if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
        
            query.setParameter("processType", accountingProcessTo.getProcessType());
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
            
        	query.setParameter("executionDate",  accountingProcessTo.getExecutionDate(),TemporalType.DATE);
        }
        
        List<AccountingProcess> accountingProcessList = null;
        accountingProcessList = query.getResultList();
        
        return accountingProcessList;
              
	}
	
	/**
	 * Find accounting process by filter.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the list
	 */
	public List<AccountingProcess> findAccountingProcessByFilter(AccountingProcessTo accountingProcessTo ){
		
		/**
		 * AccountingProcess(Long idAccountingProcessPk, Integer processType,
				Integer status, Date executionDate, Date operationDate,
				String descriptionProcessType, String calculateDate) 
		 */
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select new  AccountingProcess(   ");
		sbQuery.append(" acp.idAccountingProcessPk, ");
		sbQuery.append(" acp.processType, acp.status,  ");
		sbQuery.append(" acp.executionDate, acp.operationDate, ");
		sbQuery.append(" acp.nameFile, ");
		sbQuery.append(" (select p.description from AccountingParameter p where p.idAccountingParameterPk = acp.processType) ");
		sbQuery.append(" ) ");
		sbQuery.append(" From AccountingProcess acp");
		sbQuery.append(" Where ");

		sbQuery.append("  trunc(acp.executionDate) between trunc(:startDate) and trunc(:endDate) ");
		if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getStatus())){
			sbQuery.append(" AND  acp.status= :processedState ");
		}
	
		if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
			sbQuery.append(" AND acp.idAccountingProcessPk = :idAccountingProcessPk");
		}
		
		sbQuery.append(" ORDER BY acp.executionDate desc  ");
		
		TypedQuery<AccountingProcess> query =  em.createQuery(sbQuery.toString(),AccountingProcess.class);

		if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getStatus())){

			query.setParameter("processedState", accountingProcessTo.getStatus() );
		}
		
		query.setParameter("startDate", accountingProcessTo.getInitialDate(),TemporalType.DATE);
		query.setParameter("endDate", accountingProcessTo.getFinalDate(),TemporalType.DATE);

		if(Validations.validateIsNotNullAndPositive(accountingProcessTo.getIdAccountingProcessPk())){
			query.setParameter("idAccountingProcessPk", accountingProcessTo.getIdAccountingProcessPk());
		}
		
		List<AccountingProcess> listAccountingProcess = query.getResultList();
		
		return listAccountingProcess;
	}
	
	
	/**
	 * Busqueda de los registros para re inicio de saldos contables
	 * */
	public List<AccountingProcess> findBalanceStartRecordByFilter(FilterStartBalanceTO searchFilterStartBalanceTO ){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ap from AccountingProcess ap ");
		sbQuery.append(" where 1 = 1 ");
		
		if(searchFilterStartBalanceTO.getExecutionDate()!=null)
			sbQuery.append(" and ap.executionDate = :operationDate ");
		
		if(searchFilterStartBalanceTO.getAuxiliaryType()!=null)
			sbQuery.append(" and ap.processType = :processType ");
		
		sbQuery.append("and ap.startType = :startType ");
		
		TypedQuery<AccountingProcess> query =  em.createQuery(sbQuery.toString(),AccountingProcess.class);
		
		if(searchFilterStartBalanceTO.getExecutionDate()!=null)
			query.setParameter("operationDate", searchFilterStartBalanceTO.getExecutionDate());
		
		if(searchFilterStartBalanceTO.getAuxiliaryType()!=null)
			query.setParameter("processType",searchFilterStartBalanceTO.getAuxiliaryType());

		query.setParameter("startType",AccountingStartType.START.getCode());
		
		List<AccountingProcess> listAccountingProcess = query.getResultList();
		
		return listAccountingProcess;
	}
	

	/**
	 * Busqueda de registros para Proceso mensual
	 * @param searchFilterStartBalanceTO
	 * @return
	 */
	public List<AccountingProcess> findMonthlyProcessRecordByFilter(FilterStartBalanceTO searchFilterStartBalanceTO ){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ap from AccountingProcess ap ");
		sbQuery.append(" where 1 = 1 ");
		
		if(searchFilterStartBalanceTO.getExecutionDate()!=null) {
			sbQuery.append(" and ap.executionDate = :operationDate ");
		}
			
		
		if(Validations.validateIsNotNullAndPositive(searchFilterStartBalanceTO.getAuxiliaryType())) {
			sbQuery.append(" and ap.processType = :processType ");
		}
			
		sbQuery.append("and ap.startType = :startType ");
		
		TypedQuery<AccountingProcess> query =  em.createQuery(sbQuery.toString(),AccountingProcess.class);
		
		if(searchFilterStartBalanceTO.getExecutionDate()!=null) {
			query.setParameter("operationDate", searchFilterStartBalanceTO.getExecutionDate());
		}
			
		
		if(Validations.validateIsNotNullAndPositive(searchFilterStartBalanceTO.getAuxiliaryType())) {
			query.setParameter("processType",searchFilterStartBalanceTO.getAuxiliaryType());
		}
		
		query.setParameter("startType",AccountingStartType.MONTHLY.getCode());
		
		
		List<AccountingProcess> listAccountingProcess = query.getResultList();
		
		return listAccountingProcess;
	}
	
	
	/**
	 * Save list object.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @param numberbatch the numberbatch
	 * @throws ServiceException the service exception
	 */
	public <T> void saveListObject(List<T> t,int numberbatch) throws ServiceException {
		persistForBatch(t,numberbatch);
	}

	/**
	 * Delete accounting receipt detail batch.
	 *
	 * @param listReceiptDetailPk the list receipt detail pk
	 */
	public void deleteAccountingReceiptDetailBatch(List<Long> listReceiptDetailPk){

		if(Validations.validateListIsNotNullAndNotEmpty(listReceiptDetailPk)){
			if(listReceiptDetailPk.size()<=1000){
				
				deleteAccountingReceiptDetailByPk(listReceiptDetailPk);
							
			 }else{

				 for (int i = 0; i < listReceiptDetailPk.size();) {			 
					 int ifinal=i+1000-1;
					 if(ifinal>=listReceiptDetailPk.size()){
						 ifinal=listReceiptDetailPk.size();					 
					 }

					 deleteAccountingReceiptDetailByPk(listReceiptDetailPk.subList(i, ifinal));
					 
					 em.flush();
					 em.clear();
					 i=i+1000;
				}
				 
			 }

		}
		log.info(":::Finish Deleted Accounting Source Logger And Accounting Source Detail");
	
	}


	/**
	 * Delete accounting receipt detail batch.
	 *
	 * @param listReceiptDetailPk the list receipt detail pk
	 */
	public void deleteAccountingReceiptAndDetailBatch(List<Long> listReceiptPk){

		if(Validations.validateListIsNotNullAndNotEmpty(listReceiptPk)){
			if(listReceiptPk.size()<=1000){
				
				deleteAccountingReceipt(listReceiptPk);
				
				
			 }else{

				 for (int i = 0; i < listReceiptPk.size();) {			 
					 int ifinal=i+1000-1;
					 if(ifinal>=listReceiptPk.size()){
						 ifinal=listReceiptPk.size();					 
					 }

					 deleteAccountingReceipt(listReceiptPk.subList(i, ifinal));
					 
					 em.flush();
					 em.clear();
					 i=i+1000;
				}
				 
			 }

		}
		log.info(":::Finish Deleted Accounting Source Logger And Accounting Source Detail");
	
	}
	
	/**
	 * Delete accounting source logger and source detail batch.
	 *
	 * @param listSourceLoggerPk the list source logger pk
	 */
	public void deleteAccountingSourceLoggerAndSourceDetailBatch(List<Long> listSourceLoggerPk){

		if(Validations.validateListIsNotNullAndNotEmpty(listSourceLoggerPk)){
			if(listSourceLoggerPk.size()<=1000){

				deleteAccountingSourceSourceDetail(listSourceLoggerPk);
				deleteAccountingSourceLogger(listSourceLoggerPk);
				
			 }else{
				 
				 for (int i = 0; i < listSourceLoggerPk.size();) {
					 int ifinal=i+1000-1;
					 if(ifinal>=listSourceLoggerPk.size()){
						 ifinal=listSourceLoggerPk.size();
					 }

					 deleteAccountingSourceSourceDetail(listSourceLoggerPk.subList(i, ifinal));
					 em.flush();
					 em.clear();
					 deleteAccountingSourceLogger(listSourceLoggerPk.subList(i, ifinal));
					 em.flush();
					 em.clear();
					 i=i+1000;
				}
				 
			 }

		}
		log.info(":::Finish Deleted Accounting Source Logger And Accounting Source Detail");
	
	}
	
	
	/**
	 * Delete accounting receipt detail.
	 *
	 * @param listReceiptDetailPk the list receipt detail pk
	 */
	public void deleteAccountingReceipt(List<Long> listReceiptPk){

        StringBuilder sbQuerydetail = new StringBuilder();
        sbQuerydetail.append(" DELETE FROM AccountingReceipt AR  " ).
        		append(" WHERE AR.idAccountingReceiptsPk ").
        		append(" IN (:listReceiptPk)");
     

        
        Query query = em.createQuery(sbQuerydetail.toString());
        if(Validations.validateListIsNotNullAndNotEmpty(listReceiptPk)){
        
            query.setParameter("listReceiptPk", listReceiptPk);
        }
        

        int deletedCount=query.executeUpdate();
        log.info(":::Deleted "+deletedCount+" AccountingReceipt");
	
	}
	
	/**
	 * Delete accounting receipt detail.
	 *
	 * @param listReceiptDetailPk the list receipt detail pk
	 */
	public void deleteAccountingReceiptDetail(List<Long> listReceiptPk){

        StringBuilder sbQuerydetail = new StringBuilder();
        sbQuerydetail.append(" DELETE FROM AccountingReceiptDetail ARD  " ).
        		append(" WHERE ARD.accountingReceipt.idAccountingReceiptsPk ").
        		append(" IN (:listReceiptFk)");

        Query query = em.createQuery(sbQuerydetail.toString());
        if(Validations.validateListIsNotNullAndNotEmpty(listReceiptPk)){
        
            query.setParameter("listReceiptFk", listReceiptPk);
        }        

        int deletedCount=query.executeUpdate();
        log.info(":::Deleted "+deletedCount+" AccountingReceiptDetail");
	
	}
	
	/**
	 * Delete accounting source logger.
	 *
	 * @param listSourceLoggerPk the list source logger pk
	 */
	public void deleteAccountingSourceLogger(List<Long> listSourceLoggerPk){

        StringBuilder sbQuerydetail = new StringBuilder();
        sbQuerydetail.append(" DELETE FROM AccountingSourceLogger ASL  " ).
        		append(" WHERE ASL.idAccountingSourceLoggerPk ").
        		append(" IN (:listSourceLoggerPk)");
     

        
        Query query = em.createQuery(sbQuerydetail.toString());
        if(Validations.validateListIsNotNullAndNotEmpty(listSourceLoggerPk)){
        
            query.setParameter("listSourceLoggerPk", listSourceLoggerPk);
        }
        

        int deletedCount=query.executeUpdate();
        log.info(":::Deleted "+deletedCount+" AccountingSourceLogger");
	
	}
	
	/**
	 * Delete accounting source source detail.
	 *
	 * @param listSourceLoggerPk the list source logger pk
	 */
	public void deleteAccountingSourceSourceDetail(List<Long> listSourceLoggerPk){

        StringBuilder sbQuerydetail = new StringBuilder();
        sbQuerydetail.append(" DELETE FROM AccountingSourceDetail ASD  " ).
        		append(" WHERE ASD.accountingSourceLogger.idAccountingSourceLoggerPk ").
        		append(" IN (:listSourceLoggerPk)");
        
        Query query = em.createQuery(sbQuerydetail.toString());
        if(Validations.validateListIsNotNullAndNotEmpty(listSourceLoggerPk)){
            query.setParameter("listSourceLoggerPk", listSourceLoggerPk);
        }

        int deletedCount=query.executeUpdate();
        log.info(":::Deleted "+deletedCount+" AccountingSourceDetail");
	
	}
	
	/**
	 * Delete accounting receipt detail.
	 *
	 * @param listReceiptDetailPk the list receipt detail pk
	 */
	public void deleteAccountingReceiptDetailByPk(List<Long> listReceiptDetailPk){

        StringBuilder sbQuerydetail = new StringBuilder();
        sbQuerydetail.append(" DELETE FROM AccountingReceiptDetail ARD  " ).
        		append(" WHERE ARD.idAccountingReceiptsDetailPk ").
        		append(" IN (:listReceiptDetailPk)");

        Query query = em.createQuery(sbQuerydetail.toString());
        if(Validations.validateListIsNotNullAndNotEmpty(listReceiptDetailPk)){
        
            query.setParameter("listReceiptDetailPk", listReceiptDetailPk);
        }        

        int deletedCount=query.executeUpdate();
        log.info(":::Deleted "+deletedCount+" AccountingReceiptDetail");
	
	}
	
	
	/**
	 * Execute All Package Refresh
	 */
	public void executeRefreshViewAccountingReports(){
		long inicio = System.currentTimeMillis();
		executeRefreshAvailableBalance();
		executeRefreshReportedBalance();
		executeRefreshReportingBalance();
		executeRefreshBlockBalance();
		executeRefreshOtherBlockBalance();
		executeRefreshPhysicalBalance();
		executeRefreshNotPlaced();
		executeRefreshUnblockPawn();
		
		long fin = System.currentTimeMillis();
		log.info("TERMINOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO REFRESH VIEW CONTA: " + (fin - inicio));
	}
	
	/**
	 * Execute DBMS View Refresh: View Available Balance
	 */
	@Performance
	public void executeRefreshUnblockPawn(){
		StringBuilder sb= new StringBuilder();
		log.info("INICIO: Refreshing View Materialized VIEW_UNBLOCK_PAWN finish succesfull  ");
		long inicio = System.currentTimeMillis();
		sb.append(" BEGIN DBMS_MVIEW.REFRESH('VIEW_UNBLOCK_PAWN'); END; ");
		Query query = em.createNativeQuery(sb.toString());
		query.executeUpdate();
		long fin = System.currentTimeMillis();
		log.info("FIN: Refreshing View Materialized VIEW_UNBLOCK_PAWN finish succesfull:  " + (fin - inicio));
		
	}
	
	/**
	 * Execute DBMS View Refresh: View Available Balance
	 */
	@Performance
	public void executeRefreshAvailableBalance(){
		StringBuilder sb= new StringBuilder();
		log.info("INICIO: Refreshing View Materialized VIEW_AVAILABLE_BALANCE finish succesfull  ");
		long inicio = System.currentTimeMillis();
		sb.append(" BEGIN DBMS_MVIEW.REFRESH('VIEW_AVAILABLE_BALANCE'); END; ");
		Query query = em.createNativeQuery(sb.toString());
		query.executeUpdate();
		long fin = System.currentTimeMillis();
		log.info("FIN: Refreshing View Materialized VIEW_AVAILABLE_BALANCE finish succesfull  " + (fin - inicio));
		
	}
	
	/**
	 * Execute DBMS View Refresh: View Reported Balance
	 */
	@Performance
	public void executeRefreshReportedBalance(){
		StringBuilder sb= new StringBuilder();
		log.info("INICIO: Refreshing View Materialized VIEW_REPORTED_BALANCE finish succesfull  ");
		long inicio = System.currentTimeMillis();
		sb.append(" BEGIN DBMS_MVIEW.REFRESH('VIEW_REPORTED_BALANCE'); END; ");
		Query query = em.createNativeQuery(sb.toString());
		query.executeUpdate();
		long fin = System.currentTimeMillis();
		log.info("FIN: Refreshing View Materialized VIEW_REPORTED_BALANCE finish succesfull  " + (fin - inicio));
		
		
	}
	
	/**
	 * Execute DBMS View Refresh: View Reporting Balance
	 */
	@Performance
	public void executeRefreshReportingBalance(){
		StringBuilder sb= new StringBuilder();
		log.info("INICIO: Refreshing View Materialized VIEW_BLOCK_BALANCE finish succesfull  ");
		long inicio = System.currentTimeMillis();
		sb.append(" BEGIN DBMS_MVIEW.REFRESH('VIEW_REPORTING_BALANCE'); END; ");
		Query query = em.createNativeQuery(sb.toString());
		query.executeUpdate();
		long fin = System.currentTimeMillis();
		log.info("FIN: Refreshing View Materialized VIEW_REPORTING_BALANCE finish succesfull  " + (fin - inicio));
		
	}
	
	/**
	 * Execute DBMS View Refresh: View Block Balance
	 */
	@Performance
	public void executeRefreshBlockBalance(){
		StringBuilder sb= new StringBuilder();
		log.info("INICIO: Refreshing View Materialized VIEW_BLOCK_BALANCE finish succesfull  ");
		long inicio = System.currentTimeMillis();		
		sb.append(" BEGIN DBMS_MVIEW.REFRESH('VIEW_BLOCK_BALANCE'); END; ");
		Query query = em.createNativeQuery(sb.toString());
		query.executeUpdate();
		long fin = System.currentTimeMillis();
		log.info("FIN: Refreshing View Materialized VIEW_BLOCK_BALANCE finish succesfull  " + (fin - inicio));
		
	}
	
	/**
	 * Execute DBMS View Refresh: View Other Block Balance
	 */
	@Performance
	public void executeRefreshOtherBlockBalance(){
		StringBuilder sb= new StringBuilder();
		log.info("INICIO: Refreshing View Materialized VIEW_OTHER_BLOCK_BALANCE finish succesfull  ");
		long inicio = System.currentTimeMillis();
		sb.append(" BEGIN DBMS_MVIEW.REFRESH('VIEW_OTHER_BLOCK_BALANCE'); END; ");
		Query query = em.createNativeQuery(sb.toString());
		query.executeUpdate();
		long fin = System.currentTimeMillis();
		log.info("FIN: Refreshing View Materialized VIEW_OTHER_BLOCK_BALANCE finish succesfull  " + (fin - inicio));
		
	}
	
	/**
	 * Execute DBMS View Refresh: View Physical Balance
	 */
	@Performance
	public void executeRefreshPhysicalBalance(){
		StringBuilder sb= new StringBuilder();
		log.info("INICIO: Refreshing View Materialized VIEW_PHYSICAL_BALANCE finish succesfull  ");
		long inicio = System.currentTimeMillis();
		sb.append(" BEGIN DBMS_MVIEW.REFRESH('VIEW_PHYSICAL_BALANCE'); END; ");
		Query query = em.createNativeQuery(sb.toString());
		query.executeUpdate();
		long fin = System.currentTimeMillis();
		log.info("FIN: Refreshing View Materialized VIEW_PHYSICAL_BALANCE finish succesfull  " + (fin - inicio));
		
	}
	
	/**
	 * Execute DBMS View Refresh: View Not Placed
	 */
	@Performance
	public void executeRefreshNotPlaced(){
		StringBuilder sb= new StringBuilder();
		log.info("INICIO: Refreshing View Materialized VIEW_NOT_PLACED finish succesfull ");
		long inicio = System.currentTimeMillis();
		sb.append(" BEGIN DBMS_MVIEW.REFRESH('VIEW_NOT_PLACED'); END; ");
		Query query = em.createNativeQuery(sb.toString());
		query.executeUpdate();
		long fin = System.currentTimeMillis();
		log.info("FIN: Refreshing View Materialized VIEW_NOT_PLACED finish succesfull " + (fin - inicio));
		
	}
	
	/**
	 * Registro del historico de contabilidad a una determinada fecha de corte**
	 * @param operationDate
	 * @param idAccountingProcessHistoryFk
	 * @param userInfo
	 * @return
	 */
//	public List<AccountingProcessHistoryDetail> getListAccountProcessHistoryDetails(Date operationDate, Long idAccountingProcessHistoryFk,UserInfo userInfo){
//		StringBuffer sbQuery=new StringBuffer();
//		sbQuery.append(" SELECT BASE.ACCOUNT_CODE AS CUENTA_PART ");
//		sbQuery.append(" ,SUM(BASE.AMOUNT) AS TOTAL_MONTO ");
//		sbQuery.append(" ,ID_ACCOUNTING_ACCOUNT_PK ");
//		sbQuery.append(" ,ID_PARTICIPANT_PK ");
//		
//		sbQuery.append(" FROM(  SELECT  ");
//		sbQuery.append(" AR.ID_ACCOUNTING_PROCESS_FK, ");
//		sbQuery.append(" AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK, ");
//		sbQuery.append(" decode(ACC.IND_REFERENCE, 0, ACC.ACCOUNT_CODE||'.'||PA.MNEMONIC, 1, ACC.ACCOUNT_CODE) AS ACCOUNT_CODE, ");
//		sbQuery.append(" decode(AMD.DYNAMIC_TYPE,9,ARD.AMOUNT,10,-ARD.AMOUNT) as AMOUNT , ");
//		sbQuery.append(" ACC.ID_ACCOUNTING_ACCOUNT_PK as ID_ACCOUNTING_ACCOUNT_PK ");
//		sbQuery.append(" ,AR.KEY_ASSOCIATE as ID_PARTICIPANT_PK ");
//		sbQuery.append(" FROM ");
//		sbQuery.append(" ACCOUNTING_PROCESS AP ");
//		sbQuery.append(" INNER JOIN ACCOUNTING_RECEIPT AR ON AP.ID_ACCOUNTING_PROCESS_PK=AR.ID_ACCOUNTING_PROCESS_FK ");
//		sbQuery.append(" INNER JOIN ACCOUNTING_RECEIPT_DETAIL ARD ON AR.ID_ACCOUNTING_RECEIPTS_PK=ARD.ID_ACCOUNTING_RECEIPTS_FK   ");
//		sbQuery.append(" INNER JOIN ACCOUNTING_SOURCE_LOGGER ASL ON ARD.ID_ACCOUNTING_RECEIPT_DET_PK=ASL.ID_ACCOUNTING_RECEIPT_DET_FK  ");
//		sbQuery.append(" INNER JOIN ACCOUNTING_SOURCE_DETAIL ASD ON ASL.ID_ACCOUNTING_SOURCE_LOGGER_PK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK   ");
//		sbQuery.append(" INNER JOIN ACCOUNTING_SOURCE_DETAIL ASD2 ON ASD2.ID_ACCOUNTING_SOURCE_LOGGER_FK=ASD.ID_ACCOUNTING_SOURCE_LOGGER_FK ");
//		sbQuery.append(" INNER JOIN ACCOUNTING_MATRIX_DETAIL AMD ON ARD.ID_ACCOUNTING_MATRIX_DETAIL_FK=AMD.ID_ACCOUNTING_MATRIX_DETAIL_PK ");
//		sbQuery.append(" INNER JOIN ACCOUNTING_ACCOUNT ACC ON AMD.ID_ACCOUNTING_ACCOUNT_FK=ACC.ID_ACCOUNTING_ACCOUNT_PK ");
//		sbQuery.append(" INNER JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK=ASD.PARAMETER_VALUE ");
//		sbQuery.append(" wHERE 1 = 1 ");
//		sbQuery.append(" AND ASD2.PARAMETER_TYPE = 2105 ");
//		sbQuery.append(" and ASD2.PARAMETER_NAME = 'MOVEMENT_TYPE' ");
//		sbQuery.append(" and ASD.PARAMETER_TYPE = 2107 ");
//		sbQuery.append(" and ASD.PARAMETER_NAME = 'PARTICIPANT' ");
//		sbQuery.append(" and AP.PROCESS_TYPE = 12 ");
//		sbQuery.append(" and trunc(AP.EXECUTION_DATE) = :operationDate ");
//		sbQuery.append(" ORDER BY ACC.ACCOUNT_CODE, AMD.DYNAMIC_TYPE, ARD.AMOUNT ,ID_ACCOUNTING_ACCOUNT_PK  ");
//		sbQuery.append(" )  BASE  ");
//		sbQuery.append(" group by BASE.ACCOUNT_CODE  , ID_ACCOUNTING_ACCOUNT_PK    ,ID_PARTICIPANT_PK   ");
//		sbQuery.append(" ORDER BY BASE.ACCOUNT_CODE  ");
//		
//		
// 
//		Query query = em.createNativeQuery(sbQuery.toString()); 		
//		
//		query.setParameter("operationDate", operationDate );	
//		
//		List<Object[]>  objectList = query.getResultList();
//		
//		List<AccountingProcessHistoryDetail> list = new ArrayList<>();
//		AccountingProcessHistoryDetail accountingProcessHistoryDetail ;
//		
//		for (Object[] objects : objectList) {
//			accountingProcessHistoryDetail = new AccountingProcessHistoryDetail();
//			accountingProcessHistoryDetail.setIdAccountingProcessHistoryFk(idAccountingProcessHistoryFk);
//			//accountingProcessHistoryDetail.setAccountDesc(objects[0].toString());
//			accountingProcessHistoryDetail.setAmount(new BigDecimal(objects[1].toString()));
//			accountingProcessHistoryDetail.setIdAccountingAccountPk(new BigDecimal(objects[2].toString()).longValue());
//			//accountingProcessHistoryDetail.setIdParticipantPk(new BigDecimal(objects[3].toString()).longValue());
//			accountingProcessHistoryDetail.setState(ReStartBalancesType.REGISTERED.getCode());
//			accountingProcessHistoryDetail.setLastModifyApp(userInfo.getLastModifyPriv());
//			accountingProcessHistoryDetail.setLastModifyDate(CommonsUtilities.currentDate());
//			accountingProcessHistoryDetail.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
//			accountingProcessHistoryDetail.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
//			list.add(accountingProcessHistoryDetail);
//		}
//		return list;
//	}
}
