package com.pradera.accounting.document.service.sender;

import javax.ejb.Local;

import com.pradera.accounting.document.to.AccountingProcessSchemaTo;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.integration.exception.ServiceException;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface SenderAccountigService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Local 
public interface SenderAccountingPartialService {

	/**
	 * Send process batch generate document.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @throws ServiceException the service exception
	 */
	public void sendProcessBatchGenerateDocument(AccountingProcessTo accountingProcessTo) throws ServiceException;


	/**
	 * Send process batch Re start generate document.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @throws ServiceException the service exception
	 */
	public void sendProcessBatchReStartGenerateDocument(AccountingProcessSchemaTo accountingProcessTo) throws ServiceException ;
	
	/**
	 * Sender service for generate document.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	public void senderServiceForGenerateDocument(AccountingProcessTo accountingProcessTo);
	
	/**
	 * Prepare service for generate document.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	public void prepareServiceForGenerateDocument(AccountingProcessTo accountingProcessTo) ;
}


