package com.pradera.accounting.document.to;

import java.io.Serializable;
import java.util.Date;


public class FilterStartBalanceTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5908724314668958285L;

	private Integer auxiliaryType;
	
	private Date  executionDate;
	
	private String dataOrigin;
	
	public FilterStartBalanceTO() {
		dataOrigin = "1";
	}

	public Integer getAuxiliaryType() {
		return auxiliaryType;
	}

	public void setAuxiliaryType(Integer auxiliaryType) {
		this.auxiliaryType = auxiliaryType;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public String getDataOrigin() {
		return dataOrigin;
	}

	public void setDataOrigin(String dataOrigin) {
		this.dataOrigin = dataOrigin;
	}
}
