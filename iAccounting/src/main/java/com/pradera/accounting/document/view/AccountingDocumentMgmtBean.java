package com.pradera.accounting.document.view;


import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;

import com.pradera.accounting.document.facade.DocumentAccountingServiceFacade;
import com.pradera.accounting.document.to.AccountingDataMonitorTo;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.notification.facade.AccountingNotificationServiceFacade;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.type.AccountingParameterStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingDocumentMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */

@DepositaryWebBean
@LoggerCreateBean
public class AccountingDocumentMgmtBean extends GenericBaseBean {

	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5222378272513878921L;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	/** The log. */
	@Inject
	transient PraderaLogger log;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade	generalParametersFacade;
	
	/** The document accounting service facade. */
	@EJB
	DocumentAccountingServiceFacade documentAccountingServiceFacade;
	
	/** The parameter accounting service facade. */
	@EJB
	ParameterAccountingServiceFacade parameterAccountingServiceFacade;
	
	/** The accounting notification service facade. */
	@EJB
	AccountingNotificationServiceFacade accountingNotificationServiceFacade;


	/** The list parameter auxiliary type. */
	private List<AccountingParameter> listParameterAuxiliaryType;
	
	/** The accounting parameter to. */
	private AccountingParameterTO accountingParameterTo = new AccountingParameterTO();	
	
	/** The parameter auxiliary type selected. */
	private Integer parameterAuxiliaryTypeSelected;


	/** The change currency. */
	private BigDecimal changeCurrency = BigDecimal.ZERO;

	
	/** The list accounting process. */
	private List<AccountingProcess> listAccountingProcess ;
	
	private List<AccountingDataMonitorTo> listAccountingDataMonitorTos ;
	
	private List<AccountingDataMonitorTo> listAccountingDataMonitorToAux ;
	
	/** The accounting process data model. */
	private GenericDataModel<AccountingProcess> accountingProcessDataModel;
	
	private GenericDataModel<AccountingDataMonitorTo> accountingDataModel;
	
	private GenericDataModel<AccountingDataMonitorTo> accountingDataModifyModel;
	
	/** The accounting process selection. */
	private AccountingProcess accountingProcessSelection;
	
	private AccountingDataMonitorTo accountingDataMonitorTo;
	
	/** The execution date. */
	private Date  executionDate ;
	
	
	private Date  accountingDate ;
	
	private String  idCustodyOperation ;
	
	private String  idNegotiationOperation ;
	
	private String  idCorporativeOperation ;
	
	private String  idSecurityCodePk ;
	
	
	/** The today. */
	private Calendar today =  Calendar.getInstance(); 

	/** To Logger. */
	private Date validityInitialDate;
	
	/** The validity final date. */
	private Date validityFinalDate;
	
	/** The initial date. */
	private Date initialDate = CommonsUtilities.currentDate();
	
	/** The final date. */
	private Date finalDate = CommonsUtilities.currentDate();
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException  {

		//if (!FacesContext.getCurrentInstance().isPostback()){

			// Call to load cmbData
			loadCmbDataSearch();
			
			executionDate= today.getTime();
			accountingDate= today.getTime();
			
			changeCurrency = documentAccountingServiceFacade.findDailyChange();

		//}

	}
	
	

	/**
	 * Gets the list parameter auxiliary type.
	 *
	 * @return the listParameterAuxiliaryType
	 */
	public List<AccountingParameter> getListParameterAuxiliaryType() {
		return listParameterAuxiliaryType;
	}



	/**
	 * Sets the list parameter auxiliary type.
	 *
	 * @param listParameterAuxiliaryType the listParameterAuxiliaryType to set
	 */
	public void setListParameterAuxiliaryType(
			List<AccountingParameter> listParameterAuxiliaryType) {
		this.listParameterAuxiliaryType = listParameterAuxiliaryType;
	}



	/**
	 * Gets the accounting parameter to.
	 *
	 * @return the accountingParameterTo
	 */
	public AccountingParameterTO getAccountingParameterTo() {
		return accountingParameterTo;
	}



	/**
	 * Sets the accounting parameter to.
	 *
	 * @param accountingParameterTo the accountingParameterTo to set
	 */
	public void setAccountingParameterTo(AccountingParameterTO accountingParameterTo) {
		this.accountingParameterTo = accountingParameterTo;
	}



	/**
	 * Gets the parameter auxiliary type selected.
	 *
	 * @return the parameterAuxiliaryTypeSelected
	 */
	public Integer getParameterAuxiliaryTypeSelected() {
		return parameterAuxiliaryTypeSelected;
	}



	/**
	 * Sets the parameter auxiliary type selected.
	 *
	 * @param parameterAuxiliaryTypeSelected the parameterAuxiliaryTypeSelected to set
	 */
	public void setParameterAuxiliaryTypeSelected(
			Integer parameterAuxiliaryTypeSelected) {
		this.parameterAuxiliaryTypeSelected = parameterAuxiliaryTypeSelected;
	}



	/**
	 * Gets the change currency.
	 *
	 * @return the changeCurrency
	 */
	public BigDecimal getChangeCurrency() {
		return changeCurrency;
	}



	/**
	 * Sets the change currency.
	 *
	 * @param changeCurrency the changeCurrency to set
	 */
	public void setChangeCurrency(BigDecimal changeCurrency) {
		this.changeCurrency = changeCurrency;
	}



	/**
	 * Gets the list accounting process.
	 *
	 * @return the listAccountingProcess
	 */
	public List<AccountingProcess> getListAccountingProcess() {
		return listAccountingProcess;
	}



	/**
	 * Sets the list accounting process.
	 *
	 * @param listAccountingProcess the listAccountingProcess to set
	 */
	public void setListAccountingProcess(
			List<AccountingProcess> listAccountingProcess) {
		this.listAccountingProcess = listAccountingProcess;
	}



	/**
	 * Gets the accounting process data model.
	 *
	 * @return the accountingProcessDataModel
	 */
	public GenericDataModel<AccountingProcess> getAccountingProcessDataModel() {
		return accountingProcessDataModel;
	}



	/**
	 * Sets the accounting process data model.
	 *
	 * @param accountingProcessDataModel the accountingProcessDataModel to set
	 */
	public void setAccountingProcessDataModel(
			GenericDataModel<AccountingProcess> accountingProcessDataModel) {
		this.accountingProcessDataModel = accountingProcessDataModel;
	}



	/**
	 * Gets the accounting process selection.
	 *
	 * @return the accountingProcessSelection
	 */
	public AccountingProcess getAccountingProcessSelection() {
		return accountingProcessSelection;
	}



	/**
	 * Sets the accounting process selection.
	 *
	 * @param accountingProcessSelection the accountingProcessSelection to set
	 */
	public void setAccountingProcessSelection(
			AccountingProcess accountingProcessSelection) {
		this.accountingProcessSelection = accountingProcessSelection;
	}



	/**
	 * Gets the execution date.
	 *
	 * @return the executionDate
	 */
	public Date getExecutionDate() {
		return executionDate;
	}



	/**
	 * Sets the execution date.
	 *
	 * @param executionDate the executionDate to set
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}



	/**
	 * Gets the today.
	 *
	 * @return the today
	 */
	public Calendar getToday() {
		return today;
	}



	/**
	 * Sets the today.
	 *
	 * @param today the today to set
	 */
	public void setToday(Calendar today) {
		this.today = today;
	}


	/**
	 * Gets the validity initial date.
	 *
	 * @return the validityInitialDate
	 */
	public Date getValidityInitialDate() {
		return validityInitialDate;
	}



	/**
	 * Sets the validity initial date.
	 *
	 * @param validityInitialDate the validityInitialDate to set
	 */
	public void setValidityInitialDate(Date validityInitialDate) {
		this.validityInitialDate = validityInitialDate;
	}



	/**
	 * Gets the validity final date.
	 *
	 * @return the validityFinalDate
	 */
	public Date getValidityFinalDate() {
		return validityFinalDate;
	}



	/**
	 * Sets the validity final date.
	 *
	 * @param validityFinalDate the validityFinalDate to set
	 */
	public void setValidityFinalDate(Date validityFinalDate) {
		this.validityFinalDate = validityFinalDate;
	}



	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}



	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}



	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}



	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}



	/**
	 * *	Apply privileges to Rate Security	**.
	 */
	public void setTrueValidButtons(){        

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if(userPrivilege.getUserAcctions().isRegister()){
			privilegeComponent.setBtnBlockView(true);
		}
		if(userPrivilege.getUserAcctions().isAdjustments()){
			privilegeComponent.setBtnModifyView(true);    
		}
		if(userPrivilege.getUserAcctions().isDefinitive()){
			privilegeComponent.setBtnBlockView(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);

	}
	
	/**
	 * Load cmb data search.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void loadCmbDataSearch() throws ServiceException {

		// load dailyAuxiliaryType
		if(Validations.validateListIsNullOrEmpty(listParameterAuxiliaryType)){

			this.accountingParameterTo.setParameterType(PropertiesConstants.DAILY_AUXILIARY);
			this.accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			this.listParameterAuxiliaryType  =   parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
		} 
	}
	
	/**
	 * Go to.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {

			if (viewToGo.equals("prelimimary")) {

				viewToGo = "prelimimary";
				
			} 


		return viewToGo;
	}
	
	/**
	 * Load registration page.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void loadRegistrationPage() throws ServiceException {
		
		loadCmbDataSearch();
	}
	
	/**
	 * Search documents.
	 */
	@LoggerAuditWeb
	public void searchDocuments(){
		

		listAccountingProcess = new ArrayList<AccountingProcess>();
		AccountingProcess accountingProcessTemp = new AccountingProcess();
		if(Validations.validateIsNotNullAndPositive(parameterAuxiliaryTypeSelected)&&
				Validations.validateIsNotNull(executionDate)){
			if(Validations.validateIsNotNull(accountingProcessDataModel)){
				accountingProcessDataModel = new GenericDataModel<AccountingProcess>();
			}
			
			accountingProcessTemp.setProcessType(parameterAuxiliaryTypeSelected);   
			accountingProcessTemp.setExecutionDate(executionDate);		
			
			listAccountingProcess =  documentAccountingServiceFacade.loadAccountingProcess(accountingProcessTemp);
			
			if(Validations.validateIsNotNullAndNotEmpty(listAccountingProcess)){
				accountingProcessDataModel = new GenericDataModel<AccountingProcess>(listAccountingProcess);
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE ,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}
		
		/** clean selected on data table */
		this.accountingProcessSelection = null;	
		
	}
	
	
	/**
	 * 
	 * @return
	 */
	@LoggerAuditWeb
	public String loadRegisterAccountingMonitor(){
		try {
			
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			return "accountingMonitorRegister";
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	
	/**
	 * Metodo para buscar datos 
	 */
	@LoggerAuditWeb
	public void searchDataAccounting(){

		this.accountingDataModel = null;
		this.accountingDataModifyModel = null;
		
		listAccountingDataMonitorTos = new ArrayList<AccountingDataMonitorTo>();
		
		AccountingDataMonitorTo accountingDataMonitorTo = new AccountingDataMonitorTo();
		
		if( Validations.validateIsNotNull(accountingDate)
			/*Validations.validateIsNotNullAndPositive(parameterAuxiliaryTypeSelected) && */
				){
			
			if(Validations.validateIsNotNull(accountingDataModel)){
				accountingDataModel = new GenericDataModel<AccountingDataMonitorTo>();
			}
			
			/* Seteamos los filtros de la pantalla*/
			accountingDataMonitorTo.setExecutionDate(accountingDate);
			accountingDataMonitorTo.setIdCustodyOperation(idCustodyOperation);
			accountingDataMonitorTo.setIdNegotiationOperation(idNegotiationOperation);
			accountingDataMonitorTo.setIdCorporativeOperation(idCorporativeOperation);
			accountingDataMonitorTo.setIdSecurityCodePk(idSecurityCodePk);
			
			/* Ejecutamos la consulta*/
			listAccountingDataMonitorTos =  documentAccountingServiceFacade.loadDataAccounting(accountingDataMonitorTo);
			
			if(Validations.validateIsNotNullAndNotEmpty(listAccountingDataMonitorTos)){
				
				accountingDataModel = new GenericDataModel<AccountingDataMonitorTo>(listAccountingDataMonitorTos);
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE ,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}
		
		/** clean selected on data table */
		this.accountingProcessSelection = null;	
		
	}
	
	/**
	 * Metodo para mostrar la grilla de modificacion
	 * @param selectEvent
	 */
	public void test(SelectEvent selectEvent){
		
		AccountingDataMonitorTo accountingDataMonitorToX = (AccountingDataMonitorTo) selectEvent.getObject();
		
		List<AccountingDataMonitorTo> listAccountingDataMonitorModify = new ArrayList<AccountingDataMonitorTo>();
		
		//Lista auxiliar temporal cada vez q hacemos clic sobre un elemento de la lista
		listAccountingDataMonitorToAux = new ArrayList<AccountingDataMonitorTo>();
		
		listAccountingDataMonitorToAux.add(accountingDataMonitorToX);
		
		//Para mostrar en pantalla de registro
		listAccountingDataMonitorModify.add(accountingDataMonitorToX);
		
		accountingDataModifyModel = new GenericDataModel<AccountingDataMonitorTo>(listAccountingDataMonitorModify);
	}
	
	/**
	 * Mensaje de confirmacion del registro de modificacion de movimiento SI o NO
	 * @param event
	 * @throws ServiceException
	 */
	@LoggerAuditWeb
	public void beforeRegisterModifyData() throws ServiceException{
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.REGISTER_MODIFY_DATA_ACCOUNTNG));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmAdjustmentsDialog.show();"); 
	}
	

	/**
	 * Before process preliminary execution.
	 */
	public void beforeProcessPreliminaryExecution()  {
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
				, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PROCESS_ACCOUNTING_SEND_GENERATION));
		JSFUtilities.executeJavascriptFunction("cnfwProcessSendGeneration.show();");
	}
	
	/**
	 * Button Test 
	 */
	@LoggerAuditWeb
	public void deleteProcess(){
		//action="#{accountingDocumentMgmtBean.goTo('generateDocument')}"
		//process="@this"
		//immediate="true"
		AccountingProcessTo accountingProcessTo = new AccountingProcessTo();
		accountingProcessTo.setProcessType(this.parameterAuxiliaryTypeSelected);
		accountingProcessTo.setExecutionDate(this.executionDate);
		//update="@form"
		documentAccountingServiceFacade.deleteProcess(accountingProcessTo);
	}
	
	/**
	 * *
	 * Preliminary Process.
	 */
	@LoggerAuditWeb
	public void processPreliminaryExecution(){
		
		log.info("Sending parameter for execute process batch ::::> GENERATOR DOCUMENTS ACCOUNTING ");		
		log.debug(this.parameterAuxiliaryTypeSelected.toString());
		
		AccountingProcessTo accountingProcessTo = new AccountingProcessTo();
		accountingProcessTo.setProcessType(this.parameterAuxiliaryTypeSelected);
		accountingProcessTo.setExecutionDate(this.executionDate);
		
		try {
			documentAccountingServiceFacade.sendProcessBatchGenerateDocument(accountingProcessTo);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESS_ACCOUNTING_SEND_GENERATION));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();"); 
		
	}

	
	/**
	 * Clean documents.
	 */
	public void cleanDocuments(){
		JSFUtilities.resetComponent("fmrBillingProcess");  
		JSFUtilities.resetComponent("fmrBillingProcess2");
		this.accountingProcessDataModel = null;
		this.accountingDataModel = null;
		this.parameterAuxiliaryTypeSelected = null;
		this.executionDate = today.getTime();
		this.initialDate = CommonsUtilities.currentDate();
		this.finalDate = CommonsUtilities.currentDate();
		this.listAccountingProcess = null;
		
	}
	
	/**
	 * Clean documents.
	 */
	public void cleanDataAccounting(){
		JSFUtilities.resetComponent("fmrBillingProcess");  
		JSFUtilities.resetComponent("fmrBillingProcess2");
		this.accountingDataModel = null;
		this.accountingDataModifyModel = null;
		this.accountingDate = today.getTime();
		this.listAccountingDataMonitorTos = null;
		this.idSecurityCodePk = null;
		this.idCustodyOperation = null;
		this.idNegotiationOperation = null;
		this.idCorporativeOperation = null;
		
	}
	
	/**
	 * Before on adjustments.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void beforeOnAdjustments(ActionEvent event) throws ServiceException{
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESS_ACCOUNTING_SEND_GENERATION));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmAdjustmentsDialog.show();"); 
		
	}
	
	/**
	 * Before on definitive.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void beforeOnDefinitive(ActionEvent event) throws ServiceException{
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESS_ACCOUNTING_SEND_GENERATION));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDefinitiveDialog.show();"); 
		
	}
	
	/**
	 * Process adjusment execution.
	 */
	@LoggerAuditWeb
	public void processAdjusmentExecution(){
		
	}
	
	/**
	 * Process definitive execution.
	 */
	@LoggerAuditWeb
	public void processDefinitiveExecution(){
		
	}
	
	/**
	 * Valid clean form search.
	 */
	public void validCleanFormSearch() {

		cleanDocuments();

	}
	
	
	/**
	 * Search logger accounting.
	 */
	@LoggerAuditWeb
	public void searchLoggerAccounting(){
		
		AccountingProcessTo accountingFilterTo= new AccountingProcessTo();
		accountingFilterTo.setInitialDate(initialDate);
		accountingFilterTo.setFinalDate(finalDate);
//		accountingFilterTo.setStatus(AccountingProcessStateType.PROCESADO.getCode());
		
		listAccountingProcess=documentAccountingServiceFacade.findAccountingProcessByFilter(accountingFilterTo);
		accountingProcessDataModel=new GenericDataModel<>(listAccountingProcess);
	}
	

	
	/**
	 * Gets the streamed content.
	 *
	 * @param accountingProcess the accounting process
	 * @return the streamed content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@LoggerAuditWeb
	public StreamedContent getStreamedContent(AccountingProcess accountingProcess) throws IOException{
		byte[] fileContent = documentAccountingServiceFacade.getAccountingProcessFileContent(accountingProcess.getIdAccountingProcessPk());
		return getStreamedContentFromFile(fileContent, null, accountingProcess.getNameFile());
	}


	public Date getAccountingDate() {
		return accountingDate;
	}

	public void setAccountingDate(Date accountingDate) {
		this.accountingDate = accountingDate;
	}

	public GenericDataModel<AccountingDataMonitorTo> getAccountingDataModel() {
		return accountingDataModel;
	}

	public void setAccountingDataModel(
			GenericDataModel<AccountingDataMonitorTo> accountingDataModel) {
		this.accountingDataModel = accountingDataModel;
	}

	public AccountingDataMonitorTo getAccountingDataMonitorTo() {
		return accountingDataMonitorTo;
	}

	public void setAccountingDataMonitorTo(
			AccountingDataMonitorTo accountingDataMonitorTo) {
		this.accountingDataMonitorTo = accountingDataMonitorTo;
	}

	public String getIdCustodyOperation() {
		return idCustodyOperation;
	}

	public void setIdCustodyOperation(String idCustodyOperation) {
		this.idCustodyOperation = idCustodyOperation;
	}

	public String getIdNegotiationOperation() {
		return idNegotiationOperation;
	}

	public void setIdNegotiationOperation(String idNegotiationOperation) {
		this.idNegotiationOperation = idNegotiationOperation;
	}

	public String getIdCorporativeOperation() {
		return idCorporativeOperation;
	}

	public void setIdCorporativeOperation(String idCorporativeOperation) {
		this.idCorporativeOperation = idCorporativeOperation;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public GenericDataModel<AccountingDataMonitorTo> getAccountingDataModifyModel() {
		return accountingDataModifyModel;
	}

	public void setAccountingDataModifyModel(
			GenericDataModel<AccountingDataMonitorTo> accountingDataModifyModel) {
		this.accountingDataModifyModel = accountingDataModifyModel;
	}

	public List<AccountingDataMonitorTo> getListAccountingDataMonitorToAux() {
		return listAccountingDataMonitorToAux;
	}

	public void setListAccountingDataMonitorToAux(
			List<AccountingDataMonitorTo> listAccountingDataMonitorToAux) {
		this.listAccountingDataMonitorToAux = listAccountingDataMonitorToAux;
	}
	
	
	
}




