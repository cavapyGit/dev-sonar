package com.pradera.accounting.document.to;

import java.io.Serializable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingReceiptTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingReceiptTo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The list accounting receipt detail to. */
	private List<AccountingReceiptDetailTo> listAccountingReceiptDetailTo;
	
	/** The schema. */
	private String 	schema;
	
	
	/**
	 * Instantiates a new accounting receipt to.
	 */
	public AccountingReceiptTo() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Gets the list accounting receipt detail to.
	 *
	 * @return the listAccountingReceiptDetailTo
	 */
	public List<AccountingReceiptDetailTo> getListAccountingReceiptDetailTo() {
		return listAccountingReceiptDetailTo;
	}


	/**
	 * Sets the list accounting receipt detail to.
	 *
	 * @param listAccountingReceiptDetailTo the listAccountingReceiptDetailTo to set
	 */
	public void setListAccountingReceiptDetailTo(
			List<AccountingReceiptDetailTo> listAccountingReceiptDetailTo) {
		this.listAccountingReceiptDetailTo = listAccountingReceiptDetailTo;
	}


	/**
	 * Gets the schema.
	 *
	 * @return the schema
	 */
	public String getSchema() {
		return schema;
	}


	/**
	 * Sets the schema.
	 *
	 * @param schema the schema to set
	 */
	public void setSchema(String schema) {
		this.schema = schema;
	}

}
