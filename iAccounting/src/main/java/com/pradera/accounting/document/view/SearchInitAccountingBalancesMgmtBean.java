package com.pradera.accounting.document.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.accounting.document.facade.DocumentAccountingServiceFacade;
import com.pradera.accounting.document.to.AccountingProcessReStartTO;
import com.pradera.accounting.document.to.AccountingProcessSchemaTo;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.document.to.FilterStartBalanceTO;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.schema.facade.SchemaAccountingServiceFacade;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcessSchema;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingProcessStateType;
import com.pradera.model.accounting.type.AccountingStartType;

@DepositaryWebBean
@LoggerCreateBean
public class SearchInitAccountingBalancesMgmtBean extends GenericBaseBean {

	
	private static final long serialVersionUID = -5222378272513878921L;
	
	
	private static final String PAGE_VIEW = "detailPageInitAccount";

	@Inject
	UserInfo userInfo;
	
	@Inject
	UserPrivilege userPrivilege;

	@Inject
	transient PraderaLogger log;
	
	@EJB
	private DocumentAccountingServiceFacade documentAccountingServiceFacade;
	
	@EJB
	private ParameterAccountingServiceFacade parameterAccountingServiceFacade;
	
	@EJB
	private SchemaAccountingServiceFacade schemaAccountingServiceFacade;
	
	private boolean disableSend, renderedButtonReject;
	
	private AccountingProcessSchemaTo accountingProcessSchemaToSelected;
	
	private FilterStartBalanceTO searchFilterStartBalanceTO, registreFilterStartBalanceTO ,viewFilterStartBalanceTO;

	private List<AccountingParameter> listParameterAuxiliaryType;
	
	private AccountingParameterTO accountingParameterTo = new AccountingParameterTO();	
	
	private List<AccountingProcessReStartTO> listBalanceStartRecords ;
	
	private List<AccountingProcessSchemaTo> listAccountingProcessSchemaTo;
	
	private GenericDataModel<AccountingProcessReStartTO> accountingProcessDataModel;
	
	/**Accounting process seleccionado*/
	private AccountingProcessReStartTO accountingProcessSelected;
	
	@PostConstruct
	public void init() throws ServiceException  {
			loadCmbDataSearch();
			searchFilterStartBalanceTO = new FilterStartBalanceTO();
			searchFilterStartBalanceTO.setExecutionDate(CommonsUtilities.currentDate());
			searchFilterStartBalanceTO.setDataOrigin("1");
	}
	
	
	@LoggerAuditWeb
	public String preReStartAccountingBalancesReject(){
		if(accountingProcessSelected!=null){			
			log.info("ID:"+accountingProcessSelected.getId());
			viewFilterStartBalanceTO = new FilterStartBalanceTO();
			viewFilterStartBalanceTO.setAuxiliaryType(accountingProcessSelected.getProcessType());
			viewFilterStartBalanceTO.setExecutionDate(accountingProcessSelected.getOperationDate());
			listAccountingProcessSchemaTo = schemaAccountingServiceFacade.searchAccountingProcessSchema(accountingProcessSelected);
			disableSend = true;
			renderedButtonReject = true;
			return PAGE_VIEW;
		}else{
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
            JSFUtilities.putViewMap("headerMessageView", headerMessage);
            String bodyMessage = PropertiesUtilities.getMessage("warning.modify.not.select");
            JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
            JSFUtilities.updateComponent("opnlDialogs");
            JSFUtilities.showSimpleValidationDialog();
		}
		return null;
	}
	
	/**
	 * Dialogo para confirmar si se va a ejecutar o no la copia de cartera a contabilidad*/
	@LoggerAuditWeb
	public String preConfirmReStartBalances(){
		if(accountingProcessSelected!=null){
//			if(!accountingProcessSelected.getStatusDesc().equals("PENDIENTE")){
//				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
//	            JSFUtilities.putViewMap("headerMessageView", headerMessage);
//	            String bodyMessage = PropertiesUtilities.getMessage("lbl.message.verify.state.no.correct");
//	            JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
//	            JSFUtilities.updateComponent("opnlDialogs");
//	            JSFUtilities.showSimpleValidationDialog();
//	            return null;
//			}
			log.info("ID:"+accountingProcessSelected.getId());
			viewFilterStartBalanceTO = new FilterStartBalanceTO();
			viewFilterStartBalanceTO.setAuxiliaryType(accountingProcessSelected.getProcessType());
			viewFilterStartBalanceTO.setExecutionDate(accountingProcessSelected.getOperationDate());
			listAccountingProcessSchemaTo = schemaAccountingServiceFacade.searchAccountingProcessSchema(accountingProcessSelected);
			disableSend = false;
			renderedButtonReject = false;
			return PAGE_VIEW;
		}else{
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
            JSFUtilities.putViewMap("headerMessageView", headerMessage);
            String bodyMessage = PropertiesUtilities.getMessage("warning.modify.not.select");
            JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
            JSFUtilities.updateComponent("opnlDialogs");
            JSFUtilities.showSimpleValidationDialog();
		}
		return null;
	}
	

	/**
	 * Carga de detalle del registro
	 * */
	@LoggerAuditWeb
	public String loadDetailReStartBalance(ActionEvent event){

		AccountingProcessReStartTO accountingProcess = (AccountingProcessReStartTO) event.getComponent().getAttributes().get("idViewProcess");
		listAccountingProcessSchemaTo=schemaAccountingServiceFacade.searchAccountingProcessSchema(accountingProcess);

		viewFilterStartBalanceTO = new FilterStartBalanceTO();
		viewFilterStartBalanceTO.setAuxiliaryType(searchFilterStartBalanceTO.getAuxiliaryType());
		viewFilterStartBalanceTO.setExecutionDate(searchFilterStartBalanceTO.getExecutionDate());
		
		disableSend = true;
		renderedButtonReject = false;
		return PAGE_VIEW;
	}

	@LoggerAuditWeb
	public void loadCmbDataSearch() throws ServiceException {
		if(Validations.validateListIsNullOrEmpty(listParameterAuxiliaryType)){
			this.accountingParameterTo.setParameterType(PropertiesConstants.DAILY_AUXILIARY);
			this.accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			this.listParameterAuxiliaryType  =   parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
		} 
	}

	@LoggerAuditWeb
	public void loadRegistrationPage() throws ServiceException {
		registreFilterStartBalanceTO = new  FilterStartBalanceTO();
		registreFilterStartBalanceTO.setExecutionDate(CommonsUtilities.currentDate());
		registreFilterStartBalanceTO.setDataOrigin("1");
		loadCmbDataSearch();
	}
	
	
	public String loadSearchPage(){
		return "loadSearchPageInitAccount";
	}
	/**
	 * Busqueda de procesos de reinicio de saldos buscados.
	 */
	@LoggerAuditWeb
	public void searchReStartBalancesRecords(){
		accountingProcessSelected = null;
		listBalanceStartRecords = new ArrayList<AccountingProcessReStartTO>();
		listBalanceStartRecords=documentAccountingServiceFacade.findBalanceStartRecordByFilter(searchFilterStartBalanceTO);
		accountingProcessDataModel=new GenericDataModel<>(listBalanceStartRecords);
	}
	

	public String registerReStartBalances()  {
		JSFUtilities.executeJavascriptFunction("cnfwProcessSendReInitAccounting.show();");
		return null;
	}
	
	/**
	 * Metodo hacer:
	 * 1. Crear un AccountingProcess
	 * 2. Crear los AccountingProcessSchema
	 * 3. Crear la foto de los datos historicos contables
	 * @return
	 */
	@LoggerAuditWeb
	public String registreConfirmedBalanceReStartRecord(){
		log.info("CONTABILIDAD: Se registra en re inicio de saldos contables. ");		
		AccountingProcessTo accountingProcessTo = new AccountingProcessTo();
		accountingProcessTo.setProcessType(registreFilterStartBalanceTO.getAuxiliaryType());
		accountingProcessTo.setPortofolioOrigenAccount(true);
		accountingProcessTo.setExecutionDate(registreFilterStartBalanceTO.getExecutionDate());
		Long idAccountingProcessPk = documentAccountingServiceFacade.registerReStartAccountingBalances(accountingProcessTo);
		//Crear AccountingProcessSchema
		createAccountingProcessSchema(idAccountingProcessPk);
		JSFUtilities.updateComponent("opnlDialogs");
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
		JSFUtilities.putViewMap("headerMessageView", headerMessage);
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogReStartBalance.show();"); 
		return null;
	}
	
	private void createAccountingProcessSchema(Long idAccountingProcessPk) {
		AccountingSchemaTo parameter=new AccountingSchemaTo();
		parameter.setStartType(AccountingStartType.START.getCode());
		List<AccountingSchema> listSchemas=schemaAccountingServiceFacade.searchAccountingSchema(parameter);
		for (AccountingSchema accountingSchema : listSchemas) {
			AccountingProcessSchema accountingProcessSchema=new AccountingProcessSchema();
			accountingProcessSchema.setAccountingSchema(accountingSchema);
			accountingProcessSchema.getAccountingProcess().setIdAccountingProcessPk(idAccountingProcessPk);
			accountingProcessSchema.setStartType(AccountingStartType.START.getCode());
			accountingProcessSchema.setStatus(AccountingProcessStateType.PENDIENTE.getCode());
			schemaAccountingServiceFacade.saveAccountingProcessSchema(accountingProcessSchema);
		}
	}
	
	
	/**
	 * Clean documents.
	 */
	public void cleanSearchScreen(){
		JSFUtilities.resetComponent("fmrBillingProcess");  
		JSFUtilities.resetComponent("fmrBillingProcess2");
		this.accountingProcessDataModel = null;
		this.listBalanceStartRecords = null;
	}
	
	@LoggerAuditWeb
	public void beforeExecuteProcess(ActionEvent event){
		accountingProcessSchemaToSelected = (AccountingProcessSchemaTo) event.getComponent().getAttributes().get("selectedObject");
		log.info("Id esquema:"+accountingProcessSchemaToSelected.getAccountingSchema().getIdAccountingSchemaPk());
		log.info("Id proceso:"+accountingProcessSchemaToSelected.getAccountingProcess().getIdAccountingProcessPk());
		accountingProcessSchemaToSelected.setExecutionDate(accountingProcessSelected.getOperationDate());
		log.info("Sending parameter for execute process batch ::::> GENERATOR DOCUMENTS ACCOUNTING ");

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESS_ACCOUNTING_SEND_GENERATION));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogPreExecute.show();");
	}

	@LoggerAuditWeb
	public void executeProcessSchema(){
		try {
			documentAccountingServiceFacade.sendProcessBatchGeneratePartialReStartDocument(accountingProcessSchemaToSelected);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESS_ACCOUNTING_SEND_GENERATION));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogAccept.show();"); 
		
		
		JSFUtilities.resetViewRoot();
		
		accountingProcessSchemaToSelected = null;
		viewFilterStartBalanceTO = null;
		listAccountingProcessSchemaTo = null;
		
		JSFUtilities.resetComponent("frmGenerateDocAccountingReStart");  
		JSFUtilities.resetComponent("frmViewAccountingReStart"); 
		
		searchReStartBalancesRecords();
	}
	public String returnSearchPage(){
		return "backRegistrePageInitAccount";
	}
	
	public void beforeRejectAccountingProcess(){
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogPreReject.show();"); 
	}
	
	@LoggerAuditWeb
	public void confirmRejectAccountingProcess(){
		documentAccountingServiceFacade.rejectAccountingProcess(accountingProcessSelected);
		searchReStartBalancesRecords();
		JSFUtilities.resetComponent("frmGenerateDocAccountingReStart");  
		
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogPreRejectAccept.show();"); 
	}
}




