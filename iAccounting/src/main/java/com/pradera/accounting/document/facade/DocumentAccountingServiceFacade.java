package com.pradera.accounting.document.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.common.utils.SequenceGeneratorCode;
import com.pradera.accounting.document.service.DocumentAccountingServiceBean;
import com.pradera.accounting.document.service.sender.SenderAccountigService;
import com.pradera.accounting.document.service.sender.SenderAccountingMonthlyService;
import com.pradera.accounting.document.service.sender.SenderAccountingPartialService;
import com.pradera.accounting.document.to.AccountingDataMonitorTo;
import com.pradera.accounting.document.to.AccountingProcessReStartTO;
import com.pradera.accounting.document.to.AccountingProcessSchemaTo;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.document.to.AccountingReceiptDetailTo;
import com.pradera.accounting.document.to.FilterStartBalanceTO;
import com.pradera.accounting.formulate.util.Parser;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingFormulate;
import com.pradera.model.accounting.AccountingMatrixDetail;
import com.pradera.model.accounting.AccountingMonthResult;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounting.AccountingReceipt;
import com.pradera.model.accounting.AccountingReceiptDetail;
import com.pradera.model.accounting.AccountingSource;
import com.pradera.model.accounting.AccountingSourceDetail;
import com.pradera.model.accounting.AccountingSourceLogger;
import com.pradera.model.accounting.VariableFormulate;
import com.pradera.model.accounting.type.AccountingAssociateCodeType;
import com.pradera.model.accounting.type.AccountingFieldSourceType;
import com.pradera.model.accounting.type.AccountingStartType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.type.ReStartBalancesType;
 

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DocumentAccountingServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class DocumentAccountingServiceFacade implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;	
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The document accounting service bean. */
	@EJB
	DocumentAccountingServiceBean 		documentAccountingServiceBean;	
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry 	transactionRegistry;
	
	/** The sequence generator code. */
	@EJB
	SequenceGeneratorCode  				sequenceGeneratorCode;
	
	/** The sender accountig service. */
	@EJB
	SenderAccountigService				senderAccountigService;
	
	/** The sender accountig service. */
	@EJB
	SenderAccountingPartialService				senderAccountingPartialService;
	
	/** The sender accountig service. */
	@EJB
	SenderAccountingMonthlyService				senderAccountingMonthlyService;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade		accountingMatrixServiceFacade;

	/**
	 * Find daily change.
	 *
	 * @return the big decimal
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY) 
	public BigDecimal findDailyChange(){
		
		BigDecimal  dailyChange = BigDecimal.ZERO;
		Date date = CommonsUtilities.currentDate();
		
		try {
			
			dailyChange = documentAccountingServiceBean.getFindUSDExchangeRate(date);
			
		} catch (ServiceException e) {
		
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dailyChange;
	}
	
	
	/**
	 * Send process batch generate document.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void sendProcessBatchGenerateDocument(AccountingProcessTo accountingProcessTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		senderAccountigService.sendProcessBatchGenerateDocument(accountingProcessTo);
	}
	
	
	/**
	 * Send process batch generate document.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void sendProcessBatchGenerateReStartDocument(AccountingProcessTo accountingProcessTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		senderAccountigService.sendProcessBatchReStartGenerateDocument(accountingProcessTo);
	}
	
	
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void sendProcessBatchGeneratePartialReStartDocument(AccountingProcessSchemaTo accountingProcessTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		senderAccountingPartialService.sendProcessBatchReStartGenerateDocument(accountingProcessTo);
	}
	

	
	
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void sendProcessBatchGenerateMonthlyDocument(AccountingProcessSchemaTo accountingProcessTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		senderAccountingMonthlyService.sendProcessBatchMonthlyGenerateDocument(accountingProcessTo);
	}
	
	

	/**
	 * Load accounting process.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingProcess> loadAccountingProcess(AccountingProcess filter){
	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
	
		List<AccountingProcess> accountingReceiptTempList = new ArrayList<AccountingProcess>();
		
		accountingReceiptTempList = documentAccountingServiceBean.searchDocumentAccountingProcess(filter);
		
		
		return accountingReceiptTempList;
	}
	
	public List<AccountingSchemaTo> getListAccountingSchemaTos(FilterStartBalanceTO filterStartBalanceTO){
		
		List<AccountingSchemaTo> accountingSchemas = documentAccountingServiceBean.getLisAccountingSchemas(filterStartBalanceTO);
		return accountingSchemas;
	}
	
	/**
	 * Load accounting process.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingDataMonitorTo> loadDataAccounting(AccountingDataMonitorTo filter){
	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
	
		List<AccountingDataMonitorTo> listHolderAccountMovementData = new ArrayList<AccountingDataMonitorTo>();
		listHolderAccountMovementData = documentAccountingServiceBean.searchDataAccounting(filter);
		
		
		return listHolderAccountMovementData;
	}
	
	
	/**
	 * Find accounting process.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<Object[]> findAccountingProcess(AccountingProcessTo filter){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
			
			return documentAccountingServiceBean.findAccountingProcess(filter);
	}
	
	/**
	 * Save accounting process.
	 *
	 * @param accountingProcess the accounting process
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveAccountingProcess(AccountingProcess accountingProcess) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		documentAccountingServiceBean.saveAccountingProcess(accountingProcess);
	}

	/**
	 * Update accounting process.
	 *
	 * @param accountingProcess the accounting process
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void updateAccountingProcess(AccountingProcess accountingProcess) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		documentAccountingServiceBean.updateAccountingProcess(accountingProcess);
	}
	
	/**
	 * Save accounting receipt.
	 *
	 * @param accountingReceipt the accounting receipt
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void saveAccountingReceipt(AccountingReceipt accountingReceipt) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		documentAccountingServiceBean.saveAccountingReceipt(accountingReceipt);
	}
	
	
	/**
	 * Update accounting receipt.
	 *
	 * @param accountingReceipt the accounting receipt
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateAccountingReceipt(AccountingReceipt accountingReceipt) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		documentAccountingServiceBean.update(accountingReceipt);
	}
	
	/**
	 * Save accounting receipt detail.
	 *
	 * @param accountingReceiptDetail the accounting receipt detail
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void saveAccountingReceiptDetail(AccountingReceiptDetail accountingReceiptDetail) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		documentAccountingServiceBean.saveAccountingReceiptDetail(accountingReceiptDetail);
	}
	
	
	/**
	 * Save accounting source logger list.
	 *
	 * @param accountingSourceLoggerList the accounting source logger list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void saveAccountingSourceLoggerList(List<AccountingSourceLogger> accountingSourceLoggerList) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		documentAccountingServiceBean.saveAll(accountingSourceLoggerList);
	}
	
	
	/**
	 * Save accounting receipt detail with logger.
	 *
	 * @param accountingReceiptDetail the accounting receipt detail
	 * @throws ServiceException the service exception
	 */
	public void saveAccountingReceiptDetailWithLogger(AccountingReceiptDetail accountingReceiptDetail) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
				
		if (Validations.validateListIsNotNullAndNotEmpty(accountingReceiptDetail.getAccountingSourceLogger())){
			
			for (AccountingSourceLogger accountingSourceLogger : accountingReceiptDetail.getAccountingSourceLogger()) {
				accountingSourceLogger.setAccountingReceiptDetail(accountingReceiptDetail);
			}

		}
				
		
	}
	
	/**
	 * Save AccountingReceipt.
	 *
	 * @param accountingReceipt the accounting receipt
	 * @throws ServiceException the service exception
	 */	
	public void saveAccountingReceiptWithDetail(AccountingReceipt accountingReceipt) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());



		if(Validations.validateListIsNotNullAndNotEmpty(accountingReceipt.getAccountingReceiptDetail())){
			
			for (AccountingReceiptDetail accountingReceiptDetail : accountingReceipt.getAccountingReceiptDetail()) {
				accountingReceiptDetail.setAccountingReceipt(accountingReceipt);
				
				saveAccountingReceiptDetailWithLogger(accountingReceiptDetail);
				
			}
			
		}
		
		
	}
	
	/**
	 * Save List AccountingMatrix.
	 *
	 * @param listAccountingReceipt the list accounting receipt
	 * @throws ServiceException the service exception
	 */	
	public void saveListAccountingReceiptWithDetail(List<AccountingReceipt> listAccountingReceipt) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		List<AccountingReceiptDetail> listRecDetailCopy=new ArrayList<>();

		List<AccountingSourceLogger> accLoggerCopy=new ArrayList<>();
		List<AccountingSourceDetail> accLoggerDet=new ArrayList<>();
		
		for (AccountingReceipt accountingReceipt : listAccountingReceipt) {
			saveAccountingReceiptWithDetail(accountingReceipt);
			listRecDetailCopy.addAll(accountingReceipt.getAccountingReceiptDetail());
			
		}
		for (AccountingReceiptDetail accRecDetail : listRecDetailCopy) {
			accLoggerCopy.addAll(accRecDetail.getAccountingSourceLogger());
		}

		for (AccountingSourceLogger accLogger : accLoggerCopy) {
			accLoggerDet.addAll(accLogger.getAccountingSourceDetail());
		}
		
		documentAccountingServiceBean.saveListObject(listAccountingReceipt,10000);
		
		documentAccountingServiceBean.saveListObject(listRecDetailCopy , 10000);
		documentAccountingServiceBean.saveListObject(accLoggerCopy , 10000);
		documentAccountingServiceBean.saveListObject(accLoggerDet , 20000);
		log.info("::::END OF Save Accounting ::::::::");
	}
	

	
	/**
	 * Save AccountingReceipt.
	 *
	 * @param accountingReceipt the accounting receipt
	 * @throws ServiceException the service exception
	 */	
	public void saveAccountingReceiptWithMonthResult(AccountingReceipt accountingReceipt) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		if(Validations.validateListIsNotNullAndNotEmpty(accountingReceipt.getAccountingMonthResult())){
			
			for (AccountingMonthResult accountingMonthResult : accountingReceipt.getAccountingMonthResult()) {
				accountingMonthResult.setAccountingReceipt(accountingReceipt);				
			}
		}
	}
	
	/**
	 * Save List AccountingMatrix.
	 *
	 * @param listAccountingReceipt the list accounting receipt
	 * @throws ServiceException the service exception
	 */	
	public void saveListAccountingReceiptWithResult(List<AccountingReceipt> listAccountingReceipt) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		List<AccountingMonthResult> listAccountingMonthResult=new ArrayList<>();
		
		for (AccountingReceipt accountingReceipt : listAccountingReceipt) {
			saveAccountingReceiptWithMonthResult(accountingReceipt);
			listAccountingMonthResult.addAll(accountingReceipt.getAccountingMonthResult());
		}

		documentAccountingServiceBean.saveListObject(listAccountingReceipt,10000);
		documentAccountingServiceBean.saveListObject(listAccountingMonthResult , 10000);
		log.info("::::END OF Save Accounting ::::::::");
	}
	
	/**
	 * Find accunting receipt by id accounting process.
	 *
	 * @param idAccountingProcess the id accounting process
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public List<AccountingReceipt> findAccuntingReceiptByIdAccountingProcess(Long idAccountingProcess ){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		return documentAccountingServiceBean.findAccuntingReceiptByIdAccountingProcess(idAccountingProcess);
	}

	/**
	 * Find accounting process with accunting receipt.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the list
	 */
	public List<AccountingProcess> findAccountingProcessWithAccuntingReceipt(AccountingProcessTo accountingProcessTo ){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		return documentAccountingServiceBean.findAccountingProcessWithAccuntingReceipt(accountingProcessTo);
				
	}
	
	
	/**
	 * Calculate document detail.
	 *
	 * @param accountingMatrixDetail the accounting matrix detail
	 * @param executionDate the execution date
	 * @param associateCode the associate code
	 * @param mapSource the map source
	 * @return the list
	 * @throws Exception the exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	@AccessTimeout(unit=TimeUnit.HOURS,value= 1 )
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public List<AccountingReceiptDetailTo> calculateDocumentDetail(AccountingMatrixDetail accountingMatrixDetail , 
											Date executionDate, Integer associateCode,
											Map<Long,AccountingSourceInfo> mapSource ) throws Exception{
		
		List<AccountingReceiptDetail>	listAccountingReceiptDetail=new ArrayList<AccountingReceiptDetail>();
		
		/***
		 * List AccountingReceiptDetailTo for Performance
		 */
		List<AccountingReceiptDetailTo>  listAccountingReceiptDetailTo=new ArrayList<AccountingReceiptDetailTo>();
		
		AccountingSource accountingSource	= accountingMatrixDetail.getAccountingSource();			
		AccountingReceiptDetail 	accountingReceiptDetail = null;
		
		try {
		
			AccountingSourceInfo accountingSourceInfo =mapSource.get(accountingSource.getIdAccountingSourcePk());
					//accountingMatrixServiceFacade.loadAccountingSourceInfoById(accountingSource.getIdAccountingSourcePk());
			if(Validations.validateIsNull(accountingSourceInfo)){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_SOURCE_ERROR);
			}
			
			/** Load formulate with all VariableFormulate */
			AccountingFormulate	accountingFormulate = accountingMatrixServiceFacade.findAccountingFormulateByMatrixDetailId(accountingMatrixDetail.getIdAccountingMatrixDetailPk());						
			List<VariableFormulate>  variableFormulateList	= accountingFormulate.getVariableFormulates();
			List<AccountingSourceLogger> accountingSourceLoggerList=null;
			
			accountingSourceInfo.setExecutionDate(executionDate);
			accountingSourceInfo.setAssociateCode(associateCode);
			
			accountingSourceInfo.setTypeAssociation(accountingMatrixDetail.getAccountingAccount().getAccountCode());
			accountingSourceInfo.setCurrency(accountingMatrixDetail.getAccountingAccount().getCurrency());
			accountingSourceInfo.setInstrumentType(accountingMatrixDetail.getAccountingAccount().getInstrumentType());
			
			/**Por cada tupla un documento**/
			/** HERE BE EXECUTED AND LOADED THE DATA GOT FOR ONE DATE ESPECIFIC ***/
			accountingSourceLoggerList = accountingMatrixServiceFacade.fillAccountingSourceLogger(accountingSourceInfo);
					
			
			int numberTuplas=accountingSourceLoggerList.size();
			
			Double resultTemp	= null	;		
			String formulateInput	=	accountingFormulate.getFormulateInput();
			
			Parser parser = new Parser();
			
			for (int i = 0; i < numberTuplas; i++) {
				
				AccountingSourceLogger accountingSourceLogger= accountingSourceLoggerList.get(i);					
				accountingReceiptDetail = new AccountingReceiptDetail();
						
				for (VariableFormulate variableFormulate : variableFormulateList) {
								
								
					/***
					 * Load Accounting Source Detail, Value, parameter Value, Source Type.
					 */
					for (AccountingSourceDetail accountingSourceDetailTemp : accountingSourceLogger.getAccountingSourceDetail()) {
						
						if(Validations.validateIsNotNull(accountingSourceDetailTemp.getParameterType()) &&
								(AccountingFieldSourceType.get(accountingSourceDetailTemp.getParameterType()).getValue().
								  equalsIgnoreCase(AccountingFieldSourceType.REFERENCE.getValue())) ){
							
							if(Validations.validateIsNotNull(variableFormulate.getPositionSource()) && 
								 variableFormulate.getPositionSource().equals(accountingSourceDetailTemp.getPosition())){
							
								  variableFormulate.setValueDefault(new BigDecimal(accountingSourceDetailTemp.getParameterValue()) );
								  break;
							}
						}else if(Validations.validateIsNotNull(accountingSourceDetailTemp.getParameterType()) &&
								(AccountingFieldSourceType.get(accountingSourceDetailTemp.getParameterType()).getValue().
										  equalsIgnoreCase(AccountingFieldSourceType.VALUE.getValue())) ){
							
							if (Validations.validateIsNotNull(variableFormulate.getPositionSource()) && 
								variableFormulate.getPositionSource().equals(accountingSourceDetailTemp.getPosition())){
								accountingSourceDetailTemp.setParameterValue(variableFormulate.getValueDefault().toString());
								  break;
							}
								  
						}								
														
					}
								
				}
				
				
				/**
				 * 
				 * The String FormulateInput, calculate Result Formulate and Variable
				 * **/	
				String formulateInputValue = AccountingUtils.replaceFormulateForNumberWithOutRegex(formulateInput, variableFormulateList);
				parser.settingFormulate(formulateInputValue);
				resultTemp = parser.calculateFormulate();
				accountingSourceLogger.setResultPartial( new BigDecimal(resultTemp));
				accountingSourceLogger.setAccountingSource(accountingSource);
				accountingSourceLogger.setFormulateInput(formulateInputValue);
				
				//resultFinal = resultFinal+resultTemp;
				/****
				 * Este es la lista de SourceLogger que se agrega al detalle del recibo
				 */
				List<AccountingSourceLogger> accountingSourceLoggerListOne=new ArrayList<AccountingSourceLogger>();
				accountingSourceLoggerListOne.add(accountingSourceLogger);
				
				accountingReceiptDetail.setAmount(new BigDecimal(resultTemp));
				accountingReceiptDetail.setAccountingSourceLogger(accountingSourceLoggerListOne);
				
				accountingReceiptDetail.setDocumentDate(CommonsUtilities.currentDateTime());				
				accountingReceiptDetail.setAccountingMatrixDetail(accountingMatrixDetail);
	
				
				listAccountingReceiptDetail.add(accountingReceiptDetail);
			}
			
			
			/***
			 * Getting AssociateCodeTo ReceiptDetail and setting in AccountingReceiptDetailTo
			 */
			listAccountingReceiptDetailTo=getAssociateCodeToReceiptDetail(listAccountingReceiptDetail,associateCode);


		}catch (Exception e) {
			
			log.error(e.getMessage());
			
			if(e instanceof ServiceException){
				 if((((ServiceException) e).getErrorService()!=null&&
						 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT))){
					 
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT);
				 }else if((((ServiceException) e).getErrorService()!=null&&
						 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_WRONG_STRUCTURED))){
					 
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_SOURCE_WRONG_STRUCTURED);
				 }else if((((ServiceException) e).getErrorService()!=null&&
						 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_QUERY_GRAMMAR))){
					 
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_QUERY_GRAMMAR);
				 }else if((((ServiceException) e).getErrorService()!=null&&
						 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE))){
					 
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
				 }
				
			}else {
				 throw new ServiceException(ErrorServiceType.ACCOUNTING_OTHER_ERROR);
			 
			}
			
					
		}
			
		return listAccountingReceiptDetailTo;
		
	}
	
	
	
	

	
	/**
	 * Calculate document detail.
	 *
	 * @param accountingMatrixDetail the accounting matrix detail
	 * @param executionDate the execution date
	 * @param associateCode the associate code
	 * @param mapSource the map source
	 * @return the list
	 * @throws Exception the exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	@AccessTimeout(unit=TimeUnit.HOURS,value= 1 )
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public List<AccountingMonthResult> calculateDocumentDetailMonthly(AccountingMatrixDetail accountingMatrixDetail , 
											Date executionDate, Integer associateCode,
											Map<Long,AccountingSourceInfo> mapSource ) throws Exception{
		
		List<AccountingMonthResult> accountingMonthResultList=null;
		AccountingSource accountingSource	= accountingMatrixDetail.getAccountingSource();		
		
		try {
		
			AccountingSourceInfo accountingSourceInfo =mapSource.get(accountingSource.getIdAccountingSourcePk());
					//accountingMatrixServiceFacade.loadAccountingSourceInfoById(accountingSource.getIdAccountingSourcePk());
			if(Validations.validateIsNull(accountingSourceInfo)){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_SOURCE_ERROR);
			}
			
			/** Load formulate with all VariableFormulate */
			AccountingFormulate	accountingFormulate = accountingMatrixServiceFacade.findAccountingFormulateByMatrixDetailId(accountingMatrixDetail.getIdAccountingMatrixDetailPk());						
			List<VariableFormulate>  variableFormulateList	= accountingFormulate.getVariableFormulates();
			
			
			accountingSourceInfo.setExecutionDate(executionDate);
			accountingSourceInfo.setAssociateCode(associateCode);
			
			accountingSourceInfo.setTypeAssociation(accountingMatrixDetail.getAccountingAccount().getAccountCode());
			accountingSourceInfo.setCurrency(accountingMatrixDetail.getAccountingAccount().getCurrency());
			accountingSourceInfo.setInstrumentType(accountingMatrixDetail.getAccountingAccount().getInstrumentType());
			
			/**Por cada tupla un documento**/
			/** HERE BE EXECUTED AND LOADED THE DATA GOT FOR ONE DATE ESPECIFIC ***/
			accountingMonthResultList = accountingMatrixServiceFacade.fillAccountingMonthResult(accountingSourceInfo);
			String formulateInput	=	accountingFormulate.getFormulateInput();
			
			Parser parser = new Parser();
			
			for (AccountingMonthResult accountingMonthResult: accountingMonthResultList) {
						
				for (VariableFormulate variableFormulate : variableFormulateList) {

					if(AccountingFieldSourceType.NOMINAL_VALUE.getValue().equals(variableFormulate.getVariableSource())){
						variableFormulate.setValueDefault(accountingMonthResult.getNominalValue());
					}else if(AccountingFieldSourceType.MOVEMENT_QUANTITY.getValue().equals(variableFormulate.getVariableSource())) {
						variableFormulate.setValueDefault(accountingMonthResult.getMovementQuantity());
					}
						
				}
				/**
				 * 
				 * The String FormulateInput, calculate Result Formulate and Variable
				 * **/	
				String formulateInputValue = AccountingUtils.replaceFormulateForNumberWithOutRegex(formulateInput, variableFormulateList);
				parser.settingFormulate(formulateInputValue);
				accountingMonthResult.setAmount(new BigDecimal(parser.calculateFormulate()));

				accountingMonthResult.setAccountingMatrixDetail(accountingMatrixDetail);
			}
			
			
//			/***
//			 * Getting AssociateCodeTo accountingMonthResultList
//			 */
			accountingMonthResultList=getAssociateCodeToMonthResult(accountingMonthResultList,associateCode);
			

		}catch (Exception e) {
			
			log.error(e.getMessage());
			
			if(e instanceof ServiceException){
				 if((((ServiceException) e).getErrorService()!=null&&
						 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT))){
					 
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT);
				 }else if((((ServiceException) e).getErrorService()!=null&&
						 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_WRONG_STRUCTURED))){
					 
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_SOURCE_WRONG_STRUCTURED);
				 }else if((((ServiceException) e).getErrorService()!=null&&
						 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_QUERY_GRAMMAR))){
					 
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_QUERY_GRAMMAR);
				 }else if((((ServiceException) e).getErrorService()!=null&&
						 ((ServiceException) e).getErrorService().equals(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE))){
					 
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
				 }
				
			}else {
				 throw new ServiceException(ErrorServiceType.ACCOUNTING_OTHER_ERROR);
			 
			}
			
					
		}
			
		return accountingMonthResultList;
		
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Gets the associate code to receipt detail.
	 *
	 * @param listAccountingReceiptDetail the list accounting receipt detail
	 * @param associateCode the associate code
	 * @return the associate code to receipt detail
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<AccountingReceiptDetailTo> getAssociateCodeToReceiptDetail(List<AccountingReceiptDetail> listAccountingReceiptDetail,Integer associateCode) throws ServiceException{
		
		List<AccountingReceiptDetailTo> listAccountingReceiptDetailTo=new ArrayList<AccountingReceiptDetailTo>();
		
		/**
		 * One Detail for each Detail Matrix
		  * For each code associated, grouped and putting it in a bag
		  * 
		  */
		 for (AccountingReceiptDetail accReceiptDetail : listAccountingReceiptDetail) {
			 
			 AccountingReceiptDetailTo accountingReceiptDetailTo=new AccountingReceiptDetailTo();
			 
			 /***
			  * Key Associate by Source
			  */
			 String keyAssociate="";
			 
			 /**
			  * DetailReceipt with SourceLogger One To One
			  */
			 for (AccountingSourceLogger accountingSourceLogger : accReceiptDetail.getAccountingSourceLogger()) {
				 
				 /**
				  * idParticipantPk
				  */
				 String participant="";
				 /**
				  * idIssinCodePk
				  */
				 String security="";
				 /***
				  * idIssuerPk
				  */
				 String issuer="";
				 /***
				  * idHolderPk
				  */
				 String holder="";
				 /**
				  * EntityCollection only Daily Auxiliary = Admin
				  */
				 String entityCollection="";
				 /**
				  * movementType 
				  */
				 String movementType="";
				 
				 /***
				  * Getting id entity by source
				  */
				 for (int i = 0; i < accountingSourceLogger.getAccountingSourceDetail().size(); i++) {
					 AccountingSourceDetail accountingSourceDetail=accountingSourceLogger.getAccountingSourceDetail().get(i);
					 
					 if(accountingSourceDetail.getParameterType().equals(AccountingFieldSourceType.ENTITY_COLLECTION.getCode())){
							AccountingSourceDetail detailSource= accountingSourceLogger.getAccountingSourceDetail().get(i);
							entityCollection=detailSource.getParameterValue().concat(AccountingUtils.VERTICAL);
					 }
					 if(accountingSourceDetail.getParameterType().equals(AccountingFieldSourceType.PARTICIPANT.getCode())){
							AccountingSourceDetail detailSource= accountingSourceLogger.getAccountingSourceDetail().get(i);
							participant=detailSource.getParameterValue();
					 }
					 if(accountingSourceDetail.getParameterType().equals(AccountingFieldSourceType.ISSUER.getCode())){
							AccountingSourceDetail detailSource= accountingSourceLogger.getAccountingSourceDetail().get(i);
							issuer=detailSource.getParameterValue();
					 }
					 if(accountingSourceDetail.getParameterType().equals(AccountingFieldSourceType.HOLDER.getCode())){
							AccountingSourceDetail detailSource= accountingSourceLogger.getAccountingSourceDetail().get(i);
							holder=detailSource.getParameterValue();
					 }
					 if(accountingSourceDetail.getParameterType().equals(AccountingFieldSourceType.SECURITY.getCode())){
							AccountingSourceDetail detailSource= accountingSourceLogger.getAccountingSourceDetail().get(i);
							security=detailSource.getParameterValue();
					 }
					 if(accountingSourceDetail.getParameterType().equals(AccountingFieldSourceType.MOVEMENT_TYPE.getCode())){
							AccountingSourceDetail detailSource= accountingSourceLogger.getAccountingSourceDetail().get(i);
							movementType=detailSource.getParameterValue();
					 }
					 
					 
				}
				 
				 /***
				  * AssociateCode by SCHEMA, combination according to AssociateCode
				  */
				 if(associateCode.equals(AccountingAssociateCodeType.PARTICIPANT.getCode())){
					 if(Validations.validateIsNotNullAndNotEmpty(participant)){
						 keyAssociate=participant;
					 }else{
						 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
						
					 }
					 
				 }else if(associateCode.equals(AccountingAssociateCodeType.SECURITY.getCode())){
					 if(Validations.validateIsNotNullAndNotEmpty(security)){
						 keyAssociate=security;
					 }else{
						 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
					 }
					 
				
				 }else if(associateCode.equals(AccountingAssociateCodeType.ISSUER.getCode())){
					 if(Validations.validateIsNotNullAndNotEmpty(issuer)){
						 keyAssociate=issuer;
					 }else{
						 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
						
					 }
					 
				 }else if(associateCode.equals(AccountingAssociateCodeType.PARTICIPANT_SECURITY.getCode())){
					 if(Validations.validateIsNotNullAndNotEmpty(participant)&&
							 Validations.validateIsNotNullAndNotEmpty(security)){
						 keyAssociate=participant.concat(AccountingUtils.VERTICAL).concat(security);
					 }else{
						 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
					 }
					 
					 
				 }else if(associateCode.equals(AccountingAssociateCodeType.ISSUER_SECURITY.getCode())){
					 if(Validations.validateIsNotNullAndNotEmpty(issuer)&&
							 Validations.validateIsNotNullAndNotEmpty(security)){
						 keyAssociate=issuer.concat(AccountingUtils.VERTICAL).concat(security);
					 }else{
						 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
					 }
					 
					 
				 }else if(associateCode.equals(AccountingAssociateCodeType.HOLDER_SECURITY.getCode())){
					 if(Validations.validateIsNotNullAndNotEmpty(holder)&&
							 Validations.validateIsNotNullAndNotEmpty(security)){
						 keyAssociate=holder.concat(AccountingUtils.VERTICAL).concat(security);
					 }else{
						 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
					 }
					 
					 
				 }else if(associateCode.equals(AccountingAssociateCodeType.PARTICIPANT_SECURITY_MOVEMENT.getCode())){
					 if(Validations.validateIsNotNullAndNotEmpty(participant)&&
							 Validations.validateIsNotNullAndNotEmpty(security)&&
							 Validations.validateIsNotNullAndNotEmpty(movementType)){
						 keyAssociate=holder.concat(AccountingUtils.VERTICAL).concat(security).concat(movementType);
					 }else{
						 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
					 }
					 
					 
				 }
				 
			}
			 /**
			  * Setting Key Associate Code
			  */
			 accReceiptDetail.setKeyAssociateCode(keyAssociate);
			 
			 /***
			  * Add to list for sort
			  */
			 accountingReceiptDetailTo.setKeyAssociateCode(accReceiptDetail.getKeyAssociateCode());
			 accountingReceiptDetailTo.setAccountingReceiptDetail(accReceiptDetail);
			 listAccountingReceiptDetailTo.add(accountingReceiptDetailTo);
			  
		}
		 
		 
		 return listAccountingReceiptDetailTo;
	}
	
	
	

	
	
	
	
	/**
	 * Gets the associate code to receipt detail.
	 *
	 * @param listAccountingReceiptDetail the list accounting receipt detail
	 * @param associateCode the associate code
	 * @return the associate code to receipt detail
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<AccountingMonthResult> getAssociateCodeToMonthResult(List<AccountingMonthResult> listAccountingMonthResult,Integer associateCode) throws ServiceException{
				
		/**
		 * One Detail for each Detail Matrix
		  * For each code associated, grouped and putting it in a bag
		  * 
		  */
		 for (AccountingMonthResult accountingMonthResult : listAccountingMonthResult) {
	
			 /***
			  * AssociateCode by SCHEMA, combination according to AssociateCode
			  */
			 if(AccountingAssociateCodeType.PARTICIPANT.getCode().equals(associateCode)){
				 if(Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdParticipant())){
					 accountingMonthResult.setKeyAssociateCode(accountingMonthResult.getIdParticipant().toString());
				 }else{
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
					
				 }
				 
			 }else if(AccountingAssociateCodeType.SECURITY.getCode().equals(associateCode)){
				 if(Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdSecurityCodeFk())){
					 accountingMonthResult.setKeyAssociateCode(accountingMonthResult.getIdSecurityCodeFk().toString());
				 }else{
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
				 }
				 
			
			 }else if(AccountingAssociateCodeType.ISSUER.getCode().equals(associateCode)){
				 if(Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdIssuer())){
					 accountingMonthResult.setKeyAssociateCode(accountingMonthResult.getIdIssuer().toString());
				 }else{
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
					
				 }
				 
			 }else if(AccountingAssociateCodeType.PARTICIPANT_SECURITY.getCode().equals(associateCode)){
				 if(Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdParticipant())&&
						 Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdSecurityCodeFk())){
					 accountingMonthResult.setKeyAssociateCode(accountingMonthResult.getIdParticipant().toString()
							 .concat(AccountingUtils.VERTICAL)
							 .concat(accountingMonthResult.getIdSecurityCodeFk()));
				 }else{
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
				 }
				 
				 
			 }else if(AccountingAssociateCodeType.ISSUER_SECURITY.getCode().equals(associateCode)){
				 if(Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdIssuer())&&
						 Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdSecurityCodeFk())){
					 accountingMonthResult.setKeyAssociateCode(accountingMonthResult.getIdIssuer().toString()
							 .concat(AccountingUtils.VERTICAL)
							 .concat(accountingMonthResult.getIdSecurityCodeFk()));
				 }else{
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
				 }
				 
				 
			 }else if(AccountingAssociateCodeType.HOLDER_SECURITY.getCode().equals(associateCode)){
				 if(Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdHolder())&&
						 Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdSecurityCodeFk())){
					 accountingMonthResult.setKeyAssociateCode(accountingMonthResult.getIdHolder().toString()
							 .concat(AccountingUtils.VERTICAL)
							 .concat(accountingMonthResult.getIdSecurityCodeFk()));
				 }else{
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
				 }
				 
				 
			 }else if(AccountingAssociateCodeType.PARTICIPANT_SECURITY_MOVEMENT.getCode().equals(associateCode)){
				 if(Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdParticipant())&&
						 Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getIdSecurityCodeFk())&&
						 Validations.validateIsNotNullAndNotEmpty(accountingMonthResult.getMovementType())){
					 accountingMonthResult.setKeyAssociateCode(accountingMonthResult.getIdParticipant().toString()
							 .concat(AccountingUtils.VERTICAL)
							 .concat(accountingMonthResult.getIdSecurityCodeFk())
							 .concat(AccountingUtils.VERTICAL)
							 .concat(accountingMonthResult.getIdParticipant().toString()));
				 }else{
					 throw new ServiceException(ErrorServiceType.ACCOUNTING_NOT_FOUND_ASSOCIATED_CODE);
				 }
				 
				 
			 }
			  
		}
		 
		 
		 return listAccountingMonthResult;
	}
	
	
	
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	public void deleteProcess(AccountingProcessTo accountingProcessTo){
		LoggerUser loggerUser = accountingProcessTo.getLoggerUser();
		if(loggerUser!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
			transactionRegistry.putResource(RegistryContextHolderType.LOGGER_USER, loggerUser);
	
		}else{
			loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			accountingProcessTo.setLoggerUser(loggerUser);
		}
		
		List<AccountingProcess> accountingProcessList= findAccountingProcessWithAccuntingReceipt( accountingProcessTo );
		deleteAccountingProcessBatch(accountingProcessTo, accountingProcessList);
	}
	/**
	 *  DELETE ACCOUNTING PROCESS , ACCOUNTING RECEIPTS AND ACCOUNTING RECEIPTS DETAILS.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	@Asynchronous
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void  deleteAccountingProcess(AccountingProcessTo accountingProcessTo, List<AccountingProcess> accountingProcessList) {
		LoggerUser loggerUser = accountingProcessTo.getLoggerUser();
		if(loggerUser!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
			transactionRegistry.putResource(RegistryContextHolderType.LOGGER_USER, loggerUser);
	
		}else{
			loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			accountingProcessTo.setLoggerUser(loggerUser);
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(accountingProcessList)){
					
			for (AccountingProcess accountingProcess : accountingProcessList) {
				
				List<AccountingReceipt> accountingReceiptList = accountingProcess.getAccountingReceipt();
				if (Validations.validateListIsNotNullAndNotEmpty(accountingReceiptList)){
					
					for (AccountingReceipt accountingReceipt : accountingReceiptList) {
															
						List<AccountingReceiptDetail>	accountingReceiptDetailList = accountingReceipt.getAccountingReceiptDetail();
						
						if (Validations.validateListIsNotNullAndNotEmpty(accountingReceiptDetailList)){						
								for (AccountingReceiptDetail accountingReceiptDetail : accountingReceiptDetailList) {								
									 
									/**INVOKE deleteAccountinSourceLogger ***/
					
									/***
									 *  documentAccountingServiceBean.deleteAccountingSourceLoggerAndSourceDetailBatch(listIdSourceLoggerPk);
										documentAccountingServiceBean.deleteAccountingReceiptDetailBatch(listIdReceiptDetailPk);
										for (Long idAccountingReceiptsPk : listIdReceiptPk) {
											documentAccountingServiceBean.delete(AccountingReceipt.class,idAccountingReceiptsPk);
										}
									 */
									deleteAccountingSourceLogger(accountingReceiptDetail.getAccountingSourceLogger());
									documentAccountingServiceBean.delete(AccountingReceiptDetail.class, accountingReceiptDetail.getIdAccountingReceiptsDetailPk());							
								}
						}
						
						documentAccountingServiceBean.delete(AccountingReceipt.class,accountingReceipt.getIdAccountingReceiptsPk());
					}
					
				}
				
				documentAccountingServiceBean.delete(AccountingProcess.class,accountingProcess.getIdAccountingProcessPk());
				documentAccountingServiceBean.flushTransaction();
			 }
		 }
		
	}

	/**
	 * Delete Process Accounting Last To way Batch
	 * @param accountingProcessTo
	 * @param accountingProcessList
	 */
	@Asynchronous
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance
	public void  deleteAccountingProcessBatch(AccountingProcessTo accountingProcessTo, List<AccountingProcess> accountingProcessList) {
		LoggerUser loggerUser = accountingProcessTo.getLoggerUser();
		if(loggerUser!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
			transactionRegistry.putResource(RegistryContextHolderType.LOGGER_USER, loggerUser);
	
		}else{
			loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			accountingProcessTo.setLoggerUser(loggerUser);
		}
		
		List<Long> listIdSourceLoggerPk=new ArrayList<>();
		List<Long> listIdReceiptDetailPk=new ArrayList<>();
		List<Long> listIdReceiptPk=new ArrayList<>();
		List<Long> listIdProcessPk=new ArrayList<>();

		/**
		 * Get List To Pk For Delete Batch
		 * listIdReceiptPk: List To Primary Key AccountingReceipt
		 * listIdReceiptDetailPk: List To Primary Key AccountingReceiptDetail
		 * listIdSourceLoggerPk: List To Primary Key AccountingSourceLogger
		 */
		if (Validations.validateListIsNotNullAndNotEmpty(accountingProcessList)){
			for (AccountingProcess accountingProcess : accountingProcessList) {
				listIdProcessPk.add(accountingProcess.getIdAccountingProcessPk());
				List<AccountingReceipt> accountingReceiptList = accountingProcess.getAccountingReceipt();
				if (Validations.validateListIsNotNullAndNotEmpty(accountingReceiptList)){
					for (AccountingReceipt accountingReceipt : accountingReceiptList) {
						listIdReceiptPk.add(accountingReceipt.getIdAccountingReceiptsPk());
						List<AccountingReceiptDetail>	accountingReceiptDetailList = accountingReceipt.getAccountingReceiptDetail();
						
						if (Validations.validateListIsNotNullAndNotEmpty(accountingReceiptDetailList)){//
							for (AccountingReceiptDetail accountingReceiptDetail : accountingReceiptDetailList) {
								listIdReceiptDetailPk.add(accountingReceiptDetail.getIdAccountingReceiptsDetailPk());
								List<AccountingSourceLogger> listAccountingSourceLogger=accountingReceiptDetail.getAccountingSourceLogger();
								
								if(Validations.validateListIsNotNullAndNotEmpty(listAccountingSourceLogger)){
									
									for (AccountingSourceLogger accountingSourceLogger : listAccountingSourceLogger) {
										listIdSourceLoggerPk.add(accountingSourceLogger.getIdAccountingSourceLoggerPk());
									}
								}									
							}
						}
						
					}
				}
								
			}
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(listIdProcessPk)){
			
			//Delete Tables By Way Batch
			documentAccountingServiceBean.deleteAccountingSourceLoggerAndSourceDetailBatch(listIdSourceLoggerPk);
			documentAccountingServiceBean.deleteAccountingReceiptDetailBatch(listIdReceiptDetailPk);
			documentAccountingServiceBean.deleteAccountingReceiptAndDetailBatch(listIdReceiptPk);
			for (Long idProcessPk : listIdProcessPk) {
				documentAccountingServiceBean.delete(AccountingProcess.class, idProcessPk );
			}
			documentAccountingServiceBean.flushTransaction();
			documentAccountingServiceBean.clearTransaction();
		}

	}
	
	
	/**
	 *  DELETE ACCOUNTING SOURCE LOGGER AND  LOGGER DETAILS.
	 *
	 * @param accountingSourceLoggerList the accounting source logger list
	 */
	public void  deleteAccountingSourceLogger(List<AccountingSourceLogger> accountingSourceLoggerList) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		if (Validations.validateListIsNotNullAndNotEmpty(accountingSourceLoggerList)){
			
			for (AccountingSourceLogger accountingSourceLogger : accountingSourceLoggerList) {
				
				 List<AccountingSourceDetail> accountingSourceDetailList = accountingSourceLogger.getAccountingSourceDetail();
				 log.debug("Debug : "+accountingSourceLogger.getIdAccountingSourceLoggerPk());
				 log.info("Info   : "+accountingSourceLogger.getIdAccountingSourceLoggerPk());
				 log.error("Error : "+accountingSourceLogger.getIdAccountingSourceLoggerPk());
				 if (Validations.validateListIsNotNullAndNotEmpty(accountingSourceDetailList)){
					 
					 for (AccountingSourceDetail accountingSourceDetail : accountingSourceDetailList) {
						
					  documentAccountingServiceBean.delete(AccountingSourceDetail.class, accountingSourceDetail.getIdAccountingSourceDetailPk());
					}
				 }
				 
				 documentAccountingServiceBean.delete(AccountingSourceLogger.class, accountingSourceLogger.getIdAccountingSourceLoggerPk()); 
			}
			
		}
	}
	
	
	
//	@Asynchronous
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	@Performance
	public void  executeRefreshView(){
		documentAccountingServiceBean.executeRefreshViewAccountingReports();
	}
	
	/**
	 * Find accounting process by filter.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingProcess> findAccountingProcessByFilter(AccountingProcessTo accountingProcessTo){
		
		return documentAccountingServiceBean.findAccountingProcessByFilter(accountingProcessTo);
	}
	
	/**
	 * Gets the accounting process file content.
	 *
	 * @param idAccountingProcessPk the id accounting process pk
	 * @return the accounting process file content
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public byte[] getAccountingProcessFileContent(Long idAccountingProcessPk) {
	   	   Map<String, Object> parameters = new HashMap<String, Object>();
	   	   parameters.put("idAccountingProcessPk", idAccountingProcessPk);
	   	   return (byte[])documentAccountingServiceBean.findObjectByNamedQuery(AccountingProcess.ACCOUNTING_PROCESS_CONTENT_BY_PK, parameters);
	 }
	
	/**
	 * Find accounting process by code.
	 *
	 * @param idAccountingProcessPk the id accounting process pk
	 * @return the accounting process
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingProcess findAccountingProcessByCode(Long idAccountingProcessPk){
		return documentAccountingServiceBean.find(AccountingProcess.class, idAccountingProcessPk);
	}
	

	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingProcessReStartTO> findBalanceStartRecordByFilter(FilterStartBalanceTO searchFilterStartBalanceTO){
		
		List<AccountingProcess> list = documentAccountingServiceBean.findBalanceStartRecordByFilter(searchFilterStartBalanceTO);
		
		List<AccountingProcessReStartTO> accountingProcessReStarts = new ArrayList<>();
		AccountingProcessReStartTO accountingProcessReStart = null;
		for (AccountingProcess accountingProcess : list) {
			accountingProcessReStart = new AccountingProcessReStartTO();
			accountingProcessReStart.setId(accountingProcess.getIdAccountingProcessPk());
			accountingProcessReStart.setOrigenDesc("CARTERA");
			ParameterTable parameterTable = documentAccountingServiceBean.find(ParameterTable.class, accountingProcess.getStatus());
			accountingProcessReStart.setStatusDesc(parameterTable.getParameterName());
			accountingProcessReStart.setOperationDate(accountingProcess.getExecutionDate());
			accountingProcessReStart.setProcessType(accountingProcess.getProcessType());
			AccountingParameter accountingParameter = documentAccountingServiceBean.find(AccountingParameter.class, accountingProcess.getProcessType()) ;
			accountingProcessReStart.setProccessTypeDesc(accountingParameter.getParameterName());
			accountingProcessReStarts.add(accountingProcessReStart);
		}
		return accountingProcessReStarts;
	}
	
	
	
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingProcessReStartTO> findBalanceMonthlyRecordByFilter(FilterStartBalanceTO searchFilterStartBalanceTO){
		
		List<AccountingProcess> list = documentAccountingServiceBean.findMonthlyProcessRecordByFilter(searchFilterStartBalanceTO);
		
		List<AccountingProcessReStartTO> accountingProcessReStarts = new ArrayList<>();
		AccountingProcessReStartTO accountingProcessReStart = null;
		for (AccountingProcess accountingProcess : list) {
			accountingProcessReStart = new AccountingProcessReStartTO();
			accountingProcessReStart.setId(accountingProcess.getIdAccountingProcessPk());
			accountingProcessReStart.setOrigenDesc("CARTERA");
			ParameterTable parameterTable = documentAccountingServiceBean.find(ParameterTable.class, accountingProcess.getStatus());
			accountingProcessReStart.setStatusDesc(parameterTable.getParameterName());
			accountingProcessReStart.setOperationDate(accountingProcess.getExecutionDate());
			accountingProcessReStart.setProcessType(accountingProcess.getProcessType());
			AccountingParameter accountingParameter = documentAccountingServiceBean.find(AccountingParameter.class, accountingProcess.getProcessType()) ;
			accountingProcessReStart.setProccessTypeDesc(accountingParameter.getParameterName());
			accountingProcessReStarts.add(accountingProcessReStart);
		}
		return accountingProcessReStarts;
	}
	
	
	
	/**
	 * Registro de re inicio de saldos contables
	 * **/
	public Long registerReStartAccountingBalances(AccountingProcessTo accountingProcessTo){
		
		AccountingProcess  accountingProcess = new AccountingProcess();
		accountingProcess.setStartType(AccountingStartType.START.getCode());
		accountingProcess.setStatus(ReStartBalancesType.REGISTERED.getCode());
		byte[] fileXml = "texto".getBytes();
		accountingProcess.setProcessType(accountingProcessTo.getProcessType());
		accountingProcess.setExecutionDate(accountingProcessTo.getExecutionDate());
		accountingProcess.setFileXml(fileXml);
		
		/**Campos de auditoria*/
		accountingProcess.setLastModifyApp(userInfo.getLastModifyPriv());
		accountingProcess.setLastModifyDate(CommonsUtilities.currentDate());
		accountingProcess.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		accountingProcess.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		
		documentAccountingServiceBean.createWithFlush(accountingProcess);
		
		Long idAccountingProcessHistoryPk = accountingProcess.getIdAccountingProcessPk();
		//Registrar los saldos historicos
		//List<AccountingProcessHistoryDetail> list = documentAccountingServiceBean.getListAccountProcessHistoryDetails(accountingProcessTo.getExecutionDate(), idAccountingProcessHistoryPk,userInfo);
		//documentAccountingServiceBean.saveAll(list);
		return idAccountingProcessHistoryPk;
	}
	
	public void rejectAccountingProcess(AccountingProcessReStartTO accountingProcessSelected){
		AccountingProcess accountingProcess = documentAccountingServiceBean.find(AccountingProcess.class, accountingProcessSelected.getId());
		accountingProcess.setStatus(ReStartBalancesType.REJECTED.getCode());
		//documentAccountingServiceBean.updateWithOutFlush(accountingProcess);
	}
	
	
	/**MDZ*/

	/**
	 * Registro de proceso mensual
	 * **/
	public Long registerMonthlyProcessAccounting(AccountingProcessTo accountingProcessTo){
		
		AccountingProcess  accountingProcess = new AccountingProcess();
		accountingProcess.setStartType(AccountingStartType.MONTHLY.getCode());
		accountingProcess.setStatus(ReStartBalancesType.REGISTERED.getCode());
		byte[] fileXml = "texto".getBytes();
		accountingProcess.setProcessType(accountingProcessTo.getProcessType());
		accountingProcess.setExecutionDate(accountingProcessTo.getExecutionDate());
		accountingProcess.setFileXml(fileXml);
		accountingProcess.setFileSap(fileXml);
		
		/**Campos de auditoria*/
		accountingProcess.setLastModifyApp(userInfo.getLastModifyPriv());
		accountingProcess.setLastModifyDate(CommonsUtilities.currentDate());
		accountingProcess.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		accountingProcess.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		
		documentAccountingServiceBean.createWithFlush(accountingProcess);
		
		Long idAccountingProcessHistoryPk = accountingProcess.getIdAccountingProcessPk();
		return idAccountingProcessHistoryPk;
	}
}
