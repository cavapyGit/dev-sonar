package com.pradera.accounting.account.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountAccountingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AccountAccountingServiceBean extends CrudDaoServiceBean {
	
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
//	/** The em. */
//	@Inject @DepositaryDataBase
//	protected EntityManager em;
	
	/**
	 * Find List Accounting Account By AccountingAccountTo filter.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccountingAccount>  findAccountingAccountByFilter(AccountingAccountTo filter, String participantMnemonic)throws ServiceException{
		StringBuilder stringBuilderSql = new StringBuilder();	
		
		// AccountingAccount full Constructor
		stringBuilderSql.append("Select new AccountingAccount(aa.idAccountingAccountPk, aas , aa.accountCode ,   ");
		stringBuilderSql.append("  aa.description, aa.accountType, aa.natureType, aa.currency, aa.portFolio,   ");
		stringBuilderSql.append("  aa.indAccumulation, aa.indBalanceControl,  ");
		stringBuilderSql.append("  aa.indDimension, aa.indReference, aa.expiredDate, aa.adjustmentType, ");
		stringBuilderSql.append("  aa.auxiliaryType, aa.status, aa.accountingType,  aa.indSubAccount,   ");
		stringBuilderSql.append("  aa.indEntityAccount, aa.entityAccount, aa.instrumentType, aa.registerDate,  ");
		stringBuilderSql.append("  aa.registerUser, aa.updateDate, aa.updateUser, aa.cancelDate,  aa.cancelUser, ");
		stringBuilderSql.append("  (select (ap.parameterName||' - '||ap.description) as parameterDescriptionAccountType from AccountingParameter ap where ap.idAccountingParameterPk = aa.accountType),  ");
		stringBuilderSql.append("  (select (ap.parameterName||' - '||ap.description) as parameterDescriptionAuxiliaryType from AccountingParameter ap where ap.idAccountingParameterPk =aa.auxiliaryType),  ");
		stringBuilderSql.append("  (select (ap.parameterName) as parameterDescriptionNatureType from AccountingParameter ap where ap.idAccountingParameterPk =aa.natureType),  ");
		stringBuilderSql.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = aa.currency), ");
		stringBuilderSql.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = aa.status), ");
		stringBuilderSql.append("  (select (ap.parameterName||' - '||ap.description) as parameterDescriptionAccountType from AccountingParameter ap where ap.idAccountingParameterPk = aa.entityAccount), ");
		stringBuilderSql.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = aa.instrumentType) ) ");
		stringBuilderSql.append(" From AccountingAccount aa    ");
		stringBuilderSql.append("  left outer join  aa.accountingAccount aas ");

		stringBuilderSql.append(" WHERE 1=1   ");	

//		if (Validations.validateIsNotNullAndNotEmpty(participantMnemonic)){
//			 
//			stringBuilderSql.append(" and SUBSTRING(aa.accountCode, 15, 17) = :participantMnemonic  ");
//		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getIdAccountingAccountPk())){
			 
			stringBuilderSql.append(" and aa.idAccountingAccountPk = :idAccountingAccountPk  ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
			stringBuilderSql.append(" and aa.accountCode = :accountCode  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			 
			stringBuilderSql.append(" and aa.accountType = :accountType ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
			 
			stringBuilderSql.append(" and aa.natureType = :natureType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
			 
			stringBuilderSql.append(" and aa.adjustmentType = :adjustmentType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
			 
			stringBuilderSql.append(" and aa.auxiliaryType = :auxiliaryType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getStatus())){
			 
			stringBuilderSql.append(" and aa.status  = :status  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIndEntityAccount())){
			 
			stringBuilderSql.append(" and aa.indEntityAccount  = :indEntityAccount  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getEntityAccount())){
			 
			stringBuilderSql.append(" and aa.entityAccount  = :entityAccount  "); 
		}
		
		stringBuilderSql.append(" ORDER BY aa.accountCode, aa.natureType, aa.accountType, aa.auxiliaryType  ");
		
		Query query = em.createQuery(stringBuilderSql.toString());

		if (Validations.validateIsNotNullAndNotEmpty(filter.getIdAccountingAccountPk())){
			 
			query.setParameter("idAccountingAccountPk", filter.getIdAccountingAccountPk());
		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
			query.setParameter("accountCode",filter.getAccountCode() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			 
			query.setParameter("accountType",filter.getAccountType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
			 
			query.setParameter("natureType",filter.getNatureType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
			 
			query.setParameter("adjustmentType",filter.getAdjustmentType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
			 
			query.setParameter("auxiliaryType", filter.getAuxiliaryType());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getStatus())){
			 
			query.setParameter("status", filter.getStatus());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIndEntityAccount())){
			 
			query.setParameter("indEntityAccount", filter.getIndEntityAccount());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getEntityAccount())){
			 
			query.setParameter("entityAccount", filter.getEntityAccount());
		}
		
		List<AccountingAccount> listAccountingAccount=null;
		try{
			listAccountingAccount=query.getResultList();
		}
		 catch (Exception e) {
			log.debug(e.getMessage());
		}
		if(Validations.validateIsNotNullAndNotEmpty(participantMnemonic))
			return findAccountingAccountByParticipant(listAccountingAccount,participantMnemonic );
		return listAccountingAccount;
	}
	public List<AccountingAccount>  findAccountingAccountByParticipant(List<AccountingAccount> list,String participantMnemonic ){
		List<AccountingAccount> accountingAccounts=new ArrayList<>();
		for (AccountingAccount accountingAccount : list) {
			if(accountingAccount.getAccountCode().length()==18){
				if(accountingAccount.getAccountCode().substring(15,18).equalsIgnoreCase(participantMnemonic))
					accountingAccounts.add(accountingAccount);
			}
		}
		return accountingAccounts;
	}

	/**
	 * Find  Accounting Account By Filter.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccountingAccountTo>  findAccountingAccountToByFilter(AccountingAccountTo filter)throws ServiceException{
		
		List<AccountingAccountTo> listAccountingAccountTo=null;
		 StringBuilder sbQuery = new StringBuilder();
	     sbQuery.append("    select AA.ID_ACCOUNTING_ACCOUNT_PK,AA.ACCOUNT_CODE, AA.DESCRIPTION,");
	     sbQuery.append("    NVL(AA.STATUS ,0) AS STATUS,                  " );
	     sbQuery.append("    NVL((select description from ACCOUNTING_PARAMETER pt where PT.ID_ACCOUNTING_PARAMETER_PK=aa.ACCOUNT_TYPE),' ') AS DESC_ACCOUNT_TYPE,                  " );
	     sbQuery.append("    NVL((select description from parameter_table pt where PT.PARAMETER_TABLE_PK=aa.CURRENCY_TYPE ),' ')AS DESC_CURRENCY,                 " );
	     sbQuery.append("    NVL((select description from parameter_table pt where PT.PARAMETER_TABLE_PK=aa.STATUS ),' ') AS DESC_STATUS,                 " );
	     sbQuery.append("    NVL(AA.NATURE_TYPE ,0) AS NATURE_TYPE,                " );
	     sbQuery.append("    NVL(AA.IND_ACCUMULATION ,0) AS IND_ACCUMULATION,                       " );
	     sbQuery.append("    NVL(AA.IND_BALANCE_CONTROL ,0) AS IND_BALANCE_CONTROL,                       " );
	     sbQuery.append("    NVL(AA.IND_DIMENSION ,0) AS IND_DIMENSION,                       " );
	     sbQuery.append("    NVL(AA.ADJUSTMENT_TYPE ,0) AS ADJUSTMENT_TYPE,                       " );
	     sbQuery.append("    NVL(AA.AUXILIARY_TYPE ,0) AS AUXILIARY_TYPE,                       " );
	     sbQuery.append("    NVL(AA.ACCOUNT_TYPE  ,0) AS ACCOUNT_TYPE ,                " );
	     sbQuery.append("    NVL( AA.CURRENCY_TYPE,0) AS CURRENCY,               " );
	     
	     sbQuery.append("     AA.REGISTER_DATE,                           " );
	     sbQuery.append("    NVL(AA.REGISTER_USER ,' ') AS REGISTER_USER  ,              " );
	     sbQuery.append("    AA.UPDATE_DATE,                " );
	     sbQuery.append("    NVL(AA.UPDATE_USER ,' ') AS UPDATE_USER   ,             " );
	     sbQuery.append("     AA.CANCEL_DATE,               " );
	     sbQuery.append("    NVL(AA.CANCEL_USER ,' ') AS CANCEL_USER  ,              " );
	     sbQuery.append("    ( SELECT ACCOUNT_CODE FROM ACCOUNTING_ACCOUNT WHERE ID_ACCOUNTING_ACCOUNT_PK = AA.ID_ACCOUNTING_ACCOUNT_FK ) AS  FATHER_CODE ,   " );   
	     sbQuery.append("    NVL( AA.IND_SUB_ACCOUNT,0) AS IND_SUB_ACCOUNT , ID_ACCOUNTING_ACCOUNT_FK      " );
	     sbQuery.append("    from ACCOUNTING_ACCOUNT AA               " );
	     sbQuery.append("    WHERE 1=1                 " );  
	     	if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
	    	 sbQuery.append(" and AA.account_code = :accountCode  ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
				
				sbQuery.append(" and AA.account_type = :accountType ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
				 
				sbQuery.append(" and AA.NATURE_TYPE = :natureType  ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
				 
				sbQuery.append(" and AA.ADJUSTMENT_TYPE = :adjustmentType  ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getIdAccountingAccountFk())){
				 
				sbQuery.append(" and AA.ID_ACCOUNTING_ACCOUNT_FK = :idAccountingAccountFk  ");
			}
			
			sbQuery.append("  ORDER BY AA.ID_ACCOUNTING_ACCOUNT_PK                " );	
			
			
			Query query = em.createNativeQuery(sbQuery.toString());
			
			if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
				 
				query.setParameter("accountCode",filter.getAccountCode() );
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
				 
				query.setParameter("accountType",filter.getAccountType() );
			}
			if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
				 
				query.setParameter("natureType",filter.getNatureType() );
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
				 
				query.setParameter("adjustmentType",filter.getAdjustmentType() );
			}
			if (Validations.validateIsNotNullAndPositive(filter.getIdAccountingAccountFk())){
				 
				query.setParameter("idAccountingAccountFk", filter.getIdAccountingAccountFk() );
			}
			
			List<Object[]>  objectList = query.getResultList();
			listAccountingAccountTo=new ArrayList<AccountingAccountTo>();
			if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
				AccountingAccountTo accountingAccountTo=null;
				for(int i=0;i<objectList.size();i++){
					Object[] sResults = (Object[])objectList.get(i);
					accountingAccountTo=new AccountingAccountTo();
					accountingAccountTo.setIdAccountingAccountPk(Long.parseLong(sResults[0].toString()));
					accountingAccountTo.setAccountCode(sResults[1].toString());
					accountingAccountTo.setDescription(sResults[2].toString());
					accountingAccountTo.setStatus(Integer.parseInt(sResults[3].toString()));
					accountingAccountTo.setDescriptionTypeAccount(sResults[4].toString());
					accountingAccountTo.setDescriptionCurrency(sResults[5].toString());
					accountingAccountTo.setDescriptionStateAccount(sResults[6].toString());
					
					accountingAccountTo.setNatureType(Integer.parseInt(sResults[7].toString()));
					accountingAccountTo.setIndAccumulation(Integer.parseInt(sResults[8].toString()));
					accountingAccountTo.setIndBalanceControl(Integer.parseInt(sResults[9].toString()));
					accountingAccountTo.setIndDimension(Integer.parseInt(sResults[10].toString()));
					accountingAccountTo.setAdjustmentType(Integer.parseInt(sResults[11].toString()));
					accountingAccountTo.setAuxiliaryType(Integer.parseInt(sResults[12].toString()));
					accountingAccountTo.setAccountType(Integer.parseInt(sResults[13].toString()));
					accountingAccountTo.setCurrency(Integer.parseInt(sResults[14].toString()));
					
					accountingAccountTo.setRegisterDate((Date)sResults[15]);
					accountingAccountTo.setRegisterUser(sResults[16].toString());
					accountingAccountTo.setUpdateDate((Date)sResults[17]);
					accountingAccountTo.setUpdateUser(sResults[18].toString());
					accountingAccountTo.setCancelDate((Date)sResults[19]);
					accountingAccountTo.setCancelUser(sResults[20].toString());
					accountingAccountTo.setFatherCode(sResults[21]==null?"":sResults[21].toString());
					accountingAccountTo.setIndSubAccount(Integer.parseInt(sResults[22].toString()));
					accountingAccountTo.setIdAccountingAccountFk(sResults[23]==null?null:Long.parseLong(sResults[23].toString()));
					
					listAccountingAccountTo.add(accountingAccountTo);
					
				}
			}
		
		return listAccountingAccountTo;
	}
	
	/**
	 * Find List Accounting Account By Filter.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingAccount> findAllAccountingAccountByFilter(AccountingAccountTo filter){
		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" Select distinct aa From AccountingAccount aa left join fetch aa.accountingAccounts sr  ");

		sbQuery.append(" WHERE 1=1   ");	
		 	
		if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
			sbQuery.append(" and aa.accountCode = :accountCode  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			 
			sbQuery.append(" and aa.accountType = :accountType ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
			 
			sbQuery.append(" and aa.natureType = :natureType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
			 
			sbQuery.append(" and aa.adjustmentType = :adjustmentType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
			 
			sbQuery.append(" and aa.auxiliaryType = :auxiliaryType  ");
		}
		
		sbQuery.append(" ORDER BY aa.accountCode, aa.natureType, aa.accountType, aa.auxiliaryType  ");
		 
		Query query = em.createQuery(sbQuery.toString());   
		
		if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
			query.setParameter("accountCode",filter.getAccountCode() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			 
			query.setParameter("accountType",filter.getAccountType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
			 
			query.setParameter("natureType",filter.getNatureType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
			 
			query.setParameter("adjustmentType",filter.getAdjustmentType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
			 
			query.setParameter("auxiliaryType", filter.getAuxiliaryType());
		}
		
		List<AccountingAccount>  accountingAccountList= null ;

		accountingAccountList = query.getResultList();
		
		return accountingAccountList ;
	}
	
	/**
	 * Find Accounting Account By Account Code.
	 *
	 * @param filter AccountingAccountTo
	 * @return the accounting account
	 */
	public AccountingAccount findAccountingAccountByAccountingCode(AccountingAccountTo filter){
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select distinct aa From AccountingAccount aa ");
			
			if(Validations.validateIsNotNull(filter.getAccountCode()) ) {
				sbQuery.append(" Where  aa.accountCode = :code ");
			}
			
			sbQuery.append(" Order by aa.accountCode asc  ");
			
			 
			Query query = em.createQuery(sbQuery.toString());   
			
			if(Validations.validateIsNotNull(filter.getAccountCode()) ) {
				query.setParameter("code", filter.getAccountCode());			
			}
			AccountingAccount account=(AccountingAccount) query.getSingleResult();
			return account;
		
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * *
	 * Find Account Father By Id Accounting Account .
	 *
	 * @param filter the filter
	 * @return the accounting account
	 */
	public AccountingAccount findFatherAccountingAccountByFilterId(AccountingAccountTo filter){
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select distinct aa From AccountingAccount aa left join fetch aa.accountingAccounts sr  ");
			
			if(Validations.validateIsNotNull(filter.getAccountCode()) ) {
				sbQuery.append(" Where  aa.accountCode = :code ");
			}
			if(Validations.validateIsNotNull(filter.getIdAccountingAccountPk()) ) {
				sbQuery.append(" Where  aa.idAccountingAccountPk =  (select ac.accountingAccount from AccountingAccount ac where ac.idAccountingAccountPk=:id)");
			}
			
			sbQuery.append(" Order by aa.accountCode asc  ");
			
			 
			Query query = em.createQuery(sbQuery.toString());   
			
			if(Validations.validateIsNotNull(filter.getAccountCode()) ) {
				query.setParameter("code", filter.getAccountCode());			
			}
			if(Validations.validateIsNotNull(filter.getIdAccountingAccountPk()) ) {
				query.setParameter("id", filter.getIdAccountingAccountPk());			
			}
			
			return (AccountingAccount) query.getSingleResult();
		
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	public List<AccountingAccount> getListAccountingAccount(){
		String querySql = "SELECT ID_ACCOUNTING_ACCOUNT_PK,"
				+ " ACCOUNT_CODE,IND_SUB_ACCOUNT,CURRENCY_TYPE,"
				+ " INSTRUMENT_TYPE,IND_REFERENCE,"
				+ " ID_PORTFOLIO "
				+ " FROM ACCOUNTING_ACCOUNT ACCF "
				+ " WHERE IND_REFERENCE=0 "
				+ " AND IND_ACCUMULATION=1 "
				+ " AND IND_SUB_ACCOUNT=1 "
				+ " AND IND_ENTITY_ACCOUNT=0 "
				+ " AND ID_ACCOUNTING_ACCOUNT_PK "
				+ " IN (SELECT DISTINCT ID_ACCOUNTING_ACCOUNT_FK FROM ACCOUNTING_ACCOUNT ACCC WHERE ACCC.IND_ENTITY_ACCOUNT=1) "
				+ " ORDER BY ACCOUNT_CODE";
		Query query = em.createNativeQuery(querySql);
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)query.getResultList();
		}catch(NoResultException ex){
			ex.printStackTrace();
			return null;
		}
		List<AccountingAccount> accountingAccounts = new ArrayList<AccountingAccount>();
		AccountingAccount account=null;
		for (Object[] objects : objectData) {
			account=new AccountingAccount();
			account.setIdAccountingAccountFk(((BigDecimal)(objects[0])).longValue());
			account.setAccountType(7);//7 Cuentas de orden
			account.setAccountCode(objects[1].toString());
			account.setIndSubAccount(((BigDecimal)(objects[2])).intValue());
			account.setEntityAccount(68);//68 valores-valores
			account.setAuxiliaryType(12);//Valores
			account.setCurrency(((BigDecimal)(objects[3])).intValue());
			account.setInstrumentType(((BigDecimal)(objects[4])).intValue());
			account.setIndReference(((BigDecimal)(objects[5])).intValue());
			account.setPortFolio(((BigDecimal)(objects[6])).intValue());
			accountingAccounts.add(account);
		}
		return accountingAccounts;
	}
	
	
	/*Para verificar si ya existen registros con el nemonico del participante*/
	public int getCountAccountingAccountForParticipant(String participantMnemonic){
		String querySql = "select count(*) from ACCOUNTING_ACCOUNT where ACCOUNT_CODE like '%"+participantMnemonic+"'";
		Query query = em.createNativeQuery(querySql);
		Object object=query.getSingleResult();
		return ((BigDecimal)(object)).intValue();
	}
	public String getDescriptionParticipant(String participantMnemonic){
		String querySql = "select DESCRIPTION from participant where MNEMONIC='"+participantMnemonic+"'";
		Query query = em.createNativeQuery(querySql);
		try {
			String descripton=(String) query.getSingleResult();
			return descripton;
		} catch (Exception e) {
			return "";
		}
	}
}
