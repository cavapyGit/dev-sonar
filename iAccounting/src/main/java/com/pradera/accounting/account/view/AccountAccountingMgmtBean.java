package com.pradera.accounting.account.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.TransferEvent;

import com.pradera.accounting.account.facade.AccountAccountingServiceFacade;
import com.pradera.accounting.account.to.Formulate;
import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.type.AccountingAccountStateType;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingParameterType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.MovementType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountAccountingMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class AccountAccountingMgmtBean extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3682715134777318567L;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;

	/** The account accounting service facade. */
	@EJB
	AccountAccountingServiceFacade accountAccountingServiceFacade;

	/** The parameter accounting service facade. */
	@EJB
	ParameterAccountingServiceFacade parameterAccountingServiceFacade;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade accountingMatrixServiceFacade;

	/**  Account To for Search. */
	private AccountingAccountTo accountTo;
	
	/** The account selected. */
	private AccountingAccount accountSelected;
	
	/** The account by helper. */
	private AccountingAccountTo accountByHelper;
	
	/**  Entity for persist. */
	private AccountingAccount accountingAccount; // Use on register
	
	/** The validity initial date. */
	// private Boolean validityExpirationDateDisable;
	private Date validityInitialDate;
	
	/** The accounting account to data model. */
	private GenericDataModel<AccountingAccountTo> accountingAccountToDataModel;
	
	/** The accounting account data model. */
	private GenericDataModel<AccountingAccount> accountingAccountDataModel;
	
	/** The accounting account to temp. */
	private AccountingAccountTo accountingAccountToTemp = new AccountingAccountTo();

	/**  Variables for disabled. */
	private Boolean accountDisable;
	
	/** The account father disable. */
	private Boolean accountFatherDisable;
	
	/** The account type of adjustment disable. */
	private Boolean accountTypeOfAdjustmentDisable;
	
	/** The account ind entity disable. */
	private Boolean accountIndEntityDisable;
	
	/** The account entity disable. */
	private Boolean accountEntityDisable;

	/** The list accounting account to. */
	private List<AccountingAccountTo> listAccountingAccountTo;
	
	/** The list accounting account. */
	private List<AccountingAccount> listAccountingAccount;

	/** The list type account. */
	private List<AccountingParameter> listTypeAccount;
	
	/** The list branch account. */
	private List<AccountingParameter> listBranchAccount;
	
	/** The list type of adjustment. */
	private List<AccountingParameter> listTypeOfAdjustment;
	
	/** The list type auxiliary. */
	private List<AccountingParameter> listTypeAuxiliary;
	
	/** The list state account. */
	private List<ParameterTable> 	listStateAccount;
	
	/** The list instrument type. */
	private List<ParameterTable> 	listInstrumentType;
	
	/** The list nature type. */
	private List<AccountingParameter> listNatureType;
	
	/** The list partnership entity. */
	private List<AccountingParameter> listPartnershipEntity;
	
	/** The list currency. */
	private List<ParameterTable> listCurrency;

	/** The movement. */
	private MovementType movement;
	
	/** The description movement. */
	private String descriptionMovement;
	
	/** The accounting parameter. */
	private AccountingParameter accountingParameter;
	
	/** The accounting parameter to. */
	private com.pradera.core.component.accounting.account.to.AccountingParameterTo accountingParameterTo;

	/**  AccountingAccountTo of Helper *. */

	private AccountingAccountTo accountingByHelper;
	
	/** The flag account code. */
	private Boolean flagAccountCode = Boolean.FALSE;
	
	/** The disable account code. */
	private Boolean disableAccountCode = Boolean.FALSE;
	
	/** The disable helper account code. */
	private Boolean disableHelperAccountCode = Boolean.TRUE;
	
	/** The choose account code. */
	private Boolean chooseAccountCode = Boolean.FALSE;
	
	/** The choose helper account code. */
	private Boolean chooseHelperAccountCode = Boolean.FALSE;
	
	/** The choose type filter account code. */
	private Integer chooseTypeFilterAccountCode = 2;
	
	/** The ind type adjustment. */
	// private Boolean natureType = Boolean.FALSE;
	private Integer indTypeAdjustment = 0;
	
	/** The ind partnership entity. */
	private Integer indPartnershipEntity = 0;
	
	/** The type adjustment select. */
	private Boolean typeAdjustmentSelect = Boolean.TRUE;
	
	/** The partnership entity select. */
	private Boolean partnershipEntitySelect = Boolean.TRUE;

	private List<Participant> listParticipant;
	
	private String participantSelected;
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		accountTo = new AccountingAccountTo();
		accountSelected = new AccountingAccount();
		accountingAccount = new AccountingAccount();
		accountByHelper = new AccountingAccountTo();
		loadCmbDataSearch();
		movement = new MovementType();
		accountingParameter = new AccountingParameter();
		accountingParameterTo = new com.pradera.core.component.accounting.account.to.AccountingParameterTo();
		
		Participant participantTO = new Participant();		
		listParticipant=new ArrayList<Participant>(0);
		try {
			listParticipant=accountAccountingServiceFacade.getLisParticipantServiceBean(participantTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * On transfer.
	 *
	 * @param event the event
	 */
	public void onTransfer(TransferEvent event) {
		StringBuilder builder = new StringBuilder();
		for (Object item : event.getItems()) {
			builder.append(((Formulate) item).getName()).append("<br />");
		}

		FacesMessage msg = new FacesMessage();
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		msg.setSummary("Items Transferred");
		msg.setDetail(builder.toString());

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	/**
	 * Load registration page.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void loadRegistrationPage() throws ServiceException {

		/**
		 * BEGIN PRE CONDITIONS WHEN LOAD PAGE REGISTER
		 ****/
		// validityExpirationDateDisable=Boolean.TRUE;

		accountingAccount = new AccountingAccount();
		Calendar today = Calendar.getInstance();
		validityInitialDate = today.getTime();
		accountingAccount.setExpiredDate(today.getTime());
		
		loadCmbDataRegister();
		resetEnableAllFields();
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());

		/** END **/
	}

	/**
	 * Load cmb data search.
	 */
	@LoggerAuditWeb
	public void loadCmbDataSearch() {

		AccountingParameterTO accountingParameterTo = new AccountingParameterTO();

		if (Validations.validateListIsNullOrEmpty(listTypeAccount)) {
			log.debug("lista de cuentas:");
			accountingParameterTo.setParameterType(AccountingParameterType.TIPO_DE_CUENTA.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listTypeAccount = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
		}

		if (Validations.validateListIsNullOrEmpty(listTypeOfAdjustment)) {
			log.debug("lista de cuentas:");
			accountingParameterTo.setParameterType(AccountingParameterType.TIPO_DE_AJUSTE.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listTypeOfAdjustment = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
		}

		if (Validations.validateListIsNullOrEmpty(listNatureType)) {
			accountingParameterTo
					.setParameterType(AccountingParameterType.NATURALEZA_DE_LA_CUENTA
							.getCode());
			accountingParameterTo
					.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listNatureType = parameterAccountingServiceFacade
					.findListParameterAccounting(accountingParameterTo);
		}
		
		if (Validations.validateListIsNullOrEmpty(listPartnershipEntity)) {
			accountingParameterTo
					.setParameterType(AccountingParameterType.ENTIDAD_DE_CUENTA
							.getCode());
			accountingParameterTo
					.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listPartnershipEntity = parameterAccountingServiceFacade
					.findListParameterAccounting(accountingParameterTo);
		}
		
		if (Validations.validateListIsNullOrEmpty(listTypeAuxiliary)) {
			log.debug(" listTypeAuxiliary:");
			accountingParameterTo
					.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR
							.getCode());
			accountingParameterTo
					.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listTypeAuxiliary = parameterAccountingServiceFacade
					.findListParameterAccounting(accountingParameterTo);
		}

	}

	/**
	 * Load cmb data register.
	 */
	@LoggerAuditWeb
	public void loadCmbDataRegister() {

		ParameterTableTO parameterTableTO = new ParameterTableTO();
		AccountingParameterTO accountingParameterTo = new AccountingParameterTO();
		accountingByHelper = new AccountingAccountTo();
		try {
			if (Validations.validateListIsNullOrEmpty(listCurrency)) {
				log.debug("lista de monedas:");
				parameterTableTO.setState(1);
				parameterTableTO
						.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);

				listCurrency = generalParametersFacade
						.getListParameterTableServiceBean(parameterTableTO);
			}

			if (Validations.validateListIsNullOrEmpty(listTypeAccount)) {
				log.debug("lista de cuentas:");
				accountingParameterTo
						.setParameterType(AccountingParameterType.TIPO_DE_CUENTA
								.getCode());
				accountingParameterTo
						.setStatus(AccountingParameterStateType.ACTIVED
								.getCode());
				listTypeAccount = parameterAccountingServiceFacade
						.findListParameterAccounting(accountingParameterTo);
			}

			if (Validations.validateListIsNullOrEmpty(listBranchAccount)) {
				log.debug("listBranchAccount");
				accountingParameterTo
						.setParameterType(AccountingParameterType.SUCURSALES
								.getCode());
				accountingParameterTo
						.setStatus(AccountingParameterStateType.ACTIVED
								.getCode());
				listBranchAccount = parameterAccountingServiceFacade
						.findListParameterAccounting(accountingParameterTo);
			}

			if (Validations.validateListIsNullOrEmpty(listTypeOfAdjustment)) {
				log.debug("listTypeOfAdjustment");
				accountingParameterTo
						.setParameterType(AccountingParameterType.TIPO_DE_AJUSTE
								.getCode());
				accountingParameterTo
						.setStatus(AccountingParameterStateType.ACTIVED
								.getCode());
				listTypeOfAdjustment = parameterAccountingServiceFacade
						.findListParameterAccounting(accountingParameterTo);
			}

			if (Validations.validateListIsNullOrEmpty(listTypeAuxiliary)) {
				log.debug(" listTypeAuxiliary:");
				accountingParameterTo
						.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR
								.getCode());
				accountingParameterTo
						.setStatus(AccountingParameterStateType.ACTIVED
								.getCode());
				listTypeAuxiliary = parameterAccountingServiceFacade
						.findListParameterAccounting(accountingParameterTo);
			}

			if (Validations.validateListIsNullOrEmpty(listStateAccount)) {
				parameterTableTO.setState(1);
				parameterTableTO
						.setMasterTableFk(PropertiesConstants.PARAMETER_TYPE);
				listStateAccount = generalParametersFacade
						.getListParameterTableServiceBean(parameterTableTO);
			}
			
			if (Validations.validateListIsNullOrEmpty(listInstrumentType)) {
				parameterTableTO.setState(1);
				parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
				listInstrumentType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.accountingAccount.setRegisterUser(this.userInfo
				.getUserAccountSession().getUserName());
		this.accountingAccount.setRegisterDate(CommonsUtilities.currentDate());

		if (Validations.validateIsNotNullAndPositive(accountingAccount
				.getAdjustmentType())) {
			this.indTypeAdjustment = 1;

		} else {
			typeAdjustmentSelect = Boolean.TRUE;
			this.indTypeAdjustment = 0;
		}
		accountingAccount.setIndEntityAccount(0);
		this.accountingAccount.setIndSubAccount(0);
	}

	/**
	 * *.
	 */
	public void clearRegister() {

		accountTo = new AccountingAccountTo();
		this.accountingAccount = new AccountingAccount();

		this.accountingAccount.setRegisterUser(this.userInfo
				.getUserAccountSession().getUserName());
		this.accountingAccount.setRegisterDate(CommonsUtilities.currentDate());

		this.accountingAccount.setIndSubAccount(0);

		typeAdjustmentSelect = Boolean.TRUE;
		this.accountingAccount.setAdjustmentType(null);
		indTypeAdjustment = 0;

		accountingByHelper = null;

		JSFUtilities.resetComponent("frmAccountAccounting");
	}

	/**
	 * Go to.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {
		if (Validations.validateIsNotNull(accountSelected)) {
			if (viewToGo.equals("editAccount")) {

				viewToGo = "newAccount";
				if (accountSelected.getStatus().equals(
						AccountingAccountStateType.DELETED.getCode())) {
					showWarningDialog(
							GeneralConstants.LBL_HEADER_ALERT_WARNING,
							PropertiesConstants.WARNING_BOOK_ACCOUNT_CANCELLED,
							accountSelected.getAccountCode());
					return "";
				}
			} else if (viewToGo.equals("consultAccounting")) {
				viewToGo = "newAccount";
			}
		} else {

		}
		participantSelected=null;
		return viewToGo;
	}

	/**
	 * Before registration listener book account.
	 */
	public void beforeRegistrationListenerBookAccount() {
		log.debug("beforeRegistrationListenerBookAccount :");
		if(Validations.validateIsNotNullAndNotEmpty(participantSelected)){
			saveMassRegistrationAccounts();
			return;
		}
		Boolean flagSave = Boolean.FALSE;
		flagSave = Boolean.TRUE;

		/** Que todo este correcto */

		if (flagSave) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					PropertiesUtilities
							.getMessage(PropertiesConstants.CONFIRM_BOOK_ACCOUNT_SAVE));
			JSFUtilities
					.executeJavascriptFunction("cnfwBookAccountSave.show();");
		} else {

			showWarningDialog(GeneralConstants.LBL_HEADER_ALERT_WARNING,
					PropertiesConstants.CONFIRM_BOOK_ACCOUNT_SAVE);

		}

	}

	
	public void disabledFilters(){
		if(Validations.validateIsNotNullAndNotEmpty(participantSelected))
			this.getEvaluateDisableComponent();
		else{
			resetEnableAllFields();
			this.validateAccountFather();
		}
	}
	/**
	 * Before update listener.
	 */
	public void beforeUpdateListener() {
		log.debug("beforeUpdateListener :");
		Object codigo = accountingAccount.getAccountCode();
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
				PropertiesUtilities
						.getMessage(
								PropertiesConstants.CONFIRM_BOOK_ACCOUNT_UPDATE,
								codigo));
		JSFUtilities.executeJavascriptFunction("cnfwAccountUpdate.show()");

	}
	@LoggerAuditWeb
	public void saveMassRegistrationAccounts(){
		int dim=accountAccountingServiceFacade.getCountAccountingAccountForParticipant(participantSelected);
		if(dim>0L){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
								PropertiesUtilities.getMessage(PropertiesConstants.LABEL_FOR_ACCOUNT_RECORDS_COUNT,dim));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			return;
		}
			
		List<AccountingAccount> list=accountAccountingServiceFacade.getListAccountingAccount();
		AccountingAccount accountingAccountSaved = null;
		try {
			for (AccountingAccount accountingAccount : list) {
				accountingAccount.setAccountCode(accountingAccount.getAccountCode()+"."+participantSelected);
				accountingAccount.setDescription(accountAccountingServiceFacade.getDescriptionParticipant(participantSelected));
				accountingAccount.setIndEntityAccount(ViewOperationsType.REGISTER.getCode());
				accountingAccountSaved = accountAccountingServiceFacade.saveBookAccount(accountingAccount,getViewOperationType(),accountingAccount.getIdAccountingAccountFk());
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		if (Validations.validateIsNotNullAndNotEmpty(accountingAccountSaved)) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities
							.getMessage(
									PropertiesConstants.RECORD_LABEL_FOR_ACCOUNTS,participantSelected));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			cleanBookAccount();
			clearRegister();
			searchBookAccount();
		}
		// When exist one register saved before like this new.
	}

	/**
	 * Save book account.
	 */
	@LoggerAuditWeb
	public void saveBookAccount() {

		AccountingParameterTO accountingParameterTo = new AccountingParameterTO();
		List<AccountingParameter> listTypeAuxiliaryTemp;
		String code = accountingAccount.getAccountCode();
		accountingParameterTo.setIdAccountingParameterPk(accountingAccount.getAuxiliaryType());

		listTypeAuxiliaryTemp = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);

		AccountingAccount accountingAccountSaved = null;

		try {
			Long idAccountingAccountByHelper = accountingByHelper.getIdAccountingAccountPk();
			accountingAccountSaved = accountAccountingServiceFacade.saveBookAccount(accountingAccount,getViewOperationType(),idAccountingAccountByHelper);

		} catch (ServiceException e) {
			
			e.printStackTrace();
		}

		if (Validations.validateIsNotNullAndNotEmpty(accountingAccountSaved)) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities
							.getMessage(
									PropertiesConstants.SUCCESS_ACCOUNTING_ACCOUNT_SAVE,
									code));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			cleanBookAccount();
			clearRegister();
			searchBookAccount();
		}
		// When exist one register saved before like this new.
		else {
			executeAction();
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(
							PropertiesConstants.EXIST_ACCOUNTING_ACCOUNT_SAVED,
							code, listTypeAuxiliaryTemp.get(0).getParameterName()));
			JSFUtilities
					.executeJavascriptFunction("cnfWConfirmDialogExist.show();");
		}
	}

	/**
	 * Update accounting.
	 */
	@LoggerAuditWeb
	public void updateAccounting() {

		try {

			Long idAccountingAccountByHelper = accountingByHelper.getIdAccountingAccountPk();

			if (this.accountingAccount.getIndSubAccount() == 0){

				accountingAccount.setAccountingAccount(null);
			}

			accountAccountingServiceFacade.updateBookAccount(accountingAccount,getViewOperationType(), idAccountingAccountByHelper);

		} catch (ServiceException e) {
			e.printStackTrace();
		}
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(
						PropertiesConstants.SUCCESS_ACCOUNTING_ACCOUNT_UPDATE,
						accountingAccount.getAccountCode()));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");

		searchBookAccount();
	}

	/**
	 *  Search Book Account *.
	 */
	@LoggerAuditWeb
	public void searchBookAccount() {
		log.debug("searchBookAccount :");

		try {
			accountSelected = null;
			listAccountingAccount = new ArrayList<AccountingAccount>();

			/**
			 * Accounting Account father, if enabled should pull all the
			 * parent's children
			 **/
			if (Validations.validateIsNotNull(accountByHelper)
					&& Validations.validateIsNotNull(accountByHelper
							.getIdAccountingAccountPk())) {
				accountTo.setIdAccountingAccountFk(accountByHelper
						.getIdAccountingAccountPk());

				/** Helper Code **/
				accountTo.setAccountCode(accountByHelper.getAccountCode());

				List<AccountingAccount> listAccountingAccounts = accountAccountingServiceFacade
						.findAccountParentDescWithDescription(accountTo);

				accountingAccountDataModel = new GenericDataModel<AccountingAccount>(
						listAccountingAccounts);
			} else {

				listAccountingAccount = accountAccountingServiceFacade
						.findAccountingAccountByFilter(accountTo,participantSelected);
				accountingAccountDataModel = new GenericDataModel<AccountingAccount>(
						listAccountingAccount);
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (accountingAccountDataModel.getRowCount() > 0) {

			setTrueValidButtons();
		}

	}

	/**
	 * *
	 * Amarre de Tipo Diario Auxiliar con Tipo de Cuenta.
	 */
	@LoggerAuditWeb
	public void dynamicAuxiliarytype() {

		/** Use on Account Type **/
		AccountingParameterTO accountingParameterTo = new AccountingParameterTO();

		this.accountingAccount.setAccountingType(null);
		if (PropertiesConstants.ADM.equals(this.accountingAccount
				.getAuxiliaryType())) {

			accountingParameterTo
					.setParameterType(AccountingParameterType.TIPO_DE_CUENTA
							.getCode());
			accountingParameterTo
					.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listTypeAccount = parameterAccountingServiceFacade
					.findListParameterAccounting(accountingParameterTo);

			List<AccountingParameter> listAccountTypeTemp = AccountingUtils
					.cloneList(listTypeAccount);
			Collection<AccountingParameter> collectionRemove = new ArrayList<AccountingParameter>();

			for (AccountingParameter accountingParameter : listAccountTypeTemp) {
				if (accountingParameter.getIdAccountingParameterPk().equals(
						PropertiesConstants.CUENTAS_ORDEN.intValue())) {
					collectionRemove.add(accountingParameter);
				}
			}
			listAccountTypeTemp.removeAll(collectionRemove);
			listTypeAccount = AccountingUtils.cloneList(listAccountTypeTemp);
		}
		/** When Auxiliary Type is Valor y Fondo **/
		else if (PropertiesConstants.VALOR.equals(this.accountingAccount
				.getAuxiliaryType())
				|| PropertiesConstants.FONDO.equals(this.accountingAccount
						.getAuxiliaryType())) {
			accountingParameterTo
					.setParameterType(AccountingParameterType.TIPO_DE_CUENTA
							.getCode());
			accountingParameterTo
					.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listTypeAccount = parameterAccountingServiceFacade
					.findListParameterAccounting(accountingParameterTo);

			List<AccountingParameter> listAccountTypeTemp = AccountingUtils
					.cloneList(listTypeAccount);
			Collection<AccountingParameter> collectionRemove = new ArrayList<AccountingParameter>();

			for (AccountingParameter accountingParameter : listAccountTypeTemp) {
				if (accountingParameter.getIdAccountingParameterPk().equals(
						PropertiesConstants.CUENTAS_FUERA_ORDEN.intValue())) {

					collectionRemove.add(accountingParameter);
				}
			}
			listAccountTypeTemp.removeAll(collectionRemove);
			listTypeAccount = AccountingUtils.cloneList(listAccountTypeTemp);
		} else {

		}

	}

	/**
	 * Choose filter account code.
	 */
	public void chooseFilterAccountCode() {

		if (chooseTypeFilterAccountCode == 1) {
			this.accountTo.setAccountCode(null);
			this.disableAccountCode = Boolean.TRUE;
			this.disableHelperAccountCode = Boolean.FALSE;

			if (Validations.validateIsNotNullAndNotEmpty(accountByHelper)
					&& Validations.validateIsNotNullAndNotEmpty(accountByHelper
							.getAccountCode())) {
				accountByHelper.setAccountCode(null);
				accountByHelper.setDescription(null);
			}

		} else if (chooseTypeFilterAccountCode == 2) {
			this.accountTo.setAccountCode(null);
			this.disableAccountCode = Boolean.FALSE;
			this.disableHelperAccountCode = Boolean.TRUE;

			if (Validations.validateIsNotNullAndNotEmpty(accountByHelper)
					&& Validations.validateIsNotNullAndNotEmpty(accountByHelper
							.getAccountCode())) {
				accountByHelper.setAccountCode(null);
				accountByHelper.setDescription(null);
			}

		}

		cleanResults();

	}

	/**
	 *  Clean Book Account *.
	 */
	public void cleanBookAccount() {

		this.accountingAccount = new AccountingAccount();

		log.debug("cleanBookAccount :");
		accountingAccountToDataModel = null;
		accountTo = new AccountingAccountTo();
		accountByHelper = new AccountingAccountTo();
		accountSelected = null;
		accountingAccountDataModel = null;
		chooseTypeFilterAccountCode = null;

		// filter accountCode
		disableAccountCode = Boolean.FALSE;
		disableHelperAccountCode = Boolean.TRUE;
		chooseTypeFilterAccountCode = 2;
	}

	/**
	 * Clean results.
	 */
	public void cleanResults() {
		accountingAccountDataModel = null;
		accountByHelper = new AccountingAccountTo();
	}

	/**
	 * Before modify book account.
	 *
	 * @param e the e
	 */
	@LoggerAuditWeb
	public void beforeModifyBookAccount(ActionEvent e) {
		if (Validations.validateIsNotNull(accountSelected)) {

			this.setViewOperationType(ViewOperationsType.MODIFY.getCode());
			loadCmbDataRegister();
			this.loadAccountingAccount(accountSelected);
			getDisableComponentOnModify();

		} else {
			showWarningDialog(GeneralConstants.LBL_HEADER_ALERT_WARNING,
					PropertiesConstants.MODIFY_BOOK_ACCOUNT_NO_SELECTED);
		}

	}

	/**
	 * Call WarningDialog General.
	 *
	 * @param header the header
	 * @param message the message
	 * @param params the params
	 */
	public void showWarningDialog(String header, String message,
			Object... params) {
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(header),
				PropertiesUtilities.getMessage(message, params));
		JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
	}

	/**
	 * Check book account.
	 *
	 * @param e the e
	 */
	public void checkBookAccount(SelectEvent e) {
		AccountingAccount accountTo = (AccountingAccount) e.getObject();
		log.debug("book account checking " + accountTo.getDescription());
	}

	/**
	 * Reset enable all fields.
	 */
	public void resetEnableAllFields() {
		accountDisable = Boolean.FALSE;
		accountFatherDisable = Boolean.FALSE;
		accountTypeOfAdjustmentDisable = Boolean.FALSE;
		accountEntityDisable=Boolean.FALSE;
		accountIndEntityDisable=Boolean.FALSE;
	}

	/**
	 * Gets the evaluate disable component.
	 *
	 * @return the evaluate disable component
	 */
	public void getEvaluateDisableComponent() {
		accountDisable = Boolean.TRUE;
		accountFatherDisable = Boolean.TRUE;
		accountTypeOfAdjustmentDisable = Boolean.TRUE;
		accountEntityDisable=Boolean.TRUE;
		accountIndEntityDisable=Boolean.TRUE;
	}

	/**
	 * Gets the disable component on modify.
	 *
	 * @return the disable component on modify
	 */
	public void getDisableComponentOnModify() {
		accountDisable = Boolean.TRUE;
		accountFatherDisable = Boolean.FALSE;
		accountTypeOfAdjustmentDisable = Boolean.FALSE;
		accountEntityDisable=Boolean.FALSE;
		accountIndEntityDisable=Boolean.FALSE;
	}

	/**
	 * Accounting on consult.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	public void accountingOnConsult(ActionEvent event) throws ServiceException {

		if (event != null) {
			AccountingAccount accountSelection = (AccountingAccount) event
					.getComponent().getAttributes().get("blockInformation");
			loadCmbDataRegister();
			this.loadAccountingAccount(accountSelection);
			getEvaluateDisableComponent();
			this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
			accountSelected = accountSelection;
		}
	}

	/**
	 * Load accounting account.
	 *
	 * @param accountToSelection the account to selection
	 */
	@LoggerAuditWeb
	protected void loadAccountingAccount(AccountingAccount accountToSelection) {
		log.debug("cargando objeto para ser modificado o consultado");

		AccountingAccountTo accountingAccountToTemp = null;

		accountingAccount.setIdAccountingAccountPk(accountToSelection
				.getIdAccountingAccountPk());
		accountingAccount.setAccountCode(accountToSelection.getAccountCode());
		accountingAccount.setDescription(accountToSelection.getDescription());
		accountingAccount.setAccountType(accountToSelection.getAccountType());
		accountingAccount.setAccountingType(accountToSelection
				.getAccountingType());
		accountingAccount.setInstrumentType(accountToSelection
				.getInstrumentType());
		accountingAccount.setNatureType(accountToSelection.getNatureType());
		accountingAccount.setCurrency(accountToSelection.getCurrency());
		accountingAccount.setIndAccumulation(accountToSelection
				.getIndAccumulation());
		accountingAccount.setIndBalanceControl(accountToSelection
				.getIndBalanceControl());
		accountingAccount.setIndDimension(accountToSelection.getIndDimension());
		accountingAccount.setAdjustmentType(accountToSelection
				.getAdjustmentType());
		accountingAccount.setAuxiliaryType(accountToSelection
				.getAuxiliaryType());
		accountingAccount.setRegisterDate(CommonsUtilities
				.truncateDateTime(accountToSelection.getRegisterDate()));
		accountingAccount.setRegisterUser(accountToSelection.getRegisterUser());
		accountingAccount.setIndSubAccount(accountToSelection
				.getIndSubAccount());

		accountingAccount.setIndEntityAccount(accountToSelection.getIndEntityAccount());
		accountingAccount.setEntityAccount(accountToSelection.getEntityAccount());
		accountingAccount.setFatherCode(null);
		accountingAccount.setFatherDescription(null);

		if (Validations.validateIsNotNullAndNotEmpty(accountToSelection
				.getAccountingAccount())
				&& Validations.validateIsNotNullAndNotEmpty(accountToSelection
						.getAccountingAccount().getIdAccountingAccountPk())) {
			try {

				AccountingAccountTo accountToCodeTemp = new AccountingAccountTo();
				accountToCodeTemp.setIdAccountingAccountPk(accountToSelection
						.getAccountingAccount().getIdAccountingAccountPk());

				listAccountingAccount = accountAccountingServiceFacade
						.findAccountingAccountByFilter(accountToCodeTemp,participantSelected);

				accountingAccount.setFatherCode(listAccountingAccount.get(0)
						.getAccountCode());
				accountingAccount.setFatherDescription(listAccountingAccount
						.get(0).getDescription());

				accountingByHelper.setAccountCode(listAccountingAccount.get(0)
						.getAccountCode());
				accountingByHelper.setDescription(listAccountingAccount.get(0)
						.getDescription());

				this.accountingAccount.setIndSubAccount(1);

			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (Validations.validateIsNotNull(accountToSelection.getUpdateDate())) {

			accountingAccount.setUpdateDate(CommonsUtilities
					.truncateDateTime(accountToSelection.getUpdateDate()));
		}
		if (Validations.validateIsNotNull(accountToSelection.getUpdateUser())) {

			accountingAccount.setUpdateUser(accountToSelection.getUpdateUser());
		}
		if (Validations.validateIsNotNull(accountToSelection.getCancelDate())) {

			accountingAccount.setCancelDate(accountToSelection.getCancelDate());
		}
		if (Validations.validateIsNotNull(accountToSelection.getCancelUser())) {

			accountingAccount.setCancelUser(accountToSelection.getCancelUser());
		}
		if (Validations.validateIsNotNull(accountToSelection.getFatherCode())) {

			accountingAccount.setFatherCode(accountToSelection.getFatherCode());
		}

	}

//	static public class ColumnModel implements Serializable {
//
//		/**
//		 * 
//		 */
//		private static final long serialVersionUID = 2813240270876606445L;
//		private String header;
//		private String property;
//
//		public ColumnModel(String header, String property) {
//			this.header = header;
//			this.property = property;
//		}
//
//		public String getHeader() {
//			return header;
//		}
//
//		public String getProperty() {
//			return property;
//		}
//	}

	/**
 * Before delete book account.
 *
 * @param event the event
 */
@LoggerAuditWeb
	public void beforeDeleteBookAccount(ActionEvent event) {

		if (Validations.validateIsNotNullAndNotEmpty(accountSelected)) {

			Object code = accountSelected.getAccountCode();
			if (accountSelected.getStatus().equals(
					AccountingAccountStateType.DELETED.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ACCOUNTING_ACCOUNT_DELETED,
										code));
				JSFUtilities
						.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			}

			// When is registered

			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities
							.getMessage(
									PropertiesConstants.CONFIRM_ACCOUNTING_ACCOUNT_DELETED,
									code));
			JSFUtilities
					.executeJavascriptFunction("cnfwAccountDeleted.show();");

		}

		else {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}

	}

	/**
	 * Delete accounting account.
	 */
	@LoggerAuditWeb
	public void deleteAccountingAccount() {

		String stringSchemaCode = null;

		try {

			if (Validations.validateIsNotNull(this.accountSelected)) {

				// Validate if the account is not used in many schema
				List<AccountingSchema> listAccountingSchema = new ArrayList<AccountingSchema>();

				listAccountingSchema = accountingMatrixServiceFacade
						.findAccountingAccountOnMatrixDetail(this.accountSelected
								.getIdAccountingAccountPk());

				if (!(Validations
						.validateListIsNotNullAndNotEmpty(listAccountingSchema))) {

					// Change the statu to delete
					AccountingAccount accountingAccountToTemp = accountAccountingServiceFacade
							.getAccountingAccountByPk(this.accountSelected
									.getIdAccountingAccountPk());

					accountingAccountToTemp
							.setStatus(AccountingAccountStateType.DELETED
									.getCode());
					accountAccountingServiceFacade
							.updateAccountingStatus(accountingAccountToTemp);

					searchBookAccount();
					return;

				} else {

					// Show schema references
					if (listAccountingSchema.size() > 1)
						stringSchemaCode = AccountingUtils
								.convertListObjectToString(AccountingUtils
										.convertListAccountingSchemaObject(listAccountingSchema));
					else
						stringSchemaCode = listAccountingSchema.get(0)
								.getSchemaCode();

					showMessageOnDialog(
							PropertiesUtilities
									.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities
									.getMessage(
											PropertiesConstants.WARNING_ACCOUNTING_ACCOUNT_IS_USED,
											stringSchemaCode));
					JSFUtilities
							.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
				}
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Validate.
	 */
	public void validateAccountFather() {

		/***
		 * Option NOT
		 */
		if (GeneralConstants.ZERO_VALUE_INTEGER.equals(accountingAccount
				.getIndSubAccount())) {

			accountingAccount.setAuxiliaryType(null);
			accountingAccount.setAccountType(null);
			accountingAccount.setAdjustmentType(null);
			indTypeAdjustment = null;

			accountFatherDisable = Boolean.FALSE;
			accountTypeOfAdjustmentDisable = Boolean.FALSE;
			accountIndEntityDisable=Boolean.FALSE;
		}
		/***
		 * OPTION YES
		 */
		else if (GeneralConstants.ONE_VALUE_INTEGER.equals(accountingAccount
				.getIndSubAccount())) {

			accountingAccount.setAuxiliaryType(null);
			accountingAccount.setAccountType(null);
			accountingAccount.setAdjustmentType(null);
			indTypeAdjustment = null;

			accountFatherDisable = Boolean.TRUE;
			accountTypeOfAdjustmentDisable = Boolean.TRUE;
			accountIndEntityDisable=Boolean.TRUE;

		}
		accountingByHelper = new AccountingAccountTo();

	}

	/**
	 * Validate adjustment type.
	 */
	public void validateAdjustmentType() {

		this.accountingAccount.setAdjustmentType(null);
		if (this.indTypeAdjustment == 0) {
			typeAdjustmentSelect = Boolean.TRUE;
		} else
			typeAdjustmentSelect = Boolean.FALSE;

	}
	
	/**
	 * Validate partnership entity.
	 */
	public void validatePartnershipEntity() {

		this.accountingAccount.setEntityAccount(null);
		if (this.accountingAccount.getIndEntityAccount() == 0) {
			partnershipEntitySelect = Boolean.TRUE;
		} else
			partnershipEntitySelect = Boolean.FALSE;

	}
	
	  public Participant searchParticipantOnListFromPK(Long idParticipantPk){
			for(Participant participant: listParticipant)
				if(participant.getIdParticipantPk().equals(idParticipantPk))
					return participant;
			return null;
		}

	/**
	 * Gets the accounting parameter to.
	 *
	 * @return the accounting parameter to
	 */
	public com.pradera.core.component.accounting.account.to.AccountingParameterTo getAccountingParameterTo() {
		return accountingParameterTo;
	}

	/**
	 * Sets the accounting parameter to.
	 *
	 * @param accountingParameterTo the new accounting parameter to
	 */
	public void setAccountingParameterTo(
			com.pradera.core.component.accounting.account.to.AccountingParameterTo accountingParameterTo) {
		this.accountingParameterTo = accountingParameterTo;
	}

	/**
	 * Gets the accounting parameter.
	 *
	 * @return the accounting parameter
	 */
	public AccountingParameter getAccountingParameter() {
		return accountingParameter;
	}

	/**
	 * Sets the accounting parameter.
	 *
	 * @param accountingParameter the new accounting parameter
	 */
	public void setAccountingParameter(AccountingParameter accountingParameter) {
		this.accountingParameter = accountingParameter;
	}

	/**
	 * Gets the movement.
	 *
	 * @return the movement
	 */
	public MovementType getMovement() {
		return movement;
	}

	/**
	 * Sets the movement.
	 *
	 * @param movement the new movement
	 */
	public void setMovement(MovementType movement) {
		this.movement = movement;
	}

	/**
	 * Gets the description movement.
	 *
	 * @return the description movement
	 */
	public String getDescriptionMovement() {
		return descriptionMovement;
	}

	/**
	 * Sets the description movement.
	 *
	 * @param descriptionMovement the new description movement
	 */
	public void setDescriptionMovement(String descriptionMovement) {
		this.descriptionMovement = descriptionMovement;
	}

	/**
	 * Gets the list currency.
	 *
	 * @return the list currency
	 */
	public List<ParameterTable> getListCurrency() {
		return listCurrency;
	}

	/**
	 * Sets the list currency.
	 *
	 * @param listCurrency the new list currency
	 */
	public void setListCurrency(List<ParameterTable> listCurrency) {
		this.listCurrency = listCurrency;
	}

	/**
	 * Gets the accounting account.
	 *
	 * @return the accounting account
	 */
	public AccountingAccount getAccountingAccount() {
		return accountingAccount;
	}

	/**
	 * Sets the accounting account.
	 *
	 * @param accountingAccount the new accounting account
	 */
	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}

	/**
	 * Gets the account disable.
	 *
	 * @return the account disable
	 */
	public Boolean getAccountDisable() {
		return accountDisable;
	}

	/**
	 * Sets the account disable.
	 *
	 * @param accountDisable the new account disable
	 */
	public void setAccountDisable(Boolean accountDisable) {
		this.accountDisable = accountDisable;
	}

	/**
	 * Gets the accounting account data model.
	 *
	 * @return the accounting account data model
	 */
	public GenericDataModel<AccountingAccount> getAccountingAccountDataModel() {
		return accountingAccountDataModel;
	}

	/**
	 * Sets the accounting account data model.
	 *
	 * @param accountingAccountDataModel the new accounting account data model
	 */
	public void setAccountingAccountDataModel(
			GenericDataModel<AccountingAccount> accountingAccountDataModel) {
		this.accountingAccountDataModel = accountingAccountDataModel;
	}

	/**
	 * Gets the list accounting account.
	 *
	 * @return the list accounting account
	 */
	public List<AccountingAccount> getListAccountingAccount() {
		return listAccountingAccount;
	}

	/**
	 * Sets the list accounting account.
	 *
	 * @param listAccountingAccount the new list accounting account
	 */
	public void setListAccountingAccount(
			List<AccountingAccount> listAccountingAccount) {
		this.listAccountingAccount = listAccountingAccount;
	}

	/**
	 * Gets the list type account.
	 *
	 * @return the list type account
	 */
	public List<AccountingParameter> getListTypeAccount() {
		return listTypeAccount;
	}

	/**
	 * Sets the list type account.
	 *
	 * @param listTypeAccount the new list type account
	 */
	public void setListTypeAccount(List<AccountingParameter> listTypeAccount) {
		this.listTypeAccount = listTypeAccount;
	}

	/**
	 * Gets the list branch account.
	 *
	 * @return the list branch account
	 */
	public List<AccountingParameter> getListBranchAccount() {
		return listBranchAccount;
	}

	/**
	 * Sets the list branch account.
	 *
	 * @param listBranchAccount the new list branch account
	 */
	public void setListBranchAccount(List<AccountingParameter> listBranchAccount) {
		this.listBranchAccount = listBranchAccount;
	}

	/**
	 * Gets the list type of adjustment.
	 *
	 * @return the list type of adjustment
	 */
	public List<AccountingParameter> getListTypeOfAdjustment() {
		return listTypeOfAdjustment;
	}

	/**
	 * Sets the list type of adjustment.
	 *
	 * @param listTypeOfAdjustment the new list type of adjustment
	 */
	public void setListTypeOfAdjustment(
			List<AccountingParameter> listTypeOfAdjustment) {
		this.listTypeOfAdjustment = listTypeOfAdjustment;
	}

	/**
	 * Gets the list type auxiliary.
	 *
	 * @return the list type auxiliary
	 */
	public List<AccountingParameter> getListTypeAuxiliary() {
		return listTypeAuxiliary;
	}

	/**
	 * Sets the list type auxiliary.
	 *
	 * @param listTypeAuxiliary the new list type auxiliary
	 */
	public void setListTypeAuxiliary(List<AccountingParameter> listTypeAuxiliary) {
		this.listTypeAuxiliary = listTypeAuxiliary;
	}

	/**
	 * Gets the list state account.
	 *
	 * @return the list state account
	 */
	public List<ParameterTable> getListStateAccount() {
		return listStateAccount;
	}

	/**
	 * Sets the list state account.
	 *
	 * @param listStateAccount the new list state account
	 */
	public void setListStateAccount(List<ParameterTable> listStateAccount) {
		this.listStateAccount = listStateAccount;
	}


	/**
	 * Gets the list instrument type.
	 *
	 * @return the listInstrumentType
	 */
	public List<ParameterTable> getListInstrumentType() {
		return listInstrumentType;
	}

	/**
	 * Sets the list instrument type.
	 *
	 * @param listInstrumentType the listInstrumentType to set
	 */
	public void setListInstrumentType(List<ParameterTable> listInstrumentType) {
		this.listInstrumentType = listInstrumentType;
	}

	/**
	 * Gets the accounting account to temp.
	 *
	 * @return the accounting account to temp
	 */
	public AccountingAccountTo getAccountingAccountToTemp() {
		return accountingAccountToTemp;
	}

	/**
	 * Sets the accounting account to temp.
	 *
	 * @param accountingAccountToTemp the new accounting account to temp
	 */
	public void setAccountingAccountToTemp(
			AccountingAccountTo accountingAccountToTemp) {
		this.accountingAccountToTemp = accountingAccountToTemp;
	}

	/**
	 * Gets the account by helper.
	 *
	 * @return the accountByHelper
	 */
	public AccountingAccountTo getAccountByHelper() {
		return accountByHelper;
	}

	/**
	 * Sets the account by helper.
	 *
	 * @param accountByHelper            the accountByHelper to set
	 */
	public void setAccountByHelper(AccountingAccountTo accountByHelper) {
		this.accountByHelper = accountByHelper;
	}

	/**
	 * Gets the accounting by helper.
	 *
	 * @return the accountingByHelper
	 */
	public AccountingAccountTo getAccountingByHelper() {
		return accountingByHelper;
	}

	/**
	 * Gets the account to.
	 *
	 * @return the account to
	 */
	public AccountingAccountTo getAccountTo() {
		return accountTo;
	}

	/**
	 * Sets the account to.
	 *
	 * @param accountTo the new account to
	 */
	public void setAccountTo(AccountingAccountTo accountTo) {
		this.accountTo = accountTo;
	}

	/**
	 * Gets the validity initial date.
	 *
	 * @return the validity initial date
	 */
	public Date getValidityInitialDate() {
		return validityInitialDate;
	}

	/**
	 * Sets the validity initial date.
	 *
	 * @param validityInitialDate the new validity initial date
	 */
	public void setValidityInitialDate(Date validityInitialDate) {
		this.validityInitialDate = validityInitialDate;
	}

	/**
	 * Gets the account selected.
	 *
	 * @return the accountSelected
	 */
	public AccountingAccount getAccountSelected() {
		return accountSelected;
	}

	/**
	 * Sets the account selected.
	 *
	 * @param accountSelected            the accountSelected to set
	 */
	public void setAccountSelected(AccountingAccount accountSelected) {
		this.accountSelected = accountSelected;
	}

	/**
	 * Gets the accounting account to data model.
	 *
	 * @return the accountingAccountToDataModel
	 */
	public GenericDataModel<AccountingAccountTo> getAccountingAccountToDataModel() {
		return accountingAccountToDataModel;
	}

	/**
	 * Sets the accounting account to data model.
	 *
	 * @param accountingAccountToDataModel            the accountingAccountToDataModel to set
	 */
	public void setAccountingAccountToDataModel(
			GenericDataModel<AccountingAccountTo> accountingAccountToDataModel) {
		this.accountingAccountToDataModel = accountingAccountToDataModel;
	}

	/**
	 * Gets the list accounting account to.
	 *
	 * @return the listAccountingAccountTo
	 */
	public List<AccountingAccountTo> getListAccountingAccountTo() {
		return listAccountingAccountTo;
	}

	/**
	 * Sets the list accounting account to.
	 *
	 * @param listAccountingAccountTo            the listAccountingAccountTo to set
	 */
	public void setListAccountingAccountTo(
			List<AccountingAccountTo> listAccountingAccountTo) {
		this.listAccountingAccountTo = listAccountingAccountTo;
	}

	/**
	 * Sets the accounting by helper.
	 *
	 * @param accountingByHelper            the accountingByHelper to set
	 */
	public void setAccountingByHelper(AccountingAccountTo accountingByHelper) {
		this.accountingByHelper = accountingByHelper;
	}

	/**
	 * Gets the list nature type.
	 *
	 * @return the list nature type
	 */
	public List<AccountingParameter> getListNatureType() {
		return listNatureType;
	}

	/**
	 * Sets the list nature type.
	 *
	 * @param listNatureType the new list nature type
	 */
	public void setListNatureType(List<AccountingParameter> listNatureType) {
		this.listNatureType = listNatureType;
	}

	/**
	 * Gets the list partnership entity.
	 *
	 * @return the listPartnershipEntity
	 */
	public List<AccountingParameter> getListPartnershipEntity() {
		return listPartnershipEntity;
	}

	/**
	 * Sets the list partnership entity.
	 *
	 * @param listPartnershipEntity the listPartnershipEntity to set
	 */
	public void setListPartnershipEntity(
			List<AccountingParameter> listPartnershipEntity) {
		this.listPartnershipEntity = listPartnershipEntity;
	}

	/**
	 * Gets the flag account code.
	 *
	 * @return the flag account code
	 */
	public Boolean getFlagAccountCode() {
		return flagAccountCode;
	}

	/**
	 * Sets the flag account code.
	 *
	 * @param flagAccountCode the new flag account code
	 */
	public void setFlagAccountCode(Boolean flagAccountCode) {
		this.flagAccountCode = flagAccountCode;
	}

	/**
	 * Gets the choose account code.
	 *
	 * @return the choose account code
	 */
	public Boolean getChooseAccountCode() {
		return chooseAccountCode;
	}

	/**
	 * Sets the choose account code.
	 *
	 * @param chooseAccountCode the new choose account code
	 */
	public void setChooseAccountCode(Boolean chooseAccountCode) {
		this.chooseAccountCode = chooseAccountCode;
	}

	/**
	 * Gets the disable account code.
	 *
	 * @return the disable account code
	 */
	public Boolean getDisableAccountCode() {
		return disableAccountCode;
	}

	/**
	 * Sets the disable account code.
	 *
	 * @param disableAccountCode the new disable account code
	 */
	public void setDisableAccountCode(Boolean disableAccountCode) {
		this.disableAccountCode = disableAccountCode;
	}

	/**
	 * Gets the disable helper account code.
	 *
	 * @return the disable helper account code
	 */
	public Boolean getDisableHelperAccountCode() {
		return disableHelperAccountCode;
	}

	/**
	 * Sets the disable helper account code.
	 *
	 * @param disableHelperAccountCode the new disable helper account code
	 */
	public void setDisableHelperAccountCode(Boolean disableHelperAccountCode) {
		this.disableHelperAccountCode = disableHelperAccountCode;
	}

	/**
	 * Gets the choose helper account code.
	 *
	 * @return the choose helper account code
	 */
	public Boolean getChooseHelperAccountCode() {
		return chooseHelperAccountCode;
	}

	/**
	 * Sets the choose helper account code.
	 *
	 * @param chooseHelperAccountCode the new choose helper account code
	 */
	public void setChooseHelperAccountCode(Boolean chooseHelperAccountCode) {
		this.chooseHelperAccountCode = chooseHelperAccountCode;
	}

	/**
	 * Gets the choose type filter account code.
	 *
	 * @return the choose type filter account code
	 */
	public Integer getChooseTypeFilterAccountCode() {
		return chooseTypeFilterAccountCode;
	}

	/**
	 * Sets the choose type filter account code.
	 *
	 * @param chooseTypeFilterAccountCode the new choose type filter account code
	 */
	public void setChooseTypeFilterAccountCode(
			Integer chooseTypeFilterAccountCode) {
		this.chooseTypeFilterAccountCode = chooseTypeFilterAccountCode;
	}

	/**
	 * Gets the ind type adjustment.
	 *
	 * @return the ind type adjustment
	 */
	public Integer getIndTypeAdjustment() {
		return indTypeAdjustment;
	}

	/**
	 * Sets the ind type adjustment.
	 *
	 * @param indTypeAdjustment the new ind type adjustment
	 */
	public void setIndTypeAdjustment(Integer indTypeAdjustment) {
		this.indTypeAdjustment = indTypeAdjustment;
	}

	/**
	 * Gets the ind partnership entity.
	 *
	 * @return the indPartnershipEntity
	 */
	public Integer getIndPartnershipEntity() {
		return indPartnershipEntity;
	}

	/**
	 * Sets the ind partnership entity.
	 *
	 * @param indPartnershipEntity the indPartnershipEntity to set
	 */
	public void setIndPartnershipEntity(Integer indPartnershipEntity) {
		this.indPartnershipEntity = indPartnershipEntity;
	}

	/**
	 * Gets the type adjustment select.
	 *
	 * @return the type adjustment select
	 */
	public Boolean getTypeAdjustmentSelect() {
		return typeAdjustmentSelect;
	}

	/**
	 * Sets the type adjustment select.
	 *
	 * @param typeAdjustmentSelect the new type adjustment select
	 */
	public void setTypeAdjustmentSelect(Boolean typeAdjustmentSelect) {
		this.typeAdjustmentSelect = typeAdjustmentSelect;
	}

	/**
	 * Gets the partnership entity select.
	 *
	 * @return the partnershipEntitySelect
	 */
	public Boolean getPartnershipEntitySelect() {
		return partnershipEntitySelect;
	}

	/**
	 * Sets the partnership entity select.
	 *
	 * @param partnershipEntitySelect the partnershipEntitySelect to set
	 */
	public void setPartnershipEntitySelect(Boolean partnershipEntitySelect) {
		this.partnershipEntitySelect = partnershipEntitySelect;
	}

	/**
	 * Gets the account father disable.
	 *
	 * @return the account father disable
	 */
	public Boolean getAccountFatherDisable() {
		return accountFatherDisable;
	}

	/**
	 * Sets the account father disable.
	 *
	 * @param accountFatherDisable the new account father disable
	 */
	public void setAccountFatherDisable(Boolean accountFatherDisable) {
		this.accountFatherDisable = accountFatherDisable;
	}

	/**
	 * Gets the account type of adjustment disable.
	 *
	 * @return the account type of adjustment disable
	 */
	public Boolean getAccountTypeOfAdjustmentDisable() {
		return accountTypeOfAdjustmentDisable;
	}

	/**
	 * Sets the account type of adjustment disable.
	 *
	 * @param accountTypeOfAdjustmentDisable the new account type of adjustment disable
	 */
	public void setAccountTypeOfAdjustmentDisable(
			Boolean accountTypeOfAdjustmentDisable) {
		this.accountTypeOfAdjustmentDisable = accountTypeOfAdjustmentDisable;
	}

	/**
	 * Gets the account entity disable.
	 *
	 * @return the accountEntityDisable
	 */
	public Boolean getAccountEntityDisable() {
		return accountEntityDisable;
	}

	/**
	 * Gets the account ind entity disable.
	 *
	 * @return the accountIndEntityDisable
	 */
	public Boolean getAccountIndEntityDisable() {
		return accountIndEntityDisable;
	}

	/**
	 * Sets the account ind entity disable.
	 *
	 * @param accountIndEntityDisable the accountIndEntityDisable to set
	 */
	public void setAccountIndEntityDisable(Boolean accountIndEntityDisable) {
		this.accountIndEntityDisable = accountIndEntityDisable;
	}

	/**
	 * Sets the account entity disable.
	 *
	 * @param accountEntityDisable the accountEntityDisable to set
	 */
	public void setAccountEntityDisable(Boolean accountEntityDisable) {
		this.accountEntityDisable = accountEntityDisable;
	}
	
	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}
	

	public String getParticipantSelected() {
		return participantSelected;
	}

	public void setParticipantSelected(String participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if (userPrivilege.getUserAcctions().isModify()) {
			privilegeComponent.setBtnModifyView(true);
		}
		if (userPrivilege.getUserAcctions().isDelete()) {
			privilegeComponent.setBtnDeleteView(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);

	}

	/**
	 * **
	 * Inherited characteristics of the parent account.
	 *
	 * @param event the event
	 */
	public void inheritFromAccountFather(ActionEvent event) {
		if (Validations.validateIsNotNull(accountingByHelper)) {
			accountingAccount.setAuxiliaryType(accountingByHelper.getAuxiliaryType());
			accountingAccount.setAccountType(accountingByHelper.getAccountType());
			accountingAccount.setAdjustmentType(accountingByHelper.getAdjustmentType());
			accountingAccount.setInstrumentType(accountingByHelper.getInstrumentType());
			accountingAccount.setCurrency(accountingByHelper.getCurrency());
			
		} else {
			accountingAccount.setAuxiliaryType(null);
			accountingAccount.setAccountType(null);
			accountingAccount.setAdjustmentType(null);
			accountingAccount.setInstrumentType(null);
			accountingAccount.setCurrency(null);
		}
	}
}
