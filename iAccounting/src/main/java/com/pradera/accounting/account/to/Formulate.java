package com.pradera.accounting.account.to;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class Formulate.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class Formulate  implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2715895576797223728L;
	
	/** The code. */
	private Long code;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The type. */
	private String type;
	
	/** The index. */
	private Integer index;
	
	
	
	/**
	 * Instantiates a new formulate.
	 */
	public Formulate() {
	}
	
	/**
	 * Instantiates a new formulate.
	 *
	 * @param code the code
	 * @param name the name
	 */
	public Formulate(Long code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * Instantiates a new formulate.
	 *
	 * @param code the code
	 * @param name the name
	 * @param type the type
	 */
	public Formulate(Long code, String name, String type) {
		this.code = code;
		this.name = name;
		this.type = type;
	}
	
	/**
	 * Instantiates a new formulate.
	 *
	 * @param code the code
	 * @param name the name
	 * @param index the index
	 */
	public Formulate(Long code, String name, Integer index) {
		this.code = code;
		this.name = name;
		this.index = index;
	}
	
	/**
	 * Instantiates a new formulate.
	 *
	 * @param code the code
	 * @param name the name
	 * @param type the type
	 * @param index the index
	 */
	public Formulate(Long code, String name, String type, Integer index) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.index = index;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code to set
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Gets the index.
	 *
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}
	
	/**
	 * Sets the index.
	 *
	 * @param index the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	
}
