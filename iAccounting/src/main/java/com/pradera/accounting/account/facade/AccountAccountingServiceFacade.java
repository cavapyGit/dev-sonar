package com.pradera.accounting.account.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounting.account.service.AccountAccountingServiceBean;
import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.type.AccountingAccountStateType;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingParameterType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountAccountingServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AccountAccountingServiceFacade {  
	
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;	
	
	/** The account accounting service bean. */
	@EJB 
	AccountAccountingServiceBean accountAccountingServiceBean;
	
	/** The parameter accounting service facade bean. */
	@EJB 
	ParameterAccountingServiceFacade parameterAccountingServiceFacadeBean;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;	
	
	@EJB
	private ParticipantServiceBean participantServiceBean;
	/**
	 * Save In The DB the Accounting Account.
	 *
	 * @param accountingAccount the accounting account
	 * @param viewOperationType the view operation type
	 * @param idAccountingAccountByHelper the id accounting account by helper
	 * @return the accounting account
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public AccountingAccount saveBookAccount(AccountingAccount accountingAccount,Integer viewOperationType, Long idAccountingAccountByHelper) throws ServiceException{
		
		// Validate if the Accounting Account is in the BBDD
		AccountingAccountTo accountingAccountToTemp = new AccountingAccountTo()   ;
		
		accountingAccountToTemp.setAccountCode(accountingAccount.getAccountCode());
		accountingAccountToTemp.setAuxiliaryType(accountingAccount.getAuxiliaryType());
		accountingAccountToTemp.setStatus(AccountingAccountStateType.REGISTERED.getCode());
		
		List<AccountingAccount> listAccountingAccounts = this.findAccountingAccountByFilter(accountingAccountToTemp,null);
		
		if(Validations.validateListIsNotNullAndNotEmpty(listAccountingAccounts)){
			accountingAccount = null;
		}
		// Save Accounting Account
		else{
		
			if(Validations.validateIsNotNullAndPositive(idAccountingAccountByHelper)){
				
				AccountingAccount  accountingAccountTemp = findAccountingAccountById(idAccountingAccountByHelper) ;
				accountingAccount.setAccountingAccount(accountingAccountTemp);
			}
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		    
		    if(viewOperationType.equals(ViewOperationsType.REGISTER.getCode())){
		    	accountingAccount.setStatus(AccountingAccountStateType.REGISTERED.getCode());
		    	accountingAccount.setRegisterDate(loggerUser.getAuditTime());
			    accountingAccount.setRegisterUser(loggerUser.getUserName());
			    // For default Yes=1 , No=0
			    accountingAccount.setIndDimension(1);
			    accountingAccount.setIndBalanceControl(1);
			    accountingAccount.setIndAccumulation(1);
			    accountAccountingServiceBean.create(accountingAccount);
		    }
		}
		return accountingAccount;
	}
	
	public List<AccountingAccount> getListAccountingAccount(){
		return accountAccountingServiceBean.getListAccountingAccount();
	}
	public int getCountAccountingAccountForParticipant(String participantMnemonic){
		return accountAccountingServiceBean.getCountAccountingAccountForParticipant(participantMnemonic);
	}
	public String getDescriptionParticipant(String participantMnemonic){
		return accountAccountingServiceBean.getDescriptionParticipant(participantMnemonic);
	}
	/**
	 * Gets the lis participant service bean.
	 *
	 * @param filter the filter
	 * @return the lis participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getLisParticipantServiceBean(Participant filter)
			throws ServiceException {
		return participantServiceBean.getLisParticipantServiceBean(filter);
	}
	
	/**
	 * *
	 * Update Book Account.
	 *
	 * @param accountingAccount the accounting account
	 * @param viewOperationType the view operation type
	 * @param idAccountingAccountByHelper the id accounting account by helper
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateBookAccount(AccountingAccount accountingAccount,Integer viewOperationType, Long idAccountingAccountByHelper) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
	    if(Validations.validateIsNotNullAndPositive(idAccountingAccountByHelper)){
			AccountingAccount  accountingAccountTemp = findAccountingAccountById(idAccountingAccountByHelper) ;
			accountingAccount.setAccountingAccount(accountingAccountTemp);
		}

	    if(viewOperationType.equals(ViewOperationsType.MODIFY.getCode())){
	    	accountingAccount.setStatus(AccountingAccountStateType.REGISTERED.getCode());
	    	accountingAccount.setUpdateDate(loggerUser.getAuditTime());
		    accountingAccount.setUpdateUser(loggerUser.getUserName());
		    accountAccountingServiceBean.update(accountingAccount);
	    }
	    else if(viewOperationType.equals(ViewOperationsType.CANCEL.getCode())){
	    	accountingAccount.setStatus(AccountingAccountStateType.DELETED.getCode());
	    	accountingAccount.setCancelDate(loggerUser.getAuditTime());
		    accountingAccount.setCancelUser(loggerUser.getUserName());
		    accountAccountingServiceBean.update(accountingAccount);
	    }
	    	
	    
	}
	
	/**
	 * Find Accounting Account By Filter.
	 *
	 * @param filter AccountingAccountTo Filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingAccount>  findAccountingAccountByFilter(AccountingAccountTo filter,String participantSelected)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountAccountingServiceBean.findAccountingAccountByFilter(filter,participantSelected);
		
	}
	
	/**
	 * Find Accounting Account.
	 *
	 * @param filter AccountingAccountTo filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingAccountTo>  findAccountingAccountToByFilter(AccountingAccountTo filter)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountAccountingServiceBean.findAccountingAccountToByFilter(filter);
	}
	
	/**
	 * Find Accounting Account By Id.
	 *
	 * @param idAccountingAccountPk id Accounting Account
	 * @return the accounting account
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingAccount findAccountingAccountById(Long idAccountingAccountPk){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountAccountingServiceBean.find(AccountingAccount.class, idAccountingAccountPk);
	}
	

	/**
	 * Update Accounting Account For Change Status.
	 *
	 * @param accountingAccount the accounting account
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateAccountingStatus(AccountingAccount accountingAccount) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		
		if(accountingAccount.getStatus().equals(AccountingAccountStateType.DELETED.getCode())){
			
			accountingAccount.setCancelDate(loggerUser.getAuditTime());
			accountingAccount.setCancelUser(loggerUser.getUserName());
			
		}
		
		accountingAccount.setUpdateDate(loggerUser.getAuditTime());
		accountingAccount.setUpdateUser(loggerUser.getUserName());		
		
		accountAccountingServiceBean.update(accountingAccount);
	}
	
	/**
	 * Find Parent Since Accounting Account.
	 *
	 * @param accountingAccountToLocal accountingAccountToLocal
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingAccount> findAccountParentDescWithDescription(AccountingAccountTo accountingAccountToLocal){
		
		List<AccountingParameter> listAccountingAccountNature  = new ArrayList<AccountingParameter>();
		List<AccountingParameter> listAccountingAccountType    = new ArrayList<AccountingParameter>();
		List<AccountingParameter> listAccountingAuxiliaryDaily = new ArrayList<AccountingParameter>();
		
		List<ParameterTable> 	listAccountingAccountState 	 = new ArrayList<ParameterTable>();
		List<ParameterTable> 	listCurrency 	 = new ArrayList<ParameterTable>();
		List<ParameterTable> 	listInstrumentType 	 = new ArrayList<ParameterTable>();

		
		List<AccountingAccount> listResultAccounting = new ArrayList<AccountingAccount>(); 
		
		// List result AccountingAccount
		listResultAccounting = findAccountParentDesc(accountingAccountToLocal);

		
		// Find and Set Description
		ParameterTableTO 	  parameterTableTO  	= new ParameterTableTO();
		
		AccountingParameterTO accountingParameterTo	= new AccountingParameterTO();
		
		if(Validations.validateListIsNullOrEmpty(listAccountingAccountNature)){
			accountingParameterTo.setParameterType(AccountingParameterType.NATURALEZA_DE_LA_CUENTA.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listAccountingAccountNature=parameterAccountingServiceFacadeBean.findListParameterAccounting(accountingParameterTo);
		} 
		if(Validations.validateListIsNullOrEmpty(listAccountingAccountType)){
			accountingParameterTo.setParameterType(AccountingParameterType.TIPO_DE_CUENTA.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listAccountingAccountType=parameterAccountingServiceFacadeBean.findListParameterAccounting(accountingParameterTo);
		}
		if(Validations.validateListIsNullOrEmpty(listAccountingAuxiliaryDaily)){
			accountingParameterTo.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listAccountingAuxiliaryDaily=parameterAccountingServiceFacadeBean.findListParameterAccounting(accountingParameterTo);
		}
		if (Validations.validateListIsNullOrEmpty(listAccountingAccountState)){
			
			try {
				parameterTableTO.setState(1);
				parameterTableTO.setMasterTableFk(PropertiesConstants.ACCOUNTING_ACCOUNT_STATE);
				listAccountingAccountState= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (Validations.validateListIsNullOrEmpty(listCurrency)){
			
			try {
				parameterTableTO.setState(1);
				parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				listCurrency = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (Validations.validateListIsNullOrEmpty(listInstrumentType)){
			
			try {
				parameterTableTO.setState(1);
				parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
				listInstrumentType= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		//Value and set description		 
	  for (AccountingAccount accountingAccount : listResultAccounting) {
		   
		   for (AccountingParameter natureType : listAccountingAccountNature) {
		    if( Validations.validateIsNotNullAndNotEmpty(accountingAccount.getNatureType()) &&
		    		accountingAccount.getNatureType().equals(natureType.getIdAccountingParameterPk().intValue())){
		    	accountingAccount.setDescriptionNatureType(natureType.getDescription());
		     break;
		    }
		   }
		   
		    for (AccountingParameter accountType : listAccountingAccountType) {
		     if( Validations.validateIsNotNullAndNotEmpty(accountingAccount.getAccountType()) &&
		    		 accountingAccount.getAccountType().equals(accountType.getIdAccountingParameterPk().intValue())){
		    	 accountingAccount.setDescriptionTypeAccount(accountType.getDescription());
		      break;
		     }
		    }
		   
		   for (AccountingParameter auxiliaryDaily : listAccountingAuxiliaryDaily) {
		    if( Validations.validateIsNotNullAndNotEmpty(accountingAccount.getAuxiliaryType()) &&
		    		accountingAccount.getAuxiliaryType().equals(auxiliaryDaily.getIdAccountingParameterPk().intValue())){
		    	accountingAccount.setDescriptionAuxiliaryType(auxiliaryDaily.getDescription());
		     break;
		    }
		   }
		   
		   
		   
		   for (ParameterTable stateAccount : listAccountingAccountState) {
		    if( Validations.validateIsNotNullAndNotEmpty(accountingAccount.getStatus()) &&
		    		accountingAccount.getStatus().equals(stateAccount.getParameterTablePk())){
		    	accountingAccount.setDescriptionStateAccount(stateAccount.getParameterName());
		     break;
		    }
		   }
		   
		   for (ParameterTable currency : listCurrency) {
			    if( Validations.validateIsNotNullAndNotEmpty(accountingAccount.getCurrency()) &&
			    		accountingAccount.getCurrency().equals(currency.getParameterTablePk())){
			    	accountingAccount.setDescriptionCurrency(currency.getParameterName());;
		     break;
		    }
		   }
		   
		   for (ParameterTable instrument : listInstrumentType) {
			    if( Validations.validateIsNotNullAndNotEmpty(accountingAccount.getStatus()) &&
			    		accountingAccount.getInstrumentType().equals(instrument.getParameterTablePk())){
			    	accountingAccount.setDescriptionInstrumentType(instrument.getParameterName());
		     break;
		    }
		   }
		   
		  }	
  
		
		
		return listResultAccounting ;
	}
	
	/**
	 * *
	 * FIND ACCOUNTING ACCOUNT DESC
	 * INPUT: ACCOUNT FATHER
	 * 
	 * OUTPUT: FATHER + CHILD+ GRAND CHILD, ETC.
	 *
	 * @param accountingAccountToLocal the accounting account to local
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingAccount> findAccountParentDesc(AccountingAccountTo accountingAccountToLocal){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingAccount> listAccumulateAccountingAccount = null;
		AccountingAccount accountingAccount = new AccountingAccount();
		List<AccountingAccount> listFinal = new ArrayList<AccountingAccount>();
		
		/**  GETTING LIST WITH ONE ACCOUNTING ACCOUNT , ALWAYS
		 * **/
	    List<AccountingAccount> listResultTemp =  findAllAccountingAccountByFilter(accountingAccountToLocal);	  
	  
	    if(Validations.validateListIsNotNullAndNotEmpty(listResultTemp)){
	    	  accountingAccount = listResultTemp.get(0);
	    	    	
		if(Validations.validateListIsNotNullAndNotEmpty(accountingAccount.getAccountingAccounts()) && 
				Validations.validateIsNotNullAndPositive(accountingAccount.getAccountingAccounts().size())){
			
			List<AccountingAccount> listAccountingAccountsTemp = null; //
			
			for(AccountingAccount  accountingAccountTemp  : accountingAccount.getAccountingAccounts()){
				
				AccountingAccountTo accAccountTo = new AccountingAccountTo();
				accAccountTo.setAccountCode(accountingAccountTemp.getAccountCode());
				
				listAccountingAccountsTemp = findAccountParentDesc(accAccountTo);
				
				
				
				if(Validations.validateListIsNullOrEmpty(listAccumulateAccountingAccount)){
					
					listAccumulateAccountingAccount = new ArrayList<AccountingAccount>();
					
				}
				
				
				for(AccountingAccount account : listAccountingAccountsTemp){
					listAccumulateAccountingAccount.add(account);
				}
				
			}
		
		}
		
		// agregas primero al padre y luego a lo q se acumulo en result ( en el mismo orden)
		
		  listFinal = null;
		  
		if(Validations.validateListIsNotNullAndNotEmpty(listAccumulateAccountingAccount)){
			 listAccumulateAccountingAccount.add(0,accountingAccount );
			 return listAccumulateAccountingAccount;
		}else{
			listFinal =new ArrayList<AccountingAccount>();		
			// agregas primero al padre y luego a lo q se acumulo en result ( en el mismo orden)
			listFinal.add(accountingAccount);
		}
	    }

		return listFinal ;
			
	}
	
	
	/**
	 * *
	 * FIND ACCOUNTING ACCOUNT ASC
	 * INPUT: 	ACCOUNT CHILD
	 * 
	 * OUTPUT: 	CHILD + FATHER + GRANDFATHER, ETC.
	 *
	 * @param accountingAccountToLocal the accounting account to local
	 * @param amount the amount
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingAccount> findAccountParentAsc(AccountingAccountTo accountingAccountToLocal,BigDecimal amount){
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingAccount> listAccumulateAccountingAccount = null;
		
		/**  GETTING ONE ACCOUNTING ACCOUNT 
		 * **/
	    AccountingAccount accountingAccount =  findAccountingAccountById(accountingAccountToLocal.getIdAccountingAccountPk());	    
	    
	     	
	    
		if(Validations.validateIsNotNullAndNotEmpty(accountingAccount) &&
				Validations.validateIsNotNull(accountingAccount.getAccountingAccount())){
			//BigDecimal amountD=new BigDecimal(0);
			/**CALL TO RECURSIVE **/
			AccountingAccountTo accAccountTo = new AccountingAccountTo();
			accAccountTo.setIdAccountingAccountPk(accountingAccount.getAccountingAccount().getIdAccountingAccountPk());
			accAccountTo.setAmount(amount);
			listAccumulateAccountingAccount=findAccountParentAsc(accAccountTo,accAccountTo.getAmount());
		}
			
		accountingAccount.setAmount(amount);	
	    /****/
				
		
		// agregas primero al padre y luego a lo q se acumulo en result ( en el mismo orden)
		
		List<AccountingAccount> listFinal = null;
		if(Validations.validateListIsNotNullAndNotEmpty(listAccumulateAccountingAccount)){
			 listAccumulateAccountingAccount.add(accountingAccount);
			 return listAccumulateAccountingAccount;
		}else{
			listFinal =new ArrayList<AccountingAccount>();		
			// agregas primero al padre y luego a lo q se acumulo en result ( en el mismo orden)
			listFinal.add(accountingAccount);
		}
		
		return listFinal ;
			
	}
	
	/**
	 * Add List Accounting Account .
	 *
	 * @param listGeneral the list general
	 * @param listAccount the list account
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingAccount> addListAccounting(List<AccountingAccount> listGeneral,List<AccountingAccount> listAccount){
		Boolean idIcumulated=Boolean.FALSE;
		for (AccountingAccount accountingAccount : listAccount) {
			idIcumulated=Boolean.FALSE;
		for (AccountingAccount account : listGeneral) {

			if(account.getAccountCode().equals(accountingAccount.getAccountCode())){
				account.setAmount(AccountingUtils.addTwoDecimal(account.getAmount(), accountingAccount.getAmount(), 4));
				idIcumulated=Boolean.TRUE;
				break;
			}
		}
		if(!idIcumulated){
			listGeneral.add(accountingAccount);
		}
		
			
		}
		return listGeneral;
	}
	
	/**
	 * Find List Accounting Account By Filter .
	 *
	 * @param filter AccountingAccountTo
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingAccount> findAllAccountingAccountByFilter(AccountingAccountTo filter){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountAccountingServiceBean.findAllAccountingAccountByFilter(filter);
		
	}
	
	/**
	 * Find Accounting Account Father Since id Accounting Account.
	 *
	 * @param filter the filter
	 * @return the accounting account
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingAccount findFatherAccountingAccountByFilterId(AccountingAccountTo filter){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountAccountingServiceBean.findFatherAccountingAccountByFilterId(filter);
		
	}
	
	/**
	 * Find Accounting Account By Account Code.
	 *
	 * @param filter AccountingAccountTo
	 * @return the accounting account
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingAccount findAccountingAccountByAccountingCode(AccountingAccountTo filter){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountAccountingServiceBean.findAccountingAccountByAccountingCode(filter);
		
	}

	/**
	 * Get Accounting Account By Id.
	 *
	 * @param id idAccountingAccountId
	 * @return the accounting account by pk
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingAccount getAccountingAccountByPk(Long id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountAccountingServiceBean.find(AccountingAccount.class, id);
	}
	 

}
