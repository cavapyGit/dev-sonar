package com.pradera.accounting.schema.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.pradera.accounting.account.facade.AccountAccountingServiceFacade;
import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.formulate.to.ParameterFormulateTo;
import com.pradera.accounting.formulate.to.VariableFormulateTo;
import com.pradera.accounting.formulate.util.Parser;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.matrix.to.AccountingMatrixDetailTo;
import com.pradera.accounting.matrix.to.AccountingMatrixTo;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.schema.facade.SchemaAccountingServiceFacade;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.accounting.source.facade.SourceAccountingServiceFacade;
import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.accounting.source.to.FieldSourceTo;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounting.account.to.AccountingFormulateTo;
import com.pradera.core.component.accounting.account.to.AccountingSourceTo;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingMatrixDetail;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.AccountingSource;
import com.pradera.model.accounting.type.AccountingFieldSourceType;
import com.pradera.model.accounting.type.AccountingMatrixDetailStateType;
import com.pradera.model.accounting.type.AccountingMatrixStateType;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingParameterType;
import com.pradera.model.accounting.type.AccountingSchemaStateType;
import com.pradera.model.accounting.type.AccountingSourceStateType;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingSchemaMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class AccountingSchemaMgmtBean extends GenericBaseBean  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The log. */
	@Inject
	PraderaLogger log;

	/** The parameter accounting service facade. */
	@EJB
	ParameterAccountingServiceFacade parameterAccountingServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade	generalParametersFacade;

	/** The schema accounting service facade. */
	@EJB
	SchemaAccountingServiceFacade schemaAccountingServiceFacade;
	
	/** The account accounting service facade. */
	@EJB
	AccountAccountingServiceFacade accountAccountingServiceFacade;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade accountingMatrixServiceFacade;
	
	/** The source accounting service facade. */
	@EJB
	SourceAccountingServiceFacade sourceAccountingServiceFacade;

//	@Inject
//	PraderaLogger log;
	
	/**  parameters used on page search. */
	private List<ParameterTable> listParameterSchema;
	
	/** The list schema status. */
	private List<ParameterTable> listSchemaStatus;
	
	/** The list parameter branch. */
	private List<AccountingParameter> listParameterBranch;
	
	/** The list parameter auxiliary type. */
	private List<AccountingParameter> listParameterAuxiliaryType;
	
	/** The list nature type. */
	private List<AccountingParameter> listNatureType;
	
	/** The list parameter associate code. */
	private List<AccountingParameter> listParameterAssociateCode;
	
	/** The list parameter associate code. */
	private List<AccountingParameter> listParameterRestartType;
	
	/** The accounting parameter to. */
	private AccountingParameterTO accountingParameterTo = new AccountingParameterTO();
	
	/**  This bean is used for save information when SEARCH begin**. */
	private AccountingSchemaTo accountingSchemaTo;
	
	/** The parameter branch selected. */
	private Integer parameterBranchSelected;
	
	/** The parameter auxiliary type selected. */
	private Integer parameterAuxiliaryTypeSelected;

	/** The parameter associate code selected. */
	private Integer parameterAssociateCodeSelected;
	
	/**  This bean is getting for database , is one bean selection of the list **. */
	private AccountingSchema accountingSchemaSelection;
	
	/** The accounting source selection. */
	private AccountingSource accountingSourceSelection;
	
	/** The accounting schema. */
	private AccountingSchema accountingSchema;
	
	/** The register date. */
	private Date registerDate;
	
	/** The accounting schema data model. */
	private GenericDataModel<AccountingSchema> accountingSchemaDataModel;
	
	/** The accounting source data model. */
	private GenericDataModel<AccountingSource> accountingSourceDataModel;

	/** The consult. */
	private Boolean consult 			= Boolean.FALSE;
	
	/** The accept formulate. */
	private Boolean acceptFormulate 	= Boolean.FALSE;
	
	/** The disable formulate. */
	private Boolean disableFormulate 	= Boolean.FALSE;
	
	/** The associated variable. */
	private Boolean associatedVariable  = Boolean.FALSE;
	
	/** The exist matrix detail accounts. */
	private Boolean existMatrixDetailAccounts = Boolean.FALSE; 
	
	/**  Accounting Account To by Helper *. */
	private AccountingAccountTo accountingAccountTo;

	/**  Accounting Parameter for Formulate *. */
	// private List<AccountingParameterTO> listAccountingParameterTo;
	private List<ParameterFormulateTo> listParameterFormulateTo;
	
	/** The variable formulate to selected. */
	private ParameterFormulateTo[] 	   variableFormulateToSelected;
	
	/** The variable formulate to. */
	private VariableFormulateTo 	   variableFormulateTo;
	
	/** The list variable formulate to. */
	private List<VariableFormulateTo>  listVariableFormulateTo;

	/**  Accounting Account Temp to formulate *. */
	private AccountingAccountTo accountingAccountToTemp;

	/** The accounting matrix to. */
	private AccountingMatrixTo 			   accountingMatrixTo;
	
	/** The list accounting matrix detail to. */
	private List<AccountingMatrixDetailTo> listAccountingMatrixDetailTo;
	
	/** 		Use on account with matrix before saved		*. */
	private List<AccountingMatrixDetailTo> listAccountingMatrixDetailTemp;
	
	/** The list accounting matrix detail for modify. */
	private List<AccountingMatrixDetailTo> listAccountingMatrixDetailForModify ;
	
	/** The list accounting matrix detail for modify original. */
	private List<AccountingMatrixDetailTo> listAccountingMatrixDetailForModifyOriginal ;
	
	/** The list source accounting source. */
	private List<ParameterFormulateTo> listSourceAccountingSource;
	
	/** *. */

	private AccountingMatrixDetailTo accountingMatrixDetailSelection;
	
	/** The accounting matrix detail to. */
	private AccountingMatrixDetailTo accountingMatrixDetailTo;
	
	/** The list accounting sources. */
	private List<AccountingSource>   listAccountingSources;

	
	/**  Data Model FieldSourceTo *. */
	private GenericDataModel<FieldSourceTo> fieldSourceToDataModel;
	
	/** The list accounting account to. */
	private List<AccountingAccountTo>       listAccountingAccountTo = new ArrayList<AccountingAccountTo>();
	
	/** The accounting account data model. */
	private GenericDataModel<AccountingAccountTo>      accountingAccountDataModel;
	
	/** The accounting matrix detail data model. */
	private GenericDataModel<AccountingMatrixDetailTo> accountingMatrixDetailDataModel;

	/** *. */
	private String stringFinalFormula;

	/**  VARIABLES DE FORMULA *. */
	private List<AccountingParameterTO> listVariable = new ArrayList<AccountingParameterTO>();
	
	/** The list operator. */
	private List<AccountingParameterTO> listOperator = new ArrayList<AccountingParameterTO>();
	
	/** The list variable tmp. */
	private List<AccountingParameterTO> listVariableTmp = new ArrayList<AccountingParameterTO>();
	
	/** The list operator tmp. */
	private List<AccountingParameterTO> listOperatorTmp = new ArrayList<AccountingParameterTO>();
	
	/** The formula final. */
	private List<AccountingParameterTO> formulaFinal = new ArrayList<AccountingParameterTO>();

	/** The variable hidden name. */
	private String variableHiddenName = "";
	
	/** The variable hidden code. */
	private String variableHiddenCode = "";
	
	/** The variable hidden index. */
	private String variableHiddenIndex = "";
	
	/** The identify validation. */
	private String identifyValidation = "";
	
	/** The auxiliary type selection to source. */
	private Integer auxiliaryTypeSelectionToSource ;
	
	/** The file source name. */
	private String fileSourceName;
	
	/** The file source name Description. */
	private String fileDescription;
	
	/** The accounting account. */
	private AccountingAccount accountingAccount;  
	
	/** The enable formulate. */
	private Boolean enableFormulate = Boolean.FALSE;
	
	/** 		Use on Modify	*. */
	private List<AccountingMatrixDetailTo> listAccountingMatrixDetailToModify;
	
	/** The accounting matrix detail data for modify. */
	private AccountingMatrixDetailTo 	   accountingMatrixDetailDataForModify ;
	
	/** The go to add account on schema. */
	private Boolean goToAddAccountOnSchema = Boolean.FALSE;
	
	/** The go to modify matrix detail on schema. */
	private Boolean goToModifyMatrixDetailOnSchema = Boolean.FALSE;
	
	/** The ind modify matrix detail. */
	private Integer indModifyMatrixDetail = null;
	
	/** The matrix detail is deleted. */
	private Integer matrixDetailIsDeleted = AccountingMatrixDetailStateType.CANCELED.getCode() ;
 
	
	
	
	/** TreeNode. */
	private TreeNode root;  
	  
    /** The selected node. */
    private TreeNode selectedNode; 
	
	/**
	 * Gets the accounting parameter to.
	 *
	 * @return the accounting parameter to
	 */
	public AccountingParameterTO getAccountingParameterTo() {
		return accountingParameterTo;
	}

	/**
	 * Sets the accounting parameter to.
	 *
	 * @param accountingParameterTo the new accounting parameter to
	 */
	public void setAccountingParameterTo(AccountingParameterTO accountingParameterTo) {
		this.accountingParameterTo = accountingParameterTo;
	}

	/**
	 * Gets the list variable tmp.
	 *
	 * @return the listVariableTmp
	 */
	public List<AccountingParameterTO> getListVariableTmp() {
		return listVariableTmp;
	}

	/**
	 * Sets the list variable tmp.
	 *
	 * @param listVariableTmp            the listVariableTmp to set
	 */
	public void setListVariableTmp(List<AccountingParameterTO> listVariableTmp) {
		this.listVariableTmp = listVariableTmp;
	}

	/**
	 * Gets the list operator tmp.
	 *
	 * @return the listOperatorTmp
	 */
	public List<AccountingParameterTO> getListOperatorTmp() {
		return listOperatorTmp;
	}

	/**
	 * Sets the list operator tmp.
	 *
	 * @param listOperatorTmp            the listOperatorTmp to set
	 */
	public void setListOperatorTmp(List<AccountingParameterTO> listOperatorTmp) {
		this.listOperatorTmp = listOperatorTmp;
	}
 

		
	/**
	 * Gets the identify validation.
	 *
	 * @return the identify validation
	 */
	public String getIdentifyValidation() {
		return identifyValidation;
	}

	/**
	 * Sets the identify validation.
	 *
	 * @param identifyValidation the new identify validation
	 */
	public void setIdentifyValidation(String identifyValidation) {
		this.identifyValidation = identifyValidation;
	}



	/** UPLOAD    *. */
 
	private AccountingSource accountingSource;

	/**
	 * Gets the list accounting account to.
	 *
	 * @return the list accounting account to
	 */
	public List<AccountingAccountTo> getListAccountingAccountTo() {
		return listAccountingAccountTo;
	}

	/**
	 * Sets the list accounting account to.
	 *
	 * @param listAccountingAccountTo the new list accounting account to
	 */
	public void setListAccountingAccountTo(
			List<AccountingAccountTo> listAccountingAccountTo) {
		this.listAccountingAccountTo = listAccountingAccountTo;
	}

	/**
	 * Gets the accounting account data model.
	 *
	 * @return the accounting account data model
	 */
	public GenericDataModel<AccountingAccountTo> getAccountingAccountDataModel() {
		return accountingAccountDataModel;
	}

	/**
	 * Sets the accounting account data model.
	 *
	 * @param accountingAccountDataModel the new accounting account data model
	 */
	public void setAccountingAccountDataModel(
			GenericDataModel<AccountingAccountTo> accountingAccountDataModel) {
		this.accountingAccountDataModel = accountingAccountDataModel;
	}

	/**
	 * Gets the accounting matrix detail selection.
	 *
	 * @return the accountingMatrixDetailSelection
	 */
	public AccountingMatrixDetailTo getAccountingMatrixDetailSelection() {
		return accountingMatrixDetailSelection;
	}

	/**
	 * Sets the accounting matrix detail selection.
	 *
	 * @param accountingMatrixDetailSelection            the accountingMatrixDetailSelection to set
	 */
	public void setAccountingMatrixDetailSelection(
			AccountingMatrixDetailTo accountingMatrixDetailSelection) {
		this.accountingMatrixDetailSelection = accountingMatrixDetailSelection;
	}

	/**
	 * Gets the list parameter schema.
	 *
	 * @return the list parameter schema
	 */
	public List<ParameterTable> getListParameterSchema() {
		return listParameterSchema;
	}

	/**
	 * Sets the list parameter schema.
	 *
	 * @param listParameterSchema the new list parameter schema
	 */
	public void setListParameterSchema(List<ParameterTable> listParameterSchema) {
		this.listParameterSchema = listParameterSchema;
	}

	/**
	 * Gets the disable formulate.
	 *
	 * @return the disable formulate
	 */
	public Boolean getDisableFormulate() {
		return disableFormulate;
	}

	/**
	 * Sets the disable formulate.
	 *
	 * @param disableFormulate the new disable formulate
	 */
	public void setDisableFormulate(Boolean disableFormulate) {
		this.disableFormulate = disableFormulate;
	}

	/**
	 * Gets the list schema status.
	 *
	 * @return the list schema status
	 */
	public List<ParameterTable> getListSchemaStatus() {
		return listSchemaStatus;
	}

	/**
	 * Sets the list schema status.
	 *
	 * @param listSchemaStatus the new list schema status
	 */
	public void setListSchemaStatus(List<ParameterTable> listSchemaStatus) {
		this.listSchemaStatus = listSchemaStatus;
	}

	/**
	 * Gets the list parameter branch.
	 *
	 * @return the list parameter branch
	 */
	public List<AccountingParameter> getListParameterBranch() {
		return listParameterBranch;
	}

	/**
	 * Sets the list parameter branch.
	 *
	 * @param listParameterBranch the new list parameter branch
	 */
	public void setListParameterBranch(
			List<AccountingParameter> listParameterBranch) {
		this.listParameterBranch = listParameterBranch;
	}

	/**
	 * Gets the parameter branch selected.
	 *
	 * @return the parameter branch selected
	 */
	public Integer getParameterBranchSelected() {
		return parameterBranchSelected;
	}

	/**
	 * Sets the parameter branch selected.
	 *
	 * @param parameterBranchSelected the new parameter branch selected
	 */
	public void setParameterBranchSelected(Integer parameterBranchSelected) {
		this.parameterBranchSelected = parameterBranchSelected;
	}

	/**
	 * Gets the consult.
	 *
	 * @return the consult
	 */
	public Boolean getConsult() {
		return consult;
	}

	/**
	 * Sets the consult.
	 *
	 * @param consult the new consult
	 */
	public void setConsult(Boolean consult) {
		this.consult = consult;
	}

	/**
	 * Gets the accept formulate.
	 *
	 * @return the accept formulate
	 */
	public Boolean getAcceptFormulate() {
		return acceptFormulate;
	}

	/**
	 * Sets the accept formulate.
	 *
	 * @param acceptFormulate the new accept formulate
	 */
	public void setAcceptFormulate(Boolean acceptFormulate) {
		this.acceptFormulate = acceptFormulate;
	}

	/**
	 * Gets the accounting schema selection.
	 *
	 * @return the accounting schema selection
	 */
	public AccountingSchema getAccountingSchemaSelection() {
		return accountingSchemaSelection;
	}

	/**
	 * Sets the accounting schema selection.
	 *
	 * @param accountingSchemaSelection the new accounting schema selection
	 */
	public void setAccountingSchemaSelection(
			AccountingSchema accountingSchemaSelection) {
		this.accountingSchemaSelection = accountingSchemaSelection;
	}

	/**
	 * Gets the accounting schema.
	 *
	 * @return the accounting schema
	 */
	public AccountingSchema getAccountingSchema() {
		return accountingSchema;
	}

	/**
	 * Sets the accounting schema.
	 *
	 * @param accountingSchema the new accounting schema
	 */
	public void setAccountingSchema(AccountingSchema accountingSchema) {
		this.accountingSchema = accountingSchema;
	}

	/**
	 * Gets the accounting schema to.
	 *
	 * @return the accounting schema to
	 */
	public AccountingSchemaTo getAccountingSchemaTo() {
		return accountingSchemaTo;
	}

	/**
	 * Sets the accounting schema to.
	 *
	 * @param accountingSchemaTo the new accounting schema to
	 */
	public void setAccountingSchemaTo(AccountingSchemaTo accountingSchemaTo) {
		this.accountingSchemaTo = accountingSchemaTo;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the accounting schema data model.
	 *
	 * @return the accounting schema data model
	 */
	public GenericDataModel<AccountingSchema> getAccountingSchemaDataModel() {
		return accountingSchemaDataModel;
	}

	/**
	 * Sets the accounting schema data model.
	 *
	 * @param accountingSchemaDataModel the new accounting schema data model
	 */
	public void setAccountingSchemaDataModel(
			GenericDataModel<AccountingSchema> accountingSchemaDataModel) {
		this.accountingSchemaDataModel = accountingSchemaDataModel;
	}

	/**
	 * Gets the accounting account to.
	 *
	 * @return the accounting account to
	 */
	public AccountingAccountTo getAccountingAccountTo() {
		return accountingAccountTo;
	}

	/**
	 * Sets the accounting account to.
	 *
	 * @param accountingAccountTo the new accounting account to
	 */
	public void setAccountingAccountTo(AccountingAccountTo accountingAccountTo) {
		this.accountingAccountTo = accountingAccountTo;
	}

	/**
	 * Gets the list parameter auxiliary type.
	 *
	 * @return the list parameter auxiliary type
	 */
	public List<AccountingParameter> getListParameterAuxiliaryType() {
		return listParameterAuxiliaryType;
	}

	/**
	 * Sets the list parameter auxiliary type.
	 *
	 * @param listParameterAuxiliaryType the new list parameter auxiliary type
	 */
	public void setListParameterAuxiliaryType(
			List<AccountingParameter> listParameterAuxiliaryType) {
		this.listParameterAuxiliaryType = listParameterAuxiliaryType;
	}

	/**
	 * Gets the accounting source.
	 *
	 * @return the accounting source
	 */
	public AccountingSource getAccountingSource() {
		return accountingSource;
	}

	/**
	 * Sets the accounting source.
	 *
	 * @param accountingSource the new accounting source
	 */
	public void setAccountingSource(AccountingSource accountingSource) {
		this.accountingSource = accountingSource;
	}

	/**
	 * Gets the list variable.
	 *
	 * @return the listVariable
	 */
	public List<AccountingParameterTO> getListVariable() {
		return listVariable;
	}

	/**
	 * Sets the list variable.
	 *
	 * @param listVariable            the listVariable to set
	 */
	public void setListVariable(List<AccountingParameterTO> listVariable) {
		this.listVariable = listVariable;
	}

	/**
	 * Gets the formula final.
	 *
	 * @return the formulaFinal
	 */
	public List<AccountingParameterTO> getFormulaFinal() {
		return formulaFinal;
	}

	/**
	 * Sets the formula final.
	 *
	 * @param formulaFinal            the formulaFinal to set
	 */
	public void setFormulaFinal(List<AccountingParameterTO> formulaFinal) {
		this.formulaFinal = formulaFinal;
	}

	/**
	 * Gets the list operator.
	 *
	 * @return the listOperator
	 */
	public List<AccountingParameterTO> getListOperator() {
		return listOperator;
	}

	/**
	 * Sets the list operator.
	 *
	 * @param listOperator            the listOperator to set
	 */
	public void setListOperator(List<AccountingParameterTO> listOperator) {
		this.listOperator = listOperator;
	}

	/**
	 * Gets the list variable formulate to.
	 *
	 * @return the listVariableFormulateTo
	 */
	public List<VariableFormulateTo> getListVariableFormulateTo() {
		return listVariableFormulateTo;
	}

	/**
	 * Sets the list variable formulate to.
	 *
	 * @param listVariableFormulateTo            the listVariableFormulateTo to set
	 */
	public void setListVariableFormulateTo(
			List<VariableFormulateTo> listVariableFormulateTo) {
		this.listVariableFormulateTo = listVariableFormulateTo;
	}

	/**
	 * Gets the variable formulate to.
	 *
	 * @return the variableFormulateTo
	 */
	public VariableFormulateTo getVariableFormulateTo() {
		return variableFormulateTo;
	}

	/**
	 * Sets the variable formulate to.
	 *
	 * @param variableFormulateTo            the variableFormulateTo to set
	 */
	public void setVariableFormulateTo(VariableFormulateTo variableFormulateTo) {
		this.variableFormulateTo = variableFormulateTo;
	}

	

	/**
	 * Gets the variable formulate to selected.
	 *
	 * @return the variableFormulateToSelected
	 */
	public ParameterFormulateTo[] getVariableFormulateToSelected() {
		return variableFormulateToSelected;
	}

	/**
	 * Sets the variable formulate to selected.
	 *
	 * @param variableFormulateToSelected the variableFormulateToSelected to set
	 */
	public void setVariableFormulateToSelected(
			ParameterFormulateTo[] variableFormulateToSelected) {
		this.variableFormulateToSelected = variableFormulateToSelected;
	}

	/**
	 * Gets the list parameter formulate to.
	 *
	 * @return the listParameterFormulateTo
	 */
	public List<ParameterFormulateTo> getListParameterFormulateTo() {
		return listParameterFormulateTo;
	}

	/**
	 * Sets the list parameter formulate to.
	 *
	 * @param listParameterFormulateTo            the listParameterFormulateTo to set
	 */
	public void setListParameterFormulateTo(
			List<ParameterFormulateTo> listParameterFormulateTo) {
		this.listParameterFormulateTo = listParameterFormulateTo;
	}

	/**
	 * Gets the field source to data model.
	 *
	 * @return the fieldSourceToDataModel
	 */
	public GenericDataModel<FieldSourceTo> getFieldSourceToDataModel() {
		return fieldSourceToDataModel;
	}

	/**
	 * Sets the field source to data model.
	 *
	 * @param fieldSourceToDataModel            the fieldSourceToDataModel to set
	 */
	public void setFieldSourceToDataModel(
			GenericDataModel<FieldSourceTo> fieldSourceToDataModel) {
		this.fieldSourceToDataModel = fieldSourceToDataModel;
	}

	/**
	 * Gets the associated variable.
	 *
	 * @return the associated variable
	 */
	public Boolean getAssociatedVariable() {
		return associatedVariable;
	}

	/**
	 * Sets the associated variable.
	 *
	 * @param associatedVariable the new associated variable
	 */
	public void setAssociatedVariable(Boolean associatedVariable) {
		this.associatedVariable = associatedVariable;
	}


	/**
	 * Gets the file source name.
	 *
	 * @return the fileSourceName
	 */
	public String getFileSourceName() {
		return fileSourceName;
	}

	/**
	 * Sets the file source name.
	 *
	 * @param fileSourceName the fileSourceName to set
	 */
	public void setFileSourceName(String fileSourceName) {
		this.fileSourceName = fileSourceName;
	}

	
	/**
	 * @return the fileDescription
	 */
	public String getFileDescription() {
		return fileDescription;
	}

	/**
	 * @param fileDescription the fileDescription to set
	 */
	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}

	/**
	 * Gets the accounting matrix detail to.
	 *
	 * @return the accountingMatrixDetailTo
	 */
	public AccountingMatrixDetailTo getAccountingMatrixDetailTo() {
		return accountingMatrixDetailTo;
	}

	/**
	 * Sets the accounting matrix detail to.
	 *
	 * @param accountingMatrixDetailTo            the accountingMatrixDetailTo to set
	 */
	public void setAccountingMatrixDetailTo(
			AccountingMatrixDetailTo accountingMatrixDetailTo) {
		this.accountingMatrixDetailTo = accountingMatrixDetailTo;
	}

	/**
	 * Gets the accounting matrix to.
	 *
	 * @return the accountingMatrixTo
	 */
	public AccountingMatrixTo getAccountingMatrixTo() {
		return accountingMatrixTo;
	}

	/**
	 * Sets the accounting matrix to.
	 *
	 * @param accountingMatrixTo            the accountingMatrixTo to set
	 */
	public void setAccountingMatrixTo(AccountingMatrixTo accountingMatrixTo) {
		this.accountingMatrixTo = accountingMatrixTo;
	}

	/**
	 * Gets the list accounting matrix detail to.
	 *
	 * @return the listAccountingMatrixDetailTo
	 */
	public List<AccountingMatrixDetailTo> getListAccountingMatrixDetailTo() {
		return listAccountingMatrixDetailTo;
	}

	/**
	 * Sets the list accounting matrix detail to.
	 *
	 * @param listAccountingMatrixDetailTo            the listAccountingMatrixDetailTo to set
	 */
	public void setListAccountingMatrixDetailTo(
			List<AccountingMatrixDetailTo> listAccountingMatrixDetailTo) {
		this.listAccountingMatrixDetailTo = listAccountingMatrixDetailTo;
	}

	/**
	 * Gets the list source accounting source.
	 *
	 * @return the listSourceAccountingSource
	 */
	public List<ParameterFormulateTo> getListSourceAccountingSource() {
		return listSourceAccountingSource;
	}

	/**
	 * Sets the list source accounting source.
	 *
	 * @param listSourceAccountingSource            the listSourceAccountingSource to set
	 */
	public void setListSourceAccountingSource(
			List<ParameterFormulateTo> listSourceAccountingSource) {
		this.listSourceAccountingSource = listSourceAccountingSource;
	}

 
	/**
	 * Gets the accounting source selection.
	 *
	 * @return the accountingSourceSelection
	 */
	public AccountingSource getAccountingSourceSelection() {
		return accountingSourceSelection;
	}

	/**
	 * Sets the accounting source selection.
	 *
	 * @param accountingSourceSelection            the accountingSourceSelection to set
	 */
	public void setAccountingSourceSelection(
			AccountingSource accountingSourceSelection) {
		this.accountingSourceSelection = accountingSourceSelection;
	}

	/**
	 * Gets the accounting source data model.
	 *
	 * @return the accountingSourceDataModel
	 */
	public GenericDataModel<AccountingSource> getAccountingSourceDataModel() {
		return accountingSourceDataModel;
	}

	/**
	 * Sets the accounting source data model.
	 *
	 * @param accountingSourceDataModel            the accountingSourceDataModel to set
	 */
	public void setAccountingSourceDataModel(
			GenericDataModel<AccountingSource> accountingSourceDataModel) {
		this.accountingSourceDataModel = accountingSourceDataModel;
	}

	/**
	 * Gets the parameter auxiliary type selected.
	 *
	 * @return the parameterAuxiliaryTypeSelected
	 */
	public Integer getParameterAuxiliaryTypeSelected() {
		return parameterAuxiliaryTypeSelected;
	}

	/**
	 * Sets the parameter auxiliary type selected.
	 *
	 * @param parameterAuxiliaryTypeSelected            the parameterAuxiliaryTypeSelected to set
	 */
	public void setParameterAuxiliaryTypeSelected(
			Integer parameterAuxiliaryTypeSelected) {
		this.parameterAuxiliaryTypeSelected = parameterAuxiliaryTypeSelected;
	}
	
	

	/**
	 * Gets the parameter associate code selected.
	 *
	 * @return the parameterAssociateCodeSelected
	 */
	public Integer getParameterAssociateCodeSelected() {
		return parameterAssociateCodeSelected;
	}

	/**
	 * Sets the parameter associate code selected.
	 *
	 * @param parameterAssociateCodeSelected the parameterAssociateCodeSelected to set
	 */
	public void setParameterAssociateCodeSelected(
			Integer parameterAssociateCodeSelected) {
		this.parameterAssociateCodeSelected = parameterAssociateCodeSelected;
	}

	/**
	 * Gets the accounting matrix detail data model.
	 *
	 * @return the accountingMatrixDetailDataModel
	 */
	public GenericDataModel<AccountingMatrixDetailTo> getAccountingMatrixDetailDataModel() {
		return accountingMatrixDetailDataModel;
	}

	/**
	 * Sets the accounting matrix detail data model.
	 *
	 * @param accountingMatrixDetailDataModel            the accountingMatrixDetailDataModel to set
	 */
	public void setAccountingMatrixDetailDataModel(
			GenericDataModel<AccountingMatrixDetailTo> accountingMatrixDetailDataModel) {
		this.accountingMatrixDetailDataModel = accountingMatrixDetailDataModel;
	}

	/**
	 * Gets the variable hidden name.
	 *
	 * @return the variableHiddenName
	 */
	public String getVariableHiddenName() {
		return variableHiddenName;
	}

	/**
	 * Sets the variable hidden name.
	 *
	 * @param variableHiddenName            the variableHiddenName to set
	 */
	public void setVariableHiddenName(String variableHiddenName) {
		this.variableHiddenName = variableHiddenName;
	}

	/**
	 * Gets the variable hidden code.
	 *
	 * @return the variableHiddenCode
	 */
	public String getVariableHiddenCode() {
		return variableHiddenCode;
	}

	/**
	 * Sets the variable hidden code.
	 *
	 * @param variableHiddenCode            the variableHiddenCode to set
	 */
	public void setVariableHiddenCode(String variableHiddenCode) {
		this.variableHiddenCode = variableHiddenCode;
	}

	/**
	 * Gets the variable hidden index.
	 *
	 * @return the variableHiddenIndex
	 */
	public String getVariableHiddenIndex() {
		return variableHiddenIndex;
	}

	/**
	 * Sets the variable hidden index.
	 *
	 * @param variableHiddenIndex            the variableHiddenIndex to set
	 */
	public void setVariableHiddenIndex(String variableHiddenIndex) {
		this.variableHiddenIndex = variableHiddenIndex;
	}

	/**
	 * Gets the accounting account to temp.
	 *
	 * @return the accountingAccountToTemp
	 */
	public AccountingAccountTo getAccountingAccountToTemp() {
		return accountingAccountToTemp;
	}

	/**
	 * Sets the accounting account to temp.
	 *
	 * @param accountingAccountToTemp            the accountingAccountToTemp to set
	 */
	public void setAccountingAccountToTemp(
			AccountingAccountTo accountingAccountToTemp) {
		this.accountingAccountToTemp = accountingAccountToTemp;
	}
	

	/**
	 * Gets the list nature type.
	 *
	 * @return the list nature type
	 */
	public List<AccountingParameter> getListNatureType() {
		return listNatureType;
	}

	/**
	 * Sets the list nature type.
	 *
	 * @param listNatureType the new list nature type
	 */
	public void setListNatureType(List<AccountingParameter> listNatureType) {
		this.listNatureType = listNatureType;
	}
	
	


	/**
	 * @return the listParameterRestartType
	 */
	public List<AccountingParameter> getListParameterRestartType() {
		return listParameterRestartType;
	}

	/**
	 * @param listParameterRestartType the listParameterRestartType to set
	 */
	public void setListParameterRestartType(List<AccountingParameter> listParameterRestartType) {
		this.listParameterRestartType = listParameterRestartType;
	}

	/**
	 * Gets the list parameter associate code.
	 *
	 * @return the listParameterAssociateCode
	 */
	public List<AccountingParameter> getListParameterAssociateCode() {
		return listParameterAssociateCode;
	}

	/**
	 * Sets the list parameter associate code.
	 *
	 * @param listParameterAssociateCode the listParameterAssociateCode to set
	 */
	public void setListParameterAssociateCode(
			List<AccountingParameter> listParameterAssociateCode) {
		this.listParameterAssociateCode = listParameterAssociateCode;
	}

	/**
	 * Gets the list accounting sources.
	 *
	 * @return the list accounting sources
	 */
	public List<AccountingSource> getListAccountingSources() {
		return listAccountingSources;
	}

	/**
	 * Sets the list accounting sources.
	 *
	 * @param listAccountingSources the new list accounting sources
	 */
	public void setListAccountingSources(
			List<AccountingSource> listAccountingSources) {
		this.listAccountingSources = listAccountingSources;
	}

	/**
	 * Gets the auxiliary type selection to source.
	 *
	 * @return the auxiliary type selection to source
	 */
	public Integer getAuxiliaryTypeSelectionToSource() {
		return auxiliaryTypeSelectionToSource;
	}

	/**
	 * Sets the auxiliary type selection to source.
	 *
	 * @param auxiliaryTypeSelectionToSource the new auxiliary type selection to source
	 */
	public void setAuxiliaryTypeSelectionToSource(
			Integer auxiliaryTypeSelectionToSource) {
		this.auxiliaryTypeSelectionToSource = auxiliaryTypeSelectionToSource;
	}
	

	/**
	 * Gets the accounting account.
	 *
	 * @return the accounting account
	 */
	public AccountingAccount getAccountingAccount() {
		return accountingAccount;
	}

	/**
	 * Sets the accounting account.
	 *
	 * @param accountingAccount the new accounting account
	 */
	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}
	
	/**
	 * Gets the exist matrix detail accounts.
	 *
	 * @return the exist matrix detail accounts
	 */
	public Boolean getExistMatrixDetailAccounts() {
		return existMatrixDetailAccounts;
	}

	/**
	 * Sets the exist matrix detail accounts.
	 *
	 * @param existMatrixDetailAccounts the new exist matrix detail accounts
	 */
	public void setExistMatrixDetailAccounts(Boolean existMatrixDetailAccounts) {
		this.existMatrixDetailAccounts = existMatrixDetailAccounts;
	}

	/**
	 * Gets the string final formula.
	 *
	 * @return the string final formula
	 */
	public String getStringFinalFormula() {
		return stringFinalFormula;
	}

	/**
	 * Sets the string final formula.
	 *
	 * @param stringFinalFormula the new string final formula
	 */
	public void setStringFinalFormula(String stringFinalFormula) {
		this.stringFinalFormula = stringFinalFormula;
	}

	/**
	 * Gets the enable formulate.
	 *
	 * @return the enable formulate
	 */
	public Boolean getEnableFormulate() {
		return enableFormulate;
	}

	/**
	 * Sets the enable formulate.
	 *
	 * @param enableFormulate the new enable formulate
	 */
	public void setEnableFormulate(Boolean enableFormulate) {
		this.enableFormulate = enableFormulate;
	}

	/**
	 * Gets the matrix detail is deleted.
	 *
	 * @return the matrix detail is deleted
	 */
	public Integer getMatrixDetailIsDeleted() {
		return matrixDetailIsDeleted;
	}

	/**
	 * Sets the matrix detail is deleted.
	 *
	 * @param matrixDetailIsDeleted the new matrix detail is deleted
	 */
	public void setMatrixDetailIsDeleted(Integer matrixDetailIsDeleted) {
		this.matrixDetailIsDeleted = matrixDetailIsDeleted;
	}

	/**
	 * Gets the root.
	 *
	 * @return the root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Sets the root.
	 *
	 * @param root the root to set
	 */
	public void setRoot(TreeNode root) {
		this.root = root;
	}

	/**
	 * Gets the selected node.
	 *
	 * @return the selectedNode
	 */
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	/**
	 * Sets the selected node.
	 *
	 * @param selectedNode the selectedNode to set
	 */
	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	} 
	
	

	/**
	 * PostConstruct.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {

		if (!FacesContext.getCurrentInstance().isPostback()) {

			this.registerDate = CommonsUtilities.currentDate();
			this.accountingSchemaTo = new AccountingSchemaTo();
			this.accountingAccountTo = new AccountingAccountTo();
			this.accountingAccountDataModel = new GenericDataModel<AccountingAccountTo>();
			this.accountingSource = new AccountingSource();
			this.accountingAccount= new AccountingAccount();
			this.consult = Boolean.FALSE;
			listAccountingMatrixDetailTo = new ArrayList<AccountingMatrixDetailTo>();
			listAccountingMatrixDetailTemp= new ArrayList<AccountingMatrixDetailTo>();
			accountingMatrixTo = new AccountingMatrixTo();
			loadListImputToFormula();	
			// Call to load cmbData
			loadCmbDataSearch();
		}

	}
	
	/**
	 * *
	 * Amarre de Tipo Diario Auxiliar con Sucursal.
	 */
	@LoggerAuditWeb
	public void dynamicAuxiliarytype(){

		/** Use on Account Type	**/
		AccountingParameterTO accountingParameterTo=new AccountingParameterTO();
 	
		parameterBranchSelected=null;
		/** When Auxiliary Type is Admin **/
		if(PropertiesConstants.ADM.equals(this.parameterAuxiliaryTypeSelected)){
		
			accountingParameterTo.setParameterType(AccountingParameterType.SUCURSALES.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listParameterBranch=parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
			
			List<AccountingParameter> listAccountTypeTemp = AccountingUtils.cloneList(listParameterBranch);
			Collection<AccountingParameter> collectionRemove = new ArrayList<AccountingParameter>();

			for(AccountingParameter accountingParameter : listAccountTypeTemp){
				if(accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_VALORIZADO.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_EN_CANTIDADES_DE_VALORES.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_FONDOS_MARGEN.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_FONDOS_COMPENSACION.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_FONDOS_COORPERATIVO.intValue())){
					collectionRemove.add(accountingParameter);
				}
			}
			listAccountTypeTemp.removeAll(collectionRemove);
			listParameterBranch=AccountingUtils.cloneList(listAccountTypeTemp);
		} 
			/** When Auxiliary Type is Valor **/
		else if(PropertiesConstants.VALOR.equals(this.parameterAuxiliaryTypeSelected)){
			accountingParameterTo.setParameterType(PropertiesConstants.SUCURSALES);
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listParameterBranch=parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
			
			List<AccountingParameter> listAccountTypeTemp = AccountingUtils.cloneList(listParameterBranch);
			Collection<AccountingParameter> collectionRemove = new ArrayList<AccountingParameter>();

			for(AccountingParameter accountingParameter : listAccountTypeTemp){
				if(accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_SERVICIOS.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_FONDOS_MARGEN.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_FONDOS_COMPENSACION.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_FONDOS_COORPERATIVO.intValue())){
					collectionRemove.add(accountingParameter);
				}
			}
			listAccountTypeTemp.removeAll(collectionRemove);
			listParameterBranch=AccountingUtils.cloneList(listAccountTypeTemp);
		}
		/** When Auxiliary Type is Fondos **/
		else if(PropertiesConstants.FONDO.equals(this.parameterAuxiliaryTypeSelected)){
			accountingParameterTo.setParameterType(PropertiesConstants.SUCURSALES);
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listParameterBranch=parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
			
			List<AccountingParameter> listAccountTypeTemp = AccountingUtils.cloneList(listParameterBranch);
			Collection<AccountingParameter> collectionRemove = new ArrayList<AccountingParameter>();

			for(AccountingParameter accountingParameter : listAccountTypeTemp){
				if(accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_VALORIZADO.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_EN_CANTIDADES_DE_VALORES.intValue()) ||
					    accountingParameter.getIdAccountingParameterPk().equals(PropertiesConstants.CONTABILIDAD_DE_SERVICIOS.intValue())){
					collectionRemove.add(accountingParameter);
				}
			}
			listAccountTypeTemp.removeAll(collectionRemove);
			listParameterBranch=AccountingUtils.cloneList(listAccountTypeTemp);
		}
		else{
			accountingParameterTo.setParameterType(PropertiesConstants.SUCURSALES);
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listParameterBranch=parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);

		}
		
	}
	
	/**
	 * Formula Drag And Drop
	 * *.
	 */

	/**removeFromFormulate***/
	public void removeFromFormulate() {
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();

		String property = params.get("formMemory:variableHiddenName");
		Long idCode = Long.parseLong(params.get("formMemory:variableHiddenCode"));
		Integer indexFormulate = Integer.parseInt(params.get("formMemory:variableHiddenIndex"));
		for (Iterator iterator = formulaFinal.iterator(); iterator.hasNext();) {
			AccountingParameterTO formulate = (AccountingParameterTO) iterator.next();
			if (formulate.getIndex().equals(indexFormulate)) {
				iterator.remove();
				break;
			}
		}
	}

	/**
	 * Adds the from formulate.
	 */
	public void addFromFormulate() {

		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();

		String property = params.get("formMemory:variableHiddenName");
		Integer idCode = Integer.parseInt(params.get("formMemory:variableHiddenCode"));
		sortIndexes();
		formulaFinal.add(new AccountingParameterTO(idCode, property ,formulaFinal.size()));
		/** Indicator validated acceptFormulate false **/
		setAcceptFormulate(Boolean.FALSE);

	}

	/**
	 * Sort indexes.
	 */
	public void sortIndexes() {
		int i = 0;
		for (AccountingParameterTO formulas : formulaFinal) {
			formulas.setIndex(i);
			i++;
		}
	}
	
	
	/**
	 * Load list imput to formula.
	 */
	public void loadListImputToFormula() {
		AccountingParameterTO accountingParameterTo = new AccountingParameterTO();

		if (Validations.validateListIsNullOrEmpty(listVariable)) {
			// log.debug("lista de variables:");
			accountingParameterTo.setParameterType(AccountingParameterType.VARIABLE_DE_FORMULA.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listVariable = parameterAccountingServiceFacade.searchParameterAccountingTO(accountingParameterTo);
			for (AccountingParameterTO list : listVariable) {
				listVariableTmp.add(list);
			}
		}
		if (Validations.validateListIsNullOrEmpty(listOperator)) {
			// log.debug("lista de operadores:");
			accountingParameterTo.setParameterType(AccountingParameterType.OPERADOR_DE_FORMULA.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listOperator = parameterAccountingServiceFacade.searchParameterAccountingTO(accountingParameterTo);
			for (AccountingParameterTO list : listOperator) {
				listOperatorTmp.add(list);
			}
		}
		
	}

	/**
	 * On column drop.
	 */
	public void onColumnDrop() {
		
	}



	/**
	 * Register new schema.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void registerNewSchema() throws ServiceException {
		goToAddAccountOnSchema = Boolean.TRUE;
		goToModifyMatrixDetailOnSchema = Boolean.FALSE;
		accountingMatrixDetailDataModel = null;
		listAccountingMatrixDetailTo=new ArrayList<AccountingMatrixDetailTo>();
		loadCmbDataSearch();
	}
	
	/**
	 * *.
	 *
	 * @throws ServiceException the service exception
	 */
	/** Load cmbData **/
	@LoggerAuditWeb
	public void loadCmbDataSearch() throws ServiceException {
		
		

		ParameterTableTO parameterTableTO = new ParameterTableTO();

		this.consult = Boolean.FALSE;

		if (Validations.validateListIsNullOrEmpty(listSchemaStatus)) {

			parameterTableTO.setState(1);
			parameterTableTO
					.setMasterTableFk(PropertiesConstants.PARAMETER_SCHEMA_STATUS);
			listSchemaStatus = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateIsNotNullAndNotEmpty(parameterBranchSelected)) {

			parameterBranchSelected = -1;
		}
		// load dailyAuxiliaryType
		if (Validations.validateListIsNullOrEmpty(listParameterAuxiliaryType)) {

			this.accountingParameterTo.setParameterType(PropertiesConstants.DAILY_AUXILIARY);
			this.accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			this.listParameterAuxiliaryType = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);

		}
		if (Validations.validateListIsNullOrEmpty(listParameterAssociateCode)) {

			this.accountingParameterTo.setParameterType(PropertiesConstants.ASSOCIATE_CODE);
			this.accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			this.listParameterAssociateCode = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);

		}
		if (Validations.validateListIsNullOrEmpty(listParameterRestartType)) {

			this.accountingParameterTo.setParameterType(PropertiesConstants.RESTART_TYPE);
			this.accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			this.listParameterRestartType = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);

		}
		
		this.accountingSchemaTo = new AccountingSchemaTo();

		if (Validations.validateListIsNullOrEmpty(listParameterBranch)) {

			accountingParameterTo.setParameterType(PropertiesConstants.PARAMETER_ACCOUNTING_BRANCH);
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listParameterBranch = parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
		}
		
		if(Validations.validateListIsNullOrEmpty(listNatureType)){
			accountingParameterTo.setParameterType(AccountingParameterType.NATURALEZA_DE_LA_CUENTA.getCode());
			accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listNatureType=parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
		}
		
		parameterBranchSelected = -1;
		parameterAuxiliaryTypeSelected = -1;
		parameterAssociateCodeSelected=-1;
		
		this.accountingSchemaTo.setRegisterUser(this.userInfo.getUserAccountSession().getUserName());

	}

	/**
	 * ALL BEFORE.
	 */


	/**
	 * beforeRegisterSchema
	 */
	public void beforeRegisterSchema() {

		if (Validations.validateIsNotNullAndPositive(parameterBranchSelected)
				|| Validations
						.validateIsNotNullAndNotEmpty(this.accountingSchemaTo
								.getSchemaCode())
				|| Validations
						.validateIsNotNullAndNotEmpty(this.accountingSchemaTo
								.getDescription())) {

			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(
							PropertiesConstants.CONFIRM_SAVE_SCHEMA, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSave.show();");

		}

	}


	/**
	 * Validate before Modify Accounting Schema.
	 *
	 * @return the string
	 */
	public String beforeModifyAccountingSchema() {

		if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {
			Object code = accountingSchemaSelection.getSchemaCode();
			if (accountingSchemaSelection.getStatus().equals(
					AccountingSchemaStateType.CANCELED.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_SCHEMA_CANCELED,
								code));
				JSFUtilities
						.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return "";
			}
			if (accountingSchemaSelection.getStatus().equals(
					AccountingSchemaStateType.LOCK.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_SCHEMA_LOCK, code));
				JSFUtilities
						.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return "";
			}

			/**CLEAN to view SCHEMAMODIFY**/
			//cleanAddAccountingMatrix();
			/**Redirect to view SCHEMAMODIFY***/
			return "schemaModify";

		}

		else {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

			return "";
		}

	}

	/**
	 * Validate before goTo Add Account on Schema.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String gotoAddAccount() {  
		
		goToAddAccountOnSchema = Boolean.TRUE;
		goToModifyMatrixDetailOnSchema = Boolean.FALSE;
		accountingMatrixDetailDataModel = null;
		accountingAccountTo = new AccountingAccountTo();
		
		if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {

			this.accountingAccountDataModel = new GenericDataModel<AccountingAccountTo>();
			Object code = accountingSchemaSelection.getSchemaCode();

			if (accountingSchemaSelection.getStatus().equals(AccountingSchemaStateType.CANCELED.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_SCHEMA_CANCELED,
								code));
				JSFUtilities
						.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
				return "";
			}
			if (accountingSchemaSelection.getStatus().equals(
					AccountingSchemaStateType.LOCK.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SCHEMA_LOCK, code));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
				return "";
			}

				
			/**		Load accountingSchemaSelection  	**/
			AccountingSchemaTo accountingSchemaToTemp = new AccountingSchemaTo();
			accountingSchemaToTemp.setSchemaCode(this.accountingSchemaSelection.getSchemaCode());
			accountingSchemaToTemp.setDescriptionParameterStatus(this.accountingSchemaSelection.getDescriptionParameterStatus());
			accountingSchemaToTemp.setDescription(this.accountingSchemaSelection.getDescription());
			accountingSchemaToTemp.setAuxiliaryType(this.accountingSchemaSelection.getAuxiliaryType());
			accountingSchemaToTemp.setBranchType(this.accountingSchemaSelection.getBranchType());
			accountingSchemaToTemp.setRegisterDate(this.accountingSchemaSelection.getRegisterDate());
			accountingSchemaToTemp.setRegisterUser(this.accountingSchemaSelection.getRegisterUser());
			accountingSchemaToTemp.setModifyUser(this.accountingSchemaSelection.getUpdateUser()); 
			accountingSchemaToTemp.setModifyDate(this.accountingSchemaSelection.getUpdateDate());			
			this.accountingMatrixTo.setAccountingSchemaTo(accountingSchemaToTemp);
			
			
			//Search all Schema's matrix
			listAccountingMatrixDetailTemp = schemaAccountingServiceFacade.findAccountingMatrixDetailBySchemaFilter(this.accountingSchemaSelection);	
			
			//Add ListAccountingMatrixDetailTemp on DataModel
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTemp)){
				accountingMatrixDetailDataModel = new GenericDataModel<AccountingMatrixDetailTo>(listAccountingMatrixDetailTemp);
				this.existMatrixDetailAccounts = Boolean.TRUE;
			}
			
			return "schemaRegister";
			
		}
		else {
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
			
			return "";
		}

	}
	
	
	/**
	 * Before lock accounting schema.
	 *
	 * @param event the event
	 */
	public void beforeLockAccountingSchema(ActionEvent event) {

		if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {
			Object code = accountingSchemaSelection.getSchemaCode();
			if (accountingSchemaSelection.getStatus().equals(
					AccountingSchemaStateType.CANCELED.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_SCHEMA_CANCELED,
								code));
				JSFUtilities
						.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			}
			if (accountingSchemaSelection.getStatus().equals(
					AccountingSchemaStateType.LOCK.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_SCHEMA_LOCK, code));
				JSFUtilities
						.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			}

			// When is registered

			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(
							PropertiesConstants.CONFIRM_SCHEMA_LOCKED, code));
			JSFUtilities.executeJavascriptFunction("cnfwShemaLocked.show();");

		}

		else {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

		}

	}
	
	
	/**
	 * Before un lock accounting schema.
	 *
	 * @param event the event
	 */
	public void beforeUnLockAccountingSchema(ActionEvent event) {

		if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {
			Object code = accountingSchemaSelection.getSchemaCode();
			if (accountingSchemaSelection.getStatus().equals(
					AccountingSchemaStateType.CANCELED.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SCHEMA_CANCELED,code));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			}
			if (accountingSchemaSelection.getStatus().equals(
					AccountingSchemaStateType.REGISTERED.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SCHEMA_REGISTERED,code));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			}

			// When is lock

			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SCHEMA_UNLOCKED, code));
			JSFUtilities.executeJavascriptFunction("cnfwShemaUnLocked.show();");

		}

		else {
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

		}

	}

	/**
	 * Before cancel accounting schema.
	 *
	 * @param e the e
	 */
	public void beforeCancelAccountingSchema(ActionEvent e) {

		if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {
			Object code = accountingSchemaSelection.getSchemaCode();
			if (accountingSchemaSelection.getStatus().equals(
					AccountingSchemaStateType.CANCELED.getCode())) {
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SCHEMA_CANCELED,code));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return;
			}

			// When is register and lock

			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SCHEMA_CANCELED, code));
			JSFUtilities.executeJavascriptFunction("cnfwShemaCanceled.show();");

		}

		else {
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

		}

	}

	/**
	 * END BEFORE.
	 */

	/***
	 * Save Schema DB
	 */
	@LoggerAuditWeb
	public void saveSchemaAccounting() {

		accountingSchema = new AccountingSchema();

		accountingSchema.setSchemaCode(this.accountingSchemaTo.getSchemaCode());
		accountingSchema.setDescription(this.accountingSchemaTo.getDescription());
		accountingSchema.setStatus(AccountingSchemaStateType.REGISTERED.getCode());
		accountingSchema.setBranchType(this.parameterBranchSelected);
		accountingSchema.setAuxiliaryType(this.parameterAuxiliaryTypeSelected);
		
		accountingSchema.setAssociateCode(this.parameterAssociateCodeSelected);
		

		schemaAccountingServiceFacade.saveAccountingSchema(accountingSchema);

		// Clean
		cleanAll();

	}

	/**
	 * Search Accounting Schema.
	 */
	@LoggerAuditWeb
	public void searchAccountingSchema() {

		List<AccountingSchema> listAccountingSchema;

		if (Validations.validateIsNotNullAndNotEmpty(this.accountingSchemaTo)) {

			listAccountingSchema = schemaAccountingServiceFacade.searchAccountingSchema(this.accountingSchemaTo);
			accountingSchemaDataModel = new GenericDataModel<AccountingSchema>(listAccountingSchema);

		}
		/** clean selected on data table */
		this.accountingSchemaSelection = null;
		if(accountingSchemaDataModel.getRowCount()>0){
			setTrueValidButtons();
		}
	}

	/**
	 * Update accounting schema locked.
	 */
	// LOCK
	@LoggerAuditWeb
	public void updateAccountingSchemaLocked() {

		try {

			if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {
				Object code = accountingSchemaSelection.getSchemaCode();
				AccountingSchema accountingSchema = schemaAccountingServiceFacade.getAccountingSchemaByPk(this.accountingSchemaSelection.getIdAccountingSchemaPk());

				accountingSchema.setStatus(AccountingSchemaStateType.LOCK.getCode());

				schemaAccountingServiceFacade.updateAccountingShemaAndMatrix(accountingSchema);

				accountingSchemaDataModel = null;
				searchAccountingSchema();

				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_SCHEMA_MODIFY,code));
				JSFUtilities.executeJavascriptFunction("cnfWDialogMessageSuccess.show();");

			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Update accounting schema un locked.
	 */
	// UNLOCK
	@LoggerAuditWeb
	public void updateAccountingSchemaUnLocked() {

		try {

			if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {
				Object code = accountingSchemaSelection.getSchemaCode();
				AccountingSchema accountingSchema = schemaAccountingServiceFacade.getAccountingSchemaByPk(this.accountingSchemaSelection.getIdAccountingSchemaPk());

				accountingSchema.setStatus(AccountingSchemaStateType.REGISTERED.getCode());

				schemaAccountingServiceFacade.updateAccountingShemaAndMatrix(accountingSchema);

				accountingSchemaDataModel = null;
				searchAccountingSchema();

				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_SCHEMA_MODIFY,code));
				JSFUtilities.executeJavascriptFunction("cnfWDialogMessageSuccess.show();");

			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Update accounting schema canceled.
	 */
	// CANCELED
	@LoggerAuditWeb
	public void updateAccountingSchemaCanceled() {

		try {

			if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {
				Object code = accountingSchemaSelection.getSchemaCode();
				AccountingSchema accountingSchema = schemaAccountingServiceFacade.getAccountingSchemaByPk(this.accountingSchemaSelection.getIdAccountingSchemaPk());
				accountingSchema.setStatus(AccountingSchemaStateType.CANCELED.getCode());

				schemaAccountingServiceFacade.updateAccountingShemaAndMatrix(accountingSchema);

				accountingSchemaDataModel = null;
				searchAccountingSchema();

				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_SCHEMA_MODIFY,code));
				JSFUtilities.executeJavascriptFunction("cnfWDialogMessageSuccess.show();");

			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	/**
	 * Show associated variables.
	 *
	 * @param event the event
	 */
	public void showAssociatedVariables(ActionEvent event) {
		
		if (event != null) {
			accountingSourceSelection=(AccountingSource )event.getComponent().getAttributes().get("blockInformation");
			
			AccountingSourceInfo accountingSourceInfo = null;
			List<FieldSourceTo> listFieldSourceTo = new ArrayList<FieldSourceTo>();

			if (Validations.validateIsNotNullAndNotEmpty(this.accountingSourceSelection)) {
			
				/**Load the file XML in physical**/
				byte[] file = sourceAccountingServiceFacade.getAccountingSourceContent(accountingSourceSelection.getIdAccountingSourcePk());
				try {
					/**
					 * Validate Structure the File XML and Load AccountingSourceInfo with the information
					 * **/
					accountingSourceInfo = AccountingUtils.loadXMLToObjectSource(file);
				} catch (ServiceException e) {

					/**If the structure is wrong produced**/
					if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_WRONG_STRUCTURED)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_WRONG_STRUCTURED, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}/**If Accounting Source Value Format**/
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_VALUE_FORMAT)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_VALUE_FORMAT, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}/**If Wrong visibility format**/
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_VISIBILITY_FORMAT)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_VISIBILITY_FORMAT, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}/**If Position format wrong**/
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_POSITION_FORMAT)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_POSITION_FORMAT, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}/**If Accounting Source Type Format**/
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_TYPE_FORMAT)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_FORMAT, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_TYPE_ASSOCIATE)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_ASSOCIATE, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}
					

//					log.error(e.getMessage());
					e.printStackTrace();
				}
				
				
				if (accountingSourceInfo != null) {

					for (Integer key : accountingSourceInfo.getFields().keySet()) {
						FieldSourceTo fieldSourceTo = accountingSourceInfo.getFields().get(key);

						if (fieldSourceTo.getVisibility()) {

							listFieldSourceTo.add(fieldSourceTo);
						}
					}

					fieldSourceToDataModel = new GenericDataModel<FieldSourceTo>(listFieldSourceTo);
					associatedVariable = Boolean.TRUE;
				}
				else if (Validations.validateIsNullOrEmpty(accountingSourceInfo)) {
					
					fieldSourceToDataModel = null;
					associatedVariable = Boolean.FALSE;
					
					if(Validations.validateListIsNotNullAndNotEmpty(listVariableFormulateTo))
						listVariableFormulateTo.clear();
					
				}
					
			}
			
			JSFUtilities.executeJavascriptFunction("cnIdFlagAddFormulate.show(); initDND();");	
		}
		
	}
	
	

	/**
	 * CLEAN PARAMETER TO.
	 */

	public void cleanResultAccountingSchemaDataModel() {
 
			accountingSchemaDataModel = null ;
	}

	/**
	 * Clean accounting schema search.
	 */
	public void cleanAccountingSchemaSearch() {

		this.accountingSchemaTo = new AccountingSchemaTo();
		accountingSchemaDataModel = null ;
	}

	/**
	 * Clean all.
	 */
	public void cleanAll() {

		accountingSchemaDataModel = null ;
		accountingSchemaTo = new AccountingSchemaTo();
		parameterBranchSelected = -1;
		parameterAuxiliaryTypeSelected = -1;
		parameterAssociateCodeSelected= -1;

		this.accountingSchemaTo.setRegisterUser(this.userInfo.getUserAccountSession().getUserName());

	}

	/**
	 * clean list Source, variable formulate and field Source.
	 */
	public void cleanBackToSource(){
		
		associatedVariable = Boolean.FALSE;
		
		accountingSourceDataModel=new GenericDataModel<AccountingSource>(null);
		listVariableFormulateTo=new ArrayList<VariableFormulateTo>();
		fieldSourceToDataModel= new GenericDataModel<FieldSourceTo>();
		
		if(goToAddAccountOnSchema)
			accountingMatrixDetailSelection=null;		
	}
	
	
	/**
	 * *
	 * Clean list to back , si es registrar, si es modificar no debe borrar.
	 */
	public void cleanBeforeBackToSource(){
		
		if(Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailSelection.getListVariableFormulateTo()) &&
				Validations.validateIsNotNullAndPositive(accountingMatrixDetailSelection.getListVariableFormulateTo().size()) ){
			for( VariableFormulateTo formulateTo : accountingMatrixDetailSelection.getListVariableFormulateTo() ){
					formulateTo.setValueDefault(null);
					formulateTo.setVariableSource(null);
			}
		}
		
	}
	
	/**
	 * Before Back to Source
	 * Question if go to Back
	 * *.
	 */
	public void beforeBackToSource(){
		
		Boolean existAssociation = Boolean.FALSE;
		
		if(	Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailSelection) &&
			Validations.validateListIsNotNullAndNotEmpty(accountingMatrixDetailSelection.getListVariableFormulateTo())){
			
			for( VariableFormulateTo formulateTo : accountingMatrixDetailSelection.getListVariableFormulateTo() ){
				if( Validations.validateIsNotNullAndNotEmpty(formulateTo.getValueDefault()) ||
					Validations.validateIsNotNullAndNotEmpty(formulateTo.getVariableSource())	){
						existAssociation = Boolean.TRUE;
				}
			}
		}
		
		
		if(existAssociation){
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WARNING_BACK_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWAlertHideDialog.show()");
		}
		else
			{
				cleanBeforeBackToSource();
				JSFUtilities.executeJavascriptFunction("cnIdFlagAddFormulate.hide()");	
			}
		
	}
	
	
	/**
	 * Before clean.
	 */
	public void beforeClean() {

		if ((Validations.validateIsNotNullAndNotEmpty(this.accountingSchemaTo.getSchemaCode()))
				|| (Validations.validateIsNotNullAndNotEmpty(this.accountingSchemaTo.getDescription()))
				|| (Validations.validateIsNotNullAndPositive(parameterBranchSelected))
				|| (Validations.validateIsNotNullAndPositive(parameterAuxiliaryTypeSelected))
				|| (Validations.validateIsNotNullAndPositive(parameterAssociateCodeSelected))
				) {

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CLEAN_DATA_EXCEPTION,""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanRegister.show();");

		}
	}

	/**
	 * OTHER.
	 *
	 * @return the string
	 */

	public String beforeBackToSearch() {

		if ((Validations.validateIsNotNullAndNotEmpty(this.accountingSchemaTo .getSchemaCode()))
				|| (Validations .validateIsNotNullAndNotEmpty(this.accountingSchemaTo.getDescription()))
				|| (Validations .validateIsNotNullAndPositive(parameterBranchSelected))
				|| (Validations .validateIsNotNullAndPositive(parameterAuxiliaryTypeSelected)) 
				|| (Validations .validateIsNotNullAndPositive(parameterAssociateCodeSelected))) {

			showMessageOnDialog( PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage( PropertiesConstants.WARNING_BACK_EXCEPTION_SEARCH, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanBack.show();");

			return "";

		} else

			return "schemaSearch";
	}

	/**
	 * Go to back to search.
	 *
	 * @return the string
	 */
	public String goToBackToSearch() {

		// Clean
		cleanAll();
		return "schemaSearch";
	}

	/**
	 * Accounting shema on consult.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void accountingShemaOnConsult(ActionEvent event)
			throws ServiceException {

		if (event != null) {

			AccountingSchema accountingSchema = (AccountingSchema) event.getComponent().getAttributes().get("blockInformation");

			this.consult = Boolean.TRUE;

			this.accountingSchemaTo.setSchemaCodeConsult(accountingSchema.getSchemaCode());
			this.accountingSchemaTo.setDescription(accountingSchema.getDescription());
			this.accountingSchemaTo.setStatusDescription(accountingSchema.getDescriptionParameterStatus());

			this.accountingSchemaTo.setRegisterDate(accountingSchema.getRegisterDate());
			this.accountingSchemaTo.setRegisterUser(accountingSchema.getRegisterUser());

			if (Validations.validateIsNotNullAndNotEmpty(accountingSchema.getLastModifyUser())) {
				this.accountingSchemaTo.setModifyDate(accountingSchema.getLastModifyDate());
				this.accountingSchemaTo.setModifyUser(accountingSchema.getLastModifyUser());
			}
			if (Validations.validateIsNotNullAndNotEmpty(accountingSchema.getCancelUser())) {
				this.accountingSchemaTo.setCancelDate(accountingSchema.getCancelDate());
				this.accountingSchemaTo.setCancelUser(accountingSchema.getCancelUser());
			}

			parameterBranchSelected  = accountingSchema.getBranchType(); 
			parameterAuxiliaryTypeSelected = accountingSchema.getAuxiliaryType();
			parameterAssociateCodeSelected=accountingSchema.getAssociateCode();
			
			/***
			 * Add to Accounting Account
			 */
			
			//Search all Schema's matrix
			listAccountingMatrixDetailForModify = new ArrayList<AccountingMatrixDetailTo>();
		
			/**		search all matrix detail with formulate, source and state		**/
			listAccountingMatrixDetailForModify 	   = schemaAccountingServiceFacade.findAccountingMatrixDetailBySchemaFilter(accountingSchema);

			
			accountingMatrixDetailDataModel = new GenericDataModel<AccountingMatrixDetailTo>(listAccountingMatrixDetailForModify);

		}

	}

	/**
	 * Before modify schema.
	 *
	 * @return the boolean
	 */
	@LoggerAuditWeb
	public Boolean beforeModifySchema() {

		AccountingSchema accountingSchema = 
				schemaAccountingServiceFacade.getAccountingSchemaByPk(this.accountingSchemaSelection.getIdAccountingSchemaPk());

		Object code = accountingSchema.getSchemaCode();

		if (accountingSchema.getSchemaCode().equals(this.accountingSchemaSelection.getSchemaCode())
				&& accountingSchema.getDescription().equals(this.accountingSchemaSelection.getDescription())
				&& accountingSchema.getBranchType().equals(this.accountingSchemaSelection.getBranchType())
				&& accountingSchema.getAuxiliaryType().equals(	this.accountingSchemaSelection.getAuxiliaryType()) ) {

			return Boolean.FALSE;

		} else{
		return Boolean.TRUE;
		}
	}

	/**
	 * MODIFY Only Schema.
	 */
	@LoggerAuditWeb
	public void modifyAccountingSchema() {

		try {

			AccountingSchema accountingSchema = schemaAccountingServiceFacade.getAccountingSchemaByPk(this.accountingSchemaSelection.getIdAccountingSchemaPk());

			accountingSchema.setSchemaCode(this.accountingSchemaSelection.getSchemaCode());
			accountingSchema.setDescription(this.accountingSchemaSelection.getDescription());
			accountingSchema.setBranchType(this.accountingSchemaSelection.getBranchType());
			accountingSchema.setAuxiliaryType(this.accountingSchemaSelection.getAuxiliaryType());

			schemaAccountingServiceFacade.updateAccountingShema(accountingSchema);

			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_MODIFY_SCHEMA, ""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");

			// Clean
			cleanAll();

			return;

		} catch (ServiceException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Modify back to search.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String modifyBackToSearch() {

		AccountingSchema accountingSchema = schemaAccountingServiceFacade.getAccountingSchemaByPk(this.accountingSchemaSelection.getIdAccountingSchemaPk());

		Object code = accountingSchema.getSchemaCode();

		if (!(accountingSchema.getSchemaCode()
				.equals(this.accountingSchemaSelection.getSchemaCode()))
				|| !(accountingSchema.getDescription()
						.equals(this.accountingSchemaSelection.getDescription()))
				|| !(accountingSchema.getBranchType()
						.equals(this.accountingSchemaSelection.getBranchType()))) {

			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WARNING_MODIFY_SCHEMA,code));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanBack.show();");

			return "";
		} else

			return "schemaSearch";

	}
	
	/**
	 * ADD ACOUNTING ON SCHEMA MODIFY.
	 */
	@LoggerAuditWeb
	public void addAccountingAccountOnSchemaForModify() {
		
		Boolean correct = Boolean.TRUE;
		
		if( Validations.validateIsNotNullAndNotEmpty(this.accountingAccountTo) && 
		    Validations.validateIsNotNullAndNotEmpty(this.accountingAccountTo.getAccountCode()) ){
			/**		2do List		**/
			
			if( Validations.validateListIsNullOrEmpty(listAccountingMatrixDetailForModify) 
					&& Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTemp)){
				listAccountingMatrixDetailForModify = AccountingUtils.cloneList(listAccountingMatrixDetailTemp);
			}
			
			/**		1st List		**/
			if( listAccountingMatrixDetailForModify.size() >= 1){
				for(AccountingMatrixDetailTo accountingMatrixDetail : listAccountingMatrixDetailForModify ){
					if (accountingMatrixDetail.getAccountingAccountTo().getIdAccountingAccountPk().equals(this.accountingAccountTo.getIdAccountingAccountPk())) {
										
						showMessageOnDialog(PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ERROR_REPEAT));
						JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
						
						correct = Boolean.FALSE;
						
						break ;
					}else
						correct = Boolean.TRUE;
				}
			}else
				
				correct = Boolean.TRUE;
		}
		
		if( Validations.validateIsNotNullAndNotEmpty(this.accountingAccountTo) && 
			    Validations.validateIsNotNullAndNotEmpty(this.accountingAccountTo.getAccountCode()) &&
			    					correct)	{
			
			accountingMatrixDetailTo=new AccountingMatrixDetailTo();
			accountingMatrixDetailTo.setAccountingAccountTo(accountingAccountTo);
			accountingMatrixDetailTo.setDynamicType(accountingAccountTo.getNatureType());
			
			indModifyMatrixDetail = 1;
			
			/**AccountingAccount TreeNode**/
			accountingMatrixDetailTo.setListAccountingAccountTree(accountAccountingServiceFacade.findAccountParentAsc(accountingAccountTo,new BigDecimal(1)));
			
			listAccountingMatrixDetailForModify.add(accountingMatrixDetailTo);
			accountingMatrixDetailDataModel = new GenericDataModel<AccountingMatrixDetailTo>(listAccountingMatrixDetailForModify);
			this.accountingAccountTo = new AccountingAccountTo();
		} 
		
	}
	
	/**
	 * DELETE ACOUNTING ON SCHEMA WHEN IS MODIFY.
	 *
	 * @param event the event
	 */
		public void accountingAccountRemoveModify(ActionEvent event) {
			AccountingMatrixDetailTo accountingMatrixDetailTo = (AccountingMatrixDetailTo) event
					.getComponent().getAttributes().get("removeName");

			listAccountingMatrixDetailForModify.remove(accountingMatrixDetailTo);

		}
	
	/**
	 * Adds the accounting account on schema.
	 */
	// ADD ACOUNTING ON SCHEMA
	@LoggerAuditWeb
	public void addAccountingAccountOnSchema() {
		
		Boolean correct = Boolean.TRUE;
		
		if(Validations.validateIsNull(accountingAccountTo)){
			showMessageOnDialog(PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ERROR_OTHER));
			JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
			return;
		}else if(Validations.validateIsNullOrNotPositive(accountingAccountTo.getNatureType())){
			showMessageOnDialog(PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ERROR_NATURE));
			JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
			return;
		}else if(Validations.validateIsNullOrNotPositive(this.accountingAccountTo.getIdAccountingAccountPk())){
			showMessageOnDialog(PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ERROR_NUMBER_ACCOUNT));
			JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
			return;
		}
		
		if( Validations.validateIsNotNullAndNotEmpty(this.accountingAccountTo) && 
		    Validations.validateIsNotNullAndNotEmpty(this.accountingAccountTo.getAccountCode()) ){
			/**		2do List		**/
			
			if( Validations.validateListIsNullOrEmpty(listAccountingMatrixDetailTo) 
					&& Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTemp)){
				listAccountingMatrixDetailTo = AccountingUtils.cloneList(listAccountingMatrixDetailTemp);
			}
			
			/**		1st List		**/
			if( listAccountingMatrixDetailTo.size() >= 1){
				for(AccountingMatrixDetailTo accountingMatrixDetail : listAccountingMatrixDetailTo ){
					if (accountingMatrixDetail.getAccountingAccountTo().getIdAccountingAccountPk().equals(this.accountingAccountTo.getIdAccountingAccountPk())) {
										
						showMessageOnDialog(PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ERROR_REPEAT));
						JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
						
						correct = Boolean.FALSE;
						
						break ;
					}else
						correct = Boolean.TRUE;
				}
			}else
				
				correct = Boolean.TRUE;
		}
		
		if( Validations.validateIsNotNullAndNotEmpty(this.accountingAccountTo) && 
			    Validations.validateIsNotNullAndNotEmpty(this.accountingAccountTo.getAccountCode()) &&
			    					correct)	{
			
			accountingMatrixDetailTo=new AccountingMatrixDetailTo();
			accountingMatrixDetailTo.setAccountingAccountTo(accountingAccountTo);
			accountingMatrixDetailTo.setDynamicType(accountingAccountTo.getNatureType());
			
			/**AccountingAccount TreeNode**/
			accountingMatrixDetailTo.setListAccountingAccountTree(accountAccountingServiceFacade.findAccountParentAsc(accountingAccountTo,new BigDecimal(1)));
			
			listAccountingMatrixDetailTo.add(accountingMatrixDetailTo);
			accountingMatrixDetailDataModel = new GenericDataModel<AccountingMatrixDetailTo>(listAccountingMatrixDetailTo);
			this.accountingAccountTo = new AccountingAccountTo();
		} 
		
	}
	
	  
	/**
	 * Accounting account remove.
	 *
	 * @param event the event
	 */
	// DELETE ACOUNTING ON SCHEMA WHEN IS REGISTER
	public void accountingAccountRemove(ActionEvent event) {
		AccountingMatrixDetailTo accountingMatrixDetailTo = (AccountingMatrixDetailTo) event
				.getComponent().getAttributes().get("removeName");

		listAccountingMatrixDetailTo.remove(accountingMatrixDetailTo);

	}

	/**
	 * Adds the account back to search.
	 *
	 * @return the string
	 */
	public String addAccountBackToSearch() {

		if (Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTo) && 
				listAccountingMatrixDetailTemp.size()  !=  listAccountingMatrixDetailTo.size()) {  

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WARNING_ADD_ACCOUNT_BACK,""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanBack.show();");   
			return "";

		} else{
			
			if(Validations.validateIsNotNull(listAccountingMatrixDetailTo)){
				this.listAccountingMatrixDetailTo.clear();
			}
			this.accountingSchemaSelection =null;			
			return "schemaSearch";	
		}
	}

	/**
	 * Before save accounting accounts.
	 */
	public void beforeSaveAccountingAccounts() {
		
		//Remove old register
		List<AccountingMatrixDetailTo> listMatrixDetailToTemp = AccountingUtils.cloneList(listAccountingMatrixDetailTo);
		Collection<AccountingMatrixDetailTo> collectionRemove = new ArrayList<AccountingMatrixDetailTo>();

		for(AccountingMatrixDetailTo accountingMatrixDetailTemp : listMatrixDetailToTemp){
			if(Validations.validateIsNull(accountingMatrixDetailTemp.getAccountingAccountTo().getIndAccumulation())){
				collectionRemove.add(accountingMatrixDetailTemp);
			}
		}
		listMatrixDetailToTemp.removeAll(collectionRemove);
		listAccountingMatrixDetailTo=AccountingUtils.cloneList(listMatrixDetailToTemp);
		
		
		
		
		if (Validations.validateListIsNullOrEmpty(listAccountingMatrixDetailTo)) {
			
			/** NOT ACCOUNT TO ADD **/
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NOT_ACCOUNT_TO_ADD));
			JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
			
		}else  {
			
			
			/**
			 * Verify if all matrices are formulated and detailed accounts
			 * **/
			Boolean prepareDetailMatrix=Boolean.TRUE;
			for (AccountingMatrixDetailTo accountingMatrixDetailTo : listAccountingMatrixDetailTo) {
				
				if (!accountingMatrixDetailTo.getPrepareForSave()){
					prepareDetailMatrix=Boolean.FALSE;
					break;
				}
			}
			
			/**IT´S OK**/
			if(prepareDetailMatrix){
				Object code = this.accountingSchemaTo.getSchemaCode();		
				
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_ADD_ACCOUNTING_ACCOUNT,code));			
				JSFUtilities.executeJavascriptFunction("cnfWConfirmAddAccount.show();");	
			}
			else{
				/**IF MISSING FORMULA OR SOURCE**/
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_MISSING_FORMULA_OR_SOURCE));
				JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
			}
					
			
		}

	}

	/**
	 * Save Matrix and DetailMatrix.
	 */
	@LoggerAuditWeb
	public void saveAccountingMatrix() {

		// 2nd List, if ListAccountingMatrixDetailTemp > 1 so exist old matrix
		if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTo) &&
				Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTemp) &&
				Validations.validateIsNotNullAndNotEmpty(listAccountingMatrixDetailTemp.get(0).getAccountingMatrixFk()) ){

			try {
				
				// Get Schema PK accountingSchemaSelection
				this.accountingMatrixTo.setAccountingSchemaFk(this.accountingSchema.getIdAccountingSchemaPk());   
				
				// Save MatrixDetail and not Matrix because there exist.
				accountingMatrixServiceFacade.saveAccountingMatrixDetail(this.accountingMatrixTo, this.accountingSchema);
				
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_ASSOCIATED_ACCOUNT_SCHEMA, ""));
				JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");
			
			} catch (ServiceException e) {
				/*** MENSAJE: OCURRIO UN ERROR INESPERADO, POR FAVOR VERIFIQUE */
				e.printStackTrace();
			}
		
		}
		else{

			//Load AccountingSchemaTo
			accountingSchemaTo.setStatus(AccountingSchemaStateType.REGISTERED.getCode());
			accountingSchemaTo.setBranchType(this.parameterBranchSelected);
			accountingSchemaTo.setAuxiliaryType(this.parameterAuxiliaryTypeSelected);
			accountingSchemaTo.setAssociateCode(this.parameterAssociateCodeSelected);
			
			//Setting Schema to AccountingMatrixTo
			accountingMatrixTo.setAccountingSchemaTo(accountingSchemaTo);
			
			
			
			try {
				accountingMatrixServiceFacade.saveAccountingSchemaWithMatrix(this.accountingMatrixTo);
			} catch (ServiceException e) {
				/*** MENSAJE: OCURRIO UN ERROR INESPERADO, POR FAVOR VERIFIQUE */
				e.printStackTrace();
			}
	
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_SAVE_SCHEMA_AND_MATRIX, ""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");
		
		}

		//Clean List
		if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTo))
			listAccountingMatrixDetailTo.clear();
		if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTemp))
			listAccountingMatrixDetailTemp.clear();
		
	}
	
	
	/**
	 * viewFormulate*.
	 *
	 * @param event the event
	 */
	public void viewFormulateCompoment(ActionEvent event) {
		if (event != null) {

			accountingMatrixDetailSelection = (AccountingMatrixDetailTo) event.getComponent().getAttributes().get("toFormulate");
			loadListImputToFormula();
			if(Validations.validateIsNotNull(accountingMatrixDetailSelection.getAccountingFormulateTo())){
				formulaFinal=accountingMatrixDetailSelection.getListAccountingParameterTO();
				stringFinalFormula=accountingMatrixDetailSelection.getAccountingFormulateTo().getFormulateInput();
			}
			
			JSFUtilities.executeJavascriptFunction("cnIdFlagAddFormulate.show()");
		}
	}

	
	
	/**
	 *  message to confirm delete source when modify the formulate *.
	 *
	 * @param event the event
	 */
	public void alerToModifyFormulateCompoment(ActionEvent event) {
		
		showMessageOnDialog(
				PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM ),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DELETE_SOURCE_ON_MATRIX, "" ));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDeleteMatrixDetail.show()");

	}
	
	
	/**
	 * viewSource*.
	 *
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void viewSourceCompomentOnModify(ActionEvent event) {
		if (event != null) {
			accountingMatrixDetailSelection = (AccountingMatrixDetailTo) event.getComponent().getAttributes().get("toSorce");
			AccountingMatrix accountingMatrix = new AccountingMatrix();
			accountingMatrix.setIdAccountingMatrixPk(accountingMatrixDetailSelection.getAccountingMatrixFk());
			
			// Find the Source
			List<AccountingMatrixDetail> listAccountingMatrixs = schemaAccountingServiceFacade.findFormulateAndSourceByAccountingMatrixDetail(accountingMatrixDetailSelection);
		
			// Get the file source
			byte[] file  = sourceAccountingServiceFacade.getAccountingSourceContent(listAccountingMatrixs.get(0).getAccountingSource().getIdAccountingSourcePk());
			
			loadListImputToFormula();
			JSFUtilities.executeJavascriptFunction("cnIdFlagAddFormulate.show()");
		
		}
	}
	
	

	/**
	 *  CHECK ADD SOURCE *.
	 *
	 * @param event the event
	 */
	public void addSourceToSchema(ActionEvent event) {
		if (event != null) {
		
			accountingMatrixDetailSelection = (AccountingMatrixDetailTo) event.getComponent().getAttributes().get("toSource");
			
		}
	} 
	
	
	
	/**
	 * Go to source from matrix.
	 *
	 * @return the string
	 */
	public String goToSourceFromMatrix(){
		
		/**Si el archivo tiene asociada una formula**/
		if(Validations.validateIsNotNull(accountingMatrixDetailSelection)&&
				Validations.validateIsNotNull(accountingMatrixDetailSelection.getAccountingFormulateTo())){
		
			accountingSourceDataModel = new GenericDataModel<AccountingSource>();
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingSources)){

				listAccountingSources.clear();
			}
			
			accountingSourceDataModel=null;
			
			return "accountingSourceMgmtUpload";
			
		}
		else{
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NO_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
			return "";
		}
				
	}
	
	
	/**
	 * ADD FORMULATE AND.
	 */

	public void consultFormulateOnSchema() {

		JSFUtilities.executeJavascriptFunction("cnIdFlagConsultFormulateSourse.show()");
	}

	

	/**
	 * Validate formulate.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateFormulate() throws ServiceException {
		
		Boolean isValidate = Boolean.FALSE;		
		String stringOfFormula = null;
		Parser parser = new Parser();
		
		if (Validations.validateListIsNotNullAndNotEmpty(formulaFinal)) {
			stringOfFormula = AccountingUtils.convertListObjectToStringWithOutComma(
					AccountingUtils.convertListGenericToObject(formulaFinal)).
						replaceAll(" ", AccountingUtils.VERTICAL);
			parser.settingFormulate(stringOfFormula);
			isValidate = parser.validateFormulate();
			parser.resetFormulate();
		
		}else {
			
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_MISSING_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWValidate.show(); initDND();");
			
			return;
		}
		
		
		
		/** RIGHT FORMULA */
		if (isValidate) {

			/** SETTING FORMULA **/
			stringFinalFormula = stringOfFormula;

			/** RIGHT FORMULA */
			setAcceptFormulate(Boolean.TRUE);

			/** OFFSET ***/

			listVariableFormulateTo = parser.verifyAbsolutePosition(stringFinalFormula);

			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_VALIDATE_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWValidate.show(); initDND();");
		} else {
			stringFinalFormula = null;
			listVariableFormulateTo = null;
			/** WRONG FORMULA */
			setAcceptFormulate(Boolean.FALSE);
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_VALIDATE_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWValidate.show(); initDND();");
		}

	}

	/**
	 * BACK TO SCHEMA ADD ACCOUNT
	 * 
	 * *.
	 */
	public void cancelFromFormulate() {
		
		if(	( Validations.validateIsNotNullAndNotEmpty(formulaFinal) && Validations.validateIsNotNullAndPositive(formulaFinal.size()) ) ||
		    ( Validations.validateIsNotNullAndNotEmpty(formulaFinal)  && 
		    		Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailSelection.getListAccountingParameterTO()) && 
		    		Validations.validateIsNotNullAndPositive(accountingMatrixDetailSelection.getListAccountingParameterTO().size()) ) ){
		
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WARNING_BACK_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWAlertCancelFormulate.show(); initDND();");
		}
		else{
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WARNING_NOT_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWAlertCancel.show();  initDND();");
		}
	}
	

	/**
	 * *ADD FORMULATE TO DETAIL MATRIX, AND HIDE DIALOG*.
	 */
	public void addFormulateToMatrixDetail() {

		accountingMatrixDetailSelection.setListVariableFormulateTo(listVariableFormulateTo);		
		accountingMatrixDetailSelection.setListAccountingParameterTO(formulaFinal);		
		accountingMatrixDetailSelection.setAccountingFormulateTo(new AccountingFormulateTo(stringFinalFormula));
		/**
		 *    ASSOCIATE PARAMETER ACCUNTING WITH VARIABLE FORMULATE
		 */
		
		for (VariableFormulateTo variableFormulateTo : listVariableFormulateTo) {
			
			String nameVariableFormulate	= variableFormulateTo.getNameVariable();
			
			for (AccountingParameterTO accountingParameterTO : formulaFinal) {
				
				if (accountingParameterTO.getParameterName().trim().
						equalsIgnoreCase(nameVariableFormulate.replace(AccountingUtils.VERTICAL, " ").trim())){
					
					variableFormulateTo.setParameterFormulate(accountingParameterTO);
					break;
				}
			}
			
		}
		
		
		this.stringFinalFormula=null;
		this.formulaFinal= new ArrayList<AccountingParameterTO>();

	}

	/**
	 * CLEAN FORMULATE*.
	 */
	public void cleanFormulate(){
		setAcceptFormulate(Boolean.FALSE);
		formulaFinal.clear();
	}
	
	
	/**
	 * *CANCEL FORMULATE AND HIDE DIALOG.
	 */
	public void cancelFormulate() {

		// Limpiar formulario = null ;
		if( ( Validations.validateIsNotNullAndNotEmpty(formulaFinal) && Validations.validateIsNotNullAndPositive(formulaFinal.size()) ))
				formulaFinal.clear();
		
		if  ( Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailSelection)  && Validations.
   													validateListIsNotNullAndNotEmpty(accountingMatrixDetailSelection.getListAccountingParameterTO()) )
				accountingMatrixDetailSelection.setListAccountingParameterTO(formulaFinal);
		
		this.acceptFormulate = Boolean.FALSE;
		this.disableFormulate = Boolean.FALSE;
		
		JSFUtilities.executeJavascriptFunction("cnIdFlagAddFormulate.hide();"); 

	}

	/**
	 * *
	 * Clean Add Accounting Schema.
	 */
	public void cleanAddAccountingAccount() {


		if (Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTo)&&
				Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTemp)&&
				listAccountingMatrixDetailTemp.size()  !=  listAccountingMatrixDetailTo.size()) {

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CLEAN_DATA_EXCEPTION,""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmClean.show();");

			return;
			
		} else

			cleanAddAccountingMatrix(); 

	}

	/**
	 * Clean to back add accounting matrix.
	 */
	public void cleanToBackAddAccountingMatrix() {

		accountingAccountTo = new AccountingAccountTo();
		accountingSchemaSelection=new AccountingSchema();
		accountingSchema=new AccountingSchema();
		accountingMatrixDetailDataModel= new GenericDataModel<AccountingMatrixDetailTo>();
		listAccountingMatrixDetailTo.clear();
		
 
	}
	
	/**
	 * Clean add accounting matrix.
	 */
	public void cleanAddAccountingMatrix() {

		cleanToBackAddAccountingMatrix()  ;

		if( Validations.validateListIsNullOrEmpty(listAccountingMatrixDetailTo) 
				&& Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailTemp)){
			listAccountingMatrixDetailTo = AccountingUtils.cloneList(listAccountingMatrixDetailTemp);
			accountingMatrixDetailDataModel = new GenericDataModel<AccountingMatrixDetailTo>(listAccountingMatrixDetailTo);
		}
	}

	/**
	 * Load branch.
	 */
	@LoggerAuditWeb
	public void loadBranch() {

	}
 

	/**
	 * Load sources.
	 */
	@LoggerAuditWeb
	public void loadSources() {

		AccountingSourceTo accountingSourceToTemp = new AccountingSourceTo();
		listAccountingSources = new ArrayList<AccountingSource>();
		accountingSourceToTemp.setAuxiliaryType(parameterAuxiliaryTypeSelected);
		accountingSourceToTemp.setFileSourceName(fileSourceName);
		accountingSourceToTemp.setDescriptionSource(fileDescription);
		accountingSourceToTemp.setStatus(AccountingSourceStateType.REGISTERED.getCode()); 
		listAccountingSources = sourceAccountingServiceFacade.loadAccountingSource(accountingSourceToTemp);

		accountingSourceDataModel = new GenericDataModel<AccountingSource>();

		if (Validations.validateListIsNotNullAndNotEmpty(listAccountingSources)) {
			accountingSourceDataModel = new GenericDataModel<AccountingSource>(listAccountingSources);
		}
		
		/** clean selected on data table */
		this.accountingSourceSelection = null;
		
	}

	
	/**
	 * Back add account.
	 *
	 * @return the string
	 */
	public String backAddAccount(){
		
 		String backAddAccount = null;
		
		if(Validations.validateIsNotNullAndNotEmpty(accountingSourceSelection) ){
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CLEAN_DATA_EXCEPTION ,""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmBack.show();");
		}
		else{
			
			if(goToAddAccountOnSchema)
				backAddAccount="schemaRegister";
			else if(goToModifyMatrixDetailOnSchema)
				backAddAccount="schemaModify";
		}
			
		return backAddAccount;
				
	}
			 

	/**
	 * Back to add account.
	 *
	 * @return the string
	 */
	public String backToAddAccount(){

		String backAddAccount = null ;
		
		accountingSourceDataModel = new GenericDataModel<AccountingSource>();
		associatedVariable = Boolean.FALSE;
		// Limpiar las dos listas
		
		if(goToAddAccountOnSchema)
			backAddAccount="schemaRegister";
		else if(goToModifyMatrixDetailOnSchema)
			backAddAccount="schemaModify";
		
		return backAddAccount ;
	}

	/**
	 * Method Remove Reference To Table
	 * variableFormulate= variableFormulate.getPositionFormulate()
	 */
	public void removeReferenceToTable(){
		
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		Integer indexFormulate = Integer.parseInt(params.get("frmSearchBookAccounts:variableHiddenIndexId"));
		
		for (Iterator iterator = accountingMatrixDetailSelection.getListVariableFormulateTo().iterator(); iterator.hasNext();) {
			
			VariableFormulateTo variableFormulate = (VariableFormulateTo) iterator.next();
			if (variableFormulate.getPositionFormulate().equals(indexFormulate)) {
				variableFormulate.setVariableSource(null);
				variableFormulate.setPositionSource(null);
				variableFormulate.setValueSource(null);
				variableFormulate.setValueDefault(null);
				variableFormulate.setSourceType(null);
				break;
			}
		}
		cleanVariableContainers();
	}
	
	
	
	/**
	 * 		Before the setting values , should validate that exists only value : VALUE OR REFERENCE  
	 * *.
	 */
	public void validateSettingValues() {
		
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		/*** INDICATORS RECOVER **/
		String variableValidation	= params.get("frmSearchBookAccounts:identifyValidationId");		
		
		showMessageOnDialog(
				PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PARAMETERIZE_TEMPORAL, ""));
		JSFUtilities.executeJavascriptFunction("cnfWarningValidateMatch.show();");
				
	}
	
	
	/**
	 *  MATCH SOURCE'S VALUES WITH FORMULATE'S VALUES.
	 */
	public void addSourceToFormula() {
		
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		/*** INDICATORS RECOVER **/
		String variableValidation	= params.get("frmSearchBookAccounts:identifyValidationId");		
		
		
		
		Integer positionSource = null;
		Integer indexTarget = null;/*** need for selected the variable ***/
		if (Validations.validateIsNotNullAndNotEmpty(params.get("frmSearchBookAccounts:variableHiddenCodeId"))){
			 positionSource = Integer.parseInt(params.get("frmSearchBookAccounts:variableHiddenCodeId"));
		}
		
		if (Validations.validateIsNotNullAndNotEmpty(params.get("frmSearchBookAccounts:variableHiddenIndexId"))){
			indexTarget = Integer.parseInt(params.get("frmSearchBookAccounts:variableHiddenIndexId"));
		}
	
		
		List<FieldSourceTo> listFieldSourceTo = this.fieldSourceToDataModel.getDataList();
		
		FieldSourceTo fieldSourceToTemp =null;
		for (FieldSourceTo fieldSourceTo : listFieldSourceTo) {
			if (fieldSourceTo.getPosicion().equals(positionSource)) {
				fieldSourceToTemp = fieldSourceTo;
				break;
			}
		}
		
		List<VariableFormulateTo> variableFormulateToList  =  accountingMatrixDetailSelection.getListVariableFormulateTo();
		
		for (VariableFormulateTo variableFormulateTo : variableFormulateToList) {
			
			if (variableFormulateTo.getPositionFormulate().equals(indexTarget)) {
				
					
					if (Validations.validateIsNotNull(variableValidation) && variableValidation.equals(PropertiesConstants.SETTING_VALUE)){
						
						variableFormulateTo.setVariableSource(null);
						variableFormulateTo.setPositionSource(null);						
						variableFormulateTo.setValueSource(null);
						variableFormulateTo.setSourceType(AccountingFieldSourceType.VALUE.getCode());					
					}else if (Validations.validateIsNotNull(variableValidation) && variableValidation.equals(PropertiesConstants.REMOVE_VALUE)){
												 
						variableFormulateTo.setValueDefault(null);
					}else{
											
						if (Validations.validateIsNotNull(variableValidation) && variableValidation.equals(PropertiesConstants.SETTING_REFERENCE)){
							variableFormulateTo.setValueDefault(null);							
						}
						
						variableFormulateTo.setVariableSource(fieldSourceToTemp.getName());
						variableFormulateTo.setPositionSource(fieldSourceToTemp.getPosicion());								
						if (AccountingFieldSourceType.get(fieldSourceToTemp.getParameterType()).getValue().
								equalsIgnoreCase(AccountingFieldSourceType.VALUE.getValue())){
							variableFormulateTo.setValueSource( new BigDecimal(fieldSourceToTemp.getValue()));
							variableFormulateTo.setSourceType(AccountingFieldSourceType.VALUE.getCode());	
						}else{
							variableFormulateTo.setSourceType(AccountingFieldSourceType.REFERENCE.getCode());	
						}
					}
															
					break;
			}
						
		}
		cleanVariableContainers();
		
		
		
	}
	
	
	
	/**
	 * clean variables containers, visually.
	 */
	public void cleanVariableContainers(){
		this.variableHiddenName = "";
		this.variableHiddenCode = "";
		this.variableHiddenIndex = "";
		this.identifyValidation = "";
	}
	
	

	/**
	 *  Accounting Source *.
	 *
	 * @return the string
	 */
	public String beforeSaveTempMatrixDetail() {
	
		
		Boolean detailMatrixComplete=Boolean.TRUE;
		
		if (Validations.validateListIsNotNullAndNotEmpty(this.accountingMatrixDetailSelection.getListVariableFormulateTo())) {
			
			/** FIRST COMPLETE INFORMATION FOR EACH VARIABLE FORMULATE ****/
			for (VariableFormulateTo variableFormulateTo : accountingMatrixDetailSelection.getListVariableFormulateTo()) {
				
				if(Validations.validateIsNotNull(variableFormulateTo.getValueDefault()) &&
				  Validations.validateIsPositiveNumber(variableFormulateTo.getValueDefault().intValue())	){
					
					variableFormulateTo.setSourceType(AccountingFieldSourceType.VALUE.getCode());											
				}						
			}
			
			
			String  valueSource  = "";
			String  valueDefault  = "";	
			for (VariableFormulateTo variableFormulateTo : accountingMatrixDetailSelection.getListVariableFormulateTo()) {
				
				valueSource = (variableFormulateTo.getValueSource()==null || variableFormulateTo.getValueSource().toString().equals(GeneralConstants.ZERO_VALUE_STRING)) ? "" :variableFormulateTo.getValueSource().toString();
				valueDefault = (variableFormulateTo.getValueDefault()==null || variableFormulateTo.getValueDefault().toString().equals(GeneralConstants.ZERO_VALUE_STRING)) ? "" :variableFormulateTo.getValueDefault().toString();

				if(   Validations.validateIsNull(variableFormulateTo.getSourceType()) ||						
					  
					  (  AccountingFieldSourceType.get(variableFormulateTo.getSourceType()).getValue().
						 equalsIgnoreCase(AccountingFieldSourceType.VALUE.getValue())  &&
						 ( Validations.validateIsEmpty(valueSource) && Validations.validateIsEmpty(valueDefault))  ) ||
													
						  (  AccountingFieldSourceType.get(variableFormulateTo.getSourceType()).getValue().
							 equalsIgnoreCase(AccountingFieldSourceType.REFERENCE.getValue())     &&						 
							 Validations.validateIsNull(variableFormulateTo.getPositionSource())									 
									  )	  								  
						){
					
					detailMatrixComplete=Boolean.FALSE;				
					
				}
								
				if (!detailMatrixComplete){
					break;
				}
			}
			
			if(detailMatrixComplete){
				accountingMatrixDetailSelection.setPrepareForSave(Boolean.TRUE);
				accountingMatrixDetailSelection.setAccountingSourceFk(this.accountingSourceSelection.getIdAccountingSourcePk());														
				this.accountingMatrixTo.setAccountingMatrixDetail(this.listAccountingMatrixDetailTo);
				
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(
								PropertiesConstants.CONFIRM_PARAMETERIZE_TEMPORAL, ""));
				JSFUtilities.executeJavascriptFunction("cnfWConfirmMatrixDetailSave.show();");
			}else{
			
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_COMPLETE_DATA_EXCEPTION, ""));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
			}
		}

		return "";
	}

	
	/**
	 *   Save on memory temporal , 1° formulate and 2° source accounting.
	 *
	 * @return the string
	 */
	public String saveMatrixDetail() {
		
		String gotoPage = null;
		
		accountingMatrixDetailSelection.setAccountingSourceFk(this.accountingSourceSelection.getIdAccountingSourcePk());
		
		this.accountingMatrixTo.setAccountingMatrixDetail(this.listAccountingMatrixDetailTo);
		indModifyMatrixDetail = 1;
		
		cleanBackToSource();
		
		if(goToAddAccountOnSchema)
			gotoPage="schemaRegister";
		else if(goToModifyMatrixDetailOnSchema)
			gotoPage="schemaModify";
			
		
		return gotoPage;
		 
	}
	
	
	
	
	
	/*
	 * 
	 * 	START
	 * 
	 * DEVELOPMENT TO MODIFY MATRIX
	 * 
	 */
	/**
	 * *
	 * findFormulateAndSourceModify.
	 *
	 * @return the list
	 */
	public List<AccountingMatrixDetailTo> findFormulateAndSourceModify(){
		
	Collection<AccountingMatrixDetailTo> collectionListForModify = new ArrayList<AccountingMatrixDetailTo>();
	Boolean idChangesSource=Boolean.FALSE;
	
	/**		There are Changes 		**/
	if(Validations.validateIsNotNull(indModifyMatrixDetail) && indModifyMatrixDetail == 1){
	
		if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailForModify)){
			
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailForModifyOriginal)){
				
				for (AccountingMatrixDetailTo accountingMatrixDetailToModify : listAccountingMatrixDetailForModify) {
					for (AccountingMatrixDetailTo accountingMatrixDetailToTwo  :  listAccountingMatrixDetailForModifyOriginal) {
						/**
						 * Find the list modified 
						 * 
						 */
						if(accountingMatrixDetailToTwo.getIdAccountingMatrixDetailPk().equals(accountingMatrixDetailToModify.getIdAccountingMatrixDetailPk())){
							if( !(accountingMatrixDetailToTwo.getAccountingSourceTo().getIdAccountingSourcePk().
										equals(accountingMatrixDetailToModify.getAccountingSourceFk())) || 
								!(accountingMatrixDetailToTwo.getAccountingFormulateTo().getFormulateInput().
										equals(accountingMatrixDetailToModify.getAccountingFormulateTo().getFormulateInput())) || 
								!(accountingMatrixDetailToTwo.getState().
								  		equals(accountingMatrixDetailToModify.getState()))  ){
								
								collectionListForModify.add(accountingMatrixDetailToModify);
							}else{
								/**Only Source**/
								
								for (VariableFormulateTo variableFormulateTwo : accountingMatrixDetailToTwo.getListVariableFormulateTo()) {
									for (VariableFormulateTo variableFormulateToModify : accountingMatrixDetailToModify.getListVariableFormulateTo()) {
										if(variableFormulateTwo.getIdVariableFormulatePk().equals(variableFormulateToModify.getIdVariableFormulatePk())){
											String variableSourceModify =variableFormulateToModify.getVariableSource()==null ?"":variableFormulateToModify.getVariableSource() ;
											String variableSourceOriginal =variableFormulateTwo.getVariableSource()==null ?"":variableFormulateTwo.getVariableSource();
											BigDecimal valueDefaultModify =variableFormulateToModify.getValueDefault()==null ?BigDecimal.ZERO.setScale(2):variableFormulateToModify.getValueDefault().setScale(2);
											BigDecimal valueDefaultOriginal =variableFormulateTwo.getValueDefault()==null? BigDecimal.ZERO.setScale(2):variableFormulateTwo.getValueDefault().setScale(2) ;
											Integer positionSourceModify =variableFormulateToModify.getPositionSource()==null?0:variableFormulateToModify.getPositionSource() ;
											Integer positionSourceOriginal =variableFormulateTwo.getPositionSource()==null?00:variableFormulateTwo.getPositionSource() ;
											
						
											
											if(!(variableFormulateTwo.getSourceType().equals(variableFormulateToModify.getSourceType()))||
													!(variableSourceOriginal.equals(variableSourceModify))||
													!(valueDefaultOriginal.equals(valueDefaultModify))||
													!(positionSourceOriginal.equals(positionSourceModify))
													){
												 idChangesSource=Boolean.TRUE;
												break;
												
											}
											break;
										}
									}
								}
								
								if(idChangesSource){
									collectionListForModify.add(accountingMatrixDetailToModify);
								}
								
							}
							
							break ;
						}
						/**
						 * If you add one more to the list matrix
						 * **/
						else if(Validations.validateIsNull(accountingMatrixDetailToModify.getIdAccountingMatrixDetailPk())){
							collectionListForModify.add(accountingMatrixDetailToModify);
							break;
						}
					}
				}
			}
			/**
			 * If you do not have associated accounts, create new accounts
			 * **/
			else{
				collectionListForModify.addAll(listAccountingMatrixDetailForModify);
			}
			
		}
	}
 
		return (List<AccountingMatrixDetailTo>) collectionListForModify ;
	}
	
	
	/**
	 *  	Validate before Go to Modify Account's Matrix	*.
	 *
	 * @return the string
	 */ 
	@LoggerAuditWeb
	public String gotoModifyAccount(){

		goToAddAccountOnSchema = Boolean.FALSE;
		goToModifyMatrixDetailOnSchema = Boolean.TRUE;
		accountingAccountTo = new AccountingAccountTo(); 
		accountingAccountTo = new AccountingAccountTo();
		indModifyMatrixDetail = 0;

		String gotoModifyAccount = null;
		
		if (Validations.validateIsNotNull(this.accountingSchemaSelection)) {

			Object code = accountingSchemaSelection.getSchemaCode();

			if (accountingSchemaSelection.getStatus().equals(AccountingSchemaStateType.CANCELED.getCode())) {
				showMessageOnDialog( PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SCHEMA_CANCELED, code));
				JSFUtilities .executeJavascriptFunction("PF('cnfWDialogMessage').show();");
				return gotoModifyAccount;
			}
			if (accountingSchemaSelection.getStatus().equals(AccountingSchemaStateType.LOCK.getCode())) { 
				showMessageOnDialog( PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SCHEMA_LOCK, code));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
				return gotoModifyAccount;
			}
			
			
			/**		Load accountingSchemaSelection  	**/
			AccountingSchemaTo accountingSchemaToTemp = new AccountingSchemaTo();
			accountingSchemaToTemp.setSchemaCode(this.accountingSchemaSelection.getSchemaCode());
			accountingSchemaToTemp.setDescriptionParameterStatus(this.accountingSchemaSelection.getDescriptionParameterStatus());
			accountingSchemaToTemp.setDescription(this.accountingSchemaSelection.getDescription());
			accountingSchemaToTemp.setAuxiliaryType(this.accountingSchemaSelection.getAuxiliaryType());
			accountingSchemaToTemp.setBranchType(this.accountingSchemaSelection.getBranchType());
			accountingSchemaToTemp.setRegisterDate(this.accountingSchemaSelection.getRegisterDate());
			accountingSchemaToTemp.setRegisterUser(this.accountingSchemaSelection.getRegisterUser());
			accountingSchemaToTemp.setModifyUser(this.accountingSchemaSelection.getUpdateUser()); 
			accountingSchemaToTemp.setModifyDate(this.accountingSchemaSelection.getUpdateDate());	
			accountingSchemaToTemp.setStatus(this.accountingSchemaSelection.getStatus());
			this.accountingMatrixTo.setAccountingSchemaTo(accountingSchemaToTemp);
			
			// Validate if the schema have any schema  
			if (accountingSchemaSelection.getStatus().equals(AccountingSchemaStateType.REGISTERED.getCode())) {
				//Search all Schema's matrix
				listAccountingMatrixDetailForModify = new ArrayList<AccountingMatrixDetailTo>();
			
				/**		search all matrix detail with formulate, source and state		**/
				listAccountingMatrixDetailForModify 	   =  schemaAccountingServiceFacade.findAccountingMatrixDetailBySchemaFilter(this.accountingSchemaSelection);
				listAccountingMatrixDetailForModifyOriginal  =  schemaAccountingServiceFacade.findAccountingMatrixDetailBySchemaFilter(this.accountingSchemaSelection);	
				/***
				 * Set Auxiliary Daily
				 */
				parameterAuxiliaryTypeSelected=accountingSchemaSelection.getAuxiliaryType();

					accountingMatrixDetailDataModel = new GenericDataModel<AccountingMatrixDetailTo>(listAccountingMatrixDetailForModify);
					gotoModifyAccount = "schemaModify";

				return gotoModifyAccount;	 
			}

		}
		else {
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PARAMETER_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
			
			return gotoModifyAccount;
		}
		
		return gotoModifyAccount;
	}

	 
	/**
	 * viewFormulate*.
	 *
	 * @param event the event
	 */
	public void beforeViewFormulateCompomentOnModify(ActionEvent event) {
		
		if (event != null) {
			accountingMatrixDetailSelection = (AccountingMatrixDetailTo) event.getComponent().getAttributes().get("toFormulate");
			
			if(Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailSelection.getAccountingSourceTo())) {
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DELETE_SOURCE_ON_MATRIX,""));			
				JSFUtilities.executeJavascriptFunction("cnfWConfirmCancelSource.show();");
			}else
				viewFormulateCompomentOnModify();
		}
	}
	
	/**
	 * View formulate compoment on modify.
	 */
	@LoggerAuditWeb
	public void viewFormulateCompomentOnModify(){
		
		// Delete the AccountingSource in the MatrixDetail
		accountingMatrixDetailSelection.setAccountingSourceTo(null);

		List<AccountingParameter> listParameterOperators;
		List<AccountingParameter> listParameterOperatorToTemp;
		
		AccountingMatrix accountingMatrix = new AccountingMatrix();
		accountingMatrix.setIdAccountingMatrixPk(accountingMatrixDetailSelection.getAccountingMatrixFk());
		 
		List<AccountingMatrixDetail> listAccountingMatrixs = schemaAccountingServiceFacade.findFormulateAndSourceByAccountingMatrixDetail(accountingMatrixDetailSelection);
		loadListImputToFormula();
		if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixs)){
			if(Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailSelection.getListAccountingParameterTO()) &&
					Validations.validateIsNotNullAndPositive(accountingMatrixDetailSelection.getListAccountingParameterTO().size())){

				formulaFinal =  accountingMatrixDetailSelection.getListAccountingParameterTO() ;
				stringFinalFormula=accountingMatrixDetailSelection.getAccountingFormulateTo().getFormulateInput();
			
		}else{
				
				stringFinalFormula = listAccountingMatrixs.get(0).getAccountingFormulate().getFormulateInput();
				
			}
		}
		
		JSFUtilities.executeJavascriptFunction("cnIdFlagAddFormulate.show()");
	}
	
	
	/**
	 * CLEAN FORMULATE*.
	 */
	public void cleanFormulateModify(){
		setAcceptFormulate(Boolean.FALSE);
		
		formulaFinal = new ArrayList<AccountingParameterTO>();
		stringFinalFormula = null;
		
	}
	
	/**
	 * Before modify accounting accounts.
	 */
	@LoggerAuditWeb
	public void beforeModifyAccountingAccounts() {
	
		/**
		 * 	 Find list modify
		 * 	 Get the list modify for SAVE
		 **/
		   listAccountingMatrixDetailToModify = findFormulateAndSourceModify(); 
		   
		/**
		 * Verify if all matrices are formulated and detailed accounts
		 * **/
		Boolean prepareDetailMatrix=Boolean.TRUE;
		for (AccountingMatrixDetailTo accountingMatrixDetailTo : listAccountingMatrixDetailToModify) {
			
			if (!accountingMatrixDetailTo.getPrepareForSave()){
				prepareDetailMatrix=Boolean.FALSE;
				break;
			}
		}
		
		/**IT´S OK**/
		if(prepareDetailMatrix){
			
			/**	 When indicate of modify is = 1		**/
			if ( Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailToModify) && 
				   Validations.validateIsNotNullAndPositive(listAccountingMatrixDetailToModify.size()) &&
					 Validations.validateIsNotNull(indModifyMatrixDetail) && indModifyMatrixDetail == 1 ){
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_MODIFY_MATRIX_DETAIL,""));			
				JSFUtilities.executeJavascriptFunction("cnfWConfirmModifyMatrixDetail.show();");
			} 
			else{
				/***
				 * Exists Only Changes in schema
				 */
				if(beforeModifySchema()){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
							PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_MODIFY_SCHEMA, accountingSchemaSelection.getSchemaCode()));
				JSFUtilities.executeJavascriptFunction("cnfWConfirmModify.show();");
				}
				else{
					/***
					 * Not changes
					 */
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MODIFY_SCHEMA, ""));
					JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
				}
			}
		}else{
			/***
			 * Missing formula or source
			 */
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_MISSING_FORMULA_OR_SOURCE));
			JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
		}
		 
	}
	
	
	/**
	 * Modify matrix detail on account.
	 */
	@LoggerAuditWeb
	public void modifyMatrixDetailOnAccount(){   
		
		String stringAccountCode = null ;
		String stringSchemaCode  = this.accountingSchemaSelection.getSchemaCode();   
		
		/**		Change Matrix State to Delete		**/
		this.accountingMatrixTo.setAccountingSchemaFk(this.accountingSchemaSelection.getIdAccountingSchemaPk());  
		
		
		/** 
		 * 	listAccountingMatrixDetailToModify is
		 *  the list for Save because has 
		 *  all the modifications
		 **/
		try {
			
			schemaAccountingServiceFacade.updateAccountingShema(accountingSchemaSelection);
			/**
			 * Update Matrix
			 * **/
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailForModifyOriginal)){
				schemaAccountingServiceFacade.modifyAccountingMatrixDetail(listAccountingMatrixDetailForModifyOriginal , listAccountingMatrixDetailToModify);
			}
			/**
			 * Create new AccountingMatrix
			 * 
			 * */
			else{
				
				// When is new - 1st time to save matrix
				this.accountingMatrixTo.setAccountingSchemaFk(this.accountingSchemaSelection.getIdAccountingSchemaPk());
				this.accountingMatrixTo.setAuxiliaryType(this.accountingSchemaSelection.getAuxiliaryType());
				this.accountingMatrixTo.setStatus(AccountingMatrixStateType.REGISTERED.getCode());
				this.accountingMatrixTo.setAccountingMatrixDetail(listAccountingMatrixDetailToModify);
				
				accountingMatrixServiceFacade.saveAccountingMatrix(this.accountingMatrixTo);
			}
			
		
		} catch (ServiceException e) {
			e.printStackTrace();
		} 
		
		log.info("Matrix Detail Modified");
		
		/** Account Code's from the List modified	**/
 		stringAccountCode =  AccountingUtils.convertListObjectToString(AccountingUtils.convertListAccountingAccountMatrixDetailObject(listAccountingMatrixDetailToModify));

		
		
		showMessageOnDialog(
				PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_MODIFY_MATRIX_DETAIL, stringAccountCode , stringSchemaCode));
		JSFUtilities.executeJavascriptFunction("cnfWSuccessDialogModify.show();");
	}
	
	/**
	 * Clear after modify matrix.
	 */
	public void clearAfterModifyMatrix(){
		// Clean Data Model
		if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixDetailForModify)){
			listAccountingMatrixDetailForModify.clear();
			accountingMatrixDetailDataModel = null;
			formulaFinal.clear();
		}
	}
	
	/**
	 * Modify account back to search.
	 *
	 * @return the string
	 */
	public String modifyAccountBackToSearch() {
		
		String modifyAccountBackToSearch = null;

		if(Validations.validateIsNotNull(indModifyMatrixDetail) && indModifyMatrixDetail == 1){

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WARNING_MODIFY_ACCOUNT_BACK,""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmBackModify.show();");   
			return modifyAccountBackToSearch ;
		}  
		else {
			modifyAccountBackToSearch = "schemaSearch";
		}

		return modifyAccountBackToSearch ;
	}
	
	
	/**
	 *  DELETE ON MODIFY ACOUNTING ON SCHEMA	*.
	 *
	 * @param event the event
	 */
	public void beforeDeleteAccountingAccountOnMatrix(ActionEvent event) {
		
		accountingMatrixDetailDataForModify = (AccountingMatrixDetailTo) event.getComponent().getAttributes().get("removeName");  
		
		/**It's memory**/
		if(Validations.validateIsNull(accountingMatrixDetailDataForModify.getIdAccountingMatrixDetailPk())){
			listAccountingMatrixDetailForModify.remove(accountingMatrixDetailDataForModify);

			log.info("matrix memoria");
			return;
		}
		showMessageOnDialog(
				PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM ),
				PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DELETE_ACCOUNT_ON_MATRIX, "" ));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDeleteMatrixDetail.show()");
	}
	
	
	/**
	 * Delete accounting account on matrix.
	 */
	public void deleteAccountingAccountOnMatrix(){

		for(AccountingMatrixDetailTo accountingMatrixDetailToTemp : listAccountingMatrixDetailForModify){
			if(accountingMatrixDetailToTemp.getIdAccountingMatrixDetailPk().equals(accountingMatrixDetailDataForModify.getIdAccountingMatrixDetailPk())){
				
				accountingMatrixDetailToTemp.setState(AccountingMatrixDetailStateType.CANCELED.getCode());
				break;
			}
		}
		
		indModifyMatrixDetail = 1;
		log.info("matrix eliminada");
	}
 
	
	/**
	 * Modify File xml.
	 * the Accounting Matrix Detail 
	 *
	 * @param event the event
	 */
	public void modifyFileXML(ActionEvent event) {
		
		AccountingSourceInfo accountingSourceInfo = null;
		List<FieldSourceTo> listFieldSourceTo = new ArrayList<FieldSourceTo>();
		//Clean File Source Name
		fileSourceName="";
		fileDescription="";
		byte[]asx = null ;
		
		if (event != null) {
			accountingMatrixDetailSelection = (AccountingMatrixDetailTo) event.getComponent().getAttributes().get("toSource");
			
			if(Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailSelection.getAccountingSourceTo())){
				asx = accountingMatrixDetailSelection.getAccountingSourceTo().getFileXml();
			}
				
			
			if(Validations.validateIsNotNullAndNotEmpty(asx)){
				try {
					accountingSourceInfo = AccountingUtils.loadXMLToObjectSource(asx);
				} catch (ServiceException e) {
					
					/**If the structure is wrong produced**/
					if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_WRONG_STRUCTURED)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_WRONG_STRUCTURED, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}/**If Accounting Source Value Format**/
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_VALUE_FORMAT)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_VALUE_FORMAT, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}/**If Wrong visibility format**/
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_VISIBILITY_FORMAT)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_VISIBILITY_FORMAT, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}/**If Position format wrong**/
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_POSITION_FORMAT)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_POSITION_FORMAT, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}/**If Accounting Source Type Format**/
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_TYPE_FORMAT)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_FORMAT, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}
					else if(e.getErrorService().equals(ErrorServiceType.ACCOUNTING_SOURCE_TYPE_ASSOCIATE)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_ASSOCIATE, ""));
						JSFUtilities.executeJavascriptFunction("warningDialogSource.show(); initDND();");
						return;
					}
					
					
					e.printStackTrace();
				}
			}
				
			
			if (accountingSourceInfo != null) {
				for (Integer key : accountingSourceInfo.getFields().keySet()) {
					FieldSourceTo fieldSourceTo = accountingSourceInfo.getFields().get(key);
					if (fieldSourceTo.getVisibility()) {
						listFieldSourceTo.add(fieldSourceTo);
					}
				}
				fieldSourceToDataModel = new GenericDataModel<FieldSourceTo>(listFieldSourceTo);
				associatedVariable = Boolean.TRUE;
			}else if (Validations.validateIsNullOrEmpty(accountingSourceInfo)) {
				
				fieldSourceToDataModel = null;
				associatedVariable = Boolean.FALSE;
				
				if(Validations.validateListIsNotNullAndNotEmpty(listVariableFormulateTo) && goToAddAccountOnSchema){
						listVariableFormulateTo.clear();
					 
				}
			}
			
		}
			
		JSFUtilities.executeJavascriptFunction("cnIdFlagAddFormulate.show()");	
	}
	
	
	/**
	 * Adds the formulate to matrix detail modify.
	 */
	public void addFormulateToMatrixDetailModify(){
		
		addFormulateToMatrixDetail();
		accountingMatrixDetailSelection.setPrepareForSave(Boolean.FALSE);
		
		indModifyMatrixDetail = 1;
		
	}
	
	
	
	/**
	 * Cancel from formulate on modify.
	 */
	public void cancelFromFormulateOnModify() {
		
		if(	( Validations.validateIsNotNullAndNotEmpty(formulaFinal) && Validations.validateIsNotNullAndPositive(formulaFinal.size()) ) ||
		    ( Validations.validateIsNotNullAndNotEmpty(formulaFinal)  && 
		    		Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailSelection.getListAccountingParameterTO()) && 
		    		Validations.validateIsNotNullAndPositive(accountingMatrixDetailSelection.getListAccountingParameterTO().size()) ) ){
		
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WARNING_BACK_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWAlertCancelFormulate.show(); initDND();");
		}
		else{
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WARNING_NOT_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWAlertCancel.show();  initDND();");
		}
	}
	
	
	/**
	 * Close modal formulate.
	 */
	public void closeModalFormulate() {
		 
		JSFUtilities.executeJavascriptFunction("cnIdFlagAddFormulate.hide();"); 
	}	 
	
	/**
	 * *.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String goToSourceFromMatrixModify(){
		
		Parser parser = new Parser();

		/**Si la matriz tiene asociada una formula**/
		if(Validations.validateIsNotNull(accountingMatrixDetailSelection)&&
				Validations.validateIsNotNull(accountingMatrixDetailSelection.getAccountingFormulateTo())){
		
			accountingSourceDataModel = new GenericDataModel<AccountingSource>();
			/***
			 * Set Auxiliary Daily
			 */
			parameterAuxiliaryTypeSelected=accountingSchemaSelection.getAuxiliaryType();
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingSources)){

				listAccountingSources.clear();
			}
			
			accountingSourceDataModel=null;
			
			List<AccountingMatrixDetail> listAccountingMatrixs = schemaAccountingServiceFacade.findFormulateAndSourceByAccountingMatrixDetail(accountingMatrixDetailSelection);

			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixs)){
			
				formulaFinal =  accountingMatrixDetailSelection.getListAccountingParameterTO() ;
     			stringFinalFormula=accountingMatrixDetailSelection.getAccountingFormulateTo().getFormulateInput();
     			listVariableFormulateTo =  parser.verifyAbsolutePosition(stringFinalFormula);
			}
			
			
			return "accountingSourceMgmtUpload";
			
		}
		else{
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NO_FORMULATE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWAlertModifyDialog.show();");
			return "";
		}
				
	}


	
	/**
	 * 		END
	 * 
	 * DEVELOPMENT TO MODIFY MATRICES.
	 *
	 * @param event the event
	 */

	
	
	/**Method By Helper Accounting Account**/
	public void updateAccountNature(ActionEvent event){
		if(Validations.validateIsNotNullAndNotEmpty(accountingAccountTo)){
			/***
			 * get AccountingByHelper
			 */
		}
		
	}
	
	/**
	 * 	Method By Helper Accounting Account.
	 */
	public void updateNatureTypeOfAccount(){
		if(Validations.validateIsNotNullAndNotEmpty(accountingAccountTo)){
			/***
			 * Get info by Helper
			 */
		}	
	}

	
	/**
	 * On node expand.
	 *
	 * @param event the event
	 */
	public void onNodeExpand(NodeExpandEvent event) {  
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Expanded", event.getTreeNode().toString());  
  
        FacesContext.getCurrentInstance().addMessage(null, message);  
    }  
  
    /**
     * On node collapse.
     *
     * @param event the event
     */
    public void onNodeCollapse(NodeCollapseEvent event) {  
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Collapsed", event.getTreeNode().toString());  
  
        FacesContext.getCurrentInstance().addMessage(null, message);  
    }  
  
    /**
     * On node select.
     *
     * @param event the event
     */
    public void onNodeSelect(NodeSelectEvent event) {  
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected", event.getTreeNode().toString());  
  
        FacesContext.getCurrentInstance().addMessage(null, message);  
    }  
  
    /**
     * On node unselect.
     *
     * @param event the event
     */
    public void onNodeUnselect(NodeUnselectEvent event) {  
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unselected", event.getTreeNode().toString());  
  
        FacesContext.getCurrentInstance().addMessage(null, message);  
    }
    
    /**
     * *
     * Assembling the structure of the accounts
     * consultAccountTree.
     *
     * @param event the event
     */
    public void consultAccountTree(ActionEvent event) {
		
		/**Arming the structure of the tree**/
		if (event != null) {
			
			AccountingMatrixDetailTo accountingMatrixDetailTo=(AccountingMatrixDetailTo) event.getComponent().getAttributes().get("blockInformation");
			root=new DefaultTreeNode("", null);
			TreeNode nodeFather=new DefaultTreeNode();
			TreeNode nodeSon=new DefaultTreeNode();
			int i=0;
			if(Validations.validateListIsNotNullAndNotEmpty(accountingMatrixDetailTo.getListAccountingAccountTree())){
				for (AccountingAccount accountingAccount : accountingMatrixDetailTo.getListAccountingAccountTree()) {
					if(i==0){
						nodeFather=root;
					}
					else{
						nodeFather=nodeSon;
					}
					
					nodeSon= new DefaultTreeNode(accountingAccount.getAccountCode(), nodeFather);
					i++;
				}
				JSFUtilities.executeJavascriptFunction("cnfWConsultTemp.show()");
			}
			
			
		}

		
	}

    /**
     * Sets the true valid buttons.
     */
    public void setTrueValidButtons(){

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);    
		}
		if(userPrivilege.getUserAcctions().isBlock()){
			privilegeComponent.setBtnBlockView(true);
		}
		if(userPrivilege.getUserAcctions().isUnblock()){
			privilegeComponent.setBtnUnblockView(true);
		}
		if(userPrivilege.getUserAcctions().isCancel()){
			privilegeComponent.setBtnCancel(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent); 

	}

	

}