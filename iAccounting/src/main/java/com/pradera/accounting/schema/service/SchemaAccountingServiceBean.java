package com.pradera.accounting.schema.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.accounting.document.to.AccountingProcessReStartTO;
import com.pradera.accounting.document.to.AccountingProcessSchemaTo;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.matrix.to.AccountingMatrixDetailTo;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounting.account.to.AccountingSourceTo;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingMatrixDetail;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.AccountingSource;
import com.pradera.model.accounting.type.AccountingMatrixDetailStateType;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SchemaAccountingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SchemaAccountingServiceBean extends CrudDaoServiceBean{ 
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	

	/**
	 * *
	 * 
	 * 		Begin  Section Accounting Schema.
	 *
	 * @param schemaAccounting the schema accounting
	 */
	
	
	/**
	 * @param parameterAccounting
	 *  Method for save one entity
	 */	
	public void saveAccountingSchema(Object schemaAccounting){
		create(schemaAccounting);
	}
		
	
	/**
	 * Search schema accounting.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingSchema> searchSchemaAccounting(AccountingSchemaTo filter){
		
		StringBuilder stringBuilderSql = new StringBuilder();

		stringBuilderSql.append(" Select new AccountingSchema(acs.idAccountingSchemaPk, ")
						.append("  acs.schemaCode, acs.description, ")
						.append("  acs.branchType, acs.status, ")
						.append("  acs.auxiliaryType, acs.associateCode, ")
						.append("  (select ap.parameterName from AccountingParameter ap where ap.idAccountingParameterPk = acs.branchType), ")
						.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = acs.status) ,")
						.append("  (select ap.description from AccountingParameter ap where ap.idAccountingParameterPk = acs.associateCode) )")
						.append(" from AccountingSchema acs")
						.append(" where 1=1");
		
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			stringBuilderSql.append(" AND acs.status = :parameterStatus  ");
		}
		
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getSchemaCode()) ){
			stringBuilderSql.append(" AND acs.schemaCode = :shemaCode    ");
		}
		
		if ( Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType()) ){
			stringBuilderSql.append(" AND acs.auxiliaryType = :auxiliaryType    ");
		}
		
		if ( Validations.validateIsNotNullAndPositive(filter.getStartType()) ){
			stringBuilderSql.append(" AND acs.startType = :startType    ");
		}
		
		stringBuilderSql.append(" ORDER BY acs.idAccountingSchemaPk ");
		
		
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			query.setParameter("parameterStatus", filter.getStatus()	);
		}
		
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getSchemaCode()) ){
			query.setParameter("shemaCode", filter.getSchemaCode()	);
		}
		
		if ( Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType()) ){
			query.setParameter("auxiliaryType", filter.getAuxiliaryType()	);
		}
		
		if ( Validations.validateIsNotNullAndPositive(filter.getStartType()) ){
			query.setParameter("startType", filter.getStartType()	);
		}
		
		
		List<AccountingSchema> accountingMatrixs = query.getResultList();

		return accountingMatrixs;

	} 
	
	/**
	 * Find schema accounting by id.
	 *
	 * @param id the id
	 * @return the accounting schema
	 */
	public AccountingSchema findSchemaAccountingById(Long  id){
		
		return null;
	}
	
	
	/**
	 * 		find accounts in matrix by schema	*.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingMatrix>  findAccountingMatrixDetailBySchemaFilter(AccountingSchema filter){

		StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select distinct am From AccountingMatrix am  " );
        sbQuery.append(" left join fetch am.accountingMatrixDetail amd   " );
        sbQuery.append(" left join fetch amd.accountingAccount " );
        sbQuery.append(" left join fetch amd.accountingFormulate acf " );
        
        sbQuery.append("  " );
        sbQuery.append("  " );
        sbQuery.append(" where 1=1 and amd.status =  "+AccountingMatrixDetailStateType.REGISTERED.getCode() );
        
        
        if(Validations.validateIsNotNull(filter.getIdAccountingSchemaPk())){
            sbQuery.append(" and  am.accountingSchema.idAccountingSchemaPk = :idAccountingSchemaPk ");
        }
        
        Query query = em.createQuery(sbQuery.toString());
       
        if(Validations.validateIsNotNull(filter.getIdAccountingSchemaPk())){
            query.setParameter("idAccountingSchemaPk", filter.getIdAccountingSchemaPk());
        }
        
        List<AccountingMatrix> accountingMatrixs = null;
        accountingMatrixs = query.getResultList();
		
		
		return accountingMatrixs;
		
	}
	
	
	/**
	 * 		find formulate and source ByAccountingMatrixDetail	*.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingMatrixDetail>  findFormulateAndSourceByAccountingMatrixDetail(AccountingMatrixDetailTo filter){

		StringBuilder sbQuery = new StringBuilder(); 
        sbQuery.append(" Select distinct amd From AccountingMatrixDetail amd left join fetch amd.accountingSource left join fetch amd.accountingFormulate " );
        sbQuery.append(" where 1=1 " );
        
        if(Validations.validateIsNotNull(filter.getAccountingAccountTo().getIdAccountingAccountPk())){
            sbQuery.append(" and  amd.accountingAccount.idAccountingAccountPk  = :idAccountingAccountPk  ");
        }
        
        Query query = em.createQuery(sbQuery.toString());
       
        if(Validations.validateIsNotNull(filter.getAccountingAccountTo().getIdAccountingAccountPk())){
            query.setParameter("idAccountingAccountPk", filter.getAccountingAccountTo().getIdAccountingAccountPk());
        }
        
        List<AccountingMatrixDetail> accountingMatrixDetails = null;
        accountingMatrixDetails = query.getResultList();
		
		
		return accountingMatrixDetails;
		
	}
	
	
	
	/**
	 * *
	 * 
	 * 		End Section Accounting Schema.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	
	
	
	
	
	
	/***
	 * 
	 * 		Begin Section Accounting Source
	 */
	
	
	public List<AccountingSource> searchAllAccountingSource(AccountingSourceTo filter){
		
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT ID_ACCOUNTING_SOURCE_PK, FILE_SOURCE_NAME, DESCRIPTION_SOURCE  ");
		stringBuilderSql.append(" FROM   ACCOUNTING_SOURCE    ");  
		
		if ( Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType()) ){
			stringBuilderSql.append(" WHERE  AUXILIARY_TYPE = :auxiliaryType    ");
		}
		
		Query query = em.createNativeQuery(stringBuilderSql.toString());
		
		if ( Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType()) ){
			query.setParameter("auxiliaryType", filter.getAuxiliaryType()	);
		}
		
		List<Object[]> objectList = query.getResultList();
		
		List<AccountingSource> listAccountingSource = new ArrayList<AccountingSource>();
		
		for (int i=0; i<objectList.size();++i) {
			Object[] sResults = (Object[])objectList.get(i);
			AccountingSource accountingSource = new AccountingSource();
			
			accountingSource.setIdAccountingSourcePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));
			accountingSource.setFileSourceName(sResults[1]==null?"":sResults[1].toString());
			accountingSource.setDescriptionSource(sResults[2]==null?"":sResults[2].toString());
			//accountingSource.setStatus(sResults[4]==null?null:Integer.parseInt(sResults[4].toString())); 
			
			listAccountingSource.add(accountingSource);
		}
		
		
		return listAccountingSource;
	} 
	
	/**
	 * Find accounting source file by code.
	 *
	 * @param code the code
	 * @return the accounting source
	 */
	public AccountingSource findAccountingSourceFileByCode(Long code){
		AccountingSource accountingSource;
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT acs FROM AccountingSource acs ");
		stringBuilderSql.append("  WHERE acs.idAccountingSourcePk="+code);
		
		Query query = em.createQuery(stringBuilderSql.toString());
		try{
			accountingSource=(AccountingSource) query.getSingleResult();
		}
		catch(Exception e){
			accountingSource=null;
			e.printStackTrace();
		}
		
		return accountingSource;
	}


	
	/***
	 * 
	 * 		End Section Accounting Source
	 */
	
	
	/**
	 * Process Schema
	 */

	@SuppressWarnings("unchecked")
	public List<AccountingProcessSchemaTo> searchAccountingProcessSchema(AccountingProcessReStartTO filter){
		
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT     ");
		stringBuilderSql.append(" aps.idAccountingProcessSchemaPk,    ");
		stringBuilderSql.append(" aps.accountingProcess.idAccountingProcessPk,    ");
		stringBuilderSql.append(" aps.accountingSchema.idAccountingSchemaPk, ");
		stringBuilderSql.append(" aps.accountingSchema.description, ");
		stringBuilderSql.append(" aps.status ");
		stringBuilderSql.append(" FROM   AccountingProcessSchema aps    ");  
		
		if ( Validations.validateIsNotNullAndPositive(filter.getId()) ){
			stringBuilderSql.append(" WHERE  aps.accountingProcess.idAccountingProcessPk = :idAccountingProcessPk    ");
		}
		
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if ( Validations.validateIsNotNullAndPositive(filter.getId()) ){
			query.setParameter("idAccountingProcessPk", filter.getId()	);
		}
		
		List<Object[]> objectList = query.getResultList();
		
		List<AccountingProcessSchemaTo> listAccountingSource = new ArrayList<AccountingProcessSchemaTo>();
		for (int i=0; i<objectList.size();++i) {
			ParameterTable descriptionStatus = null;
			AccountingSchemaTo accountingSchemaTo = null;
			
			Object[] sResults = (Object[])objectList.get(i);
			AccountingProcessSchemaTo accountingProcessSchema = new AccountingProcessSchemaTo();
			accountingProcessSchema.setIdAccountingProcessSchemaPk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));
			AccountingProcessTo accountingProcessTo=new AccountingProcessTo();
			accountingProcessTo.setIdAccountingProcessPk(Long.parseLong(sResults[1]==null?"":sResults[1].toString()));
			accountingSchemaTo=new AccountingSchemaTo();
			accountingSchemaTo.setIdAccountingSchemaPk(Long.parseLong(sResults[2]==null?"":sResults[2].toString()));
			accountingSchemaTo.setDescription(sResults[3]==null?"":sResults[3].toString());
			accountingProcessSchema.setStatus(sResults[4]==null?null:Integer.parseInt(sResults[4].toString()));
			descriptionStatus = em.find(ParameterTable.class,accountingProcessSchema.getStatus());
			accountingProcessSchema.setDescriptionStatus(descriptionStatus.getParameterName());
			accountingProcessSchema.setAccountingSchema(accountingSchemaTo);
			accountingProcessSchema.setAccountingProcess(accountingProcessTo);
			listAccountingSource.add(accountingProcessSchema);
		}
		
		
		return listAccountingSource;
	} 
	/**
	 * 
	 */

}
