package com.pradera.accounting.schema.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounting.account.facade.AccountAccountingServiceFacade;
import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.common.utils.SequenceGeneratorCode;
import com.pradera.accounting.document.to.AccountingProcessReStartTO;
import com.pradera.accounting.document.to.AccountingProcessSchemaTo;
import com.pradera.accounting.formulate.to.VariableFormulateTo;
import com.pradera.accounting.matrix.facade.AccountingMatrixServiceFacade;
import com.pradera.accounting.matrix.service.AccountinMatrixServiceBean;
import com.pradera.accounting.matrix.to.AccountingMatrixDetailTo;
import com.pradera.accounting.matrix.to.AccountingMatrixTo;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.schema.service.SchemaAccountingServiceBean;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.accounting.source.facade.SourceAccountingServiceFacade;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounting.account.to.AccountingFormulateTo;
import com.pradera.core.component.accounting.account.to.AccountingSourceTo;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingFormulate;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingMatrixDetail;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcessSchema;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.AccountingSource;
import com.pradera.model.accounting.VariableFormulate;
import com.pradera.model.accounting.type.AccountingFieldSourceType;
import com.pradera.model.accounting.type.AccountingMatrixDetailStateType;
import com.pradera.model.accounting.type.AccountingMatrixStateType;
import com.pradera.model.accounting.type.AccountingSchemaStateType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SchemaAccountingServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SchemaAccountingServiceFacade {  

	/** The user info. */
	@Inject
	UserInfo userInfo;	
	
	/** The schema accounting service bean. */
	@EJB
	SchemaAccountingServiceBean schemaAccountingServiceBean;
	
	/** The accounting matrix service facade. */
	@EJB
	AccountingMatrixServiceFacade accountingMatrixServiceFacade;
	
	/** The parameter accounting service facade. */
	@EJB
	ParameterAccountingServiceFacade parameterAccountingServiceFacade;
	
	/** The source accounting service facade. */
	@EJB
	SourceAccountingServiceFacade sourceAccountingServiceFacade;
	
	/** The accountin matrix service bean. */
	@EJB
    AccountinMatrixServiceBean 		accountinMatrixServiceBean;
	
	/** The account accounting service facade. */
	@EJB
	AccountAccountingServiceFacade accountAccountingServiceFacade;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sequence generator code. */
	@EJB
	SequenceGeneratorCode  				sequenceGeneratorCode;
	

	/**
	 * Save accounting schema.
	 *
	 * @param accountingSchema the accounting schema
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void saveAccountingSchema(AccountingSchema accountingSchema){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		accountingSchema.setRegisterUser(loggerUser.getUserName());
		accountingSchema.setRegisterDate(loggerUser.getAuditTime());			
		
		schemaAccountingServiceBean.saveAccountingSchema(accountingSchema);
			 
	}
	
	 
	/**
	 * Search accounting schema.
	 *
	 * @param parameter the parameter
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingSchema> searchAccountingSchema(AccountingSchemaTo parameter){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingSchema> listAccountingSchema = new ArrayList<AccountingSchema>();
		
		listAccountingSchema = schemaAccountingServiceBean.searchSchemaAccounting(parameter); 
		
		return listAccountingSchema;
	}
	
	
	/**
	 * Find accounting matrix detail by schema filter.
	 *
	 * @param accountingSchema the accounting schema
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingMatrixDetailTo> findAccountingMatrixDetailBySchemaFilter(AccountingSchema accountingSchema){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingMatrix> listAccountingMatrixs  = schemaAccountingServiceBean.
																		findAccountingMatrixDetailBySchemaFilter(accountingSchema); 
		
		List<AccountingMatrixDetailTo> listAccountingMatrixDetailTos = new ArrayList<AccountingMatrixDetailTo>();

		AccountingMatrixDetailTo  accountingMatrixDetailTo	=  null ;
		AccountingAccountTo       accountingAccountTo 		=  null ;
		AccountingSourceTo		  accountingSourceTo		=  null ;
		Long                      accountingSourcePk		=  null ;
		AccountingFormulateTo	  accountingFormulateTo	    =  null ;
		
		for(AccountingMatrix accountingMatrixTemp : listAccountingMatrixs){
			for(AccountingMatrixDetail accountingMatrixDetailTemp : listAccountingMatrixs.get(0).getAccountingMatrixDetail()){
				
				accountingAccountTo 		=  	new  AccountingAccountTo();
				accountingMatrixDetailTo	=	new  AccountingMatrixDetailTo();
				
				accountingSourcePk			=   null ;
				accountingFormulateTo       =   new  AccountingFormulateTo();
				
				/** set accountingAccountTo **/
				accountingAccountTo.setAccountCode(accountingMatrixDetailTemp.getAccountingAccount().getAccountCode());
				accountingAccountTo.setDescription(accountingMatrixDetailTemp.getAccountingAccount().getDescription());
				accountingAccountTo.setStatus(accountingMatrixDetailTemp.getAccountingAccount().getStatus());
				accountingAccountTo.setIdAccountingAccountPk(accountingMatrixDetailTemp.getAccountingAccount().getIdAccountingAccountPk());
	
				accountingMatrixDetailTo.setAccountingAccountTo(accountingAccountTo);
				accountingMatrixDetailTo.setAccountingMatrixFk(accountingMatrixTemp.getIdAccountingMatrixPk());
				accountingMatrixDetailTo.setState(accountingMatrixDetailTemp.getStatus());
				accountingMatrixDetailTo.setIdAccountingMatrixDetailPk(accountingMatrixDetailTemp.getIdAccountingMatrixDetailPk());
				 
				/**Setting the Source**/
				if(Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailTemp.getAccountingSource())){
					accountingSourceTo		    =   new  AccountingSourceTo();
					accountingSourceTo.setIdAccountingSourcePk(accountingMatrixDetailTemp.getAccountingSource().getIdAccountingSourcePk());
					accountingSourcePk = accountingMatrixDetailTemp.getAccountingSource().getIdAccountingSourcePk();
					accountingSourceTo.setFileSourceName(accountingMatrixDetailTemp.getAccountingSource().getFileSourceName());
					accountingSourceTo.setStatus(accountingMatrixDetailTemp.getAccountingSource().getStatus());
					accountingSourceTo.setAuxiliaryType(accountingMatrixDetailTemp.getAccountingSource().getAuxiliaryType());
					accountingSourceTo.setDescriptionSource(accountingMatrixDetailTemp.getAccountingSource().getDescriptionSource());
					byte[] file = sourceAccountingServiceFacade.getAccountingSourceContent(accountingSourcePk);
					accountingSourceTo.setFileXml(file);
				}
				
				/** Setting the Formulate**/
				accountingFormulateTo.setIdAccountingFormulateTo(accountingMatrixDetailTemp.getAccountingFormulate().getIdAccountingFormulatePk());
				accountingFormulateTo.setFormulateInput(accountingMatrixDetailTemp.getAccountingFormulate().getFormulateInput());
				
				/**Setting List AccountingParameterTo**/
				accountingMatrixDetailTo.setListAccountingParameterTO(getListAccountingParameter(AccountingUtils.getNameAccountingParameter(accountingMatrixDetailTemp.getAccountingFormulate().getFormulateInput())));
				/**Setting List VariableFormulateTo**/
				accountingMatrixDetailTo.setListVariableFormulateTo(getListVariableFormulateTo(accountingMatrixDetailTemp.getAccountingFormulate().getVariableFormulates()));
				/**Setting List AccountingAccountTreeNode**/
				accountingMatrixDetailTo.setListAccountingAccountTree(accountAccountingServiceFacade.findAccountParentAsc(accountingAccountTo,new BigDecimal(1)));
				
				accountingMatrixDetailTo.setAccountingFormulateTo(accountingFormulateTo);
				accountingMatrixDetailTo.setAccountingSourceTo(accountingSourceTo);
				accountingMatrixDetailTo.setAccountingSourceFk(accountingSourceTo.getIdAccountingSourcePk());
				accountingMatrixDetailTo.setPrepareForSave(Boolean.TRUE);
				accountingMatrixDetailTo.setDynamicType(accountingMatrixDetailTemp.getDynamicType());
				
				listAccountingMatrixDetailTos.add(accountingMatrixDetailTo);
			}
		}
		
		return listAccountingMatrixDetailTos;
	}
	
	
	/**
	 * Find accounting matrix by filter.
	 *
	 * @param accountingSchema the accounting schema
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	// Find The accounts' matrices
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public List<AccountingMatrix> findAccountingMatrixByFilter(AccountingSchema accountingSchema) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		//Find accounts' matrices
		AccountingMatrixTo accountingMatrixTo = new AccountingMatrixTo();
		AccountingSchemaTo accountingSchemaTo = new AccountingSchemaTo();

		accountingSchemaTo.setIdAccountingSchemaPk(accountingSchema.getIdAccountingSchemaPk());
		accountingMatrixTo.setAccountingSchemaTo(accountingSchemaTo);
		
		List<AccountingMatrix> listAccountingMatrixs = accountingMatrixServiceFacade.findAccountingMatrixByParameter(accountingMatrixTo);
		
		return	listAccountingMatrixs;
	}
	
	
	/**
	 * Update accounting shema and matrix.
	 *
	 * @param accountingSchema the accounting schema
	 * @throws ServiceException the service exception
	 */
	//UPDATE_SCHEMA_WITH_MATRIX: The accounts' matrices
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateAccountingShemaAndMatrix(AccountingSchema accountingSchema) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		//Find accounts' matrices
		List<AccountingMatrix> listAccountingMatrixs = this.findAccountingMatrixByFilter(accountingSchema);
		
		//Update Matrix status
		
		if(accountingSchema.getStatus().equals(AccountingSchemaStateType.LOCK.getCode())){
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixs)){
				for(AccountingMatrix accountingMatrixTemp : listAccountingMatrixs){
					accountingMatrixTemp.setStatus(AccountingMatrixStateType.LOCK.getCode());
				}
			}
		}
		if(accountingSchema.getStatus().equals(AccountingSchemaStateType.REGISTERED.getCode())){
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixs)){
				for(AccountingMatrix accountingMatrixTemp : listAccountingMatrixs){
					accountingMatrixTemp.setStatus(AccountingMatrixStateType.REGISTERED.getCode());
				}
			}
		}
		if(accountingSchema.getStatus().equals(AccountingSchemaStateType.CANCELED.getCode())){
			if(Validations.validateListIsNotNullAndNotEmpty(listAccountingMatrixs)){
				for(AccountingMatrix accountingMatrixTemp : listAccountingMatrixs){
					accountingMatrixTemp.setStatus(AccountingMatrixStateType.CANCELED.getCode());
					
					// Cancel the matrix Detail 
					List<AccountingMatrixDetail> listAccountingMatrixDetail = listAccountingMatrixs.get(0).getAccountingMatrixDetail();  
					
					for(AccountingMatrixDetail accountingMatrixDetailTemp : listAccountingMatrixDetail){
						accountingMatrixDetailTemp.setStatus(AccountingMatrixDetailStateType.CANCELED.getCode());
					}
				}
			}
		}
		
		// Update schema
		updateAccountingShema(accountingSchema); 
		 
	}
	
	
	/**
	 * Update accounting shema.
	 *
	 * @param accountingSchema the accounting schema
	 * @throws ServiceException the service exception
	 */
	// UPDATE ACCOUNTING_SCHEMA
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateAccountingShema(AccountingSchema accountingSchema) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		
		if(accountingSchema.getStatus().equals(AccountingSchemaStateType.CANCELED.getCode())){
			
			accountingSchema.setCancelDate(loggerUser.getAuditTime());
			accountingSchema.setCancelUser(loggerUser.getUserName());
			
		}
			
		accountingSchema.setUpdateDate(loggerUser.getAuditTime());
		accountingSchema.setUpdateUser(loggerUser.getUserName());

		schemaAccountingServiceBean.update(accountingSchema);
	}
 
	
	
 
	/**
	 * Gets the accounting schema by pk.
	 *
	 * @param id the id
	 * @return the accounting schema by pk
	 */
	// Find  AccountingSchema by Pk
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingSchema getAccountingSchemaByPk(Long id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return schemaAccountingServiceBean.find(AccountingSchema.class, id);
	}

	
	/**
	 * Load accounting source.
	 *
	 * @param accountingTo the accounting to
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingSource> loadAccountingSource(AccountingSourceTo accountingTo){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingSource> listAccountingSource = new ArrayList<AccountingSource>();
		
		listAccountingSource = schemaAccountingServiceBean.searchAllAccountingSource(accountingTo); 
		
		return listAccountingSource;
	}
	
	
	
	
	
	/**
	 * Find accounting source file by code.
	 *
	 * @param code the code
	 * @return the accounting source
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingSource findAccountingSourceFileByCode(Long code){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		
		return schemaAccountingServiceBean.findAccountingSourceFileByCode(code);
	}
	
	
	
	/**
	 * Gets the report logger file content.
	 *
	 * @param idAccountingSourcePk the id accounting source pk
	 * @return the report logger file content
	 */
    public byte[] getAccountingSourceContent(Long idAccountingSourcePk) {
	   
	   Map<String, Object> parameters = new HashMap<String, Object>();
	   parameters.put("idAccountingSourcePk", idAccountingSourcePk);
	   return (byte[])schemaAccountingServiceBean.findObjectByNamedQuery(AccountingSource.ACCOUNTING_SOURCE_CONTENT_BY_PK, parameters);
    }
	  

	   
//////--------------------------------------------/////////
		
	/**
 * Update accouting shema.
 *
 * @param accountingSchema the accounting schema
 * @throws ServiceException the service exception
 */
// UPDATE ACCOUTING_SCHEMA
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateAccoutingShema(AccountingSchema accountingSchema) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		
		if(accountingSchema.getStatus().equals(AccountingSchemaStateType.CANCELED.getCode())){
			
			accountingSchema.setCancelDate(loggerUser.getAuditTime());
			accountingSchema.setCancelUser(loggerUser.getUserName());
			
		}
			
		accountingSchema.setUpdateDate(loggerUser.getAuditTime());
		accountingSchema.setUpdateUser(loggerUser.getUserName());

		schemaAccountingServiceBean.update(accountingSchema);
	}
	 
		
		
	 
	/**
	 * Gets the accouting schema by pk.
	 *
	 * @param id the id
	 * @return the accouting schema by pk
	 */
	// Find  AccoutingSchema by Pk
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public AccountingSchema getAccoutingSchemaByPk(Long id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return schemaAccountingServiceBean.find(AccountingSchema.class, id);
	}

	
	/**
	 * 		FIND FORMULATE AND SOURCE BY MATRIX PK		*.
	 *
	 * @param accountingMatrixDetailTo the accounting matrix detail to
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<AccountingMatrixDetail>  findFormulateAndSourceByAccountingMatrixDetail(AccountingMatrixDetailTo accountingMatrixDetailTo){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return schemaAccountingServiceBean.findFormulateAndSourceByAccountingMatrixDetail(accountingMatrixDetailTo);
	}
	
	
	/**
	 * *
	 * Modify AccountingMatrixDetail,  consider that there are also details of new matrices.
	 *
	 * @param listAccountingMatrixDetailOriginal the list accounting matrix detail original
	 * @param listAccountingMatrixDetailToModify the list accounting matrix detail to modify
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void modifyAccountingMatrixDetail(List<AccountingMatrixDetailTo> listAccountingMatrixDetailOriginal , List<AccountingMatrixDetailTo> listAccountingMatrixDetailToModify) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		Boolean saveNewMatrix=Boolean.FALSE;
		/**
		 * List of new matrices to record
		 * **/
		List<AccountingMatrixDetailTo> listMatrixDetailNew=new ArrayList<AccountingMatrixDetailTo>();
		AccountingMatrix accountingMatrixNew	= new AccountingMatrix();
		accountingMatrixNew=schemaAccountingServiceBean.find(listAccountingMatrixDetailOriginal.get(0).getAccountingMatrixFk(), AccountingMatrix.class);
		
		for (AccountingMatrixDetailTo accountingMatrixDetailToOriginal  :  listAccountingMatrixDetailOriginal) {
			for (AccountingMatrixDetailTo accountingMatrixDetailToModify : listAccountingMatrixDetailToModify) {
				
				if(accountingMatrixDetailToOriginal.getIdAccountingMatrixDetailPk().equals(accountingMatrixDetailToModify.getIdAccountingMatrixDetailPk())){
					
				AccountingMatrixDetail accountingMatrixDetailTemp = schemaAccountingServiceBean.find(AccountingMatrixDetail.class, accountingMatrixDetailToModify.getIdAccountingMatrixDetailPk()) ; 
				
				if(accountingMatrixDetailToOriginal.getIdAccountingMatrixDetailPk().equals(accountingMatrixDetailToModify.getIdAccountingMatrixDetailPk())){
					if( accountingMatrixDetailToModify.getState().equals(AccountingMatrixDetailStateType.CANCELED.getCode()) ){
						
						/** Cancel the Matrix Detail	**/
						AccountingMatrixDetail accountingMatrixDetail = accountingMatrixServiceFacade.findListAccountingMatrixDetail(accountingMatrixDetailToModify.getIdAccountingMatrixDetailPk());
						accountingMatrixDetail.setStatus(AccountingMatrixDetailStateType.CANCELED.getCode());
							
						schemaAccountingServiceBean.update(accountingMatrixDetail);
					}
				}
				/**	 Modify the Formulate	**/
				if( !(accountingMatrixDetailToModify.getState().equals(AccountingMatrixDetailStateType.CANCELED.getCode())) &&
								!(accountingMatrixDetailToOriginal.getAccountingFormulateTo().getFormulateInput().
								equals(accountingMatrixDetailToModify.getAccountingFormulateTo().getFormulateInput())) ){
					
						// Associate new Source
						AccountingSource accountingSource = schemaAccountingServiceBean.find(AccountingSource.class, accountingMatrixDetailToModify.getAccountingSourceFk());
						
						accountingMatrixDetailTemp.setAccountingSource(accountingSource);
						
						
						// dropear la formula
						Long idAccountingFormulate = accountingMatrixDetailToOriginal.getAccountingFormulateTo().getIdAccountingFormulateTo();
						schemaAccountingServiceBean.delete(AccountingFormulate.class, idAccountingFormulate);
						
						AccountingFormulate accountingFormulateModify = loadAccountingFormulate(accountingMatrixDetailToModify);
						
						accountinMatrixServiceBean.saveAccountingFormulate(accountingFormulateModify);
						

						accountingMatrixDetailTemp.setAccountingFormulate(accountingFormulateModify);
						
						//schemaAccountingServiceBean.update(accountingMatrixDetailTemp);
					}
				else if( !(accountingMatrixDetailToModify.getState().equals(AccountingMatrixDetailStateType.CANCELED.getCode()))) {
					// Associate new Source
					AccountingSource accountingSource = schemaAccountingServiceBean.find(AccountingSource.class, accountingMatrixDetailToModify.getAccountingSourceFk());
					
					accountingMatrixDetailTemp.setAccountingSource(accountingSource);
					
					
					// dropear la formula
					Long idAccountingFormulate = accountingMatrixDetailToOriginal.getAccountingFormulateTo().getIdAccountingFormulateTo();
					schemaAccountingServiceBean.delete(AccountingFormulate.class, idAccountingFormulate);
					
					AccountingFormulate accountingFormulateModify = loadAccountingFormulate(accountingMatrixDetailToModify);
					
					accountinMatrixServiceBean.saveAccountingFormulate(accountingFormulateModify);
					

					accountingMatrixDetailTemp.setAccountingFormulate(accountingFormulateModify);
				}
				}
				/**Add New Matrix, save MatrixDetail**/
				else if(Validations.validateIsNull(accountingMatrixDetailToModify.getIdAccountingMatrixDetailPk())){
					
					if(!listMatrixDetailNew.contains(accountingMatrixDetailToModify)){
						/**Save Detail with Id Matrix*/
						accountingMatrixDetailToModify.setAccountingMatrix(accountingMatrixNew);
						listMatrixDetailNew.add(accountingMatrixDetailToModify);
						saveNewMatrix=Boolean.TRUE;
						break;
					}
					
				}
			}
			}
		/**
		 * Add and Save new MatrixDetail
		 * **/
		if(saveNewMatrix){
			accountingMatrixServiceFacade.saveAccountingMatrixDetail(listMatrixDetailNew);
		}
	}
	
	
	/**
	 * **
	 * Load AccountingFormulate by AccountingMatrixDetailTo.
	 *
	 * @param accountingMatrixDetailToModify the accounting matrix detail to modify
	 * @return the accounting formulate
	 */
	private AccountingFormulate loadAccountingFormulate(AccountingMatrixDetailTo accountingMatrixDetailToModify ){
		
		AccountingFormulate    accountingFormulate 	    = new AccountingFormulate();
 
		List<VariableFormulateTo> variableFormulateToList = accountingMatrixDetailToModify.getListVariableFormulateTo();
		
		VariableFormulate	   variableFormulate		= null;
		
		List<VariableFormulate>   variableFormulateList	= new ArrayList<VariableFormulate>();
		
		accountingFormulate.setFormulateInput(accountingMatrixDetailToModify.getAccountingFormulateTo().getFormulateInput());
		
		for (VariableFormulateTo variableFormulateTo : variableFormulateToList) {
			
			variableFormulate = new VariableFormulate();
			variableFormulate.setAccountingFormulate(accountingFormulate);
			variableFormulate.setParameterFormulate(variableFormulateTo.getParameterFormulate().getIdAccountingParameterPk());
			variableFormulate.setPositionFormulate(variableFormulateTo.getPositionFormulate());
			variableFormulate.setPositionOffSet(variableFormulateTo.getPositionOffSet());		
			variableFormulate.setSourceType(variableFormulateTo.getSourceType());
			if (AccountingFieldSourceType.get(variableFormulateTo.getSourceType()).getValue().
					equalsIgnoreCase(AccountingFieldSourceType.VALUE.getValue())){
				
				if (Validations.validateIsNotNull(variableFormulateTo.getValueDefault()) ){							
					BigDecimal valueDefault=new BigDecimal(variableFormulateTo.getValueDefault().toString());							
					if (valueDefault.doubleValue()>0){
						variableFormulate.setValueDefault(variableFormulateTo.getValueDefault());
					}
				}
				
				if (Validations.validateIsNotNull(variableFormulateTo.getValueSource()) ){
					
					BigDecimal valueDefault=new BigDecimal(variableFormulateTo.getValueSource().toString());							
					if (valueDefault.doubleValue()>0){
						variableFormulate.setValueDefault(variableFormulateTo.getValueSource());
					}
				}											
			}
			
			variableFormulate.setPositionSource(variableFormulateTo.getPositionSource());
			variableFormulate.setVariableSource(variableFormulateTo.getVariableSource());
			
			
			variableFormulateList.add(variableFormulate);
		}
			
			accountingFormulate.setVariableFormulates(variableFormulateList);
			
		 
		
		return accountingFormulate;
		
	}
	
	
	/**
	 * Get List Accounting Parameter.
	 *
	 * @param nameParameter the name parameter
	 * @return listAccountingParameterTO: list AccountingParameter, by name Parameter
	 */
	public List<AccountingParameterTO> getListAccountingParameter(List<String> nameParameter){
		
		List<AccountingParameterTO> listAccountingParameterTO=new ArrayList<AccountingParameterTO>();
		AccountingParameterTO accountingParameterTO=null;
		
		int i=0;
		for (String name : nameParameter) {
			accountingParameterTO=parameterAccountingServiceFacade.findAccountingParameterToByName(name);
			accountingParameterTO.setIndex(i);
			listAccountingParameterTO.add(accountingParameterTO);
			i++;
		}
		
		return listAccountingParameterTO;
	}
	
	/**
	 * *.
	 *
	 * @param listVariableFormulate the list variable formulate
	 * @return the list variable formulate to
	 */
	public List<VariableFormulateTo> getListVariableFormulateTo(List<VariableFormulate> listVariableFormulate){
		List<VariableFormulateTo> listVariableFormulateTo=new ArrayList<VariableFormulateTo>();
		
		VariableFormulateTo variableFormulateTo=null;
		AccountingParameter accountingParameter=null;
		for (VariableFormulate variableFormulate : listVariableFormulate) {
			
			variableFormulateTo=new VariableFormulateTo();
			
			AccountingParameterTO accountingParameterTO=new AccountingParameterTO();
			accountingParameterTO.setIdAccountingParameterPk(variableFormulate.getParameterFormulate());
			accountingParameter=parameterAccountingServiceFacade.getAccountingParameterByPk(variableFormulate.getParameterFormulate());
			
			/**Falta el nombre de la variable con separador y sin el**/
			variableFormulateTo.setIdVariableFormulatePk(variableFormulate.getIdVariableFormulatePk());
			variableFormulateTo.setParameterFormulate(accountingParameterTO);
			variableFormulateTo.setPositionFormulate(variableFormulate.getPositionFormulate());
			variableFormulateTo.setPositionOffSet(variableFormulate.getPositionOffSet());
			variableFormulateTo.setPositionSource(variableFormulate.getPositionSource());
			variableFormulateTo.setSourceType(variableFormulate.getSourceType());
			variableFormulateTo.setVariableSource(variableFormulate.getVariableSource());
			variableFormulateTo.setValueDefault(variableFormulate.getValueDefault());
			variableFormulateTo.setNameVariableWithOutSeparator(accountingParameter.getParameterName());
			listVariableFormulateTo.add(variableFormulateTo);
			
		}
		return listVariableFormulateTo;
	}
	

	/**
	 * Save accounting schema.
	 *
	 * @param accountingSchema the accounting schema
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void saveAccountingProcessSchema(AccountingProcessSchema accountingProcessSchema){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		schemaAccountingServiceBean.saveAccountingSchema(accountingProcessSchema);
			 
	}

	public List<AccountingProcessSchemaTo> searchAccountingProcessSchema(AccountingProcessReStartTO filter){
		
		return schemaAccountingServiceBean.searchAccountingProcessSchema(filter);
			 
	}
	
	
}


