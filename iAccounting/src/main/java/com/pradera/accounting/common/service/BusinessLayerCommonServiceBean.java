package com.pradera.accounting.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingCorrelative;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BusinessLayerCommonServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
public class BusinessLayerCommonServiceBean extends CrudDaoServiceBean {

	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	
	
	/**
	 * *.
	 *
	 * @param accountingCorrelativeFilter the accounting correlative filter
	 * @return the last sequence generator code
	 * @throws ServiceException the service exception
	 */
	public  AccountingCorrelative    getLastSequenceGeneratorCode(AccountingCorrelative accountingCorrelativeFilter) throws ServiceException{

		StringBuilder stringBuilderSql = new StringBuilder();	
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		AccountingCorrelative	accountingCorrelative = null;
		
		stringBuilderSql.append("SELECT ac FROM  AccountingCorrelative ac  ");		
		stringBuilderSql.append(" WHERE  1=1  ");
		if (Validations.validateIsNotNullAndNotEmpty(accountingCorrelativeFilter.getCorrelativeCD())){
		 
			stringBuilderSql.append(" AND  ac.correlativeCD = :correlativeCD  ");
			parameters.put("correlativeCD", accountingCorrelativeFilter.getCorrelativeCD());
		}else{
			
			throw new ServiceException(ErrorServiceType.ACCOUNTING_RECEIPT_CODE_ERROR);
		}
		
		/**REPEIPT**/
		if (Validations.validateIsNotNullAndNotEmpty(accountingCorrelativeFilter.getMonth())&&
				Validations.validateIsNotNullAndNotEmpty(accountingCorrelativeFilter.getYear())){
			stringBuilderSql.append(" AND  ac.month = :month  ");
			stringBuilderSql.append(" AND  ac.year = :year ");
			parameters.put("month", accountingCorrelativeFilter.getMonth());
			parameters.put("year", accountingCorrelativeFilter.getYear());
		}
		
		
		try{
			
			accountingCorrelative = (AccountingCorrelative) findObjectByQueryString( stringBuilderSql.toString(), parameters);
			
		} catch(Exception  ex){
			
			if ( ex instanceof NoResultException){
				
				return accountingCorrelative;
				
			}else{
			
				throw new ServiceException(ErrorServiceType.ACCOUNTING_RECEIPT_CODE_ERROR);
			}
			
		}
		
		return accountingCorrelative;
	}
	
	/**
	 * *.
	 *
	 * @param businessProcess the business process
	 * @return the list usser by business process
	 */
	public List<UserAccountSession> getListUsserByBusinessProcess(Long businessProcess)
	{
		List<UserAccountSession> listUserAccountSession=null;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("    SELECT ");
		sbQuery.append("    	NVL(dn.id_user_account,' '),  ");		
		sbQuery.append("    	NVL(dn.email,' '),  ");
		sbQuery.append("    	NVL(dn.mobile_number,' '),   ");
		sbQuery.append("    	NVL(dn.full_name,' ') ");
		sbQuery.append("    FROM  ");
		sbQuery.append("    	business_process bp ");
		sbQuery.append("    inner join process_notification pn on pn.id_business_process_fk=bp.id_business_process_pk ");
		sbQuery.append("    inner join destination_notification dn on dn.id_process_notification_fk=pn.id_process_notification_pk ");
		sbQuery.append("    WHERE ");
		sbQuery.append("    	bp.id_business_process_pk=:idBusinessProcessPk ");
		Query query = em.createNativeQuery(sbQuery.toString());
		sbQuery.append("    ORDER BY id_business_process_pk  ");
		if(Validations.validateIsNotNullAndPositive(businessProcess)){
			query.setParameter("idBusinessProcessPk", businessProcess);
		}
		List<Object[]>  objectList = query.getResultList();
		listUserAccountSession=new ArrayList<UserAccountSession>();
		for(int i=0;i<objectList.size();++i){ 
	    	Object[] sResults = (Object[])objectList.get(i); 
			UserAccountSession userAccountSession=new UserAccountSession();
			userAccountSession.setUserName(sResults[0].toString());
			userAccountSession.setEmail(sResults[1].toString());
			userAccountSession.setPhoneNumber(sResults[2].toString());
			userAccountSession.setFullName(sResults[3].toString());
			listUserAccountSession.add(userAccountSession);
		}
		
		return listUserAccountSession;
	}
	
}
