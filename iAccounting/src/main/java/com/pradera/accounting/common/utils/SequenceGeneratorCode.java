package com.pradera.accounting.common.utils;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;

import com.pradera.accounting.common.facade.BusinessLayerCommonFacade;
import com.pradera.accounting.document.to.AccountingCorrelativeTo;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingCorrelative;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SequenceGeneratorCode.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Singleton
public class SequenceGeneratorCode    {


	/** The log. */
	@Inject
	PraderaLogger log;
	 
	/** The load entity service. */
	@EJB
	LoaderEntityServiceBean 	loadEntityService;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean 			crudDaoServiceBean;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade	businessLayerCommonFacade;
	
	 	
	/** The em. */
	@Inject @DepositaryDataBase
	private EntityManager em;
	
	/**
	 * Gets the last accounting correlative.
	 *
	 * @param accountingCorrelativeFilter the accounting correlative filter
	 * @return next sequenceGeneratorCode
	 * @throws ServiceException the service exception
	 * @deprecated 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	public  AccountingCorrelativeTo    getLastAccountingCorrelative(AccountingCorrelative accountingCorrelativeFilter) throws ServiceException{
		AccountingCorrelativeTo accountingCorrelativeTo;
			
		AccountingCorrelative correlativeResult = businessLayerCommonFacade.getLastSequenceGeneratorCode(accountingCorrelativeFilter);
						
		if( Validations.validateIsNull(correlativeResult)
			|| Validations.validateIsNull(correlativeResult.getIdAccountingCorrelativePk()) ){
			
			correlativeResult.setCorrelativeNumber(1L);
			accountingCorrelativeTo=new AccountingCorrelativeTo(correlativeResult);

		}
		else{
			
			Long sequence = correlativeResult.getCorrelativeNumber();
			correlativeResult.setCorrelativeNumber(sequence +1);
			accountingCorrelativeTo=new AccountingCorrelativeTo(correlativeResult);

		}
		
		return accountingCorrelativeTo;
		
	}
	
	/**
	 * Gets the last sequence generator code.
	 *
	 * @param accountingCorrelativeFilter the accounting correlative filter
	 * @return next sequenceGeneratorCode
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	public  Long    getLastSequenceGeneratorCode(AccountingCorrelative accountingCorrelativeFilter) throws ServiceException{
		
		Long     	codeGenerate  = null;
			
		AccountingCorrelative correlativeResult = businessLayerCommonFacade.getLastSequenceGeneratorCode(accountingCorrelativeFilter);
						
		if( Validations.validateIsNull(correlativeResult)
			|| Validations.validateIsNull(correlativeResult.getIdAccountingCorrelativePk()) ){
		
			correlativeResult	= new AccountingCorrelative();	
			correlativeResult.setCorrelativeCD(accountingCorrelativeFilter.getCorrelativeCD());
			correlativeResult.setCorrelativeNumber(1L);
			correlativeResult.setDescription(accountingCorrelativeFilter.getCorrelativeCD());
			correlativeResult.setYear(accountingCorrelativeFilter.getYear());
			correlativeResult.setMonth(accountingCorrelativeFilter.getMonth());
//			crudDaoServiceBean.create(correlativeResult);							
		}
		else{
			
			Long sequence = correlativeResult.getCorrelativeNumber();
			correlativeResult.setCorrelativeNumber(sequence +1);						
//			crudDaoServiceBean.update(correlativeResult);						
		}	
		
//		em.flush();
		codeGenerate = correlativeResult.getCorrelativeNumber();
				
		
	
		return codeGenerate;
		
	}
	
	/**
	 * Find accounting correlative.
	 *
	 * @param accountingCorrelativeFilter the accounting correlative filter
	 * @return the accounting correlative
	 * @throws ServiceException the service exception
	 */
	public  AccountingCorrelative    findAccountingCorrelative(AccountingCorrelative accountingCorrelativeFilter) 
			throws ServiceException{
		
		AccountingCorrelative correlativeResult = businessLayerCommonFacade.getLastSequenceGeneratorCode(accountingCorrelativeFilter);
		
		return correlativeResult;
	}
	
	/**
	 * Correlative to show. FORM: MM+CORRELATIVE, complete with zero
	 * Ex.  Month: January.
	 * 			01 000000001
	 * 			01 000000002
	 * 
	 * 		Month: February.
	 * 			02 000000001
	 * 			02 000000002
	 * 
	 * 		Month: December
	 * 			12 000000001
	 * 			12 000000002
	 *
	 * @param accountingCorrelativeFilter the accounting correlative filter
	 * @return the last sequence for receipt
	 * @throws ServiceException the service exception
	 */
	public  String    getLastSequenceForReceipt(AccountingCorrelative accountingCorrelativeFilter) throws ServiceException{
		
		Long       	codeGenerate  = getLastSequenceGeneratorCode(accountingCorrelativeFilter);
		
		/**String containing number Generate**/
		String 	   	strSecuence	 = ""; 
		
		/**String containing the result of the month*/
		String 		month="";
		
		/**string containing the result*/
		String		resultSequence="";
		
		if (Validations.validateIsNotNull(codeGenerate)){
			
			strSecuence=getNumberFormatWithZero(codeGenerate, 9);
			month=getNumberFormatWithZero(accountingCorrelativeFilter.getMonth().longValue(), 2);
			resultSequence=month.concat(strSecuence);
		}
		
		 	
		return resultSequence;
		
	}
	
	/**
	 * * Ex.  Month: January.
	 * 			01 000000001
	 * 			01 000000002
	 * 
	 * 		Month: February.
	 * 			02 000000001
	 * 			02 000000002
	 * 
	 * 		Month: December
	 * 			12 000000001
	 * 			12 000000002
	 *
	 * @param lastCodeGenerate the last code generate
	 * @return the string
	 */
	public  String    addLastSequenceForReceipt(String lastCodeGenerate) {
		
		/**String containing number Generate**/
		String 	   	strSecuence	 = ""; 
		
		/**String containing the result of the month*/
		String 		month="";
		String correlative="";
		Long correlativeNumber=null;
		
		/**string containing the result*/
		String		resultSequence="";
		
		if (Validations.validateIsNotNull(lastCodeGenerate)){
			month=lastCodeGenerate.substring(0, 2);
			correlative=lastCodeGenerate.substring(2, lastCodeGenerate.length());
			correlativeNumber=new Long(correlative);
			correlativeNumber=correlativeNumber+1L;
			
			
			strSecuence=getNumberFormatWithZero(correlativeNumber, 9);
			resultSequence=month.concat(strSecuence);
		}
		return resultSequence;
	}
	
	/**
	 * Gets the number format with zero.
	 *
	 * @param codeGenerate the code generate
	 * @param longSequence the long sequence
	 * @return  the format complete with zero
	 */
	public String getNumberFormatWithZero(Long codeGenerate,Integer longSequence){
		String 	   strSecuence	 = "";
		Integer    numDigits	 = 0;
		
		
		if (Validations.validateIsNotNull(codeGenerate)){
			
			strSecuence = codeGenerate.toString();
			numDigits	= codeGenerate.toString().length();
		
			for (int i = 0; i < (longSequence - numDigits); i++) {
				
				strSecuence="0"+strSecuence;
			}
		}else{
			return "";
		}
		
		return strSecuence;
		
	}

}
