
package com.pradera.accounting.common.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounting.common.service.BusinessLayerCommonServiceBean;
import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.document.facade.DocumentAccountingServiceFacade;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.security.model.type.ExternalInterfaceType;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingCorrelative;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingReceipt;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.type.ReportFormatType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BusinessLayerCommonFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BusinessLayerCommonFacade {

		
	
	/** The bussiness layer common service bean. */
	@EJB
	private BusinessLayerCommonServiceBean 	bussinessLayerCommonServiceBean;
	
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry 			transactionRegistry;
	
	/** The holiday query service bean. */
	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;

	/** The client rest service. */
	@Inject
	ClientRestService 							clientRestService;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade 					notificationServiceFacade;
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The document accounting service facade. */
	@EJB
	DocumentAccountingServiceFacade				documentAccountingServiceFacade;

	/** The report generation service. */
	@Inject
    Instance<ReportGenerationService> 			reportGenerationService;
	
	/** The parameter accounting service facade. */
	@EJB
	ParameterAccountingServiceFacade			parameterAccountingServiceFacade;
	
	/**
	 * Gets the value parameter table.
	 *
	 * @param parameterTableList the parameter table list
	 * @param key the key
	 * @return the value parameter table
	 */
	public String getValueParameterTable(List<ParameterTable> parameterTableList ,Integer key ){
		String name="";
			for (ParameterTable parameterTable : parameterTableList) {
				if (parameterTable.getParameterTablePk().intValue()==key.intValue()) {
					name=parameterTable.getDescription();
				}
			}
		return name;
	}

	/**
	 * If the day is not a business.
	 *
	 * @param date : Date consult
	 * @return TRUE if is Holiday
	 * @throws ServiceException the service exception
	 */
	public  Boolean ifDayHoliday(Date date) throws ServiceException{
		
		if(!CommonsUtilities.isUtilDate(date) || holidayQueryServiceBean.isHolidayDateServiceBean(date)){
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the last sequence generator code.
	 *
	 * @param accountingCorrelativeFilter the accounting correlative filter
	 * @return the last sequence generator code
	 * @throws ServiceException the service exception
	 */
	public  AccountingCorrelative    getLastSequenceGeneratorCode(AccountingCorrelative accountingCorrelativeFilter) throws ServiceException{
	    return  bussinessLayerCommonServiceBean.getLastSequenceGeneratorCode(accountingCorrelativeFilter);
	}
	
	/**
	 * Gets the logger user.
	 *
	 * @return the logger user
	 */
	public LoggerUser getLoggerUser(){
		return (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	}
  
	/**
	 * Send notification Manual.
	 *
	 * @param businessProcess 		the business process
	 * @param subject 				the subject
	 * @param message 			the message
	 * @param notificationType 	the Notification Type
	 */
	public void sendNotificationManual(BusinessProcess businessProcess,String subject,String message,NotificationType notificationType ){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		List<UserAccountSession> usersAccounts= null;
		usersAccounts = clientRestService.getUsersInformation(null, null, null, null,loggerUser.getUserName(),ExternalInterfaceType.NO.getCode());
		
		notificationServiceFacade.sendNotificationManual(loggerUser.getUserName(), businessProcess, usersAccounts,subject, message, notificationType);
		
	}
	
	/**
	 * Send notification manual.
	 *
	 * @param businessProcess 	the business process
	 * @param usersAccounts 		the User Accounts
	 * @param subject 			the subject
	 * @param message 			the message
	 * @param notificationType 	the Notification Type
	 */
	public void sendNotificationManual(BusinessProcess businessProcess,List<UserAccountSession> usersAccounts,String subject,String message,NotificationType notificationType ){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		
		notificationServiceFacade.sendNotificationManual(loggerUser.getUserName(), businessProcess, usersAccounts,subject, message, notificationType);

	}
	
	
	/**
	 * Send notification automatic.
	 *
	 * @param businessProcess 	the business process
	 * @param intitutionCode the institution code
	 * @param parameters the parameters
	 */
	public void sendNotificationAutomatic(BusinessProcess businessProcess,List<Object> intitutionCode,Object[] parameters){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProcess, intitutionCode, parameters);
	}
	
	/**
	 * *
	 * Get User By Business Process.
	 *
	 * @param businessProcess the business process
	 * @return List<UserAccountSession>
	 */
	public List<UserAccountSession> getListUsserByBusinessProcess(Long businessProcess){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		
		return bussinessLayerCommonServiceBean.getListUsserByBusinessProcess(businessProcess);
	}
	
	/**
	 * Inits the notification.
	 *
	 * @param accountingProcessTo the accounting process to
	 */
	public void initNotification(AccountingProcessTo accountingProcessTo){
		
	List<AccountingReceipt> accountingReceiptList = documentAccountingServiceFacade.findAccuntingReceiptByIdAccountingProcess(accountingProcessTo.getIdAccountingProcessPk());
	
		if (Validations.validateListIsNotNullAndNotEmpty(accountingReceiptList)){
			
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCOUNTING_DOCUMENT_GENERATE.getCode());
			
			
			String branchTypeDescription=getBranchTypeDescriptionInString(accountingProcessTo.getAccountingMatrixList());

			AccountingParameter accountingParameter = parameterAccountingServiceFacade.getAccountingParameterByPk(accountingProcessTo.getProcessType());
			Object[] arrTemp = { accountingParameter.getDescription() ,branchTypeDescription, CommonsUtilities.currentDateTime().toString()};
			
			sendNotificationAutomatic(businessProcess, null, arrTemp);
			

		}
		
	}
	
	/**
	 * Gets the branch type description in string.
	 *
	 * @param listAccountingMatrix the list accounting matrix
	 * @return the branch type description in string
	 */
	public String getBranchTypeDescriptionInString(List<AccountingMatrix> listAccountingMatrix ){
		
		List<Integer> listBranchTypeId=new ArrayList<Integer>();
		
		
		/**Find Branch**/
		for (AccountingMatrix accountingMatrix : listAccountingMatrix) {
			Integer branchType=accountingMatrix.getAccountingSchema().getBranchType();
			if(!listBranchTypeId.contains(branchType)){
				listBranchTypeId.add(branchType);
			}
			
		}
		
		List<Object> listBranchType=new ArrayList<Object>();
		for (Integer branchType : listBranchTypeId) {
			AccountingParameter accountingParameter=parameterAccountingServiceFacade.getAccountingParameterByPk(branchType);
			listBranchType.add(accountingParameter.getDescription());
		}
		
		String branchTypeDescription=AccountingUtils.convertListObjectToString(listBranchType);
		return branchTypeDescription;
	}
	
	
	/**
	 * Sending report on billing.
	 *
	 * @param idAccountingProcessPk the id accounting process pk
	 * @param reportId the report id
	 * @throws ServiceException the service exception
	 */
	public void sendingReportOnAccounting(Long idAccountingProcessPk,Long reportId) throws ServiceException{
		
		/**LoggerUser*/
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		/**Parameter*/
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("process_id", idAccountingProcessPk.toString());
		parameters.put(GeneralConstants.REPORT_BY_PROCESS, BooleanType.YES.getCode().toString());
		/**ParameterDetails*/
		Map<String,String> parameterDetails = new HashMap<String, String>();
		/**ReportUser*/
		ReportUser reportUser = new ReportUser();
				
		reportUser.setUserName(loggerUser.getUserName());
		reportUser.setReportFormat(ReportFormatType.PDF.getCode());
		//how report is send for screen so for default send notification
		reportUser.setShowNotification(BooleanType.YES.getCode());
		
		reportGenerationService.get().saveReportExecution(reportId, parameters, parameterDetails, reportUser, loggerUser);
		
	}

	
}
