package com.pradera.accounting.common.utils;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.pradera.accounting.matrix.to.AccountingMatrixDetailTo;
import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.VariableFormulate;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingUtils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingUtils {


	/** The Constant DATE_PATTERN_yyyy_MM. */
	public static final String 	DATE_PATTERN_yyyy_MM	="yyyy/MM"; 	
	
	/** The Constant DATE_PATTERN_yyyy_MM_dd. */
	public static final String  DATE_PATTERN_yyyy_MM_dd = "yyyy/MM/dd";
	
	/** The Constant SLACH. */
	public static final String  SLACH					="/";
	
	/** The Constant VERTICAL. */
	public static final String  VERTICAL				="|";

	/**
	 * Gets the pattern year month.
	 *
	 * @param strDate the str date
	 * @param date_pattern yyyy/MM/dd
	 * @return yyyy/MM
	 */
	public  static String getPatternYearMonth(String strDate,String date_pattern){
		String strYearMonth="";
		if (AccountingUtils.DATE_PATTERN_yyyy_MM_dd.equalsIgnoreCase(date_pattern)){
			strYearMonth = strDate.substring(0, strDate.lastIndexOf("/"));
		}
		return strYearMonth;
	}

	/**
	 * Gets the pattern year.
	 *
	 * @param strDate the str date
	 * @param date_pattern the date_pattern
	 * @return the pattern year
	 */
	public static Integer getPatternYear(String strDate,String date_pattern){
		Integer intYear=null;
		if (AccountingUtils.DATE_PATTERN_yyyy_MM.equalsIgnoreCase(date_pattern)){
			String strYear= strDate.substring(0, strDate.lastIndexOf("/"));
			intYear= Integer.parseInt(strYear);
		}
		return intYear;
	}

	/**
	 * Gets the pattern month.
	 *
	 * @param strDate the str date
	 * @param date_pattern the date_pattern
	 * @return the pattern month
	 */
	public static Integer getPatternMonth(String strDate,String date_pattern){
		Integer intMonth=null;
		if (AccountingUtils.DATE_PATTERN_yyyy_MM.equalsIgnoreCase(date_pattern)){			
			String strMonth= strDate.substring(strDate.lastIndexOf("/")+1);
			intMonth= Integer.parseInt(strMonth);
		}
		return intMonth;
	}

	/**
	 * Gets the max date of month.
	 *
	 * @param objDate the obj date
	 * @return the max date of month
	 */
	public static Date truncateDayOfMonth(Date objDate){
		Calendar cal = Calendar.getInstance();
		cal.setTime(objDate);
		cal.set(cal.DAY_OF_MONTH, 0);
		return cal.getTime();
	}


	/**
	 * Clone list.
	 *
	 * @param <T> the generic type
	 * @param list the list
	 * @return the list
	 */
	public  static  <T> List<T>  cloneList(List<T> list) {
		List<T> clone = new ArrayList<T>(list.size());
		for (T item: list) {	
			clone.add(item);
		}
		return clone;
	}



	/**
	 * Multiply BigDecimal and Integer result BigDecimal.
	 *
	 * @param numDecimal the num decimal
	 * @param numEntero the num entero
	 * @param round the round
	 * @return the big decimal
	 */
	public static BigDecimal multiplyDecimalInteger(BigDecimal numDecimal,Integer numEntero,Integer round){
		BigDecimal decimalOfInteger=null;
		BigDecimal result=new BigDecimal(0);
		int mode = BigDecimal.ROUND_HALF_UP;
		if(Validations.validateIsNull(numDecimal))
			numDecimal=BigDecimal.ZERO;
		if(Validations.validateIsNull(numEntero))
			decimalOfInteger=BigDecimal.ZERO; 
		decimalOfInteger=new BigDecimal(numEntero);
		result=numDecimal.multiply(decimalOfInteger).setScale(round,mode);
		return result;
	}




	/**
	 * Multiply decimal long.
	 *
	 * @param numDecimal the num decimal
	 * @param numEntero the num entero
	 * @param round the round
	 * @return the big decimal
	 */
	public static BigDecimal multiplyDecimalLong(BigDecimal numDecimal,Long numEntero,Integer round){
		BigDecimal decimalOfLong=null;
		BigDecimal result=new BigDecimal(0);
		int mode = BigDecimal.ROUND_HALF_UP;
		if(Validations.validateIsNull(numDecimal)){
			numDecimal=BigDecimal.ZERO; 
		} 
		if(Validations.validateIsNull(numEntero)){
			numEntero=Long.parseLong("0"); 
		}	  
		decimalOfLong=new BigDecimal(numEntero);
		result=numDecimal.multiply(decimalOfLong).setScale(round,mode);
		return result;
	}
	
	/**
	 * Multiply two BigDecimal result BigDecimal.
	 *
	 * @param numDecimal1 the num decimal1
	 * @param numDecimal2 the num decimal2
	 * @param round the round
	 * @return the big decimal
	 */
	public static BigDecimal multiplyDecimals(BigDecimal numDecimal1,BigDecimal numDecimal2,Integer round){
		BigDecimal result=new BigDecimal(0);
		if(Validations.validateIsNull(numDecimal1))
			numDecimal1=BigDecimal.ZERO;
		if(Validations.validateIsNull(numDecimal2))
			numDecimal2=BigDecimal.ZERO;
		int mode = BigDecimal.ROUND_HALF_UP;
		result=numDecimal1.multiply(numDecimal2).setScale(round,mode);
		return result;
	}
	
	/**
	 * Add two BigDecimal result BigDecimal.
	 *
	 * @param numDecimal1 the num decimal1
	 * @param numDecimal2 the num decimal2
	 * @param round the round
	 * @return the big decimal
	 */
	public static BigDecimal addTwoDecimal(BigDecimal numDecimal1,BigDecimal numDecimal2,Integer round){
		BigDecimal result=new BigDecimal(0);
		if(Validations.validateIsNull(numDecimal1))
			numDecimal1=BigDecimal.ZERO;
		if(Validations.validateIsNull(numDecimal2))
			numDecimal2=BigDecimal.ZERO;
		int mode = BigDecimal.ROUND_HALF_UP;
		result=numDecimal1.add(numDecimal2).setScale(round, mode);
		return result;
	}
	
	/**
	 * Multiply BigDecimal and Integer result BigDecimal.
	 *
	 * @param numDecimal the num decimal
	 * @param numEntero the num entero
	 * @param round the round
	 * @return the big decimal
	 */
	public static BigDecimal addDecimalInteger(BigDecimal numDecimal,Integer numEntero,Integer round){
		BigDecimal decimalOfInteger=null;
		BigDecimal result=new BigDecimal(0);
		int mode = BigDecimal.ROUND_HALF_UP;
		if(Validations.validateIsNull(numDecimal))
			numDecimal=BigDecimal.ZERO;
		if(Validations.validateIsNull(numEntero))
			decimalOfInteger=BigDecimal.ZERO; 
		decimalOfInteger=new BigDecimal(numEntero);
		result=numDecimal.add(decimalOfInteger).setScale(round, mode);
		return result;
	}
	
	/**
	 * Add two BigDecimal  (BigDecimal, Object)result BigDecimal.
	 *
	 * @param numDecimal1 the num decimal1
	 * @param obj the obj
	 * @param round the round
	 * @return the big decimal
	 */
	public static BigDecimal addDecimalObject(BigDecimal numDecimal1,Object obj,Integer round){
		BigDecimal result=new BigDecimal(0);
		BigDecimal objDecimal=null;
		if(Validations.validateIsNull(numDecimal1))
			numDecimal1=BigDecimal.ZERO;
		if(Validations.validateIsNull(obj))
			objDecimal=BigDecimal.ZERO;
		else objDecimal= new BigDecimal(obj.toString());
		int mode = BigDecimal.ROUND_HALF_UP;
		result=numDecimal1.add(objDecimal).setScale(round, mode);
		return result;
	}

	/**
	 * Divide two Decimal result BigDecimal.
	 *
	 * @param numDecimal1 the num decimal1
	 * @param numDecimal2 the num decimal2
	 * @param round the round
	 * @return the big decimal
	 * @throws ArithmeticException the arithmetic exception
	 */
	public static BigDecimal divideTwoDecimal(BigDecimal numDecimal1,BigDecimal numDecimal2,Integer round) throws ArithmeticException{
		BigDecimal result=new BigDecimal(0);

		int mode = BigDecimal.ROUND_HALF_UP;
		if(numDecimal1==null)
			numDecimal1=BigDecimal.ZERO;
		if(numDecimal2==null){
			return null;
		}			 
		result=numDecimal1.divide(numDecimal2,round,mode);
		return result;
	}
	
	/**
	 * Divide two Decimal (BigDecimal, Object) result BigDecimal.
	 *
	 * @param numDecimal1 the num decimal1
	 * @param obj the obj
	 * @param round the round
	 * @return the big decimal
	 * @throws ArithmeticException the arithmetic exception
	 */
	public static BigDecimal divideDecimalObject(BigDecimal numDecimal1,Object obj,Integer round) throws ArithmeticException{
		BigDecimal result=new BigDecimal(0);
		BigDecimal objDecimal=null;
		if(Validations.validateIsNull(numDecimal1))
			numDecimal1=BigDecimal.ZERO;
		if(Validations.validateIsNull(obj))
			return null;
		else objDecimal= new BigDecimal(obj.toString());
		int mode = BigDecimal.ROUND_HALF_UP;
		result=numDecimal1.divide(objDecimal,round,mode);
		return result;
	}

	/**
	 * getFirstDayOfMonth.
	 *
	 * @param date the date
	 * @return the first day of month
	 */
	public static Date getFirstDayOfMonth(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(cal.get(Calendar.YEAR),

				cal.get(Calendar.MONTH),

				cal.getActualMinimum(Calendar.DAY_OF_MONTH),

				cal.getMinimum(Calendar.HOUR_OF_DAY),

				cal.getMinimum(Calendar.MINUTE),

				cal.getMinimum(Calendar.SECOND));

		return cal.getTime();
	}
	
	/**
	 * getLastDayOfMonth.
	 *
	 * @param date the date
	 * @return the last day of month
	 */
	public static Date getLastDayOfMonth(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(cal.get(Calendar.YEAR),

				cal.get(Calendar.MONTH),

				cal.getActualMaximum(Calendar.DAY_OF_MONTH),

				cal.getMaximum(Calendar.HOUR_OF_DAY),

				cal.getMaximum(Calendar.MINUTE),

				cal.getMaximum(Calendar.SECOND));

		return cal.getTime();
	}

	/**
	 * Get First Day Of the Year.
	 *
	 * @param date the date
	 * @return the first day of year
	 */
	public static Date getFirstDayOfYear(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(cal.get(Calendar.YEAR),

				cal.getActualMinimum(Calendar.MONTH),

				cal.getActualMinimum(Calendar.DAY_OF_MONTH),

				cal.getMinimum(Calendar.HOUR_OF_DAY),

				cal.getMinimum(Calendar.MINUTE),

				cal.getMinimum(Calendar.SECOND));

		return cal.getTime();
	}


	/**
	 * Get Last Day of the Year.
	 *
	 * @param date the date
	 * @return the last day of year
	 */
	public static Date getLastDayOfYear(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(cal.get(Calendar.YEAR),

				cal.getActualMaximum(Calendar.MONTH),

				cal.getActualMaximum(Calendar.DAY_OF_MONTH),

				cal.getMaximum(Calendar.HOUR_OF_DAY),

				cal.getMaximum(Calendar.MINUTE),

				cal.getMaximum(Calendar.SECOND));

		return cal.getTime();
	}

	/**
	 * Delete space white.
	 *
	 * @param parameter the parameter
	 * @return the string
	 */
	public String deleteSpaceWhite(String parameter){

		return (Validations.validateIsNullOrEmpty(parameter) ? parameter:parameter.replaceAll(" ", parameter));
	}

	
	

	/**
	 * Load file to object source.
	 *
	 * @param path the path
	 * @return the accounting source info
	 */
	public static AccountingSourceInfo loadFileToObjectSource(String path){
		
		/**
		 *  byte[] byteArr = new byte[] { 0xC, 0xA, 0xF, 0xE };
			InputStream is = new ByteArrayInputStream(byteArr);
		 */
		SAXParserFactory factory = SAXParserFactory.newInstance();
		AccountingSourceInfo accountingSourceInfo = null  ;
		
		try {
			InputStream    xmlInput  =
					new FileInputStream(path);

			SAXParser      saxParser = factory.newSAXParser();
			LoaderDataSourceHandler handler   = new LoaderDataSourceHandler();
			saxParser.parse(xmlInput, handler);
			
			accountingSourceInfo	= handler.getAccountingSourceInfo();
			handler.cleanAccountingSourceInfo();
			
			
		} 
		catch (SAXException se) {
			System.out.println("SAX exception: " + se.getMessage());
		}
		catch (IOException ioe) {
			System.out.println("IO exception: " + ioe.getMessage());
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return accountingSourceInfo;
	}

	/**
	 * *
	 * 
	 * Make AccountingSourceFile from XML 
	 * Validate Structure the File XML and Load AccountingSourceInfo with the information
	 * 
	 * byte[] byteArr = new byte[] { 0xC, 0xA, 0xF, 0xE };
	 * InputStream is = new ByteArrayInputStream(byteArr);.
	 *
	 * @param byteArr the byte arr
	 * @return AccountingSourceInfo
	 * @throws ServiceException the service exception
	 */
	public static AccountingSourceInfo loadXMLToObjectSource(byte[] byteArr)throws ServiceException{

		SAXParserFactory factory = SAXParserFactory.newInstance();
		AccountingSourceInfo accountingSourceInfo = null  ;

		try {
			
			InputStream xmlInput = new ByteArrayInputStream(byteArr);

			SAXParser      saxParser = factory.newSAXParser();
			LoaderDataSourceHandler handler   = new LoaderDataSourceHandler();
			saxParser.parse(xmlInput, handler);
			
			accountingSourceInfo	= handler.getAccountingSourceInfo();
			handler.cleanAccountingSourceInfo();
			
			
		}
		catch (SAXException se) {
			if(se.getMessage().equals(PropertiesConstants.ACCOUNTING_SOURCE_WRONG_STRUCTURED)){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_SOURCE_WRONG_STRUCTURED);
			}
			else if(se.getMessage().equals(PropertiesConstants.ACCOUNTING_SOURCE_VALUE_FORMAT)){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_SOURCE_VALUE_FORMAT);
			}
			
			else if(se.getMessage().equals(PropertiesConstants.ACCOUNTING_SOURCE_VISIBILITY_FORMAT)){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_SOURCE_VISIBILITY_FORMAT);
			}
			else if(se.getMessage().equals(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_FORMAT)){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_SOURCE_TYPE_FORMAT);
			}
			else if(se.getMessage().equals(PropertiesConstants.ACCOUNTING_SOURCE_POSITION_FORMAT)){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_SOURCE_POSITION_FORMAT);
			}
			else if(se.getMessage().equals(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_ASSOCIATE)){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_SOURCE_TYPE_ASSOCIATE);
			}
			se.printStackTrace();
//			System.out.println("SAX exception: " + se.getMessage());
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
//			System.out.println("IO exception: " + ioe.getMessage());
		} catch (ParserConfigurationException e) {
			
			e.printStackTrace();
		}
		
		return accountingSourceInfo;
	}


	/**
	 * *.
	 *
	 * @param lstObjects the lst objects
	 * @return the string
	 */
	public static String convertListObjectToStringWithOutComma(List<Object> lstObjects) {		

		StringBuilder sbString = new StringBuilder();
		for (int i=0; i<lstObjects.size(); i++) {
			sbString.append(lstObjects.get(i).toString());

		}

		return sbString.toString();
	}

	/**
	 * *.
	 *
	 * @param lstObjects the lst objects
	 * @return the string
	 */
	public static String convertListObjectToString(List<Object> lstObjects) {		
		
		StringBuilder sbString = new StringBuilder();
		for (int i=0; i<lstObjects.size(); i++) {
			sbString.append(lstObjects.get(i).toString());
			if(i < (lstObjects.size() - 1)){
				sbString.append(",");
			}
		}
		
		return sbString.toString();
	}
	
	/**
	 * *.
	 *
	 * @param formulate the formulate
	 * @return the list
	 */
	public static List<Object> convertListGenericToObject(List<AccountingParameterTO> formulate){
		List<Object> listObjectReturn=new ArrayList<Object>();
		for (Iterator iterator = formulate.iterator(); iterator
				.hasNext();) {
			AccountingParameterTO object = (AccountingParameterTO) iterator.next();
			listObjectReturn.add(object.getParameterName());
		}
		return listObjectReturn;
	}
	
	/**
	 * *.
	 *
	 * @param <T> the generic type
	 * @param listAccountingSchema the list accounting schema
	 * @return the list
	 */
	public static <T> List<Object> convertListAccountingSchemaObject(List<T> listAccountingSchema){
		List<Object> listObjectReturn=new ArrayList<Object>();
		for (Iterator iterator = listAccountingSchema.iterator(); iterator
				.hasNext();) {
			AccountingSchema object = (AccountingSchema) iterator.next();
			listObjectReturn.add(object.getSchemaCode());
		}
		return listObjectReturn;
	}

	/**
	 * *.
	 *
	 * @param <T> the generic type
	 * @param listAccountingAccountMatrix the list accounting account matrix
	 * @return the list
	 */
	public static <T> List<Object> convertListAccountingAccountMatrixDetailObject(List<T> listAccountingAccountMatrix){
		List<Object> listObjectReturn=new ArrayList<Object>();
		for (Iterator iterator = listAccountingAccountMatrix.iterator(); iterator
				.hasNext();) {
			AccountingMatrixDetailTo object = (AccountingMatrixDetailTo) iterator.next();
			listObjectReturn.add(object.getAccountingAccountTo().getAccountCode());
		}
		return listObjectReturn;
	}
	
	/**
	 * *.
	 *
	 * @param <T> the generic type
	 * @param formulate the formulate
	 * @return the list
	 */
	public static <T> List<Object> convertListGenericToListObject(List<T> formulate){
		List<Object> listObjectReturn=new ArrayList<Object>();
		for (Iterator iterator = formulate.iterator(); iterator
				.hasNext();) {
			Object object =  iterator.next();
			listObjectReturn.add(object);
		}
		return listObjectReturn;
	}
	
	/**
	 * *.
	 *
	 * @param a the a
	 * @param b the b
	 * @return the boolean
	 */
	public static Boolean findStringInList(String a, List<String> b){
		Boolean find=Boolean.FALSE;
		for(String bs:b){
			if( a.trim().contentEquals(bs.trim())){
				return Boolean.TRUE;
			}
		}
		return find;

	}
	
	/**
	 * Replace formulate for number with out regex.
	 *
	 * @param formulateInput the formulate input
	 * @param variableFormulateList the variable formulate list
	 * @return the string
	 */
	public static String replaceFormulateForNumberWithOutRegex(String formulateInput , List<VariableFormulate> variableFormulateList){
		    
			
		    String formulateInputValueDuplic	=	formulateInput;		   
		    String ax="";
		   
		    for (VariableFormulate variableFormulate : variableFormulateList) {
				
		    	 ax=String.copyValueOf(formulateInputValueDuplic.toCharArray(), variableFormulate.getPositionFormulate(), variableFormulate.getPositionOffSet());
		    	 formulateInput=formulateInput.replaceFirst(Pattern.quote(ax), Matcher.quoteReplacement(variableFormulate.getValueDefault().toString()));
			}
		   
		    return formulateInput;
	}
	
	/**
	 * *
	 * Get List Name AccountingParameter.
	 *
	 * @param formulaCopy the formula copy
	 * @return the name accounting parameter
	 * @return: list String with name AccountingParameter
	 */
	public static List<String>  getNameAccountingParameter(String formulaCopy){
		
		List<String> varia=new ArrayList<String>();
		List<String> listVariable=new ArrayList<String>();
		StringTokenizer stream = new StringTokenizer(formulaCopy, "+*-/^() ", true);

		 /**String Tokenizer separate and find variables*/
		 do{
			 
			 varia.add(stream.nextToken());
		 
		 }
		 while(stream.countTokens()>0);
		 
		/**I have the list and use a service that will look for the name, look for the variable "&" not going to find anything**/
		
		for (String string : varia) {
			string=string.replaceAll(Pattern.quote("|"), Matcher.quoteReplacement(" "));
			listVariable.add(string);
		}
		
		return listVariable;
	}
}
