package com.pradera.accounting.common.utils;

import java.math.BigDecimal;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.accounting.source.to.FieldSourceTo;
import com.pradera.accounting.source.to.QuerySourceTo;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.type.AccountingFieldSourceType;
import com.pradera.model.accounting.type.AccountingFieldSourceVisibilityType;
import com.pradera.model.accounting.type.AccountingFieldTypeAssociation;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class LoaderDataSourceHandler.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public  class LoaderDataSourceHandler extends DefaultHandler {
	 
	 	
	 /** The accounting source info. */
 	private AccountingSourceInfo   accountingSourceInfo ;	 
	 
 	/** The element stack. */
 	private Stack<String> 		 	elementStack = new Stack<String>();
	 
 	/** The object stack. */
 	private Stack<Object> 		 	objectStack  = new Stack<Object>();
	
	 
	 /**
 	 * *
 	 * Begin Tag Elements XML.
 	 *
 	 * @param uri the uri
 	 * @param localName the local name
 	 * @param qName the q name
 	 * @param attributes the attributes
 	 * @throws SAXException the SAX exception
 	 */
	 public void startElement(String uri, String localName,
		        String qName, Attributes attributes) throws SAXException {
		 			 
	        this.elementStack.push(qName);
	        
	        /**Tag Parent**/
	        if(PropertiesConstants.ACCOUNTING_SOURCE_INFO.equals(qName)){
	        	accountingSourceInfo = new AccountingSourceInfo();
	        }
	        else if(PropertiesConstants.ACCOUNTING_SOURCE_FIELD.equals(qName)){
	        	
	        	if (accountingSourceInfo==null) {
	        		throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_WRONG_STRUCTURED);
	        	}
	        	FieldSourceTo fieldSourceTo = new FieldSourceTo();
	          
	            for (int i = 0; i < attributes.getLength(); i++) {
	                System.out.println("             attribute: /" + attributes.getURI(i) + "/" + attributes.getLocalName(i) + "/" + attributes.getQName(i) + "/" + attributes.getType(i) + "/" + attributes.getValue(i) + "/");
	               
	                /**Validate Field attribute Structure File XML
	                 * Example:
	                 * 		<field  visibility="true" type="REFERENCE" posicion="0">
	                 * 
	                 * **/
	                
	                /**Attribute Visibility**/
	                if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_VISIBILITY)){
	                	Boolean exist=Boolean.FALSE;
	                	for (AccountingFieldSourceVisibilityType type : AccountingFieldSourceVisibilityType.list) {
							if(type.getValue().compareTo(attributes.getValue(i).toUpperCase())==0){
								exist=Boolean.TRUE;
								break;
							}
							
						}
	                	if(exist){
	                		fieldSourceTo.setVisibility(Boolean.valueOf(attributes.getValue(i)));
	                	}
	                	else{
	                		throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_VISIBILITY_FORMAT);
	                	}
	                	
	                /**Attribute type**/	
	                }else  if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_TYPE)){
	                	
	                	Boolean exist=Boolean.FALSE;
	                	for (AccountingFieldSourceType type : AccountingFieldSourceType.list) {
							if(type.getValue().compareTo(attributes.getValue(i).toUpperCase())==0){
								exist=Boolean.TRUE;
								break;
							}
							
						}
	                	if(exist){
	                		fieldSourceTo.setParameterType(attributes.getValue(i));
	                	}
	                	else{
	                		throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_FORMAT);
	                	}
	                	
	                	
	                /***Attribute position**/	
	                }else if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_POSICION)){
	                	
	                	try{
	                		fieldSourceTo.setPosicion(Integer.valueOf(attributes.getValue(i)));
	                	}
	                	catch(Exception ex){
	             		   if(ex instanceof NumberFormatException){
	             			   throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_POSITION_FORMAT);
	             		   }
	             	   }
	                	
	                /**Attribute value**/
	                }else  if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_VALUE)){
	                	fieldSourceTo.setParameterType(attributes.getValue(i));
	                }
	            }
	            	            
	            this.objectStack.push(fieldSourceTo);
	          
	        } else if(PropertiesConstants.ACCOUNTING_SOURCE_QUERY.equals(qName)){
	        	
	        	
	        	/**
	        	 * Validate Query attribute Structure File XML
	        	 * Example:
	        	 * 		<query  date="true">	
	        	 * 
	        	 * **/
	        	if (accountingSourceInfo==null) {
	        		
	        		throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_WRONG_STRUCTURED);
	        		
	        	}
	        	
	        	QuerySourceTo	querySourceTo = new  QuerySourceTo();
	        		        	
	        	 for (int i = 0; i < attributes.getLength(); i++) {
//		                System.out.println("             attribute: /" + attributes.getURI(i) + "/" + attributes.getLocalName(i) + "/" + attributes.getQName(i) + "/" + attributes.getType(i) + "/" + attributes.getValue(i) + "/");
		               
	        		 /**Attribute Visibility**/
	                if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_QUERY_ASSOCIATION)){
	                	Boolean exist=Boolean.FALSE;
	                	/***
	                	 * If association is TRUE,
	                	 * association if the parameter type of association 
	                	 * which is the parameter of the query statement.
	                	 */
	                	for (AccountingFieldSourceVisibilityType type : AccountingFieldSourceVisibilityType.list) {
							if(type.getValue().compareTo(attributes.getValue(i).toUpperCase())==0){
								exist=Boolean.TRUE;
								break;
							}
							
						}
	                	if(exist){
	                		querySourceTo.setAssociate(Boolean.valueOf(attributes.getValue(i)));
	                	}
	                	else{
	                		throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_VISIBILITY_FORMAT);
	                	}
	                	
	                /**Attribute type associate**/	
	                }
	                else if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_ASSOCIATION)){
	                	Boolean exist=Boolean.FALSE;
	                	/***
	                	 * If association was TRUE
	                	 */
	                	if(querySourceTo.getAssociate()){
	                		for (AccountingFieldTypeAssociation type : AccountingFieldTypeAssociation.list) {
								if(type.getValue().compareTo(attributes.getValue(i).toUpperCase())==0){
									exist=Boolean.TRUE;
									break;
								}
								
							}
	                	}else{
	                		continue;
	                	}
	                	
	                	if(exist){
	                		querySourceTo.setTypeAssociate(attributes.getValue(i));
	                	}
	                	else{
	                		throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_TYPE_ASSOCIATE);
	                	}
	                	
	                
	                }
	                /**Attribute Currency **/
	                else if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_CURRENCY)){
	                	Boolean exist=Boolean.FALSE;
	                	/***
	                	 * If association is TRUE,
	                	 * association if the parameter type of association 
	                	 * which is the parameter of the query statement.
	                	 */
	                	for (AccountingFieldSourceVisibilityType type : AccountingFieldSourceVisibilityType.list) {
							if(type.getValue().compareTo(attributes.getValue(i).toUpperCase())==0){
								exist=Boolean.TRUE;
								break;
							}
							
						}
	                	if(exist){
	                		querySourceTo.setIsCurrency(Boolean.valueOf(attributes.getValue(i)));
	                	}
	                	else{
	                		throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_VISIBILITY_FORMAT);
	                	}
	                	
	                	
	                }
	                /**Attribute Instrument Type**/
	                else if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_INSTRUMENT_TYPE)){
	                	Boolean exist=Boolean.FALSE;
	                	/***
	                	 * If association is TRUE,
	                	 * association if the parameter type of association 
	                	 * which is the parameter of the query statement.
	                	 */
	                	for (AccountingFieldSourceVisibilityType type : AccountingFieldSourceVisibilityType.list) {
							if(type.getValue().compareTo(attributes.getValue(i).toUpperCase())==0){
								exist=Boolean.TRUE;
								break;
							}
							
						}
	                	if(exist){
	                		querySourceTo.setIsInstrument(Boolean.valueOf(attributes.getValue(i)));;
	                	}
	                	else{
	                		throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_VISIBILITY_FORMAT);
	                	}
	                	
	                /**Attribute type associate**/	
	                }
	                else if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.ACCOUNTING_SOURCE_DATE)){		                	
		                	querySourceTo.setDate(Boolean.valueOf(attributes.getValue(i)));
		                }
		                
		                
		          }
	        	
	        	 this.objectStack.push(querySourceTo);
	        }
	 }
	 
	 /**
 	 * End Tag Elements File XML.
 	 *
 	 * @param uri the uri
 	 * @param localName the local name
 	 * @param qName the q name
 	 * @throws SAXException the SAX exception
 	 */
	 public void endElement(String uri, String localName,
		        String qName) throws SAXException {

        this.elementStack.pop();

        if(PropertiesConstants.ACCOUNTING_SOURCE_FIELD.equals(qName)){
        	
        	Object object = this.objectStack .pop();
        	FieldSourceTo fieldSourceTo = (FieldSourceTo) object;
            
            this.accountingSourceInfo.setFieldSourceTo(fieldSourceTo.getPosicion(), fieldSourceTo);
            
        }else if(PropertiesConstants.ACCOUNTING_SOURCE_NAME.equals(qName)){
        }else if(PropertiesConstants.ACCOUNTING_SOURCE_DESCRIPTION.equals(qName)){
        	
        }else if(PropertiesConstants.ACCOUNTING_SOURCE_VALUE.equals(qName)){
        	
        }else if(PropertiesConstants.ACCOUNTING_SOURCE_QUERY.equals(qName)){
        	   
        	Object object = this.objectStack .pop();
        	QuerySourceTo querySource =  (QuerySourceTo) object;
        	this.accountingSourceInfo.setQuerySource(querySource);
        	
        }else if(PropertiesConstants.ACCOUNTING_SOURCE_INFO.equals(qName)){
        	
        }
        
	 }
	 
	 /* (non-Javadoc)
 	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
 	 */
 	public void characters(char ch[], int start, int length)
		        throws SAXException {

		 /**Value: Content between the <> and </> **/
        String value = new String(ch, start, length);
        if(value.length() == 0) return; // ignore white space
        
        /**Tag parent <field> **/
        if (PropertiesConstants.ACCOUNTING_SOURCE_FIELD.equals(currentElementParent())){
        	
        	/**Tag <name> **/
           if(PropertiesConstants.ACCOUNTING_SOURCE_NAME.equals(currentElement())){

           	FieldSourceTo fieldSourceTo = (FieldSourceTo) this.objectStack.peek();
           	fieldSourceTo.setName(  (fieldSourceTo.getName() != null ? fieldSourceTo.getName()  : "") .concat(value));
           
           	/**Tag <description>**/
           }else  if(PropertiesConstants.ACCOUNTING_SOURCE_DESCRIPTION.equals(currentElement())){

           	FieldSourceTo fieldSourceTo = (FieldSourceTo) this.objectStack.peek();
           	fieldSourceTo.setDescription((fieldSourceTo.getDescription() != null ? fieldSourceTo.getDescription()  : "") .concat(value)) ;
          
           	/**Tag <value>**/
           }else  if(PropertiesConstants.ACCOUNTING_SOURCE_VALUE.equals(currentElement())){

        	   
        	   try{
        		   BigDecimal valueBigDecimal=new BigDecimal(value);
        		   FieldSourceTo fieldSourceTo = (FieldSourceTo) this.objectStack.peek();
                   fieldSourceTo.setValue( (fieldSourceTo.getValue() != null ? fieldSourceTo.getValue()  : "") .concat(value));
        	   }
        	   catch(Exception ex){
        		   if(ex instanceof NumberFormatException){
        			   throw new SAXException(PropertiesConstants.ACCOUNTING_SOURCE_VALUE_FORMAT);
        		   }
        	   																																																																																																							
        	   }
        	   
              	
            }
        }

        
        /**Contains label <query>, includes the query and the attribute date**/
       if(PropertiesConstants.ACCOUNTING_SOURCE_QUERY.equals(currentElement()) &&
          PropertiesConstants.ACCOUNTING_SOURCE_INFO.equals(currentElementParent())){
    	       	   
    	   Object object = this.objectStack.peek();
       	   QuerySourceTo querySource =  (QuerySourceTo) object;
    	 
       	  if (Validations.validateIsNullOrEmpty(querySource.getQuery())){ 
       		querySource.setQuery(value);
    	  }else {    		 
    		  String queryOld = querySource.getQuery();
    		  if(start==0){
    			  querySource.setQuery(queryOld.concat(value));
    		  }else{
    			  querySource.setQuery(queryOld.concat(" ").concat(value));
    		  }
    		  
    	  }
       }
		      
	}
	 
	 /**
 	 * Current element.
 	 *
 	 * @return the string
 	 */
 	private String currentElement() {
	    return this.elementStack.peek();
	 }

	 /**
 	 * Current element parent.
 	 *
 	 * @return the string
 	 */
 	private String currentElementParent() {
	    if(this.elementStack.size() < 2) return null;
	    return this.elementStack.get(this.elementStack.size()-2);
	 }


	 
     /**
      * Gets the accounting source info.
      *
      * @return the accounting source info
      */
     public   AccountingSourceInfo getAccountingSourceInfo(){    	 
    	 return this.accountingSourceInfo;
     }
     
     
     /**
      * Clean accounting source info.
      */
     public void cleanAccountingSourceInfo(){
    	 this.accountingSourceInfo=null;
     }
     
     
}
