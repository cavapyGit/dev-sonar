package com.pradera.accounting.matrix.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.model.accounting.AccountingMatrix;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingMatrixTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingMatrixTo implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 0xfc167ee37c4c7cb6L;
    
    /** The id accounting matrix pk. */
    private Long 	idAccountingMatrixPk;
    
    /** The accounting schema fk. */
    private Long 	accountingSchemaFk;
    
    /** The status. */
    private Integer status;
    
    /** The auxiliary type. */
    private Integer auxiliaryType;
    
    /** The file source name. */
    private String 	fileSourceName;
    
    /** The description source. */
    private String 	descriptionSource;
    
    /** The status schema. */
    private Integer statusSchema;
    
    /** The Tipo de Inicio Contable. */
    private Integer startType;
    
    /** The accounting schema to. */
    private AccountingSchemaTo 				accountingSchemaTo;
    
    /** The accounting matrix list. */
    private List<AccountingMatrix> 			accountingMatrixList ;
    
    /** The accounting matrix detail. */
    private List<AccountingMatrixDetailTo> 	accountingMatrixDetail;
    
	/**
	 * Gets the id accounting matrix pk.
	 *
	 * @return the idAccountingMatrixPk
	 */
	public Long getIdAccountingMatrixPk() {
		return idAccountingMatrixPk;
	}
	
	/**
	 * Sets the id accounting matrix pk.
	 *
	 * @param idAccountingMatrixPk the idAccountingMatrixPk to set
	 */
	public void setIdAccountingMatrixPk(Long idAccountingMatrixPk) {
		this.idAccountingMatrixPk = idAccountingMatrixPk;
	}
	
	/**
	 * Gets the accounting schema fk.
	 *
	 * @return the accountingSchemaFk
	 */
	public Long getAccountingSchemaFk() {
		return accountingSchemaFk;
	}
	
	/**
	 * Sets the accounting schema fk.
	 *
	 * @param accountingSchemaFk the accountingSchemaFk to set
	 */
	public void setAccountingSchemaFk(Long accountingSchemaFk) {
		this.accountingSchemaFk = accountingSchemaFk;
	}
	
	/**
	 * Gets the accounting matrix detail.
	 *
	 * @return the accountingMatrixDetail
	 */
	public List<AccountingMatrixDetailTo> getAccountingMatrixDetail() {
		return accountingMatrixDetail;
	}
	
	/**
	 * Sets the accounting matrix detail.
	 *
	 * @param accountingMatrixDetail the accountingMatrixDetail to set
	 */
	public void setAccountingMatrixDetail(
			List<AccountingMatrixDetailTo> accountingMatrixDetail) {
		this.accountingMatrixDetail = accountingMatrixDetail;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * Gets the auxiliary type.
	 *
	 * @return the auxiliaryType
	 */
	public Integer getAuxiliaryType() {
		return auxiliaryType;
	}
	
	/**
	 * Sets the auxiliary type.
	 *
	 * @param auxiliaryType the auxiliaryType to set
	 */
	public void setAuxiliaryType(Integer auxiliaryType) {
		this.auxiliaryType = auxiliaryType;
	}
	
	/**
	 * Gets the file source name.
	 *
	 * @return the fileSourceName
	 */
	public String getFileSourceName() {
		return fileSourceName;
	}
	
	/**
	 * Sets the file source name.
	 *
	 * @param fileSourceName the fileSourceName to set
	 */
	public void setFileSourceName(String fileSourceName) {
		this.fileSourceName = fileSourceName;
	}
	
	/**
	 * Gets the description source.
	 *
	 * @return the descriptionSource
	 */
	public String getDescriptionSource() {
		return descriptionSource;
	}
	
	/**
	 * Sets the description source.
	 *
	 * @param descriptionSource the descriptionSource to set
	 */
	public void setDescriptionSource(String descriptionSource) {
		this.descriptionSource = descriptionSource;
	}
	
	/**
	 * Gets the accounting schema to.
	 *
	 * @return the accountingSchemaTo
	 */
	public AccountingSchemaTo getAccountingSchemaTo() {
		return accountingSchemaTo;
	}
	
	/**
	 * Sets the accounting schema to.
	 *
	 * @param accountingSchemaTo the accountingSchemaTo to set
	 */
	public void setAccountingSchemaTo(AccountingSchemaTo accountingSchemaTo) {
		this.accountingSchemaTo = accountingSchemaTo;
	}
	
	/**
	 * Gets the accounting matrix list.
	 *
	 * @return the accountingMatrixList
	 */
	public List<AccountingMatrix> getAccountingMatrixList() {
		return accountingMatrixList;
	}
	
	/**
	 * Sets the accounting matrix list.
	 *
	 * @param accountingMatrixList the accountingMatrixList to set
	 */
	public void setAccountingMatrixList(List<AccountingMatrix> accountingMatrixList) {
		this.accountingMatrixList = accountingMatrixList;
	}
	
	/**
	 * Gets the status schema.
	 *
	 * @return the statusSchema
	 */
	public Integer getStatusSchema() {
		return statusSchema;
	}
	
	/**
	 * Sets the status schema.
	 *
	 * @param statusSchema the statusSchema to set
	 */
	public void setStatusSchema(Integer statusSchema) {
		this.statusSchema = statusSchema;
	}

	/**
	 * @return the startType
	 */
	public Integer getStartType() {
		return startType;
	}

	/**
	 * @param startType the startType to set
	 */
	public void setStartType(Integer startType) {
		this.startType = startType;
	}
    
    
    
    
   
    
    
}
