package com.pradera.accounting.matrix.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounting.account.facade.AccountAccountingServiceFacade;
import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.accounting.document.to.AccountingProcessTo;
import com.pradera.accounting.formulate.to.VariableFormulateTo;
import com.pradera.accounting.matrix.service.AccountinMatrixServiceBean;
import com.pradera.accounting.matrix.to.AccountingMatrixDetailTo;
import com.pradera.accounting.matrix.to.AccountingMatrixTo;
import com.pradera.accounting.parameter.facade.ParameterAccountingServiceFacade;
import com.pradera.accounting.schema.facade.SchemaAccountingServiceFacade;
import com.pradera.accounting.schema.to.AccountingSchemaTo;
import com.pradera.accounting.source.facade.SourceAccountingServiceFacade;
import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.accounting.source.to.FieldSourceTo;
import com.pradera.accounting.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounting.account.to.AccountingSourceTo;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingFormulate;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingMatrixDetail;
import com.pradera.model.accounting.AccountingMonthResult;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.AccountingSource;
import com.pradera.model.accounting.AccountingSourceDetail;
import com.pradera.model.accounting.AccountingSourceLogger;
import com.pradera.model.accounting.VariableFormulate;
import com.pradera.model.accounting.type.AccountingFieldSourceType;
import com.pradera.model.accounting.type.AccountingMatrixDetailStateType;
import com.pradera.model.accounting.type.AccountingMatrixStateType;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingSourceStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingMatrixServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AccountingMatrixServiceFacade  implements Serializable {  


    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The user info. */
    @Inject
	UserInfo userInfo;

	/** The log. */
	@Inject
	PraderaLogger log;
    
    /** The account accounting service facade. */
    @EJB
    AccountAccountingServiceFacade		accountAccountingServiceFacade;
    
    /** The accountin matrix service bean. */
    @EJB
    AccountinMatrixServiceBean 			accountinMatrixServiceBean;
    
    /** The schema accounting service facade. */
    @EJB
    SchemaAccountingServiceFacade 		schemaAccountingServiceFacade;
    
    /** The source accounting service facade. */
    @EJB
    SourceAccountingServiceFacade 		sourceAccountingServiceFacade;

    /** The parameter accounting service facade. */
    @EJB
	ParameterAccountingServiceFacade 	parameterAccountingServiceFacade;
    
    /** The transaction registry. */
    @Resource
	TransactionSynchronizationRegistry 	transactionRegistry;
    
    /** The loader entity service. */
    @EJB
	LoaderEntityServiceBean 			loaderEntityService;
    
    
    
    /**
     * Find accounting matrix by parameter.
     *
     * @param accountingMatrixTo the accounting matrix to
     * @return the list
     */
    @ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
    public List<AccountingMatrix> findAccountingMatrixByParameter(AccountingMatrixTo accountingMatrixTo){
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
        return accountinMatrixServiceBean.findAccountingMatrixByParameter(accountingMatrixTo);
    }

    /**
     * Gets the all accounting matrix detailby id.
     *
     * @param idAccountingMatrixDetailPk the id accounting matrix detail pk
     * @return the all accounting matrix detailby id
     */
    public AccountingMatrixDetail getAllAccountingMatrixDetailbyId(Long idAccountingMatrixDetailPk){
    
        return null;
    }
    
    /**
     * Find accounting account on matrix detail.
     *
     * @param accontingAccountId the acconting account id
     * @return the list
     */
    @ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
    public List<AccountingSchema> findAccountingAccountOnMatrixDetail(Long accontingAccountId){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
			
		List<AccountingSchema> listAccountingSchema = new ArrayList<AccountingSchema>();
    	
		listAccountingSchema = accountinMatrixServiceBean.findAccountingAccountOnMatrixDetail(accontingAccountId);
    	
    	return listAccountingSchema;
    }
    
    /**
     * *
     * Save AccountingMatrix With AccountingMatrixDetail.
     *
     * @param accountingMatrixTo the accounting matrix to
     * @return AccountingMatrix
     * @throws ServiceException the service exception
     */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public AccountingMatrix saveAccountingMatrix(AccountingMatrixTo accountingMatrixTo)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		
		/**
		 *    SAVE MATRIX ACCOUNTING  
		 *	  Note. This Transaction is unique within operation.    
		 */
		
		/** Load Matrix Accounting **/
		AccountingMatrix accountingMatrixNew	= new AccountingMatrix();
		AccountingSchema accountingSchema		= schemaAccountingServiceFacade.getAccountingSchemaByPk(accountingMatrixTo.getAccountingSchemaFk());
		accountingMatrixNew.setAccountingSchema(accountingSchema);
		accountingMatrixNew.setAuxiliaryType(accountingMatrixTo.getAuxiliaryType());
		accountingMatrixNew.setStatus(accountingMatrixTo.getStatus());
		
		
		accountingMatrixNew	=accountinMatrixServiceBean.saveAccountingMatrix(accountingMatrixNew);
		
		List<AccountingMatrixDetailTo> accountingMatrixDetailList = accountingMatrixTo.getAccountingMatrixDetail();
		
		if (Validations.validateListIsNotNullAndNotEmpty(accountingMatrixDetailList)){
			
			/** SETTING MATRIX EACH MATRIX DETAIL **/
			for (AccountingMatrixDetailTo accountingMatrixDetailTo : accountingMatrixDetailList) {
				accountingMatrixDetailTo.setAccountingMatrix(accountingMatrixNew);
			}
			
			saveAccountingMatrixDetail(accountingMatrixDetailList);
		}
		
		return accountingMatrixNew;
	}
	
	/**
	 * *
	 * Save Schema with Matrix and Matrix Detail.
	 *
	 * @param accountingMatrixTo the accounting matrix to
	 * @return the accounting matrix
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public AccountingMatrix saveAccountingSchemaWithMatrix(AccountingMatrixTo accountingMatrixTo)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	
	
		AccountingSchema accountingSchema = new AccountingSchema();
		AccountingSchemaTo accountingSchemaTo=accountingMatrixTo.getAccountingSchemaTo();
	
		accountingSchema.setSchemaCode(accountingSchemaTo.getSchemaCode());
		accountingSchema.setDescription(accountingSchemaTo.getDescription());
		accountingSchema.setStatus(accountingSchemaTo.getStatus());
		accountingSchema.setBranchType(accountingSchemaTo.getBranchType());
		accountingSchema.setAuxiliaryType(accountingSchemaTo.getAuxiliaryType());
		accountingSchema.setAssociateCode(accountingSchemaTo.getAssociateCode());
		schemaAccountingServiceFacade.saveAccountingSchema(accountingSchema);
		
			
		// When is new - 1st time to save matrix
		accountingMatrixTo.setAccountingSchemaFk(accountingSchema.getIdAccountingSchemaPk());
		accountingMatrixTo.setAuxiliaryType(accountingSchema.getAuxiliaryType());
		accountingMatrixTo.setStatus(AccountingMatrixStateType.REGISTERED.getCode());
		/**
		 *    SAVE MATRIX ACCOUNTING  
		 *	  Note. This Transaction is unique within operation.    
		 */
		
		/** Load Matrix Accounting **/
		AccountingMatrix accountingMatrixNew	= new AccountingMatrix();
		accountingMatrixNew.setAccountingSchema(accountingSchema);
		accountingMatrixNew.setAuxiliaryType(accountingMatrixTo.getAuxiliaryType());
		accountingMatrixNew.setStatus(accountingMatrixTo.getStatus());
		
		
		accountingMatrixNew	=accountinMatrixServiceBean.saveAccountingMatrix(accountingMatrixNew);
		
		List<AccountingMatrixDetailTo> accountingMatrixDetailList = accountingMatrixTo.getAccountingMatrixDetail();
		
		if (Validations.validateListIsNotNullAndNotEmpty(accountingMatrixDetailList)){
			
			/** SETTING MATRIX EACH MATRIX DETAIL **/
			for (AccountingMatrixDetailTo accountingMatrixDetailTo : accountingMatrixDetailList) {
				accountingMatrixDetailTo.setAccountingMatrix(accountingMatrixNew);
			}
			
			saveAccountingMatrixDetail(accountingMatrixDetailList);
		}
		
		return accountingMatrixNew;
	}
	
	/**
	 *    SAVE JUST MATRIX DETAIL - 2nd TIME                              idAccountingMatrix
	 * 	  Note. This Transaction is unique within operation.
	 *
	 * @param accountingMatrixTo the accounting matrix to
	 * @param accountingSchema the accounting schema
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void saveAccountingMatrixDetail(AccountingMatrixTo accountingMatrixTo, AccountingSchema accountingSchema) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		/**
		 *    SAVE JUST MATRIX DETAIL - 2nd TIME                              idAccountingMatrix
		 *	  Note. This Transaction is unique within operation.    
		 */
		
		List<AccountingMatrixDetailTo> ListAccountingMatrixDetailExist = schemaAccountingServiceFacade.findAccountingMatrixDetailBySchemaFilter(accountingSchema);
		List<AccountingMatrixDetailTo> ListAccountingMatrixDetailExistAndTemp = accountingMatrixTo.getAccountingMatrixDetail(); 

		
		List<AccountingMatrixDetailTo> listAccountingMatrixDetailToTemp = AccountingUtils.cloneList(ListAccountingMatrixDetailExistAndTemp);
		Collection<AccountingMatrixDetailTo> collectionRemove = new ArrayList<AccountingMatrixDetailTo>();

			for(AccountingMatrixDetailTo accountingMatrixDetailToTemp : listAccountingMatrixDetailToTemp){
				if(Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetailToTemp.getAccountingMatrixFk()))
					collectionRemove.add(accountingMatrixDetailToTemp);
			}

		ListAccountingMatrixDetailExistAndTemp.removeAll(collectionRemove);
		ListAccountingMatrixDetailExistAndTemp=AccountingUtils.cloneList(ListAccountingMatrixDetailExistAndTemp);

		//Set MatrixPk on ListAccountingMatrixDetailExistAndTemp
		
		for(AccountingMatrixDetailTo accountingMatrixDetailTo : ListAccountingMatrixDetailExistAndTemp){
			AccountingMatrix accountingMatrixTemp = new AccountingMatrix();
			accountingMatrixTemp.setIdAccountingMatrixPk(ListAccountingMatrixDetailExist.get(0).getAccountingMatrixFk());
			accountingMatrixDetailTo.setAccountingMatrix(accountingMatrixTemp);
		}
		
		saveAccountingMatrixDetail(ListAccountingMatrixDetailExistAndTemp);
	}
	
	
	
	/**
	 *  SAVE MATRIX DETAIL WITH FORMULATE ACCOUNTING.
	 *
	 * @param accountingMatrixDetailToList the accounting matrix detail to list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void saveAccountingMatrixDetail(List<AccountingMatrixDetailTo> accountingMatrixDetailToList)throws ServiceException{
		
		
		AccountingFormulate accountingFormulate = null;
		VariableFormulate	variableFormulate	= null;
		AccountingSource	accountingSource	= null;
		AccountingAccount	accountingAccount	= null;
		
		AccountingMatrixDetail accountingMatrixDetail = null;
		
		
		List<VariableFormulateTo> variableFormulateToList	= null;
		List<VariableFormulate>   variableFormulateList		= new ArrayList<VariableFormulate>();
		
		for (AccountingMatrixDetailTo accountingMatrixDetailTo : accountingMatrixDetailToList) {
				
				accountingMatrixDetail = new AccountingMatrixDetail();
				
				
				
				/** BEGIN SAVE ACCOUNTING FORMULATE ***/
				accountingFormulate = new AccountingFormulate();
				accountingFormulate.setFormulateInput(accountingMatrixDetailTo.getAccountingFormulateTo().getFormulateInput());				
				variableFormulateToList	= accountingMatrixDetailTo.getListVariableFormulateTo();
				
				for (VariableFormulateTo variableFormulateTo : variableFormulateToList) {
					
					variableFormulate = new VariableFormulate();
					variableFormulate.setAccountingFormulate(accountingFormulate);
					variableFormulate.setParameterFormulate(variableFormulateTo.getParameterFormulate().getIdAccountingParameterPk());
					variableFormulate.setPositionFormulate(variableFormulateTo.getPositionFormulate());
					variableFormulate.setPositionOffSet(variableFormulateTo.getPositionOffSet());		
					variableFormulate.setSourceType(variableFormulateTo.getSourceType());
					if (AccountingFieldSourceType.get(variableFormulateTo.getSourceType()).getValue().
							equalsIgnoreCase(AccountingFieldSourceType.VALUE.getValue())){
						
						if (Validations.validateIsNotNull(variableFormulateTo.getValueDefault()) ){							
							BigDecimal valueDefault=new BigDecimal(variableFormulateTo.getValueDefault().toString());							
							if (valueDefault.doubleValue()>0){
								variableFormulate.setValueDefault(variableFormulateTo.getValueDefault());
							}
														
						}
						
						if (Validations.validateIsNotNull(variableFormulateTo.getValueSource()) ){
							
							BigDecimal valueDefault=new BigDecimal(variableFormulateTo.getValueSource().toString());							
							if (valueDefault.doubleValue()>0){
								variableFormulate.setValueDefault(variableFormulateTo.getValueSource());
							}
							
						}											
					}
					
					variableFormulate.setPositionSource(variableFormulateTo.getPositionSource());
					variableFormulate.setVariableSource(variableFormulateTo.getVariableSource());
					
					
					variableFormulateList.add(variableFormulate);
				}
				
				accountingFormulate.setVariableFormulates(variableFormulateList);
				accountingFormulate = accountinMatrixServiceBean.saveAccountingFormulate(accountingFormulate);
				
				/*** END SAVE ACCOUNTING FORMULATE***/														
				accountingMatrixDetail.setAccountingFormulate(accountingFormulate);
				
				
				accountingAccount	= accountAccountingServiceFacade.findAccountingAccountById(accountingMatrixDetailTo.getAccountingAccountTo().getIdAccountingAccountPk());
				accountingMatrixDetail.setAccountingAccount(accountingAccount);
				accountingMatrixDetail.setDynamicType(accountingMatrixDetailTo.getDynamicType());
				accountingSource	= schemaAccountingServiceFacade.findAccountingSourceFileByCode(accountingMatrixDetailTo.getAccountingSourceFk());
				accountingMatrixDetail.setAccountingSource(accountingSource);
				
				accountingMatrixDetail.setAccountingMatrix(accountingMatrixDetailTo.getAccountingMatrix());
				accountingMatrixDetail.setStatus(AccountingMatrixDetailStateType.REGISTERED.getCode());
				accountinMatrixServiceBean.saveAccountingMatrixDetail(accountingMatrixDetail);
		}
	}
	
	
	/**
	 * Modify accounting matrix detail.
	 *
	 * @param accountingMatrixDetailToList the accounting matrix detail to list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void modifyAccountingMatrixDetail(List<AccountingMatrixDetailTo> accountingMatrixDetailToList)throws ServiceException{
		
		
		AccountingFormulate accountingFormulate = null;
		VariableFormulate	variableFormulate	= null;
		AccountingSource	accountingSource	= null;
		AccountingAccount	accountingAccount	= null;
		
		AccountingMatrixDetail accountingMatrixDetail = null;
		
		
		List<VariableFormulateTo> variableFormulateToList	= null;
		List<VariableFormulate>   variableFormulateList		= new ArrayList<VariableFormulate>();
		
		for (AccountingMatrixDetailTo accountingMatrixDetailTo : accountingMatrixDetailToList) {
				
				accountingMatrixDetail = new AccountingMatrixDetail();
				
				
				
				/** BEGIN SAVE ACCOUNTING FORMULATE ***/
				accountingFormulate = new AccountingFormulate();
				accountingFormulate.setFormulateInput(accountingMatrixDetailTo.getAccountingFormulateTo().getFormulateInput());				
				variableFormulateToList	= accountingMatrixDetailTo.getListVariableFormulateTo();
				
				for (VariableFormulateTo variableFormulateTo : variableFormulateToList) {
					
					variableFormulate = new VariableFormulate();
					variableFormulate.setAccountingFormulate(accountingFormulate);
					variableFormulate.setParameterFormulate(variableFormulateTo.getParameterFormulate().getIdAccountingParameterPk());
					variableFormulate.setPositionFormulate(variableFormulateTo.getPositionFormulate());
					variableFormulate.setPositionOffSet(variableFormulateTo.getPositionOffSet());		
					variableFormulate.setSourceType(variableFormulateTo.getSourceType());
					if (AccountingFieldSourceType.get(variableFormulateTo.getSourceType()).getValue().
							equalsIgnoreCase(AccountingFieldSourceType.VALUE.getValue())){
						
						if (Validations.validateIsNotNull(variableFormulateTo.getValueDefault()) ){							
							BigDecimal valueDefault=new BigDecimal(variableFormulateTo.getValueDefault().toString());							
							if (valueDefault.doubleValue()>0){
								variableFormulate.setValueDefault(variableFormulateTo.getValueDefault());
							}
														
						}
						
						if (Validations.validateIsNotNull(variableFormulateTo.getValueSource()) ){
							
							BigDecimal valueDefault=new BigDecimal(variableFormulateTo.getValueSource().toString());							
							if (valueDefault.doubleValue()>0){
								variableFormulate.setValueDefault(variableFormulateTo.getValueSource());
							}
							
						}											
					}
					
					variableFormulate.setPositionSource(variableFormulateTo.getPositionSource());
					variableFormulate.setVariableSource(variableFormulateTo.getVariableSource());
					
					
					variableFormulateList.add(variableFormulate);
				}
				
				accountingFormulate.setVariableFormulates(variableFormulateList);
				accountingFormulate = accountinMatrixServiceBean.saveAccountingFormulate(accountingFormulate);
				
				/*** END SAVE ACCOUNTING FORMULATE***/														
				accountingMatrixDetail.setAccountingFormulate(accountingFormulate);
				
				
				accountingAccount	= accountAccountingServiceFacade.findAccountingAccountById(accountingMatrixDetailTo.getAccountingAccountTo().getIdAccountingAccountPk());
				accountingMatrixDetail.setAccountingAccount(accountingAccount);
				accountingMatrixDetail.setDynamicType(accountingMatrixDetailTo.getDynamicType());
				accountingSource	= schemaAccountingServiceFacade.findAccountingSourceFileByCode(accountingMatrixDetailTo.getAccountingSourceFk());
				accountingMatrixDetail.setAccountingSource(accountingSource);
				
				accountingMatrixDetail.setAccountingMatrix(accountingMatrixDetailTo.getAccountingMatrix());
				accountingMatrixDetail.setStatus(AccountingMatrixDetailStateType.REGISTERED.getCode());
				accountinMatrixServiceBean.saveAccountingMatrixDetail(accountingMatrixDetail);
		}
	}	
	
	/**
	 * Load all file source.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the map
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)	
	public Map<Long,AccountingSourceInfo> loadAllFileSource(AccountingProcessTo accountingProcessTo){
		
		Map<Long,AccountingSourceInfo> mapSourceInfo=new HashMap<Long,AccountingSourceInfo>();
		AccountingSourceInfo accountingSourceInfo=new AccountingSourceInfo();
		AccountingSourceTo accountingTo=new AccountingSourceTo();
		accountingTo.setAuxiliaryType(accountingProcessTo.getProcessType());
		accountingTo.setStatus(AccountingSourceStateType.REGISTERED.getCode());
		List<AccountingSource> listSource=sourceAccountingServiceFacade.loadAccountingSource(accountingTo);
		Map<Long, Map<String, ErrorServiceType>> totalErrorsSource=new HashMap<>();

		for (AccountingSource accountingSource : listSource) {
			
			try {
				
				accountingSourceInfo=loadAccountingSourceInfoById(accountingSource.getIdAccountingSourcePk());
				mapSourceInfo.put(accountingSource.getIdAccountingSourcePk(), accountingSourceInfo);
				
			} catch (ServiceException se) {
				
				String description=accountingSource.getIdAccountingSourcePk().toString().concat(GeneralConstants.HYPHEN.concat(accountingSource.getDescriptionSource()));
				
				if(ErrorServiceType.ACCOUNTING_SOURCE_WRONG_STRUCTURED.equals(se.getErrorService())||
						ErrorServiceType.ACCOUNTING_SOURCE_VALUE_FORMAT.equals(se.getErrorService())||
						ErrorServiceType.ACCOUNTING_SOURCE_VISIBILITY_FORMAT.equals(se.getErrorService())||
						ErrorServiceType.ACCOUNTING_SOURCE_TYPE_FORMAT.equals(se.getErrorService())||
						ErrorServiceType.ACCOUNTING_SOURCE_POSITION_FORMAT.equals(se.getErrorService())||
						ErrorServiceType.ACCOUNTING_SOURCE_TYPE_ASSOCIATE.equals(se.getErrorService())){
					log.error(":::::::::::::::::::::::::::::::::Fallo al leer el archivo De Fuente ");
					log.error(accountingSource.getDescriptionSource()+":::::::::::::::::::::::::::::::::::");
					
					Map<String, ErrorServiceType> error = new HashMap<>();
					error.put(description, se.getErrorService());
					totalErrorsSource.put(accountingSource.getIdAccountingSourcePk(), error);
					
					se.getMessage();
					continue;
				}
			}
			
		}

		
//		for (Entry<Long, Map<String, ErrorServiceType>> securityCod : totalErrorsSource.entrySet()){
//			Long idAccountingSourcePk=securityCod.getKey();
//			Map<String, ErrorServiceType> ratePrice = securityCod.getValue();
//			
//			for (Entry<String, ErrorServiceType> rateMap : ratePrice.entrySet()){
//				String rate= rateMap.getKey();	
//			}
//			
//		}
//		/**
//		 * Se Encontraron los siguientes errores en los archivos
//		 * Recorremos un FOR con todos los errores
//		 */
//		/**Send Notification EMAIL*/
//		 sendNotificationAccountingError(BusinessProcessType.ACCOUNTING_DOCUMENT_GENERATE,
//				 PropertiesConstants.BODY_MESSAGE_WARNING_DOCUMENT_ERROR,
//				 schemaDescription, 
//				 error);

		return mapSourceInfo;
	}
	
	
	/**
	 * Load accounting source info by id.
	 *
	 * @param id the id
	 * @return the accounting source info
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public AccountingSourceInfo	 loadAccountingSourceInfoById(Long id) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		
		byte[] file =	schemaAccountingServiceFacade.getAccountingSourceContent(id);
		return  AccountingUtils.loadXMLToObjectSource(file);
		
	}
	
	
	/**
	 * Fill accounting source logger.
	 *
	 * @param accountingSourceInfo the accounting source info
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<AccountingSourceLogger> fillAccountingSourceLogger(AccountingSourceInfo	accountingSourceInfo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingSourceLogger>  accountingSourceLoggerList = new ArrayList<AccountingSourceLogger>();
		AccountingSourceLogger 		  accountingSourceLogger = null;
		
		List<AccountingSourceDetail>  accountingSourceDetailList = null;
		AccountingSourceDetail		  accountingSourceDetail = null;
		
		List<Object> dataMap=null;

		log.info("------------------ENTRADA A LECTURA DE QUERY-----------------");
		/**  GETTING DATA FROM IDEPOSITARY DB**/
		dataMap = executeAccountingSource(accountingSourceInfo);
		log.info("------------------FIN A LECTURA DE QUERY-----------------");
		if(dataMap.size()==0){
			throw new ServiceException(ErrorServiceType.ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT);
		}
		
		/** Load list of field's position ***/
		Map<Integer, FieldSourceTo>	  fieldSourceMap	=  accountingSourceInfo.getFields();		
		
		/*** Data for each Tupla **/
		
		int numberTuplas	= dataMap.size();
		
		int numberColumns	= ( (Object[])dataMap.get(0) ).length;
		
		
		List<Integer> positionsData =  new ArrayList<Integer>();	
		for (int position = 0; position < numberColumns; position++) {
			positionsData.add(position);
		}
		
		Collection<FieldSourceTo> fieldSourceToCollection = fieldSourceMap.values();
		
		Object[] fieldSourceToArr =fieldSourceToCollection.toArray();
		

		for (int i = 0; i < numberTuplas; i++) {
			
			Object tupla = 	dataMap.get(i);
//			if (dataMap.get(4).equals("LBS-N000521439")){
//				System.out.println("VALOR NOMINAL "+dataMap.get(0));
//			}
			
			/**
			 * 	Begin Load List of Field with value  for each tupla
			 */
			
			for (int j = 0; j < fieldSourceToArr.length; j++) {
				FieldSourceTo  fieldSourceTo	= (FieldSourceTo) fieldSourceToArr[j];
				
				int positionTemp = fieldSourceTo.getPosicion();
				if (positionsData.contains(positionTemp)){
					
					if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.REFERENCE.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.HOLDER.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():" ");
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.ISSUER.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():" ");
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.PARTICIPANT.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.SECURITY.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():" ");
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.MOVEMENT_TYPE.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():" ");
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.MOVEMENT_DATE.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():" ");
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.ENTITY_COLLECTION.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.ISSUANCE.getValue())){
						fieldSourceTo.setValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():" ");
					}
													
				 }
				
				if (Validations.validateIsNullOrEmpty(fieldSourceTo.getValue())){
					fieldSourceTo.setValue(" ");//vacio
				}
			}
			
			
			
		/**
		 * 	End Begin Load List of Field with value  for each tupla
		 */
			
			
			
			
		 /**  Begin
		  *	  Create and Load List of Logger Detail for each tupla			 	
		  **/
		
			accountingSourceDetailList = new ArrayList<AccountingSourceDetail>();
			accountingSourceLogger	   = new AccountingSourceLogger();
			
			for (int j = 0; j < fieldSourceToArr.length; j++) {
				FieldSourceTo  fieldSourceTo	= (FieldSourceTo) fieldSourceToArr[j];
				
				accountingSourceDetail	= new AccountingSourceDetail();
				
				accountingSourceDetail.setParameterName(fieldSourceTo.getName());
				accountingSourceDetail.setPosition(fieldSourceTo.getPosicion());
				accountingSourceDetail.setParameterDescription(fieldSourceTo.getDescription());
				accountingSourceDetail.setParameterValue(fieldSourceTo.getValue());
				accountingSourceDetail.setParameterType( AccountingFieldSourceType.get(fieldSourceTo.getParameterType()).getCode().intValue()  );
				accountingSourceDetail.setAccountingSourceLogger(accountingSourceLogger);
				accountingSourceDetailList.add(accountingSourceDetail);
			}
								
			/**  End
			  *	 Create and Load List of Logger Detail for each tupla		 	
			  **/
			accountingSourceLogger.setAccountingSourceDetail(accountingSourceDetailList);
			accountingSourceLoggerList.add(accountingSourceLogger);
				
			
			
		}
		return accountingSourceLoggerList;
		
	}
	
	
	
	

	
	
	/**
	 * Fill accounting month result.
	 *
	 * @param accountingSourceInfo the accounting source info
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<AccountingMonthResult> fillAccountingMonthResult(AccountingSourceInfo	accountingSourceInfo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<AccountingMonthResult>  accountingMonthResultList = new ArrayList<AccountingMonthResult>();
		AccountingMonthResult 		  accountingMonthResult = null;
		
//		List<AccountingSourceDetail>  accountingSourceDetailList = null;
//		AccountingSourceDetail		  accountingSourceDetail = null;
		
		List<Object> dataMap=null;

		log.info("------------------ENTRADA A LECTURA DE QUERY-----------------");
		/**  GETTING DATA FROM IDEPOSITARY DB**/
		dataMap = executeAccountingSource(accountingSourceInfo);
		log.info("------------------FIN A LECTURA DE QUERY-----------------");
		if(dataMap.size()==0){
			throw new ServiceException(ErrorServiceType.ACCOUNTING_RECEIPT_SOURCE_INFO_NO_RESULT);
		}
		
		/** Load list of field's position ***/
		Map<Integer, FieldSourceTo>	  fieldSourceMap	=  accountingSourceInfo.getFields();		
		
		/*** Data for each Tupla **/
		
		int numberTuplas	= dataMap.size();
		
		int numberColumns	= ( (Object[])dataMap.get(0) ).length;
		
		
		List<Integer> positionsData =  new ArrayList<Integer>();	
		for (int position = 0; position < numberColumns; position++) {
			positionsData.add(position);
		}
		
		Collection<FieldSourceTo> fieldSourceToCollection = fieldSourceMap.values();
		
		Object[] fieldSourceToArr =fieldSourceToCollection.toArray();
		

		for (int i = 0; i < numberTuplas; i++) {
			
			Object tupla = 	dataMap.get(i);
			accountingMonthResult=new AccountingMonthResult();
			
			/**
			 * 	Begin Load List of Field with value  for each tupla
			 */
			
			for (int j = 0; j < fieldSourceToArr.length; j++) {
				FieldSourceTo  fieldSourceTo	= (FieldSourceTo) fieldSourceToArr[j];
				fillResultByQuery(accountingMonthResult, fieldSourceTo, tupla, positionsData);
				
			}
			
			//Calcular el monto de P*Q
			if(Validations.validateIsNotNullAndPositive(accountingMonthResult.getMovementQuantity())
					&&Validations.validateIsNotNullAndPositive(accountingMonthResult.getNominalValue())) {
				BigDecimal amount=CommonsUtilities.multiplyDecimals(accountingMonthResult.getNominalValue(), accountingMonthResult.getMovementQuantity(), 2);
				accountingMonthResult.setAmount(amount);
			}else {
				accountingMonthResult.setAmount(BigDecimal.ZERO);
			}
			
		/**
		 * 	End Begin Load List of Field with value  for each tupla
		 */
	
			accountingMonthResultList.add(accountingMonthResult);
			
		}
		return accountingMonthResultList;
		
	}
	
	public void fillResultByQuery(AccountingMonthResult accountingMonthResult, FieldSourceTo  fieldSourceTo, Object tupla, List<Integer> positionsData) {
		
		int positionTemp = fieldSourceTo.getPosicion();
		if (positionsData.contains(positionTemp) && ((Object[])tupla)[positionTemp]!=null ){
			Object value=(((Object[])tupla)[positionTemp]);
			if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.REFERENCE.getValue())){
				if(fieldSourceTo.getName().equalsIgnoreCase(AccountingFieldSourceType.MOVEMENT_QUANTITY.getValue())) {
					accountingMonthResult.setMovementQuantity(new BigDecimal(value.toString()));
				}else if(fieldSourceTo.getName().equalsIgnoreCase(AccountingFieldSourceType.NOMINAL_VALUE.getValue())) {
					accountingMonthResult.setNominalValue(new BigDecimal(value.toString()));
				}else if(fieldSourceTo.getName().equalsIgnoreCase(AccountingFieldSourceType.NOT_PLACED.getValue())) {
					accountingMonthResult.setNotPlaced(new BigDecimal(value.toString()));
				}
			}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.HOLDER.getValue())){
				accountingMonthResult.setIdHolder(Long.valueOf(value.toString()));
			}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.ISSUER.getValue())){
				accountingMonthResult.setIdIssuer(value.toString());
			}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.PARTICIPANT.getValue())){
				accountingMonthResult.setIdParticipant(Long.valueOf(value.toString()));
			}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.SECURITY.getValue())){
				accountingMonthResult.setIdSecurityCodeFk(value.toString());
			}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.MOVEMENT_TYPE.getValue())){
				accountingMonthResult.setMovementType(Integer.valueOf(value.toString()));
			}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.MOVEMENT_DATE.getValue())){
				accountingMonthResult.setMovementDate((Date)value);
			}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.ENTITY_COLLECTION.getValue())){
				accountingMonthResult.setEntityCollection(Integer.valueOf(value.toString()));
			}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(AccountingFieldSourceType.ISSUANCE.getValue())){
				accountingMonthResult.setIdIssuance(value.toString());
			}
											
		 }
	}
	
	
	
	
	/**
	 * Find accounting formulate by matrix detail id.
	 *
	 * @param id the id
	 * @return the accounting formulate
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public AccountingFormulate findAccountingFormulateByMatrixDetailId(Long id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		
		AccountingFormulate	accountingFormulate = accountinMatrixServiceBean.findAccountingFormulateByMatrixDetailId(id);						
		List<VariableFormulate>  variableFormulateList	= accountingFormulate.getVariableFormulates();
		
		loaderEntityService.detachmentEntities(variableFormulateList);
		loaderEntityService.detachmentEntity(accountingFormulate);
		
		return accountingFormulate;
	}
	
	
	/**
	 * Find list accounting matrix detail.
	 *
	 * @param id the id
	 * @return the accounting matrix detail
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)	
	public AccountingMatrixDetail findListAccountingMatrixDetail(Long id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		AccountingMatrixDetail	accountingMatrixDetail = accountinMatrixServiceBean.findListAccountingMatrixDetail(id);						
		
		return accountingMatrixDetail;
	}
	
	
	/**
	 * Execute accounting source.
	 *
	 * @param accountingSourceInfo the accounting source info
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)	
	public List<Object>  executeAccountingSource(AccountingSourceInfo accountingSourceInfo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountinMatrixServiceBean.executeAccountingSource(accountingSourceInfo);
	} 
	
	
	/**
	 * Find accounting matrix detail.
	 *
	 * @param idFk the id fk
	 * @return the accounting matrix detail
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)	
	public List<AccountingMatrixDetail> findAccountingMatrixDetail(Long idFk){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return accountinMatrixServiceBean.findAccountingMatrixDetail(idFk);
	}
	
	/**
	 * Gets the parameter auxiliary type.
	 *
	 * @return the parameter auxiliary type
	 */
	public List<AccountingParameter> getParameterAuxiliaryType(){
		AccountingParameterTO accountingParameterTo = new AccountingParameterTO();
		List<AccountingParameter> listParameterAuxiliaryType;
		
		accountingParameterTo.setParameterType(PropertiesConstants.DAILY_AUXILIARY);
		accountingParameterTo.setStatus(AccountingParameterStateType.ACTIVED.getCode());
		listParameterAuxiliaryType  =   parameterAccountingServiceFacade.findListParameterAccounting(accountingParameterTo);
		
		return listParameterAuxiliaryType;
	}

 }
