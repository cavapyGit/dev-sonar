package com.pradera.accounting.matrix.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Parameter;
import javax.persistence.Query;

import org.hibernate.exception.SQLGrammarException;

import com.pradera.accounting.matrix.to.AccountingMatrixTo;
import com.pradera.accounting.source.to.AccountingSourceInfo;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounting.AccountingFormulate;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingMatrixDetail;
import com.pradera.model.accounting.AccountingSchema;
import com.pradera.model.accounting.type.AccountingParameterInputQuery;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountinMatrixServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
public class AccountinMatrixServiceBean extends CrudDaoServiceBean {  

	/** The log. */
	@Inject
    private PraderaLogger log;
 
    /**
     * Find accounting matrix by parameter.
     *
     * @param accountingMatrixTo the accounting matrix to
     * @return the list
     */
    public List<AccountingMatrix> findAccountingMatrixByParameter(AccountingMatrixTo accountingMatrixTo) {
   
        log.info(" Starting with searching all shemas group by auxiliaryType");
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select distinct am From AccountingMatrix am  " );
        sbQuery.append("  left join fetch am.accountingMatrixDetail amd " );
        sbQuery.append("  left join fetch am.accountingSchema asch" );
        sbQuery.append("  left join fetch amd.accountingSource" );
        sbQuery.append("  left join fetch amd.accountingAccount" );
        sbQuery.append(" where 1=1 " );
        
        if(Validations.validateIsNotNull(accountingMatrixTo.getAuxiliaryType())){
        
            sbQuery.append(" and  am.auxiliaryType = :auxiliaryType ");
        }
        if(Validations.validateIsNotNullAndPositive(accountingMatrixTo.getStatus())){
            
            sbQuery.append(" and am.status = :status ");
        }
        if(Validations.validateIsNotNullAndPositive(accountingMatrixTo.getStatusSchema())){
            
            sbQuery.append(" and asch.status = :statusSchema ");
        }
        if(Validations.validateIsNotNullAndPositive(accountingMatrixTo.getStartType())){
            
            sbQuery.append(" and asch.startType = :startType ");
        }
        if(Validations.validateIsNotNull(accountingMatrixTo.getAccountingSchemaTo())&&
        		Validations.validateIsNotNull(accountingMatrixTo.getAccountingSchemaTo().getIdAccountingSchemaPk())){
            
            sbQuery.append(" and  am.accountingSchema.idAccountingSchemaPk = :idAccountingSchemaPk ");  
        }
        
        Query query = em.createQuery(sbQuery.toString());
        if(Validations.validateIsNotNull(accountingMatrixTo.getAuxiliaryType())){
        
            query.setParameter("auxiliaryType", accountingMatrixTo.getAuxiliaryType());
        }
        if(Validations.validateIsNotNullAndPositive(accountingMatrixTo.getStatus())){
            
            query.setParameter("status", accountingMatrixTo.getStatus());
        }
        if(Validations.validateIsNotNullAndPositive(accountingMatrixTo.getStatusSchema())){
            
            query.setParameter("statusSchema", accountingMatrixTo.getStatusSchema());
        }
        if(Validations.validateIsNotNullAndPositive(accountingMatrixTo.getStartType())){
            
        	query.setParameter("startType", accountingMatrixTo.getStartType());
        }
        if(Validations.validateIsNotNull(accountingMatrixTo.getAccountingSchemaTo())&&
        		Validations.validateIsNotNull(accountingMatrixTo.getAccountingSchemaTo().getIdAccountingSchemaPk())){
            
        	query.setParameter("idAccountingSchemaPk", accountingMatrixTo.getAccountingSchemaTo().getIdAccountingSchemaPk() );
        }
        
        List<AccountingMatrix> accountingMatrixList = null;
        accountingMatrixList = query.getResultList();
        return accountingMatrixList;
    }
    
    /**
     * Gets the all accounting matrix detailby id.
     *
     * @param idAccountingMatrixDetailPk the id accounting matrix detail pk
     * @return the all accounting matrix detailby id
     */
    public AccountingMatrixDetail getAllAccountingMatrixDetailbyId(Long idAccountingMatrixDetailPk){
    
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select distinct  amd From AccountingMatrixDetail amd left join fetch amd.accountingSource acs  left join fetch amd.accountingFormulate" );
        sbQuery.append(" Where  amd.idAccountingMatrixDetailPk = :idAccountingMatrixDetailPk ");
        Query query = em.createQuery(sbQuery.toString());
        query.setParameter("idAccountingMatrixDetailPk", idAccountingMatrixDetailPk);
        AccountingMatrixDetail accountingMatrixDetail = null;
        try
        {
            accountingMatrixDetail = (AccountingMatrixDetail)query.getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }
        return accountingMatrixDetail;
    }
    
    
    /**
     * Save accounting matrix.
     *
     * @param accountingMatrix the accounting matrix
     * @return the accounting matrix
     * @throws ServiceException the service exception
     */
    public AccountingMatrix saveAccountingMatrix(AccountingMatrix accountingMatrix)throws ServiceException{
    	return create(accountingMatrix);
    }
    
	/**
	 * Save accounting formulate.
	 *
	 * @param accountingFormulate the accounting formulate
	 * @return the accounting formulate
	 * @throws ServiceException the service exception
	 */
	public AccountingFormulate saveAccountingFormulate(AccountingFormulate accountingFormulate)throws ServiceException{
		return create(accountingFormulate);
	}
    
	
	/**
	 * Save accounting matrix detail.
	 *
	 * @param accountingMatrixDetail the accounting matrix detail
	 * @return the accounting matrix detail
	 * @throws ServiceException the service exception
	 */
	public AccountingMatrixDetail saveAccountingMatrixDetail(AccountingMatrixDetail accountingMatrixDetail)throws ServiceException{
		return create(accountingMatrixDetail);
	}
	
	 /**
 	 * Find accounting account on matrix detail.
 	 *
 	 * @param accontingAccountId the acconting account id
 	 * @return the list
 	 */
 	public List<AccountingSchema> findAccountingAccountOnMatrixDetail(Long accontingAccountId){
	    	
	        StringBuilder sbQuery = new StringBuilder();
	        sbQuery.append(" SELECT AC.SCHEMA_CODE FROM ACCOUNTING_MATRIX_DETAIL AMD  ");
	        sbQuery.append(" INNER JOIN ACCOUNTING_MATRIX AM ON AM.ID_ACCOUNTING_MATRIX_PK = AMD.ID_ACCOUNTING_MATRIX_FK   ");
	        sbQuery.append(" INNER JOIN ACCOUNTING_SCHEMA AC ON AC.ID_ACCOUNTING_SCHEMA_PK = AM.ID_ACCOUNTING_SCHEMA_FK  ");
	        
	        sbQuery.append(" Where 1=1 ");
	         
	        if(Validations.validateIsNotNullAndNotEmpty(accontingAccountId))
	        {
	        	 sbQuery.append(" AND  AMD.ID_ACCOUNTING_ACCOUNT_FK  = :accontingAccountFk  ");
	        }
	       
	        Query query = em.createNativeQuery(sbQuery.toString());
	        
	        if(Validations.validateIsNotNullAndNotEmpty(accontingAccountId))
	        {
	        	 query.setParameter("accontingAccountFk", accontingAccountId );
	        }
	        
			List<Object[]>  objectList = query.getResultList();
			List<AccountingSchema>  listAccountingSchema = null ;
	         
			
			if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
				listAccountingSchema = new ArrayList<AccountingSchema>();
				
				 for(int i=0;i<objectList.size();++i){ 
					   Object sResults =  objectList.get(i);
					   AccountingSchema accountingSchemaTemp=new AccountingSchema();
					   
					   accountingSchemaTemp.setSchemaCode(sResults==null?"":sResults.toString());
				 
					   listAccountingSchema.add(accountingSchemaTemp);
				   }
			   }
			   
				return listAccountingSchema;
	 }
	 
	 /**
 	 * Find accounting formulate by matrix detail id.
 	 *
 	 * @param idAccountingFormulatePk the id accounting formulate pk
 	 * @return the accounting formulate
 	 */
 	public AccountingFormulate findAccountingFormulateByMatrixDetailId(Long idAccountingFormulatePk){
		 
	 	StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select distinct  amd.accountingFormulate  From AccountingMatrixDetail amd ");
        sbQuery.append(" where  amd.idAccountingMatrixDetailPk = :idAccountingMatrixDetailPk");
        
        Query query = em.createQuery(sbQuery.toString());
        query.setParameter("idAccountingMatrixDetailPk", idAccountingFormulatePk);
        AccountingFormulate accountingFormulate = null;
        
        try {
       
        	accountingFormulate = (AccountingFormulate)query.getSingleResult();
        }
        catch(Exception e){
        
            return null;
        }
	        
	    return accountingFormulate;		 
	 }
	 
	 
	 
	 /**
 	 * Find list accounting matrix detail.
 	 *
 	 * @param idAccountingMatrixDetailPk the id accounting matrix detail pk
 	 * @return the accounting matrix detail
 	 */
 	public AccountingMatrixDetail findListAccountingMatrixDetail(Long idAccountingMatrixDetailPk){
		 
		 	StringBuilder sbQuery = new StringBuilder();
	        sbQuery.append(" Select distinct  amd  From AccountingMatrixDetail amd ");
	        sbQuery.append(" where  amd.idAccountingMatrixDetailPk = :idAccountingMatrixDetailPk");
	        
	        Query query = em.createQuery(sbQuery.toString());
	        query.setParameter("idAccountingMatrixDetailPk", idAccountingMatrixDetailPk);
	        AccountingMatrixDetail accountingMatrixDetail = null;
	        
	        try {
	       
	        	accountingMatrixDetail = (AccountingMatrixDetail)query.getSingleResult();
	        }
	        catch(Exception e){
	        
	            return null;
	        }
		        
		    return accountingMatrixDetail;		 
	}
	 
	 
	 /**
 	 * Execute accounting source.
 	 *
 	 * @param accountingSourceInfo the accounting source info
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	public List<Object>  executeAccountingSource(AccountingSourceInfo accountingSourceInfo) throws ServiceException{
		
		 
		 StringBuilder sbQuery = new StringBuilder();
		 
		 sbQuery.append(accountingSourceInfo.getQuerySource().getQuery());
		 
		
		 
		Query query = em.createNativeQuery(sbQuery.toString());
		 
		List<Object> listObject = null;
		
		Boolean hasDate = accountingSourceInfo.getQuerySource().getDate();
		Boolean hasAssociate =Boolean.FALSE;
		Boolean hasCurrency =Boolean.FALSE;
		Boolean hasInstrumentType =Boolean.FALSE;
		
		if(Validations.validateIsNotNull(accountingSourceInfo.getQuerySource().getAssociate())){
			hasAssociate=accountingSourceInfo.getQuerySource().getAssociate();
		}
		if(Validations.validateIsNotNull(accountingSourceInfo.getQuerySource().getIsCurrency())){
			hasCurrency=accountingSourceInfo.getQuerySource().getIsCurrency();
		}
		if(Validations.validateIsNotNull(accountingSourceInfo.getQuerySource().getIsInstrument())){
			hasInstrumentType=accountingSourceInfo.getQuerySource().getIsInstrument();
		}
			
		/** ASK IF SHOULD SETTING THE DATE SELECTION**/
		if (hasDate){
						
			java.util.Set<Parameter<?>> 	listParameter =  query.getParameters();			
			Object[] parameterArr = listParameter.toArray();
			for (int i = 0; i < parameterArr.length; i++) {
				Object parameterObject	= parameterArr[i];
				if(AccountingParameterInputQuery.DATE.getValue().equals(((Parameter<Date>)parameterObject).getName())){
	
					Parameter<Date> parameter = (Parameter<Date>)parameterObject;
					query.setParameter(parameter.getName(), accountingSourceInfo.getExecutionDate());
					log.debug(":::::::::Execution Date: ::::::::::::: "+accountingSourceInfo.getExecutionDate());
					break;
				}
				
			}
						
		}
		
		if (hasAssociate){
			
			java.util.Set<Parameter<?>> 	listParameter =  query.getParameters();			
			Object[] parameterArr = listParameter.toArray();
			for (int i = 0; i < parameterArr.length; i++) {
				Object parameterObject	= parameterArr[i];
			
				if(AccountingParameterInputQuery.MNEMONIC.getValue().equals(((Parameter<Date>)parameterObject).getName())){
					Parameter<String> parameter = (Parameter<String>)parameterObject;
					
					int lastPoint=accountingSourceInfo.getTypeAssociation().lastIndexOf(GeneralConstants.DOT);
					String mnemonic=accountingSourceInfo.getTypeAssociation().substring(lastPoint+1);
					query.setParameter(parameter.getName(), mnemonic);
					log.debug(":::::::::Execution Date:::::::::::::: "+mnemonic);
					break;
				}
			}
						
		}
		if(hasCurrency){
			java.util.Set<Parameter<?>> 	listParameter =  query.getParameters();			
			Object[] parameterArr = listParameter.toArray();
			for (int i = 0; i < parameterArr.length; i++) {
				Object parameterObject	= parameterArr[i];
				if(AccountingParameterInputQuery.CURRENCY.getValue().equals(((Parameter<Date>)parameterObject).getName())){
	
					Parameter<Integer> parameter = (Parameter<Integer>)parameterObject;
					query.setParameter(parameter.getName(), accountingSourceInfo.getCurrency());
					log.debug(":::::::::Execution Date:::::::::::::: "+accountingSourceInfo.getCurrency());
					break;
				}
				
			}
		}
		
		if(hasInstrumentType){
			java.util.Set<Parameter<?>> 	listParameter =  query.getParameters();			
			Object[] parameterArr = listParameter.toArray();
			for (int i = 0; i < parameterArr.length; i++) {
				Object parameterObject	= parameterArr[i];
				if(AccountingParameterInputQuery.INSTRUMENT_TYPE.getValue().equals(((Parameter<Date>)parameterObject).getName())){
	
					Parameter<Integer> parameter = (Parameter<Integer>)parameterObject;
					
					query.setParameter(parameter.getName(), accountingSourceInfo.getInstrumentType());
					log.error(":::::::::Execution Date:::::::::::::: "+accountingSourceInfo.getInstrumentType());
					break;
				}
				
			}
		}
		 			
		try {
			
			  listObject=query.getResultList();
			  
		} catch (Exception ex) {			
			if(ex.getCause() instanceof SQLGrammarException){
				throw new ServiceException(ErrorServiceType.ACCOUNTING_QUERY_GRAMMAR);
			}
			
		}
			
		return listObject;
	 }
	 
	 
	 
	 /**
 	 * Find accounting matrix detail.
 	 *
 	 * @param idFk the id fk
 	 * @return the accounting matrix detail
 	 */
 	public List<AccountingMatrixDetail> findAccountingMatrixDetail(Long idFk){
 		List<AccountingMatrixDetail> accountingMatrixDetail;
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT amd FROM AccountingMatrixDetail amd left join fetch amd.accountingMatrix  ");
		stringBuilderSql.append("  WHERE amd.accountingSource.idAccountingSourcePk="+idFk);
		
		Query query = em.createQuery(stringBuilderSql.toString());
		try{
			accountingMatrixDetail=(List<AccountingMatrixDetail>) query.getResultList();
		}
		catch(Exception e){
			accountingMatrixDetail=null;
			e.printStackTrace();
		}
		
		return accountingMatrixDetail;
	}
	 
	 
	 
 }
