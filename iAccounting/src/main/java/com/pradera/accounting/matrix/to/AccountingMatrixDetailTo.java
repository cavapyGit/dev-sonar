package com.pradera.accounting.matrix.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.accounting.formulate.to.VariableFormulateTo;
import com.pradera.core.component.accounting.account.to.AccountingFormulateTo;
import com.pradera.core.component.accounting.account.to.AccountingSourceTo;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.type.AccountingNatureAccountType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountingMatrixDetailTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class AccountingMatrixDetailTo   implements Serializable {


    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 0x86d9b6bee769ac9aL;
    
    /** The id accounting matrix detail pk. */
    private Long 		idAccountingMatrixDetailPk;
    
    /** The accounting matrix fk. */
    private Long 		accountingMatrixFk;
    
    /** The accounting account fk. */
    private Long 		accountingAccountFk;
    
    /** The accounting source fk. */
    private Long 		accountingSourceFk;
    
    /** The accounting formulate fk. */
    private Long 		accountingFormulateFk;
    
    /** The accounting formulate string. */
    private String 		accountingFormulateString;
    
    /** The dynamic type. */
    private Integer 	dynamicType;
    
    /** The associate code. */
    private Integer 	associateCode;
    
    /** The document type. */
    private Integer 	documentType;
    
    /** The area type. */
    private Integer 	areaType;
    
    /** The state. */
    private Integer 	state;
    
    /** The description dynamic type. */
    private String 		descriptionDynamicType;
    
    /** The accounting account to. */
    private AccountingAccountTo 		accountingAccountTo;
    
    /** The accounting formulate to. */
    private AccountingFormulateTo 		accountingFormulateTo;
    
    /** The accounting source to. */
    private AccountingSourceTo 			accountingSourceTo;
    
    /** The accounting parameter to. */
    private AccountingParameterTO 		accountingParameterTO;
    
    /** The accounting matrix. */
    private AccountingMatrix 			accountingMatrix;
    
    /** The list accounting parameter to. */
    private List<AccountingParameterTO> listAccountingParameterTO;
    
    /** The list variable formulate to. */
    private List<VariableFormulateTo> 	listVariableFormulateTo;
    
    /** The list accounting account tree. */
    private List<AccountingAccount> 	listAccountingAccountTree;
    
    /** The prepare for save. */
    private Boolean prepareForSave = Boolean.FALSE;

    
    
    
    
    /**
     * Instantiates a new accounting matrix detail to.
     */
    public AccountingMatrixDetailTo(){
    
    }

    /**
     * Gets the id accounting matrix detail pk.
     *
     * @return the id accounting matrix detail pk
     */
    public Long getIdAccountingMatrixDetailPk(){
    
        return idAccountingMatrixDetailPk;
    }

    /**
     * Sets the id accounting matrix detail pk.
     *
     * @param idAccountingMatrixDetailPk the new id accounting matrix detail pk
     */
    public void setIdAccountingMatrixDetailPk(Long idAccountingMatrixDetailPk){
    
        this.idAccountingMatrixDetailPk = idAccountingMatrixDetailPk;
    }

    /**
     * Gets the accounting matrix fk.
     *
     * @return the accounting matrix fk
     */
    public Long getAccountingMatrixFk(){
    
        return accountingMatrixFk;
    }

    /**
     * Sets the accounting matrix fk.
     *
     * @param accountingMatrixFk the new accounting matrix fk
     */
    public void setAccountingMatrixFk(Long accountingMatrixFk){
    
        this.accountingMatrixFk = accountingMatrixFk;
    }

    /**
     * Gets the accounting account fk.
     *
     * @return the accounting account fk
     */
    public Long getAccountingAccountFk(){
    
        return accountingAccountFk;
    }

    /**
     * Sets the accounting account fk.
     *
     * @param accountingAccountFk the new accounting account fk
     */
    public void setAccountingAccountFk(Long accountingAccountFk){
    
        this.accountingAccountFk = accountingAccountFk;
    }

    /**
     * Gets the accounting source fk.
     *
     * @return the accounting source fk
     */
    public Long getAccountingSourceFk(){
    
        return accountingSourceFk;
    }

    /**
     * Sets the accounting source fk.
     *
     * @param accountingSourceFk the new accounting source fk
     */
    public void setAccountingSourceFk(Long accountingSourceFk){
    
        this.accountingSourceFk = accountingSourceFk;
    }

    /**
     * Gets the accounting formulate fk.
     *
     * @return the accounting formulate fk
     */
    public Long getAccountingFormulateFk(){
    
        return accountingFormulateFk;
    }

    /**
     * Sets the accounting formulate fk.
     *
     * @param accountingFormulateFk the new accounting formulate fk
     */
    public void setAccountingFormulateFk(Long accountingFormulateFk){
    
        this.accountingFormulateFk = accountingFormulateFk;
    }

    /**
     * *.
     *
     * @return the accounting formulate string
     */
    public String getAccountingFormulateString(){
    
        return accountingFormulateString;
    }

    /**
     * Sets the accounting formulate string.
     *
     * @param accountingFormulateString the new accounting formulate string
     */
    public void setAccountingFormulateString(String accountingFormulateString){
    
        this.accountingFormulateString = accountingFormulateString;
    }

    /**
     * Gets the dynamic type.
     *
     * @return the dynamic type
     */
    public Integer getDynamicType(){
    
        return dynamicType;
    }

    /**
     * Sets the dynamic type.
     *
     * @param dynamicType the new dynamic type
     */
    public void setDynamicType(Integer dynamicType){
    
        this.dynamicType = dynamicType;
    }

    /**
     * *.
     *
     * @return the associate code
     */
    public Integer getAssociateCode(){
    
        return associateCode;
    }

    /**
     * *.
     *
     * @param associateCode the new associate code
     */
    public void setAssociateCode(Integer associateCode){
    
        this.associateCode = associateCode;
    }

    /**
     * Gets the document type.
     *
     * @return the document type
     */
    public Integer getDocumentType(){
    
        return documentType;
    }

    /**
     * Sets the document type.
     *
     * @param documentType the new document type
     */
    public void setDocumentType(Integer documentType){
    
        this.documentType = documentType;
    }

    /**
     * Gets the area type.
     *
     * @return the area type
     */
    public Integer getAreaType(){
    
        return areaType;
    }

    /**
     * *.
     *
     * @param areaType the new area type
     */
    public void setAreaType(Integer areaType){
    
        this.areaType = areaType;
    }

    /**
     * Gets the accounting formulate to.
     *
     * @return the accounting formulate to
     */
    public AccountingFormulateTo getAccountingFormulateTo(){
    
        return accountingFormulateTo;
    }

    /**
     * Sets the accounting formulate to.
     *
     * @param accountingFormulateTo the new accounting formulate to
     */
    public void setAccountingFormulateTo(AccountingFormulateTo accountingFormulateTo){
    
        this.accountingFormulateTo = accountingFormulateTo;
    }

    /**
     * Gets the accounting source to.
     *
     * @return the accounting source to
     */
    public AccountingSourceTo getAccountingSourceTo(){
    
        return accountingSourceTo;
    }

    /**
     * Sets the accounting source to.
     *
     * @param accountingSourceTo the new accounting source to
     */
    public void setAccountingSourceTo(AccountingSourceTo accountingSourceTo) {
   
        this.accountingSourceTo = accountingSourceTo;
    }

    /**
     * Gets the accounting parameter to.
     *
     * @return the accounting parameter to
     */
    public AccountingParameterTO getAccountingParameterTO(){
    
        return accountingParameterTO;
    }

    /**
     * Sets the accounting parameter to.
     *
     * @param accountingParameterTO the new accounting parameter to
     */
    public void setAccountingParameterTO(AccountingParameterTO accountingParameterTO){
    
        this.accountingParameterTO = accountingParameterTO;
    }

    /**
     * Gets the accounting account to.
     *
     * @return the accounting account to
     */
    public AccountingAccountTo getAccountingAccountTo(){
    
        return accountingAccountTo;
    }

    /**
     * Sets the accounting account to.
     *
     * @param accountingAccountTo the new accounting account to
     */
    public void setAccountingAccountTo(AccountingAccountTo accountingAccountTo){
    
        this.accountingAccountTo = accountingAccountTo;
    }

	/**
	 * Gets the list accounting parameter to.
	 *
	 * @return the listAccountingParameterTO
	 */
	public List<AccountingParameterTO> getListAccountingParameterTO() {
		return listAccountingParameterTO;
	}

	/**
	 * Sets the list accounting parameter to.
	 *
	 * @param listAccountingParameterTO the listAccountingParameterTO to set
	 */
	public void setListAccountingParameterTO(List<AccountingParameterTO> listAccountingParameterTO) {
		this.listAccountingParameterTO = listAccountingParameterTO;
	}
	
	/**
	 * Gets the list variable formulate to.
	 *
	 * @return the listVariableFormulateTo
	 */
	public List<VariableFormulateTo> getListVariableFormulateTo() {
		return listVariableFormulateTo;
	}

	/**
	 * Sets the list variable formulate to.
	 *
	 * @param listVariableFormulateTo the listVariableFormulateTo to set
	 */
	public void setListVariableFormulateTo(List<VariableFormulateTo> listVariableFormulateTo) {
		this.listVariableFormulateTo = listVariableFormulateTo;
	}

	/**
	 * Gets the prepare for save.
	 *
	 * @return the prepare for save
	 */
	public Boolean getPrepareForSave() {
		return prepareForSave;
	}

	/**
	 * Sets the prepare for save.
	 *
	 * @param prepareForSave the new prepare for save
	 */
	public void setPrepareForSave(Boolean prepareForSave) {
		this.prepareForSave = prepareForSave;
	}

	/**
	 * Gets the accounting matrix.
	 *
	 * @return the accounting matrix
	 */
	public AccountingMatrix getAccountingMatrix() {
		return accountingMatrix;
	}

	/**
	 * Sets the accounting matrix.
	 *
	 * @param accountingMatrix the new accounting matrix
	 */
	public void setAccountingMatrix(AccountingMatrix accountingMatrix) {
		this.accountingMatrix = accountingMatrix;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the description dynamic type.
	 *
	 * @return the descriptionDynamicType
	 */
	public String getDescriptionDynamicType() {
		if(Validations.validateIsNotNullAndPositive(dynamicType)){
			return AccountingNatureAccountType.get(dynamicType).getValue();
		}
		return descriptionDynamicType;
	}

	/**
	 * Sets the description dynamic type.
	 *
	 * @param descriptionDynamicType the descriptionDynamicType to set
	 */
	public void setDescriptionDynamicType(String descriptionDynamicType) {
		this.descriptionDynamicType = descriptionDynamicType;
	}

	/**
	 * Gets the list accounting account tree.
	 *
	 * @return the listAccountingAccountTree
	 */
	public List<AccountingAccount> getListAccountingAccountTree() {
		return listAccountingAccountTree;
	}

	/**
	 * Sets the list accounting account tree.
	 *
	 * @param listAccountingAccountTree the listAccountingAccountTree to set
	 */
	public void setListAccountingAccountTree(
			List<AccountingAccount> listAccountingAccountTree) {
		this.listAccountingAccountTree = listAccountingAccountTree;
	}

	
	
	
}
