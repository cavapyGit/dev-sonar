package com.pradera.accounting.formulate.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.accounting.common.utils.AccountingUtils;
import com.pradera.core.component.helperui.to.AccountingParameterTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingFormulate;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class VariableFormulateTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class VariableFormulateTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id variable formulate pk. */
	private Long idVariableFormulatePk;
	
	/** The name variable. */
	private String nameVariable;
	
  	/** The accounting formulate. */
	  private AccountingFormulate accountingFormulate;

  	/** The formulate input. */
	  private String formulateInput;
  	
	/** The position source. */
	private Integer positionSource;
  	
	/** The variable source. */
	private String variableSource;
		  	
	/**  REFERENCE TO PARAMETER ACCOUNTING *. */
	private  AccountingParameterTO parameterFormulate;

	/** The position formulate. */
	private Integer positionFormulate;
  	
	/** Used when user setting value for screen *. */
	private BigDecimal valueDefault;
	
	/** Used when user setting value for xml*. */
	private BigDecimal valueSource;
	  	
	/** The position off set. */
	private Integer positionOffSet;
	
	/** The source type. */
	private Integer sourceType;
	
	/** The name variable with out separator. */
	private String nameVariableWithOutSeparator;
	
	/**
	 * Gets the id variable formulate pk.
	 *
	 * @return the idVariableFormulatePk
	 */
	public Long getIdVariableFormulatePk() {
		return idVariableFormulatePk;
	}

	/**
	 * Sets the id variable formulate pk.
	 *
	 * @param idVariableFormulatePk the idVariableFormulatePk to set
	 */
	public void setIdVariableFormulatePk(Long idVariableFormulatePk) {
		this.idVariableFormulatePk = idVariableFormulatePk;
	}

	/**
	 * Gets the accounting formulate.
	 *
	 * @return the accountingFormulate
	 */
	public AccountingFormulate getAccountingFormulate() {
		return accountingFormulate;
	}

	/**
	 * Sets the accounting formulate.
	 *
	 * @param accountingFormulate the accountingFormulate to set
	 */
	public void setAccountingFormulate(AccountingFormulate accountingFormulate) {
		this.accountingFormulate = accountingFormulate;
	}

	/**
	 * Gets the position source.
	 *
	 * @return the positionSource
	 */
	public Integer getPositionSource() {
		return positionSource;
	}

	/**
	 * Sets the position source.
	 *
	 * @param positionSource the positionSource to set
	 */
	public void setPositionSource(Integer positionSource) {
		this.positionSource = positionSource;
	}

	/**
	 * Gets the variable source.
	 *
	 * @return the variableSource
	 */
	public String getVariableSource() {
		return variableSource;
	}

	/**
	 * Sets the variable source.
	 *
	 * @param variableSource the variableSource to set
	 */
	public void setVariableSource(String variableSource) {
		this.variableSource = variableSource;
	}

	
	
	
	/**
	 * Gets the parameter formulate.
	 *
	 * @return the parameterFormulate
	 */
	public AccountingParameterTO getParameterFormulate() {
		return parameterFormulate;
	}

	/**
	 * Sets the parameter formulate.
	 *
	 * @param parameterFormulate the parameterFormulate to set
	 */
	public void setParameterFormulate(AccountingParameterTO parameterFormulate) {
		this.parameterFormulate = parameterFormulate;
	}

	/**
	 * Gets the position formulate.
	 *
	 * @return the positionFormulate
	 */
	public Integer getPositionFormulate() {
		return positionFormulate;
	}

	/**
	 * Sets the position formulate.
	 *
	 * @param positionFormulate the positionFormulate to set
	 */
	public void setPositionFormulate(Integer positionFormulate) {
		this.positionFormulate = positionFormulate;
	}



	/**
	 * Gets the value default.
	 *
	 * @return the valueDefault
	 */
	public BigDecimal getValueDefault() {
		return valueDefault;
	}

	/**
	 * Sets the value default.
	 *
	 * @param valueDefault the valueDefault to set
	 */
	public void setValueDefault(BigDecimal valueDefault) {
		this.valueDefault = valueDefault;
	}

	/**
	 * Gets the position off set.
	 *
	 * @return the positionOffSet
	 */
	public Integer getPositionOffSet() {
		return positionOffSet;
	}

	/**
	 * Sets the position off set.
	 *
	 * @param positionOffSet the positionOffSet to set
	 */
	public void setPositionOffSet(Integer positionOffSet) {
		this.positionOffSet = positionOffSet;
	}

	/**
	 * Gets the formulate input.
	 *
	 * @return the formulateInput
	 */
	public String getFormulateInput() {
		return formulateInput;
	}

	/**
	 * Sets the formulate input.
	 *
	 * @param formulateInput the formulateInput to set
	 */
	public void setFormulateInput(String formulateInput) {
		this.formulateInput = formulateInput;
	}

	/**
	 * Gets the name variable.
	 *
	 * @return the nameVariable
	 */
	public String getNameVariable() {
		return nameVariable;
	}

	/**
	 * Sets the name variable.
	 *
	 * @param nameVariable the nameVariable to set
	 */
	public void setNameVariable(String nameVariable) {
		this.nameVariable = nameVariable;
	}

	/**
	 * Gets the name variable with out separator.
	 *
	 * @return the nameVariableWithOutSeparator
	 */
	public String getNameVariableWithOutSeparator() {
		if(Validations.validateIsNotNullAndNotEmpty(nameVariable)){
			nameVariableWithOutSeparator=nameVariable.replace(AccountingUtils.VERTICAL, " ");
		}
		return nameVariableWithOutSeparator;
	}

	/**
	 * Sets the name variable with out separator.
	 *
	 * @param nameVariableWithOutSeparator the nameVariableWithOutSeparator to set
	 */
	public void setNameVariableWithOutSeparator(String nameVariableWithOutSeparator) {
		this.nameVariableWithOutSeparator = nameVariableWithOutSeparator;
	}

	
	/**
	 * Gets the source type.
	 *
	 * @return the source type
	 */
	public Integer getSourceType() {
		return sourceType;
	}

	/**
	 * Sets the source type.
	 *
	 * @param sourceType the new source type
	 */
	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	/**
	 * Gets the value source.
	 *
	 * @return the value source
	 */
	public BigDecimal getValueSource() {
		return valueSource;
	}

	/**
	 * Sets the value source.
	 *
	 * @param valueSource the new value source
	 */
	public void setValueSource(BigDecimal valueSource) {
		this.valueSource = valueSource;
	}
	
	
	
}
