package com.pradera.accounting.formulate.util;

import java.util.ArrayList;
import java.util.List;
import java.util.MissingFormatArgumentException;
import java.util.StringTokenizer;

import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class EvalTokenizer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class EvalTokenizer {
	
	/** The str. */
	private StringTokenizer str;
	
	/** The variables. */
	private List<Double> variables=new ArrayList<Double>();
	
	/** The parser. */
	private Parser parser;
	
	/**
	 * Instantiates a new eval tokenizer.
	 *
	 * @param is the is
	 * @param parser the parser
	 * @param args the args
	 */
	public EvalTokenizer(StringTokenizer is, Parser parser,double ... args) {
		this.str = is;
		this.parser=parser;
		
		// index of last argument referenced
        int last = -1;
        // last ordinary index
        int lasto = -1;
        if(args!=null){
        	for (int i = 0; i < args.length; i++) {
   			 
				if ((args != null && last > args.length - 1))
					 throw new MissingFormatArgumentException("");
				variables.add(args[i]);
				
			
		}
        }
		
		 
	}

	/**
	 * Find the next token, skipping blanks, and return it. For VALUE token,
	 * place the processed value in currentValue. Print error message if input
	 * is unrecognized.
	 *
	 * @return the token
	 * @throws ServiceException the service exception
	 */
	public Token getToken() throws ServiceException {
		double theValue;
		if (!str.hasMoreTokens())
			return new Token();
		String s = str.nextToken();
		if (s.equals(" "))
			return getToken();
		if (s.equals("^"))
			return new Token(Parser.EXP);
		if (s.equals("/"))
			return new Token(Parser.DIV);
		if (s.equals("*"))
			return new Token(Parser.MULT);
		if (s.equals("("))
			return new Token(Parser.OPAREN);
		if (s.equals(")"))
			return new Token(Parser.CPAREN);
		if (s.equals("+"))
			return new Token(Parser.PLUS);
		if (s.equals("-"))
			return new Token(Parser.MINUS);
		if (Character.isLetter(s.charAt(0)))
		{
			int i = searchFunction(s);
			if (i >= 0)
				return new Token(Parser.FUNCT + i);
			else {
				i=searchVariable(s);
				return new Token(Parser.VARIABLE+i,variables.get(i));
			}
		}
		
		try {
			theValue = Double.valueOf(s).doubleValue();
		} catch (NumberFormatException e) {
			int i=searchVariable(s);
			if(i>=0){
				return new Token(Parser.VARIABLE+i,variables.get(i));
			}
			else{
				System.err.println("Parse error");
				throw new ServiceException(ErrorServiceType.PARSE_ERROR);
				//return new Token();
			}
		}
		return new Token(Parser.VALUE, theValue);
	}

	/**
	 * Search variable.
	 *
	 * @param s the s
	 * @return the int
	 */
	public int searchVariable(String s){
		
		for (int i = 0; i < this.parser.listVariable.size(); i++)
			if (s.equals(this.parser.listVariable.get(i)))
				return i;
		return -1;
	}
	
	
	/**
	 * Search function.
	 *
	 * @param s the s
	 * @return the int
	 */
	public int searchFunction(String s) {
		for (int i = 0; i < Parser.function.length; i++)
			if (s.equals(Parser.function[i]))
				return i;
		return -1;
	}

	
}
