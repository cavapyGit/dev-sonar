package com.pradera.accounting.formulate.util;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class Precedence.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class Precedence {

	/** The input symbol. */
	public int inputSymbol;
	
	/** The top of stack. */
	public int topOfStack;

	/**
	 * Instantiates a new precedence.
	 *
	 * @param inSymbol the in symbol
	 * @param topSymbol the top symbol
	 */
	public Precedence(int inSymbol, int topSymbol) {
		inputSymbol = inSymbol;
		topOfStack = topSymbol;
	}

	/** The prec table. */
	// PrecTable matches order of Token enumeration
	public static Precedence[] precTable = new Precedence[] {
			new Precedence(0, -1), // EOL
			new Precedence(0, 0), // VALUE
			new Precedence(100, 0), // OPAREN
			new Precedence(0, 99), // CPAREN
			new Precedence(6, 5), // EXP
			new Precedence(3, 4), // MULT
			new Precedence(3, 4), // DIV
			new Precedence(1, 2), // PLUS
			new Precedence(1, 2), // MINUS
			new Precedence(7, 6) // FUNCT
			
			
	};
	
	
}
