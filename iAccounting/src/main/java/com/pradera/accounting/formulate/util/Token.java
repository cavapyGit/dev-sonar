package com.pradera.accounting.formulate.util;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class Token.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public  class Token {

	/** The type. */
	private int type = Parser.EOL;
	
	/** The value. */
	private double value = 0;
	
	/**
	 * Instantiates a new token.
	 */
	public Token() {
		this(Parser.EOL);
	}

	/**
	 * Instantiates a new token.
	 *
	 * @param t the t
	 */
	public Token(int t) {
		this(t, 0);
	}

	/**
	 * Instantiates a new token.
	 *
	 * @param t the t
	 * @param v the v
	 */
	public Token(int t, double v) {
		type = t;
		value = v;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

}