package com.pradera.accounting.formulate.util;


import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.orderlist.OrderList;
import com.pradera.core.component.helperui.to.AccountingParameterTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ParameterConverter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@FacesConverter(value = "parameterConverter")
public class ParameterConverter implements Converter {
	
	/** The formulate. */
	public static List<AccountingParameterTO> formulate;


	/* (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		Object ret = null;
	    if (arg1 instanceof OrderList) {
	        Object dualList = ((OrderList) arg1).getValue();
	        List<AccountingParameterTO> formulate=new ArrayList<AccountingParameterTO>();
	        formulate=(List<AccountingParameterTO>) dualList;
	        int i=0;
	        
//	        for (Iterator iterator = formulate.iterator(); iterator.hasNext();) {
//	        	AccountingParameterTO formulate2 = (AccountingParameterTO) iterator.next();
//				formulate2.setIndex(i);
//				i++;
//			}
	        for(Object form:formulate) {
	        	 String id = "" + ((AccountingParameterTO) form).getIndex();
		            if (arg2.equals(id)) {
		                ret = form; break;
		            } 
	        }
	           
	        if (ret == null)  {
	        	for(Object form:formulate) {
	                String ids = "" + ((AccountingParameterTO) form).getIndex();
	                if (arg2.equals(ids)) {
	                    ret = form;
	                }
	            }}
	    }
	    return ret;
	}

	/* (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		String str = "";
	    if (arg2 instanceof AccountingParameterTO) {
	        str = "" + ((AccountingParameterTO) arg2).getIndex();
	    }
	    return str;
	}
	

}
