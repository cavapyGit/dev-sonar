package com.pradera.accounting.formulate.to;

import java.io.Serializable;
import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ParameterFormulateTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class ParameterFormulateTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4032948053032353280L;
	
	/** The id parameter formulate. */
	private Long idParameterFormulate;
	
	/** The index. */
	private Integer index;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The reference. */
	private String reference;
	
	/** The value string. */
	private String valueString;
	
	/** The value double. */
	private BigDecimal valueDouble;
	
	/** The value integer. */
	private Integer valueInteger;
	
	/** The selection of data. */
	private Integer selectionOfData;
	
	/** The flag. */
	private Boolean flag;
	 
	
	/**
	 * Instantiates a new parameter formulate to.
	 */
	public ParameterFormulateTo() { 
	} 

	/**
	 * Instantiates a new parameter formulate to.
	 *
	 * @param idParameterFormulate the id parameter formulate
	 * @param index the index
	 * @param name the name
	 * @param description the description
	 */
	public ParameterFormulateTo(Long idParameterFormulate, Integer index,
			String name, String description) { 
		this.idParameterFormulate = idParameterFormulate;
		this.index = index;
		this.name = name;
		this.description = description;
	} 

	/**
	 * Instantiates a new parameter formulate to.
	 *
	 * @param idParameterFormulate the id parameter formulate
	 * @param index the index
	 * @param name the name
	 */
	public ParameterFormulateTo(Long idParameterFormulate, Integer index, String name) { 
		this.idParameterFormulate = idParameterFormulate;
		this.index = index;
		this.name = name;
	} 


	/**
	 * Instantiates a new parameter formulate to.
	 *
	 * @param idParameterFormulate the id parameter formulate
	 * @param name the name
	 * @param description the description
	 */
	public ParameterFormulateTo(Long idParameterFormulate, String name,
			String description) {
		this.idParameterFormulate = idParameterFormulate;
		this.name = name;
		this.description = description;
	}

	
	
	/**
	 * Gets the id parameter formulate.
	 *
	 * @return the idParameterFormulate
	 */
	public Long getIdParameterFormulate() {
		return idParameterFormulate;
	}

	/**
	 * Sets the id parameter formulate.
	 *
	 * @param idParameterFormulate the idParameterFormulate to set
	 */
	public void setIdParameterFormulate(Long idParameterFormulate) {
		this.idParameterFormulate = idParameterFormulate;
	}

	/**
	 * Gets the index.
	 *
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}

 
	/**
	 * Sets the index.
	 *
	 * @param index the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
 

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
 

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

 
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

 
	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the reference.
	 *
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * Sets the reference.
	 *
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * Gets the value string.
	 *
	 * @return the valueString
	 */
	public String getValueString() {
		return valueString;
	}

	/**
	 * Sets the value string.
	 *
	 * @param valueString the valueString to set
	 */
	public void setValueString(String valueString) {
		this.valueString = valueString;
	}

	/**
	 * Gets the value double.
	 *
	 * @return the valueDouble
	 */
	public BigDecimal getValueDouble() {
		return valueDouble;
	}

	/**
	 * Sets the value double.
	 *
	 * @param valueDouble the valueDouble to set
	 */
	public void setValueDouble(BigDecimal valueDouble) {
		this.valueDouble = valueDouble;
	}

	/**
	 * Gets the value integer.
	 *
	 * @return the valueInteger
	 */
	public Integer getValueInteger() {
		return valueInteger;
	}

	/**
	 * Sets the value integer.
	 *
	 * @param valueInteger the valueInteger to set
	 */
	public void setValueInteger(Integer valueInteger) {
		this.valueInteger = valueInteger;
	}

	/**
	 * Gets the selection of data.
	 *
	 * @return the selectionOfData
	 */
	public Integer getSelectionOfData() {
		return selectionOfData;
	}

	/**
	 * Sets the selection of data.
	 *
	 * @param selectionOfData the selectionOfData to set
	 */
	public void setSelectionOfData(Integer selectionOfData) {
		this.selectionOfData = selectionOfData;
	}

	/**
	 * Gets the flag.
	 *
	 * @return the flag
	 */
	public Boolean getFlag() {
		return flag;
	}

	/**
	 * Sets the flag.
	 *
	 * @param flag the flag to set
	 */
	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	
	

}
