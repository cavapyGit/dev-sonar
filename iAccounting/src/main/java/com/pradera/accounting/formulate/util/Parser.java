package com.pradera.accounting.formulate.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

import com.pradera.accounting.formulate.to.VariableFormulateTo;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class Parser.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class Parser {
	
	/** The Constant EOL. */
	public static final int EOL       = 0;
	
	/** The Constant VALUE. */
	public static final int VALUE     = 1;
	
	/** The Constant OPAREN. */
	public static final int OPAREN    = 2;
	
	/** The Constant CPAREN. */
	public static final int CPAREN    = 3;
	
	/** The Constant EXP. */
	public static final int EXP       = 4;
	
	/** The Constant MULT. */
	public static final int MULT      = 5;
	
	/** The Constant DIV. */
	public static final int DIV       = 6;
	
	/** The Constant PLUS. */
	public static final int PLUS      = 7;
	
	/** The Constant MINUS. */
	public static final int MINUS     = 8;
	
	/** The Constant FUNCT. */
	public static final int FUNCT     = 9;
	
	/** The Constant VARIABLE. */
	public static final int VARIABLE  =50;
	
	/** The function. */
	public static String[] function   = { "sqrt", "sin", "cos", "tan", "asin",
			"acos", "atan", "log", "floor", "eXp" };

	
	/** The list variable. */
	public List<String> listVariable = null;

	
	/** The str formulate input. */
	private String strFormulateInput="";
	
	/** The op stack. */
	private Stack opStack=null; // Operator stack for conversion
	
	/** The postfix stack. */
	private Stack postfixStack=null; // Stack for postfix machine
	
	/** The stream. */
	private StringTokenizer stream=null; // StringTokenizer stream


	/**
	 * Construct an evaluator object.
	 *
	 * @param formulateInput the new ting formulate
	 * @throws ServiceException the service exception
	 */
	
	
	public void settingFormulate(String formulateInput) throws ServiceException{
		
		assignVariable(formulateInput);
		opStack = new Stack();	
		postfixStack = new Stack();
		strFormulateInput = unary2Binary(formulateInput);
		stream = new StringTokenizer(strFormulateInput, "+*-/^() ", true);
		opStack.push(new Integer(EOL));
		
	}
	
	/**
	 * Reset formulate.
	 */
	public void resetFormulate(){
		
		strFormulateInput="";
		opStack=null; // Operator stack for conversion
		postfixStack=null; // Stack for postfix machine
		stream=null; // StringTokenizer stream
		listVariable = null;
	}
	
	/**
	 * Calculate formulate.
	 *
	 * @return the double
	 * @throws ServiceException the service exception
	 */
	public double calculateFormulate() throws ServiceException{
		

		double[] variable=new double[listVariable.size()];
		for(int i=0;i<listVariable.size();i++){
			variable[i]=1;
		}
		double value =  getValue(variable);
		
		resetFormulate();
		return value;
	}
	
	/**
	 * Validate formulate.
	 *
	 * @return the boolean
	 */
	public Boolean validateFormulate(){
		
		try {			
			calculateFormulate();
		} catch (Exception e) {
			
			e.printStackTrace();
			return Boolean.FALSE;
			
		}
		
		return Boolean.TRUE;
	}

	/**
	 * Function eval.
	 *
	 * @param topOp the top op
	 * @param d the d
	 * @return the double
	 */
	private double functionEval(int topOp, double d) {
		double y = 0;
		switch (topOp) {
		case 9:
			y = Math.sqrt(d);
			break;
		case 10:
			y = Math.sin(d);
			break;
		case 11:
			y = Math.cos(d);
			break;
		case 12:
			y = Math.tan(d);
			break;
		case 13:
			y = Math.asin(d);
			break;
		case 14:
			y = Math.acos(d);
			break;
		case 15:
			y = Math.atan(d);
			break;
		case 16:
			y = Math.log(d);
			break;
		case 17:
			y = Math.floor(d);
			break;
		case 18:
			y = Math.exp(d);
		}
		return y;
	}

	/**
	 * Process an operator by taking two items off the postfix stack, applying
	 * the operator, and pushing the result. Print error if missing closing
	 * parenthesis or division by 0.
	 *
	 * @param topOp the top op
	 * @throws ServiceException the service exception
	 */
	private void binaryOp(int topOp) throws ServiceException {
		if (topOp == OPAREN) {
			System.err.println("Unbalanced parentheses");
			opStack.pop();
			throw new ServiceException(ErrorServiceType.UNBALANCED_PARENTHESIS);
			//return;
		}
		if (topOp >= FUNCT) {
			double d = getTop();
			postfixStack.push(new Double(functionEval(topOp, d)));
			opStack.pop();
			return;
		}
		double rhs = getTop();
		double lhs = getTop();
		if (topOp == EXP)
			postfixStack.push(new Double(pow(lhs, rhs)));
		else if (topOp == PLUS)
			postfixStack.push(new Double(lhs + rhs));
		else if (topOp == MINUS)
			postfixStack.push(new Double(lhs - rhs));
		else if (topOp == MULT)
			postfixStack.push(new Double(lhs * rhs));
		else if (topOp == DIV)
			if (rhs != 0)

				postfixStack.push(new Double(lhs / rhs));
			else {
				
				System.err.println("Division by zero");
				postfixStack.push(new Double(lhs));
				throw new ServiceException(ErrorServiceType.DIVISION_BY_ZERO);
			}
		opStack.pop();
	}

	// The only publicy visible routine
	/**
	 * Public routine that performs the evaluation. Examine the postfix machine
	 * to see if a single result is left and if so, return it; otherwise print
	 * error.
	 *
	 * @param variables the variables
	 * @return the result.
	 * @throws ServiceException the service exception
	 */

	public double getValue(double ... variables) throws ServiceException {
		// for each call
		opStack = new Stack();
		postfixStack = new Stack();
		stream = new StringTokenizer(strFormulateInput, "+*-/^() ", true);
		opStack.push(new Integer(EOL));
		EvalTokenizer tok = new EvalTokenizer(stream,this,variables);
		Token lastToken;
		do {
			lastToken = tok.getToken();
			processToken(lastToken);
		} while (lastToken.getType() != EOL);
		if (postfixStack.isEmpty()) {
			System.err.println("Missing operand!");
			
			//return 0;
			throw new ServiceException(ErrorServiceType.MISSING_OPERAND);
		}
		double theResult = postFixTopAndPop();
		if (!postfixStack.isEmpty()){
			System.err.println("Warning: missing operators!");
			throw new ServiceException(ErrorServiceType.MISSING_OPERAND);
		}
			
		return theResult;
	}

	/**
	 * Internal method that unary to binary.
	 *
	 * @param s the s
	 * @return the string
	 */
	private String unary2Binary(String s) {
		int i;
		s = s.trim();
		if (s.charAt(0) == '-')
			s = "0.0" + s;
		while ((i = s.indexOf("(-")) >= 0)
			s = s.substring(0, i + 1) + "0.0" + s.substring(i + 1);
		return s;
	}

	/**
	 * Internal method that hides type-casting.
	 *
	 * @return the double
	 */
	private double postFixTopAndPop() {
		return ((Double) (postfixStack.pop())).doubleValue();
	}

	/**
	 * Another internal method that hides type-casting.
	 *
	 * @return the int
	 */
	private int opStackTop() {
		return ((Integer) (opStack.peek())).intValue();
	}

	/**
	 * After a token is read, use operator precedence parsing algorithm to
	 * process it; missing opening parentheses are detected here.
	 *
	 * @param lastToken the last token
	 * @throws ServiceException the service exception
	 */
	private void processToken(Token lastToken) throws ServiceException {
		int topOp;
		int lastType = lastToken.getType();
		switch (lastType) {
		case VALUE:
			postfixStack.push(new Double(lastToken.getValue()));
			return;
		case CPAREN:
			while ((topOp = opStackTop()) != OPAREN && topOp != EOL)
				binaryOp(topOp);
			if (topOp == OPAREN)
				opStack.pop(); // Get rid of opening parentheseis
			else{
				System.err.println("Missing open parenthesis");
				throw new ServiceException(ErrorServiceType.MISSING_OPEN_PARENTHESIS);
			}
				
				
			break;
		
		default: // General operator case
			int last=0;
			if(lastType >= FUNCT && lastType<VARIABLE){
				last=FUNCT;
			}
			else if(lastType>=VARIABLE){
				postfixStack.push(new Double(lastToken.getValue()));
				break;
			}
			else{
				last=lastType;
			}
			
			while (Precedence.precTable[last].inputSymbol <= Precedence.precTable[opStackTop() >= FUNCT ? FUNCT: opStackTop()].topOfStack)
				binaryOp(opStackTop());
			if (lastType != EOL)
				opStack.push(new Integer(lastType));
			break;
		}
	}

	/**
	 * Gets the top.
	 *
	 * @return the top
	 * @throws ServiceException the service exception
	 */
	/*
	 * topAndPop the postfix machine stack; return the result. If the stack is
	 * empty, print an error message.
	 */
	private double getTop() throws ServiceException {
		if (postfixStack.isEmpty()) {
			System.err.println("Missing operand");
			throw new ServiceException(ErrorServiceType.MISSING_OPERAND);
			//return 0;
		}
		return postFixTopAndPop();
	}

	/**
	 * Internal routine to compute x^n.
	 *
	 * @param x the x
	 * @param n the n
	 * @return the double
	 * @throws ServiceException the service exception
	 */
	private static double pow(double x, double n) throws ServiceException {
		if (x == 0) {
			if (n == 0){
				System.err.println("0^0 is undefined");
				throw new ServiceException(ErrorServiceType.ZERO_ELEVATED_ZERO);
			}
				
			return 0;
		}
		if (n < 0) {
			System.err.println("Negative exponent");
			throw new ServiceException(ErrorServiceType.NEGATIVE_EXPONENT);
			//return 0;
		}
		if (n == 0)
			return 1;
		if (n % 2 == 0)
			return pow(x * x, n / 2);
		else
			return x * pow(x, n - 1);
	}

	
	
	
	/**
	 * Assign variable.
	 *
	 * @param expresionEvaluate the expresion evaluate
	 */
	public void assignVariable(String expresionEvaluate ){
		
		
		 this.listVariable = new ArrayList<String>();
		 
		 StringTokenizer str;
		 List<String> varia=new ArrayList<String>();
		 str = new StringTokenizer(expresionEvaluate, "+*-/^() ", true);
		 int i=0;
		 /**String Tokenizer separar y encontrar las variables*/
		 do{
			 
			 varia.add(str.nextToken());
			 i++;
		 }
		 while(str.countTokens()>0);
		 
		 for (Object var : varia) {
			 String s=var.toString();
			 	if (s.equals(" ")){
			 	}
					
			 	else if (s.equals("^")){
				}
					
			 	else if (s.equals("/")){
				}
					
			 	else if (s.equals("*")){
				}
					
			 	else if (s.equals("(")){
				}
					
			 	else if (s.equals(")")){
				}
				
			 	else 	if (s.equals("+")){
				}
					
			 	else if (s.equals("-")){
				}
			 	else if (Character.isLetter(s.charAt(0)))
				{
					int in = searchFunction(s);
					if (in >= 0);
					else {
						listVariable.add(s);
					}
		 		}
			 	else {
			 		try {
			 			double theValue = Double.valueOf(s).doubleValue();
					} catch (NumberFormatException e) {
						listVariable.add(s);
					}
			 	}
					
			
		}
	}
	
	/**
	 * Search function.
	 *
	 * @param s the s
	 * @return the int
	 */
	public  int searchFunction(String s) {
		for (int i = 0; i < Parser.function.length; i++)
			if (s.equals(Parser.function[i]))
				return i;
		return -1;
	}
	
	/**
	 * Gets the list variable.
	 *
	 * @param expresionEvaluate the expresion evaluate
	 * @return the list variable
	 */
	public  List<String> getListVariable(String expresionEvaluate){
		List<String> listVariable=new ArrayList<String>();
		 StringTokenizer str;
		 List<String> varia=new ArrayList<String>();
		 str = new StringTokenizer(expresionEvaluate, "+*-/^() ", true);
		 int i=0;
		 /**String Tokenizer separar y encontrar las variables*/
		 do{
			 
			 varia.add(str.nextToken());
			 i++;
		 }
		 while(str.countTokens()>0);
		 
		 for (Object var : varia) {
			 String s=var.toString();
			 	if (s.equals(" ")){
			 	}
					
			 	else if (s.equals("^")){
				}
					
			 	else if (s.equals("/")){
				}
					
			 	else if (s.equals("*")){
				}
					
			 	else if (s.equals("(")){
				}
					
			 	else if (s.equals(")")){
				}
				
			 	else 	if (s.equals("+")){
				}
					
			 	else if (s.equals("-")){
				}
			 	else if (Character.isLetter(s.charAt(0)))
				{
					int in = searchFunction(s);
					if (in >= 0);
					else {
						listVariable.add(s);
					}
		 		}
			 	else {
			 		try {
			 			double theValue = Double.valueOf(s).doubleValue();
					} catch (NumberFormatException e) {
						listVariable.add(s);
					}
			 	}
					
			
		}
		 return listVariable;
	}
	
	/**
	 * Verify absolute position.
	 *
	 * @param expresionEvaluate the expresion evaluate
	 * @return the list
	 */
	public List<VariableFormulateTo> verifyAbsolutePosition(String expresionEvaluate){
		List<VariableFormulateTo> listVariableFormulateTo=new ArrayList<VariableFormulateTo>();
		VariableFormulateTo variableFormulateTo=null;
		List<String> listVariable=new ArrayList<String>();
		 StringTokenizer str;
		 List<String> varia=new ArrayList<String>();
		 str = new StringTokenizer(expresionEvaluate, "+*-/^() ", true);
		 int i=0;
		 /**String Tokenizer separar y encontrar las variables*/
		 do{
			 
			 varia.add(str.nextToken());
			 i++;
		 }
		 while(str.countTokens()>0);
		 
		 for (Object var : varia) {
			 String s=var.toString();
			 	if (s.equals(" ")){
			 	}
					
			 	else if (s.equals("^")){
				}
					
			 	else if (s.equals("/")){
				}
					
			 	else if (s.equals("*")){
				}
					
			 	else if (s.equals("(")){
				}
					
			 	else if (s.equals(")")){
				}
				
			 	else 	if (s.equals("+")){
				}
					
			 	else if (s.equals("-")){
				}
			 	else if (Character.isLetter(s.charAt(0)))
				{
					int in = searchFunction(s);
					if (in >= 0);
					else {
						listVariable.add(s);
					}
		 		}
			 	else {
			 		try {
			 			double theValue = Double.valueOf(s).doubleValue();
					} catch (NumberFormatException e) {
						listVariable.add(s);
					}
			 	}
		}
		 
//		 StringBuffer stringBuffer=new StringBuffer(expresionEvaluate.replaceAll(" ", ""));
		 String expresionSubString=expresionEvaluate.replaceAll(" ", "");
		 if(listVariable.size()>0){
			 
			 Integer[] inicios=new Integer[listVariable.size()];
			 Integer[] offset=new Integer[listVariable.size()];
			 int acumulador=0;
			 int j = 0;
			 do{
				 variableFormulateTo =new VariableFormulateTo();
				 
				 	inicios[j]=expresionSubString.indexOf(listVariable.get(j),acumulador);
				 	offset[j]=listVariable.get(j).length();
				 	variableFormulateTo.setNameVariable(listVariable.get(j));
					variableFormulateTo.setPositionOffSet(offset[j]);
					variableFormulateTo.setPositionFormulate(inicios[j]);
					 acumulador=acumulador+offset[j];
				 j++;
				 listVariableFormulateTo.add(variableFormulateTo);
			 }
			 while(j < listVariable.size());
		 }
		 else{
			  return null;
				
		 }
		 return listVariableFormulateTo;
	}
	
	
	
}
