package com.pradera.core.framework.batchprocess;

import javax.enterprise.util.AnnotationLiteral;

public class BatchProcessLiteral extends AnnotationLiteral<BatchProcess> implements BatchProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9042006741313855735L;
	
	private String name;

	public BatchProcessLiteral() {
		// TODO Auto-generated constructor stub
	}
	
	public BatchProcessLiteral(String name) {
		this.name = name;
	}

	public String name() {
		// TODO Auto-generated method stub
		return this.name;
	}

}
