package com.pradera.core.framework.extension.transaction.jta;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Typed;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.core.framework.extension.transaction.TransactionSupport;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class DefaultTransactionSupport.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2013
 */
@ApplicationScoped
@Typed(TransactionSupport.class)
public class DefaultTransactionSupport extends AbstractTransactionSupport {
	
	/** The Constant TRANSACTION_MANAGER_JNDI_NAMES. */
	private static final String[] TRANSACTION_MANAGER_JNDI_NAMES = {
        "java:appserver/TransactionManager", // Glassfish
        "java:jboss/TransactionManager", // JBoss
        "java:/TransactionManager", // JBoss
        "java:pm/TransactionManager", // TopLink
        "java:comp/TransactionManager" // Some servlet containers
    };

    /** The Constant TRANSACTION_SYNCHRONIZATION_REGISTRY_JNDI_NAME. */
    private static final String TRANSACTION_SYNCHRONIZATION_REGISTRY_JNDI_NAME =
        "java:comp/TransactionSynchronizationRegistry";

	/**
	 * Instantiates a new default transaction support.
	 */
	public DefaultTransactionSupport() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.extension.transaction.jta.AbstractTransactionSupport#lookupTransactionManager()
	 */
	@Override
	protected TransactionManager lookupTransactionManager() {
		InitialContext ctx;
        try
        {
            ctx = new InitialContext();
        }
        catch (NamingException e)
        {
            throw new RuntimeException(e);
        }

        for (String jndiName : TRANSACTION_MANAGER_JNDI_NAMES)
        {
            try
            {
                return (TransactionManager) ctx.lookup(jndiName);
            }
            catch (NamingException e)
            {
                // Try next.
            }
        }

        throw new RuntimeException();
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.extension.transaction.jta.AbstractTransactionSupport#lookupSynchronizationRegistry()
	 */
	@Override
	protected TransactionSynchronizationRegistry lookupSynchronizationRegistry() {
		try
        {
            return (TransactionSynchronizationRegistry) new InitialContext()
                .lookup(TRANSACTION_SYNCHRONIZATION_REGISTRY_JNDI_NAME);
        }
        catch (NamingException e)
        {
            throw new RuntimeException(e);
        }
	}

}
