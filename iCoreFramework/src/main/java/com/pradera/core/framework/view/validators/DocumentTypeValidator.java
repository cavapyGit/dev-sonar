package com.pradera.core.framework.view.validators;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.type.DocumentType;

@FacesValidator("com.pradera.core.framework.view.validators.DocumentTypeValidator")
public class DocumentTypeValidator implements Validator {
	
	private String errorMessage;
	private boolean valid;

	public void validate(FacesContext context, UIComponent component, Object value) 
			throws ValidatorException { 
		
		Integer documentType = (Integer) component.getAttributes().get("docType");
		if(Validations.validateIsNull(documentType) || Validations.validateIsNull(value)){
			return;
		}
		
		DocumentTypeValidator bean = doDocumentTypeValidation(documentType, (String)value);
		
		if(!bean.isValid()){
			Locale locale = context.getViewRoot().getLocale();

			String facesMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale, 
					bean.getErrorMessage());
			
			FacesMessage msg = new FacesMessage(facesMsg);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			
			throw new ValidatorException(msg);
		}

	}
	
	private DocumentTypeValidator doDocumentTypeValidation(Integer documentType, String documentNumberAsString){
		DocumentTypeValidator bean = new DocumentTypeValidator();
		bean.setValid(false);
		
		boolean matches = false;
		if (documentType.equals(DocumentType.PAS.getCode()) || documentType.equals(DocumentType.DIO.getCode())){
			matches = CommonsUtilities.matchAlphanumeric(documentNumberAsString);
			if(!matches){
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_ALPHANUMERIC);
				return bean;
			}
		}
		
		if (documentType.equals(DocumentType.CI.getCode()) || documentType.equals(DocumentType.RUC.getCode()) ||
				documentType.equals(DocumentType.CDN.getCode())){
			matches = CommonsUtilities.matchNumeric(documentNumberAsString);
			if(!matches){
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NUMERIC);
				return bean;
			}
		}
		
		if(documentType.equals(DocumentType.CI.getCode())){
			if(documentNumberAsString.length() == 11){
				// TODO validate Luhn
				bean.setValid(true);
			}else{
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_11_DIGITS); // formato de CI no valido
			}
		} else if (documentType.equals(DocumentType.RUC.getCode())){
			if(documentNumberAsString.length() == 11){
				bean.setValid(true);
			} else if (documentNumberAsString.length() == 9){
				if(!documentNumberAsString.startsWith("0")){
					bean.setValid(true);
				}else{
					bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_1_FIRST_ZERO); // El primer caracter no puede ser cero.
				}
			} else{
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_9_11_DIGITS); // Numero de caracteres incorrecto
			}
//		} else if (documentType.equals(DocumentType.NUI.getCode())){
//			// TODO validate Luhn
//			if(documentNumberAsString.length() == 11){
//				bean.setValid(true);
//			}else{
//				bean.setErrorMessage(PropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_11_DIGITS); // formato de CI no valido
//			}
		} else if (documentType.equals(DocumentType.CDN.getCode())){
			if(documentNumberAsString.length() == 16 || 
					documentNumberAsString.length() == 18 || 
					documentNumberAsString.length() == 21){
				if(Long.valueOf(documentNumberAsString.substring(
						documentNumberAsString.length()-4,documentNumberAsString.length()-1)) > 1900){
					bean.setValid(true);
				}else{
					bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_AN_LESS_THAN_1900);  
					// los 4 ultimos digitos de  numero de acta no pueden ser mayores a 1900
				}
			}else{
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_16_18_21_DIGITS);   // numero de carecteres incorrectos 
			}
		} else if (documentType.equals(DocumentType.PAS.getCode()) || documentType.equals(DocumentType.DIO.getCode())){
			if(documentNumberAsString.length() >= 6 && documentNumberAsString.length() <= 15){
				//TODO validar los 3 primeros caracteres pueden ser ceros
				bean.setValid(true);
			}else{
				bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS); // numero de carecteres incorrectos
			}
		} else {
			// TODO documento no soportado.
			bean.setErrorMessage(GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_SUPPORTED);
		}
		return bean;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	

}