package com.pradera.core.framework.notification.service;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pradera.commons.httpevent.NotificationEvent;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class NotificationObserverServiceBean.
 * is a listener for coming events notifications
 * @author PraderaTechnologies.
 * @version 1.0 , 13/05/2013
 */
@Stateless
public class NotificationObserverServiceBean {
	
	/** The log. */
	@Inject
	PraderaLogger log;

	/** The em. */
	@Inject
	@DepositaryDataBase
	private EntityManager em;
	
	
	/** The cliente rest service. */
	@Inject
	ClientRestService clienteRestService;
	
	/**
	 * Send event process.
	 *
	 * @param browser the browser
	 */
	@Asynchronous
	public void sendEventProcess(@Observes(during=TransactionPhase.AFTER_SUCCESS) @NotificationEvent(NotificationType.PROCESS) BrowserWindow browser) {
		clienteRestService.notifyEventBrowserWindow(browser);
	}
	
	/**
	 * Send event message.
	 *
	 * @param browser the browser
	 */
	@Asynchronous
	public void sendEventMessage(@Observes(during=TransactionPhase.AFTER_SUCCESS) @NotificationEvent(NotificationType.MESSAGE) BrowserWindow browser) {
		clienteRestService.notifyEventBrowserWindow(browser);
	}
	
	/**
	 * Send event email.
	 *
	 * @param notification the notification
	 */
	@Asynchronous
	public void sendEventEmail(@Observes(during=TransactionPhase.AFTER_SUCCESS) @NotificationEvent(NotificationType.EMAIL) BrowserWindow browser) {
		//TODO now send to security module manage mail, check if this send should be in coreframework
//		notificationHandler.sendMessage(browser);
		clienteRestService.notifyEventBrowserWindow(browser);
	}
	
	@Asynchronous
	public void sendReportNotification(@Observes(during=TransactionPhase.AFTER_SUCCESS) @NotificationEvent(NotificationType.REPORT) BrowserWindow browser){
		clienteRestService.notifyEventBrowserWindow(browser);
	}
	
}
