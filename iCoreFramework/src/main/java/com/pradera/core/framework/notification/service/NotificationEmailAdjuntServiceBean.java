package com.pradera.core.framework.notification.service;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

//import net.markenwerk.utils.mail.smime.SmimeKey;
//import net.markenwerk.utils.mail.smime.SmimeUtil;

//import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.notification.service.type.EmailCdeParameterType;

@ApplicationScoped
public class NotificationEmailAdjuntServiceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7926232671529569983L;
	
	@Inject
	@InjectableResource(location = "EmailParametersCDE.properties")
	private Properties emailParameterCDE;

	
	 @Inject
	 private PraderaLogger log;
	
	private static Session session = null;
	
	//private Transport transport;
	
	private  Properties props;
	
	public NotificationEmailAdjuntServiceBean() {
		props = new Properties();

	}
	/**
	 * Envio de email
	 * */
	
	private void preparteSendEmail(){
		 if(session==null){
			 String host = emailParameterCDE.getProperty(EmailCdeParameterType.HOST.getValue());
			 props = new Properties();	            	
			 props.put(EmailCdeParameterType.HOST.getValue(), host);
			 props.setProperty(EmailCdeParameterType.START_TLS.getValue(), emailParameterCDE.getProperty(EmailCdeParameterType.START_TLS.getValue()));
			 props.setProperty(EmailCdeParameterType.PORT.getValue(), emailParameterCDE.getProperty(EmailCdeParameterType.PORT.getValue()));
			 props.setProperty(EmailCdeParameterType.USER.getValue(), emailParameterCDE.getProperty(EmailCdeParameterType.USER.getValue()));
             props.setProperty(EmailCdeParameterType.AUTH.getValue(), emailParameterCDE.getProperty(EmailCdeParameterType.AUTH.getValue()));
             session = Session.getDefaultInstance(props, null);	      
		 }
	}
	public void startFileSending(String detFullName,String subject,String message,String email,File file){
		preparteSendEmail();
		 try {
		        Message mess = new MimeMessage(session);
		        mess.setFrom(new InternetAddress(emailParameterCDE.getProperty(EmailCdeParameterType.USER.getValue())));
		        mess.setRecipients(Message.RecipientType.TO,InternetAddress.parse(email));
		        mess.setSubject(subject);
		        BodyPart text = new MimeBodyPart();
		        text.setText(message);
		        Multipart multipart = new MimeMultipart();
		        if(file!=null){
		        	 MimeBodyPart messageBodyPart = new MimeBodyPart();
		        	 String fileName = file.getName();
				     DataSource source = new FileDataSource(file.getAbsolutePath());
				     messageBodyPart.setDataHandler(new DataHandler(source));
				     messageBodyPart.setFileName(fileName);
				     multipart.addBodyPart(messageBodyPart);
		        }
		        multipart.addBodyPart(text);
		        mess.setContent(multipart);
		        
		        Transport t = session.getTransport("smtp");
                t.connect(emailParameterCDE.getProperty(EmailCdeParameterType.USER.getValue()), emailParameterCDE.getProperty(EmailCdeParameterType.PASS.getValue()));
                t.sendMessage(mess, mess.getAllRecipients());
                t.close();
		        
		    } catch (MessagingException e) {
		        e.printStackTrace();
		        session = null;
		    }
	}
	/**
	 * Envio de correo electronico con contenido html
	 * */
	public void startFileSendingHtmlContent(String detFullName,String subject,String message,String email,List<File> files){
		preparteSendEmail();
		 try {
		        Message mess = new MimeMessage(session);
		        mess.setFrom(new InternetAddress(emailParameterCDE.getProperty(EmailCdeParameterType.USER.getValue())));
		        mess.setRecipients(Message.RecipientType.TO,InternetAddress.parse(email));
		        mess.setSubject(subject);
		        BodyPart text = new MimeBodyPart();
		        Multipart multipart = new MimeMultipart();
		        if(files!=null&&files.size()>0){
		        	int count = 0;
		        	MimeBodyPart messageBodyPart = null;
		        	DataSource source = null;
		        	String fileName = null;
		        	for (File fileToSend : files) {
		        		 fileName = fileToSend.getName();
		        		 messageBodyPart = new MimeBodyPart();
		        		 source= new FileDataSource(fileToSend.getAbsolutePath());
					     messageBodyPart.setDataHandler(new DataHandler(source));
					     messageBodyPart.setFileName(fileName);
					     multipart.addBodyPart(messageBodyPart,count);
					     count++;
					}
		        }
		        text.setContent(message, "text/html" );
		        multipart.addBodyPart(text);
		        mess.setContent(multipart);
		        
		        Transport t = session.getTransport("smtp");
		        //t.connect(emailParameterCDE.getProperty(EmailCdeParameterType.USER.getValue()), emailParameterCDE.getProperty(EmailCdeParameterType.PASS.getValue()));
		        t.connect("infoedv@edv.com.bo", "Cdde$2019#3.141592654");
                t.sendMessage(mess, mess.getAllRecipients());
                t.close();
                
		    } catch (MessagingException e) {
		    	log.info("CDE :::Revise su conexion, no se pudo enviar correo electronico a :"+email);
		    	session = null;
		        e.printStackTrace();
		    }
	}
	/**
	 * Envio de correo electronico con contenido html, firmado y/o encriptado
	 * @throws Exception 
	 * 
	 * */
//	public void startFileSendingHtmlContent(SmimeKey smimeKey,InputStream inputStreamCerticate,String subject,String message,String email,List<File> files,boolean sign,boolean encrypt) throws Exception{
//		preparteSendEmail();
//	 try {
//		    MimeMessage mimeMessage = new MimeMessage(session);
//		    mimeMessage.setFrom(new InternetAddress(emailParameterCDE.getProperty(EmailCdeParameterType.USER.getValue())));
//		    mimeMessage.setRecipients(Message.RecipientType.TO,InternetAddress.parse(email));
//		    mimeMessage.setSubject(subject);
//	        BodyPart text = new MimeBodyPart();
//	        Multipart multipart = new MimeMultipart();
//	        if(files!=null&&files.size()>0){
//	        	int count = 0;
//	        	MimeBodyPart messageBodyPart = null;
//	        	DataSource source = null;
//	        	String fileName = null;
//	        	for (File fileToSend : files) {
//	        		 fileName = fileToSend.getName();
//	        		 messageBodyPart = new MimeBodyPart();
//	        		 source= new FileDataSource(fileToSend.getAbsolutePath());
//				     messageBodyPart.setDataHandler(new DataHandler(source));
//				     messageBodyPart.setFileName(fileName);
//				     multipart.addBodyPart(messageBodyPart,count);
//				     count++;
//				}
//	        }
//	        text.setContent(message, "text/html" );
//	        multipart.addBodyPart(text);
//	        mimeMessage.setContent(multipart);
//	        
//	        if(sign){
//				mimeMessage = getSignMessage(session, smimeKey,mimeMessage);
//				log.info("CDE::::::::::Firmando Correo Electronico");
//			}
//			
//			if(encrypt){
//				log.info("CDE:::::::::::Cifrando Correo Electronico");
//				mimeMessage = getEncryptMessage(session, inputStreamCerticate, mimeMessage);
//			}
//	        
//	        Transport t = session.getTransport("smtp");
//	        System.out.println("SESION:"+session);
//	        System.out.println("USER:"+emailParameterCDE.getProperty(EmailCdeParameterType.USER.getValue()));
//	        System.out.println("PASS:"+emailParameterCDE.getProperty(EmailCdeParameterType.PASS.getValue()));
//            t.connect(emailParameterCDE.getProperty(EmailCdeParameterType.USER.getValue()), emailParameterCDE.getProperty(EmailCdeParameterType.PASS.getValue()));
//	        //t.connect("infoedv@edv.com.bo", "Cdde$2019#3.141592654");
//            t.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
//            t.close();
//            
//	    } catch (MessagingException e) {
//	    	log.info("CDE ::::::   Revise su conexion, no se pudo enviar correo electronico a :"+email);
//	    	session = null;
//	        e.printStackTrace();
//	    }
//	}
	/***/
//	private MimeMessage getSignMessage(Session session, SmimeKey smimeKey,MimeMessage message) throws Exception {
//		return SmimeUtil.sign(session, message, smimeKey);
//	}

//	private MimeMessage getEncryptMessage(Session session, InputStream inputStreamCerticate,MimeMessage message){
//		
//		X509Certificate certificate = getCertificateForRecipient(inputStreamCerticate);
//		return SmimeUtil.encrypt(session, message, certificate);
//	}
	
//	private X509Certificate getCertificateForRecipient(InputStream inputStream) {
//		X509Certificate certificate = null;
//		try {
//			 if (Security.getProvider("BC") == null) 
//		            Security.addProvider(new BouncyCastleProvider());
//			 
//			CertificateFactory factory = CertificateFactory.getInstance("X.509", "BC");
//			certificate = (X509Certificate) factory.generateCertificate(inputStream);
//		} catch (CertificateException e) {
//			e.printStackTrace();
//		} catch (NoSuchProviderException e) {
//			e.printStackTrace();
//		}
//		return certificate;
//	}
}
