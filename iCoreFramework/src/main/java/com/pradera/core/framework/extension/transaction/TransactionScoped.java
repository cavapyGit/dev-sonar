package com.pradera.core.framework.extension.transaction;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.enterprise.context.NormalScope;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface TransactionScoped.
 * The transaction scope is destroyed when a transaction is committed or rolled
 * back
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2013
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.FIELD })
@Documented
@Inherited
@NormalScope
public @interface TransactionScoped {

}
