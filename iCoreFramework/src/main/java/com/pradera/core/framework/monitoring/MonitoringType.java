package com.pradera.core.framework.monitoring;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum MonitoringType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/10/2014
 */
public enum MonitoringType {
	
	/** The report. */
	REPORT,
	
	/** The service. */
	SERVICE,
	
	/** The process. */
	PROCESS
}
