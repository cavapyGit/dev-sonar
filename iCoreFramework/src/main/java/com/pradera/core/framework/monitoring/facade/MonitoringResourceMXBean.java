package com.pradera.core.framework.monitoring.facade;

import java.util.List;
import java.util.Map;
import com.pradera.core.framework.monitoring.Invocation;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface MonitoringResourceMXBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/10/2014
 */
public interface MonitoringResourceMXBean {
	
	/**
	 * Gets the slowest methods.
	 *
	 * @return the slowest methods
	 */
	List<Invocation> getSlowestMethods();
    
    /**
     * Gets the diagnostics.
     *
     * @return the diagnostics
     */
    Map<String,String> getDiagnostics();
    
    /**
     * Gets the number of exceptions.
     *
     * @return the number of exceptions
     */
    String getNumberOfExceptions();
    
    /**
     * Clear.
     */
    void clear();
}
