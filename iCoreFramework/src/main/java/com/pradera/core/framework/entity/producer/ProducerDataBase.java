package com.pradera.core.framework.entity.producer;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;

/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class ProducerDataBase. Produces centralized persistence unit name
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 29/11/2012
 */
@Stateless
public class ProducerDataBase {

	/** Produces EntityManager with specific unitName. */
//	@Produces
//	@DepositaryDataBase
//	@PersistenceContext(unitName = "iDepositary")
//	private EntityManager em;

	@Resource
	SessionContext context;

	@Produces
	@DepositaryDataBase
	public EntityManager getEntityiDepositary(){
	      //Lookup the entity manager
		 String jndiName ="java:global/iModel/iDepositary/em";
	      EntityManager manager = (EntityManager) context.lookup(jndiName);
	      
	      if (manager == null) {
	          throw new RuntimeException("Tenant unknown");
	      }
	      return manager;

	}
	
//	@Produces
//	@DepositaryDataBaseBBV
//	public EntityManager getEntityiDepositaryBBV(){
//	      //Lookup the entity manager
//		 String jndiName ="java:global/iModel/iDepositaryBBV/embbv";
//	      EntityManager manager = (EntityManager) context.lookup(jndiName);
//	      
//	      if (manager == null) {
//	          throw new RuntimeException("Tenant unknown");
//	      }
//	      return manager;
//
//	}
}
