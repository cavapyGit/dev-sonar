package com.pradera.core.framework.batchprocess.service.sequential;

import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.pradera.commons.audit.UserTrackProcess;
import com.pradera.commons.audit.service.UserAuditServiceBean;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcessLiteral;
import com.pradera.core.framework.batchprocess.SequentialProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.batchprocess.service.ProcessLoggerServiceBean;
import com.pradera.core.framework.batchprocess.service.WorkManagerService;
import com.pradera.core.framework.batchprocess.to.BatchRegisterTO;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.process.type.ProcessLoggerStateType;
import com.pradera.model.process.type.ScheduleExecuteType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SequentialWorkManager.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/03/2013
 */
@ApplicationScoped
@SequentialProcess
public class SequentialWorkManager implements WorkManagerService {
	
	/** The job execution. */
	@Inject
	@Any
	Instance<JobExecution> jobExecution;
	
	/** The process logger service. */
	@EJB
	ProcessLoggerServiceBean processLoggerService;
	
	/** The notification service. */
	@EJB
	NotificationServiceBean notificationService;
	
	/** The user audit service. */
	@EJB
	UserAuditServiceBean userAuditService;
	
	@Inject
	ClientRestService clientRestService;
	
	/** The log. */
	@Inject
	private transient  PraderaLogger log;
	
	/** The server path. */
	@Inject @StageDependent
	private String serverPath;
	
	@Inject
	StageApplication stageApplication;

	/**
	 * Instantiates a new sequential work manager.
	 */
	public SequentialWorkManager() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.WorkManagerService#initBatch(com.pradera.core.framework.batchprocess.to.BatchRegisterTO)
	 */
	public void initBatch(BatchRegisterTO batch) {
		ProcessLogger processLog = null;
		try {
		//Inject beand CDI from DB
		JobExecution job = getJob(batch.getProcess().getBeanName());
		processLog = processLoggerService.getProcessLoggerById(batch.getIdProcessLogger());
		processLoggerService.updateStatus(batch.getIdProcessLogger(), ProcessLoggerStateType.PROCESSING.getCode());
		//register audit fields in thread
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), batch.getLoggerUser());
		//register audit batch
		registerAuditBatch(batch,processLog);
		//run batch
		job.startJob(processLog);
		//all is ok
		processLog.setLoggerState(ProcessLoggerStateType.FINISHED.getCode());
		processLog.setFinishingDate(CommonsUtilities.currentDateTime());
		processLog.setLastModifyDate(CommonsUtilities.currentDateTime());
		//call notification and change status
		//finish sucess batch process
		processLoggerService.updateProcessLogger(processLog);
		if(batch.getIdProcessSchedule()!=null){
			//If batch was call from process scheduler
			processLoggerService.updateProcessScheduleExecution(batch.getIdProcessSchedule(), ScheduleExecuteType.FINISH.getCode());
			if(stageApplication.equals(StageApplication.Production)){
				clientRestService.removeBatchExecution(batch.getIdProcessSchedule());
			}
		}
		//check if should be send notification according to batch process business logic
			if(job.sendNotification()){
				if(processLog.getBusinessProcess().getIndNotices().equals(BooleanType.YES.getCode())){
					//if is configurate send notification
				  List<Object> institutions = job.getDestinationInstitutions();
				  if(institutions != null && !institutions.isEmpty()){
					  //send notification for each institution
					  for(Object institution : institutions){
						  notificationService.sendProcessBatchNotification(batch.getLoggerUser(),batch.getLoggerUser().getUserName(), processLog, institution,
								  job.getParametersNotification());
					  }
				  } else {
					  //only send one notification 
					  notificationService.sendProcessBatchNotification(batch.getLoggerUser(),batch.getLoggerUser().getUserName(), processLog, null,
							  job.getParametersNotification());
				  }
				}
			}
		} catch (Exception ex) {
			log.error(ExceptionUtils.getStackTrace(ex));
			if(batch.getIdProcessSchedule()!=null){
				clientRestService.updateProcessScheduleExecution(batch.getIdProcessSchedule(), ScheduleExecuteType.RUNNING_ERROR.getCode());
				if(stageApplication.equals(StageApplication.Production)){
					clientRestService.removeBatchExecution(batch.getIdProcessSchedule());
				}
			}
			//register error batch
			if (processLog != null) {
				processLog.setLoggerState( ProcessLoggerStateType.ERROR.getCode());
				processLog.setFinishingDate(CommonsUtilities.currentDateTime());
				processLog.setErrorDetail(ex.getMessage());
				processLog.setLastModifyDate(CommonsUtilities.currentDateTime());
				processLoggerService.updateProcessLogger(processLog);
			} else {
				processLoggerService.updateStatus(batch.getIdProcessLogger(), ProcessLoggerStateType.ERROR.getCode());
			}
		} 
	}
	
	/**
	 * Register audit batch.
	 *
	 * @param batchRegister the batch register
	 * @param processLogger the process logger
	 */
	private void registerAuditBatch(BatchRegisterTO batchRegister,ProcessLogger processLogger){
		if(batchRegister.getLoggerUser() != null && batchRegister.getLoggerUser().getIdUserSessionPk() !=null){
			//only register when process was initiated by user log in
			UserTrackProcess usertTrackProcess = new UserTrackProcess();
			usertTrackProcess.setBusinessMethod("startJob");
			usertTrackProcess.setClassName(batchRegister.getProcess().getBeanName());
			usertTrackProcess.setIdBusinessProcess(batchRegister.getProcess().getIdBusinessProcessPk());
			usertTrackProcess.setIdUserSessionPk(batchRegister.getLoggerUser().getIdUserSessionPk());
			usertTrackProcess.setIdProcessLogger(processLogger.getIdProcessLoggerPk());
			if(processLogger.getProcessLoggerDetails()!= null && !processLogger.getProcessLoggerDetails().isEmpty()){
				usertTrackProcess.setParameters(new HashMap<String,String>());
				for(ProcessLoggerDetail logDetail : processLogger.getProcessLoggerDetails()){
					usertTrackProcess.getParameters().put(logDetail.getParameterName(), logDetail.getParameterValue());
				}
			}
			if(batchRegister.getLoggerUser() != null && batchRegister.getLoggerUser().getIdPrivilegeOfSystem()!=null){
				usertTrackProcess.setIdPrivilegeSystem(batchRegister.getLoggerUser().getIdPrivilegeOfSystem());
				usertTrackProcess.setIndError(BooleanType.NO.getCode());
			} else {
				usertTrackProcess.setIdPrivilegeSystem(BooleanType.NO.getCode());
				usertTrackProcess.setIndError(BooleanType.YES.getCode());
			}
			//register tracking
			userAuditService.registerUserTrackProcess(usertTrackProcess);
		}
	}
	
	/**
	 * Gets the job.
	 *
	 * @param name the name
	 * @return the job
	 */
	private JobExecution getJob(String name) {
		return this.jobExecution.select(new BatchProcessLiteral(name)).get();
	}
	
}
