package com.pradera.core.framework.schedulerprocess.facade;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.processes.scheduler.SynchronizedBatch;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.batchprocess.service.ChunkerService;
import com.pradera.core.framework.batchprocess.service.ProcessLoggerServiceBean;
import com.pradera.core.framework.batchprocess.to.BatchRegisterTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.type.ScheduleExecuteType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BatchProcessResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/06/2013
 */
@Path("/batchprocess")
@Stateless
public class BatchProcessResource {
    
    /** The batch service. */
    @EJB
    BatchServiceBean batchService;
    
    @EJB
    ProcessLoggerServiceBean processLoggerService;
    
    @EJB
    ChunkerService chunkerService;
    
    /** The log. */
    @Inject
    PraderaLogger log;
    
    /** The registry. */
    @Resource
	private TransactionSynchronizationRegistry registry;
    
    @Resource
    TimerService timerService;
    
    @Inject
    ClientRestService clientRestService;
    
    @Inject
	StageApplication stageApplication;
    
    /** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
    
    /**
     * Excute batch process.
     *
     * @param scheduleBatch the schedule batch
     * @param request the request
     * @return the response
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response excuteBatchProcess(Schedulebatch scheduleBatch,@Context HttpServletRequest request) {
        try {
            if(scheduleBatch.getIdBusinessProcess() != null 
                    && scheduleBatch.getUserName()!=null
                    && scheduleBatch.getPrivilegeCode() != null){
                log.info("register batch with id:"+scheduleBatch.getIdBusinessProcess());
                scheduleBatch.setIpAddres(request.getRemoteAddr());
        		//Setting initial running batch process
                if(scheduleBatch.getIdProcessSchedule()!=null){
                	//Only batch fire from timer
	                 processLoggerService.updateProcessScheduleExecution(scheduleBatch.getIdProcessSchedule(), ScheduleExecuteType.RUNNING.getCode());
	                 //if(stageApplication.equals(StageApplication.Production)){
	                	 //clientRestService.registerBatchExecution(scheduleBatch.getIdProcessSchedule());
	                	 
	                	 BusinessProcess businessProcess = new BusinessProcess();
	                	 businessProcess.setIdBusinessProcessPk(scheduleBatch.getIdBusinessProcess());
	             		
	                	 Map<String, Object> param = new HashMap<String, Object>();
	                	 
	                	 batchService.registerScheduledBatch(businessProcess, param);	
	                 //}
                }
                TimerConfig timerConfig= new TimerConfig(scheduleBatch, false);
                //registry batch after 1 seconds
                timerService.createSingleActionTimer(1000, timerConfig);
            }
        }catch(Exception ex) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
        }
        return Response.status(Response.Status.OK.getStatusCode()).entity("sucess").build();
    }
    
    /**
     * Excute sync batch process.
     *
     * @param scheduleBatch the schedule batch
     * @param request the request
     * @return the response
     */
    @POST
    @Path("/synchronized")
    @Consumes(MediaType.APPLICATION_JSON)
    @TransactionTimeout(unit=TimeUnit.MINUTES,value=120)
    public Response excuteSyncBatchProcess(SynchronizedBatch syncBatch,@Context HttpServletRequest request) {
        try {
        	String strUser = syncBatch.getUserName();
        	String strIpAddress = request.getRemoteAddr();
        	LoggerUser loggerUser = CommonsUtilities.getLoggerUser(strUser, strIpAddress);
        	//How is required_new transaction dont propagate transactionRegistry, so we propagate with threadLocal
            ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
            BusinessProcess businessProc = new BusinessProcess();
    		businessProc.setIdBusinessProcessPk(syncBatch.getBusinessProcess());
    		//Register processLogger
    		BatchRegisterTO batchRegisterTO = batchService.registerBatch(strUser, businessProc, syncBatch.getObjectParameters());
    		batchRegisterTO.setIndSynchronized(BooleanType.YES.getCode());
    		chunkerService.executeBatch(batchRegisterTO);
        }catch(Exception ex) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(ex.getMessage()).build();
        }
        return Response.status(Response.Status.OK.getStatusCode()).entity("sucess").build();
    }
    
    /**
     * Gets the parameters.
     *
     * @param scheduleBatch the schedule batch
     * @return the parameters
     */
    private Map<String,Object> getParameters(Schedulebatch scheduleBatch){
    	HashMap<String, Object> parameter = null;
    	if (scheduleBatch.getParameters() != null && !scheduleBatch.getParameters().isEmpty()) {
    		parameter = new HashMap<String, Object>();
    		Iterator<Map.Entry<String, String>> itMap = scheduleBatch.getParameters().entrySet().iterator();
    		while (itMap.hasNext()) {
    			Map.Entry<String,String> entry = itMap.next();
    			parameter.put(entry.getKey(), entry.getValue());
    		}
    	}
    	return parameter;
    }
    
    /**
     * Registry batch for delay execution.
     *
     * @param timer the timer
     */
    @Timeout
    @TransactionTimeout(unit=TimeUnit.MINUTES,value=120)
    public void registryBatch(Timer timer) throws ServiceException{
    	log.info("Running timer :"+timer.getInfo() + " hour "+CommonsUtilities.currentDateTime());
        //get scheduler
    	Schedulebatch scheduleBatch = (Schedulebatch)timer.getInfo();
    	//Register audit thread
        LoggerUser loggerUser = new LoggerUser();
		loggerUser.setIpAddress(scheduleBatch.getIpAddres());
		loggerUser.setUserName(scheduleBatch.getUserName());
//		loggerUser.setIdUserSessionPk(1l);
//		loggerUser.setSessionId(userInfo.getUserAccountSession().getTicketSession());
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		//Praderafilter in each request get current UserAcction
		UserAcctions userAcctions = new UserAcctions();
		//Using only register privilege
		userAcctions.setIdPrivilegeRegister(scheduleBatch.getPrivilegeCode());
		userAcctions.setIdPrivilegeReject(scheduleBatch.getPrivilegeCode());
		userAcctions.setIdPrivilegeAnnular(scheduleBatch.getPrivilegeCode());
		userAcctions.setIdPrivilegeSettlement(scheduleBatch.getPrivilegeCode());
		userAcctions.setIdPrivilegeSearch(scheduleBatch.getPrivilegeCode());
		userAcctions.setIdPrivilegeModify(scheduleBatch.getPrivilegeCode());
		userAcctions.setIdPrivilegeActivate(scheduleBatch.getPrivilegeCode());
		loggerUser.setUserAction(userAcctions);
		loggerUser.setIdPrivilegeOfSystem(userAcctions.getIdPrivilegeRegister());
		//How is required_new transaction dont propagate transactionRegistry, so we propagate with threadLocal
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        BusinessProcess businessProc = new BusinessProcess();
		businessProc.setIdBusinessProcessPk(scheduleBatch.getIdBusinessProcess());
		//Register processLogger
		BatchRegisterTO batchRegisterTO = batchService.registerBatch(scheduleBatch.getUserName(), businessProc, getParameters(scheduleBatch));
		//set processSchedule pk
		batchRegisterTO.setIdProcessSchedule(scheduleBatch.getIdProcessSchedule());
		chunkerService.executeBatch(batchRegisterTO);
    }
}
