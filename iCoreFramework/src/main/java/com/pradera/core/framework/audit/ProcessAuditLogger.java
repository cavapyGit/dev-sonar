package com.pradera.core.framework.audit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

import com.pradera.commons.type.BusinessProcessType;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface ProcessAuditLogger.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-nov-2013
 */
@InterceptorBinding
@Inherited
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
@Documented
public @interface ProcessAuditLogger {
	
	/**
	 * Process.
	 *
	 * @return the business process type
	 */
	@Nonbinding
	BusinessProcessType process() default BusinessProcessType.SYSTEM_AUDIT;
	
	/**
	 * Validation user.
	 *
	 * @return true, if successful
	 */
	@Nonbinding
	boolean validationUser() default false;
	
}
