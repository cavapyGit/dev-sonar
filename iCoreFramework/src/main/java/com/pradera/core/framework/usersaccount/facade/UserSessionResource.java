package com.pradera.core.framework.usersaccount.facade;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.pradera.commons.security.SessionRegistry;

@Path("/usersessions")
@Stateless
public class UserSessionResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1574423817450617454L;
	
	@Inject
	SessionRegistry sessionRegistry;
	
	@Path("/close")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	public Response closeSessionModule(String userName){
		HttpSession session = sessionRegistry.getSession(userName);
		if(session != null){
			//close remote session
			sessionRegistry.remove(userName);
			try{
			session.invalidate();
			} catch(IllegalStateException ise){
				//if error was in this war and not iSecurity
			}
		}
		return Response.status(Response.Status.OK.getStatusCode()).entity("sucess").build();
	}
	
}
