package com.pradera.core.framework.notification.service;

import com.pradera.commons.httpevent.publish.BrowserWindow;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Interface NotificarionHandler.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/12/2012
 */
public interface NotificarionHandler {
	
	/**
	 * Send message of notification.
	 *
	 * @param notification the notification
	 * @return true, if successful
	 */
	public boolean sendMessage(BrowserWindow browser);
}
