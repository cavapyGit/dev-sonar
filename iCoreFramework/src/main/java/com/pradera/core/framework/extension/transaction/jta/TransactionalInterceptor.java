package com.pradera.core.framework.extension.transaction.jta;

import javax.ejb.ApplicationException;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.core.framework.extension.transaction.TransactionSupport;
import com.pradera.core.framework.extension.transaction.Transactional;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class TransactionalInterceptor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2013
 */
@Transactional
@Interceptor
public class TransactionalInterceptor {

	/** The Constant log. */
	private static final Logger log = LoggerFactory
			.getLogger(TransactionalInterceptor.class);

	/** The extension. */
	@Inject
	private TransactionExtension extension;

	/** The transaction support. */
	@Inject
	private TransactionSupport transactionSupport;
	
	/**
	 * Instantiates a new transactional interceptor.
	 */
	public TransactionalInterceptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Around invoke.
	 *
	 * @param invocationContext the invocation context
	 * @return the object
	 * @throws Exception the exception
	 */
	@AroundInvoke
	public Object aroundInvoke(InvocationContext invocationContext) throws Exception {
		boolean active = isTransactionActive();
		TransactionAttributeType attr = extension.getTransactionAttribute(invocationContext.getMethod());
		Boolean requiresNew = requiresNew(active, attr);
		boolean debug = log.isInfoEnabled();

		if (debug) {
			log.debug("Invoking transactional method "
					+ invocationContext.getMethod() + ", attr = " + attr
					+ ", active = " + active + ", requiresNew = " + requiresNew);
		}

		// Suspend the current transaction if transaction attribute is
		// REQUIRES_NEW or NOT_SUPPORTED.
		Transaction previous = null;
		if ((requiresNew != Boolean.FALSE) && active) {
			if (debug) {
				log.debug("Suspending the current transaction");
			}

			previous = transactionSupport.suspend();
		}

		try {
			if (requiresNew == Boolean.TRUE) {
				if (debug) {
					log.debug("Starting a new transaction");
				}

				transactionSupport.begin();
			}

			Object result;
			try {
				//put log audit fields in trasaction
				if (ThreadLocalContextHolder.checkInitialited()) {
					transactionSupport.putResource(RegistryContextHolderType.LOGGER_USER, ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name()));
				}
				result = invocationContext.proceed();
			} catch (Exception e) {
				if (requiresNew == Boolean.FALSE) {
					if (needsRollback(e)) {
						transactionSupport.setRollbackOnly();
					}
				} else if (requiresNew == Boolean.TRUE) {
					if (needsRollback(e)) {
						if (debug) {
							log.debug("Rolling back the current transaction");
						}

						transactionSupport.rollback();
					} else {
						if (debug) {
							log.debug("Committing the current transaction");
						}

						transactionSupport.commit();
					}
				}

				throw e;
			}

			if (requiresNew == Boolean.TRUE) {
				if (transactionSupport.getStatus() == Status.STATUS_MARKED_ROLLBACK) {
					if (debug) {
						log.debug("Rolling back the current transaction");
					}

					transactionSupport.rollback();
				} else {
					if (debug) {
						log.debug("Committing the current transaction");
					}

					transactionSupport.commit();
				}
			}

			return result;
		} finally {
			// Resume the previous transaction if it was suspended.
			if (previous != null) {
				if (debug) {
					log.debug("Resuming the previous transaction");
				}

				transactionSupport.resume(previous);
			}
		}
	}

	/**
	 * Checks if the current transaction is active, rolled back or marked for
	 * rollback.
	 * 
	 * @return {@code true} if the current transaction is active, rolled back or
	 *         marked for rollback, {@code false} otherwise.
	 * @throws SystemException
	 *             thrown if the transaction manager encounters an unexpected
	 *             error condition
	 */
	private boolean isTransactionActive() throws SystemException {
		switch (transactionSupport.getStatus()) {
		case Status.STATUS_ACTIVE:
		case Status.STATUS_MARKED_ROLLBACK:
		case Status.STATUS_ROLLEDBACK:
			return true;

		default:
			return false;
		}
	}

	/**
	 * Determines whether it is necessary to begin a new transaction.
	 * 
	 * @param active
	 *            the status of the current transaction.
	 * @param attribute
	 *            the transaction attribute of the current method.
	 * @return {@code Boolean.TRUE} if the interceptor should suspend the
	 *         current transaction and invoke the method within a new
	 *         transaction, {@code Boolean.FALSE} if the interceptor should
	 *         invoke the method within the current transaction, {@code null} if
	 *         the interceptor should suspend the current transaction and invoke
	 *         the method outside of transaction.
	 */
	private Boolean requiresNew(boolean active,TransactionAttributeType attribute) {
		switch (attribute) {
		case MANDATORY:
			if (active) {
				return false;
			} else {
				throw new IllegalStateException(
						"Transaction is required to perform this method");
			}

		case NEVER:
			if (!active) {
				return false;
			} else {
				throw new IllegalStateException(
						"This method cannot be invoked within a transaction");
			}

		case NOT_SUPPORTED:
			return false;

		case REQUIRED:
			return !active;

		case REQUIRES_NEW:
			return true;

		case SUPPORTS:
			if (active) {
				return false;
			} else {
				return false;
			}

		default:
			throw new UnsupportedOperationException(
					"Unsupported TransactionAttribute value " + attribute);
		}
	}

	/**
	 * Determines whether it is necessary to rollback the current transaction
	 * when the specified exception occurred during the method invocation.
	 * 
	 * @param exception
	 *            the exception that occurred during the method invocation.
	 * @return {@code true} if the interceptor should rollback the current
	 *         transaction, {@code false} if the interceptor should commit the
	 *         current transaction.
	 */
	private boolean needsRollback(Exception exception) {
		boolean rollback = exception instanceof RuntimeException;

		for (Class<?> clazz = exception.getClass(); clazz != null; clazz = clazz
				.getSuperclass()) {
			ApplicationException ae = clazz
					.getAnnotation(ApplicationException.class);
			if (ae != null) {
				if (ae.inherited()) {
					return ae.rollback();
				} else {
					break;
				}
			}
		}

		return rollback;
	}

}
