package com.pradera.core.framework.notification.facade;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notification.type.NotificationInformation;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.core.framework.notification.service.NotificationAditional;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.process.BusinessProcess;
import com.edv.collection.economic.rigth.PaymentAgent;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class NotificationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/09/2013
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class NotificationServiceFacade {
	
	/** The log. */
	@Inject private PraderaLogger log;

	/** The notification service. */
	@EJB 
	private NotificationServiceBean notificationService;
	
	@EJB private NotificationAditional notificationAditional;
	
	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
//	@Asynchronous
	public void executeNotificationIntercepted(NotificationInformation notificationData){
			
		LoggerUser loggerUser = notificationData.getLoggerUser();
		BusinessProcess bProcess = new BusinessProcess();
		bProcess.setIdBusinessProcessPk(notificationData.getBusinessProcess());
		
		List<Object> participantCodes = Arrays.asList(notificationData.getParticipantCodes().toArray());
		List<Object> issuerCodes = Arrays.asList(notificationData.getIssuerCodes().toArray());
		
		Map<Integer,List<BusinessProcess>> bProcessToMap = notificationAditional.getProcessNotificationByType(Arrays.asList(bProcess.getIdBusinessProcessPk()));
	}
	
	@Asynchronous
	public void sendNotification(NotificationInformation notificationData){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		String userName = notificationData.getLoggerUser().getUserName();
		BusinessProcess bProcess = new BusinessProcess();
		bProcess.setIdBusinessProcessPk(notificationData.getBusinessProcess());
	}
	
	/**
	 * Send notification.
	 *
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode is the Participant oe Issuer code to send Notification if is other type user put null
	 * @param parameters the parameters
	 */
	public void sendNotification(String userName,BusinessProcess businessProc,Object intitutionCode,Object[] parameters,UserAccountSession userAccountSession){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem()==null){
    		//default privilege register from where is caller
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}
    	notificationService.sendNotification(loggerUser, userName, businessProc, intitutionCode,parameters, userAccountSession);
	}
	
	/**
	 * Send notification.
	 *
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode is the Participant oe Issuer code to send Notification if is other type user put null
	 * @param parameters the parameters
	 */
	public void sendNotification(String userName,BusinessProcess businessProc,Object intitutionCode,Object[] parameters){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser==null)
			loggerUser = new LoggerUser();
		if(loggerUser.getIdPrivilegeOfSystem()==null){
			if(loggerUser.getUserAction()==null){
				loggerUser.setUserAction(new UserAcctions());
				loggerUser.getUserAction().setIdPrivilegeRegister(1);
			}
    	}
		if(loggerUser.getIdPrivilegeOfSystem()==null)
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	notificationService.sendNotification(loggerUser, userName, businessProc, intitutionCode,parameters, null);
	}

	public void sendParticipantNotificationEmail(String userName,BusinessProcess businessProc, Long idParticipantPk,Object[] parameters){
		/*
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser==null)
			loggerUser = new LoggerUser();
		if(loggerUser.getIdPrivilegeOfSystem()==null){
			if(loggerUser.getUserAction()==null){
				loggerUser.setUserAction(new UserAcctions());
				loggerUser.getUserAction().setIdPrivilegeRegister(1);
			}
    	}
		if(loggerUser.getIdPrivilegeOfSystem()==null) {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		loggerUser.setUserName(userName);

    	notificationService.sendParticipantNotificationEmail(loggerUser, businessProc, idParticipantPk ,parameters);
    	*/
	}

	public void sendNotificationEmailProfileNoConfig(String userName,BusinessProcess businessProcess, String subject, String message, Integer idSystemProfilePk,Object[] parameters){
		/*LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser==null)
			loggerUser = new LoggerUser();
		if(loggerUser.getIdPrivilegeOfSystem()==null){
			if(loggerUser.getUserAction()==null){
				loggerUser.setUserAction(new UserAcctions());
				loggerUser.getUserAction().setIdPrivilegeRegister(1);
			}
    	}
		if(loggerUser.getIdPrivilegeOfSystem()==null) {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		loggerUser.setUserName(userName);
    	notificationService.sendNotificationEmailProfileNoConfig(loggerUser, businessProcess, subject, message, idSystemProfilePk ,parameters);
    	*/
	}

	public void sendNotificationEmailParticipantNoConfig(String userName,BusinessProcess businessProcess, String subject, String message, Long idParticipantPk,Object[] parameters){
		/*LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser==null)
			loggerUser = new LoggerUser();
		if(loggerUser.getIdPrivilegeOfSystem()==null){
			if(loggerUser.getUserAction()==null){
				loggerUser.setUserAction(new UserAcctions());
				loggerUser.getUserAction().setIdPrivilegeRegister(1);
			}
    	}
		if(loggerUser.getIdPrivilegeOfSystem()==null) {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		loggerUser.setUserName(userName);
    	notificationService.sendNotificationEmailParticipantNoConfig(loggerUser, businessProcess, subject, message, idParticipantPk ,parameters);
    	*/
	}
	
	/**
	 * Send notification to List users of type(Participants or Issuers).
	 *
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode the intitution code
	 * @param parameters the parameters
	 */
	public void sendNotificationGroup(String userName,BusinessProcess businessProc,List<Object> intitutionCode,Object[] parameters,UserAccountSession userAccountSession){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem()==null){
    		//default privilege register from where is caller
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}
    	notificationService.sendNotificationGroup(loggerUser, userName, businessProc, intitutionCode,parameters,userAccountSession);
	}
	
	/**
	 * Send notification manual.
	 *
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param users the users
	 * @param subject the subject
	 * @param message the message
	 * @param notification the notification
	 */
	public void sendNotificationManual(String userName,BusinessProcess businessProc,List<UserAccountSession> users,String subject,String message,
			NotificationType notification){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem()==null){
    		//default privilege register from where is caller
    		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	}
		notificationService.sendNotificationManual(loggerUser, userName, businessProc, users, subject, message, notification);
	}
	/**
	 * Envio de notificaciones  con archivos de adjunto
	 * */
	public synchronized void sendNotificationManualEmailAdjuntHtmlContent(String userName,BusinessProcess businessProc,List<UserAccountSession> users,String subject,String message,
			NotificationType notification,List<File> files){
		//Enviar codigo Html
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser==null)
			loggerUser = new LoggerUser();
		notificationService.sendNotificationManual(loggerUser, userName, businessProc, users, subject, message, NotificationType.EMAIL_ADJUNT_CONTENT_HTML,files);
	}
	
	public void sendNotificationManualSms(String userName,BusinessProcess businessProc,List<UserAccountSession> users,String cellPhone,String message){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser==null){
			loggerUser = new LoggerUser();
			if(loggerUser.getUserAction()==null){
				loggerUser.setUserAction(new UserAcctions());
				loggerUser.getUserAction().setIdPrivilegeRegister(1);
			}
    	}
		if(loggerUser.getIdPrivilegeOfSystem()==null)
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		notificationService.sendNotificationManual(loggerUser, userName, businessProc, users, cellPhone,message,NotificationType.SMS_MESSAGE_TO_CELL_PHONE);
	}
	/**
	 * Envio de correo cifrado y/o firmado digitalmente
	 * */
	public synchronized void sendNotificationManualEmailAdjuntHtmlContent(String userName,PaymentAgent paymentAgent,BusinessProcess businessProc,List<UserAccountSession> users,String subject,String message,
			NotificationType notification,List<File> files){
		//Enviar codigo Html
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser==null)
			loggerUser = new LoggerUser();
		try {
			notificationService.sendNotificationManual(loggerUser, userName,paymentAgent, businessProc, users, subject, message, NotificationType.EMAIL_ADJUNT_CONTENT_HTML,files);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Send notification.
	 * Issue 1094 Implementacion de un mensaje de aviso en la bandeja de mensajes del CSDCORE
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode is the Participant oe Issuer code to send Notification if is other type user put null
	 * @param parameters the parameters
	 */
	public void sendNotification(String userName,BusinessProcess businessProc,Long instParticipant,String instIssuer,Object[] parameters){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser==null)
			loggerUser = new LoggerUser();
		if(loggerUser.getIdPrivilegeOfSystem()==null){
			if(loggerUser.getUserAction()==null){
				loggerUser.setUserAction(new UserAcctions());
				loggerUser.getUserAction().setIdPrivilegeRegister(1);
			}
    	}
		if(loggerUser.getIdPrivilegeOfSystem()==null)
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
    	notificationService.sendNotification(loggerUser, userName, businessProc, instParticipant,instIssuer,parameters, null);
	}
	
}
