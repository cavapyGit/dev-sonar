package com.pradera.core.framework.batchprocess.to;

import java.io.Serializable;

import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.process.BusinessProcess;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class BatchRegister.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/12/2012
 */
public class BatchRegisterTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2442861035436850158L;

	/** The id process logger. */
	private Long idProcessLogger;
	
	/** The process. */
	private BusinessProcess process;
	
	/** The logger user. */
	private LoggerUser loggerUser;
	
	/** The id process schedule. */
	private Long idProcessSchedule;
	
	/** The ind synchronized. */
	private Integer indSynchronized;
	
	/**
	 * Instantiates a new batch register.
	 */
	public BatchRegisterTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new batch register.
	 *
	 * @param idProcessLogger the id process logger
	 * @param process the process
	 */
	public BatchRegisterTO(Long idProcessLogger, BusinessProcess process) {
		this.idProcessLogger = idProcessLogger;
		this.process = process;
	}

	/**
	 * Gets the id process logger.
	 *
	 * @return the id process logger
	 */
	public Long getIdProcessLogger() {
		return idProcessLogger;
	}

	/**
	 * Sets the id process logger.
	 *
	 * @param idProcessLogger the new id process logger
	 */
	public void setIdProcessLogger(Long idProcessLogger) {
		this.idProcessLogger = idProcessLogger;
	}

	/**
	 * Gets the process.
	 *
	 * @return the process
	 */
	public BusinessProcess getProcess() {
		return process;
	}

	/**
	 * Sets the process.
	 *
	 * @param process the new process
	 */
	public void setProcess(BusinessProcess process) {
		this.process = process;
	}

	/**
	 * Gets the logger user.
	 *
	 * @return the loggerUser
	 */
	public LoggerUser getLoggerUser() {
		return loggerUser;
	}

	/**
	 * Sets the logger user.
	 *
	 * @param loggerUser the loggerUser to set
	 */
	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}

	/**
	 * Gets the id process schedule.
	 *
	 * @return the id process schedule
	 */
	public Long getIdProcessSchedule() {
		return idProcessSchedule;
	}

	/**
	 * Sets the id process schedule.
	 *
	 * @param idProcessSchedule the new id process schedule
	 */
	public void setIdProcessSchedule(Long idProcessSchedule) {
		this.idProcessSchedule = idProcessSchedule;
	}

	/**
	 * Gets the ind synchronized.
	 *
	 * @return the ind synchronized
	 */
	public Integer getIndSynchronized() {
		return indSynchronized;
	}

	/**
	 * Sets the ind synchronized.
	 *
	 * @param indSynchronized the new ind synchronized
	 */
	public void setIndSynchronized(Integer indSynchronized) {
		this.indSynchronized = indSynchronized;
	}
}