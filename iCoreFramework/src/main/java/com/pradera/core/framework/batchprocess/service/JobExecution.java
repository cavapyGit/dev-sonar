package com.pradera.core.framework.batchprocess.service;

import java.util.List;

import com.pradera.model.process.ProcessLogger;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface JobExecution.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/10/2013
 */
public interface JobExecution {
	
	/**
	 * Start job.
	 *
	 * @param processLogger the process logger
	 */
	public void startJob(ProcessLogger processLogger);
	
	/**
	 * Gets the parameters notification, this parameters are replaced in notification message 
	 * with characters %s.
	 * @return the parameters notification
	 */
	public Object[] getParametersNotification();
	
	/**
	 * Gets the destination institutions, this list have code of participants or issues 
	 * to whom should send the notification.
	 * this configurations should be registered in the configuration of notification
	  * @return the destinadestination institutions
	 */
	public List<Object> getDestinationInstitutions();
	
	/**
	 * Send notification.
	 * if return true after finish batch process will send notification
	 * , if return false don't send notification
	 * @return true, if successful
	 */
	public boolean sendNotification();
}
