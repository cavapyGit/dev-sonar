package com.pradera.core.framework.extension.transaction.jta;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessManagedBean;
import javax.enterprise.inject.spi.ProcessSessionBean;
import javax.enterprise.util.AnnotationLiteral;

import com.pradera.core.framework.extension.transaction.Transactional;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class TransactionExtension.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2013
 */
public class TransactionExtension implements Extension {

	/** The transaction attributes. */
	private Map<Method, TransactionAttributeType> transactionAttributes;

	/**
	 * Instantiates a new transaction extension.
	 */
	public TransactionExtension() {
		transactionAttributes = new HashMap<Method, TransactionAttributeType>();
	}

	/**
	 * Observes {@link ProcessManagedBean} event.
	 * 
	 * @param <X>
	 *            the generic type
	 * @param event
	 *            the event {@link ProcessManagedBean} event.
	 */
	<X> void processBean(@Observes ProcessManagedBean<X> event) {
		boolean sessionBean = event instanceof ProcessSessionBean<?>;
		AnnotatedType<X> annotatedType = event.getAnnotatedBeanClass();
		boolean hasClassInterceptor = annotatedType.isAnnotationPresent(Transactional.class);
		if (hasClassInterceptor && sessionBean) {
			event.addDefinitionError(new RuntimeException(
					"@Transactional is forbidden for session bean "
							+ event.getBean()));
		} else {
			TransactionAttribute classAttr = annotatedType.getAnnotation(TransactionAttribute.class);
			for (AnnotatedMethod<? super X> am : annotatedType.getMethods()) {
				boolean hasMethodInterceptor = am.isAnnotationPresent(Transactional.class);
				if (hasMethodInterceptor && sessionBean) {
					event.addDefinitionError(new RuntimeException(
							"@Transactional is forbidden for session bean method "
									+ am));
				} else if (hasClassInterceptor || hasMethodInterceptor) {
					TransactionAttribute attr = am.getAnnotation(TransactionAttribute.class);
					Method method = am.getJavaMember();
					TransactionAttributeType attrType = mergeTransactionAttributes(
							classAttr, attr);
					transactionAttributes.put(method, attrType);
				}
			}
		}
	}

	/**
	 * Observes {@link AfterBeanDiscovery} event.
	 * 
	 * @param event
	 *            the event
	 * @param beanManager
	 *            the bean manager {@link AfterBeanDiscovery} event.
	 *            {@link BeanManager}.
	 */
	void afterBeanDiscovered(@Observes AfterBeanDiscovery event,
			BeanManager beanManager) {
		event.addContext(new TransactionalContext(beanManager));
	}

	/**
	 * Calculates a transaction attribute for the specified method. If there is
	 * no attribute declared directly on the method, the class attribute is
	 * used.
	 * 
	 * @param classAttribute
	 *            the class attribute
	 * @param methodAttribute
	 *            the method attribute
	 * @return transaction attribute of the method. {@link TransactionAttribute}
	 *         of the class. {@link TransactionAttribute} of the method.
	 */
	private TransactionAttributeType mergeTransactionAttributes(TransactionAttribute classAttribute,
			TransactionAttribute methodAttribute) {
		if (methodAttribute != null) {
			return methodAttribute.value();
		}

		if (classAttribute != null) {
			return classAttribute.value();
		}

		return TransactionAttributeType.REQUIRED;
	}

	/**
	 * Retrieves a transaction attribute for the specified method.
	 * 
	 * @param method
	 *            the method.
	 * @return a transaction attribute or {@code null} if the method is not
	 *         transactional.
	 */
	TransactionAttributeType getTransactionAttribute(Method method) {
		return transactionAttributes.get(method);
	}

	/**
	 * Annotation literal for {@link Transactional}.
	 * 
	 * @author PraderaTechnologies.
	 * @version 1.0 , 20/06/2013
	 */
	@SuppressWarnings("all")
	private static class TransactionalLiteral extends
			AnnotationLiteral<Transactional> implements Transactional {

		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = -5204983598749555200L;

		/** The Constant INSTANCE. */
		static final TransactionalLiteral INSTANCE = new TransactionalLiteral();

		/**
		 * Instantiates a new transactional literal.
		 */
		private TransactionalLiteral() {
		}
	}
}
