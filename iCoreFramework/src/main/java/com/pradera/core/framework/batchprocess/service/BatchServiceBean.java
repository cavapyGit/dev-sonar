package com.pradera.core.framework.batchprocess.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.ParallelProcess;
import com.pradera.core.framework.batchprocess.SequentialProcess;
import com.pradera.core.framework.batchprocess.to.BatchRegisterTO;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.type.BatchProcessType;
import com.pradera.model.process.type.ProcessLoggerStateType;
import com.pradera.model.process.type.ProcessType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BatchServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/03/2013
 */
@Stateless
public class BatchServiceBean implements Serializable  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6835934586705084893L;

	/** The em. */
	@Inject
	@DepositaryDataBase
	private EntityManager em;
	
	/** The manager parameter. */
	@Inject
	ManagerParameter managerParameter;
	
	/** The session context. */
	@Resource
	SessionContext sessionContext;
	
	/** The batch notification. */
	@Inject
	@Any
	Event<BatchRegisterTO> batchNotification;
	
	/** The loader entity service. */
	@EJB
	LoaderEntityServiceBean loaderEntityService;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Register batch.
	 *
	 * @param userName the user name
	 * @param process the process
	 * @param parameters the parameters
	 * @return the batch register to
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public BatchRegisterTO registerBatch(String userName,BusinessProcess process, Map<String,Object> parameters) throws ServiceException {
		//Read process configuration
		process = em.find(BusinessProcess.class, process.getIdBusinessProcessPk());
		//Instance processLogger
		ProcessLogger processLogger = new ProcessLogger();
		processLogger.setBusinessProcess(process);
		processLogger.setLoggerState(ProcessLoggerStateType.REGISTERED.getCode());
		processLogger.setProcessDate(CommonsUtilities.currentDateTime());
		processLogger.setIdUserAccount(userName);
		//register parameters
		managerParameter.getParametersProcessLogger(processLogger, parameters);
		//persisted Logger
		setAuditFields(processLogger);
		em.persist(processLogger);
		BatchRegisterTO batchRegisterTO = new BatchRegisterTO(processLogger.getIdProcessLoggerPk(), process);
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if (object instanceof LoggerUser) {
			batchRegisterTO.setLoggerUser((LoggerUser)object);
		}
		return batchRegisterTO;
	}
	
	/**
	 * Register batchprocess in new tx.
	 * Using when is call from services layer and you need register in
	 * independent trasaction
	 * @param userName the user name
	 * @param process the process
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public void registerBatchTx(String userName,BusinessProcess process, Map<String,Object> parameters) throws ServiceException{
		//How is required_new transaction dont propagate transactionRegistry, so we propagate with threadLocal
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem()== null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		BatchRegisterTO batchRegisterTO = sessionContext.getBusinessObject(BatchServiceBean.class).registerBatch(userName,process,parameters);
		if (batchRegisterTO.getProcess().getProcessType().equals(ProcessType.BATCH.getCode())) {
			sessionContext.getBusinessObject(BatchServiceBean.class).startBatch(batchRegisterTO);	
		}
	}
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	public BatchRegisterTO registerScheduledBatch(BusinessProcess process, Map<String,Object> parameters) throws ServiceException {
   	 
		LoggerUser loggerUser = new LoggerUser();
		loggerUser.setIpAddress("127.0.0.1");
		loggerUser.setUserName("ADMIN");
		loggerUser.setIdUserSessionPk(1l);
		loggerUser.setSessionId("sessssxkxkdd");
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
	
		//Read process configuration
		process = em.find(BusinessProcess.class, process.getIdBusinessProcessPk());
		//Instance processLogger
		ProcessLogger processLogger = new ProcessLogger();
		processLogger.setBusinessProcess(process);
		processLogger.setLoggerState(ProcessLoggerStateType.REGISTERED.getCode());
		processLogger.setProcessDate(CommonsUtilities.currentDateTime());
		processLogger.setIdUserAccount(loggerUser.getUserName());
		//register parameters
		managerParameter.getParametersProcessLogger(processLogger, parameters);
		//persisted Logger
		setScheduleAuditFields(processLogger, loggerUser);
		em.persist(processLogger);
		BatchRegisterTO batchRegisterTO = new BatchRegisterTO(processLogger.getIdProcessLoggerPk(), process);
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if (object instanceof LoggerUser) {
			batchRegisterTO.setLoggerUser((LoggerUser)object);
		}
		return batchRegisterTO;
	}
	
	/**
	 * Start batch.
	 *
	 * @param batchRegisterTO the batch register
	 */
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void startBatch(BatchRegisterTO batchRegisterTO) {
		//Not begin transaction
		if (batchRegisterTO.getProcess().getIndSequenceBatch() != null) {
			ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), batchRegisterTO.getLoggerUser());
			BatchProcessType batchProcessType = BatchProcessType.get(batchRegisterTO.getProcess().getIndSequenceBatch());
			switch (batchProcessType) {
			case SEQUENTIAL:
				batchNotification = batchNotification.select(new AnnotationLiteral<SequentialProcess>() {});
				break;
				
			case PARALELL :
				batchNotification = batchNotification.select(new AnnotationLiteral<ParallelProcess>() {});
				break;
			default:
				//nothing
				break;
			}
			//fire event
			batchNotification.fire(batchRegisterTO);
		}

	}
	
	/**
	 * Sets the audit fields.
	 *
	 * @param processLogger the new audit fields
	 */
	private void setAuditFields(ProcessLogger processLogger) {
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if (object instanceof LoggerUser) {
			if (processLogger instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				Auditable auditable = (Auditable)processLogger;
				auditable.setAudit(loggerUser);
				Map<String,List<? extends Auditable>> listAudit = auditable.getListForAudit();
				if (listAudit != null) {
					for(List<? extends Auditable> auditableList :listAudit.values()) {
						loaderEntityService.setAudit(auditableList, loggerUser);
					}
				}
			}
		}
	}
	
	private void setScheduleAuditFields(ProcessLogger processLogger, LoggerUser loggerUser) {
		if (processLogger instanceof Auditable) {
			Auditable auditable = (Auditable)processLogger;
			auditable.setAudit(loggerUser);
			Map<String,List<? extends Auditable>> listAudit = auditable.getListForAudit();
			if (listAudit != null) {
				for(List<? extends Auditable> auditableList :listAudit.values()) {
					loaderEntityService.setAudit(auditableList, loggerUser);
				}
			}
		}
	}
	
	
	@SuppressWarnings("rawtypes")
	public boolean isExecutingBatchProcess(Long idBusinessProcess, Long idProcessLogger) {
		StringBuilder stringBuilder= new StringBuilder();
		stringBuilder.append(" SELECT PL.idProcessLoggerPk ");
		stringBuilder.append(" FROM ProcessLogger PL ");
		stringBuilder.append(" WHERE PL.businessProcess.idBusinessProcessPk = :idBusinessProcess ");
		stringBuilder.append(" and PL.loggerState in (:lstProcessLoggerState) ");
		stringBuilder.append(" and PL.processDate >= :compareDateTime "); 
		if (Validations.validateIsNotNull(idProcessLogger)) {
			stringBuilder.append(" and PL.idProcessLoggerPk <> :idProcessLogger ");
		}
		
		Query query= em.createQuery(stringBuilder.toString());
		query.setParameter("idBusinessProcess", idBusinessProcess);
		List<Integer> lstProcessLoggerState= new ArrayList<Integer>();
		lstProcessLoggerState.add(ProcessLoggerStateType.REGISTERED.getCode());
		lstProcessLoggerState.add(ProcessLoggerStateType.PROCESSING.getCode());
		query.setParameter("lstProcessLoggerState", lstProcessLoggerState);
		Date compareDateTime= CommonsUtilities.addMinutes(CommonsUtilities.currentDateTime(), -2); //2 minutes ago
		query.setParameter("compareDateTime", compareDateTime);
		if (Validations.validateIsNotNull(idProcessLogger)) {
			query.setParameter("idProcessLogger", idProcessLogger);
		}
		List lstProcesses= query.getResultList();
		if (Validations.validateListIsNotNullAndNotEmpty(lstProcesses)) {
			return true;
		}
		return false;
	}
	
	
	public void updateProcessErrorDetail(Long idProcessLogger, String errorDetail) {
		StringBuilder stringBuilder= new StringBuilder();
		stringBuilder.append(" UPDATE ProcessLogger SET errorDetail = :errorDetail ");
		stringBuilder.append(" WHERE idProcessLoggerPk = :idProcessLogger ");
		
		Query query= em.createQuery(stringBuilder.toString());
		query.setParameter("errorDetail", errorDetail);
		query.setParameter("idProcessLogger", idProcessLogger);
		
		query.executeUpdate();
	}
	
}
