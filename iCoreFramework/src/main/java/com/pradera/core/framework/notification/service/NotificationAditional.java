package com.pradera.core.framework.notification.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.notification.DestinationNotification;
import com.pradera.model.notification.NotificationConfiguration;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.process.BusinessProcess;

@Stateless
public class NotificationAditional {

	@EJB
	private LoaderEntityServiceBean loaderEntityService;
	
	/**
	 * Method to get notification by business process
	 * @param bProcessList
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer,List<BusinessProcess>> getProcessNotificationByType(List<Long> bProcessList){
		StringBuilder sbQuery = new StringBuilder();
//		Map<String,Object> parameter = new HashMap<String,Object>();
//		Map<Integer,List<ProcessNotification>> dataInMap = new HashMap<Integer,List<ProcessNotification>>();
		
		sbQuery.append("Select bproc.idBusinessProcessPk,										");
		
		sbQuery.append("	   notif.idProcessNotificationPk, 									");
		sbQuery.append("	   notif.notificationType, notif.destinationType,					");
		sbQuery.append("	   notif.notificationMessage, notif.notificationSubject,			");
		sbQuery.append("	   notif.institutionType, notif.systemProfile,						");
		sbQuery.append("	   notif.priority, 													");
		
		sbQuery.append("	   config.idNotificationPk, config.paramValue,						");
		sbQuery.append("	   destin.idDestinationPk, destin.email, destin.idUserAccount		");
		sbQuery.append("From ProcessNotification notif											");
		sbQuery.append("Inner Join notif.businessProcess bproc									");
		sbQuery.append("Left Join bproc.processConfigurations config							");
		sbQuery.append("Left Join notif.destinationNotifications destin							");
		sbQuery.append("Where notif.businessProcess.idBusinessProcessPk	In (:bProcessPk)		");
		sbQuery.append("	  And notif.indStatus = :oneParam									");
		sbQuery.append("	  And bproc.indNotices = :oneParam									");
		
		
		Query query = loaderEntityService.getEm().createNamedQuery(sbQuery.toString());
		query.setParameter("bProcessPk", bProcessList);
		query.setParameter("oneParam", BooleanType.YES.getCode());
		
		return populateDataFromNofication((List<Object[]>)query.getResultList());
	}
	
	
	public Map<Integer,List<BusinessProcess>> populateDataFromNofication(List<Object[]> dataInObjectList){
		Map<Integer,List<BusinessProcess>> dataForNotification = new HashMap<>();
		
		for(Object[] data: dataInObjectList){
			Long pProcessPk = (Long) data[0];
			
			Long notificationPk 	= (Long) data[1];
			Integer notType 		= (Integer) data[2];
			Integer destinatType 	= (Integer) data[3];
			String notificationMssg = data[4].toString();
			String notificationSbjt = data[5].toString();
			Integer institutionType = (Integer) data[6];
			Integer systemProfile 		= (Integer) data[7];
			Integer indPriority 	= (Integer) data[8];
			
			Long configurationPk 		= (Long) data[9];
			String configurationValue 	= data[10].toString();
			
			Long destinationPk = (Long) data[11];
			String destinationMail = (String) data[12];
			String destinationUser = (String) data[13];
			
			/**Setting data in map, if doesn't exists*/
			if(dataForNotification.get(notType)==null){
				dataForNotification.put(notType, new ArrayList<BusinessProcess>());
			}
			
			/**creating business process like object*/
			BusinessProcess bProcess = new BusinessProcess();
			bProcess.setIdBusinessProcessPk(pProcessPk);
			bProcess.setProcessConfigurations(new ArrayList<NotificationConfiguration>());
			
			/**creating process notification*/
			ProcessNotification pNotification = new ProcessNotification();
			pNotification.setIdProcessNotificationPk(notificationPk);
			pNotification.setDestinationType(destinatType);
			pNotification.setNotificationMessage(notificationMssg);
			pNotification.setNotificationSubject(notificationSbjt);
			pNotification.setInstitutionType(institutionType);
			pNotification.setSystemProfile(systemProfile);
			pNotification.setPriority(indPriority);
			pNotification.setDestinationNotifications(new ArrayList<DestinationNotification>());
			
			/**creating destination configuration from object [] data*/
			DestinationNotification nDestination = null;
			if(destinationPk!=null){
				nDestination = new DestinationNotification();
				nDestination.setIdDestinationPk(destinationPk);
				nDestination.setEmail(destinationMail);
				nDestination.setIdUserAccount(destinationUser);
			}
			
			/**creating notification configuration based on parameters from query*/
			NotificationConfiguration nConfiguration = null;
			if(configurationPk!=null){
				/**Add notification configuration in new object reference*/
				nConfiguration = new NotificationConfiguration();
				nConfiguration.setIdNotificationPk(configurationPk);
				nConfiguration.setParamValue(configurationValue);
			}
			
			
			/**Verified if notification exists in list*/
			if(!dataForNotification.get(notType).contains(bProcess)){
				dataForNotification.get(notType).add(bProcess);			
			}
			
			/**getting list of business process from hash map*/
			List<BusinessProcess> listBusinessProcess = dataForNotification.get(notType);
			
			/**getting business process from list added*/
			Integer indxBProcess = listBusinessProcess.indexOf(bProcess);
			BusinessProcess bProcessFromMap = listBusinessProcess.get(indxBProcess);
			
			if(bProcessFromMap.getProcessNotifications()==null){
				bProcessFromMap.setProcessNotifications(new ArrayList<ProcessNotification>());
			}
			
			/**If notification configuration doesn't exist add it*/
			if(!bProcessFromMap.getProcessConfigurations().contains(nConfiguration)){
				bProcessFromMap.getProcessConfigurations().add(nConfiguration);
			}
			
			/**getting list of process notification from hash map*/
			List<ProcessNotification> listProcessNotification = bProcessFromMap.getProcessNotifications();
			
			if(!listProcessNotification.contains(pNotification)){
				listProcessNotification.add(pNotification);
			}
			
			/**getting process notification from business process list*/
			Integer indxPNotification = listProcessNotification.indexOf(pNotification);
			ProcessNotification pNotificationFromMap = listProcessNotification.get(indxPNotification);
			
			if(nDestination!=null){
				/**If destination doesn't exists in process notification, we must add it*/
				if(!pNotificationFromMap.getDestinationNotifications().contains(nDestination)){
					pNotificationFromMap.getDestinationNotifications().add(nDestination);
				}
			}
		}
		
		return dataForNotification;
	}
}