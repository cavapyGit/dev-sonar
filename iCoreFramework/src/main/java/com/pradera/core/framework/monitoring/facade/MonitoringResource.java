package com.pradera.core.framework.monitoring.facade;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.pradera.core.framework.monitoring.Invocation;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class MonitoringResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/10/2014
 */ 
@Singleton
@Startup
@LocalBean
@Path("monitoring")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class MonitoringResource implements MonitoringResourceMXBean {
	
	/** The sc. */
	@Resource
    SessionContext sc;
	
	/** The module name. */
	@Resource(lookup="java:module/ModuleName")
	private String moduleName;
	
	/** The platform m bean server. */
	private MBeanServer platformMBeanServer;
    
    /** The object name. */
    private ObjectName objectName = null;
    
    /** The methods. */
    private ConcurrentHashMap<String, Invocation> methods = new ConcurrentHashMap<String, Invocation>();
    
    /** The diagnostics. */
    private ConcurrentHashMap<String, String> diagnostics = new ConcurrentHashMap<String, String>();
    
    /** The exception count. */
    private AtomicLong exceptionCount;
    
    /**
     * Register in jmx.
     */
    @PostConstruct
    public void registerInJMX() {
        this.exceptionCount = new AtomicLong();
        try {
            objectName = new ObjectName(moduleName+"Monitoring:type=" + this.getClass().getName());
            platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
            platformMBeanServer.registerMBean(sc.getBusinessObject(MonitoringResource.class), objectName);
        } catch (Exception e) {
            throw new IllegalStateException("Problem during registration of Monitoring into JMX:" + e);
        }
    }
    
    /**
     * Gets the slowest methods.
     *
     * @param maxResult the max result
     * @return the slowest methods
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Invocation> getSlowestMethods(@QueryParam("max") @DefaultValue("50") int maxResult) {
        List<Invocation> list = new ArrayList<Invocation>(methods.values());
        Collections.sort(list);
        Collections.reverse(list);
        if (list.size() > maxResult)
            return list.subList(0, maxResult);
        else
            return list;
    }

    /* (non-Javadoc)
     * @see com.pradera.core.framework.monitoring.facade.MonitoringResourceMXBean#getNumberOfExceptions()
     */
    @GET
    @Path("exceptionCount")
    @Produces(MediaType.TEXT_PLAIN)
    @Override
    public String getNumberOfExceptions() {
        return String.valueOf(exceptionCount.get());
    }

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.monitoring.facade.MonitoringResourceMXBean#getSlowestMethods()
	 */
	@Override
	public List<Invocation> getSlowestMethods() {
		// TODO Auto-generated method stub
		return getSlowestMethods(50);
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.monitoring.facade.MonitoringResourceMXBean#getDiagnostics()
	 */
	@Override
	public Map<String, String> getDiagnostics() {
		// TODO Auto-generated method stub
		return diagnostics;
	}


	/**
	 * Gets the diagnostics as string.
	 *
	 * @return the diagnostics as string
	 */
	@GET
    @Path("diagnostics")
    @Produces(MediaType.TEXT_PLAIN)
    public String getDiagnosticsAsString() {
        return getDiagnostics().toString();
    }

    /**
     * Gets the diagnostics for key.
     *
     * @param key the key
     * @return the diagnostics for key
     */
    @GET
    @Path("diagnostics/{key}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getDiagnosticsForKey(@PathParam("key") String key) {
        return getDiagnostics().get(key);
    }
    
    /* (non-Javadoc)
     * @see com.pradera.core.framework.monitoring.facade.MonitoringResourceMXBean#clear()
     */
    @Override
    @DELETE
    public void clear() {
        methods.clear();
        exceptionCount.set(0);
        diagnostics.clear();
    }
    
    /**
     * Unregister from jmx.
     */
    @PreDestroy
    public void unregisterFromJMX() {
        try {
            platformMBeanServer.unregisterMBean(this.objectName);
        } catch (Exception e) {
            throw new IllegalStateException("Problem during unregistration of Monitoring into JMX:" + e);
        }
    }


    /**
     * Adds the.
     *
     * @param invocation the invocation
     */
    public void add(Invocation invocation) {
        String methodName = invocation.getMethodName();
        if (methods.containsKey(methodName)) {
            Invocation existing = methods.get(methodName);
            if (existing.isSlowerThan(invocation)) {
                return;
            }
        }
        methods.put(methodName, invocation);
    }

    /**
     * Adds the.
     *
     * @param methodName the method name
     * @param performance the performance
     */
    public void add(String methodName, long performance) {
        Invocation invocation = new Invocation(methodName, performance);
        this.add(invocation);
    }
    
    /**
     * Adds the.
     *
     * @param methodName the method name
     * @param performance the performance
     * @param parametersMethod the parameters method
     */
    public void add(String methodName, long performance, String parametersMethod) {
        Invocation invocation = new Invocation(methodName, performance,parametersMethod);
        this.add(invocation);
    }

    /**
     * Exception occurred.
     *
     * @param methodName the method name
     * @param e the e
     */
    public void exceptionOccurred(String methodName, Exception e) {
        exceptionCount.incrementAndGet();
    }

//future implementacion
//    public void onNewDiagnostics(@Observes Diagnostics diagnostics) {
//        Map<String, String> map = diagnostics.asMap();
//        if (map != null) {
//            this.diagnostics.putAll(map);
//        }
//    }

  


    

}
