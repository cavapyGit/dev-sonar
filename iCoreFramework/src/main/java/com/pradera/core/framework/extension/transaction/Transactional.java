package com.pradera.core.framework.extension.transaction;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface Transactional.
 * Interceptor binding for container transaction management. You can use
 * {@link TransactionAttribute} to control the scope of the transaction.
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2013
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@InterceptorBinding
public @interface Transactional {

}
