package com.pradera.core.framework.notification.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.MissingFormatArgumentException;
import java.util.Properties;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

//import net.markenwerk.utils.mail.smime.SmimeKey;
//import net.markenwerk.utils.mail.smime.SmimeKeyStore;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.contextholder.interceptor.AsynchronousLoggerInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.NotificationEvent;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notification.type.NotificationInformation;
import com.pradera.commons.security.model.type.ExternalInterfaceType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.core.framework.usersaccount.service.UserAccountControlServiceBean;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;
import com.pradera.model.notification.DestinationNotification;
import com.pradera.model.notification.NotificationLogger;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.notification.type.NotificationDestinationType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.type.SendNotificationType;
import com.edv.collection.economic.rigth.PaymentAgent;




/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class NotificationSenderServiceBean.
 * using for send event of type notification
 * @author PraderaTechnologies.
 * @version 1.0 , 01/12/2012
 */
@Stateless
@Interceptors(AsynchronousLoggerInterceptor.class)
public class NotificationServiceBean {
	
	 private final Integer TYPE_SIGNATURE_SING = 2444;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The loader entity service. */
	@EJB
	private LoaderEntityServiceBean loaderEntityService;
	
	/** The user account control service. */
	@EJB
	private UserAccountControlServiceBean userAccountControlService;
	
	@Inject
	private NotificationEmailAdjuntServiceBean adjuntServiceBean;
	
	
	
	/** The event process. */
	@Inject
	@NotificationEvent(NotificationType.PROCESS)
	private Event<BrowserWindow> eventProcess;
	
	/** The event message. */
	@Inject
	@NotificationEvent(NotificationType.MESSAGE)
	private Event<BrowserWindow> eventMessage;
	
	@Inject
	@NotificationEvent(NotificationType.REPORT)
	private Event<BrowserWindow> eventReport;
	
	/** The event email. */
	@Inject
	@NotificationEvent(NotificationType.EMAIL)
	private Event<BrowserWindow> eventEmail;
	
	/** The application configuration. */
	@Inject
	@InjectableResource(location = "GenericConstants.properties")
	Properties applicationConfiguration;
	
	
	/** The em. */
	@Inject
	@DepositaryDataBase
	EntityManager em;
	
	/** The transaction registry. */
	@javax.annotation.Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The session context. */
	@javax.annotation.Resource
	private SessionContext sessionContext;
	
	public void sendEmailNotificationMasively(LoggerUser loggerUser, List<BusinessProcess> bProcessList, Map<Long,List<NotificationInformation>> data){
		List<BrowserWindow> allNotification = new ArrayList<>();
		for(BusinessProcess bProcess: bProcessList){
			allNotification.addAll(getBrowserWindowList(bProcess.getProcessNotifications()));
		}		
		for(BrowserWindow browseNotification: allNotification){
			eventEmail.fire(browseNotification);
		}
	}
	
	public void sendNotificationMasively(LoggerUser loggerUser, List<BusinessProcess> bProcessList, Object institutionCode){
		List<BrowserWindow> allNotification = new ArrayList<>();
		for(BusinessProcess bProcess: bProcessList){
			allNotification.addAll(getBrowserWindowList(bProcess.getProcessNotifications()));
		}		
		for(BrowserWindow browseNotification: allNotification){
			eventMessage.fire(browseNotification);
		}
	}
	
	public List<BrowserWindow> getBrowserWindowList(List<ProcessNotification> pNotificationList){		
		List<BrowserWindow> browserWindowList = new ArrayList<>();		
		for(ProcessNotification pNotification: pNotificationList){
			BrowserWindow bWindow = new BrowserWindow();
			
			bWindow.setNotificationType(pNotification.getNotificationType());
//			bWindow.set
			
			browserWindowList.add(new BrowserWindow());
		}		
		return browserWindowList;
	}
	
	/**
	 * Fire process batch notification.
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param processLogger the process logger
	 * @param intitutionCode the intitution code
	 */
	@Asynchronous
	public void sendProcessBatchNotification(LoggerUser loggerUser,String userName,ProcessLogger processLogger,Object intitutionCode,Object[] parameters) {
		//Only send data of process finish
		if (processLogger != null) {
			List<NotificationLogger> notificationLoggers = createDestinations(userName, processLogger.getBusinessProcess(), null ,intitutionCode,NotificationType.PROCESS,null);
			//check notifications to fire
			if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
				for(NotificationLogger notificationLogger : notificationLoggers) {
					notificationLogger.setProcessLogger(processLogger);
					formatMessage(notificationLogger,parameters);
					setAuditFields(notificationLogger);
					em.persist(notificationLogger);
					eventProcess.fire(createBrowserWindow(notificationLogger));
				}
			}
		} else {
			log.error("Error ProcessLogger is null,not sending notification");
		}
	}
	

	@Asynchronous
	public void sendNotificationEmailProfileNoConfig(LoggerUser loggerUser,BusinessProcess businessProcess ,String subject, String message, Integer idSystemProfilePk,Object[] parameters){
		List<NotificationLogger> notificationLoggers = createEmailProfileNoConfigDestinations(loggerUser.getUserName(), businessProcess, subject, message, idSystemProfilePk);
		//check notifications to fire
		if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
			for(NotificationLogger notificationLogger : notificationLoggers) {
				formatMessage(notificationLogger,parameters);
				setAuditFields(notificationLogger);
				System.out.println("------------------- start persistence sendNotificationEmailProfileNoConfig "+notificationLogger.toString());
				em.persist(notificationLogger);
				System.out.println("------------------- end persistence sendNotificationEmailProfileNoConfig "+notificationLogger.toString());
				eventMessage.fire(createBrowserWindowEmail(notificationLogger));
			}
		}
	}
	
	@Asynchronous
	public void sendNotificationEmailParticipantNoConfig(LoggerUser loggerUser,BusinessProcess businessProcess ,String subject, String message, Long idParticipantPk,Object[] parameters){
		List<NotificationLogger> notificationLoggers = createEmailParticipantNoConfigDestinations(loggerUser.getUserName(), businessProcess, subject, message, idParticipantPk);
		//check notifications to fire
		if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
			for(NotificationLogger notificationLogger : notificationLoggers) {
				formatMessage(notificationLogger,parameters);
				setAuditFields(notificationLogger);
				System.out.println("------------------- start persistence sendNotificationEmailParticipantNoConfig "+notificationLogger.toString());
				em.persist(notificationLogger);
				System.out.println("------------------- end persistence sendNotificationEmailParticipantNoConfig "+notificationLogger.toString());
				eventMessage.fire(createBrowserWindowEmail(notificationLogger));
			}
		}
	}
	
	
	@Asynchronous
	public void sendParticipantNotificationEmail(LoggerUser loggerUser,BusinessProcess businessProc, Long idParticipantPk,Object[] parameters){
		if (businessProc != null && businessProc.getIdBusinessProcessPk() !=null) {
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
			List<NotificationLogger> notificationLoggers = createParticipantEmailDestinations(loggerUser.getUserName(), businessProc, idParticipantPk);
			//check notifications to fire
			if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
				for(NotificationLogger notificationLogger : notificationLoggers) {
					formatMessage(notificationLogger,parameters);
					setAuditFields(notificationLogger);
					em.persist(notificationLogger);
					eventMessage.fire(createBrowserWindow(notificationLogger));
				}
			}
		}
	}
	
	/**
	 * Send message notification.
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode the intitution code
	 * @param parameters the parameters
	 */
	@Asynchronous
	public void sendNotification(LoggerUser loggerUser,String userName,BusinessProcess businessProc,Object intitutionCode,Object[] parameters, UserAccountSession userAccountSession){
		if (businessProc != null && businessProc.getIdBusinessProcessPk() !=null) {
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
			List<NotificationLogger> notificationLoggers = createDestinations(userName,businessProc, null,intitutionCode,null,userAccountSession);
			//check notifications to fire
			if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
				for(NotificationLogger notificationLogger : notificationLoggers) {
					formatMessage(notificationLogger,parameters);
					setAuditFields(notificationLogger);
					em.persist(notificationLogger);
					eventMessage.fire(createBrowserWindow(notificationLogger));
				}
			}
		}
	}
	
	@Asynchronous
	public void sendNotification(LoggerUser loggerUser,String userName,BusinessProcess businessProc, Long idHolderAccountPk,Object intitutionCode,Object[] parameters, UserAccountSession userAccountSession){
		if (businessProc != null && businessProc.getIdBusinessProcessPk() !=null) {
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
			List<NotificationLogger> notificationLoggers = createDestinations(userName,businessProc, idHolderAccountPk,intitutionCode,null,userAccountSession);
			//check notifications to fire
			if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
				for(NotificationLogger notificationLogger : notificationLoggers) {
					formatMessage(notificationLogger,parameters);
					setAuditFields(notificationLogger);
					em.persist(notificationLogger);
					eventMessage.fire(createBrowserWindow(notificationLogger));
				}
			}
		}
	}
	
	/**
	 * Send message notification.
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode the intitution code
	 * @param parameters the parameters
	 */
	@Asynchronous
	public void sendNotification(LoggerUser loggerUser,String userName,BusinessProcess businessProc,Object intitutionCode,Object[] parameters){		
		if (businessProc != null && businessProc.getIdBusinessProcessPk() !=null) {
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());			
			List<NotificationLogger> notificationLoggers = createDestinations(userName,businessProc, null,intitutionCode,null,null);
			//check notifications to fire
			if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
				for(NotificationLogger notificationLogger : notificationLoggers) {
					formatMessage(notificationLogger,parameters);
					setAuditFields(notificationLogger);
					em.persist(notificationLogger);
					eventMessage.fire(createBrowserWindow(notificationLogger));
				}
			}
		}
	}
	
	/**
	 * Send notification to one list of(Participants or Issures users).
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode the intitution code
	 * @param parameters the parameters
	 */
	@Asynchronous
	public void sendNotificationGroup(LoggerUser loggerUser,String userName,BusinessProcess businessProc,List<Object> intitutionCode,Object[] parameters){
		if (businessProc != null && businessProc.getIdBusinessProcessPk() !=null) {
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
			if(intitutionCode != null && !intitutionCode.isEmpty()){
				for(Object institution : intitutionCode){
					sendNotification(loggerUser,userName,businessProc,institution,parameters,null);
				}
			}
		}
	}
	
	/**
	 * Send notification to one list of(Participants or Issures users).
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode the intitution code
	 * @param parameters the parameters
	 */
	@Asynchronous
	public void sendNotificationGroup(LoggerUser loggerUser,String userName,BusinessProcess businessProc,List<Object> intitutionCode,Object[] parameters,UserAccountSession userAccountSession){
		if (businessProc != null && businessProc.getIdBusinessProcessPk() !=null) {
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
			if(intitutionCode != null && !intitutionCode.isEmpty()){
				for(Object institution : intitutionCode){
					sendNotification(loggerUser,userName,businessProc,institution,parameters,userAccountSession);
				}
			}
		}
	}
	
	/**
	 * Send notification manual.
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param users the users
	 * @param subject the subject
	 * @param message the message
	 * @param notification the notification
	 */
	@Asynchronous
	public void sendNotificationManual(LoggerUser loggerUser,String userName,BusinessProcess businessProc,List<UserAccountSession> users,String subject,String message,
			NotificationType notification){
		log.info("Start  method sendNotificationManual() ");
		if(businessProc!= null && StringUtils.isNotBlank(userName)){
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
			List<NotificationLogger> notificationLoggers = createNotifications(userName,businessProc,users,subject,message,notification);
			//check notifications to fire
			if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
				StringBuilder destinations = new StringBuilder(100);
				for(NotificationLogger notificationLogger : notificationLoggers) {
					setAuditFields(notificationLogger);
					em.persist(notificationLogger);
					if(!notification.equals(NotificationType.EMAIL)){
						if(notification.equals(NotificationType.MESSAGE)) {
							eventMessage.fire(createBrowserWindow(notificationLogger));
						} else if(notification.equals(NotificationType.REPORT)){
							eventReport.fire(createBrowserWindow(notificationLogger));
						} else if(notification.equals(NotificationType.PROCESS)){
							eventProcess.fire(createBrowserWindow(notificationLogger));
						}
					} else {
						//only for email send one email for all receptors
						destinations.append(notificationLogger.getDestEmail()+";");
					}
					
				}
				if(notification.equals(NotificationType.EMAIL)){
					NotificationLogger notificationLogger = new NotificationLogger();
					notificationLogger.setNotificationType(notification.getCode());
					notificationLogger.setNotificationMessage(message);
					notificationLogger.setNotificationSubject(subject);
					//remove end ;
					notificationLogger.setDestEmail(StringUtils.removeEnd(destinations.toString(),";"));
					eventEmail.fire(createBrowserWindow(notificationLogger));
				}
			}
		}
		
		log.info("End  method sendNotificationManual() ");
	}
	
	/**
	 * Creates the notifications.
	 *
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param users the users
	 * @param subject the subject
	 * @param message the message
	 * @param notification the notification
	 * @return the list
	 */
	private List<NotificationLogger> createNotifications(String userName,BusinessProcess businessProc,List<UserAccountSession> users,String subject,String message,
			NotificationType notification){
		List<NotificationLogger> notifications = null;
		businessProc = loaderEntityService.loadEntity(businessProc,businessProc.getIdBusinessProcessPk());
		SendNotificationType sendNotification = SendNotificationType.get(businessProc.getIndNotices());
		if (sendNotification.equals(SendNotificationType.ON)) {
			if(users != null && !users.isEmpty() && StringUtils.isNotBlank(subject) && StringUtils.isNotBlank(message)){
				notifications = new ArrayList<NotificationLogger>();
				for(UserAccountSession userAccount : users){
					NotificationLogger notificationLogger = new NotificationLogger();
					notificationLogger.setBusinessProcess(businessProc);
					notificationLogger.setSourceUserName(userName);
					//trunc subject longs
					notificationLogger.setNotificationSubject(StringUtils.substring(subject, 0, 100));
					notificationLogger.setNotificationMessage(message);
					notificationLogger.setNotificationType(notification.getCode());
					notificationLogger.setIndDelivered(BooleanType.NO.getCode());
					notificationLogger.setRegistryDate(CommonsUtilities.currentDateTime());
					notificationLogger.setDestEmail(userAccount.getEmail());
					notificationLogger.setDestMobileNumber(userAccount.getPhoneNumber());
					notificationLogger.setDestUserName(userAccount.getUserName());
					notificationLogger.setDestFullName(userAccount.getFullName());
					notifications.add(notificationLogger);
				}
			}
		}
		return notifications;
	}
	
	
	private List<NotificationLogger> createEmailProfileNoConfigDestinations(String userName, BusinessProcess businessProcess ,String subject, String message, Integer idSystemProfilePk) {
		List<NotificationLogger> notificationLoggers = null;
		if(businessProcess != null) {
			businessProcess = loaderEntityService.loadEntity(businessProcess, businessProcess.getIdBusinessProcessPk());
			notificationLoggers = new ArrayList<NotificationLogger>();
			List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
			destinations.addAll(getDestinationEmailProfileNoConfig(userName, idSystemProfilePk));	
			notificationLoggers.addAll(getNotificationLoggersNoConfig(userName, businessProcess, subject, message,destinations));
		}
		return notificationLoggers;
	}
	
	private List<NotificationLogger> createEmailParticipantNoConfigDestinations(String userName, BusinessProcess businessProcess ,String subject, String message, Long idParticipantPk) {
		List<NotificationLogger> notificationLoggers = null;
		if(businessProcess != null) {
			businessProcess = loaderEntityService.loadEntity(businessProcess, businessProcess.getIdBusinessProcessPk());
			notificationLoggers = new ArrayList<NotificationLogger>();
			List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
			destinations.addAll(getDestinationEmailParticipantNoConfig(userName, idParticipantPk));	
			notificationLoggers.addAll(getNotificationLoggersNoConfig(userName, businessProcess, subject, message, destinations));
		}
		return notificationLoggers;
	}

	private List<NotificationLogger> createParticipantEmailDestinations(String userName, BusinessProcess businessProc, Long idParticipantPk) {
		List<NotificationLogger> notificationLoggers = null;
		if(businessProc != null) {
			businessProc = loaderEntityService.loadEntity(businessProc, businessProc.getIdBusinessProcessPk());
			SendNotificationType sendNotification = SendNotificationType.get(businessProc.getIndNotices());
			if (sendNotification.equals(SendNotificationType.ON)) {
				if (businessProc.getProcessNotifications() != null 
						&& !businessProc.getProcessNotifications().isEmpty()) {
					notificationLoggers = new ArrayList<NotificationLogger>();
					for(ProcessNotification processNotification : businessProc.getProcessNotifications()){
						if(processNotification.getIndStatus().equals(BooleanType.YES.getCode())){
							List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
							destinations.addAll(getParticipantDestinationEmailNotification(userName, processNotification, idParticipantPk));	
							notificationLoggers.addAll(getNotificationLoggers(userName,processNotification, destinations));
						}
					}
				}else {
					notificationLoggers = new ArrayList<NotificationLogger>();
					List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
					destinations.addAll(getParticipantDestinationEmailNotification(userName, null, idParticipantPk));	
					notificationLoggers.addAll(getNotificationLoggers(userName,null, destinations));
				}
			}
		} else {
			log.error("Error BusinessProcess is null,not sending notification");
		}
		return notificationLoggers;
	}
	
	/**
	 * Creates the destinations.
	 *
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param intitutionCode the intitution code
	 * @param notification the notification
	 * @return the list
	 */
	private List<NotificationLogger> createDestinations(String userName,BusinessProcess businessProc, Long idHolderPk,Object intitutionCode,NotificationType notification,UserAccountSession userAccountSession) {
		List<NotificationLogger> notificationLoggers = null;
		if(businessProc != null) {
			businessProc = loaderEntityService.loadEntity(businessProc, businessProc.getIdBusinessProcessPk());
			SendNotificationType sendNotification = SendNotificationType.get(businessProc.getIndNotices());
			if (sendNotification.equals(SendNotificationType.ON)) {
				if (businessProc.getProcessNotifications() != null 
						&& !businessProc.getProcessNotifications().isEmpty()) {
					notificationLoggers = new ArrayList<NotificationLogger>();
					for(ProcessNotification processNotification : businessProc.getProcessNotifications()){
						//only read active
						if(processNotification.getIndStatus().equals(BooleanType.YES.getCode())){
							NotificationType notificationType = NotificationType.get(processNotification.getNotificationType());
							List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
							//get destinations configurations(manual,dynamic,only executor)
							switch (notificationType) {
							case PROCESS:
							case MESSAGE:
							case KILLSESSION:
								destinations.addAll(getDestinationNotification(userName,
										processNotification, intitutionCode, userAccountSession));	
								break;
							case EMAIL:
								destinations.addAll(getDestinationEmailNotification(userName,
										processNotification, intitutionCode, userAccountSession));	
								break;
							}
							//get notifications data for registers in logs db
							notificationLoggers.addAll(getNotificationLoggers(userName,processNotification, destinations));
						}
					}
					
					
				} else {
					//if is enabled notifications and there is not configurations send default message from businessprocess
					notificationLoggers = new ArrayList<NotificationLogger>();
					if(notification == null){
						//default notification without configuration is message
						notificationLoggers.add(createNotificationLogger(userName, businessProc, NotificationType.MESSAGE));
					} else {
						//else is parameter notification
						notificationLoggers.add(createNotificationLogger(userName, businessProc, notification));
					}
					
				}
			}
		} else {
			log.error("Error BusinessProcess is null,not sending notification");
		}
		return notificationLoggers;
	}
	
	/**
	 * Gets the destination notification.
	 *
	 * @param userName the user name
	 * @param processNotification the process notification
	 * @param institutionCode the institution code
	 * @return the destination notification
	 */
	private List<DestinationNotification> getDestinationNotification(String userName,ProcessNotification processNotification,
			Object institutionCode, UserAccountSession userAccountSession) {
		NotificationDestinationType destination = NotificationDestinationType.get(processNotification.getDestinationType());
		List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
		switch (destination) {
		case MANUAL:
			if(userAccountSession != null) {
				for(DestinationNotification objDestinationNotification : processNotification.getDestinationNotifications()) {
					if(Validations.validateIsNotNullAndNotEmpty(userAccountSession.getInstitutionType()) && 
							userAccountSession.getInstitutionType().equals(objDestinationNotification.getInstitutionType())) {
						destinations.add(objDestinationNotification);
					}
				}
			} else {
				destinations = processNotification.getDestinationNotifications();
			}						
			break;
		case DYNAMIC:
			if(processNotification.getInstitutionType() != null && 
					Validations.validateIsNotNullAndNotEmpty(institutionCode)){				
				if(userAccountSession != null && 
						!userAccountSession.getInstitutionCode().equals(processNotification.getInstitutionType())) {
					break;
				}				
				UserFilterTO userFilter = new UserFilterTO();
				userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
				userFilter.setInstitutionType(processNotification.getInstitutionType());
				userFilter.setEntityCode(institutionCode);
				userFilter.setResponsibility(processNotification.getUserResponsability());
				userFilter.setProfileSystem(processNotification.getSystemProfile());
				List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformation(userFilter);
				if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
					for(UserAccountSession userAccount : usersAccounts){
						DestinationNotification destNotification = new DestinationNotification();
						destNotification.setIdUserAccount(userAccount.getUserName());
						if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
							destNotification.setEmail(userAccount.getEmail());
							destinations.add(destNotification);
						}						
					}
				}				
			}			
			break;			
		case EXECUTOR:
			if(Validations.validateIsNullOrEmpty(institutionCode)){
				DestinationNotification destNotification = new DestinationNotification();
				destNotification.setIdUserAccount(userName);
				destinations.add(destNotification);
			}			
			break;			
		case SUPERVISOR:
			UserFilterTO userFilter = new UserFilterTO();
			userFilter.setInstitutionType(userAccountSession.getInstitutionType());
			userFilter.setInstitutionSecurity(userAccountSession.getInstitutionCode());
			userFilter.setDepartment(userAccountSession.getDepartment());
			List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformationSupervisor(userFilter);
			for(UserAccountSession userAccount : usersAccounts){
				DestinationNotification destinationNotification = new DestinationNotification();
				destinationNotification.setIdUserAccount(userAccount.getUserName());
				destinationNotification.setEmail(userAccount.getEmail());
				destinations.add(destinationNotification);	
			}
			
			break;
		}
		
		
		return destinations;
	}

	private List<DestinationNotification> getDestinationEmailProfileNoConfig(String userName,Integer idSystemProfile) {
		//ProcessNotification processNotification
		List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();

		UserFilterTO userFilter = new UserFilterTO();
		userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
		userFilter.setProfileSystem(idSystemProfile);
		
		List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformation(userFilter);
		if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
			for(UserAccountSession userAccount : usersAccounts){
				DestinationNotification destNotification = new DestinationNotification();
				destNotification.setIdUserAccount(userAccount.getUserName());
				if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
					destNotification.setEmail(userAccount.getEmail());
					destinations.add(destNotification);
				}						
			}
		}
		
		return destinations;
	}
	
	private List<DestinationNotification> getDestinationEmailParticipantNoConfig(String userName, Long idParticipantPk) {
		//ProcessNotification processNotification
		List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();

		UserFilterTO userFilter = new UserFilterTO();
		userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
		userFilter.setInstitutionType(InstitutionType.PARTICIPANT.getCode());
		userFilter.setEntityCode(idParticipantPk);
		
		List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformationSupervisorAndOperator(userFilter);
		if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
			for(UserAccountSession userAccount : usersAccounts){
				DestinationNotification destNotification = new DestinationNotification();
				destNotification.setIdUserAccount(userAccount.getUserName());
				if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
					destNotification.setEmail(userAccount.getEmail());
					destinations.add(destNotification);
				}						
			}
		}
		
		return destinations;
	}
	
	private List<DestinationNotification> getParticipantDestinationEmailNotification(String userName,ProcessNotification processNotification,Long idParticipantPk) {
		NotificationDestinationType destination = NotificationDestinationType.get(processNotification.getDestinationType());
		List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
		UserFilterTO userFilter = new UserFilterTO();
		List<UserAccountSession> usersAccounts = null;
		switch (destination) {
		case FREE:
			userFilter = new UserFilterTO();
			userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
			userFilter.setInstitutionType(InstitutionType.PARTICIPANT.getCode());
			userFilter.setEntityCode(idParticipantPk);
			
			usersAccounts = userAccountControlService.getUsersInformation(userFilter);
			if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
				for(UserAccountSession userAccount : usersAccounts){
					DestinationNotification destNotification = new DestinationNotification();
					destNotification.setIdUserAccount(userAccount.getUserName());
					if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
						destNotification.setEmail(userAccount.getEmail());
						destinations.add(destNotification);
					}						
				}
			}
			
			break;
		case DYNAMIC:
			userFilter = new UserFilterTO();
			userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
			userFilter.setInstitutionType(InstitutionType.PARTICIPANT.getCode());
			userFilter.setEntityCode(idParticipantPk);
			
			usersAccounts = userAccountControlService.getUsersInformationSupervisorAndOperator(userFilter);
			if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
				for(UserAccountSession userAccount : usersAccounts){
					DestinationNotification destNotification = new DestinationNotification();
					destNotification.setIdUserAccount(userAccount.getUserName());
					if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
						destNotification.setEmail(userAccount.getEmail());
						destinations.add(destNotification);
					}						
				}
			}
			break;			
		case EXECUTOR:
			userFilter = new UserFilterTO();
			userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
			userFilter.setInstitutionType(InstitutionType.PARTICIPANT.getCode());
			userFilter.setEntityCode(idParticipantPk);
			
			usersAccounts = userAccountControlService.getUsersInformationOperator(userFilter);
			if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
				for(UserAccountSession userAccount : usersAccounts){
					DestinationNotification destNotification = new DestinationNotification();
					destNotification.setIdUserAccount(userAccount.getUserName());
					if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
						destNotification.setEmail(userAccount.getEmail());
						destinations.add(destNotification);
					}						
				}
			}
			break;
		case SUPERVISOR:
			userFilter = new UserFilterTO();
			userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
			userFilter.setInstitutionType(InstitutionType.PARTICIPANT.getCode());
			userFilter.setEntityCode(idParticipantPk);
			
			usersAccounts = userAccountControlService.getUsersInformationSupervisor(userFilter);
			if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
				for(UserAccountSession userAccount : usersAccounts){
					DestinationNotification destNotification = new DestinationNotification();
					destNotification.setIdUserAccount(userAccount.getUserName());
					if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
						destNotification.setEmail(userAccount.getEmail());
						destinations.add(destNotification);
					}						
				}
			}
			break;
		}
		return destinations;
	}
	/**
	 * Gets the destination email notification.
	 *
	 * @param userName the user name
	 * @param processNotification the process notification
	 * @param institutionCode the institution code
	 * @return the destination email notification
	 */
	private List<DestinationNotification> getDestinationEmailNotification(String userName,ProcessNotification processNotification,
			Object institutionCode, UserAccountSession userAccountSession) {
		NotificationDestinationType destination = NotificationDestinationType.get(processNotification.getDestinationType());
		List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
		switch (destination) {
		case MANUAL:
			if(userAccountSession != null) {
				for(DestinationNotification objDestinationNotification : processNotification.getDestinationNotifications()) {
					if(Validations.validateIsNotNullAndNotEmpty(userAccountSession.getInstitutionType()) && 
							userAccountSession.getInstitutionType().equals(objDestinationNotification.getInstitutionType())) {
						destinations.add(objDestinationNotification);
					}
				}
			} else {
				destinations = processNotification.getDestinationNotifications();
			}			
			break;
		case DYNAMIC:
			if(processNotification.getInstitutionType() != null && 
					Validations.validateIsNotNullAndNotEmpty(institutionCode)){				
				if(userAccountSession != null && 
						!userAccountSession.getInstitutionType().equals(processNotification.getInstitutionType())) {
					break;
				}				
				UserFilterTO userFilter = new UserFilterTO();
				userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
				userFilter.setInstitutionType(processNotification.getInstitutionType());
				userFilter.setEntityCode(institutionCode);
				userFilter.setResponsibility(processNotification.getUserResponsability());
				userFilter.setProfileSystem(processNotification.getSystemProfile());
				List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformation(userFilter);
				if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
					for(UserAccountSession userAccount : usersAccounts){
						DestinationNotification destNotification = new DestinationNotification();
						destNotification.setIdUserAccount(userAccount.getUserName());
						if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
							destNotification.setEmail(userAccount.getEmail());
							destinations.add(destNotification);
						}						
					}
				}				
			}
			break;			
		case EXECUTOR:
			//if(Validations.validateIsNullOrEmpty(institutionCode)){	//si hubiera el pk igual podria ejecutar esta casuistica
				List<UserAccountSession> userAccounts = userAccountControlService.getUsersInformation(null, null, null, null,userName,ExternalInterfaceType.NO.getCode());
				if(userAccounts != null && !userAccounts.isEmpty()){
					DestinationNotification destNotification = new DestinationNotification();
					UserAccountSession account = userAccounts.get(0);
					if(Validations.validateIsNotNullAndNotEmpty(account.getEmail())){
						destNotification.setEmail(account.getEmail());;
						destinations.add(destNotification);
					}					
				}				
			//}	
			break;
		case SUPERVISOR:
			if(userAccountSession.getIsOperator().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				UserFilterTO userFilter = new UserFilterTO();
				userFilter.setInstitutionType(userAccountSession.getInstitutionType());
				userFilter.setInstitutionSecurity(userAccountSession.getInstitutionCode());
				userFilter.setDepartment(userAccountSession.getDepartment());
				List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformationSupervisor(userFilter);
				for(UserAccountSession userAccount : usersAccounts){
					DestinationNotification destNotification = new DestinationNotification();
					destNotification.setIdUserAccount(userAccount.getUserName());
					if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
						destNotification.setEmail(userAccount.getEmail());
						destinations.add(destNotification);	
					}					
				}
			}
			break;
		}
		return destinations;
	}
	
	
	/**
	 * Creates the notification logger.
	 *
	 * @param userName the user name
	 * @param businessProc the business proc
	 * @param notificationType the notification type
	 * @return the notification logger
	 */
	private NotificationLogger createNotificationLogger(String userName,BusinessProcess businessProc,NotificationType notificationType){
		NotificationLogger notificationLogger = new NotificationLogger();
		notificationLogger.setBusinessProcess(businessProc);
		notificationLogger.setIndDelivered(BooleanType.NO.getCode());
		notificationLogger.setNotificationType(notificationType.getCode());
		switch (notificationType) {
		case PROCESS:
			notificationLogger.setDestUserName(userName);
			notificationLogger.setNotificationSubject(applicationConfiguration.getProperty("notification.batchprocess.registered"));
			break;
		case MESSAGE:
		case KILLSESSION:
			notificationLogger.setDestUserName(userName);
			notificationLogger.setNotificationSubject(applicationConfiguration.getProperty("notification.messages.registered"));
			break;
		case EMAIL:
			//get user´s email
			List<UserAccountSession> userAccounts = userAccountControlService.getUsersInformation(null, null, null, null,userName,ExternalInterfaceType.NO.getCode());
			if(userAccounts != null && !userAccounts.isEmpty()){
				UserAccountSession account = userAccounts.get(0);
				notificationLogger.setDestEmail(account.getEmail());
				notificationLogger.setDestUserName(userName);
				notificationLogger.setNotificationSubject(applicationConfiguration.getProperty("notification.messages.registered"));
			}
			
			break;
		}
		notificationLogger.setNotificationMessage(businessProc.getProcessName());
		notificationLogger.setRegistryDate(CommonsUtilities.currentDateTime());
		notificationLogger.setSourceUserName(userName);
		return notificationLogger;
	}
	
	
	
	
	/**
	 * Creates the browser window.
	 *
	 * @param notificationLogger the notification logger
	 * @return the browser window
	 */
	private BrowserWindow createBrowserWindow(NotificationLogger notificationLogger) {
		BrowserWindow browserWindow = new BrowserWindow();
		browserWindow.setNotificationType(notificationLogger.getNotificationType());
		browserWindow.setSubject(notificationLogger.getNotificationSubject());
		browserWindow.setMessage(notificationLogger.getNotificationMessage());
		NotificationType notificationType = NotificationType.get(notificationLogger.getNotificationType());
		switch (notificationType) {
		case PROCESS:
			browserWindow.setIdNotification(notificationLogger.getProcessLogger().getIdProcessLoggerPk());
			browserWindow.setUserChannel(notificationLogger.getDestUserName());
		case MESSAGE:
			browserWindow.setIdNotification(notificationLogger.getIdNotificationLoggerPk());
			browserWindow.setUserChannel(notificationLogger.getDestUserName());
		case KILLSESSION:
			browserWindow.setUserChannel(notificationLogger.getDestUserName());
			break;
		case EMAIL:
			browserWindow.setUserChannel(notificationLogger.getDestEmail());
			break;
		case REPORT:
			browserWindow.setUserChannel(notificationLogger.getDestUserName());
			break;
		}
		return browserWindow;
	}
	
	private BrowserWindow createBrowserWindowEmail(NotificationLogger notificationLogger) {
		BrowserWindow browserWindow = new BrowserWindow();
		browserWindow.setNotificationType(notificationLogger.getNotificationType());
		browserWindow.setSubject(notificationLogger.getNotificationSubject());
		browserWindow.setMessage(notificationLogger.getNotificationMessage());
		browserWindow.setUserChannel(notificationLogger.getDestEmail());
		
		return browserWindow;
	}
	
	/**
	 * Gets the notification loggers.
	 *
	 * @param userName the user name
	 * @param processNotification the process notification
	 * @param destinations the destinations
	 * @return the notification loggers
	 */
	private List<NotificationLogger> getNotificationLoggers(String userName,ProcessNotification processNotification,List<DestinationNotification> destinations){
		List<NotificationLogger> notificationLoggers = new ArrayList<NotificationLogger>();
		for(DestinationNotification destination : destinations) {
			NotificationLogger notification = new NotificationLogger();
			notification.setBusinessProcess(processNotification.getBusinessProcess());
			if(destination.getIdDestinationPk() != null){
			notification.setDestinationNotification(destination);
			}
			notification.setDestEmail(destination.getEmail());
			notification.setDestFullName(destination.getFullName());
			notification.setDestMobileNumber(destination.getMobileNumber());
			notification.setDestUserName(destination.getIdUserAccount());
			notification.setNotificationMessage(processNotification.getNotificationMessage());
			notification.setNotificationSubject(processNotification.getNotificationSubject());
			notification.setRegistryDate(CommonsUtilities.currentDateTime());
			notification.setNotificationType(processNotification.getNotificationType());
			notification.setIndDelivered(BooleanType.NO.getCode());
			notification.setSourceUserName(userName);
			notificationLoggers.add(notification);
		}
		return notificationLoggers;
	}
	
	private List<NotificationLogger> getNotificationLoggersNoConfig(String userName,BusinessProcess businessProcess ,String subject, String message,List<DestinationNotification> destinations){
		
		List<NotificationLogger> notificationLoggers = new ArrayList<NotificationLogger>();
		for(DestinationNotification destination : destinations) {
			NotificationLogger notification = new NotificationLogger();
			notification.setBusinessProcess(businessProcess);
			if(destination.getIdDestinationPk() != null){
			notification.setDestinationNotification(destination);
			}
			notification.setDestEmail(destination.getEmail());
			notification.setDestFullName(destination.getFullName());
			notification.setDestMobileNumber(destination.getMobileNumber());
			notification.setDestUserName(destination.getIdUserAccount());
			notification.setNotificationMessage(message);
			notification.setNotificationSubject(subject);
			notification.setRegistryDate(CommonsUtilities.currentDateTime());
			notification.setNotificationType(NotificationType.EMAIL.getCode());
			notification.setIndDelivered(BooleanType.NO.getCode());
			notification.setSourceUserName(userName);
			notificationLoggers.add(notification);
		}
		return notificationLoggers;
	}
	
	/**
	 * Sets the audit fields.
	 *
	 * @param notificationLogger the new audit fields
	 */
	private void setAuditFields(NotificationLogger notificationLogger) {
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if (object instanceof LoggerUser) {
			if (notificationLogger instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				Auditable auditable = (Auditable)notificationLogger;
				auditable.setAudit(loggerUser);
				Map<String,List<? extends Auditable>> listAudit = auditable.getListForAudit();
				if (listAudit != null) {
					for(List<? extends Auditable> auditableList :listAudit.values()) {
						loaderEntityService.setAudit(auditableList, loggerUser);
					}
				}
			}
		}
	}
	
	/**
	 * Format message.
	 *
	 * @param notification the notification
	 * @param parameters the parameters
	 */
	private void formatMessage(NotificationLogger notification,Object[] parameters){
		if(parameters != null && parameters.length >0){
			if(notification.getNotificationMessage().contains("%s")){								
				try {
					notification.setNotificationMessage(String.format(notification.getNotificationMessage(), parameters));
				} catch (MissingFormatArgumentException mfa) {
					log.info("Error controlado notificaciones ::::::::");
					notification.setNotificationMessage(notification.getNotificationMessage());
				}								
			}
		}
	}
	
	/**
	 * Update notification status.
	 *
	 * @param loggerUser the logger user
	 * @param userName the user name
	 * @param notifications the notifications
	 * @param status the status
	 * @param notificationType the notification type
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void updateNotificationStatus(LoggerUser loggerUser,
			String userName, List<Long> notifications, Integer status,
			Integer notificationType) throws ServiceException {
			StringBuilder builder = new StringBuilder(100);
			builder.append(" update NotificationLogger nl set nl.indDelivered = :status, ").
			append(" nl.lastModifyIp = :ip, ").
			append(" nl.lastModifyApp = :app, ").
			append(" nl.lastModifyUser = :userModificator, ").
			append(" nl.lastModifyDate = :date ").
			append(" where nl.destUserName = :user ").
			append(" and nl.notificationType = :notificationType ");
			if(notifications != null && !notifications.isEmpty()){
				builder.append(" and nl.idNotificationLoggerPk in (:notifications) ");
			} else {
				builder.append(" and nl.indDelivered = 0 ");
			}
			Query query = em.createQuery(builder.toString());
			query.setParameter("status", status);
			query.setParameter("ip", loggerUser.getIpAddress());
			query.setParameter("app", loggerUser.getIdPrivilegeOfSystem());
			query.setParameter("userModificator", loggerUser.getUserName());
			query.setParameter("date", CommonsUtilities.currentDateTime());
			query.setParameter("user", userName);
			query.setParameter("notificationType", notificationType);
			if(notifications != null && !notifications.isEmpty()){
				query.setParameter("notifications", notifications);
			}
			query.executeUpdate();
	}
	
	
	/**
	 * Method to run  
	 */
	public void runMultipleNotification(){
		
	}
	
	/**
	 * Envio de email con adjunto y notificaciones
	 * */
	@Asynchronous
	public void sendNotificationManual(LoggerUser loggerUser,String userName,BusinessProcess businessProc,List<UserAccountSession> users,String subject,String message,
			NotificationType notification,List<File> files){
		log.info("Start  method sendNotificationManual() ");
		if(businessProc!= null && StringUtils.isNotBlank(userName)){
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
			List<NotificationLogger> notificationLoggers = createNotifications(userName,businessProc,users,subject,message,notification);
			//check notifications to fire
			if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
				StringBuilder destinations = new StringBuilder(100);
				String contentLong;
				for(NotificationLogger notificationLogger : notificationLoggers) {
					setAuditFields(notificationLogger);
					contentLong=notificationLogger.getNotificationMessage();
					//si excede los 4000 caracteres entonces colocar S/M(Sin Mensaje)
					if(contentLong.length()>=4000){
						notificationLogger.setNotificationMessage("S/M");
					}
					em.persist(notificationLogger);
					if(!notification.equals(NotificationType.EMAIL)){
						if(notification.equals(NotificationType.MESSAGE)) {
							eventMessage.fire(createBrowserWindow(notificationLogger));
						} else if(notification.equals(NotificationType.REPORT)){
							eventReport.fire(createBrowserWindow(notificationLogger));
						} else if(notification.equals(NotificationType.PROCESS)){
							eventProcess.fire(createBrowserWindow(notificationLogger));
						}else if(notification.equals( NotificationType.EMAIL_ADJUNT)){
							sendEmailAdjunt(notificationLogger.getDestFullName(),subject,message,notificationLogger.getDestEmail(),files.get(0));
							return;
						}else if(notification.equals(NotificationType.EMAIL_ADJUNT_CONTENT_HTML)){
							sendEmailAdjuntHtmlContent(notificationLogger.getDestFullName(),subject,message,notificationLogger.getDestEmail(),files);
							return;
						}
					} else {
						//only for email send one email for all receptors
						destinations.append(notificationLogger.getDestEmail()+";");
					}
				}
				if(notification.equals(NotificationType.EMAIL)){
					NotificationLogger notificationLogger = new NotificationLogger();
					notificationLogger.setNotificationType(notification.getCode());
					notificationLogger.setNotificationMessage(message);
					notificationLogger.setNotificationSubject(subject);
					//remove end ;
					notificationLogger.setDestEmail(StringUtils.removeEnd(destinations.toString(),";"));
					eventEmail.fire(createBrowserWindow(notificationLogger));
				}
			}
		}
		
		log.info("End  method sendNotificationManual() ");
	}
	
	@Asynchronous
	public void sendNotificationManual(LoggerUser loggerUser,String userName,PaymentAgent paymentAgent,BusinessProcess businessProc,List<UserAccountSession> users,String subject,String message,
			NotificationType notification,List<File> files) throws Exception{
		log.info("Start  method sendNotificationManual() ");
		if(businessProc!= null && StringUtils.isNotBlank(userName)){
			log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
			List<NotificationLogger> notificationLoggers = createNotifications(userName,businessProc,users,subject,message,notification);
			//check notifications to fire
			if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
				StringBuilder destinations = new StringBuilder(100);
				String contentLong;
				for(NotificationLogger notificationLogger : notificationLoggers) {
					setAuditFields(notificationLogger);
					contentLong=notificationLogger.getNotificationMessage();
					//si excede los 4000 caracteres entonces colocar S/M(Sin Mensaje)
					if(contentLong.length()>=4000){
						notificationLogger.setNotificationMessage("S/M");
					}
					em.persist(notificationLogger);
					if(!notification.equals(NotificationType.EMAIL)){
						if(notification.equals(NotificationType.MESSAGE)) {
							eventMessage.fire(createBrowserWindow(notificationLogger));
						} else if(notification.equals(NotificationType.REPORT)){
							eventReport.fire(createBrowserWindow(notificationLogger));
						} else if(notification.equals(NotificationType.PROCESS)){
							eventProcess.fire(createBrowserWindow(notificationLogger));
						}else if(notification.equals( NotificationType.EMAIL_ADJUNT)){
							sendEmailAdjunt(notificationLogger.getDestFullName(),subject,message,notificationLogger.getDestEmail(),files.get(0));
							return;
						}else if(notification.equals(NotificationType.EMAIL_ADJUNT_CONTENT_HTML)){
							//sendEmailAdjuntHtmlContent(notificationLogger.getDestFullName(), paymentAgent,subject,message,notificationLogger.getDestEmail(),files);
							return;
						}
					} else {
						//only for email send one email for all receptors
						destinations.append(notificationLogger.getDestEmail()+";");
					}
				}
				if(notification.equals(NotificationType.EMAIL)){
					NotificationLogger notificationLogger = new NotificationLogger();
					notificationLogger.setNotificationType(notification.getCode());
					notificationLogger.setNotificationMessage(message);
					notificationLogger.setNotificationSubject(subject);
					//remove end ;
					notificationLogger.setDestEmail(StringUtils.removeEnd(destinations.toString(),";"));
					eventEmail.fire(createBrowserWindow(notificationLogger));
				}
			}
		}
		
		log.info("End  method sendNotificationManual() ");
	}
	private void sendEmailAdjunt(String detFullName,String subject,String message,String email,File file) {
		if(!file.exists())
		try {
			throw new FileNotFoundException("Error al leer los archivos");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		adjuntServiceBean.startFileSending(detFullName, subject, message, email, file);
	}
	
	private void sendEmailAdjuntHtmlContent(String detFullName,String subject,String message,String email,List<File> files) {
		adjuntServiceBean.startFileSendingHtmlContent(detFullName, subject, message, email, files);
	}
	/**
	 * Envio de mensaje firmado y cifrado
	 * @throws Exception 
	 * 
	 * */
//	private void sendEmailAdjuntHtmlContent(String detFullName,PaymentAgent paymentAgent,String subject,String message,String email,List<File> files) throws Exception {
//		
//		boolean signEmail = false;
//		boolean encryptEmail = false;
//		
//		SmimeKey smimeKey = null;
//		InputStream inputStreamCerticate = null;
//		
//		/**Firmar correo electronico**/
//		if(paymentAgent.getSign()!=null&&paymentAgent.getSign().equals(BooleanType.YES.getCode())){
//			
//			/***/
//			if (Security.getProvider("BC") == null) {
//	            Security.addProvider(new BouncyCastleProvider());
//	        }
//			
//			DigitalSignature digitalSignature = searchDigitalSignatureSing();
//			String passwordP12File = null;
//			passwordP12File=AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY.getValue(), digitalSignature.getPasswordSignature());
//			//System.out.println("CONTRASEÑA: "+passwordP12File);
//			/*Para JAVA 8*/
//			//fixKeyLength();
//			//System.out.println("**************************************************************");
//			ByteArrayInputStream arrayInputStream=new ByteArrayInputStream(digitalSignature.getSignature());
//			/**set de instancia*/
//			//Security.addProvider(new BouncyCastleProvider());
//	        //classLoader = NotificationServiceBean.class.getClassLoader();
//			try {
//				smimeKey = getSmimeKeyForSender(arrayInputStream,passwordP12File);
//			} catch (KeyStoreException e) {
//				e.printStackTrace();
//			} catch (NoSuchProviderException e) {
//				e.printStackTrace();
//			}
//			signEmail = true;
//		}
//		/**Cifrar correo**/
//		if(paymentAgent.getEncryp()!=null&&paymentAgent.getEncryp().equals(BooleanType.YES.getCode())){
//			byte[] signature = paymentAgent.getCertificate();
//			inputStreamCerticate = new ByteArrayInputStream(signature);
//			if(inputStreamCerticate!=null)
//				encryptEmail = true;
//		}
//		try {
//			adjuntServiceBean.startFileSendingHtmlContent(smimeKey,inputStreamCerticate, subject, message, email, files, signEmail, encryptEmail);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	/**
	 * Metodo que carga la llave privada ara realizar la firma
	 * 
	 * @param from
	 * @return
	 * @throws NoSuchProviderException 
	 * @throws KeyStoreException 
	 */
//	private SmimeKey getSmimeKeyForSender(ByteArrayInputStream arrayInputStream,String storePass) throws KeyStoreException, NoSuchProviderException {
//		SmimeKey smimeKey = null;
//		SmimeKeyStore smimeKeyStore = new SmimeKeyStore(arrayInputStream, storePass.toCharArray());
//		Set<String> list =  smimeKeyStore.getPrivateKeyAliases();
//		smimeKey = smimeKeyStore.getPrivateKey(list.iterator().next(), storePass.toCharArray()); // keyPass
//		return smimeKey;
//	}
//	
//	public static void main(String cor[]) throws Exception {
//	        if (Security.getProvider("BC") == null) {
//	            Security.addProvider(new BouncyCastleProvider());
//	        }
//	        KeyStore keyStore = KeyStore.getInstance("PKCS12", "BC");
//	        fixKeyLength();
//	        keyStore.load(new FileInputStream("D:\\EXCEL_ISSUES\\680\\certificados\\mailsan.p12"), "$M@ils@n3dv18$".toCharArray());
//	        
//	        Enumeration e = keyStore.aliases();
//
//	        System.out.println("LISTA DE ALIAS: "+Collections.list(e));
//	        
//	        /*Key key = keyStore.getKey("1298f30c1239321d87b49560ff5433c4a722632b","3.141592654".toCharArray());
//	        System.out.println("KEY: "+key);
//	        
//	        Certificate certificate = keyStore.getCertificate("1298f30c1239321d87b49560ff5433c4a722632b");
//	        System.out.println("CERT: "+certificate);*/
//	    }
//	 public static void fixKeyLength() {
//	        String errorString = "Failed manually overriding key-length permissions.";
//	        int newMaxKeyLength;
//	        try {
//	            if ((newMaxKeyLength = Cipher.getMaxAllowedKeyLength("AES")) < 256) {
//	                Class c = Class.forName("javax.crypto.CryptoAllPermissionCollection");
//	                java.lang.reflect.Constructor con = c.getDeclaredConstructor();
//	                con.setAccessible(true);
//	                Object allPermissionCollection = con.newInstance();
//	                java.lang.reflect.Field f = c.getDeclaredField("all_allowed");
//	                f.setAccessible(true);
//	                f.setBoolean(allPermissionCollection, true);
//
//	                c = Class.forName("javax.crypto.CryptoPermissions");
//	                con = c.getDeclaredConstructor();
//	                con.setAccessible(true);
//	                Object allPermissions = con.newInstance();
//	                f = c.getDeclaredField("perms");
//	                f.setAccessible(true);
//	                ((Map) f.get(allPermissions)).put("*", allPermissionCollection);
//
//	                c = Class.forName("javax.crypto.JceSecurityManager");
//	                f = c.getDeclaredField("defaultPolicy");
//	                f.setAccessible(true);
//	                java.lang.reflect.Field mf = java.lang.reflect.Field.class.getDeclaredField("modifiers");
//	                mf.setAccessible(true);
//	                mf.setInt(f, f.getModifiers() & ~java.lang.reflect.Modifier.FINAL);
//	                f.set(null, allPermissions);
//
//	                newMaxKeyLength = Cipher.getMaxAllowedKeyLength("AES");
//	            }
//	        } catch (Exception e) {
//	            throw new RuntimeException(errorString, e);
//	        }
//	        if (newMaxKeyLength < 256) {
//	            throw new RuntimeException(errorString); // hack failed
//	        }
//	    }
	 
	 /**
	  * Buscamos el certificado para firmar el correo electronico
	  * */
	 private DigitalSignature searchDigitalSignatureSing(){
		 StringBuilder builder = new StringBuilder();
		 builder.append(" select ds from DigitalSignature ds ");
		 builder.append(" where 1 = 1 ");
		 builder.append(" and ds.typeSignature = "+TYPE_SIGNATURE_SING);
		 builder.append(" and ds.activeSignature = "+BooleanType.YES.getCode());
		 
		 Query query = loaderEntityService.getEm().createQuery(builder.toString());
		 
		 DigitalSignature digitalSignature = (DigitalSignature) query.getSingleResult();
		 
		 return digitalSignature;
	 }
	 
		/**
		 * Send message notification.
		 * Issue 1094 Implementacion de un mensaje de aviso en la bandeja de mensajes del CSDCORE
		 * @param loggerUser the logger user
		 * @param userName the user name
		 * @param businessProc the business proc
		 * @param intitutionCode the intitution code
		 * @param parameters the parameters
		 */
		@Asynchronous
		public void sendNotification(LoggerUser loggerUser,String userName,BusinessProcess businessProc,Long instParticipant,String instIssuer,Object[] parameters, UserAccountSession userAccountSession){
			if (businessProc != null && businessProc.getIdBusinessProcessPk() !=null) {
				log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
				List<NotificationLogger> notificationLoggers = createDestinations(userName,businessProc, null,instParticipant,instIssuer,null,userAccountSession);
				//check notifications to fire
				if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
					for(NotificationLogger notificationLogger : notificationLoggers) {
						formatMessage(notificationLogger,parameters);
						setAuditFields(notificationLogger);
						em.persist(notificationLogger);
						eventMessage.fire(createBrowserWindow(notificationLogger));
					}
				}
			}
		}
		
		/*
		@Asynchronous
		public void sendNotification(LoggerUser loggerUser,String userName,BusinessProcess businessProc, Long idHolderPk,Long instParticipant,String instIssuer,Object[] parameters, UserAccountSession userAccountSession){
			if (businessProc != null && businessProc.getIdBusinessProcessPk() !=null) {
				log.info("Proceso de Negocio : " + businessProc.getIdBusinessProcessPk());	
				List<NotificationLogger> notificationLoggers = createDestinations(userName,businessProc, idHolderPk,instParticipant,instIssuer,null,userAccountSession);
				//check notifications to fire
				if(notificationLoggers != null && !notificationLoggers.isEmpty()) {
					for(NotificationLogger notificationLogger : notificationLoggers) {
						formatMessage(notificationLogger,parameters);
						setAuditFields(notificationLogger);
						em.persist(notificationLogger);
						eventMessage.fire(createBrowserWindow(notificationLogger));
					}
				}
			}
		}*/
		
		/**
		 * Creates the destinations.
		 * Issue 1094 Implementacion de un mensaje de aviso en la bandeja de mensajes del CSDCORE
		 * @param userName the user name
		 * @param businessProc the business proc
		 * @param intitutionCode the intitution code
		 * @param notification the notification
		 * @return the list
		 */
		private List<NotificationLogger> createDestinations(String userName,BusinessProcess businessProc, Long idHolderPk,Long instParticipant,String instIssuer,NotificationType notification,UserAccountSession userAccountSession) {
			List<NotificationLogger> notificationLoggers = null;
			if(businessProc != null) {
				businessProc = loaderEntityService.loadEntity(businessProc,businessProc.getIdBusinessProcessPk());
				SendNotificationType sendNotification = SendNotificationType.get(businessProc.getIndNotices());
				if (sendNotification.equals(SendNotificationType.ON)) {
					if (businessProc.getProcessNotifications() != null 
							&& !businessProc.getProcessNotifications().isEmpty()) {
						notificationLoggers = new ArrayList<NotificationLogger>();
						for(ProcessNotification processNotification : businessProc.getProcessNotifications()){
							Object institutionCode = instParticipant;
							//only read active
							if(processNotification.getIndStatus().equals(BooleanType.YES.getCode())){
								//Verificando el tipo de institucion
								if(processNotification.getInstitutionType() != null){
									if(processNotification.getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode())
											|| processNotification.getInstitutionType().equals(InstitutionType.ISSUER.getCode())){
										institutionCode = instIssuer;
									}
								}
								NotificationType notificationType = NotificationType.get(processNotification.getNotificationType());
								List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
								//get destinations configurations(manual,dynamic,only executor)
								switch (notificationType) {
								case PROCESS:
								case MESSAGE:
								case KILLSESSION:
									destinations.addAll(getDestinationNotificationNew(userName,
											processNotification, institutionCode, userAccountSession));	
									break;
								case EMAIL:
									destinations.addAll(getDestinationEmailNotificationNew(userName,
											processNotification, institutionCode, userAccountSession, idHolderPk));	
									break;
								}
								//get notifications data for registers in logs db
								notificationLoggers.addAll(getNotificationLoggers(userName,processNotification, destinations));
							}
						}
						
						
					} else {
						//if is enabled notifications and there is not configurations send default message from businessprocess
						notificationLoggers = new ArrayList<NotificationLogger>();
						if(notification == null){
							//default notification without configuration is message
							notificationLoggers.add(createNotificationLogger(userName, businessProc, NotificationType.MESSAGE));
						} else {
							//else is parameter notification
							notificationLoggers.add(createNotificationLogger(userName, businessProc, notification));
						}
						
					}
				}
			} else {
				log.error("Error BusinessProcess is null,not sending notification");
			}
			return notificationLoggers;
		}
		
		/**
		 * Gets the destination notification.
		 * Issue 1094 Implementacion de un mensaje de aviso en la bandeja de mensajes del CSDCORE
		 * @param userName the user name
		 * @param processNotification the process notification
		 * @param institutionCode the institution code
		 * @return the destination notification
		 */
		private List<DestinationNotification> getDestinationNotificationNew(String userName,ProcessNotification processNotification,
				Object institutionCode, UserAccountSession userAccountSession) {
			NotificationDestinationType destination = NotificationDestinationType.get(processNotification.getDestinationType());
			List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
			switch (destination) {
			case MANUAL:
				if(userAccountSession != null) {
					for(DestinationNotification objDestinationNotification : processNotification.getDestinationNotifications()) {
						if(Validations.validateIsNotNullAndNotEmpty(userAccountSession.getInstitutionType()) && 
								userAccountSession.getInstitutionType().equals(objDestinationNotification.getInstitutionType())) {
							destinations.add(objDestinationNotification);
						}
					}
				} else {
					destinations = processNotification.getDestinationNotifications();
				}						
				break;
			case DYNAMIC:
				if(processNotification.getInstitutionType() != null){
					//Verificando si la institucion es 
					if(!processNotification.getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode())){
						if(Validations.validateIsNullOrEmpty(institutionCode)){
							break;	
						}
					}
					if(userAccountSession != null && 
							!userAccountSession.getInstitutionCode().equals(processNotification.getInstitutionType())) {
						break;
					}
					UserFilterTO userFilter = new UserFilterTO();
					userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
					userFilter.setInstitutionType(processNotification.getInstitutionType());
					userFilter.setEntityCode(institutionCode);
					userFilter.setResponsibility(processNotification.getUserResponsability());
					userFilter.setProfileSystem(processNotification.getSystemProfile());
					List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformation(userFilter);					
					if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
						for(UserAccountSession userAccount : usersAccounts){
							// Verificando duplicados
							if (!containsUserAccount(destinations,userAccount)) {
								DestinationNotification destNotification = new DestinationNotification();
								destNotification.setIdUserAccount(userAccount.getUserName());
								if (Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())) {
									destNotification.setEmail(userAccount.getEmail());
									destinations.add(destNotification);
								}
							}
						}
					}				
				}			
				break;			
			case EXECUTOR:
				if(Validations.validateIsNullOrEmpty(institutionCode)){
					DestinationNotification destNotification = new DestinationNotification();
					destNotification.setIdUserAccount(userName);
					destinations.add(destNotification);
				}			
				break;			
			case SUPERVISOR:
				UserFilterTO userFilter = new UserFilterTO();
				userFilter.setInstitutionType(userAccountSession.getInstitutionType());
				userFilter.setInstitutionSecurity(userAccountSession.getInstitutionCode());
				userFilter.setDepartment(userAccountSession.getDepartment());
				List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformationSupervisor(userFilter);
				for(UserAccountSession userAccount : usersAccounts){
					DestinationNotification destinationNotification = new DestinationNotification();
					destinationNotification.setIdUserAccount(userAccount.getUserName());
					destinationNotification.setEmail(userAccount.getEmail());
					destinations.add(destinationNotification);	
				}
				break;
			}
			return destinations;
		}
		
		/**
		 * Gets the destination email notification.
		 * Issue 1094 Implementacion de un mensaje de aviso en la bandeja de mensajes del CSDCORE
		 * @param userName the user name
		 * @param processNotification the process notification
		 * @param institutionCode the institution code
		 * @return the destination email notification
		 */
		private List<DestinationNotification> getDestinationEmailNotificationNew(String userName,ProcessNotification processNotification,
				Object institutionCode, UserAccountSession userAccountSession, Long idHolderPk) {
			NotificationDestinationType destination = NotificationDestinationType.get(processNotification.getDestinationType());
			List<DestinationNotification> destinations = new ArrayList<DestinationNotification>();
			switch (destination) {
			case MANUAL:
				if(userAccountSession != null) {
					for(DestinationNotification objDestinationNotification : processNotification.getDestinationNotifications()) {
						if(Validations.validateIsNotNullAndNotEmpty(userAccountSession.getInstitutionType()) && 
								userAccountSession.getInstitutionType().equals(objDestinationNotification.getInstitutionType())) {
							destinations.add(objDestinationNotification);
						}
					}
				} else {
					destinations = processNotification.getDestinationNotifications();
				}			
				break;
			case DYNAMIC:
				if(processNotification.getInstitutionType() != null){				
					//Verificando si la institucion es 
					if(!processNotification.getInstitutionType().equals(InstitutionType.DEPOSITARY.getCode())){
						if(Validations.validateIsNullOrEmpty(institutionCode)){
							break;
						}
					}
					if(userAccountSession != null && 
							!userAccountSession.getInstitutionCode().equals(processNotification.getInstitutionType())) {
						break;
					}	
					UserFilterTO userFilter = new UserFilterTO();
					userFilter.setState(UserAccountStateType.CONFIRMED.getCode());
					userFilter.setInstitutionType(processNotification.getInstitutionType());
					userFilter.setEntityCode(institutionCode);
					userFilter.setResponsibility(processNotification.getUserResponsability());
					userFilter.setProfileSystem(processNotification.getSystemProfile());
					List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformation(userFilter);
					if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
						for(UserAccountSession userAccount : usersAccounts){
							// Verificando duplicados
							if (!containsUserAccount(destinations,userAccount)) {
								DestinationNotification destNotification = new DestinationNotification();
								destNotification.setIdUserAccount(userAccount.getUserName());
								if (Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())) {
									destNotification.setEmail(userAccount.getEmail());
									destinations.add(destNotification);
								}
							}
						}
					}				
				}
				break;			
			case EXECUTOR:
				if(Validations.validateIsNullOrEmpty(institutionCode)){
					List<UserAccountSession> userAccounts = userAccountControlService.getUsersInformation(null, null, null, null,userName,ExternalInterfaceType.NO.getCode());
					if(userAccounts != null && !userAccounts.isEmpty()){
						DestinationNotification destNotification = new DestinationNotification();
						UserAccountSession account = userAccounts.get(0);
						if(Validations.validateIsNotNullAndNotEmpty(account.getEmail())){
							destNotification.setEmail(account.getEmail());;
							destinations.add(destNotification);
						}					
					}				
				}	
				break;
			case SUPERVISOR:
				if(userAccountSession.getIsOperator().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					UserFilterTO userFilter = new UserFilterTO();
					userFilter.setInstitutionType(userAccountSession.getInstitutionType());
					userFilter.setInstitutionSecurity(userAccountSession.getInstitutionCode());
					userFilter.setDepartment(userAccountSession.getDepartment());
					List<UserAccountSession> usersAccounts = userAccountControlService.getUsersInformationSupervisor(userFilter);
					for(UserAccountSession userAccount : usersAccounts){
						DestinationNotification destNotification = new DestinationNotification();
						destNotification.setIdUserAccount(userAccount.getUserName());
						if(Validations.validateIsNotNullAndNotEmpty(userAccount.getEmail())){
							destNotification.setEmail(userAccount.getEmail());
							destinations.add(destNotification);	
						}					
					}
				}
				break;
				
			case HOLDER_ACCOUNT:
				Holder holder = new Holder();
				try {
					holder = findHolder(idHolderPk);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				DestinationNotification destNotification = new DestinationNotification();
				destNotification.setIdUserAccount(holder.getEmail());
				destNotification.setEmail(holder.getEmail());
				destinations.add(destNotification);	
				
				break;
			}
			return destinations;
		}
		
		public Holder findHolder(Long idHolderPk) throws ServiceException {
			StringBuilder builder = new StringBuilder();
			builder.append(" select h from Holder h where h.idHolderPk = :idHolderPk ");
			Query query = em.createQuery(builder.toString());
			query.setParameter("idHolderPk", idHolderPk);
			
			return (Holder) query.getSingleResult();
		}
		
		/**
		 * Verifica si un userAccount ya se encuentra en el listado de destinatarios
		 * Issue 1094 Implementacion de un mensaje de aviso en la bandeja de mensajes del CSDCORE
		 * @param destinations
		 * @param userAccount
		 * @return
		 */
		public boolean containsUserAccount(List<DestinationNotification> destinations,
				UserAccountSession userAccount) {
			for (DestinationNotification destination : destinations) {
				if (destination.getIdUserAccount().trim().toUpperCase().equals(userAccount.getUserName().trim().toUpperCase())) {
					return true;
				}
			}
			return false;
		}
}
