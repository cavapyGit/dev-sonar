package com.pradera.core.framework.usersaccount.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pradera.commons.security.model.type.UserAccountResponsabilityType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.InstitutionType;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserAccountControlServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/07/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class UserAccountControlServiceBean {
	
	/** The em. */
	@Inject
	@DepositaryDataBase
	EntityManager em;
	
	@Inject
	ClientRestService clientRestService;

	public UserAccountControlServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the users group.
	 *
	 * @param institution the institution
	 * @param institutionCode the institution code
	 * @param responsability the responsability
	 * @return the users group
	 */
	public List<String> getUsersGroup(InstitutionType institution,Object institutionCode
			,UserAccountResponsabilityType responsability) {
		List<String> userAccounts = null;
		userAccounts = clientRestService.getUsersOrEmails(null, true, institution, institutionCode, responsability);
		return userAccounts;
	}
	
	/**
	 * Gets the users mails.
	 *
	 * @param institution the institution
	 * @param institutionCode the institution code
	 * @param responsability the responsability
	 * @return the users mails
	 */
	public List<String> getUsersMails(InstitutionType institution,Object institutionCode
			,UserAccountResponsabilityType responsability) {
		List<String> userEmails = null;
		userEmails = clientRestService.getUsersOrEmails(null, false, institution, institutionCode, responsability);
		return userEmails;
	}
	
	/**
	 * Gets the users info.
	 *
	 * @param state the state
	 * @param institution the institution
	 * @param institutionCode the institution code
	 * @param responsability the responsability
	 * @param userName the user name
	 * @param indExtInterface 
	 * @return the users info
	 */
	public List<UserAccountSession> getUsersInformation(Integer state, InstitutionType institution,Object institutionCode
			,UserAccountResponsabilityType responsability,String userName, Integer indExtInterface){
		List<UserAccountSession> usersAccounts= null;
		usersAccounts = clientRestService.getUsersInformation(state, institution, institutionCode, responsability,userName,indExtInterface);
		return usersAccounts;
	}

	/**
	 * Gets the users information.
	 *
	 * @param userFilter the user filter
	 * @return the users information
	 */
	public List<UserAccountSession> getUsersInformation(UserFilterTO userFilter){
		return  clientRestService.getUsersInformation(userFilter);
	}
	
	/**
	 * Gets the users information supervisors.
	 *
	 * @param userFilter the user filter
	 * @return the users information
	 */
	public List<UserAccountSession> getUsersInformationSupervisor(UserFilterTO userFilter){
		return  clientRestService.getUsersInformationSupervisor(userFilter);
	}
	
	public List<UserAccountSession> getUsersInformationOperator(UserFilterTO userFilter){
		return  clientRestService.getUsersInformationOperator(userFilter);
	}
	
	public List<UserAccountSession> getUsersInformationSupervisorAndOperator(UserFilterTO userFilter){
		return  clientRestService.getUsersInformationSupervisorAndOperator(userFilter);
	}

	public List<UserAccountSession> getUsersInformationHolderAccount(UserFilterTO userFilter){
		return  clientRestService.getUsersInformationHolderAccount(userFilter);
	}
}
