package com.pradera.core.framework.audit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.audit.UserTrackProcess;
import com.pradera.commons.audit.service.UserAuditServiceBean;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;

@ProcessAuditLogger
@Interceptor
public class AuditProcessLoggerInterceptor {
	
	@EJB
	UserAuditServiceBean userAuditService;
	
	@Resource(lookup="java:comp/TransactionSynchronizationRegistry")
	TransactionSynchronizationRegistry registry;
	
	public AuditProcessLoggerInterceptor() {
		// TODO Auto-generated constructor stub
	}

	@AroundInvoke
	public Object aroundProcessInvoke(InvocationContext ic) throws Exception {
		String businessMethod = ic.getMethod().getName();
		String className = ic.getTarget().getClass().getName();
		LoggerUser loggerUser = (LoggerUser)registry.getResource(RegistryContextHolderType.LOGGER_USER);
		Long idSessionUser = loggerUser.getIdUserSessionPk();
		ProcessAuditLogger processAuditLogger = ic.getMethod().getAnnotation(ProcessAuditLogger.class);
		//TODO check if is necessary control security about participants,issues and balances
		try {
		return ic.proceed();
		}finally{
			UserTrackProcess usertTrackProcess = new UserTrackProcess();
			usertTrackProcess.setBusinessMethod(businessMethod);
			usertTrackProcess.setClassName(className);
			usertTrackProcess.setIdBusinessProcess(processAuditLogger.process().getCode());
			usertTrackProcess.setIdUserSessionPk(idSessionUser);
			if(loggerUser != null && loggerUser.getIdPrivilegeOfSystem()!=null){
				usertTrackProcess.setIdPrivilegeSystem(loggerUser.getIdPrivilegeOfSystem());
				usertTrackProcess.setIndError(BooleanType.NO.getCode());
			} else {
				usertTrackProcess.setIdPrivilegeSystem(BooleanType.NO.getCode());
				usertTrackProcess.setIndError(BooleanType.YES.getCode());
			}
			//register tracking
			userAuditService.registerUserTrackProcess(usertTrackProcess);
		}
	}

}
