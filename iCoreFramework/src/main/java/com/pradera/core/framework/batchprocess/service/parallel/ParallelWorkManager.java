package com.pradera.core.framework.batchprocess.service.parallel;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.core.framework.batchprocess.BatchProcessLiteral;
import com.pradera.core.framework.batchprocess.ParallelProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.batchprocess.service.WorkManagerService;
import com.pradera.core.framework.batchprocess.to.BatchRegisterTO;

@ApplicationScoped
@ParallelProcess
public class ParallelWorkManager implements WorkManagerService {
	
	@Inject
	@Any
	Instance<JobExecution> jobExecution;

	public ParallelWorkManager() {
		// TODO Auto-generated constructor stub
	}

	public void initBatch(BatchRegisterTO batch) {
		// TODO Auto-generated method stub

	}
	
	private JobExecution getJob(String name) {
		return this.jobExecution.select(new BatchProcessLiteral(name)).get();
	}

}
