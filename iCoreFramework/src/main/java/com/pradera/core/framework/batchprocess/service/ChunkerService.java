package com.pradera.core.framework.batchprocess.service;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.pradera.core.framework.batchprocess.SequentialProcess;
import com.pradera.core.framework.batchprocess.to.BatchRegisterTO;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChunkerService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-jun-2015
 */
@Stateless
public class ChunkerService implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3096701944532197265L;
	
	/** The work manager sequential. */
	@Inject
	@SequentialProcess
	WorkManagerService workManagerSequential;
	
	/**
	 * Execute batch.
	 *
	 * @param batchRegisterTO the batch register to
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void executeBatch(BatchRegisterTO batchRegisterTO){
		workManagerSequential.initBatch(batchRegisterTO);
	}

}
