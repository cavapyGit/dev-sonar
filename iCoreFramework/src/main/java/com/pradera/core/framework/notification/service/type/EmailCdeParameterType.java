package com.pradera.core.framework.notification.service.type;

public enum EmailCdeParameterType {
	HOST("mail.smtp.host"),
	START_TLS("mail.smtp.starttls.enable"),
	PORT("mail.smtp.port"),
	AUTH("mail.smtp.auth"),
	USER("mail.smtp.user"),
	PASS("mail.smtp.pass");
		
	private String value;
	
	private EmailCdeParameterType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
