package com.pradera.core.framework.extension.transactiontimeout;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * TransactionTimeoutDepositary using for setting timeout of transactions 
 * by default unit time is in minutes
 * @author PraderaTechnologies.
 * @version 1.0 , 18/06/2014
 */
@Inherited
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
@Documented
public @interface TransactionTimeoutDepositary {
	
	/**
	 * Value.
	 *
	 * @return the long
	 */
	long value();
	
	/**
	 * Unit.
	 *
	 * @return the time unit
	 */
	TimeUnit unit() default TimeUnit.MINUTES;
}
