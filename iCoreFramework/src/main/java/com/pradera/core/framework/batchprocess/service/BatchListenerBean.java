package com.pradera.core.framework.batchprocess.service;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.pradera.core.framework.batchprocess.ParallelProcess;
import com.pradera.core.framework.batchprocess.SequentialProcess;
import com.pradera.core.framework.batchprocess.to.BatchRegisterTO;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class BatchListenerBean.
 * Listener for trigger batch process
 * @author PraderaTechnologies.
 * @version 1.0 , 18/12/2012
 */
@ApplicationScoped
public class BatchListenerBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6857913869409440756L;

	@Inject
	@SequentialProcess
	WorkManagerService workManagerSequential;
	
	@Inject
	@ParallelProcess
	WorkManagerService workManagerParallel;

	/**
	 * Instantiates a new batch listener bean.
	 */
	public BatchListenerBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * New process sequential.
	 *
	 * @param batchRegisterTO the batch register
	 */
	public void newProcessSequential(@Observes @SequentialProcess BatchRegisterTO batchRegisterTO) {
		workManagerSequential.initBatch(batchRegisterTO);
	}
	
	/**
	 * New process parallel.
	 *
	 * @param batchRegisterTO the batch register
	 */
	public void newProcessParallel(@Observes @ParallelProcess BatchRegisterTO batchRegisterTO) {
		workManagerParallel.initBatch(batchRegisterTO);
	}

}
