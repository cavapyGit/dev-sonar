package com.pradera.core.framework.monitoring;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface PerformanceAuditor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/10/2014
 */
@InterceptorBinding
@Inherited
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
@Documented
public @interface PerformanceAuditor {
 
 /**
  * Value.
  *
  * @return the monitoring type
  */
 @Nonbinding 
 MonitoringType value() default MonitoringType.SERVICE;
}
