package com.pradera.core.framework.batchprocess.facade;

import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.batchprocess.to.BatchRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.type.ProcessType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BatchProcessServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/03/2013
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class BatchProcessServiceFacade {
	
	/** The batch service. */
	@EJB
	BatchServiceBean batchService;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * Instantiates a new batch process service facade.
	 */
	public BatchProcessServiceFacade() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Register batch.
	 *
	 * @param process the process
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void registerBatch(String userName,BusinessProcess process, Map<String,Object> parameters) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getIdPrivilegeOfSystem() == null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
        //How is required_new transaction dont propagate transactionRegistry, so we propagate with threadLocal
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        BatchRegisterTO batchRegister = batchService.registerBatch(userName,process,parameters);
        if (batchRegister.getProcess().getProcessType().equals(ProcessType.BATCH.getCode())) {
        	batchService.startBatch(batchRegister);
        }
	}
	
	/**
	 * Cancel batch.
	 */
	public void cancelBatch() {
		
	}
	
	/**
	 * Restart batch.
	 */
	public void restartBatch() {
		
	}
	
	public boolean isExecutingBatchProcess(Long idBusinessProcess, Long idProcessLogger) {
		return batchService.isExecutingBatchProcess(idBusinessProcess, idProcessLogger);
	}

	public void updateProcessErrorDetail(Long idProcessLogger, String errorDetail) {
		batchService.updateProcessErrorDetail(idProcessLogger, errorDetail);
	}
}
