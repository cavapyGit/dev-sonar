package com.pradera.core.framework.monitoring;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class Invocation.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/10/2014
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Invocation implements Comparable<Invocation> {
	
	/** The method name. */
	private String methodName;
	
	/** The invocation performance. */
	private Long invocationPerformance;
	
	/** The parameters method. */
	private String parametersMethod;
	
	/**
	 * Instantiates a new invocation.
	 *
	 * @param methodName the method name
	 * @param invocationPerformance the invocation performance
	 */
	public Invocation(String methodName, long invocationPerformance) {
        this.methodName = methodName;
        this.invocationPerformance = invocationPerformance;
    }
	
	/**
	 * Instantiates a new invocation.
	 *
	 * @param methodName the method name
	 * @param invocationPerformance the invocation performance
	 * @param parametersMethod the parameters method
	 */
	public Invocation(String methodName, long invocationPerformance, String parametersMethod) {
        this.methodName = methodName;
        this.invocationPerformance = invocationPerformance;
        this.parametersMethod=parametersMethod;
    }
	
	 /**
 	 * Instantiates a new invocation.
 	 */
 	public Invocation() { /* JAXB...*/}
	 
	 /**
 	 * Checks if is slower than.
 	 *
 	 * @param invocation the invocation
 	 * @return true, if is slower than
 	 */
 	public boolean isSlowerThan(Invocation invocation){
	        return this.compareTo(invocation) > 0;
	}
	 
	 /* (non-Javadoc)
 	 * @see java.lang.Comparable#compareTo(java.lang.Object)
 	 */
 	@Override
	    public int compareTo(Invocation anotherInvocation){
	        return this.invocationPerformance.compareTo(anotherInvocation.invocationPerformance);
	    }

	 /* (non-Javadoc)
 	 * @see java.lang.Object#hashCode()
 	 */
 	@Override
	    public int hashCode() {
	        return methodName != null ? methodName.hashCode() : 0;
	    }

	    /* (non-Javadoc)
    	 * @see java.lang.Object#equals(java.lang.Object)
    	 */
    	@Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;

	        Invocation that = (Invocation) o;

	        if (methodName != null ? !methodName.equals(that.methodName) : that.methodName != null) return false;

	        return true;
	    }

	    /* (non-Javadoc)
    	 * @see java.lang.Object#toString()
    	 */
    	@Override
	    public String toString() {
	        return "Invocation{" +
	                "methodName='" + methodName + '\'' +
	                ", invocationPerformance=" + invocationPerformance +
	                ", parametersMethod=" +parametersMethod+
	                '}';
	    }

	/**
	 * Gets the method name.
	 *
	 * @return the method name
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * Sets the method name.
	 *
	 * @param methodName the new method name
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * Gets the invocation performance.
	 *
	 * @return the invocation performance
	 */
	public Long getInvocationPerformance() {
		return invocationPerformance;
	}

	/**
	 * Sets the invocation performance.
	 *
	 * @param invocationPerformance the new invocation performance
	 */
	public void setInvocationPerformance(Long invocationPerformance) {
		this.invocationPerformance = invocationPerformance;
	}

	/**
	 * Gets the parameters method.
	 *
	 * @return the parameters method
	 */
	public String getParametersMethod() {
		return parametersMethod;
	}

	/**
	 * Sets the parameters method.
	 *
	 * @param parametersMethod the new parameters method
	 */
	public void setParametersMethod(String parametersMethod) {
		this.parametersMethod = parametersMethod;
	}

}
