package com.pradera.core.framework.monitoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.monitoring.facade.MonitoringResource;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.ReportLogger;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class PerformanceAuditorInterceptor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/10/2014
 */
@PerformanceAuditor
@Interceptor
public class PerformanceAuditorInterceptor implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2160684314428554970L;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The monitoring service. */
	@EJB
	MonitoringResource monitoringService;
	
	@EJB
	NotificationServiceFacade notificationService;
	
	/** The max seconds execution report. */
	@Inject @Configurable
	Long maxSecondsExecutionReport;
	
	/** The max seconds execution process. */
	@Inject @Configurable
	Long maxSecondsExecutionProcess;
	
	/** The max seconds execution service. */
	@Inject @Configurable
	Long maxSecondsExecutionService;
	
	@Inject @Configurable
	Integer registerNotificationPerformance;
	
	@Inject @Configurable
	String usersToNotificationPerformance;
	
	@Inject @Configurable
	String notificationSubjectPerformance;
	

	/**
	 * Instantiates a new performance auditor interceptor.
	 */
	public PerformanceAuditorInterceptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Measure performance.
	 *
	 * @param context the context
	 * @return the object
	 * @throws Exception the exception
	 */
	@AroundInvoke
	public Object measurePerformance(InvocationContext context) throws Exception {
		String methodName = context.getMethod().toString();
        long start = System.currentTimeMillis();
        try {
            return context.proceed();
        } catch (ServiceException se) {
        	throw se;
        } catch (Exception e) {
            log.error(String.format("!!!During invocation of: %s exception occured: %s", new Object[]{methodName, e}));
            monitoringService.exceptionOccurred(methodName, e);
            throw e;
        } finally {
            long duration = System.currentTimeMillis() - start;
            duration = getSeconds(duration);
            PerformanceAuditor performance  = context.getMethod().getAnnotation(PerformanceAuditor.class);
            BooleanType sendNotification = BooleanType.get(registerNotificationPerformance);
            switch (performance.value()) {
			case REPORT:
				if(duration > maxSecondsExecutionReport.longValue()){
					//register parameters only forreports
					ReportLogger reportlogger =(ReportLogger) context.getParameters()[0];
					monitoringService.add(methodName, duration, reportlogger.getParametersString());
					registerNotification(sendNotification.getBooleanValue(),methodName,duration,reportlogger.getParametersString());
				}
				break;
			case SERVICE:
				if(duration > maxSecondsExecutionService.longValue()){
					monitoringService.add(methodName, duration);
					registerNotification(sendNotification.getBooleanValue(),methodName,duration,"");
				}
				break;
			case PROCESS:
				if(duration > maxSecondsExecutionProcess.longValue()){
					monitoringService.add(methodName, duration);
					registerNotification(sendNotification.getBooleanValue(),methodName,duration,"");
				}
				break;
			}
        }
	}
	
	public void registerNotification(boolean send,String method, long duration, String parameters){
		if(send){
			BusinessProcess businessProc = new BusinessProcess();
			businessProc.setIdBusinessProcessPk(BusinessProcessType.PROCESS_AUDIT_MONITORING.getCode());
			String[] usersAndEmails = StringUtils.split(usersToNotificationPerformance, ",");
			List<UserAccountSession> users = new ArrayList<>();
			for(String usersMap :usersAndEmails){
				//index 0 is user and 1 is email
				String[] user = StringUtils.split(usersMap, ":");
				UserAccountSession userAccount = new UserAccountSession();
				userAccount.setUserName(user[0]);
				userAccount.setEmail(user[1]);
				users.add(userAccount);
			}
			StringBuilder message = new StringBuilder("Metodo: "+method+" <br/>"+"ejecutado en "+duration+" segundos"+"<br/> ");
			if(StringUtils.isNotBlank(parameters)){
				message.append("Con parametros: "+parameters);
			}
			LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
			if(loggerUser == null){
				loggerUser = new LoggerUser();
				loggerUser.setIpAddress("192.168.1.1");
				loggerUser.setUserName("MONITORING");
				loggerUser.setIdUserSessionPk(1l);
				loggerUser.setSessionId("sessssxkxkdd");
				loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
				//Praderafilter in each request get current UserAcction
				UserAcctions userAcctions = new UserAcctions();
				userAcctions.setIdPrivilegeAdd(1);
				userAcctions.setIdPrivilegeRegister(1);
				loggerUser.setUserAction(userAcctions);
				loggerUser.setIdPrivilegeOfSystem(1);
				ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
			} else {
				loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
				loggerUser.setIdPrivilegeOfSystem(1);
			}
			notificationService.sendNotificationManual("MONITORING", businessProc, users, notificationSubjectPerformance, message.toString(), NotificationType.EMAIL);
			notificationService.sendNotificationManual("MONITORING", businessProc, users, notificationSubjectPerformance, message.toString(), NotificationType.MESSAGE);
		}
	}
	
	/**
	 * Gets the seconds.
	 *
	 * @param miliseconds the miliseconds
	 * @return the seconds
	 */
	private long getSeconds(long miliseconds){
		return new BigDecimal(String.valueOf(miliseconds)).divide(new BigDecimal("1000"),RoundingMode.HALF_DOWN).longValue();
	}

}
