package com.pradera.core.framework.extension.transactiontimeout;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessAnnotatedType;

import com.pradera.commons.configuration.type.ServerApplication;
import com.pradera.commons.extension.AnnotatedMethodWrapper;
import com.pradera.commons.extension.AnnotatedTypeWrapper;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class TransactionTimeoutExtension.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/06/2014
 */
public class TransactionTimeoutExtension implements Extension {
	
	/** The server name key. */
	private static String SERVER_NAME_KEY="app.server.application.key";
	
	/**
	 * Process annotated type.
	 *
	 * @param <T> the generic type
	 * @param processAnnotatedType the process annotated type
	 */
	public <T> void processAnnotatedType(@Observes ProcessAnnotatedType<T> processAnnotatedType) {
		    AnnotatedType<T> annotatedType = processAnnotatedType.getAnnotatedType();
		    if(hasTimeoutMethods(annotatedType)){
		    	try(InputStream input= ServerApplication.class.getResourceAsStream("/ApplicationConfiguration.properties")){
		    		Properties fileConfig=new Properties();
		    		fileConfig.load(input);
		    		AnnotatedTypeWrapper<T> annotatedTypeWrapper = new AnnotatedTypeWrapper<>(annotatedType);
			    	for (AnnotatedMethod<? super T> annotatedMethod : annotatedType.getMethods()) {
				    	if(annotatedMethod.isAnnotationPresent(TransactionTimeoutDepositary.class)){
				    		TransactionTimeoutDepositary transactioniDepositary = annotatedMethod.getAnnotation(TransactionTimeoutDepositary.class);
					    	long timeOut = transactioniDepositary.value();
					    	TimeUnit unit = transactioniDepositary.unit();
				    		String serverName=fileConfig.getProperty(SERVER_NAME_KEY);
			    			if(serverName.equals(ServerApplication.JBOSS.name())){
			    				//create annotation
				    			Annotation timeoutAnnotation = new TransactionTimeoutLiteral(timeOut, unit);
				    			//wrapper method
				    			AnnotatedMethodWrapper<? super T> annotatedMethodWrapper = new AnnotatedMethodWrapper<>(annotatedMethod);
				    			//add annotation in wrapper
				    			annotatedMethodWrapper.addAnnotation(timeoutAnnotation);
				    			//remove old method
				    			annotatedTypeWrapper.getMethods().remove(annotatedMethod);
				    			//add method with new annotation
				                annotatedTypeWrapper.getMethods().add(annotatedMethodWrapper);
				    		} else {
				    			//will implement with others servers
				    		}
					    	
				    	}
				    }
			    	//process change in bean
			    	processAnnotatedType.setAnnotatedType(annotatedTypeWrapper);
		    	} catch (Exception e) {
					e.printStackTrace();
				}
		    }
		    
	}
	
	private <T> boolean hasTimeoutMethods(AnnotatedType<T> annotatedType) {
        for (AnnotatedMethod<?> annotatedMethod : annotatedType.getMethods()) {
            if (annotatedMethod.isAnnotationPresent(TransactionTimeoutDepositary.class)) {
                return true;
            }
        }
 
        return false;
    }
}
