package com.pradera.core.framework.batchprocess;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.TYPE;
import javax.inject.Qualifier;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Qualifier BatchProcess.
 * Qualifier for inject batch process
 * used to identify classes that implement batchs
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE, METHOD, FIELD, PARAMETER})
public @interface BatchProcess {
	
	/**
	 * Name.
	 *
	 * @return the string
	 */
	String name();
}
