package com.pradera.core.framework.extension.transactiontimeout;

import java.util.concurrent.TimeUnit;

import javax.enterprise.util.AnnotationLiteral;

import org.jboss.ejb3.annotation.TransactionTimeout;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class TransactionTimeoutLiteral.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/06/2014
 */
public class TransactionTimeoutLiteral extends
		AnnotationLiteral<TransactionTimeout> implements TransactionTimeout {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 94943921002870889L;
	
	/** The value. */
	private long value;
	
	/** The unit. */
	private TimeUnit unit;
	
	/**
	 * Instantiates a new transaction timeout literal.
	 */
	public TransactionTimeoutLiteral(){
		
	}
	
	/**
	 * Instantiates a new transaction timeout literal.
	 *
	 * @param value the value
	 * @param unit the unit
	 */
	public TransactionTimeoutLiteral(long value,TimeUnit unit){
		this.value=value;
		this.unit=unit;
	}

	/* (non-Javadoc)
	 * @see org.jboss.ejb3.annotation.TransactionTimeout#unit()
	 */
	@Override
	public TimeUnit unit() {
		// TODO Auto-generated method stub
		return this.unit;
	}

	/* (non-Javadoc)
	 * @see org.jboss.ejb3.annotation.TransactionTimeout#value()
	 */
	@Override
	public long value() {
		// TODO Auto-generated method stub
		return this.value;
	}

}
