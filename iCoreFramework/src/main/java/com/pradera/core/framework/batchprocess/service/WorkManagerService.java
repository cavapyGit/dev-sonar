package com.pradera.core.framework.batchprocess.service;

import com.pradera.core.framework.batchprocess.to.BatchRegisterTO;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Interface WorkManagerService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/12/2012
 */
public interface WorkManagerService {
	
	/**
	 * Inits the batch.
	 *
	 * @param batch the batch
	 */
	public void initBatch(BatchRegisterTO batch);
}
