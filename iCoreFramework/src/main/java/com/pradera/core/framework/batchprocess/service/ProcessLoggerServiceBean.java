package com.pradera.core.framework.batchprocess.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessSchedule;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ProcessLoggerServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/03/2013
 */
@Stateless
public class ProcessLoggerServiceBean {
	
	/** The em. */
	@Inject
	@DepositaryDataBase
	EntityManager em;
	
	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;

	/**
	 * Instantiates a new process logger service bean.
	 */
	public ProcessLoggerServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Update status.
	 *
	 * @param idProcess the id process
	 * @param newStatus the new status
	 */
	public void updateStatus(Long idProcess,Integer newStatus) {
		ProcessLogger processLogger = em.find(ProcessLogger.class, idProcess);
		processLogger.setLastModifyDate(CommonsUtilities.currentDateTime());
		processLogger.setLoggerState(newStatus);
		em.merge(processLogger);
	}
	
	/**
	 * Gets the process logger by id.
	 *
	 * @param idProcessLogger the id process logger
	 * @return the process logger by id
	 */
	public ProcessLogger getProcessLoggerById(Long idProcessLogger) {
		 TypedQuery<ProcessLogger> query = em.createNamedQuery(ProcessLogger.PROCESSLOGGER_DETAILS, ProcessLogger.class);
		 query.setParameter("processId", idProcessLogger);
		return query.getSingleResult();
	}
	
	/**
	 * Update process logger.
	 *
	 * @param processLogger the process logger
	 */
	public void updateProcessLogger(ProcessLogger processLogger) {
		em.merge(processLogger);
	}
	
	/**
	 * Update process schedule execution.
	 * FINISH OR RUNNING
	 * @param processSchedule the process schedule
	 * @param execution the execution
	 */
	public void updateProcessScheduleExecution(Long processSchedule,Integer execution){
		ProcessSchedule schedule = em.find(ProcessSchedule.class, processSchedule);
		schedule.setScheduleExecution(execution);
	}
	
}
