package com.pradera.core.framework.extension.transaction.jta;

import javax.annotation.PostConstruct;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Synchronization;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.core.framework.extension.transaction.TransactionSupport;

public abstract class AbstractTransactionSupport implements TransactionSupport {
	
	private TransactionManager transactionManager;

    private TransactionSynchronizationRegistry synchronizationRegistry;

	public AbstractTransactionSupport() {
		// TODO Auto-generated constructor stub
	}

	/**
     * Looks up a transaction manager.
     *
     * @return {@link TransactionManager}.
     */
    protected abstract TransactionManager lookupTransactionManager();

    /**
     * Looks up a transaction synchronization registry.
     *
     * @return {@link TransactionSynchronizationRegistry}.
     */
    protected abstract TransactionSynchronizationRegistry lookupSynchronizationRegistry();

    /**
     * Initializes the current object.
     */
    @PostConstruct
    void setup()
    {
        transactionManager = lookupTransactionManager();
        synchronizationRegistry = lookupSynchronizationRegistry();
    }

 
    public void begin()
    throws NotSupportedException, SystemException
    {
        transactionManager.begin();
    }

   
    public void commit()
    throws RollbackException, HeuristicMixedException,
        HeuristicRollbackException, SecurityException, IllegalStateException,
        SystemException
    {
        transactionManager.commit();
    }

    
    public void rollback()
    throws IllegalStateException, SecurityException, SystemException
    {
        transactionManager.rollback();
    }

   
    public int getStatus()
    throws SystemException
    {
        return transactionManager.getStatus();
    }

    
    public Transaction suspend()
    throws SystemException
    {
        return transactionManager.suspend();
    }

    
    public void resume(
        Transaction transaction)
    throws InvalidTransactionException, IllegalStateException, SystemException
    {
        transactionManager.resume(transaction);
    }

    
    public void setRollbackOnly()
    throws IllegalStateException, SystemException
    {
        transactionManager.setRollbackOnly();
    }

    
    public void registerSynchronization(
        Synchronization synchronization)
    {
        synchronizationRegistry
            .registerInterposedSynchronization(synchronization);
    }

  
    public Object getResource(
        Object key)
    {
        return synchronizationRegistry.getResource(key);
    }

    
    public void putResource(
        Object key,
        Object value)
    {
        synchronizationRegistry.putResource(key, value);
    }

}
