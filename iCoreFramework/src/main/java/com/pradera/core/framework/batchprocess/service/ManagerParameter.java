package com.pradera.core.framework.batchprocess.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ManagerParameter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/03/2013
 */
@ApplicationScoped
public class ManagerParameter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6235787057809220896L;

	/**
	 * Instantiates a new manager parameter.
	 */
	public ManagerParameter() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the parameters process logger.
	 *
	 * @param processLogger the process logger
	 * @param parameters the parameters
	 * @return the parameters process logger
	 */
	public void getParametersProcessLogger(ProcessLogger processLogger,Map<String,Object> parameters) {
		ProcessLoggerDetail plDetail;
		if (processLogger.getProcessLoggerDetails() == null) {
			processLogger.setProcessLoggerDetails(new ArrayList<ProcessLoggerDetail>());
		}
		//For default value should be Strings
		if (parameters != null){
			Set<Entry<String, Object>>  setMap = parameters.entrySet();
			for (Iterator<Entry<String, Object>>  it = setMap.iterator();it.hasNext(); ){
				Entry<String, Object> entry = it.next();
				plDetail = null;
				plDetail = new ProcessLoggerDetail();
				plDetail.setProcessLogger(processLogger);
				plDetail.setParameterName(entry.getKey());
				//Value in String
				plDetail.setParameterValue(entry.getValue().toString());
				//add parameter
				processLogger.getProcessLoggerDetails().add(plDetail);
			}
		}
	}

}
