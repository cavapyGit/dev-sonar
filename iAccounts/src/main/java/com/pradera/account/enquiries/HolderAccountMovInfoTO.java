package com.pradera.account.enquiries;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * La clase HolderAccountMovInfoTO filtro  HolderAccountMovement .
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
public class HolderAccountMovInfoTO implements Serializable {

/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The market date. */
	private Date marketDate;
	
	/** The block operation. */
	private String blockOperation;
	
	/** The movement quantity. */
	private BigDecimal movementQuantity;
	
	/** The balance type. */
	private Long balanceType;

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}
	
	/**
	 * Gets the block operation.
	 *
	 * @return the block operation
	 */
	public String getBlockOperation() {
		return blockOperation;
	}

	/**
	 * Sets the block operation.
	 *
	 * @param blockOperation the new block operation
	 */
	public void setBlockOperation(String blockOperation) {
		this.blockOperation = blockOperation;
	}

	/**
	 * Gets the movement quantity.
	 *
	 * @return the movement quantity
	 */
	public BigDecimal getMovementQuantity() {
		return movementQuantity;
	}

	/**
	 * Sets the movement quantity.
	 *
	 * @param movementQuantity the new movement quantity
	 */
	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	/**
	 * Gets the balance type.
	 *
	 * @return the balance type
	 */
	public Long getBalanceType() {
		return balanceType;
	}

	/**
	 * Sets the balance type.
	 *
	 * @param balanceType the new balance type
	 */
	public void setBalanceType(Long balanceType) {
		this.balanceType = balanceType;
	}

}
