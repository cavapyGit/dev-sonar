package com.pradera.accounts.assetlaundry.service;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.accounts.assetlaundry.facade.AssetLaundryFacade;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class OfacMassiveRegisterProcess.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Mar 21, 2013
 */
@BatchProcess(name="OfacMassiveRegisterProcessBatch")
@RequestScoped
public class OfacMassiveRegisterProcessBatch implements JobExecution,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The asset laundry facade. */
	@Inject
	AssetLaundryFacade assetLaundryFacade;



	//	/** The asset laundry service bean. */
	@Inject
	AssetLaundryServiceBean assetLaundryServiceBean;



	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		
		String mnemonicOfac = "ofac";
		String currentDt=CommonsUtilities.convertDatetoString(new Date());
		String extension=".xml";
		String validPathOfac=mnemonicOfac+currentDt.replace("/", "")+extension;
		File file = new File(validPathOfac);
		
		try {
			assetLaundryFacade.registerMassiveOfacList(file);
		} catch (ServiceException e) {
			log.info(e.getMessage());
		}
	}



	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
