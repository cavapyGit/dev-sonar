package com.pradera.accounts.assetlaundry.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.valuatorprocess.ValuatorEventBehavior;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class EjemploBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
@BatchProcess(name = "EjemploBatch")
@RequestScoped
public class EjemploBatch implements JobExecution, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The valuator event behaviors. */
	
	private List<ValuatorEventBehavior> valuatorEventBehaviors;
	
	/** USER INFO. */
	private String userSession = "ADMIN";
	
	/** The valuator setup. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorSetup;
	
	/** The ejemplo batch facade. */
	@EJB
	EjemploBatchFacade ejemploBatchFacade;
	
	/** The logger. */
	@Inject
	PraderaLogger logger;


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void startJob(ProcessLogger processLogger) {
		for (int i =1; i<1000;i++){
			try {
				logger.error("EJECUCION DE TAREA PROGRAMADA::::::::::: "+i);
				Thread.sleep(2000);
//				try {
//					ejemploBatchFacade.registerInstitutioInformation(Long.valueOf(i));
//				} catch (ServiceException e) {
//					e.printStackTrace();
//				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}
	

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}