package com.pradera.accounts.assetlaundry.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.accounts.assertlaundry.view.BlockEntityTO;
import com.pradera.accounts.util.PepPersonTO;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.CrossInformationResult;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.type.AssertLaundryType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.process.ProcessLogger;


@BatchProcess(name="PepCrossInformationProcessBatch")
@RequestScoped
public class PepCrossInformationProcessBatch implements Serializable,JobExecution {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private AccountsFacade accountsFacade;	
	@Inject
	private AssetLaundryServiceBean assetLaundryFacade;	
	private List<HolderTO> listHolder;
	private List<PepPerson> listPepPerson;
	private List<LegalRepresentative> listLegalRepresentative;
	private List<BlockEntity> listBlockEntities;
	private List<CrossInformationResult> crossInformationResultList;
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		crossInformationResultList = new ArrayList<CrossInformationResult>();
		this.intializeLists();
		this.compareList();
		this.saveCrossList();
	}
	
  	public void intializeLists(){
		HolderTO filter= new HolderTO();
		BlockEntityTO bEntityTo= new BlockEntityTO();
		PepPersonTO pepPersonTO = new PepPersonTO();
		pepPersonTO.setStartDate(null);
		pepPersonTO.setEndDate(null);
		try {			
			listPepPerson = this.assetLaundryFacade.getPepPersonList(pepPersonTO);
		    listLegalRepresentative = this.getLegalRepresentative();
		    listBlockEntities = this.assetLaundryFacade.getBlockEntityListByFilter(bEntityTo);
			listHolder = this.accountsFacade.getListHoldersByfilter(filter);
		} catch (ServiceException e) {
			e.printStackTrace();
		}	
	}	
 	public void saveCrossList(){
		this.assetLaundryFacade.saveAll(crossInformationResultList);		
	}	
 	public void compareList(){
 		//Concatenated first and second last name of holder,rep,blockE because pna/pep person just have "last_name"
 		String fullLastName=null;
 		for(PepPerson pepPerson: listPepPerson){
			if(pepPerson.getFirstName()!=null && pepPerson.getLastName()!=null){					
				//1. Compare all the names with the holders
				for(int i=0; i<listHolder.size(); i++){
					HolderTO holder=listHolder.get(i);
					if(holder.getName()!=null && holder.getFirstLastName()!=null && holder.getSecondLastName()!=null){
						fullLastName=holder.getFirstLastName()+" "+holder.getSecondLastName();
						if(pepPerson.getFirstName().equals(holder.getName()) && pepPerson.getLastName().equals(fullLastName))	{		
							CrossInformationResult result = this.getInformation(holder, pepPerson);
							crossInformationResultList.add(result);							
						}
					}			
				}				
				//2. Compare legal representative of the holder
				for(LegalRepresentative rep: listLegalRepresentative){
					if(rep.getName()!=null && rep.getFirstLastName()!=null && rep.getSecondLastName()!=null){
						fullLastName=rep.getFirstLastName()+" "+rep.getSecondLastName();
						if(pepPerson.getFirstName().equals(rep.getName()) && pepPerson.getLastName().equals(fullLastName)){
							CrossInformationResult result = this.getInformation(rep, pepPerson,rep.getHolderfk());			
							crossInformationResultList.add(result);		
						}
					}
				}				
				//3. Compare with the Block Entities				
				for(BlockEntity bEntity: listBlockEntities){
					if(bEntity.getName()!=null && bEntity.getFirstLastName()!=null && bEntity.getSecondLastName()!=null){
						fullLastName=bEntity.getFirstLastName()+" "+bEntity.getSecondLastName();
						if(bEntity.getName().equals(pepPerson.getFirstName()) && fullLastName.equals(pepPerson.getLastName())){
							CrossInformationResult result = this.getInformation(bEntity,pepPerson);
							crossInformationResultList.add(result);
						} 
					}
				}
			}			
		}		
	}	
 	public List<LegalRepresentative> getLegalRepresentative(){
		try {
			return accountsFacade.getLegalRepresentative();
		} catch (ServiceException e) {	
			
 		}
		return null;
	} 	
 	public CrossInformationResult getInformation(BlockEntity blockEntity,PepPerson pepPerson){
		CrossInformationResult entity = new CrossInformationResult();
		entity.setIdBlockEntityFk(blockEntity);
		entity.setIndBlockEntity(BooleanType.YES.getCode());
		entity.setIndIssuer(BooleanType.NO.getCode());
		entity.setIndParticipant(BooleanType.NO.getCode());
		entity.setDocumentNumber(blockEntity.getDocumentNumber());
		entity.setDocumentType(blockEntity.getDocumentType());
		entity.setNationality(blockEntity.getNationality());
		entity.setFullName(blockEntity.getName()+" "+ blockEntity.getFirstLastName());
		entity.setPepPersonFk(pepPerson);
		entity.setPersonType(AssertLaundryType.PEP.getCode());
		return entity;
 	}	 
	public CrossInformationResult getInformation(LegalRepresentative legal,PepPerson pepPerson,Long holderPk){
		CrossInformationResult entity = new CrossInformationResult();
		entity.setIdLegalRepresentativeFk(legal);
		entity.setNationality(legal.getNationality());
		entity.setResidenceCountry(legal.getLegalResidenceCountry());
		entity.setFullName(legal.getFullName());
 		entity.setIdHolderFk(new Holder(holderPk));
		entity.setDocumentNumber(legal.getDocumentNumber());
		entity.setDocumentType(legal.getDocumentType());
		entity.setPersonType(AssertLaundryType.PEP.getCode());
		entity.setPepPersonFk(pepPerson);
		entity.setIndBlockEntity(BooleanType.NO.getCode());
		entity.setIndIssuer(BooleanType.NO.getCode());
		entity.setIndParticipant(BooleanType.NO.getCode()); 		
		return entity;
	}
	
 	public CrossInformationResult getInformation(HolderTO holder, PepPerson pepPerson){
		CrossInformationResult entity = new CrossInformationResult();
		entity.setPepPersonFk(pepPerson);
		entity.setDocumentType(holder.getDocumentType());
		entity.setDocumentNumber(holder.getDocumentNumber());
		entity.setFullName(holder.getFullName());
		entity.setNationality(holder.getNationality());
		entity.setIdHolderFk(new Holder(holder.getHolderId()));
		entity.setResidenceCountry(holder.getLegalResidenceCountry());
		Long pk = holder.getParticipantFk();
		entity.setIdParticipantFk(new Participant(holder.getParticipantFk()));
		entity.setPersonType(AssertLaundryType.PEP.getCode());		
		entity.setIndBlockEntity(holder.getBlockEntityFk()==null?BooleanType.NO.getCode():BooleanType.YES.getCode());
		entity.setIndIssuer(holder.getIssuerFk()==null?BooleanType.NO.getCode():BooleanType.YES.getCode());
		entity.setIndParticipant(holder.getIndParticipant());
		return entity; 		
 	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}