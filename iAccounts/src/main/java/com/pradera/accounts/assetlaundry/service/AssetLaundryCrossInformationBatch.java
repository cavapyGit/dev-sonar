package com.pradera.accounts.assetlaundry.service;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.CrossInformationResult;
import com.pradera.model.process.ProcessLogger;

@BatchProcess(name="AssetLaundryCrossInformationBatch")
@RequestScoped
public class AssetLaundryCrossInformationBatch implements JobExecution,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	@Inject
	@BatchProcess(name="PnaCrossInformationProcessBatch")
	JobExecution pnaCrossInformationProcessBatch;

	@Inject
	@BatchProcess(name="PepCrossInformationProcessBatch")
	JobExecution pepCrossInformationProcessBatch;

	@Inject
	@BatchProcess(name="OfacCrossInformationProcessBatch")
	JobExecution ofacCrossInformationProcessBatch;
	
	@Inject
	AssetLaundryServiceBean assetLaundryService;

	@Inject 
	Instance<ReportGenerationService> reportService;

	@Override
	@Transactional
	public void startJob(ProcessLogger process) {

		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());					

		if(deletePreviousData()){
			pnaCrossInformationProcessBatch.startJob(process);	
			pepCrossInformationProcessBatch.startJob(process);
			ofacCrossInformationProcessBatch.startJob(process);
			
			ReportUser user = new ReportUser();
			user.setUserName(loggerUser.getUserName());
			try {
				//if there are information in CrossInformationResultList, send report
				List<CrossInformationResult> lstCrossInf = assetLaundryService.getCrossInformationResultList();
				if(lstCrossInf.size()>0){
					reportService.get().saveReportExecution(new Long(56), null, null, user, loggerUser);
				}
			} catch (ServiceException e) {
 				e.printStackTrace();
			}  
		}
	}

	public boolean deletePreviousData(){
		try {
			//delete all data from cross_information_result
			return assetLaundryService.deletePreviousCrossInformation();
		} catch (ServiceException e) {
 			e.printStackTrace();
		}

		return false;
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}