package com.pradera.accounts.assetlaundry.facade;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.assertlaundry.view.BlockEntityTO;
import com.pradera.accounts.assetlaundry.service.AssetLaundryServiceBean;
import com.pradera.accounts.util.OfacPersonTO;
import com.pradera.accounts.util.PepPersonTO;
import com.pradera.accounts.util.PnaPersonTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.CrossInformationResult;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantHistoryState;
import com.pradera.model.accounts.ParticipantRequest;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.PnaPerson;
import com.pradera.model.accounts.type.AssertLaundryType;
import com.pradera.model.accounts.type.ParticipantRequestStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PepChargeType;
import com.pradera.model.accounts.type.PepMotiveType;
import com.pradera.model.accounts.type.PnaPersonInformationSourceType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.GeographicLocationType;


// TODO: Auto-generated Javadoc
/**
 * The Class AssetLaundryFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class EjemploBatchFacade {


	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;

	/** The log. */
	@Inject
	PraderaLogger log;

/**
 * Register institutio information.
 *
 * @param correlativo the correlativo
 * @throws ServiceException the service exception
 */
//	@ProcessAuditLogger(process=BusinessProcessType.PARTICIPANT_REGISTRATION)
	public void registerInstitutioInformation(Long correlativo) throws ServiceException {
		
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        InstitutionInformation information = new InstitutionInformation();
//        information.setIdInstitutionInformationPk(correlativo);
        information.setDocumentNumber(correlativo.toString());
        information.setDocumentType(correlativo.intValue());
        
        information.setLastModifyUser("EDVJA");
        information.setLastModifyDate(new Date());
        information.setLastModifyIp("127.0.0.1");
        information.setLastModifyApp(12);
		crudDaoServiceBean.create(information);
	
	}

}
