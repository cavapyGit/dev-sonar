package com.pradera.accounts.assetlaundry.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.accounts.assertlaundry.view.BlockEntityTO;
import com.pradera.accounts.assetlaundry.facade.AssetLaundryFacade;
import com.pradera.accounts.util.OfacPersonTO;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.CrossInformationResult;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.AssertLaundryType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.process.ProcessLogger;


@BatchProcess(name="OfacCrossInformationProcessBatch")
@RequestScoped
public class OfacCrossInformationProcessBatch implements JobExecution,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private AccountsFacade accountsFacade;
	@Inject
	private AssetLaundryFacade assetLaundryFacade;
	
	private List<HolderTO> listHolder;
	private List<OfacPerson> listOfacPerson;
	private List<LegalRepresentative> listLegalRepresentative;
	private List<BlockEntity> listBlockEntities;
	private List<CrossInformationResult> crossInformationResultList;

	@Override
	public void startJob(ProcessLogger processLogger) {
		crossInformationResultList = new ArrayList<CrossInformationResult>();
 		this.intializeLists();
		this.compareList();
		try {
			this.saveCrossList();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public void intializeLists(){
		HolderTO filter= new HolderTO();
		BlockEntityTO bEntityTo= new BlockEntityTO();
		OfacPersonTO ofacFilterTO = new OfacPersonTO();
		ofacFilterTO.setInitialDate(null);
		ofacFilterTO.setEndDate(null);
		try {
			listOfacPerson = this.assetLaundryFacade.getOfacPersonList(ofacFilterTO);
			listLegalRepresentative = this.getLegalRepresentative();
			listBlockEntities = this.assetLaundryFacade.getBlockEntitiesByFilter(bEntityTo);
			listHolder = this.accountsFacade.getListHoldersByfilter(filter);
		} catch (ServiceException e) {
			e.printStackTrace();
		}	
	}

	public void saveCrossList() throws ServiceException{
		this.assetLaundryFacade.saveCrossInformationResultList(crossInformationResultList);
	}
	public void compareList(){
		//Concatenated first and second last name of holder,rep,blockE because pna/pep person just have "last_name"
		String fullLastName=null;
 		for(OfacPerson ofacPerson: listOfacPerson){
			if(ofacPerson.getFirstName()!=null && ofacPerson.getLastName()!=null){				
				//1. Compare all the names with the holders
				for(int i=0; i<listHolder.size(); i++){
					HolderTO holder=listHolder.get(i);
					if(holder.getName()!=null && holder.getFirstLastName()!=null && holder.getSecondLastName()!=null){
						fullLastName=holder.getFirstLastName()+" "+holder.getSecondLastName();
						if(ofacPerson.getFirstName().equals(holder.getName()) && ofacPerson.getLastName().equals(fullLastName))	{		
							CrossInformationResult result = this.getInformation(holder, ofacPerson);
							crossInformationResultList.add(result);							
						}
					}									
				}				
				//2. Compare legal representative of the holder
				for(LegalRepresentative rep: listLegalRepresentative){
					if(rep.getName()!=null && rep.getFirstLastName()!=null && rep.getSecondLastName()!=null){
						fullLastName=rep.getFirstLastName()+" "+rep.getSecondLastName();
						if(ofacPerson.getFirstName().equals(rep.getName()) && ofacPerson.getLastName().equals(fullLastName)){
							CrossInformationResult result = this.getInformation(rep, ofacPerson,rep.getHolderfk());								
							crossInformationResultList.add(result);		
						}
					}
				}				
				//3. Compare with the Block Entities				
				for(BlockEntity bEntity: listBlockEntities){
					if(bEntity.getName()!=null && bEntity.getFirstLastName()!=null && bEntity.getSecondLastName()!=null){
						fullLastName=bEntity.getFirstLastName()+" "+bEntity.getSecondLastName();
						if(bEntity.getName().equals(ofacPerson.getFirstName()) && fullLastName.equals(ofacPerson.getLastName())){
							CrossInformationResult result = this.getInformation(bEntity,ofacPerson);
							crossInformationResultList.add(result);
						} 
					}
				}
			}
		}
	}
	public List<LegalRepresentative> getLegalRepresentative(){
		try {
			return accountsFacade.getLegalRepresentative();
		} catch (ServiceException e) {
		}
		return null;
	}
	public CrossInformationResult getInformation(BlockEntity blockEntity,OfacPerson ofacPerson){
		CrossInformationResult entity = new CrossInformationResult();
		entity.setIdBlockEntityFk(blockEntity);
		entity.setIndBlockEntity(BooleanType.YES.getCode());
		entity.setIndIssuer(BooleanType.NO.getCode());
		entity.setIndParticipant(BooleanType.NO.getCode());
		entity.setDocumentNumber(blockEntity.getDocumentNumber());
		entity.setDocumentType(blockEntity.getDocumentType());
		entity.setNationality(blockEntity.getNationality());
		entity.setFullName(blockEntity.getName()+" "+ blockEntity.getFirstLastName());
		entity.setOfacPersonFk(ofacPerson);
		entity.setPersonType(AssertLaundryType.OFAC.getCode());
		return entity;
	}
	public CrossInformationResult getInformation(LegalRepresentative legal,OfacPerson ofacPerson,Long holderPk){
		CrossInformationResult entity = new CrossInformationResult();
		entity.setIdLegalRepresentativeFk(legal);
		entity.setNationality(legal.getNationality());
		entity.setResidenceCountry(legal.getLegalResidenceCountry());
		entity.setFullName(legal.getFullName());
		entity.setPersonType(AssertLaundryType.OFAC.getCode());
		entity.setIdHolderFk(new Holder(holderPk));
		entity.setDocumentNumber(legal.getDocumentNumber());
		entity.setDocumentType(legal.getDocumentType());
		entity.setPersonType(AssertLaundryType.OFAC.getCode());
		entity.setOfacPersonFk(ofacPerson);
		entity.setIndBlockEntity(BooleanType.NO.getCode());
		entity.setIndIssuer(BooleanType.NO.getCode());
		entity.setIndParticipant(BooleanType.NO.getCode());
		return entity;
	}
	public CrossInformationResult getInformation(HolderTO holder, OfacPerson ofacPerson){
		CrossInformationResult entity = new CrossInformationResult();
		entity.setOfacPersonFk(ofacPerson);
		entity.setDocumentType(holder.getDocumentType());
		entity.setDocumentNumber(holder.getDocumentNumber());
		entity.setFullName(holder.getFullName());
		entity.setNationality(holder.getNationality());
		entity.setIdHolderFk(new Holder(holder.getHolderId()));
		entity.setResidenceCountry(holder.getLegalResidenceCountry());
		Long pk = holder.getParticipantFk();
		entity.setIdParticipantFk(new Participant(holder.getParticipantFk()));
		entity.setPersonType(AssertLaundryType.OFAC.getCode());		
		entity.setIndBlockEntity(holder.getBlockEntityFk()==null?BooleanType.NO.getCode():BooleanType.YES.getCode());
		entity.setIndIssuer(holder.getIssuerFk()==null?BooleanType.NO.getCode():BooleanType.YES.getCode());
		entity.setIndParticipant(holder.getIndParticipant());
		return entity; 		
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
