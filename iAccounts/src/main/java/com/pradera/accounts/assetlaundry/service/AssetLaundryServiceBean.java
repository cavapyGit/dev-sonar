package com.pradera.accounts.assetlaundry.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.accounts.assertlaundry.view.BlockEntityTO;
import com.pradera.accounts.util.FileProcessor;
import com.pradera.accounts.util.OfacPersonTO;
import com.pradera.accounts.util.PepPersonTO;
import com.pradera.accounts.util.PnaPersonTO;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.CrossInformationResult;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.PnaPerson;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.GeographicLocation;
// TODO: Auto-generated Javadoc

/**
 * The Class AssetLaundryServiceBean.
 */
@Stateless
public class AssetLaundryServiceBean extends CrudDaoServiceBean {

	/** The Constant UNUSED. */
	private static final String UNUSED = "unused";
	

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	@Inject
	private FileProcessor fileProcesor;
	
	 @EJB
	 ParameterServiceBean parameterServiceBean;

	//	@Inject
	//	PraderaLogger log;


	 public List<BlockEntity> getBlockEntityListByFilter(BlockEntityTO filter)throws ServiceException{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("select id_block_entity_pk,");
			sbQuery.append("name,");
			sbQuery.append("first_last_name,");
			sbQuery.append("nationality,");
			sbQuery.append("document_type,");
			sbQuery.append("document_number ");
			sbQuery.append(" from block_entity be");
			sbQuery.append(" where 1=1");
			
			if(filter.getPersonType()!=null	){
				sbQuery.append(" and be.person_type=:personType");
			}
			Query query = em.createNativeQuery(sbQuery.toString());
			
			if(filter.getPersonType()!=null){
			query.setParameter("personType",filter.getPersonType());
			}
			
			List<Object[]> listObjects = query.getResultList();
			List<BlockEntity> listBlockEntity = new ArrayList<BlockEntity>();
			BlockEntity bEntity;
			for(Object[] temp: listObjects){
				bEntity = new BlockEntity();
				bEntity.setIdBlockEntityPk(Long.parseLong(temp[0].toString()));
				bEntity.setName(temp[1]!=null?temp[1].toString():null);
				bEntity.setFirstLastName(temp[2]!=null?temp[2].toString():null);
				bEntity.setNationality(Integer.parseInt(temp[3].toString()));
				bEntity.setDocumentType(Integer.parseInt(temp[4].toString()));
				bEntity.setDocumentNumber(temp[5].toString());
				listBlockEntity.add(bEntity);
			}
			return listBlockEntity;
		}

	/**
	 * Save the AssetLaundry Entities
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 */
	public <T> T save(T t) throws ServiceException
	{
		 super.create(t);
		 
    		return t;
	}


	/**
	 * Read xml file file and process the result
	 *
	 * @param file the file
	 * @return the list
	 * @throws ServiceException the service exception
	 */


	@SuppressWarnings("rawtypes")
	public Map<String, List> readXMLFile(File file, LoggerUser loggerUser) throws ServiceException{
		Map<String, List> map = new HashMap<String, List>();
		map = fileProcesor.processOfacXml(file, loggerUser);

		return map;
	}

	/**
	 * Read xls file.
	 *
	 * @param file the file
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	public Map<String, List> readXlsFile(File file) throws ServiceException{
		System.out.println("ReadXLS FILE ");
		if(file==null){
			System.out.println("THe file is null");
		}
		else{
			System.out.println("THe file is not null");

			
		}

		Map<String, List> map = new HashMap<String, List>();
		List<PnaPerson> pnaPersonList = new ArrayList<PnaPerson>();
		List<PepPerson> pepPersonList = new ArrayList<PepPerson>();

		map.put("pna", pnaPersonList);
		map.put("pep", pepPersonList);

		return map;
		
 	}


	/**
	 * Gets the pna person list.
	 *
	 * @param filter the filter
	 * @return the pna person list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<PnaPerson> getPnaPersonList(PnaPersonTO filter) throws ServiceException
	{
		List<PnaPerson> list;
		 

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from PnaPerson p left join fetch p.country c left join fetch p.expeditionPlace ep ");
		sbQuery.append("where 1 = 1");
		
		if(Validations.validateIsNotNull(filter.getRegisterNumber())){
			if(filter.getRegisterNumber()!=0){
			sbQuery.append(" And p.idPnaPersonPk =:rNumber");
			}
		}

		if(Validations.validateIsNotNull(filter.getName())){
			if(!Validations.validateIsEmpty(filter.getName())){
				sbQuery.append(" And p.firstName like :fName");
			}
		}
		
		if(Validations.validateIsNotNull(filter.getLastName())){
			if(!Validations.validateIsEmpty(filter.getLastName())){
				sbQuery.append(" And p.lastName like :lName");
			}
		}

		if(Validations.validateIsNotNull(filter.getStartDate()) && Validations.validateIsNotNull(filter.getEndDate())){

			sbQuery.append(" And trunc(p.registryDate) BETWEEN :startDate AND :endDate");
		}

		if(Validations.validateIsNotNull(filter.getIdCountry())){

			if(filter.getIdCountry()!=-1){
				sbQuery.append(" And p.country.idGeographicLocationPk = :idCountry");
			}
		}
		sbQuery.append(" order by p.idPnaPersonPk");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNull(filter.getRegisterNumber())){
			if(filter.getRegisterNumber()!=0){
				query.setParameter("rNumber", filter.getRegisterNumber());
			}
		}

		if(Validations.validateIsNotNull(filter.getName())){

			if(!Validations.validateIsEmpty(filter.getName())){

				query.setParameter("fName", "%"+filter.getName()+"%");
			}
		}
		
		if(Validations.validateIsNotNull(filter.getLastName())){
			if(!Validations.validateIsEmpty(filter.getLastName())){
				query.setParameter("lName", "%"+filter.getLastName()+"%");

 			}
		}

		if(Validations.validateIsNotNull(filter.getStartDate()) && Validations.validateIsNotNull(filter.getEndDate())){

			query.setParameter("startDate", filter.getStartDate());
			query.setParameter("endDate", filter.getEndDate());
 		}

		if(Validations.validateIsNotNull(filter.getIdCountry())){

			if(filter.getIdCountry()!=-1){

				query.setParameter("idCountry", filter.getIdCountry());

			}
		}

		list = (List<PnaPerson>)query.getResultList();
		 
		log.info("Asset laundr service bean getOnaList total of the list:"+list.size());
		return list;

	}

	/**
	 * Gets the ofac person list.
	 *
	 * @param filter the filter
	 * @return the ofac person list
	 */
	@SuppressWarnings("unchecked")
	public List<OfacPerson> getOfacPersonList(OfacPersonTO filter) throws ServiceException{
		List<OfacPerson> list;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from OfacPerson p");
		sbQuery.append(" Where 1 = 1");
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getOfacPersonType())){
			sbQuery.append(" and p.personType=:entity");
		}
		
 		if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
			sbQuery.append(" and p.firstName like :fName");
		}
 		
 		if(Validations.validateIsNotNullAndNotEmpty(filter.getLastName())){
			sbQuery.append(" and p.lastName like :lName");
 		}
 		
 		if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
			sbQuery.append(" and p.fullName like :fName");
 		}
 		
 		if(Validations.validateIsNotNullAndNotEmpty(filter.getCountry())){
 			sbQuery.append(" and p.residenceCountry like :country");
 		}
 		
		if(Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getEndDate())){
			sbQuery.append(" and trunc(p.registryDate) between trunc(:starDate) and trunc(:endDate)");
		}
		
		if(Validations.validateIsNotNull(filter.getRegisterNumber())){
			if(filter.getRegisterNumber()!=0){
				sbQuery.append(" and p.idOfacPersonPk=:regNumber");
			}
		}
		sbQuery.append(" order by p.idOfacPersonPk");
		
 		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
			query.setParameter("fName", "%"+filter.getName()+"%");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getOfacPersonType())){
			query.setParameter("entity", filter.getOfacPersonType());
 		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getLastName())){
			query.setParameter("lName", "%"+filter.getLastName()+"%");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
			query.setParameter("fName", "%"+filter.getFullName()+"%");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCountry())){
			query.setParameter("country", "%"+filter.getCountry()+"%");
		}
		
		if(Validations.validateIsNotNull(filter.getRegisterNumber())){
			if(filter.getRegisterNumber()!=0){
				query.setParameter("regNumber",filter.getRegisterNumber());
			}
		}
		
		if(Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getEndDate())){
			query.setParameter("starDate",filter.getInitialDate());
			query.setParameter("endDate",filter.getEndDate());
		}

		list = (List<OfacPerson>)query.getResultList();

		return list;

	}

	/**
	 * Gets the pep person list.
	 *
	 * @param filter the filter
	 * @return the pep person list
	 */
	@SuppressWarnings("unchecked")
	public List<PepPerson> getPepPersonList(PepPersonTO filter) throws ServiceException
	{
		//		log.info("From search " + filter.getName());


		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from PepPerson p join fetch p.geographicLocation g left join fetch p.expeditionPlace ep ");
		sbQuery.append("where 1 = 1");
		
		if(Validations.validateIsNotNull(filter.getRegisterNumber())){
			if(filter.getRegisterNumber()!=0){
			sbQuery.append(" And p.idPepPersonPk = :rNumber");
			}
			
		}
		
		if(Validations.validateIsNotNull(filter.getName())){
			if(!Validations.validateIsEmpty(filter.getName())){
				sbQuery.append(" And p.firstName like :fName");
			}
		}
		
		if(Validations.validateIsNotNull(filter.getLastName())){
			if(!Validations.validateIsEmpty(filter.getLastName())){
				sbQuery.append(" And p.lastName like :lName");
			}
		}
		if(Validations.validateIsNotNull(filter.getIdMotive())){
			if(filter.getIdMotive()!=-1){
				sbQuery.append(" And p.category = :idMotive");

			}
		}

		if(Validations.validateIsNotNull(filter.getIdCountry())){

			if(filter.getIdCountry()!=-1){
				sbQuery.append(" And p.geographicLocation.idGeographicLocationPk = :idCountry");
			}
		}

		if(Validations.validateIsNotNull(filter.getStartDate()) && Validations.validateIsNotNull(filter.getEndDate())){
			sbQuery.append(" and trunc(p.registryDate) between trunc(:starDate) and trunc(:endDate)");

		}
		sbQuery.append(" order by p.idPepPersonPk");
		//********* CREATE QUERY *************************
		Query query = em.createQuery(sbQuery.toString());
		
		
		if(Validations.validateIsNotNull(filter.getRegisterNumber())){
			if(filter.getRegisterNumber()!=0){
			query.setParameter("rNumber", filter.getRegisterNumber());
			}
		}
		
		
		
		if(Validations.validateIsNotNull(filter.getName())){
			if(!Validations.validateIsEmpty(filter.getName())){
				query.setParameter("fName", "%"+filter.getName().toUpperCase()+"%");
			}
		}
		
		if(Validations.validateIsNotNull(filter.getLastName())){
			if(!Validations.validateIsEmpty(filter.getLastName())){
				query.setParameter("lName", "%"+filter.getLastName().toUpperCase()+"%");
			}
		}

		if(Validations.validateIsNotNull(filter.getIdMotive())){
			if(filter.getIdMotive()!=-1){
				query.setParameter("idMotive", filter.getIdMotive());
			}

		}

		if(Validations.validateIsNotNull(filter.getIdCountry())){
			if(filter.getIdCountry()!=-1){
				query.setParameter("idCountry",filter.getIdCountry());
			}
		}
		if(Validations.validateIsNotNull(filter.getStartDate()) && Validations.validateIsNotNull(filter.getEndDate())){
			query.setParameter("starDate",filter.getStartDate());
			query.setParameter("endDate",filter.getEndDate());
		}
		
		List<PepPerson> list = (List<PepPerson>)query.getResultList();
		int cont=0;
		
		for (PepPerson pp : list) {
			PepPerson pep= new PepPerson();
			
			if(Validations.validateIsNotNullAndNotEmpty(pp.getClassification())){
				pep.setClassification(pp.getClassification());
				if(pp.getClassification()!=0){
					pep.setClassificationDesc(parameterServiceBean.getParameterTableById(pp.getClassification()).getDescription());
				}else{
					pep.setClassificationDesc(pp.getClassification().toString());
				}
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getIdPepPersonPk())){
				pep.setIdPepPersonPk(pp.getIdPepPersonPk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getFirstName())){
				pep.setFirstName(pp.getFirstName());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getLastName())){
				pep.setLastName(pp.getLastName());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getGeographicLocation().getName())){
				GeographicLocation gl = new GeographicLocation();
				gl.setName(pp.getGeographicLocation().getName());
				pep.setGeographicLocation(gl);
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getExpeditionPlace().getName())){
				GeographicLocation gl = new GeographicLocation();
				gl.setName(pp.getGeographicLocation().getName());
				pep.setExpeditionPlace(gl);
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getDocumentType())){
				pep.setDocumentType(pp.getDocumentType());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getRole())){
				pep.setRole(pp.getRole());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getDocumentNumber())){
				pep.setDocumentNumber(pp.getDocumentNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getNotificationDate())){
				pep.setNotificationDate(pp.getNotificationDate());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getRegistryDate())){
				pep.setRegistryDate(pp.getRegistryDate());
			}
//			if(Validations.validateIsNotNullAndNotEmpty(pp.getMotiveDescription())){
//				pep.setMotiveDescription(pp.getMotiveDescription());
//			}
//			if(Validations.validateIsNotNullAndNotEmpty(pp.getChargeDescription())){
//				pep.setChargeDescription(pp.getChargeDescription());
//			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getBeginingPeriod())){
				pep.setBeginingPeriod(pp.getBeginingPeriod());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getEndingPeriod())){
				pep.setEndingPeriod(pp.getEndingPeriod());
			}
			if(Validations.validateIsNotNullAndNotEmpty(pp.getCategory())){
				pep.setCategory(pp.getCategory());
			}
			
			list.set(cont, pep);
			cont++;
		}
	
		return list;
	}
	
	public boolean deletePreviousCrossInformation()throws ServiceException{
		 Query query = em.createQuery("delete from CrossInformationResult");
		 query.executeUpdate();
		 return true;
	}
	
	public List<CrossInformationResult> getCrossInformationResultList(){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select cir from CrossInformationResult cir left join fetch cir.pnaPersonFk pna"
						+ " left join fetch cir.pepPersonFk pep left join fetch cir.idHolderFk "
						+ " left join fetch cir.idLegalRepresentativeFk left join fetch cir.idParticipantFk"
						+ " left join fetch cir.idBlockEntityFk left join fetch pna.country cou "
						+ " left join fetch pep.geographicLocation geo left join fetch cir.ofacPersonFk");
		Query query = em.createQuery(sbQuery.toString());
		List<CrossInformationResult> list = (List<CrossInformationResult>) query.getResultList();
		
		return list;
	}
	
	
	/**
	 * Process ofac lif.
	 *
	 * @param ofacPersonList the ofac person list
	 * @throws ServiceException the service exception
	 */
	public void processOfacLif(List<OfacPerson> ofacPersonList)throws ServiceException{
		
		if(Validations.validateIsNotNull(ofacPersonList)){
			
			for(OfacPerson ofac: ofacPersonList){
				log.info("Ofac PK:" +ofac.getIdOfacPersonPk());
				this.update(ofac);
			}
		}
	}
	
	
	public List<String[]> processExcelFile(File file)throws ServiceException{
		 List<String[]>  temp = FileProcessor.processExcelFile(file);
		 return temp;

	}

}
