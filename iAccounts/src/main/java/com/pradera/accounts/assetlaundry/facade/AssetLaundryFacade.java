package com.pradera.accounts.assetlaundry.facade;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.assertlaundry.view.BlockEntityTO;
import com.pradera.accounts.assetlaundry.service.AssetLaundryServiceBean;
import com.pradera.accounts.util.OfacPersonTO;
import com.pradera.accounts.util.PepPersonTO;
import com.pradera.accounts.util.PnaPersonTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.CrossInformationResult;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.PnaPerson;
import com.pradera.model.accounts.type.AssertLaundryType;
import com.pradera.model.accounts.type.PepChargeType;
import com.pradera.model.accounts.type.PepMotiveType;
import com.pradera.model.accounts.type.PnaPersonInformationSourceType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.GeographicLocationType;


// TODO: Auto-generated Javadoc
/**
 * The Class AssetLaundryFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */	
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AssetLaundryFacade {


	/** The general parameters bean. */
	@Inject
	private GeneralParametersFacade generalParametersBean;


	/** The asset laundry service. */
	@Inject
	private AssetLaundryServiceBean assetLaundryService;

	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	/** The log. */
	@Inject
	PraderaLogger log;

	
	/**
	 * Gets the block entities by filter.
	 *
	 * @param filter the filter
	 * @return the block entities by filter
	 * @throws ServiceException the service exception
	 */
	public List<BlockEntity> getBlockEntitiesByFilter(BlockEntityTO filter)throws ServiceException{
		return assetLaundryService.getBlockEntityListByFilter(filter);
	}
	
	/**
	 * Gets the assert laundry type list.
	 *
	 * @return the assert laundry type list
	 * @throws ServiceException the service exception
	 */
	public List<AssertLaundryType> getAssertLaundryTypeList() throws ServiceException
	{
		return AssertLaundryType.list;
	}

	/**
	 * Gets the country list.
	 *
	 * @return the country list
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getCountryList() throws ServiceException
	{
		GeographicLocationTO geoLocation=new GeographicLocationTO();
		geoLocation.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());

		List<GeographicLocation> countryList= new ArrayList<GeographicLocation>();

		countryList = generalParametersBean.getListGeographicLocationServiceFacade(geoLocation);

		return countryList;
	}



	/**
	 * Save pna person.
	 *
	 * @param pnaPerson the pna person
	 * @return the pna person
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PNA_PERSONA_REGISTER)
	public PnaPerson savePnaPerson(PnaPerson pnaPerson) throws ServiceException
	{
		//Audit Process
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		pnaPerson.setAudit(loggerUser);
		PnaPerson temp = null;
		pnaPerson.setRegistryDate(CommonsUtilities.currentDateTime());
		pnaPerson.setRegistryUser(loggerUser.getUserName());

		temp = assetLaundryService.save(pnaPerson);

		return temp;


	}

	/**
	 * Save pep person.
	 *
	 * @param pepPerson the pep person
	 * @return the pep person
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PEP_PERSONA_REGISTER)
	public PepPerson savePepPerson(PepPerson pepPerson) throws ServiceException
	{
		//Audit Process
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		pepPerson.setRegistryDate(CommonsUtilities.currentDateTime());
		pepPerson.setRegistryUser(loggerUser.getUserName());
		pepPerson.setAudit(loggerUser);

		PepPerson temp = null;

		temp = assetLaundryService.save(pepPerson);

		return temp;

	}

	/**
	 * Gets the ofac count.
	 *
	 * @param file the file
	 * @param loggerUser the logger user
	 * @return the ofac count
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("rawtypes")
	public Map<String, List> getOfacCount(File file, LoggerUser loggerUser) throws ServiceException{

//		Map<String,String> result = new HashMap<String,String>();

		return assetLaundryService.readXMLFile(file, loggerUser);
	}

	/**
	 * Register massive ofac list.
	 *
	 * @param file the file
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.OFAC_FILE_PROCESS)
	public void registerMassiveOfacList(File file) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAdd());
		if(loggerUser.getIdPrivilegeOfSystem()==null){
			loggerUser.setIdPrivilegeOfSystem(1);
		}
		Map<String, List> ofacLists;

		ofacLists = this.getOfacCount(file, loggerUser);
		
		List<OfacPerson> ofacPersonList = ofacLists.get("success");
		for(OfacPerson person: ofacPersonList){
			person.setAudit(loggerUser);
		}
		assetLaundryService.processOfacLif(ofacPersonList);
	}


	/**
	 * Gets the pna pep count.
	 *
	 * @param file the file
	 * @return the pna pep count
	 * @throws Exception the exception
	 */
	public Map<String,String> getPnaPepCount(File file) throws Exception{

		return null;
	}


	/**
	 * Gets the pna person.
	 *
	 * @param filter the filter
	 * @return the pna person
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ASSEUT_LAUNDERING_PERSONS_QUERY)
	public List<PnaPerson> getPnaPerson(PnaPersonTO filter)  throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());

		return 	assetLaundryService.getPnaPersonList(filter);
	}

	/**
	 * Gets the pep person.
	 *
	 * @param filter the filter
	 * @return the pep person
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ASSEUT_LAUNDERING_PERSONS_QUERY)
	public List<PepPerson> getPepPerson(PepPersonTO filter) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());

		return 	assetLaundryService.getPepPersonList(filter);
	}

	/**
	 * Gets the ofac person list.
	 *
	 * @param filter the filter
	 * @return the ofac person list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ASSEUT_LAUNDERING_PERSONS_QUERY)
	public List<OfacPerson> getOfacPersonList(OfacPersonTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());

		return assetLaundryService.getOfacPersonList(filter);
	}

	/**
	 * Process excel file.
	 *
	 * @param file the file
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	public void processExcelFile(File file) throws ServiceException{

		List<String[]> temp= assetLaundryService.processExcelFile(file);
		this.insertAssetLaundryLists(temp);
		print(temp);

	}

	/**
	 * Prints the.
	 *
	 * @param objectList the object list
	 * @throws ServiceException the service exception
	 */
	public void print(List<String[]> objectList)throws ServiceException{
		for(String[] temp:objectList){

			for(int i =0; i<temp.length;i++){
				System.out.print(temp[i] + " ");
			}
			System.out.println();
		}

	}

	/**
	 * Insertar Lista de Lavado de Activos
	 *
	 * @param objectList the object list
	 * @throws ServiceException the service exception
	 */
	public void insertAssetLaundryLists(List<String[]> objectList)throws ServiceException{
		int rownNum=0;

		try{
			PnaPerson pnaPerson = new PnaPerson();
			PepPerson pepPerson = new PepPerson();

			for(String[] temp: objectList){
				if(temp[0]!=null){
					if(temp[0].toString().equals("PNA")){
						rownNum++;

						if(temp[1]!=null){
							pnaPerson.setFirstName(temp[1]);
						}

						if(temp[2]!=null){
							pnaPerson.setLastName(temp[2]);
						}
						if(temp[3]!=null){
							System.out.println(temp[3]);
							int length = temp[3].length();
							Integer d = Integer.parseInt(temp[3].substring(0, length-2));
							GeographicLocation country = new GeographicLocation();
							country.setIdGeographicLocationPk(d);
							pnaPerson.setCountry(country);

						}

						if(temp[5]!=null){
							pnaPerson.setComment(temp[5]);
						}

						if(temp[8]!=null){
							pnaPerson.setMotive(temp[8]);					
						}
						if(temp[9]!=null){
							int length = temp[9].length();
							Integer d = Integer.parseInt(temp[9].substring(0, length-2));
							PnaPersonInformationSourceType information =  PnaPersonInformationSourceType.get(d);
							if(information==null){
								Object[] paramObject = new Object[1];
								paramObject[0] = rownNum;
								throw new ServiceException(ErrorServiceType.ASSET_LAUNDERING_PROCESS_MASSIVE_FILE,paramObject);
							}

							pnaPerson.setInformationSource(d);
						}

						pnaPerson.setNotificationDate(CommonsUtilities.currentDate());
						pnaPerson.setRegistryDate(CommonsUtilities.currentDate());
						this.savePnaPerson(pnaPerson);
					}

					else if(temp[0].toString().equals("PEP")){
						rownNum++;
						if(temp[1]!=null){
							pepPerson.setFirstName(temp[1]);
						}

						if(temp[2]!=null){
							pepPerson.setLastName(temp[2]);
						}
						if(temp[3]!=null){
							System.out.println(temp[3]);
							int length = temp[3].length();
							Integer d = Integer.parseInt(temp[3].substring(0, length-2));
							System.out.println("NUMBER "+ d);
							GeographicLocation country = new GeographicLocation();
							country.setIdGeographicLocationPk(d);
							pepPerson.setGeographicLocation(country);

						}

						if(temp[4]!=null){
							int length = temp[4].length();
							Integer d = Integer.parseInt(temp[4].substring(0, length-2));
							PepChargeType charge = PepChargeType.get(d);
							if(charge==null){
								Object[] paramObject = new Object[1];
								paramObject[0] = rownNum;
								throw new ServiceException(ErrorServiceType.ASSET_LAUNDERING_PROCESS_MASSIVE_FILE,paramObject);
							}
							pepPerson.setRole(d);	

						}

						//StartDate
						if(temp[5]!=null){
							Date date = CommonsUtilities.convertStringtoDate(temp[5]);
							pepPerson.setBeginingPeriod(date);
						}

						//End Date
						if(temp[6]!=null){
							Date date = CommonsUtilities.convertStringtoDate(temp[6]);
							pepPerson.setEndingPeriod(date);
						}

						if(temp[7]!=null){
							int length = temp[7].length();
							Integer d = Integer.parseInt(temp[7].substring(0, length-2));
							PepMotiveType motive = PepMotiveType.get(d);
							if(motive==null){
								Object[] paramObject = new Object[1];
								paramObject[0] = rownNum;
								throw new ServiceException(ErrorServiceType.ASSET_LAUNDERING_PROCESS_MASSIVE_FILE,paramObject);
							}
							pepPerson.setCategory(d);			
						}
					}

					pepPerson.setNotificationDate(CommonsUtilities.currentDate());
					pepPerson.setRegistryDate(CommonsUtilities.currentDate());
					this.savePepPerson(pepPerson);
				}
			}				

		}catch (Exception e) {
			e.printStackTrace();
			Object[] paramObject = new Object[1];
			paramObject[0] = rownNum;
			throw new ServiceException(ErrorServiceType.ASSET_LAUNDERING_PROCESS_MASSIVE_FILE,paramObject);
		}

	}

	/**
	 * Save cross information result list.
	 *
	 * @param list the list
	 * @throws ServiceException the service exception
	 */
	public void saveCrossInformationResultList(List<CrossInformationResult> list)throws ServiceException{
		assetLaundryService.saveAll(list);
	}

}
