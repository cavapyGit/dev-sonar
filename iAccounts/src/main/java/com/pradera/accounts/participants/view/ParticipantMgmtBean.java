package com.pradera.accounts.participants.view;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.investorType.facade.InvestorTypeFacade;
import com.pradera.accounts.investorType.to.InvestorTypeModelTO;
import com.pradera.accounts.participants.facade.ParticipantServiceFacade;
import com.pradera.accounts.participants.to.ParticipantIntDepositoryTO;
import com.pradera.accounts.participants.to.ParticipantMechanismTO;
import com.pradera.accounts.participants.to.ParticipantTO;
import com.pradera.accounts.participants.to.SearchBankAccountsParticipantTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.InternationalDepositoryTO;
import com.pradera.core.component.accounts.to.LegalRepresentativeTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.extension.view.LegalRepresentativeHelperBean;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantFile;
import com.pradera.model.accounts.ParticipantIntDepositary;
import com.pradera.model.accounts.ParticipantIntDepositaryPK;
import com.pradera.model.accounts.ParticipantMechanism;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.InternationalDepositoryStateType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.ParticipantFileStateType;
import com.pradera.model.accounts.type.ParticipantIntDepositaryStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantRoleType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RequestParticipantFileType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.process.BusinessProcess;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/02/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ParticipantMgmtBean extends GenericBaseBean implements Serializable{		
	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5419913882509199156L;
	
	/** The legal representative helper bean. */
	@Inject
    private LegalRepresentativeHelperBean legalRepresentativeHelperBean;
	
	/** The document validator. */
	@Inject
    DocumentValidator documentValidator;

	/** The document to. */
	private DocumentTO documentTO;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	/** The ind numeric second type document legal. */
	private boolean indNumericSecondTypeDocumentLegal;
	
	/** The lst second document type representative. */
	private List<ParameterTable> lstSecondDocumentTypeRepresentative;
	
	/** The lst second countries representative. */
	private List<GeographicLocation> lstSecondCountriesRepresentative;
	
	/** The max lenght second document number legal. */
	private int maxLenghtSecondDocumentNumberLegal;
	
	/** The participant related settlement. */
	private boolean participantRelatedSettlement;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The email legal representative. */
	private String emailLegalRepresentative;
	
	/** The lst representative file history. */
	private List<RepresentativeFileHistory> lstRepresentativeFileHistory;
	
	/** The representative file name display. */
	private String representativeFileNameDisplay;
	
	/** The representative file type. */
	private Integer representativeFileType;
	
	/** The lstt holder file types. */
	private List<ParameterTable> lsttHolderFileTypes;
	
	/** The lst representative file types. */
	private List<ParameterTable> lstRepresentativeFileTypes;
	
	/** The participant file name display. */
	private String participantFileNameDisplay;
	
	/** The participant file type. */
	private Integer participantFileType;
	
	/** The lst participant file types. */
	private List<ParameterTable> lstParticipantFileTypes;
	
	/** The List currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The List bank provider. */
	private List<Bank> lstBankProvider;
	
	/** The List Bank Account Type. */
	private List<ParameterTable> lstBankAccountType;
	
	/** The lst represented entity. */
	private List<RepresentedEntity> lstRepresentedEntity;
	
	/** The lst legal representative. */
	private List<LegalRepresentative> lstLegalRepresentative;
	
	/** The legal representative history. */
	private LegalRepresentativeHistory legalRepresentativeHistory;
	
	/** The lst legal representative history. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistory;	
	
	/** The lst representative class. */
	private List<ParameterTable> lstRepresentativeClass;
	
	/** The lst document type representative. */
	private List<ParameterTable> lstDocumentTypeRepresentative;
	
	/** The lst economic activity representative. */
	private List<ParameterTable> lstEconomicActivityRepresentative;
	
	/** The lst participant roles. */
	private List<ParameterTable> lstParticipantRoles;
	
	/** The lst sex. */
	private List<ParameterTable> lstSex;
	
	/** The ind representative natural person. */
	private boolean indRepresentativeNaturalPerson;
	
	/** The ind representative juridic person. */
	private boolean indRepresentativeJuridicPerson;
	
	/** The flag cmb representative class. */
	private boolean flagCmbRepresentativeClass;
	
	/** The ind representative resident. */
	private boolean indRepresentativeResident;
	
	/** The disabled country resident representative. */
	private boolean disabledCountryResidentRepresentative;
	
	/** The ind representative is pep. */
	private boolean indRepresentativeIsPEP;
	
	/** The ind copy legal representative. */
	private boolean indCopyLegalRepresentative;
	
	/** The lst person type. */
	private List<ParameterTable> lstPersonType;
	
	/** The lst boolean. */
	private List<BooleanType> lstBoolean;    
    
    /** The lst postal provinces representative. */
    private List<GeographicLocation> lstPostalProvincesRepresentative;
    
    /** The lst postal districts representative. */
    private List<GeographicLocation> lstPostalDistrictsRepresentative;    
    
    /** The lst legal provinces representative. */
    private List<GeographicLocation> lstLegalProvincesRepresentative;
    
    /** The lst legal districts representative. */
    private List<GeographicLocation> lstLegalDistrictsRepresentative;
	
	/** The participant. */
	private Participant participant;	
    
    /** The lst international depository. */
    private List<InternationalDepository> lstInternationalDepository;        
    
    /** The lst countries. */
    private List<GeographicLocation> lstCountries;
    
    /** The lst departments. */
    private List<GeographicLocation> lstDepartments;
    
    /** The lst provinces. */
    private List<GeographicLocation> lstProvinces;
    
    /** The lst districts. */
    private List<GeographicLocation> lstDistricts;
    
    /** The lst participant class. */
    private List<ParameterTable> lstParticipantClass;

    /** The lst participant class for search. */
    private List<ParameterTable> lstParticipantClassForSearch;
    
    /** The lst participant document type. */
    private List<ParameterTable> lstParticipantDocumentType;
    
    /** The lst participant document type for search. */
    private List<ParameterTable> lstParticipantDocumentTypeForSearch;
    
    /** The lst participant type. */
    private List<ParameterTable> lstParticipantType;
    
    /** The lst participant type for search. */
    private List<ParameterTable> lstParticipantTypeForSearch;
    
    /** The lst economic sector. */
    private List<ParameterTable> lstEconomicSector;
    
    /** The lst economic activity. */
    private List<ParameterTable> lstEconomicActivity;
    
    private List<InvestorTypeModelTO> listInvestorType;
    
    /** The lst participant state. */
    private List<ParameterTable> lstParticipantState;
    
    /** The lst participant state for search. */
    private List<ParameterTable> lstParticipantStateForSearch;
    
    /** The lst participant request state. */
    private List<ParameterTable> lstParticipantRequestState;
    
    /** The lst registered participants. */
    private List<Participant> lstRegisteredParticipants;
    
    /** The participant pay commission. */
    private boolean participantPayCommission;
    
    /** The participant cavapy. */
    private boolean indParticipantCavapy;
    
    /** The participant cavapy. */
    private boolean indManagedThirdCavapy;
    
	/** The participant data model. */
    private ParticipantDataModel participantDataModel;
    
    /** The participant to. */
    private ParticipantTO participantTO;
    
    /** The ind participant selected modif. */
    private boolean indParticipantSelectedModif;
    
    /** The id participant pk. */
    private Long idParticipantPk;
    
    /** The state participant. */
    private Integer stateParticipant;    
    
    /** The participant files. */
    private List<ParticipantFile> lstParticipantFiles;
    
    /** The id represented entity pk. */
    private Long idRepresentedEntityPk;
    
    /** The represented entity. */
    private RepresentedEntity representedEntity;
    
    /** The lst holder files. */
    private List<HolderFile> lstHolderFiles;
    
    /** The option selected one radio. */
	private Integer optionSelectedOneRadio;
	
	/** lista de cui disponibles para realizar el registro */
	private List<Long> listAvailableCui;
    
    /** Document Document Size. */
    private String fUploadFileSize="16777216";//15MB en bytes;
    
    /** The size document display. */
	private String fUploadFileSizeDisplay="15000";
	
	 /** The institution bank account Session. */
	private InstitutionBankAccount institutionBankAccountSession;
	
	private InstitutionBankAccount institutionBankAccountSessionSelected;
	
	/** The institution bank account Session. */
	private List<InstitutionBankAccount> lstInstitutionBankAccountSession;
	
	/** The general parameters facade. */
    @EJB
    GeneralParametersFacade generalParametersFacade;
    
    /** The accounts facade. */
    @EJB
    AccountsFacade accountsFacade;
    
    /** The participant service facade. */
    @EJB
    ParticipantServiceFacade participantServiceFacade;
    
    /** The holder service facade. */
    @EJB
    HolderServiceFacade holderServiceFacade;
    
    @EJB
	InvestorTypeFacade investorTypeFacade;
    
    /** The user info. */
    @Inject UserInfo userInfo;
        
    /** The min date participant contract. */
    private Date minDateParticipantContract;
    
    /** The lst category pep. */
    private List<ParameterTable> lstCategoryPep;
	
	/** The lst role pep. */
	private List<ParameterTable> lstRolePep;
	
	/** The lst document type representative result. */
	private List<ParameterTable> lstDocumentTypeRepresentativeResult;
	
	/** The max lenght document number legal. */
	private int maxLenghtDocumentNumberLegal;
	
	/** The ind numeric type document legal. */
	private boolean indNumericTypeDocumentLegal;
	
	/** The lst negotiation modalities. */
	private List<NegotiationModality> lstNegotiationModalities;	
	
	/** The registered negotiation modalities. */
	private boolean registeredNegotiationModalities;
	
	/** The ind holder account bank. */
	private boolean indHolderAccountBank;
	
	/** The allow search all. */
	private boolean allowSearchAll;
	
	/** FORMA PARTE DEL PROCESO DE COMPENSACION Y LIQUIDACION. */
	private boolean particiantTypeProcessCompLiq;
	
	/** The list document source. */
	private List<ParameterTable> listDocumentSource;

	/** The disabled Mnemonic ASFI and FONDO. */
	private boolean flagSendASFI;
	/** The disabled flag Mnemonic ASFI and FONDO. */
	private boolean flagFlagSendASFI;
	/** The fund administrator (Mnemonic ASFI). */
    private String fundAdministrator;
	
	/** The typo document display. */
	private String pdfFileUploadFileTypesDisplay="*.pdf";
    
    /** The transfer number (Transfer number). */
    private String transferNumber;
    
    /** The mnemonic (Mnemonic Fondo). */
    private String mnemonic;
    
	
    
    /**
	 * Instantiates a new participant mgmt bean.
	 */
	public ParticipantMgmtBean(){
		  participant = new Participant();
		  participant.setIssuer(new Issuer());
		  participantTO = new ParticipantTO();
		  legalRepresentativeHistory = new LegalRepresentativeHistory();
		  flagFlagSendASFI = false;
	}
	
	/**
	 * Load register participant action.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadRegisterParticipantAction(){
		try {
			
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			
			createObjectBankAccountsSession();
			createObjectRegister();
			
			participant = new Participant();
			participant.setHolder(new Holder());
			participant.setRoleParticipant(ParticipantRoleType.NORMAL.getCode());
			participant.setResidenceCountry(countryResidence); //bolivia
			
			lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
			legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(new  ArrayList<LegalRepresentativeHistory>());
			
			loadCombosParticipantManagement();
			
			listParticipantFileTypes();
			
			getLstDocumentSource();
			
			loadNegotiationCombos();	
			
			changeSelectedCountry();
			
			loadCurrency();
			
			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
			
			validateBankType();
			
			loadBankAccountType();
			
			lstInstitutionBankAccountSession=new ArrayList<InstitutionBankAccount>();
			
			participantPayCommission = Boolean.FALSE;
			
			indParticipantCavapy = Boolean.FALSE;
			
			indManagedThirdCavapy = Boolean.FALSE;
			
			participantRelatedSettlement = Boolean.FALSE;
			
			minDateParticipantContract = null;
			
			lstParticipantFiles = new ArrayList<ParticipantFile>();			
			participantFileNameDisplay = null;
			
			optionSelectedOneRadio = BooleanType.NO.getCode();
			
			return "registerParticipantRule";
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	public String validateAddBankAccount(){
		hideDialogsListener(null);
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getInstitutionType()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())
				|| Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getProviderBank().getIdBankPk()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankAcountType())
				|| Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getAccountNumber()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getCurrency())) {
    		if (Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getInstitutionType()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBankType())
    				|| Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getProviderBank().getIdBankPk()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBankAcountType())
    				|| Validations.validateIsNullOrEmpty(institutionBankAccountSession.getAccountNumber()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getCurrency()))
    		{
    			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
    						, PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));	
    			   JSFUtilities.showSimpleValidationDialog();
    			   return null;
    		}
    	}
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getInstitutionType()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())
				&& Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getProviderBank().getIdBankPk()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankAcountType())
				&& Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getAccountNumber()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getCurrency())) 
		{
			if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccountSession)) {
				for(InstitutionBankAccount institutionBankAccountObj:lstInstitutionBankAccountSession) {
					if(institutionBankAccountObj.getAccountNumber().equals(institutionBankAccountSession.getAccountNumber())
							&& institutionBankAccountObj.getCurrency().equals(institutionBankAccountSession.getCurrency())
							&& institutionBankAccountObj.getProviderBank().getIdBankPk().equals(institutionBankAccountSession.getProviderBank().getIdBankPk())
							&& institutionBankAccountObj.getBankAcountType().equals(institutionBankAccountSession.getBankAcountType())) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER));	
					   JSFUtilities.showSimpleValidationDialog();
					   createObjectBankAccountsSession();
					   institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
					   institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
					   return null;
					}
				}
			}
//			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
			for (ParameterTable prmBankAcountType : lstBankAccountType) {
				if(prmBankAcountType.getParameterTablePk().equals(institutionBankAccountSession.getBankAcountType().intValue())) {
					institutionBankAccountSession.setDescriptionBankAcountType(prmBankAcountType.getDescription());
					break;
				}
			}
			for (ParameterTable prmCurrency : lstCurrency) {
				if(prmCurrency.getParameterTablePk().equals(institutionBankAccountSession.getCurrency())) {
					institutionBankAccountSession.setDescriptionCurrency(prmCurrency.getDescription());
					break;
				}
			}
			for (Bank prmBank :lstBankProvider) {
				if(prmBank.getIdBankPk().equals(institutionBankAccountSession.getProviderBank().getIdBankPk())) {
					institutionBankAccountSession.getProviderBank().setDescription(prmBank.getDescription());
					break;
				}
			}
			lstInstitutionBankAccountSession.add(institutionBankAccountSession);
			createObjectBankAccountsSession();
			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));	
		   JSFUtilities.showSimpleValidationDialog();
		   return null;
		}
		return null;
	}
	
	/**
	 * Delete cash account.
	 */
	public void deleteBankAccount(){
		hideDialogsListener(null);
		if (institutionBankAccountSessionSelected == null) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ADM_VALIDATION_BANK_ACCOUNT_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		} else {
			try {
				for (ParameterTable prmCurrency : lstCurrency) {
					if(prmCurrency.getParameterTablePk().equals(institutionBankAccountSessionSelected.getCurrency())) {
						institutionBankAccountSessionSelected.setDescriptionCurrency(prmCurrency.getDescription());
						break;
					}
				}
				Object[] argObj = new Object[2];
				argObj[0] = institutionBankAccountSessionSelected.getDescriptionCurrency();
				argObj[1] = institutionBankAccountSessionSelected.getAccountNumber();
				JSFUtilities.executeJavascriptFunction("PF('wvRemoveBank').show()");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE)
						, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_REMOVE_CONFIRM, argObj));
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}
	
	/**
	 * Delete cash account confirmed.
	 */
	public void deleteBankAccountConfirmed() {
		hideDialogsListener(null);
		List<InstitutionBankAccount> lstBankData = new ArrayList<InstitutionBankAccount>(lstInstitutionBankAccountSession);
		List<InstitutionBankAccount> lstBankActData = new ArrayList<InstitutionBankAccount>(lstInstitutionBankAccountSession);
		for (int i = 0; i < lstBankData.size(); i++) {
			if (institutionBankAccountSessionSelected != null && lstBankData.get(i).getAccountNumber().equals(institutionBankAccountSessionSelected.getAccountNumber())
					&& lstBankData.get(i).getCurrency().equals(institutionBankAccountSessionSelected.getCurrency())
					&& lstBankData.get(i).getProviderBank().getIdBankPk().equals(institutionBankAccountSessionSelected.getProviderBank().getIdBankPk())
					&& lstBankData.get(i).getBankAcountType().equals(institutionBankAccountSessionSelected.getBankAcountType())) {
				lstBankActData.remove(lstBankData.get(i));
			}
		}
		lstInstitutionBankAccountSession=lstBankActData;
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_DELETE_BANK_ACCOUNT));
		JSFUtilities.showValidationDialog();
	}
	
	/**
	 * Gets the lst document source.
	 *
	 * @return the lst document source
	 */
	public void getLstDocumentSource() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			
			
			listDocumentSource = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		participantTO = new ParticipantTO();
		participantDataModel = null;
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()) {			
			Participant objParticipant= new Participant(userInfo.getUserAccountSession().getParticipantCode()); 
			try {
				objParticipant= participantServiceFacade.findParticipantByFilters(objParticipant);
				participantTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				participantTO.setMnemonic(objParticipant.getMnemonic());
				participantTO.setDescription(objParticipant.getDescription());
				participantTO.setStateParticipant(objParticipant.getState());
				participantTO.setDocumentType(objParticipant.getDocumentType());
				participantTO.setDocumentNumber(objParticipant.getDocumentNumber());
				participantTO.setParticipantType(objParticipant.getAccountType());
				changeSelectedParticipantTypeSearch();
				participantTO.setParticipantClass(objParticipant.getAccountClass());
			} catch (ServiceException e) {
			}
		}
	}
	
	/**
	 * Search participant by filters.
	 */
	@LoggerAuditWeb
	public void searchParticipantByFilters(){
		try{
		firsRegisterDataTable = 0;
		participantTO.setSortBy("mnemonic");	
		
		List<Participant> lstParticipantResult = participantServiceFacade.getListParticipantSearch(participantTO);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantResult)){
			fillDescriptionsParticipant(lstParticipantResult);
		}
		
		participantDataModel = new ParticipantDataModel(lstParticipantResult);
		
		
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Fill descriptions participant.
	 *
	 * @param lstParticipants the lst participants
	 * @throws ServiceException the service exception
	 */
	private void fillDescriptionsParticipant(List<Participant> lstParticipants) throws ServiceException{
		
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
							
		List<ParameterTable> lstParticipantDocumentTypeTmp = loadListDocumentType();
				
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_TYPE.getCode());			
		List<ParameterTable> lstParticipantTypesTmp  = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_CLASS.getCode());			
		List<ParameterTable> lstParticipantClassesTmp  = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);	
		
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_STATE.getCode());
		List<ParameterTable> lstParticipantStatesTmp = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		
		for (Participant objParticipant : lstParticipants) {
			
			for (ParameterTable prmDocType : lstParticipantDocumentTypeTmp) {
				if(prmDocType.getParameterTablePk().equals(objParticipant.getDocumentType())) {
					objParticipant.setDocumentTypeDescription(prmDocType.getIndicator1());
					objParticipant.setFullDocument(prmDocType.getIndicator1() + GeneralConstants.DASH + objParticipant.getDocumentNumber());
					break;
				}
			}
			
			for (ParameterTable prmPartType : lstParticipantTypesTmp) {
				if(prmPartType.getParameterTablePk().equals(objParticipant.getAccountType())) {
					objParticipant.setAccountTypeDescription(prmPartType.getParameterName());
					break;
				}
			}
			
			for (ParameterTable prmPartClass : lstParticipantClassesTmp) {
				if(prmPartClass.getParameterTablePk().equals(objParticipant.getAccountClass())) {
					objParticipant.setAccountClassDescription(prmPartClass.getParameterName());
					break;
				}
			}
			
			for (ParameterTable prmStates : lstParticipantStatesTmp) {
				if(prmStates.getParameterTablePk().equals(objParticipant.getState())) {
					objParticipant.setStateDescription(prmStates.getParameterName());
					break;
				}
			}
			
		}
	}
	
	/**
	 * Load default combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCombosParticipantManagement() throws ServiceException{
		/**
		 * Load Countries
		 */
		GeographicLocationTO filterCountry = new GeographicLocationTO();
		filterCountry.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		filterCountry.setState(GeographicLocationStateType.REGISTERED.getCode());
		lstCountries = generalParametersFacade.getListGeographicLocationServiceFacade(filterCountry);
		
		/**
		 * Instance a filter for ParameterTable
		 */
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
					
		//List the document types of Participant
		lstParticipantDocumentType = loadListDocumentType();
		
		//List the Participant types
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_TYPE.getCode());			
		lstParticipantType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);										
		
		//List the Economic sectors
		filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		lstEconomicSector = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		
		//List the State of Participant
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_STATE.getCode());
		lstParticipantState = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		
		loadParticipantRoles();

	}
	
	/**
	 * Load list document type.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<ParameterTable> loadListDocumentType() throws ServiceException{
		PersonTO personTO = new PersonTO();
		personTO.setPersonType(PersonType.JURIDIC.getCode());
		personTO.setFlagIssuerIndicator(true);
		return documentValidator.getDocumentTypeResult(personTO);
		
	}
	
	/**
	 * Load combos for search.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCombosForSearch() throws ServiceException{
		
		/**
		 * Instance a filter for ParameterTable
		 */
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
							
		lstParticipantDocumentTypeForSearch = loadListDocumentType();
		
		//List the Participant types
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_TYPE.getCode());			
		lstParticipantTypeForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);										
				
		//List the State of Participant
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_STATE.getCode());
		lstParticipantStateForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		
		if(Validations.validateIsNotNullAndNotEmpty(lstParticipantStateForSearch)) {
			for(ParameterTable iteracion : lstParticipantStateForSearch) {
				if(Validations.validateIsNotNullAndNotEmpty(iteracion.getShortInteger()) && iteracion.getShortInteger() == 0) {
					Integer indicador = lstParticipantStateForSearch.indexOf(iteracion);
					lstParticipantStateForSearch.remove(indicador);
				}
			}
		}
		
		lstParticipantClassForSearch = null;		
	}	
	
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
			try {
				
				listAvailableCui=new ArrayList<>();
				
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
				
				loadCombosForSearch();
				
				createObjectBankAccountsSession();
				
				listHolidays();
				
				optionSelectedOneRadio = null;
				
				if(userInfo.getUserAccountSession().isDepositaryInstitution() || userInfo.getUserAccountSession().isIssuerInstitucion()) {
					allowSearchAll = true;
				} else {
					allowSearchAll = false;
					Participant objParticipant= new Participant(userInfo.getUserAccountSession().getParticipantCode()); 
					objParticipant= participantServiceFacade.findParticipantByFilters(objParticipant);
					participantTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
					participantTO.setMnemonic(objParticipant.getMnemonic());
					participantTO.setDescription(objParticipant.getDescription());
					participantTO.setStateParticipant(objParticipant.getState());
					participantTO.setDocumentType(objParticipant.getDocumentType());
					participantTO.setDocumentNumber(objParticipant.getDocumentNumber());
					participantTO.setParticipantType(objParticipant.getAccountType());
					changeSelectedParticipantTypeSearch();
					participantTO.setParticipantClass(objParticipant.getAccountClass());
				}
				
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	
	/**
	 * Change selected participant type search.
	 */
	@LoggerAuditWeb
	public void changeSelectedParticipantTypeSearch(){
		
		try {
			
			
			//Validate the account type of participant
			if(Validations.validateIsNull(participantTO.getParticipantType()) || participantTO.getParticipantType() == -1){
				lstParticipantClassForSearch = null;
			} else {
				//List the Participant classes
				ParameterTableTO filterParameterTable = new ParameterTableTO();
				filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
				filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_CLASS.getCode());
				filterParameterTable.setIdRelatedParameterFk(participantTO.getParticipantType());
				lstParticipantClassForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Load participant roles.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipantRoles() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_ROLE.getCode());
		lstParticipantRoles = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * Load negotiation combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadNegotiationCombos() throws ServiceException{
		//List the negotiation mechanisms
		loadNegotiationModalities();
		
		//List the International Depositaries
		InternationalDepositoryTO filterInternationalDepository = new InternationalDepositoryTO();
		filterInternationalDepository.setStateIntDepository(Integer.valueOf(InternationalDepositoryStateType.ACTIVE.getCode()));
		lstInternationalDepository = participantServiceFacade.getListInternationalDepositoryServiceFacade(filterInternationalDepository);
	}
	
	/**
	 * Load negotiation modalities.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadNegotiationModalities() throws ServiceException{
		List<NegotiationModality> lstNegotiationModalitiesAux=participantServiceFacade.getListActiveNegotiationModalities();
		lstNegotiationModalities = new ArrayList<NegotiationModality>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstNegotiationModalitiesAux)){
			for (int i = 0; i < lstNegotiationModalitiesAux.size(); i++) {
				if(lstNegotiationModalitiesAux.get(i).getIdNegotiationModalityPk().equals(NegotiationModalityType.VIEW_REPO_FIXED_INCOME.getCode()) ||
						lstNegotiationModalitiesAux.get(i).getIdNegotiationModalityPk().equals(NegotiationModalityType.VIEW_REPO_EQUITIES.getCode()) ||
						lstNegotiationModalitiesAux.get(i).getIdNegotiationModalityPk().equals(NegotiationModalityType.SECUNDARY_REPO_FIXED_INCOME.getCode()) ||
						lstNegotiationModalitiesAux.get(i).getIdNegotiationModalityPk().equals(NegotiationModalityType.SECUNDARY_REPO_EQUITIES.getCode())){
					lstNegotiationModalitiesAux.remove(i);
				}	
			}
			lstNegotiationModalities.addAll(lstNegotiationModalitiesAux);
		}
		
		for(NegotiationModality mod : lstNegotiationModalities){
			List<NegotiationMechanism> lstNegotiationMechanism = new ArrayList<NegotiationMechanism>();
			for(NegotiationMechanismType negMechanismType : NegotiationMechanismType.list) {
				lstNegotiationMechanism.add(new NegotiationMechanism(negMechanismType.getCode(),negMechanismType.getDescripcion(),false,true));
			}
			mod.setNegotiationMechanisms(lstNegotiationMechanism);
			
			for(NegotiationMechanism negMec : mod.getNegotiationMechanisms()){
				for(MechanismModality mecModality : mod.getMechanismModalities()){
					if(negMec.getIdNegotiationMechanismPk().equals( mecModality.getId().getIdNegotiationMechanismPk() )){
						negMec.setDisabled(false);
					}
				}
			}
		}
	}
	
	/**
	 * Change selected participant type.
	 */
	@LoggerAuditWeb
	public void changeSelectedParticipantType(){
		try{
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			
			if(Validations.validateIsNull(participant.getAccountType()) || !Validations.validateIsPositiveNumber(participant.getAccountType())){
				lstParticipantClass = null;
				return;
			}
			
			/** issue 198
			 * 26	FORMAN PARTE DEL PROCESO DE COMPENSACION Y LIQUIDACION
		     * 27	NO FORMAN PARTE DEL PROCESO DE COMPENSACION Y LIQUIDACION
			 */
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			if(participant.getAccountType().equals(26)) {
				particiantTypeProcessCompLiq = Boolean.TRUE;
				filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
				filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_CLASS.getCode());
				filterParameterTable.setIdRelatedParameterFk(participant.getAccountType());
			} else {
				particiantTypeProcessCompLiq = Boolean.FALSE;
				filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
				filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_CLASS.getCode());
				filterParameterTable.setIndicator4(1);
			}
			
			//List the Participant classes
			
			lstParticipantClass = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstNegotiationModalities)){
				for (NegotiationModality negotiationModality : lstNegotiationModalities) {
					for (NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()) {
						negotiationMechanism.setSelected(false);
					}
				}				
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Change selected country.
	 */
	@LoggerAuditWeb
	public void changeSelectedCountry(){
		try{
			hideDialogsListener(null);		
			
			lstDepartments = null;
			lstProvinces = null;
			lstDistricts = null;			
			participant.setAddress(null);
			participant.setDepartment(null);
			participant.setProvince(null);
			participant.setDistrict(null);
			flagFlagSendASFI = false;
			
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");							
			
			participant.setHolder(new Holder());
			
			participantFileType = null;
			lstParticipantFiles = new ArrayList<ParticipantFile>();			
			participantFileNameDisplay = null;
						
			//Validate if the country was no selected
			if(Validations.validateIsNull(participant.getResidenceCountry()) || !Validations.validateIsPositiveNumber(participant.getResidenceCountry())){
				participant.setDocumentType(null);				
			} else {
				
				GeographicLocationTO filter = new GeographicLocationTO();
				filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
				filter.setState(GeographicLocationStateType.REGISTERED.getCode());
				filter.setIdLocationReferenceFk(participant.getResidenceCountry());
				
				//List The provinces
				lstDepartments = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
								
				
				//Validate if the participant is national or foreign in order to set the document type and trade name
				if(countryResidence.equals(participant.getResidenceCountry())){
					participant.setDocumentType(DocumentType.RUC.getCode());							
				} else {
					participant.setDocumentType(DocumentType.DIO.getCode());										
				}
			}
						
				
			participant.setDocumentNumber(null);
			participant.setDescription(null);
			documentTO = documentValidator.loadMaxLenghtType(participant.getDocumentType());			
			boolean isNumeric = documentTO.isNumericTypeDocument();
			Integer maxlenght = documentTO.getMaxLenghtDocumentNumber();
			documentTO = documentValidator.validateEmissionRules(participant.getDocumentType());
			documentTO.setNumericTypeDocument(isNumeric);
			documentTO.setMaxLenghtDocumentNumber(maxlenght);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load participant location info.
	 */
	private void loadParticipantLocationInfo(){
		try{
			hideDialogsListener(null);			
			
			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setState(GeographicLocationStateType.REGISTERED.getCode());
			
			if(participant.getResidenceCountry()!=null){
				//List the Departments
				filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());			
				filter.setIdLocationReferenceFk(participant.getResidenceCountry());						
				lstDepartments = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
			}
			
			if(participant.getDepartment()!=null){
				//List The provinces
				filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());			
				filter.setIdLocationReferenceFk(participant.getDepartment());
				lstProvinces = generalParametersFacade.getListGeographicLocationServiceFacade(filter);			
			}
			
			if(participant.getProvince()!=null){
				//List the districts
				filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());			
				filter.setIdLocationReferenceFk(participant.getProvince());			
				lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);	
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change selected province.
	 */
	@LoggerAuditWeb
	public void changeSelectedProvince(){
		try{
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			
			if(participant.getProvince()!=null){
				GeographicLocationTO filter = new GeographicLocationTO();
				filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
				filter.setState(GeographicLocationStateType.REGISTERED.getCode());
				filter.setIdLocationReferenceFk(participant.getProvince());
				
				lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
			} else {
				lstDistricts = null;
			}
			
			participant.setDistrict(null);
			participant.setAddress(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change selected department.
	 */
	@LoggerAuditWeb
	public void changeSelectedDepartment(){
		try{
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			if(participant.getDepartment()!=null){
				GeographicLocationTO filter = new GeographicLocationTO();
				filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
				filter.setState(GeographicLocationStateType.REGISTERED.getCode());
				filter.setIdLocationReferenceFk(participant.getDepartment());
				
				lstProvinces = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
			} else {
				lstProvinces = null;
			}
			
			lstDistricts = null;
			participant.setProvince(null);
			participant.setDistrict(null);
			participant.setAddress(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change selected economic sector.
	 */
	@LoggerAuditWeb
	public void changeSelectedEconomicSector(){
		try{
			hideDialogsListener(null);			
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			if(Validations.validateIsNotNull(participant.getEconomicSector())){
				lstEconomicActivity = generalParametersFacade.getListEconomicActivityBySector(participant.getEconomicSector());
			} else {
				lstEconomicActivity = null;
			}
			
			participant.setEconomicActivity(null);			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void buildInvestorTypeByEconAcSelected(Integer economicActivity){
		// construyendo la lista de tipo de inversionista
		InvestorTypeModelTO it = new InvestorTypeModelTO();
		it.setEconomicActivity(economicActivity);
		try {
			listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(it);
			
			// issue 1394: sugerir el tipo de inversionista por defecto
			if(Validations.validateIsNotNull(participant)){
				participant.setInvestorType(getInvestorTypeByDefault(economicActivity));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			listInvestorType = new ArrayList<>();
		}
	}
	
	private Integer getInvestorTypeByDefault(Integer economicActivity){
		try {
			if(Validations.validateIsNotNullAndPositive(economicActivity)){
				List<ParameterTable> lstEconomicActivityWithInvetorType = investorTypeFacade.getLstEconomicActivityToRelationInvestortype();
				for(ParameterTable pt:lstEconomicActivityWithInvetorType){
					if( Validations.validateIsNotNull(pt) 
						&& Validations.validateIsNotNullAndPositive(pt.getParameterTablePk())
						&& pt.getParameterTablePk().equals(economicActivity) ){
						return pt.getShortInteger();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("::: Error al sugerir el tipo de inversionista: (error irrelevante) "+e.getMessage());
		}
		return null;
	}
	
	/**
	 * issue 611
	 * metodo para retornar los CUIs disponibles que se pueden asignar a las actividades economicas con CUIs reservados [1-999] 
	 */
	public void getListCuisAvailables() {
		listAvailableCui = new ArrayList<>();
		try {
			// verificando que la actividad economica no este nula
			if (Validations.validateIsNotNull(participant.getEconomicActivity())) {
				
				// construyendo la lista de tipo de inversionista
				buildInvestorTypeByEconAcSelected(participant.getEconomicActivity());
				
				if (participant.getEconomicActivity().equals(EconomicActivityType.AGENCIAS_BOLSA.getCode()))
					listAvailableCui = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_BAG_AGENCY, CuiRangeRevervedConstanst.FINAL_BAG_AGENCY);
				else if (participant.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()))
					listAvailableCui = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_FUNDS_ADMINISTRATION_COMPANIES,
							CuiRangeRevervedConstanst.FINAL_FUNDS_ADMINISTRATION_COMPANIES);
				else if (participant.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())
						||participant.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode())
						||participant.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode())){
					listAvailableCui = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_INVESTMENT_FUND,
							CuiRangeRevervedConstanst.FINAL_INVESTMENT_FUND);
					List<Long> listAvailableCuiAux=holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_INVESTMENT_FUND2,
							CuiRangeRevervedConstanst.FINAL_INVESTMENT_FUND2);
					
					listAvailableCui.addAll(listAvailableCuiAux);
				}
				else if (participant.getEconomicActivity().equals(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode()))
					listAvailableCui = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_PENSION_FUND_ADMINISTRATOR,
							CuiRangeRevervedConstanst.FINAL_PENSION_FUND_ADMINISTRATOR);
				else if (participant.getEconomicActivity().equals(EconomicActivityType.BANCOS.getCode()))
					listAvailableCui = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_BANK, CuiRangeRevervedConstanst.FINAL_BAK);
				else if (participant.getEconomicActivity().equals(EconomicActivityType.MUTUALES.getCode())
						|| participant.getEconomicActivity().equals(EconomicActivityType.FONDOS_FINANCIEROS_PRIVADOS.getCode())
						|| participant.getEconomicActivity().equals(EconomicActivityType.COOPERATIVAS.getCode()))
					listAvailableCui = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_MUTUALS_AND_PRIVATE_FINANCIAL_FUNDS_AND_COOPERATIVES,
							CuiRangeRevervedConstanst.FINAL_MUTUALS_AND_PRIVATE_FINANCIAL_FUNDS_AND_COOPERATIVES);
				else if (participant.getEconomicActivity().equals(EconomicActivityType.COMPANIAS_SEGURO.getCode()))
					listAvailableCui = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_INSURANCE_COMPANIES,
							CuiRangeRevervedConstanst.FINAL_INSURANCE_COMPANIES);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Change selected economic sector for detail.
	 *
	 * @throws ServiceException the service exception
	 */
	private void changeSelectedEconomicSectorForDetail() throws ServiceException{		
			if(participant.getEconomicSector()!=null){
				lstEconomicActivity = generalParametersFacade.getListEconomicActivityBySector(participant.getEconomicSector());
			}						
	}
	
	/**
	 * Validate document participant.
	 */
	@LoggerAuditWeb
	public void validateDocumentParticipant(){				
		
		try{
			hideDialogsListener(null);			
			String idComponent = "frmParticipantRegister:tabViewParticipant:txtDocumentNumber";
			if(Validations.validateIsNullOrEmpty(participant.getDocumentNumber())){
				participant.setDescription(null);
				//Clean the holder of Participant
				participant.setHolder(new Holder());
				participant.setIndHolderWillBeCreated(Boolean.TRUE);
				return;
			}
			else if(documentValidator.validateFormatDocumentNumber(participant.getDocumentType(),participant.getDocumentNumber(),idComponent))
		    {
			
			Participant participantFilterValidations = new Participant();
			participantFilterValidations.setDocumentType(participant.getDocumentType());
			participantFilterValidations.setDocumentNumber(participant.getDocumentNumber());
			participantFilterValidations.setResidenceCountry(participant.getResidenceCountry());
			//National Participant
			if(countryResidence.equals(participant.getResidenceCountry())){	
				//Validate if exists participant with entered  document type and document number
				Participant participantResult = 
						participantServiceFacade.findParticipantByFilters(participantFilterValidations);
				
				if(Validations.validateIsNotNull(participantResult)){
					StringBuilder sbBodyData = new StringBuilder();
					sbBodyData.append(DocumentType.get(participantResult.getDocumentType()).getValue());
					sbBodyData.append(GeneralConstants.HYPHEN);
					sbBodyData.append(participantResult.getDocumentNumber());
										
					Object[] arrBodyData = {sbBodyData.toString()};															
					
					showMessageOnDialog(
							PropertiesConstants.MESSAGES_ALERT,
		    				null,
		    				PropertiesConstants.ERROR_ADM_PARTICIPANT_DOC_NUM_NATIONAL_REPEATED,							
		    				arrBodyData);
		    		JSFUtilities.showSimpleValidationDialog();
		    		participant.setDocumentNumber(null);
		    		//Clean the holder of Participant
					participant.setHolder(new Holder());
					participant.setIndHolderWillBeCreated(Boolean.TRUE);
				} else {
					
					if(DocumentType.RUC.getCode().equals(participant.getDocumentType())){
								/*
								 * Validate if exists holder with document type and documentNumber
								 */
								Holder holderFilter = new Holder();
								holderFilter.setDocumentType(participant.getDocumentType());								
								holderFilter.setDocumentNumber(participant.getDocumentNumber());
								holderFilter.setDocumentSource(participant.getDocumentSource());
								
								Holder holder = accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(holderFilter);
								if(Validations.validateIsNotNull(holder)){
									
									if(HolderStateType.BLOCKED.getCode().equals(holder.getStateHolder())){
										Object[] arrBodyData = {String.valueOf(holder.getIdHolderPk())};
										
										showMessageOnDialog(
												PropertiesConstants.MESSAGES_ALERT,
							    				null,
							    				PropertiesConstants.ADM_PARTICIPANT_HOLDER_FOUND_BLOCKED,							
							    				arrBodyData);
							    		JSFUtilities.showSimpleValidationDialog();
										
										participant.setDocumentNumber(null);
										participant.setDescription(null);
										//Clean the holder of Participant
										participant.setHolder(holder);									
										participant.setIndHolderWillBeCreated(Boolean.FALSE);
									} else {
										participant.setHolder(holder);									
										participant.setIndHolderWillBeCreated(Boolean.FALSE);
									}
									//disabled flag flagsendASFI
									flagFlagSendASFI = false;
									flagSendASFI = false;
									
								} else {
									//Clean the holder of Participant
									participant.setHolder(new Holder());
									participant.setIndHolderWillBeCreated(Boolean.TRUE);
									showMessageOnDialog(
											PropertiesConstants.MESSAGES_ALERT,
						    				null,
						    				PropertiesConstants.ADM_PARTICIPANT_HOLDER_WILL_BE_CREATED,							
						    				null);
						    		JSFUtilities.showSimpleValidationDialog();
						    		
						    		//view the flag flag send asfi
						    		flagFlagSendASFI = true;
								}
								
													
					}
				}
			} else {
				validateDescriptionParticipant();
			}
		}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate description participant.
	 */
	@LoggerAuditWeb
	public void validateDescriptionParticipant(){
		try {
			//hideDialogsListener(null);
			if(isIndParticipantForeign()){
				if(Validations.validateIsNotNullAndNotEmpty(participant.getDescription())){
					
					Participant participantFilterValidations = new Participant();
					participantFilterValidations.setDocumentType(participant.getDocumentType());
					participantFilterValidations.setDocumentNumber(participant.getDocumentNumber());
					participantFilterValidations.setDescription(participant.getDescription());
					
					Participant participantResult = 
							participantServiceFacade.findParticipantByFilters(participantFilterValidations);
					
					if(Validations.validateIsNotNull(participantResult)){
						StringBuilder sbBodyData = new StringBuilder();
						sbBodyData.append(DocumentType.get(participantResult.getDocumentType()).getValue());
						sbBodyData.append(GeneralConstants.HYPHEN);
						sbBodyData.append(participantResult.getDocumentNumber());
						sbBodyData.append(GeneralConstants.HYPHEN);
						sbBodyData.append(participantResult.getDescription());
											
						Object[] arrBodyData = {sbBodyData.toString()};
		
						showMessageOnDialog(
								PropertiesConstants.MESSAGES_ALERT,
			    				null,
			    				PropertiesConstants.ERROR_ADM_PARTICIPANT_DOC_NUM_FOREIGN_REPEATED,							
			    				arrBodyData);
			    		JSFUtilities.showSimpleValidationDialog();
			    		participant.setDocumentNumber(null);
			    		participant.setDescription(null);
			    		//Clean the holder of Participant
						participant.setHolder(new Holder());
						participant.setIndHolderWillBeCreated(Boolean.TRUE);
					} else {						
						/*
						 * Validate if exists holder with document type and documentNumber
						 */
						Holder holderFilter = new Holder();
						holderFilter.setDocumentType(participant.getDocumentType());	
						holderFilter.setDocumentNumber(participant.getDocumentNumber());
						holderFilter.setFullName(participant.getDescription());
						
						Holder holder = participantServiceFacade.getHolderByDocumentTypeAndDocumentNumber(holderFilter);
						if(Validations.validateIsNotNull(holder)){
							
							if(HolderStateType.BLOCKED.getCode().equals(holder.getStateHolder())){
								Object[] arrBodyData = {String.valueOf(holder.getIdHolderPk())};
								
								showMessageOnDialog(
										PropertiesConstants.MESSAGES_ALERT,
					    				null,
					    				PropertiesConstants.ADM_PARTICIPANT_HOLDER_FOUND_BLOCKED,							
					    				arrBodyData);
					    		JSFUtilities.showSimpleValidationDialog();
								
								participant.setDocumentNumber(null);
								participant.setDescription(null);
								//Clean the holder of Participant
								participant.setHolder(new Holder());
								participant.setIndHolderWillBeCreated(Boolean.TRUE);
							} else {
								participant.setHolder(holder);
								participant.setIndHolderWillBeCreated(Boolean.FALSE);
							}						
							
						} else {
							//Clean the holder of Participant
							participant.setHolder(new Holder());
							participant.setIndHolderWillBeCreated(Boolean.TRUE);
							showMessageOnDialog(
									PropertiesConstants.MESSAGES_ALERT,
				    				null,
				    				PropertiesConstants.ADM_PARTICIPANT_HOLDER_WILL_BE_CREATED,							
				    				null);
				    		JSFUtilities.showSimpleValidationDialog();
						}
						
					}
				} else {
					//Clean the holder of Participant
					participant.setHolder(new Holder());
					participant.setIndHolderWillBeCreated(Boolean.TRUE);
				}
			}
			
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate mnemonic participant.
	 */
	@LoggerAuditWeb
	public void validateMnemonicParticipant(){
		try{
			hideDialogsListener(null);
			
			if(Validations.validateIsNotNullAndNotEmpty(participant.getMnemonic())){							
			
				Participant participantFilterValidations = new Participant();
				participantFilterValidations.setMnemonic(participant.getMnemonic());
				
				Participant participantResult = 
						participantServiceFacade.validateMnemonicParticipant(participantFilterValidations);
				
				if(Validations.validateIsNotNull(participantResult)){
					StringBuilder sbBodyData = new StringBuilder();
					sbBodyData.append(String.valueOf(participantResult.getIdParticipantPk()));
					sbBodyData.append(GeneralConstants.HYPHEN);
					sbBodyData.append(participantResult.getDescription());
										
					Object[] arrBodyData = {sbBodyData.toString()};
					
					showMessageOnDialog(
							PropertiesConstants.MESSAGES_ALERT, null,
							PropertiesConstants.ERROR_ADM_PARTICIPANT_MNEMONIC_REPEATED,
							arrBodyData);				
					
					JSFUtilities.showSimpleValidationDialog();
					participant.setMnemonic(null);
				}
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
     
    /**
     * Begin register listener.
     *
     * @return the string
     */
	@LoggerAuditWeb
    public String beginRegisterAction(){
    	
    	hideDialogsListener(null);
    	
    	//Clean validations on fields
    	JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
    	
    	if(Validations.validateIsNullOrEmpty(participant.getContractDate()) && Validations.validateIsNullOrEmpty(participant.getCreationDate())){
    		if(participant.getCreationDate().after(participant.getContractDate())){
    			
    			JSFUtilities.addContextMessage("frmParticipantRegister:tabViewParticipant:calContractDate",
        				FacesMessage.SEVERITY_ERROR,
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CREATION_CONTRACT_DATE),
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CREATION_CONTRACT_DATE));
        		
    			showMessageOnDialog(
    					PropertiesConstants.MESSAGES_ALERT,
        				null,
        				PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CREATION_CONTRACT_DATE,							
        				null);
        		JSFUtilities.showSimpleValidationDialog();
    			
    		}
    	}
    	
    	//Validate if at least one Legal Representative
    	if(Validations.validateListIsNullOrEmpty(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory())){
    		showMessageOnDialog(
    				PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_ADM_PARTICIPANT_NO_LEGAL_REPRESENTATIVE,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		
			return null;
    	}
    	
    	//Validate if at least one International Depository was selected
    	boolean existsSelectedNegotiationMechanism = false;
    	
    	for(NegotiationModality negotiationModality : lstNegotiationModalities){
			for(NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()){
				if(negotiationMechanism.isSelected()){
					existsSelectedNegotiationMechanism = Boolean.TRUE;
					break;
				}
			}
		}
    	
    	if(!existsSelectedNegotiationMechanism){
    		//Show message error when none negotiation mechanism was selected
    		showMessageOnDialog(
    				PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_ADM_PARTICIPANT_NO_NEGOTIATION_MECHANISM,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return null;
    	}
    	

		//Validations of participant files

		if(!validateAllParticipantFileLoaded()){
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,
					null,
					PropertiesConstants.ERROR_ADM_PARTICIPANT_FILE_NO_COMPLETE,							
					null);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
    	
    	
		showMessageOnDialog(
				PropertiesConstants.MESSAGES_CONFIRM,
				null,
				PropertiesConstants.LBL_CONFIRM_PARTICIPANT_REGISTER,							
				null);
		JSFUtilities.showComponent("cnfParticipantRegister");
		
		return null;
    }
	
	/**
	 * Is Institution Type Participant.
	 *
	 * @return validate Institution Type Participant
	 */
	public boolean isIndInstitutionTypeParticipant(){
		return Validations.validateIsNotNull(institutionBankAccountSession.getInstitutionType()) &&
				institutionBankAccountSession.getInstitutionType() > 0 &&
				(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institutionBankAccountSession.getInstitutionType()));
	}
	
	/**
	 * Creates the object bank accounts session.
	 */
	public void createObjectBankAccountsSession(){
		institutionBankAccountSession = new InstitutionBankAccount();
		institutionBankAccountSession.setBank(new Bank());
		institutionBankAccountSession.setProviderBank(new Bank());
		institutionBankAccountSession.setParticipant(new Participant());
		institutionBankAccountSession.setIssuer(new Issuer());
	}
	
	/**
	 * Create Object Register.
	 */
	public void createObjectRegister(){
		lstBankAccountType=(new ArrayList<ParameterTable>());
		lstCurrency=(new ArrayList<ParameterTable>());
		lstBankProvider=(new ArrayList<Bank>());
	}
	
	
	/**
	 * load Currency.
	 *
	 * @throws ServiceException  the service exception
	 */
	private void loadCurrency() throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		List<ParameterTable> lstParameterTable = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		List<ParameterTable> lstParameterTableAux = new ArrayList<ParameterTable>();
		for(ParameterTable parameterTable : lstParameterTable){
			if(parameterTable.getParameterTablePk().equals(CurrencyType.PYG.getCode()) 
					|| parameterTable.getParameterTablePk().equals(CurrencyType.USD.getCode()))
				lstParameterTableAux.add(parameterTable);
		}
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
			lstCurrency=lstParameterTableAux;
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			lstCurrency=lstParameterTableAux;
	}
	
	/**
	 * Load Bank Account Type.
	 *
	 * @throws ServiceException the Service Exception
	 */
	private void loadBankAccountType()throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		lstBankAccountType=(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Validate Bank Type.
	 */
	public void validateBankType(){
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())){
			try{
				if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
					JSFUtilities.resetComponent(":frmParticipantRegister:bank");
				lstBankProvider=(participantServiceFacade.getListBank(null,BankStateType.REGISTERED.getCode())); 
				if(institutionBankAccountSession.getBankType().equals(BankType.COMMERCIAL.getCode())) {
					if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
						institutionBankAccountSession.setProviderBank(new Bank());
					if(!getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
						institutionBankAccountSession.setProviderBank(institutionBankAccountSession.getProviderBank());
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}	
		}else{
			lstBankProvider=new ArrayList<Bank>();
		}
			
	}

    
    /**
     * Save register action.
     *
     * @param actionEvent the action event
     */
	@LoggerAuditWeb
    public void saveRegisterListener(ActionEvent actionEvent){
    	
    	try {
    		
    		hideDialogsListener(null);
    		    		
    		participant.setState(ParticipantStateType.REGISTERED.getCode());
    		participant.setIndPayCommission(participantPayCommission ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    		participant.setIndDepositary(indParticipantCavapy ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    		participant.setIndManagedThird(indManagedThirdCavapy ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    		participant.setIndSettlementIncharge(participantRelatedSettlement ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    		participant.setSocialCapital(GeneralConstants.ZERO_VALUE_DOUBLE);
//    		indicador de cobro de servicio 0: no cobra | 1: cobra
    		participant.setPaymentState(GeneralConstants.ZERO_VALUE_INT);
    		participant.setAccountsState(optionSelectedOneRadio);
    		
	    	if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantFiles)){
	    	       participant.setParticipantFiles(lstParticipantFiles);
	    	} else {
	    		participant.setParticipantFiles(null);
	    	}
	    	
	    	List<ParticipantIntDepositary> participantIntDepositaries = new ArrayList<ParticipantIntDepositary>();
	    	ParticipantIntDepositary participantIntDepositary = null;
	    	ParticipantIntDepositaryPK participantIntDepositaryPK = null;
	    	for (InternationalDepository internationalDepository : lstInternationalDepository) {
	    		if(internationalDepository.isSelected()){
	    			participantIntDepositary = new ParticipantIntDepositary();
	    			participantIntDepositary.setParticipant(participant);
	    			participantIntDepositary.setInternationalDepository(internationalDepository);
	    			participantIntDepositary.setRegistryUser(userInfo.getUserAccountSession().getUserName());
	    			participantIntDepositary.setRegistryDate(CommonsUtilities.currentDateTime());
	    			participantIntDepositary.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
	    			participantIntDepositary.setLastModifyDate(CommonsUtilities.currentDateTime());
	    			participantIntDepositary.setLastModifyApp(Integer.valueOf(0));
			    	participantIntDepositary.setLastModifyIp(JSFUtilities.getRemoteIpAddress());
			    	participantIntDepositary.setStateIntDepositary(Integer.valueOf(ParticipantIntDepositaryStateType.REGISTERED.getCode()));
			    	
			    	participantIntDepositaryPK = new ParticipantIntDepositaryPK();
			    	participantIntDepositaryPK.setIdInternationalDepositoryPk(internationalDepository.getIdInternationalDepositoryPk());
			    	participantIntDepositaryPK.setIdParticipantPk(participant.getIdParticipantPk());
			    	participantIntDepositary.setId(participantIntDepositaryPK);			    	
			    	
			    	participantIntDepositaries.add(participantIntDepositary);
	    		}
			}
	    	
	    	if(!participantIntDepositaries.isEmpty()){
	    		participant.setParticipantIntDepositaries(participantIntDepositaries);
	    	}
	    	
	    	List<ParticipantMechanism> participantMechanisms = new ArrayList<ParticipantMechanism>();
	    	ParticipantMechanism participantMechanism = null;	    		    
			for(NegotiationModality negotiationModality : lstNegotiationModalities){
				for(NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()){
					if(negotiationMechanism.isSelected()){
						participantMechanism = new ParticipantMechanism();
						participantMechanism.setMechanismModality(new MechanismModality( 
								new MechanismModalityPK(negotiationMechanism.getIdNegotiationMechanismPk(), 
								negotiationModality.getIdNegotiationModalityPk() ) ));
						participantMechanism.setParticipant(participant);
						participantMechanism.setStateParticipantMechanism(ParticipantMechanismStateType.REGISTERED.getCode());									
						participantMechanisms.add(participantMechanism);						
					}
				}
			}	    	
	    	
	    	if(!participantMechanisms.isEmpty()){
	    		participant.setParticipantMechanisms(participantMechanisms);
	    	}
	    	
	    	if(participant.isIndHolderWillBeCreated()){
	    		loadListHolderFileTypeParameter();
	    		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderFiles)){
	    			participant.getHolder().setHolderFile(lstHolderFiles);
	    		}
	    	}
	    	
	    	participant.getHolder().setMnemonic(mnemonic);
	    	participant.getHolder().setFundAdministrator(fundAdministrator);
	    	participant.getHolder().setTransferNumber(transferNumber);
	    		    	
	    	boolean resultParticipantRegister = participantServiceFacade.registryParticipantOnCascadeServiceFacade(participant,legalRepresentativeHelperBean.getLstLegalRepresentativeHistory());	    	
	    	
	    	if(resultParticipantRegister){
	    		
//	    		participantServiceFacade.createSequenceParticipantDinamic(participant.getMnemonic());
	    		
	    		//Sending notification
	    		String idInstitution=null;
				 if(institutionBankAccountSession.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					 idInstitution= participant.getIdParticipantPk().toString();
				 InstitutionBankAccount institutionBankAccount=participantServiceFacade.getInstitutionBankAccountForAccountNumberAndCurrency(institutionBankAccountSession.getInstitutionType(),idInstitution,institutionBankAccountSession.getProviderBank().getIdBankPk() ,institutionBankAccountSession.getAccountNumber() ,institutionBankAccountSession.getCurrency());
				 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount)){
					  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER_CURRENCY));	
					 JSFUtilities.showSimpleValidationDialog();
					   return;
				 }
				//validate if number account exist in the same institution
				 SearchBankAccountsParticipantTO objSearchBankAccountsTO= new SearchBankAccountsParticipantTO();
				 objSearchBankAccountsTO.setAccountNumber(institutionBankAccountSession.getAccountNumber());
				 objSearchBankAccountsTO.setInstitutionType(institutionBankAccountSession.getInstitutionType());
				 objSearchBankAccountsTO.setInstitution(idInstitution);
				 objSearchBankAccountsTO.setIdBankProviderPk(institutionBankAccountSession.getProviderBank().getIdBankPk());
				 List<InstitutionBankAccount> lstInstitutionBankAccount =participantServiceFacade.validateInstitutionCashAccount(objSearchBankAccountsTO);
				 if(Validations.validateIsNotNull(lstInstitutionBankAccount) && lstInstitutionBankAccount.size()>0){
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER));	
					 JSFUtilities.showSimpleValidationDialog();
					   return;
				 }
	    		
	    		if(Validations.validateIsNotNullAndNotEmpty(participant.getIdParticipantPk())
	    				&& Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccountSession)) {
	    			for(InstitutionBankAccount InstitutionBankAccountObj:lstInstitutionBankAccountSession) {
	    				InstitutionBankAccountObj.getParticipant().setIdParticipantPk(participant.getIdParticipantPk());
	    				InstitutionBankAccount institutionBankAccountNew = participantServiceFacade.registryBankAccountsFacade(InstitutionBankAccountObj);
	    				InstitutionCashAccount institutionCashAccountNew = participantServiceFacade.registryCashAccountFacade(institutionBankAccountNew);
	    				participantServiceFacade.registryCashAccountDetailFacade(institutionBankAccountNew,institutionCashAccountNew);
	    			}
	    		}
	    		
				BusinessProcess businessProcessNotification = new BusinessProcess();					
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.PARTICIPANT_REGISTRATION.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																participant.getIdParticipantPk(), null);
								        
    			
    			if(participant.isIndHolderWillBeCreated()) {
    				Object[] arrBodyData = {String.valueOf(participant.getIdParticipantPk()), String.valueOf(participant.getHolder().getIdHolderPk())};
					showMessageOnDialog(
							PropertiesConstants.MESSAGES_SUCCESFUL, null,
							PropertiesConstants.LBL_SUCCESS_PARTICIPANT_AND_HOLDER_REGISTER, 
							arrBodyData);
    			} else {
    				Object[] arrBodyData = {String.valueOf(participant.getIdParticipantPk())};
					showMessageOnDialog(
							PropertiesConstants.MESSAGES_SUCCESFUL, null,
							PropertiesConstants.LBL_SUCCESS_PARTICIPANT_REGISTER, 
							arrBodyData);
    			}
				cleanSearch();
				JSFUtilities.showComponent("cnfEndTransactionParticipant");

	    	}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
    	
    }
    
    /**
     * Hide dialogs listener.
     *
     * @param actionEvent the action event
     */
    public void hideDialogsListener(ActionEvent actionEvent){
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();
		
		JSFUtilities.hideComponent("cnfParticipantRegister");
		JSFUtilities.hideComponent("cnfEndTransactionParticipant");		
		JSFUtilities.hideComponent("detailRepresentativeDialog");
		
	}
    
    /**
     * Find participant information.
     *
     * @param filter the filter
     * @return the participant
     * @throws ServiceException the service exception
     */
    private Participant findParticipantInformation(Participant filter) throws ServiceException{
    	Participant participantResult = participantServiceFacade.findParticipantByFilters(filter);
    	
    	if(Validations.validateIsNotNull(participantResult)){
    		
    		participant = participantResult;
    		
    		loadCombosParticipantManagement();
    		
    		loadParticipantLocationInfo();
    		
    		loadNegotiationCombos();
    		
    		//List Participant class by participant type
    		changeSelectedParticipantType();
    		
    		//List the Economic Activities by Economic Sector of participant
    		changeSelectedEconomicSectorForDetail();
    		
    		participantPayCommission = BooleanType.YES.getCode().equals(participant.getIndPayCommission());
    		
    		indParticipantCavapy = BooleanType.YES.getCode().equals(participant.getIndDepositary());
    		
    		indManagedThirdCavapy = BooleanType.YES.getCode().equals(participant.getIndManagedThird());
    		
    		participantRelatedSettlement = BooleanType.YES.getCode().equals(participant.getIndSettlementIncharge());
    		
    		ParticipantMechanismTO participantMechanismTO = new ParticipantMechanismTO();
    		participantMechanismTO.setIdParticipantPk(participant.getIdParticipantPk());
    		participantMechanismTO.setStateParticipantMechanism(ParticipantMechanismStateType.REGISTERED.getCode());
    		
    		//List ParticipantMechanism of the selected Participant
    		List<ParticipantMechanism> participantMechanisms = 
    		participantServiceFacade.getListParticipantMechanismByParticipantServiceFacade(participantMechanismTO);    		    		    		    		
    		
    		registeredNegotiationModalities = Boolean.FALSE;
    		StringBuilder sbIdConverted=new StringBuilder();
    		for (NegotiationModality negotiationModality : lstNegotiationModalities) {
				for (NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()) {
					sbIdConverted.delete(0, sbIdConverted.length());
					sbIdConverted.append(negotiationMechanism.getIdNegotiationMechanismPk()).append("-").append(negotiationModality.getIdNegotiationModalityPk());
					for (ParticipantMechanism participantMechanism : participantMechanisms) {
						if(sbIdConverted.toString().equals(participantMechanism.getMechanismModality().getIdConverted())){
							negotiationMechanism.setSelected(Boolean.TRUE);
							registeredNegotiationModalities = Boolean.TRUE;
							break;
						}
					}
				}
			}
    		
    		ParticipantIntDepositoryTO participantIntDepositoryTO = new ParticipantIntDepositoryTO();
    		participantIntDepositoryTO.setIdParticipantPk(participant.getIdParticipantPk());
    		participantIntDepositoryTO.setStateParticipantIntDepositary(ParticipantIntDepositaryStateType.REGISTERED.getCode());
    		
    		//List ParticipantIntDepositaries of the selected Participant
    		List<ParticipantIntDepositary> participantIntDepositaries =
    				participantServiceFacade.getListParticipantIntDepoByParticipantServiceFacade(participantIntDepositoryTO);	 
    		
    		for (InternationalDepository internationalDepository : lstInternationalDepository) {
				for (ParticipantIntDepositary participantIntDepositary : participantIntDepositaries) {
					if(participantIntDepositary.getId().getIdParticipantPk().equals(participant.getIdParticipantPk())
							&& participantIntDepositary.getId().getIdInternationalDepositoryPk().equals(internationalDepository.getIdInternationalDepositoryPk())){
						internationalDepository.setSelected(Boolean.TRUE);
						break;
					}
				}
			}
    		
    		return participantResult;
    	} else {
    		return null;
    	}
    }
    
	
	/**
	 * Detail participant.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailParticipant(){
		try{
			Participant participantFilter = new Participant();
	    	participantFilter.setIdParticipantPk(idParticipantPk);
	    	participantFilter.setState(stateParticipant);
	    	lstInstitutionBankAccountSession=new ArrayList<InstitutionBankAccount>();
	    	
	    	Participant participantResult = findParticipantInformation(participantFilter);	    		    	
			
	    	
	    	if(Validations.validateIsNotNull(participantResult)) {
	    		lstParticipantFiles = participantServiceFacade.getListParticipantFilesServiceFacade(participantFilter);
	    			    		
		    	listParticipantFileTypes();	    			    			    	
	    		
	    		for (ParticipantFile participantFile : lstParticipantFiles) {
	    			for (ParameterTable fileType : lstParticipantFileTypes) {
						if(fileType.getParameterTablePk().equals(participantFile.getDocumentType())){
							participantFile.setDescription(fileType.getParameterName());
							break;
						}						
					}
	    			
				}
				createObjectBankAccountsSession();
				createObjectRegister();
				loadBankAccountType();
				loadCurrency();
				institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
				institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
				lstBankProvider=(participantServiceFacade.getListBank(institutionBankAccountSession.getBankType(),BankStateType.REGISTERED.getCode())); 
	    		lstInstitutionBankAccountSession = participantServiceFacade.getBankAccountsForId(participantFilter);
	    		if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccountSession)) {
	    			for(InstitutionBankAccount institutionBankAccountObj:lstInstitutionBankAccountSession) {
	    				for (ParameterTable prmBankAcountType : lstBankAccountType) {
							if(prmBankAcountType.getParameterTablePk().equals(institutionBankAccountObj.getBankAcountType().intValue())) {
								institutionBankAccountObj.setDescriptionBankAcountType(prmBankAcountType.getDescription());
								break;
							}
						}
						for (ParameterTable prmCurrency : lstCurrency) {
							if(prmCurrency.getParameterTablePk().equals(institutionBankAccountObj.getCurrency())) {
								institutionBankAccountObj.setDescriptionCurrency(prmCurrency.getDescription());
								break;
							}
						}
						for (Bank prmBank :lstBankProvider) {
							if(prmBank.getIdBankPk().equals(institutionBankAccountObj.getProviderBank().getIdBankPk())) {
								institutionBankAccountObj.getProviderBank().setDescription(prmBank.getDescription());
								break;
							}
						}
	    			}
	    		}
	    		lstRepresentedEntity = participantServiceFacade.getListRepresentedEntityByParticipant(participantFilter);
	    		participant = participantResult;
	    		
	    		if (Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)) {
	    			fillLegalRepresentativeHistory(lstRepresentedEntity);	
				}
	    		
	    		if(Validations.validateIsNotNull(participant.getEconomicActivity())){
	    			buildInvestorTypeByEconAcSelected(participant.getEconomicActivity());
	    		}
	    		
	    		//For load information of Legal representative details
	    		representedEntity = new RepresentedEntity();	    		
	    		representedEntity.setLegalRepresentative(new LegalRepresentative());
	    		
	    		if( Validations.validateIsNotNull(participant.getAccountsState())) {
	    			optionSelectedOneRadio = participant.getAccountsState();
	    		} else {
	    			optionSelectedOneRadio = null;
	    		}
	    		
	    		return "participantDetailRule";
	    	} else {
	    		return null;
	    	}
				
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return null;
		}
	}
	
	/**
	 * Fill legal representative history.
	 *
	 * @param lstRepresentedEntity the lst represented entity
	 * @throws ServiceException the service exception
	 */
	public void fillLegalRepresentativeHistory(List<RepresentedEntity> lstRepresentedEntity) throws ServiceException{
		lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		LegalRepresentativeHistory objLegalRepresentativeHistory;
		for (RepresentedEntity objRepresentedEntity : lstRepresentedEntity) {
			objLegalRepresentativeHistory = new LegalRepresentativeHistory();
			objLegalRepresentativeHistory.setIdRepresentativeHistoryPk(objRepresentedEntity.getLegalRepresentative().getIdLegalRepresentativePk());
			objLegalRepresentativeHistory.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
			objLegalRepresentativeHistory.setBirthDate(objRepresentedEntity.getLegalRepresentative().getBirthDate());
			objLegalRepresentativeHistory.setDocumentNumber(objRepresentedEntity.getLegalRepresentative().getDocumentNumber());
			objLegalRepresentativeHistory.setDocumentType(objRepresentedEntity.getLegalRepresentative().getDocumentType());
			objLegalRepresentativeHistory.setEconomicActivity(objRepresentedEntity.getLegalRepresentative().getEconomicActivity());
			objLegalRepresentativeHistory.setEconomicSector(objRepresentedEntity.getLegalRepresentative().getEconomicSector());
			objLegalRepresentativeHistory.setEmail(objRepresentedEntity.getLegalRepresentative().getEmail());
			objLegalRepresentativeHistory.setFaxNumber(objRepresentedEntity.getLegalRepresentative().getFaxNumber());
			objLegalRepresentativeHistory.setFirstLastName(objRepresentedEntity.getLegalRepresentative().getFirstLastName());
			objLegalRepresentativeHistory.setFullName(objRepresentedEntity.getLegalRepresentative().getFullName());
			objLegalRepresentativeHistory.setHomePhoneNumber(objRepresentedEntity.getLegalRepresentative().getHomePhoneNumber());
			objLegalRepresentativeHistory.setIndResidence(objRepresentedEntity.getLegalRepresentative().getIndResidence());
			objLegalRepresentativeHistory.setLegalAddress(objRepresentedEntity.getLegalRepresentative().getLegalAddress());
			objLegalRepresentativeHistory.setLegalDistrict(objRepresentedEntity.getLegalRepresentative().getLegalDistrict());
			objLegalRepresentativeHistory.setLegalProvince(objRepresentedEntity.getLegalRepresentative().getLegalProvince());
			objLegalRepresentativeHistory.setLegalDepartment(objRepresentedEntity.getLegalRepresentative().getLegalDepartment());
			objLegalRepresentativeHistory.setLegalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getLegalResidenceCountry());
			objLegalRepresentativeHistory.setMobileNumber(objRepresentedEntity.getLegalRepresentative().getMobileNumber());
			objLegalRepresentativeHistory.setName(objRepresentedEntity.getLegalRepresentative().getName());
			objLegalRepresentativeHistory.setNationality(objRepresentedEntity.getLegalRepresentative().getNationality());
			objLegalRepresentativeHistory.setOfficePhoneNumber(objRepresentedEntity.getLegalRepresentative().getOfficePhoneNumber());
			objLegalRepresentativeHistory.setPersonType(objRepresentedEntity.getLegalRepresentative().getPersonType());
			objLegalRepresentativeHistory.setPostalAddress(objRepresentedEntity.getLegalRepresentative().getPostalAddress());
			objLegalRepresentativeHistory.setPostalDepartment(objRepresentedEntity.getLegalRepresentative().getPostalDepartment());
			objLegalRepresentativeHistory.setPostalDistrict(objRepresentedEntity.getLegalRepresentative().getPostalDistrict());
			objLegalRepresentativeHistory.setPostalProvince(objRepresentedEntity.getLegalRepresentative().getPostalProvince());
			objLegalRepresentativeHistory.setPostalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getPostalResidenceCountry());
			objLegalRepresentativeHistory.setSecondDocumentNumber(objRepresentedEntity.getLegalRepresentative().getSecondDocumentNumber());
			objLegalRepresentativeHistory.setSecondDocumentType(objRepresentedEntity.getLegalRepresentative().getSecondDocumentType());
			objLegalRepresentativeHistory.setSecondLastName(objRepresentedEntity.getLegalRepresentative().getSecondLastName());
			objLegalRepresentativeHistory.setSecondNationality(objRepresentedEntity.getLegalRepresentative().getSecondNationality());
			objLegalRepresentativeHistory.setSex(objRepresentedEntity.getLegalRepresentative().getSex());
			objLegalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
			
			PepPerson pepPersonRepresentative = participantServiceFacade.findPepPersonByLegalRepresentativeServiceFacade(objRepresentedEntity.getLegalRepresentative());
			if(Validations.validateIsNotNull(pepPersonRepresentative)) {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.YES.getCode());
				objLegalRepresentativeHistory.setCategory(pepPersonRepresentative.getCategory());
				objLegalRepresentativeHistory.setRole(pepPersonRepresentative.getRole());
				objLegalRepresentativeHistory.setBeginningPeriod(pepPersonRepresentative.getBeginingPeriod());
				objLegalRepresentativeHistory.setEndingPeriod(pepPersonRepresentative.getEndingPeriod());
			} else {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
			}
			
			LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
			legalRepresentativeTO.setViewOperationType(ViewOperationsType.DETAIL.getCode());
			legalRepresentativeTO.setFlagIssuerOrParticipant(Boolean.TRUE);
			legalRepresentativeHelperBean.setLegalRepresentativeTO(legalRepresentativeTO);
			lstLegalRepresentativeHistory.add(objLegalRepresentativeHistory);			
		}
		legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);
	}
	
	
	/**
	 * Validate email format.
	 */
	public void validateEmailFormat(){
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponent("frmParticipantRegister:tabViewParticipant:txtEmail");
    	if(Validations.validateIsNotNullAndNotEmpty(participant.getEmail())){    		
    		if(!CommonsUtilities.matchEmail(participant.getEmail())){
    			JSFUtilities.addContextMessage("frmParticipantRegister:tabViewParticipant:txtEmail",
        				FacesMessage.SEVERITY_ERROR,
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_EMAIL_FORMAT), 
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_EMAIL_FORMAT));
    			
    			showMessageOnDialog(
    					PropertiesConstants.MESSAGES_ALERT, null,
						PropertiesConstants.ADM_PARTICIPANT_VALIDATION_EMAIL_FORMAT,
						null);				
    			//Clean the email
    			participant.setEmail(null);
				JSFUtilities.showSimpleValidationDialog();
    		}
    	}
	}
	
	/**
	 * Validate web page format.
	 */
	public void validateWebPageFormat(){
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponent("frmParticipantRegister:tabViewParticipant:txtWebPage");
    	if(Validations.validateIsNotNullAndNotEmpty(participant.getWebsite())){
    		if(!CommonsUtilities.matchWebPage(participant.getWebsite())){
    			JSFUtilities.addContextMessage("frmParticipantRegister:tabViewParticipant:txtWebPage",
        				FacesMessage.SEVERITY_ERROR,
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_WEB_PAGE_FORMAT), 
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_WEB_PAGE_FORMAT));
    			
    			showMessageOnDialog(
    					PropertiesConstants.MESSAGES_ALERT, null,
						PropertiesConstants.ADM_PARTICIPANT_VALIDATION_WEB_PAGE_FORMAT,
						null);
    			//Clean the web site
    			participant.setWebsite(null);
				JSFUtilities.showSimpleValidationDialog();
    		}
    	}
	}
	
	/**
	 * Validate address format.
	 */
	public void validateAddressFormat(){
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponent("frmParticipantRegister:tabViewParticipant:txtAddress");
    	if(Validations.validateIsNotNullAndNotEmpty(participant.getAddress())){    		
    		if(!CommonsUtilities.matchAddress(participant.getAddress())){
    			JSFUtilities.addContextMessage("frmParticipantRegister:tabViewParticipant:txtAddress",
        				FacesMessage.SEVERITY_ERROR,
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_ADDRESS_FORMAT), 
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_ADDRESS_FORMAT));
    			
    			showMessageOnDialog(
    					PropertiesConstants.MESSAGES_ALERT, null,
						PropertiesConstants.ADM_PARTICIPANT_VALIDATION_ADDRESS_FORMAT,
						null);				
				//Clean the address
    			participant.setAddress(null);
				JSFUtilities.showSimpleValidationDialog();
    		}
    	}
	}
	
	/**
	 * Validate contact email format.
	 */
	public void validateContactEmailFormat(){	
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponent("frmParticipantRegister:tabViewParticipant:txtEmailCont");
    	if(Validations.validateIsNotNullAndNotEmpty(participant.getContactEmail())){    		
    		if(!CommonsUtilities.matchEmail(participant.getContactEmail())){
    			JSFUtilities.addContextMessage("frmParticipantRegister:tabViewParticipant:txtEmailCont",
        				FacesMessage.SEVERITY_ERROR,
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL_FORMAT), 
        				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL_FORMAT));
    			
    			showMessageOnDialog(
    					PropertiesConstants.MESSAGES_ALERT, null,
						PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL_FORMAT,
						null);				
    			//Clean the contact email
    			participant.setContactEmail(null);
				JSFUtilities.showSimpleValidationDialog();
    		}
    	}
	}
	
	
	


	/**
	 * Change selected creation date.
	 *
	 * @param event the event
	 */
	public void changeSelectedCreationDate(SelectEvent event){
		minDateParticipantContract = CommonsUtilities.copyDate(participant.getCreationDate());	
		
		if(Validations.validateIsNotNull(participant.getContractDate())){
			if(participant.getCreationDate().after(participant.getContractDate())){
				participant.setContractDate(null);
			}
		}
				
	}
	
	/**
	 * Change selected participant file type.
	 */
	public void changeSelectedParticipantFileType(){
		participantFileNameDisplay = null;		
		if(Validations.validateIsNotNullAndPositive(participantFileType)){
			validateFileAlreadyLoaded();
		}							
	}
	
	/**
	 * Insert or replace participant file item.
	 *
	 * @param participantFile the participant file
	 */
	public void insertOrReplaceParticipantFileItem(ParticipantFile participantFile){
		
		boolean flag = false;
		
		int listSize = lstParticipantFiles.size();
		if(listSize>0){
			for(int i=0;i<listSize;i++)
			{
			
				if(participantFile.getDocumentType().equals(lstParticipantFiles.get(i).getDocumentType())){
					lstParticipantFiles.remove(i);
					lstParticipantFiles.add(i,participantFile);
					participantFileType = null;
					
					flag = true;
				}
			}
			if(!flag){
				lstParticipantFiles.add(participantFile);
				participantFileType = null;
			}
		}
		else{
			lstParticipantFiles.add(participantFile);
			participantFileType = null;
		}
		
	}
	
	/**
	 * Document attach participant file.
	 *
	 * @param event the event
	 */
	public void documentAttachParticipantFile(FileUploadEvent event){
		
		if(Validations.validateIsNotNullAndPositive(participantFileType)){
			
				String fDisplayName=fUploadValidateFile(event.getFile(), 
						  null,fUploadFileSize, getfUploadFileTypesImgPdfXlsTypes());
				 
				 if(fDisplayName!=null){
					 participantFileNameDisplay=fDisplayName;
				 }

				 for(int i=0;i<lstParticipantFileTypes.size();i++){
					if(lstParticipantFileTypes.get(i).getParameterTablePk().equals(participantFileType)){					
					    	ParticipantFile participantFile = new ParticipantFile();
					    	participantFile.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());
					    	participantFile.setDocumentType(lstParticipantFileTypes.get(i).getParameterTablePk());
						    participantFile.setDescription(lstParticipantFileTypes.get(i).getParameterName());

					    	participantFile.setFilename(fUploadValidateFileNameLength(event.getFile().getFileName()));
					    	participantFile.setFile(event.getFile().getContents());		    	
					    	participantFile.setStateFile(ParticipantFileStateType.REGISTERED.getCode());
					    	participantFile.setParticipant(participant);				    	

						
					    	insertOrReplaceParticipantFileItem(participantFile);
					}
				}						
			
		}
		
	}
	
	/**
	 * Validate all participant file loaded.
	 *
	 * @return true, if successful
	 */
	private boolean validateAllParticipantFileLoaded(){
		if(Validations.validateListIsNullOrEmpty(lstParticipantFiles)){
			return false;
		} else {
	    	/*Issue 1231- Para esto, se definió que solo sea necesario adjuntar un documento como obligatorio, 
			 * pero que a su vez sea posible adjuntar mas de uno.*/
			return true;
		}
			
	}
	
	/**
	 * Validate file already loaded.
	 *
	 * @return true, if successful
	 */
	private boolean validateFileAlreadyLoaded(){
		if(Validations.validateListIsNullOrEmpty(lstParticipantFiles)){
			return false;
		} else {
			for (ParticipantFile objParticipantFile : lstParticipantFiles) {
				if(participantFileType.equals(objParticipantFile.getDocumentType())){
					participantFileType = null;
					participantFileNameDisplay = null;
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_DOCUMENT_ALREADY_LOADED));
					JSFUtilities.showSimpleValidationDialog();
					return true;
				}
			}
			return false;
		}
			
	}
	
	/**
	 * List participant file national types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listParticipantFileTypes() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());
		filterParameterTable
				.setMasterTableFk(MasterTableType.PARTICIPANT_FILE_TYPE
						.getCode());
		lstParticipantFileTypes = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * Load list holder file type parameter.
	 */
	private void loadListHolderFileTypeParameter(){
		try {

		Document pdfDocument;
		ByteArrayOutputStream byteArrayOutputStream;
		HolderFile holderFile;
		StringBuilder sbFileName;
		String fileNamePrefix = PropertiesUtilities.getMessage(PropertiesConstants.AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_NAME_PREFIX);
		String fileExtension = PropertiesUtilities.getMessage(PropertiesConstants.AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_EXTENSION);
		String contentFile = PropertiesUtilities.getMessage(PropertiesConstants.AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_CONTENT);
		
		loadHolderFileTypeParameter(MasterTableType.BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_JUR.getCode());
		
		lstHolderFiles = new ArrayList<HolderFile>();
		
		for (int i = 0 ; i<lsttHolderFileTypes.size(); i++) {
			pdfDocument = new Document();

			// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
			byteArrayOutputStream = new ByteArrayOutputStream();
			
			PdfWriter.getInstance(pdfDocument, byteArrayOutputStream);

			pdfDocument.open();  
			pdfDocument.setPageSize(PageSize.A4);  
		  
			pdfDocument.add(new Paragraph(contentFile));		
			
			byteArrayOutputStream.flush();			
			byteArrayOutputStream.close();
			pdfDocument.close();
			
			sbFileName = new StringBuilder();
			sbFileName.append(fileNamePrefix);
			sbFileName.append(i+1);
			sbFileName.append(fileExtension);
			
			holderFile = new HolderFile();			
			holderFile.setFilename(sbFileName.toString());
			holderFile.setFileHolder(byteArrayOutputStream.toByteArray());			
			holderFile.setDocumentType(lsttHolderFileTypes.get(i).getParameterTablePk());
			holderFile.setDescription(lsttHolderFileTypes.get(i).getParameterName());			
			
			lstHolderFiles.add(holderFile);
			
		}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}
	
	/**
	 * Load holder file type parameter.
	 *
	 * @param masterTableTypeCode the master table type code
	 */
	private void loadHolderFileTypeParameter(Integer masterTableTypeCode){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(masterTableTypeCode);
			
			lsttHolderFileTypes = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * ValidateIssuer Method
	 */
	public void validateIssuer(){
		participant.setIssuer(new Issuer());
		//participant.getIssuer().setEconomicSector(participant.getEconomicSector());
		participant.getIssuer().setStateIssuer(IssuerStateType.REGISTERED.getCode());
	}
	
	/**
	 * Dummy Validation to ensure update
	 */
	public void validateBcbCode() {
		participant.getIdBcbCode();
	}
	
	/**
	 * Dummy Validations to ensure update
	 */
	public void validateIndDepositary() {
		isIndParticipantCavapy();
	}
	
	public void validateIndManagedThird() {
		isIndManagedThirdCavapy();
	}
	
	/**
	 * Gets the lst sector location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst sector location
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getLstSectorLocation(Integer referenceFk) throws ServiceException{

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DISTRICT
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			return generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);

	}
	
	/**
	 * Removes the participant file.
	 *
	 * @param participantFile the participant file
	 */
	public void removeParticipantFile(ParticipantFile participantFile) {
		lstParticipantFiles.remove(participantFile);
		participantFileNameDisplay = null;
		participantFileType = null;		
	}
        

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the lst countries.
	 *
	 * @return the lst countries
	 */
	public List<GeographicLocation> getLstCountries() {
		return lstCountries;
	}

	/**
	 * Sets the lst countries.
	 *
	 * @param lstCountries the new lst countries
	 */
	public void setLstCountries(List<GeographicLocation> lstCountries) {
		this.lstCountries = lstCountries;
	}

	/**
	 * Gets the lst provinces.
	 *
	 * @return the lst provinces
	 */
	public List<GeographicLocation> getLstProvinces() {
		return lstProvinces;
	}

	/**
	 * Sets the lst provinces.
	 *
	 * @param lstProvinces the new lst provinces
	 */
	public void setLstProvinces(List<GeographicLocation> lstProvinces) {
		this.lstProvinces = lstProvinces;
	}

	/**
	 * Gets the lst districts.
	 *
	 * @return the lst districts
	 */
	public List<GeographicLocation> getLstDistricts() {
		return lstDistricts;
	}

	/**
	 * Sets the lst districts.
	 *
	 * @param lstDistricts the new lst districts
	 */
	public void setLstDistricts(List<GeographicLocation> lstDistricts) {
		this.lstDistricts = lstDistricts;
	}

	/**
	 * Gets the lst participant class.
	 *
	 * @return the lst participant class
	 */
	public List<ParameterTable> getLstParticipantClass() {
		return lstParticipantClass;
	}

	/**
	 * Sets the lst participant class.
	 *
	 * @param lstParticipantClass the new lst participant class
	 */
	public void setLstParticipantClass(List<ParameterTable> lstParticipantClass) {
		this.lstParticipantClass = lstParticipantClass;
	}

	/**
	 * Gets the lst participant document type.
	 *
	 * @return the lst participant document type
	 */
	public List<ParameterTable> getLstParticipantDocumentType() {
		return lstParticipantDocumentType;
	}

	/**
	 * Sets the lst participant document type.
	 *
	 * @param lstParticipantDocumentType the new lst participant document type
	 */
	public void setLstParticipantDocumentType(
			List<ParameterTable> lstParticipantDocumentType) {
		this.lstParticipantDocumentType = lstParticipantDocumentType;
	}

	/**
	 * Gets the lst participant type.
	 *
	 * @return the lst participant type
	 */
	public List<ParameterTable> getLstParticipantType() {
		return lstParticipantType;
	}

	/**
	 * Sets the lst participant type.
	 *
	 * @param lstParticipantType the new lst participant type
	 */
	public void setLstParticipantType(List<ParameterTable> lstParticipantType) {
		this.lstParticipantType = lstParticipantType;
	}

	/**
	 * Gets the lst economic sector.
	 *
	 * @return the lst economic sector
	 */
	public List<ParameterTable> getLstEconomicSector() {
		return lstEconomicSector;
	}

	/**
	 * Sets the lst economic sector.
	 *
	 * @param lstEconomicSector the new lst economic sector
	 */
	public void setLstEconomicSector(List<ParameterTable> lstEconomicSector) {
		this.lstEconomicSector = lstEconomicSector;
	}

	/**
	 * Gets the lst economic activity.
	 *
	 * @return the lst economic activity
	 */
	public List<ParameterTable> getLstEconomicActivity() {
		return lstEconomicActivity;
	}

	/**
	 * Sets the lst economic activity.
	 *
	 * @param lstEconomicActivity the new lst economic activity
	 */
	public void setLstEconomicActivity(List<ParameterTable> lstEconomicActivity) {
		this.lstEconomicActivity = lstEconomicActivity;
	}

	/**
	 * Gets the lst international depository.
	 *
	 * @return the lst international depository
	 */
	public List<InternationalDepository> getLstInternationalDepository() {
		return lstInternationalDepository;
	}

	/**
	 * Sets the lst international depository.
	 *
	 * @param lstInternationalDepository the new lst international depository
	 */
	public void setLstInternationalDepository(
			List<InternationalDepository> lstInternationalDepository) {
		this.lstInternationalDepository = lstInternationalDepository;
	}

	/**
	 * Checks if is participant pay commission.
	 *
	 * @return true, if is participant pay commission
	 */
	public boolean isParticipantPayCommission() {
		return participantPayCommission;
	}

	/**
	 * Sets the participant pay commission.
	 *
	 * @param participantPayCommission the new participant pay commission
	 */
	public void setParticipantPayCommission(boolean participantPayCommission) {
		this.participantPayCommission = participantPayCommission;
	}

	/**
	 * Checks if is participant cavapy.
	 *
	 * @return true, if is participant cavapy
	 */
	public boolean isIndParticipantCavapy() {
		return indParticipantCavapy;
	}

	/**
	 * Sets the participant cavapy.
	 *
	 * @param indParticipantCavapy the new participant cavapy
	 */
	public void setIndParticipantCavapy(boolean indParticipantCavapy) {
		this.indParticipantCavapy = indParticipantCavapy;
	}
	
	/**
	 * Checks if is Ind Managed Third cavapy.
	 *
	 * @return true, if is Ind Managed Third cavapy
	 */
    public boolean isIndManagedThirdCavapy() {
		return indManagedThirdCavapy;
	}
    
    /**
	 * Sets the Managed Third cavapy.
	 *
	 * @param indManagedThirdCavapy the new Managed Third cavapy
	 */
	public void setIndManagedThirdCavapy(boolean indManagedThirdCavapy) {
		this.indManagedThirdCavapy = indManagedThirdCavapy;
	}

	
	/**
	 * Gets the lst participant state.
	 *
	 * @return the lst participant state
	 */
	public List<ParameterTable> getLstParticipantState() {
		return lstParticipantState;
	}

	/**
	 * Sets the lst participant state.
	 *
	 * @param lstParticipantState the new lst participant state
	 */
	public void setLstParticipantState(List<ParameterTable> lstParticipantState) {
		this.lstParticipantState = lstParticipantState;
	}

	/**
	 * Gets the participant data model.
	 *
	 * @return the participant data model
	 */
	public ParticipantDataModel getParticipantDataModel() {
		return participantDataModel;
	}

	/**
	 * Sets the participant data model.
	 *
	 * @param participantDataModel the new participant data model
	 */
	public void setParticipantDataModel(ParticipantDataModel participantDataModel) {
		this.participantDataModel = participantDataModel;
	}

	/**
	 * Gets the participant to.
	 *
	 * @return the participant to
	 */
	public ParticipantTO getParticipantTO() {
		return participantTO;
	}

	/**
	 * Sets the participant to.
	 *
	 * @param participantTO the new participant to
	 */
	public void setParticipantTO(ParticipantTO participantTO) {
		this.participantTO = participantTO;
	}

	/**
	 * Checks if is ind participant national.
	 *
	 * @return true, if is ind participant national
	 */
	public boolean isIndParticipantNational() {
		return Validations.validateIsNotNull(participant.getResidenceCountry()) && 
				countryResidence.equals(participant.getResidenceCountry());
	}
	
	/**
	 * Checks if is ind participant foreign.
	 *
	 * @return true, if is ind participant foreign
	 */
	public boolean isIndParticipantForeign() {
		return Validations.validateIsNotNull(participant.getResidenceCountry()) &&
				participant.getResidenceCountry() > 0 &&
				!(countryResidence.equals(participant.getResidenceCountry()));
	}
	
	/**
	 * Checks if is ind representative nationality national.
	 *
	 * @return true, if is ind representative nationality national
	 */
	public boolean isIndRepresentativeNationalityNational() {
		return Validations.validateIsNotNull(legalRepresentativeHistory.getNationality()) && 
				countryResidence.equals(legalRepresentativeHistory.getNationality());
	}
	
	/**
	 * Checks if is ind representative nationality foreign.
	 *
	 * @return true, if is ind representative nationality foreign
	 */
	public boolean isIndRepresentativeNationalityForeign() {
		return Validations.validateIsNotNullAndPositive(legalRepresentativeHistory.getNationality()) &&				
				!(countryResidence.equals(legalRepresentativeHistory.getNationality()));
	}

	
	/**
	 * Gets the lst participant request state.
	 *
	 * @return the lst participant request state
	 */
	public List<ParameterTable> getLstParticipantRequestState() {
		return lstParticipantRequestState;
	}

	/**
	 * Sets the lst participant request state.
	 *
	 * @param lstParticipantRequestState the new lst participant request state
	 */
	public void setLstParticipantRequestState(
			List<ParameterTable> lstParticipantRequestState) {
		this.lstParticipantRequestState = lstParticipantRequestState;
	}

	/**
	 * Gets the lst registered participants.
	 *
	 * @return the lst registered participants
	 */
	public List<Participant> getLstRegisteredParticipants() {
		return lstRegisteredParticipants;
	}

	/**
	 * Sets the lst registered participants.
	 *
	 * @param lstRegisteredParticipants the new lst registered participants
	 */
	public void setLstRegisteredParticipants(
			List<Participant> lstRegisteredParticipants) {
		this.lstRegisteredParticipants = lstRegisteredParticipants;
	}

	/**
	 * Checks if is ind participant selected modif.
	 *
	 * @return true, if is ind participant selected modif
	 */
	public boolean isIndParticipantSelectedModif() {
		return indParticipantSelectedModif;
	}

	/**
	 * Sets the ind participant selected modif.
	 *
	 * @param indParticipantSelectedModif the new ind participant selected modif
	 */
	public void setIndParticipantSelectedModif(boolean indParticipantSelectedModif) {
		this.indParticipantSelectedModif = indParticipantSelectedModif;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}		
	
	/**
	 * Clean representative.
	 */
	public void cleanRepresentative(){
		hideDialogsListener(null);
		//legalRepresentativeHistory = new legalRepresentativeHistory();
		legalRepresentativeHistory = new LegalRepresentativeHistory();
	}

	/**
	 * Gets the lst representative class.
	 *
	 * @return the lst representative class
	 */
	public List<ParameterTable> getLstRepresentativeClass() {
		return lstRepresentativeClass;
	}

	/**
	 * Sets the lst representative class.
	 *
	 * @param lstRepresentativeClass the new lst representative class
	 */
	public void setLstRepresentativeClass(
			List<ParameterTable> lstRepresentativeClass) {
		this.lstRepresentativeClass = lstRepresentativeClass;
	}

	/**
	 * Gets the lst document type representative.
	 *
	 * @return the lst document type representative
	 */
	public List<ParameterTable> getLstDocumentTypeRepresentative() {
		return lstDocumentTypeRepresentative;
	}

	/**
	 * Sets the lst document type representative.
	 *
	 * @param lstDocumentTypeRepresentative the new lst document type representative
	 */
	public void setLstDocumentTypeRepresentative(
			List<ParameterTable> lstDocumentTypeRepresentative) {
		this.lstDocumentTypeRepresentative = lstDocumentTypeRepresentative;
	}

	/**
	 * Checks if is ind representative natural person.
	 *
	 * @return true, if is ind representative natural person
	 */
	public boolean isIndRepresentativeNaturalPerson() {
		return indRepresentativeNaturalPerson;
	}

	/**
	 * Sets the ind representative natural person.
	 *
	 * @param indRepresentativeNaturalPerson the new ind representative natural person
	 */
	public void setIndRepresentativeNaturalPerson(
			boolean indRepresentativeNaturalPerson) {
		this.indRepresentativeNaturalPerson = indRepresentativeNaturalPerson;
	}

	/**
	 * Checks if is ind representative juridic person.
	 *
	 * @return true, if is ind representative juridic person
	 */
	public boolean isIndRepresentativeJuridicPerson() {
		return indRepresentativeJuridicPerson;
	}

	/**
	 * Sets the ind representative juridic person.
	 *
	 * @param indRepresentativeJuridicPerson the new ind representative juridic person
	 */
	public void setIndRepresentativeJuridicPerson(
			boolean indRepresentativeJuridicPerson) {
		this.indRepresentativeJuridicPerson = indRepresentativeJuridicPerson;
	}

	/**
	 * Checks if is flag cmb representative class.
	 *
	 * @return true, if is flag cmb representative class
	 */
	public boolean isFlagCmbRepresentativeClass() {
		return flagCmbRepresentativeClass;
	}

	/**
	 * Sets the flag cmb representative class.
	 *
	 * @param flagCmbRepresentativeClass the new flag cmb representative class
	 */
	public void setFlagCmbRepresentativeClass(boolean flagCmbRepresentativeClass) {
		this.flagCmbRepresentativeClass = flagCmbRepresentativeClass;
	}

	/**
	 * Checks if is ind representative resident.
	 *
	 * @return true, if is ind representative resident
	 */
	public boolean isIndRepresentativeResident() {
		return indRepresentativeResident;
	}

	/**
	 * Sets the ind representative resident.
	 *
	 * @param indRepresentativeResident the new ind representative resident
	 */
	public void setIndRepresentativeResident(boolean indRepresentativeResident) {
		this.indRepresentativeResident = indRepresentativeResident;
	}

	/**
	 * Gets the lst person type.
	 *
	 * @return the lst person type
	 */
	public List<ParameterTable> getLstPersonType() {
		return lstPersonType;
	}

	/**
	 * Sets the lst person type.
	 *
	 * @param lstPersonType the new lst person type
	 */
	public void setLstPersonType(List<ParameterTable> lstPersonType) {
		this.lstPersonType = lstPersonType;
	}

	/**
	 * Gets the lst boolean.
	 *
	 * @return the lst boolean
	 */
	public List<BooleanType> getLstBoolean() {
		return lstBoolean;
	}

	/**
	 * Sets the lst boolean.
	 *
	 * @param lstBoolean the new lst boolean
	 */
	public void setLstBoolean(List<BooleanType> lstBoolean) {
		this.lstBoolean = lstBoolean;
	}

	/**
	 * Gets the lst postal provinces representative.
	 *
	 * @return the lst postal provinces representative
	 */
	public List<GeographicLocation> getLstPostalProvincesRepresentative() {
		return lstPostalProvincesRepresentative;
	}

	/**
	 * Sets the lst postal provinces representative.
	 *
	 * @param lstPostalProvincesRepresentative the new lst postal provinces representative
	 */
	public void setLstPostalProvincesRepresentative(
			List<GeographicLocation> lstPostalProvincesRepresentative) {
		this.lstPostalProvincesRepresentative = lstPostalProvincesRepresentative;
	}

	/**
	 * Gets the lst postal districts representative.
	 *
	 * @return the lst postal districts representative
	 */
	public List<GeographicLocation> getLstPostalDistrictsRepresentative() {
		return lstPostalDistrictsRepresentative;
	}

	/**
	 * Sets the lst postal districts representative.
	 *
	 * @param lstPostalDistrictsRepresentative the new lst postal districts representative
	 */
	public void setLstPostalDistrictsRepresentative(
			List<GeographicLocation> lstPostalDistrictsRepresentative) {
		this.lstPostalDistrictsRepresentative = lstPostalDistrictsRepresentative;
	}

	/**
	 * Gets the lst legal provinces representative.
	 *
	 * @return the lst legal provinces representative
	 */
	public List<GeographicLocation> getLstLegalProvincesRepresentative() {
		return lstLegalProvincesRepresentative;
	}

	/**
	 * Sets the lst legal provinces representative.
	 *
	 * @param lstLegalProvincesRepresentative the new lst legal provinces representative
	 */
	public void setLstLegalProvincesRepresentative(
			List<GeographicLocation> lstLegalProvincesRepresentative) {
		this.lstLegalProvincesRepresentative = lstLegalProvincesRepresentative;
	}

	/**
	 * Gets the lst legal districts representative.
	 *
	 * @return the lst legal districts representative
	 */
	public List<GeographicLocation> getLstLegalDistrictsRepresentative() {
		return lstLegalDistrictsRepresentative;
	}

	/**
	 * Sets the lst legal districts representative.
	 *
	 * @param lstLegalDistrictsRepresentative the new lst legal districts representative
	 */
	public void setLstLegalDistrictsRepresentative(
			List<GeographicLocation> lstLegalDistrictsRepresentative) {
		this.lstLegalDistrictsRepresentative = lstLegalDistrictsRepresentative;
	}

	/**
	 * Gets the lst economic activity representative.
	 *
	 * @return the lst economic activity representative
	 */
	public List<ParameterTable> getLstEconomicActivityRepresentative() {
		return lstEconomicActivityRepresentative;
	}

	/**
	 * Sets the lst economic activity representative.
	 *
	 * @param lstEconomicActivityRepresentative the new lst economic activity representative
	 */
	public void setLstEconomicActivityRepresentative(
			List<ParameterTable> lstEconomicActivityRepresentative) {
		this.lstEconomicActivityRepresentative = lstEconomicActivityRepresentative;
	}

	/**
	 * Checks if is disabled country resident representative.
	 *
	 * @return true, if is disabled country resident representative
	 */
	public boolean isDisabledCountryResidentRepresentative() {
		return disabledCountryResidentRepresentative;
	}

	/**
	 * Sets the disabled country resident representative.
	 *
	 * @param disabledCountryResidentRepresentative the new disabled country resident representative
	 */
	public void setDisabledCountryResidentRepresentative(
			boolean disabledCountryResidentRepresentative) {
		this.disabledCountryResidentRepresentative = disabledCountryResidentRepresentative;
	}

	/**
	 * Checks if is ind representative is pep.
	 *
	 * @return true, if is ind representative is pep
	 */
	public boolean isIndRepresentativeIsPEP() {
		return indRepresentativeIsPEP;
	}

	/**
	 * Sets the ind representative is pep.
	 *
	 * @param indRepresentativeIsPEP the new ind representative is pep
	 */
	public void setIndRepresentativeIsPEP(boolean indRepresentativeIsPEP) {
		this.indRepresentativeIsPEP = indRepresentativeIsPEP;
	}

	/**
	 * Gets the lst sex.
	 *
	 * @return the lst sex
	 */
	public List<ParameterTable> getLstSex() {
		return lstSex;
	}

	/**
	 * Sets the lst sex.
	 *
	 * @param lstSex the new lst sex
	 */
	public void setLstSex(List<ParameterTable> lstSex) {
		this.lstSex = lstSex;
	}

	/**
	 * Checks if is ind copy legal representative.
	 *
	 * @return true, if is ind copy legal representative
	 */
	public boolean isIndCopyLegalRepresentative() {
		return indCopyLegalRepresentative;
	}

	/**
	 * Sets the ind copy legal representative.
	 *
	 * @param indCopyLegalRepresentative the new ind copy legal representative
	 */
	public void setIndCopyLegalRepresentative(boolean indCopyLegalRepresentative) {
		this.indCopyLegalRepresentative = indCopyLegalRepresentative;
	}

	/**
	 * Gets the state participant.
	 *
	 * @return the state participant
	 */
	public Integer getStateParticipant() {
		return stateParticipant;
	}

	/**
	 * Sets the state participant.
	 *
	 * @param stateParticipant the new state participant
	 */
	public void setStateParticipant(Integer stateParticipant) {
		this.stateParticipant = stateParticipant;
	}

	/**
	 * Gets the legal representative history.
	 *
	 * @return the legal representative history
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistory() {
		return legalRepresentativeHistory;
	}

	/**
	 * Sets the legal representative history.
	 *
	 * @param legalRepresentativeHistory the new legal representative history
	 */
	public void setLegalRepresentativeHistory(
			LegalRepresentativeHistory legalRepresentativeHistory) {
		this.legalRepresentativeHistory = legalRepresentativeHistory;
	}

	/**
	 * Gets the lst legal representative history.
	 *
	 * @return the lst legal representative history
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistory() {
		return lstLegalRepresentativeHistory;
	}

	/**
	 * Sets the lst legal representative history.
	 *
	 * @param lstLegalRepresentativeHistory the new lst legal representative history
	 */
	public void setLstLegalRepresentativeHistory(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory) {
		this.lstLegalRepresentativeHistory = lstLegalRepresentativeHistory;
	}

	/**
	 * Gets the lst participant files.
	 *
	 * @return the lst participant files
	 */
	public List<ParticipantFile> getLstParticipantFiles() {
		return lstParticipantFiles;
	}

	/**
	 * Sets the lst participant files.
	 *
	 * @param lstParticipantFiles the new lst participant files
	 */
	public void setLstParticipantFiles(List<ParticipantFile> lstParticipantFiles) {
		this.lstParticipantFiles = lstParticipantFiles;
	}

	/**
	 * Gets the lst legal representative.
	 *
	 * @return the lst legal representative
	 */
	public List<LegalRepresentative> getLstLegalRepresentative() {
		return lstLegalRepresentative;
	}

	/**
	 * Sets the lst legal representative.
	 *
	 * @param lstLegalRepresentative the new lst legal representative
	 */
	public void setLstLegalRepresentative(
			List<LegalRepresentative> lstLegalRepresentative) {
		this.lstLegalRepresentative = lstLegalRepresentative;
	}

	/**
	 * Gets the lst represented entity.
	 *
	 * @return the lst represented entity
	 */
	public List<RepresentedEntity> getLstRepresentedEntity() {
		return lstRepresentedEntity;
	}

	/**
	 * Sets the lst represented entity.
	 *
	 * @param lstRepresentedEntity the new lst represented entity
	 */
	public void setLstRepresentedEntity(List<RepresentedEntity> lstRepresentedEntity) {
		this.lstRepresentedEntity = lstRepresentedEntity;
	}

	/**
	 * Gets the lst participant roles.
	 *
	 * @return the lst participant roles
	 */
	public List<ParameterTable> getLstParticipantRoles() {
		return lstParticipantRoles;
	}

	/**
	 * Sets the lst participant roles.
	 *
	 * @param lstParticipantRoles the new lst participant roles
	 */
	public void setLstParticipantRoles(List<ParameterTable> lstParticipantRoles) {
		this.lstParticipantRoles = lstParticipantRoles;
	}

	/**
	 * Gets the lst participant class for search.
	 *
	 * @return the lst participant class for search
	 */
	public List<ParameterTable> getLstParticipantClassForSearch() {
		return lstParticipantClassForSearch;
	}

	/**
	 * Sets the lst participant class for search.
	 *
	 * @param lstParticipantClassForSearch the new lst participant class for search
	 */
	public void setLstParticipantClassForSearch(
			List<ParameterTable> lstParticipantClassForSearch) {
		this.lstParticipantClassForSearch = lstParticipantClassForSearch;
	}

	/**
	 * Gets the lst participant document type for search.
	 *
	 * @return the lst participant document type for search
	 */
	public List<ParameterTable> getLstParticipantDocumentTypeForSearch() {
		return lstParticipantDocumentTypeForSearch;
	}

	/**
	 * Sets the lst participant document type for search.
	 *
	 * @param lstParticipantDocumentTypeForSearch the new lst participant document type for search
	 */
	public void setLstParticipantDocumentTypeForSearch(
			List<ParameterTable> lstParticipantDocumentTypeForSearch) {
		this.lstParticipantDocumentTypeForSearch = lstParticipantDocumentTypeForSearch;
	}

	/**
	 * Gets the lst participant type for search.
	 *
	 * @return the lst participant type for search
	 */
	public List<ParameterTable> getLstParticipantTypeForSearch() {
		return lstParticipantTypeForSearch;
	}

	/**
	 * Sets the lst participant type for search.
	 *
	 * @param lstParticipantTypeForSearch the new lst participant type for search
	 */
	public void setLstParticipantTypeForSearch(
			List<ParameterTable> lstParticipantTypeForSearch) {
		this.lstParticipantTypeForSearch = lstParticipantTypeForSearch;
	}

	/**
	 * Gets the lst participant state for search.
	 *
	 * @return the lst participant state for search
	 */
	public List<ParameterTable> getLstParticipantStateForSearch() {
		return lstParticipantStateForSearch;
	}

	/**
	 * Sets the lst participant state for search.
	 *
	 * @param lstParticipantStateForSearch the new lst participant state for search
	 */
	public void setLstParticipantStateForSearch(
			List<ParameterTable> lstParticipantStateForSearch) {
		this.lstParticipantStateForSearch = lstParticipantStateForSearch;
	}

	/**
	 * Gets the id represented entity pk.
	 *
	 * @return the id represented entity pk
	 */
	public Long getIdRepresentedEntityPk() {
		return idRepresentedEntityPk;
	}

	/**
	 * Sets the id represented entity pk.
	 *
	 * @param idRepresentedEntityPk the new id represented entity pk
	 */
	public void setIdRepresentedEntityPk(Long idRepresentedEntityPk) {
		this.idRepresentedEntityPk = idRepresentedEntityPk;
	}

	/**
	 * Gets the represented entity.
	 *
	 * @return the represented entity
	 */
	public RepresentedEntity getRepresentedEntity() {
		return representedEntity;
	}

	/**
	 * Sets the represented entity.
	 *
	 * @param representedEntity the new represented entity
	 */
	public void setRepresentedEntity(RepresentedEntity representedEntity) {
		this.representedEntity = representedEntity;
	}

	/**
	 * Gets the min date participant contract.
	 *
	 * @return the min date participant contract
	 */
	public Date getMinDateParticipantContract() {
		return minDateParticipantContract;
	}

	/**
	 * Sets the min date participant contract.
	 *
	 * @param minDateParticipantContract the new min date participant contract
	 */
	public void setMinDateParticipantContract(Date minDateParticipantContract) {
		this.minDateParticipantContract = minDateParticipantContract;
	}

	/**
	 * Gets the lst category pep.
	 *
	 * @return the lst category pep
	 */
	public List<ParameterTable> getLstCategoryPep() {
		return lstCategoryPep;
	}

	/**
	 * Sets the lst category pep.
	 *
	 * @param lstCategoryPep the new lst category pep
	 */
	public void setLstCategoryPep(List<ParameterTable> lstCategoryPep) {
		this.lstCategoryPep = lstCategoryPep;
	}

	/**
	 * Gets the lst role pep.
	 *
	 * @return the lst role pep
	 */
	public List<ParameterTable> getLstRolePep() {
		return lstRolePep;
	}

	/**
	 * Sets the lst role pep.
	 *
	 * @param lstRolePep the new lst role pep
	 */
	public void setLstRolePep(List<ParameterTable> lstRolePep) {
		this.lstRolePep = lstRolePep;
	}

	/**
	 * Gets the lst document type representative result.
	 *
	 * @return the lst document type representative result
	 */
	public List<ParameterTable> getLstDocumentTypeRepresentativeResult() {
		return lstDocumentTypeRepresentativeResult;
	}

	/**
	 * Sets the lst document type representative result.
	 *
	 * @param lstDocumentTypeRepresentativeResult the new lst document type representative result
	 */
	public void setLstDocumentTypeRepresentativeResult(
			List<ParameterTable> lstDocumentTypeRepresentativeResult) {
		this.lstDocumentTypeRepresentativeResult = lstDocumentTypeRepresentativeResult;
	}

	/**
	 * Gets the max lenght document number legal.
	 *
	 * @return the max lenght document number legal
	 */
	public int getMaxLenghtDocumentNumberLegal() {
		return maxLenghtDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght document number legal.
	 *
	 * @param maxLenghtDocumentNumberLegal the new max lenght document number legal
	 */
	public void setMaxLenghtDocumentNumberLegal(int maxLenghtDocumentNumberLegal) {
		this.maxLenghtDocumentNumberLegal = maxLenghtDocumentNumberLegal;
	}

	/**
	 * Checks if is ind numeric type document legal.
	 *
	 * @return true, if is ind numeric type document legal
	 */
	public boolean isIndNumericTypeDocumentLegal() {
		return indNumericTypeDocumentLegal;
	}

	/**
	 * Sets the ind numeric type document legal.
	 *
	 * @param indNumericTypeDocumentLegal the new ind numeric type document legal
	 */
	public void setIndNumericTypeDocumentLegal(boolean indNumericTypeDocumentLegal) {
		this.indNumericTypeDocumentLegal = indNumericTypeDocumentLegal;
	}

	/**
	 * Gets the lst participant file types.
	 *
	 * @return the lst participant file types
	 */
	public List<ParameterTable> getLstParticipantFileTypes() {
		return lstParticipantFileTypes;
	}

	/**
	 * Sets the lst participant file types.
	 *
	 * @param lstParticipantFileTypes the new lst participant file types
	 */
	public void setLstParticipantFileTypes(
			List<ParameterTable> lstParticipantFileTypes) {
		this.lstParticipantFileTypes = lstParticipantFileTypes;
	}	
	
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	public List<Bank> getLstBankProvider() {
		return lstBankProvider;
	}

	public void setLstBankProvider(List<Bank> lstBankProvider) {
		this.lstBankProvider = lstBankProvider;
	}

	public List<ParameterTable> getLstBankAccountType() {
		return lstBankAccountType;
	}

	public void setLstBankAccountType(List<ParameterTable> lstBankAccountType) {
		this.lstBankAccountType = lstBankAccountType;
	}
	
    public InstitutionBankAccount getInstitutionBankAccountSession() {
		return institutionBankAccountSession;
	}

	public void setInstitutionBankAccountSession(InstitutionBankAccount institutionBankAccountSession) {
		this.institutionBankAccountSession = institutionBankAccountSession;
	}
	
	 public InstitutionBankAccount getInstitutionBankAccountSessionSelected() {
		return institutionBankAccountSessionSelected;
	}

	public void setInstitutionBankAccountSessionSelected(InstitutionBankAccount institutionBankAccountSessionSelected) {
		this.institutionBankAccountSessionSelected = institutionBankAccountSessionSelected;
	}
	
	public List<InstitutionBankAccount> getLstInstitutionBankAccountSession() {
		return lstInstitutionBankAccountSession;
	}

	public void setLstInstitutionBankAccountSession(List<InstitutionBankAccount> lstInstitutionBankAccountSession) {
		this.lstInstitutionBankAccountSession = lstInstitutionBankAccountSession;
	}

	/**
	 * Gets the participant file type.
	 *
	 * @return the participant file type
	 */
	public Integer getParticipantFileType() {
		return participantFileType;
	}

	/**
	 * Sets the participant file type.
	 *
	 * @param participantFileType the new participant file type
	 */
	public void setParticipantFileType(Integer participantFileType) {
		this.participantFileType = participantFileType;
	}

	/**
	 * Gets the participant file name display.
	 *
	 * @return the participant file name display
	 */
	public String getParticipantFileNameDisplay() {
		return participantFileNameDisplay;
	}

	/**
	 * Sets the participant file name display.
	 *
	 * @param participantFileNameDisplay the new participant file name display
	 */
	public void setParticipantFileNameDisplay(String participantFileNameDisplay) {
		this.participantFileNameDisplay = participantFileNameDisplay;
	}

	/**
	 * Checks if is enable select participant file.
	 *
	 * @return true, if is enable select participant file
	 */
	public boolean isEnableSelectParticipantFile() {
		return Validations.validateIsNotNullAndPositive(participantFileType);
	}

	/**
	 * Gets the lst representative file types.
	 *
	 * @return the lst representative file types
	 */
	public List<ParameterTable> getLstRepresentativeFileTypes() {
		return lstRepresentativeFileTypes;
	}

	/**
	 * Sets the lst representative file types.
	 *
	 * @param lstRepresentativeFileTypes the new lst representative file types
	 */
	public void setLstRepresentativeFileTypes(
			List<ParameterTable> lstRepresentativeFileTypes) {
		this.lstRepresentativeFileTypes = lstRepresentativeFileTypes;
	}

	/**
	 * Gets the representative file type.
	 *
	 * @return the representative file type
	 */
	public Integer getRepresentativeFileType() {
		return representativeFileType;
	}

	/**
	 * Sets the representative file type.
	 *
	 * @param representativeFileType the new representative file type
	 */
	public void setRepresentativeFileType(Integer representativeFileType) {
		this.representativeFileType = representativeFileType;
	}

	/**
	 * Gets the representative file name display.
	 *
	 * @return the representative file name display
	 */
	public String getRepresentativeFileNameDisplay() {
		return representativeFileNameDisplay;
	}

	/**
	 * Sets the representative file name display.
	 *
	 * @param representativeFileNameDisplay the new representative file name display
	 */
	public void setRepresentativeFileNameDisplay(
			String representativeFileNameDisplay) {
		this.representativeFileNameDisplay = representativeFileNameDisplay;
	}
	
	/**
	 * Checks if is enable select representative file.
	 *
	 * @return true, if is enable select representative file
	 */
	public boolean isEnableSelectRepresentativeFile() {
		return Validations.validateIsNotNullAndPositive(representativeFileType);
	}

	/**
	 * Gets the lst representative file history.
	 *
	 * @return the lst representative file history
	 */
	public List<RepresentativeFileHistory> getLstRepresentativeFileHistory() {
		return lstRepresentativeFileHistory;
	}

	/**
	 * Sets the lst representative file history.
	 *
	 * @param lstRepresentativeFileHistory the new lst representative file history
	 */
	public void setLstRepresentativeFileHistory(
			List<RepresentativeFileHistory> lstRepresentativeFileHistory) {
		this.lstRepresentativeFileHistory = lstRepresentativeFileHistory;
	}

	/**
	 * Gets the email legal representative.
	 *
	 * @return the email legal representative
	 */
	public String getEmailLegalRepresentative() {
		return emailLegalRepresentative;
	}

	/**
	 * Sets the email legal representative.
	 *
	 * @param emailLegalRepresentative the new email legal representative
	 */
	public void setEmailLegalRepresentative(String emailLegalRepresentative) {
		this.emailLegalRepresentative = emailLegalRepresentative;
	}

	/**
	 * Gets the lstt holder file types.
	 *
	 * @return the lstt holder file types
	 */
	public List<ParameterTable> getLsttHolderFileTypes() {
		return lsttHolderFileTypes;
	}

	/**
	 * Sets the lstt holder file types.
	 *
	 * @param lsttHolderFileTypes the new lstt holder file types
	 */
	public void setLsttHolderFileTypes(List<ParameterTable> lsttHolderFileTypes) {
		this.lsttHolderFileTypes = lsttHolderFileTypes;
	}

	/**
	 * Gets the lst holder files.
	 *
	 * @return the lst holder files
	 */
	public List<HolderFile> getLstHolderFiles() {
		return lstHolderFiles;
	}

	/**
	 * Sets the lst holder files.
	 *
	 * @param lstHolderFiles the new lst holder files
	 */
	public void setLstHolderFiles(List<HolderFile> lstHolderFiles) {
		this.lstHolderFiles = lstHolderFiles;
	}

	/**
	 * Gets the max lenght second document number legal.
	 *
	 * @return the max lenght second document number legal
	 */
	public int getMaxLenghtSecondDocumentNumberLegal() {
		return maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght second document number legal.
	 *
	 * @param maxLenghtSecondDocumentNumberLegal the new max lenght second document number legal
	 */
	public void setMaxLenghtSecondDocumentNumberLegal(
			int maxLenghtSecondDocumentNumberLegal) {
		this.maxLenghtSecondDocumentNumberLegal = maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Gets the lst second document type representative.
	 *
	 * @return the lst second document type representative
	 */
	public List<ParameterTable> getLstSecondDocumentTypeRepresentative() {
		return lstSecondDocumentTypeRepresentative;
	}

	/**
	 * Sets the lst second document type representative.
	 *
	 * @param lstSecondDocumentTypeRepresentative the new lst second document type representative
	 */
	public void setLstSecondDocumentTypeRepresentative(
			List<ParameterTable> lstSecondDocumentTypeRepresentative) {
		this.lstSecondDocumentTypeRepresentative = lstSecondDocumentTypeRepresentative;
	}

	/**
	 * Gets the lst second countries representative.
	 *
	 * @return the lst second countries representative
	 */
	public List<GeographicLocation> getLstSecondCountriesRepresentative() {
		return lstSecondCountriesRepresentative;
	}

	/**
	 * Sets the lst second countries representative.
	 *
	 * @param lstSecondCountriesRepresentative the new lst second countries representative
	 */
	public void setLstSecondCountriesRepresentative(
			List<GeographicLocation> lstSecondCountriesRepresentative) {
		this.lstSecondCountriesRepresentative = lstSecondCountriesRepresentative;
	}

	/**
	 * Checks if is ind numeric second type document legal.
	 *
	 * @return true, if is ind numeric second type document legal
	 */
	public boolean isIndNumericSecondTypeDocumentLegal() {
		return indNumericSecondTypeDocumentLegal;
	}
		

	/**
	 * Gets the lst negotiation modalities.
	 *
	 * @return the lst negotiation modalities
	 */
	public List<NegotiationModality> getLstNegotiationModalities() {
		return lstNegotiationModalities;
	}

	/**
	 * Sets the lst negotiation modalities.
	 *
	 * @param lstNegotiationModalities the new lst negotiation modalities
	 */
	public void setLstNegotiationModalities(
			List<NegotiationModality> lstNegotiationModalities) {
		this.lstNegotiationModalities = lstNegotiationModalities;
	}

	/**
	 * Sets the ind numeric second type document legal.
	 *
	 * @param indNumericSecondTypeDocumentLegal the new ind numeric second type document legal
	 */
	public void setIndNumericSecondTypeDocumentLegal(
			boolean indNumericSecondTypeDocumentLegal) {
		this.indNumericSecondTypeDocumentLegal = indNumericSecondTypeDocumentLegal;
	}
	
	/** lista de cui disponibles para realizar el registro */
	public List<Long> getListAvailableCui() {
		return listAvailableCui;
	}

	/** lista de cui disponibles para realizar el registro */
	public void setListAvailableCui(List<Long> listAvailableCui) {
		this.listAvailableCui = listAvailableCui;
	}

	/**
	 * Checks if is participant indirect.
	 *
	 * @return true, if is participant indirect
	 */
	public boolean isParticipantIndirect(){
		return ParticipantType.INDIRECT.getCode().equals(participant.getAccountType());
	}

	/**
	 * Checks if is registered negotiation modalities.
	 *
	 * @return true, if is registered negotiation modalities
	 */
	public boolean isRegisteredNegotiationModalities() {
		return registeredNegotiationModalities;
	}

	/**
	 * Sets the registered negotiation modalities.
	 *
	 * @param registeredNegotiationModalities the new registered negotiation modalities
	 */
	public void setRegisteredNegotiationModalities(
			boolean registeredNegotiationModalities) {
		this.registeredNegotiationModalities = registeredNegotiationModalities;
	}
	
	/**
	 * Checks if is ind holder account bank.
	 *
	 * @return true, if is ind holder account bank
	 */
	public boolean isIndHolderAccountBank() {
		return indHolderAccountBank;
	}

	/**
	 * Sets the ind holder account bank.
	 *
	 * @param indHolderAccountBank the new ind holder account bank
	 */
	public void setIndHolderAccountBank(boolean indHolderAccountBank) {
		this.indHolderAccountBank = indHolderAccountBank;
	}

	/**
	 * Checks if is allow search all.
	 *
	 * @return true, if is allow search all
	 */
	public boolean isAllowSearchAll() {
		return allowSearchAll;
	}

	/**
	 * Sets the allow search all.
	 *
	 * @param allowSearchAll the new allow search all
	 */
	public void setAllowSearchAll(boolean allowSearchAll) {
		this.allowSearchAll = allowSearchAll;
	}
	
	/**
	 * 
	 */
	public boolean isParticiantTypeProcessCompLiq() {
		return particiantTypeProcessCompLiq;
	}

	/**
	 * 
	 */
	public void setParticiantTypeProcessCompLiq(boolean particiantTypeProcessCompLiq) {
		this.particiantTypeProcessCompLiq = particiantTypeProcessCompLiq;
	}
	
	/**
	 * Gets the lst departments.
	 *
	 * @return the lst departments
	 */
	public List<GeographicLocation> getLstDepartments() {
		return lstDepartments;
	}

	/**
	 * Sets the lst departments.
	 *
	 * @param lstDepartments the new lst departments
	 */
	public void setLstDepartments(List<GeographicLocation> lstDepartments) {
		this.lstDepartments = lstDepartments;
	}

	/**
	 * Checks if is participant related settlement.
	 *
	 * @return true, if is participant related settlement
	 */
	public boolean isParticipantRelatedSettlement() {
		return participantRelatedSettlement;
	}

	/**
	 * Sets the participant related settlement.
	 *
	 * @param participantRelatedSettlement the new participant related settlement
	 */
	public void setParticipantRelatedSettlement(boolean participantRelatedSettlement) {
		this.participantRelatedSettlement = participantRelatedSettlement;
	}

	/**
	 * Gets the document to.
	 *
	 * @return the document to
	 */
	public DocumentTO getDocumentTO() {
		return documentTO;
	}

	/**
	 * Sets the document to.
	 *
	 * @param documentTO the new document to
	 */
	public void setDocumentTO(DocumentTO documentTO) {
		this.documentTO = documentTO;
	}

	/**
	 * Gets the list document source.
	 *
	 * @return the list document source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list document source.
	 *
	 * @param listDocumentSource the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}

	/**  */
	public Integer getOptionSelectedOneRadio() {
		return optionSelectedOneRadio;
	}

	public void setOptionSelectedOneRadio(Integer optionSelectedOneRadio) {
		this.optionSelectedOneRadio = optionSelectedOneRadio;
	}
	
	public boolean isFlagSendASFI() {
		return flagSendASFI;
	}

	public void setFlagSendASFI(boolean flagSendASFI) {
		this.flagSendASFI = flagSendASFI;
	}

	public boolean isFlagFlagSendASFI() {
		return flagFlagSendASFI;
	}

	public void setFlagFlagSendASFI(boolean flagFlagSendASFI) {
		this.flagFlagSendASFI = flagFlagSendASFI;
	}

	public String getFundAdministrator() {
		return fundAdministrator;
	}

	public void setFundAdministrator(String fundAdministrator) {
		this.fundAdministrator = fundAdministrator;
	}

	public String getTransferNumber() {
		return transferNumber;
	}

	public void setTransferNumber(String transferNumber) {
		this.transferNumber = transferNumber;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	

	public String getfUploadFileSize() {
		return fUploadFileSize;
	}

	public void setfUploadFileSize(String fUploadFileSize) {
		this.fUploadFileSize = fUploadFileSize;
	}

	public String getfUploadFileSizeDisplay() {
		return fUploadFileSizeDisplay;
	}

	public void setfUploadFileSizeDisplay(String fUploadFileSizeDisplay) {
		this.fUploadFileSizeDisplay = fUploadFileSizeDisplay;
	}

	public List<InvestorTypeModelTO> getListInvestorType() {
		return listInvestorType;
	}

	public void setListInvestorType(List<InvestorTypeModelTO> listInvestorType) {
		this.listInvestorType = listInvestorType;
	}

}
