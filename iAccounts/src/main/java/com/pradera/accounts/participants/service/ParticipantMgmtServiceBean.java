package com.pradera.accounts.participants.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.accounts.participants.to.ParticipantHistoryStateTO;
import com.pradera.accounts.participants.to.ParticipantIntDepositoryTO;
import com.pradera.accounts.participants.to.ParticipantMechanismTO;
import com.pradera.accounts.participants.to.ParticipantRequestTO;
import com.pradera.accounts.participants.to.ParticipantTO;
import com.pradera.accounts.participants.to.SearchBankAccountsParticipantTO;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.InternationalDepositoryTO;
import com.pradera.core.component.accounts.to.NegotiationMechanismTO;
import com.pradera.core.component.accounts.to.RepresentedEntityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantBankAccount;
import com.pradera.model.accounts.ParticipantFile;
import com.pradera.model.accounts.ParticipantFileHistory;
import com.pradera.model.accounts.ParticipantHistoryState;
import com.pradera.model.accounts.ParticipantIntDepoHistory;
import com.pradera.model.accounts.ParticipantIntDepositary;
import com.pradera.model.accounts.ParticipantMechanism;
import com.pradera.model.accounts.ParticipantMechanismHistory;
import com.pradera.model.accounts.ParticipantRequest;
import com.pradera.model.accounts.ParticipantRequestHistory;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.ParticipantBankAccountStateType;
import com.pradera.model.accounts.type.ParticipantFileStateType;
import com.pradera.model.accounts.type.ParticipantIntDepositaryStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantRequestStateType;
import com.pradera.model.accounts.type.ParticipantRequestType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.accounts.type.RequestParticipantFileType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionBankAccountHistory;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationModalityStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantMgmtServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/02/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ParticipantMgmtServiceBean extends CrudDaoServiceBean{	
	
	/**
	 * Instantiates a new participant mgmt service bean.
	 */
	public ParticipantMgmtServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Find participant by filters service bean.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantByFiltersServiceBean(Participant filter) throws ServiceException{
		try {
			CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
			CriteriaQuery<Participant> criteriaQuery=criteriaBuilder.createQuery(Participant.class);
			Root<Participant> participantRoot=criteriaQuery.from(Participant.class);
			List<Predicate> criteriaParameters=new ArrayList<Predicate>();
			
			criteriaQuery.select(participantRoot);
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdParticipantPk()) &&
					Validations.validateIsPositiveNumber(filter.getIdParticipantPk())){
				ParameterExpression<Long> param = criteriaBuilder.parameter(Long.class,"idPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("idParticipantPk"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getState()) &&
					Validations.validateIsPositiveNumber(filter.getState())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"statePrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("state"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentType()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentType())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("documentType"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentNumber() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"documentNumberPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("documentNumber"), param));	
			}
			
			if(filter.getDocumentType()!=null && filter.getDocumentType().equals(DocumentType.DIO.getCode())){
				  if(filter.getResidenceCountry()!=null){
						ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"residenceCountryPrm");
						criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("residenceCountry"), param));	
				  }
			  }
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDescription() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"descriptionPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("description"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getMnemonic() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("mnemonic"), param));	
			}
			
			if(!criteriaParameters.isEmpty()){
				if (criteriaParameters.size() == 1) {
					criteriaQuery.where(criteriaParameters.get(0));
			    } else {
			    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			    }
			}
			
			TypedQuery<Participant> participantCriteria = em.createQuery(criteriaQuery);
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdParticipantPk()) &&
					Validations.validateIsPositiveNumber(filter.getIdParticipantPk())){
				participantCriteria.setParameter("idPrm", filter.getIdParticipantPk());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getState()) &&
					Validations.validateIsPositiveNumber(filter.getState())){
				participantCriteria.setParameter("statePrm", filter.getState());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentType()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentType())){
				participantCriteria.setParameter("documentTypePrm", filter.getDocumentType());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
				participantCriteria.setParameter("documentNumberPrm", filter.getDocumentNumber());
			}
			
			if(filter.getDocumentType()!=null && filter.getDocumentType().equals(DocumentType.DIO.getCode())){
				if(Validations.validateIsNotNullAndNotEmpty(filter.getResidenceCountry())){
					participantCriteria.setParameter("residenceCountryPrm", filter.getResidenceCountry());
				}
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
				participantCriteria.setParameter("descriptionPrm", filter.getDescription());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
				participantCriteria.setParameter("mnemonicPrm", filter.getMnemonic());
			}
			
			return (Participant)participantCriteria.getSingleResult();
		
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Get List Bank.
	 *
	 * @param idTypeBank type bank
	 * @param idState the id state
	 * @return List bank
	 * @throws ServiceException the Service Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Bank> getListBank(Integer idTypeBank,Integer idState)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM Bank cs	where 1=1 ");
			if(Validations.validateIsNotNull(idTypeBank))
				sbQuery.append(" and cs.bankType = :bankType ");
			if(Validations.validateIsNotNull(idState))
				sbQuery.append(" AND cs.state = :state");
			sbQuery.append(" order by cs.description ");
			Query query = em.createQuery(sbQuery.toString()); 
			if(Validations.validateIsNotNull(idTypeBank))
				query.setParameter("bankType",  idTypeBank);
			if(Validations.validateIsNotNull(idState))
				query.setParameter("state",  BankStateType.REGISTERED.getCode());
			
			
			List<Bank> bank = (List<Bank>)query.getResultList();			
			return bank;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Find participant by filters.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantByFilters(Participant filter) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder ();
			sbQuery.append(" Select P From Participant P ");
			sbQuery.append(" Left Join Fetch P.issuer I ");
			sbQuery.append(" Left Join Fetch P.holder H ");
			sbQuery.append(" Where P.idParticipantPk != null ");
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdParticipantPk()) && Validations.validateIsPositiveNumber(filter.getIdParticipantPk())){
				sbQuery.append(" and P.idParticipantPk  = :idPrm ");
			}
			if(Validations.validateIsNotNullAndNotEmpty( filter.getState()) && Validations.validateIsPositiveNumber(filter.getState())){
				sbQuery.append(" and P.state  = :statePrm ");
			}
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentType()) && Validations.validateIsPositiveNumber(filter.getDocumentType())){
				sbQuery.append(" and P.documentType  = :documentTypePrm ");
			}
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentNumber() )){
				sbQuery.append(" and P.documentNumber  = :documentNumberPrm ");
			}
			if(filter.getDocumentType()!=null && filter.getDocumentType().equals(DocumentType.DIO.getCode())){
				sbQuery.append(" and P.residenceCountry  = :residenceCountryPrm ");
			}
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDescription() )){
				sbQuery.append(" and P.description  = :descriptionPrm ");
			}
			if(Validations.validateIsNotNullAndNotEmpty( filter.getMnemonic() )){
				sbQuery.append(" and P.mnemonic = :mnemonicPrm ");
			}
			Query query = em.createQuery(sbQuery.toString());
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdParticipantPk()) && Validations.validateIsPositiveNumber(filter.getIdParticipantPk())){
				query.setParameter("idPrm", filter.getIdParticipantPk());
			}
			if(Validations.validateIsNotNullAndNotEmpty( filter.getState()) && Validations.validateIsPositiveNumber(filter.getState())){
				query.setParameter("statePrm", filter.getState());
			}
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentType()) && Validations.validateIsPositiveNumber(filter.getDocumentType())){
				query.setParameter("documentTypePrm", filter.getDocumentType());
			}
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
				query.setParameter("documentNumberPrm", filter.getDocumentNumber());
			}
			if(filter.getDocumentType()!=null && filter.getDocumentType().equals(DocumentType.DIO.getCode())){
				if(Validations.validateIsNotNullAndNotEmpty(filter.getResidenceCountry())){
					query.setParameter("residenceCountryPrm", filter.getResidenceCountry());
				}
			}
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
				query.setParameter("descriptionPrm", filter.getDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
				query.setParameter("mnemonicPrm", filter.getMnemonic());
			}
			return (Participant) query.getSingleResult();
		
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Validate mnemonic participant service bean.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant validateMnemonicParticipantServiceBean(Participant filter) throws ServiceException{

			CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
			CriteriaQuery<Participant> criteriaQuery=criteriaBuilder.createQuery(Participant.class);
			Root<Participant> participantRoot=criteriaQuery.from(Participant.class);
			List<Predicate> criteriaParameters=new ArrayList<Predicate>();
			
			criteriaQuery.select(participantRoot);
			
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
			criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("mnemonic"), param));
			
			//Validate the mnemonic is not of the same participant
			if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
				ParameterExpression<Long> param2 = criteriaBuilder.parameter(Long.class,"idPrm");
				criteriaParameters.add(criteriaBuilder.notEqual(participantRoot.get("idParticipantPk"), param2));			
			}
			
			
			if(!criteriaParameters.isEmpty()){
				if (criteriaParameters.size() == 1) {
					criteriaQuery.where(criteriaParameters.get(0));
			    } else {
			    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			    }
			}
			
			TypedQuery<Participant> participantCriteria = em.createQuery(criteriaQuery);
			
			participantCriteria.setParameter("mnemonicPrm", filter.getMnemonic());
			
			if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
				participantCriteria.setParameter("idPrm", filter.getIdParticipantPk());
			}			
			
			List<Participant> listResult = participantCriteria.getResultList();
			
			if(Validations.validateListIsNotNullAndNotEmpty(listResult)){
				return listResult.get(0);
			} else {
				return null;
			}			
	
	}
	
	/**
	 * Validate document participant.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant validateDocumentParticipant(Participant filter) throws ServiceException{

		CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
		CriteriaQuery<Participant> criteriaQuery=criteriaBuilder.createQuery(Participant.class);
		Root<Participant> participantRoot=criteriaQuery.from(Participant.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		criteriaQuery.select(participantRoot);
		
		ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
		criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("documentType"), param));
		
		ParameterExpression<String> param2 = criteriaBuilder.parameter(String.class,"documentNumberPrm");
		criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("documentNumber"), param2));
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
			ParameterExpression<String> param3 = criteriaBuilder.parameter(String.class,"descriptionPrm");
			criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("description"), param3));
		}
		
		//Validate the document and description is not of the same participant
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			ParameterExpression<Long> param4 = criteriaBuilder.parameter(Long.class,"idPrm");
			criteriaParameters.add(criteriaBuilder.notEqual(participantRoot.get("idParticipantPk"), param4));			
		}
		
		
		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
				criteriaQuery.where(criteriaParameters.get(0));
		    } else {
		    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		
		TypedQuery<Participant> participantCriteria = em.createQuery(criteriaQuery);
		
		participantCriteria.setParameter("documentTypePrm", filter.getDocumentType());
		participantCriteria.setParameter("documentNumberPrm", filter.getDocumentNumber());
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
			participantCriteria.setParameter("descriptionPrm", filter.getDescription());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			participantCriteria.setParameter("idPrm", filter.getIdParticipantPk());
		}			
		
		List<Participant> listResult = participantCriteria.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(listResult)){
			return listResult.get(0);
		} else {
			return null;
		}			

}
	
	/**
	 * Gets the list participant by filters service bean.
	 *
	 * @param filter the filter
	 * @return the list participant by filters service bean
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getListParticipantByFiltersServiceBean(ParticipantTO filter) throws ServiceException{
			CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
			CriteriaQuery<Participant> criteriaQuery=criteriaBuilder.createQuery(Participant.class);
			Root<Participant> participantRoot=criteriaQuery.from(Participant.class);
			List<Predicate> criteriaParameters=new ArrayList<Predicate>();
			
			criteriaQuery.select(participantRoot);
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipantPk())){
				ParameterExpression<Long> param = criteriaBuilder.parameter(Long.class,"idParticipantPkPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("idParticipantPk"), param));
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getStateParticipant()) && Validations.validateIsPositiveNumber(filter.getStateParticipant())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"statePrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("state"), param));				
			}
			
			if(Validations.validateIsNotNull(filter.getLstParticipantStates()) && Validations.validateIsPositiveNumber(filter.getLstParticipantStates().size())){
				ParameterExpression<List> param = criteriaBuilder.parameter(List.class,"lstStatePrm");
				criteriaParameters.add(participantRoot.get("state").in(param));				
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentType()) && Validations.validateIsPositiveNumber(filter.getDocumentType())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("documentType"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"documentNumberPrm");
				criteriaParameters.add(criteriaBuilder.like(participantRoot.get("documentNumber"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"descriptionPrm");
				criteriaParameters.add(criteriaBuilder.like(participantRoot.<String>get("description"), param));					
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
				criteriaParameters.add(criteriaBuilder.like(participantRoot.<String>get("mnemonic"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantType()) && Validations.validateIsPositiveNumber(filter.getParticipantType())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"accountTypePrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("accountType"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantClass()) && Validations.validateIsPositiveNumber(filter.getParticipantClass())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"accountClassPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("accountClass"), param));	
			}
			
			//Sort By
			if(Validations.validateIsNotNullAndNotEmpty(filter.getSortBy())){
				criteriaQuery.orderBy(criteriaBuilder.asc(participantRoot.get(filter.getSortBy())));
			}
			
			if(!criteriaParameters.isEmpty()){
				if (criteriaParameters.size() == 1) {
					criteriaQuery.where(criteriaParameters.get(0));
			    } else {
			    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			    }
			}
			
			TypedQuery<Participant> participantCriteria = em.createQuery(criteriaQuery);
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipantPk())){
				participantCriteria.setParameter("idParticipantPkPrm", filter.getIdParticipantPk());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getStateParticipant()) && Validations.validateIsPositiveNumber(filter.getStateParticipant())){
				participantCriteria.setParameter("statePrm", filter.getStateParticipant());
			}
			
			if(Validations.validateIsNotNull(filter.getLstParticipantStates()) && Validations.validateIsPositiveNumber(filter.getLstParticipantStates().size())){
				participantCriteria.setParameter("lstStatePrm", filter.getLstParticipantStates());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentType()) && Validations.validateIsPositiveNumber(filter.getDocumentType())){
				participantCriteria.setParameter("documentTypePrm", filter.getDocumentType());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
				participantCriteria.setParameter("documentNumberPrm", "%"+filter.getDocumentNumber()+"%");
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
				if(filter.getDescription().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
					participantCriteria.setParameter("descriptionPrm", filter.getDescription().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
				} else {
					participantCriteria.setParameter("descriptionPrm", "%"+filter.getDescription()+"%");
				}
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
				if(filter.getMnemonic().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
					participantCriteria.setParameter("mnemonicPrm", filter.getMnemonic().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
				} else {
					participantCriteria.setParameter("mnemonicPrm", "%"+filter.getMnemonic()+"%");
				}
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantType()) && Validations.validateIsPositiveNumber(filter.getParticipantType())){
				participantCriteria.setParameter("accountTypePrm", filter.getParticipantType());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantClass()) && Validations.validateIsPositiveNumber(filter.getParticipantClass())){
				participantCriteria.setParameter("accountClassPrm", filter.getParticipantClass());
			}
			
			return (List<Participant>)participantCriteria.getResultList();				
	}
	
	/**
	 * Gets the list participant mechanism by participant service bean.
	 *
	 * @param filter the filter
	 * @return the list participant mechanism by participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantMechanism> getListParticipantMechanismByParticipantServiceBean(ParticipantMechanismTO filter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder("Select PM From ParticipantMechanism PM ");
		sbQuery.append(" Join Fetch PM.mechanismModality MechMod ");
		sbQuery.append(" Where PM.participant.idParticipantPk = :idParticipantPrm ");
		sbQuery.append(" And PM.stateParticipantMechanism = :stateParticipantMechanismPrm ");
		
		TypedQuery<ParticipantMechanism> query = em.createQuery(sbQuery.toString(), ParticipantMechanism.class);
		query.setParameter("idParticipantPrm", filter.getIdParticipantPk());
		query.setParameter("stateParticipantMechanismPrm", ParticipantMechanismStateType.REGISTERED.getCode());
		
		return (List<ParticipantMechanism>) query.getResultList();
	}
	
	/**
	 * Gets the list participant int depo by participant service bean.
	 *
	 * @param filter the filter
	 * @return the list participant int depo by participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantIntDepositary> getListParticipantIntDepoByParticipantServiceBean(ParticipantIntDepositoryTO filter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder("Select PIntD From ParticipantIntDepositary PIntD ");
		sbQuery.append(" Join Fetch  PIntD.internationalDepository IntDep ");
		sbQuery.append(" Where PIntD.stateIntDepositary = :statePartIntDepPrm ");
		sbQuery.append(" And PIntD.participant.idParticipantPk = :idParticipantPrm ");		
		
		TypedQuery<ParticipantIntDepositary> query = em.createQuery(sbQuery.toString(), ParticipantIntDepositary.class);
		query.setParameter("statePartIntDepPrm", filter.getStateParticipantIntDepositary());
		query.setParameter("idParticipantPrm", filter.getIdParticipantPk());		
		
		return (List<ParticipantIntDepositary>) query.getResultList();
	}
	
	/**
	 * Gets the list negotiation mechanism service bean.
	 *
	 * @param filter the filter
	 * @return the list negotiation mechanism service bean
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getListNegotiationMechanismServiceBean(NegotiationMechanismTO filter) throws ServiceException	{
		
		Map<String,Object> parameters = new HashMap<String, Object>();
	 	parameters.put("stateMechanismPrm", filter.getNegotiationMechanismState());
	 	parameters.put("stateMechanismModalityPrm", filter.getMechanismModalityState());
    	
	 	List<NegotiationMechanism> lstNegotiationMechanism = (List<NegotiationMechanism>)findWithNamedQuery(NegotiationMechanism.NEGOTIATION_MECHANISM_BY_STATE, parameters);
    	
    	for (NegotiationMechanism negotiationMechanism : lstNegotiationMechanism) {
			List<MechanismModality> mechanismModalities = negotiationMechanism.getMechanismModalities();
			for (MechanismModality mechanismModality : mechanismModalities) {
				if(Validations.validateIsNotNull(mechanismModality.getNegotiationModality())){
					mechanismModality.getNegotiationModality().getModalityName();
				}
			}
		}
    	
    	return lstNegotiationMechanism;
	}

	/**
	 * Gets the list international depository service bean.
	 *
	 * @param filter the filter
	 * @return the list international depository service bean
	 * @throws ServiceException the service exception
	 */
	public List<InternationalDepository> getListInternationalDepositoryServiceBean(InternationalDepositoryTO filter) throws ServiceException{		
		Map<String,Object> parameters = new HashMap<String, Object>();
	 	parameters.put("stateIntDepository", filter.getStateIntDepository());
	 	    
    	return (List<InternationalDepository>) findWithNamedQuery(InternationalDepository.INTERNATIONAL_DEPOSITORIES_BY_STATE, parameters);
	}

	/**
	 * Gets the list participant files service bean.
	 *
	 * @param participantFilter the participant filter
	 * @return the list participant files service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantFile> getListParticipantFilesServiceBean(Participant participantFilter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder("Select PFile From ParticipantFile PFile ");
		sbQuery.append(" Where PFile.participant.idParticipantPk = :idParticipantPrm and PFile.participantRequest.idParticipantRequestPk is null ");
		sbQuery.append(" And PFile.stateFile = :stateFilePrm ");	
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPrm", participantFilter.getIdParticipantPk());
		query.setParameter("stateFilePrm", ParticipantFileStateType.REGISTERED.getCode());		
		
		return (List<ParticipantFile>) query.getResultList();
	}
	
	/**
	 * Gets the list participant files service bean.
	 *
	 * @param participantFilter the participant filter
	 * @return the list participant files service bean
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionBankAccount> getListInstitutionBankAccountsServiceBean(Participant participantFilter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder("Select Iba From InstitutionBankAccount Iba ");
		sbQuery.append(" Where Iba.participant.idParticipantPk = :idParticipantPrm ");
		sbQuery.append(" And Iba.state = :statePrm ");	
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPrm", participantFilter.getIdParticipantPk());
		query.setParameter("statePrm", BankAccountsStateType.REGISTERED.getCode());		
		
		return (List<InstitutionBankAccount>) query.getResultList();
	}
	
	/**
	 * Get Bank Accounts For Id.
	 *
	 * @param idInstitutionBankAccount id Institution Bank Account
	 * @return Institution Bank Account
	 * @throws ServiceException Institution Bank Account
	 */
	public List<InstitutionBankAccount> getBankAccountsForId(Participant participantFilter)throws ServiceException{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs	"
					+ " INNER JOIN FETCH cs.providerBank "
					+ "WHERE cs.participant.idParticipantPk = :idParticipantPk ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("idParticipantPk",  participantFilter.getIdParticipantPk());
			List<InstitutionBankAccount> lstInstitutionBankAccount = (List<InstitutionBankAccount>)query.getResultList();	
			for(InstitutionBankAccount institutionBankAccountObj:lstInstitutionBankAccount) {
				if(Validations.validateIsNotNull(institutionBankAccountObj)){
					if(Validations.validateIsNotNull(institutionBankAccountObj.getProviderBank())){
						institutionBankAccountObj.setBankType(BankType.COMMERCIAL.getCode());
						institutionBankAccountObj.getProviderBank().getDescription();
					}
					if(Validations.validateIsNotNull(institutionBankAccountObj.getParticipant())){
						institutionBankAccountObj.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
						institutionBankAccountObj.getParticipant().getDescription();
					}	
				}
			}
			return lstInstitutionBankAccount;
	}

	/**
	 * Gets the list represented entity by participant service bean.
	 *
	 * @param participantFilter the participant filter
	 * @return the list represented entity by participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<RepresentedEntity> getListRepresentedEntityByParticipant(
			Participant participantFilter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Select distinct RP From RepresentedEntity RP ");
		sbQuery.append(" Join Fetch RP.legalRepresentative LegRep ");
		sbQuery.append(" Where LegRep.state = :stateLegalReprePrm ");
		sbQuery.append(" And RP.stateRepresented = :stateRepreEntityPrm ");
		sbQuery.append(" And RP.participant.idParticipantPk = :idParticipantPrm ");
		
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("stateLegalReprePrm", LegalRepresentativeStateType.REGISTERED.getCode());
		query.setParameter("stateRepreEntityPrm", RepresentativeEntityStateType.REGISTERED.getCode());
		query.setParameter("idParticipantPrm", participantFilter.getIdParticipantPk());
		
		return (List<RepresentedEntity>) query.getResultList();
	}
	
	/**
	 * Find rnc information by id service bean.
	 *
	 * @param rncFilter the rnc filter
	 * @return the rnc information
	 * @throws ServiceException the service exception
	 */
	public InstitutionInformation findRncInformationByIdServiceBean(InstitutionInformation rncFilter) throws ServiceException {
		try{
			Query query = em.createNamedQuery(InstitutionInformation.INSTITUTION_INFORMATION_BY_ID);
			
			query.setParameter("idRncInformationPrm", rncFilter.getIdInstitutionInformationPk());
			
			return (InstitutionInformation) query.getSingleResult();
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Validate exists participant request service bean.
	 *
	 * @param participantRequestTO the participant request to
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequest validateExistsParticipantRequestServiceBean(ParticipantRequestTO participantRequestTO)
			throws ServiceException{
			
			StringBuilder sbQuery = new StringBuilder("Select PR From ParticipantRequest PR ");
			sbQuery.append(" Where PR.stateRequest = :stateParticipantRequestPrm ");
			
			if(Validations.validateIsNotNullAndPositive(participantRequestTO.getIdParticipantPk())){
				sbQuery.append(" And PR.participant.idParticipantPk = :idParticipantPrm ");
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(participantRequestTO.getLstParticipantRequestTypes())){
				sbQuery.append(" And PR.requestType In (:lstParticipantRequestTypePrm) ");		
			}
			
			if(Validations.validateIsNotNullAndPositive(participantRequestTO.getRequestType())){
				sbQuery.append(" And PR.requestType = :requestTypePrm ");
			}
			
			TypedQuery<ParticipantRequest> query = em.createQuery(sbQuery.toString(),ParticipantRequest.class);		
			query.setParameter("stateParticipantRequestPrm", participantRequestTO.getStateParticipantRequest());
			
			if(Validations.validateIsNotNullAndPositive(participantRequestTO.getIdParticipantPk())){
				query.setParameter("idParticipantPrm", participantRequestTO.getIdParticipantPk());
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(participantRequestTO.getLstParticipantRequestTypes())){
				query.setParameter("lstParticipantRequestTypePrm", participantRequestTO.getLstParticipantRequestTypes());				
			}
			
			if(Validations.validateIsNotNullAndPositive(participantRequestTO.getRequestType())){
				query.setParameter("requestTypePrm", participantRequestTO.getRequestType());
			}
			
			List<ParticipantRequest> participantRequests = query.getResultList();
			
			if(Validations.validateListIsNotNullAndNotEmpty(participantRequests)){
				return participantRequests.get(0);
			} else {
				return null;
			}

	}
	
	/**
	 * Gets the list participant request by filters service bean.
	 *
	 * @param filter the filter
	 * @return the list participant request by filters service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantRequest> getListParticipantRequestByFilters(ParticipantTO filter) throws ServiceException{
		CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
		CriteriaQuery<ParticipantRequest> criteriaQuery=criteriaBuilder.createQuery(ParticipantRequest.class);
		Root<ParticipantRequest> participantRequestRoot=criteriaQuery.from(ParticipantRequest.class);
		participantRequestRoot.fetch("participant");
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		criteriaQuery.select(participantRequestRoot);
		
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			ParameterExpression<Long> param = criteriaBuilder.parameter(Long.class,"idParticipantPkPrm");
			criteriaParameters.add(criteriaBuilder.equal(participantRequestRoot.get("participant").get("idParticipantPk"), param));
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
			criteriaParameters.add(criteriaBuilder.like(participantRequestRoot.get("participant").<String>get("mnemonic"), param));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"descriptionPrm");
			criteriaParameters.add(criteriaBuilder.like(participantRequestRoot.get("participant").<String>get("description"), param));	
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
			criteriaParameters.add(criteriaBuilder.equal(participantRequestRoot.get("participant").get("documentType"), param));	
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"documentNumberPrm");
			criteriaParameters.add(criteriaBuilder.equal(participantRequestRoot.get("participant").get("documentNumber"), param));	
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getLstParticipantRequestTypes())){
			ParameterExpression<List> param = criteriaBuilder.parameter(List.class,"lstParticipantRequestTypePrm");
			criteriaParameters.add(participantRequestRoot.get("requestType").in(param));				
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getParticipantRequestTO().getRequestType())){
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"requestTypePrm");
			criteriaParameters.add(criteriaBuilder.equal(participantRequestRoot.get("requestType"), param));	
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getRequestNumber())){
			ParameterExpression<Long> param = criteriaBuilder.parameter(Long.class,"requestNumberPrm");
			criteriaParameters.add(criteriaBuilder.equal(participantRequestRoot.get("requestNumber"), param));	
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getLstParticipantRequestStates())){
			ParameterExpression<List> param = criteriaBuilder.parameter(List.class,"lstParticipantRequestStatePrm");
			criteriaParameters.add(participantRequestRoot.get("stateRequest").in(param));				
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getParticipantRequestTO().getStateParticipantRequest())){
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"stateParticipantRequestPrm");
			criteriaParameters.add(criteriaBuilder.equal(participantRequestRoot.get("stateRequest"), param));	
		}				
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getInitiaRequestDate()) &&
				Validations.validateIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getEndRequestDate())){
			ParameterExpression<Date> paramDate1 = criteriaBuilder.parameter(Date.class,"initReqDatePrm");
			ParameterExpression<Date> paramDate2 = criteriaBuilder.parameter(Date.class,"endReqDatePrm");
			criteriaParameters.add(criteriaBuilder.between(participantRequestRoot.<Date>get("registryDate"), paramDate1 , paramDate2));
		}
		
		criteriaQuery.orderBy(criteriaBuilder.desc(participantRequestRoot.get("idParticipantRequestPk")));
		
		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
				criteriaQuery.where(criteriaParameters.get(0));
		    } else {
		    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		
		TypedQuery<ParticipantRequest> participantCriteria = em.createQuery(criteriaQuery);
		
		if(Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())){
			participantCriteria.setParameter("idParticipantPkPrm", filter.getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
			if(filter.getMnemonic().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				participantCriteria.setParameter("mnemonicPrm", filter.getMnemonic().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
			} else {
				participantCriteria.setParameter("mnemonicPrm", filter.getMnemonic());
			}			
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
			if(filter.getDescription().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				participantCriteria.setParameter("descriptionPrm", filter.getDescription().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
			} else {
				participantCriteria.setParameter("descriptionPrm", filter.getDescription());
			}
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
			participantCriteria.setParameter("documentTypePrm", filter.getDocumentType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			participantCriteria.setParameter("documentNumberPrm", filter.getDocumentNumber());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getLstParticipantRequestTypes())){
			participantCriteria.setParameter("lstParticipantRequestTypePrm", filter.getParticipantRequestTO().getLstParticipantRequestTypes());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getParticipantRequestTO().getRequestType())){
			participantCriteria.setParameter("requestTypePrm", filter.getParticipantRequestTO().getRequestType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getRequestNumber())){
			participantCriteria.setParameter("requestNumberPrm", filter.getParticipantRequestTO().getRequestNumber());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getLstParticipantRequestStates())){
			participantCriteria.setParameter("lstParticipantRequestStatePrm", filter.getParticipantRequestTO().getLstParticipantRequestStates());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getParticipantRequestTO().getStateParticipantRequest())){
			participantCriteria.setParameter("stateParticipantRequestPrm", filter.getParticipantRequestTO().getStateParticipantRequest());
		}				
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getInitiaRequestDate()) &&
				Validations.validateIsNotNullAndNotEmpty(filter.getParticipantRequestTO().getEndRequestDate())) {
			participantCriteria.setParameter("initReqDatePrm", filter.getParticipantRequestTO().getInitiaRequestDate(), TemporalType.DATE);			
			participantCriteria.setParameter("endReqDatePrm", CommonsUtilities.addDate(filter.getParticipantRequestTO().getEndRequestDate(), 1) , TemporalType.DATE);
		}
		
		return (List<ParticipantRequest>)participantCriteria.getResultList();				
	}
	
	/**
	 * Find participant request by id service bean.
	 *
	 * @param participantRequestTO the participant request to
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequest findParticipantRequestByIdServiceBean(ParticipantRequestTO participantRequestTO)
			throws ServiceException{
		
		try {
			
			StringBuilder sbQuery = new StringBuilder("Select PR From ParticipantRequest PR ");			
			sbQuery.append(" Join Fetch PR.participant P ");
			sbQuery.append(" Where PR.idParticipantRequestPk = :idParticipantRequestPrm ");
				
			Query query = em.createQuery(sbQuery.toString());		
			query.setParameter("idParticipantRequestPrm", participantRequestTO.getIdParticipantRequestPk());			
						
			ParticipantRequest participantRequest = (ParticipantRequest) query.getSingleResult();
			participantRequest.setParticipantFiles(listParticipantFileByRequest(participantRequestTO.getIdParticipantRequestPk())); 
			
			return participantRequest;
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Find participant request modification by id service bean.
	 *
	 * @param participantRequestTO the participant request to
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequest findParticipantRequestModificationById(ParticipantRequestTO participantRequestTO)
			throws ServiceException{
		try {
			
			StringBuilder sbQuery = new StringBuilder("Select PR From ParticipantRequest PR ");
			sbQuery.append(" Join Fetch PR.participant P ");
			sbQuery.append(" Where PR.idParticipantRequestPk = :idParticipantRequestPrm ");
			
			TypedQuery<ParticipantRequest> query = em.createQuery(sbQuery.toString(), ParticipantRequest.class);		
			query.setParameter("idParticipantRequestPrm", participantRequestTO.getIdParticipantRequestPk());
			
			ParticipantRequest participantRequest = (ParticipantRequest) query.getSingleResult();
			
			if(participantRequestTO.isIndRequiredFiles()) {
				if(Validations.validateIsNotNull(participantRequest)) {
					if((Validations.validateIsNotNull(participantRequest.getParticipantFileHistories()))){
						participantRequest.getParticipantFileHistories().size();
					}
				}
			}
						
			return participantRequest;
			
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Registry Bank Accounts Facade.
	 *
	 * @param institutionBankAccount institution Bank Account
	 * @param loggerUser the logger user
	 * @return  institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount registryBankAccountsFacade(InstitutionBankAccount institutionBankAccount,LoggerUser loggerUser)throws ServiceException{
		try{
			Map<String,Object> paramat= new HashMap<String,Object>();		
			paramat.put("idBankPkParam", institutionBankAccount.getProviderBank().getIdBankPk());
			Bank bank = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramat);
			if(!bank.getState().equals(BankStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
			}
									
			if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode())){
				institutionBankAccount.setParticipant(null);
				institutionBankAccount.setBank(null);
				Map<String,Object> paramatAu= new HashMap<String,Object>();		
				paramatAu.put("idIssuerPkParam", institutionBankAccount.getIssuer().getIdIssuerPk());
				Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, paramatAu);
				if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
					throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
				}
			}
			else{
				if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode())){
					institutionBankAccount.setIssuer(null);
					institutionBankAccount.setBank(null);
					Map<String,Object> paramatA= new HashMap<String,Object>();		
					paramatA.put("idParticipantPkParam", institutionBankAccount.getParticipant().getIdParticipantPk());
					Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, paramatA);
					if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
					}
				}
				else{
					if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					{
						institutionBankAccount.setParticipant(null);
						institutionBankAccount.setIssuer(null);
						Map<String,Object> paramatAux= new HashMap<String,Object>();		
						paramatAux.put("idBankPkParam", institutionBankAccount.getBank().getIdBankPk());
						Bank bankAux = findObjectByNamedQuery(Bank.BANK_REQUEST_STATE, paramatAux);
						if(!bankAux.getState().equals(BankStateType.REGISTERED.getCode())){
							throw new ServiceException(ErrorServiceType.BANK_BLOCKED_ERROR);
						}
					}
				}
			}
			 if(institutionBankAccount.getBankType().equals(BankType.CENTRALIZING.getCode())){
				 String idInstitution=null;
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
				 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
				 InstitutionBankAccount institutionBankAccountForCurrency = getInstitutionBankAccountForCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getCurrency());
				 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForCurrency))
					 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				 
			 }
			 else{
				 
				 if(institutionBankAccount.getBankType().equals(BankType.COMMERCIAL.getCode())){
					 String idInstitution=null;
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
						 idInstitution= institutionBankAccount.getIssuer().getIdIssuerPk();
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
						 idInstitution= institutionBankAccount.getParticipant().getIdParticipantPk().toString();
					 if(institutionBankAccount.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
						 idInstitution= institutionBankAccount.getBank().getIdBankPk().toString();
					 InstitutionBankAccount institutionBankAccountForAccountNumberAndCurrency = getInstitutionBankAccountForAccountNumberAndCurrency(institutionBankAccount.getInstitutionType(),idInstitution,institutionBankAccount.getProviderBank().getIdBankPk(), institutionBankAccount.getAccountNumber(), institutionBankAccount.getCurrency());
					 if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountForAccountNumberAndCurrency))
						 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
					 
					 
				 }
			 }
			 
			 institutionBankAccount.setState(BankAccountsStateType.REGISTERED.getCode());
			 institutionBankAccount.setRegistryUser(loggerUser.getUserName());
			 institutionBankAccount.setRegistryDate(CommonsUtilities.currentDateTime());
			 institutionBankAccount.setAudit(loggerUser);
			 create(institutionBankAccount);
			 return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Get Institution Bank Account For Currency.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idBank the id bank
	 * @param idCurrency id currency
	 * @return Institution Bank Account
	 * @throws ServiceException  the Service Exception
	 */
	public InstitutionBankAccount getInstitutionBankAccountForCurrency(Integer institution,String idInstitution,Long idBank,Integer idCurrency)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs	WHERE cs.currency = :currency AND cs.providerBank.idBankPk = :idBankProviderPk AND cs.state = :state ");
			if(InstitutionBankAccountsType.ISSUER.getCode().equals(institution))
				sbQuery.append("  AND cs.issuer.idIssuerPk = :idIssuerPk ");
			if(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institution))
				sbQuery.append("  AND cs.participant.idParticipantPk = :idParticipantPk ");
			if(InstitutionBankAccountsType.BANK.getCode().equals(institution))
				sbQuery.append("  AND cs.bank.idBankPk = :idBankPk ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("currency",  idCurrency);
			query.setParameter("idBankProviderPk",  idBank);
			query.setParameter("state",  BankAccountsStateType.REGISTERED.getCode());
			if(InstitutionBankAccountsType.ISSUER.getCode().equals(institution))
				query.setParameter("idIssuerPk",  idInstitution);
			if(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institution))
				query.setParameter("idParticipantPk",  new Long(idInstitution));
			if(InstitutionBankAccountsType.BANK.getCode().equals(institution))
				query.setParameter("idBankPk",  new Long(idInstitution));
			InstitutionBankAccount institutionBankAccount = (InstitutionBankAccount)query.getSingleResult();			
			return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * getInstitutionBankAccountForAccounu Number And Currency.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idBank the id bank
	 * @param number number account
	 * @param idCurrency id currency
	 * @return Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount getInstitutionBankAccountForAccountNumberAndCurrency(Integer institution,String idInstitution,Long idBank,String number , Integer idCurrency)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs	WHERE cs.currency = :currency AND cs.accountNumber = :accountNumber AND cs.providerBank.idBankPk = :idBankProviderPk AND cs.state = :state ");
			if(InstitutionBankAccountsType.ISSUER.getCode().equals(institution))
				sbQuery.append("  AND cs.issuer.idIssuerPk = :idIssuerPk ");
			if(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institution))
				sbQuery.append("  AND cs.participant.idParticipantPk = :idParticipantPk ");
			if(InstitutionBankAccountsType.BANK.getCode().equals(institution))
				sbQuery.append("  AND cs.bank.idBankPk = :idBankPk ");
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("currency",  idCurrency);
			query.setParameter("accountNumber",  number);
			query.setParameter("idBankProviderPk",  idBank);
			query.setParameter("state",  BankAccountsStateType.REGISTERED.getCode());
			if(InstitutionBankAccountsType.ISSUER.getCode().equals(institution))
				query.setParameter("idIssuerPk",  idInstitution);
			if(InstitutionBankAccountsType.PARTICIPANT.getCode().equals(institution))
				query.setParameter("idParticipantPk",  new Long(idInstitution));
			if(InstitutionBankAccountsType.BANK.getCode().equals(institution))
				query.setParameter("idBankPk",  new Long(idInstitution));
			InstitutionBankAccount institutionBankAccount = (InstitutionBankAccount)query.getSingleResult();			
			return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Validate institution cash account.
	 *
	 * @param searchBankAccountsTO the search bank accounts to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionBankAccount> validateInstitutionCashAccount(SearchBankAccountsParticipantTO searchBankAccountsTO)throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs	"
					+ " LEFT JOIN FETCH cs.providerBank b"
					+ " LEFT JOIN FETCH cs.bank  ban"
					+ " WHERE 1=1 ");
			if(Validations.validateIsNotNull(searchBankAccountsTO.getAccountNumber()))
				sbQuery.append(" and cs.accountNumber = :accountNumber");
			if(Validations.validateIsNotNull(searchBankAccountsTO.getIdBankProviderPk()))
				sbQuery.append(" and b.idBankPk = :idBankPk");
			if(Validations.validateIsNotNull(searchBankAccountsTO.getCurrency()))
				sbQuery.append(" and cs.currency = :currency");
			if(Validations.validateIsNotNull(searchBankAccountsTO.getInstitutionType()))
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					sbQuery.append(" and cs.issuer.idIssuerPk = :idBankInstitutionPk");
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					sbQuery.append(" and cs.participant.idParticipantPk = :idBankInstitutionPk");
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					sbQuery.append(" and ban.idBankPk = :idBankInstitutionPk");
			Query query = em.createQuery(sbQuery.toString()); 
			if(Validations.validateIsNotNull(searchBankAccountsTO.getAccountNumber()))
				query.setParameter("accountNumber",  searchBankAccountsTO.getAccountNumber());
			if(Validations.validateIsNotNull(searchBankAccountsTO.getIdBankProviderPk()))
				query.setParameter("idBankPk",  searchBankAccountsTO.getIdBankProviderPk());
			if(Validations.validateIsNotNull(searchBankAccountsTO.getCurrency()))
				query.setParameter("currency",  searchBankAccountsTO.getCurrency());
			if(Validations.validateIsNotNull(searchBankAccountsTO.getInstitutionType()))
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.ISSUER.getCode()))
					query.setParameter("idBankInstitutionPk",  searchBankAccountsTO.getInstitution());
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.PARTICIPANT.getCode()))
					query.setParameter("idBankInstitutionPk",  Long.valueOf(searchBankAccountsTO.getInstitution()));
				if(searchBankAccountsTO.getInstitutionType().equals(InstitutionBankAccountsType.BANK.getCode()))
					query.setParameter("idBankInstitutionPk",  Long.valueOf(searchBankAccountsTO.getInstitution()));
			List<InstitutionBankAccount> lstInstitutionBankAccount = (ArrayList<InstitutionBankAccount>)query.getResultList();	
		
			return lstInstitutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	/**
	 * Gets the list participant history states service bean.
	 *
	 * @param participantHistoryStateTO the participant history state to
	 * @return the list participant history states service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantHistoryState> getListParticipantHistoryStatesServiceBean(ParticipantHistoryStateTO participantHistoryStateTO)
			throws ServiceException {
			StringBuilder sbQuery = new StringBuilder("Select PH From ParticipantHistoryState PH ");
			sbQuery.append(" Join Fetch PH.participant P ");
			sbQuery.append(" Where PH.participant.idParticipantPk = :idParticipantPrm ");

			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idParticipantPrm", participantHistoryStateTO.getIdParticipantPk());

			return (List<ParticipantHistoryState>) query.getResultList();
	}
	
	/**
	 * Find represented entity by id service bean.
	 *
	 * @param filter the filter
	 * @return the represented entity
	 * @throws ServiceException the service exception
	 */
	public RepresentedEntity findRepresentedEntityByIdServiceBean(			
			RepresentedEntityTO filter) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder("Select distinct RP From RepresentedEntity RP ");
			sbQuery.append(" Join Fetch RP.legalRepresentative LegRep ");
			sbQuery.append(" Where LegRep.state = :stateLegalReprePrm ");
			sbQuery.append(" And RP.stateRepresented = :stateRepreEntityPrm ");
			sbQuery.append(" And RP.idRepresentedEntityPk = :idRepresentedEntityPrm ");
			
			Query query = em.createQuery(sbQuery.toString());		
			query.setParameter("stateLegalReprePrm",filter.getStateLegalRepresentative());
			query.setParameter("stateRepreEntityPrm", filter.getStateRepresentedEntity());
			query.setParameter("idRepresentedEntityPrm", filter.getIdRepresentedEntityPk());
			
			return (RepresentedEntity) query.getSingleResult();
		
			} catch(NoResultException ex){
				   return null;
			} catch(NonUniqueResultException ex){
				   return null;
			}
	}

	/**
	 * Find participant history by request id service bean.
	 *
	 * @param participantRequestTO the participant request to
	 * @return the participant request history
	 */
	public ParticipantRequestHistory findParticipantHistoryByRequest(
			ParticipantRequestTO participantRequestTO) {		
		try {
			
			StringBuilder sbQuery = new StringBuilder("Select PRH From ParticipantRequestHistory PRH ");			
			sbQuery.append(" Where PRH.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm ");
			sbQuery.append(" And PRH.indNew = :indNewPrm ");
			
			TypedQuery<ParticipantRequestHistory> query = em.createQuery(sbQuery.toString() , ParticipantRequestHistory.class);		
			query.setParameter("idParticipantRequestPrm", participantRequestTO.getIdParticipantRequestPk());
			query.setParameter("indNewPrm", participantRequestTO.getIndNewData());
			
			return (ParticipantRequestHistory) query.getSingleResult();
			
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Gets the holder by document type and document number service bean.
	 *
	 * @param holder the holder
	 * @return the holder by document type and document number service bean
	 * @throws ServiceException the service exception
	 */
	public Holder getHolderByDocumentTypeAndDocumentNumberServiceBean(Holder holder) throws ServiceException{
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM Holder h WHERE h.documentType = :documentType ");	      
		  stringBuilderSql.append(" and h.documentNumber = :documentNumber ");
		  
		  if(Validations.validateIsNotNullAndNotEmpty(holder.getFullName())){
			  stringBuilderSql.append(" and h.fullName = :fullName ");  
		  }
		  
		  TypedQuery<Holder> query = em.createQuery(stringBuilderSql.toString(), Holder.class);
		  query.setParameter("documentType", holder.getDocumentType());	
		  query.setParameter("documentNumber", holder.getDocumentNumber());	
		  
		  if(Validations.validateIsNotNullAndNotEmpty(holder.getFullName())){
			  query.setParameter("fullName", holder.getFullName());	
		  }
		  
		  List<Holder> lstHolders = query.getResultList();
		  
		  if(Validations.validateListIsNotNullAndNotEmpty(lstHolders)){
			  return lstHolders.get(0);
		  } else {
			  return null;
		  }
	}

	/**
	 * Gets the list legal representative hist by participant request service bean.
	 *
	 * @param participantRequest the participant request
	 * @param registryTypeLegal the registry type legal
	 * @return the list legal representative hist by participant request service bean
	 * @throws ServiceException the service exception
	 */
	public List<LegalRepresentativeHistory> listLegalRepresentHistByParticipantRequest(ParticipantRequest participantRequest, Integer registryTypeLegal) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Select distinct LRH From LegalRepresentativeHistory LRH ");
		sbQuery.append(" Where LRH.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm ");
		sbQuery.append(" And LRH.registryType = :registryTypePrm ");
		
		TypedQuery<LegalRepresentativeHistory> query = em.createQuery(sbQuery.toString(),LegalRepresentativeHistory.class);
		query.setParameter("idParticipantRequestPrm", participantRequest.getIdParticipantRequestPk());	
		query.setParameter("registryTypePrm", registryTypeLegal);
		
		List<LegalRepresentativeHistory> lstResultLegalRepresentativeHist = query.getResultList();
		
		for (LegalRepresentativeHistory objLegalRepresentativeHistory : lstResultLegalRepresentativeHist) {			
				objLegalRepresentativeHistory.setRepresenteFileHistory(getListLegalRepresentativeFileHist(objLegalRepresentativeHistory));
		}
		
		return lstResultLegalRepresentativeHist;
	}

	/**
	 * Gets the list participant mechanism histories by participant service bean.
	 *
	 * @param participantMechanismTO the participant mechanism to
	 * @return the list participant mechanism histories by participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantMechanismHistory> getListParticipantMechanismHistoriesByParticipantServiceBean(ParticipantMechanismTO participantMechanismTO) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder("Select PM From ParticipantMechanismHistory PM ");
		sbQuery.append(" Join Fetch PM.mechanismModality MechMod ");
		sbQuery.append(" Where PM.stateParticipantMechanism = :statePartMechPrm ");
		sbQuery.append(" And PM.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm ");
		
		TypedQuery<ParticipantMechanismHistory> query = em.createQuery(sbQuery.toString(), ParticipantMechanismHistory.class);
		query.setParameter("statePartMechPrm", participantMechanismTO.getStateParticipantMechanism());
		query.setParameter("idParticipantRequestPrm", participantMechanismTO.getIdParticipantRequestPk());
		
		return (List<ParticipantMechanismHistory>) query.getResultList();
	}

	/**
	 * Gets the list participant int depo history by participant service bean.
	 *
	 * @param participantIntDepositoryTO the participant int depository to
	 * @return the list participant int depo history by participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantIntDepoHistory> getListParticipantIntDepoHistoryByParticipantServiceBean(
			ParticipantIntDepositoryTO participantIntDepositoryTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Select PIntD From ParticipantIntDepoHistory PIntD ");
		sbQuery.append(" Join Fetch  PIntD.internationalDepository IntDep ");
		sbQuery.append(" Where PIntD.stateIntDepositary = :statePartIntDepPrm ");
		sbQuery.append(" And PIntD.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm ");
		sbQuery.append(" And PIntD.indNew = :indNewPrm ");
		
		TypedQuery<ParticipantIntDepoHistory> query = em.createQuery(sbQuery.toString(), ParticipantIntDepoHistory.class);
		query.setParameter("statePartIntDepPrm", participantIntDepositoryTO.getStateParticipantIntDepositary());
		query.setParameter("idParticipantRequestPrm", participantIntDepositoryTO.getIdParticipantRequestPk());	
		query.setParameter("indNewPrm", participantIntDepositoryTO.getIndNew());		
		
		return (List<ParticipantIntDepoHistory>) query.getResultList();
	}
	
	/**
	 * Disable participant files state service bean.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableParticipantFilesStateServiceBean(Participant participant, LoggerUser loggerUser) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update ParticipantFile IFile ");
		sbQuery.append(" Set IFile.stateFile = :statePrm, IFile.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" IFile.lastModifyDate = :lastModifyDatePrm, IFile.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" IFile.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where IFile.participant.idParticipantPk = :idParticipantPrm And IFile.participantRequest.idParticipantRequestPk is null ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",ParticipantFileStateType.DELETED.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());

		return query.executeUpdate();
	}
	
	/**
	 * Disable participant bank accounts.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @return the int
	 */
	public int disableParticipantBankAccounts(Participant participant, LoggerUser loggerUser){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update ParticipantBankAccount PBA ");
		sbQuery.append(" Set PBA.accountState = :statePrm, PBA.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" PBA.lastModifyDate = :lastModifyDatePrm, PBA.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" PBA.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where PBA.participant.idParticipantPk = :idParticipantPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",ParticipantBankAccountStateType.DELETED.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());

		return query.executeUpdate();
	}

	/**
	 * Disable represented entities state service bean.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableRepresentedEntitiesStateServiceBean(Participant participant, LoggerUser loggerUser) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update RepresentedEntity RE ");	
		sbQuery.append(" Set RE.stateRepresented = :statePrm, RE.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" RE.lastModifyDate = :lastModifyDatePrm, RE.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" RE.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where RE.participant.idParticipantPk = :idParticipantPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",RepresentativeEntityStateType.DELETED.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());

		return query.executeUpdate();
	}
	
	/**
	 * Update state participant service bean.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStateParticipantServiceBean(Participant participant, LoggerUser loggerUser) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Participant Part ");
		sbQuery.append(" Set Part.state = :statePrm, Part.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" Part.lastModifyDate = :lastModifyDatePrm, Part.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" Part.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where Part.idParticipantPk = :idParticipantPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm", participant.getState());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());

		return query.executeUpdate();
	}
	
	/**
	 * Update state participant request service bean.
	 *
	 * @param participantRequest the participant request
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStateParticipantRequestServiceBean(ParticipantRequest participantRequest, LoggerUser loggerUser) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update ParticipantRequest PR ");
		sbQuery.append(" Set PR.stateRequest = :statePrm, PR.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" PR.lastModifyDate = :lastModifyDatePrm, PR.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" PR.lastModifyApp = :lastModifyAppPrm ");
		
		if(Validations.validateIsNotNull(participantRequest.getConfirmationUser())){
			sbQuery.append(" ,PR.confirmationUser = :confirmationUserPrm ");
		}
		if(Validations.validateIsNotNull(participantRequest.getConfirmationDate())){
			sbQuery.append(" ,PR.confirmationDate = :confirmationDatePrm ");		
		}
		if(Validations.validateIsNotNull(participantRequest.getRejectedUser())){
			sbQuery.append(" ,PR.rejectedUser = :rejectedUserPrm ");
		}
		if(Validations.validateIsNotNull(participantRequest.getRejectedDate())){
			sbQuery.append(" ,PR.rejectedDate = :rejectedDatePrm ");
		}
		
		sbQuery.append(" Where PR.idParticipantRequestPk = :idParticipantPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm", participantRequest.getStateRequest());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		if(Validations.validateIsNotNull(participantRequest.getConfirmationUser())){
			query.setParameter("confirmationUserPrm", participantRequest.getConfirmationUser());
		}
		if(Validations.validateIsNotNull(participantRequest.getConfirmationDate())){
			query.setParameter("confirmationDatePrm", participantRequest.getConfirmationDate());
		}
		if(Validations.validateIsNotNull(participantRequest.getRejectedUser())){
			query.setParameter("rejectedUserPrm", participantRequest.getRejectedUser());
		}
		if(Validations.validateIsNotNull(participantRequest.getRejectedDate())){
			query.setParameter("rejectedDatePrm", participantRequest.getRejectedDate());
		}		
		query.setParameter("idParticipantPrm", participantRequest.getIdParticipantRequestPk());

		return query.executeUpdate();
	}
	
	/**
	 * Gets the list legal representative file service bean.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the list legal representative file service bean
	 * @throws ServiceException the service exception
	 */
	public List<LegalRepresentativeFile> getListLegalRepresentativeFile(
			LegalRepresentative legalRepresentative) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder("Select LRF From LegalRepresentativeFile LRF ");
		sbQuery.append(" Where LRF.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePrm ");
		sbQuery.append(" And LRF.stateFile = :statePrm ");
		
		TypedQuery<LegalRepresentativeFile> query = em.createQuery(sbQuery.toString(),LegalRepresentativeFile.class);
		query.setParameter("idLegalRepresentativePrm", legalRepresentative.getIdLegalRepresentativePk());
		query.setParameter("statePrm", HolderFileStateType.REGISTERED.getCode());
		
		List<LegalRepresentativeFile> legalRepresentativeFiles = (List<LegalRepresentativeFile>) query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeFiles)){
			for (LegalRepresentativeFile legalRepresentativeFile : legalRepresentativeFiles) {
				legalRepresentativeFile.getFileLegalRepre();
			}
		}
				
		return legalRepresentativeFiles;
	}

	/**
	 * Gets the list legal representative file hist service bean.
	 *
	 * @param legalRepresentativeHistory the legal representative history
	 * @return the list legal representative file hist service bean
	 * @throws ServiceException the service exception
	 */
	public List<RepresentativeFileHistory> getListLegalRepresentativeFileHist(
			LegalRepresentativeHistory legalRepresentativeHistory) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder("Select RFH From RepresentativeFileHistory RFH ");
		sbQuery.append(" Where RFH.legalRepresentativeHistory.idRepresentativeHistoryPk = :ididRepresentativeHistoryPrm ");
		
		TypedQuery<RepresentativeFileHistory> query = em.createQuery(sbQuery.toString(),RepresentativeFileHistory.class);
		query.setParameter("ididRepresentativeHistoryPrm", legalRepresentativeHistory.getIdRepresentativeHistoryPk());		
		
		List<RepresentativeFileHistory> representativeFileHistories = (List<RepresentativeFileHistory>) query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(representativeFileHistories)){
			for (RepresentativeFileHistory representativeFileHistory : representativeFileHistories) {
				representativeFileHistory.getFileRepresentative();
			}
		}
		return representativeFileHistories;
	}
	
	/**
	 * Find pep person by legal representative service bean.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the pep person
	 * @throws ServiceException the service exception
	 */
	public PepPerson findPepPersonByLegalRepresentative(LegalRepresentative legalRepresentative)
			throws ServiceException{
		try {
			
			StringBuilder sbQuery = new StringBuilder("Select P From PepPerson P ");			
			sbQuery.append(" Where P.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePrm");
			
			TypedQuery<PepPerson> query = em.createQuery(sbQuery.toString() , PepPerson.class);		
			query.setParameter("idLegalRepresentativePrm", legalRepresentative.getIdLegalRepresentativePk());
			
			return (PepPerson) query.getSingleResult();
			
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Gets the legal representative by document type and document number service bean.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the legal representative by document type and document number service bean
	 * @throws ServiceException the service exception
	 */
	public LegalRepresentative getLegalRepresentativeByDocumentTypeAndNumber(LegalRepresentative legalRepresentative) throws ServiceException {
		
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT h FROM LegalRepresentative h WHERE h.documentType = :documentType ");	      
		  stringBuilderSql.append(" and h.documentNumber = :documentNumber");
		  TypedQuery<LegalRepresentative> query = em.createQuery(stringBuilderSql.toString(),LegalRepresentative.class);
		  query.setParameter("documentType", legalRepresentative.getDocumentType());	
		  query.setParameter("documentNumber", legalRepresentative.getDocumentNumber());	
		 
		  List<LegalRepresentative> lstresultRepresentative = (List<LegalRepresentative>) query.getResultList();
		  
		  if(Validations.validateListIsNotNullAndNotEmpty(lstresultRepresentative)) {
			  return lstresultRepresentative.get(0);
		  } else {
			  return null;
		  }
	}

	/**
	 * Disable legal representative files service bean.
	 *
	 * @param objLegalRepresentative the obj legal representative
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableLegalRepresentativeFilesServiceBean(LegalRepresentative objLegalRepresentative, LoggerUser loggerUser) 
									throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update LegalRepresentativeFile LFile ");
		sbQuery.append(" Set LFile.stateFile = :statePrm, LFile.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" LFile.lastModifyDate = :lastModifyDatePrm, LFile.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" LFile.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where LFile.legalRepresentative.idLegalRepresentativePk = :idLegaRepresentativePrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",HolderFileStateType.DELETED.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idLegaRepresentativePrm", objLegalRepresentative.getIdLegalRepresentativePk());

		return query.executeUpdate();		
	}
	
	/**
	 * Gets the list account balances by participant service bean.
	 *
	 * @param preHolderAccountTO the pre holder account to
	 * @return the list account balances by participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getListAccountBalancesByParticipantServiceBean(HolderAccountTO preHolderAccountTO) throws ServiceException {
		
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT hab.transitBalance, hab.accreditationBalance, hab.availableBalance, ");
		  stringBuilderSql.append(" hab.banBalance, hab.borrowerBalance, hab.purchaseBalance, ");
		  stringBuilderSql.append(" hab.lenderBalance, hab.loanableBalance, hab.marginBalance, ");
		  stringBuilderSql.append(" hab.oppositionBalance, hab.otherBlockBalance, hab.pawnBalance, ");
		  stringBuilderSql.append(" hab.reportedBalance, hab.reportingBalance, hab.reserveBalance, ");
		  stringBuilderSql.append(" hab.saleBalance, hab.totalBalance ");
		  stringBuilderSql.append(" FROM HolderAccountBalance hab ");
		  stringBuilderSql.append(" Where hab.participant.idParticipantPk = :idParticipantPrm ");
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  
		  query.setParameter("idParticipantPrm", preHolderAccountTO.getParticipantTO());	
		  
		  List<Object[]> lstArrObjectAccountBalance = query.getResultList();
		  
		  if(Validations.validateListIsNotNullAndNotEmpty(lstArrObjectAccountBalance)){
			  List<HolderAccountBalance> lstHolderAccountBalances = new ArrayList<HolderAccountBalance>();
			  HolderAccountBalance objHolderAccountBalance;
			  for (Object[] arrAccountBalance : lstArrObjectAccountBalance) {
				  objHolderAccountBalance = new HolderAccountBalance();
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[0])){
					  objHolderAccountBalance.setTransitBalance(new BigDecimal(arrAccountBalance[0].toString())); 
				  } else {
					  objHolderAccountBalance.setTransitBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[1])){
					  objHolderAccountBalance.setAccreditationBalance(new BigDecimal(arrAccountBalance[1].toString())); 
				  } else {
					  objHolderAccountBalance.setAccreditationBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[2])){
					  objHolderAccountBalance.setAvailableBalance(new BigDecimal(arrAccountBalance[2].toString()));
				  } else {
					  objHolderAccountBalance.setAvailableBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[3])){
					  objHolderAccountBalance.setBanBalance(new BigDecimal(arrAccountBalance[3].toString()));	  
				  } else {
					  objHolderAccountBalance.setBanBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[4])){
					  objHolderAccountBalance.setBorrowerBalance(new BigDecimal(arrAccountBalance[4].toString())); 
				  } else {
					  objHolderAccountBalance.setBorrowerBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[5])){
					  objHolderAccountBalance.setPurchaseBalance(new BigDecimal(arrAccountBalance[5].toString())); 
				  } else {
					  objHolderAccountBalance.setPurchaseBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[6])){
					  objHolderAccountBalance.setLenderBalance(new BigDecimal(arrAccountBalance[6].toString()));  
				  } else {
					  objHolderAccountBalance.setLenderBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[7])){
					  objHolderAccountBalance.setLoanableBalance(new BigDecimal(arrAccountBalance[7].toString())); 
				  } else {
					  objHolderAccountBalance.setLoanableBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[8])){
					  objHolderAccountBalance.setMarginBalance(new BigDecimal(arrAccountBalance[8].toString())); 
				  } else {
					  objHolderAccountBalance.setMarginBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[9])){
					  objHolderAccountBalance.setOppositionBalance(new BigDecimal(arrAccountBalance[9].toString())); 
				  } else {
					  objHolderAccountBalance.setOppositionBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[10])){
					  objHolderAccountBalance.setOtherBlockBalance(new BigDecimal(arrAccountBalance[10].toString())); 	   
				  } else {
					  objHolderAccountBalance.setOtherBlockBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[11])){
					  objHolderAccountBalance.setPawnBalance(new BigDecimal(arrAccountBalance[11].toString())); 
				  } else {
					  objHolderAccountBalance.setPawnBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[12])){
					  objHolderAccountBalance.setReportedBalance(new BigDecimal(arrAccountBalance[12].toString())); 
				  } else {
					  objHolderAccountBalance.setReportedBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[13])){
					  objHolderAccountBalance.setReportingBalance(new BigDecimal(arrAccountBalance[13].toString())); 
				  } else {
					  objHolderAccountBalance.setReportingBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[14])){
					  objHolderAccountBalance.setReserveBalance(new BigDecimal(arrAccountBalance[14].toString())); 
				  } else {
					  objHolderAccountBalance.setReserveBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[15])){
					  objHolderAccountBalance.setSaleBalance(new BigDecimal(arrAccountBalance[15].toString())); 
				  } else {
					  objHolderAccountBalance.setSaleBalance(BigDecimal.ZERO);
				  }
				  
				  if(Validations.validateIsNotNull(arrAccountBalance[16])){
					  objHolderAccountBalance.setTotalBalance(new BigDecimal(arrAccountBalance[16].toString())); 
				  } else {
					  objHolderAccountBalance.setTotalBalance(BigDecimal.ZERO);
				  }
				  
				  
				  lstHolderAccountBalances.add(objHolderAccountBalance);
			}
			  
			  return lstHolderAccountBalances;
		  } else {
			  return null;
		  }
	}

	/**
	 * Find participant request state by id service bean.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer findParticipantRequestStateByIdServiceBean(Long idParticipantRequestPk) throws ServiceException{
		try {
			  StringBuilder stringBuilderSql = new StringBuilder();			
			  stringBuilderSql.append("SELECT P.stateRequest FROM ParticipantRequest P ");	      
			  stringBuilderSql.append(" Where P.idParticipantRequestPk = :idParticipantRequestPrm");
			 
			  TypedQuery<Integer> query = em.createQuery(stringBuilderSql.toString(),Integer.class);
			  query.setParameter("idParticipantRequestPrm", idParticipantRequestPk);	
			  			 
			  return (Integer)query.getSingleResult();
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}

	/**
	 * List participant int depo history by request service bean.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantIntDepoHistory> listParticipantIntDepoHistoryByRequest(
			Long idParticipantRequestPk, Integer indNew) throws ServiceException {
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT P FROM ParticipantIntDepoHistory P ");		  
		  stringBuilderSql.append(" JOIN Fetch P.internationalDepository IDEP ");
		  stringBuilderSql.append(" Where P.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm");
		  stringBuilderSql.append(" And P.indNew = :indNewPrm ");
		 
		  TypedQuery<ParticipantIntDepoHistory> query = em.createQuery(stringBuilderSql.toString(),ParticipantIntDepoHistory.class);
		  query.setParameter("idParticipantRequestPrm", idParticipantRequestPk);
		  query.setParameter("indNewPrm", indNew);
		  			 
		  return (List<ParticipantIntDepoHistory>) query.getResultList();
	}

	/**
	 * Find participant int depo by participant and depo service bean.
	 *
	 * @param participantIntDepositaryFound the participant int depositary found
	 * @return the participant int depositary
	 * @throws ServiceException the service exception
	 */
	public ParticipantIntDepositary findParticipantIntDepoByParticipantAndDepoServiceBean(
			ParticipantIntDepositary participantIntDepositaryFound) throws ServiceException{
		try {
			StringBuilder stringBuilderSql = new StringBuilder();			
			  stringBuilderSql.append("SELECT P FROM ParticipantIntDepositary P ");
			  stringBuilderSql.append(" Where P.id.idParticipantPk = :idParticipantPrm ");
			  stringBuilderSql.append(" And P.id.idInternationalDepositoryPk = :idInternationalDepositaryPrm ");
			 
			  TypedQuery<ParticipantIntDepositary> query = em.createQuery(stringBuilderSql.toString(),ParticipantIntDepositary.class);
			  query.setParameter("idParticipantPrm", participantIntDepositaryFound.getParticipant().getIdParticipantPk());
			  query.setParameter("idInternationalDepositaryPrm", participantIntDepositaryFound.getInternationalDepository().getIdInternationalDepositoryPk());
			  			 
			  return (ParticipantIntDepositary) query.getSingleResult();
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}

	/**
	 * Update state participant int depositary service bean.
	 *
	 * @param participantIntDepoHistoryUpdate the participant int depo history update
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStateParticipantIntDepositaryServiceBean(
			ParticipantIntDepositary participantIntDepoHistoryUpdate,
			LoggerUser loggerUser) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update ParticipantIntDepositary P ");
		sbQuery.append(" Set P.stateIntDepositary = :statePrm, P.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" P.lastModifyDate = :lastModifyDatePrm, P.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" P.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where P.id.idParticipantPk = :idParticipantPrm ");
		sbQuery.append(" And P.id.idInternationalDepositoryPk = :idInternationalDepositaryPrm ");

		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",participantIntDepoHistoryUpdate.getStateIntDepositary());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idParticipantPrm", participantIntDepoHistoryUpdate.getParticipant().getIdParticipantPk());
		query.setParameter("idInternationalDepositaryPrm", participantIntDepoHistoryUpdate.getInternationalDepository().getIdInternationalDepositoryPk());

		return query.executeUpdate();
	}

	/**
	 * Disable participant int depositaries not in list service bean.
	 *
	 * @param participant the participant
	 * @param participantIntDepoHistories the participant int depo histories
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableParticipantIntDepositariesNotInListServiceBean(
			Participant participant,
			List<ParticipantIntDepoHistory> participantIntDepoHistories,
			LoggerUser loggerUser) throws ServiceException{
		
		List<Long> lstIdInternationalDepositary = new ArrayList<Long>();
		for (ParticipantIntDepoHistory participantIntDepoHistory : participantIntDepoHistories) {
			lstIdInternationalDepositary.add(participantIntDepoHistory.getInternationalDepository().getIdInternationalDepositoryPk());
		}
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update ParticipantIntDepositary P ");
		sbQuery.append(" Set P.stateIntDepositary = :statePrm, P.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" P.lastModifyDate = :lastModifyDatePrm, P.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" P.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where P.id.idParticipantPk = :idParticipantPrm ");
		sbQuery.append(" And P.id.idInternationalDepositoryPk not in (:lstIdInternationalDepositaryPrm) ");

		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",ParticipantIntDepositaryStateType.DELETED.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());
		query.setParameter("lstIdInternationalDepositaryPrm", lstIdInternationalDepositary);

		return query.executeUpdate();
	}

	/**
	 * List participant mech history by request service bean.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantMechanismHistory> listParticipantMechHistoryByRequest(
			Long idParticipantRequestPk, Integer indNew) throws ServiceException {
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT P FROM ParticipantMechanismHistory P ");		  
		  stringBuilderSql.append(" JOIN Fetch P.mechanismModality MechMod ");
		  stringBuilderSql.append(" Where P.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm");
		  stringBuilderSql.append(" And P.indNew = :indNewPrm ");
		 
		  TypedQuery<ParticipantMechanismHistory> query = em.createQuery(stringBuilderSql.toString(),ParticipantMechanismHistory.class);
		  query.setParameter("idParticipantRequestPrm", idParticipantRequestPk);
		  query.setParameter("indNewPrm", indNew);
		  			 
		  return (List<ParticipantMechanismHistory>) query.getResultList();
	}

	/**
	 * Disable participant mechanisms state service bean.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableParticipantMechanismsStateServiceBean(
			Participant participant, LoggerUser loggerUser) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update ParticipantMechanism P ");
		sbQuery.append(" Set P.stateParticipantMechanism = :statePrm, P.indPurchaseRole = :indPurchaseRolePrm, P.indSaleRole = :indSaleRolePrm, P.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" P.lastModifyDate = :lastModifyDatePrm, P.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" P.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where P.participant.idParticipantPk = :idParticipantPrm ");		

		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",ParticipantMechanismStateType.DELETED.getCode());
		query.setParameter("indPurchaseRolePrm",BooleanType.NO.getCode());
		query.setParameter("indSaleRolePrm",BooleanType.NO.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());		

		return query.executeUpdate();
	}

	/**
	 * Gets the list active negotiation modalities.
	 *
	 * @return the list active negotiation modalities
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getListActiveNegotiationModalities() throws ServiceException{
		StringBuilder sb=new StringBuilder("Select distinct nm from	NegotiationModality nm");
		sb.append(" join Fetch nm.mechanismModalities mm");
		sb.append(" Where nm.modalityState = :modalityStatePrm And mm.stateMechanismModality = :stateMechanismModalityPrm");		
		sb.append(" Order By nm.modalityCode");
		
		TypedQuery<NegotiationModality> query=em.createQuery( sb.toString() , NegotiationModality.class);
		query.setParameter("modalityStatePrm", NegotiationModalityStateType.ACTIVE.getCode());
		query.setParameter("stateMechanismModalityPrm", MechanismModalityStateType.ACTIVE.getCode());		
		
		return query.getResultList();
	}

	/**
	 * Exist participant bic code.
	 *
	 * @param bicCode the bic code
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant existParticipantBicCode(String bicCode, Long idParticipantPk) throws ServiceException {
		StringBuilder sb = new StringBuilder("Select P.idParticipantPk, P.description, P.bicCode from Participant P ");
		sb.append(" Where P.bicCode = :bicCodePrm ");
		
		if(Validations.validateIsNotNullAndPositive(idParticipantPk)){
			sb.append(" And P.idParticipantPk != :idParticipantPrm ");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("bicCodePrm", bicCode);
		if(Validations.validateIsNotNullAndPositive(idParticipantPk)){
			query.setParameter("idParticipantPrm", idParticipantPk);
		}
		
		List<Object[]> lstResult = query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)) {
			Participant objParticipant = new Participant();
			objParticipant.setIdParticipantPk((Long)lstResult.get(0)[0]);
			objParticipant.setDescription((String)lstResult.get(0)[1]);
			objParticipant.setBicCode((String)lstResult.get(0)[2]);
			
			return objParticipant;
		} else {
			return null;
		}
	}

	/**
	 * Exist participant modif request bic code.
	 *
	 * @param bicCode the bic code
	 * @param idParticipantPk the id participant pk
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequest existParticipantModifRequestBicCode(String bicCode, Long idParticipantPk) throws ServiceException {
		StringBuilder sb = new StringBuilder("Select PR.idParticipantRequestPk from ParticipantRequestHistory PRH ");
		sb.append(" Join PRH.participantRequest PR ");
		sb.append(" Where PRH.bicCode = :bicCodePrm And PR.stateRequest = :stateParticipantRequestPrm ");
		sb.append(" And PR.requestType = :requestTypePrm ");
		
		if(Validations.validateIsNotNullAndPositive(idParticipantPk)){
			sb.append(" And PR.participant.idParticipantPk != :idParticipantPrm ");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("bicCodePrm", bicCode);
		query.setParameter("stateParticipantRequestPrm", ParticipantRequestStateType.REGISTERED.getCode());
		query.setParameter("requestTypePrm", ParticipantRequestType.MODIFICATION.getCode());
		if(Validations.validateIsNotNullAndPositive(idParticipantPk)){
			query.setParameter("idParticipantPrm", idParticipantPk);
		}
		
		List<Long> lstResult = query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)) {
			ParticipantRequest objParticipantRequest = new ParticipantRequest();
			objParticipantRequest.setIdParticipantRequestPk(lstResult.get(0));			
			
			return objParticipantRequest;
		} else {
			return null;
		}
	}

	/**
	 * Exist bank bic code.
	 *
	 * @param bicCode the bic code
	 * @return the bank
	 * @throws ServiceException the service exception
	 */
	public Bank existBankBicCode(String bicCode) throws ServiceException{
		StringBuilder sb = new StringBuilder("Select  ");
		sb.append(" B.idBankPk, B.description, B.state, B.documentType, ").
		append(" B.documentNumber, B.bicCode, B.country.idGeographicLocationPk ").
		append(" From Bank B ").
		append(" Where B.bicCode = :bicCodePrm ");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("bicCodePrm", bicCode);
		
		List<Object[]> lstResult = query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)) {
			GeographicLocation country = new GeographicLocation();
			Bank objBank = new Bank();
			objBank.setIdBankPk((Long)lstResult.get(0)[0]);
			objBank.setDescription((String)lstResult.get(0)[1]);
			objBank.setState((Integer)lstResult.get(0)[2]);
			objBank.setDocumentType((Long)lstResult.get(0)[3]);
			objBank.setDocumentNumber((String)lstResult.get(0)[4]);
			objBank.setBicCode((String)lstResult.get(0)[5]);
			
			country.setIdGeographicLocationPk((Integer)lstResult.get(0)[6]);
			objBank.setCountry(country);
			
			return objBank;
		} else {
			return null;
		}
	}

	/**
	 * List institution bank account central.
	 *
	 * @param providerBank the provider bank
	 * @param clientBank the client bank
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionBankAccount> listInstitutionBankAccountCentral(
			Bank providerBank, Bank clientBank) throws ServiceException {
		StringBuilder sb = new StringBuilder("Select IBA ");
		sb.append(" From InstitutionBankAccount IBA  ").
		append(" Where IBA.providerBank.idBankPk = :idProviderBankPkPrm ").
		append(" and IBA.clientBank.idBankPk = :idClientBankPkPrm ").
		append(" and IBA.state = :stateBankAccountPrm ");
		
		TypedQuery<InstitutionBankAccount> query = em.createQuery(sb.toString(), InstitutionBankAccount.class);
		query.setParameter("idProviderBankPkPrm", providerBank.getIdBankPk());
		query.setParameter("idClientBankPkPrm", clientBank.getIdBankPk());
		query.setParameter("stateBankAccountPrm", Long.valueOf(CashAccountStateType.ACTIVATE.getCode()));
		
		return query.getResultList();		
	}
	
	/**
	 * Find provider bank.
	 *
	 * @return the bank
	 * @throws ServiceException the service exception
	 */
	public Bank findProviderBank() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("select b from Bank b where b.bankType = :bankTypePrm and b.state = :statePrm ");		

		TypedQuery<Bank> query = em.createQuery(sbQuery.toString(), Bank.class);
		
		query.setParameter("bankTypePrm", BankType.CENTRALIZING.getCode());
		query.setParameter("statePrm", MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
		
		List<Bank> lstResult = query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			return lstResult.get(0);
		} else {
			return null;
		}
	}

	/**
	 * List participant bank account.
	 *
	 * @param participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantBankAccount> listParticipantBankAccount(Participant participant) throws ServiceException{
		StringBuilder sb = new StringBuilder("Select PBA ");
		sb.append(" From ParticipantBankAccount PBA  ").
		append(" Join Fetch PBA.bank B ").
		append(" Where PBA.participant.idParticipantPk = :idParticipantPrm ").
		append(" and PBA.accountState = :accountStatePrm ");
		
		TypedQuery<ParticipantBankAccount> query = em.createQuery(sb.toString(), ParticipantBankAccount.class);
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());		
		query.setParameter("accountStatePrm", ParticipantBankAccountStateType.REGISTERED.getCode());
		
		return query.getResultList();
	}

	/**
	 * Validate exists holder account bank.
	 *
	 * @param bicCode the bic code
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsHolderAccountBank(String bicCode) throws ServiceException{
		
		Bank centralizingBank = findProviderBank();
		
		StringBuilder sb = new StringBuilder("Select Count(HAB) ");
		sb.append(" From HolderAccountBank HAB  ").
		append(" Where HAB.bank.idBankPk = :idBankPkPrm ").
		append(" And HAB.bicCode = :bicCodePrm And HAB.currency in (:currencyUsdPrm,:currencyDopPrm)");	
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("idBankPkPrm", centralizingBank.getIdBankPk());
		query.setParameter("bicCodePrm", bicCode);
		query.setParameter("currencyUsdPrm", CurrencyType.USD.getCode());
		query.setParameter("currencyDopPrm", CurrencyType.PYG.getCode());
		
		int resultCount = Integer.parseInt(query.getSingleResult().toString());
		
		return resultCount > 0;	
	}

	/**
	 * List participant bank account.
	 *
	 * @param bicCode the bic code
	 * @param providerBank the provider bank
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBank> listParticipantBankAccount(String bicCode,
			Bank providerBank) throws ServiceException{
		StringBuilder sb = new StringBuilder("Select HAB ");
		sb.append(" From HolderAccountBank HAB  ").
		append(" Where HAB.bank.idBankPk = :idBankPkPrm ").
		append(" And HAB.bicCode = :bicCodePrm And HAB.currency in (:currencyUsdPrm,:currencyDopPrm) ").
		append(" Order By HAB.holderAccount.idHolderAccountPk ");
		
		
		TypedQuery<HolderAccountBank> query = em.createQuery(sb.toString(), HolderAccountBank.class);
		query.setParameter("idBankPkPrm", providerBank.getIdBankPk());
		query.setParameter("bicCodePrm", bicCode);
		query.setParameter("currencyUsdPrm", CurrencyType.USD.getCode());
		query.setParameter("currencyDopPrm", CurrencyType.PYG.getCode());
		
		return query.getResultList();
	}

	/**
	 * Find holder by bic code.
	 *
	 * @param bicCode the bic code
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderByBicCode(String bicCode) throws ServiceException{
		
		Bank centralizingBank = findProviderBank();
		
		StringBuilder sb = new StringBuilder("Select H.idHolderPk, H.fullName, H.documentType, H.documentNumber, H.legalResidenceCountry ");
		sb.append(" From HolderAccountBank HAB ").
		append(" Join HAB.holder H  ").
		append(" Where HAB.bank.idBankPk = :idBankPkPrm ").
		append(" And HAB.bicCode = :bicCodePrm And HAB.currency in (:currencyUsdPrm,:currencyDopPrm) ").
		append(" Order By HAB.holderAccount.idHolderAccountPk ");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("idBankPkPrm", centralizingBank.getIdBankPk());
		query.setParameter("bicCodePrm", bicCode);
		query.setParameter("currencyUsdPrm", CurrencyType.USD.getCode());
		query.setParameter("currencyDopPrm", CurrencyType.PYG.getCode());
		
		List<Object[]> lstResult =  query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			Holder objHolder = new Holder();
			objHolder.setIdHolderPk((Long)lstResult.get(0)[0]);
			objHolder.setFullName((String)lstResult.get(0)[1]);		
			objHolder.setDocumentType((Integer)lstResult.get(0)[2]);
			objHolder.setDocumentNumber((String)lstResult.get(0)[3]);
			objHolder.setLegalResidenceCountry((Integer)lstResult.get(0)[4]);	
			return objHolder;
		} else {
			return null;
		}
	}	

	/**
	 * Find bic code participant.
	 *
	 * @param participant the participant
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String findBicCodeParticipant(Participant participant) throws ServiceException{
		StringBuilder sb = new StringBuilder("Select P.bicCode ");
		sb.append(" From Participant P ").		
		append(" Where P.idParticipantPk = :idParticipantPrm ");
		
		TypedQuery<String> query = em.createQuery(sb.toString(), String.class);
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());	
		
		return query.getSingleResult();
	}
	
	/**
	 * Find bic code partipant request hist.
	 *
	 * @param participantRequest the participant request
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String findBicCodePartipantRequestHist(ParticipantRequest participantRequest) throws ServiceException{
		StringBuilder sb = new StringBuilder("Select P.bicCode ");
		sb.append(" From ParticipantRequestHistory P ").		
		append(" Where P.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm ");
		
		TypedQuery<String> query = em.createQuery(sb.toString(), String.class);
		query.setParameter("idParticipantRequestPrm", participantRequest.getIdParticipantRequestPk());	
		
		return query.getSingleResult();
	}
	
	/**
	 * Validate bank by document type and number.
	 *
	 * @param objBank the obj bank
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateBankByDocumentTypeAndNumber(Bank objBank) throws ServiceException {
		StringBuilder sb = new StringBuilder("Select Count(B) ");
		sb.append(" From Bank B ").		
		append(" Where B.documentType = :documentTypePrm ").
		append(" And B.documentNumber = :documentNumberPrm ");
		
		if(Validations.validateIsNotNull(objBank.getCountry())){
			sb.append(" And B.country.idGeographicLocationPk = :countryPrm ");
		}
		
		Query query = em.createQuery(sb.toString());
		
		query.setParameter("documentTypePrm", objBank.getDocumentType());
		query.setParameter("documentNumberPrm", objBank.getDocumentNumber());
		
		if(Validations.validateIsNotNull(objBank.getCountry())){
			query.setParameter("countryPrm", objBank.getCountry().getIdGeographicLocationPk());
		}
		
		int countResult = Integer.parseInt(query.getSingleResult().toString());
		
		return countResult > 0;
	}

	/**
	 * Validate exists holder account bank by document and bic.
	 *
	 * @param holder the holder
	 * @param bicCode the bic code
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsHolderAccountBankByDocumentAndBic(Holder holder, String bicCode) throws ServiceException{
		
		Bank centralizingBank = findProviderBank();
		
		StringBuilder sb = new StringBuilder("Select Count(HAB) ");
		sb.append(" From HolderAccountBank HAB  ").
		append(" Join HAB.holderAccount HA  ").
		append(" Where HAB.bank.idBankPk = :idBankPkPrm ").
		append(" And HAB.holder.idHolderPk = :idHolderPkPrm And HAB.currency in (:currencyUsdPrm,:currencyDopPrm) ").
		append(" And HA.stateAccount in (:stateAccRegisteredPrm,:stateAccBlockedPrm) ").
		append(" And HAB.bicCode = :bicCodePrm ");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("idBankPkPrm", centralizingBank.getIdBankPk());
		query.setParameter("idHolderPkPrm", holder.getIdHolderPk());
		query.setParameter("currencyUsdPrm", CurrencyType.USD.getCode());
		query.setParameter("currencyDopPrm", CurrencyType.PYG.getCode());
		query.setParameter("stateAccRegisteredPrm", HolderAccountStatusType.ACTIVE.getCode());
		query.setParameter("stateAccBlockedPrm", HolderAccountStatusType.BLOCK.getCode());
		query.setParameter("bicCodePrm", bicCode);
		
		int resultCount = Integer.parseInt(query.getSingleResult().toString());
		
		return resultCount > 0;
	}
	
	/**
	 * Find bank by document type and number.
	 *
	 * @param objBank the obj bank
	 * @return the bank
	 * @throws ServiceException the service exception
	 */
	public Bank findBankByDocumentTypeAndNumber(Bank objBank) throws ServiceException{
		StringBuilder sb = new StringBuilder("Select B From Bank B ");
		sb.append(" Where B.documentType = :documentTypePrm ").
		append(" And B.documentNumber = :documentNumberPrm ");
		
		if(Validations.validateIsNotNull(objBank.getCountry())){
			sb.append(" And B.country.idGeographicLocationPk = :countryPrm ");
		}
		
		TypedQuery<Bank> query = em.createQuery(sb.toString(),Bank.class);
		
		query.setParameter("documentTypePrm", objBank.getDocumentType());
		query.setParameter("documentNumberPrm", objBank.getDocumentNumber());
		
		if(Validations.validateIsNotNull(objBank.getCountry())){
			query.setParameter("countryPrm", objBank.getCountry().getIdGeographicLocationPk());
		}
		
		List<Bank> lstResult = query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			return lstResult.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Update bic code bank.
	 *
	 * @param objBank the obj bank
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateBicCodeBank(Bank objBank, LoggerUser loggerUser) throws ServiceException{
		StringBuilder sb = new StringBuilder("Update Bank B ");
		sb.append(" Set B.bicCode = :bicCodePrm, ").
		append(" B.lastModifyUser = :lastModifyUserPrm, ").
		append(" B.lastModifyDate = :lastModifyDatePrm, ").
		append(" B.lastModifyIp = :lastModifyIpPrm, ").
		append(" B.lastModifyApp = :lastModifyAppPrm ").
		append(" Where B.idBankPk = :idBankPkPrm ");
		
		Query query = em.createQuery(sb.toString());
		
		query.setParameter("bicCodePrm", objBank.getBicCode());
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idBankPkPrm", objBank.getIdBankPk());
		
		return query.executeUpdate();
	}
	
	/**
	 * Disable institution bank accounts.
	 *
	 * @param objBank the obj bank
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableInstitutionBankAccounts(Bank objBank, LoggerUser loggerUser) throws ServiceException{
		StringBuilder sb = new StringBuilder("Update InstitutionBankAccount IBA ");
		sb.append(" Set IBA.state = :statePrm, ").
		append(" IBA.lastModifyUser = :lastModifyUserPrm, ").
		append(" IBA.lastModifyDate = :lastModifyDatePrm, ").
		append(" IBA.lastModifyIp = :lastModifyIpPrm, ").
		append(" IBA.lastModifyApp = :lastModifyAppPrm ").
		append(" Where IBA.clientBank.idBankPk = :idBankPkPrm ");
		
		Query query = em.createQuery(sb.toString());
		
		query.setParameter("statePrm", Long.valueOf(CashAccountStateType.BLOCKED.getCode()));
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idBankPkPrm", objBank.getIdBankPk());
		
		return query.executeUpdate();
	}

	/**
	 * Find holder by participant.
	 *
	 * @param participant the participant
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderByParticipant(Participant participant) throws ServiceException{
		StringBuilder sb = new StringBuilder("Select H.idHolderPk, H.fullName, H.documentType, H.documentNumber, H.legalResidenceCountry ");
		sb.append(" From Participant P ").
		append(" Join P.holder H  ").
		append(" Where P.idParticipantPk = :idParticipantPrm ");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("idParticipantPrm", participant.getIdParticipantPk());		
		
		List<Object[]> lstResult =  query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			Holder objHolder = new Holder();
			objHolder.setIdHolderPk((Long)lstResult.get(0)[0]);
			objHolder.setFullName((String)lstResult.get(0)[1]);		
			objHolder.setDocumentType((Integer)lstResult.get(0)[2]);
			objHolder.setDocumentNumber((String)lstResult.get(0)[3]);
			objHolder.setLegalResidenceCountry((Integer)lstResult.get(0)[4]);	
			return objHolder;
		} else {
			return null;
		}
	}
	
	/**
	 * List holder account info by document and bic.
	 *
	 * @param holder the holder
	 * @param centralizingBank the centralizing bank
	 * @param bicCode the bic code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> listHolderAccountInfoByDocumentAndBic(Holder holder, Bank centralizingBank, String bicCode) throws ServiceException{
		
		List<Long> lstHolderAccountPks = listHolderAccountIdsByDocumentAndBic(holder, centralizingBank, bicCode);
		
		StringBuilder sb = new StringBuilder("Select distinct HA ");
		sb.append(" From HolderAccount HA  ").
		append(" Join Fetch HA.participant P ").
		append(" Where HA.idHolderAccountPk in (:idHolderAccountListPrm) ");
		
		TypedQuery<HolderAccount> query = em.createQuery(sb.toString(), HolderAccount.class);
		query.setParameter("idHolderAccountListPrm", lstHolderAccountPks);
		
		List<HolderAccount> lstHolderAccountResult = query.getResultList();
		
		for (HolderAccount holderAccount : lstHolderAccountResult) {
			holderAccount.getHolderAccountFiles().size();
			for (HolderAccountFile holderAccountFile : holderAccount.getHolderAccountFiles()) {
				holderAccountFile.getDocumentFile();
			}
			
			holderAccount.getHolderAccountDetails().size();
		}
		
		return lstHolderAccountResult;
	}
	
	/**
	 * List holder account ids by document and bic.
	 *
	 * @param holder the holder
	 * @param centralizingBank the centralizing bank
	 * @param bicCode the bic code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Long> listHolderAccountIdsByDocumentAndBic(Holder holder, Bank centralizingBank, String bicCode) throws ServiceException{
		
		StringBuilder sb = new StringBuilder("Select distinct HA.idHolderAccountPk ");
		sb.append(" From HolderAccountBank HAB  ").
		append(" Join HAB.holderAccount HA  ").
		append(" Where HAB.bank.idBankPk = :idBankPkPrm ").
		append(" And HAB.holder.idHolderPk = :idHolderPkPrm And HAB.currency in (:currencyUsdPrm,:currencyDopPrm) ").
		append(" And HA.stateAccount in (:stateAccRegisteredPrm,:stateAccBlockedPrm) ").
		append(" And HAB.bicCode = :bicCodePrm ");
		
		TypedQuery<Long> query = em.createQuery(sb.toString(), Long.class);
		query.setParameter("idBankPkPrm", centralizingBank.getIdBankPk());
		query.setParameter("idHolderPkPrm", holder.getIdHolderPk());
		query.setParameter("currencyUsdPrm", CurrencyType.USD.getCode());
		query.setParameter("currencyDopPrm", CurrencyType.PYG.getCode());
		query.setParameter("stateAccRegisteredPrm", HolderAccountStatusType.ACTIVE.getCode());
		query.setParameter("stateAccBlockedPrm", HolderAccountStatusType.BLOCK.getCode());
		query.setParameter("bicCodePrm", bicCode);
		
		return query.getResultList();
	}	
	
	/**
	 * Update participant information.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateParticipantInformation(Participant participant, LoggerUser loggerUser) 
			throws ServiceException{
		StringBuilder sb = new StringBuilder("Update Participant P ");
		sb.append(" Set ").
		append(" P.mnemonic = :mnemonicPrm, ").
		append(" P.description = :descriptionPrm, ").
		append(" P.documentType = :documentTypePrm, ").
		append(" P.documentNumber = :documentNumberPrm, ").
		append(" P.accountType = :accountTypePrm, ").
		append(" P.accountClass = :accountClassPrm, ").
		append(" P.contractNumber = :contractNumberPrm, ").
		append(" P.contractDate = :contractDatePrm, ").
		append(" P.commercialRegister = :commercialRegisterPrm, ").
		append(" P.superintendentResolution = :superintendentResolutionPrm, ").
		append(" P.superintendentResolutionDate = :superintendentResolutionDatePrm, ").
		append(" P.socialCapital = :socialCapitalPrm, ").
		append(" P.residenceCountry = :residenceCountryPrm, ").
		append(" P.department = :departmentPrm, ").
		append(" P.province = :provincePrm, ").
		append(" P.district = :districtPrm, ").
		append(" P.address = :addressPrm, ").		
		append(" P.phoneNumber = :phoneNumberPrm, ").
		append(" P.faxNumber = :faxNumberPrm, ").
		append(" P.website = :websitePrm, ").
		append(" P.email = :emailPrm, ").
		append(" P.contactPhone = :contactPhonePrm, ").
		append(" P.contactName = :contactNamePrm, ").
		append(" P.comments = :commentsPrm, ").
		append(" P.state = :statePrm, ").
		append(" P.creationDate = :creationDatePrm, ").
		append(" P.economicSector = :economicSectorPrm, ").
		append(" P.economicActivity = :economicActivityPrm, ").		
		append(" P.currency = :currencyPrm, ").
		append(" P.contactEmail = :contactEmailPrm, ").
		append(" P.roleParticipant = :roleParticipantPrm, ").
		append(" P.indPayCommission = :indPayCommissionPrm, ").
		append(" P.indDepositary = :indDepositaryPrm, ").
		append(" P.indManagedThird = :indManagedThirdPrm, ").
		append(" P.bicCode = :bicCodePrm, ").
		append(" P.lastModifyUser = :lastModifyUserPrm, ").
		append(" P.lastModifyDate = :lastModifyDatePrm, ").
		append(" P.lastModifyIp = :lastModifyIpPrm, ").
		append(" P.lastModifyApp = :lastModifyAppPrm ").
		append(" Where P.idParticipantPk = :idParticipantPkPrm ");
		
		Query query = em.createQuery(sb.toString());

		query.setParameter("mnemonicPrm", participant.getMnemonic());
		query.setParameter("descriptionPrm", participant.getDescription());
		query.setParameter("documentTypePrm", participant.getDocumentType());
		query.setParameter("documentNumberPrm", participant.getDocumentNumber());
		query.setParameter("accountTypePrm", participant.getAccountType());
		query.setParameter("accountClassPrm", participant.getAccountClass());
		query.setParameter("contractNumberPrm", participant.getContractNumber());
		query.setParameter("contractDatePrm", participant.getContractDate());
		query.setParameter("commercialRegisterPrm", participant.getCommercialRegister());
		query.setParameter("superintendentResolutionPrm", participant.getSuperintendentResolution());
		query.setParameter("superintendentResolutionDatePrm", participant.getSuperintendentResolutionDate());
		query.setParameter("socialCapitalPrm", participant.getSocialCapital());
		query.setParameter("residenceCountryPrm", participant.getResidenceCountry());
		query.setParameter("departmentPrm", participant.getDepartment());
		query.setParameter("provincePrm", participant.getProvince());
		query.setParameter("districtPrm", participant.getDistrict());
		query.setParameter("addressPrm", participant.getAddress());
		query.setParameter("phoneNumberPrm", participant.getPhoneNumber());
		query.setParameter("faxNumberPrm", participant.getFaxNumber());
		query.setParameter("websitePrm", participant.getWebsite());
		query.setParameter("emailPrm", participant.getEmail());
		query.setParameter("contactPhonePrm", participant.getContactPhone());
		query.setParameter("contactNamePrm", participant.getContactName());
		query.setParameter("commentsPrm", participant.getComments());
		query.setParameter("statePrm", participant.getState());
		query.setParameter("creationDatePrm", participant.getCreationDate());
		query.setParameter("economicSectorPrm", participant.getEconomicSector());
		query.setParameter("economicActivityPrm", participant.getEconomicActivity());
		query.setParameter("currencyPrm", participant.getCurrency());
		query.setParameter("contactEmailPrm", participant.getContactEmail());
		query.setParameter("roleParticipantPrm", participant.getRoleParticipant());
		query.setParameter("indPayCommissionPrm", participant.getIndPayCommission());
		query.setParameter("indDepositaryPrm", participant.getIndDepositary());
		query.setParameter("indManagedThirdPrm", participant.getIndManagedThird());
		query.setParameter("bicCodePrm", participant.getBicCode());		
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idParticipantPkPrm", participant.getIdParticipantPk());
		
		query.executeUpdate();
	}

	/**
	 * Find pep person by id legal representative service facade.
	 *
	 * @param idLegalRepresentativePk the id legal representative pk
	 * @return the pep person
	 * @throws ServiceException the service exception
	 */
	public PepPerson findPepPersonByIdLegalRepresentativeServiceFacade(
			Long idLegalRepresentativePk) throws ServiceException{
		
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT p.idPepPersonPk FROM PepPerson p WHERE p.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePk");	      
		  
		  TypedQuery<Long> query = em.createQuery(stringBuilderSql.toString(), Long.class);
		  query.setParameter("idLegalRepresentativePk", idLegalRepresentativePk);	 
	      
		  List<Long> lstResult =  query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)) {
			PepPerson pepPerson = new PepPerson();
			pepPerson.setIdPepPersonPk(lstResult.get(0));
			return pepPerson;
		} else {
			return null;
		}
	}

	/**
	 * Update pep person information.
	 *
	 * @param pepPerson the pep person
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updatePepPersonInformation(PepPerson pepPerson,
			LoggerUser loggerUser) throws ServiceException{
		StringBuilder sb = new StringBuilder("Update PepPerson P ");
		sb.append(" Set  ").
		append(" P.category = :categoryPrm, ").
		append(" P.comments = :commentsPrm, ").
		append(" P.beginingPeriod = :beginingPeriodPrm, ").
		append(" P.endingPeriod = :endingPeriodPrm, ").
		append(" P.role = :rolePrm, ").
		append(" P.notificationDate = :notificationDatePrm, ").		
		append(" P.geographicLocation.idGeographicLocationPk = :countryPrm, ").		
		append(" P.lastModifyUser = :lastModifyUserPrm, ").
		append(" P.lastModifyDate = :lastModifyDatePrm, ").
		append(" P.lastModifyIp = :lastModifyIpPrm, ").
		append(" P.lastModifyApp = :lastModifyAppPrm ").
		append(" Where P.idPepPersonPk = :idPepPersonPkPrm ");
		
		Query query = em.createQuery(sb.toString());
		
		query.setParameter("categoryPrm", pepPerson.getCategory());
		query.setParameter("commentsPrm", pepPerson.getComments());
		query.setParameter("beginingPeriodPrm", pepPerson.getBeginingPeriod());
		query.setParameter("endingPeriodPrm", pepPerson.getEndingPeriod());
		query.setParameter("rolePrm", pepPerson.getRole());
		query.setParameter("notificationDatePrm", pepPerson.getNotificationDate());
		query.setParameter("countryPrm", pepPerson.getGeographicLocation().getIdGeographicLocationPk());
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idPepPersonPkPrm", pepPerson.getIdPepPersonPk());
		
		query.executeUpdate();
		
	}
	
	/**
	 * Prepare and save participant history.
	 *
	 * @param participantRequest the participant request
	 * @param participant the participant
	 * @param indNew the ind new
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void prepareAndSaveParticipantHistory(ParticipantRequest participantRequest, Participant participant, Integer indNew, LoggerUser loggerUser)
			throws ServiceException{
		ParticipantRequestHistory participantRequestHistory = new ParticipantRequestHistory();
		participantRequestHistory.setParticipant(participant);
		participantRequestHistory.setParticipantRequest(participantRequest);
		participantRequestHistory.setMnemonic(participant.getMnemonic());
		participantRequestHistory.setDescription(participant.getDescription());
		participantRequestHistory.setDocumentType(participant.getDocumentType());
		participantRequestHistory.setDocumentNumber(participant.getDocumentNumber());
		participantRequestHistory.setDocumentSource(participant.getDocumentSource());
		participantRequestHistory.setAccountType(participant.getAccountType());
		participantRequestHistory.setAccountClass(participant.getAccountClass());
		participantRequestHistory.setContractNumber(participant.getContractNumber());
		participantRequestHistory.setContractDate(participant.getContractDate());
		participantRequestHistory.setCommercialRegister(participant.getCommercialRegister());
		participantRequestHistory.setSuperintendentResolution(participant.getSuperintendentResolution());
		participantRequestHistory.setSuperintendentResolutionDate(participant.getSuperintendentResolutionDate());
		participantRequestHistory.setSocialCapital(participant.getSocialCapital());
		participantRequestHistory.setResidenceCountry(participant.getResidenceCountry());
		participantRequestHistory.setDepartment(participant.getDepartment()==null?0:participant.getDepartment());
		participantRequestHistory.setProvince(participant.getProvince());
		participantRequestHistory.setDistrict(participant.getDistrict());
		participantRequestHistory.setAddress(participant.getAddress());
		participantRequestHistory.setPhoneNumber(participant.getPhoneNumber());
		participantRequestHistory.setFaxNumber(participant.getFaxNumber());
		participantRequestHistory.setWebsite(participant.getWebsite());
		participantRequestHistory.setEmail(participant.getEmail());
		participantRequestHistory.setContactPhone(participant.getContactPhone());
		participantRequestHistory.setContactName(participant.getContactName());
		participantRequestHistory.setComments(participant.getComments());
		participantRequestHistory.setParticipantState(participant.getState());
		participantRequestHistory.setCreationDate(participant.getCreationDate());		
		participantRequestHistory.setEconomicSector(participant.getEconomicSector());		
		participantRequestHistory.setEconomicActivity(participant.getEconomicActivity());
		participantRequestHistory.setInvestorType(participant.getInvestorType());
		participantRequestHistory.setContactPhone(participant.getContactPhone());
		participantRequestHistory.setCurrency(participant.getCurrency());
		participantRequestHistory.setContactEmail(participant.getContactEmail());
		participantRequestHistory.setRoleParticipant(participant.getRoleParticipant());
		participantRequestHistory.setIndPayCommission(participant.getIndPayCommission());
		participantRequestHistory.setIndDepositary(participant.getIndDepositary());
		participantRequestHistory.setIndManagedThird(participant.getIndManagedThird());
		participantRequestHistory.setBicCode(participant.getBicCode());
		participantRequestHistory.setRegistryUser(loggerUser.getUserName());
		participantRequestHistory.setRegistryDate(CommonsUtilities.currentDateTime());
		participantRequestHistory.setIndSettlementIncharge(participant.getIndSettlementIncharge());
		participantRequestHistory.setIndNew(indNew);	
		participantRequestHistory.setIdBcbCode(participant.getIdBcbCode());
		participantRequestHistory.setAccountsState(participant.getAccountsState());
		if(Validations.validateIsNotNullAndNotEmpty(participant.getIssuer()))
		participantRequestHistory.setIssuerFk(participant.getIssuer().getIdIssuerPk());
		
		//participantRequestHistory.setDepartment(null);
		
		create(participantRequestHistory);
	}
	
	/**
	 * Prepare participant files hist.
	 *
	 * @param participantRequest the participant request
	 * @param participant the participant
	 * @param indNew the ind new
	 * @param loggerUser the logger user
	 * @param indNullList the ind null list
	 * @throws ServiceException the service exception
	 */
	public void prepareParticipantFilesHist(ParticipantRequest participantRequest,  Participant participant,
			Integer indNew, LoggerUser loggerUser, Boolean indNullList) throws ServiceException{
		if(Validations.validateListIsNotNullAndNotEmpty(participant.getParticipantFiles())){
			List<ParticipantFileHistory> participantRequestFiles = new ArrayList<ParticipantFileHistory>();
			ParticipantFileHistory participantFileHistoryRequest;
			for (ParticipantFile participantFile : participant.getParticipantFiles()) {
				participantFileHistoryRequest = new ParticipantFileHistory();
				
				participantFileHistoryRequest.setDocumentType(participantFile.getDocumentType());				
				participantFileHistoryRequest.setDescription(participantFile.getDescription());		    	
				participantFileHistoryRequest.setRequestFileType(participantFile.getRequestFileType());
				participantFileHistoryRequest.setFilename(participantFile.getFilename());
				participantFileHistoryRequest.setFile(participantFile.getFile());

				participantFileHistoryRequest.setRegistryUser(loggerUser.getUserName());
				participantFileHistoryRequest.setRegistryDate(CommonsUtilities.currentDateTime());
				
				participantFileHistoryRequest.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());		    	
				participantFileHistoryRequest.setParticipantRequest(participantRequest);
		    	participantFileHistoryRequest.setParticipant(participant);
		    	participantFileHistoryRequest.setState(ParticipantFileStateType.REGISTERED.getCode());
		    	participantFileHistoryRequest.setIndNew(indNew);
		    	
		    	participantRequestFiles.add(participantFileHistoryRequest);
			}
			if(Validations.validateListIsNullOrEmpty(participantRequest.getParticipantFileHistories())) {
				participantRequest.setParticipantFileHistories(participantRequestFiles);
			} else {
				participantRequest.getParticipantFileHistories().addAll(participantRequestFiles);
			}
			
			if(indNullList) {
				participant.setParticipantFiles(null);
			}				
		}		
	}
	
	/**
	 * Prepare participant files hist.
	 *
	 * @param participantRequest the participant request
	 * @param participant the participant
	 * @param indNew the ind new
	 * @param loggerUser the logger user
	 * @param indNullList the ind null list
	 * @throws ServiceException the service exception
	 */
	public void prepareInstitutionBankAccountsHist(ParticipantRequest participantRequest,  Participant participant,
			Integer indNew, LoggerUser loggerUser, Boolean indNullList) throws ServiceException{
		if(Validations.validateListIsNotNullAndNotEmpty(participant.getInstitutionBankAccounts())){
			List<InstitutionBankAccountHistory> participantRequestInstitutionBankAccount = new ArrayList<InstitutionBankAccountHistory>();
			InstitutionBankAccountHistory institutionBankAccountHistoryRequest;
			for (InstitutionBankAccount institutionBankAccount : participant.getInstitutionBankAccounts()) {
				institutionBankAccountHistoryRequest = new InstitutionBankAccountHistory();
				
				if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccount.getIdInstitutionBankAccountPk())) {
					institutionBankAccountHistoryRequest.setIdInstitutionBankAccountFk(institutionBankAccount.getIdInstitutionBankAccountPk());
				}
				institutionBankAccountHistoryRequest.setProviderBank(institutionBankAccount.getProviderBank());				
				institutionBankAccountHistoryRequest.setBankAcountType(institutionBankAccount.getBankAcountType());		    	
				institutionBankAccountHistoryRequest.setCurrency(institutionBankAccount.getCurrency());
				institutionBankAccountHistoryRequest.setAccountNumber(institutionBankAccount.getAccountNumber());

				institutionBankAccountHistoryRequest.setRegistryUser(loggerUser.getUserName());
				institutionBankAccountHistoryRequest.setRegistryDate(CommonsUtilities.currentDateTime());
						    	
				institutionBankAccountHistoryRequest.setParticipantRequest(participantRequest);
				institutionBankAccountHistoryRequest.setParticipant(participant);
				institutionBankAccountHistoryRequest.setState(BankAccountsStateType.REGISTERED.getCode());
				institutionBankAccountHistoryRequest.setIndNew(indNew);
		    	
		    	participantRequestInstitutionBankAccount.add(institutionBankAccountHistoryRequest);
			}
			if(Validations.validateListIsNullOrEmpty(participantRequest.getInstitutionBankAccountHistories())) {
				participantRequest.setInstitutionBankAccountHistories(participantRequestInstitutionBankAccount);
			} else {
				participantRequest.getInstitutionBankAccountHistories().addAll(participantRequestInstitutionBankAccount);
			}
			
			if(indNullList) {
				participant.setInstitutionBankAccounts(null);
			}				
		}		
	}

	/**
	 * Prepare participant int deposit hist.
	 *
	 * @param participantRequest the participant request
	 * @param participant the participant
	 * @param indNew the ind new
	 * @param loggerUser the logger user
	 * @param indNullList the ind null list
	 * @throws ServiceException the service exception
	 */
	public void prepareParticipantIntDepositHist(ParticipantRequest participantRequest, 
			Participant participant, Integer indNew, LoggerUser loggerUser, Boolean indNullList) throws ServiceException{
		if(Validations.validateListIsNotNullAndNotEmpty(participant.getParticipantIntDepositaries())){
			List<ParticipantIntDepoHistory> participantIntDepositariesHist = new ArrayList<ParticipantIntDepoHistory>();
	    	ParticipantIntDepoHistory participantIntDepoHistory = null;
	    	for (ParticipantIntDepositary  participantIntDepositary : participant.getParticipantIntDepositaries()) {
	    			participantIntDepoHistory = new ParticipantIntDepoHistory();
	    			participantIntDepoHistory.setParticipant(participant);
	    			participantIntDepoHistory.setParticipantRequest(participantRequest);
	    			participantIntDepoHistory.setInternationalDepository(participantIntDepositary.getInternationalDepository());
	    			participantIntDepoHistory.setRegistryUser(loggerUser.getUserName());
	    			participantIntDepoHistory.setRegistryDate(CommonsUtilities.currentDateTime());
			    	participantIntDepoHistory.setStateIntDepositary(ParticipantIntDepositaryStateType.REGISTERED.getCode());			 
			    	participantIntDepoHistory.setIndNew(indNew);
			    	participantIntDepositariesHist.add(participantIntDepoHistory);
			}
	    	
	    	if(Validations.validateListIsNullOrEmpty(participantRequest.getParticipantIntDepoHistories())) {
	    		participantRequest.setParticipantIntDepoHistories(participantIntDepositariesHist);
			} else {
				participantRequest.getParticipantIntDepoHistories().addAll(participantIntDepositariesHist);
			}	    	    	
	    	
	    	if(indNullList){
	    		participant.setParticipantIntDepositaries(null);
	    	}
		}
		
	}

	/**
	 * Prepare participant mechanism hist.
	 *
	 * @param participantRequest the participant request
	 * @param participant the participant
	 * @param indNew the ind new
	 * @param loggerUser the logger user
	 * @param indNullList the ind null list
	 * @throws ServiceException the service exception
	 */
	public void prepareParticipantMechanismHist(ParticipantRequest participantRequest, Participant participant,
			Integer indNew, LoggerUser loggerUser, Boolean indNullList) throws ServiceException{
		if(Validations.validateListIsNotNullAndNotEmpty(participant.getParticipantMechanisms())){
			List<ParticipantMechanismHistory> participantMechanismsHistories = new ArrayList<ParticipantMechanismHistory>();
	    	ParticipantMechanismHistory participantMechanismHistory = null;
	    	for (ParticipantMechanism participantMechanism : participant.getParticipantMechanisms()) {	    			
				participantMechanismHistory = new ParticipantMechanismHistory();
				participantMechanismHistory.setParticipantRequest(participantRequest);
				participantMechanismHistory.setMechanismModality(participantMechanism.getMechanismModality());
				participantMechanismHistory.setParticipant(participant);
				participantMechanismHistory.setStateParticipantMechanism(ParticipantMechanismStateType.REGISTERED.getCode());
				participantMechanismHistory.setRegistryUser(loggerUser.getUserName());
				participantMechanismHistory.setRegistryDate(CommonsUtilities.currentDateTime());
				if(participantMechanism.getIndPurchaseRole()!=null){
					participantMechanismHistory.setIndPurchaseRole(participantMechanism.getIndPurchaseRole());
				} else {
					participantMechanismHistory.setIndPurchaseRole(BooleanType.YES.getCode());
				}
				if(participantMechanism.getIndSaleRole()!=null){
					participantMechanismHistory.setIndSaleRole(participantMechanism.getIndSaleRole());
				} else {
					participantMechanismHistory.setIndSaleRole(BooleanType.YES.getCode());
				}				
				participantMechanismHistory.setIndNew(indNew);
				participantMechanismsHistories.add(participantMechanismHistory);
			}
	    	
	    	if(Validations.validateListIsNullOrEmpty(participantRequest.getParticipantMechanismHistories())) {
	    		participantRequest.setParticipantMechanismHistories(participantMechanismsHistories);
			} else {
				participantRequest.getParticipantMechanismHistories().addAll(participantMechanismsHistories);
			}	    	
	    	
	    	if(indNullList){
	    		participant.setParticipantMechanisms(null);
	    	}
		}
	}

	/**
	 * Fill representative file hist.
	 *
	 * @param lstLegalRepresentativeHistory the lst legal representative history
	 * @throws ServiceException the service exception
	 */
	public void fillRepresentativeFileHist(List<LegalRepresentativeHistory> lstLegalRepresentativeHistory) throws ServiceException{
		if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistory)) {
	    	for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistory) {
		    	
	    		if(Validations.validateIsNotNullAndPositive(legalRepresentativeHistory.getIdRepresentativeHistoryPk()) &&
	    				Validations.validateListIsNullOrEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
	    			
	    			LegalRepresentative legalRepresentative = new LegalRepresentative();
					legalRepresentative.setIdLegalRepresentativePk(legalRepresentativeHistory.getIdRepresentativeHistoryPk());
					List<LegalRepresentativeFile> representativeFiles = getListLegalRepresentativeFile(legalRepresentative);
					List<RepresentativeFileHistory> lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
					
					RepresentativeFileHistory representativeFileHistory;
					for (LegalRepresentativeFile legalRepresentativeFile : representativeFiles) {
						representativeFileHistory = new RepresentativeFileHistory();					
						representativeFileHistory.setDocumentType(legalRepresentativeFile.getDocumentType());
						representativeFileHistory.setFilename(legalRepresentativeFile.getFilename());
						representativeFileHistory.setDescription(legalRepresentativeFile.getDescription());
						representativeFileHistory.setFileRepresentative(legalRepresentativeFile.getFileLegalRepre());
						lstRepresentativeFileHistory.add(representativeFileHistory);
					}
	    			
					legalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
	    		}
		    									
			}
	    }
		
	}

	/**
	 * Prepare and save list legal representative hist.
	 *
	 * @param participantRequest the participant request
	 * @param lstLegalRepresentativeHistory the lst legal representative history
	 * @param registryType the registry type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void prepareAndSaveListLegalRepresentativeHist(ParticipantRequest participantRequest,List<LegalRepresentativeHistory> lstLegalRepresentativeHistory,
			Integer registryType, LoggerUser loggerUser) throws ServiceException{
		if(!lstLegalRepresentativeHistory.isEmpty()){
			for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistory) {
				legalRepresentativeHistory.setIdRepresentativeHistoryPk(null);
				legalRepresentativeHistory.setRegistryUser(loggerUser.getUserName());
				legalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
				legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.MODIFICATION.getCode());
				legalRepresentativeHistory.setRegistryType(registryType);
				legalRepresentativeHistory.setParticipantRequest(participantRequest);
				
				if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
					for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeHistory.getRepresenteFileHistory()) {
						representativeFileHistory.setIdRepresentFileHisPk(null);
						representativeFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
						representativeFileHistory.setRegistryUser(loggerUser.getUserName());
						representativeFileHistory.setRegistryDate(CommonsUtilities.currentDate());
						representativeFileHistory.setLegalRepresentativeHistory(legalRepresentativeHistory);
						representativeFileHistory.setRegistryType(registryType);
						representativeFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
					}
				}
				
				create(legalRepresentativeHistory); 
			}
		}
	}

	/**
	 * Prepare and save list legal representative hist.
	 *
	 * @param participantRequest the participant request
	 * @param participant the participant
	 * @param registryType the registry type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void prepareAndSaveListLegalRepresentativeHist(ParticipantRequest participantRequest, Participant participant,
			Integer registryType, LoggerUser loggerUser) throws ServiceException {
		List<RepresentedEntity> lstRepresentedEntity = getListRepresentedEntityByParticipant(participant);
		List<LegalRepresentativeFile> lstLegalRepresentativeFiles;
		List<RepresentativeFileHistory> lstRepresentativeFileHistory;
		if (Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)) {			
			LegalRepresentativeHistory objLegalRepresentativeHistory;
			for (RepresentedEntity objRepresentedEntity : lstRepresentedEntity) {
				objLegalRepresentativeHistory = new LegalRepresentativeHistory();				
				objLegalRepresentativeHistory.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
				objLegalRepresentativeHistory.setBirthDate(objRepresentedEntity.getLegalRepresentative().getBirthDate());
				objLegalRepresentativeHistory.setDocumentNumber(objRepresentedEntity.getLegalRepresentative().getDocumentNumber());
				objLegalRepresentativeHistory.setDocumentType(objRepresentedEntity.getLegalRepresentative().getDocumentType());
				objLegalRepresentativeHistory.setEconomicActivity(objRepresentedEntity.getLegalRepresentative().getEconomicActivity());
				objLegalRepresentativeHistory.setEconomicSector(objRepresentedEntity.getLegalRepresentative().getEconomicSector());
				objLegalRepresentativeHistory.setEmail(objRepresentedEntity.getLegalRepresentative().getEmail());
				objLegalRepresentativeHistory.setFaxNumber(objRepresentedEntity.getLegalRepresentative().getFaxNumber());
				objLegalRepresentativeHistory.setFirstLastName(objRepresentedEntity.getLegalRepresentative().getFirstLastName());
				objLegalRepresentativeHistory.setFullName(objRepresentedEntity.getLegalRepresentative().getFullName());
				objLegalRepresentativeHistory.setHomePhoneNumber(objRepresentedEntity.getLegalRepresentative().getHomePhoneNumber());
				objLegalRepresentativeHistory.setIndResidence(objRepresentedEntity.getLegalRepresentative().getIndResidence());
				objLegalRepresentativeHistory.setLegalAddress(objRepresentedEntity.getLegalRepresentative().getLegalAddress());
				objLegalRepresentativeHistory.setLegalDistrict(objRepresentedEntity.getLegalRepresentative().getLegalDistrict());
				objLegalRepresentativeHistory.setLegalProvince(objRepresentedEntity.getLegalRepresentative().getLegalProvince());
				objLegalRepresentativeHistory.setLegalDepartment(objRepresentedEntity.getLegalRepresentative().getLegalDepartment());
				objLegalRepresentativeHistory.setLegalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getLegalResidenceCountry());
				objLegalRepresentativeHistory.setMobileNumber(objRepresentedEntity.getLegalRepresentative().getMobileNumber());
				objLegalRepresentativeHistory.setName(objRepresentedEntity.getLegalRepresentative().getName());
				objLegalRepresentativeHistory.setNationality(objRepresentedEntity.getLegalRepresentative().getNationality());
				objLegalRepresentativeHistory.setOfficePhoneNumber(objRepresentedEntity.getLegalRepresentative().getOfficePhoneNumber());
				objLegalRepresentativeHistory.setPersonType(objRepresentedEntity.getLegalRepresentative().getPersonType());
				objLegalRepresentativeHistory.setPostalAddress(objRepresentedEntity.getLegalRepresentative().getPostalAddress());
				objLegalRepresentativeHistory.setPostalDepartment(objRepresentedEntity.getLegalRepresentative().getPostalDepartment());
				objLegalRepresentativeHistory.setPostalDistrict(objRepresentedEntity.getLegalRepresentative().getPostalDistrict());
				objLegalRepresentativeHistory.setPostalProvince(objRepresentedEntity.getLegalRepresentative().getPostalProvince());
				objLegalRepresentativeHistory.setPostalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getPostalResidenceCountry());
				objLegalRepresentativeHistory.setSecondDocumentNumber(objRepresentedEntity.getLegalRepresentative().getSecondDocumentNumber());
				objLegalRepresentativeHistory.setSecondDocumentType(objRepresentedEntity.getLegalRepresentative().getSecondDocumentType());
				objLegalRepresentativeHistory.setSecondLastName(objRepresentedEntity.getLegalRepresentative().getSecondLastName());
				objLegalRepresentativeHistory.setSecondNationality(objRepresentedEntity.getLegalRepresentative().getSecondNationality());
				objLegalRepresentativeHistory.setSex(objRepresentedEntity.getLegalRepresentative().getSex());
				objLegalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
				objLegalRepresentativeHistory.setRegistryType(registryType);
				objLegalRepresentativeHistory.setRepreHistoryType(HolderRequestType.MODIFICATION.getCode());
				objLegalRepresentativeHistory.setParticipantRequest(participantRequest);
				
				PepPerson pepPersonRepresentative = findPepPersonByLegalRepresentative(objRepresentedEntity.getLegalRepresentative());
				if(Validations.validateIsNotNull(pepPersonRepresentative)) {
					objLegalRepresentativeHistory.setIndPEP(BooleanType.YES.getCode());
					objLegalRepresentativeHistory.setCategory(pepPersonRepresentative.getCategory());
					objLegalRepresentativeHistory.setRole(pepPersonRepresentative.getRole());
					objLegalRepresentativeHistory.setBeginningPeriod(pepPersonRepresentative.getBeginingPeriod());
					objLegalRepresentativeHistory.setEndingPeriod(pepPersonRepresentative.getEndingPeriod());
				} else {
					objLegalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
				}
				
				lstLegalRepresentativeFiles = getListLegalRepresentativeFile(objRepresentedEntity.getLegalRepresentative());
				if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeFiles)) {
					RepresentativeFileHistory representativeFileHistory;
					lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
					for (LegalRepresentativeFile legalRepresentativeFile : lstLegalRepresentativeFiles) {
						representativeFileHistory = new RepresentativeFileHistory();					
						representativeFileHistory.setDocumentType(legalRepresentativeFile.getDocumentType());
						representativeFileHistory.setFilename(legalRepresentativeFile.getFilename());
						representativeFileHistory.setDescription(legalRepresentativeFile.getDescription());
						representativeFileHistory.setFileRepresentative(legalRepresentativeFile.getFileLegalRepre());
						representativeFileHistory.setLegalRepresentativeHistory(objLegalRepresentativeHistory);
						representativeFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
						representativeFileHistory.setRegistryUser(loggerUser.getUserName());
						representativeFileHistory.setRegistryDate(CommonsUtilities.currentDate());
						representativeFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
						representativeFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
						lstRepresentativeFileHistory.add(representativeFileHistory);
					}
					objLegalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
				}
				
				create(objLegalRepresentativeHistory);			
			}
		}
	}
	
	/**
	 * List participant file histories by request.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantFileHistory> listParticipantFileHistoriesByRequest(Long idParticipantRequestPk, Integer indNew) throws ServiceException{
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT P FROM ParticipantFileHistory P ");		  
		  stringBuilderSql.append(" Where P.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm");
		  stringBuilderSql.append(" And P.indNew = :indNewPrm ");
		 
		  TypedQuery<ParticipantFileHistory> query = em.createQuery(stringBuilderSql.toString(),ParticipantFileHistory.class);
		  query.setParameter("idParticipantRequestPrm", idParticipantRequestPk);
		  query.setParameter("indNewPrm", indNew);
		  			 
		  return (List<ParticipantFileHistory>) query.getResultList();
	}
	
	/**
	 * List participant file histories by request.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionBankAccountHistory> listInstitutionBankAccountHistoriesByRequest(Long idParticipantRequestPk, Integer indNew) throws ServiceException{
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT Ibah FROM InstitutionBankAccountHistory Ibah ");
		  stringBuilderSql.append("INNER JOIN FETCH Ibah.providerBank ");
		  stringBuilderSql.append(" Where Ibah.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm");
		  stringBuilderSql.append(" And Ibah.indNew = :indNewPrm ");
		 
		  TypedQuery<InstitutionBankAccountHistory> query = em.createQuery(stringBuilderSql.toString(),InstitutionBankAccountHistory.class);
		  query.setParameter("idParticipantRequestPrm", idParticipantRequestPk);
		  query.setParameter("indNewPrm", indNew);
		  			 
		  return (List<InstitutionBankAccountHistory>) query.getResultList();
	}
	
	/**
	 * List participant file by request.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantFile> listParticipantFileByRequest(Long idParticipantRequestPk) throws ServiceException{
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT P FROM ParticipantFile P ");		  
		  stringBuilderSql.append(" Where P.participantRequest.idParticipantRequestPk = :idParticipantRequestPrm ");
		  stringBuilderSql.append(" And P.stateFile = :stateFilePrm ");
		 
		  TypedQuery<ParticipantFile> query = em.createQuery(stringBuilderSql.toString(),ParticipantFile.class);
		  query.setParameter("idParticipantRequestPrm", idParticipantRequestPk);
		  query.setParameter("stateFilePrm", ParticipantFileStateType.REGISTERED.getCode());
		  			 
		  return query.getResultList();
	}

	/**
	 * Update holder to participant.
	 *
	 * @param participant the participant
	 * @param holder the holder
	 * @throws ServiceException the service exception
	 */
	public void updateHolderToParticipant(Participant participant, Holder holder) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Update Participant p set p.holder = :holderPrm ");
		sbQuery.append(" Where p.idParticipantPk = :idPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("holderPrm", holder);
		query.setParameter("idPrm", participant.getIdParticipantPk());
		
		query.executeUpdate();		
	}
	
	/**
	 * Creates the sequence participant dinamic.
	 *
	 * @param mnenomicParticipant the mnenomic participant
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int createSequenceParticipantDinamic(String mnenomicParticipant) throws ServiceException {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" CREATE SEQUENCE SQ_ID_ACCNUMBER_" + mnenomicParticipant);
		stringBuffer.append(" INCREMENT BY 1 ");
		stringBuffer.append(" START WITH 1  ");
		stringBuffer.append(" MINVALUE 1 ");
		stringBuffer.append(" MAXVALUE 99999999 ");				
		Query query= em.createNativeQuery(stringBuffer.toString());			
		return query.executeUpdate();		
	}
	
}
