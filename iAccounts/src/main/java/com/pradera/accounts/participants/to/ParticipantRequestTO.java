package com.pradera.accounts.participants.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantRequestTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public class ParticipantRequestTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new participant request to.
	 */
	public ParticipantRequestTO() {
		// TODO Auto-generated constructor stub
	}
	
	/** The id participant request pk. */
	private Long idParticipantRequestPk;
	
	/** The initia request date. */
	private Date initiaRequestDate;
	
	/** The end request date. */
	private Date endRequestDate;
	
	/** The state participant request. */
	private Integer stateParticipantRequest;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The request type. */
	private Integer requestType;
	
	/** The request number. */
	private Long requestNumber;
	
	/** The lst participant request states. */
	private List<Integer> lstParticipantRequestStates;
	
	/** The lst participant request types. */
	private List<Integer> lstParticipantRequestTypes;
	
	/** The required full information. */
	private boolean requiredFullInformation;
	
	/** The ind new data. */
	private Integer indNewData;
	
	/** The ind required files. */
	private boolean indRequiredFiles;

	/**
	 * Gets the id participant request pk.
	 *
	 * @return the id participant request pk
	 */
	public Long getIdParticipantRequestPk() {
		return idParticipantRequestPk;
	}

	/**
	 * Sets the id participant request pk.
	 *
	 * @param idParticipantRequestPk the new id participant request pk
	 */
	public void setIdParticipantRequestPk(Long idParticipantRequestPk) {
		this.idParticipantRequestPk = idParticipantRequestPk;
	}

	/**
	 * Gets the initia request date.
	 *
	 * @return the initia request date
	 */
	public Date getInitiaRequestDate() {
		return initiaRequestDate;
	}

	/**
	 * Sets the initia request date.
	 *
	 * @param initiaRequestDate the new initia request date
	 */
	public void setInitiaRequestDate(Date initiaRequestDate) {
		this.initiaRequestDate = initiaRequestDate;
	}

	/**
	 * Gets the end request date.
	 *
	 * @return the end request date
	 */
	public Date getEndRequestDate() {
		return endRequestDate;
	}

	/**
	 * Sets the end request date.
	 *
	 * @param endRequestDate the new end request date
	 */
	public void setEndRequestDate(Date endRequestDate) {
		this.endRequestDate = endRequestDate;
	}

	/**
	 * Gets the state participant request.
	 *
	 * @return the state participant request
	 */
	public Integer getStateParticipantRequest() {
		return stateParticipantRequest;
	}

	/**
	 * Sets the state participant request.
	 *
	 * @param stateParticipantRequest the new state participant request
	 */
	public void setStateParticipantRequest(Integer stateParticipantRequest) {
		this.stateParticipantRequest = stateParticipantRequest;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the lst participant request states.
	 *
	 * @return the lst participant request states
	 */
	public List<Integer> getLstParticipantRequestStates() {
		return lstParticipantRequestStates;
	}

	/**
	 * Sets the lst participant request states.
	 *
	 * @param lstParticipantRequestStates the new lst participant request states
	 */
	public void setLstParticipantRequestStates(
			List<Integer> lstParticipantRequestStates) {
		this.lstParticipantRequestStates = lstParticipantRequestStates;
	}

	/**
	 * Gets the lst participant request types.
	 *
	 * @return the lst participant request types
	 */
	public List<Integer> getLstParticipantRequestTypes() {
		return lstParticipantRequestTypes;
	}

	/**
	 * Sets the lst participant request types.
	 *
	 * @param lstParticipantRequestTypes the new lst participant request types
	 */
	public void setLstParticipantRequestTypes(
			List<Integer> lstParticipantRequestTypes) {
		this.lstParticipantRequestTypes = lstParticipantRequestTypes;
	}

	/**
	 * Checks if is required full information.
	 *
	 * @return true, if is required full information
	 */
	public boolean isRequiredFullInformation() {
		return requiredFullInformation;
	}

	/**
	 * Sets the required full information.
	 *
	 * @param requiredFullInformation the new required full information
	 */
	public void setRequiredFullInformation(boolean requiredFullInformation) {
		this.requiredFullInformation = requiredFullInformation;
	}

	/**
	 * Gets the ind new data.
	 *
	 * @return the ind new data
	 */
	public Integer getIndNewData() {
		return indNewData;
	}

	/**
	 * Sets the ind new data.
	 *
	 * @param indNewData the new ind new data
	 */
	public void setIndNewData(Integer indNewData) {
		this.indNewData = indNewData;
	}

	/**
	 * Checks if is ind required files.
	 *
	 * @return true, if is ind required files
	 */
	public boolean isIndRequiredFiles() {
		return indRequiredFiles;
	}

	/**
	 * Sets the ind required files.
	 *
	 * @param indRequiredFiles the new ind required files
	 */
	public void setIndRequiredFiles(boolean indRequiredFiles) {
		this.indRequiredFiles = indRequiredFiles;
	}
	
}
