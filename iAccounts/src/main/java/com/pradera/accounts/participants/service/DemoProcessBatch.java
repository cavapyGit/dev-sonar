package com.pradera.accounts.participants.service;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DemoProcessBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@RequestScoped
@BatchProcess(name="DemoProcessBatch")
public class DemoProcessBatch implements JobExecution {
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The holiday query service bean. */
	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;
	

	/**
	 * Instantiates a new demo process batch.
	 */
	public DemoProcessBatch() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info("iniciando batch y durmiendo 2 minutos");
		try {
			for(int index=0;index<300;index++){
				holidayQueryServiceBean.isHolidayDateServiceBean(CommonsUtilities.currentDate());
				log.info("index :"+index);
				Thread.sleep(1000);
			}
		log.info("Termino el proceso");
		}catch(Exception iex){
			log.error(iex.getMessage()); 
		}

	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
