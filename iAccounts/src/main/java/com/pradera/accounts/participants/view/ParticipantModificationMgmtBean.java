package com.pradera.accounts.participants.view;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.investorType.facade.InvestorTypeFacade;
import com.pradera.accounts.investorType.to.InvestorTypeModelTO;
import com.pradera.accounts.participants.facade.ParticipantServiceFacade;
import com.pradera.accounts.participants.to.ParticipantIntDepositoryTO;
import com.pradera.accounts.participants.to.ParticipantMechanismTO;
import com.pradera.accounts.participants.to.ParticipantRequestTO;
import com.pradera.accounts.participants.to.ParticipantTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.InternationalDepositoryTO;
import com.pradera.core.component.accounts.to.LegalRepresentativeTO;
import com.pradera.core.component.accounts.to.NegotiationMechanismTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.extension.view.LegalRepresentativeHelperBean;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantFile;
import com.pradera.model.accounts.ParticipantFileHistory;
import com.pradera.model.accounts.ParticipantIntDepoHistory;
import com.pradera.model.accounts.ParticipantIntDepositary;
import com.pradera.model.accounts.ParticipantIntDepositaryPK;
import com.pradera.model.accounts.ParticipantMechanism;
import com.pradera.model.accounts.ParticipantMechanismHistory;
import com.pradera.model.accounts.ParticipantRequest;
import com.pradera.model.accounts.ParticipantRequestHistory;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.InternationalDepositoryStateType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.accounts.type.PartModificationRejectMotiveType;
import com.pradera.model.accounts.type.ParticipantFileNationalType;
import com.pradera.model.accounts.type.ParticipantFileStateType;
import com.pradera.model.accounts.type.ParticipantIntDepositaryStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantRequestStateType;
import com.pradera.model.accounts.type.ParticipantRequestType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.accounts.type.RequestParticipantFileType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionBankAccountHistory;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.process.BusinessProcess;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantMgmtBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 08/02/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ParticipantModificationMgmtBean extends GenericBaseBean implements
		Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1426344267069953188L;
	
	/** The legal representative helper bean. */
	@Inject
    private LegalRepresentativeHelperBean legalRepresentativeHelperBean;

	/** The document validator. */
	@Inject
    DocumentValidator documentValidator;

	/** The document to. */
	private DocumentTO documentTO;
	
	/** The user info. */
	@Inject UserInfo userInfo;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	@EJB
	InvestorTypeFacade investorTypeFacade;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The lst legal representative history before. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryBefore;
	
	/** The lst legal representative history after. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryAfter;
	
	/** The lst representative file history. */
	private List<RepresentativeFileHistory> lstRepresentativeFileHistory;
	
	/** The lstt holder file types. */
	private List<ParameterTable> lsttHolderFileTypes;
	
	/** The participant file name display. */
	private String participantFileNameDisplay;
	
	/** The ind selected other motive reject. */
	private boolean indSelectedOtherMotiveReject;
	
	/** The ind disable accept motive reject. */
	private boolean indDisableAcceptMotiveReject;
	
	/** The lst participant request reject motives. */
	private List<ParameterTable> lstParticipantRequestRejectMotives;
	
	/** The lst participant file hy before. */
	private List<ParticipantFileHistory> lstParticipantFileHyBefore;

	/** The participant file histories. */
	private List<ParticipantFileHistory> lstParticipantFileHyAfter;
	
	/** The lst participant file hy before. */
	private List<InstitutionBankAccountHistory> lstInstitutionBankAccountHyBefore;

	/** The participant file histories. */
	private List<InstitutionBankAccountHistory> lstInstitutionBankAccountHyAfter;

	/** The min date participant contract. */
	private Date minDateParticipantContract;

	/** The lst represented entity. */
	private List<RepresentedEntity> lstRepresentedEntity;

	/** The lst legal representative. */
	private List<LegalRepresentative> lstLegalRepresentative;

	/** The legal representative history. */
	private LegalRepresentativeHistory legalRepresentativeHistory;

	/** The lst legal representative history. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistory;

	/** The lst participant roles. */
	private List<ParameterTable> lstParticipantRoles;

	/** The participant. */
	private Participant participant;

	/** The participant request hy before. */
	private ParticipantRequestHistory participantRequestHyBefore;
	
	/** The participant request history. */
	private ParticipantRequestHistory participantRequestHyAfter;	

	/** The lst negotiation mechanism before. */
	private List<NegotiationModality> lstNegotiationModalitiesBefore;

	/** The lst negotiation mechanism after. */
	private List<NegotiationModality> lstNegotiationModalitiesAfter;

	/** The lst international depository. */
	private List<InternationalDepository> lstInternationalDepository;

	/** The lst international depository before. */
	private List<InternationalDepository> lstInternationalDepositoryBefore;

	/** The lst international depository after. */
	private List<InternationalDepository> lstInternationalDepositoryAfter;

	/** The lst countries. */
	private List<GeographicLocation> lstCountries;
	
	/** The lst departments. */
	private List<GeographicLocation> lstDepartments;

	/** The lst provinces. */
	private List<GeographicLocation> lstProvinces;

	/** The lst districts. */
	private List<GeographicLocation> lstDistricts;

	/** The lst participant class. */
	private List<ParameterTable> lstParticipantClass;

	/** The lst participant class for search. */
	private List<ParameterTable> lstParticipantClassForSearch;

	/** The lst participant document type. */
	private List<ParameterTable> lstParticipantDocumentType;

	/** The lst participant document type for search. */
	private List<ParameterTable> lstParticipantDocumentTypeForSearch;

	/** The lst participant type. */
	private List<ParameterTable> lstParticipantType;

	/** The lst participant type for search. */
	private List<ParameterTable> lstParticipantTypeForSearch;

	/** The lst economic sector. */
	private List<ParameterTable> lstEconomicSector;

	/** The lst economic activity. */
	private List<ParameterTable> lstEconomicActivity;
	
	private List<InvestorTypeModelTO> listInvestorType;

	/** The lst participant state. */
	private List<ParameterTable> lstParticipantState;

	/** The lst participant state for search. */
	private List<ParameterTable> lstParticipantStateForSearch;

	/** The lst participant request state. */
	private List<ParameterTable> lstParticipantRequestState;

	/** The lst registered participants. */
	private List<Participant> lstRegisteredParticipants;

	/** The participant pay commission. */
	private boolean participantPayCommission;
	
	/** The participant cavapy. */
    private boolean indParticipantCavapy;
    
    /** The participant cavapy. */
    private boolean indManagedThirdCavapy;

	/** The participant data model. */
	private ParticipantDataModel participantDataModel;

	/** The participant to. */
	private ParticipantTO participantTO;

	/** The ind participant selected modif. */
	private boolean indParticipantSelectedModif;

	/** The id participant pk. */
	private Long idParticipantPk;

	/** The state participant. */
	private Integer stateParticipant;

	/** The participant files. */
	private List<ParticipantFile> lstParticipantFiles;

	/** The participant file type. */
	private Integer participantFileType;

	/** The lst participant file types. */
	private List<ParameterTable> lstParticipantFileTypes;

	/** The id represented entity pk. */
	private Long idRepresentedEntityPk;

	/** The represented entity. */
	private RepresentedEntity representedEntity;

	/** The participant request data model. */
	private ParticipantRequestDataModel participantRequestDataModel;

	/** The ind disable confirn reject. */
	private boolean indDisableConfirnReject;
	
	/** The lst negotiation modalities. */
	private List<NegotiationModality> lstNegotiationModalities;
	
	/** The option selected one radio. */
	private Integer optionSelectedOneRadio;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	@EJB
	ParameterServiceBean parameterServiceBean;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;

	/** The participant service facade. */
	@EJB
	ParticipantServiceFacade participantServiceFacade;

	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;

	/** The participant request. */
	private ParticipantRequest participantRequest;

	/** The id participant request pk. */
	private Long idParticipantRequestPk;
	
	/** The registered negotiation modalities before. */
	private boolean registeredNegotiationModalitiesBefore;
	
	/** The allow search all. */
	private boolean allowSearchAll;
	
	/** The participant related settlement. */
	private boolean participantRelatedSettlement;
	
	/** The original document number. */
	private String originalDocumentNumber;
	
	/** The original trade name. */
	private String originalTradeName;
	
	/** The original mnemonic. */
	private String originalMnemonic;		
	
	/** The ind before foreign now national. */
	private boolean indBeforeForeignNowNational;
	
	/** The ind participant info changed. */
	private boolean indParticipantInfoChanged;
	
	/** The ind participant files changed. */
	private boolean indParticipantFilesChanged;
	
	/** The ind participant files changed. */
	private boolean indInstitutionBankAccountsChanged;
	
	/** The ind participant ind depos changed. */
	private boolean indParticipantIndDeposChanged;
	
	/** The ind participant mechanisms changed. */
	private boolean indParticipantMechanismsChanged;
	
	/** The ind legal representative changed. */
	private boolean indLegalRepresentativeChanged;
	
	/** The files modified. */
	private String filesModified;

	/** The list document source. */
	private List<ParameterTable> listDocumentSource;
	
	/** FORMA PARTE DEL PROCESO DE COMPENSACION Y LIQUIDACION. */
	private boolean particiantTypeProcessCompLiq;
	
	/** Document Document Size. */
    private String fUploadFileSize="16777216";//15MB en bytes;
    
    /** The size document display. */
	private String fUploadFileSizeDisplay="15000";
	
	private Issuer originalIssuer;
	
	/** The institution bank account Session. */
	private InstitutionBankAccount institutionBankAccountSession;
	
	private InstitutionBankAccount institutionBankAccountSessionSelected;
	
	/** The institution bank account Session. */
	private List<InstitutionBankAccount> lstInstitutionBankAccountSession;
	
	/** The List currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The List bank provider. */
	private List<Bank> lstBankProvider;
	
	/** The List Bank Account Type. */
	private List<ParameterTable> lstBankAccountType;
	
	/**
	 * Instantiates a new participant mgmt bean.
	 */
	public ParticipantModificationMgmtBean() {
		participant = new Participant();
		participant.setHolder(new Holder());
		participantTO = new ParticipantTO();
		participantTO.getParticipantRequestTO().setInitiaRequestDate(
				CommonsUtilities.currentDate());
		participantTO.getParticipantRequestTO().setEndRequestDate(
				CommonsUtilities.currentDate());
		legalRepresentativeHistory = new LegalRepresentativeHistory();	
		filesModified = BooleanType.NO.getValue();
	}

	/**
	 * Clean search.
	 */
	public void cleanSearchModifRequest() {
		participantTO = new ParticipantTO();
		participantTO.getParticipantRequestTO().setInitiaRequestDate(
				CommonsUtilities.currentDate());
		participantTO.getParticipantRequestTO().setEndRequestDate(
				CommonsUtilities.currentDate());
		participantRequestDataModel = null;
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()) {			
			participantTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		
		JSFUtilities.setValidViewComponentAndChildrens("frmGeneral");
	}
	
	/**
	 * Gets the lst document source.
	 *
	 * @return the lst document source
	 */
	public void getLstDocumentSource() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			
			
			listDocumentSource = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}


	/**
	 * Load default combos.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadCombosParticipantManagement() throws ServiceException {
		/**
		 * Load Countries
		 */
		GeographicLocationTO filterCountry = new GeographicLocationTO();
		filterCountry.setGeographicLocationType(GeographicLocationType.COUNTRY
				.getCode());
		filterCountry
				.setState(GeographicLocationStateType.REGISTERED.getCode());
		lstCountries = generalParametersFacade
				.getListGeographicLocationServiceFacade(filterCountry);

		/**
		 * Instance a filter for ParameterTable
		 */
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());

		// List the document types of Participant
		lstParticipantDocumentType = listParticipantDocumentTypes();

		// List the Participant types
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_TYPE
				.getCode());
		lstParticipantType = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);

		// List the Economic sectors
		filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR
				.getCode());
		lstEconomicSector = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);

		// List the State of Participant
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_STATE
				.getCode());
		lstParticipantState = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
		
		InvestorTypeModelTO it = new InvestorTypeModelTO();
		it.setEconomicActivity(participant.getEconomicActivity());
		listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(it);

		loadParticipantRoles();

	}
	
	/**
	 * List participant document types.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<ParameterTable> listParticipantDocumentTypes() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		filterParameterTable.setLstParameterTablePk(new ArrayList<Integer>());
		filterParameterTable.getLstParameterTablePk().add(DocumentType.RUC.getCode());
		filterParameterTable.getLstParameterTablePk().add(DocumentType.DIO.getCode());
		return generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try {

			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
			filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_BLOCK_UBBLOCK_ANNUL_STATE.getCode());
			lstParticipantRequestState = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);

			lstParticipantDocumentTypeForSearch = listParticipantDocumentTypes();

			getLstDocumentSource();
			
			listHolidays();
			
			if(userInfo.getUserAccountSession().isDepositaryInstitution() || userInfo.getUserAccountSession().isIssuerInstitucion()) {
				allowSearchAll = true;
			} else {
				allowSearchAll = false;
				participantTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}

	/**
	 * Load participants.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadParticipants() throws ServiceException {
		Participant participantFilter = new Participant();
		participantFilter.setState(ParticipantStateType.REGISTERED
				.getCode());
		participantFilter.setIndDepositary(null);
		lstRegisteredParticipants = accountsFacade.getLisParticipantServiceBean(participantFilter);
	}

	/**
	 * Load all participant classes.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadAllParticipantClasses() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		
		//List the class for Participant type 'Directo', 'Emisor' and 'Cuenta Especial'
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_CLASS.getCode());
		lstParticipantClassForSearch = 
				generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);				
	}

	/**
	 * Change selected participant type search.
	 */
	@LoggerAuditWeb
	public void changeSelectedParticipantTypeSearch(){
		
		try {
			
			
			//Validate the account type of participant
			if(Validations.validateIsNullOrNotPositive(participantTO.getParticipantType())){
				loadAllParticipantClasses();
			} else {
				//List the Participant classes
				ParameterTableTO filterParameterTable = new ParameterTableTO();
				filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
				filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_CLASS.getCode());
				filterParameterTable.setIdRelatedParameterFk(participantTO.getParticipantType());
				lstParticipantClassForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load participant roles.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadParticipantRoles() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_ROLE
				.getCode());
		lstParticipantRoles = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * Load negotiation combos.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadNegotiationCombos() throws ServiceException {
		// List the negotiation mechanisms
		loadNegotiationModalities();

		// List the International Depositaries
		InternationalDepositoryTO filterInternationalDepository = new InternationalDepositoryTO();
		filterInternationalDepository.setStateIntDepository(Integer
				.valueOf(InternationalDepositoryStateType.ACTIVE.getCode()));
		lstInternationalDepository = participantServiceFacade
				.getListInternationalDepositoryServiceFacade(filterInternationalDepository);
	}
	
	/**
	 * Load negotiation modalities.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadNegotiationModalities() throws ServiceException{
		lstNegotiationModalities = participantServiceFacade.getListActiveNegotiationModalities();
		
		for(NegotiationModality mod : lstNegotiationModalities){
			List<NegotiationMechanism> lstNegotiationMechanism = new ArrayList<NegotiationMechanism>();
			for(NegotiationMechanismType negMechanismType : NegotiationMechanismType.list) {
				lstNegotiationMechanism.add(new NegotiationMechanism(negMechanismType.getCode(),negMechanismType.getDescripcion(),false,true));
			}
			mod.setNegotiationMechanisms(lstNegotiationMechanism);
			
			for(NegotiationMechanism negMec : mod.getNegotiationMechanisms()){
				for(MechanismModality mecModality : mod.getMechanismModalities()){
					if(negMec.getIdNegotiationMechanismPk().equals( mecModality.getId().getIdNegotiationMechanismPk() )){
						negMec.setDisabled(false);
					}
				}
			}
		}
	}

	/**
	 * Change selected participant type.
	 */
	@LoggerAuditWeb
	public void changeSelectedParticipantType(){
		try{
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			
			if(Validations.validateIsNull(participant.getAccountType()) || !Validations.validateIsPositiveNumber(participant.getAccountType())){
				lstParticipantClass = null;
				return;
			}
			
			/** issue 198
			 * 26	FORMAN PARTE DEL PROCESO DE COMPENSACION Y LIQUIDACION
		     * 27	NO FORMAN PARTE DEL PROCESO DE COMPENSACION Y LIQUIDACION
			 */
			//List the Participant classes
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
			filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_CLASS.getCode());
			if(participant.getAccountType().equals(ParticipantType.DIRECT.getCode())) {
				particiantTypeProcessCompLiq = Boolean.TRUE;
				filterParameterTable.setIdRelatedParameterFk(participant.getAccountType());
			} else {
				particiantTypeProcessCompLiq = Boolean.FALSE;	
				filterParameterTable.setIndicator4(GeneralConstants.ONE_VALUE_INTEGER);
			}
			
			lstParticipantClass = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstNegotiationModalities)){
				for (NegotiationModality negotiationModality : lstNegotiationModalities) {
					for (NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()) {
						negotiationMechanism.setSelected(false);
					}
				}				
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Change selected country.
	 */
	@LoggerAuditWeb
	public void changeSelectedCountry(){
		try{
			hideDialogsListener(null);		
			
			lstDepartments = null;
			lstProvinces = null;
			lstDistricts = null;			
			participant.setAddress(null);
			participant.setDepartment(null);
			participant.setProvince(null);
			participant.setDistrict(null);
			
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");												
			
			participantFileType = null;			
			participantFileNameDisplay = null;
						
			//Validate if the country was no selected
			if(Validations.validateIsNullOrNotPositive(participant.getResidenceCountry())){
				participant.setDocumentType(null);
			} else {
				
				GeographicLocationTO filter = new GeographicLocationTO();
				filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
				filter.setState(GeographicLocationStateType.REGISTERED.getCode());
				filter.setIdLocationReferenceFk(participant.getResidenceCountry());
				
				//List The provinces
				lstDepartments = generalParametersFacade.getListGeographicLocationServiceFacade(filter);

				if(countryResidence.equals(participant.getResidenceCountry())){
					participant.setDocumentType(DocumentType.RUC.getCode());
				} else {
					participant.setDocumentType(DocumentType.DIO.getCode());
				}
				
			}						
				
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	

	/**
	 * List participant file national types.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void listParticipantFileTypes() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());
		filterParameterTable
				.setMasterTableFk(MasterTableType.PARTICIPANT_FILE_TYPE
						.getCode());
		lstParticipantFileTypes = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * Change selected country from registered participant.
	 */
	@LoggerAuditWeb
	public void changeSelectedCountryFromRegisteredParticipant() {
		try {
			hideDialogsListener(null);
			JSFUtilities
					.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");

			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setGeographicLocationType(GeographicLocationType.PROVINCE
					.getCode());
			filter.setState(GeographicLocationStateType.REGISTERED.getCode());
			filter.setIdLocationReferenceFk(participant.getResidenceCountry());

			// List The provinces
			lstProvinces = generalParametersFacade
					.getListGeographicLocationServiceFacade(filter);
			participant.setProvince(null);
			// Clean the list of sectors
			lstDistricts = null;
			participant.setDistrict(null);

			InputText txtDocumentNumber = (InputText) JSFUtilities
					.findViewComponent("frmParticipantRegister:tabViewParticipant:txtDocumentNumber");
			if (Validations.validateIsNotNull(txtDocumentNumber)) {
				txtDocumentNumber.setDisabled(Boolean.FALSE);
			}

			// Validate if the participant is national or foreign in order to
			// set the document type and trade name
			if (countryResidence.equals(
					participant.getResidenceCountry())) {
				// Max Length of document number to 9
				if (Validations.validateIsNotNull(txtDocumentNumber)) {
					txtDocumentNumber.setMaxlength(9);
				}
			} else {
				participant.setDocumentType(DocumentType.DIO
						.getCode());
				// Max Length of document number to 20
				if (Validations.validateIsNotNull(txtDocumentNumber)) {
					txtDocumentNumber.setMaxlength(20);
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load participant location info.
	 */
	private void loadParticipantLocationInfo() {
		try {
			hideDialogsListener(null);			
			
			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setState(GeographicLocationStateType.REGISTERED.getCode());
			
			if(participant.getResidenceCountry()!=null){
				//List the Departments
				filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());			
				filter.setIdLocationReferenceFk(participant.getResidenceCountry());						
				lstDepartments = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
			}
			
			if(participant.getDepartment()!=null){
				//List The provinces
				filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());			
				filter.setIdLocationReferenceFk(participant.getDepartment());
				lstProvinces = generalParametersFacade.getListGeographicLocationServiceFacade(filter);			
			}
			
			if(participant.getProvince()!=null){
				//List the districts
				filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());			
				filter.setIdLocationReferenceFk(participant.getProvince());			
				lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);	
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Change selected province.
	 */
	@LoggerAuditWeb
	public void changeSelectedProvince() {
		try {
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			
			if(participant.getProvince()!=null){
				GeographicLocationTO filter = new GeographicLocationTO();
				filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
				filter.setState(GeographicLocationStateType.REGISTERED.getCode());
				filter.setIdLocationReferenceFk(participant.getProvince());
				
				lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
			} else {
				lstDistricts = null;
			}
			
			participant.setDistrict(null);
			participant.setAddress(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change selected department.
	 */
	@LoggerAuditWeb
	public void changeSelectedDepartment(){
		try{
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			if(participant.getDepartment()!=null){
				GeographicLocationTO filter = new GeographicLocationTO();
				filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
				filter.setState(GeographicLocationStateType.REGISTERED.getCode());
				filter.setIdLocationReferenceFk(participant.getDepartment());
				
				lstProvinces = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
			} else {
				lstProvinces = null;
			}
			
			lstDistricts = null;
			participant.setProvince(null);
			participant.setDistrict(null);
			participant.setAddress(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Change selected economic sector.
	 */
	@LoggerAuditWeb
	public void changeSelectedEconomicSector() {
		try {
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			if(Validations.validateIsNotNull(participant.getEconomicSector())){							
				ParameterTableTO filterParameterTable = new ParameterTableTO();
				filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
				filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
				filterParameterTable.setIdRelatedParameterFk(participant.getEconomicSector());
				lstEconomicActivity = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			} else {
				lstEconomicActivity = null;
			}
			participant.setEconomicActivity(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Validate document participant.
	 */
	@LoggerAuditWeb
	public void validateDocumentParticipant(){				
		
		try{
			hideDialogsListener(null);		
			
			if(Validations.validateIsNull(participant.getDocumentNumber())){
				return;
			}
			
			Participant participantFilterValidations = new Participant();
			participantFilterValidations.setIdParticipantPk(participant.getIdParticipantPk());
			participantFilterValidations.setDocumentType(participant.getDocumentType());
			participantFilterValidations.setDocumentNumber(participant.getDocumentNumber());
			//National Participant
			if(countryResidence.equals(participant.getResidenceCountry())){
				//Validate if exists participant with entered  document type and document number
				Participant participantResult = 
						participantServiceFacade.validateDocumentParticipant(participantFilterValidations);
				
				if(Validations.validateIsNotNull(participantResult)){
					StringBuilder sbBodyData = new StringBuilder();
					sbBodyData.append(DocumentType.get(participantResult.getDocumentType()).getValue());
					sbBodyData.append(GeneralConstants.HYPHEN);
					sbBodyData.append(participantResult.getDocumentNumber());
										
					Object[] arrBodyData = {sbBodyData.toString()};															
					
					showMessageOnDialog(
							PropertiesConstants.MESSAGES_ALERT,
		    				null,
		    				PropertiesConstants.ERROR_ADM_PARTICIPANT_DOC_NUM_NATIONAL_REPEATED,							
		    				arrBodyData);
		    		JSFUtilities.showSimpleValidationDialog();
		    		participant.setDocumentNumber(originalDocumentNumber);
				}
			} else {
				validateDescriptionParticipant();
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate description participant.
	 */
	@LoggerAuditWeb
	public void validateDescriptionParticipant(){
		try {
			hideDialogsListener(null);
			if(isIndParticipantForeign()){
				if(Validations.validateIsNotNullAndNotEmpty(participant.getDescription())){
					Participant participantFilterValidations = new Participant();
					participantFilterValidations.setIdParticipantPk(participant.getIdParticipantPk());
					participantFilterValidations.setDocumentType(participant.getDocumentType());
					participantFilterValidations.setDocumentNumber(participant.getDocumentNumber());
					participantFilterValidations.setDescription(participant.getDescription());
					
					Participant participantResult = 
							participantServiceFacade.validateDocumentParticipant(participantFilterValidations);
					
					if(Validations.validateIsNotNull(participantResult)){
						StringBuilder sbBodyData = new StringBuilder();
						sbBodyData.append(DocumentType.get(participantResult.getDocumentType()).getValue());
						sbBodyData.append(GeneralConstants.HYPHEN);
						sbBodyData.append(participantResult.getDocumentNumber());
						sbBodyData.append(GeneralConstants.HYPHEN);
						sbBodyData.append(participantResult.getDescription());
											
						Object[] arrBodyData = {sbBodyData.toString()};
	
						showMessageOnDialog(
								PropertiesConstants.MESSAGES_ALERT,
			    				null,
			    				PropertiesConstants.ERROR_ADM_PARTICIPANT_DOC_NUM_FOREIGN_REPEATED,							
			    				arrBodyData);
			    		JSFUtilities.showSimpleValidationDialog();
			    		
			    			participant.setDocumentNumber(originalDocumentNumber);
				    		participant.setDescription(originalTradeName);	    
						
					} 
				}
			}	
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Validate mnemonic participant.
	 */
	@LoggerAuditWeb
	public void validateMnemonicParticipant() {
		try {
			hideDialogsListener(null);

			if (Validations.validateIsNotNullAndNotEmpty(participant.getMnemonic())) {

				Participant participantFilterValidations = new Participant();
				participantFilterValidations.setIdParticipantPk(participant.getIdParticipantPk());
				participantFilterValidations.setMnemonic(participant.getMnemonic());

				Participant participantResult = participantServiceFacade
						.validateMnemonicParticipant(participantFilterValidations);

				if (Validations.validateIsNotNull(participantResult)) {
					StringBuilder sbBodyData = new StringBuilder();
					sbBodyData.append(String.valueOf(participantResult.getIdParticipantPk()));
					sbBodyData.append(GeneralConstants.HYPHEN);
					sbBodyData.append(participantResult.getDescription());

					Object[] arrBodyData = { sbBodyData.toString() };

					showMessageOnDialog(
							PropertiesConstants.MESSAGES_ALERT,
							null,
							PropertiesConstants.ERROR_ADM_PARTICIPANT_MNEMONIC_REPEATED,
							arrBodyData);

					JSFUtilities.showSimpleValidationDialog();
					participant.setMnemonic(originalMnemonic);
				}
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Begin register listener.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public void beginRegisterModificationRequest() {
		try {
			
			hideDialogsListener(null);
	
			// Clean validations on fields
			JSFUtilities
					.setValidViewComponentAndChildrens("frmParticipantRegister:tabViewParticipant");
			
			  //Validate if at least one Legal Representative
			  if(Validations.validateListIsNullOrEmpty(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory())){
			  
				  showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,
						  PropertiesConstants.ERROR_ADM_PARTICIPANT_NO_LEGAL_REPRESENTATIVE,
						  null); 
				  JSFUtilities.showSimpleValidationDialog();
			  
				  return; 
			  }
			 
	
			// Validate if at least one Negotiation Mechanism was selected
			boolean existsSelectedNegotiationMechanism = false;
			for(NegotiationModality negotiationModality : lstNegotiationModalities){
				for(NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()){
					if(negotiationMechanism.isSelected()){
						existsSelectedNegotiationMechanism = Boolean.TRUE;
						break;
					}
				}
			}
	
			if (!existsSelectedNegotiationMechanism) {
				// Show message error when none negotiation mechanism was selected
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ERROR_ADM_PARTICIPANT_NO_NEGOTIATION_MECHANISM,
						null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			//Validtions of participant files
			if(!validateAllParticipantFileLoaded()){
	    		showMessageOnDialog(
	    				PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ERROR_ADM_PARTICIPANT_FILE_NO_COMPLETE,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
	    	}
	    	
	    	ParticipantRequestTO participantRequestTO = new ParticipantRequestTO();
			participantRequestTO.setIdParticipantPk(participant.getIdParticipantPk());			
			participantRequestTO.setStateParticipantRequest(ParticipantRequestStateType.REGISTERED.getCode());	
			List<Integer> lstParticipantRequestTypes = new ArrayList<Integer>();
			lstParticipantRequestTypes.add(ParticipantRequestType.MODIFICATION.getCode());
			participantRequestTO.setLstParticipantRequestTypes(lstParticipantRequestTypes);
			
			
			//Validate if participant has previous request
			ParticipantRequest participantRequestResult = participantServiceFacade.validateExistsParticipantRequestServiceFacade(participantRequestTO);
			
			
			
			if(Validations.validateIsNotNull(participantRequestResult)){				
				//If exists previous participant request, show error and return
				ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequestResult.getRequestType());
				Object[] arrBodyData = {paramRequestType.getParameterName(),String.valueOf(idParticipantPk)};
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ERROR_ADM_PARTICIPANT_BLOCK_PREVIOUS_REQUEST,							
	    				arrBodyData);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
	
			Object[] arrBodyData = { String.valueOf(participant.getIdParticipantPk()) };
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_CONFIRM,
					null,
					PropertiesConstants.LBL_CONFIRM_PARTICIPANT_MODIFICATION_REQUEST_REGISTER,
					arrBodyData);
			JSFUtilities.showComponent("cnfParticipantModifReqRegister");
	
			return;
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate all participant file loaded.
	 *
	 * @return true, if successful
	 */
	private boolean validateAllParticipantFileLoaded(){
		if(Validations.validateListIsNullOrEmpty(lstParticipantFiles)){
			return false;
		} else {
			/*Issue 1231- Para esto, se definió que solo sea necesario adjuntar un documento como obligatorio, 
			 * pero que a su vez sea posible adjuntar mas de uno.*/
			
			return true;
		}
			
	}
	

	/**
	 * Save register action.
	 * 
	 * @param actionEvent
	 *            the action event
	 */
	@LoggerAuditWeb
	public void saveRegisterModificationRequestListener(ActionEvent actionEvent) {

		try {

			hideDialogsListener(null);

			participant.setIndPayCommission(participantPayCommission ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			participant.setIndDepositary(indParticipantCavapy ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			participant.setIndManagedThird(indManagedThirdCavapy ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			participant.setIndSettlementIncharge(participantRelatedSettlement ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			participant.setAccountsState(optionSelectedOneRadio);
			
			if (Validations
					.validateListIsNotNullAndNotEmpty(lstParticipantFiles)) {
				participant.setParticipantFiles(lstParticipantFiles);
			} else {
				participant.setParticipantFiles(null);
			}
			
			if (Validations
					.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccountSession)) {
				participant.setInstitutionBankAccounts(lstInstitutionBankAccountSession);
			} else {
				participant.setInstitutionBankAccounts(null);
			}

			List<ParticipantIntDepositary> participantIntDepositaries = new ArrayList<ParticipantIntDepositary>();
			ParticipantIntDepositary participantIntDepositary = null;
			ParticipantIntDepositaryPK participantIntDepositaryPK = null;
			for (InternationalDepository internationalDepository : lstInternationalDepository) {
				if (internationalDepository.isSelected()) {
					participantIntDepositary = new ParticipantIntDepositary();
					participantIntDepositary.setParticipant(participant);
					participantIntDepositary
							.setInternationalDepository(internationalDepository);
					participantIntDepositary
							.setStateIntDepositary(Integer
									.valueOf(ParticipantIntDepositaryStateType.REGISTERED
											.getCode()));

					participantIntDepositaryPK = new ParticipantIntDepositaryPK();
					participantIntDepositaryPK
							.setIdInternationalDepositoryPk(internationalDepository
									.getIdInternationalDepositoryPk());
					participantIntDepositaryPK.setIdParticipantPk(participant
							.getIdParticipantPk());
					participantIntDepositary.setId(participantIntDepositaryPK);

					participantIntDepositaries.add(participantIntDepositary);
				}
			}

			if (!participantIntDepositaries.isEmpty()) {
				participant
						.setParticipantIntDepositaries(participantIntDepositaries);
			} else {
				participant.setParticipantIntDepositaries(null);
			}

			List<ParticipantMechanism> participantMechanisms = new ArrayList<ParticipantMechanism>();
			ParticipantMechanism participantMechanism = null;
			for(NegotiationModality negotiationModality : lstNegotiationModalities){
				for(NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()){
					if(negotiationMechanism.isSelected()){
						participantMechanism = new ParticipantMechanism();
						participantMechanism.setMechanismModality(new MechanismModality( 
								new MechanismModalityPK(negotiationMechanism.getIdNegotiationMechanismPk(), 
								negotiationModality.getIdNegotiationModalityPk() ) ));
						participantMechanism.setParticipant(participant);
						participantMechanism.setStateParticipantMechanism(ParticipantMechanismStateType.REGISTERED.getCode());									
						participantMechanisms.add(participantMechanism);						
					}
				}
			}

			if (!participantMechanisms.isEmpty()) {
				participant.setParticipantMechanisms(participantMechanisms);
			} else {
				participant.setParticipantMechanisms(null);
			}

			ParticipantRequest participantRequestResult = participantServiceFacade
					.registryParticipantModification(
							participant, legalRepresentativeHelperBean.getLstLegalRepresentativeHistory());
			
			optionSelectedOneRadio = null;
			
			if (Validations.validateIsNotNull(participantRequestResult)) {
				
				BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.MODIFICATION_PARTICIPANT_REGISTRATION.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															businessProcessNotification, 
															participant.getIdParticipantPk(), null);

				Object[] arrBodyData = { String.valueOf(participantRequestResult
						.getRequestNumber()), String.valueOf(participant.getIdParticipantPk()) };
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_SUCCESFUL,
						null,
						PropertiesConstants.LBL_SUCCESS_PARTICIPANT_MODIFICATION_REQUEST_REGISTER,
						arrBodyData);
				cleanSearchModifRequest();
				JSFUtilities.showComponent("cnfEndTransactionParticipant");
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Hide dialogs listener.
	 * 
	 * @param actionEvent
	 *            the action event
	 */
	public void hideDialogsListener(ActionEvent actionEvent) {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();

		JSFUtilities.hideComponent("cnfParticipantModifReqRegister");
		JSFUtilities.hideComponent("detailRepresentativeDialog");		
		JSFUtilities.hideComponent("cnfIssuerRequestConfirmation");	
	}
	
	
	private boolean existsPreviousRequestParticipant() throws ServiceException{
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || Validations.validateIsNotNull(participantTO.getIdParticipantPk())) {
			if(userInfo.getUserAccountSession().isParticipantInstitucion() && Validations.validateIsNull(participantTO.getIdParticipantPk())) {
				participantTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			}
			ParticipantRequestTO participantRequestTO = new ParticipantRequestTO();
			participantRequestTO.setIdParticipantPk(participantTO.getIdParticipantPk());			
			participantRequestTO.setStateParticipantRequest(ParticipantRequestStateType.REGISTERED.getCode());	
			List<Integer> lstParticipantRequestTypes = new ArrayList<Integer>();
			lstParticipantRequestTypes.add(ParticipantRequestType.MODIFICATION.getCode());
			participantRequestTO.setLstParticipantRequestTypes(lstParticipantRequestTypes);
			
			ParticipantRequest participantRequestResult = participantServiceFacade.validateExistsParticipantRequestServiceFacade(participantRequestTO);
							
			if(Validations.validateIsNotNull(participantRequestResult)){				
				//If exists previous participant request, show error and return
				ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequestResult.getRequestType());
				Object[] arrBodyData = {paramRequestType.getParameterName(),String.valueOf(participantTO.getIdParticipantPk())};
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ERROR_ADM_PARTICIPANT_BLOCK_PREVIOUS_REQUEST,							
	    				arrBodyData);
				JSFUtilities.showSimpleValidationDialog();
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Exists previous request.
	 *
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	private boolean existsPreviousRequest() throws ServiceException{
		ParticipantRequestTO participantRequestTO = new ParticipantRequestTO();
		participantRequestTO.setIdParticipantPk(participant.getIdParticipantPk());			
		participantRequestTO.setStateParticipantRequest(ParticipantRequestStateType.REGISTERED.getCode());	
		List<Integer> lstParticipantRequestTypes = new ArrayList<Integer>();
		lstParticipantRequestTypes.add(ParticipantRequestType.MODIFICATION.getCode());
		participantRequestTO.setLstParticipantRequestTypes(lstParticipantRequestTypes);
		
		ParticipantRequest participantRequestResult = participantServiceFacade.validateExistsParticipantRequestServiceFacade(participantRequestTO);
						
		if(Validations.validateIsNotNull(participantRequestResult)){				
			//If exists previous participant request, show error and return
			ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequestResult.getRequestType());
			Object[] arrBodyData = {paramRequestType.getParameterName(),String.valueOf(idParticipantPk)};
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_ADM_PARTICIPANT_BLOCK_PREVIOUS_REQUEST,							
    				arrBodyData);
			JSFUtilities.showSimpleValidationDialog();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Change selected participant modif.
	 */
	@LoggerAuditWeb
	public void changeSelectedParticipantModif() {

		try {
			filesModified = BooleanType.NO.getValue();
			Participant participantFilter = new Participant();
			participantFilter.setIdParticipantPk(idParticipantPk);
			participantFilter.setState(ParticipantStateType.REGISTERED
					.getCode());

			Participant participantResult = findParticipantInformation(participantFilter);

			if (Validations.validateIsNotNull(participantResult)) {								
				
				participant = participantResult;
				
				if(Validations.validateIsNull(participant.getIssuer())){
					participant.setIssuer(new Issuer());
				}else{
					originalIssuer = participant.getIssuer();
				}
				
				if(existsPreviousRequest()){
					clearParticipantData();
					return;
				}

				lstParticipantFiles = participantServiceFacade
						.getListParticipantFilesServiceFacade(participantFilter);

				for (ParticipantFile participantFile : lstParticipantFiles) {
					for (ParameterTable fileType : lstParticipantFileTypes) {
						if(fileType.getParameterTablePk().equals(participantFile.getDocumentType())){
							participantFile.setDescription(fileType.getParameterName());
							break;
						}						
					}

				}

				indParticipantSelectedModif = Boolean.TRUE;

				lstRepresentedEntity = participantServiceFacade
						.getListRepresentedEntityByParticipant(participantFilter);
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)) {
					fillLegalRepresentativeHistory(lstRepresentedEntity);
				}

				if(participant.getCreationDate()!=null){
					minDateParticipantContract = CommonsUtilities.copyDate(participant.getCreationDate());
				}
				

				representedEntity = new RepresentedEntity();
				representedEntity.setLegalRepresentative(new LegalRepresentative());		
				
				originalDocumentNumber = participant.getDocumentNumber();
				originalTradeName = participant.getDescription();
				originalMnemonic = participant.getMnemonic();
				indBeforeForeignNowNational = false;
				optionSelectedOneRadio = participant.getAccountsState();

			} else {
				clearParticipantData();
			}
			
			documentTO = documentValidator.loadMaxLenghtType(participant.getDocumentType());			
			boolean isNumeric = documentTO.isNumericTypeDocument();
			Integer maxlenght = documentTO.getMaxLenghtDocumentNumber();
			documentTO = documentValidator.validateEmissionRules(participant.getDocumentType());
			documentTO.setNumericTypeDocument(isNumeric);
			documentTO.setMaxLenghtDocumentNumber(maxlenght);
				
			participantFileNameDisplay = null;
			
			validateIssuer();
			
			/** issue 198
			 * 26	FORMAN PARTE DEL PROCESO DE COMPENSACION Y LIQUIDACION
		     * 27	NO FORMAN PARTE DEL PROCESO DE COMPENSACION Y LIQUIDACION
			 */
			if(participant.getAccountType().equals(26)) {
				particiantTypeProcessCompLiq = Boolean.TRUE;						
			} else {
				particiantTypeProcessCompLiq = Boolean.FALSE;	
			}
			
			createObjectBankAccountsSession();
			createObjectRegister();
			loadBankAccountType();
			loadCurrency();
			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
			validateBankType(); 
    		lstInstitutionBankAccountSession = participantServiceFacade.getBankAccountsForId(participantFilter);
    		if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccountSession)) {
    			for(InstitutionBankAccount institutionBankAccountObj:lstInstitutionBankAccountSession) {
    				for (ParameterTable prmBankAcountType : lstBankAccountType) {
						if(prmBankAcountType.getParameterTablePk().equals(institutionBankAccountObj.getBankAcountType().intValue())) {
							institutionBankAccountObj.setDescriptionBankAcountType(prmBankAcountType.getDescription());
							break;
						}
					}
					for (ParameterTable prmCurrency : lstCurrency) {
						if(prmCurrency.getParameterTablePk().equals(institutionBankAccountObj.getCurrency())) {
							institutionBankAccountObj.setDescriptionCurrency(prmCurrency.getDescription());
							break;
						}
					}
					for (Bank prmBank :lstBankProvider) {
						if(prmBank.getIdBankPk().equals(institutionBankAccountObj.getProviderBank().getIdBankPk())) {
							institutionBankAccountObj.getProviderBank().setDescription(prmBank.getDescription());
							break;
						}
					}
    			}
    		}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Creates the object bank accounts session.
	 */
	public void createObjectBankAccountsSession(){
		institutionBankAccountSession = new InstitutionBankAccount();
		institutionBankAccountSession.setBank(new Bank());
		institutionBankAccountSession.setProviderBank(new Bank());
		institutionBankAccountSession.setParticipant(new Participant());
		institutionBankAccountSession.setIssuer(new Issuer());
	}
	
	/**
	 * Create Object Register.
	 */
	public void createObjectRegister(){
		lstBankAccountType=(new ArrayList<ParameterTable>());
		lstCurrency=(new ArrayList<ParameterTable>());
		lstBankProvider=(new ArrayList<Bank>());
	}
	
	/**
	 * Load Bank Account Type.
	 *
	 * @throws ServiceException the Service Exception
	 */
	private void loadBankAccountType()throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_BANK_ACCOUNT_TYPE.getCode());
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		lstBankAccountType=(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * load Currency.
	 *
	 * @throws ServiceException  the service exception
	 */
	private void loadCurrency() throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		List<ParameterTable> lstParameterTable = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		List<ParameterTable> lstParameterTableAux = new ArrayList<ParameterTable>();
		for(ParameterTable parameterTable : lstParameterTable){
			if(parameterTable.getParameterTablePk().equals(CurrencyType.PYG.getCode()) 
					|| parameterTable.getParameterTablePk().equals(CurrencyType.USD.getCode()))
				lstParameterTableAux.add(parameterTable);
		}
		if(getViewOperationType().equals(ViewOperationsType.CONSULT.getCode()))
			lstCurrency=lstParameterTableAux;
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
			lstCurrency=lstParameterTableAux;
	}
	
	/**
	 * Validate Bank Type.
	 */
	public void validateBankType(){
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())){
			try{
				if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
					JSFUtilities.resetComponent(":frmParticipantRegister:bank");
				lstBankProvider=(participantServiceFacade.getListBank(null,BankStateType.REGISTERED.getCode())); 
				if(institutionBankAccountSession.getBankType().equals(BankType.COMMERCIAL.getCode())) {
					if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
						institutionBankAccountSession.setProviderBank(new Bank());
					if(!getViewOperationType().equals(ViewOperationsType.REGISTER.getCode()))
						institutionBankAccountSession.setProviderBank(institutionBankAccountSession.getProviderBank());
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}	
		}else{
			lstBankProvider=new ArrayList<Bank>();
		}
			
	}
	
	
	public void modifyBankAccount(){
		hideDialogsListener(null);
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSessionSelected.getIdInstitutionBankAccountPk())) {
			institutionBankAccountSession=institutionBankAccountSessionSelected;
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ADM_VALIDATION_BANK_ACCOUNT_NOT_MODIFY));
			JSFUtilities.showValidationDialog();
		}
	}
	
	public String validateUpdateBankAccount(){
		hideDialogsListener(null);
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getInstitutionType()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())
				|| Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getProviderBank().getIdBankPk()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankAcountType())
				|| Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getAccountNumber()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getCurrency())) {
    		if (Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getInstitutionType()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBankType())
    				|| Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getProviderBank().getIdBankPk()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBankAcountType())
    				|| Validations.validateIsNullOrEmpty(institutionBankAccountSession.getAccountNumber()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getCurrency()))
    		{
    			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
    						, PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));	
    			   JSFUtilities.showSimpleValidationDialog();
    			   return null;
    		}
    	}
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getInstitutionType()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())
				&& Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getProviderBank().getIdBankPk()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankAcountType())
				&& Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getAccountNumber()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getCurrency())) 
		{
			for (ParameterTable prmBankAcountType : lstBankAccountType) {
				if(prmBankAcountType.getParameterTablePk().equals(institutionBankAccountSession.getBankAcountType().intValue())) {
					institutionBankAccountSession.setDescriptionBankAcountType(prmBankAcountType.getDescription());
					break;
				}
			}
			for (ParameterTable prmCurrency : lstCurrency) {
				if(prmCurrency.getParameterTablePk().equals(institutionBankAccountSession.getCurrency())) {
					institutionBankAccountSession.setDescriptionCurrency(prmCurrency.getDescription());
					break;
				}
			}
			for (Bank prmBank :lstBankProvider) {
				if(prmBank.getIdBankPk().equals(institutionBankAccountSession.getProviderBank().getIdBankPk())) {
					institutionBankAccountSession.getProviderBank().setDescription(prmBank.getDescription());
					break;
				}
			}
			for(InstitutionBankAccount institutionBankAccountObj:lstInstitutionBankAccountSession) {
				if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountObj.getIdInstitutionBankAccountPk())) {
					if(institutionBankAccountObj.getIdInstitutionBankAccountPk().equals(institutionBankAccountSession.getIdInstitutionBankAccountPk())) {
						institutionBankAccountObj=institutionBankAccountSession;
					}
				}
			}
			createObjectBankAccountsSession();
			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));	
		   JSFUtilities.showSimpleValidationDialog();
		   return null;
		}
		return null;
	}
	
	/**
	 * Delete cash account.
	 */
	public void deleteBankAccount(){
		hideDialogsListener(null);
		if (institutionBankAccountSessionSelected == null) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ADM_VALIDATION_BANK_ACCOUNT_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
		} else {
			try {
				if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSessionSelected.getIdInstitutionBankAccountPk())) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ADM_VALIDATION_BANK_ACCOUNT_NOT_DELETE));
					JSFUtilities.showValidationDialog();
				}else {
					for (ParameterTable prmCurrency : lstCurrency) {
						if(prmCurrency.getParameterTablePk().equals(institutionBankAccountSessionSelected.getCurrency())) {
							institutionBankAccountSessionSelected.setDescriptionCurrency(prmCurrency.getDescription());
							break;
						}
					}
					Object[] argObj = new Object[2];
					argObj[0] = institutionBankAccountSessionSelected.getDescriptionCurrency();
					argObj[1] = institutionBankAccountSessionSelected.getAccountNumber();
					JSFUtilities.executeJavascriptFunction("PF('wvRemoveBank').show()");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE)
							, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_REMOVE_CONFIRM, argObj));
				}
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}
	
	/**
	 * Delete cash account confirmed.
	 */
	public void deleteBankAccountConfirmed() {
		hideDialogsListener(null);
		List<InstitutionBankAccount> lstBankData = new ArrayList<InstitutionBankAccount>(lstInstitutionBankAccountSession);
		List<InstitutionBankAccount> lstBankActData = new ArrayList<InstitutionBankAccount>(lstInstitutionBankAccountSession);
		for (int i = 0; i < lstBankData.size(); i++) {
			if (institutionBankAccountSessionSelected != null && lstBankData.get(i).getAccountNumber().equals(institutionBankAccountSessionSelected.getAccountNumber())
					&& lstBankData.get(i).getCurrency().equals(institutionBankAccountSessionSelected.getCurrency())
					&& lstBankData.get(i).getProviderBank().getIdBankPk().equals(institutionBankAccountSessionSelected.getProviderBank().getIdBankPk())
					&& lstBankData.get(i).getBankAcountType().equals(institutionBankAccountSessionSelected.getBankAcountType())) {
				lstBankActData.remove(lstBankData.get(i));
			}
		}
		lstInstitutionBankAccountSession=lstBankActData;
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_DELETE_BANK_ACCOUNT));
		JSFUtilities.showValidationDialog();
	}
	
	public String validateAddBankAccount(){
		hideDialogsListener(null);
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getInstitutionType()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())
				|| Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getProviderBank().getIdBankPk()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankAcountType())
				|| Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getAccountNumber()) || Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getCurrency())) {
    		if (Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getInstitutionType()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBankType())
    				|| Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getProviderBank().getIdBankPk()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getBankAcountType())
    				|| Validations.validateIsNullOrEmpty(institutionBankAccountSession.getAccountNumber()) || Validations.validateIsNullOrNotPositive(institutionBankAccountSession.getCurrency()))
    		{
    			  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
    						, PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));	
    			   JSFUtilities.showSimpleValidationDialog();
    			   return null;
    		}
    	}
		if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getInstitutionType()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankType())
				&& Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getProviderBank().getIdBankPk()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getBankAcountType())
				&& Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getAccountNumber()) && Validations.validateIsNotNullAndNotEmpty(institutionBankAccountSession.getCurrency())) 
		{
			if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccountSession)) {
				for(InstitutionBankAccount institutionBankAccountObj:lstInstitutionBankAccountSession) {
					if(institutionBankAccountObj.getAccountNumber().equals(institutionBankAccountSession.getAccountNumber())
							&& institutionBankAccountObj.getCurrency().equals(institutionBankAccountSession.getCurrency())
							&& institutionBankAccountObj.getProviderBank().getIdBankPk().equals(institutionBankAccountSession.getProviderBank().getIdBankPk())
							&& institutionBankAccountObj.getBankAcountType().equals(institutionBankAccountSession.getBankAcountType())) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER));	
					   JSFUtilities.showSimpleValidationDialog();
					   createObjectBankAccountsSession();
					   institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
					   institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
					   return null;
					}
				}
			}
//			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
//			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
			for (ParameterTable prmBankAcountType : lstBankAccountType) {
				if(prmBankAcountType.getParameterTablePk().equals(institutionBankAccountSession.getBankAcountType().intValue())) {
					institutionBankAccountSession.setDescriptionBankAcountType(prmBankAcountType.getDescription());
					break;
				}
			}
			for (ParameterTable prmCurrency : lstCurrency) {
				if(prmCurrency.getParameterTablePk().equals(institutionBankAccountSession.getCurrency())) {
					institutionBankAccountSession.setDescriptionCurrency(prmCurrency.getDescription());
					break;
				}
			}
			for (Bank prmBank :lstBankProvider) {
				if(prmBank.getIdBankPk().equals(institutionBankAccountSession.getProviderBank().getIdBankPk())) {
					institutionBankAccountSession.getProviderBank().setDescription(prmBank.getDescription());
					break;
				}
			}
			lstInstitutionBankAccountSession.add(institutionBankAccountSession);
			createObjectBankAccountsSession();
			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED));	
		   JSFUtilities.showSimpleValidationDialog();
		   return null;
		}
		return null;
	}
	
	/**
	 * Clear participant data.
	 */
	private void clearParticipantData(){
		participant = new Participant();
		participant.setHolder(new Holder());
		lstLegalRepresentativeHistory = null;
		lstNegotiationModalities = null;
		lstInternationalDepository = null;
		lstParticipantFileTypes = null;
		indParticipantSelectedModif = Boolean.FALSE;
		lstParticipantFiles = new ArrayList<ParticipantFile>();		
		originalDocumentNumber = null;
		originalTradeName = null;
		originalMnemonic = null;
		indBeforeForeignNowNational = false;
		idParticipantPk = null;
	}
	
	/**
	 * Fill legal representative history.
	 *
	 * @param lstRepresentedEntity the lst represented entity
	 * @throws ServiceException the service exception
	 */
	public void fillLegalRepresentativeHistory(List<RepresentedEntity> lstRepresentedEntity) throws ServiceException {
		lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		LegalRepresentativeHistory objLegalRepresentativeHistory;
		for (RepresentedEntity objRepresentedEntity : lstRepresentedEntity) {
			objLegalRepresentativeHistory = new LegalRepresentativeHistory();
			objLegalRepresentativeHistory.setIdRepresentativeHistoryPk(objRepresentedEntity.getLegalRepresentative().getIdLegalRepresentativePk());
			objLegalRepresentativeHistory.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
			objLegalRepresentativeHistory.setBirthDate(objRepresentedEntity.getLegalRepresentative().getBirthDate());
			objLegalRepresentativeHistory.setDocumentNumber(objRepresentedEntity.getLegalRepresentative().getDocumentNumber());
			objLegalRepresentativeHistory.setDocumentType(objRepresentedEntity.getLegalRepresentative().getDocumentType());
			objLegalRepresentativeHistory.setEconomicActivity(objRepresentedEntity.getLegalRepresentative().getEconomicActivity());
			objLegalRepresentativeHistory.setEconomicSector(objRepresentedEntity.getLegalRepresentative().getEconomicSector());
			objLegalRepresentativeHistory.setEmail(objRepresentedEntity.getLegalRepresentative().getEmail());
			objLegalRepresentativeHistory.setFaxNumber(objRepresentedEntity.getLegalRepresentative().getFaxNumber());
			objLegalRepresentativeHistory.setFirstLastName(objRepresentedEntity.getLegalRepresentative().getFirstLastName());
			objLegalRepresentativeHistory.setFullName(objRepresentedEntity.getLegalRepresentative().getFullName());
			objLegalRepresentativeHistory.setHomePhoneNumber(objRepresentedEntity.getLegalRepresentative().getHomePhoneNumber());
			objLegalRepresentativeHistory.setIndResidence(objRepresentedEntity.getLegalRepresentative().getIndResidence());
			objLegalRepresentativeHistory.setLegalAddress(objRepresentedEntity.getLegalRepresentative().getLegalAddress());
			objLegalRepresentativeHistory.setLegalDistrict(objRepresentedEntity.getLegalRepresentative().getLegalDistrict());
			objLegalRepresentativeHistory.setLegalProvince(objRepresentedEntity.getLegalRepresentative().getLegalProvince());
			objLegalRepresentativeHistory.setLegalDepartment(objRepresentedEntity.getLegalRepresentative().getLegalDepartment());
			objLegalRepresentativeHistory.setLegalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getLegalResidenceCountry());
			objLegalRepresentativeHistory.setMobileNumber(objRepresentedEntity.getLegalRepresentative().getMobileNumber());
			objLegalRepresentativeHistory.setName(objRepresentedEntity.getLegalRepresentative().getName());
			objLegalRepresentativeHistory.setNationality(objRepresentedEntity.getLegalRepresentative().getNationality());
			objLegalRepresentativeHistory.setOfficePhoneNumber(objRepresentedEntity.getLegalRepresentative().getOfficePhoneNumber());
			objLegalRepresentativeHistory.setPersonType(objRepresentedEntity.getLegalRepresentative().getPersonType());
			objLegalRepresentativeHistory.setPostalAddress(objRepresentedEntity.getLegalRepresentative().getPostalAddress());
			objLegalRepresentativeHistory.setPostalDepartment(objRepresentedEntity.getLegalRepresentative().getPostalDepartment());
			objLegalRepresentativeHistory.setPostalDistrict(objRepresentedEntity.getLegalRepresentative().getPostalDistrict());
			objLegalRepresentativeHistory.setPostalProvince(objRepresentedEntity.getLegalRepresentative().getPostalProvince());
			objLegalRepresentativeHistory.setPostalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getPostalResidenceCountry());
			objLegalRepresentativeHistory.setSecondDocumentNumber(objRepresentedEntity.getLegalRepresentative().getSecondDocumentNumber());
			objLegalRepresentativeHistory.setSecondDocumentType(objRepresentedEntity.getLegalRepresentative().getSecondDocumentType());
			objLegalRepresentativeHistory.setSecondLastName(objRepresentedEntity.getLegalRepresentative().getSecondLastName());
			objLegalRepresentativeHistory.setSecondNationality(objRepresentedEntity.getLegalRepresentative().getSecondNationality());
			objLegalRepresentativeHistory.setSex(objRepresentedEntity.getLegalRepresentative().getSex());
			objLegalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
			
			PepPerson pepPersonRepresentative = participantServiceFacade.findPepPersonByLegalRepresentativeServiceFacade(objRepresentedEntity.getLegalRepresentative());
			if(Validations.validateIsNotNull(pepPersonRepresentative)) {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.YES.getCode());
				objLegalRepresentativeHistory.setCategory(pepPersonRepresentative.getCategory());
				objLegalRepresentativeHistory.setRole(pepPersonRepresentative.getRole());
				objLegalRepresentativeHistory.setBeginningPeriod(pepPersonRepresentative.getBeginingPeriod());
				objLegalRepresentativeHistory.setEndingPeriod(pepPersonRepresentative.getEndingPeriod());
			} else {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
			}			
									
			lstLegalRepresentativeHistory.add(objLegalRepresentativeHistory);			
		}
		
		LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
		legalRepresentativeTO.setViewOperationType(ViewOperationsType.DETAIL.getCode());
		legalRepresentativeTO.setFlagIssuerOrParticipant(Boolean.TRUE);
		legalRepresentativeHelperBean.setLegalRepresentativeTO(legalRepresentativeTO);
		legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);
	}
	
	/**
	 * Validate country list.
	 */
	private void validateCountryList(){
		if(isIndParticipantForeign()){
			Iterator<GeographicLocation> itCountries = lstCountries.iterator();
			GeographicLocation geographicLocationTmp = null;
			while(itCountries.hasNext()) {
				geographicLocationTmp = itCountries.next();
				if(countryResidence.equals(geographicLocationTmp.getIdGeographicLocationPk())){
					itCountries.remove();
				}
			}
		}
	}

	/**
	 * Find participant information.
	 * 
	 * @param filter
	 *            the filter
	 * @return the participant
	 * @throws ServiceException
	 *             the service exception
	 */
	private Participant findParticipantInformation(Participant filter)throws ServiceException {
		Participant participantResult = participantServiceFacade.findParticipantByFilters(filter);

		if (Validations.validateIsNotNull(participantResult)) {

			participant = participantResult;

			loadCombosParticipantManagement();
			
			validateCountryList();

			loadParticipantLocationInfo();

			loadNegotiationCombos();

			// List Participant class by participant type
			changeSelectedParticipantType();

			// List the Economic Activities by Economic Sector of participant
			changeSelectedEconomicSectorForDetail();
			
			

			listParticipantFileTypes();

			participantPayCommission = BooleanType.YES.getCode().equals(participant.getIndPayCommission());
			
			indParticipantCavapy = BooleanType.YES.getCode().equals(participant.getIndDepositary());
			
			indManagedThirdCavapy = BooleanType.YES.getCode().equals(participant.getIndManagedThird());
			
			participantRelatedSettlement = BooleanType.YES.getCode().equals(participant.getIndSettlementIncharge());

			ParticipantMechanismTO participantMechanismTO = new ParticipantMechanismTO();
			participantMechanismTO.setIdParticipantPk(participant.getIdParticipantPk());
			participantMechanismTO.setStateParticipantMechanism(ParticipantMechanismStateType.REGISTERED.getCode());

			// List ParticipantMechanism of the selected Participant
			List<ParticipantMechanism> participantMechanisms = participantServiceFacade
					.getListParticipantMechanismByParticipantServiceFacade(participantMechanismTO);			    		
    		
    		StringBuilder sbIdConverted=new StringBuilder();
    		for (NegotiationModality negotiationModality : lstNegotiationModalities) {
				for (NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()) {
					sbIdConverted.delete(0, sbIdConverted.length());
					sbIdConverted.append(negotiationMechanism.getIdNegotiationMechanismPk()).append("-").append(negotiationModality.getIdNegotiationModalityPk());
					for (ParticipantMechanism participantMechanism : participantMechanisms) {
						if(sbIdConverted.toString().equals(participantMechanism.getMechanismModality().getIdConverted())){
							negotiationMechanism.setSelected(Boolean.TRUE);
							break;
						}
					}
				}
			}

			ParticipantIntDepositoryTO participantIntDepositoryTO = new ParticipantIntDepositoryTO();
			participantIntDepositoryTO.setIdParticipantPk(participant.getIdParticipantPk());
			participantIntDepositoryTO.setStateParticipantIntDepositary(ParticipantIntDepositaryStateType.REGISTERED.getCode());

			// List ParticipantIntDepositaries of the selected Participant
			List<ParticipantIntDepositary> participantIntDepositaries = participantServiceFacade
					.getListParticipantIntDepoByParticipantServiceFacade(participantIntDepositoryTO);

			for (InternationalDepository internationalDepository : lstInternationalDepository) {
				for (ParticipantIntDepositary participantIntDepositary : participantIntDepositaries) {
					if (participantIntDepositary.getId().getIdParticipantPk().equals(participant.getIdParticipantPk())
							&& participantIntDepositary.getId().getIdInternationalDepositoryPk().equals(internationalDepository.getIdInternationalDepositoryPk())) {
						internationalDepository.setSelected(Boolean.TRUE);
						break;
					}
				}
			}

			return participantResult;
		} else {
			return null;
		}
	}

	/**
	 * Load participant modif req action.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadParticipantModifReqAction() {
		try {
			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()) {				
				Participant participantFilter = new Participant();
				participantFilter.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				participantFilter.setState(ParticipantStateType.REGISTERED
						.getCode());

				Participant participantResult = findParticipantInformation(participantFilter);
				
				if(Validations.validateIsNull(participantResult)) {
					showMessageOnDialog(
							PropertiesUtilities
							.getMessage(PropertiesConstants.MESSAGES_ALERT),
							PropertiesUtilities
							.getMessage(PropertiesConstants.WARNING_PARTICIPANT_IS_NOT_REGISTERED));
		    		JSFUtilities.showSimpleValidationDialog();
		    		return "";
				}					
			}
			cleanLists();
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			indParticipantSelectedModif = Boolean.FALSE;
			idParticipantPk = null;
			loadParticipants();
			participantPayCommission = Boolean.FALSE;
			indParticipantCavapy = Boolean.FALSE;
			indManagedThirdCavapy = Boolean.FALSE;
			participantRelatedSettlement = Boolean.FALSE;
			participant = new Participant();
			participant.setHolder(new Holder());
			lstParticipantFiles = new ArrayList<ParticipantFile>();			
			participantFileNameDisplay = null;
			participantRequest = new ParticipantRequest();			
			legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(new  ArrayList<LegalRepresentativeHistory>());	
			legalRepresentativeHelperBean.setFlagModifyLegalRepresentantive(Boolean.TRUE);
			optionSelectedOneRadio = null;
			originalDocumentNumber = null;
			originalTradeName = null;
			originalMnemonic = null;
			filesModified = BooleanType.NO.getValue();
			
			createObjectBankAccountsSession();
			createObjectRegister();
			loadCurrency();
			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
			validateBankType();
			loadBankAccountType();
			lstInstitutionBankAccountSession=new ArrayList<InstitutionBankAccount>();


			if(existsPreviousRequestParticipant() && userInfo.getUserAccountSession().isParticipantInstitucion()){
				return "";
			}
			
			//If the user is participant, set the id and fill information.
			if(userInfo.getUserAccountSession().isParticipantInstitucion()) {
				idParticipantPk = userInfo.getUserAccountSession().getParticipantCode();
				changeSelectedParticipantModif();
				legalRepresentativeHelperBean.setFlagModifyLegalRepresentantive(Boolean.FALSE);
			}
			
			return "participantMoficiationRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/**
	 * Clean lists.
	 */
	private void cleanLists() {
		lstNegotiationModalities = null;
		lstInternationalDepository = null;
		lstCountries = null;
		lstProvinces = null;
		lstDistricts = null;
		lstParticipantClass = null;
		lstParticipantDocumentType = null;
		lstParticipantType = null;
		lstEconomicSector = null;
		lstEconomicActivity = null;
		lstParticipantState = null;
		lstRegisteredParticipants = null;
		lstLegalRepresentativeHistory = null;
		lstParticipantFiles = null;
	}
		
	/**
	 * Change selected economic sector for detail.
	 *
	 * @throws ServiceException the service exception
	 */
	private void changeSelectedEconomicSectorForDetail() throws ServiceException{	
		if(participant.getEconomicSector()!=null){
			lstEconomicActivity = generalParametersFacade.getListEconomicActivityBySector(participant.getEconomicSector());
		}
	}


	/**
	 * Upload participant file.
	 * 
	 * @param eventFile
	 *            the event file
	 */
	public void uploadParticipantFile(FileUploadEvent eventFile) {
		try {
			hideDialogsListener(null);
			JSFUtilities
					.setValidViewComponentAndChildrens("frmParticipantRegister");

			if (Validations.validateIsNull(lstParticipantFiles)) {
				lstParticipantFiles = new ArrayList<ParticipantFile>();
			}

			ParticipantFile participantFile = new ParticipantFile();
			participantFile = new ParticipantFile();
			participantFile.setDocumentType(participantFileType);
			if (countryResidence.equals(
					participant.getResidenceCountry())) {
				participantFile.setDescription(ParticipantFileNationalType.get(
						participantFileType).getValue());
			} else {
				participantFile.setDescription(ParticipantFileNationalType.get(
						participantFileType).getValue());
			}
			participantFile
					.setRequestFileType(RequestParticipantFileType.REGISTER
							.getCode());
			participantFile.setFilename(fUploadValidateFileNameLength(eventFile.getFile().getFileName()));
			participantFile.setFile(eventFile.getFile().getContents());

			lstParticipantFiles.add(participantFile);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Search participant modif requests.
	 */
	@LoggerAuditWeb
	public void searchParticipantModifRequests() {
		try {
			hideDialogsListener(null);

			// Clean validations on fields
			JSFUtilities.setValidViewComponentAndChildrens("frmGeneral");

			// Validations of initial dare not greater than system date
			if (participantTO.getParticipantRequestTO().getInitiaRequestDate()
					.after(CommonsUtilities.currentDate())) {
				JSFUtilities
						.addContextMessage(
								"frmGeneral:calBeginDate",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE));
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE,
						null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			// Validations of end dare not greater than system date
			if (participantTO.getParticipantRequestTO().getEndRequestDate()
					.after(CommonsUtilities.currentDate())) {
				JSFUtilities
						.addContextMessage(
								"frmGeneral:calEndDate",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE));
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE,
						null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			// Validations of initial dare not greater than end date
			if (participantTO
					.getParticipantRequestTO()
					.getInitiaRequestDate()
					.after(participantTO.getParticipantRequestTO()
							.getEndRequestDate())) {
				JSFUtilities
						.addContextMessage(
								"frmGeneral:calBeginDate",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE));
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE,
						null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			// Validations of end dare not less than initial date
			if (participantTO
					.getParticipantRequestTO()
					.getEndRequestDate()
					.before(participantTO.getParticipantRequestTO()
							.getInitiaRequestDate())) {
				JSFUtilities
						.addContextMessage(
								"frmGeneral:calEndDate",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE));
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE,
						null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			participantTO.getParticipantRequestTO().setRequestType(ParticipantRequestType.MODIFICATION.getCode());
			participantTO.getParticipantRequestTO().setRequiredFullInformation(Boolean.TRUE);
			// If validations are OK, list the participants requests by filters
			participantRequestDataModel = new ParticipantRequestDataModel(
					participantServiceFacade.getListParticipantRequestModificationSearch(participantTO));

			indDisableConfirnReject = true;

			participantRequest = new ParticipantRequest();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change selected participant request.
	 *
	 * @param selectEvent the select event
	 */
	public void changeSelectedParticipantRequest(SelectEvent selectEvent) {
		if(ParticipantRequestStateType.REGISTERED.getCode().equals(participantRequest.getStateRequest())) {
			indDisableConfirnReject = false;
		} else {
			indDisableConfirnReject = true;
		}
	}
	
	/**
	 * Return participant request mgmt.
	 */
	public void returnParticipantRequestMgmt(){
		participantRequest = new ParticipantRequest();
		indDisableConfirnReject = true;
	}

	/**
	 * Fill geo location participant req hy.
	 *
	 * @param objParticipantRequestHistory the obj participant request history
	 * @throws ServiceException the service exception
	 */
	private void fillGeoLocationParticipantReqHy(ParticipantRequestHistory objParticipantRequestHistory) throws ServiceException{
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		GeographicLocation geographicLocationResult;
		
		// Find the Country of Participant Request History
		geographicLocationTO.setIdGeographicLocationPk(objParticipantRequestHistory.getResidenceCountry());
		geographicLocationResult = generalParametersFacade.findGeographicLocationByIdServiceFacade(geographicLocationTO);

		// Set the country description of Participant Request History
		if (Validations.validateIsNotNull(geographicLocationResult)) {
			objParticipantRequestHistory.setResidenceCountryDescription(geographicLocationResult.getName());
		}
		
		// Find the department of Participant Request History
		geographicLocationTO.setIdGeographicLocationPk(objParticipantRequestHistory.getDepartment());
		geographicLocationResult = generalParametersFacade.findGeographicLocationByIdServiceFacade(geographicLocationTO);

		// Set the department description of Participant Request History
		if (Validations.validateIsNotNull(geographicLocationResult)) {
			objParticipantRequestHistory.setDepartmentDescription(geographicLocationResult.getName());
		}

		// Find the Province of Participant Request History
		if(Validations.validateIsNotNullAndNotEmpty(objParticipantRequestHistory.getProvince())){
			geographicLocationTO.setIdGeographicLocationPk(objParticipantRequestHistory.getProvince());
			geographicLocationResult = generalParametersFacade.findGeographicLocationByIdServiceFacade(geographicLocationTO);

			// Set the Province description of Participant Request History
			if (Validations.validateIsNotNull(geographicLocationResult)) {
				objParticipantRequestHistory.setProvinceDescription(geographicLocationResult.getName());
			}
		}		

		// Find the District of Participant Request History
		if(Validations.validateIsNotNullAndNotEmpty(objParticipantRequestHistory.getDistrict())){
			geographicLocationTO.setIdGeographicLocationPk(objParticipantRequestHistory.getDistrict());
			geographicLocationResult = generalParametersFacade.findGeographicLocationByIdServiceFacade(geographicLocationTO);

			// Set the District description of Participant Request History
			if (Validations.validateIsNotNull(geographicLocationResult)) {
				objParticipantRequestHistory.setDistrictDescription(geographicLocationResult.getName());
			}
		}
		
	}
	
	/**
	 * Fill parameters participant before and after.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillParametersParticipantBeforeAndAfter() throws ServiceException{

		fillGeoLocationParticipantReqHy(participantRequestHyBefore);
		fillGeoLocationParticipantReqHy(participantRequestHyAfter);
		
		fillOtherDescriptionsParticipantReqHy(participantRequestHyBefore);
		fillOtherDescriptionsParticipantReqHy(participantRequestHyAfter);
	}
	
	/**
	 * Fill other descriptions participant req hy.
	 *
	 * @param objParticipantRequestHistory the obj participant request history
	 * @throws ServiceException the service exception
	 */
	private void fillOtherDescriptionsParticipantReqHy(ParticipantRequestHistory objParticipantRequestHistory) throws ServiceException{
		ParameterTableTO filterParameter = new ParameterTableTO();
		filterParameter.setState(ParameterTableStateType.REGISTERED.getCode());
		
		filterParameter.setMasterTableFk(MasterTableType.PARTICIPANT_TYPE.getCode());
		List<ParameterTable> lstAccountTypeTmp = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
		
		filterParameter.setMasterTableFk(MasterTableType.PARTICIPANT_CLASS.getCode());
		List<ParameterTable> lstAccountClassTmp = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
		
		filterParameter.setMasterTableFk(MasterTableType.PARTICIPANT_ROLE.getCode());
		List<ParameterTable> lstAccountRoleTmp = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
		
		filterParameter.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		List<ParameterTable> lstEconomicSectorsTmp = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
		
		filterParameter.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
		List<ParameterTable> lstEconomicActivityTmp = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
		
		filterParameter.setMasterTableFk(MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
		List<ParameterTable> lstInvestorTypeTmp = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
		
		filterParameter.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		List<ParameterTable> lstDocumentTypeTmp = generalParametersFacade.getListParameterTableServiceBean(filterParameter);
		
		for (ParameterTable param : lstAccountTypeTmp) {
			if(param.getParameterTablePk().equals(objParticipantRequestHistory.getAccountType())) {
				objParticipantRequestHistory.setAccountTypeDescription(param.getParameterName());
			}
		}
		
		for (ParameterTable param : lstAccountClassTmp) {
			if(param.getParameterTablePk().equals(objParticipantRequestHistory.getAccountClass())) {
				objParticipantRequestHistory.setAccountClassDescription(param.getParameterName());
			}
		}
		
		for (ParameterTable param : lstAccountRoleTmp) {
			if(param.getParameterTablePk().equals(objParticipantRequestHistory.getRoleParticipant())) {
				objParticipantRequestHistory.setRoleParticipantDescription(param.getParameterName());
			}
		}
		
		for (ParameterTable param : lstEconomicSectorsTmp) {
			if(param.getParameterTablePk().equals(objParticipantRequestHistory.getEconomicSector())) {
				objParticipantRequestHistory.setEconomicSectorDescription(param.getParameterName());
			}
		}
		
		for (ParameterTable param : lstEconomicActivityTmp) {
			if(param.getParameterTablePk().equals(objParticipantRequestHistory.getEconomicActivity())) {
				objParticipantRequestHistory.setEconomicActivityDescription(param.getParameterName());
			}
		}
		
		for (ParameterTable param : lstInvestorTypeTmp) {
			if(param.getParameterTablePk().equals(objParticipantRequestHistory.getInvestorType())) {
				objParticipantRequestHistory.setInvestorTypeDescription(param.getParameterName());
			}
		}
		
		for (ParameterTable param : lstDocumentTypeTmp) {
			if(param.getParameterTablePk().equals(objParticipantRequestHistory.getDocumentType())) {
				objParticipantRequestHistory.setDocumentTypeDescription(param.getIndicator1());
			}
		}
	}

	/**
	 * Detail participant modification.
	 * 
	 * @return the string
	 */
	public String detailParticipantModification() {
		try {
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			ParticipantRequestTO participantRequestTO = new ParticipantRequestTO();
			participantRequestTO.setIdParticipantRequestPk(idParticipantRequestPk);			

			participantRequest = participantServiceFacade.findParticipantRequestModificationById(participantRequestTO);
			participantRequest.setParticipantRequestStateDescription(
					generalParametersFacade.getParameterTableById(participantRequest.getStateRequest()).getParameterName());

			participant = participantRequest.getParticipant();
			
			participantRequestTO.setIndNewData(BooleanType.NO.getCode());
			participantRequestHyBefore = participantServiceFacade.findParticipantHistoryByRequest(participantRequestTO);

			participantRequestTO.setIndNewData(BooleanType.YES.getCode());
			participantRequestHyAfter = participantServiceFacade.findParticipantHistoryByRequest(participantRequestTO);
			
			if(Validations.validateIsNotNullAndNotEmpty(participantRequestHyBefore.getIssuerFk())){
				participantRequestHyBefore.setIssuer(parameterServiceBean.find(Issuer.class,participantRequestHyBefore.getIssuerFk()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(participantRequestHyAfter.getIssuerFk())){
				participantRequestHyAfter.setIssuer(parameterServiceBean.find(Issuer.class,participantRequestHyAfter.getIssuerFk()));
			}
			
			fillParametersParticipantBeforeAndAfter();
			
	    	listParticipantFileTypes();

			// Get The Participant Files
	    	lstParticipantFileHyBefore = participantServiceFacade.listParticipantFileHistoriesByRequest(
					participantRequest.getIdParticipantRequestPk(), BooleanType.NO.getCode());

			if (Validations.validateListIsNotNullAndNotEmpty(lstParticipantFileHyBefore)) {
				for (ParticipantFileHistory participantFileHistory : lstParticipantFileHyBefore) {
					for (ParameterTable fileType : lstParticipantFileTypes) {
						if(fileType.getParameterTablePk().equals(participantFileHistory.getDocumentType())){
							participantFileHistory.setDescription(fileType.getParameterName());
							break;
						}						
					}					
				}
			}

			lstParticipantFileHyAfter = participantServiceFacade.listParticipantFileHistoriesByRequest(
					participantRequest.getIdParticipantRequestPk(), BooleanType.YES.getCode());

			if (Validations.validateListIsNotNullAndNotEmpty(lstParticipantFileHyAfter)) {
				for (ParticipantFileHistory participantFileHistory : lstParticipantFileHyAfter) {
					for (ParameterTable fileType : lstParticipantFileTypes) {
						if(fileType.getParameterTablePk().equals(participantFileHistory.getDocumentType())){
							participantFileHistory.setDescription(fileType.getParameterName());
							break;
						}						
					}
				}
			}		
			
			// Get The Institution Bank Accounts
			createObjectBankAccountsSession();
			createObjectRegister();
			loadBankAccountType();
			loadCurrency();
			institutionBankAccountSession.setInstitutionType(InstitutionBankAccountsType.PARTICIPANT.getCode());
			institutionBankAccountSession.setBankType(BankType.COMMERCIAL.getCode());
			validateBankType();
    		
			lstInstitutionBankAccountHyBefore = participantServiceFacade.listInstitutionBankAccountHistoriesByRequest(
					participantRequest.getIdParticipantRequestPk(), BooleanType.NO.getCode());

			if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccountHyBefore)) {
    			for(InstitutionBankAccountHistory institutionBankAccountObj:lstInstitutionBankAccountHyBefore) {
    				for (ParameterTable prmBankAcountType : lstBankAccountType) {
						if(prmBankAcountType.getParameterTablePk().equals(institutionBankAccountObj.getBankAcountType().intValue())) {
							institutionBankAccountObj.setDescriptionBankAcountType(prmBankAcountType.getDescription());
							break;
						}
					}
					for (ParameterTable prmCurrency : lstCurrency) {
						if(prmCurrency.getParameterTablePk().equals(institutionBankAccountObj.getCurrency())) {
							institutionBankAccountObj.setDescriptionCurrency(prmCurrency.getDescription());
							break;
						}
					}
					for (Bank prmBank :lstBankProvider) {
						if(prmBank.getIdBankPk().equals(institutionBankAccountObj.getProviderBank().getIdBankPk())) {
							institutionBankAccountObj.getProviderBank().setDescription(prmBank.getDescription());
							break;
						}
					}
    			}
    		}

			lstInstitutionBankAccountHyAfter = participantServiceFacade.listInstitutionBankAccountHistoriesByRequest(
					participantRequest.getIdParticipantRequestPk(), BooleanType.YES.getCode());

			if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccountHyAfter)) {
    			for(InstitutionBankAccountHistory institutionBankAccountObj:lstInstitutionBankAccountHyAfter) {
    				for (ParameterTable prmBankAcountType : lstBankAccountType) {
						if(prmBankAcountType.getParameterTablePk().equals(institutionBankAccountObj.getBankAcountType().intValue())) {
							institutionBankAccountObj.setDescriptionBankAcountType(prmBankAcountType.getDescription());
							break;
						}
					}
					for (ParameterTable prmCurrency : lstCurrency) {
						if(prmCurrency.getParameterTablePk().equals(institutionBankAccountObj.getCurrency())) {
							institutionBankAccountObj.setDescriptionCurrency(prmCurrency.getDescription());
							break;
						}
					}
					for (Bank prmBank :lstBankProvider) {
						if(prmBank.getIdBankPk().equals(institutionBankAccountObj.getProviderBank().getIdBankPk())) {
							institutionBankAccountObj.getProviderBank().setDescription(prmBank.getDescription());
							break;
						}
					}
    			}
    		}
			
			lstLegalRepresentativeHistoryBefore = participantServiceFacade.listLegalRepresentHistByParticipantRequest(participantRequest, RegistryType.ORIGIN_OF_THE_REQUEST.getCode());

			// Get the list of Legal Representative History of the Participant
			// Request
			lstLegalRepresentativeHistoryAfter = participantServiceFacade.listLegalRepresentHistByParticipantRequest(participantRequest, RegistryType.HISTORICAL_COPY.getCode());
			
			validateLegalRepresentativesChanged(lstLegalRepresentativeHistoryBefore, lstLegalRepresentativeHistoryAfter);
			if(!indLegalRepresentativeChanged){
				lstLegalRepresentativeHistoryAfter = new ArrayList<LegalRepresentativeHistory>();
			}
			
			LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
			legalRepresentativeTO.setViewOperationType(ViewOperationsType.DETAIL.getCode());
			legalRepresentativeTO.setFlagIssuerOrParticipant(Boolean.TRUE);
			legalRepresentativeHelperBean.setLegalRepresentativeTO(legalRepresentativeTO);
			legalRepresentativeHelperBean.setLstLegalRepresentativeHistoryDetailBefore(lstLegalRepresentativeHistoryBefore);
			legalRepresentativeHelperBean.setLstLegalRepresentativeHistoryDetail(lstLegalRepresentativeHistoryAfter);

			NegotiationMechanismTO filterNegotiationMechanism = new NegotiationMechanismTO();
			filterNegotiationMechanism.setNegotiationMechanismState(NegotiationMechanismStateType.ACTIVE.getCode());
			filterNegotiationMechanism.setMechanismModalityState(MechanismModalityStateType.ACTIVE.getCode());
			lstNegotiationModalitiesBefore = participantServiceFacade.getListActiveNegotiationModalities();
			
			for(NegotiationModality mod : lstNegotiationModalitiesBefore){
				List<NegotiationMechanism> lstNegotiationMechanism = new ArrayList<NegotiationMechanism>();
				for(NegotiationMechanismType negMechanismType : NegotiationMechanismType.list) {
					lstNegotiationMechanism.add(new NegotiationMechanism(negMechanismType.getCode(),negMechanismType.getDescripcion(),false,true));
				}
				mod.setNegotiationMechanisms(lstNegotiationMechanism);
			}
			
			lstNegotiationModalitiesAfter = participantServiceFacade.getListActiveNegotiationModalities();
			
			for(NegotiationModality mod : lstNegotiationModalitiesAfter){
				List<NegotiationMechanism> lstNegotiationMechanism = new ArrayList<NegotiationMechanism>();
				for(NegotiationMechanismType negMechanismType : NegotiationMechanismType.list) {
					lstNegotiationMechanism.add(new NegotiationMechanism(negMechanismType.getCode(),negMechanismType.getDescripcion(),false,true));
				}
				mod.setNegotiationMechanisms(lstNegotiationMechanism);
			}
			
			lstInternationalDepositoryBefore = fillListInternationalDepository(participantRequest, BooleanType.NO.getCode());
			lstInternationalDepositoryAfter = fillListInternationalDepository(participantRequest, BooleanType.YES.getCode());

			ParticipantMechanismTO participantMechanismTO = new ParticipantMechanismTO();
			participantMechanismTO.setIdParticipantPk(participant.getIdParticipantPk());
			participantMechanismTO.setStateParticipantMechanism(ParticipantMechanismStateType.REGISTERED.getCode());

			// List ParticipantMechanism of the selected Participant
			List<ParticipantMechanismHistory> participantMechanismHyBefore = 
					participantServiceFacade.listParticipantMechHistoryByRequest(participantRequest.getIdParticipantRequestPk(), BooleanType.NO.getCode());
			
			

			StringBuilder sbIdConverted=new StringBuilder();
			registeredNegotiationModalitiesBefore = Boolean.FALSE;
    		for (NegotiationModality negotiationModality : lstNegotiationModalitiesBefore) {
				for (NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()) {
					sbIdConverted.delete(0, sbIdConverted.length());
					sbIdConverted.append(negotiationMechanism.getIdNegotiationMechanismPk()).append("-").append(negotiationModality.getIdNegotiationModalityPk());
					for (ParticipantMechanismHistory participantMechanism : participantMechanismHyBefore) {
						if(sbIdConverted.toString().equals(participantMechanism.getMechanismModality().getIdConverted())){
							negotiationMechanism.setSelected(Boolean.TRUE);		
							registeredNegotiationModalitiesBefore = Boolean.TRUE;
							break;
						}
					}
				}
			}
			
			participantMechanismTO.setIdParticipantRequestPk(participantRequest.getIdParticipantRequestPk());

			List<ParticipantMechanismHistory> participantMechanismHyAfter = 
					participantServiceFacade.listParticipantMechHistoryByRequest(participantRequest.getIdParticipantRequestPk(), BooleanType.YES.getCode());

			for (NegotiationModality negotiationModality : lstNegotiationModalitiesAfter) {
				for (NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()) {
					sbIdConverted.delete(0, sbIdConverted.length());
					sbIdConverted.append(negotiationMechanism.getIdNegotiationMechanismPk()).append("-").append(negotiationModality.getIdNegotiationModalityPk());
					for (ParticipantMechanismHistory participantMechanismHistory : participantMechanismHyAfter) {
						if(sbIdConverted.toString().equals(participantMechanismHistory.getMechanismModality().getIdConverted())){
							negotiationMechanism.setSelected(Boolean.TRUE);
							break;
						}
					}
				}
			}
			
			//Validate Info changed
			validateParticipantInfoChanged(participantRequestHyBefore, participantRequestHyAfter);			
			validateParticipantIntDepositaryChanged(lstInternationalDepositoryBefore, lstInternationalDepositoryAfter);
			validateParticipantMechChanged(lstNegotiationModalitiesBefore, lstNegotiationModalitiesAfter);
			validateParticipantFilesChanged(lstParticipantFileHyBefore, lstParticipantFileHyAfter);
			validateInstitutionBankAccountsChanged(lstInstitutionBankAccountHyBefore, lstInstitutionBankAccountHyAfter);
			validateParticipantInfoChanged(participantRequestHyBefore, participantRequestHyAfter);	
			
			
			
			setViewOperationType(ViewOperationsType.DETAIL.getCode());

			return "participantModificationDetailRule";

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}

	}
	
	/**
	 * Validate participant info changed.
	 *
	 * @param originalParticipant the original participant
	 * @param copyParticipant the copy participant
	 */
	private void validateParticipantInfoChanged(ParticipantRequestHistory originalParticipant, ParticipantRequestHistory copyParticipant){		
		
		indParticipantInfoChanged = Validations.validateOldAndNewObjectChanged(originalParticipant.getRoleParticipant(), copyParticipant.getRoleParticipant()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getResidenceCountry(), copyParticipant.getResidenceCountry()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getDocumentType(), copyParticipant.getDocumentType()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getDocumentNumber(), copyParticipant.getDocumentNumber()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getDescription(), copyParticipant.getDescription()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getMnemonic(), copyParticipant.getMnemonic()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getAccountType(), copyParticipant.getAccountType()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getAccountClass(), copyParticipant.getAccountClass()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getEconomicSector(), copyParticipant.getEconomicSector()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getEconomicActivity(), copyParticipant.getEconomicActivity()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getContractDate(), copyParticipant.getContractDate()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getCreationDate(), copyParticipant.getCreationDate()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getIndSettlementIncharge(), copyParticipant.getIndSettlementIncharge()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getCommercialRegister(), copyParticipant.getCommercialRegister()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getIndPayCommission(), copyParticipant.getIndPayCommission()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getIndDepositary(), copyParticipant.getIndDepositary()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getIndManagedThird(), copyParticipant.getIndManagedThird()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getDepartment(), copyParticipant.getDepartment()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getProvince(), copyParticipant.getProvince()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getDistrict(), copyParticipant.getDistrict()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getAddress(), copyParticipant.getAddress()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getPhoneNumber(), copyParticipant.getPhoneNumber()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getFaxNumber(), copyParticipant.getFaxNumber()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getWebsite(), copyParticipant.getWebsite()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getEmail(), copyParticipant.getEmail()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getContactName(), copyParticipant.getContactName()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getContactPhone(), copyParticipant.getContactPhone()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getContactEmail(), copyParticipant.getContactEmail()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getAccountsState(), copyParticipant.getAccountsState()) ||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getIssuerFk(), copyParticipant.getIssuerFk())||
				Validations.validateOldAndNewObjectChanged(originalParticipant.getComments(), copyParticipant.getComments());
		indParticipantInfoChanged = true;
	}
	
	/**
	 * Validate legal representatives changed.
	 *
	 * @param lstOldLegalRepresentative the lst old legal representative
	 * @param lstNewLegalRepresentative the lst new legal representative
	 */
	private void validateLegalRepresentativesChanged(List<LegalRepresentativeHistory> lstOldLegalRepresentative, List<LegalRepresentativeHistory> lstNewLegalRepresentative){
		indLegalRepresentativeChanged = false;
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstOldLegalRepresentative) && Validations.validateListIsNotNullAndNotEmpty(lstNewLegalRepresentative)){
			if(lstOldLegalRepresentative.size()!=lstNewLegalRepresentative.size()){
				indLegalRepresentativeChanged = true;
			} else {
				boolean existInNew = false;
				
				for (LegalRepresentativeHistory oldLegalRepHistory : lstOldLegalRepresentative) {
					existInNew = false;
					
					for (LegalRepresentativeHistory newLegalRepHistory : lstNewLegalRepresentative) {
						if(oldLegalRepHistory.getDocumentType().equals(newLegalRepHistory.getDocumentType()) && 
								oldLegalRepHistory.getDocumentNumber().equals(newLegalRepHistory.getDocumentNumber())) {
							existInNew = true;
							if(validateModifiedLegalRepresentativeInfo(oldLegalRepHistory, newLegalRepHistory)){
								indLegalRepresentativeChanged = true;
								return;
							}
						}
					}
					
					if(!existInNew){
						indLegalRepresentativeChanged = true;
						return;
					}
				}
				
			}
		} else if((Validations.validateListIsNotNullAndNotEmpty(lstOldLegalRepresentative) && Validations.validateListIsNullOrEmpty(lstNewLegalRepresentative)) ||
				(Validations.validateListIsNullOrEmpty(lstOldLegalRepresentative) && Validations.validateListIsNotNullAndNotEmpty(lstNewLegalRepresentative))){
			indLegalRepresentativeChanged = true;
		}
	}
	
	/**
	 * Validate modified legal representative info.
	 *
	 * @param oldLegalRepresentative the old legal representative
	 * @param newLegalRepresentative the new legal representative
	 * @return true, if successful
	 */
	private boolean validateModifiedLegalRepresentativeInfo(LegalRepresentativeHistory oldLegalRepresentative, LegalRepresentativeHistory newLegalRepresentative){
		return Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getPersonType(), newLegalRepresentative.getPersonType()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getRepresentativeClass(), newLegalRepresentative.getRepresentativeClass()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getNationality(), newLegalRepresentative.getNationality()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getIndResidence(), newLegalRepresentative.getIndResidence()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getLegalResidenceCountry(), newLegalRepresentative.getLegalResidenceCountry()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getDocumentType(), newLegalRepresentative.getDocumentType()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getDocumentNumber(), newLegalRepresentative.getDocumentNumber()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getSecondNationality(), newLegalRepresentative.getSecondNationality()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getSecondDocumentType(), newLegalRepresentative.getSecondDocumentType()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getSecondDocumentNumber(), newLegalRepresentative.getSecondDocumentNumber()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getName(), newLegalRepresentative.getName()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getFirstLastName(), newLegalRepresentative.getFirstLastName()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getSecondLastName(), newLegalRepresentative.getSecondLastName()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getBirthDate(), newLegalRepresentative.getBirthDate()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getSex(), newLegalRepresentative.getSex()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getCategory(), newLegalRepresentative.getCategory()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getRole(), newLegalRepresentative.getRole()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getBeginningPeriod(), newLegalRepresentative.getBeginningPeriod()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getEndingPeriod(), newLegalRepresentative.getEndingPeriod()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getLegalDepartment(), newLegalRepresentative.getLegalDepartment()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getLegalProvince(), newLegalRepresentative.getLegalProvince()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getLegalDistrict(), newLegalRepresentative.getLegalDistrict()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getLegalAddress(), newLegalRepresentative.getLegalAddress()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getPostalResidenceCountry(), newLegalRepresentative.getPostalResidenceCountry()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getPostalDepartment(), newLegalRepresentative.getPostalDepartment()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getPostalProvince(), newLegalRepresentative.getPostalProvince()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getPostalDistrict(), newLegalRepresentative.getPostalDistrict()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getPostalAddress(), newLegalRepresentative.getPostalAddress()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getHomePhoneNumber(), newLegalRepresentative.getHomePhoneNumber()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getOfficePhoneNumber(), newLegalRepresentative.getOfficePhoneNumber()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getMobileNumber(), newLegalRepresentative.getMobileNumber()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getEmail(), newLegalRepresentative.getEmail()) ||
				Validations.validateOldAndNewObjectChanged(oldLegalRepresentative.getFaxNumber(), newLegalRepresentative.getFaxNumber()) || 
				validateRepresentativeFilesChanged(oldLegalRepresentative.getRepresenteFileHistory(), newLegalRepresentative.getRepresenteFileHistory());
	}
	
	/**
	 * Validate representative files changed.
	 *
	 * @param oldFiles the old files
	 * @param newFiles the new files
	 * @return true, if successful
	 */
	private boolean validateRepresentativeFilesChanged(List<RepresentativeFileHistory> oldFiles, List<RepresentativeFileHistory> newFiles){
		
		if(Validations.validateListIsNotNullAndNotEmpty(oldFiles) && Validations.validateListIsNotNullAndNotEmpty(newFiles)){
			if(oldFiles.size() != newFiles.size()){
				return true;
			} else {
				boolean existInNewFiles = false;
				for (RepresentativeFileHistory oldFile : oldFiles) {
					existInNewFiles = false;
					
					for (RepresentativeFileHistory newFile : newFiles) {
						if(oldFile.getDocumentType().equals(newFile.getDocumentType()) && oldFile.getFilename().equals(newFile.getFilename()) &&
								Arrays.equals(oldFile.getFileRepresentative(), newFile.getFileRepresentative())) {
							existInNewFiles = true;
						}
					}
					
					if(!existInNewFiles){
						return true;
					}
				}
				
				return false;
			}
		} else if((Validations.validateListIsNotNullAndNotEmpty(oldFiles) && Validations.validateListIsNullOrEmpty(newFiles)) || 
				Validations.validateListIsNullOrEmpty(oldFiles) && Validations.validateListIsNotNullAndNotEmpty(newFiles)){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Validate participant files changed.
	 *
	 * @param oldFiles the old files
	 * @param newFiles the new files
	 */
	private void validateParticipantFilesChanged(List<ParticipantFileHistory> oldFiles, List<ParticipantFileHistory> newFiles){
		indParticipantFilesChanged = false;
		if(Validations.validateListIsNotNullAndNotEmpty(oldFiles) && Validations.validateListIsNotNullAndNotEmpty(newFiles)){
			if(oldFiles.size() != newFiles.size()){
				indParticipantFilesChanged = true;
			} else {
				boolean existInNewFiles = false;
				for (ParticipantFileHistory oldFile : oldFiles) {
					existInNewFiles = false;
					
					for (ParticipantFileHistory newFile : newFiles) {
						if(validateSameFile(oldFile, newFile)) {
							existInNewFiles = true;
						}
					}
					
					if(!existInNewFiles){
						indParticipantFilesChanged = true;
						return;
					}
				}
			}
		} else if((Validations.validateListIsNotNullAndNotEmpty(oldFiles) && Validations.validateListIsNullOrEmpty(newFiles)) || 
				Validations.validateListIsNullOrEmpty(oldFiles) && Validations.validateListIsNotNullAndNotEmpty(newFiles)){
			indParticipantFilesChanged = true;
		} else {
			indParticipantFilesChanged = false;
		}
	}
	
	/**
	 * Validate participant files changed.
	 *
	 * @param oldFiles the old files
	 * @param newFiles the new files
	 */
	private void validateInstitutionBankAccountsChanged(List<InstitutionBankAccountHistory> oldInstitutionBankAccounts, List<InstitutionBankAccountHistory> newInstitutionBankAccounts){
		indInstitutionBankAccountsChanged = false;
		if(Validations.validateListIsNotNullAndNotEmpty(oldInstitutionBankAccounts) && Validations.validateListIsNotNullAndNotEmpty(newInstitutionBankAccounts)){
			if(oldInstitutionBankAccounts.size() != newInstitutionBankAccounts.size()){
				indInstitutionBankAccountsChanged = true;
			} else {
				boolean existInNewInstitutionBankAccounts = false;
				for (InstitutionBankAccountHistory oldInstitutionBankAccount : oldInstitutionBankAccounts) {
					existInNewInstitutionBankAccounts = false;
					
					for (InstitutionBankAccountHistory newInstitutionBankAccount : newInstitutionBankAccounts) {
						if(oldInstitutionBankAccount.getProviderBank().getIdBankPk().equals(newInstitutionBankAccount.getProviderBank().getIdBankPk())
								&& oldInstitutionBankAccount.getAccountNumber().equals(newInstitutionBankAccount.getAccountNumber())
								&& oldInstitutionBankAccount.getBankAcountType().equals(newInstitutionBankAccount.getBankAcountType())
								&& oldInstitutionBankAccount.getCurrency().equals(newInstitutionBankAccount.getCurrency())) {
							existInNewInstitutionBankAccounts = false;
						}else {
							existInNewInstitutionBankAccounts = true;
						}
					}
					
					if(existInNewInstitutionBankAccounts){
						indInstitutionBankAccountsChanged = true;
						return;
					}
				}
			}
		} else if((Validations.validateListIsNotNullAndNotEmpty(oldInstitutionBankAccounts) && Validations.validateListIsNullOrEmpty(newInstitutionBankAccounts)) || 
				Validations.validateListIsNullOrEmpty(oldInstitutionBankAccounts) && Validations.validateListIsNotNullAndNotEmpty(newInstitutionBankAccounts)){
			indInstitutionBankAccountsChanged = true;
		} else {
			indInstitutionBankAccountsChanged = false;
		}
	}
	
	/**
	 * Validate same file.
	 *
	 * @param oldFile the old file
	 * @param newFile the new file
	 * @return true, if successful
	 */
	private boolean validateSameFile(ParticipantFileHistory oldFile, ParticipantFileHistory newFile){
		return filesWithSameType(oldFile, newFile) && filesWithSameName(oldFile, newFile) && filesWithSameContent(oldFile, newFile);
	}
	
	/**
	 * Files with same type.
	 *
	 * @param oldFile the old file
	 * @param newFile the new file
	 * @return true, if successful
	 */
	private boolean filesWithSameType(ParticipantFileHistory oldFile, ParticipantFileHistory newFile){
				if(Validations.validateIsNull(oldFile.getDocumentType()) && Validations.validateIsNull(newFile.getDocumentType()) ) {
			return true;
		} else if( (Validations.validateIsNull(oldFile.getDocumentType()) && Validations.validateIsNotNull(newFile.getDocumentType())) || 
				(Validations.validateIsNotNull(oldFile.getDocumentType()) && Validations.validateIsNull(newFile.getDocumentType()))){
			return false;
		} else {
			return oldFile.getDocumentType().equals(newFile.getDocumentType());
		}
	}
	
	/**
	 * Files with same name.
	 *
	 * @param oldFile the old file
	 * @param newFile the new file
	 * @return true, if successful
	 */
	private boolean filesWithSameName(ParticipantFileHistory oldFile, ParticipantFileHistory newFile){
		if(Validations.validateIsNull(oldFile.getFilename()) && Validations.validateIsNull(newFile.getFilename()) ) {
			return true;
		} else if( (Validations.validateIsNull(oldFile.getFilename()) && Validations.validateIsNotNull(newFile.getFilename())) || 
				(Validations.validateIsNotNull(oldFile.getFilename()) && Validations.validateIsNull(newFile.getFilename()))){
			return false;
		} else {
			return oldFile.getFilename().equals(newFile.getFilename());
		}
	}
	
	/**
	 * Files with same content.
	 *
	 * @param oldFile the old file
	 * @param newFile the new file
	 * @return true, if successful
	 */
	private boolean filesWithSameContent(ParticipantFileHistory oldFile, ParticipantFileHistory newFile){
		if(Validations.validateIsNull(oldFile.getFile()) && Validations.validateIsNull(newFile.getFile()) ) {
			return true;
		} else if( (Validations.validateIsNull(oldFile.getFile()) && Validations.validateIsNotNull(newFile.getFile())) || 
				(Validations.validateIsNotNull(oldFile.getFile()) && Validations.validateIsNull(newFile.getFile()))){
			return false;
		} else {
			return Arrays.equals(oldFile.getFile(), newFile.getFile());
		}
	}
	
	/**
	 * Validate participant int depositary changed.
	 *
	 * @param lstOldIntDepository the lst old int depository
	 * @param newNewIntDepository the new new int depository
	 */
	private void validateParticipantIntDepositaryChanged(List<InternationalDepository> lstOldIntDepository, List<InternationalDepository> newNewIntDepository){
		indParticipantIndDeposChanged = false;
		for (InternationalDepository oldIntDepository : lstOldIntDepository) {
			for (InternationalDepository newIntDepository : newNewIntDepository) {
				if(oldIntDepository.getIdInternationalDepositoryPk().equals(
								newIntDepository.getIdInternationalDepositoryPk()) &&
								oldIntDepository.isSelected()!=newIntDepository.isSelected()){
					indParticipantIndDeposChanged = true;
					return;
				}
			}
		}
	}
	
	/**
	 * Validate participant mech changed.
	 *
	 * @param lstOldNegotiationMechanism the lst old negotiation mechanism
	 * @param lstNewNegotiationMechanism the lst new negotiation mechanism
	 */
	private void validateParticipantMechChanged(List<NegotiationModality> lstOldNegotiationMechanism, List<NegotiationModality> lstNewNegotiationMechanism){
		indParticipantMechanismsChanged = false;
		StringBuilder sbIdOldConverted=new StringBuilder();
		StringBuilder sbIdNewConverted=new StringBuilder();
		for (NegotiationModality oldNegotiationModality : lstOldNegotiationMechanism) {
			for (NegotiationMechanism oldNegotiationMechanism : oldNegotiationModality.getNegotiationMechanisms()) {
				sbIdOldConverted.delete(0, sbIdOldConverted.length());
				sbIdOldConverted.append(oldNegotiationMechanism.getIdNegotiationMechanismPk()).append("-").append(oldNegotiationModality.getIdNegotiationModalityPk());
				
				for (NegotiationModality newNegotiationModality : lstNewNegotiationMechanism) {
					for (NegotiationMechanism newNegotiationMechanism : newNegotiationModality.getNegotiationMechanisms()) {
						sbIdNewConverted.delete(0, sbIdNewConverted.length());
						sbIdNewConverted.append(newNegotiationMechanism.getIdNegotiationMechanismPk()).append("-").append(newNegotiationModality.getIdNegotiationModalityPk());											
					
						if(sbIdOldConverted.toString().equals(sbIdNewConverted.toString()) && oldNegotiationMechanism.isSelected()!=newNegotiationMechanism.isSelected()){
							indParticipantMechanismsChanged = true;
							return;
						}
						
					}
				}
				
			}
		}
	}
	
	
	/**
	 * Fill list international depository.
	 *
	 * @param objParticipantRequest the obj participant request
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<InternationalDepository> fillListInternationalDepository(ParticipantRequest objParticipantRequest, Integer indNew) throws ServiceException{
		// List the International Depositaries
		InternationalDepositoryTO filterInternationalDepository = new InternationalDepositoryTO();
		filterInternationalDepository.setStateIntDepository(Integer.valueOf(InternationalDepositoryStateType.ACTIVE.getCode()));
		List<InternationalDepository> lstInternationalDepository = participantServiceFacade.getListInternationalDepositoryServiceFacade(filterInternationalDepository);
					
		ParticipantIntDepositoryTO participantIntDepositoryTO = new ParticipantIntDepositoryTO();
		participantIntDepositoryTO.setStateParticipantIntDepositary(ParticipantIntDepositaryStateType.REGISTERED.getCode());
		participantIntDepositoryTO.setIdParticipantRequestPk(objParticipantRequest.getIdParticipantRequestPk());
		participantIntDepositoryTO.setIndNew(indNew);

		// List ParticipantIntDepositaries of the selected Participant
		List<ParticipantIntDepoHistory> partIntDepositHistoriesBefore = 
				participantServiceFacade.getListParticipantIntDepoHistoryByParticipantServiceFacade(participantIntDepositoryTO);

		for (InternationalDepository internationalDepository : lstInternationalDepository) {
			for (ParticipantIntDepoHistory participantIntDepoHistory : partIntDepositHistoriesBefore) {
				if (participantIntDepoHistory.getInternationalDepository().getIdInternationalDepositoryPk().
						equals(internationalDepository.getIdInternationalDepositoryPk())) {
					internationalDepository.setSelected(Boolean.TRUE);
					break;
				}
			}
		}
		
		return lstInternationalDepository;
	}

	/**
	 * Validate email format.
	 */
	public void validateEmailFormat() {
		hideDialogsListener(null);
		JSFUtilities
				.setValidViewComponent("frmParticipantRegister:tabViewParticipant:txtEmail");
		if (Validations.validateIsNotNullAndNotEmpty(participant.getEmail())) {
			if (!CommonsUtilities.matchEmail(participant.getEmail())) {
				JSFUtilities
						.addContextMessage(
								"frmParticipantRegister:tabViewParticipant:txtEmail",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_EMAIL_FORMAT),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_EMAIL_FORMAT));

				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ADM_PARTICIPANT_VALIDATION_EMAIL_FORMAT,
						null);
				// Clean the email
				participant.setEmail(null);
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/**
	 * Validate web page format.
	 */
	public void validateWebPageFormat() {
		hideDialogsListener(null);
		JSFUtilities
				.setValidViewComponent("frmParticipantRegister:tabViewParticipant:txtWebPage");
		if (Validations.validateIsNotNullAndNotEmpty(participant.getWebsite())) {
			if (!CommonsUtilities.matchWebPage(participant.getWebsite())) {
				JSFUtilities
						.addContextMessage(
								"frmParticipantRegister:tabViewParticipant:txtWebPage",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_WEB_PAGE_FORMAT),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_WEB_PAGE_FORMAT));

				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ADM_PARTICIPANT_VALIDATION_WEB_PAGE_FORMAT,
						null);
				// Clean the web site
				participant.setWebsite(null);
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/**
	 * Validate address format.
	 */
	public void validateAddressFormat() {
		hideDialogsListener(null);
		JSFUtilities
				.setValidViewComponent("frmParticipantRegister:tabViewParticipant:txtAddress");
		if (Validations.validateIsNotNullAndNotEmpty(participant.getAddress())) {
			if (!CommonsUtilities.matchAddress(participant.getAddress())) {
				JSFUtilities
						.addContextMessage(
								"frmParticipantRegister:tabViewParticipant:txtAddress",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_ADDRESS_FORMAT),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_ADDRESS_FORMAT));

				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ADM_PARTICIPANT_VALIDATION_ADDRESS_FORMAT,
						null);
				// Clean the address
				participant.setAddress(null);
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/**
	 * Validate contact email format.
	 */
	public void validateContactEmailFormat() {
		hideDialogsListener(null);
		JSFUtilities
				.setValidViewComponent("frmParticipantRegister:tabViewParticipant:txtContactEmail");
		if (Validations.validateIsNotNullAndNotEmpty(participant
				.getContactEmail())) {
			if (!CommonsUtilities.matchEmail(participant.getContactEmail())) {
				JSFUtilities
						.addContextMessage(
								"frmParticipantRegister:tabViewParticipant:txtContactEmail",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL_FORMAT),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL_FORMAT));

				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
						null,
						PropertiesConstants.ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL_FORMAT,
						null);
				// Clean the contact email
				participant.setContactEmail(null);
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	

	/**
	 * Change selected contract date.
	 * 
	 * @param event
	 *            the event
	 */
	public void changeSelectedCreationDate(SelectEvent event){
		minDateParticipantContract = CommonsUtilities.copyDate(participant.getCreationDate());	
		
		if(Validations.validateIsNotNull(participant.getContractDate())){
			if(participant.getCreationDate().after(participant.getContractDate())){
				participant.setContractDate(null);
			}
		}
				
	}
	
	/**
	 * Begin reject participant request.
	 */
	@LoggerAuditWeb
	public void beginRejectParticipantRequest(){
		try {
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
		
		if(Validations.validateIsNull(participantRequest)){
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_RECORD_NOT_SELECTED,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return;
		}
		
		Integer stateParticipantRequest = 
				participantServiceFacade.findParticipantRequestStateByIdServiceFacade(participantRequest.getIdParticipantRequestPk());
		
		if(!ParticipantRequestStateType.REGISTERED.getCode().equals(stateParticipantRequest)){
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_RECORD_SELECTED_NOT_REGISTERED_STATE,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return;
		}
		
		loadLstParticipantRequestRejectMotives();
		
		participantRequest.setRejectedMotive(null);
		participantRequest.setRejectOtherMotive(null);
		
		indDisableAcceptMotiveReject = Boolean.TRUE;
		indSelectedOtherMotiveReject = Boolean.FALSE;
		
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveRejectIssRequest').show()");
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
			
	}
	
	/**
	 * Load lst participant request reject motives.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstParticipantRequestRejectMotives() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_MODIFICATION_REJECT_MOTIVE.getCode());		
		lstParticipantRequestRejectMotives = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * Change motive reject.
	 */
	@LoggerAuditWeb
	public void changeMotiveReject(){
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
		if(PartModificationRejectMotiveType.OTHER_MOTIVE_REJECT.getCode().equals(participantRequest.getRejectedMotive())){
			indSelectedOtherMotiveReject = Boolean.TRUE;
			indDisableAcceptMotiveReject = Boolean.TRUE;
		} else {
			indSelectedOtherMotiveReject = Boolean.FALSE;
			if(Validations.validateIsNotNullAndPositive(participantRequest.getRejectedMotive())){
				indDisableAcceptMotiveReject = Boolean.FALSE;
			} else {
				indDisableAcceptMotiveReject = Boolean.TRUE;
			}			
		}
		participantRequest.setRejectOtherMotive(null);		
	}
	
	/**
	 * Type other motive reject.
	 */
	public void typeOtherMotiveReject(){	
		if(Validations.validateIsNotNullAndNotEmpty(participantRequest.getRejectOtherMotive())){
			setIndDisableAcceptMotiveReject(Boolean.FALSE);
		} else {
			setIndDisableAcceptMotiveReject(Boolean.TRUE); 
		}
	}
	
	/**
	 * Accept reject motive participant request.
	 */
	public void acceptRejectMotiveParticipantRequest(){
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
		
		int countValidationRequiredErrors = 0;
    	
    	if(Validations.validateIsNullOrNotPositive(participantRequest.getRejectedMotive())){
    		JSFUtilities.addContextMessage("frmMotiveRejectIssRequest:cboMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	if(indSelectedOtherMotiveReject &&
    			Validations.validateIsNullOrEmpty(participantRequest.getRejectOtherMotive())){
    		JSFUtilities.addContextMessage("frmMotiveRejectIssRequest:txtOtherMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	
    	if(countValidationRequiredErrors > 0){
    		showMessageOnDialog(
    				PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.MESSAGE_DATA_REQUIRED,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return;
    	}
    	
    	Object[] arrBodyData = {String.valueOf(participantRequest.getRequestNumber())};
		showMessageOnDialog(
				PropertiesConstants.MESSAGES_CONFIRM,
				null,
				PropertiesConstants.LBL_CONFIRM_PARTICIPANT_REQUEST_REJECT,							
				arrBodyData);
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveRejectIssRequest').hide()");
		JSFUtilities.executeJavascriptFunction("PF('cnfwIssuerRequestReject').show()");
    	
	}
	
	/**
	 * Save reject participant request.
	 */
	@LoggerAuditWeb
	public void saveRejectParticipantRequest(){
		try {
			hideDialogsListener(null);			
			
			ParticipantRequest participantRequestResult = participantServiceFacade.rejectParticipantModificationRequestServiceFacade(participantRequest);
			
			BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.MODIFICATION_PARTICIPANT_REJECT.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
														businessProcessNotification, 
														participantRequest.getParticipant().getIdParticipantPk(), null);
			
			//if(transactionResult) {
			ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequestResult.getRequestType());
				Object[] arrBodyData = {paramRequestType.getParameterName(), String.valueOf(participantRequestResult.getRequestNumber())};
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_SUCCESFUL, null,
						PropertiesConstants.LBL_SUCCESS_PARTICIPANT_REQUEST_REJECT, 
						arrBodyData);					
				cleanSearchModifRequest();
				
				
				JSFUtilities.showSimpleEndTransactionDialog("searchParticipantModification");					
									
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
				
				JSFUtilities.executeJavascriptFunction("PF('cnfwIssuerRequestReject').hide()");
			//}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Detail confirm participant modification.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailConfirmParticipantModification(){
		try {
		hideDialogsListener(null);
		
		if(Validations.validateIsNull(participantRequest)){
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_RECORD_NOT_SELECTED,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return null;
		}
		
		Integer stateParticipantRequest = 
				participantServiceFacade.findParticipantRequestStateByIdServiceFacade(participantRequest.getIdParticipantRequestPk());
		
		if(!ParticipantRequestStateType.REGISTERED.getCode().equals(stateParticipantRequest)){
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_RECORD_SELECTED_NOT_REGISTERED_STATE,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return null;
		}
		
		idParticipantRequestPk = participantRequest.getIdParticipantRequestPk();
		
		detailParticipantModification();
		
		setViewOperationType(ViewOperationsType.CONFIRM.getCode());

		return "participantModificationDetailRule";
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Detail reject participant modification.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailRejectParticipantModification(){
		try {
			hideDialogsListener(null);
			
			if(Validations.validateIsNull(participantRequest)){
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ERROR_RECORD_NOT_SELECTED,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return null;
			}
			
			Integer stateParticipantRequest = 
					participantServiceFacade.findParticipantRequestStateByIdServiceFacade(participantRequest.getIdParticipantRequestPk());
			
			if(!ParticipantRequestStateType.REGISTERED.getCode().equals(stateParticipantRequest)){
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ERROR_RECORD_SELECTED_NOT_REGISTERED_STATE,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return null;
			}
			
			idParticipantRequestPk = participantRequest.getIdParticipantRequestPk();
			
			detailParticipantModification();
			
			setViewOperationType(ViewOperationsType.REJECT.getCode());

			return "participantModificationDetailRule";
			
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
				return null;
			}
	}
	
	/**
	 * Begin confirm participant request.
	 */
	@LoggerAuditWeb
	public void beginConfirmParticipantRequest(){
		try{
			hideDialogsListener(null);
			
			if(Validations.validateIsNull(participantRequest)){
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ERROR_RECORD_NOT_SELECTED,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
			}			
			
			Integer stateParticipantRequest = 
					participantServiceFacade.findParticipantRequestStateByIdServiceFacade(participantRequest.getIdParticipantRequestPk());
			
			if(!ParticipantRequestStateType.REGISTERED.getCode().equals(stateParticipantRequest)){
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ERROR_RECORD_SELECTED_NOT_REGISTERED_STATE,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
			}
			

			ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequest.getRequestType());
			Object[] arrBodyData = {paramRequestType.getParameterName(), String.valueOf(participantRequest.getRequestNumber())};
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_CONFIRM,
					null,
					PropertiesConstants.LBL_CONFIRM_PARTICIPANT_REQUEST_CONFIRMATION,							
					arrBodyData);
			
						
			JSFUtilities.showComponent("cnfIssuerRequestConfirmation");
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Load list holder file type parameter.
	 *
	 * @return the list
	 */
	private List<HolderFile> loadListHolderFileTypeParameter(){
		
		List<HolderFile> lstHolderFiles = new ArrayList<HolderFile>();
		try {

		Document pdfDocument;
		ByteArrayOutputStream byteArrayOutputStream;
		HolderFile holderFile;
		StringBuilder sbFileName;
		String fileNamePrefix = PropertiesUtilities.getMessage(PropertiesConstants.AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_NAME_PREFIX);
		String fileExtension = PropertiesUtilities.getMessage(PropertiesConstants.AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_EXTENSION);
		String contentFile = PropertiesUtilities.getMessage(PropertiesConstants.AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_CONTENT);
		
		loadHolderFileTypeParameter(MasterTableType.BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_JUR.getCode());				
		
		for (int i = 0 ; i<lsttHolderFileTypes.size(); i++) {
			pdfDocument = new Document();

			// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
			byteArrayOutputStream = new ByteArrayOutputStream();
			
			PdfWriter.getInstance(pdfDocument, byteArrayOutputStream);

			pdfDocument.open();  
			pdfDocument.setPageSize(PageSize.A4);  
		  
			pdfDocument.add(new Paragraph(contentFile));		
			
			byteArrayOutputStream.flush();			
			byteArrayOutputStream.close();
			pdfDocument.close();
			
			sbFileName = new StringBuilder();
			sbFileName.append(fileNamePrefix);
			sbFileName.append(i+1);
			sbFileName.append(fileExtension);
			
			holderFile = new HolderFile();			
			holderFile.setFilename(sbFileName.toString());
			holderFile.setFileHolder(byteArrayOutputStream.toByteArray());			
			holderFile.setDocumentType(lsttHolderFileTypes.get(i).getParameterTablePk());
			holderFile.setDescription(lsttHolderFileTypes.get(i).getParameterName());			
			
			lstHolderFiles.add(holderFile);
			
		}
						
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		return lstHolderFiles;
	}
	
	/**
	 * Load holder file type parameter.
	 *
	 * @param masterTableTypeCode the master table type code
	 */
	private void loadHolderFileTypeParameter(Integer masterTableTypeCode){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(masterTableTypeCode);
			
			lsttHolderFileTypes = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Save confirm participant request.
	 */
	@LoggerAuditWeb
	public void saveConfirmParticipantRequest(){
		try {
			//hideDialogsListener(null);
			Holder holderFound = participantServiceFacade.findHolderByParticipant(participant);
			if(holderFound == null){
				participantRequest.setHolderFiles(loadListHolderFileTypeParameter());
				participantRequest.setIndHolderWillBeCreated(true);
			}
			boolean transactionResult = participantServiceFacade.confirmParticipantModificationRequestServiceFacade(participantRequest);
			
			if(transactionResult){
				
				BusinessProcess businessProcessNotification = new BusinessProcess();					
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.MODIFICATION_PARTICIPANT_CONFIRM.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															businessProcessNotification, 
															participantRequest.getParticipant().getIdParticipantPk(), null);		
				
				//if(transactionResult) {
				ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequest.getRequestType());
				Object[] arrBodyData = {paramRequestType.getParameterName(), String.valueOf(participantRequest.getRequestNumber())};
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_SUCCESFUL, null,
						PropertiesConstants.LBL_SUCCESS_PARTICIPANT_REQUEST_CONFINRMATION, 
						arrBodyData);					
				cleanSearchModifRequest();
				
				
				JSFUtilities.showComponent("cnfEndTransactionParticipant");						
				
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
				
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Change selected participant file type.
	 */
	public void changeSelectedParticipantFileType(){
		participantFileNameDisplay = null;			
		if(Validations.validateIsNotNullAndPositive(participantFileType)){
			validateFileAlreadyLoaded();
		}
	}
	
	/**
	 * Validate file already loaded.
	 *
	 * @return true, if successful
	 */
	private boolean validateFileAlreadyLoaded(){
		if(Validations.validateListIsNullOrEmpty(lstParticipantFiles)){
			return false;
		} else {
			for (ParticipantFile objParticipantFile : lstParticipantFiles) {
				if(participantFileType.equals(objParticipantFile.getDocumentType())){
					participantFileType = null;
					participantFileNameDisplay = null;
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_DOCUMENT_ALREADY_LOADED));
					JSFUtilities.showSimpleValidationDialog();
					return true;
				}
			}
			return false;
		}
			
	}
	
	/**
	 * Insert or replace participant file item.
	 *
	 * @param participantFile the participant file
	 */
	public void insertOrReplaceParticipantFileItem(ParticipantFile participantFile){
		
		boolean flag = false;
		
		int listSize = lstParticipantFiles.size();
		if(listSize>0){
			for(int i=0;i<listSize;i++)
			{
			
				if(participantFile.getDocumentType().equals(lstParticipantFiles.get(i).getDocumentType())){
					lstParticipantFiles.remove(i);
					lstParticipantFiles.add(i,participantFile);
					participantFileType = null;
					
					flag = true;
				}
			}
			if(!flag){
				lstParticipantFiles.add(participantFile);
				participantFileType = null;;
			}
		}
		else{
			lstParticipantFiles.add(participantFile);
			participantFileType = null;
		}
		
	}
	
	/**
	 * Document attach participant file.
	 *
	 * @param event the event
	 */
	public void documentAttachParticipantFile(FileUploadEvent event){
		
		if(Validations.validateIsNotNullAndPositive(participantFileType)){

			
			String fDisplayName=fUploadValidateFile(event.getFile(), 
					  null,fUploadFileSize, getfUploadFileTypesImgPdfXlsTypes());
			 
			 if(fDisplayName!=null){
				 participantFileNameDisplay=fDisplayName;
			 }

			 for(int i=0;i<lstParticipantFileTypes.size();i++){
				if(lstParticipantFileTypes.get(i).getParameterTablePk().equals(participantFileType)){					
				    	ParticipantFile participantFile = new ParticipantFile();
				    	participantFile.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());
				    	participantFile.setDocumentType(lstParticipantFileTypes.get(i).getParameterTablePk());
					    participantFile.setDescription(lstParticipantFileTypes.get(i).getParameterName());

				    	participantFile.setFilename(fUploadValidateFileNameLength(event.getFile().getFileName()));
				    	participantFile.setFile(event.getFile().getContents());		    	
				    	participantFile.setStateFile(ParticipantFileStateType.REGISTERED.getCode());
				    	participantFile.setParticipant(participant);				    	

					
				    	insertOrReplaceParticipantFileItem(participantFile);
				}
			}
			 
			 filesModified = BooleanType.YES.getValue();
		}
		
	}
	
	
	
	
	
	/**
	 * Removes the participant file.
	 *
	 * @param participantFile the participant file
	 */
	public void removeParticipantFile(ParticipantFile participantFile) {
		lstParticipantFiles.remove(participantFile);
		participantFileNameDisplay = null;
		participantFileType = null;		
	}
	
	
	/**
	 * Change initial date search.
	 *
	 * @param event the event
	 */
	public void changeInitialDateSearch(SelectEvent event){
		if(participantTO.getParticipantRequestTO().getEndRequestDate().after(CommonsUtilities.addDaysToDate(participantTO.getParticipantRequestTO().getInitiaRequestDate(), 60))){			
			if(CommonsUtilities.addDaysToDate(participantTO.getParticipantRequestTO().getInitiaRequestDate(), 60).after(getCurrentSystemDate())){
				participantTO.getParticipantRequestTO().setEndRequestDate(getCurrentSystemDate());
			} else {
				participantTO.getParticipantRequestTO().setEndRequestDate(CommonsUtilities.addDaysToDate(participantTO.getParticipantRequestTO().getInitiaRequestDate(), 60));
			}						
		} else if(participantTO.getParticipantRequestTO().getEndRequestDate().before(participantTO.getParticipantRequestTO().getInitiaRequestDate())) {
			participantTO.getParticipantRequestTO().setEndRequestDate(CommonsUtilities.copyDate(participantTO.getParticipantRequestTO().getInitiaRequestDate()));
		}
	}
	
	public void changeEconomicActivity(){
		if(Validations.validateIsNotNull(participant) && Validations.validateIsNotNull(participant.getEconomicActivity())){
			buildInvestorTypeByEconAcSelected(participant.getEconomicActivity());
		}
	}
	
	public void buildInvestorTypeByEconAcSelected(Integer economicActivity){
		// construyendo la lista de tipo de inversionista
		InvestorTypeModelTO it = new InvestorTypeModelTO();
		it.setEconomicActivity(economicActivity);
		try {
			listInvestorType = investorTypeFacade.getLstInvestorTypeModelTO(it);
			
			// sugerir el tipo de inversionista por defecto
			if(Validations.validateIsNotNull(participant)){
				participant.setInvestorType(getInvestorTypeByDefault(economicActivity));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			listInvestorType = new ArrayList<>();
		}
	}
	
	private Integer getInvestorTypeByDefault(Integer economicActivity){
		try {
			if(Validations.validateIsNotNullAndPositive(economicActivity)){
				List<ParameterTable> lstEconomicActivityWithInvetorType = investorTypeFacade.getLstEconomicActivityToRelationInvestortype();
				for(ParameterTable pt:lstEconomicActivityWithInvetorType){
					if( Validations.validateIsNotNull(pt) 
						&& Validations.validateIsNotNullAndPositive(pt.getParameterTablePk())
						&& pt.getParameterTablePk().equals(economicActivity) ){
						return pt.getShortInteger();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("::: Error al sugerir el tipo de inversionista: (error irrelevante) "+e.getMessage());
		}
		return null;
	}
	
	/**
	 * Change selected document type search.
	 */
	public void changeSelectedDocumentTypeSearch(){
		participantTO.setDocumentNumber(null);
	}
	
	/**
	 * ValidateIssuer Method
	 */
	public void validateIssuer(){
		if(Validations.validateIsNullOrEmpty(originalIssuer)){
			participant.setIssuer(new Issuer());
			participant.getIssuer().setStateIssuer(IssuerStateType.REGISTERED.getCode());
		}else{
			participant.setIssuer(originalIssuer);
			originalIssuer = null;
		}
		
	}
	
	/**
	 * Gets the max length document number search.
	 *
	 * @return the max length document number search
	 */
	public Integer getMaxLengthDocumentNumberSearch() {
		if(Validations.validateIsNotNull(participantTO) && DocumentType.RUC.getCode().equals(participantTO.getDocumentType())) {
			return 9;
		} else {
			return 20;
		}
	}
	
	/**
	 * Checks if is document number search numeric.
	 *
	 * @return true, if is document number search numeric
	 */
	public boolean isDocumentNumberSearchNumeric(){
		return Validations.validateIsNotNull(participantTO) && DocumentType.RUC.getCode().equals(participantTO.getDocumentType());
	}
	
	/**
	 * Gets the min final date.
	 *
	 * @return the min final date
	 */
	public Date getMinFinalDate(){
		if(Validations.validateIsNotNull(participantTO) && Validations.validateIsNotNull(participantTO.getParticipantRequestTO()) &&
				Validations.validateIsNotNullAndNotEmpty(participantTO.getParticipantRequestTO().getInitiaRequestDate())) {
			return CommonsUtilities.copyDate(participantTO.getParticipantRequestTO().getInitiaRequestDate());
		} else {
			return null;
		}
	}
	
	/**
	 * Gets the max final date.
	 *
	 * @return the max final date
	 */
	public Date getMaxFinalDate(){
//		if(Validations.validateIsNotNull(participantTO) && Validations.validateIsNotNull(participantTO.getParticipantRequestTO()) &&
//				Validations.validateIsNotNullAndNotEmpty(participantTO.getParticipantRequestTO().getInitiaRequestDate())) {
//			if(CommonsUtilities.addDaysToDate(participantTO.getParticipantRequestTO().getInitiaRequestDate(), 60).after(getCurrentSystemDate())){
//				return getCurrentSystemDate();
//			} else {
//				return CommonsUtilities.addDaysToDate(participantTO.getParticipantRequestTO().getInitiaRequestDate(), 60);
//			}
//		} else {
			return getCurrentSystemDate();
//		}
	}
		

	/**
	 * Gets the participant.
	 * 
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 * 
	 * @param participant
	 *            the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}	

	/**
	 * Gets the lst negotiation modalities.
	 *
	 * @return the lst negotiation modalities
	 */
	public List<NegotiationModality> getLstNegotiationModalities() {
		return lstNegotiationModalities;
	}

	/**
	 * Sets the lst negotiation modalities.
	 *
	 * @param lstNegotiationModalities the new lst negotiation modalities
	 */
	public void setLstNegotiationModalities(
			List<NegotiationModality> lstNegotiationModalities) {
		this.lstNegotiationModalities = lstNegotiationModalities;
	}

	/**
	 * Gets the lst countries.
	 * 
	 * @return the lst countries
	 */
	public List<GeographicLocation> getLstCountries() {
		return lstCountries;
	}

	/**
	 * Sets the lst countries.
	 * 
	 * @param lstCountries
	 *            the new lst countries
	 */
	public void setLstCountries(List<GeographicLocation> lstCountries) {
		this.lstCountries = lstCountries;
	}

	/**
	 * Gets the lst provinces.
	 * 
	 * @return the lst provinces
	 */
	public List<GeographicLocation> getLstProvinces() {
		return lstProvinces;
	}

	/**
	 * Sets the lst provinces.
	 * 
	 * @param lstProvinces
	 *            the new lst provinces
	 */
	public void setLstProvinces(List<GeographicLocation> lstProvinces) {
		this.lstProvinces = lstProvinces;
	}

	/**
	 * Gets the lst districts.
	 * 
	 * @return the lst districts
	 */
	public List<GeographicLocation> getLstDistricts() {
		return lstDistricts;
	}

	/**
	 * Sets the lst districts.
	 * 
	 * @param lstDistricts
	 *            the new lst districts
	 */
	public void setLstDistricts(List<GeographicLocation> lstDistricts) {
		this.lstDistricts = lstDistricts;
	}

	/**
	 * Gets the lst participant class.
	 * 
	 * @return the lst participant class
	 */
	public List<ParameterTable> getLstParticipantClass() {
		return lstParticipantClass;
	}

	/**
	 * Sets the lst participant class.
	 * 
	 * @param lstParticipantClass
	 *            the new lst participant class
	 */
	public void setLstParticipantClass(List<ParameterTable> lstParticipantClass) {
		this.lstParticipantClass = lstParticipantClass;
	}

	/**
	 * Gets the lst participant document type.
	 * 
	 * @return the lst participant document type
	 */
	public List<ParameterTable> getLstParticipantDocumentType() {
		return lstParticipantDocumentType;
	}

	/**
	 * Sets the lst participant document type.
	 * 
	 * @param lstParticipantDocumentType
	 *            the new lst participant document type
	 */
	public void setLstParticipantDocumentType(
			List<ParameterTable> lstParticipantDocumentType) {
		this.lstParticipantDocumentType = lstParticipantDocumentType;
	}

	/**
	 * Gets the lst participant type.
	 * 
	 * @return the lst participant type
	 */
	public List<ParameterTable> getLstParticipantType() {
		return lstParticipantType;
	}

	/**
	 * Sets the lst participant type.
	 * 
	 * @param lstParticipantType
	 *            the new lst participant type
	 */
	public void setLstParticipantType(List<ParameterTable> lstParticipantType) {
		this.lstParticipantType = lstParticipantType;
	}

	/**
	 * Gets the lst economic sector.
	 * 
	 * @return the lst economic sector
	 */
	public List<ParameterTable> getLstEconomicSector() {
		return lstEconomicSector;
	}

	/**
	 * Sets the lst economic sector.
	 * 
	 * @param lstEconomicSector
	 *            the new lst economic sector
	 */
	public void setLstEconomicSector(List<ParameterTable> lstEconomicSector) {
		this.lstEconomicSector = lstEconomicSector;
	}

	/**
	 * Gets the lst economic activity.
	 * 
	 * @return the lst economic activity
	 */
	public List<ParameterTable> getLstEconomicActivity() {
		return lstEconomicActivity;
	}

	/**
	 * Sets the lst economic activity.
	 * 
	 * @param lstEconomicActivity
	 *            the new lst economic activity
	 */
	public void setLstEconomicActivity(List<ParameterTable> lstEconomicActivity) {
		this.lstEconomicActivity = lstEconomicActivity;
	}

	/**
	 * Gets the lst international depository.
	 * 
	 * @return the lst international depository
	 */
	public List<InternationalDepository> getLstInternationalDepository() {
		return lstInternationalDepository;
	}

	/**
	 * Sets the lst international depository.
	 * 
	 * @param lstInternationalDepository
	 *            the new lst international depository
	 */
	public void setLstInternationalDepository(
			List<InternationalDepository> lstInternationalDepository) {
		this.lstInternationalDepository = lstInternationalDepository;
	}

	/**
	 * Checks if is participant pay commission.
	 * 
	 * @return true, if is participant pay commission
	 */
	public boolean isParticipantPayCommission() {
		return participantPayCommission;
	}

	/**
	 * Sets the participant pay commission.
	 * 
	 * @param participantPayCommission
	 *            the new participant pay commission
	 */
	public void setParticipantPayCommission(boolean participantPayCommission) {
		this.participantPayCommission = participantPayCommission;
	}
	
	/**
	 * Checks if is participant cavapy.
	 * 
	 * @return true, if is participant cavapy
	 */
	public boolean isIndParticipantCavapy() {
		return indParticipantCavapy;
	}

	/**
	 * Sets the participant pay commission.
	 * 
	 * @param participantPayCommission
	 *            the new participant pay commission
	 */
	public void setIndParticipantCavapy(boolean indParticipantCavapy) {
		this.indParticipantCavapy = indParticipantCavapy;
	}
	
	/**
	 * Checks if is Ind Managed Third cavapy.
	 *
	 * @return true, if is Ind Managed Third cavapy
	 */
    public boolean isIndManagedThirdCavapy() {
		return indManagedThirdCavapy;
	}
    
    /**
	 * Sets the Managed Third cavapy.
	 *
	 * @param indManagedThirdCavapy the new Managed Third cavapy
	 */
	public void setIndManagedThirdCavapy(boolean indManagedThirdCavapy) {
		this.indManagedThirdCavapy = indManagedThirdCavapy;
	}

	/**
	 * Gets the lst participant state.
	 * 
	 * @return the lst participant state
	 */
	public List<ParameterTable> getLstParticipantState() {
		return lstParticipantState;
	}

	/**
	 * Sets the lst participant state.
	 * 
	 * @param lstParticipantState
	 *            the new lst participant state
	 */
	public void setLstParticipantState(List<ParameterTable> lstParticipantState) {
		this.lstParticipantState = lstParticipantState;
	}

	/**
	 * Gets the participant data model.
	 * 
	 * @return the participant data model
	 */
	public ParticipantDataModel getParticipantDataModel() {
		return participantDataModel;
	}

	/**
	 * Sets the participant data model.
	 * 
	 * @param participantDataModel
	 *            the new participant data model
	 */
	public void setParticipantDataModel(
			ParticipantDataModel participantDataModel) {
		this.participantDataModel = participantDataModel;
	}

	/**
	 * Gets the participant to.
	 * 
	 * @return the participant to
	 */
	public ParticipantTO getParticipantTO() {
		return participantTO;
	}

	/**
	 * Sets the participant to.
	 * 
	 * @param participantTO
	 *            the new participant to
	 */
	public void setParticipantTO(ParticipantTO participantTO) {
		this.participantTO = participantTO;
	}

	/**
	 * Checks if is ind participant national.
	 * 
	 * @return true, if is ind participant national
	 */
	public boolean isIndParticipantNational() {
		return Validations.validateIsNotNull(participant.getResidenceCountry())
				&& countryResidence.equals(
						participant.getResidenceCountry());
	}

	/**
	 * Checks if is ind participant foreign.
	 * 
	 * @return true, if is ind participant foreign
	 */
	public boolean isIndParticipantForeign() {
		return Validations.validateIsNotNull(participant.getResidenceCountry())
				&& participant.getResidenceCountry() > 0
				&& !(countryResidence.equals(participant.getResidenceCountry()));
	}

	/**
	 * Gets the lst participant request state.
	 * 
	 * @return the lst participant request state
	 */
	public List<ParameterTable> getLstParticipantRequestState() {
		return lstParticipantRequestState;
	}

	/**
	 * Sets the lst participant request state.
	 * 
	 * @param lstParticipantRequestState
	 *            the new lst participant request state
	 */
	public void setLstParticipantRequestState(
			List<ParameterTable> lstParticipantRequestState) {
		this.lstParticipantRequestState = lstParticipantRequestState;
	}

	/**
	 * Gets the lst registered participants.
	 * 
	 * @return the lst registered participants
	 */
	public List<Participant> getLstRegisteredParticipants() {
		return lstRegisteredParticipants;
	}

	/**
	 * Sets the lst registered participants.
	 * 
	 * @param lstRegisteredParticipants
	 *            the new lst registered participants
	 */
	public void setLstRegisteredParticipants(
			List<Participant> lstRegisteredParticipants) {
		this.lstRegisteredParticipants = lstRegisteredParticipants;
	}

	/**
	 * Checks if is ind participant selected modif.
	 * 
	 * @return true, if is ind participant selected modif
	 */
	public boolean isIndParticipantSelectedModif() {
		return indParticipantSelectedModif;
	}

	/**
	 * Sets the ind participant selected modif.
	 * 
	 * @param indParticipantSelectedModif
	 *            the new ind participant selected modif
	 */
	public void setIndParticipantSelectedModif(
			boolean indParticipantSelectedModif) {
		this.indParticipantSelectedModif = indParticipantSelectedModif;
	}

	/**
	 * Gets the id participant pk.
	 * 
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 * 
	 * @param idParticipantPk
	 *            the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Clean representative.
	 */
	public void cleanRepresentative() {
		hideDialogsListener(null);
		legalRepresentativeHistory = new LegalRepresentativeHistory();
	}

	/**
	 * Gets the state participant.
	 * 
	 * @return the state participant
	 */
	public Integer getStateParticipant() {
		return stateParticipant;
	}

	/**
	 * Sets the state participant.
	 * 
	 * @param stateParticipant
	 *            the new state participant
	 */
	public void setStateParticipant(Integer stateParticipant) {
		this.stateParticipant = stateParticipant;
	}

	/**
	 * Gets the legal representative history.
	 * 
	 * @return the legal representative history
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistory() {
		return legalRepresentativeHistory;
	}

	/**
	 * Sets the legal representative history.
	 * 
	 * @param legalRepresentativeHistory
	 *            the new legal representative history
	 */
	public void setLegalRepresentativeHistory(
			LegalRepresentativeHistory legalRepresentativeHistory) {
		this.legalRepresentativeHistory = legalRepresentativeHistory;
	}

	/**
	 * Gets the lst legal representative history.
	 * 
	 * @return the lst legal representative history
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistory() {
		return lstLegalRepresentativeHistory;
	}

	/**
	 * Sets the lst legal representative history.
	 * 
	 * @param lstLegalRepresentativeHistory
	 *            the new lst legal representative history
	 */
	public void setLstLegalRepresentativeHistory(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory) {
		this.lstLegalRepresentativeHistory = lstLegalRepresentativeHistory;
	}

	/**
	 * Gets the lst participant files.
	 * 
	 * @return the lst participant files
	 */
	public List<ParticipantFile> getLstParticipantFiles() {
		return lstParticipantFiles;
	}

	/**
	 * Sets the lst participant files.
	 * 
	 * @param lstParticipantFiles
	 *            the new lst participant files
	 */
	public void setLstParticipantFiles(List<ParticipantFile> lstParticipantFiles) {
		this.lstParticipantFiles = lstParticipantFiles;
	}

	/**
	 * Gets the lst legal representative.
	 * 
	 * @return the lst legal representative
	 */
	public List<LegalRepresentative> getLstLegalRepresentative() {
		return lstLegalRepresentative;
	}

	/**
	 * Sets the lst legal representative.
	 * 
	 * @param lstLegalRepresentative
	 *            the new lst legal representative
	 */
	public void setLstLegalRepresentative(
			List<LegalRepresentative> lstLegalRepresentative) {
		this.lstLegalRepresentative = lstLegalRepresentative;
	}

	/**
	 * Gets the lst represented entity.
	 * 
	 * @return the lst represented entity
	 */
	public List<RepresentedEntity> getLstRepresentedEntity() {
		return lstRepresentedEntity;
	}

	/**
	 * Sets the lst represented entity.
	 * 
	 * @param lstRepresentedEntity
	 *            the new lst represented entity
	 */
	public void setLstRepresentedEntity(
			List<RepresentedEntity> lstRepresentedEntity) {
		this.lstRepresentedEntity = lstRepresentedEntity;
	}

	/**
	 * Gets the lst participant roles.
	 * 
	 * @return the lst participant roles
	 */
	public List<ParameterTable> getLstParticipantRoles() {
		return lstParticipantRoles;
	}

	/**
	 * Sets the lst participant roles.
	 * 
	 * @param lstParticipantRoles
	 *            the new lst participant roles
	 */
	public void setLstParticipantRoles(List<ParameterTable> lstParticipantRoles) {
		this.lstParticipantRoles = lstParticipantRoles;
	}

	/**
	 * Gets the lst participant class for search.
	 * 
	 * @return the lst participant class for search
	 */
	public List<ParameterTable> getLstParticipantClassForSearch() {
		return lstParticipantClassForSearch;
	}

	/**
	 * Sets the lst participant class for search.
	 * 
	 * @param lstParticipantClassForSearch
	 *            the new lst participant class for search
	 */
	public void setLstParticipantClassForSearch(
			List<ParameterTable> lstParticipantClassForSearch) {
		this.lstParticipantClassForSearch = lstParticipantClassForSearch;
	}

	/**
	 * Gets the lst participant document type for search.
	 * 
	 * @return the lst participant document type for search
	 */
	public List<ParameterTable> getLstParticipantDocumentTypeForSearch() {
		return lstParticipantDocumentTypeForSearch;
	}

	/**
	 * Sets the lst participant document type for search.
	 * 
	 * @param lstParticipantDocumentTypeForSearch
	 *            the new lst participant document type for search
	 */
	public void setLstParticipantDocumentTypeForSearch(
			List<ParameterTable> lstParticipantDocumentTypeForSearch) {
		this.lstParticipantDocumentTypeForSearch = lstParticipantDocumentTypeForSearch;
	}

	/**
	 * Gets the lst participant type for search.
	 * 
	 * @return the lst participant type for search
	 */
	public List<ParameterTable> getLstParticipantTypeForSearch() {
		return lstParticipantTypeForSearch;
	}

	/**
	 * Sets the lst participant type for search.
	 * 
	 * @param lstParticipantTypeForSearch
	 *            the new lst participant type for search
	 */
	public void setLstParticipantTypeForSearch(
			List<ParameterTable> lstParticipantTypeForSearch) {
		this.lstParticipantTypeForSearch = lstParticipantTypeForSearch;
	}

	/**
	 * Gets the lst participant state for search.
	 * 
	 * @return the lst participant state for search
	 */
	public List<ParameterTable> getLstParticipantStateForSearch() {
		return lstParticipantStateForSearch;
	}

	/**
	 * Sets the lst participant state for search.
	 * 
	 * @param lstParticipantStateForSearch
	 *            the new lst participant state for search
	 */
	public void setLstParticipantStateForSearch(
			List<ParameterTable> lstParticipantStateForSearch) {
		this.lstParticipantStateForSearch = lstParticipantStateForSearch;
	}

	/**
	 * Gets the id represented entity pk.
	 * 
	 * @return the id represented entity pk
	 */
	public Long getIdRepresentedEntityPk() {
		return idRepresentedEntityPk;
	}

	/**
	 * Sets the id represented entity pk.
	 * 
	 * @param idRepresentedEntityPk
	 *            the new id represented entity pk
	 */
	public void setIdRepresentedEntityPk(Long idRepresentedEntityPk) {
		this.idRepresentedEntityPk = idRepresentedEntityPk;
	}

	/**
	 * Gets the represented entity.
	 * 
	 * @return the represented entity
	 */
	public RepresentedEntity getRepresentedEntity() {
		return representedEntity;
	}

	/**
	 * Sets the represented entity.
	 * 
	 * @param representedEntity
	 *            the new represented entity
	 */
	public void setRepresentedEntity(RepresentedEntity representedEntity) {
		this.representedEntity = representedEntity;
	}

	/**
	 * Gets the participant file type.
	 * 
	 * @return the participant file type
	 */
	public Integer getParticipantFileType() {
		return participantFileType;
	}

	/**
	 * Sets the participant file type.
	 * 
	 * @param participantFileType
	 *            the new participant file type
	 */
	public void setParticipantFileType(Integer participantFileType) {
		this.participantFileType = participantFileType;
	}

	/**
	 * Gets the lst participant file types.
	 * 
	 * @return the lst participant file types
	 */
	public List<ParameterTable> getLstParticipantFileTypes() {
		return lstParticipantFileTypes;
	}

	/**
	 * Sets the lst participant file types.
	 * 
	 * @param lstParticipantFileTypes
	 *            the new lst participant file types
	 */
	public void setLstParticipantFileTypes(
			List<ParameterTable> lstParticipantFileTypes) {
		this.lstParticipantFileTypes = lstParticipantFileTypes;
	}

	/**
	 * Gets the participant request data model.
	 * 
	 * @return the participant request data model
	 */
	public ParticipantRequestDataModel getParticipantRequestDataModel() {
		return participantRequestDataModel;
	}

	/**
	 * Sets the participant request data model.
	 * 
	 * @param participantRequestDataModel
	 *            the new participant request data model
	 */
	public void setParticipantRequestDataModel(
			ParticipantRequestDataModel participantRequestDataModel) {
		this.participantRequestDataModel = participantRequestDataModel;
	}

	/**
	 * Checks if is ind disable confirn reject.
	 * 
	 * @return true, if is ind disable confirn reject
	 */
	public boolean isIndDisableConfirnReject() {
		return indDisableConfirnReject;
	}

	/**
	 * Sets the ind disable confirn reject.
	 * 
	 * @param indDisableConfirnReject
	 *            the new ind disable confirn reject
	 */
	public void setIndDisableConfirnReject(boolean indDisableConfirnReject) {
		this.indDisableConfirnReject = indDisableConfirnReject;
	}

	/**
	 * Gets the participant request.
	 * 
	 * @return the participant request
	 */
	public ParticipantRequest getParticipantRequest() {
		return participantRequest;
	}

	/**
	 * Sets the participant request.
	 * 
	 * @param participantRequest
	 *            the new participant request
	 */
	public void setParticipantRequest(ParticipantRequest participantRequest) {
		this.participantRequest = participantRequest;
	}

	/**
	 * Gets the id participant request pk.
	 * 
	 * @return the id participant request pk
	 */
	public Long getIdParticipantRequestPk() {
		return idParticipantRequestPk;
	}

	/**
	 * Sets the id participant request pk.
	 * 
	 * @param idParticipantRequestPk
	 *            the new id participant request pk
	 */
	public void setIdParticipantRequestPk(Long idParticipantRequestPk) {
		this.idParticipantRequestPk = idParticipantRequestPk;
	}

	/**
	 * Gets the lst international depository before.
	 * 
	 * @return the lst international depository before
	 */
	public List<InternationalDepository> getLstInternationalDepositoryBefore() {
		return lstInternationalDepositoryBefore;
	}

	/**
	 * Sets the lst international depository before.
	 * 
	 * @param lstInternationalDepositoryBefore
	 *            the new lst international depository before
	 */
	public void setLstInternationalDepositoryBefore(
			List<InternationalDepository> lstInternationalDepositoryBefore) {
		this.lstInternationalDepositoryBefore = lstInternationalDepositoryBefore;
	}

	/**
	 * Gets the lst international depository after.
	 * 
	 * @return the lst international depository after
	 */
	public List<InternationalDepository> getLstInternationalDepositoryAfter() {
		return lstInternationalDepositoryAfter;
	}

	/**
	 * Sets the lst international depository after.
	 * 
	 * @param lstInternationalDepositoryAfter
	 *            the new lst international depository after
	 */
	public void setLstInternationalDepositoryAfter(
			List<InternationalDepository> lstInternationalDepositoryAfter) {
		this.lstInternationalDepositoryAfter = lstInternationalDepositoryAfter;
	}



	/**
	 * Checks if is ind selected other motive reject.
	 *
	 * @return true, if is ind selected other motive reject
	 */
	public boolean isIndSelectedOtherMotiveReject() {
		return indSelectedOtherMotiveReject;
	}

	/**
	 * Sets the ind selected other motive reject.
	 *
	 * @param indSelectedOtherMotiveReject the new ind selected other motive reject
	 */
	public void setIndSelectedOtherMotiveReject(boolean indSelectedOtherMotiveReject) {
		this.indSelectedOtherMotiveReject = indSelectedOtherMotiveReject;
	}

	/**
	 * Checks if is ind disable accept motive reject.
	 *
	 * @return true, if is ind disable accept motive reject
	 */
	public boolean isIndDisableAcceptMotiveReject() {
		return indDisableAcceptMotiveReject;
	}

	/**
	 * Sets the ind disable accept motive reject.
	 *
	 * @param indDisableAcceptMotiveReject the new ind disable accept motive reject
	 */
	public void setIndDisableAcceptMotiveReject(boolean indDisableAcceptMotiveReject) {
		this.indDisableAcceptMotiveReject = indDisableAcceptMotiveReject;
	}

	/**
	 * Gets the lst participant request reject motives.
	 *
	 * @return the lst participant request reject motives
	 */
	public List<ParameterTable> getLstParticipantRequestRejectMotives() {
		return lstParticipantRequestRejectMotives;
	}

	/**
	 * Sets the lst participant request reject motives.
	 *
	 * @param lstParticipantRequestRejectMotives the new lst participant request reject motives
	 */
	public void setLstParticipantRequestRejectMotives(
			List<ParameterTable> lstParticipantRequestRejectMotives) {
		this.lstParticipantRequestRejectMotives = lstParticipantRequestRejectMotives;
	}

	/**
	 * Gets the participant file name display.
	 *
	 * @return the participant file name display
	 */
	public String getParticipantFileNameDisplay() {
		return participantFileNameDisplay;
	}

	/**
	 * Sets the participant file name display.
	 *
	 * @param participantFileNameDisplay the new participant file name display
	 */
	public void setParticipantFileNameDisplay(String participantFileNameDisplay) {
		this.participantFileNameDisplay = participantFileNameDisplay;
	}
	
	/**
	 * Checks if is enable select participant file.
	 *
	 * @return true, if is enable select participant file
	 */
	public boolean isEnableSelectParticipantFile() {
		return Validations.validateIsNotNullAndPositive(participantFileType);
	}

	/**
	 * Gets the lst representative file history.
	 *
	 * @return the lst representative file history
	 */
	public List<RepresentativeFileHistory> getLstRepresentativeFileHistory() {
		return lstRepresentativeFileHistory;
	}

	/**
	 * Sets the lst representative file history.
	 *
	 * @param lstRepresentativeFileHistory the new lst representative file history
	 */
	public void setLstRepresentativeFileHistory(
			List<RepresentativeFileHistory> lstRepresentativeFileHistory) {
		this.lstRepresentativeFileHistory = lstRepresentativeFileHistory;
	}

	/**
	 * Gets the lst legal representative history before.
	 *
	 * @return the lst legal representative history before
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistoryBefore() {
		return lstLegalRepresentativeHistoryBefore;
	}

	/**
	 * Sets the lst legal representative history before.
	 *
	 * @param lstLegalRepresentativeHistoryBefore the new lst legal representative history before
	 */
	public void setLstLegalRepresentativeHistoryBefore(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryBefore) {
		this.lstLegalRepresentativeHistoryBefore = lstLegalRepresentativeHistoryBefore;
	}

	/**
	 * Gets the lst legal representative history after.
	 *
	 * @return the lst legal representative history after
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistoryAfter() {
		return lstLegalRepresentativeHistoryAfter;
	}

	/**
	 * Sets the lst legal representative history after.
	 *
	 * @param lstLegalRepresentativeHistoryAfter the new lst legal representative history after
	 */
	public void setLstLegalRepresentativeHistoryAfter(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryAfter) {
		this.lstLegalRepresentativeHistoryAfter = lstLegalRepresentativeHistoryAfter;
	}
	
	/**
	 * Checks if is ind representative nationality national.
	 *
	 * @return true, if is ind representative nationality national
	 */
	public boolean isIndRepresentativeNationalityNational() {
		return Validations.validateIsNotNull(legalRepresentativeHistory.getNationality()) && 
				countryResidence.equals(legalRepresentativeHistory.getNationality());
	}
	
	/**
	 * Checks if is ind representative nationality foreign.
	 *
	 * @return true, if is ind representative nationality foreign
	 */
	public boolean isIndRepresentativeNationalityForeign() {
		return Validations.validateIsNotNullAndPositive(legalRepresentativeHistory.getNationality()) &&				
				!(countryResidence.equals(legalRepresentativeHistory.getNationality()));
	}

	/**
	 * Gets the min date participant contract.
	 *
	 * @return the min date participant contract
	 */
	public Date getMinDateParticipantContract() {
		return minDateParticipantContract;
	}

	/**
	 * Sets the min date participant contract.
	 *
	 * @param minDateParticipantContract the new min date participant contract
	 */
	public void setMinDateParticipantContract(Date minDateParticipantContract) {
		this.minDateParticipantContract = minDateParticipantContract;
	}
	
	/**
	 * Checks if is participant indirect.
	 *
	 * @return true, if is participant indirect
	 */
	public boolean isParticipantIndirect(){
		return ParticipantType.INDIRECT.getCode().equals(participant.getAccountType());
	}

	/**
	 * Gets the lst negotiation modalities before.
	 *
	 * @return the lst negotiation modalities before
	 */
	public List<NegotiationModality> getLstNegotiationModalitiesBefore() {
		return lstNegotiationModalitiesBefore;
	}

	/**
	 * Sets the lst negotiation modalities before.
	 *
	 * @param lstNegotiationModalitiesBefore the new lst negotiation modalities before
	 */
	public void setLstNegotiationModalitiesBefore(
			List<NegotiationModality> lstNegotiationModalitiesBefore) {
		this.lstNegotiationModalitiesBefore = lstNegotiationModalitiesBefore;
	}

	/**
	 * Gets the lst negotiation modalities after.
	 *
	 * @return the lst negotiation modalities after
	 */
	public List<NegotiationModality> getLstNegotiationModalitiesAfter() {
		return lstNegotiationModalitiesAfter;
	}

	/**
	 * Sets the lst negotiation modalities after.
	 *
	 * @param lstNegotiationModalitiesAfter the new lst negotiation modalities after
	 */
	public void setLstNegotiationModalitiesAfter(
			List<NegotiationModality> lstNegotiationModalitiesAfter) {
		this.lstNegotiationModalitiesAfter = lstNegotiationModalitiesAfter;
	}	
	
	/**
	 * Checks if is allow search all.
	 *
	 * @return true, if is allow search all
	 */
	public boolean isAllowSearchAll() {
		return allowSearchAll;
	}

	/**
	 * Sets the allow search all.
	 *
	 * @param allowSearchAll the new allow search all
	 */
	public void setAllowSearchAll(boolean allowSearchAll) {
		this.allowSearchAll = allowSearchAll;
	}

	/**
	 * Gets the lst departments.
	 *
	 * @return the lst departments
	 */
	public List<GeographicLocation> getLstDepartments() {
		return lstDepartments;
	}

	/**
	 * Sets the lst departments.
	 *
	 * @param lstDepartments the new lst departments
	 */
	public void setLstDepartments(List<GeographicLocation> lstDepartments) {
		this.lstDepartments = lstDepartments;
	}

	/**
	 * Checks if is participant related settlement.
	 *
	 * @return true, if is participant related settlement
	 */
	public boolean isParticipantRelatedSettlement() {
		return participantRelatedSettlement;
	}

	/**
	 * Sets the participant related settlement.
	 *
	 * @param participantRelatedSettlement the new participant related settlement
	 */
	public void setParticipantRelatedSettlement(boolean participantRelatedSettlement) {
		this.participantRelatedSettlement = participantRelatedSettlement;
	}		
	
	/**
	 * Checks if is disabled search document number.
	 *
	 * @return true, if is disabled search document number
	 */
	public boolean isDisabledSearchDocumentNumber() {
		return Validations.validateIsNull(participantTO) || Validations.validateIsNull(participantTO.getDocumentType()); 
	}
	
	/**
	 * Checks if is disabled invalid inputs.
	 *
	 * @return true, if is disabled invalid inputs
	 */
	public boolean isDisabledInvalidInputs() {
		return !indParticipantSelectedModif || !userInfo.getUserAccountSession().isDepositaryInstitution(); 
	}

	/**
	 * Gets the lst participant file hy before.
	 *
	 * @return the lst participant file hy before
	 */
	public List<ParticipantFileHistory> getLstParticipantFileHyBefore() {
		return lstParticipantFileHyBefore;
	}

	/**
	 * Sets the lst participant file hy before.
	 *
	 * @param lstParticipantFileHyBefore the new lst participant file hy before
	 */
	public void setLstParticipantFileHyBefore(
			List<ParticipantFileHistory> lstParticipantFileHyBefore) {
		this.lstParticipantFileHyBefore = lstParticipantFileHyBefore;
	}

	/**
	 * Gets the lst participant file hy after.
	 *
	 * @return the lst participant file hy after
	 */
	public List<ParticipantFileHistory> getLstParticipantFileHyAfter() {
		return lstParticipantFileHyAfter;
	}

	/**
	 * Sets the lst participant file hy after.
	 *
	 * @param lstParticipantFileHyAfter the new lst participant file hy after
	 */
	public void setLstParticipantFileHyAfter(
			List<ParticipantFileHistory> lstParticipantFileHyAfter) {
		this.lstParticipantFileHyAfter = lstParticipantFileHyAfter;
	}
	
	/**
	 * Gets the lst Institution Bank Account hy before.
	 *
	 * @return the lst Institution Bank Account hy before
	 */
	public List<InstitutionBankAccountHistory> getLstInstitutionBankAccountHyBefore() {
		return lstInstitutionBankAccountHyBefore;
	}
	
	/**
	 * Sets the lst Institution Bank Account hy before.
	 *
	 * @param lstInstitutionBankAccountHyBefore the new lst Institution Bank Account hy before
	 */
	public void setLstInstitutionBankAccountHyBefore(
			List<InstitutionBankAccountHistory> lstInstitutionBankAccountHyBefore) {
		this.lstInstitutionBankAccountHyBefore = lstInstitutionBankAccountHyBefore;
	}
	
	/**
	 * Gets the lst Institution Bank Account hy after.
	 *
	 * @return the lst Institution Bank Account hy after
	 */
	public List<InstitutionBankAccountHistory> getLstInstitutionBankAccountHyAfter() {
		return lstInstitutionBankAccountHyAfter;
	}
	
	/**
	 * Sets the lst Institution Bank Account hy after.
	 *
	 * @param lstInstitutionBankAccountHyAfter the new Institution Bank Account file hy after
	 */
	public void setLstInstitutionBankAccountHyAfter(List<InstitutionBankAccountHistory> lstInstitutionBankAccountHyAfter) {
		this.lstInstitutionBankAccountHyAfter = lstInstitutionBankAccountHyAfter;
	}

	/**
	 * Gets the participant request hy before.
	 *
	 * @return the participant request hy before
	 */
	public ParticipantRequestHistory getParticipantRequestHyBefore() {
		return participantRequestHyBefore;
	}

	/**
	 * Sets the participant request hy before.
	 *
	 * @param participantRequestHyBefore the new participant request hy before
	 */
	public void setParticipantRequestHyBefore(
			ParticipantRequestHistory participantRequestHyBefore) {
		this.participantRequestHyBefore = participantRequestHyBefore;
	}

	/**
	 * Gets the participant request hy after.
	 *
	 * @return the participant request hy after
	 */
	public ParticipantRequestHistory getParticipantRequestHyAfter() {
		return participantRequestHyAfter;
	}

	/**
	 * Sets the participant request hy after.
	 *
	 * @param participantRequestHyAfter the new participant request hy after
	 */
	public void setParticipantRequestHyAfter(
			ParticipantRequestHistory participantRequestHyAfter) {
		this.participantRequestHyAfter = participantRequestHyAfter;
	}
	
	/**
	 * Checks if is registered negotiation modalities before.
	 *
	 * @return true, if is registered negotiation modalities before
	 */
	public boolean isRegisteredNegotiationModalitiesBefore() {
		return registeredNegotiationModalitiesBefore;
	}

	/**
	 * Sets the registered negotiation modalities before.
	 *
	 * @param registeredNegotiationModalitiesBefore the new registered negotiation modalities before
	 */
	public void setRegisteredNegotiationModalitiesBefore(
			boolean registeredNegotiationModalitiesBefore) {
		this.registeredNegotiationModalitiesBefore = registeredNegotiationModalitiesBefore;
	}

	/**
	 * Checks if is ind participant info changed.
	 *
	 * @return true, if is ind participant info changed
	 */
	public boolean isIndParticipantInfoChanged() {
		return indParticipantInfoChanged;
	}

	/**
	 * Sets the ind participant info changed.
	 *
	 * @param indParticipantInfoChanged the new ind participant info changed
	 */
	public void setIndParticipantInfoChanged(boolean indParticipantInfoChanged) {
		this.indParticipantInfoChanged = indParticipantInfoChanged;
	}

	/**
	 * Checks if is ind participant files changed.
	 *
	 * @return true, if is ind participant files changed
	 */
	public boolean isIndParticipantFilesChanged() {
		return indParticipantFilesChanged;
	}

	/**
	 * Sets the ind participant files changed.
	 *
	 * @param indParticipantFilesChanged the new ind participant files changed
	 */
	public void setIndParticipantFilesChanged(boolean indParticipantFilesChanged) {
		this.indParticipantFilesChanged = indParticipantFilesChanged;
	}
	
	/**
	 * Checks if is ind Institution Bank Accounts changed.
	 *
	 * @return true, if is ind Institution Bank Accounts changed
	 */
	public boolean isIndInstitutionBankAccountsChanged() {
		return indInstitutionBankAccountsChanged;
	}
	
	/**
	 * Sets the ind Institution Bank Accounts changed.
	 *
	 * @param indInstitutionBankAccountsChanged the new ind Institution Bank Accounts changed
	 */
	public void setIndInstitutionBankAccountsChanged(boolean indInstitutionBankAccountsChanged) {
		this.indInstitutionBankAccountsChanged = indInstitutionBankAccountsChanged;
	}

	/**
	 * Checks if is ind participant ind depos changed.
	 *
	 * @return true, if is ind participant ind depos changed
	 */
	public boolean isIndParticipantIndDeposChanged() {
		return indParticipantIndDeposChanged;
	}

	/**
	 * Sets the ind participant ind depos changed.
	 *
	 * @param indParticipantIndDeposChanged the new ind participant ind depos changed
	 */
	public void setIndParticipantIndDeposChanged(
			boolean indParticipantIndDeposChanged) {
		this.indParticipantIndDeposChanged = indParticipantIndDeposChanged;
	}

	/**
	 * Checks if is ind participant mechanisms changed.
	 *
	 * @return true, if is ind participant mechanisms changed
	 */
	public boolean isIndParticipantMechanismsChanged() {
		return indParticipantMechanismsChanged;
	}

	/**
	 * Sets the ind participant mechanisms changed.
	 *
	 * @param indParticipantMechanismsChanged the new ind participant mechanisms changed
	 */
	public void setIndParticipantMechanismsChanged(
			boolean indParticipantMechanismsChanged) {
		this.indParticipantMechanismsChanged = indParticipantMechanismsChanged;
	}

	/**
	 * Checks if is ind legal representative changed.
	 *
	 * @return true, if is ind legal representative changed
	 */
	public boolean isIndLegalRepresentativeChanged() {
		return indLegalRepresentativeChanged;
	}

	/**
	 * Sets the ind legal representative changed.
	 *
	 * @param indLegalRepresentativeChanged the new ind legal representative changed
	 */
	public void setIndLegalRepresentativeChanged(
			boolean indLegalRepresentativeChanged) {
		this.indLegalRepresentativeChanged = indLegalRepresentativeChanged;
	}

	/**
	 * Gets the files modified.
	 *
	 * @return the files modified
	 */
	public String getFilesModified() {
		return filesModified;
	}

	/**
	 * Sets the files modified.
	 *
	 * @param filesModified the new files modified
	 */
	public void setFilesModified(String filesModified) {
		this.filesModified = filesModified;
	}

	/**
	 * Gets the document to.
	 *
	 * @return the document to
	 */
	public DocumentTO getDocumentTO() {
		return documentTO;
	}

	/**
	 * Sets the document to.
	 *
	 * @param documentTO the new document to
	 */
	public void setDocumentTO(DocumentTO documentTO) {
		this.documentTO = documentTO;
	}

	/**
	 * Gets the list document source.
	 *
	 * @return the list document source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list document source.
	 *
	 * @param listDocumentSource the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}
	
	/**
	 * 
	 */
	public boolean isParticiantTypeProcessCompLiq() {
		return particiantTypeProcessCompLiq;
	}

	/**
	 * 
	 */
	public void setParticiantTypeProcessCompLiq(boolean particiantTypeProcessCompLiq) {
		this.particiantTypeProcessCompLiq = particiantTypeProcessCompLiq;
	}
	
	/**
	 * 
	 */
	public Integer getOptionSelectedOneRadio() {
		return optionSelectedOneRadio;
	}

	public void setOptionSelectedOneRadio(Integer optionSelectedOneRadio) {
		this.optionSelectedOneRadio = optionSelectedOneRadio;
	}	

	public String getfUploadFileSize() {
		return fUploadFileSize;
	}

	public void setfUploadFileSize(String fUploadFileSize) {
		this.fUploadFileSize = fUploadFileSize;
	}

	public String getfUploadFileSizeDisplay() {
		return fUploadFileSizeDisplay;
	}

	public void setfUploadFileSizeDisplay(String fUploadFileSizeDisplay) {
		this.fUploadFileSizeDisplay = fUploadFileSizeDisplay;
	}

	public List<InvestorTypeModelTO> getListInvestorType() {
		return listInvestorType;
	}

	public void setListInvestorType(List<InvestorTypeModelTO> listInvestorType) {
		this.listInvestorType = listInvestorType;
	}
	
	public InstitutionBankAccount getInstitutionBankAccountSession() {
		return institutionBankAccountSession;
	}

	public void setInstitutionBankAccountSession(InstitutionBankAccount institutionBankAccountSession) {
		this.institutionBankAccountSession = institutionBankAccountSession;
	}
	
	 public InstitutionBankAccount getInstitutionBankAccountSessionSelected() {
		return institutionBankAccountSessionSelected;
	}

	public void setInstitutionBankAccountSessionSelected(InstitutionBankAccount institutionBankAccountSessionSelected) {
		this.institutionBankAccountSessionSelected = institutionBankAccountSessionSelected;
	}
	
	public List<InstitutionBankAccount> getLstInstitutionBankAccountSession() {
		return lstInstitutionBankAccountSession;
	}

	public void setLstInstitutionBankAccountSession(List<InstitutionBankAccount> lstInstitutionBankAccountSession) {
		this.lstInstitutionBankAccountSession = lstInstitutionBankAccountSession;
	}
	
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	public List<Bank> getLstBankProvider() {
		return lstBankProvider;
	}

	public void setLstBankProvider(List<Bank> lstBankProvider) {
		this.lstBankProvider = lstBankProvider;
	}

	public List<ParameterTable> getLstBankAccountType() {
		return lstBankAccountType;
	}

	public void setLstBankAccountType(List<ParameterTable> lstBankAccountType) {
		this.lstBankAccountType = lstBankAccountType;
	}
		
}
