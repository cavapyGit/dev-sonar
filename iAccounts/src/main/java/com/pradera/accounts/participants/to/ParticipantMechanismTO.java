package com.pradera.accounts.participants.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantMechanismTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public class ParticipantMechanismTO implements Serializable{

	/**
	 * Instantiates a new participant mechanism to.
	 */
	public ParticipantMechanismTO() {
		// TODO Auto-generated constructor stub
	}
	
	/** The state participant mechanism. */
	public Integer stateParticipantMechanism;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id participant request pk. */
	private Long idParticipantRequestPk;

	/**
	 * Gets the state participant mechanism.
	 *
	 * @return the state participant mechanism
	 */
	public Integer getStateParticipantMechanism() {
		return stateParticipantMechanism;
	}

	/**
	 * Sets the state participant mechanism.
	 *
	 * @param stateParticipantMechanism the new state participant mechanism
	 */
	public void setStateParticipantMechanism(Integer stateParticipantMechanism) {
		this.stateParticipantMechanism = stateParticipantMechanism;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id participant request pk.
	 *
	 * @return the id participant request pk
	 */
	public Long getIdParticipantRequestPk() {
		return idParticipantRequestPk;
	}

	/**
	 * Sets the id participant request pk.
	 *
	 * @param idParticipantRequestPk the new id participant request pk
	 */
	public void setIdParticipantRequestPk(Long idParticipantRequestPk) {
		this.idParticipantRequestPk = idParticipantRequestPk;
	}
		
}
