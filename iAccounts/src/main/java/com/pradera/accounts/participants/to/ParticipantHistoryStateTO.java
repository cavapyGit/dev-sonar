package com.pradera.accounts.participants.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantHistoryStateTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/03/2013
 */
public class ParticipantHistoryStateTO implements Serializable{
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id participant request pk. */
	private Long idParticipantRequestPk;
	
	/**
	 * Instantiates a new participant history state to.
	 */
	public ParticipantHistoryStateTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id participant request pk.
	 *
	 * @return the id participant request pk
	 */
	public Long getIdParticipantRequestPk() {
		return idParticipantRequestPk;
	}

	/**
	 * Sets the id participant request pk.
	 *
	 * @param idParticipantRequestPk the new id participant request pk
	 */
	public void setIdParticipantRequestPk(Long idParticipantRequestPk) {
		this.idParticipantRequestPk = idParticipantRequestPk;
	}
	
	
}
