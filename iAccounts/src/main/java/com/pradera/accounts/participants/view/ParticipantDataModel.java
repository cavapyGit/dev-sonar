package com.pradera.accounts.participants.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.Participant;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ParticipantDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class ParticipantDataModel extends ListDataModel<Participant> implements SelectableDataModel<Participant>, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new participant data model.
	 */
	public ParticipantDataModel(){
		
	}
	
	/**
	 * Instantiates a new participant data model.
	 *
	 * @param data the data
	 */
	public ParticipantDataModel(List<Participant> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public Participant getRowData(String rowKey) {
		List<Participant> lstParticipant = (List<Participant>)getWrappedData();
        for(Participant participant : lstParticipant ) {  
        	
            if(String.valueOf(participant.getIdParticipantPk()).equals(rowKey))
                return participant;  
        }
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(Participant participant) {
		return participant.getIdParticipantPk();
	}		

}
