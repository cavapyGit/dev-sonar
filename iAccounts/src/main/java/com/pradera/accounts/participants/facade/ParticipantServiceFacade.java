package com.pradera.accounts.participants.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holderaccount.facade.HolderAccountServiceFacade;
import com.pradera.accounts.participants.service.ParticipantMgmtServiceBean;
import com.pradera.accounts.participants.to.ParticipantHistoryStateTO;
import com.pradera.accounts.participants.to.ParticipantIntDepositoryTO;
import com.pradera.accounts.participants.to.ParticipantMechanismTO;
import com.pradera.accounts.participants.to.ParticipantRequestTO;
import com.pradera.accounts.participants.to.ParticipantTO;
import com.pradera.accounts.participants.view.CuiRangeRevervedConstanst;
import com.pradera.accounts.webservices.ServiceAPIClients;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.security.model.type.InstitutionStateType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.InternationalDepositoryTO;
import com.pradera.core.component.accounts.to.NegotiationMechanismTO;
import com.pradera.core.component.accounts.to.RepresentedEntityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.accounts.participants.to.SearchBankAccountsParticipantTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantBankAccount;
import com.pradera.model.accounts.ParticipantFile;
import com.pradera.model.accounts.ParticipantFileHistory;
import com.pradera.model.accounts.ParticipantHistoryState;
import com.pradera.model.accounts.ParticipantIntDepoHistory;
import com.pradera.model.accounts.ParticipantIntDepositary;
import com.pradera.model.accounts.ParticipantMechanism;
import com.pradera.model.accounts.ParticipantMechanismHistory;
import com.pradera.model.accounts.ParticipantRequest;
import com.pradera.model.accounts.ParticipantRequestHistory;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.JuridicClassType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.ParticipantFileStateType;
import com.pradera.model.accounts.type.ParticipantIntDepositaryStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantRequestStateType;
import com.pradera.model.accounts.type.ParticipantRequestType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PepPersonClassificationType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.accounts.type.RequestParticipantFileType;
import com.pradera.model.accounts.type.StateHistoryStateHolderType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionBankAccountHistory;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.DetailCashAccountStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/02/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ParticipantServiceFacade {
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;

	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** The participant mgmt service bean. */
	@EJB
	ParticipantMgmtServiceBean participantMgmtServiceBean;
	
	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;
	
	/** 	Cash Account Service. */
	@EJB
	CashAccountManagementService cashAccountService;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	/** The accounts facade. */
	@EJB
	private HolderAccountComponentServiceBean accountServiceBean;
	
	/** The holder account service facade. */
	@EJB
	private HolderAccountServiceFacade holderAccountServiceFacade;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean; 
	
	@EJB
	ServiceAPIClients serviceAPIClients;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/**
	 * Registry participant with representant service facade.
	 *
	 * @param legalRepresentativeHistory the legal representative history
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryLegalRepresentativeHistory(LegalRepresentativeHistory legalRepresentativeHistory) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		crudDaoServiceBean.create(legalRepresentativeHistory);
		return true;
	}
	
	/**
	 * Registry legal representative service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryLegalRepresentative(LegalRepresentative legalRepresentative) throws ServiceException{
		crudDaoServiceBean.create(legalRepresentative);
		return true;
	}
	
	/**
	 * Update legal representative service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateLegalRepresentative(LegalRepresentative legalRepresentative) throws ServiceException{
		crudDaoServiceBean.update(legalRepresentative);
		return true;
	}
	
	/**
	 * Registry participant service facade.
	 *
	 * @param participant the participant
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryParticipant(Participant participant) throws ServiceException{		
		crudDaoServiceBean.create(participant);
		return true;
	}
	
	/**
	 * Update participant service facade.
	 *
	 * @param participant the participant
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateParticipant(Participant participant) throws ServiceException{		
		crudDaoServiceBean.update(participant);
		return true;
	}
	
	/**
	 * Find participant by filters service bean.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantByFilters(Participant filter) throws ServiceException{
		 Participant participant = participantMgmtServiceBean.findParticipantByFiltersServiceBean(filter);
		 if(Validations.validateIsNotNull(participant)){
			 if(Validations.validateIsNotNull(participant.getHolder())){
				 participant.getHolder().getFirstLastName();
			 }
			 
		 }
		 
		 if(Validations.validateIsNotNull(participant)){
			 if(Validations.validateIsNotNull(participant.getIssuer())){
				 participant.getIssuer().getMnemonic();
			 }
		 }
		 
		 return participant;
	}
	
	/**
	 * Get List Bank.
	 *
	 * @param idTypeBank type bank
	 * @param idState the id state
	 * @return List bank
	 * @throws ServiceException the Service Exception
	 */
	public List<Bank> getListBank(Integer idTypeBank,Integer idState)throws ServiceException{
		return participantMgmtServiceBean.getListBank(idTypeBank,idState);
	}
	
	/**
	 * Find participant by filters.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantByFilter(Participant filter) throws ServiceException{
		 Participant participant = participantMgmtServiceBean.findParticipantByFilters(filter);
		 if(Validations.validateIsNotNull(participant)){
			 if(Validations.validateIsNotNull(participant.getHolder())){
				 participant.getHolder().getFirstLastName();
			 }
		 }
		 return participant;
	}
	
	/**
	 * Validate mnemonic participant service facade.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant validateMnemonicParticipant(Participant filter) throws ServiceException{		 
		 return participantMgmtServiceBean.validateMnemonicParticipantServiceBean(filter);
	}	 
	
	/**
	 * Validate document participant.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant validateDocumentParticipant(Participant filter) throws ServiceException{		 
		 return participantMgmtServiceBean.validateDocumentParticipant(filter);
	}	 
	
	/**
	 * Gets the list participant search.
	 *
	 * @param filter the filter
	 * @return the list participant search
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PARTICIPANT_QUERY)
	public List<Participant> getListParticipantSearch(ParticipantTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());

		return participantMgmtServiceBean.getListParticipantByFiltersServiceBean(filter);
	}
		
	
	/**
	 * Gets the list participant by filters service facade.
	 *
	 * @param filter the filter
	 * @return the list participant by filters service facade
	 * @throws ServiceException the service exception
	 */	
	public List<Participant> getListParticipantByFiltersServiceFacade(ParticipantTO filter) throws ServiceException{
		return participantMgmtServiceBean.getListParticipantByFiltersServiceBean(filter);
	}
	
	/**
	 * Gets the list participant mechanism by participant service facade.
	 *
	 * @param filter the filter
	 * @return the list participant mechanism by participant service facade
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantMechanism> getListParticipantMechanismByParticipantServiceFacade(ParticipantMechanismTO filter) throws ServiceException {
		return participantMgmtServiceBean.getListParticipantMechanismByParticipantServiceBean(filter);
	}
	
	/**
	 * Gets the list participant int depo by participant service facade.
	 *
	 * @param filter the filter
	 * @return the list participant int depo by participant service facade
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantIntDepositary> getListParticipantIntDepoByParticipantServiceFacade(ParticipantIntDepositoryTO filter) throws ServiceException {
		return participantMgmtServiceBean.getListParticipantIntDepoByParticipantServiceBean(filter);
	}
	
	/**
	 * Gets the list negotiation mechanism service facade.
	 *
	 * @param filter the filter
	 * @return the list negotiation mechanism service facade
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getListNegotiationMechanismServiceFacade(NegotiationMechanismTO filter) throws ServiceException {
    	return participantMgmtServiceBean.getListNegotiationMechanismServiceBean(filter);
	}
	
	/**
	 * Gets the list international depository service facade.
	 *
	 * @param filter the filter
	 * @return the list international depository service facade
	 * @throws ServiceException the service exception
	 */
	public List<InternationalDepository> getListInternationalDepositoryServiceFacade(InternationalDepositoryTO filter) throws ServiceException{
		return participantMgmtServiceBean.getListInternationalDepositoryServiceBean(filter);
	}

	/**
	 * Gets the list participant files service facade.
	 *
	 * @param participantFilter the participant filter
	 * @return the list participant files service facade
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantFile> getListParticipantFilesServiceFacade(Participant participantFilter) throws ServiceException{
		return participantMgmtServiceBean.getListParticipantFilesServiceBean(participantFilter);
	}
	
	/**
	 * Get Bank Accounts For Id.
	 *
	 * @param idInstitutionBankAccount id Institution Bank Account
	 * @return Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public List<InstitutionBankAccount> getBankAccountsForId(Participant participantFilter)throws ServiceException{
		return participantMgmtServiceBean.getBankAccountsForId(participantFilter);
	}

	/**
	 * Gets the list represented entity by participant service facade.
	 *
	 * @param participantFilter the participant filter
	 * @return the list represented entity by participant service facade
	 * @throws ServiceException the service exception
	 */
	public List<RepresentedEntity> getListRepresentedEntityByParticipant(
			Participant participantFilter) throws ServiceException{
		return participantMgmtServiceBean.getListRepresentedEntityByParticipant(participantFilter);
	}
	
	/**
	 * Find rnc information by id service facade.
	 *
	 * @param rncFilter the rnc filter
	 * @return the rnc information
	 * @throws ServiceException the service exception
	 */
	public InstitutionInformation findRncInformationByIdServiceFacade(InstitutionInformation rncFilter) throws ServiceException {
		return participantMgmtServiceBean.findRncInformationByIdServiceBean(rncFilter);
	}
	
	/**
	 * Validate exists participant request service facade.
	 *
	 * @param participantRequestTO the participant request to
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequest validateExistsParticipantRequestServiceFacade(ParticipantRequestTO participantRequestTO)
			throws ServiceException{
		return participantMgmtServiceBean.validateExistsParticipantRequestServiceBean(participantRequestTO);
	}
	
	/**
	 * Update participant request service facade.
	 *
	 * @param participantRequest the participant request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateParticipantRequestServiceFacade(ParticipantRequest participantRequest) throws ServiceException{	
		crudDaoServiceBean.update(participantRequest);
		return true;
	}
	
	/**
	 * Gets the list participant request modification search.
	 *
	 * @param filter the filter
	 * @return the list participant request modification search
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MODIFICATION_PARTICIPANT_QUERY)
	public List<ParticipantRequest> getListParticipantRequestModificationSearch(ParticipantTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
        List<ParticipantRequest> lstParticipantRequest =  participantMgmtServiceBean.getListParticipantRequestByFilters(filter);
        
        if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantRequest) && filter.getParticipantRequestTO().isRequiredFullInformation()) {
			fillParticipantRequestParams(lstParticipantRequest);
			fillParticipantParams(lstParticipantRequest);
		}
		
		return lstParticipantRequest;
	}
	
	/**
	 * Gets the list participant request by filters service facade.
	 *
	 * @param filter the filter
	 * @return the list participant request by filters service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BLOCK_UNBLOCK_CANCEL_PARTICIPANT_QUERY)
	public List<ParticipantRequest> getListParticipantRequestBlockUnblockAnnull(ParticipantTO filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<ParticipantRequest> lstParticipantRequest = participantMgmtServiceBean.getListParticipantRequestByFilters(filter);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantRequest) && filter.getParticipantRequestTO().isRequiredFullInformation()) {
			fillParticipantRequestParams(lstParticipantRequest);			
		}
		
		return lstParticipantRequest;
	}
	
	/**
	 * Fill participant params.
	 *
	 * @param lstParticipantRequest the lst participant request
	 * @throws ServiceException the service exception
	 */
	private void fillParticipantParams(List<ParticipantRequest> lstParticipantRequest) throws ServiceException{
		
		List<ParameterTable> lstDocumentTypeTmp = parameterServiceBean.getListParameterTableServiceBean(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());		
		for (ParticipantRequest objParticipantRequest : lstParticipantRequest) {
			
			for (ParameterTable docTypePrm : lstDocumentTypeTmp) {
				if(docTypePrm.getParameterTablePk().equals(objParticipantRequest.getParticipant().getDocumentType())){
					objParticipantRequest.getParticipant().setDocumentTypeDescription(docTypePrm.getIndicator1());					
					break;
				}
			}
			
		}
	}
	
	/**
	 * Fill participant request params.
	 *
	 * @param lstParticipantRequest the lst participant request
	 * @throws ServiceException the service exception
	 */
	private void fillParticipantRequestParams(List<ParticipantRequest> lstParticipantRequest) throws ServiceException{
		
		List<ParameterTable> lstStates = parameterServiceBean.getListParameterTableServiceBean(MasterTableType.PARTICIPANT_BLOCK_UBBLOCK_ANNUL_STATE.getCode());
		
		List<ParameterTable> lstMotiveRequests = parameterServiceBean.getListParameterTableServiceBean(MasterTableType.PARTICIPANT_BLOCK_MOTIVE.getCode());
		lstMotiveRequests.addAll(parameterServiceBean.getListParameterTableServiceBean(MasterTableType.PARTICIPANT_UNBLOCK_MOTIVE.getCode()));
		lstMotiveRequests.addAll(parameterServiceBean.getListParameterTableServiceBean(MasterTableType.PARTICIPANT_ANNUL_MOTIVE.getCode()));
		
		List<ParameterTable> lstRequestTypes = parameterServiceBean.getListParameterTableServiceBean(MasterTableType.PARTICIPANT_BLOCK_UBBLOCK_ANNUL_TYPE.getCode());
		
		for (ParticipantRequest objParticipantRequest : lstParticipantRequest) {
			
			for (ParameterTable statePrm : lstStates) {
				if(statePrm.getParameterTablePk().equals(objParticipantRequest.getStateRequest())){
					objParticipantRequest.setParticipantRequestStateDescription(statePrm.getParameterName());
					break;
				}
			}
			
			if(Validations.validateIsNotNull(objParticipantRequest.getRequestMotive())) {
				for (ParameterTable motiveRequestPrm : lstMotiveRequests) {
					if(motiveRequestPrm.getParameterTablePk().equals(objParticipantRequest.getRequestMotive())){
						objParticipantRequest.setRequestMotiveDescription(motiveRequestPrm.getParameterName());
						break;
					}
				}
			}
			
			
			for (ParameterTable requestTypePrm : lstRequestTypes) {
				if(requestTypePrm.getParameterTablePk().equals(objParticipantRequest.getRequestType())){
					objParticipantRequest.setRequestTypeDescription(requestTypePrm.getParameterName());
					break;
				}
			}
			
		}
	}
	
	/**
	 * Find participant request by id service facade.
	 *
	 * @param participantRequestTO the participant request to
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequest findParticipantRequestByIdServiceFacade(ParticipantRequestTO participantRequestTO)
			throws ServiceException{
		return participantMgmtServiceBean.findParticipantRequestByIdServiceBean(participantRequestTO);
	}
	
	/**
	 * Update holder request service facade.
	 *
	 * @param holderRequest the holder request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateHolderRequestServiceFacade(HolderRequest holderRequest)throws ServiceException{			
   	 	crudDaoServiceBean.update(holderRequest);
        return true;   	 
    }
	
	/**
	 * Gets the list participant history states service facade.
	 *
	 * @param participantHistoryStateTO the participant history state to
	 * @return the list participant history states service facade
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantHistoryState> getListParticipantHistoryStatesServiceFacade(ParticipantHistoryStateTO participantHistoryStateTO)
			throws ServiceException {
		return participantMgmtServiceBean.getListParticipantHistoryStatesServiceBean(participantHistoryStateTO);
	}
	
	/**
	 * Find represented entity by id service bean.
	 *
	 * @param filter the filter
	 * @return the represented entity
	 * @throws ServiceException the service exception
	 */
	public RepresentedEntity findRepresentedEntityByIdServiceBean(RepresentedEntityTO filter) throws ServiceException{
		return participantMgmtServiceBean.findRepresentedEntityByIdServiceBean(filter);
	}
	
	/**
	 * Registry participant modificacion request service facade.
	 *
	 * @param participant the participant
	 * @param lstLegalRepresentativeHistory the lst legal representative history
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MODIFICATION_PARTICIPANT_REGISTRATION)
	public ParticipantRequest registryParticipantModification(Participant participant, List<LegalRepresentativeHistory> lstLegalRepresentativeHistory) throws ServiceException{				
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		//Getting data of old participant
		Participant participantOld = crudDaoServiceBean.find(Participant.class, participant.getIdParticipantPk());
		
		//Set list of active participant files
		participantOld.setParticipantFiles(participantMgmtServiceBean.getListParticipantFilesServiceBean(participantOld));
		
		//Set list of active participant files
		participantOld.setInstitutionBankAccounts(participantMgmtServiceBean.getListInstitutionBankAccountsServiceBean(participantOld));
		
		//Set list of active participant int depositories
		ParticipantIntDepositoryTO participantIntDepositoryTO = new ParticipantIntDepositoryTO();
		participantIntDepositoryTO.setIdParticipantPk(participantOld.getIdParticipantPk());
		participantIntDepositoryTO.setStateParticipantIntDepositary(ParticipantIntDepositaryStateType.REGISTERED.getCode());
		participantOld.setParticipantIntDepositaries(participantMgmtServiceBean.getListParticipantIntDepoByParticipantServiceBean(participantIntDepositoryTO));
		

		ParticipantMechanismTO participantMechanismTO = new ParticipantMechanismTO();
		participantMechanismTO.setIdParticipantPk(participantOld.getIdParticipantPk());
		participantMechanismTO.setStateParticipantMechanism(ParticipantMechanismStateType.REGISTERED.getCode());
		
		participantOld.setParticipantMechanisms(participantMgmtServiceBean.getListParticipantMechanismByParticipantServiceBean(participantMechanismTO));
		
		//Set the list of Representative File History
		participantMgmtServiceBean.fillRepresentativeFileHist(lstLegalRepresentativeHistory);

		//Instance Participant Request for modification
		ParticipantRequest participantRequest = new ParticipantRequest();
		participantRequest.setParticipant(participant);
		participantRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		participantRequest.setRegistryUser(loggerUser.getUserName());
		participantRequest.setRequestType(ParticipantRequestType.MODIFICATION.getCode());
		participantRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_MODIFICATION_PARTICIPANT));
		participantRequest.setStateRequest(ParticipantRequestStateType.REGISTERED.getCode());		
		
		//Setting old participant file histories
		participantMgmtServiceBean.prepareParticipantFilesHist(participantRequest, participantOld, BooleanType.NO.getCode(), loggerUser, Boolean.FALSE);
		
		//Setting new participant file histories
		participantMgmtServiceBean.prepareParticipantFilesHist(participantRequest, participant, BooleanType.YES.getCode(), loggerUser, Boolean.TRUE);
		
		//Setting old participant file histories
		participantMgmtServiceBean.prepareInstitutionBankAccountsHist(participantRequest, participantOld, BooleanType.NO.getCode(), loggerUser, Boolean.FALSE);
		
		//Setting new participant file histories
		participantMgmtServiceBean.prepareInstitutionBankAccountsHist(participantRequest, participant, BooleanType.YES.getCode(), loggerUser, Boolean.TRUE);
		
		
		//Setting old Participant Int Depositary History
		participantMgmtServiceBean.prepareParticipantIntDepositHist(participantRequest, participantOld, BooleanType.NO.getCode(), loggerUser, Boolean.FALSE);
		
		//Setting new Participant Int Depositary History
		participantMgmtServiceBean.prepareParticipantIntDepositHist(participantRequest, participant, BooleanType.YES.getCode(), loggerUser, Boolean.TRUE);
		
		
		//Setting old Participant Mechanism
		participantMgmtServiceBean.prepareParticipantMechanismHist(participantRequest, participantOld, BooleanType.NO.getCode(), loggerUser, Boolean.FALSE);

		//Setting new Participant Mechanism
		participantMgmtServiceBean.prepareParticipantMechanismHist(participantRequest, participant, BooleanType.YES.getCode(), loggerUser, Boolean.TRUE);
		
		
		//Persist Participant request
		crudDaoServiceBean.create(participantRequest);
		
		//Save old data on Participant Request History
		participantMgmtServiceBean.prepareAndSaveParticipantHistory(participantRequest, participantOld, BooleanType.NO.getCode(), loggerUser);	
		
		//Save new data on Participant Request History
		participantMgmtServiceBean.prepareAndSaveParticipantHistory(participantRequest, participant, BooleanType.YES.getCode(), loggerUser);
		
		//Save Old Data on Legal Representative History
		participantMgmtServiceBean.prepareAndSaveListLegalRepresentativeHist(participantRequest, 
				participantOld, RegistryType.ORIGIN_OF_THE_REQUEST.getCode(), loggerUser);
		
		//Save New Data on Legal Representative History
		participantMgmtServiceBean.prepareAndSaveListLegalRepresentativeHist(participantRequest, 
				lstLegalRepresentativeHistory, RegistryType.HISTORICAL_COPY.getCode(), loggerUser);
				
		
		return participantRequest;
		
	}
	
	/**
	 * Register holder request service facade.
	 *
	 * @param holderRequest the holder request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerHolderRequestServiceFacade(HolderRequest holderRequest) throws ServiceException{
		holderRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_REGISTER_RNT));
		crudDaoServiceBean.create(holderRequest);				
		return true;
	}
	
	/**
	 * Find participant request modification by id service facade.
	 *
	 * @param participantRequestTO the participant request to
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequest findParticipantRequestModificationById(ParticipantRequestTO participantRequestTO)
			throws ServiceException{
		return participantMgmtServiceBean.findParticipantRequestModificationById(participantRequestTO);
	}
	
	/**
	 * Registry Bank Accounts Facade.
	 *
	 * @param institutionBankAccount institution Bank Account
	 * @return  institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.BANK_ACCOUNT_REGISTRATION)
	public InstitutionBankAccount registryBankAccountsFacade(InstitutionBankAccount institutionBankAccount)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        return participantMgmtServiceBean.registryBankAccountsFacade(institutionBankAccount, loggerUser);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public InstitutionCashAccount registryCashAccountFacade(InstitutionBankAccount institutionBankAccount) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		InstitutionCashAccount institutionCashAcc = new InstitutionCashAccount();		
		institutionCashAcc.setSituation(BooleanType.YES.getCode());
		institutionCashAcc.setAccountState(CashAccountStateType.ACTIVATE.getCode());
		institutionCashAcc.setIndAutomaticProcess(BooleanType.NO.getCode());
		institutionCashAcc.setDepositAmount(BigDecimal.ZERO);
		institutionCashAcc.setTotalAmount(BigDecimal.ZERO);
		institutionCashAcc.setAvailableAmount(BigDecimal.ZERO);
		institutionCashAcc.setRetirementAmount(BigDecimal.ZERO);
		institutionCashAcc.setMarginAmount(BigDecimal.ZERO);
		institutionCashAcc.setIndRelatedBcrd(BooleanType.NO.getCode());
		institutionCashAcc.setAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
		institutionCashAcc.setIndSettlementProcess(BooleanType.NO.getCode());
		institutionCashAcc.setAccountType(AccountCashFundsType.BENEFIT.getCode());
		institutionCashAcc.setCurrency(institutionBankAccount.getCurrency());
		InstitutionCashAccount centralInstCashAccount = cashAccountService.getCentralInstitutionCashAccount(institutionBankAccount.getCurrency(),AccountCashFundsType.CENTRALIZING_BENEFIT.getCode());
		if(Validations.validateIsNull(centralInstCashAccount))
			throw new ServiceException(ErrorServiceType.CENTRAL_CASH_ACCOUNT_NOT_ACTIVE);				
		institutionCashAcc.setInstitutionCashAccount(centralInstCashAccount);
		institutionCashAcc.setParticipant(institutionBankAccount.getParticipant());
		institutionCashAcc.setIndSendSettlementFunds(BooleanType.NO.getCode());
		institutionCashAcc.setAudit(loggerUser);
		participantMgmtServiceBean.create(institutionCashAcc);
		return institutionCashAcc;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CashAccountDetail registryCashAccountDetailFacade(InstitutionBankAccount institutionBankAccount,InstitutionCashAccount institutionCashAccount) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		CashAccountDetail cashAccountDetailSave = new CashAccountDetail();
		cashAccountDetailSave.setBankAccountClass(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getLngCode().intValue());
		cashAccountDetailSave.setIndReception(GeneralConstants.ONE_VALUE_INTEGER);
		cashAccountDetailSave.setIndSending(GeneralConstants.ONE_VALUE_INTEGER);
		cashAccountDetailSave.setInstitutionBankAccount(institutionBankAccount);
		cashAccountDetailSave.setInstitutionCashAccount(institutionCashAccount);
		cashAccountDetailSave.setAccountState(DetailCashAccountStateType.ACTIVATE.getCode());
		cashAccountDetailSave.setAudit(loggerUser);
		participantMgmtServiceBean.create(cashAccountDetailSave);
		return cashAccountDetailSave;
	}

	/**
	 * Find participant history by request id service facade.
	 *
	 * @param participantRequestTO the participant request to
	 * @return the participant request history
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequestHistory findParticipantHistoryByRequest(
			ParticipantRequestTO participantRequestTO) throws ServiceException{
		return participantMgmtServiceBean.findParticipantHistoryByRequest(participantRequestTO);
	}
	
	/**
	 * getInstitutionBankAccountForAccounu Number And Currency.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idBank the id bank
	 * @param number number account
	 * @param idCurrency id currency
	 * @return Institution Bank Account
	 * @throws ServiceException the Service Exception
	 */
	public InstitutionBankAccount getInstitutionBankAccountForAccountNumberAndCurrency(Integer institution,String idInstitution,Long idBank,String number , Integer idCurrency)throws ServiceException{
		return participantMgmtServiceBean.getInstitutionBankAccountForAccountNumberAndCurrency(institution, idInstitution, idBank, number, idCurrency);
	}
	
	/**
	 * Validate institution cash account.
	 *
	 * @param objSearchBankAccountsTO the obj search bank accounts to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionBankAccount> validateInstitutionCashAccount(SearchBankAccountsParticipantTO objSearchBankAccountsTO)throws ServiceException{
		return participantMgmtServiceBean.validateInstitutionCashAccount(objSearchBankAccountsTO);
	}
	
	/**
	 * Registry issuer on cascade service facade.
	 *
	 * @param participant the participant
	 * @param lstLegalRepresentativeHistory the lst legal representative history
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PARTICIPANT_REGISTRATION)
	public boolean registryParticipantOnCascadeServiceFacade(Participant participant, List<LegalRepresentativeHistory> lstLegalRepresentativeHistory)  throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());  
        
		// asignacion de clave primaria manualmente issue611
		Long nextVal = null;
		try {
			nextVal = participant.getHolder().getIdHolderPk();
			if (accountsFacade.validateExistHolderServiceFacade(nextVal))
				// si el cui ingresado ya esiste lanzar a proposito la excepcion para que le asigne uno calculado
				throw new Exception();
		} catch (Exception e) {
			nextVal=null;
			System.out.println("::: no se ha seleccionado o ya existe CUI. se asignarA uno por defecto segun actividad economica");
		}
		
		if(participant.getIssuer()!=null && participant.getIssuer().getIdIssuerPk() == null){
			participant.setIssuer(null);
		}
    	
        List<LegalRepresentative> lstLegalRepresentativeFound = new ArrayList<LegalRepresentative>();
        LegalRepresentative legalRepresentativeFound;
        
        /** The fund administrator (Mnemonic ASFI). */
        String fundAdministrator = participant.getHolder().getFundAdministrator();
        
        /** The transfer number (Transfer number). */
        String transferNumber = participant.getHolder().getTransferNumber();
        
        /** The mnemonic (Mnemonic Fondo). */
        String mnemonic = participant.getHolder().getMnemonic();
        
        for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistory) {
        	if(legalRepresentativeHistory.isFlagLegalImport() && 
        			!legalRepresentativeHistory.isFlagActiveFilesByHolderImport()) {
        		legalRepresentativeFound = new LegalRepresentative();
            	legalRepresentativeFound.setDocumentType(legalRepresentativeHistory.getDocumentType());
            	legalRepresentativeFound.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
            	legalRepresentativeFound.setNationality(legalRepresentativeHistory.getNationality());
            	legalRepresentativeFound.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
            	
            	if(legalRepresentativeHistory.getDocumentSource()!=null){
            		legalRepresentativeFound.setDocumentSource(legalRepresentativeHistory.getDocumentSource());
            	}
            	
            	legalRepresentativeFound = accountsFacade.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(legalRepresentativeFound);
            	
            	if(Validations.validateIsNotNull(legalRepresentativeFound)){
            		lstLegalRepresentativeFound.add(legalRepresentativeFound);
            	}
        	}
		}
        
	    Holder holder = null;
	    HolderRequest holderRequest = null;
	    	   
	    List<HolderFile> holderFiles = null;
	    
	    if(participant.isIndHolderWillBeCreated()){
	    	
	    	holderFiles = participant.getHolder().getHolderFile();
	    	
	    	holderRequest = new HolderRequest();
	    	holderRequest.setStateHolderRequest(HolderRequestStateType.CONFIRMED.getCode());
	    	holderRequest.setRequestType(HolderRequestType.CREATION.getCode());
	    	holderRequest.setActionDate(CommonsUtilities.currentDateTime());    	
			holderRequest.setRegistryDate(CommonsUtilities.currentDateTime());
			holderRequest.setRegistryUser(loggerUser.getUserName());
			holderRequest.setApproveDate(CommonsUtilities.currentDateTime());
			holderRequest.setApproveUser(loggerUser.getUserName());	
			holderRequest.setConfirmDate(CommonsUtilities.currentDateTime());
			holderRequest.setConfirmUser(loggerUser.getUserName());		
			holderRequest.setParticipant(participant);
			
	    	
	    	HolderHistory holderHistory = new HolderHistory();	    	
	    	holderHistory.setIndMinor(BooleanType.NO.getCode());
	    	holderHistory.setIndDisabled(BooleanType.NO.getCode());
	    	holderHistory.setIndPEP(BooleanType.NO.getCode());	    	
	    	//Set the document type Parameter of Holder instead of document tipe parameter of Participant	    	
	    	holderHistory.setDocumentType(participant.getDocumentType());	    	
	    	holderHistory.setDocumentNumber(participant.getDocumentNumber());
	    	holderHistory.setDocumentSource(participant.getDocumentSource());
	    	holderHistory.setEconomicActivity(participant.getEconomicActivity());
	    	holderHistory.setInvestorType(participant.getInvestorType());
	    	holderHistory.setEconomicSector(participant.getEconomicSector());
	    	holderHistory.setEmail(participant.getEmail());
	    	holderHistory.setFaxNumber(participant.getFaxNumber());
	    	holderHistory.setFullName(participant.getDescription());
	    	holderHistory.setHolderType(PersonType.JURIDIC.getCode());
	    	holderHistory.setHomePhoneNumber(participant.getPhoneNumber());
	    	holderHistory.setOfficePhoneNumber(participant.getPhoneNumber());
	    	holderHistory.setJuridicClass(JuridicClassType.NORMAL.getCode());
	    	holderHistory.setLegalAddress(participant.getAddress());
	    	holderHistory.setLegalDepartment(participant.getDepartment());
	    	holderHistory.setLegalDistrict(participant.getDistrict());
	    	holderHistory.setLegalProvince(participant.getProvince());
	    	holderHistory.setLegalResidenceCountry(participant.getResidenceCountry());	    		    	
	    	holderHistory.setNationality(participant.getResidenceCountry());
	    	holderHistory.setPostalAddress(participant.getAddress());
	    	holderHistory.setPostalDepartment(participant.getDepartment());
	    	holderHistory.setPostalDistrict(participant.getDistrict());
			holderHistory.setPostalProvince(participant.getProvince());
			holderHistory.setPostalResidenceCountry(participant.getResidenceCountry());
			holderHistory.setIndResidence(countryResidence.equals(participant.getResidenceCountry()) ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			holderHistory.setStateHolderHistory(HolderRequestStateType.CONFIRMED.getCode());			
			holderHistory.setRegistryDate(CommonsUtilities.currentDateTime());
			holderHistory.setRegistryUser(loggerUser.getUserName());
			holderHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			holderHistory.setHolderRequest(holderRequest);
			holderHistory.setFundAdministrator(participant.getHolder().getFundAdministrator());
			holderHistory.setMnemonic(participant.getHolder().getMnemonic());
			holderHistory.setTransferNumber(participant.getHolder().getTransferNumber());
			holderHistory.setIndSirtexNeg(BooleanType.YES.getCode());
			
			List<HolderHistory> holderHistories = new ArrayList<HolderHistory>();
			holderHistories.add(holderHistory);
			
			holderRequest.setHolderHistories(holderHistories);
			
			if(Validations.validateListIsNotNullAndNotEmpty(holderFiles)){
				List<HolderReqFileHistory> holderReqFileHistories = new ArrayList<HolderReqFileHistory>();
				for (HolderFile holderFile : holderFiles) {
					HolderReqFileHistory holderReqFileHistory = new HolderReqFileHistory();

					holderReqFileHistory.setHolderReqFileHisType(HolderReqFileHisType.CREATION.getCode());
					holderReqFileHistory.setFilename(holderFile.getFilename());
					holderReqFileHistory.setFileHolderReq(holderFile.getFileHolder());
					holderReqFileHistory.setRegistryUser(loggerUser.getUserName());
					holderReqFileHistory.setRegistryDate(CommonsUtilities.currentDate());
					holderReqFileHistory.setHolderRequest(holderRequest);
					holderReqFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					holderReqFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
					holderReqFileHistory.setDocumentType(holderFile.getDocumentType());
					holderReqFileHistory.setDescription(holderFile.getDescription());
					
					holderReqFileHistories.add(holderReqFileHistory);
				}
				
				holderRequest.setHolderReqFileHistories(holderReqFileHistories);
			}
			
			participant.setHolder(null);						
	    	
	    }
	    
	    if(Validations.validateListIsNotNullAndNotEmpty(participant.getParticipantFiles())){
    		for (ParticipantFile participantFile : participant.getParticipantFiles()) {
    			participantFile.setRegistryUser(loggerUser.getUserName());
    			participantFile.setRegistryDate(CommonsUtilities.currentDateTime());
			}
    	}
	    
	    if(Validations.validateListIsNotNullAndNotEmpty(participant.getParticipantIntDepositaries())){
    		for (ParticipantIntDepositary participantIntDepositary : participant.getParticipantIntDepositaries()) {
    			participantIntDepositary.setRegistryUser(loggerUser.getUserName());
    			participantIntDepositary.setRegistryDate(CommonsUtilities.currentDateTime());
			}
    	}
    	
    	if(Validations.validateListIsNotNullAndNotEmpty(participant.getParticipantMechanisms())){
    		for (ParticipantMechanism participantMechanism : participant.getParticipantMechanisms()) {
    			participantMechanism.setRegistryUser(loggerUser.getUserName());
    			participantMechanism.setRegistryDate(CommonsUtilities.currentDateTime());
    			participantMechanism.setIndPurchaseRole(BooleanType.YES.getCode());
    			participantMechanism.setIndSaleRole(BooleanType.YES.getCode());
			}
    	}
    	
    	participant.setRegistryUser(loggerUser.getUserName());
    	participant.setRegistryDate(CommonsUtilities.currentDateTime());

    	Holder holderFilter = new Holder();
		holderFilter.setDocumentType(participant.getDocumentType());								
		holderFilter.setDocumentNumber(participant.getDocumentNumber());
		holderFilter.setDocumentSource(participant.getDocumentSource());
		
	    Holder auxholder = accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(holderFilter);
		if(Validations.validateIsNotNull(auxholder)){
			
			if(HolderStateType.BLOCKED.getCode().equals(auxholder.getStateHolder())){
				participant.setHolder(auxholder);									
				participant.setIndHolderWillBeCreated(Boolean.FALSE);
			}else if(HolderStateType.REGISTERED.getCode().equals(auxholder.getStateHolder())){
				participant.setHolder(auxholder);									
				participant.setIndHolderWillBeCreated(Boolean.FALSE);
			}																		
		} else {
			participant.setIndHolderWillBeCreated(Boolean.TRUE);
		}
    	
    	registryParticipant(participant);
    	
    	if(participant.isIndHolderWillBeCreated()){
    		registerHolderRequestServiceFacade(holderRequest);			
	    	
	    	holder = new Holder();	    		    	
	    	holder.setIndMinor(BooleanType.NO.getCode());	    	
	    	holder.setIndDisabled(BooleanType.NO.getCode());	    	
	    	//Set the document type Parameter of Holder instead of document tipe parameter of Participant
	    	holder.setDocumentType(participant.getDocumentType());
	    	holder.setDocumentNumber(participant.getDocumentNumber());
	    	holder.setDocumentSource(participant.getDocumentSource());
	    	holder.setEconomicActivity(participant.getEconomicActivity());
	    	holder.setInvestorType(participant.getInvestorType());
	    	holder.setEconomicSector(participant.getEconomicSector());
	    	holder.setEmail(participant.getEmail());
	    	holder.setFaxNumber(participant.getFaxNumber());
	    	holder.setFullName(participant.getDescription());
	    	holder.setHolderType(PersonType.JURIDIC.getCode());
	    	holder.setHomePhoneNumber(participant.getPhoneNumber());
	    	holder.setOfficePhoneNumber(participant.getPhoneNumber());
	    	holder.setJuridicClass(JuridicClassType.NORMAL.getCode());
	    	holder.setLegalAddress(participant.getAddress());
	    	holder.setLegalDepartment(participant.getDepartment());
	    	holder.setLegalDistrict(participant.getDistrict());
	    	holder.setLegalProvince(participant.getProvince());
	    	holder.setLegalResidenceCountry(participant.getResidenceCountry());	    		    	
	    	holder.setNationality(participant.getResidenceCountry());
	    	holder.setPostalAddress(participant.getAddress());
	    	holder.setPostalDepartment(participant.getDepartment());
	    	holder.setPostalDistrict(participant.getDistrict());
	    	holder.setPostalProvince(participant.getProvince());
	    	holder.setPostalResidenceCountry(participant.getResidenceCountry());
	    	holder.setIndResidence(countryResidence.equals(participant.getResidenceCountry()) ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
	    	holder.setStateHolder(HolderStateType.REGISTERED.getCode());    	
	    	holder.setRegistryDate(CommonsUtilities.currentDateTime());
	    	holder.setRegistryUser(loggerUser.getUserName());
	    	holder.setParticipant(participant);
	    	holder.setFundAdministrator(fundAdministrator);
	    	holder.setMnemonic(mnemonic);
	    	holder.setTransferNumber(transferNumber);
	    	holder.setIndSirtexNeg(BooleanType.YES.getCode());
	    	
	    	//issue 770
	    	holder = validateIndCanTransfer(holder);

			// asignacion de clave primaria manualmente issue611
			if (nextVal == null)
				nextVal = assignIdentityHolder(holder.getEconomicActivity());
			else if (accountsFacade.validateExistHolderServiceFacade(nextVal))
				nextVal = assignIdentityHolder(holder.getEconomicActivity());
	    	
	    	holder.setIdHolderPk(nextVal);
	    	crudDaoServiceBean.create(holder);
	    	
    		//Set Participant to holder Request
    		holderRequest.setHolder(holder);        	
        	
        	List<HolderHistoryState> lstHolderHistoryState = new ArrayList<HolderHistoryState>();
			HolderHistoryState holderHistoryState = new HolderHistoryState();
			
			holderHistoryState.setHolder(holder);
			holderHistoryState.setHolderRequest(holderRequest);
			holderHistoryState.setRegistryUser(loggerUser.getUserName());
			holderHistoryState.setHolderRequest(holderRequest);
			holderHistoryState.setOldState(StateHistoryStateHolderType.REGISTERED.getCode());
			holderHistoryState.setNewState(StateHistoryStateHolderType.REGISTERED.getCode());
			holderHistoryState.setUpdateStateDate(CommonsUtilities.currentDateTime());
			lstHolderHistoryState.add(holderHistoryState);				
			
			holderRequest.setHolderHistoryStates(lstHolderHistoryState);
        	
        	updateHolderRequestServiceFacade(holderRequest);
        	
        	//Set the created holder pk to participant
        	participant.setHolder(holder);
        	
        	if(Validations.validateListIsNotNullAndNotEmpty(holderFiles)){
				for (HolderFile holderFile : holderFiles) {

					holderFile.setHolderFileType(HolderReqFileHisType.CREATION.getCode());
					holderFile.setRegistryUser(loggerUser.getUserName());
					holderFile.setRegistryDate(CommonsUtilities.currentDate());
					holderFile.setHolder(holder);
					holderFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					holderFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
					
					crudDaoServiceBean.create(holderFile);
				}								
			}
        	
        	updateParticipant(participant);
    	}
    	
    	
    	RepresentedEntity representedEntity = null;
		List<RepresentedEntity> representedEntities = new ArrayList<RepresentedEntity>();		
		LegalRepresentative objLegalRepresentative = null;
		PepPerson pepPerson;	
		GeographicLocation countryPep;
		GeographicLocation expeditionPlacePep;
		boolean indLegalRepresentativeFoumd;
		if(!lstLegalRepresentativeHistory.isEmpty()){
			for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistory) {
				
				indLegalRepresentativeFoumd = false;
				if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeFound)){
					for (LegalRepresentative legalRepresentativeAux : lstLegalRepresentativeFound) {
						if(legalRepresentativeAux.getDocumentType().equals(legalRepresentativeHistory.getDocumentType()) &&
								legalRepresentativeAux.getDocumentNumber().equals(legalRepresentativeHistory.getDocumentNumber())){
							indLegalRepresentativeFoumd = true;
							objLegalRepresentative = legalRepresentativeAux;
							break;
						}
					}
				}
				
				if(!indLegalRepresentativeFoumd) {
					legalRepresentativeHistory.setRegistryUser(loggerUser.getUserName());
					legalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
					legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.CREATION.getCode());
					legalRepresentativeHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					
					if(participant.isIndHolderWillBeCreated()){
						legalRepresentativeHistory.setHolderRequest(holderRequest);
					}
					
					if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
						for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeHistory.getRepresenteFileHistory()) {
							representativeFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
							representativeFileHistory.setRegistryUser(loggerUser.getUserName());
							representativeFileHistory.setRegistryDate(CommonsUtilities.currentDate());
							representativeFileHistory.setLegalRepresentativeHistory(legalRepresentativeHistory);
							representativeFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
							representativeFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
						}
					}
	    							
					registryLegalRepresentativeHistory(legalRepresentativeHistory);
					
					objLegalRepresentative = new LegalRepresentative();
					objLegalRepresentative.setBirthDate(legalRepresentativeHistory.getBirthDate());
					objLegalRepresentative.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
					objLegalRepresentative.setDocumentSource(legalRepresentativeHistory.getDocumentSource());
					objLegalRepresentative.setDocumentType(legalRepresentativeHistory.getDocumentType());
					objLegalRepresentative.setEconomicActivity(legalRepresentativeHistory.getEconomicActivity());
					objLegalRepresentative.setEconomicSector(legalRepresentativeHistory.getEconomicSector());
					objLegalRepresentative.setEmail(legalRepresentativeHistory.getEmail());
					objLegalRepresentative.setFaxNumber(legalRepresentativeHistory.getFaxNumber());
					objLegalRepresentative.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
					objLegalRepresentative.setFullName(legalRepresentativeHistory.getFullName());
					objLegalRepresentative.setHomePhoneNumber(legalRepresentativeHistory.getHomePhoneNumber());
					objLegalRepresentative.setIndResidence(legalRepresentativeHistory.getIndResidence());
					objLegalRepresentative.setLegalAddress(legalRepresentativeHistory.getLegalAddress());
					objLegalRepresentative.setLegalDistrict(legalRepresentativeHistory.getLegalDistrict());
					objLegalRepresentative.setLegalProvince(legalRepresentativeHistory.getLegalProvince());
					objLegalRepresentative.setLegalDepartment(legalRepresentativeHistory.getLegalDepartment());
					objLegalRepresentative.setLegalResidenceCountry(legalRepresentativeHistory.getLegalResidenceCountry());
					objLegalRepresentative.setMobileNumber(legalRepresentativeHistory.getMobileNumber());
					objLegalRepresentative.setName(legalRepresentativeHistory.getName());
					objLegalRepresentative.setNationality(legalRepresentativeHistory.getNationality());
					objLegalRepresentative.setOfficePhoneNumber(legalRepresentativeHistory.getOfficePhoneNumber());
					objLegalRepresentative.setPersonType(legalRepresentativeHistory.getPersonType());
					objLegalRepresentative.setPostalAddress(legalRepresentativeHistory.getPostalAddress());
					objLegalRepresentative.setPostalDepartment(legalRepresentativeHistory.getPostalDepartment());
					objLegalRepresentative.setPostalDistrict(legalRepresentativeHistory.getPostalDistrict());
					objLegalRepresentative.setPostalProvince(legalRepresentativeHistory.getPostalProvince());
					objLegalRepresentative.setPostalResidenceCountry(legalRepresentativeHistory.getPostalResidenceCountry());
					objLegalRepresentative.setSecondDocumentNumber(legalRepresentativeHistory.getSecondDocumentNumber());
					objLegalRepresentative.setSecondDocumentType(legalRepresentativeHistory.getSecondDocumentType());
					objLegalRepresentative.setSecondLastName(legalRepresentativeHistory.getSecondLastName());
					objLegalRepresentative.setSecondNationality(legalRepresentativeHistory.getSecondNationality());
					objLegalRepresentative.setSex(legalRepresentativeHistory.getSex());
					objLegalRepresentative.setState(LegalRepresentativeStateType.REGISTERED.getCode());
					objLegalRepresentative.setRegistryUser(loggerUser.getUserName());
	    			objLegalRepresentative.setRegistryDate(CommonsUtilities.currentDateTime());
	    			
	    			if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
	    				List<LegalRepresentativeFile> lstLegalRepresentativeFile = new ArrayList<LegalRepresentativeFile>();
	    				LegalRepresentativeFile legalRepresentativeFile;
						for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeHistory.getRepresenteFileHistory()) {
							legalRepresentativeFile = new LegalRepresentativeFile();
							
							legalRepresentativeFile.setDescription(representativeFileHistory.getDescription());
							legalRepresentativeFile.setDocumentType(representativeFileHistory.getDocumentType());
							legalRepresentativeFile.setFilename(representativeFileHistory.getFilename());	
							legalRepresentativeFile.setFileLegalRepre(representativeFileHistory.getFileRepresentative());					
							legalRepresentativeFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
							legalRepresentativeFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
							legalRepresentativeFile.setLegalRepreFileType(HolderReqFileHisType.CREATION.getCode());
							legalRepresentativeFile.setLegalRepresentative(objLegalRepresentative);
							legalRepresentativeFile.setRegistryUser(loggerUser.getUserName());
							legalRepresentativeFile.setRegistryDate(CommonsUtilities.currentDateTime());
							
							lstLegalRepresentativeFile.add(legalRepresentativeFile);
							
						}
						objLegalRepresentative.setLegalRepresentativeFile(lstLegalRepresentativeFile);
					}
	    			
	    			registryLegalRepresentative(objLegalRepresentative);
	    			
	    			if(BooleanType.YES.getCode().equals(legalRepresentativeHistory.getIndPEP())){
	    				pepPerson = new PepPerson();
	    				pepPerson.setFirstName(legalRepresentativeHistory.getName());
	    				if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.getSecondLastName())) {
	    					pepPerson.setLastName(legalRepresentativeHistory.getFirstLastName() + GeneralConstants.BLANK_SPACE + legalRepresentativeHistory.getSecondLastName());
	    				} else {
	    					pepPerson.setLastName(legalRepresentativeHistory.getFirstLastName());
	    				}
	    				pepPerson.setBeginingPeriod(legalRepresentativeHistory.getBeginningPeriod());
	    				pepPerson.setCategory(legalRepresentativeHistory.getCategory());
	    				pepPerson.setComments(legalRepresentativeHistory.getComments());
	    				pepPerson.setEndingPeriod(legalRepresentativeHistory.getEndingPeriod());
	    				pepPerson.setBeginingPeriod(legalRepresentativeHistory.getBeginningPeriod());
	    				pepPerson.setRole(legalRepresentativeHistory.getRole());
	    				pepPerson.setLegalRepresentative(objLegalRepresentative);
	    				pepPerson.setRegistryUser(loggerUser.getUserName());
	    				pepPerson.setRegistryDate(CommonsUtilities.currentDateTime());
	    				pepPerson.setNotificationDate(CommonsUtilities.currentDateTime());
	    				countryPep = new GeographicLocation();
	    				countryPep.setIdGeographicLocationPk(objLegalRepresentative.getLegalResidenceCountry());		    			
		    		    pepPerson.setGeographicLocation(countryPep);
		    		    pepPerson.setDocumentType(legalRepresentativeHistory.getDocumentType());
		    		    pepPerson.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
		    		    pepPerson.setClassification(PepPersonClassificationType.HIGH.getCode());
		    		    expeditionPlacePep = new GeographicLocation();
		    		    expeditionPlacePep.setIdGeographicLocationPk(objLegalRepresentative.getLegalResidenceCountry());
		    		    pepPerson.setExpeditionPlace(expeditionPlacePep);
		    		    
	    				crudDaoServiceBean.create(pepPerson);
	    			}
				}
				
				representedEntity = new RepresentedEntity();				
    			representedEntity.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
    			representedEntity.setLegalRepresentative(objLegalRepresentative);
    			representedEntity.setRepresentativeClass(legalRepresentativeHistory.getRepresentativeClass());
    			representedEntities.add(representedEntity);
			}
		}		
		
		if(!representedEntities.isEmpty()){
			if(participant.isIndHolderWillBeCreated()){
				RepresentedEntity objRepresentedEntityHolder;			
				for (RepresentedEntity objRepresentedEntity : representedEntities) {
					
					objRepresentedEntityHolder = new RepresentedEntity();
					objRepresentedEntityHolder.setRegistryUser(loggerUser.getUserName());
					objRepresentedEntityHolder.setRegistryDate(CommonsUtilities.currentDateTime());
					objRepresentedEntityHolder.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
					objRepresentedEntityHolder.setHolder(holder);
					objRepresentedEntityHolder.setLegalRepresentative(objRepresentedEntity.getLegalRepresentative());
					objRepresentedEntityHolder.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
					crudDaoServiceBean.create(objRepresentedEntityHolder);
				}
			}

			RepresentedEntity objRepresentedEntityIssuer;
			for (RepresentedEntity objRepresentedEntity : representedEntities) {
				
				objRepresentedEntityIssuer = new RepresentedEntity();
				objRepresentedEntityIssuer.setRegistryUser(loggerUser.getUserName());
				objRepresentedEntityIssuer.setRegistryDate(CommonsUtilities.currentDateTime());
				objRepresentedEntityIssuer.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
				objRepresentedEntityIssuer.setParticipant(participant);				
				objRepresentedEntityIssuer.setLegalRepresentative(objRepresentedEntity.getLegalRepresentative());
				objRepresentedEntityIssuer.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
				crudDaoServiceBean.create(objRepresentedEntityIssuer);
			}
		}
		
		if(participant.getIndDepositary().equals(GeneralConstants.ONE_VALUE_INTEGER)) {
			this.serviceAPIClients.callWsCreateInstitutionSecurity(
					participant.getDescription(), participant.getMnemonic(), InstitutionType.PARTICIPANT.getCode(), 
					null, participant.getIdParticipantPk());
		}

    	return true;
		
	}
	
	/**
	 * Gets the holder by document type and document number service facade.
	 *
	 * @param holder the holder
	 * @return the holder by document type and document number service facade
	 * @throws ServiceException the service exception
	 */
	public Holder getHolderByDocumentTypeAndDocumentNumber(Holder holder) throws ServiceException{
		return participantMgmtServiceBean.getHolderByDocumentTypeAndDocumentNumberServiceBean(holder);
	}

	/**
	 * Gets the list legal representative hist by participant request service facade.
	 *
	 * @param participantRequest the participant request
	 * @param registryTypeLegal the registry type legal
	 * @return the list legal representative hist by participant request service facade
	 * @throws ServiceException the service exception
	 */
	public List<LegalRepresentativeHistory> listLegalRepresentHistByParticipantRequest(ParticipantRequest participantRequest, Integer registryTypeLegal) throws ServiceException{
		return participantMgmtServiceBean.listLegalRepresentHistByParticipantRequest(participantRequest, registryTypeLegal);
	}

	/**
	 * Gets the list participant mechanism histories by participant service facade.
	 *
	 * @param participantMechanismTO the participant mechanism to
	 * @return the list participant mechanism histories by participant service facade
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantMechanismHistory> getListParticipantMechanismHistoriesByParticipantServiceFacade(ParticipantMechanismTO participantMechanismTO) throws ServiceException {
		return participantMgmtServiceBean.getListParticipantMechanismHistoriesByParticipantServiceBean(participantMechanismTO);
	}

	/**
	 * Gets the list participant int depo history by participant service facade.
	 *
	 * @param participantIntDepositoryTO the participant int depository to
	 * @return the list participant int depo history by participant service facade
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantIntDepoHistory> getListParticipantIntDepoHistoryByParticipantServiceFacade(ParticipantIntDepositoryTO participantIntDepositoryTO) throws ServiceException{
		return participantMgmtServiceBean.getListParticipantIntDepoHistoryByParticipantServiceBean(participantIntDepositoryTO);
	}

	/**
	 * Reject participant modification request service facade.
	 *
	 * @param participantRequest the participant request
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MODIFICATION_PARTICIPANT_REJECT)
	public ParticipantRequest rejectParticipantModificationRequestServiceFacade(ParticipantRequest participantRequest) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
        
        participantRequest.setRejectedDate(CommonsUtilities.currentDateTime());
        participantRequest.setRejectedUser(loggerUser.getUserName());
		participantRequest.setStateRequest(ParticipantRequestStateType.REJECTED.getCode());
        
		return crudDaoServiceBean.update(participantRequest);
	}

	/**
	 * Confirm participant modification request service facade.
	 *
	 * @param participantRequest the participant request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.MODIFICATION_PARTICIPANT_CONFIRM)
	public boolean confirmParticipantModificationRequestServiceFacade(ParticipantRequest participantRequest) throws ServiceException{

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());                             
        
        
		ParticipantRequestTO participantRequestTO = new ParticipantRequestTO();
		participantRequestTO.setIdParticipantRequestPk(participantRequest.getIdParticipantRequestPk());
		boolean indCreateHolder = participantRequest.isIndHolderWillBeCreated();
		List<HolderFile> lstHolderFiles = participantRequest.getHolderFiles();
		participantRequest = findParticipantRequestModificationById(participantRequestTO);									
		
		participantRequestTO.setIndNewData(BooleanType.YES.getCode());
		ParticipantRequestHistory participantRequestHistory = findParticipantHistoryByRequest(participantRequestTO);
		
		Participant participant = participantRequest.getParticipant();		
		
		List<LegalRepresentativeHistory> lstLegalRepresentativeHistories = listLegalRepresentHistByParticipantRequest(participantRequest,
																			RegistryType.HISTORICAL_COPY.getCode());
		
		List<LegalRepresentative> lstLegalRepresentativeFound = new ArrayList<LegalRepresentative>();
        LegalRepresentative legalRepresentativeFound;
        
        if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistories)){
	        for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistories) {
	        		legalRepresentativeFound = new LegalRepresentative();
	            	legalRepresentativeFound.setDocumentType(legalRepresentativeHistory.getDocumentType());
	            	legalRepresentativeFound.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
	            	legalRepresentativeFound = getLegalRepresentativeByDocumentTypeAndNumber(legalRepresentativeFound);
	            	
	            	if(Validations.validateIsNotNull(legalRepresentativeFound)){
	            		lstLegalRepresentativeFound.add(legalRepresentativeFound);
	            	}	        	
			}
        }
        
        List<ParticipantIntDepositary> lstParticipantDepositariesToCreate = new ArrayList<ParticipantIntDepositary>();
        List<ParticipantIntDepositary> lstParticipantDepositariesToUpdate = new ArrayList<ParticipantIntDepositary>();
        
        //Get the Participant Int Depositary Histories from the Participant Request
        List<ParticipantIntDepoHistory> participantIntDepoHistories = listParticipantIntDepoHistoryByRequest(participantRequest.getIdParticipantRequestPk(), BooleanType.YES.getCode());
        
        if(Validations.validateListIsNotNullAndNotEmpty(participantIntDepoHistories)) {
        	ParticipantIntDepositary participantIntDepositaryFound;
            for (ParticipantIntDepoHistory participantIntDepoHistory : participantIntDepoHistories) {        	
            	
            	participantIntDepositaryFound = new ParticipantIntDepositary();
            	participantIntDepositaryFound.setParticipant(participant);
            	participantIntDepositaryFound.setInternationalDepository(participantIntDepoHistory.getInternationalDepository());
            	participantIntDepositaryFound = findParticipantIntDepoByParticipantAndDepoServiceFacade(participantIntDepositaryFound);
            	
            	if(Validations.validateIsNotNull(participantIntDepositaryFound)) {
            		if(ParticipantIntDepositaryStateType.DELETED.getCode().equals(participantIntDepositaryFound.getStateIntDepositary())) {
            			participantIntDepositaryFound.setParticipant(participant);
                    	participantIntDepositaryFound.setInternationalDepository(participantIntDepoHistory.getInternationalDepository());
            			participantIntDepositaryFound.setStateIntDepositary(ParticipantIntDepositaryStateType.REGISTERED.getCode());
            			lstParticipantDepositariesToUpdate.add(participantIntDepositaryFound);
            		}
            	} else {
            		participantIntDepositaryFound = new ParticipantIntDepositary();
                	participantIntDepositaryFound.setParticipant(participant);
                	participantIntDepositaryFound.setInternationalDepository(participantIntDepoHistory.getInternationalDepository());
                	participantIntDepositaryFound.setStateIntDepositary(ParticipantIntDepositaryStateType.REGISTERED.getCode());
                	lstParticipantDepositariesToCreate.add(participantIntDepositaryFound);
            	}
    		}
        }
        
        
        
        //Get the list of participant mechanisms histories of participant request
        List<ParticipantMechanismHistory> participantMechanismHistories = 
        		listParticipantMechHistoryByRequest(participantRequest.getIdParticipantRequestPk(), BooleanType.YES.getCode());
      
		//Disable files of participant
		disableParticipantFilesStateServiceFacade(participant,loggerUser);
		
		//Disable Represented Entities associated to participant
		disableRepresentedEntitiesStateServiceFacade(participant,loggerUser);
		
		//Disable Participant Int Depositaries
		if(Validations.validateListIsNotNullAndNotEmpty(participantIntDepoHistories)) {
			disableParticipantIntDepositariesNotInListServiceFacade(participant,participantIntDepoHistories,loggerUser);
		}
		
		//Disable Participant Mechanisms
		disableParticipantMechanismsStateServiceFacade(participant,loggerUser);
		
		participant.setMnemonic(participantRequestHistory.getMnemonic());
		participant.setDescription(participantRequestHistory.getDescription());
		participant.setDocumentType(participantRequestHistory.getDocumentType());
		participant.setDocumentNumber(participantRequestHistory.getDocumentNumber());
		participant.setAccountType(participantRequestHistory.getAccountType());
		participant.setAccountClass(participantRequestHistory.getAccountClass());
		participant.setContractNumber(participantRequestHistory.getContractNumber());
		participant.setContractDate(participantRequestHistory.getContractDate());
		participant.setCommercialRegister(participantRequestHistory.getCommercialRegister());
		participant.setSuperintendentResolution(participantRequestHistory.getSuperintendentResolution());
		participant.setSuperintendentResolutionDate(participantRequestHistory.getSuperintendentResolutionDate());
		participant.setSocialCapital(participantRequestHistory.getSocialCapital());
		participant.setResidenceCountry(participantRequestHistory.getResidenceCountry());
		participant.setDepartment(participantRequestHistory.getDepartment());
		participant.setProvince(participantRequestHistory.getProvince());
		participant.setDistrict(participantRequestHistory.getDistrict());
		participant.setAddress(participantRequestHistory.getAddress());
		participant.setPhoneNumber(participantRequestHistory.getPhoneNumber());
		participant.setFaxNumber(participantRequestHistory.getFaxNumber());
		participant.setWebsite(participantRequestHistory.getWebsite());
		participant.setEmail(participantRequestHistory.getEmail());
		participant.setContactPhone(participantRequestHistory.getContactPhone());
		participant.setContactName(participantRequestHistory.getContactName());
		participant.setComments(participantRequestHistory.getComments());
		participant.setState(participantRequestHistory.getParticipantState());
		participant.setCreationDate(participantRequestHistory.getCreationDate());		
		participant.setEconomicSector(participantRequestHistory.getEconomicSector());		
		participant.setEconomicActivity(participantRequestHistory.getEconomicActivity());
		participant.setInvestorType(participantRequestHistory.getInvestorType());
		participant.setCurrency(participantRequestHistory.getCurrency());
		participant.setContactEmail(participantRequestHistory.getContactEmail());
		participant.setRoleParticipant(participantRequestHistory.getRoleParticipant());
		participant.setIndPayCommission(participantRequestHistory.getIndPayCommission());
		participant.setIndDepositary(participantRequestHistory.getIndDepositary());
		participant.setIndManagedThird(participantRequestHistory.getIndManagedThird());
		participant.setBicCode(participantRequestHistory.getBicCode());
		participant.setIndSettlementIncharge(participantRequestHistory.getIndSettlementIncharge());
		participant.setIdBcbCode(participantRequestHistory.getIdBcbCode());
		participant.setAccountsState(participantRequestHistory.getAccountsState());
		if(Validations.validateIsNotNullAndNotEmpty(participantRequestHistory.getIssuerFk())){
			Issuer issuer = accountServiceBean.find(Issuer.class,participantRequestHistory.getIssuerFk());
			participant.setIssuer(issuer);
		}else
			participant.setIssuer(null);
		
		List<ParticipantFileHistory> lstParticipantFileHistory = 
					participantMgmtServiceBean.listParticipantFileHistoriesByRequest(participantRequest.getIdParticipantRequestPk(), BooleanType.YES.getCode());
		List<ParticipantFile> participantFiles = new ArrayList<ParticipantFile>();
		if(Validations.validateIsNotNullAndNotEmpty(lstParticipantFileHistory)){

			ParticipantFile participantFile;
			for (ParticipantFileHistory participantFileHistory : lstParticipantFileHistory) {
				participantFile = new ParticipantFile();
				
				participantFile.setDocumentType(participantFileHistory.getDocumentType());				
				participantFile.setDescription(participantFileHistory.getDescription());
				participantFile.setFilename(participantFileHistory.getFilename());
				participantFile.setFile(participantFileHistory.getFile());

				participantFile.setRegistryUser(loggerUser.getUserName());
				participantFile.setRegistryDate(CommonsUtilities.currentDateTime());
				
				participantFile.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());		    	
				participantFile.setParticipant(participant);
				participantFile.setStateFile(ParticipantFileStateType.REGISTERED.getCode());
		    	
				participantFiles.add(participantFile);
			}
			
		}
		
		participantMgmtServiceBean.updateParticipantInformation(participant, loggerUser);
		
		for (ParticipantFile objParticipantFile : participantFiles) {
			crudDaoServiceBean.create(objParticipantFile);
		}
		
		List<InstitutionBankAccountHistory> lstInstitutionBankAccountHistory = 
					participantMgmtServiceBean.listInstitutionBankAccountHistoriesByRequest(participantRequest.getIdParticipantRequestPk(), BooleanType.YES.getCode());
		List<InstitutionBankAccount> institutionBankAccounts = new ArrayList<InstitutionBankAccount>();
		if(Validations.validateIsNotNullAndNotEmpty(lstInstitutionBankAccountHistory)){
	
			InstitutionBankAccount institutionBankAccount;
			for (InstitutionBankAccountHistory institutionBankAccountHistory : lstInstitutionBankAccountHistory) {
				if(Validations.validateIsNotNullAndNotEmpty(institutionBankAccountHistory.getIdInstitutionBankAccountFk())) {
					institutionBankAccount = new InstitutionBankAccount();
					
					institutionBankAccount.setIdInstitutionBankAccountPk(institutionBankAccountHistory.getIdInstitutionBankAccountFk());
					institutionBankAccount.setProviderBank(institutionBankAccountHistory.getProviderBank());				
					institutionBankAccount.setBankAcountType(institutionBankAccountHistory.getBankAcountType());
					institutionBankAccount.setCurrency(institutionBankAccountHistory.getCurrency());
					institutionBankAccount.setAccountNumber(institutionBankAccountHistory.getAccountNumber());
		
					institutionBankAccount.setRegistryUser(loggerUser.getUserName());
					institutionBankAccount.setRegistryDate(CommonsUtilities.currentDateTime());
							    	
					institutionBankAccount.setParticipant(participant);
					institutionBankAccount.setState(BankAccountsStateType.REGISTERED.getCode());
					crudDaoServiceBean.update(institutionBankAccount);
				}else {
					institutionBankAccount = new InstitutionBankAccount();
					
					institutionBankAccount.setProviderBank(institutionBankAccountHistory.getProviderBank());				
					institutionBankAccount.setBankAcountType(institutionBankAccountHistory.getBankAcountType());
					institutionBankAccount.setCurrency(institutionBankAccountHistory.getCurrency());
					institutionBankAccount.setAccountNumber(institutionBankAccountHistory.getAccountNumber());
		
					institutionBankAccount.setRegistryUser(loggerUser.getUserName());
					institutionBankAccount.setRegistryDate(CommonsUtilities.currentDateTime());
							    	
					institutionBankAccount.setParticipant(participant);
					institutionBankAccount.setState(BankAccountsStateType.REGISTERED.getCode());
					
					InstitutionBankAccount institutionBankAccountNew = crudDaoServiceBean.create(institutionBankAccount);
					InstitutionCashAccount institutionCashAccountNew = registryCashAccountFacade(institutionBankAccountNew);
    				registryCashAccountDetailFacade(institutionBankAccountNew,institutionCashAccountNew);
				}
			}
			
		}
		
		participantMgmtServiceBean.updateParticipantInformation(participant, loggerUser);
		
		
		List<RepresentedEntity> lstRepresentedEntity = new ArrayList<RepresentedEntity>();
		RepresentedEntity representedEntity = null;			
		LegalRepresentative objLegalRepresentative = null;
		PepPerson pepPerson;
		GeographicLocation countryPep;
		GeographicLocation expeditionPlacePep;
		boolean indLegalRepresentativeFoumd;
		if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistories)){						
			
			for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistories) {
				
				indLegalRepresentativeFoumd = false;
				if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeFound)){
					for (LegalRepresentative legalRepresentativeAux : lstLegalRepresentativeFound) {
						if(legalRepresentativeAux.getDocumentType().equals(legalRepresentativeHistory.getDocumentType()) &&
								legalRepresentativeAux.getDocumentNumber().equals(legalRepresentativeHistory.getDocumentNumber())){
							indLegalRepresentativeFoumd = true;
							objLegalRepresentative = legalRepresentativeAux;
							break;
						}
					}
				}
				
				if(!indLegalRepresentativeFoumd) {
					objLegalRepresentative = new LegalRepresentative();
				}
				
				objLegalRepresentative.setBirthDate(legalRepresentativeHistory.getBirthDate());
				objLegalRepresentative.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
				objLegalRepresentative.setDocumentType(legalRepresentativeHistory.getDocumentType());
				objLegalRepresentative.setEconomicActivity(legalRepresentativeHistory.getEconomicActivity());
				objLegalRepresentative.setEconomicSector(legalRepresentativeHistory.getEconomicSector());
				objLegalRepresentative.setEmail(legalRepresentativeHistory.getEmail());
				objLegalRepresentative.setFaxNumber(legalRepresentativeHistory.getFaxNumber());
				objLegalRepresentative.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
				objLegalRepresentative.setFullName(legalRepresentativeHistory.getFullName());
				objLegalRepresentative.setHomePhoneNumber(legalRepresentativeHistory.getHomePhoneNumber());
				objLegalRepresentative.setIndResidence(legalRepresentativeHistory.getIndResidence());
				objLegalRepresentative.setLegalAddress(legalRepresentativeHistory.getLegalAddress());
				objLegalRepresentative.setLegalDistrict(legalRepresentativeHistory.getLegalDistrict());
				objLegalRepresentative.setLegalProvince(legalRepresentativeHistory.getLegalProvince());
				objLegalRepresentative.setLegalDepartment(legalRepresentativeHistory.getLegalDepartment());
				objLegalRepresentative.setLegalResidenceCountry(legalRepresentativeHistory.getLegalResidenceCountry());
				objLegalRepresentative.setMobileNumber(legalRepresentativeHistory.getMobileNumber());
				objLegalRepresentative.setName(legalRepresentativeHistory.getName());
				objLegalRepresentative.setNationality(legalRepresentativeHistory.getNationality());
				objLegalRepresentative.setOfficePhoneNumber(legalRepresentativeHistory.getOfficePhoneNumber());
				objLegalRepresentative.setPersonType(legalRepresentativeHistory.getPersonType());
				objLegalRepresentative.setPostalAddress(legalRepresentativeHistory.getPostalAddress());
				objLegalRepresentative.setPostalDepartment(legalRepresentativeHistory.getPostalDepartment());
				objLegalRepresentative.setPostalDistrict(legalRepresentativeHistory.getPostalDistrict());
				objLegalRepresentative.setPostalProvince(legalRepresentativeHistory.getPostalProvince());
				objLegalRepresentative.setPostalResidenceCountry(legalRepresentativeHistory.getPostalResidenceCountry());
				objLegalRepresentative.setSecondDocumentNumber(legalRepresentativeHistory.getSecondDocumentNumber());
				objLegalRepresentative.setSecondDocumentType(legalRepresentativeHistory.getSecondDocumentType());
				objLegalRepresentative.setSecondLastName(legalRepresentativeHistory.getSecondLastName());
				objLegalRepresentative.setSecondNationality(legalRepresentativeHistory.getSecondNationality());
				objLegalRepresentative.setSex(legalRepresentativeHistory.getSex());
				objLegalRepresentative.setState(LegalRepresentativeStateType.REGISTERED.getCode());
				if(!indLegalRepresentativeFoumd) {
					objLegalRepresentative.setRegistryUser(loggerUser.getUserName());
	    			objLegalRepresentative.setRegistryDate(CommonsUtilities.currentDateTime());
				}
				
				if(indLegalRepresentativeFoumd) {
					participantMgmtServiceBean.disableLegalRepresentativeFilesServiceBean(objLegalRepresentative,loggerUser);
				}
    			
    			if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
    				List<LegalRepresentativeFile> lstLegalRepresentativeFile = new ArrayList<LegalRepresentativeFile>();
    				LegalRepresentativeFile legalRepresentativeFile;
					for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeHistory.getRepresenteFileHistory()) {
						legalRepresentativeFile = new LegalRepresentativeFile();
						
						legalRepresentativeFile.setDescription(representativeFileHistory.getDescription());
						legalRepresentativeFile.setDocumentType(representativeFileHistory.getDocumentType());
						legalRepresentativeFile.setFilename(representativeFileHistory.getFilename());	
						legalRepresentativeFile.setFileLegalRepre(representativeFileHistory.getFileRepresentative());					
						legalRepresentativeFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
						legalRepresentativeFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
						legalRepresentativeFile.setLegalRepreFileType(HolderReqFileHisType.CREATION.getCode());
						legalRepresentativeFile.setLegalRepresentative(objLegalRepresentative);
						legalRepresentativeFile.setRegistryUser(loggerUser.getUserName());
						legalRepresentativeFile.setRegistryDate(CommonsUtilities.currentDateTime());
						
						lstLegalRepresentativeFile.add(legalRepresentativeFile);
						
					}
					objLegalRepresentative.setLegalRepresentativeFile(lstLegalRepresentativeFile);
				}
    			
    			pepPerson = null;
    			boolean indPepRepresentativeFound = false;
    			if(indLegalRepresentativeFoumd){
    				updateLegalRepresentative(objLegalRepresentative);
    				pepPerson = participantMgmtServiceBean.findPepPersonByIdLegalRepresentativeServiceFacade(objLegalRepresentative.getIdLegalRepresentativePk());
    				if(Validations.validateIsNotNull(pepPerson)){
    					indPepRepresentativeFound = true;
    				}    				
    			} else {
    				registryLegalRepresentative(objLegalRepresentative);
    			}    			
    			    			    			
    			if(BooleanType.YES.getCode().equals(legalRepresentativeHistory.getIndPEP())){
    				if(!indPepRepresentativeFound){
    					pepPerson = new PepPerson();
    				}
    				pepPerson.setFirstName(legalRepresentativeHistory.getName());
    				if(Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.getSecondLastName())) {
    					pepPerson.setLastName(legalRepresentativeHistory.getFirstLastName() + GeneralConstants.BLANK_SPACE + legalRepresentativeHistory.getSecondLastName());
    				} else {
    					pepPerson.setLastName(legalRepresentativeHistory.getFirstLastName());
    				}
    				pepPerson.setBeginingPeriod(legalRepresentativeHistory.getBeginningPeriod());
    				pepPerson.setCategory(legalRepresentativeHistory.getCategory());
    				pepPerson.setComments(legalRepresentativeHistory.getComments());
    				pepPerson.setEndingPeriod(legalRepresentativeHistory.getEndingPeriod());
    				pepPerson.setBeginingPeriod(legalRepresentativeHistory.getBeginningPeriod());
    				pepPerson.setRole(legalRepresentativeHistory.getRole());
    				pepPerson.setLegalRepresentative(objLegalRepresentative);
    				pepPerson.setRegistryUser(loggerUser.getUserName());
    				pepPerson.setRegistryDate(CommonsUtilities.currentDateTime());
    				pepPerson.setNotificationDate(CommonsUtilities.currentDateTime());
    				countryPep = new GeographicLocation();
    				countryPep.setIdGeographicLocationPk(objLegalRepresentative.getLegalResidenceCountry());		    			
	    		    pepPerson.setGeographicLocation(countryPep);
	    		    pepPerson.setDocumentType(legalRepresentativeHistory.getDocumentType());
	    		    pepPerson.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
	    		    pepPerson.setClassification(PepPersonClassificationType.HIGH.getCode());
	    		    expeditionPlacePep = new GeographicLocation();
	    		    expeditionPlacePep.setIdGeographicLocationPk(objLegalRepresentative.getLegalResidenceCountry());
	    		    pepPerson.setExpeditionPlace(expeditionPlacePep);
	    		    
	    		    if(!indPepRepresentativeFound){
	    		    	crudDaoServiceBean.create(pepPerson);
	    		    } else {
	    		    	participantMgmtServiceBean.updatePepPersonInformation(pepPerson, loggerUser);
	    		    }
    			}
				
				representedEntity = new RepresentedEntity();
				representedEntity.setRegistryUser(loggerUser.getUserName());
				representedEntity.setRegistryDate(CommonsUtilities.currentDateTime());
    			representedEntity.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
    			representedEntity.setParticipant(participant);    			
    			representedEntity.setLegalRepresentative(objLegalRepresentative);
    			representedEntity.setRepresentativeClass(legalRepresentativeHistory.getRepresentativeClass());
    			crudDaoServiceBean.create(representedEntity);
    			lstRepresentedEntity.add(representedEntity);
			}
		}
		
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantDepositariesToCreate)){
			for (ParticipantIntDepositary participantIntDepoHistoryCreate : lstParticipantDepositariesToCreate) {
				participantIntDepoHistoryCreate.setRegistryUser(loggerUser.getUserName());
				participantIntDepoHistoryCreate.setRegistryDate(CommonsUtilities.currentDateTime());
				crudDaoServiceBean.create(participantIntDepoHistoryCreate);
			}
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantDepositariesToUpdate)){
			for (ParticipantIntDepositary participantIntDepoHistoryUpdate : lstParticipantDepositariesToUpdate) {				
				participantMgmtServiceBean.updateStateParticipantIntDepositaryServiceBean(participantIntDepoHistoryUpdate,loggerUser);
			}
		}
		
		ParticipantMechanism participantMechanism;
		for (ParticipantMechanismHistory participantMechanismHistory : participantMechanismHistories) {
			participantMechanism = new ParticipantMechanism();
			participantMechanism.setParticipant(participant);
			participantMechanism.setMechanismModality(participantMechanismHistory.getMechanismModality());
			participantMechanism.setStateParticipantMechanism(ParticipantMechanismStateType.REGISTERED.getCode());
			participantMechanism.setRegistryUser(loggerUser.getUserName());
			participantMechanism.setRegistryDate(CommonsUtilities.currentDateTime());
			participantMechanism.setIndPurchaseRole(BooleanType.YES.getCode());
			participantMechanism.setIndSaleRole(BooleanType.YES.getCode());
			crudDaoServiceBean.create(participantMechanism);
		}
		
		participantRequest.setParticipantFiles(null);
		participantRequest.setConfirmationDate(CommonsUtilities.currentDateTime());
		participantRequest.setConfirmationUser(loggerUser.getUserName());
		participantRequest.setStateRequest(ParticipantRequestStateType.CONFIRMED.getCode());		

		participantMgmtServiceBean.updateStateParticipantRequestServiceBean(participantRequest, loggerUser);		
		
		if(indCreateHolder){
			//Validate if holder exists with 
			Holder holderFilter = new Holder();
			holderFilter.setDocumentType(participant.getDocumentType());
			holderFilter.setDocumentNumber(participant.getDocumentNumber());			
			
			Holder registeredHolder = accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(holderFilter);
		
			if(registeredHolder==null){
				registeredHolder = saveHolderRegistration(participant, lstLegalRepresentativeHistories, lstHolderFiles, lstRepresentedEntity , loggerUser);
			}
			participantMgmtServiceBean.updateHolderToParticipant(participant, registeredHolder);
			
		}
		
		this.serviceAPIClients.callWsUpdateInstitutionSecurity(participant.getDescription(), participant.getMnemonic(), 
				InstitutionType.PARTICIPANT.getCode(), null, participant.getIdParticipantPk(), 
				participant.getIndDepositary().equals(BooleanType.YES.getCode())? InstitutionStateType.REGISTERED.getCode(): InstitutionStateType.DELETED.getCode());
		
		return true;
	}
	
	/**
	 * Save holder registration.
	 *
	 * @param objParticipant the obj participant
	 * @param legalRepresentativeHistories the legal representative histories
	 * @param lstHolderFiles the lst holder files
	 * @param lstRepresentedEntities the lst represented entities
	 * @param loggerUser the logger user
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder saveHolderRegistration(Participant objParticipant, List<LegalRepresentativeHistory> legalRepresentativeHistories, 
			List<HolderFile> lstHolderFiles, List<RepresentedEntity> lstRepresentedEntities, LoggerUser loggerUser) throws ServiceException{
	    	
	    	HolderRequest holderRequest = new HolderRequest();
	    	holderRequest.setStateHolderRequest(HolderRequestStateType.REGISTERED.getCode());
	    	holderRequest.setRequestType(HolderRequestType.CREATION.getCode());
	    	holderRequest.setActionDate(CommonsUtilities.currentDateTime());    	
			holderRequest.setRegistryDate(CommonsUtilities.currentDateTime());
			holderRequest.setRegistryUser(loggerUser.getUserName());
			holderRequest.setApproveDate(CommonsUtilities.currentDateTime());
			holderRequest.setApproveUser(loggerUser.getUserName());	
			holderRequest.setConfirmDate(CommonsUtilities.currentDateTime());
			holderRequest.setConfirmUser(loggerUser.getUserName());
			holderRequest.setParticipant(objParticipant);
			
	    	
	    	HolderHistory holderHistory = new HolderHistory();	    	
	    	holderHistory.setIndMinor(BooleanType.NO.getCode());
	    	holderHistory.setIndDisabled(BooleanType.NO.getCode());
	    	holderHistory.setIndPEP(BooleanType.NO.getCode());	    	    	
	    	holderHistory.setDocumentType(objParticipant.getDocumentType());	    	
	    	holderHistory.setDocumentNumber(objParticipant.getDocumentNumber());
	    	holderHistory.setEconomicActivity(objParticipant.getEconomicActivity());
	    	holderHistory.setEconomicSector(objParticipant.getEconomicSector());
	    	holderHistory.setEmail(objParticipant.getEmail());
	    	holderHistory.setFaxNumber(objParticipant.getFaxNumber());
	    	holderHistory.setFullName(objParticipant.getDescription());
	    	holderHistory.setHolderType(PersonType.JURIDIC.getCode());
	    	holderHistory.setHomePhoneNumber(objParticipant.getPhoneNumber());
	    	holderHistory.setOfficePhoneNumber(objParticipant.getPhoneNumber());
	    	holderHistory.setJuridicClass(JuridicClassType.NORMAL.getCode());
	    	holderHistory.setLegalAddress(objParticipant.getAddress());
	    	holderHistory.setLegalDepartment(objParticipant.getDepartment());
	    	holderHistory.setLegalDistrict(objParticipant.getDistrict());
	    	holderHistory.setLegalProvince(objParticipant.getProvince());
	    	holderHistory.setLegalResidenceCountry(objParticipant.getResidenceCountry());	    		    	
	    	holderHistory.setNationality(objParticipant.getResidenceCountry());
	    	holderHistory.setPostalAddress(objParticipant.getAddress());
	    	holderHistory.setPostalDepartment(objParticipant.getDepartment());
	    	holderHistory.setPostalDistrict(objParticipant.getDistrict());
			holderHistory.setPostalProvince(objParticipant.getProvince());
			holderHistory.setPostalResidenceCountry(objParticipant.getResidenceCountry());
			holderHistory.setIndResidence(countryResidence.equals(objParticipant.getResidenceCountry()) ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			holderHistory.setStateHolderHistory(HolderRequestStateType.CONFIRMED.getCode());			
			holderHistory.setRegistryDate(CommonsUtilities.currentDateTime());
			holderHistory.setRegistryUser(loggerUser.getUserName());
			holderHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			holderHistory.setHolderRequest(holderRequest);			
			
			List<HolderHistory> holderHistories = new ArrayList<HolderHistory>();
			holderHistories.add(holderHistory);
			
			holderRequest.setHolderHistories(holderHistories);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderFiles)){
				List<HolderReqFileHistory> holderReqFileHistories = new ArrayList<HolderReqFileHistory>();
				for (HolderFile holderFile : lstHolderFiles) {
					HolderReqFileHistory holderReqFileHistory = new HolderReqFileHistory();

					holderReqFileHistory.setHolderReqFileHisType(HolderReqFileHisType.CREATION.getCode());
					holderReqFileHistory.setFilename(holderFile.getFilename());
					holderReqFileHistory.setFileHolderReq(holderFile.getFileHolder());
					holderReqFileHistory.setRegistryUser(loggerUser.getUserName());
					holderReqFileHistory.setRegistryDate(CommonsUtilities.currentDate());
					holderReqFileHistory.setHolderRequest(holderRequest);
					holderReqFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					holderReqFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
					holderReqFileHistory.setDocumentType(holderFile.getDocumentType());
					holderReqFileHistory.setDescription(holderFile.getDescription());
					
					holderReqFileHistories.add(holderReqFileHistory);
				}
				
				holderRequest.setHolderReqFileHistories(holderReqFileHistories);
			}			
			
			holderRequest.setLegalRepresentativeHistories(copyAllLegalRepresentativeHistory(legalRepresentativeHistories,holderRequest));
			
			//Register (and approve at the same time) holder creation request
			registerHolderRequestServiceFacade(holderRequest);
			
			
			Holder holder = new Holder();	    		    	
	    	holder.setIndMinor(BooleanType.NO.getCode());	    	
	    	holder.setIndDisabled(BooleanType.NO.getCode());	    	
	    	//Set the document type Parameter of Holder instead of document tipe parameter of Participant
	    	holder.setDocumentType(objParticipant.getDocumentType());
	    	holder.setDocumentNumber(objParticipant.getDocumentNumber());
	    	holder.setEconomicActivity(objParticipant.getEconomicActivity());
	    	holder.setEconomicSector(objParticipant.getEconomicSector());
	    	holder.setEmail(objParticipant.getEmail());
	    	holder.setFaxNumber(objParticipant.getFaxNumber());
	    	holder.setFullName(objParticipant.getDescription());
	    	holder.setHolderType(PersonType.JURIDIC.getCode());
	    	holder.setHomePhoneNumber(objParticipant.getPhoneNumber());
	    	holder.setOfficePhoneNumber(objParticipant.getPhoneNumber());
	    	holder.setJuridicClass(JuridicClassType.NORMAL.getCode());
	    	holder.setLegalAddress(objParticipant.getAddress());
	    	holder.setLegalDepartment(objParticipant.getDepartment());
	    	holder.setLegalDistrict(objParticipant.getDistrict());
	    	holder.setLegalProvince(objParticipant.getProvince());
	    	holder.setLegalResidenceCountry(objParticipant.getResidenceCountry());	    		    	
	    	holder.setNationality(objParticipant.getResidenceCountry());
	    	holder.setPostalAddress(objParticipant.getAddress());
	    	holder.setPostalDepartment(objParticipant.getDepartment());
	    	holder.setPostalDistrict(objParticipant.getDistrict());
	    	holder.setPostalProvince(objParticipant.getProvince());
	    	holder.setPostalResidenceCountry(objParticipant.getResidenceCountry());
	    	holder.setIndResidence(countryResidence.equals(objParticipant.getResidenceCountry()) ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
	    	holder.setStateHolder(HolderStateType.REGISTERED.getCode());    	
	    	holder.setRegistryDate(CommonsUtilities.currentDateTime());
	    	holder.setRegistryUser(loggerUser.getUserName());
	    	holder.setParticipant(objParticipant);
	    	
	    	if(Validations.validateListIsNotNullAndNotEmpty(lstHolderFiles)){	    		
	    		for (HolderFile holderFile : lstHolderFiles) {
					holderFile.setHolderFileType(HolderReqFileHisType.CREATION.getCode());
					holderFile.setRegistryUser(loggerUser.getUserName());
					holderFile.setRegistryDate(CommonsUtilities.currentDate());
					holderFile.setHolder(holder);
					holderFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
					holderFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
				}
	    		holder.setHolderFile(lstHolderFiles);
	    	}
	    	
	    	if(Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntities)){
	    		List<RepresentedEntity> representedEntities = new ArrayList<RepresentedEntity>();
	    		RepresentedEntity representedEntityCopy = null;
	    		for (RepresentedEntity representedEntityOriginal : lstRepresentedEntities) {
	    			representedEntityCopy = new RepresentedEntity();	    			
	    			representedEntityCopy.setRegistryUser(loggerUser.getUserName());
					representedEntityCopy.setRegistryDate(CommonsUtilities.currentDateTime());
					representedEntityCopy.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
					representedEntityCopy.setHolder(holder);    			
					representedEntityCopy.setLegalRepresentative(representedEntityOriginal.getLegalRepresentative());
					representedEntityCopy.setRepresentativeClass(representedEntityOriginal.getRepresentativeClass());
					representedEntities.add(representedEntityCopy);
				}
	    		holder.setRepresentedEntity(representedEntities);
	    	}
			
			crudDaoServiceBean.create(holder);  
			
			holderRequest.setHolder(holder);        	
        	List<HolderHistoryState> lstHolderHistoryState = new ArrayList<HolderHistoryState>();
			HolderHistoryState holderHistoryState = new HolderHistoryState();
			
			holderHistoryState.setHolder(holder);
			holderHistoryState.setHolderRequest(holderRequest);
			holderHistoryState.setRegistryUser(loggerUser.getUserName());
			holderHistoryState.setHolderRequest(holderRequest);
			holderHistoryState.setOldState(StateHistoryStateHolderType.REGISTERED.getCode());
			holderHistoryState.setNewState(StateHistoryStateHolderType.REGISTERED.getCode());
			holderHistoryState.setUpdateStateDate(CommonsUtilities.currentDateTime());
			lstHolderHistoryState.add(holderHistoryState);				
			
			holderRequest.setHolderHistoryStates(lstHolderHistoryState);
        	
        	updateHolderRequestServiceFacade(holderRequest);
			
			return holder;
	}
	
	/**
	 * Copy all legal representative history.
	 *
	 * @param lstLegalRepresentativeHistOriginal the lst legal representative hist original
	 * @param holderRequest the holder request
	 * @return the list
	 */
	public List<LegalRepresentativeHistory> copyAllLegalRepresentativeHistory(List<LegalRepresentativeHistory> lstLegalRepresentativeHistOriginal,
			HolderRequest holderRequest){
		List<LegalRepresentativeHistory> lstLegalRepresentativeHistCopy = new ArrayList<LegalRepresentativeHistory>();
		LegalRepresentativeHistory legalRepHistCopy = null;
		List<RepresentativeFileHistory> lstRepreFileHistoryCopy = null;
		RepresentativeFileHistory repreFileHistoryCopy = null;
		
		for (LegalRepresentativeHistory legalRepHistOriginal : lstLegalRepresentativeHistOriginal) {
			legalRepHistCopy = new LegalRepresentativeHistory();
						
			legalRepHistCopy.setRepresentativeClass(legalRepHistOriginal.getRepresentativeClass());
			legalRepHistCopy.setBirthDate(legalRepHistOriginal.getBirthDate());
			legalRepHistCopy.setDocumentNumber(legalRepHistOriginal.getDocumentNumber());
			legalRepHistCopy.setDocumentType(legalRepHistOriginal.getDocumentType());
			legalRepHistCopy.setEconomicActivity(legalRepHistOriginal.getEconomicActivity());
			legalRepHistCopy.setEconomicSector(legalRepHistOriginal.getEconomicSector());
			legalRepHistCopy.setEmail(legalRepHistOriginal.getEmail());
			legalRepHistCopy.setFaxNumber(legalRepHistOriginal.getFaxNumber());
			legalRepHistCopy.setFirstLastName(legalRepHistOriginal.getFirstLastName());
			legalRepHistCopy.setFullName(legalRepHistOriginal.getFullName());
			legalRepHistCopy.setHomePhoneNumber(legalRepHistOriginal.getHomePhoneNumber());
			legalRepHistCopy.setIndResidence(legalRepHistOriginal.getIndResidence());
			legalRepHistCopy.setLegalAddress(legalRepHistOriginal.getLegalAddress());
			legalRepHistCopy.setLegalDistrict(legalRepHistOriginal.getLegalDistrict());
			legalRepHistCopy.setLegalProvince(legalRepHistOriginal.getLegalProvince());
			legalRepHistCopy.setLegalDepartment(legalRepHistOriginal.getLegalDepartment());
			legalRepHistCopy.setLegalResidenceCountry(legalRepHistOriginal.getLegalResidenceCountry());
			legalRepHistCopy.setMobileNumber(legalRepHistOriginal.getMobileNumber());
			legalRepHistCopy.setName(legalRepHistOriginal.getName());
			legalRepHistCopy.setNationality(legalRepHistOriginal.getNationality());
			legalRepHistCopy.setOfficePhoneNumber(legalRepHistOriginal.getOfficePhoneNumber());
			legalRepHistCopy.setPersonType(legalRepHistOriginal.getPersonType());
			legalRepHistCopy.setPostalAddress(legalRepHistOriginal.getPostalAddress());
			legalRepHistCopy.setPostalDepartment(legalRepHistOriginal.getPostalDepartment());
			legalRepHistCopy.setPostalDistrict(legalRepHistOriginal.getPostalDistrict());
			legalRepHistCopy.setPostalProvince(legalRepHistOriginal.getPostalProvince());
			legalRepHistCopy.setPostalResidenceCountry(legalRepHistOriginal.getPostalResidenceCountry());
			legalRepHistCopy.setSecondDocumentNumber(legalRepHistOriginal.getSecondDocumentNumber());
			legalRepHistCopy.setSecondDocumentType(legalRepHistOriginal.getSecondDocumentType());
			legalRepHistCopy.setSecondLastName(legalRepHistOriginal.getSecondLastName());
			legalRepHistCopy.setSecondNationality(legalRepHistOriginal.getSecondNationality());
			legalRepHistCopy.setSex(legalRepHistOriginal.getSex());
			legalRepHistCopy.setStateRepreHistory(legalRepHistOriginal.getStateRepreHistory());						
			legalRepHistCopy.setIndPEP(legalRepHistOriginal.getIndPEP());
			legalRepHistCopy.setCategory(legalRepHistOriginal.getCategory());
			legalRepHistCopy.setRole(legalRepHistOriginal.getRole());
			legalRepHistCopy.setBeginningPeriod(legalRepHistOriginal.getBeginningPeriod());
			legalRepHistCopy.setEndingPeriod(legalRepHistOriginal.getEndingPeriod());
			legalRepHistCopy.setRegistryUser(legalRepHistOriginal.getRegistryUser());
			legalRepHistCopy.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
			legalRepHistCopy.setRepreHistoryType(HolderRequestType.CREATION.getCode());
			legalRepHistCopy.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			legalRepHistCopy.setHolderRequest(holderRequest);
			
			if(Validations.validateListIsNotNullAndNotEmpty(legalRepHistOriginal.getRepresenteFileHistory())){
				
				lstRepreFileHistoryCopy = new ArrayList<RepresentativeFileHistory>();
				for (RepresentativeFileHistory repreFileHistoryOriginal : legalRepHistOriginal.getRepresenteFileHistory()) {
					repreFileHistoryCopy = new RepresentativeFileHistory();
					repreFileHistoryCopy.setDescription(repreFileHistoryOriginal.getDescription());
					repreFileHistoryCopy.setDocumentType(repreFileHistoryOriginal.getDocumentType());
					repreFileHistoryCopy.setFileRepresentative(repreFileHistoryOriginal.getFileRepresentative());
					repreFileHistoryCopy.setFilename(repreFileHistoryOriginal.getFilename());
					repreFileHistoryCopy.setRepresentFileHistType(repreFileHistoryOriginal.getRepresentFileHistType());
					repreFileHistoryCopy.setStateFile(repreFileHistoryOriginal.getStateFile());
					repreFileHistoryCopy.setLegalRepresentativeHistory(legalRepHistCopy);					
					
					lstRepreFileHistoryCopy.add(repreFileHistoryCopy);
				}
				
				legalRepHistCopy.setRepresenteFileHistory(lstRepreFileHistoryCopy);
			}
			
			lstLegalRepresentativeHistCopy.add(legalRepHistCopy);
		}
		
		return lstLegalRepresentativeHistCopy;
	}
	
	
	
	/**
	 * Disable participant mechanisms state service facade.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableParticipantMechanismsStateServiceFacade(
			Participant participant, LoggerUser loggerUser) throws ServiceException{
		return 	participantMgmtServiceBean.disableParticipantMechanismsStateServiceBean(participant,loggerUser);
	}

	/**
	 * Disable participant int depositaries not in list service facade.
	 *
	 * @param participant the participant
	 * @param participantIntDepoHistories the participant int depo histories
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableParticipantIntDepositariesNotInListServiceFacade(
			Participant participant,
			List<ParticipantIntDepoHistory> participantIntDepoHistories,
			LoggerUser loggerUser) throws ServiceException{
		return participantMgmtServiceBean.disableParticipantIntDepositariesNotInListServiceBean(participant,participantIntDepoHistories,loggerUser);
		
	}

	/**
	 * Disable participant files state service facade.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableParticipantFilesStateServiceFacade(Participant participant, LoggerUser loggerUser) throws ServiceException {
		return participantMgmtServiceBean.disableParticipantFilesStateServiceBean(participant, loggerUser);	
	}
	
	/**
	 * Disable represented entities state service facade.
	 *
	 * @param participant the participant
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableRepresentedEntitiesStateServiceFacade(Participant participant, LoggerUser loggerUser) throws ServiceException {
		return participantMgmtServiceBean.disableRepresentedEntitiesStateServiceBean(participant, loggerUser);	
	}

	/**
	 * Confirm participant block unblock annull service facade.
	 *
	 * @param participantRequest the participant request
	 * @param userNameLogged the user name logged
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean confirmParticipantAnnullment(ParticipantRequest participantRequest, String userNameLogged) throws ServiceException {

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		Participant participant = participantMgmtServiceBean.find(Participant.class, participantRequest.getParticipant().getIdParticipantPk());
		
		participantRequest = participantMgmtServiceBean.find(ParticipantRequest.class, participantRequest.getIdParticipantRequestPk());

		ParticipantHistoryState participantHistoryState = new ParticipantHistoryState();			
				
		participantHistoryState.setMotive(parameterServiceBean.getParameterTableById(participantRequest.getRequestMotive()).getParameterName());
		participantHistoryState.setParticipant(participant);
		participantHistoryState.setParticipantRequest(participantRequest);
		participantHistoryState.setUpdateStateDate(CommonsUtilities.currentDateTime());
		participantHistoryState.setOldState(participant.getState());
		
		participantHistoryState.setNewState(ParticipantStateType.ANNULLED.getCode());		
		participant.setState(ParticipantStateType.ANNULLED.getCode());
		
		participantMgmtServiceBean.updateStateParticipantServiceBean(participant, loggerUser);
				
		participantRequest.setConfirmationUser(userNameLogged);
		participantRequest.setConfirmationDate(CommonsUtilities.currentDateTime());
		participantRequest.setStateRequest(ParticipantRequestStateType.CONFIRMED.getCode());
		
		participantHistoryState.setRegistryUser(userNameLogged);

		participantMgmtServiceBean.updateStateParticipantRequestServiceBean(participantRequest, loggerUser);
		crudDaoServiceBean.create(participantHistoryState);
			
		return true;		
	}
	
	
	/**
	 * Confirm participant block unblock annull service facade.
	 *
	 * @param participantRequest the participant request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean confirmParticipantBlockUnblockAnnullServiceFacade(ParticipantRequest participantRequest) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		Participant participantFilter = new Participant();
		participantFilter.setIdParticipantPk(participantRequest.getParticipant().getIdParticipantPk());
		
		Participant participant = findParticipantByFilters(participantFilter);

		ParticipantHistoryState participantHistoryState = new ParticipantHistoryState();			
					
		participantHistoryState.setMotive(parameterServiceBean.getParameterTableById(participantRequest.getRequestMotive()).getParameterName());
		participantHistoryState.setParticipant(participant);
		participantHistoryState.setParticipantRequest(participantRequest);
		participantHistoryState.setUpdateStateDate(CommonsUtilities.currentDateTime());
		participantHistoryState.setOldState(participant.getState());
		
		if(ParticipantRequestType.BLOCK.getCode().equals(participantRequest.getRequestType())){
			participantHistoryState.setNewState(ParticipantStateType.BLOCKED.getCode());
			participant.setState(ParticipantStateType.BLOCKED.getCode());
		} else if(ParticipantRequestType.UNBLOCK.getCode().equals(participantRequest.getRequestType())){
			participantHistoryState.setNewState(ParticipantStateType.REGISTERED.getCode());
			participant.setState(ParticipantStateType.REGISTERED.getCode());
		} else if(ParticipantRequestType.ANNULMENT.getCode().equals(participantRequest.getRequestType())){
			participantHistoryState.setNewState(ParticipantStateType.ANNULLED.getCode());
			participant.setState(ParticipantStateType.ANNULLED.getCode());
		}
		
		participantMgmtServiceBean.updateStateParticipantServiceBean(participant, loggerUser);
				
		participantRequest.setConfirmationUser(loggerUser.getUserName());
		participantRequest.setConfirmationDate(CommonsUtilities.currentDateTime());
		participantRequest.setStateRequest(ParticipantRequestStateType.CONFIRMED.getCode());
		
		participantHistoryState.setRegistryUser(loggerUser.getUserName());

		participantMgmtServiceBean.updateStateParticipantRequestServiceBean(participantRequest, loggerUser);
		crudDaoServiceBean.create(participantHistoryState);
			
		return true;		
	}

	/**
	 * Reject participant block unblock annull service facade.
	 *
	 * @param participantRequest the participant request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean rejectParticipantBlockUnblockAnnullServiceFacade(ParticipantRequest participantRequest) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
        participantRequest.setRejectedUser(loggerUser.getUserName());
		participantRequest.setRejectedDate(CommonsUtilities.currentDateTime());
		participantRequest.setStateRequest(ParticipantRequestStateType.REJECTED.getCode());
		
		participantMgmtServiceBean.updateStateParticipantRequestServiceBean(participantRequest, loggerUser);		
        
		return true;
	}
	
	/**
	 * Register participant block unblock annull service facade.
	 *
	 * @param participantRequest the participant request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerParticipantBlockUnblockAnnullServiceFacade(ParticipantRequest participantRequest) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

        participantRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCK_UNBLOCK_ANNUL_PARTICIPANT));
		participantRequest.setRegistryUser(loggerUser.getUserName());
		participantRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		participantRequest.setStateRequest(ParticipantRequestStateType.REGISTERED.getCode());
		
		if(Validations.validateListIsNotNullAndNotEmpty(participantRequest.getParticipantFiles())){
			for (ParticipantFile participantFileSustent : participantRequest.getParticipantFiles()) {
				participantFileSustent.setRegistryUser(loggerUser.getUserName());
				participantFileSustent.setRegistryDate(CommonsUtilities.currentDateTime());
				participantFileSustent.setStateFile(ParticipantFileStateType.REGISTERED.getCode());
				participantFileSustent.setParticipantRequest(participantRequest);
				participantFileSustent.setParticipant(participantRequest.getParticipant());
			}
		}
		
		crudDaoServiceBean.create(participantRequest);
		
		return true;
	}


	/**
	 * Gets the list legal representative file hist service facade.
	 *
	 * @param legalRepresentativeHistory the legal representative history
	 * @return the list legal representative file hist service facade
	 * @throws ServiceException the service exception
	 */
	public List<RepresentativeFileHistory> getListLegalRepresentativeFileHist(
			LegalRepresentativeHistory legalRepresentativeHistory) throws ServiceException {
		return participantMgmtServiceBean.getListLegalRepresentativeFileHist(legalRepresentativeHistory);
	}

	/**
	 * Find pep person by legal representative service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the pep person
	 * @throws ServiceException the service exception
	 */
	public PepPerson findPepPersonByLegalRepresentativeServiceFacade(
			LegalRepresentative legalRepresentative) throws ServiceException{
		return participantMgmtServiceBean.findPepPersonByLegalRepresentative(legalRepresentative);
	}
	/**
	 * Gets the holder account with balance list component service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account with balance list component service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountWithBalanceListComponentServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		List<HolderAccount> holderAccountList = accountServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO);
		for(int i = 0; i< holderAccountList.size();++i){
			holderAccountList.get(i).getHolderAccountBalances().size();			
		}
		return holderAccountList;
	}
	
	/**
	 * Gets the legal representative by document type and document number service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the legal representative by document type and document number service facade
	 * @throws ServiceException the service exception
	 */
	public LegalRepresentative getLegalRepresentativeByDocumentTypeAndNumber(LegalRepresentative legalRepresentative) throws ServiceException {
    	return participantMgmtServiceBean.getLegalRepresentativeByDocumentTypeAndNumber(legalRepresentative);
     }
	
	/**
	 * Gets the list account balances by participant service facade.
	 *
	 * @param preHolderAccountTO the pre holder account to
	 * @return the list account balances by participant service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getListAccountBalancesByParticipantServiceFacade(HolderAccountTO preHolderAccountTO) throws ServiceException {
		return participantMgmtServiceBean.getListAccountBalancesByParticipantServiceBean(preHolderAccountTO);
	}

	/**
	 * Find participant request state by id service facade.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer findParticipantRequestStateByIdServiceFacade(Long idParticipantRequestPk) throws ServiceException{
		return participantMgmtServiceBean.findParticipantRequestStateByIdServiceBean(idParticipantRequestPk);
	}
	
	/**
	 * List participant int depo history by request service facade.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantIntDepoHistory> listParticipantIntDepoHistoryByRequest(
			Long idParticipantRequestPk, Integer indNew) throws ServiceException {
		return participantMgmtServiceBean.listParticipantIntDepoHistoryByRequest(idParticipantRequestPk, indNew);
	}
	
	/**
	 * Find participant int depo by participant and depo service facade.
	 *
	 * @param participantIntDepositaryFound the participant int depositary found
	 * @return the participant int depositary
	 * @throws ServiceException the service exception
	 */
	public ParticipantIntDepositary findParticipantIntDepoByParticipantAndDepoServiceFacade(
			ParticipantIntDepositary participantIntDepositaryFound) throws ServiceException{
		return participantMgmtServiceBean.findParticipantIntDepoByParticipantAndDepoServiceBean(participantIntDepositaryFound);
	}
	
	/**
	 * List participant mech history by request service facade.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantMechanismHistory> listParticipantMechHistoryByRequest(
			Long idParticipantRequestPk, Integer indNew) throws ServiceException {
		return participantMgmtServiceBean.listParticipantMechHistoryByRequest(idParticipantRequestPk, indNew);
	}
	
	/**
	 * Gets the list active negotiation modalities.
	 *
	 * @return the list active negotiation modalities
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getListActiveNegotiationModalities() throws ServiceException{		
		return participantMgmtServiceBean.getListActiveNegotiationModalities();
	}

	/**
	 * Exist participant bic code.
	 *
	 * @param bicCode the bic code
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant existParticipantBicCode(String bicCode, Long idParticipantPk) throws ServiceException {
		return participantMgmtServiceBean.existParticipantBicCode(bicCode, idParticipantPk);
	}

	/**
	 * Exist participant modif request bic code.
	 *
	 * @param bicCode the bic code
	 * @param idParticipantPk the id participant pk
	 * @return the participant request
	 * @throws ServiceException the service exception
	 */
	public ParticipantRequest existParticipantModifRequestBicCode(String bicCode, Long idParticipantPk) throws ServiceException {
		return participantMgmtServiceBean.existParticipantModifRequestBicCode(bicCode, idParticipantPk);
	}

	/**
	 * Exist bank bic code.
	 *
	 * @param bicCode the bic code
	 * @return the bank
	 * @throws ServiceException the service exception
	 */
	public Bank existBankBicCode(String bicCode) throws ServiceException{
		return participantMgmtServiceBean.existBankBicCode(bicCode);
	}

	/**
	 * List institution bank account central.
	 *
	 * @param prividerBank the privider bank
	 * @param clientBank the client bank
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionBankAccount> listInstitutionBankAccountCentral(
			Bank prividerBank, Bank clientBank) throws ServiceException{
		return participantMgmtServiceBean.listInstitutionBankAccountCentral(prividerBank,clientBank);
	}
	
	/**
	 * Find provider bank.
	 *
	 * @return the bank
	 * @throws ServiceException the service exception
	 */
	public Bank findProviderBank() throws ServiceException{
		return participantMgmtServiceBean.findProviderBank();
	}

	/**
	 * List participant bank account.
	 *
	 * @param participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantBankAccount> listParticipantBankAccount(Participant participant)  throws ServiceException{
		return participantMgmtServiceBean.listParticipantBankAccount(participant);
	}

	/**
	 * Validate exists holder account bank.
	 *
	 * @param bicCode the bic code
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsHolderAccountBank(String bicCode) throws ServiceException{
		return participantMgmtServiceBean.validateExistsHolderAccountBank(bicCode);
	}
	
	/**
	 * List holder account bank.
	 *
	 * @param bicCode the bic code
	 * @param providerBank the provider bank
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBank> listHolderAccountBank(String bicCode, Bank providerBank)  throws ServiceException{
		return participantMgmtServiceBean.listParticipantBankAccount(bicCode, providerBank);
	}

	/**
	 * Find holder by bic code.
	 *
	 * @param bicCode the bic code
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderByBicCode(String bicCode) throws ServiceException{
		return participantMgmtServiceBean.findHolderByBicCode(bicCode);
	}


	/**
	 * Find bic code participant.
	 *
	 * @param participant the participant
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String findBicCodeParticipant(Participant participant) throws ServiceException{
		return participantMgmtServiceBean.findBicCodeParticipant(participant);
	}
	
	/**
	 * Find bic code partipant request hist.
	 *
	 * @param participantRequest the participant request
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String findBicCodePartipantRequestHist(ParticipantRequest participantRequest) throws ServiceException{
		return participantMgmtServiceBean.findBicCodePartipantRequestHist(participantRequest);
	}

	/**
	 * Validate bank by document type and number.
	 *
	 * @param objBank the obj bank
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateBankByDocumentTypeAndNumber(Bank objBank) throws ServiceException {
		return participantMgmtServiceBean.validateBankByDocumentTypeAndNumber(objBank);
	}
	
	/**
	 * Validate exists holder account bank by document and bic.
	 *
	 * @param holder the holder
	 * @param bicCode the bic code
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsHolderAccountBankByDocumentAndBic(Holder holder, String bicCode) throws ServiceException {
		return participantMgmtServiceBean.validateExistsHolderAccountBankByDocumentAndBic(holder, bicCode);
	}
	
	/**
	 * Find bank by document type and number.
	 *
	 * @param objBank the obj bank
	 * @return the bank
	 * @throws ServiceException the service exception
	 */
	public Bank findBankByDocumentTypeAndNumber(Bank objBank) throws ServiceException{
		return participantMgmtServiceBean.findBankByDocumentTypeAndNumber(objBank);
	}
	
	/**
	 * Update bic code bank.
	 *
	 * @param objBank the obj bank
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateBicCodeBank(Bank objBank, LoggerUser loggerUser) throws ServiceException{
		return participantMgmtServiceBean.updateBicCodeBank(objBank, loggerUser);
	}
	
	/**
	 * Disable institution bank accounts.
	 *
	 * @param objBank the obj bank
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableInstitutionBankAccounts(Bank objBank, LoggerUser loggerUser) throws ServiceException{
		return participantMgmtServiceBean.disableInstitutionBankAccounts(objBank, loggerUser);
	}

	/**
	 * Find holder by participant.
	 *
	 * @param participant the participant
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderByParticipant(Participant participant) throws ServiceException{
		return participantMgmtServiceBean.findHolderByParticipant(participant);
	}
	
	/**
	 * List holder account info by document and bic.
	 *
	 * @param holder the holder
	 * @param centralizingBank the centralizing bank
	 * @param bicCode the bic code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> listHolderAccountInfoByDocumentAndBic(Holder holder, Bank centralizingBank, String bicCode) throws ServiceException{
		return participantMgmtServiceBean.listHolderAccountInfoByDocumentAndBic(holder, centralizingBank, bicCode);
	}		
	
	/**
	 * Register batchconfirm participant annullment.
	 *
	 * @param participantRequest the participant request
	 * @throws ServiceException the service exception
	 */
	public void registerBatchconfirmParticipantAnnullment(ParticipantRequest participantRequest) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
        
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.CANCEL_PARTICIPANT_CONFIRM.getCode());
	    Map<String, Object> param = new HashMap<String, Object>();			    
	    
	    String userNameLogged = loggerUser.getUserName();
	    /*** SETTING PARAMS FOR BATCH ***/
	    param.put("motive",        		  participantRequest.getRequestMotive());			    						    
	    param.put("requestType",   	      participantRequest.getRequestType());
	    param.put("participantId", 		  participantRequest.getParticipant().getIdParticipantPk());
	    param.put("userNameLogged", userNameLogged);
	    param.put("participantRequestId", participantRequest.getIdParticipantRequestPk());		    		    		    			
	    
	    batchProcessServiceFacade.registerBatch(userNameLogged, businessProcess, param);	
	}
	
	/**
	 * List participant file histories by request.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantFileHistory> listParticipantFileHistoriesByRequest(Long idParticipantRequestPk, Integer indNew) throws ServiceException{
		return participantMgmtServiceBean.listParticipantFileHistoriesByRequest(idParticipantRequestPk, indNew);
	}
	
	/**
	 * List Institution Bank Account histories by request.
	 *
	 * @param idParticipantRequestPk the id participant request pk
	 * @param indNew the ind new
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<InstitutionBankAccountHistory> listInstitutionBankAccountHistoriesByRequest(Long idParticipantRequestPk, Integer indNew) throws ServiceException{
		return participantMgmtServiceBean.listInstitutionBankAccountHistoriesByRequest(idParticipantRequestPk, indNew);
	}
	
	/**
	 * Assign identity holder.
	 *
	 * @param economicActivity the economic activity
	 * @return the long
	 */
	public Long assignIdentityHolder(Integer economicActivity){
		Long nextVal=null;
		
		/*
		 * PARA LA ASIGNACION DE CUI:
		 * SE CAMBIO LAS SECUENCIAS POR UN RELLENADOR CUIs vacantes (ISSUE 611) 
		 */
		
		List<Long> listCuiAvailable=new ArrayList<>();
    	if(economicActivity!=null && economicActivity.equals(EconomicActivityType.AGENCIAS_BOLSA.getCode())){
			listCuiAvailable = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_BAG_AGENCY, CuiRangeRevervedConstanst.FINAL_BAG_AGENCY);
//			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_AB);
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())){
			listCuiAvailable = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_FUNDS_ADMINISTRATION_COMPANIES, CuiRangeRevervedConstanst.FINAL_FUNDS_ADMINISTRATION_COMPANIES);
			List<Long> listAvailableCuiAux=holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_INVESTMENT_FUND2,
					CuiRangeRevervedConstanst.FINAL_INVESTMENT_FUND2);
			
			listCuiAvailable.addAll(listAvailableCuiAux);
//			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_SAFI);
		}
		else if(economicActivity!=null && (economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())
				||economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode())
				||economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode())
				)){
			listCuiAvailable = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_INVESTMENT_FUND, CuiRangeRevervedConstanst.FINAL_INVESTMENT_FUND);
//			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_FIC);
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())){
			listCuiAvailable = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_PENSION_FUND_ADMINISTRATOR, CuiRangeRevervedConstanst.FINAL_PENSION_FUND_ADMINISTRATOR);
//			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_AFP);
	    }
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.BANCOS.getCode())){
			listCuiAvailable = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_BANK, CuiRangeRevervedConstanst.FINAL_BAK);
//			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_B);
		}
		else if(economicActivity!=null && (economicActivity.equals(EconomicActivityType.MUTUALES.getCode())
			|| economicActivity.equals(EconomicActivityType.FONDOS_FINANCIEROS_PRIVADOS.getCode())
			|| economicActivity.equals(EconomicActivityType.COOPERATIVAS.getCode()))){
			listCuiAvailable = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_MUTUALS_AND_PRIVATE_FINANCIAL_FUNDS_AND_COOPERATIVES, CuiRangeRevervedConstanst.FINAL_MUTUALS_AND_PRIVATE_FINANCIAL_FUNDS_AND_COOPERATIVES);
//			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_MFC);
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.COMPANIAS_SEGURO.getCode())){
			listCuiAvailable = holderServiceFacade.getListCuiAvailable(CuiRangeRevervedConstanst.INITIAL_INSURANCE_COMPANIES, CuiRangeRevervedConstanst.FINAL_INSURANCE_COMPANIES);
//			nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_CS);
		}
		else{
			//asignacion de CUI normal pero de los vacantes en el rango de [1000 - 9999] (issue 611)
			Integer idInitialRangeCuiParameterTable = 2441;
			Integer idFinalRangeCuiParameterTable = 2442;
			Long initialCuiAvailable, finalCuiAvailable;
			//obteniendo el inicio y fin del rango de CUI disponibles desde ParameterTable (issue 611)
			initialCuiAvailable = holderServiceFacade.getRangeCuiAvailableFromParameterTable(idInitialRangeCuiParameterTable);
			finalCuiAvailable = holderServiceFacade.getRangeCuiAvailableFromParameterTable(idFinalRangeCuiParameterTable);
			if (initialCuiAvailable != null && finalCuiAvailable != null)
				listCuiAvailable = holderServiceFacade.getListCuiAvailable(initialCuiAvailable, finalCuiAvailable);
			else
				nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK);
		}
    	
    	//verificando si aun hay cui disponibles (issue 611)
    	if (!listCuiAvailable.isEmpty())
			nextVal = listCuiAvailable.get(0);
	    
	    if(nextVal!=null && nextVal.equals(new Long(-1))){
		    nextVal=holderServiceFacade.getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK);
		}
	    
	    if(nextVal!=null && !nextVal.equals(new Long(-1))){
				if(accountsFacade.validateExistHolderServiceFacade(nextVal)){
					assignIdentityHolder(economicActivity);					
				}
		}
	    
	    return nextVal;
	}
	
	/**
	 * Creates the sequence participant dinamic.
	 *
	 * @param mnenomicParticipant the mnenomic participant
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int createSequenceParticipantDinamic(String mnenomicParticipant) throws ServiceException {
		return participantMgmtServiceBean.createSequenceParticipantDinamic(mnenomicParticipant);
	}
	
	/**
	 * Validate Ind Cant Transfer
	 * 
	 */
	public Holder validateIndCanTransfer (Holder holder){
		
		//ISSUE 770
		
		//cambiar si es que el indicador de la transferencia extrabursatil es 0
		if(Validations.validateIsNull(holder.getIndCanNegotiate())){
		
			//en cualquier caso la decision es SI 1
			holder.setIndCanNegotiate(1);
			
			//validando que la actividad economica no sea nula
			if (Validations.validateIsNotNull(holder.getEconomicActivity())){
				
				//exceptuando las actividades economicas:
				
				//AGENCIAS DE BOLSA
				if(holder.getEconomicActivity().intValue() == EconomicActivityType.AGENCIAS_BOLSA.getCode()){
					holder.setIndCanNegotiate(0);
				}
				
				//COMPANIAS DE SEGUROS
				if(holder.getEconomicActivity().intValue() == EconomicActivityType.COMPANIAS_SEGURO.getCode()){
					holder.setIndCanNegotiate(0);
				}
				
				//FONDOS DE INVERSION ABIERTOS
				if(holder.getEconomicActivity().intValue() == EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode()){
					holder.setIndCanNegotiate(0);
				}
				
				//FONDOS DE INVERSION CERRADOS
				if(holder.getEconomicActivity().intValue() == EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode()){
					holder.setIndCanNegotiate(0);
				}
				
				//FONDOS DE INVERSION MIXTOS
				if(holder.getEconomicActivity().intValue() == EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode()){
					holder.setIndCanNegotiate(0);
				}
				
				//SOCIEDADES ADMINISTRADORAS DE FONDOS DE INVERSION
				if(holder.getEconomicActivity().intValue() == EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()){
					holder.setIndCanNegotiate(0);
				}
			}
		}
		return holder;
	}
}
