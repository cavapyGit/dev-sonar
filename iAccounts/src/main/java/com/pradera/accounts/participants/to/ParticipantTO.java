package com.pradera.accounts.participants.to;

import java.io.Serializable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public class ParticipantTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The description. */
	private String description;
	
	/** The document type. */
	private Integer documentType;
	
	/** The document number. */
	private String documentNumber;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The participant type. */
	private Integer participantType;
	
	/** The participant class. */
	private Integer participantClass;
	
	/** The state participant. */
	private Integer stateParticipant;
	
	/** The participant request to. */
	private ParticipantRequestTO participantRequestTO;
	
	/** The lst participant states. */
	private List<Integer> lstParticipantStates;
	
	/** The sort by. */
	private String sortBy;
	
	/**
	 * Instantiates a new participant to.
	 */
	public ParticipantTO() {
		sortBy = "description";
		participantRequestTO = new ParticipantRequestTO();
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the participant type.
	 *
	 * @return the participant type
	 */
	public Integer getParticipantType() {
		return participantType;
	}

	/**
	 * Sets the participant type.
	 *
	 * @param participantType the new participant type
	 */
	public void setParticipantType(Integer participantType) {
		this.participantType = participantType;
	}

	/**
	 * Gets the participant class.
	 *
	 * @return the participant class
	 */
	public Integer getParticipantClass() {
		return participantClass;
	}

	/**
	 * Sets the participant class.
	 *
	 * @param participantClass the new participant class
	 */
	public void setParticipantClass(Integer participantClass) {
		this.participantClass = participantClass;
	}

	/**
	 * Gets the state participant.
	 *
	 * @return the state participant
	 */
	public Integer getStateParticipant() {
		return stateParticipant;
	}

	/**
	 * Sets the state participant.
	 *
	 * @param stateParticipant the new state participant
	 */
	public void setStateParticipant(Integer stateParticipant) {
		this.stateParticipant = stateParticipant;
	}

	/**
	 * Gets the participant request to.
	 *
	 * @return the participant request to
	 */
	public ParticipantRequestTO getParticipantRequestTO() {
		return participantRequestTO;
	}

	/**
	 * Sets the participant request to.
	 *
	 * @param participantRequestTO the new participant request to
	 */
	public void setParticipantRequestTO(ParticipantRequestTO participantRequestTO) {
		this.participantRequestTO = participantRequestTO;
	}

	/**
	 * Gets the lst participant states.
	 *
	 * @return the lst participant states
	 */
	public List<Integer> getLstParticipantStates() {
		return lstParticipantStates;
	}

	/**
	 * Sets the lst participant states.
	 *
	 * @param lstParticipantStates the new lst participant states
	 */
	public void setLstParticipantStates(List<Integer> lstParticipantStates) {
		this.lstParticipantStates = lstParticipantStates;
	}

	/**
	 * Gets the sort by.
	 *
	 * @return the sort by
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * Sets the sort by.
	 *
	 * @param sortBy the new sort by
	 */
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}	
	
}
