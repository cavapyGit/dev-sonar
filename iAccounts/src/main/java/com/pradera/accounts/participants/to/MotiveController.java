package com.pradera.accounts.participants.to;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class MotiveController.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/02/2013
 */
public class MotiveController implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The motive list. */
	private List<ParameterTable> motiveList;
	
	/** The id motive pk. */
	private Long idMotivePK;
	
	/** The show motive text. */
	private boolean showMotiveText;
	
	/** The motive text. */
	private String motiveText;
	
	/** The motive list cache. */
	private static Map<Integer,Object> motiveListCache;
	
	static{
		motiveListCache = new HashMap<Integer, Object>();
	}
	
	/**
	 * Instantiates a new motive controller.
	 */
	public MotiveController() {
		super();
	}
	
	/**
	 * Gets the motive list cache.
	 *
	 * @return the motive list cache
	 */
	public static Map<Integer, Object> getMotiveListCache() {
		return motiveListCache;
	}

	/**
	 * Sets the motive list cache.
	 *
	 * @param motiveListCache the motive list cache
	 */
	public static void setMotiveListCache(Map<Integer, Object> motiveListCache) {
		MotiveController.motiveListCache = motiveListCache;
	}

	/**
	 * Gets the motive list.
	 *
	 * @return the motive list
	 */
	public List<ParameterTable> getMotiveList() {
		return motiveList;
	}
	
	/**
	 * Sets the motive list.
	 *
	 * @param motiveList the new motive list
	 */
	public void setMotiveList(List<ParameterTable> motiveList) {
		this.motiveList = motiveList;
	}
	
	/**
	 * Gets the id motive pk.
	 *
	 * @return the id motive pk
	 */
	public Long getIdMotivePK() {
		return idMotivePK;
	}
	
	/**
	 * Sets the id motive pk.
	 *
	 * @param idMotivePK the new id motive pk
	 */
	public void setIdMotivePK(Long idMotivePK) {
		this.idMotivePK = idMotivePK;
	}
	
	/**
	 * Checks if is show motive text.
	 *
	 * @return true, if is show motive text
	 */
	public boolean isShowMotiveText() {
		return showMotiveText;
	}
	
	/**
	 * Sets the show motive text.
	 *
	 * @param showMotiveText the new show motive text
	 */
	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}
	
	/**
	 * Gets the motive text.
	 *
	 * @return the motive text
	 */
	public String getMotiveText() {
		return motiveText;
	}
	
	/**
	 * Sets the motive text.
	 *
	 * @param motiveText the new motive text
	 */
	public void setMotiveText(String motiveText) {
		this.motiveText = motiveText;
	}
	
	/**
	 * Sets the motive list.
	 *
	 * @param motiveList the motive list
	 * @param masterTabletType the master tablet type
	 */
	public void setMotiveList(List<ParameterTable> motiveList, MasterTableType masterTabletType){
		if(Validations.validateIsNull(MotiveController.motiveListCache.get(masterTabletType.getCode()))){
			motiveListCache.put(masterTabletType.getCode(), motiveList);
			this.setMotiveList(motiveList);			
		}
	}
}