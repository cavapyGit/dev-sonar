package com.pradera.accounts.participants.view;

import java.io.Serializable;

/** clase que contiene las cosntantes de inicio y fin de los rangos reservados para la asignacion de CUI en la creacion de
 * Participantes (issue 611)
 * 
 * @author rlarico */
public class CuiRangeRevervedConstanst implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9066428299090324235L;

	// constructor
	public CuiRangeRevervedConstanst() {
	}

	// 43 AGENCIAS DE BOLSA rangoCUI(1-100)
	public static final Long INITIAL_BAG_AGENCY = 1L;
	public static final Long FINAL_BAG_AGENCY = 100L;

	// 1879 SOCIEDADES ADMINISTRADORAS DE FONDOS DE INVERSION rangoCUI(101-219)
	public static final Long INITIAL_FUNDS_ADMINISTRATION_COMPANIES = 101L;
	public static final Long FINAL_FUNDS_ADMINISTRATION_COMPANIES = 219L;

	// 56 FONDOS DE INVERSION PRIVADOS rangoCUI(220-450)
	public static final Long INITIAL_INVESTMENT_FUND = 220L;
	public static final Long FINAL_INVESTMENT_FUND = 450L;
	// 1877 PARTICULARES (PERSONAS NATURALES) rangoCUI(501-700)
	public static final Long INITIAL_INVESTMENT_FUND2 = 501L;
	public static final Long FINAL_INVESTMENT_FUND2 = 700L;

	// 42 ADMINISTRADORAS DE FONDOS DE PENSIONES rangoCUI(451-500)
	public static final Long INITIAL_PENSION_FUND_ADMINISTRATOR = 451L;
	public static final Long FINAL_PENSION_FUND_ADMINISTRATOR = 500L;

	// 45 BANCOS rangoCUI(701-800)
	public static final Long INITIAL_BANK = 701L;
	public static final Long FINAL_BAK = 800L;

	// 61 MUTUALES
	// 58 FONDOS FINANCIEROS PRIVADOS
	// 50 COOPERATIVAS rangoCUI(801-900)
	public static final Long INITIAL_MUTUALS_AND_PRIVATE_FINANCIAL_FUNDS_AND_COOPERATIVES = 801L;
	public static final Long FINAL_MUTUALS_AND_PRIVATE_FINANCIAL_FUNDS_AND_COOPERATIVES = 900L;

	// 48 COMPANIIAS DE SEGUROS rangoCUI(901-999)
	public static final Long INITIAL_INSURANCE_COMPANIES = 901L;
	public static final Long FINAL_INSURANCE_COMPANIES = 999L;
}
