package com.pradera.accounts.participants.service;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.accounts.participants.facade.ParticipantServiceFacade;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestBlockMotiveType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ParticipantBlockBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@BatchProcess(name="ParticipantBlockBatch")
@RequestScoped
public class ParticipantBlockBatch implements JobExecution{
	
	/** The account service bean. */
	@EJB
	private HolderAccountComponentServiceBean accountServiceBean;
	
	/** The participant service facade. */
	@EJB
    ParticipantServiceFacade participantServiceFacade;	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		ParticipantRequest participantRequest = new ParticipantRequest();
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		Integer motive            = null;
		Integer requestType		  = null;
		Long participantId		  = null;
		Long participantRequestId = null;
		String userNameLogged 	  = null;
		//Getting params
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			if(processLoggerDetail.getParameterName().equals("motive")){
				motive = Integer.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("requestType")){
				requestType = Integer.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("participantId")){
				participantId = Long.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("participantRequestId")){
				participantRequestId = Long.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("userNameLogged")){
				userNameLogged = processLoggerDetail.getParameterValue();
			}
		}
		//Getting participant
		Participant participant = new Participant();
		participant.setIdParticipantPk(participantId);
		
		participantRequest.setIdParticipantRequestPk(participantRequestId);
		participantRequest.setRequestType(requestType);
		participantRequest.setRejectedMotive(motive);		
		participantRequest.setParticipant(participant);
		
		try {
			this.closeHolderAccounts(participantRequestId,participantId, userNameLogged);
			this.participantServiceFacade.confirmParticipantAnnullment(participantRequest,userNameLogged);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Close holder accounts.
	 *
	 * @param idParticipantRequest the id participant request
	 * @param idParticipant the id participant
	 * @param userNameLogged the user name logged
	 * @throws ServiceException the service exception
	 */
	public void closeHolderAccounts(Long idParticipantRequest,Long idParticipant, String userNameLogged) throws ServiceException{
		
		HolderAccountTO holderAccountTO = new HolderAccountTO();	
		holderAccountTO.setParticipantTO(idParticipant);
		
		List<HolderAccount> holderAccountList = this.accountServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO);
		
		for (HolderAccount holderAccount : holderAccountList) {
			
			Participant objParticipant = new Participant();
			objParticipant.setIdParticipantPk(idParticipant);		
	        
			HolderAccountRequest tmpHolderAccountRequestHy = new HolderAccountRequest();
			tmpHolderAccountRequestHy.setParticipant(objParticipant);
			tmpHolderAccountRequestHy.setHolderAccount(holderAccount);
			tmpHolderAccountRequestHy.setRequestType(HolderAccountRequestHysType.CLOSE.getCode());
			tmpHolderAccountRequestHy.setRequestMotive(HolderAccountRequestBlockMotiveType.REQ_BROKER.getCode());
			tmpHolderAccountRequestHy.setRequestOtherMotive("POR ANULACION DE PARTICIPANTE");
			tmpHolderAccountRequestHy.setHolderAccountGroup(holderAccount.getAccountGroup());
			tmpHolderAccountRequestHy.setHolderAccountType(holderAccount.getAccountType());
			tmpHolderAccountRequestHy.setRequestState(HolderAccountRequestHysStatusType.REGISTERED.getCode());
			tmpHolderAccountRequestHy.setRegistryDate(CommonsUtilities.currentDateTime());
			tmpHolderAccountRequestHy.setRegistryUser(userNameLogged);
			
			tmpHolderAccountRequestHy = this.accountServiceBean.regHoldAccountReqComponentServiceBean(tmpHolderAccountRequestHy);
			this.accountServiceBean.closeHolderAccountServiceBean(tmpHolderAccountRequestHy);
			
			
		}
	
	}
				
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
