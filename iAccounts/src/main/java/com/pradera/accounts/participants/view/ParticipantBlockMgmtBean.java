package com.pradera.accounts.participants.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.keyfilter.KeyFilter;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;

import com.pradera.accounts.participants.facade.ParticipantServiceFacade;
import com.pradera.accounts.participants.to.ParticipantHistoryStateTO;
import com.pradera.accounts.participants.to.ParticipantRequestTO;
import com.pradera.accounts.participants.to.ParticipantTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.ParticipantFile;
import com.pradera.model.accounts.ParticipantHistoryState;
import com.pradera.model.accounts.ParticipantRequest;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PartBlockUnblockAnullRejectMotiveType;
import com.pradera.model.accounts.type.ParticipantFileType;
import com.pradera.model.accounts.type.ParticipantRequestStateType;
import com.pradera.model.accounts.type.ParticipantRequestType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.RequestParticipantFileType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantBlockMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/03/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ParticipantBlockMgmtBean extends GenericBaseBean implements Serializable{

	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6279522556411642217L;

	/** The participant file name display. */
	private String participantFileNameDisplay;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The lst participant request type. */
	private List<ParameterTable> lstParticipantRequestType;
	
	/** The lst participant request motive. */
	private List<ParameterTable> lstParticipantRequestMotive;
	
	/** The lst participant document type. */
	private List<ParameterTable> lstParticipantDocumentType;
	
	/** The lst participant request state. */
	private List<ParameterTable> lstParticipantRequestState;
	
	/** The lst participant request reject motive. */
	private List<ParameterTable> lstParticipantRequestRejectMotive;
	
	/** The participant request. */
	private ParticipantRequest participantRequest;		
	
	/** The participant service facade. */
	@EJB
    ParticipantServiceFacade participantServiceFacade;
	
	/** The general parameters facade. */
	@EJB
    GeneralParametersFacade generalParametersFacade;
	
	@EJB
	AccountsFacade accountsFacade;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The lst participant file sustent. */
	private List<ParticipantFile> lstParticipantFileSustent;
	
	/** The participant to. */
	private ParticipantTO participantTO;
	
	/** The participant request data model. */
	private ParticipantRequestDataModel participantRequestDataModel;
	
	/** The ind disable confirn reject. */
	private boolean indDisableConfirnReject;
	
	/** The id participant request pk. */
	private Long idParticipantRequestPk;
	
	/** The ind selected other motive reject. */
	private boolean indSelectedOtherMotiveReject;
	
	/** The ind disable accept motive reject. */
	private boolean indDisableAcceptMotiveReject;
	
	/** The lst participant history states. */
	private List<ParticipantHistoryState> lstParticipantHistoryStates;
	
	/** The user info. */
	@Inject UserInfo userInfo;
	
	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	private boolean allowSearchAll;
	/**
	 * The Constructor.
	 */
	public ParticipantBlockMgmtBean() {
		participantRequest = new ParticipantRequest();
		participantTO = new ParticipantTO();
	}
	
	/**
	 * List all participants.
	 *
	 * @param filter the filter
	 * @throws ServiceException the service exception
	 */
	private void listParticipantsByRequestType(Participant filter) throws ServiceException{	
		lstParticipants = accountsFacade.getLisParticipantServiceBean(filter);
	}	
	
	/**
	 * List participant document types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listParticipantDocumentTypes() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		filterParameterTable.setLstParameterTablePk(new ArrayList<Integer>());
		filterParameterTable.getLstParameterTablePk().add(DocumentType.RUC.getCode());
		filterParameterTable.getLstParameterTablePk().add(DocumentType.DIO.getCode());
		lstParticipantDocumentType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}	
	
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try{						
			
			listParticipantDocumentTypes();
			
			listParticipantRequestTypes();
			
			listParticipantRequestStates();
			
			participantTO.getParticipantRequestTO().setInitiaRequestDate(CommonsUtilities.currentDate());
			participantTO.getParticipantRequestTO().setEndRequestDate(CommonsUtilities.currentDate());	
			
			if(userInfo.getUserAccountSession().isDepositaryInstitution() || userInfo.getUserAccountSession().isIssuerInstitucion()) {
				allowSearchAll = true;
			} else {
				allowSearchAll = false;
				participantTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
				participantTO.setMnemonic(userInfo.getUserAccountSession().getUserName());
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * List participant request types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listParticipantRequestTypes() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_BLOCK_UBBLOCK_ANNUL_TYPE.getCode());		
		lstParticipantRequestType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * List participant request states.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listParticipantRequestStates() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_BLOCK_UBBLOCK_ANNUL_STATE.getCode());		
		lstParticipantRequestState = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * List block unblock annull motive.
	 *
	 * @param filterParameterTable the filter parameter table
	 * @throws ServiceException the service exception
	 */
	private void listBlockUnblockAnnullMotive(ParameterTableTO filterParameterTable) throws ServiceException{
		lstParticipantRequestMotive = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * Change selected request type.
	 */
	@LoggerAuditWeb
	public void changeSelectedRequestType(){		
		try{
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRequest");
			
			//If request type was not selected
			if(Validations.validateIsNullOrNotPositive(participantRequest.getRequestType())){
				lstParticipants = null;
				lstParticipantRequestMotive = null;
				return;
			}
			
			Participant participantFilter = new Participant();
			List<Integer> lstParticipantStates = new ArrayList<Integer>();
			
			ParameterTableTO filterRequestMotive = new ParameterTableTO();
			filterRequestMotive.setState(ParameterTableStateType.REGISTERED.getCode());
			
			if(ParticipantRequestType.BLOCK.getCode().equals(participantRequest.getRequestType())){
				//When Participant request type is Block, list participant with state REGISTERED
				lstParticipantStates.add(ParticipantStateType.REGISTERED.getCode());
				
				//List Request Motive for Block
				filterRequestMotive.setMasterTableFk(MasterTableType.PARTICIPANT_BLOCK_MOTIVE.getCode());
			} else if(ParticipantRequestType.UNBLOCK.getCode().equals(participantRequest.getRequestType())){
				//When Participant request type is Block, list participant with state BLOCKED
				lstParticipantStates.add(ParticipantStateType.BLOCKED.getCode());
				
				//List Request Motive for Block
				filterRequestMotive.setMasterTableFk(MasterTableType.PARTICIPANT_UNBLOCK_MOTIVE.getCode());
			} else if(ParticipantRequestType.ANNULMENT.getCode().equals(participantRequest.getRequestType())){
				//When Participant request type is Block, list participant with state REGISTERED and BLOCKED
				lstParticipantStates.add(ParticipantStateType.REGISTERED.getCode());
				lstParticipantStates.add(ParticipantStateType.BLOCKED.getCode());
				
				//List Request Motive for Block
				filterRequestMotive.setMasterTableFk(MasterTableType.PARTICIPANT_ANNUL_MOTIVE.getCode());
			}
			
			participantFilter.setLstParticipantStates(lstParticipantStates);
			participantFilter.setIndDepositary(null);
			//List Participants
			if(Validations.validateIsNotNull(getViewOperationType()) && isViewOperationRegister()){
				listParticipantsByRequestType(participantFilter);
			} 
			
			//List Motives
			listBlockUnblockAnnullMotive(filterRequestMotive);
			
			lstParticipantFileSustent = new ArrayList<ParticipantFile>();
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Handle file upload.
	 *
	 * @param eventFile the event file
	 */
	@LoggerAuditWeb
	public void uploadSustentFiles(FileUploadEvent eventFile) {
		try{
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmParticipantRequest");
			
			String fDisplayName=fUploadValidateFile(eventFile.getFile(), null, null);
			 
			 if(fDisplayName!=null){
				 participantFileNameDisplay=fDisplayName;
			 }
			
			if(Validations.validateIsNull(lstParticipantFileSustent)){
				lstParticipantFileSustent = new ArrayList<ParticipantFile>();
			}
			
			ParticipantFile fileSustent = new ParticipantFile();
			fileSustent = new ParticipantFile();
			fileSustent.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());
			fileSustent.setFilename(fUploadValidateFileNameLength(eventFile.getFile().getFileName()));
			fileSustent.setFile(eventFile.getFile().getContents());
			fileSustent.setDocumentType(ParticipantFileType.DEPOSITARY_REGISTRATION_REQUEST.getCode());
			
			lstParticipantFileSustent.add(fileSustent);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
    }
	
	/**
	 * Load register participant request.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadRegisterParticipantRequest(){	
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		cleanParticipantRequest();
		return "participantBlockMgmtRule";
	}
	
	/**
	 * Validate observation.
	 */
	public void validateObservation() {
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponent("frmParticipantRequest:txtObservations");
		
		if(Validations.validateIsNotNullAndNotEmpty(participantRequest.getObservations())){
			if(!CommonsUtilities.matchObservation(participantRequest.getObservations())) { 
				JSFUtilities.addContextMessage("frmParticipantRequest:txtObservations",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_OBSERVATIONS_FORMAT), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_OBSERVATIONS_FORMAT));
				showMessageOnDialog(
	    				PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_OBSERVATIONS_FORMAT,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		participantRequest.setObservations(null);
			} else if(participantRequest.getObservations().length() > 200) {
				participantRequest.setObservations(participantRequest.getObservations().substring(0, 200));
			}
		}
	}
	
	/**
	 * Begin save register participant request.
	 */
	@LoggerAuditWeb
	public void beginSaveRegisterParticipantRequest(){
		
		hideDialogsListener(null);
		//JSFUtilities.resetViewRoot();
		
		try {
			
			if(Validations.validateIsNotNullAndNotEmpty(participantRequest.getObservations())){
				if(participantRequest.getObservations().length() > 200) {
					participantRequest.setObservations(participantRequest.getObservations().substring(0, 200));
				}
			}
			
			
			int countValidationErrors = 0;			
			
			if(Validations.validateIsNullOrNotPositive(participantRequest.getRequestType())){
	    		JSFUtilities.addContextMessage("frmParticipantRequest:cboRequestType",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REQUEST_TYPE), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REQUEST_TYPE));
	    		countValidationErrors++;
	    	}
			
			if(Validations.validateIsNullOrNotPositive(idParticipantPk)){
	    		JSFUtilities.addContextMessage("frmParticipantRequest:cboParticipant",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_PARTICIPANT), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_PARTICIPANT));
	    		countValidationErrors++;
	    	}
			
			if(Validations.validateIsNullOrNotPositive(participantRequest.getRequestMotive())){
	    		JSFUtilities.addContextMessage("frmParticipantRequest:cboMotive",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_MOTIVE),
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_MOTIVE));
	    		countValidationErrors++;
	    	}
			
			if(Validations.validateListIsNullOrEmpty(lstParticipantFileSustent)){
	    		JSFUtilities.addContextMessage("frmParticipantRequest:fuplParticipantFile",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_FILES_SUSTENT), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_FILES_SUSTENT));
	    		countValidationErrors++;
	    	}
			
			//If validations greater than 0, show error message
			if(countValidationErrors > 0) {
	    		showMessageOnDialog(
	    				PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.MESSAGE_DATA_REQUIRED,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
	    	}
			
			if(Validations.validateIsNotNullAndNotEmpty(participantRequest.getObservations())){
				if(!CommonsUtilities.matchObservation(participantRequest.getObservations())) { 
					JSFUtilities.addContextMessage("frmParticipantRequest:txtObservations",
		    				FacesMessage.SEVERITY_ERROR,
		    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_OBSERVATIONS_FORMAT), 
		    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_OBSERVATIONS_FORMAT));
					showMessageOnDialog(
		    				PropertiesConstants.MESSAGES_ALERT,
		    				null,
		    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_OBSERVATIONS_FORMAT,							
		    				null);
		    		JSFUtilities.showSimpleValidationDialog();
		    		participantRequest.setObservations(null);
		    		return;
				}
			}
			
			ParticipantRequestTO participantRequestTO = new ParticipantRequestTO();
			participantRequestTO.setIdParticipantPk(idParticipantPk);			
			participantRequestTO.setStateParticipantRequest(ParticipantRequestStateType.REGISTERED.getCode());	
			List<Integer> lstParticipantRequestTypes = new ArrayList<Integer>();
			lstParticipantRequestTypes.add(ParticipantRequestType.BLOCK.getCode());
			lstParticipantRequestTypes.add(ParticipantRequestType.UNBLOCK.getCode());
			lstParticipantRequestTypes.add(ParticipantRequestType.ANNULMENT.getCode());
			participantRequestTO.setLstParticipantRequestTypes(lstParticipantRequestTypes);
			
			
			//Validate if participant has previous request
			ParticipantRequest participantRequestResult = participantServiceFacade.validateExistsParticipantRequestServiceFacade(participantRequestTO);
			
			
			
			if(Validations.validateIsNotNull(participantRequestResult)){				
				//If exists previous participant request, show error and return
				ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequestResult.getRequestType());
				Object[] arrBodyData = {paramRequestType.getParameterName(),String.valueOf(idParticipantPk)};
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ERROR_ADM_PARTICIPANT_BLOCK_PREVIOUS_REQUEST,							
	    				arrBodyData);
	    		JSFUtilities.showSimpleValidationDialog();
				return;
			}
			/*** Validating if there is Participants accounts with any balance ***/
			if(this.getParticipantRequest().getRequestType().equals(ParticipantRequestType.ANNULMENT.getCode())){
				HolderAccountTO preHolderAccountTO = new HolderAccountTO();				
				preHolderAccountTO.setParticipantTO(idParticipantPk);
				//Gettings HolderAccounts from participant
				List<HolderAccountBalance> holderAccountBalances = this.participantServiceFacade.getListAccountBalancesByParticipantServiceFacade(preHolderAccountTO);
				if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalances)){
					
					for(HolderAccountBalance holderAccountBalance : holderAccountBalances){
						if(holderAccountBalance.getTransitBalance().compareTo(BigDecimal.ZERO)         > 0 || 
						   holderAccountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO)   > 0 ||
						   holderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO)       > 0 ||	
						   holderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) 			   > 0 ||
						   holderAccountBalance.getBorrowerBalance().compareTo(BigDecimal.ZERO) 	   > 0 ||
						   holderAccountBalance.getPurchaseBalance().compareTo(BigDecimal.ZERO)             > 0 ||						   
						   holderAccountBalance.getLenderBalance().compareTo(BigDecimal.ZERO) 	   	   > 0 ||
						   holderAccountBalance.getLoanableBalance().compareTo(BigDecimal.ZERO)    	   > 0 ||
						   holderAccountBalance.getMarginBalance().compareTo(BigDecimal.ZERO)      	   > 0 ||
						   holderAccountBalance.getOppositionBalance().compareTo(BigDecimal.ZERO)  	   > 0 ||
						   holderAccountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO)  	   > 0 ||
						   holderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO)            > 0 ||						   
						   holderAccountBalance.getReportedBalance().compareTo(BigDecimal.ZERO)    	   > 0 ||
						   holderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO)   	   > 0 ||
						   holderAccountBalance.getReserveBalance().compareTo(BigDecimal.ZERO)         > 0 ||
						   holderAccountBalance.getSaleBalance().compareTo(BigDecimal.ZERO)            > 0 ||
						   holderAccountBalance.getTotalBalance().compareTo(BigDecimal.ZERO)           > 0){
							
							JSFUtilities.showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ERROR_PARTICIPANT_ACCOUNTS_WITH_BALANCE_REG,null);
				    		JSFUtilities.showSimpleValidationDialog();
				    		return;
							
						}
					}
				}
				
			}
			Participant participant = new Participant();
			participant.setIdParticipantPk(idParticipantPk);
			participantRequest.setParticipant(participant);
			
			//Show message to confirm the registration of participant request
			ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequest.getRequestType());
			Object[] arrBodyData = {WordUtils.capitalizeFully(paramRequestType.getParameterName()), String.valueOf(idParticipantPk)};
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_REGISTER,
					null,
					PropertiesConstants.LBL_CONFIRM_PARTICIPANT_REQUEST_REGISTER,							
					arrBodyData);
			//JSFUtilities.executeJavascriptFunction("PF('cnfwRegisterParticipantRequest').show()");
			JSFUtilities.showComponent("cnfRegisterParticipantRequest");
			
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}
	
	/**
	 * Save register participant request.
	 */
	@LoggerAuditWeb
	public void saveRegisterParticipantRequest(){
		
		try {
			
			hideDialogsListener(null);					
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantFileSustent)){
				participantRequest.setParticipantFiles(lstParticipantFileSustent);
			} else {
				participantRequest.setParticipantFiles(null);
			}
			
			boolean resultTransaction = participantServiceFacade.registerParticipantBlockUnblockAnnullServiceFacade(participantRequest);
			
			if(resultTransaction) {
				
				//Sending notification
				BusinessProcess businessProcessNotification = new BusinessProcess();
				if(ParticipantRequestType.BLOCK.getCode().equals(participantRequest.getRequestType())) {
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BLOCK_PARTICIPANT_REGISTRATION.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																participantRequest.getParticipant().getIdParticipantPk(), null);
					
		        } else if(ParticipantRequestType.UNBLOCK.getCode().equals(participantRequest.getRequestType())) {
		        	businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_PARTICIPANT_REGISTRATION.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																participantRequest.getParticipant().getIdParticipantPk(), null);
		        } else if(ParticipantRequestType.ANNULMENT.getCode().equals(participantRequest.getRequestType())) {
		        	businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.CANCEL_PARTICIPANT_REGISTRATION.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																participantRequest.getParticipant().getIdParticipantPk(), null);
		        }
				
				
				ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequest.getRequestType());
				Object[] arrBodyData = {WordUtils.capitalizeFully(paramRequestType.getParameterName()),
						String.valueOf(participantRequest.getRequestNumber())};
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_SUCCESFUL, null,
						PropertiesConstants.LBL_SUCCESS_PARTICIPANT_REQUEST_REGISTER, 
						arrBodyData);
				cleanSearchParticipantRequest();			
				
				JSFUtilities.showComponent("cnfEndTransactionParticipant");
				//JSFUtilities.executeJavascriptFunction("PF('cnfwEndTransactionParticipant').show()");
			}			
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Hide dialogs listener.
	 *
	 * @param actionEvent the action event
	 */
	public void hideDialogsListener(ActionEvent actionEvent){
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		
		JSFUtilities.hideComponent("cnfRegisterParticipantRequest");
		JSFUtilities.hideComponent("cnfParticipantRequestConfirmation");
	}
	
	/**
	 * Clean participant request.
	 */	
	private void cleanParticipantRequest(){
		try{
			hideDialogsListener(null);
			participantRequest = new ParticipantRequest();
			lstParticipantFileSustent = new ArrayList<ParticipantFile>();
			lstParticipants = null;
			lstParticipantRequestMotive = null;
			idParticipantPk = null;
			participantFileNameDisplay = null;
			listParticipantRequestTypes();
			
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	@LoggerAuditWeb
	public void cleanParticipantRequestMgmt() {
		cleanParticipantRequest();
		JSFUtilities.resetComponent("frmParticipantRequest");
	}
	
	/**
	 * Clean search participant request.
	 */
	@LoggerAuditWeb
	public void cleanSearchParticipantRequest(){
		hideDialogsListener(null);
		participantTO = new ParticipantTO();		
		participantTO.getParticipantRequestTO().setInitiaRequestDate(CommonsUtilities.currentDate());
		participantTO.getParticipantRequestTO().setEndRequestDate(CommonsUtilities.currentDate());
		participantRequestDataModel = null;
		participantRequest = new ParticipantRequest();
		idParticipantPk = null;
		idParticipantRequestPk = null;
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()) {			
			participantTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		
		JSFUtilities.setValidViewComponentAndChildrens("frmGeneral");
	}
	
	/**
	 * Change participant document type search.
	 */
	@LoggerAuditWeb
	public void changeParticipantDocumentTypeSearch(){
		
		InputText txtDocumentNumber = 
				(InputText)JSFUtilities.findViewComponent("frmGeneral:txtDocumentNumber");
		
		participantTO.setDocumentNumber(null);
		
		if(Validations.validateIsNullOrNotPositive(participantTO.getDocumentType())){
			txtDocumentNumber.setDisabled(Boolean.TRUE);
		} else {
			txtDocumentNumber.setDisabled(Boolean.FALSE);
			KeyFilter kfDocumentNumber =
					(KeyFilter)JSFUtilities.findViewComponent("frmGeneral:kfDocumentNumber");
			if(DocumentType.DIO.getCode().equals(participantTO.getDocumentType())){
				kfDocumentNumber.setRegEx(PropertiesUtilities.getMessage(
						GeneralConstants.PROPERTY_FILE_CONSTANTS,JSFUtilities.getCurrentLocale(),
						PropertiesConstants.EXP_REGULAR_ALPHANUMERIC, new Object[]{}));
			} else {
				kfDocumentNumber.setRegEx(PropertiesUtilities.getMessage(
						GeneralConstants.PROPERTY_FILE_CONSTANTS,JSFUtilities.getCurrentLocale(),
						PropertiesConstants.EXP_REGULAR_NUMERIC_INTEGER, new Object[]{}));
			}
		}
	}
	
	/**
	 * Search participan requests.
	 */
	@LoggerAuditWeb
	public void searchParticipanRequests(){
		try{						
			hideDialogsListener(null);
	    	
	    	//Clean validations on fields
	    	JSFUtilities.setValidViewComponentAndChildrens("frmGeneral");	    	
	    	
	    	//Validations of initial dare not greater than system date
	    	if(participantTO.getParticipantRequestTO().getInitiaRequestDate().after(CommonsUtilities.currentDate())){
	    		JSFUtilities.addContextMessage("frmGeneral:calBeginDate",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE));
	    		showMessageOnDialog(
	    				PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
	    	}
	    	
	    	//Validations of end dare not greater than system date
	    	if(participantTO.getParticipantRequestTO().getEndRequestDate().after(CommonsUtilities.currentDate())){
	    		JSFUtilities.addContextMessage("frmGeneral:calEndDate",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE));
	    		showMessageOnDialog(
	    				PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
	    	}
	    	
	    	//Validations of initial dare not greater than end date
    		if(participantTO.getParticipantRequestTO().getInitiaRequestDate().after(participantTO.getParticipantRequestTO().getEndRequestDate())){
	    		JSFUtilities.addContextMessage("frmGeneral:calBeginDate",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE));
	    		showMessageOnDialog(
	    				PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
	    	}
    		
    		//Validations of end dare not less than initial date
    		if(participantTO.getParticipantRequestTO().getEndRequestDate().before(participantTO.getParticipantRequestTO().getInitiaRequestDate())){
	    		JSFUtilities.addContextMessage("frmGeneral:calEndDate",
	    				FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_LESS_INI_DATE), 
	    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_LESS_INI_DATE));
	    		showMessageOnDialog(
	    				PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_LESS_INI_DATE,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return;
	    	}
    		
    		List<Integer> lstParticipantRequestTypes = new ArrayList<Integer>();
    		lstParticipantRequestTypes.add(ParticipantRequestType.BLOCK.getCode());
    		lstParticipantRequestTypes.add(ParticipantRequestType.UNBLOCK.getCode());
    		lstParticipantRequestTypes.add(ParticipantRequestType.ANNULMENT.getCode());
    		
    		participantTO.getParticipantRequestTO().setLstParticipantRequestTypes(lstParticipantRequestTypes);
    		participantTO.getParticipantRequestTO().setRequiredFullInformation(Boolean.TRUE);

    		
    		//If validations are OK, list the participants requests by filters
			participantRequestDataModel = new ParticipantRequestDataModel(participantServiceFacade.getListParticipantRequestBlockUnblockAnnull(participantTO));
			
			indDisableConfirnReject = true;
			
			participantRequest = new ParticipantRequest();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void changeSelectedParticipantRequest(SelectEvent selectEvent) {
		if(ParticipantRequestStateType.REGISTERED.getCode().equals(participantRequest.getStateRequest())) {
			indDisableConfirnReject = false;
		} else {
			indDisableConfirnReject = true;
		}
	}
	
	/**
	 * Return participant request mgmt.
	 */
	public void returnParticipantRequestMgmt(){
		participantRequest = new ParticipantRequest();
		indDisableConfirnReject = true;
	}
	
	/**
	 * Removes the file sustent.
	 *
	 * @param participantFileRemove the participant file remove
	 */
	public void removeFileSustent(ParticipantFile participantFileRemove){
		lstParticipantFileSustent.remove(participantFileRemove);
		participantFileNameDisplay = null;
	}
	
	/**
	 * Detail participant request.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailParticipantRequest(){
		try {
			
		setViewOperationType(ViewOperationsType.DETAIL.getCode());	
		
		ParticipantRequestTO participantRequestTO = new ParticipantRequestTO();
		participantRequestTO.setIdParticipantRequestPk(idParticipantRequestPk);
		
		participantRequest = participantServiceFacade.findParticipantRequestByIdServiceFacade(participantRequestTO);
		
		Participant participantFilter = new Participant();
		participantFilter.setIdParticipantPk(participantRequest.getParticipant().getIdParticipantPk());
		
		Participant participantResult = participantServiceFacade.findParticipantByFilters(participantFilter);
		
		participantRequest.setParticipant(participantResult);
		
		listParticipantRequestTypes();
		changeSelectedRequestType();

		return "participantBlockDetailRule";
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Detail participant request for confirm.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailParticipantRequestForConfirm(){
		try{
			hideDialogsListener(null);
			
			if(validateConfirmParticipantRequest()){
				idParticipantRequestPk = participantRequest.getIdParticipantRequestPk();
				detailParticipantRequest();
				setViewOperationType(ViewOperationsType.CONFIRM.getCode());
				return "participantBlockDetailRule";
			} else {
				return null;
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Begin confirm participant request.
	 */
	@LoggerAuditWeb
	public void beginConfirmParticipantRequest(){
		try{
			hideDialogsListener(null);
			
			if(validateConfirmParticipantRequest()){
				JSFUtilities.showComponent("cnfParticipantRequestConfirmation");
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Validate confirm participant request.
	 *
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	private boolean validateConfirmParticipantRequest() throws ServiceException{
		
		if(Validations.validateIsNull(participantRequest)){
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_RECORD_NOT_SELECTED,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return false;
		}
		
		Participant participantFilter = new Participant();
		participantFilter.setIdParticipantPk(participantRequest.getParticipant().getIdParticipantPk());
		
		Participant participantResult = participantServiceFacade.findParticipantByFilters(participantFilter);
		
		if(ParticipantRequestType.BLOCK.getCode().equals(participantRequest.getRequestType())){
			if(!ParticipantStateType.REGISTERED.getCode().equals(participantResult.getState())){
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return false;
			}
		}
		
		if(ParticipantRequestType.UNBLOCK.getCode().equals(participantRequest.getRequestType())){
			if(!ParticipantStateType.BLOCKED.getCode().equals(participantResult.getState())){
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_BLOCKED,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return false;
			}
		}
		
		if(ParticipantRequestType.ANNULMENT.getCode().equals(participantRequest.getRequestType())){
			if(!(ParticipantStateType.REGISTERED.getCode().equals(participantResult.getState()) ||
					ParticipantStateType.BLOCKED.getCode().equals(participantResult.getState())) ){
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_ALERT,
	    				null,
	    				PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED_OR_BLOCKED,							
	    				null);
	    		JSFUtilities.showSimpleValidationDialog();
	    		return false;
			}
		}
		
		ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequest.getRequestType());
		Object[] arrBodyData = {WordUtils.capitalizeFully(paramRequestType.getParameterName()), String.valueOf(participantRequest.getRequestNumber())};
		showMessageOnDialog(
				PropertiesConstants.MESSAGES_CONFIRM,
				null,
				PropertiesConstants.LBL_CONFIRM_PARTICIPANT_REQUEST_CONFIRMATION,							
				arrBodyData);
		/*** Validating if there is Participants accounts with any balance ***/
		if(this.getParticipantRequest().getRequestType().equals(ParticipantRequestType.ANNULMENT.getCode())){
			HolderAccountTO preHolderAccountTO = new HolderAccountTO();				
			preHolderAccountTO.setParticipantTO(participantRequest.getParticipant().getIdParticipantPk());
			//Gettings HolderAccounts from participant
			List<HolderAccountBalance> holderAccountBalances = this.participantServiceFacade.getListAccountBalancesByParticipantServiceFacade(preHolderAccountTO);
			if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalances)){
				
				for(HolderAccountBalance holderAccountBalance : holderAccountBalances){
					if(holderAccountBalance.getTransitBalance().compareTo(BigDecimal.ZERO)         > 0 || 
					   holderAccountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO)   > 0 ||
					   holderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO)       > 0 ||	
					   holderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) 			   > 0 ||
					   holderAccountBalance.getBorrowerBalance().compareTo(BigDecimal.ZERO) 	   > 0 ||
					   holderAccountBalance.getPurchaseBalance().compareTo(BigDecimal.ZERO)             > 0 ||						   
					   holderAccountBalance.getLenderBalance().compareTo(BigDecimal.ZERO) 	   	   > 0 ||
					   holderAccountBalance.getLoanableBalance().compareTo(BigDecimal.ZERO)    	   > 0 ||
					   holderAccountBalance.getMarginBalance().compareTo(BigDecimal.ZERO)      	   > 0 ||
					   holderAccountBalance.getOppositionBalance().compareTo(BigDecimal.ZERO)  	   > 0 ||
					   holderAccountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO)  	   > 0 ||
					   holderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO)            > 0 ||						   
					   holderAccountBalance.getReportedBalance().compareTo(BigDecimal.ZERO)    	   > 0 ||
					   holderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO)   	   > 0 ||
					   holderAccountBalance.getReserveBalance().compareTo(BigDecimal.ZERO)         > 0 ||
					   holderAccountBalance.getSaleBalance().compareTo(BigDecimal.ZERO)            > 0 ||
					   holderAccountBalance.getTotalBalance().compareTo(BigDecimal.ZERO)           > 0){
						
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ERROR_PARTICIPANT_ACCOUNTS_WITH_BALANCE_CONF,null);
			    		JSFUtilities.showSimpleValidationDialog();
			    		return false;
						
					}
				}
			}
		}
		
		return true;
		
	}
	
	/**
	 * Save confirm participant request.
	 */
	@LoggerAuditWeb
	public void saveConfirmParticipantRequest(){
		try {
			hideDialogsListener(null);
			
			if(ParticipantRequestType.ANNULMENT.getCode().equals(participantRequest.getRequestType())) {
				
				HolderAccountTO preHolderAccountTO = new HolderAccountTO();				
				preHolderAccountTO.setParticipantTO(participantRequest.getParticipant().getIdParticipantPk());
				//Gettings HolderAccounts from participant
				List<HolderAccountBalance> holderAccountBalances = this.participantServiceFacade.getListAccountBalancesByParticipantServiceFacade(preHolderAccountTO);
				if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalances)){
					
					for(HolderAccountBalance holderAccountBalance : holderAccountBalances){
						if(holderAccountBalance.getTransitBalance().compareTo(BigDecimal.ZERO)         > 0 || 
						   holderAccountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO)   > 0 ||
						   holderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO)       > 0 ||	
						   holderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) 			   > 0 ||
						   holderAccountBalance.getBorrowerBalance().compareTo(BigDecimal.ZERO) 	   > 0 ||
						   holderAccountBalance.getPurchaseBalance().compareTo(BigDecimal.ZERO)             > 0 ||						   
						   holderAccountBalance.getLenderBalance().compareTo(BigDecimal.ZERO) 	   	   > 0 ||
						   holderAccountBalance.getLoanableBalance().compareTo(BigDecimal.ZERO)    	   > 0 ||
						   holderAccountBalance.getMarginBalance().compareTo(BigDecimal.ZERO)      	   > 0 ||
						   holderAccountBalance.getOppositionBalance().compareTo(BigDecimal.ZERO)  	   > 0 ||
						   holderAccountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO)  	   > 0 ||
						   holderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO)            > 0 ||						   
						   holderAccountBalance.getReportedBalance().compareTo(BigDecimal.ZERO)    	   > 0 ||
						   holderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO)   	   > 0 ||
						   holderAccountBalance.getReserveBalance().compareTo(BigDecimal.ZERO)         > 0 ||
						   holderAccountBalance.getSaleBalance().compareTo(BigDecimal.ZERO)            > 0 ||
						   holderAccountBalance.getTotalBalance().compareTo(BigDecimal.ZERO)           > 0){
							
							showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.ERROR_PARTICIPANT_ACCOUNTS_WITH_BALANCE_CONF,null);
				    		JSFUtilities.showSimpleValidationDialog();
				    		return;
							
						}
					}
				}
				
				participantServiceFacade.registerBatchconfirmParticipantAnnullment(participantRequest);	
			    
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.LBL_SUCCESS_PARTICIPANT_ANULLMENT_BEGIN,null);					

			} else {
				participantServiceFacade.confirmParticipantBlockUnblockAnnullServiceFacade(participantRequest);
				
				ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequest.getRequestType());
				Object[] arrBodyData = {WordUtils.capitalizeFully(paramRequestType.getParameterName()), String.valueOf(participantRequest.getRequestNumber())};
				showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null,PropertiesConstants.LBL_SUCCESS_PARTICIPANT_REQUEST_CONFINRMATION,arrBodyData);					

			}
			
			//Sending notification
			BusinessProcess businessProcessNotification = new BusinessProcess();
			if(ParticipantRequestType.BLOCK.getCode().equals(participantRequest.getRequestType())) {
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BLOCK_PARTICIPANT_CONFIRM.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															businessProcessNotification, 
															participantRequest.getParticipant().getIdParticipantPk(), null);
				
	        } else if(ParticipantRequestType.UNBLOCK.getCode().equals(participantRequest.getRequestType())) {
	        	businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_PARTICIPANT_CONFIRM.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															businessProcessNotification, 
															participantRequest.getParticipant().getIdParticipantPk(), null);
	        } else if(ParticipantRequestType.ANNULMENT.getCode().equals(participantRequest.getRequestType())) {
	        	businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.CANCEL_PARTICIPANT_CONFIRM.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															businessProcessNotification, 
															participantRequest.getParticipant().getIdParticipantPk(), null);
	        }
								    
			cleanSearchParticipantRequest();
						
			JSFUtilities.showComponent("cnfEndTransactionParticipant");				
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			
//			if(transactionResult){
//					
//			}			
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Change motive reject.
	 */
	@LoggerAuditWeb
	public void changeMotiveReject(){
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectPartRequest");
		if(PartBlockUnblockAnullRejectMotiveType.OTHER_MOTIVE_REJECT.getCode().equals(participantRequest.getRejectedMotive())){
			indSelectedOtherMotiveReject = Boolean.TRUE;
			indDisableAcceptMotiveReject = Boolean.TRUE;
		} else {
			indSelectedOtherMotiveReject = Boolean.FALSE;
			if(Validations.validateIsNotNullAndPositive(participantRequest.getRejectedMotive())){
				indDisableAcceptMotiveReject = Boolean.FALSE;
			} else {
				indDisableAcceptMotiveReject = Boolean.TRUE;
			}			
		}
		participantRequest.setRejectOtherMotive(null);		
	}
	
	/**
	 * Detail participant request for reject.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailParticipantRequestForReject(){
		try{
			hideDialogsListener(null);
			
			if(validateRejectParticipantRequest()){
				idParticipantRequestPk = participantRequest.getIdParticipantRequestPk();
				detailParticipantRequest();
				setViewOperationType(ViewOperationsType.REJECT.getCode());
				return "participantBlockDetailRule";
			} else {
				return null;
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Begin reject participant request.
	 */
	@LoggerAuditWeb
	public void beginRejectParticipantRequest(){
		try {
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectPartRequest");
		
		if(validateRejectParticipantRequest()){
			listParticipantRequestRejectMotives();
			
			participantRequest.setRejectedMotive(null);
			participantRequest.setRejectOtherMotive(null);
			
			indDisableAcceptMotiveReject = Boolean.TRUE;
			indSelectedOtherMotiveReject = Boolean.FALSE;
			
			JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveRejectPartRequest').show()");		
		}		
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
			
	}
	
	/**
	 * Validate reject participant request.
	 *
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	private boolean validateRejectParticipantRequest() throws ServiceException{
		if(Validations.validateIsNull(participantRequest)){
			showMessageOnDialog(
					PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.ERROR_RECORD_NOT_SELECTED,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return false;
		}
		return true;
	}
	
	/**
	 * List participant request reject motives.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listParticipantRequestRejectMotives() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.PARTICIPANT_BLOCK_UBBLOCK_ANNUL_REJECT_MOTIVE.getCode());
		lstParticipantRequestRejectMotive = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * Accept reject motive participant request.
	 */
	public void acceptRejectMotiveParticipantRequest(){
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectPartRequest");
		
		int countValidationRequiredErrors = 0;
    	
    	if(Validations.validateIsNullOrNotPositive(participantRequest.getRejectedMotive())){
    		JSFUtilities.addContextMessage("frmMotiveRejectPartRequest:cboMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	if(indSelectedOtherMotiveReject &&
    			Validations.validateIsNullOrEmpty(participantRequest.getRejectOtherMotive())){
    		JSFUtilities.addContextMessage("frmMotiveRejectPartRequest:txtOtherMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	
    	if(countValidationRequiredErrors > 0){
    		showMessageOnDialog(
    				PropertiesConstants.MESSAGES_ALERT,
    				null,
    				PropertiesConstants.MESSAGE_DATA_REQUIRED,							
    				null);
    		JSFUtilities.showSimpleValidationDialog();
    		return;
    	}
    	
    	Object[] arrBodyData = {String.valueOf(participantRequest.getRequestNumber())};
		showMessageOnDialog(
				PropertiesConstants.MESSAGES_REJECT,
				null,
				PropertiesConstants.LBL_CONFIRM_PARTICIPANT_REQUEST_REJECT,							
				arrBodyData);
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveRejectPartRequest').show()");
		JSFUtilities.executeJavascriptFunction("PF('cnfwParticipantRequestReject').show()");
    	
	}
	
	/**
	 * Save reject participant request.
	 */
	@LoggerAuditWeb
	public void saveRejectParticipantRequest(){
		try {
			hideDialogsListener(null);
			
			boolean transactionResult = participantServiceFacade.rejectParticipantBlockUnblockAnnullServiceFacade(participantRequest);
			
			if(transactionResult) {
				
				//Sending notification
				BusinessProcess businessProcessNotification = new BusinessProcess();
				if(ParticipantRequestType.BLOCK.getCode().equals(participantRequest.getRequestType())) {
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.BLOCK_PARTICIPANT_REJECT.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																participantRequest.getParticipant().getIdParticipantPk(), null);
					
		        } else if(ParticipantRequestType.UNBLOCK.getCode().equals(participantRequest.getRequestType())) {
		        	businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_PARTICIPANT_REJECT.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																participantRequest.getParticipant().getIdParticipantPk(), null);
		        } else if(ParticipantRequestType.ANNULMENT.getCode().equals(participantRequest.getRequestType())) {
		        	businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.CANCEL_PARTICIPANT_REJECT.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																participantRequest.getParticipant().getIdParticipantPk(), null);
		        }
				
				ParameterTable paramRequestType = generalParametersFacade.getParameterTableById(participantRequest.getRequestType());
				Object[] arrBodyData = {WordUtils.capitalizeFully(paramRequestType.getParameterName()), String.valueOf(participantRequest.getRequestNumber())};
				showMessageOnDialog(
						PropertiesConstants.MESSAGES_SUCCESFUL, null,
						PropertiesConstants.LBL_SUCCESS_PARTICIPANT_REQUEST_REJECT, 
						arrBodyData);					
				cleanSearchParticipantRequest();
								
				//JSFUtilities.showSimpleEndTransactionDialog("searchParticipantBlockUnblAnull");
				JSFUtilities.showComponent("cnfEndTransactionParticipant");	
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Type other motive reject.
	 */
	@LoggerAuditWeb
	public void typeOtherMotiveReject(){
		if(Validations.validateIsNotNullAndNotEmpty(participantRequest.getRejectOtherMotive())){
			indDisableAcceptMotiveReject = Boolean.FALSE;
		} else {
			indDisableAcceptMotiveReject = Boolean.TRUE; 
		}
	}
	
	/**
	 * Show participant history states.
	 */
	@LoggerAuditWeb
	public void showParticipantHistoryStates(){
		try {
			ParticipantHistoryStateTO participantHistoryStateTO = new ParticipantHistoryStateTO();
			participantHistoryStateTO.setIdParticipantPk(idParticipantPk);
			lstParticipantHistoryStates = participantServiceFacade.getListParticipantHistoryStatesServiceFacade(participantHistoryStateTO);
			
			JSFUtilities.executeJavascriptFunction("PF('dlgwParticipantHistoryStates').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}

	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	/**
	 * Gets the lst participant request type.
	 *
	 * @return the lst participant request type
	 */
	public List<ParameterTable> getLstParticipantRequestType() {
		return lstParticipantRequestType;
	}

	/**
	 * Sets the lst participant request type.
	 *
	 * @param lstParticipantRequestType the new lst participant request type
	 */
	public void setLstParticipantRequestType(
			List<ParameterTable> lstParticipantRequestType) {
		this.lstParticipantRequestType = lstParticipantRequestType;
	}

	/**
	 * Gets the lst participant request motive.
	 *
	 * @return the lst participant request motive
	 */
	public List<ParameterTable> getLstParticipantRequestMotive() {
		return lstParticipantRequestMotive;
	}

	/**
	 * Sets the lst participant request motive.
	 *
	 * @param lstParticipantRequestMotive the new lst participant request motive
	 */
	public void setLstParticipantRequestMotive(
			List<ParameterTable> lstParticipantRequestMotive) {
		this.lstParticipantRequestMotive = lstParticipantRequestMotive;
	}

	/**
	 * Gets the participant request.
	 *
	 * @return the participant request
	 */
	public ParticipantRequest getParticipantRequest() {
		return participantRequest;
	}

	/**
	 * Sets the participant request.
	 *
	 * @param participantRequest the new participant request
	 */
	public void setParticipantRequest(ParticipantRequest participantRequest) {
		this.participantRequest = participantRequest;
	}



	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the lst participant file sustent.
	 *
	 * @return the lst participant file sustent
	 */
	public List<ParticipantFile> getLstParticipantFileSustent() {
		return lstParticipantFileSustent;
	}

	/**
	 * Sets the lst participant file sustent.
	 *
	 * @param lstParticipantFileSustent the new lst participant file sustent
	 */
	public void setLstParticipantFileSustent(
			List<ParticipantFile> lstParticipantFileSustent) {
		this.lstParticipantFileSustent = lstParticipantFileSustent;
	}

	/**
	 * Gets the participant to.
	 *
	 * @return the participant to
	 */
	public ParticipantTO getParticipantTO() {
		return participantTO;
	}

	/**
	 * Sets the participant to.
	 *
	 * @param participantTO the new participant to
	 */
	public void setParticipantTO(ParticipantTO participantTO) {
		this.participantTO = participantTO;
	}


	/**
	 * Gets the lst participant document type.
	 *
	 * @return the lst participant document type
	 */
	public List<ParameterTable> getLstParticipantDocumentType() {
		return lstParticipantDocumentType;
	}

	/**
	 * Sets the lst participant document type.
	 *
	 * @param lstParticipantDocumentType the new lst participant document type
	 */
	public void setLstParticipantDocumentType(
			List<ParameterTable> lstParticipantDocumentType) {
		this.lstParticipantDocumentType = lstParticipantDocumentType;
	}

	/**
	 * Gets the lst participant request state.
	 *
	 * @return the lst participant request state
	 */
	public List<ParameterTable> getLstParticipantRequestState() {
		return lstParticipantRequestState;
	}

	/**
	 * Sets the lst participant request state.
	 *
	 * @param lstParticipantRequestState the new lst participant request state
	 */
	public void setLstParticipantRequestState(
			List<ParameterTable> lstParticipantRequestState) {
		this.lstParticipantRequestState = lstParticipantRequestState;
	}	

	/**
	 * Gets the participant request data model.
	 *
	 * @return the participant request data model
	 */
	public ParticipantRequestDataModel getParticipantRequestDataModel() {
		return participantRequestDataModel;
	}

	/**
	 * Sets the participant request data model.
	 *
	 * @param participantRequestDataModel the new participant request data model
	 */
	public void setParticipantRequestDataModel(
			ParticipantRequestDataModel participantRequestDataModel) {
		this.participantRequestDataModel = participantRequestDataModel;
	}

	/**
	 * Checks if is ind disable confirn reject.
	 *
	 * @return true, if is ind disable confirn reject
	 */
	public boolean isIndDisableConfirnReject() {
		return indDisableConfirnReject;
	}

	/**
	 * Sets the ind disable confirn reject.
	 *
	 * @param indDisableConfirnReject the new ind disable confirn reject
	 */
	public void setIndDisableConfirnReject(boolean indDisableConfirnReject) {
		this.indDisableConfirnReject = indDisableConfirnReject;
	}

	/**
	 * Gets the id participant request pk.
	 *
	 * @return the id participant request pk
	 */
	public Long getIdParticipantRequestPk() {
		return idParticipantRequestPk;
	}

	/**
	 * Sets the id participant request pk.
	 *
	 * @param idParticipantRequestPk the new id participant request pk
	 */
	public void setIdParticipantRequestPk(Long idParticipantRequestPk) {
		this.idParticipantRequestPk = idParticipantRequestPk;
	}

	/**
	 * Gets the lst participant request reject motive.
	 *
	 * @return the lst participant request reject motive
	 */
	public List<ParameterTable> getLstParticipantRequestRejectMotive() {
		return lstParticipantRequestRejectMotive;
	}

	/**
	 * Sets the lst participant request reject motive.
	 *
	 * @param lstParticipantRequestRejectMotive the new lst participant request reject motive
	 */
	public void setLstParticipantRequestRejectMotive(
			List<ParameterTable> lstParticipantRequestRejectMotive) {
		this.lstParticipantRequestRejectMotive = lstParticipantRequestRejectMotive;
	}

	/**
	 * Checks if is ind selected other motive reject.
	 *
	 * @return true, if is ind selected other motive reject
	 */
	public boolean isIndSelectedOtherMotiveReject() {
		return indSelectedOtherMotiveReject;
	}

	/**
	 * Sets the ind selected other motive reject.
	 *
	 * @param indSelectedOtherMotiveReject the new ind selected other motive reject
	 */
	public void setIndSelectedOtherMotiveReject(boolean indSelectedOtherMotiveReject) {
		this.indSelectedOtherMotiveReject = indSelectedOtherMotiveReject;
	}

	/**
	 * Checks if is ind disable accept motive reject.
	 *
	 * @return true, if is ind disable accept motive reject
	 */
	public boolean isIndDisableAcceptMotiveReject() {
		return indDisableAcceptMotiveReject;
	}

	/**
	 * Sets the ind disable accept motive reject.
	 *
	 * @param indDisableAcceptMotiveReject the new ind disable accept motive reject
	 */
	public void setIndDisableAcceptMotiveReject(boolean indDisableAcceptMotiveReject) {
		this.indDisableAcceptMotiveReject = indDisableAcceptMotiveReject;
	}

	/**
	 * Gets the lst participant history states.
	 *
	 * @return the lst participant history states
	 */
	public List<ParticipantHistoryState> getLstParticipantHistoryStates() {
		return lstParticipantHistoryStates;
	}

	/**
	 * Sets the lst participant history states.
	 *
	 * @param lstParticipantHistoryStates the new lst participant history states
	 */
	public void setLstParticipantHistoryStates(
			List<ParticipantHistoryState> lstParticipantHistoryStates) {
		this.lstParticipantHistoryStates = lstParticipantHistoryStates;
	}

	/**
	 * Gets the participant file name display.
	 *
	 * @return the participant file name display
	 */
	public String getParticipantFileNameDisplay() {
		return participantFileNameDisplay;
	}

	/**
	 * Sets the participant file name display.
	 *
	 * @param participantFileNameDisplay the new participant file name display
	 */
	public void setParticipantFileNameDisplay(String participantFileNameDisplay) {
		this.participantFileNameDisplay = participantFileNameDisplay;
	}

	public boolean isAllowSearchAll() {
		return allowSearchAll;
	}

	public void setAllowSearchAll(boolean allowSearchAll) {
		this.allowSearchAll = allowSearchAll;
	}		
		
}
