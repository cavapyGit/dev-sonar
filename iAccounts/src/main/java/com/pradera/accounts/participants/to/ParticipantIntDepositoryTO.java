package com.pradera.accounts.participants.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantIntDepositoryTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/02/2013
 */
public class ParticipantIntDepositoryTO implements Serializable{	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 471121268364421200L;

	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id international depository pk. */
	private Long idInternationalDepositoryPk;
	
	/** The state participant int depositary. */
	private Integer stateParticipantIntDepositary;
	
	/** The id participant request pk. */
	private Long idParticipantRequestPk;
	
	/** The ind new. */
	private Integer indNew;
	
	/**
	 * Instantiates a new participant int depository to.
	 */
	public ParticipantIntDepositoryTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id international depository pk.
	 *
	 * @return the id international depository pk
	 */
	public Long getIdInternationalDepositoryPk() {
		return idInternationalDepositoryPk;
	}

	/**
	 * Sets the id international depository pk.
	 *
	 * @param idInternationalDepositoryPk the new id international depository pk
	 */
	public void setIdInternationalDepositoryPk(Long idInternationalDepositoryPk) {
		this.idInternationalDepositoryPk = idInternationalDepositoryPk;
	}

	/**
	 * Gets the state participant int depositary.
	 *
	 * @return the state participant int depositary
	 */
	public Integer getStateParticipantIntDepositary() {
		return stateParticipantIntDepositary;
	}

	/**
	 * Sets the state participant int depositary.
	 *
	 * @param stateParticipantIntDepositary the new state participant int depositary
	 */
	public void setStateParticipantIntDepositary(
			Integer stateParticipantIntDepositary) {
		this.stateParticipantIntDepositary = stateParticipantIntDepositary;
	}

	/**
	 * Gets the id participant request pk.
	 *
	 * @return the id participant request pk
	 */
	public Long getIdParticipantRequestPk() {
		return idParticipantRequestPk;
	}

	/**
	 * Sets the id participant request pk.
	 *
	 * @param idParticipantRequestPk the new id participant request pk
	 */
	public void setIdParticipantRequestPk(Long idParticipantRequestPk) {
		this.idParticipantRequestPk = idParticipantRequestPk;
	}

	/**
	 * Gets the ind new.
	 *
	 * @return the ind new
	 */
	public Integer getIndNew() {
		return indNew;
	}

	/**
	 * Sets the ind new.
	 *
	 * @param indNew the new ind new
	 */
	public void setIndNew(Integer indNew) {
		this.indNew = indNew;
	}	
		
}
