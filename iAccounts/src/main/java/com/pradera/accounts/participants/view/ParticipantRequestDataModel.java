package com.pradera.accounts.participants.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.ParticipantRequest;;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ParticipantRequestDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/04/2013
 */
public class ParticipantRequestDataModel extends ListDataModel<ParticipantRequest> implements SelectableDataModel<ParticipantRequest>, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new participant request data model.
	 */
	public ParticipantRequestDataModel(){
		
	}
	
	/**
	 * Instantiates a new participant request data model.
	 *
	 * @param data the data
	 */
	public ParticipantRequestDataModel(List<ParticipantRequest> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public ParticipantRequest getRowData(String rowKey) {
		@SuppressWarnings("unchecked")
		List<ParticipantRequest> lstParticipantRequest = (List<ParticipantRequest>)getWrappedData();
        for(ParticipantRequest participantRequest : lstParticipantRequest ) {  
            if(String.valueOf(participantRequest.getIdParticipantRequestPk()).equals(rowKey))
                return participantRequest;  
        }
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(ParticipantRequest participant) {
		return participant.getIdParticipantRequestPk();
	}		
}