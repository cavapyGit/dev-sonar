/*
 * 
 */
package com.pradera.accounts.util.view;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
public class PropertiesConstants {

	/**
	 * Instantiates a new properties constants.
	 */
	private PropertiesConstants() {
		super();
	}
	// issue 605: navegacion de Anulacion de CUI (search,register,view)
	public static final String PAGE_REGISTER="newHolderAnnulate";
	public static final String PAGE_SEARCH="searchHolderAnnulate";
	public static final String PAGE_VIEW="viewHolderAnnulate";
	
	public static final String MSG_OPERATION_NO_PROCESS_FOR_CONCURRENCE="msg.operation.no.process.for.concurrence";
	
	public static final String LBL_MSG_CONFIRM_APPROVED_ANNULATECUI="lbl.msg.confirm.approved.annulateCui";
	
	public static final String MSG_SUCCESFUL_APPROVED_ANNULATECUI="msg.succesful.approved.annulateCui";
	
	public static final String LBL_MSG_CONFIRM_ANNULATE_ANNULATECUI="lbl.msg.confirm.annulate.annulateCui";
	
	public static final String LBL_MSG_CONFIRM_REVIEW_ANNULATECUI="lbl.msg.confirm.review.annulateCui";
	
	public static final String LBL_MSG_CONFIRM_REJECT_ANNULATECUI="lbl.msg.confirm.reject.annulateCui";
	
	public static final String LBL_MSG_CONFIRM_CONFIRMED_ANNULATECUI="lbl.msg.confirm.confirmed.annulateCui";
	
	public static final String MSG_SUCCESFUL_ANULATE_ANNULATECUI="msg.succesful.anulate.annulateCui";
	
	public static final String MSG_SUCCESFUL_REVIEWED_ANNULATECUI="msg.succesful.reviewed.annulateCui";
	
	public static final String MSG_SUCCESFUL_CONFIRMED_ANNULATECUI="msg.succesful.confirmed.annulateCui";
	
	public static final String MSG_SUCCESFUL_REJECTED_ANNULATECUI="msg.succesful.rejected.annulateCui";
	
	public static final String MSG_ERROR_ANNULATECUI_REQUIRED_STATEREGISTERED="msg.error.annulateCui.required.stateRegistered";
	
	public static final String MSG_ERROR_ANNULATECUI_REQUIRED_FILEUPLOAD="msg.error.annulateCui.required.fileUpload";
	
	public static final String MSG_ERROR_ANNULATECUI_REQUIRED_CUISTATEREGISTERED="msg.error.annulateCui.required.cuiStateRegistered";
	
	public static final String MSG_ERROR_ANNULATECUI_REQUIRED_STATEAPPROVED="msg.error.annulateCui.required.stateApproved";
	
	public static final String MSG_ERROR_ANNULATECUI_REQUIRED_STATEREVIEWED="msg.error.annulateCui.required.stateReviewed";
	
	public static final String MSG_ERROR_ANNULATECUI_REQUIRED_STATEREVIEWEDORAPPROVED="msg.error.annulateCui.required.stateReviewedOrApproved";
	
	public static final String MSG_ERROR_ANNULATECUI_CUI_IS_REQUESTBLOCK="msg.error.annulateCui.cui.is.requestBlock";
	
	public static final String MSG_ERROR_ANNULATECUI_CUI_IS_REQUESTUNBLOCK="msg.error.annulateCui.cui.is.requestUnBlock";
	
	public static final String MSG_ERROR_ANNULATECUI_CUI_IS_REQUESTANNULATION="msg.error.annulateCui.cui.is.requestAnnulation";
	
	public static final String MSG_ERROR_ANNULATECUI_CUI_HAVE_BALANCE="msg.error.annulateCui.cui.have.balance";
	
	public static final String MSG_ERROR_ANNULATECUI_REQUIRED_SELECTED="msg.error.annulateCui.required.selected";
	
	public static final String LBL_MSG_CONFIRM_REGITER_ANNULATECUI="lbl.msg.confirm.regiter.annulateCui";
	
	public static final String MSG_SUCCESFUL_REGISTER_ANNULATECUI="msg.succesful.register.annulateCui";
	
	/** The Constant ALERT_NUMBERDOCUMENT_SIZE9_CANT_STARTWITH0. */
	public static final String ALERT_NUMBERDOCUMENT_SIZE9_CANT_STARTWITH0 = "alert.numberDocument.size.nine.cant.startwith.zero";
	
	/** The Constant ALERT_NUMBERDOCUMENT_SIZE_LESS6. */
	public static final String ALERT_NUMBERDOCUMENT_SIZE_LESS6 = "alert.numberDocument.size.less.than.sixe";
	
	/** The Constant TAMANIO_FILE. */
	public static final String TAMANIO_FILE = "tamanio.file";
	
	/** The Constant MESSAGE_DATA_REQUIRED. */
	public static final String MESSAGE_DATA_REQUIRED = "message.data.required";
	
	/** The Constant BANK ACCOUNTS VALIDATE ACCOUNT NUMBER CURRENCY. */
	public static final String BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER_CURRENCY="msg.banks.accounts.validate.account.number.currency";
	
	/** The Constant BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER. */
	public static final String BANK_ACCOUNTS_VALIDATE_ACCOUNT_NUMBER="msg.banks.accounts.validate.account.number";
	
	/** The Constant BANK_ACCOUNTS_REMOVE_CONFIRM. */
	public static final String BANK_ACCOUNTS_REMOVE_CONFIRM = "msg.banks.accounts.remove.confirm";
	
	/** The Constant BANK_ACCOUNTS_DELETE_BANK_ACCOUNT. */
	public static final String BANK_ACCOUNTS_DELETE_BANK_ACCOUNT = "msg.banks.accounts.delete.bank.account";
	
	/** The Constant ERROR_RECORD_NOT_SELECTED. */
	public static final String ERROR_RECORD_NOT_SELECTED = "error.record.not.selected";
	
	/** The Constant ERROR_RECORD_SELECTED_NOT_REGISTERED_STATE. */
	public static final String ERROR_RECORD_SELECTED_NOT_REGISTERED_STATE = "error.record.selected.not.registered.state";

	/** The Constant LBL_CONFIRM_HEADER_RECORD. */
	public static final String LBL_CONFIRM_HEADER_RECORD = "lbl.confirm.header.record";

	/** The Constant ADM_REQUEST_HOLDER_CONFIRM_REGISTER. */
	public static final String ADM_REQUEST_HOLDER_CONFIRM_REGISTER = "adm.request.holder.confirm.register";
	
	/** The Constant ADM_ASSETLAUNDRY_PNA_CONFIRM_REGISTER. */
	public static final String ADM_ASSETLAUNDRY_PNA_CONFIRM_REGISTER = "adm.assetlaundry.register.confirmMessagepna";
	
	/** The Constant ADM_ASSET_LAUNDRY_CONFIRM_CLEAR_MESSAGE. */
	public static final String ADM_ASSET_LAUNDRY_CONFIRM_CLEAR_MESSAGE="adm.assetlaundry.confirm.clear.message";
	
	/** The Constant ADM_ASSET_LAUNDRY_CONFIRM_CROSS_INFORMATION_PROCESS. */
	public static final String ADM_ASSET_LAUNDRY_CONFIRM_CROSS_INFORMATION_PROCESS ="adm.assetlaundry.confirmCrossInformation";
	
	/** The Constant ADM_ASSET_LAUNDRY_CONFIRMED_CORSS_INFORMATION_PROCESS. */
	public static final String ADM_ASSET_LAUNDRY_CONFIRMED_CORSS_INFORMATION_PROCESS="adm.assetlaundry.confirmedCrossInformation";
	
	/** The Constant ADM_ASSETLAUNDRY_SELECT_PERSON. */
	public static final String ADM_ASSETLAUNDRY_SELECT_PERSON = "adm.assetlaundry.search.selectAssetLaundryPerson";
	
	/** The Constant ADM_ASSETLAUNDRY_PEP_CONFIRM_REGISTER. */
	public static final String ADM_ASSETLAUNDRY_PEP_CONFIRM_REGISTER = "adm.assetlaundry.register.confirmMessagepep";
	
	/** The Constant ADM_ASSETLAUNDRY_PNA_REGISTER_OK. */
	public static final String ADM_ASSETLAUNDRY_PNA_REGISTER_OK = "adm.assetlaundry.register.confirmedMessagepna";
	
	/** The Constant ADM_ASSETLAUNDRY_PEP_REGISTER_OK. */
	public static final String ADM_ASSETLAUNDRY_PEP_REGISTER_OK = "adm.assetlaundry.register.confirmedMessagepep";

	/** The Constant ADM_ASSETLAUNDRY_OFAC_FILE_TYPE. */
	public static final String ADM_ASSETLAUNDRY_OFAC_FILE_TYPE = "adm.assetlaundry.register.confirmfProcessFileOfac";
	
	/** The Constant ADM_ASSETLAUNDRY_PNA_FILE_TYPE. */
	public static final String ADM_ASSETLAUNDRY_PNA_FILE_TYPE = "adm.assetlaundry.register.confirmfProcessFilePna";
	
	/** The Constant ADM_ASSETLAUNDRY_INVALID_FILE_TYPE. */
	public static final String ADM_ASSETLAUNDRY_INVALID_FILE_TYPE = "adm.assetlaundry.register.invalidFileType";
	
	/** The Constant ADM_ASSETLAUNDRY_SUCCESS_UPLOAD. */
	public static final String ADM_ASSETLAUNDRY_SUCCESS_UPLOAD = "adm.assetlaundry.register.successFileUpload";
	
	/** The Constant ADM_ASSETLAUNDRY_PFAC_FILE_OK. */
	public static final String ADM_ASSETLAUNDRY_PFAC_FILE_OK="adm.assetlaundry.register.confirmedfProcessFileOfac";
	
	/** The Constant ADM_ASSETLAUNDRY_PFAC_FILE_OK. */
	public static final String ADM_ASSETLAUNDRY_OFAC_FILE_REJECT="adm.assetlaundry.massiveRegister.failedProcessFileOfac";
	
    /** The Constant WARNING_OPERATION_CONFIRM_INVALID. */
    public static final String  WARNING_OPERATION_CONFIRM_INVALID = "warning.operation.confirm.invalid";
	
	/** The Constant WARNING_OPERATION_REJECT_INVALID. */
	public static final String  WARNING_OPERATION_REJECT_INVALID = "warning.operation.reject.invalid";
	
	/** The Constant WARNING_OPERATION_ANNULAR_INVALID. */
	public static final String  WARNING_OPERATION_ANNULAR_INVALID = "warning.operation.annular.invalid";
	
	/** The Constant LBL_OTHER_MOTIVE. */
	public static final String  LBL_OTHER_MOTIVE = "lbl.other.motive";
	
	/** The Constant LBL_CONFIRM_ACTION. */
	public static final String LBL_CONFIRM_ACTION = "lbl.confirm.action";
	
	/** The Constant LBL_REGISTRY_HOLDER_REQUEST. */
	public static final String LBL_REGISTRY_HOLDER_REQUEST = "lbl.registry.holder.request";
	
	/** The Constant LBL_CONFIRM_HOLDER_REQUEST. */
	public static final String LBL_CONFIRM_HOLDER_REQUEST="lbl.confirm.holder.request";
	
	/** The Constant LBL_REJECT_HOLDER_REQUEST_PARTICIPANT_BLOCKED. */
	public static final String LBL_REJECT_HOLDER_REQUEST_PARTICIPANT_BLOCKED="adm.holder.register.participant.blocked";
	
	/** The Constant LBL_REJECT_HOLDER_REQUEST. */
	public static final String LBL_REJECT_HOLDER_REQUEST="lbl.reject.holder.request";
	
	/** The Constant LBL_ANNULAR_HOLDER_REQUEST. */
	public static final String LBL_ANNULAR_HOLDER_REQUEST="lbl.annular.holder.request";
	
	/** The Constant LBL_SUCCESS_CONFIRM_HOLDER_REQUEST. */
	public static final String LBL_SUCCESS_CONFIRM_HOLDER_REQUEST="lbl.success.confirm.holder.request";
	
	/** The Constant LBL_SUCCESS_REJECT_HOLDER_REQUEST. */
	public static final String LBL_SUCCESS_REJECT_HOLDER_REQUEST="lbl.success.reject.holder.request";
	
	/** The Constant LBL_SUCCESS_ANNUL_HOLDER_REQUEST. */
	public static final String LBL_SUCCESS_ANNUL_HOLDER_REQUEST="lbl.success.annul.holder.request";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_BANKACCOUNTS. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_BANKACCOUNTS="adm.holderaccount.validation.bankaccounts";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_QUANTITYFILE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_QUANTITYFILE="adm.holderaccount.validation.quantityfile";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_CURRENCY. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_CURRENCY="adm.holderaccount.validation.currency";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_REQUEST_EXIST. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_REQUEST_EXIST="adm.holderaccount.validation.request.exist";
	
	/** The Constant ADM_HOLDERACCOUNT_BLOCK_VALIDATION_OBSERVATION. */
	public static final String ADM_HOLDERACCOUNT_BLOCK_VALIDATION_OBSERVATION="adm.holderaccount.block.validation.obsersation";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_BANKACCOUNTSMODIFY. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_BANKACCOUNTSMODIFY="adm.holderaccount.validation.bankaccountsmodify";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_CURRENCE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_CURRENCE="adm.holderaccount.validation.currence";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_BANK_CHANGES. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_BANK_CHANGES="adm.holderaccount.validation.bank.changes";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDERNULL. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDERNULL="adm.holderaccount.validation.holdernull";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_ACCOUNTNUMBER. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_ACCOUNTNUMBER="adm.holderaccount.validation.accountnumber";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_BLOCKTYPE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_BLOCKTYPE="adm.holderaccount.validation.blocktype";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK="adm.holderaccount.validation.holder.block";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_REPETED. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_REPETED="adm.holderaccount.validation.holder.repeted";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_TYPE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_TYPE="adm.holderaccount.validation.holder.type";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_EMPTY. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_ACCOUNT_EMPTY="adm.holderaccount.validation.holderaccount.empty";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_EMPTY. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_EMPTY="adm.holderaccount.validation.holder.empty";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_DOACTION. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_DOACTION= "adm.holderaccount.validation.doaction";

	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_MAIN. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_MAIN="adm.holderaccount.validation.holder.main";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_ONE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_ONE="adm.holderaccount.validation.holder.one";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_BANK_EMPTY. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_BANK_EMPTY="adm.holderaccount.validation.bank.empty";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_DATA_EMPTY. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_DATA_EMPTY="adm.holderaccount.validation.data.empty";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_FILE_EMPTY. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_FILE_EMPTY="adm.holderaccount.validation.file.empty";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER="adm.holderaccount.confirmation.register";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER_OK. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER_OK="adm.holderaccount.confirmation.register.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_MODIFY. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_MODIFY="adm.holderaccount.confirmation.modify";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_MODIFY_OK. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_MODIFY_OK="adm.holderaccount.confirmation.modify.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT="adm.holderaccount.validation.holderaccount";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_CLOSE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_CLOSE="adm.holderaccount.validation.holderaccount.close";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_NULL. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_NULL="adm.holderaccount.validation.holderaccount.null";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_BLOCK="adm.holderaccount.confirmation.block";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_BLOCK_OK. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_BLOCK_OK="adm.holderaccount.confirmation.block.ok";

	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_UNBLOCK. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_UNBLOCK="adm.holderaccount.confirmation.unblock";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_UNBLOCK_OK. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_UNBLOCK_OK="adm.holderaccount.confirmation.unblock.ok";
			
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_CLOSE. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_CLOSE="adm.holderaccount.confirmation.close";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_CLOSE_OK. */
	public static final String ADM_HOLDERACCOUNT_CONFIRMATION_CLOSE_OK="adm.holderaccount.confirmation.close.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_ANULATE. */
	public static final String ADM_HOLDERACCOUNT_ANULATE="adm.holderaccount.anulate";
	
	/** The Constant ADM_HOLDERACCOUNT_ANULATE_OK. */
	public static final String ADM_HOLDERACCOUNT_ANULATE_OK="adm.holderaccount.anulate.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_REJECT. */
	public static final String ADM_HOLDERACCOUNT_REJECT="adm.holderaccount.reject";
	
	/** The Constant ADM_HOLDERACCOUNT_REJECT_OK. */
	public static final String ADM_HOLDERACCOUNT_REJECT_OK="adm.holderaccount.reject.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRM. */
	public static final String ADM_HOLDERACCOUNT_CONFIRM="adm.holderaccount.confirm";
	
	/** The Constant ADM_HOLDERACCOUNT_APPROVE. */
	public static final String ADM_HOLDERACCOUNT_APPROVE="adm.holderaccount.approve";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_CLASSTYPE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_CLASSTYPE="adm.holderaccount.validation.classtype";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_PARTICIPANT="adm.holderaccount.validation.participant";

	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_FILEUPLOAD. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_FILEUPLOAD="adm.holderaccount.validation.fileupload";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_OK="adm.holderaccount.request.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_CONFIRM_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_CONFIRM_OK="adm.holderaccount.request.confirm.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER_MODIFY_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_CONFIRM_MODIFY_OK="adm.holderaccount.request.confirm.modify.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK="adm.holderaccount.request.anulate.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_REJECT_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_REJECT_OK="adm.holderaccount.request.reject.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK="adm.holderaccount.request.approve.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER_BLOCK_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_CONFIRM_BLOCK_OK="adm.holderaccount.request.confirm.block.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER_UNBLOCK_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_CONFIRM_UNBLOCK_OK="adm.holderaccount.request.confirm.unblock.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRMATION_REGISTER_CLOSED_OK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_CONFIRM_CLOSED_OK="adm.holderaccount.request.confirm.closed.ok";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_NUMBER. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_NUMBER="adm.holderaccount.validation.number";
	
	/** The Constant OTC_OPERATION_VALIDATION_CLOSE_WITH_BALANCES. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES="adm.holderaccount.validation.close.with.balances";
	
	/** The Constant MESSAGES_SUCCESFUL. */
	public static final String MESSAGES_SUCCESFUL="messages.succesful";
	
	/** The Constant MESSAGES_REGISTER. */
	public static final String MESSAGES_REGISTER="messages.register";
	
	/** The Constant MESSAGES_CONFIRM. */
	public static final String MESSAGES_CONFIRM="messages.confirm";
	
	/** The Constant MESSAGES_REJECT. */
	public static final String MESSAGES_REJECT="messages.reject";
	
	/** The Constant MESSAGES_ALERT. */
	public static final String MESSAGES_ALERT="messages.alert";
	
	/** The Constant MESSAGES_WARNING. */
	public static final String MESSAGES_WARNING="messages.warning";
	
	/** The Constant HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK. */
	public static final String HOLDERACCOUNT_VALIDATION_HOLDER_BLOCK="adm.holderaccount.validation.holder.block.empty";
	
	/*
	 * Messages for Participant
	 */
	/** The Constant ERROR_ADM_PARTICIPANT_DOC_NUM_MAX_9_CHAR. */
	public static final String ERROR_ADM_PARTICIPANT_DOC_NUM_MAX_9_CHAR = "error.adm.participant.documentnumber.max9.characters";
	
	/** The Constant ERROR_ADM_PARTICIPANT_DOC_NUM_MAX_20_CHAR. */
	public static final String ERROR_ADM_PARTICIPANT_DOC_NUM_MAX_20_CHAR = "error.adm.participant.documentnumber.max20.characters";
	
	/** The Constant ERROR_ADM_PARTICIPANT_NO_LEGAL_REPRESENTATIVE. */
	public static final String ERROR_ADM_PARTICIPANT_NO_LEGAL_REPRESENTATIVE = "error.adm.participant.no.legal.representative";
	
	/** The Constant ERROR_ADM_PARTICIPANT_NO_INT_DEPOSITORY. */
	public static final String ERROR_ADM_PARTICIPANT_NO_INT_DEPOSITORY = "error.adm.participant.no.int.depository";
	
	/** The Constant ERROR_ADM_PARTICIPANT_NO_NEGOTIATION_MECHANISM. */
	public static final String ERROR_ADM_PARTICIPANT_NO_NEGOTIATION_MECHANISM = "error.adm.participant.no.negotiation.mechanism";
	
	/** The Constant ERROR_ADM_PARTICIPANT_FILE_NO_COMPLETE. */
	public static final String ERROR_ADM_PARTICIPANT_FILE_NO_COMPLETE = "error.adm.participant.file.no.complete";
	
	/** The Constant ERROR_ADM_PARTICIPANT_DOC_NUM_FOREIGN_REPEATED. */
	public static final String ERROR_ADM_PARTICIPANT_DOC_NUM_FOREIGN_REPEATED = "error.adm.participant.docnum.foreign.repeated";
	
	/** The Constant ERROR_ADM_PARTICIPANT_DOC_NUM_NATIONAL_REPEATED. */
	public static final String ERROR_ADM_PARTICIPANT_DOC_NUM_NATIONAL_REPEATED = "error.adm.participant.docnum.national.repeated";
	
	/** The Constant ERROR_ADM_PARTICIPANT_MNEMONIC_REPEATED. */
	public static final String ERROR_ADM_PARTICIPANT_MNEMONIC_REPEATED = "error.adm.participant.mnemonic.repeated";
	
	/** The Constant ERROR_ADM_PARTICIPANT_RNC_NOT_FOUND. */
	public static final String ERROR_ADM_PARTICIPANT_RNC_NOT_FOUND = "error.adm.participant.rnc.not.found";
	
	/** The Constant ADM_PARTICIPANT_HOLDER_WILL_BE_CREATED. */
	public static final String ADM_PARTICIPANT_HOLDER_WILL_BE_CREATED = "adm.participant.holder.will.be.created";
	
	/** The Constant ADM_PARTICIPANT_HOLDER_FOUND_BLOCKED. */
	public static final String ADM_PARTICIPANT_HOLDER_FOUND_BLOCKED = "adm.participant.holder.found.blocked";	
	
	/** The Constant ERROR_ADM_PARTICIPANT_DOC_NUM_NATIONAL_REPEATED. */
	public static final String ERROR_ADM_PARTICIPANT_INVALID_RNC_FORMAT = "error.adm.participant.invalid.rnc.format";
	
	/** The Constant LBL_CONFIRM_PARTICIPANT_REGISTER. */
	public static final String LBL_CONFIRM_PARTICIPANT_REGISTER = "lbl.confirm.participant.register";
	
	/** The Constant LBL_SUCCESS_PARTICIPANT_REGISTER. */
	public static final String LBL_SUCCESS_PARTICIPANT_REGISTER = "lbl.success.participant.register";
	
	/** The Constant LBL_SUCCESS_PARTICIPANT_AND_HOLDER_REGISTER. */
	public static final String LBL_SUCCESS_PARTICIPANT_AND_HOLDER_REGISTER = "lbl.success.participant.and.holder.register";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_ROLE_PARTICIPANT. */
	public static final String ADM_PARTICIPANT_VALIDATION_ROLE_PARTICIPANT = "adm.participant.validation.role.participant";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_PARTICIPANT_NOT_SELECTED. */
	public static final String ADM_PARTICIPANT_VALIDATION_PARTICIPANT_NOT_SELECTED = "adm.participant.validation.participant.not.selected";
	
	/** The Constant ADM_VALIDATION_BANK_ACCOUNT_NOT_SELECTED. */
	public static final String ADM_VALIDATION_BANK_ACCOUNT_NOT_SELECTED = "adm.validation.bank.account.not.selected";
	
	/** The Constant ADM_VALIDATION_BANK_ACCOUNT_NOT_DELETE. */
	public static final String ADM_VALIDATION_BANK_ACCOUNT_NOT_DELETE = "adm.validation.bank.account.not.delete";
	
	/** The Constant ADM_VALIDATION_BANK_ACCOUNT_NOT_MODIFY. */
	public static final String ADM_VALIDATION_BANK_ACCOUNT_NOT_MODIFY = "adm.validation.bank.account.not.modify";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_RESIDENCE_COUNTRY. */
	public static final String ADM_PARTICIPANT_VALIDATION_RESIDENCE_COUNTRY = "adm.participant.validation.residence.country";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_DOCUMENT_TYPE. */
	public static final String ADM_PARTICIPANT_VALIDATION_DOCUMENT_TYPE = "adm.participant.validation.document.type";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_DOCUMENT_NUMBER. */
	public static final String ADM_PARTICIPANT_VALIDATION_DOCUMENT_NUMBER = "adm.participant.validation.document.number";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_TRADE_NAME. */
	public static final String ADM_PARTICIPANT_VALIDATION_TRADE_NAME = "adm.participant.validation.trade.name";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_MNEMONIC. */
	public static final String ADM_PARTICIPANT_VALIDATION_MNEMONIC = "adm.participant.validation.mnemonic";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_ACCOUNT_TYPE. */
	public static final String ADM_PARTICIPANT_VALIDATION_ACCOUNT_TYPE = "adm.participant.validation.account.type";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_ACCOUNT_CLASS. */
	public static final String ADM_PARTICIPANT_VALIDATION_ACCOUNT_CLASS = "adm.participant.validation.account.class";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_ECONOM_SECTOR. */
	public static final String ADM_PARTICIPANT_VALIDATION_ECONOM_SECTOR = "adm.participant.validation.economic.sector";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_ECONOM_ACTIVITY. */
	public static final String ADM_PARTICIPANT_VALIDATION_ECONOM_ACTIVITY = "adm.participant.validation.economic.activity";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_CONTRACT_DATE. */
	public static final String ADM_PARTICIPANT_VALIDATION_CONTRACT_DATE = "adm.participant.validation.contract.date";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_CREATION_DATE. */
	public static final String ADM_PARTICIPANT_VALIDATION_CREATION_DATE = "adm.participant.validation.creation.date";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BIC_CODE. */
	public static final String ADM_PARTICIPANT_VALIDATION_BIC_CODE = "adm.participant.validation.bic.code";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACCOUNT_TYPE. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACCOUNT_TYPE = "adm.participant.validation.bank.account.type";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACCOUNT_CURRENCY. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACCOUNT_CURRENCY = "adm.participant.validation.bank.account.currency";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACCOUNT_NUMBER. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACCOUNT_NUMBER = "adm.participant.validation.bank.account.number";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_SAME_TYPE_CURRENCY. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_SAME_TYPE_CURRENCY = "adm.participant.validation.bank.same.acc.type.currency";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_SAME_ACC_NUMBER. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_SAME_ACC_NUMBER = "adm.participant.validation.bank.same.acc.number";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_NO_MORE_IND_BANK_ACC. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_NO_MORE_IND_BANK_ACC = "adm.participant.validation.bank.no.more.ind.bank.acc";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_NO_MORE_IND_HOLDER_ACC. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_NO_MORE_IND_HOLDER_ACC = "adm.participant.validation.bank.no.more.ind.holder.acc";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_SAME_CURR_IND_BANK_ACC. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_SAME_CURR_IND_BANK_ACC = "adm.participant.validation.bank.same.currency.ind.bank.acc";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_SAME_CURR_IND_HOLDE_ACC. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_SAME_CURR_IND_HOLDE_ACC = "adm.participant.validation.bank.same.currency.ind.holder.acc";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACC_NO_ADDED. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACC_NO_ADDED = "adm.participant.validation.bank.account.no.added";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACC_TOO_MUCH_ADD. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACC_TOO_MUCH_ADD = "adm.participant.validation.bank.account.too.much.added";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACC_NEED_IND_BANK. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACC_NEED_IND_BANK = "adm.participant.validation.bank.account.need.ind.bank";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACC_NEED_IND_HOLD_ACC. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACC_NEED_IND_HOLD_ACC = "adm.participant.validation.bank.account.need.ind.holder.account";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACC_MUCH_IND_BANK. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACC_MUCH_IND_BANK = "adm.participant.validation.bank.account.too.much.ind.bank";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACC_MUCH_IND_HOLD_ACC. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACC_MUCH_IND_HOLD_ACC = "adm.participant.validation.bank.account.too.much.ind.holder.account";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACC_NO_IND_BANK_CURR. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACC_NO_IND_BANK_CURR = "adm.participant.validation.bank.account.no.ind.bank.currency";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_ACC_NO_IND_HOLD_ACC_CURR. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_ACC_NO_IND_HOLD_ACC_CURR = "adm.participant.validation.bank.account.no.ind.holder.account.currency";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BANK_REMOVE_NO_SELECTED. */
	public static final String ADM_PARTICIPANT_VALIDATION_BANK_REMOVE_NO_SELECTED = "adm.participant.validation.bank.remove.no.selected";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_PROVINCE. */
	public static final String ADM_PARTICIPANT_VALIDATION_PROVINCE = "adm.participant.validation.province";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_SECTOR. */
	public static final String ADM_PARTICIPANT_VALIDATION_SECTOR = "adm.participant.validation.sector";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_ADDRESS. */
	public static final String ADM_PARTICIPANT_VALIDATION_ADDRESS = "adm.participant.validation.address";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL. */
	public static final String ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL = "adm.participant.validation.contact.email";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_REQUEST_TYPE. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_REQUEST_TYPE = "adm.participantrequest.validation.request.type";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_OBSERVATIONS_FORMAT. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_OBSERVATIONS_FORMAT = "adm.participantrequest.validation.observations.format";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_PARTICIPANT. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_PARTICIPANT = "adm.participantrequest.validation.participant";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_MOTIVE. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_MOTIVE = "adm.participantrequest.validation.motive";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_FILES_SUSTENT. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_FILES_SUSTENT = "adm.participantrequest.validation.files.sustent";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_EMAIL_FORMAT. */
	public static final String ADM_PARTICIPANT_VALIDATION_EMAIL_FORMAT = "adm.participant.validation.email.format";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL_FORMAT. */
	public static final String ADM_PARTICIPANT_VALIDATION_CONTACT_EMAIL_FORMAT = "adm.participant.validation.contactemail.format";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_ADDRESS_FORMAT. */
	public static final String ADM_PARTICIPANT_VALIDATION_ADDRESS_FORMAT = "adm.participant.validation.address.format";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_WEB_PAGE_FORMAT. */
	public static final String ADM_PARTICIPANT_VALIDATION_WEB_PAGE_FORMAT = "adm.participant.validation.web.page.format";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_CREATION_CONTRACT_DATE. */
	public static final String ADM_PARTICIPANT_VALIDATION_CREATION_CONTRACT_DATE = "adm.participant.validation.creation.contract.date";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BIC_FORMAT. */
	public static final String ADM_PARTICIPANT_VALIDATION_BIC_FORMAT = "adm.participant.validation..bic.format";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BIC_PARTICIPANT_EXISTS. */
	public static final String ADM_PARTICIPANT_VALIDATION_BIC_PARTICIPANT_EXISTS = "adm.participant.validation..bic.participant.exists";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BIC_PARTICIPANT_MODIFICATION_EXISTS. */
	public static final String ADM_PARTICIPANT_VALIDATION_BIC_PARTICIPANT_MODIFICATION_EXISTS = "adm.participant.validation..bic.participant.modification.exists";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BIC_PARTICIPANT_BANK_EXISTS. */
	public static final String ADM_PARTICIPANT_VALIDATION_BIC_PARTICIPANT_BANK_EXISTS = "adm.participant.validation..bic.participant.bank.exists";
	
	/** The Constant ADM_PARTICIPANT_VALIDATION_BIC_PARTICIPANT_HOLDER_ACCOUNT_EXISTS. */
	public static final String ADM_PARTICIPANT_VALIDATION_BIC_PARTICIPANT_HOLDER_ACCOUNT_EXISTS = "adm.participant.validation..bic.participant.holder.account.exists";
	
	/** The Constant ADM_PARTICIPANT_IMPORT_BANK_NATIONAL. */
	public static final String ADM_PARTICIPANT_IMPORT_BANK_NATIONAL = "adm.participant.import.bank.national";
	
	/** The Constant ADM_PARTICIPANT_IMPORT_BANK_FOREIGN. */
	public static final String ADM_PARTICIPANT_IMPORT_BANK_FOREIGN = "adm.participant.import.bank.foreign";
	
	/** The Constant ADM_PARTICIPANT_IMPORT_HOLDER_ACC_BANK. */
	public static final String ADM_PARTICIPANT_IMPORT_HOLDER_ACC_BANK = "adm.participant.import.holder.account.bank";
	
	/** The Constant ADM_PARTICIPANT_IMPORT_BANK_AND_HOLDER_ACC. */
	public static final String ADM_PARTICIPANT_IMPORT_BANK_AND_HOLDER_ACC = "adm.participant.import.bank.and.holder.account";
	
	/** The Constant ADM_PARTICIPANT_BANK_ACCOUNT_CONFIRM_REMOVE. */
	public static final String ADM_PARTICIPANT_BANK_ACCOUNT_CONFIRM_REMOVE = "adm.participant.bank.account.confirm.remove";
	
	/** The Constant LBL_CONFIRM_PARTICIPANT_REQUEST_REGISTER. */
	public static final String LBL_CONFIRM_PARTICIPANT_REQUEST_REGISTER = "lbl.confirm.participantrequest.register";
	
	/** The Constant ERROR_ADM_PARTICIPANT_BLOCK_PREVIOUS_REQUEST. */
	public static final String ERROR_ADM_PARTICIPANT_BLOCK_PREVIOUS_REQUEST = "error.adm.participant.block.previous.request";
	
	/** The Constant LBL_SUCCESS_PARTICIPANT_REQUEST_REGISTER. */
	public static final String LBL_SUCCESS_PARTICIPANT_REQUEST_REGISTER = "lbl.success.participantrequest.register";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_STATE_REQUEST. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_STATE_REQUEST = "adm.participantrequest.validation.search.staterequest";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_REQ. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_REQ = "adm.participantrequest.validation.search.inidate.req";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_REQ. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_REQ = "adm.participantrequest.validation.search.enddate.req";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE = "adm.participantrequest.validation.search.inidate.great.sysdate";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE = "adm.participantrequest.validation.search.enddate.great.sysdate";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE = "adm.participantrequest.validation.search.inidate.great.enddate";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_LESS_INI_DATE. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_SEARCH_END_DATE_LESS_INI_DATE = "adm.participantrequest.validation.search.enddate.less.inidate";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED = "adm.participantrequest.validation.confirm.participant.not.registered";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_BLOCKED. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_BLOCKED = "adm.participantrequest.validation.confirm.participant.not.blocked";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED_OR_BLOCKED. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED_OR_BLOCKED = "adm.participantrequest.validation.confirm.participant.not.registered.or.blocked";
	
	/** The Constant LBL_CONFIRM_PARTICIPANT_REQUEST_CONFIRMATION. */
	public static final String LBL_CONFIRM_PARTICIPANT_REQUEST_CONFIRMATION = "lbl.confirm.participantrequest.confirmation";
	
	/** The Constant LBL_CONFIRM_PARTICIPANT_REQUEST_CONFIRMATION_UPDATE_BIC. */
	public static final String LBL_CONFIRM_PARTICIPANT_REQUEST_CONFIRMATION_UPDATE_BIC = "lbl.confirm.participantrequest.confirmation.update.bic";
	
	/** The Constant LBL_SUCCESS_PARTICIPANT_REQUEST_CONFINRMATION. */
	public static final String LBL_SUCCESS_PARTICIPANT_REQUEST_CONFINRMATION = "lbl.success.participantrequest.confirmation";
	
	/** The Constant LBL_SUCCESS_PARTICIPANT_ANULLMENT_BEGIN. */
	public static final String LBL_SUCCESS_PARTICIPANT_ANULLMENT_BEGIN = "lbl.success.participant.anullment.begin";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_MOTIVE. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_MOTIVE = "adm.participantrequest.validation.reject.motive";
	
	/** The Constant ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE. */
	public static final String ADM_PARTICIPANT_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE= "adm.participantrequest.validation.reject.other.motive";
	
	/** The Constant LBL_CONFIRM_PARTICIPANT_REQUEST_REJECT. */
	public static final String LBL_CONFIRM_PARTICIPANT_REQUEST_REJECT = "lbl.confirm.participantrequest.reject";
	
	/** The Constant LBL_SUCCESS_PARTICIPANT_REQUEST_REJECT. */
	public static final String LBL_SUCCESS_PARTICIPANT_REQUEST_REJECT = "lbl.success.participantrequest.reject";
	
	/** The Constant LBL_CONFIRM_PARTICIPANT_MODIFICATION_REQUEST_REGISTER. */
	public static final String LBL_CONFIRM_PARTICIPANT_MODIFICATION_REQUEST_REGISTER = "lbl.confirm.participant.modification.request.register";
	
	/** The Constant LBL_SUCCESS_PARTICIPANT_MODIFICATION_REQUEST_REGISTER. */
	public static final String LBL_SUCCESS_PARTICIPANT_MODIFICATION_REQUEST_REGISTER = "lbl.success.participant.modification.request.register";
	
	/** The Constant AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_NAME_PREFIX. */
	public static final String AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_NAME_PREFIX = "adm.participant.holder.document.default.name.prefix";
	
	/** The Constant AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_CONTENT. */
	public static final String AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_CONTENT = "adm.participant.holder.document.default.content";
	
	/** The Constant AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_EXTENSION. */
	public static final String AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_EXTENSION = "adm.participant.holder.document.default.extension";

	/** The Constant AMD_PARTICIPANT_REPRESENTATIVE_MESSAGE_IMPORT. */
	public static final String AMD_PARTICIPANT_REPRESENTATIVE_MESSAGE_IMPORT = "adm.participant.representative.message.import";
	/*
	 * Messages for Participant
	 */
	
	/** The Constant WARNING_DISTINCT_REGISTERED_BLOCKED. */
	public static final String WARNING_DISTINCT_REGISTERED_BLOCKED = "warning.distinct.registered.blocked";
	
	/** The Constant WARNING_HOLDER_REQUEST_NO_RESULT. */
	public static final String WARNING_HOLDER_REQUEST_NO_RESULT = "warning.holder.request.no.result";
	
	/** The Constant ADM_BLOCK_REQUEST_HOLDER_CONFIRM_REGISTER. */
	public static final String ADM_BLOCK_REQUEST_HOLDER_CONFIRM_REGISTER = "adm.block.request.holder.confirm.register";
	
	/** The Constant ADM_UNBLOCK_REQUEST_HOLDER_CONFIRM_REGISTER. */
	public static final String ADM_UNBLOCK_REQUEST_HOLDER_CONFIRM_REGISTER = "adm.unblock.request.holder.confirm.register";
	
	/** The Constant SUCCESS_REGISTRY_BLOCK_HOLDER_REQUEST. */
	public static final String SUCCESS_REGISTRY_BLOCK_HOLDER_REQUEST = "success.registry.block.holder.request";
	
	/** The Constant SUCCESS_REGISTRY_UNBLOCK_HOLDER_REQUEST. */
	public static final String SUCCESS_REGISTRY_UNBLOCK_HOLDER_REQUEST = "success.registry.unblock.holder.request";
	
	/** The Constant WARNING_THERE_IS_ALREADY_REQUEST. */
	public static final String WARNING_THERE_IS_ALREADY_BLOCK_REQUEST = "warning.there.is.already.block.request";
	
	/** The Constant SUCCESS_CONFIRM_HOLDER_BLOCK_REQUEST. */
	public static final String  SUCCESS_CONFIRM_HOLDER_BLOCK_REQUEST = "lbl.success.confirm.holder.block.request";
	
	/** The Constant SUCCESS_CONFIRM_HOLDER_UNBLOCK_REQUEST. */
	public static final String SUCCESS_CONFIRM_HOLDER_UNBLOCK_REQUEST = "lbl.success.confirm.holder.unblock.request";
	
	/** The Constant LBL_MOTIVE_REJECT. */
	public static final String LBL_MOTIVE_REJECT = "lbl.motive.reject";
	
	/** The Constant LBL_MOTIVE_ANNULAR. */
	public static final String LBL_MOTIVE_ANNULAR = "lbl.motive.annular";
	
    /** The Constant ADM_BLOCK_REQUEST_HOLDER_REJECT_REGISTER. */
    public static final String ADM_BLOCK_REQUEST_HOLDER_REJECT_REGISTER = "adm.block.request.holder.reject.register";
	
	/** The Constant ADM_UNBLOCK_REQUEST_HOLDER_REJECT_REGISTER. */
	public static final String ADM_UNBLOCK_REQUEST_HOLDER_REJECT_REGISTER = "adm.unblock.request.holder.reject.register";
	
	/** The Constant ADM_BLOCK_REQUEST_HOLDER_ANNULAR_REGISTER. */
	public static final String ADM_BLOCK_REQUEST_HOLDER_ANNULAR_REGISTER = "adm.block.request.holder.annular.register";
		
	/** The Constant ADM_UNBLOCK_REQUEST_HOLDER_ANNULAR_REGISTER. */
	public static final String ADM_UNBLOCK_REQUEST_HOLDER_ANNULAR_REGISTER = "adm.unblock.request.holder.annular.register";
		
	/** The Constant ADM_REQUEST_HOLDER_CONFIRM_MODIFY. */
	public static final String ADM_REQUEST_HOLDER_CONFIRM_MODIFY =  "adm.request.holder.confirm.modify";

	/** The Constant LBL_MODIFY_HOLDER_REQUEST. */
	public static final String LBL_MODIFY_HOLDER_REQUEST = "lbl.modify.holder.request";
	
	/** The Constant LBL_REQUIRED_PARAMETER_LEAST. */
	public static final String  LBL_REQUIRED_PARAMETER_LEAST = "lbl.required.parameter.least";
	
	/** The Constant EXP_REGULAR_NUMERIC_INTEGER. */
	public static final String EXP_REGULAR_NUMERIC_INTEGER = "exp.regular.numeric.integer";
	
	/** The Constant EXP_REGULAR_NUMERIC_INTEGER. */
	public static final String EXP_REGULAR_ALPHANUMERIC_DASH_NOT_SPACE = "exp.regular.alphanumeric.dash.not.space";
	
	/** The Constant EXP_REGULAR_ALPHANUMERIC. */
	public static final String EXP_REGULAR_ALPHANUMERIC = "exp.regular.alphanumeric";
	
	/** The Constant ERROR_SAME_TYPE_DOCUMENT. */
	public static final String ERROR_SAME_TYPE_DOCUMENT = "error.same.type.document";
	
	/** The Constant LBL_SUCCESS_REJECTED_HOLDER_BLOCK_REQUEST. */
	public static final String LBL_SUCCESS_REJECTED_HOLDER_BLOCK_REQUEST = "lbl.success.rejected.holder.block.request";
	
	/** The Constant LBL_SUCCESS_REJECTED_HOLDER_UNBLOCK_REQUEST. */
	public static final String LBL_SUCCESS_REJECTED_HOLDER_UNBLOCK_REQUEST = "lbl.success.rejected.holder.unblock.request";
	
	/** The Constant LBL_SUCCESS_ANNULED_HOLDER_BLOCK_REQUEST. */
	public static final String LBL_SUCCESS_ANNULED_HOLDER_BLOCK_REQUEST = "lbl.success.annuled.holder.block.request";
	
	/** The Constant LBL_SUCCESS_ANNULED_HOLDER_UNBLOCK_REQUEST. */
	public static final String LBL_SUCCESS_ANNULED_HOLDER_UNBLOCK_REQUEST = "lbl.success.annuled.holder.unblock.request";
	
	/** The Constant ADM_BLOCK_REQUEST_HOLDER_REGISTER. */
	public static final String ADM_BLOCK_REQUEST_HOLDER_REGISTER = "adm.block.request.holder.register";
	
	/** The Constant ADM_UNBLOCK_REQUEST_HOLDER_REGISTER. */
	public static final String ADM_UNBLOCK_REQUEST_HOLDER_REGISTER = "adm.unblock.request.holder.register";

	/** The Constant WARNING_CUI_NO_MODIFICATION. */
	public static final String WARNING_CUI_NO_MODIFICATION = "warning.cui.no.modification";
	
	/** The Constant WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER. */
	public static final String WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER = "warning.document.type.document.number.other.holder";
	
	/** The Constant WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_LEGAL. */
	public static final String WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_LEGAL = "warning.document.type.document.number.other.legal";
	
	/** The Constant NECESSARY_TO_MODIFY. */
	public static final String NECESSARY_TO_MODIFY = "necessary.to.modify";
	
	/** The Constant ERROR_REPRESENTATIVE_AGE_NO_MAJOR. */
	public static final String ERROR_REPRESENTATIVE_AGE_NO_MAJOR = "error.representative.age.no.major";
	
	/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_9_11. */
	public static final String ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_9_11 = "error.representative.docnumber.lenght.11.or.9";
	
	/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_CHECK_DIGIT. */
	public static final String ERROR_REPRESENTATIVE_DOC_NUM_CHECK_DIGIT = "error.representative.docnumber.check.digit.invalid";
	
	/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_6_15. */
	public static final String ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_6_15= "error.representative.docnumber.lenght.6.to.15";
	
	/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_11. */
	public static final String ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_11 = "error.representative.docnumber.lenght.11";
	
	/** The Constant WARNING_NECESSARY_LEGAL_REPRESENTATIVE. */
	public static final String WARNING_NECESSARY_LEGAL_REPRESENTATIVE = "legal.representative.warning.necessary.legal.representative";
	
	public static final String NECESSARY_FATCA = "necessary.fatca";
	
	public static final String HOLDER_FILE_PHOTO_NO_COMPLETE = "error.adm.holder.file.photo.no.complete";
	
	/** The Constant WARNING_SAME_NATIONALITY. */
	public static final String WARNING_SAME_NATIONALITY = "warning.same.nationality";
	
	/** The Constant ERROR_ADM_BLOCK_UNBLOCK_FILTER_DATE_INITIAL. */
	public static final String ERROR_ADM_BLOCK_UNBLOCK_FILTER_DATE_INITIAL = "error.adm.block.unblock.filter.date.initial";
	
	/** The Constant ERROR_ADM_BLOCK_UNBLOCK_FILTER_DATE_FINAL. */
	public static final String ERROR_ADM_BLOCK_UNBLOCK_FILTER_DATE_FINAL = "error.adm.block.unblock.filter.date.final";
	
	/** The Constant ERROR_ID_HOLDER_PK_NO_EXIST. */
	public static final String ERROR_ID_HOLDER_PK_NO_EXIST = "error.id.holder.pk.no.exist";
	
	/** The Constant LBL_CONFIRM_HOLDER_REQUEST_MODIFY. */
	public static final String LBL_CONFIRM_HOLDER_REQUEST_MODIFY = "lbl.confirm.holder.request.modify";
	
	/** The Constant LBL_REJECT_HOLDER_REQUEST_MODIFY. */
	public static final String LBL_REJECT_HOLDER_REQUEST_MODIFY = "lbl.reject.holder.request.modify";
	
	/** The Constant LBL_ANNULAR_HOLDER_REQUEST_MODIFY. */
	public static final String LBL_ANNULAR_HOLDER_REQUEST_MODIFY = "lbl.annular.holder.request.modify";
	
	/** The Constant LBL_SUCCESS_CONFIRM_HOLDER_MODIFICATION_REQUEST. */
	public static final String LBL_SUCCESS_CONFIRM_HOLDER_MODIFICATION_REQUEST = "lbl.success.confirm.holder.modification.request";
	
	/** The Constant LBL_SUCCESS_ANNUL_HOLDER_MODIFICATION_REQUEST. */
	public static final String LBL_SUCCESS_ANNUL_HOLDER_MODIFICATION_REQUEST = "lbl.success.annul.holder.modification.request";
	
	/** The Constant LBL_SUCESS_REJECT_HOLDER_MODIFICATION_REQUEST. */
	public static final String LBL_SUCESS_REJECT_HOLDER_MODIFICATION_REQUEST =  "lbl.success.reject.holder.modification.request";
	
	/** The Constant ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_INITIAL. */
	public static final String ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_INITIAL = "error.adm.holder.modification.date.initial";
	
	/** The Constant ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_FINAL_DATE_TODAY. */
	public static final String ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_FINAL_DATE_TODAY = "error.adm.holder.modification.date.final.date.today";
	
	/** The Constant ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_INITIAL_DATE_TODAY. */
	public static final String ERROR_ADM_HOLDER_MODIFICATION_FILTER_DATE_INITIAL_DATE_TODAY = "error.adm.holder.modification.date.initial.date.today";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_INITIAL. */
	public static final String ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_INITIAL = "error.adm.holder.request.date.initial";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_FINAL_DATE_TODAY. */
	public static final String ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_FINAL_DATE_TODAY = "error.adm.holder.request.date.final.date.today";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_INITIAL_DATE_TODAY. */
	public static final String ERROR_ADM_HOLDER_REQUEST_FILTER_DATE_INITIAL_DATE_TODAY = "error.adm.holder.request.date.initial.date.today";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_FILE_ATTACH. */
	public static final String ERROR_ADM_HOLDER_REQUEST_FILE_ATTACH = "error.adm.holder.request.file.attach";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_FILE_TYPE. */
	public static final String ERROR_ADM_HOLDER_REQUEST_FILE_TYPE = "error.adm.holder.request.file.type";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_FILE_SIZE. */
	public static final String ERROR_ADM_HOLDER_REQUEST_FILE_SIZE = "error.adm.holder.request.file.size";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_TODAY. */
	public static final String ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_TODAY = "error.adm.holder.request.date.birthday.date.today";
	
	/** The Constant LBL_WARNING_ACTION. */
	public static final String LBL_WARNING_ACTION="lbl.warning.action";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO. */
	public static final String ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO="error.adm.holder.request.date.birthday.date.oneninezerozero";
	
	/** The Constant ERROR_INPUT_TEXT_CALENDAR. */
	public static final String ERROR_INPUT_TEXT_CALENDAR = "error.input.text.calendar";
	
	/** The Constant LBL_CONFIRM_HEADER_MODIFICATION. */
	public static final String LBL_CONFIRM_HEADER_MODIFICATION = "lbl.confirm.header.modification";
	
	/** The Constant ADM_REQUEST_HOLDER_CONFIRM_MODIFICATION. */
	public static final String ADM_REQUEST_HOLDER_CONFIRM_MODIFICATION = "adm.request.holder.confirm.modification";
	
	/** The Constant LBL_SUCCESS_APPROVED_HOLDER_REQUEST. */
	public static final String LBL_SUCCESS_APPROVED_HOLDER_REQUEST="lbl.success.approved.holder.request";
	
	/** The Constant LBL_SUCCESS_MODIFICATION_HOLDER_REQUEST. */
	public static final String LBL_SUCCESS_MODIFICATION_HOLDER_REQUEST = "lbl.success.modification.holder.request";
	
	/** The Constant WARNING_OPERATION_APPROVE_INVALID. */
	public static final String WARNING_OPERATION_APPROVE_INVALID = "warning.operation.approve.invalid";
	
	/** The Constant LBL_APPROVE_HOLDER_REQUEST. */
	public static final String LBL_APPROVE_HOLDER_REQUEST = "lbl.approve.holder.request";
	
	/** The Constant WARNING_THERE_IS_ALREADY_UNBLOCK_REQUEST. */
	public static final String WARNING_THERE_IS_ALREADY_UNBLOCK_REQUEST = "warning.there.is.already.unblock.request";
	
	/** The Constant WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST. */
	public static final String WARNING_DOCUMENTTYPE_DOCUMENTNUMBER_OTHER_HOLDER_REQUEST = "warning.document.type.document.number.other.holder.request";
	
	/** The Constant ADM_BLOCK_REQUEST_HOLDER_APPROVE_REGISTER. */
	public static final String ADM_BLOCK_REQUEST_HOLDER_APPROVE_REGISTER = "adm.block.request.holder.approve.register";
	
	/** The Constant ADM_UNBLOCK_REQUEST_HOLDER_APPROVE_REGISTER. */
	public static final String ADM_UNBLOCK_REQUEST_HOLDER_APPROVE_REGISTER = "adm.unblock.request.holder.approve.register";
	
	/** The Constant LBL_SUCCESS_APPROVE_HOLDER_BLOCK_REQUEST. */
	public static final String LBL_SUCCESS_APPROVE_HOLDER_BLOCK_REQUEST = "lbl.success.approve.holder.block.request";
	
	/** The Constant LBL_SUCCESS_APPROVE_HOLDER_UNBLOCK_REQUEST. */
	public static final String LBL_SUCCESS_APPROVE_HOLDER_UNBLOCK_REQUEST = "lbl.success.approve.holder.unblock.request";
	
	/** The Constant WARNING_OPERATION_MODIFICATION_INVALID. */
	public static final String WARNING_OPERATION_MODIFICATION_INVALID = "warning.operation.modification.invalid";
	
	/** The Constant WARNING_OPERATION_REGISTERED_INVALID. */
	public static final String WARNING_OPERATION_REGISTERED_INVALID = "warning.operation.registered.invalid";
	
	/** The Constant LBL_APPROVE_REQUEST_MODIFY. */
	public static final String LBL_APPROVE_REQUEST_MODIFY = "lbl.approve.holder.request.modify";
	
	/** The Constant LBL_SUCCESS_APPROVE_HOLDER_MODIFICATION_REQUEST. */
	public static final String LBL_SUCCESS_APPROVE_HOLDER_MODIFICATION_REQUEST = "lbl.success.approve.holder.modification.request";
	
	/** The Constant WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE. */
	public static final String WARNING_FILE_REQUEST_HOLDER_NO_COMPLETE = "warning.file.holder.no.complete";
	
	/** The Constant WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE. */
	public static final String WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE = "warning.file.representative.no.complete";
	
	/** The Constant WARNING_NOT_FILE_REPRESENTATIVE_CERTIFICATION. */
	public static final String WARNING_NOT_FILE_REPRESENTATIVE_CERTIFICATION = "warning.not.file.representative.certification";
	
	/** The Constant WARNING_NECESSARY_TWO_LEGAL_REPRESENTATIVE. */
	public static final String WARNING_NECESSARY_TWO_LEGAL_REPRESENTATIVE = "warning.necessary.two.legal.representative";
	
    /** The Constant ERROR_HOLDER_DOC_NUM_LEGTH_9_11. */
    public static final String ERROR_HOLDER_DOC_NUM_LEGTH_9_11 = "error.holder.docnumber.length.11.or.9";
	
	/** The Constant ERROR_HOLDER_DOC_NUM_CHECK_DIGIT. */
	public static final String ERROR_HOLDER_DOC_NUM_CHECK_DIGIT = "error.holder.docnumber.check.digit.invalid";
	
	/** The Constant ERROR_HOLDER_DOC_NUM_LEGTH_6_15. */
	public static final String ERROR_HOLDER_DOC_NUM_LEGTH_6_15= "error.holder.docnumber.length.6.to.15";
	
	/** The Constant ERROR_HOLDER_DOC_NUM_LEGTH_11. */
	public static final String ERROR_HOLDER_DOC_NUM_LEGTH_11 = "error.holder.docnumber.length.11";
	
	/** The Constant EXIST_HOLDER_INFORMATION_EXPORT. */
	public static final String EXIST_HOLDER_INFORMATION_EXPORT = "exist.holder.information.export";
	
	/** The Constant EXIST_REPRESENTATIVE_INFORMATION_EXPORT. */
	public static final String EXIST_REPRESENTATIVE_INFORMATION_EXPORT = "exist.representative.information.export";
	
	/** The Constant ERROR_HOLDER_DOC_NUM_LEGTH_16_18_21. */
	public static final String ERROR_HOLDER_DOC_NUM_LEGTH_16_18_21 = "error.holder.docnumber.length.16.or.18.or.21";
	
	/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_16_18_21. */
	public static final String ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_16_18_21 = "error.representative.docnumber.length.16.or.18.or.21";
	
	/** The Constant ERROR_DOC_COUNT_TWO_ZERO_VALID. */
	public static final String ERROR_DOC_COUNT_TWO_ZERO_VALID = "error.doc.count.two.zero.valid";
	
	/** The Constant ERROR_DOC_COUNT_THREE_ZERO_VALID. */
	public static final String ERROR_DOC_COUNT_THREE_ZERO_VALID = "error.doc.count.three.zero.valid";
	
	/** The Constant ERROR_DOC_FORMAT_LAST_FOUR_DIGITS_1900. */
	public static final String ERROR_DOC_FORMAT_LAST_FOUR_DIGITS_1900 = "error.doc.format.last.four.digits.1900";
	
	/** The Constant ERROR_PARTICIPANT_ACCOUNTS_WITH_BALANCE. */
	public static final String ERROR_PARTICIPANT_ACCOUNTS_WITH_BALANCE_REG="error.adm.participant.account.with.balance.reg";
	
	/** The Constant ERROR_PARTICIPANT_ACCOUNTS_WITH_BALANCE_CONF. */
	public static final String ERROR_PARTICIPANT_ACCOUNTS_WITH_BALANCE_CONF="error.adm.participant.account.with.balance.conf";
	
	/** The Constant ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS. */
	public static final String ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS="error.documents.type.and.number.equals";
	
	/** The Constant LBL_WARNING_EXIST_REGISTRY. */
	public static final String LBL_WARNING_EXIST_REGISTRY = "lbl.warning.exist.registry";	
	
	/** The Constant NO_RELATIONSHIP_BETWEEN_THE_HOLDER_AND_PARTICIPANT. */
	public static final String NO_RELATIONSHIP_BETWEEN_THE_HOLDER_AND_PARTICIPANT = "no.relationship.between.the.holder.and.participant";
	
	/** The Constant ERROR_ADM_MODIFICATION_HOLDER_REQUEST_EXIST. */
	public static final String ERROR_ADM_MODIFICATION_HOLDER_REQUEST_EXIST = "error.adm.modification.holder.request.exist";
	
	/** The Constant for displaying the session expired message on Idle time. */
	public static final String IDLE_TIME_EXPIRED = "idle.session.expired";
	
	/** The Constant ERROR_DOCNUMBER_LENGTH_MINIMUM_1. */
	public static final String ERROR_DOCNUMBER_LENGTH_MINIMUM_1 = "error.docnumber.length.minimum.1";

	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_FILE_EMPTY. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_FILE_REQUIRED="error.adm.accountholder.file.no.complete";

	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_TYPE_SUSSECION. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_TYPE_SUCCESION="adm.holderaccount.validation.holder.type.succesion";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_DOACTION_APROVATE_ANULATE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_DOACTION_APROVATE_ANULATE= "adm.holderaccount.validation.doaction.approvate.anulate";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_DOACTION_CONFIRM_REJECT. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_DOACTION_CONFIRM_REJECT= "adm.holderaccount.validation.doaction.confirm.reject";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_MOTIVE_REJECT_ANULATE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_MOTIVE_REJECT_ANULATE= "adm.holderaccount.validation.motive";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_OTHER_MOTIVE_REJECT_ANULATE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_OTHER_MOTIVE_REJECT_ANULATE= "adm.holderaccount.validation.other.motive";

	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_MESSAGE_DATA_REQUIRED. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_MESSAGE_DATA_REQUIRED= "adm.holderaccount.validation.data.search";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_UNBLOCK. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDERACCOUNT_UNBLOCK="adm.holderaccount.validation.holderaccount.unblock";
	
	/** The Constant ADM_HOLDERACCOUNT_APPROVE_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_APPROVE_BLOCK="adm.holderaccount.approve.block";
	
	/** The Constant ADM_HOLDERACCOUNT_ANULATE_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_ANULATE_BLOCK="adm.holderaccount.anulate.block";
	
	/** The Constant ADM_HOLDERACCOUNT_REJECT_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_REJECT_BLOCK="adm.holderaccount.reject.block";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRM_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_CONFIRM_BLOCK="adm.holderaccount.confirm.block";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK_BLOCK="adm.holderaccount.request.approve.ok.block";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK_BLOCK="adm.holderaccount.request.anulate.ok.block";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_REJECT_OK_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_REJECT_OK_BLOCK="adm.holderaccount.request.reject.ok.block";
	
	/** The Constant ADM_HOLDERACCOUNT_APPROVE_MODIFY. */
	public static final String ADM_HOLDERACCOUNT_APPROVE_MODIFY="adm.holderaccount.approve.modify";
	
	/** The Constant ADM_HOLDERACCOUNT_ANULATE_MODIFY. */
	public static final String ADM_HOLDERACCOUNT_ANULATE_MODIFY="adm.holderaccount.anulate.modify";
	
	/** The Constant ADM_HOLDERACCOUNT_REJECT_MODIFY. */
	public static final String ADM_HOLDERACCOUNT_REJECT_MODIFY="adm.holderaccount.reject.modify";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRM_MODIFY. */
	public static final String ADM_HOLDERACCOUNT_CONFIRM_MODIFY="adm.holderaccount.confirm.modify";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK_MODIFY. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_APPROVE_OK_MODIFY="adm.holderaccount.request.approve.ok.modify";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK_MODIFY. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_ANULATE_OK_MODIFY="adm.holderaccount.request.anulate.ok.modify";
	
	/** The Constant ADM_HOLDERACCOUNT_REQUEST_REJECT_OK_MODIFY. */
	public static final String ADM_HOLDERACCOUNT_REQUEST_REJECT_OK_MODIFY="adm.holderaccount.request.reject.ok.modify";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES_APPROVE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES_APPROVE="adm.holderaccount.validation.close.with.balances.approve";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES_APPROVE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_CLOSE_WITH_BALANCES_CONFIRM="adm.holderaccount.validation.close.with.balances.confirm";
	
	/** The Constant WARNING_DISTINCT_REGISTERED_CUI. */
	public static final String WARNING_DISTINCT_REGISTERED_CUI="warning.distinct.registered.cui";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND="adm.holderaccount.validation.cui.not.found";
	
	/** The Constant ADM_HOLDERACCOUNT_APPROVE_BLOCK_ACEPTED. */
	public static final String ADM_HOLDERACCOUNT_APPROVE_BLOCK_ACEPTED="adm.holderaccount.validation.approve.block.accept";
	
	/** The Constant ADM_HOLDERACCOUNT_CONFIRM_BLOCK_ACEPTED. */
	public static final String ADM_HOLDERACCOUNT_CONFIRM_BLOCK_ACEPTED="adm.holderaccount.validation.confirm.block.accept";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_MODIFY_PAYMENT_ECONOMIC. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_MODIFY_PAYMENT_ECONOMIC="adm.holderaccount.validation.modify.payment.economic";
	
	/** The Constant ERROR_NIT_NO_EXIST. */
	public static final String ERROR_NIT_NO_EXIST = "error.nit.no.exist";
	
	/** The Constant ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE. */
	public static final String ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE="error.document.type.number.holder.representative";

	/** The Constant ADM_ACCOUNT_BALANCE_QUERY_MOVEMENTS_NOT_RESULT_MARKETFACT. */
	public static final String ADM_ACCOUNT_BALANCE_QUERY_MOVEMENTS_NOT_RESULT_MARKETFACT="account.balance.query.movements.not.result.marketfact";
	
	/** The Constant ADM_ACCOUNT_BALANCE_QUERY_MOVEMENTS_NOT_RESULT_MARKETFACT_MOVEMENT. */
	public static final String ADM_ACCOUNT_BALANCE_QUERY_MOVEMENTS_NOT_RESULT_MARKETFACT_MOVEMENT="account.balance.query.movements.not.result.marketfactMovement";
	
	/** The Constant MESSAGE_BACK. */
	public static final String MESSAGE_BACK = "message.back";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT_BANK. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT_BANK="adm.holderaccount.validation.bank.number";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_CBO_BANK. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_CBO_BANK="adm.holderaccount.validation.bank";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_CBO_ACCOUNT_TYPE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_CBO_ACCOUNT_TYPE="adm.holderaccount.validation.bankaccount";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_NUMBER_ACCOUNT="adm.holderaccount.validation.bankaccountnumber";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_MONEY_TYPE. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_MONEY_TYPE="adm.holderaccount.validation.bankcurrent";
	
	/** The Constant COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT. */
	public static final String COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT = "commons.label.participant.cui.incorrect";
	
	public static final String COMMONS_LABEL_PARTICIPANT_CUI_NOT_EXIST = "commons.label.participant.cui.not.exist";
	
	/** The Constant ADM_VALIDATION_SEARCH. */
	
	public static final String ADM_VALIDATION_SEARCH="adm.validation.search";
	
	/** The Constant ADM_VALIDATION_HOLDER_ACCOUNT_REQUEST_EXIST. */
	public static final String ADM_VALIDATION_HOLDER_ACCOUNT_REQUEST_EXIST = "adm.holderaccount.request.exist";
	
	/** The Constant ADM_VALIDATION_HOLDER_ACCOUNT_EXIST_ALL. */
	public static final String ADM_VALIDATION_HOLDER_ACCOUNT_EXIST_ALL = "adm.holderaccount.exist.all";
	
	/** The Constant ERROR_ON_ACCOUNT_AND_PARTICIPANT. */
	public static final String ERROR_ON_ACCOUNT_AND_PARTICIPANT = "error.holder.account.modify.holder.participant";
	
	/** The Constant ERROR_HOLDER_WITH_NOT_ACCOUNT. */
	public static final String ERROR_HOLDER_WITH_NOT_ACCOUNT = "error.holder.account.modify.holder.not.account";
	
	/** The Constant VALIDATE_HOLDER_ACCOUNT_WRONG_HOLDER_STATE. */
	public static final String VALIDATE_HOLDER_ACCOUNT_WRONG_HOLDER_STATE = "enq.consolidated.parentAccount.wrong.holder.state";
	
	/** The Constant VALIDATE_HOLDER_ACCOUNT_WRONG_NUMBER. */
	public static final String VALIDATE_HOLDER_ACCOUNT_WRONG_NUMBER = "enq.consolidated.parentAccount.wrong.number";
	
	/** The Constant ADM_HOLDER_VALIDATION_WEB_PAGE_FORMAT. */
	public static final String ADM_HOLDER_VALIDATION_WEB_PAGE_FORMAT = "adm.holders.validation.web.page.format";
	
	/** The Constant ERROR_PERSON_JURIDIC_CLASS_TRUST_ECONOMIC_ACTIVITY_BANK. */
	public static final String ERROR_PERSON_JURIDIC_CLASS_TRUST_ECONOMIC_ACTIVITY_BANK = "error.person.juridic.class.trust.economic.activity.bank";
	
	/** The Constant VALIDATE_SELECTED_SECURITY_BALANCE. */
	public static final String VALIDATE_SELECTED_SECURITY_BALANCE = "msg.issuer.holder.query.select.balance";
	
	/** The Constant HOLDER_INVESTMENT_FUNDS_OPENED. */
	public static final String HOLDER_INVESTMENT_FUNDS_OPENED="holder.investment.funds.opened";
	
	/** The Constant HOLDER_REGISTER_NEW_TRUST. */
	public static final String HOLDER_REGISTER_NEW_TRUST="holder.register.new.trust";
	
	/** The Constant ERROR_SIZE_FILENAME_DOCUMENT. */
	public static final String ERROR_SIZE_FILENAME_DOCUMENT="error.size.filename.document";
	
	/** The Constant ERROR_ECONOMIC_ACTIVITY_FIA. */
	public static final String ERROR_ECONOMIC_ACTIVITY_FIA="error.economic.activity.fia";
	
	/** The Constant ERROR_LENGTH_SIZE. */
	public static final String ERROR_LENGTH_SIZE ="error.length.size";
	
	/** The Constant ERROR_NOT_SELECTED_ISSUER_OR_PARTICIPANT. */
	public static final String ERROR_NOT_SELECTED_ISSUER_OR_PARTICIPANT="error.select.filter.for.issuer.participant";	
	
	/** INTERFACE RECEPTION. */
	public final static String MSG_HOLDER_LOCATION_GEOGRAPHIC_INVALID = "msg.holder.location.geographic.invalid";
	
	/** The Constant MSG_HOLDER_STRUCTURE_INVALID. */
	public final static String MSG_HOLDER_STRUCTURE_INVALID = "msg.holder.structure.invalid";
	
	/** The Constant MSG_HOLDER_DOCUMENT_TYPE_INVALID. */
	public final static String MSG_HOLDER_DOCUMENT_TYPE_INVALID = "msg.holder.document.type.invalid";
	
	/** The Constant MSG_HOLDER_DOCUMENT_EXPENDED_INVALID. */
	public final static String MSG_HOLDER_DOCUMENT_EXPENDED_INVALID = "msg.holder.document.expended.invalid";
	
	/** The Constant MSG_HOLDER_NATIONALITY_INVALID. */
	public final static String MSG_HOLDER_NATIONALITY_INVALID = "msg.holder.nationality.invalid";
	
	/** The Constant MSG_HOLDER_RESIDENCE_COUNTRY_INVALID. */
	public final static String MSG_HOLDER_RESIDENCE_COUNTRY_INVALID = "msg.holder.residence.country.invalid";
	
	/** The Constant MSG_HOLDER_LEGAL_REPRENTATIVE_INVALID. */
	public final static String MSG_HOLDER_LEGAL_REPRENTATIVE_INVALID = "msg.holder.legal.representative.invalid";
	
	/** The Constant MSG_HOLDER_DEPARTAMENT_INVALID. */
	public final static String MSG_HOLDER_DEPARTAMENT_INVALID = "msg.holder.departament.invalid";
	
	/** The Constant MSG_HOLDER_ECONOMIC_ACTIVITY_INVALID. */
	public final static String MSG_HOLDER_ECONOMIC_ACTIVITY_INVALID = "msg.holder.economic.activity.invalid";
	
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_10_DIGITS = "error.validator.document.type.10.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_15_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_15_DIGITS = "error.validator.document.type.15.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_17_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_17_DIGITS = "error.validator.document.type.17.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_6_20_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_6_20_DIGITS = "error.validator.document.type.6.20.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS = "error.validator.document.type.6.15.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_COUNT_THREE_ZERO_VALID. */
	public final static String ERROR_VALIDATOR_DOCUMENT_COUNT_THREE_ZERO_VALID="error.validator.document.type.count.three.zero.valid";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_12_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_12_DIGITS = "error.validator.document.type.12.digits";
	
	/** The Constant ERROR_VALIDATOR_NUMBER_CHARACTER_HYPHEN. */
	public static final String ERROR_VALIDATOR_NUMBER_CHARACTER_HYPHEN="error.validator.number.character.hyphen";
	
	/** The Constant ERROR_VALIDATOR_NUMBER_MAX_LENGTH. */
	public static final String ERROR_VALIDATOR_NUMBER_MAX_LENGTH="error.validator.number.max.length";
	
	/** The Constant ERROR_VALIDATOR_EXTENSION_MAX_LENGTH. */
	public static final String ERROR_VALIDATOR_EXTENSION_MAX_LENGTH="error.validator.extension.max.length";
	
	/** The Constant ERROR_VALIDATOR_EXTENSION_MIN_LENGTH. */
	public static final String ERROR_VALIDATOR_EXTENSION_MIN_LENGTH="error.validator.extension.min.length";
	
	/** The Constant ERROR_VALIDATOR_EXTENSION_MAX_LENGTH_RUC. */
	public static final String ERROR_VALIDATOR_EXTENSION_MAX_LENGTH_RUC="error.validator.extension.max.length.ruc";
	
	/** The Constant ERROR_VALIDATOR_EXTENSION_MIN_LENGTH_RUC. */
	public static final String ERROR_VALIDATOR_EXTENSION_MIN_LENGTH_RUC="error.validator.extension.min.length.ruc";
	
	/** The Constant ERROR_VALIDATOR_IS_NUMERIC. */
	public static final String ERROR_VALIDATOR_IS_NUMERIC = "error.validator.is.numeric";
	
	/** The Constant ERROR_VALIDATOR_IS_NOT_ALL_ZERO. */
	public static final String ERROR_VALIDATOR_IS_NOT_ALL_ZERO = "error.validator.is.not.all.zero";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_COUNT_ONE_ZERO_VALID. */
	public final static String ERROR_VALIDATOR_DOCUMENT_COUNT_ONE_ZERO_VALID="error.validator.document.type.count.one.zero.valid";
    
    /** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH. */
    public final static String ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_MATCH="error.validator.document.type.not.match";	
    
    /** FORMAT NUMBER  *. */
    public static final String PERCENT_FORMAT_DECIMAL = "##,###,###,###,##0.0000";
    
    /** The Constant QUANTITY_FORMAT_DECIMAL. */
    public static final String QUANTITY_FORMAT_DECIMAL = "##,###,###,###,##0.00";
	
	/** The Constant INTEGER_FORMAT_NUMBER. */
	public static final String INTEGER_FORMAT_NUMBER = "##,###,###,###,##0";
	
	/** The Constant STRING_ERROR_WHRITEEN. */
	public static final String STRING_ERROR_WHRITEEN="No se pudo leer completamente el archivo ";
	
	/** Participante requerido para el registro*/
	public static final String MSG_PARTICIPANT_REQUIERED =  "adm.holder.participant.request";
	
	public static final String ALERT_DROP_APPLICANT_CONSULT_SUCCESS="alert.drop.applicant.consult.success";
	
	public static final String ALERT_FINALICE_SEARCH="alert.finalice.search";
	
	public static final String ALERT_CONFIRM_SEARCH="alert.confirm.search";

	public static final String ALERT_PARAMETER_NAME="alert.parameter.name";
	
	public static final String ALERT_BACK_FILE="alert.applicant.back.file";
	
	public static final String PARAMETER_REGISTRY_CONFIRM="parameter.registry.confirm";
	
	public static final String PARAMETER_REGISTRY_SUCCESS="parameter.registry.success";

	public static final String PARAMETER_DELETE_SUCCESS="parameter.delete.success";

	public static final String PARAMETER_DELETE_CONFIRM="parameter.delete.confirm";

	public static final String ALERT_EXCEL_FORMAT="alert.excel.format";

	public static final String ALERT_EXCEL_FILE_FORMAT="alert.excel.file.format";
	
	public static final String ALERT_EXCEL_FILE_WITH_ERRORS="alert.excel.file.with.errors";
	
	/** Issue 776*/
	public static final String LBL_ALERT_EXCHANGE_DOCUMENT_NUMBER_REQUIRED="alert.exchange.document.number.required";
	
	public static final String LBL_MSG_CONFIRM_REGITER_INVESTORTYPE="lbl.msg.confirm.regiter.investorType";

	public static final String MSG_SUCCESFUL_REGISTER_INVESTORTYPE="msg.succesful.register.investorType";
	
	public static final String MSG_VALIDATION_INVESTORTYPE_NOTFOUNDCHANGES="msg.validation.investorType.notFoundChanges";
	
	public static final String LBL_MSG_CONFIRM_MODIFY_INVESTORTYPE = "lbl.msg.confirm.modify.investorType";
	
	public static final String MSG_SUCCESFUL_MODIFY_INVESTORTYPE="msg.succesful.modify.investorType";
	
	public static final String MSG_VALIDATED_INVESTORTYPE_DUPLICATED="msg.validated.investorType.duplicated";

	/** The Constant ERROR_RECORD_NOT_SELECTED. */
	public static final String WARNING_PARTICIPANT_IS_NOT_REGISTERED = "warning.participant.is.not.registered";
	
	public static final String HOLDER_MASSIVE_FILE_NULL = "mcn.msg.holderMassiveFile.notExists";
	
	public static final String HOLDER_MASSIVE_PARTICIPANT_NULL = "mcn.msg.holderMassiveFile.participant.notExists";
	
	public static final String HOLDER_MASSIVE_REGISTER_CONFIRM = "mcn.msg.holderMassiveFile.register.confirm";
	
	public static final String HOLDER_MASSIVE_REGISTER_SUCCESS = "mcn.msg.holderMassiveFile.register.success";
	
	public static final String HOLDER_MASSIVE_HOLDER_TYPE_FIELD_ERROR_INVALID = "holder.massive.validation.holder.type.error";
	
	public static final String HOLDER_MASSIVE_MULTIPLE_HOLDER_TYPE_ERROR = "holder.massive.multiple.holder.type.error";
	
	public static final String HOLDER_MASSIVE_MULTIPLE_STOCK_CODE_ERROR = "holder.massive.multiple.stock.code.error";
	
	public static final String HOLDER_MASSIVE_MULTIPLE_RELATED_NAME_ERROR = "holder.massive.multiple.related.name.error";
	
	public static final String HOLDER_MASSIVE_OTHER_ROW_ERROR = "holder.massive.multiple.other.row.error";
	
	public static final String HOLDER_MASSIVE_REPRESENTATIVE_TYPE_FIELD_ERROR_INVALID = "holder.massive.validation.representative.type.error";
	
	public static final String HOLDER_MASSIVE_ERROR_DOWNLOADING_FILE="holder.massive.msg.massive.error.downloading.file";
	
	public static final String HOLDER_MASSIVE_UNEXPECTED_ERROR = "holder.massive.msg.unexpected.error";

	public static final String HOLDER_MASSIVE_UNEXPECTED_NULL_POINTER_ERROR = "holder.massive.msg.unexpected.null.pointer.error";

	public static final String HOLDER_MASSIVE_MGS_NAME_FIELD_REGEX_VALIDATION_FAILED = "holder.massive.msg.name.field.regex.validation.failed";
	
	public static final String HOLDER_MASSIVE_MGS_EMAIL_FIELD_REGEX_VALIDATION_FAILED = "holder.massive.msg.email.field.regex.validation.failed";
	
	public static final String HOLDER_MASSIVE_DOCUMENT_TYPE_FIELD_ERROR_INVALID = "holder.massive.validation.document_type.error";
	
	public static final String HOLDER_MASSIVE_MGS_DOCUMENT_NUMBER_FIELD_REGEX_VALIDATION_FAILED = "holder.massive.msg.document.number.field.regex.validation.failed";

	public static final String HOLDER_MASSIVE_MSG_COUNTRY_INVALID = "holder.massive.msg.country.invalid";
	
	public static final String HOLDER_MASSIVE_MSG_DEPARTMENT_INVALID = "holder.massive.msg.department.invalid";
	
	public static final String HOLDER_MASSIVE_MSG_CITY_INVALID = "holder.massive.msg.city.invalid";
	
	public static final String HOLDER_MASSIVE_MSG_RESIDENCE_INCONGRUENT = "holder.massive.msg.residence.incongruent";
	
	public static final String HOLDER_MASSIVE_MSG_NOT_RESIDENCE_INCONGRUENT = "holder.massive.msg.not.residence.incongruent";
	
	public static final String HOLDER_MASSIVE_MSG_BOOLEAN_INVALID = "holder.massive.msg.boolean.invalid";
	
	public static final String HOLDER_MASSIVE_MSG_BOOLEAN_INVALID_INDPDD = "holder.massive.msg.boolean.invalid.indpdd";
	
	public static final String HOLDER_MASSIVE_MSG_FORMAT_DATE_INVALID = "holder.massive.msg.format.date.invalid";
	
	public static final String HOLDER_MASSIVE_MSG_FIELD_NOT_IN_LIST = "holder.massive.msg.field.not.in.list";
	
	public static final String HOLDER_MASSIVE_MSG_BANK_NOT_FOUND = "holder.massive.msg.bank.not.found";
	
	public static final String HOLDER_MASSIVE_MSG_BANK_ACCOUNT_NOT_FOUND = "holder.massive.msg.bank.account.not.found";
	
	public static final String HOLDER_MASSIVE_MSG_BANK_ACCOUNT_NUMBER_NOT_FOUND = "holder.massive.msg.bank.account.number.not.found";
	
	public static final String REGEX_ONLY_NUMBERS="exp.regular.number";
	
	public static final String HOLDER_MASSIVE_MSG_REGEX_ONLY_NUMBERS_FAILED = "holder.massive.msg.regex.only.numbers.failed";
	
	public static final String HOLDER_MASSIVE_MSG_NATURAL_HOLDER_ACCOUNT_SIZE_HOLDER_ERROR = "holder.massive.msg.natural.holder.account.size.holder.error";
	
	public static final String HOLDER_MASSIVE_MSG_ACCOUNT_TYPE_MISSMATCH = "holder.massive.msg.account.type.missmatch";
	
	public static final String HOLDER_MASSIVE_MSG_HOLDER_TYPE_MISSMATCH = "holder.massive.msg.holder.type.missmatch";

	public static final String HOLDER_MASSIVE_MSG_HOLDER_REQUIRED_FIELD= "holder.massive.msg.holder.missing.required.field";
	
	public static final String HOLDER_CHANGES_MSG_REQUIRE_SUPPORTING_FILE = "holder.changes.msg.require.supporting.file";
	
	public static final String HOLDER_CHANGES_MSG_NEEDS_SUPERVISOR_CONFIRM = "holder.changes.msg.needs.supervisor.confirm";
	
	public static final String HOLDER_CHANGES_MSG_NEEDS_CAVAPY_CONFIRM = "holder.changes.msg.needs.cavapy.confirm";
	
	public static final String WARNING_FILE_REQUEST_HOLDER_DOCUMENT_NUMBER_EMPTY = "warning.file.holder.document.number.empty";
}