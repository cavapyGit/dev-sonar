package com.pradera.accounts.util;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PepPersonTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class PepPersonTO implements Serializable{



	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9146410400997228739L;
	
	/** The name. */
	private String name;
	
	/** The last name. */
	private String lastName;
	
	/** The id country. */
	private Integer idCountry;
	
	/** The id motive. */
	private Integer idMotive;
	
	/** The register number. */
	private Long registerNumber;
	
	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the register number.
	 *
	 * @return the register number
	 */
	public Long getRegisterNumber() {
		return registerNumber;
	}
	
	/**
	 * Sets the register number.
	 *
	 * @param registerNumber the new register number
	 */
	public void setRegisterNumber(Long registerNumber) {
		this.registerNumber = registerNumber;
	}
	
	/** The start date. */
	private Date startDate = new Date();
	
	/** The end date. */
	private Date endDate = new Date();
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the id country.
	 *
	 * @return the id country
	 */
	public Integer getIdCountry() {
		return idCountry;
	}
	
	/**
	 * Sets the id country.
	 *
	 * @param idCountry the new id country
	 */
	public void setIdCountry(Integer idCountry) {
		this.idCountry = idCountry;
	}
	
	/**
	 * Gets the id motive.
	 *
	 * @return the id motive
	 */
	public Integer getIdMotive() {
		return idMotive;
	}
	
	/**
	 * Sets the id motive.
	 *
	 * @param idMotive the new id motive
	 */
	public void setIdMotive(Integer idMotive) {
		this.idMotive = idMotive;
	}
	
	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}
	
	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}




}
