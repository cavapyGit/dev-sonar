package com.pradera.accounts.util;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class TextMatchingComparator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class TextMatchingComparator implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The text. */
	private char[] text;
	
	/** The pattern. */
	private char[] pattern;
	
	/** The n. */
	private int n;
	
	/** The m. */
	private int m;
	
	/**
	 * Sets the string.
	 *
	 * @param t the t
	 * @param p the p
	 */
	public void setString(String t, String p) {
		this.text = t.toCharArray();
		this.pattern = p.toCharArray();
		this.n = t.length();
		this.m = p.length();
	}

	
	
	/**
	 * Heuristic compare.
	 *
	 * @param pattern the pattern
	 * @param text the text
	 * @return the boolean
	 */
	public Boolean heuristicCompare(String pattern, String text	){
		
		String[] names = pattern.split(" ");
		int subTotal = 0;
		int total = 0;
		
		for(int i =0; i<names.length; i++){
			subTotal = compareRight(names[i],text);
			if(subTotal==0){
				subTotal = compareLeft(names[i],text);
			}
			total+=subTotal;
		}
		
		int textLength = text.length();
		int result = (total*100)/textLength;
		total = 0;
		subTotal=0;
		if(result>40){
			return true;
		}
		else
			return false;
}
	
	
	/**
	 * Search.
	 *
	 * @return the int
	 */
	private int search() {
		for (int i = 0; i < n-m; i++) {
			int j = 0;
			while (j < m && text[i+j] == pattern[j]) {
				j++;
			}
			if (j == m) return i;
		}
		return -1;
	}
	
	
	/**
	 * Compare right.
	 *
	 * @param temp the temp
	 * @param text the text
	 * @return the integer
	 */
	private Integer compareRight(String temp, String text){
		Integer total = 0;
		int l = (temp.length()*60)/100;
		for(int j =0; j<l; j++){

			String texto = temp.substring(j, temp.length());

			setString(text,texto);
			int first_occur_position = search();
			if(first_occur_position!=-1){

				total = total + texto.length();
				break;
			}

		
		}
		return total;
	}
	
	/**
	 * Compare left.
	 *
	 * @param temp the temp
	 * @param text the text
	 * @return the integer
	 */
	private  Integer compareLeft(String temp, String text){
		Integer total = 0;
		int l = (temp.length()*60)/100;
		for(int j =0; j<l; j++){

			String texto = temp.substring(0, temp.length()-j);

			setString(text,texto);
			int first_occur_position = search();
			if(first_occur_position!=-1){
				total = total + texto.length();
				break;
			}
		}
		return total;
	}
}
	
	


