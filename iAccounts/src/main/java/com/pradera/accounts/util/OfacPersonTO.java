package com.pradera.accounts.util;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class OfacPersonTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class OfacPersonTO  implements Serializable{
	
	/** The name. */
	private String name;
	
	/** The last name. */
	private String lastName;


	/** The country. */
	private String country;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The register number. */
	private Long registerNumber;
	
	/** The ofac person type. */
	private Integer ofacPersonType;
	
	/** The full name. */
	private String fullName;
	
	/**
	 * Gets the ofac person type.
	 *
	 * @return the ofac person type
	 */
	public Integer getOfacPersonType() {
		return ofacPersonType;
	}

	/**
	 * Sets the ofac person type.
	 *
	 * @param ofacPersonType the new ofac person type
	 */
	public void setOfacPersonType(Integer ofacPersonType) {
		this.ofacPersonType = ofacPersonType;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the register number.
	 *
	 * @return the register number
	 */
	public Long getRegisterNumber() {
		return registerNumber;
	}

	/**
	 * Sets the register number.
	 *
	 * @param registerNumber the new register number
	 */
	public void setRegisterNumber(Long registerNumber) {
		this.registerNumber = registerNumber;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
 

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
