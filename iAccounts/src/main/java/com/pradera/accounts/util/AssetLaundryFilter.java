package com.pradera.accounts.util;

import java.io.Serializable;

import com.pradera.model.accounts.type.AssertLaundryType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AssetLaundryFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class AssetLaundryFilter implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The full name. */
	private String fullName;
	
	/** The id. */
	private Long id;
	
	/** The type. */
	private AssertLaundryType type;
	
	
	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public AssertLaundryType getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(AssertLaundryType type) {
		this.type = type;
	}
	
	/**
	 * Instantiates a new asset laundry filter.
	 *
	 * @param fullName the full name
	 * @param id the id
	 * @param type the type
	 */
	public AssetLaundryFilter(String fullName, Long id, AssertLaundryType type) {
		super();
		this.fullName = fullName;
		this.id = id;
		this.type = type;
	}
	
	/**
	 * Instantiates a new asset laundry filter.
	 *
	 * @param fullName the full name
	 * @param id the id
	 */
	public AssetLaundryFilter(String fullName, Long id) {
		super();
		this.fullName = fullName;
		this.id = id;
	}
	
	/**
	 * Instantiates a new asset laundry filter.
	 */
	public AssetLaundryFilter() {
		super();
	}
	
	
	

}
