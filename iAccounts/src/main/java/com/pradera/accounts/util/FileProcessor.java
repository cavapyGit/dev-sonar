package com.pradera.accounts.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.pradera.accounts.assetlaundry.facade.AssetLaundryFacade;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.type.GeographicLocationType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FileProcessor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@Stateless
public class FileProcessor implements Serializable {

	/** The general parameters bean. */
	@Inject
	private GeneralParametersFacade generalParametersBean;
	//The Log

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	private static PraderaLogger log;
	
	/** The asset laundry facade. */
	@EJB
	private  AssetLaundryFacade assetLaundryFacade;

	/**
	 * Process ofac xml.
	 *
	 * @param file the file
	 * @param loggerUser the logger user
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	public  Map<String, List> processOfacXml(File file, LoggerUser loggerUser) throws ServiceException{
 		List<GeographicLocation> listGeo = assetLaundryFacade.getCountryList();
 		List<OfacPerson> listOfac = new ArrayList<OfacPerson>();
		List<OfacPerson> listOfacError = new ArrayList<OfacPerson>();
		Map<String, List> retorno = new HashMap<String, List>();

		try{
			File schemaFile;
			//Getting the .xsd for the OFAC File
			InputStream inputFile = Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/resources/xsd/sdn.xsd");
			schemaFile = File.createTempFile("sdn", ".xsd");

			FileOutputStream foStream = new FileOutputStream(schemaFile);

			foStream.write(IOUtils.toByteArray(inputFile));
			foStream.flush();
			String mnemonicOfac = "ofac";
			String currentDt=CommonsUtilities.convertDatetoString(new Date());
			String extension=".xml";
			String validPathOfac=mnemonicOfac+currentDt.replace("/", "")+extension;
			Source xmlFile = new StreamSource(new File(validPathOfac));
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(schemaFile);

			Validator validator = schema.newValidator();

			validator.validate(xmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder;
//			try {
				documentBuilder = dbFactory.newDocumentBuilder();
				Document doc = documentBuilder.parse(file);

				doc.getDocumentElement().normalize();
 
				NodeList sdnEntryList = doc.getElementsByTagName(GeneralConstants.ASSSET_LAUNDRY_SDNENTRY);

				for(int i =0; i<30; i++){
					OfacPerson ofacPerson = new OfacPerson();

					Node sdnEntry = sdnEntryList.item(i);

					if(sdnEntry.getNodeType() == Node.ELEMENT_NODE){
						Element sdnEntryElement = (Element) sdnEntry;
						String uid = com.pradera.commons.utils.GeneralConstants.EMPTY_STRING;
						String fullName = com.pradera.commons.utils.GeneralConstants.EMPTY_STRING;
						String lastName= com.pradera.commons.utils.GeneralConstants.EMPTY_STRING;
						String firstName= com.pradera.commons.utils.GeneralConstants.EMPTY_STRING;
						String entityString = com.pradera.commons.utils.GeneralConstants.EMPTY_STRING;
						String country = com.pradera.commons.utils.GeneralConstants.EMPTY_STRING;
						Integer entity =0;

						//Validating the length greater than zero means the file contains this tag
						if(sdnEntryElement.getElementsByTagName(GeneralConstants.ASSSET_LAUNDRY_UID).getLength()>0){
							uid=sdnEntryElement.getElementsByTagName(GeneralConstants.ASSSET_LAUNDRY_UID).item(0).getTextContent();
						}
						
						if(sdnEntryElement.getElementsByTagName(GeneralConstants.ASSSET_LAUNDRY_LASTNAME).getLength()>0){
							lastName=sdnEntryElement.getElementsByTagName(GeneralConstants.ASSSET_LAUNDRY_LASTNAME).item(0).getTextContent();
							fullName=sdnEntryElement.getElementsByTagName(GeneralConstants.ASSSET_LAUNDRY_LASTNAME).item(0).getTextContent();
						}

						if(sdnEntryElement.getElementsByTagName(GeneralConstants.ASSSET_LAUNDRY_FIRSTNAME).getLength()>0){
							firstName = sdnEntryElement.getElementsByTagName(GeneralConstants.ASSSET_LAUNDRY_FIRSTNAME).item(0).getTextContent();
						}

						if(sdnEntryElement.getElementsByTagName(GeneralConstants.ASSET_LAUNDRY_SDN_TYPE).getLength()>0){
							entityString=sdnEntryElement.getElementsByTagName(GeneralConstants.ASSET_LAUNDRY_SDN_TYPE).item(0).getTextContent();
							if(Validations.validateIsNotNull(entityString)){
								if(entityString.equals(GeneralConstants.ASSET_LAUNDRY_ENTITY_TYPE)){
									entity=PersonType.JURIDIC.getCode();
								}else if(entityString.equals(GeneralConstants.ASSET_LAUNDRY_INDIVIDUAL) ||
										entityString.equals(GeneralConstants.ASSET_LAUNDRY_VESSEL)){
									entity=PersonType.NATURAL.getCode();
								}
							}
						}
						//Setting ofac attributes
						ofacPerson.setOfacCode(uid);
						ofacPerson.setIdOfacPersonPk(Long.parseLong(uid));
						if(entity.equals(PersonType.JURIDIC.getCode())){
							ofacPerson.setFullName(fullName);;
						}else{
							ofacPerson.setFirstName(firstName);
							ofacPerson.setLastName(lastName);
							ofacPerson.setFullName(ofacPerson.getDescription());
						}
						ofacPerson.setPersonType(entity);
						ofacPerson.setRegistryDate(CommonsUtilities.currentDate());
						ofacPerson.setRegistryUser(loggerUser.getUserName());
 						listOfac.add(ofacPerson);
					}
				}
			} catch (ParserConfigurationException e) {
				log.info("Not valid file structure FileProcesor.processXMLFile method");
				throw new ServiceException();

			} catch (SAXException e) {
				log.info("Case not the same fileSAX");

				throw new ServiceException();

			} catch (IOException e) {
				log.info("Case not the same file IOEXCEpion");

				throw new ServiceException();
			}

//		}catch(Exception e){
//			e.printStackTrace();
//			throw new ServiceException();
//		}

		retorno.put("success", listOfac);
		retorno.put("error", listOfacError);
		return retorno;

	}
	
	/**
	 * Gets the country list.
	 *
	 * @return the country list
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getCountryList() throws ServiceException{
		GeographicLocationTO geoLocation=new GeographicLocationTO();
		geoLocation.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());

		List<GeographicLocation> countryList= new ArrayList<GeographicLocation>();

		countryList = generalParametersBean.getListGeographicLocationServiceFacade(geoLocation);

		return countryList;
	}

	/**
	 * Process excel file.
	 *
	 * @param file the file
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	public static List<String[]> processExcelFile(File file) throws ServiceException
	{
		Workbook wb;
		List<String[]> resultList = new ArrayList<String[]>();
		int index=0;
		int rowNum=0;

		try {
			wb = WorkbookFactory.create(file);
			Sheet s = wb.getSheetAt(0);
			Iterator<Row> rowIterator = s.iterator();

			while(rowIterator.hasNext()){
				rowNum++;
				  index=0;

				Row row = rowIterator.next();
				String[] result = new String[10];
				//Iterate all the cells
				Iterator<Cell> cellIterator = row.iterator();
				while(cellIterator.hasNext()){
					Cell cell = cellIterator.next();
					if(cell.getCellType()==Cell.CELL_TYPE_BLANK){
						result[index] = null;   

					}
					else if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC){
						result[index] = String.valueOf(cell.getNumericCellValue());   

					}
					else if(cell.getCellType()==Cell.CELL_TYPE_STRING){
						result[index] =  cell.getStringCellValue();   

					}				
					
					index++;

				}
				resultList.add(result);
			}
		} catch (InvalidFormatException e) {
			Object[] paramObject = new Object[1];
			paramObject[0] = rowNum;
			throw new ServiceException(ErrorServiceType.ASSET_LAUNDERING_PROCESS_MASSIVE_FILE,paramObject);
 		} catch (IOException e) {
 			Object[] paramObject = new Object[1];
			paramObject[0] = rowNum;
			throw new ServiceException(ErrorServiceType.ASSET_LAUNDERING_PROCESS_MASSIVE_FILE,paramObject);
  		} catch (Exception e) {
  			Object[] paramObject = new Object[1];
			paramObject[0] = rowNum;
			throw new ServiceException(ErrorServiceType.ASSET_LAUNDERING_PROCESS_MASSIVE_FILE,paramObject);
			}
		return resultList;

	}

}


