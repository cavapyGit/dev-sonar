package com.pradera.accounts.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que realiza la implementacion de permutaciones de una cantidad de
 * cadenas
 * 
 * @author eibanez
 * 
 */
public class Permutations implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Metodo que genera las permutaciones de una cadena con elementos separados
	 * por coma las permutaciones son sin repetticion y tomandos de la misma
	 * cantidad de las cadenas enviadas
	 * 
	 * @param cadenaPermutar
	 *            Cadena separada por comas
	 * 
	 * @return
	 */
	public static List<String> getPermutations(String cadenaPermutar) {

		List<String> listaPermutacion = generatePermutations(cadenaPermutar);

		return listaPermutacion;
	}

	/**
	 * Metodo que obtiene Permutaciones sin repeticiones de todos los
	 * 
	 * 
	 * @param cadenaPermutar
	 * @return
	 */
	private static List<String> generatePermutations(String cadenaPermutar) {

		List<String> lista = new ArrayList<String>();

		if (cadenaPermutar != null && cadenaPermutar.length() > 0) {
			String[] elementos = cadenaPermutar.split(" ");

			int n = elementos.length; // Tipos para escoger
			int r = elementos.length; // Elementos elegidos
			permutations(elementos, "", n, r, lista);
		}

		return lista;
	}

	/**
	 * 
	 * Metodo que realiza las permutaciones posibles de los elementos enviados
	 * en el arreglo
	 * 
	 * @param elem
	 *            lista elementos
	 * @param act
	 *            cadena actual
	 * @param n
	 *            numero de elementos a combinar
	 * @param r
	 *            numero de elementos combinados
	 * @param listaCombinaciones
	 *            lista de combinaciones
	 */
	private static void permutations(String[] elem, String act, int n, int r,
			List<String> listaCombinaciones) {
		if (n == 0) {
			if (act != null && act.length() > 0) {
				listaCombinaciones.add(act.substring(0, act.length() - 1));
			}
		} else {
			for (int i = 0; i < r; i++) {
				// control que no permita repetidos
				if (!act.contains(elem[i])) {
					permutations(elem, act + elem[i] + " ", n - 1, r,
							listaCombinaciones);
				}
			}
		}
	}

	/**
	 * Metodo que genera las combinaciones posibles de las palabras de una
	 * oracion
	 * 
	 * @param cadenaPermutar
	 * @return
	 */
	public static List<String> getPermutationsWordsNoSpace(String cadenaPermutar) {

        List<String> combinacionesPosiciones, combinacionesPalabras = new ArrayList<String>();
        String[] combinaciones, palabras = cadenaPermutar.split(" ");
        String nuevaPalabra, combinacionPosiciones = "", combinacionPosicionesInvertido = "", combinacionPosicionesTotal = "";
        int contador;

        contador = palabras.length -1;
        for (int i = 0; i < palabras.length; i++) {
            combinacionPosicionesTotal += i + " ";
            combinacionPosicionesInvertido += contador + " ";
            contador--;
        }

        if (palabras.length > 6) {
            //generando la cadena de numeros
            for (int i = 0; i < 5; i++) {
                combinacionPosiciones += i + " ";
            }

        } else {
            //generando la cadena de numeros
            for (int i = 0; i < palabras.length; i++) {
                combinacionPosiciones += i + " ";
            }
        }

        combinacionesPosiciones = getPermutations(combinacionPosiciones);
        
        while (combinacionPosiciones.length() > 6) {
            combinacionPosiciones = combinacionPosiciones.substring(0, combinacionPosiciones.length() - 2);
            combinacionesPosiciones.addAll(getPermutations(combinacionPosiciones));
        }
        
        combinacionesPosiciones.add(combinacionPosicionesTotal);
        combinacionesPosiciones.add(combinacionPosicionesInvertido);

        for (String posicion : combinacionesPosiciones) {
            combinaciones = posicion.split(" ");
            nuevaPalabra = "";
            for (int i = 0; i < combinaciones.length; i++) {
                nuevaPalabra += palabras[Integer.parseInt(combinaciones[i])];
            }
            if (!combinacionesPalabras.contains(nuevaPalabra)) {
                combinacionesPalabras.add(nuevaPalabra);
            }
        }
        return combinacionesPalabras;
    }

}
