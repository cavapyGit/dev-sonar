package com.pradera.accounts.holder.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.accounts.holder.facade.ShareHoldersFacade;
import com.pradera.accounts.holder.to.ShareHolderTO;
import com.pradera.accounts.util.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchShareHoldersBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
@DepositaryWebBean
public class SearchShareHoldersBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder request. */
	private Long participantId;
	
	/** The security class list. */
	private List<ParameterTable> securityClassList;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The security serie. */
	private String securitySerie;
	
	/** The issuer code. */
	private String issuerCode;
	
	/** The list result account. */
	private GenericDataModel<ShareHolderTO> listResultAccount;
	
	/** The issuer list */
	private List<Issuer> lstIssuer;
	
	/** The issuer user. */
	private boolean issuerUser;
	
	/** The list holder. */
	private List<Holder> listHolder;
	
	/** The holder account number. */
	private String holderAccountNumber;
	
	/** The map document types. */
	private Map<Integer, String> mapDocumentTypes;
	private boolean flagParticipant;
	private boolean flagIssuerBlock;
	private Long idParticipant;
	private Holder holderRequest;
	private GenericDataModel<HolderAccountHelperResultTO> lstAccountTo;
	private HolderAccountHelperResultTO selectedAccountTO;
	private HolderAccount holderAccountRequest;
	private boolean tittleVisible;
	
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The general pameters facade. */
	@EJB
	GeneralParametersFacade generalPametersFacade;

	/** The holder service facade. */
	@EJB
	HolderServiceFacade holderServiceFacade;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The share holders facade. */
	@EJB
	ShareHoldersFacade shareHoldersFacade;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		participantId=userInfo.getUserAccountSession().getParticipantCode();
		securitySerie=null;
		
		holderAccountRequest = new HolderAccount();
		
		//get actions issuer list
		lstIssuer = shareHoldersFacade.getActionsIssuer();
		
		listResultAccount=null;
		
		holderRequest = new Holder();
		flagParticipant = false;
		flagIssuerBlock = false;
		tittleVisible = false;
		
		idParticipant = userInfo.getUserAccountSession().getParticipantCode();
		
		//si es usuario emisor
		if((userInfo.getUserAccountSession().isIssuerInstitucion() && Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) ||
		   (userInfo.getUserAccountSession().isIssuerDpfInstitucion() && Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) ) {			
			issuerUser = true;
			issuerCode=userInfo.getUserAccountSession().getIssuerCode();
			flagIssuerBlock = true;
		}else{
			issuerUser = false;
		}
		
		loadSecuryClass();
	}
	
	
	
	/**
	 * Loading account information.
	 *
	 * @param event the event
	 */
	public void loadingAccountInformation(ActionEvent event) {
		
		try
		{
			Long holderAccountPk = (Long)event.getComponent().getAttributes().get("holderAccount");
			holderAccountNumber = ((Integer)event.getComponent().getAttributes().get("holderAccountNumber")).toString();
			listHolder = new ArrayList<Holder>();
			listHolder = shareHoldersFacade.getListHolder(holderAccountPk);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	
	/**
	 * Load secury class.
	 */
	public void loadSecuryClass(){
		securityClassList = new ArrayList<>();
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter = new ParameterTableTO();
		parameterFilter.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		List<ParameterTable> lstSecClasses;
		
		try {
			lstSecClasses = generalParametersFacade.getListParameterTableServiceBean(parameterFilter);
			for(ParameterTable lista:lstSecClasses){
				if(ParameterTableStateType.REGISTERED.getCode().equals(lista.getParameterState()) && 
						(lista.getParameterTablePk().intValue()==126 || 
						 lista.getParameterTablePk().intValue()==406 || 
						 lista.getParameterTablePk().intValue()==1962 ||
						 lista.getParameterTablePk().intValue()==1945 ||
						 lista.getParameterTablePk().intValue()==2246 )){
					securityClassList.add(lista);
				}
			}
				   
			  mapDocumentTypes = new HashMap<Integer, String>();
			  ParameterTableTO paramTable = new ParameterTableTO();
			  paramTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			  for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTable)) {
				  mapDocumentTypes.put(param.getParameterTablePk(), param.getIndicator1());
			  }
			   
		  } catch (ServiceException e) {
		   e.printStackTrace();
		  }
		  
	}
	
	
	public void searchHolderHandler(){	
		try {
			
			if(!Validations.validateIsNotNullAndNotEmpty(issuerCode)){
				holderRequest = new Holder();
				cleanHolderAccountBalShare();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						"Debe seleccionar el Emisor. Verifique");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			selectedAccountTO = new HolderAccountHelperResultTO();
			holderAccountRequest = new HolderAccount();
			
			listResultAccount=null;
//			executeAction();
//			JSFUtilities.hideGeneralDialogues();
			changeHolderHandler();
			Holder holder = this.getHolderRequest();			
			if (Validations.validateIsNull(holder) || Validations.validateIsNull(holder.getIdHolderPk())) {
				return;
			} 
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(holder.getIdHolderPk());
			holderTO.setFlagAllHolders(true);
//			holderTO.setParticipantFk(holderAccountBalanceTO.getIdParticipant());
			
			List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
			List<HolderAccountHelperResultTO> FinalLista= new ArrayList<>();
			for (HolderAccountHelperResultTO holderAccountTO : lista){
				if (shareHoldersFacade.verifySecurity(holderAccountTO.getParticipant().getIdParticipantPk(),holderAccountTO.getAccountPk(),issuerCode)){
					FinalLista.add(holderAccountTO);
				}
			}
			
			
			if (FinalLista==null || FinalLista.isEmpty()){
				lstAccountTo = new GenericDataModel<>();
				tittleVisible=false;
			}else{
				lstAccountTo = new GenericDataModel<>(FinalLista);
				tittleVisible=true;
			}
			
			if(lstAccountTo.getSize()<=0){
				lstAccountTo = new GenericDataModel<>();
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getGenericMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_NOT_EXIST));
				holderRequest = new Holder();
				cleanHolderAccountBalShare();
				JSFUtilities.showSimpleValidationDialog();
			}
			
//			JSFUtilities.executeJavascriptFunction("resultHelperAccountVar.clearFilters(); resultHelperAccountVar.unselectAllRows();");
//			if(FinalLista!=null && FinalLista.size()==1){
//				selectedAccountTO = FinalLista.get(0);
//				changeSelectAccount();
//			}
			
			if(FinalLista!=null && FinalLista.size()==1){
				selectedAccountTO = FinalLista.get(0);
				changeSelectAccount();
			}else if(lstAccountTo!=null){
				JSFUtilities.executeJavascriptFunction("resultHelperAccountVar.clearFilters(); resultHelperAccountVar.unselectAllRows();");
			}
			
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void changeHolderHandler(){
		JSFUtilities.resetComponent("frmHolderSearch:fldsFilters");
		this.holderAccountRequest = new HolderAccount();
//		this.holderAccountBalanceTO.setIdSecurityCode(null);
//		this.holderAccountBalanceSession = null;
//		securitiesPartHolAccount.setDescription(null);		
		lstAccountTo = null;
	}
	public void changeSelectAccount(){
		if(selectedAccountTO!=null){
			holderAccountRequest = new HolderAccount();
			holderAccountRequest.setIdHolderAccountPk(selectedAccountTO.getAccountPk());
			holderAccountRequest.setAccountNumber(selectedAccountTO.getAccountNumber());
//			joinPartHoldAccHandler();
		}
//		listResultAccount = null;
		
	}
	/**
	 * Search holder account share.
	 */
	@LoggerAuditWeb
	public void searchHolderAccountShare(){
		JSFUtilities.hideGeneralDialogues();
		try {
			
//			String issuerCode = issuerHolderQueryTO.getIssuerHelperMgmt().getIdIssuerPk();
			List<ShareHolderTO> lista = shareHoldersFacade.getListShareHolder(holderAccountRequest.getAccountNumber(), securityClass, securitySerie,issuerCode);
			
			listResultAccount = new GenericDataModel<>(lista);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
//	/**
//	 * Validate issuer.
//	 */
//	public void validateIssuer(){
//		issuerHolderQueryTO.setIdSecurityClass(null);
//		issuerHolderQueryTO.setIdSecurityCode(null);
//		issuerHolderQueryTO.setHolderAccount(new HolderAccount());
//		issuerHolderQueryTO.setHolderHelperMgmt(new Holder());
//		cleanResults();
//		
//	}
	
//	/**
//	 * Clean results.
//	 */
//	public void cleanResults(){
//		issuerHolderQueryTO.setLstIssuerHolderDetailTO(null);
//	}
	
	/**
	 * Clean holder account bal share.
	 */
	public void cleanHolderAccountBalShare(){
		JSFUtilities.resetComponent("frmHolderSearch:fielsetFilter");
		tittleVisible=false;
		securityClass = null;
		securitySerie = null;
		
		if(!userInfo.getUserAccountSession().isIssuerInstitucion()){
			//issuerCode = null;
//			issuerHolderQueryTO = new IssuerHolderQueryTO();
//			issuerHolderQueryTO = new IssuerHolderQueryTO();
//			issuerHolderQueryTO.setIssuerHelperMgmt(new Issuer());
//			issuerHolderQueryTO.setHolderHelperMgmt(new Holder());
//			issuerHolderQueryTO.setBalanceYes(true);
//			issuerHolderQueryTO.setIdSecurityCode(null);
//			issuerHolderQueryTO.setIdSecurityCode(null);
//			cleanResults();
		}
		listResultAccount = null;
		
		this.holderAccountRequest = new HolderAccount();
		holderRequest = new Holder();
		lstAccountTo = null;
		selectedAccountTO = null;
	}

	/**
	 * Gets the list holder.
	 *
	 * @return the list holder
	 */
	public List<Holder> getListHolder() {
		return listHolder;
	}

	/**
	 * Sets the list holder.
	 *
	 * @param listHolder the new list holder
	 */
	public void setListHolder(List<Holder> listHolder) {
		this.listHolder = listHolder;
	}



	/**
	 * Gets the participant id.
	 *
	 * @return the participant id
	 */
	public Long getParticipantId() {
		return participantId;
	}

	/**
	 * Sets the participant id.
	 *
	 * @param participantId the new participant id
	 */
	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}

	/**
	 * Gets the security class list.
	 *
	 * @return the security class list
	 */
	public List<ParameterTable> getSecurityClassList() {
		return securityClassList;
	}

	/**
	 * Sets the security class list.
	 *
	 * @param securityClassList the new security class list
	 */
	public void setSecurityClassList(List<ParameterTable> securityClassList) {
		this.securityClassList = securityClassList;
	}

	

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}



	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}



	/**
	 * Gets the security serie.
	 *
	 * @return the security serie
	 */
	public String getSecuritySerie() {
		return securitySerie;
	}

	/**
	 * Sets the security serie.
	 *
	 * @param securitySerie the new security serie
	 */
	public void setSecuritySerie(String securitySerie) {
		this.securitySerie = securitySerie;
	}

	

	/**
	 * Gets the list result account.
	 *
	 * @return the list result account
	 */
	public GenericDataModel<ShareHolderTO> getListResultAccount() {
		return listResultAccount;
	}



	/**
	 * Sets the list result account.
	 *
	 * @param listResultAccount the new list result account
	 */
	public void setListResultAccount(
			GenericDataModel<ShareHolderTO> listResultAccount) {
		this.listResultAccount = listResultAccount;
	}



	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}



	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}



//	/**
//	 * Gets the issuer holder query to.
//	 *
//	 * @return the issuer holder query to
//	 */
//	public IssuerHolderQueryTO getIssuerHolderQueryTO() {
//		return issuerHolderQueryTO;
//	}
//
//
//
//	/**
//	 * Sets the issuer holder query to.
//	 *
//	 * @param issuerHolderQueryTO the new issuer holder query to
//	 */
//	public void setIssuerHolderQueryTO(IssuerHolderQueryTO issuerHolderQueryTO) {
//		this.issuerHolderQueryTO = issuerHolderQueryTO;
//	}



	/**
	 * Checks if is issuer user.
	 *
	 * @return true, if is issuer user
	 */
	public boolean isIssuerUser() {
		return issuerUser;
	}



	/**
	 * Sets the issuer user.
	 *
	 * @param issuerUser the new issuer user
	 */
	public void setIssuerUser(boolean issuerUser) {
		this.issuerUser = issuerUser;
	}



	/**
	 * Gets the holder account number.
	 *
	 * @return the holder account number
	 */
	public String getHolderAccountNumber() {
		return holderAccountNumber;
	}



	/**
	 * Sets the holder account number.
	 *
	 * @param holderAccountNumber the new holder account number
	 */
	public void setHolderAccountNumber(String holderAccountNumber) {
		this.holderAccountNumber = holderAccountNumber;
	}



	/**
	 * Gets the map document types.
	 *
	 * @return the map document types
	 */
	public Map<Integer, String> getMapDocumentTypes() {
		return mapDocumentTypes;
	}



	/**
	 * Sets the map document types.
	 *
	 * @param mapDocumentTypes the map document types
	 */
	public void setMapDocumentTypes(Map<Integer, String> mapDocumentTypes) {
		this.mapDocumentTypes = mapDocumentTypes;
	}
	
	public boolean isFlagParticipant() {
		return flagParticipant;
	}



	public void setFlagParticipant(boolean flagParticipant) {
		this.flagParticipant = flagParticipant;
	}



	public boolean isFlagIssuerBlock() {
		return flagIssuerBlock;
	}



	public void setFlagIssuerBlock(boolean flagIssuerBlock) {
		this.flagIssuerBlock = flagIssuerBlock;
	}



	public Long getIdParticipant() {
		return idParticipant;
	}



	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}



	public Holder getHolderRequest() {
		return holderRequest;
	}



	public void setHolderRequest(Holder holderRequest) {
		this.holderRequest = holderRequest;
	}



	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountTo() {
		return lstAccountTo;
	}



	public void setLstAccountTo(
			GenericDataModel<HolderAccountHelperResultTO> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}



	public HolderAccountHelperResultTO getSelectedAccountTO() {
		return selectedAccountTO;
	}



	public void setSelectedAccountTO(HolderAccountHelperResultTO selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}



	public boolean isTittleVisible() {
		return tittleVisible;
	}



	public void setTittleVisible(boolean tittleVisible) {
		this.tittleVisible = tittleVisible;
	}

	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}

	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}

	public String getIssuerCode() {
		return issuerCode;
	}
	
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	
}
