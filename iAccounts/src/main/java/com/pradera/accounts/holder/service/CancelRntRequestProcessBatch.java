package com.pradera.accounts.holder.service;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.accounts.holder.facade.HolderServiceFacade;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.ProcessLogger;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class OfacMassiveRegisterProcess.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Mar 21, 2013
 */
@BatchProcess(name="CancelRntRequestProcessBatch")
@RequestScoped
public class CancelRntRequestProcessBatch implements JobExecution,Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The asset laundry facade. */
	@Inject
	HolderServiceFacade holderServiceFacade;
	
    /** The holder service bean. */
    @Inject
	HolderServiceBean holderServiceBean;
	
    /** The list indicator. */
    private List<ParameterTable> listIndicator;
    
    /** The general parameters facade. */
    @Inject
	GeneralParametersFacade generalParametersFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		
		try {
		ParameterTableTO parameterTableTO = new ParameterTableTO();

		parameterTableTO.setState(ParameterTableStateType.REGISTERED
				.getCode());
		parameterTableTO
				.setMasterTableFk(MasterTableType.MAX_DAYS_HOLDER_REQUEST
						.getCode());

	
			listIndicator = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		
	
     	Integer indicatorMaxDays = Integer.valueOf(listIndicator.get(0).getLongInteger().toString());
     	
     	holderServiceFacade.CancelRntRequest(indicatorMaxDays);
         
	    
	}catch (ServiceException e) {
	    log.info(e.getMessage());
	}

}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}