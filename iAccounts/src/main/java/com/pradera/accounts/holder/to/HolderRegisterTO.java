package com.pradera.accounts.holder.to;

import java.util.List;

import com.pradera.integration.component.accounts.to.PersonObjectTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.Participant;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderRegisterTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class HolderRegisterTO {
	
	/** The person object to. */
	private PersonObjectTO personObjectTO; // related person object;
	
	/** The holder request. */
	private HolderRequest holderRequest;
	
	/** The participant. */
	private Participant participant;
	
	/** The holder history. */
	private HolderHistory holderHistory;
	
	/** The holder. */
	private Holder holder;
	
	/** The lst legal representative histories. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistories;
	
	/** The exist holder. */
	private boolean existHolder;
	
	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public HolderRequest getHolderRequest() {
		return holderRequest;
	}
	
	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the holder history.
	 *
	 * @return the holder history
	 */
	public HolderHistory getHolderHistory() {
		return holderHistory;
	}
	
	/**
	 * Sets the holder history.
	 *
	 * @param holderHistory the new holder history
	 */
	public void setHolderHistory(HolderHistory holderHistory) {
		this.holderHistory = holderHistory;
	}
	
	/**
	 * Gets the lst legal representative histories.
	 *
	 * @return the lst legal representative histories
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistories() {
		return lstLegalRepresentativeHistories;
	}
	
	/**
	 * Sets the lst legal representative histories.
	 *
	 * @param lstLegalRepresentativeHistories the new lst legal representative histories
	 */
	public void setLstLegalRepresentativeHistories(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistories) {
		this.lstLegalRepresentativeHistories = lstLegalRepresentativeHistories;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the person object to.
	 *
	 * @return the person object to
	 */
	public PersonObjectTO getPersonObjectTO() {
		return personObjectTO;
	}
	
	/**
	 * Sets the person object to.
	 *
	 * @param personObjectTO the new person object to
	 */
	public void setPersonObjectTO(PersonObjectTO personObjectTO) {
		this.personObjectTO = personObjectTO;
	}
	
	/**
	 * Checks if is exist holder.
	 *
	 * @return true, if is exist holder
	 */
	public boolean isExistHolder() {
		return existHolder;
	}
	
	/**
	 * Sets the exist holder.
	 *
	 * @param existHolder the new exist holder
	 */
	public void setExistHolder(boolean existHolder) {
		this.existHolder = existHolder;
	}
	
}
