package com.pradera.accounts.holder.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.accounts.HolderRequest;





// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderRequestDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/08/2015
 */
public class HolderRequestDataModel extends ListDataModel<HolderRequest> implements SelectableDataModel<HolderRequest> , Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new holder request data model.
     */
    public HolderRequestDataModel(){
		
	}
	
	/**
	 * Instantiates a new holder request data model.
	 *
	 * @param data the data
	 */
	public HolderRequestDataModel(List<HolderRequest> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public HolderRequest getRowData(String rowKey) {
		List<HolderRequest> holderRequestList=(List<HolderRequest>)getWrappedData();
        for(HolderRequest holderRequest : holderRequestList) {  
        	
            if(String.valueOf(holderRequest.getIdHolderRequestPk()).equals(rowKey))
                return holderRequest;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(HolderRequest holderRequest) {
		return holderRequest.getIdHolderRequestPk();
	}		

}
