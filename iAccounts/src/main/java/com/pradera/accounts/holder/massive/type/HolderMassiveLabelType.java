package com.pradera.accounts.holder.massive.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum HolderMassiveLabelType {
	TIPO_DE_TITULAR("TipoDeInversionista"),
	PRIMER_APELLIDO("PrimerApellido"),
	SEGUNDO_APELLIDO("SegundoApellido"),
	NOMBRES("Nombres"),
	CORREO("Correo"),
	TIPO_DE_DOCUMENTO("TipoDeDocumento"),
	NUMERO_DE_DOCUMENTO("NumeroDeDocumento"),
	NACIONALIDAD("Nacionalidad"),
	ES_RESIDENTE("EsResidente"),
	PAIS("Pais"),
	DEPARTAMENTO("Departamento"),
	CIUDAD("Ciudad"),
	SEXO("Sexo"),
	FECHA_DE_NACIMIENTO("FechaDeNacimiento"),
	TIPO_DE_CUENTA_INVERSIONISTA("TipoDeCuentaInversionistas"),
	CUENTA_INVERSIONISTA_EN_DEPOSITANTE("CuentaInversionistaEnDepositante"),
	FATCA("FATCA"),
	BANCO_GUARANIES("BancoGuaranies"),
	TIPO_DE_CUENTA_BANCARIA_GUARANIES("TipoDeCuentaBancariaGuaranies"),
	NUMERO_CUENTA_BANCARIA_GUARANIES("NumeroCuentaBancariaGuaranies"),
	BANCO_DOLARES("BancoDolares"),
	TIPO_DE_CUENTA_BANCARIA_DOLARES("TipoDeCuentaBancariaDolares"),
	NUMERO_CUENTA_BANCARIA_DOLARES("NumeroCuentaBancariaDolares"),
	CODIGO_BURSATIL("CodigoBursatil"),
	NACIONALIDAD_REPRESENTANTE("NacionalidadRepresentante"),
	REPRESENTANTE_ES_RESIDENTE("RepresentanteEsResidente"),
	PAIS_REPRESENTANTE("PaisRepresentante"),
	DEPARTAMENTO_REPRESENTANTE("DepartamentoRepresentante"),
	CIUDAD_REPRESENTANTE("CiudadRepresentante"),
	TIPO_DE_DOCUMENTO_REPRESENTANTE("TipoDocumentoRepresentante"),
	NUMERO_DE_DOCUMENTO_REPRESENTANTE("NumeroDeDocumentoRepresentante"),
	PRIMER_APELLIDO_REPRESENTANTE("PrimerApellidoRepresentante"),
	SEGUNDO_APELLIDO_REPRESENTANTE("SegundoApellidoRepresentante"),
	NOMBRE_DEL_REPRESENTANTE("NombreDelRepresentante"),
	RAZON_SOCIAL_NOMBRES_COMPLETOS_REPRESENTANTE("RazonSocialNombresCompletosRepresentante"),
	CORREO_REPRESENTANTE("CorreoRepresentante"),
	SEXO_REPRESENTANTE("SexoRepresentante"),
	TIPO_DE_REPRESENTANTE("TipoDeRepresentante"),
	RAZON_SOCIAL_NOMBRE_COMPLETO("RazonSocialNombreCompleto"),
	SECTOR_ECONOMICO("SectorEconomico"),
	ACTIVIDAD_ECONOMICA("ActividadEconomica"),
	PDD("PDD"),
	NOMBRE_RELACIONADO("NombreRelacionado"),
	CLASE_JURIDICO("ClaseJuridico"),
	PEP("PEP"),
	MOTIVO_PEP("MotivoPEP"),
	CARGO_PEP("CargoPEP"),
	CLASE_REPRESENTANTE("ClaseRepresentante"),
	GRADO_PEP("GradoPEP"),
	NOMBRE_PEP("NombrePEP"),
	PERIODO_DESDE("PeriodoDesde"),
	PERIODO_HASTA("PeriodoHasta"),
	;
	
	HolderMassiveLabelType(String name){
		this.name = name;
	}
	
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static final List<HolderMassiveLabelType> list = new ArrayList<HolderMassiveLabelType>();
	
	/** The Constant lookup. */
	public static final Map<String, HolderMassiveLabelType> lookup = new HashMap<String, HolderMassiveLabelType>();
	static {
		for (HolderMassiveLabelType s : EnumSet.allOf(HolderMassiveLabelType.class)) {
			list.add(s);
			lookup.put(s.getName(), s);
		}
	}
	
	public static HolderMassiveLabelType get(String name) {
		return lookup.get(name);
	}
}
