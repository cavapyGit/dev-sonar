package com.pradera.accounts.holder.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PreviousModificationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class PreviousModificationTO implements Serializable {
	

/** The previous nationality. */
private	Integer previousNationality;

/** The previous second nationality. */
private Integer previousSecondNationality;

/** The previous residence. */
private	Integer previousResidence;

/** The previous document type. */
private	Integer previousDocumentType;

/** The previous document number. */
private String  previousDocumentNumber;

/** The previous second document type. */
private Integer previousSecondDocumentType;

/** The previous second document number. */
private String previousSecondDocumentNumber;

/** The previous birth date. */
private    Date previousBirthDate;

/** The previous incapacity. */
private	Integer previousIncapacity; 

/** The previous country resident. */
private Integer previousCountryResident;

/** The previous legal address. */
private String previousLegalAddress;

/** The previous postal address. */
private String previousPostalAddress;

/** The previous max lenght document number holder. */
private Integer previousMaxLenghtDocumentNumberHolder;

/** The previous indicator minor. */
private boolean previousIndicatorMinor;





/**
 * Gets the previous nationality.
 *
 * @return the previous nationality
 */
public Integer getPreviousNationality() {
	return previousNationality;
}

/**
 * Sets the previous nationality.
 *
 * @param previousNationality the new previous nationality
 */
public void setPreviousNationality(Integer previousNationality) {
	this.previousNationality = previousNationality;
}

/**
 * Gets the previous residence.
 *
 * @return the previous residence
 */
public Integer getPreviousResidence() {
	return previousResidence;
}

/**
 * Sets the previous residence.
 *
 * @param previousResidence the new previous residence
 */
public void setPreviousResidence(Integer previousResidence) {
	this.previousResidence = previousResidence;
}

/**
 * Gets the previous document type.
 *
 * @return the previous document type
 */
public Integer getPreviousDocumentType() {
	return previousDocumentType;
}

/**
 * Sets the previous document type.
 *
 * @param previousDocumentType the new previous document type
 */
public void setPreviousDocumentType(Integer previousDocumentType) {
	this.previousDocumentType = previousDocumentType;
}

/**
 * Gets the previous birth date.
 *
 * @return the previous birth date
 */
public Date getPreviousBirthDate() {
	return previousBirthDate;
}

/**
 * Sets the previous birth date.
 *
 * @param previousBirthDate the new previous birth date
 */
public void setPreviousBirthDate(Date previousBirthDate) {
	this.previousBirthDate = previousBirthDate;
}

/**
 * Gets the previous incapacity.
 *
 * @return the previous incapacity
 */
public Integer getPreviousIncapacity() {
	return previousIncapacity;
}

/**
 * Sets the previous incapacity.
 *
 * @param previousIncapacity the new previous incapacity
 */
public void setPreviousIncapacity(Integer previousIncapacity) {
	this.previousIncapacity = previousIncapacity;
}

/**
 * Gets the previous country resident.
 *
 * @return the previous country resident
 */
public Integer getPreviousCountryResident() {
	return previousCountryResident;
}

/**
 * Sets the previous country resident.
 *
 * @param previousCountryResident the new previous country resident
 */
public void setPreviousCountryResident(Integer previousCountryResident) {
	this.previousCountryResident = previousCountryResident;
}

/**
 * Gets the previous document number.
 *
 * @return the previous document number
 */
public String getPreviousDocumentNumber() {
	return previousDocumentNumber;
}

/**
 * Sets the previous document number.
 *
 * @param previousDocumentNumber the new previous document number
 */
public void setPreviousDocumentNumber(String previousDocumentNumber) {
	this.previousDocumentNumber = previousDocumentNumber;
}

/**
 * Gets the previous legal address.
 *
 * @return the previous legal address
 */
public String getPreviousLegalAddress() {
	return previousLegalAddress;
}

/**
 * Sets the previous legal address.
 *
 * @param previousLegalAddress the new previous legal address
 */
public void setPreviousLegalAddress(String previousLegalAddress) {
	this.previousLegalAddress = previousLegalAddress;
}

/**
 * Gets the previous postal address.
 *
 * @return the previous postal address
 */
public String getPreviousPostalAddress() {
	return previousPostalAddress;
}

/**
 * Sets the previous postal address.
 *
 * @param previousPostalAddress the new previous postal address
 */
public void setPreviousPostalAddress(String previousPostalAddress) {
	this.previousPostalAddress = previousPostalAddress;
}

/**
 * Gets the previous max lenght document number holder.
 *
 * @return the previous max lenght document number holder
 */
public Integer getPreviousMaxLenghtDocumentNumberHolder() {
	return previousMaxLenghtDocumentNumberHolder;
}

/**
 * Sets the previous max lenght document number holder.
 *
 * @param previousMaxLenghtDocumentNumberHolder the new previous max lenght document number holder
 */
public void setPreviousMaxLenghtDocumentNumberHolder(
		Integer previousMaxLenghtDocumentNumberHolder) {
	this.previousMaxLenghtDocumentNumberHolder = previousMaxLenghtDocumentNumberHolder;
}

/**
 * Checks if is previous indicator minor.
 *
 * @return true, if is previous indicator minor
 */
public boolean isPreviousIndicatorMinor() {
	return previousIndicatorMinor;
}

/**
 * Sets the previous indicator minor.
 *
 * @param previousIndicatorMinor the new previous indicator minor
 */
public void setPreviousIndicatorMinor(boolean previousIndicatorMinor) {
	this.previousIndicatorMinor = previousIndicatorMinor;
}

/**
 * Gets the previous second nationality.
 *
 * @return the previous second nationality
 */
public Integer getPreviousSecondNationality() {
	return previousSecondNationality;
}

/**
 * Sets the previous second nationality.
 *
 * @param previousSecondNationality the new previous second nationality
 */
public void setPreviousSecondNationality(Integer previousSecondNationality) {
	this.previousSecondNationality = previousSecondNationality;
}

/**
 * Gets the previous second document type.
 *
 * @return the previous second document type
 */
public Integer getPreviousSecondDocumentType() {
	return previousSecondDocumentType;
}

/**
 * Sets the previous second document type.
 *
 * @param previousSecondDocumentType the new previous second document type
 */
public void setPreviousSecondDocumentType(Integer previousSecondDocumentType) {
	this.previousSecondDocumentType = previousSecondDocumentType;
}

/**
 * Gets the previous second document number.
 *
 * @return the previous second document number
 */
public String getPreviousSecondDocumentNumber() {
	return previousSecondDocumentNumber;
}

/**
 * Sets the previous second document number.
 *
 * @param previousSecondDocumentNumber the new previous second document number
 */
public void setPreviousSecondDocumentNumber(String previousSecondDocumentNumber) {
	this.previousSecondDocumentNumber = previousSecondDocumentNumber;
}





 
}
