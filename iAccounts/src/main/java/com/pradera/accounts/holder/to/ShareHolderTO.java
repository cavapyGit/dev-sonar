package com.pradera.accounts.holder.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ShareHolderTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class ShareHolderTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The account type. */
	private String accountType;
	
	/** The security code. */
	private String securityCode;
	
	/** The available balance. */
	private BigDecimal availableBalance;
	
	/** The blocket balance. */
	private BigDecimal blocketBalance;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The participante. */
	private String participante;
	
	/** The holder account pk. */
	private Long holderAccountPk;
	
	/** The participant description. */
	private String participantDescription;
	
	/** The participant code. */
	private String participantCode;

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the account type.
	 *
	 * @return the account type
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * Sets the account type.
	 *
	 * @param accountType the new account type
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * Gets the blocket balance.
	 *
	 * @return the blocket balance
	 */
	public BigDecimal getBlocketBalance() {
		return blocketBalance;
	}

	/**
	 * Sets the blocket balance.
	 *
	 * @param blocketBalance the new blocket balance
	 */
	public void setBlocketBalance(BigDecimal blocketBalance) {
		this.blocketBalance = blocketBalance;
	}

	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	/**
	 * Gets the participante.
	 *
	 * @return the participante
	 */
	public String getParticipante() {
		return participante;
	}

	/**
	 * Sets the participante.
	 *
	 * @param participante the new participante
	 */
	public void setParticipante(String participante) {
		this.participante = participante;
	}

	/**
	 * Gets the holder account pk.
	 *
	 * @return the holder account pk
	 */
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}

	/**
	 * Sets the holder account pk.
	 *
	 * @param holderAccountPk the new holder account pk
	 */
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}

	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
		return participantDescription;
	}

	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}

	/**
	 * Gets the participant code.
	 *
	 * @return the participant code
	 */
	public String getParticipantCode() {
		return participantCode;
	}

	/**
	 * Sets the participant code.
	 *
	 * @param participantCode the new participant code
	 */
	public void setParticipantCode(String participantCode) {
		this.participantCode = participantCode;
	}
	
	
}
